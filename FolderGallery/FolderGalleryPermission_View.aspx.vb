﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UserControls_usrMessageBar

Partial Class FolderGallery_FolderGalleryPermission_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public PID_QueryString As String = ""
    Public Popup_Message As String = ""
    Public Popup_Message_Status As WarningType
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Check_If_Admin() Then

            DIV_Permission_Folder.Visible = True

        Else
            Response.Redirect("/login.aspx")
        End If
        If Page.IsPostBack = False Then
            GridBindPayHist()
        End If

    End Sub
    Private Sub GridBindPayHist()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds_Folder As New DataSet
        Dim ds_Files As New DataSet
        Dim Parent_Id As String = 0
        Dim Paret_Path As String = ""
        Try

            Using objConn
                Dim Home As String = "<li><a id='btnRootFolder' href='/PHOENIXBETA/FolderGallery/FolderGalleryView.aspx'><i class='fas fa-layer-group'></i></a></li>"
                Dim PermissionTitle As String = "<li class='active'><span> Permission </span></li>"
                If Request.QueryString("pfid") Is Nothing Then
                    'Parent_Id =

                    Dim pParms_Parent(3) As SqlClient.SqlParameter
                    pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                    pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                    Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
                    folder_nav.InnerHtml = Home + PermissionTitle
                    PID_QueryString = ""
                Else
                    Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))

                    Dim test As String = Get_Bread_crumbs(Parent_Id, Parent_Id, str_conn, "")

                    folder_nav.InnerHtml = Home + test + PermissionTitle
                    PID_QueryString = "?pfid=" + Request.QueryString("pfid").Replace(" ", "+")

                End If
                call_gvStuffPermission_Bind(Parent_Id)
                call_gvParentPermission_Bind(Parent_Id)

            End Using

        Catch ex As Exception
            Dim test As String = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Function Get_Bread_crumbs(id As String, current_id As String, str_conn As String, Full_URL As String) As String
        Try
            Dim result As String = ""
            Dim URL As String = ""
            Dim FET_DESCR As String = String.Empty
            Dim FET_ID As String = String.Empty
            Dim FET_PARENT_ID As String = String.Empty
            Dim pParms_Parent(4) As SqlClient.SqlParameter
            pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            pParms_Parent(2) = New SqlClient.SqlParameter("@FET_ID", id)
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_FILE_EXPLORER_PARENT_data", pParms_Parent)
                While reader.Read
                    FET_DESCR = Convert.ToString(reader("FET_DISPLAY_NAME"))
                    FET_ID = Convert.ToString(reader("FET_ID"))
                    FET_PARENT_ID = Convert.ToString(reader("FET_PARENT_ID"))

                    If FET_PARENT_ID <> "0" Then
                        'If id = current_id Then
                        '    URL = "<li class='active'><span>" + FET_DESCR + "</span></li>"
                        'Else
                        URL = "<li><a id='btnViewFolder' href='/FolderGallery/FolderGalleryView.aspx?pfid=" + Encr_id(FET_ID) + "' >" + FET_DESCR + "</a></li>"
                        'End If
                        Full_URL = URL + Full_URL
                        Full_URL = Get_Bread_crumbs(FET_PARENT_ID, current_id, str_conn, Full_URL)

                    End If

                End While
            End Using
            result = Full_URL
            Return result
        Catch ex As Exception

        End Try
    End Function
    Function Encr_id(id As String) As String
        Dim Enc_ID As String = ""
        Enc_ID = Encr_decrData.Encrypt(id)
        Return Enc_ID
    End Function
    Public Sub call_gvStuffPermission_Bind(ByVal Parent_ID As Integer)
        Try


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Dim PARAM(4) As SqlParameter

            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@FPT_USER_TYPE", "STAFF")
            PARAM(2) = New SqlParameter("@FPT_FET_ID", Parent_ID)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_FILE_PERMISSION_DATA_STAFF", PARAM)
            gvStuffPermission.DataSource = ds
            gvStuffPermission.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Public Sub call_gvParentPermission_Bind(ByVal Parent_ID As Integer)
        Try


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Dim PARAM(4) As SqlParameter

            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@FPT_USER_TYPE", "Parent")
            PARAM(2) = New SqlParameter("@FPT_FET_ID", Parent_ID)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_FILE_PERMISSION_DATA_PARENT", PARAM)
            gvParentPermission.DataSource = ds
            gvParentPermission.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub gvStuffPermission_PageIndexChanged(sender As Object, e As EventArgs)

    End Sub
    Protected Sub gvParentPermission_PageIndexChanged(sender As Object, e As EventArgs)

    End Sub
    Function Check_If_Admin() As Boolean
        Dim Flag As Boolean = False
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FUPA_BSU_ID", Session("sBsuid"))
            pParms(1) = New SqlClient.SqlParameter("@FUPA_USER_NAME_ID", Session("sUsr_name"))
            Flag = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[Check_If_Admin]", pParms)
        End Using
        Return Flag
    End Function
    Protected Sub btnDeleteStaff_Click(sender As Object, e As EventArgs)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim chk As CheckBox
            Dim FPT_ID As String = String.Empty
            Dim Parent_Id As String = String.Empty

            For Each rowItem As GridViewRow In gvStuffPermission.Rows

                chk = DirectCast(rowItem.FindControl("chkSelect1"), CheckBox)

                FPT_ID = DirectCast(rowItem.FindControl("lblFPT_ID"), Label).Text
                Parent_Id = DirectCast(rowItem.FindControl("lblFPT_FET_ID"), Label).Text
                If chk.Checked = True Then
                    Dim pParms_Folder(5) As SqlClient.SqlParameter
                    pParms_Folder(0) = New SqlClient.SqlParameter("@FPT_ID", FPT_ID)
                    pParms_Folder(1) = New SqlClient.SqlParameter("@FPT_FET_ID", Parent_Id)
                    pParms_Folder(2) = New SqlClient.SqlParameter("@FPT_BSU_ID", Session("sBsuid"))
                    pParms_Folder(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms_Folder(3).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Delete_FILE_PERMISSION_DATA", pParms_Folder)
                    Dim ReturnFlag As Integer = pParms_Folder(3).Value
                End If
            Next
            stTrans.Commit()
            Popup_Message = "Permission has been Deleted successfully"
            Popup_Message_Status = WarningType.Success
            call_gvStuffPermission_Bind(Parent_Id)
        Catch ex As Exception
            stTrans.Rollback()
            Popup_Message = "Permission has been not Deleted successfully  Please try again  "
            Popup_Message_Status = WarningType.Danger
        Finally
            objConn.Close()
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try
    End Sub
    Protected Sub btnDeleteParent_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim chk As CheckBox
            Dim FPT_ID As String = String.Empty
            Dim Parent_Id As String = String.Empty

            For Each rowItem As GridViewRow In gvParentPermission.Rows

                chk = DirectCast(rowItem.FindControl("chkSelect2"), CheckBox)

                FPT_ID = DirectCast(rowItem.FindControl("lblFPT_ID"), Label).Text
                Parent_Id = DirectCast(rowItem.FindControl("lblFPT_FET_ID"), Label).Text
                If chk.Checked = True Then
                    Dim pParms_Folder(5) As SqlClient.SqlParameter
                    pParms_Folder(0) = New SqlClient.SqlParameter("@FPT_ID", FPT_ID)
                    pParms_Folder(1) = New SqlClient.SqlParameter("@FPT_FET_ID", Parent_Id)
                    pParms_Folder(2) = New SqlClient.SqlParameter("@FPT_BSU_ID", Session("sBsuid"))
                    pParms_Folder(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms_Folder(3).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Delete_FILE_PERMISSION_DATA", pParms_Folder)
                    Dim ReturnFlag As Integer = pParms_Folder(3).Value
                End If
            Next
            stTrans.Commit()
            Popup_Message = "Permission has been Deleted successfully"
            Popup_Message_Status = WarningType.Success
            call_gvParentPermission_Bind(Parent_Id)
        Catch ex As Exception
            stTrans.Rollback()
            Popup_Message = "Permission has been not Deleted successfully  Please try again  "
            Popup_Message_Status = WarningType.Danger
        Finally
            objConn.Close()
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try
    End Sub
End Class
