﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FolderGalleryPermission_View.aspx.vb" Inherits="FolderGallery_FolderGalleryPermission_View" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
    <link href="/PHOENIXBETA/FolderGallery/css/main-teacher.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/FolderGallery/css/all.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/EnquiryStyle.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/FolderGallery/css/component.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/build.css" rel="stylesheet" />
    <style>
        .RB_Holder label label strong {
            color: #679c26;
        }
       td.width-10
        {
            width:10%;
        }
         td.width-40
        {
            width:40%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <i class="fa fa-book mr-3"></i>
                    Folder Gallery Permission
      
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  text-right">
                </div>



            </div>
        </div>


        <div class="card-body">
            <div class="container-fluid p-0 mt-2 mb-4">
                <div class="container-fluid p-0 mt-2 mb-4 clearfix position-relative teachers-lockers">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div class="card min-height">
                                <div class="card-body">
                                       <div class="task-search column-padding pt-3 pb-3">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-7 col-12">
                                              
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-12 text-right">
                                              
                                                <div style="display: inline-block;" runat="server" id="DIV_Permission_Folder" visible="false">
                                                  <a href="javascript:void(0)" class="btn btn-primary dropdown-toggle mr-0 mt-lg-0 mb-0 mt-md-0 waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Permission</a>
                                                <div class="dropdown-menu z-depth-1 border-0">
                                                    <a id="btnViewPermission" class="dropdown-item pt-2 pb-2" href="/PHOENIXBETA/FolderGallery/FolderGalleryPermission_View.aspx<%=PID_QueryString %>">View</a>
                                                    <a id="btnAddPermission" class="dropdown-item pt-2 pb-2" href="/PHOENIXBETA/FolderGallery/FolderGalleryPermission.aspx<%=PID_QueryString %>" >Add</a>
                                                </div></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="add-task column-padding pt-3 pb-0">
                                        <div class="row ">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                                                <ol id="folder_nav" class="breadcrumb breadcrumb-arrow" runat="server">
                                                </ol>

                                            </div>

                                        </div>
                                    </div>
                                    <!-- library content start -->
                                    <div class="library-data column-padding pt-3 pb-3">
                                        <div id="library" class="row view-group">
                                            <div class="col-lg-12 col-md-12 col-12">
                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>View Staff</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">

                                                            <asp:GridView ID="gvStuffPermission" runat="server" AllowPaging="True" AutoGenerateColumns="False" Style="font-size: 12px!important"
                                                                CssClass="table table-striped table-bordered  m-0 table-row" EmptyDataText="No Records Found" HeaderStyle-Height="30"
                                                                PageSize="20" DataKeyNames="FPT_ID" OnPageIndexChanged="gvStuffPermission_PageIndexChanged">
                                                                <RowStyle CssClass="griditem" />

                                                                <Columns>
                                                                      <asp:TemplateField HeaderText="Available" >
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <HeaderTemplate>

                                <asp:CheckBox ID="chkAll1" onclick="javascript:change_chk_state1(this);" runat="server"
                                    ToolTip="Click here to select/deselect all rows" Text=""></asp:CheckBox>


                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:CheckBox ID="chkSelect1" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>

                            </ItemTemplate>
                            <HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="FPT_ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_ID" runat="server" Text='<%# Bind("FPT_ID") %>'></asp:Label>
                                                                               <asp:Label ID="lblFPT_FET_ID" runat="server" Text='<%# Bind("FPT_FET_ID") %>'></asp:Label>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="User Name">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblUSR_DISPLAY_NAME" runat="server" Text='<%# Bind("USR_DISPLAY_NAME") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="View Folder">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_VIEW" runat="server" Text='<%# Bind("FPT_VIEW") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <ItemStyle CssClass="width-10" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Add Folder">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_ADD" runat="server" Text='<%# Bind("FPT_ADD") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <ItemStyle CssClass="width-10" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Rename Folder">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_RENAME" runat="server" Text='<%# Bind("FPT_RENAME") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <ItemStyle CssClass="width-10" />
                                                                        <HeaderStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Delete Folder">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_DELETE" runat="server" Text='<%# Bind("FPT_DELETE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <ItemStyle CssClass="width-10" />
                                                                        <HeaderStyle />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>

                                                        </div>
                                                    </div>
                                                        <div class="col-md-12 text-center mt-3">
                                                                <asp:LinkButton Text="Delete Staff Permission" OnClientClick="if ( ! UserDeleteConfirmation()) return false;"  runat="server" ID="btnDeleteStaff" CssClass="btn btn-danger " OnClick="btnDeleteStaff_Click" />

                                                            </div>
                                                </div>
                                                <div class="row mt-3">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>View Parent</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">

                                                            <asp:GridView ID="gvParentPermission" runat="server" AllowPaging="True" AutoGenerateColumns="False" Style="font-size: 12px!important"
                                                                CssClass="table table-striped table-bordered  m-0 table-row" EmptyDataText="No Records Found" HeaderStyle-Height="30"
                                                                PageSize="20" DataKeyNames="FPT_ID" OnPageIndexChanged="gvParentPermission_PageIndexChanged">
                                                                <RowStyle CssClass="griditem" />
                                                                <Columns>
                                                                      <asp:TemplateField HeaderText="Available" >
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <HeaderTemplate>

                                <asp:CheckBox ID="chkAll2" onclick="javascript:change_chk_state2(this);" runat="server"
                                    ToolTip="Click here to select/deselect all rows" Text=""></asp:CheckBox>


                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:CheckBox ID="chkSelect2" onclick="javascript:highlight2(this);" runat="server"></asp:CheckBox>

                            </ItemTemplate>
                            <HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="FPT_ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_ID" runat="server" Text='<%# Bind("FPT_ID") %>'></asp:Label>
                                                                              <asp:Label ID="lblFPT_FET_ID" runat="server" Text='<%# Bind("FPT_FET_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Grade">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_GRM_DESCR" runat="server" Text='<%# Eval("GRM_DISPLAY") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="Section">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_SCT_DESCR" runat="server" Text='<%# Eval("SCT_DESCR") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="View Folder">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_VIEW" runat="server" Text='<%# Bind("FPT_VIEW") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="lblwidth-40" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Add Folder" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_ADD" runat="server" Text='<%# Bind("FPT_ADD") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <ItemStyle CssClass="width-10" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Rename Folder" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_RENAME" runat="server" Text='<%# Bind("FPT_RENAME") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <ItemStyle CssClass="width-10" />
                                                                        <HeaderStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Delete Folder" Visible="false">

                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFPT_DELETE" runat="server" Text='<%# Bind("FPT_DELETE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <ItemStyle CssClass="width-10" />
                                                                        <HeaderStyle />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>

                                                        </div>
                                                    </div>
                                                       <div class="col-md-12 text-center mt-3">
                                                                <asp:LinkButton Text="Delete Parent Permission" OnClientClick="if ( ! UserDeleteConfirmation()) return false;"  runat="server" ID="btnDeleteParent" CssClass="btn btn-danger " OnClick="btnDeleteParent_Click" />

                                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    <script>
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStuffPermission.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
                function highlight2(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvParentPermission.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


    function change_chk_state1(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect1") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click(); //fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }
         function change_chk_state2(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect2") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click(); //fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }
        function UserDeleteConfirmation() {
    return confirm("Are you sure you want to delete this row(s)?");
}
    </script>
</asp:Content>

