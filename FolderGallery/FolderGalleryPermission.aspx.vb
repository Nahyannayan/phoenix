﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UserControls_usrMessageBar
Imports StorageHelper
Imports System.Linq
Partial Class FolderGallery_FolderGalleryPermission
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Popup_Message As String = ""
    Public Popup_Message_Status As WarningType
    Public PID_QueryString As String = ""
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Check_If_Admin() Then

            DIV_Permission_Folder.Visible = True

        Else
            Response.Redirect("/login.aspx")
        End If
        If Page.IsPostBack = False Then
            GridBindPayHist()
            callddluser_Bind()
            BindAcademicYear()

            PopulateGrade()
        End If

    End Sub
    Private Sub GridBindPayHist()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds_Folder As New DataSet
        Dim ds_Files As New DataSet
        Dim Parent_Id As String = 0
        Dim Paret_Path As String = ""
        Try

            Using objConn
                Dim Home As String = "<li><a id='btnRootFolder' href='/PHOENIXBETA/FolderGallery/FolderGalleryView.aspx'><i class='fas fa-layer-group'></i></a></li>"
                Dim PermissionTitle As String = "<li class='active'><span> Permission </span></li>"
                If Request.QueryString("pfid") Is Nothing Then
                    'Parent_Id =

                    Dim pParms_Parent(3) As SqlClient.SqlParameter
                    pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                    pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                    Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
                    folder_nav.InnerHtml = Home + PermissionTitle
                    PID_QueryString = ""
                Else
                    Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))

                    Dim test As String = Get_Bread_crumbs(Parent_Id, Parent_Id, str_conn, "")

                    folder_nav.InnerHtml = Home + test + PermissionTitle
                    PID_QueryString = "?pfid=" + Request.QueryString("pfid").Replace(" ", "+")
                End If

            End Using

        Catch ex As Exception
            Dim test As String = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Function Get_Bread_crumbs(id As String, current_id As String, str_conn As String, Full_URL As String) As String
        Try
            Dim result As String = ""
            Dim URL As String = ""
            Dim FET_DESCR As String = String.Empty
            Dim FET_ID As String = String.Empty
            Dim FET_PARENT_ID As String = String.Empty
            Dim pParms_Parent(4) As SqlClient.SqlParameter
            pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            pParms_Parent(2) = New SqlClient.SqlParameter("@FET_ID", id)
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_FILE_EXPLORER_PARENT_data", pParms_Parent)
                While reader.Read
                    FET_DESCR = Convert.ToString(reader("FET_DISPLAY_NAME"))
                    FET_ID = Convert.ToString(reader("FET_ID"))
                    FET_PARENT_ID = Convert.ToString(reader("FET_PARENT_ID"))

                    If FET_PARENT_ID <> "0" Then
                        'If id = current_id Then
                        '    URL = "<li class='active'><span>" + FET_DESCR + "</span></li>"
                        'Else
                        URL = "<li><a id='btnViewFolder' href='/PHOENIXBETA/FolderGallery/FolderGalleryView.aspx?pfid=" + Encr_id(FET_ID) + "' >" + FET_DESCR + "</a></li>"
                        'End If
                        Full_URL = URL + Full_URL
                        Full_URL = Get_Bread_crumbs(FET_PARENT_ID, current_id, str_conn, Full_URL)

                    End If

                End While
            End Using
            result = Full_URL
            Return result
        Catch ex As Exception

        End Try
    End Function
    Function Encr_id(id As String) As String
        Dim Enc_ID As String = ""
        Enc_ID = Encr_decrData.Encrypt(id)
        Return Enc_ID
    End Function
    Public Sub callddluser_Bind()
        Try
            ddluser.Items.Clear()

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Dim PARAM(2) As SqlParameter

            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@LOGIN_USER_ID", Session("sUsr_name"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_Users_Have_Acsses_To_School", PARAM)
            ddluser.Items.Clear()
            'Dim di As ListItem
            'di = New ListItem("Select User", "0")
            ddluser.DataSource = ds
            ddluser.DataTextField = "USR_DISPLAY_NAME"
            ddluser.DataValueField = "USR_NAME_ID"
            ddluser.DataBind()
            'ddluser.Items.Insert(0, di)
            'For Each item As ListItem In ddluser.Items

            '    If item.Selected = False Then
            '        item.Selected = True
            '    End If
            'Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub btnSaveStuf_Click(sender As Object, e As EventArgs)

        Dim Parent_Id As String = 0
        Dim Paret_Path As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            If Request.QueryString("pfid") Is Nothing Then
                'Parent_Id =

                Dim pParms_Parent(3) As SqlClient.SqlParameter
                pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
            Else
                Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))
            End If
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                For Each item As ListItem In ddluser.Items

                    If item.Selected = True Then
                        Dim pParms_Folder(17) As SqlClient.SqlParameter
                        pParms_Folder(0) = New SqlClient.SqlParameter("@FPT_ID", "0")
                        pParms_Folder(1) = New SqlClient.SqlParameter("@FPT_FET_ID", Parent_Id)
                        pParms_Folder(2) = New SqlClient.SqlParameter("@FPT_BSU_ID", Session("sBsuid"))
                        pParms_Folder(8) = New SqlClient.SqlParameter("@FPT_EMP_ID", item.Value)
                        pParms_Folder(9) = New SqlClient.SqlParameter("@FPT_VIEW", chb_Staf_View.Checked)
                        pParms_Folder(10) = New SqlClient.SqlParameter("@FPT_ADD", chb_Staf_add.Checked)
                        pParms_Folder(11) = New SqlClient.SqlParameter("@FPT_RENAME", chb_Staf_Rename.Checked)
                        pParms_Folder(12) = New SqlClient.SqlParameter("@FPT_DELETE", chb_Staf_Delete.Checked)
                        pParms_Folder(13) = New SqlClient.SqlParameter("@FPT_CREATED_BY", Session("sUsr_name"))
                        pParms_Folder(14) = New SqlClient.SqlParameter("@FPT_USER_TYPE", "STAFF")
                        pParms_Folder(15) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms_Folder(15).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVE_FILE_PERMISSION_DATA_Staff", pParms_Folder)
                        Dim ReturnFlag As Integer = pParms_Folder(15).Value
                    End If
                Next

                stTrans.Commit()
                Popup_Message = "Permission has been Created successfully"
                Popup_Message_Status = WarningType.Success
                'GridBindPayHist()
                btnSaveStuf.Visible = False
            Catch ex As Exception
                stTrans.Rollback()
                Popup_Message = "Permission has been not Created successfully  Please try again  "
                Popup_Message_Status = WarningType.Danger
            Finally
                objConn.Close()
                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            End Try

        End Using

    End Sub

    Private Sub BindAcademicYear()
        Try
            ddlAcademicYear.Items.Clear()
            ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()

            ddlAcademicYear.SelectedIndex = -1
            If Not Session("Current_ACD_ID") Is Nothing Then
                ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindacademicfolderfgallery")


        End Try
       
    End Sub
    Protected Sub btnSaveParent_Click(sender As Object, e As EventArgs)
        Dim Parent_Id As String = 0
        Dim Paret_Path As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        If trGrades.CheckedNodes.Count = 0 Then
            'lblError.Text = "Select atleast one section."

            Popup_Message = "Select atleast one section"
            Popup_Message_Status = WarningType.Danger
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            Exit Sub
        End If
        Using objConn
            If Request.QueryString("pfid") Is Nothing Then
                'Parent_Id =

                Dim pParms_Parent(3) As SqlClient.SqlParameter
                pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
            Else
                Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))
            End If
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                For Each node As TreeNode In trGrades.CheckedNodes
                    If (Not node Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                        Continue For
                    End If
                    If node.Value <> "ALL" Then
                        Dim pParms_Folder(17) As SqlClient.SqlParameter
                        pParms_Folder(0) = New SqlClient.SqlParameter("@FPT_ID", "0")
                        pParms_Folder(1) = New SqlClient.SqlParameter("@FPT_FET_ID", Parent_Id)
                        pParms_Folder(2) = New SqlClient.SqlParameter("@FPT_BSU_ID", Session("sBsuid"))
                        pParms_Folder(3) = New SqlClient.SqlParameter("@FPT_ACD_ID", ddlAcademicYear.SelectedValue)
                        pParms_Folder(4) = New SqlClient.SqlParameter("@FPT_GRM_ID", node.Parent.Value)
                        pParms_Folder(6) = New SqlClient.SqlParameter("@FPT_SCT_ID", node.Value)
                        pParms_Folder(9) = New SqlClient.SqlParameter("@FPT_VIEW", chb_Parent_View.Checked)
                        pParms_Folder(10) = New SqlClient.SqlParameter("@FPT_ADD", chb_Parent_add.Checked)
                        pParms_Folder(11) = New SqlClient.SqlParameter("@FPT_RENAME", chb_Parent_Rename.Checked)
                        pParms_Folder(12) = New SqlClient.SqlParameter("@FPT_DELETE", chb_Parent_Delete.Checked)
                        pParms_Folder(13) = New SqlClient.SqlParameter("@FPT_CREATED_BY", Session("sUsr_name"))
                        pParms_Folder(14) = New SqlClient.SqlParameter("@FPT_USER_TYPE", "PARENT")
                        pParms_Folder(15) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms_Folder(15).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVE_FILE_PERMISSION_DATA_Parent", pParms_Folder)
                        Dim ReturnFlag As Integer = pParms_Folder(15).Value
                    End If
                Next
                stTrans.Commit()
                Popup_Message = "Permission has been Created successfully"
                Popup_Message_Status = WarningType.Success
                'GridBindPayHist()
                btnSaveParent.Visible = False
            Catch ex As Exception
                stTrans.Rollback()
                Popup_Message = "Permission has been not Created successfully  Please try again  "
                Popup_Message_Status = WarningType.Danger
            Finally
                objConn.Close()
                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            End Try

        End Using
    End Sub

    Private Sub PopulateGrade()
        Try
            Dim dtTable As DataTable = PopulateGradeandSections(ddlAcademicYear.SelectedValue, Session("sbsuid"))
            ' PROCESS Filter
            Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "GRM_GRD_ID", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("Select All", "ALL")
            Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
            Dim drTRM_DESCRIPTION As DataRowView
            While (ienumTRM_DESCRIPTION.MoveNext())
                'Processes List
                drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("GRM_GRD_ID") Then
                        contains = True
                    End If
                End While
                Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("GRM_GRD_ID"), drTRM_DESCRIPTION("GRM_ID"))
                If contains Then
                    Continue While
                End If
                Dim strAMS_MONTH As String = "GRM_GRD_ID = '" &
                drTRM_DESCRIPTION("GRM_GRD_ID") & "'"
                Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "GRM_GRD_ID", DataViewRowState.OriginalRows)
                Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
                While (ienumAMS_MONTH.MoveNext())
                    Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                    Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("SCT_DESCR"), (drMONTH_DESCR("SCT_ID")))
                    trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
                End While
                trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
            End While
            trGrades.Nodes.Clear()
            trGrades.Nodes.Add(trSelectAll)
            trGrades.DataBind()
            trGrades.CollapseAll()
        Catch ex As Exception
            'Message = getErrorMessage("4000")
            'ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGrade: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Function PopulateGradeandSections(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataTable
        Try
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ACD_ID, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[OASIS].[GET_ALL_GRADES_INSCHOOL_fOR_fOLDER]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                Return dsData.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
            'Message = getErrorMessage("4000")
            'ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGradeandSections" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Function
    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            PopulateGrade()
            'populateDdllCategory()
        Catch ex As Exception
            'Message = getErrorMessage("4000")
            'ShowMessage(Message, True)
            UtilityObj.Errorlog("From ddlACDYear_SelectedIndexChanged: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Function Check_If_Admin() As Boolean
        Dim Flag As Boolean = False
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FUPA_BSU_ID", Session("sBsuid"))
            pParms(1) = New SqlClient.SqlParameter("@FUPA_USER_NAME_ID", Session("sUsr_name"))
            Flag = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[Check_If_Admin]", pParms)
        End Using
        Return Flag
    End Function
End Class
