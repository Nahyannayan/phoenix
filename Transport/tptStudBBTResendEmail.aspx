<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptStudBBTResendEmail.aspx.vb" Inherits="Transport_tptStudBBTResendEmail" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> <asp:Label id="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table id="tbl_ShowScreen" runat="server" align="center" width="100%">
                 
              
             
           <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblTPT" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left"  width="20%">
                            <span class="field-label">Select Curriculum</span></td>
                        
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlClm" SKINID="smallcmb" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    
                    <tr>
                        <td align="left">
                            <span class="field-label">Select Academic Year</span></td>
                        
                        <td align="left">
                           <asp:DropDownList ID="ddlAcademicYear" SKINID="smallcmb" runat="server" AutoPostBack="True">
                            </asp:DropDownList>               
                      </td>
                                  <td align="left">
                           <span class="field-label"> Grade</span></td>
                                 
                                  <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                         
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Student ID</span></td>
                        
                        <td align="left"><asp:TextBox id="txtId" runat="server">
                        </asp:TextBox></td>
                        <td align="left">
                           <span class="field-label"> Student Name</span></td>
                       
                        <td align="left">
                            <asp:TextBox id="txtName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                       <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"/></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4" style="height: 16px">
                        </td>
                    </tr>
                    
                      <tr>
                      
                      
                      
                     <td align="center" colspan="4">
                     <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     PageSize="20" Width="100%">
                         <RowStyle CssClass="griditem" />
                     <Columns>


                    <asp:TemplateField HeaderText="Student ID" ><ItemTemplate>
                                         <asp:Label ID="lblStuNo" runat="server"  Text='<%# Bind("STU_No") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="PUSER" visible="false" ><ItemTemplate>
                                         <asp:Label ID="lblPuser" runat="server"  Text='<%# Bind("PUSER") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                                         <asp:Label ID="lblStuId" runat="server"  Text='<%# Bind("STU_ID") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                                         <asp:Label ID="lblParent" runat="server"  Text='<%# Bind("BBT_PARENT") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                     <asp:Label ID="lblReg" runat="server"  Text='<%# Bind("BBT_REGFROM") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                      <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                     <asp:Label ID="lblEmail" runat="server"  Text='<%# Bind("BBT_EMAIL") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                      <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                     <asp:Label ID="lblSblID" runat="server"  Text='<%# Bind("STU_SBL_ID_PICKUP") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                     <asp:Label ID="lblMobile" runat="server"  Text='<%# Bind("BBT_MOBILEPHONE") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                       <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                     <asp:Label ID="lblBusNo" runat="server"  Text='<%# Bind("BNO_DESCR") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField HeaderText="-" Visible="false" ><ItemTemplate>
                     <asp:Label ID="lblFeeId" runat="server"  Text='<%# Bind("STU_FEE_ID") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>


<asp:TemplateField HeaderText="Student Name" SortExpression="DESCR"><HeaderTemplate>
                <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label><br />
           <asp:TextBox ID="txtStuName" runat="server" Width="75%"></asp:TextBox>
           <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click"/>
           
             
</HeaderTemplate>
<ItemTemplate>
            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
            
</ItemTemplate>

<ItemStyle ></ItemStyle>
</asp:TemplateField>
                <asp:TemplateField HeaderText="Grade"><HeaderTemplate>
              <asp:Label ID="lblH12" runat="server" Text="Grade"></asp:Label><br />
              <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
              <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
</HeaderTemplate>
<ItemTemplate>
                 <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
             
</ItemTemplate>
</asp:TemplateField>

                <asp:TemplateField HeaderText="Area"><HeaderTemplate>
              <asp:Label ID="lblH123" runat="server" Text="Area"></asp:Label><br />
              <asp:TextBox ID="txtArea" runat="server" Width="75%"></asp:TextBox>
              <asp:ImageButton ID="btnArea_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnArea_Search_Click" />
              </HeaderTemplate>
              <ItemTemplate>
              <asp:Label ID="lblArea" runat="server" Text='<%# Bind("sbl_description") %>'></asp:Label>
              </ItemTemplate>
               </asp:TemplateField>

             <asp:TemplateField HeaderText="Pickup point"><HeaderTemplate>
              
              <asp:Label ID="lblH1235" runat="server" Text="Pickup Point"></asp:Label><br />
              <asp:TextBox ID="txtPickup" runat="server" Width="75%"></asp:TextBox>
              <asp:ImageButton ID="btnPickup_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnPickup_Search_Click" />
              </HeaderTemplate>
              <ItemTemplate>
              <asp:Label ID="lblPickup" runat="server" Text='<%# Bind("pnt_description") %>'></asp:Label>
              </ItemTemplate>
               </asp:TemplateField>

<asp:ButtonField CommandName="view" Text="Send Email" HeaderText="Send Email">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px"></ItemStyle>
</asp:ButtonField>

<asp:ButtonField CommandName="edit" Text="Send Sms" HeaderText="Send Sms">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:ButtonField>



</Columns>  
                         <SelectedRowStyle CssClass="Green" />
                         <HeaderStyle CssClass="gridheader_pop" />
                         <AlternatingRowStyle CssClass="griditem_alternative" />
                     </asp:GridView>
                     </td></tr>
                    
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />
                          <input id="h_Selected_menu_9" runat="server"
                        type="hidden" value="=" />
                <asp:HiddenField id="hfACD_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfSCT_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfGRD_ID" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfSTUNO" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfNAME" runat="server">
                </asp:HiddenField><asp:HiddenField id="hfReg" runat="server"></asp:HiddenField>
               </td></tr>
           
    
</table>
  
    </div>
    
    
    </div>
     
    </div>
     
    

</asp:Content>

