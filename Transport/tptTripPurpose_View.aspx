<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptTripPurpose_View.aspx.vb" Inherits="Transport_tptTripPurpose_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Trip Purpose
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center" width="100%">
            
            <tr>    <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                        </tr>
           
           <tr>
      
            <td align="center">
            
  <table id="Table1" runat="server" align="center" width="100%" >
                                        
                    
        <tr><td align="center" >
         
         <table id="Table2" runat="server" align="center" width="100%" >
          
           <tr><td align="left">          
            <asp:LinkButton ID="lbAddNew" runat="server">Add New</asp:LinkButton>
            </td></tr>
            
            <tr><td align="center">
                      <asp:GridView ID="gvPurpose" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     PageSize="20" Width="100%"  BorderStyle="None">
                     <Columns>
                      <asp:TemplateField HeaderText="grd_id" Visible="False">
                    
                      <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblPseId" runat="server" Text='<%# Bind("pse_id") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                 <asp:TemplateField HeaderText="Purpose">
                                <HeaderTemplate>
                                 <asp:Label ID="lblPse" runat="server" CssClass="gridheader_text" Text="Purpose"></asp:Label><br />
                            <asp:TextBox ID="txtPse" runat="server" Width="75%"></asp:TextBox>
                               <asp:ImageButton ID="btnPse_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnPse_Search_Click" />
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblPurpose" runat="server" text='<%# Bind("pse_descr") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                
                     
                       <asp:ButtonField CommandName="View" HeaderText="View" Text="View"  >
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                     
                       </Columns>  
                     <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                          <RowStyle CssClass="griditem" Wrap="False" />
                          <SelectedRowStyle CssClass="Green" Wrap="False" />
                          <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                          <EmptyDataRowStyle Wrap="False" />
                          <EditRowStyle Wrap="False" />
                     </asp:GridView>
                   </td></tr>
                   </table>
                       <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
           </td>
           </tr>
           </table>
           </td></tr>
          
        </table>
        
    </div>
            </div>
        </div>
    
  

</asp:Content>

