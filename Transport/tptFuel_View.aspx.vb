Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptFuel_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            'If isPageExpired() Then
            '    Response.Redirect("expired.htm")
            'Else
            '    Session("TimeStamp") = Now.ToString
            '    ViewState("TimeStamp") = Now.ToString
            'End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100250") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Session("dateFilter") = " AND CONVERT(VARCHAR,TFL_FILLDATE,101)='" + Format(Now.Date, "MM/dd/yyyy") + "'"
                    txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
                    txtTo.Text = Format(Now.Date, "dd/MMM/yyyy")

                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub


    Protected Sub ddlgvVehicle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Protected Sub gvTptTrip_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTptTrip.PageIndexChanging
        Try
            gvTptTrip.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTptTrip_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTptTrip.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvTptTrip.Rows(index), GridViewRow)
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblVehId As Label
            lblVehId = selectedRow.FindControl("lblVehId")
            Dim lblVehicle As Label
            lblVehicle = selectedRow.FindControl("lblVehicle")
            Dim url As String
            If e.CommandName = "Add" Then
                url = String.Format("~\Transport\tptFuel_M.aspx?MainMnu_code={0}&datamode={1}&vehicleid=" + Encr_decrData.Encrypt(lblVehId.Text) + "&vehicle=" + Encr_decrData.Encrypt(lblVehicle.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try
            Dim gvDetails As GridView = DirectCast(sender, GridView)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvDetails.Rows(index), GridViewRow)
            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ' Dim lbltflvehId As Label
            Dim lblTflId As Label
            'Dim lblVeh As Label
            ' lbltflvehId = selectedRow.FindControl("lbltflvehId")
            lblTflId = selectedRow.FindControl("lblTflId")
            '  lblVeh = selectedRow.FindControl("lblVeh")
            Dim url As String
            If e.CommandName = "delete" Then
                ' url = String.Format("~\Transport\tptFuel_M.aspx?MainMnu_code={0}&datamode={1}&vehicleid=" + Encr_decrData.Encrypt(lbltflvehId.Text) + "&tfLid=" + Encr_decrData.Encrypt(lblTflId.Text) + "&vehicle=" + Encr_decrData.Encrypt(lblVeh.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                'Response.Redirect(url)

                SaveData(lblTflId.Text)
                GridBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Function SaveData(ByVal tflid As String) As Boolean
        Dim str_query As String = "exec TRANSPORT.saveFUEL " + tflid + ",NULL,NULL,NULL,NULL,NULL,NULL,'delete'"

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'If ViewState("datamode") = "edit" Then
                '    UtilityObj.InsertAuditdetails(transaction, "edit", "TRANSPORT.TRIP_FUEL_S", "TFL_ID", "TFL_VEH_ID", "TFL_ID=" + hfTFL_ID.Value.ToString)
                'End If
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                Return False
            End Try
        End Using
    End Function

    Protected Sub gvTptTrip_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTptTrip.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblvehid As New Label
                lblvehid = e.Row.FindControl("lblvehId")
                Dim gvDetails As New GridView
                gvDetails = e.Row.FindControl("gvDetails")
                BindFUELDetails(gvDetails, lblvehid.Text.ToString, Session("dateFilter"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

    End Sub

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvTptTrip.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptTrip.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub GridBind(Optional ByVal history As Boolean = False)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString


        Dim str_query As String = "SELECT DISTINCT GUID,VEH_ID AS VEH_ID,VEH_REGNO FROM " _
                             & "TRANSPORT.vv_vehicle_m WHERE veh_alto_bsu_id='" + Session("sbsuid") + "'"



        Dim ddlgvTrip As New DropDownList
        Dim ddlgvVehicle As New DropDownList
        Dim selectedTrips As String = ""
        Dim selectedVehicle As String = ""

        If gvTptTrip.Rows.Count > 0 Then

            ddlgvVehicle = gvTptTrip.HeaderRow.FindControl("ddlgvVehicle")
            If ddlgvVehicle.Text <> "ALL" Then
                str_query += " and VEH_REGNO='" + ddlgvVehicle.Text + "'"
                selectedVehicle = ddlgvVehicle.Text
            End If
        End If

        str_query += " ORDER BY VEH_REGNO"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptTrip.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTptTrip.DataBind()
            Dim columnCount As Integer = gvTptTrip.Rows(0).Cells.Count
            gvTptTrip.Rows(0).Cells.Clear()
            gvTptTrip.Rows(0).Cells.Add(New TableCell)
            gvTptTrip.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTptTrip.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTptTrip.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTptTrip.DataBind()
        End If



        Dim dt As DataTable = ds.Tables(0)

        If gvTptTrip.Rows.Count > 0 Then
            ddlgvVehicle = gvTptTrip.HeaderRow.FindControl("ddlgvVehicle")

            Dim dr As DataRow

            ddlgvVehicle.Items.Clear()
            ddlgvVehicle.Items.Add("ALL")
            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If ddlgvVehicle.Items.FindByText(.Item(2)) Is Nothing Then
                        ddlgvVehicle.Items.Add(.Item(2))
                    End If
                End With

                If selectedVehicle <> "" Then
                    ddlgvVehicle.Text = selectedVehicle
                End If
            Next

        End If

    End Sub



    Private Sub BindFUELDetails(ByVal gvDetails As GridView, ByVal vehid As String, ByVal dateFilter As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TFL_ID,TFL_VEH_ID,TFL_PREVKM,TFL_CURRENTKM,TFL_GALLON,TFL_AMOUNT,TFL_FILLDATE,VEH_REGNO " _
                                & " FROM TRANSPORT.TRIP_FUEL_S AS A INNER JOIN TRANSPORT.VV_VEHICLE_M AS B " _
                                & " ON A.TFL_VEH_ID=B.VEH_ID WHERE TFL_VEH_ID=" + vehid

        str_query += dateFilter
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvDetails.DataSource = ds
        gvDetails.DataBind()
    End Sub




    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

#End Region


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Session("dateFilter") = " AND TFL_FILLDATE BETWEEN '" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub gvDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)

    End Sub
End Class
