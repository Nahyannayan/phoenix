Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Transport_tptVehicle_Allocation_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim menu_rights As Integer = 0
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    Call gridbind(False)


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
            'Else
            '    Call gridbind()
        End If

        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
       
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvVehicleAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvVehicleAlloc.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvVehicleAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvVehicleAlloc.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvVehicleAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvVehicleAlloc.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvVehicleAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvVehicleAlloc.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvVehicleAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvVehicleAlloc.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvVehicleAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvVehicleAlloc.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(ByVal export As Boolean, Optional ByVal p_sindex As Integer = -1)
        Try

            Dim CurrentDatedType As String = String.Empty

            Dim ddlOPENONLINEH As New DropDownList

            Dim str_conn As String = ConnectionManger.GetOASISTransportConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_CAT_DESCR As String = String.Empty
            Dim str_filter_REGNO As String = String.Empty
            Dim str_filter_CAP As String = String.Empty

            Dim str_filter_Owned As String = String.Empty
            Dim str_filter_OPERA As String = String.Empty
            Dim str_filter_ALLOC As String = String.Empty
            Dim BSU_ID As String = Session("sBsuid")

            Dim ds As New DataSet

            If BSU_ID = "900501" Then
                str_Sql = "SELECT CAT_DESCR,REGNO, GPS_UNIT_ID ,CAP,VEH_ID,OWNED,OPRT,ALTO,OPRT_ID,ALTO_ID FROM(SELECT CATEGORY_M.CAT_DESCRIPTION as CAT_DESCR, VEHICLE_M.VEH_REGNO as REGNO,CONVERT(VARCHAR, VEHICLE_M.VEH_UNITID) AS GPS_UNIT_ID, VEHICLE_M.VEH_CAPACITY as CAP, VEHICLE_M.VEH_ID as VEH_ID, " & _
               " BSU_OWNED.BSU_NAME AS OWNED, BSU_OPRT.BSU_NAME AS OPRT,BSU_ALTO.BSU_NAME AS ALTO,VEHICLE_M.VEH_OPRT_BSU_ID as OPRT_ID, VEHICLE_M.VEH_ALTO_BSU_ID as ALTO_ID" & _
              " FROM VEHICLE_M INNER JOIN  CATEGORY_M ON VEHICLE_M.VEH_CAT_ID = CATEGORY_M.CAT_ID LEFT OUTER JOIN " & _
               " BUSINESSUNIT_M AS BSU_OWNED ON VEHICLE_M.VEH_OWN_BSU_ID = BSU_OWNED.BSU_ID LEFT OUTER JOIN " & _
              " BUSINESSUNIT_M AS BSU_OPRT ON VEHICLE_M.VEH_OPRT_BSU_ID = BSU_OPRT.BSU_ID LEFT OUTER JOIN " & _
               " BUSINESSUNIT_M AS BSU_ALTO ON VEHICLE_M.VEH_ALTO_BSU_ID = BSU_ALTO.BSU_ID) A where   A.VEH_ID<>''"

            Else

                str_Sql = "SELECT CAT_DESCR,REGNO, GPS_UNIT_ID ,CAP,VEH_ID,OWNED,OPRT,ALTO,OPRT_ID,ALTO_ID FROM(SELECT CATEGORY_M.CAT_DESCRIPTION as CAT_DESCR, VEHICLE_M.VEH_REGNO as REGNO,CONVERT(VARCHAR, VEHICLE_M.VEH_UNITID) AS GPS_UNIT_ID, VEHICLE_M.VEH_CAPACITY as CAP, VEHICLE_M.VEH_ID as VEH_ID, " & _
               " BSU_OWNED.BSU_NAME AS OWNED, BSU_OPRT.BSU_NAME AS OPRT,BSU_ALTO.BSU_NAME AS ALTO,VEHICLE_M.VEH_OPRT_BSU_ID as OPRT_ID, VEHICLE_M.VEH_ALTO_BSU_ID as ALTO_ID" & _
              " FROM VEHICLE_M INNER JOIN  CATEGORY_M ON VEHICLE_M.VEH_CAT_ID = CATEGORY_M.CAT_ID LEFT OUTER JOIN " & _
               " BUSINESSUNIT_M AS BSU_OWNED ON VEHICLE_M.VEH_OWN_BSU_ID = BSU_OWNED.BSU_ID LEFT OUTER JOIN " & _
              " BUSINESSUNIT_M AS BSU_OPRT ON VEHICLE_M.VEH_OPRT_BSU_ID = BSU_OPRT.BSU_ID LEFT OUTER JOIN " & _
               " BUSINESSUNIT_M AS BSU_ALTO ON VEHICLE_M.VEH_ALTO_BSU_ID = BSU_ALTO.BSU_ID) A where A.ALTO_ID='" & Session("sBsuid") & "' AND  A.VEH_ID<>''"
            End If





            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_CAT_DESCR As String = String.Empty
            Dim str_REGNO As String = String.Empty
            Dim str_CAP As String = String.Empty
            Dim str_Owned As String = String.Empty
            Dim str_Opera As String = String.Empty
            Dim str_Alloc As String = String.Empty

            If gvVehicleAlloc.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtCAT_DESCR")
                str_CAT_DESCR = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_CAT_DESCR = " AND A.CAT_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_CAT_DESCR = "  AND  NOT A.CAT_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_CAT_DESCR = " AND A.CAT_DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CAT_DESCR = " AND A.CAT_DESCR NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_CAT_DESCR = " AND A.CAT_DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_CAT_DESCR = " AND A.CAT_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtREGNO")
                str_REGNO = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_REGNO = " AND A.REGNO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_REGNO = "  AND  NOT A.REGNO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_REGNO = " AND A.REGNO  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_REGNO = " AND A.REGNO NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_REGNO = " AND A.REGNO LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_REGNO = " AND A.REGNO NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtCAP")
                str_CAP = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_CAP = " AND A.CAP LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_CAP = "  AND  NOT A.CAP  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_CAP = " AND A.CAP   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CAP = " AND A.CAP  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_CAP = " AND A.CAP LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_CAP = " AND A.CAP  NOT LIKE '%" & txtSearch.Text & "'"
                End If
                'modified code
                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtOwned")
                str_Owned = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_Owned = " AND A.OWNED LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Owned = "  AND  NOT A.OWNED  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Owned = " AND A.OWNED   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Owned = " AND A.OWNED  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Owned = " AND A.OWNED LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Owned = " AND A.OWNED  NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtOpera")
                str_Opera = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_OPERA = " AND A.OPRT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_OPERA = "  AND  NOT A.OPRT  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_OPERA = " AND A.OPRT  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_OPERA = " AND A.OPRT  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_OPERA = " AND A.OPRT LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_OPERA = " AND A.OPRT  NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtAlloc")
                str_Alloc = txtSearch.Text
                If str_search = "LI" Then
                    str_filter_ALLOC = " AND A.ALTO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ALLOC = "  AND  NOT A.ALTO  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ALLOC = " AND A.ALTO   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ALLOC = " AND A.ALTO  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ALLOC = " AND A.ALTO LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ALLOC = " AND A.ALTO  NOT LIKE '%" & txtSearch.Text & "'"
                End If





            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_CAT_DESCR & str_filter_REGNO & str_filter_CAP & str_filter_Owned & str_filter_ALLOC & " ORDER BY A.CAT_DESCR")

           

            If ds.Tables(0).Rows.Count > 0 Then

                gvVehicleAlloc.DataSource = ds.Tables(0)
                gvVehicleAlloc.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvVehicleAlloc.DataBind()

                Dim columnCount As Integer = gvVehicleAlloc.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvVehicleAlloc.Rows(0).Cells.Clear()
                gvVehicleAlloc.Rows(0).Cells.Add(New TableCell)
                gvVehicleAlloc.Rows(0).Cells(0).ColumnSpan = columnCount
                gvVehicleAlloc.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvVehicleAlloc.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            If export Then
                Dim dt As New DataTable
                dt = ds.Tables(0)
                dt.Columns.Remove("VEH_ID")
                dt.Columns.Remove("OPRT_ID")
                dt.Columns.Remove("ALTO_ID")
                ExportExcel(dt)
            End If

            txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtCAT_DESCR")
            txtSearch.Text = str_CAT_DESCR
            txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtREGNO")
            txtSearch.Text = str_REGNO
            txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtCAP")
            txtSearch.Text = str_CAP

            txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtOWNED")
            txtSearch.Text = str_Owned
            txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtOPERA")
            txtSearch.Text = str_Opera
            txtSearch = gvVehicleAlloc.HeaderRow.FindControl("txtALLOC")
            txtSearch.Text = str_Alloc




            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub gvVehicleAlloc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvVehicleAlloc.RowDataBound
        Dim VID As String
        Dim lblVID As New Label
        Dim lbtnOwned As New LinkButton
        Dim lbtnOperBy As New LinkButton
        Dim lbtnALTO As New LinkButton
        lblVID = TryCast(e.Row.FindControl("lblVID"), Label)
        If lblVID IsNot Nothing Then
            VID = lblVID.Text
        End If

        Dim url As String
        url = String.Format("tptVehicle_History.aspx?MainMnu_code={0}&VID={1}", ViewState("MainMnu_code"), VID)
        lbtnOwned = e.Row.FindControl("lbtnOwned")
        If lbtnOwned IsNot Nothing Then
            lbtnOwned.OnClientClick = "javascript:ShowDetails('" & url & "','O');return false;"
            ' gridbind()
        End If
        lbtnOperBy = e.Row.FindControl("lbtnOperBy")
        If lbtnOperBy IsNot Nothing Then
            lbtnOperBy.OnClientClick = "javascript:ShowDetails('" & url & "','P');return false;"
            'gridbind()
        End If
        lbtnALTO = e.Row.FindControl("lbtnALTO")
        If lbtnALTO IsNot Nothing Then
            lbtnALTO.OnClientClick = "javascript:ShowDetails('" & url & "','A');return false;"
            ' gridbind()
        End If

    End Sub

    Protected Sub gvVehicleAlloc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvVehicleAlloc.PageIndexChanging
        gvVehicleAlloc.PageIndex = e.NewPageIndex
        gridbind(False)
    End Sub
    ' <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    'Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
    '     Dim sTemp As New StringBuilder()

    '     sTemp.Append("<table class='popdetails'>") ' border=1 bordercolor=#1b80b6 bgcolor=#ffcccc cellpadding=1 cellspacing=0>")
    '     sTemp.Append("<tr>")
    '     sTemp.Append("<td colspan=2><b>The FEE Setup details</b></td>")
    '     sTemp.Append("</tr>")
    '     sTemp.Append("<tr>")
    '     'sTemp.Append("<td><b>FEE ID</b></td>")
    '     sTemp.Append("<td><b>FEE DESCR</b></td>")
    '     sTemp.Append("<td><b>AMOUNT</b></td>")
    '     sTemp.Append("</tr>")

    '     Dim STUD_ID As Integer = contextKey
    '     If HttpContext.Current.Session("STUD_DET") Is Nothing Then
    '         sTemp.Append("</table>")
    '         Return sTemp.ToString()
    '     End If
    '     Dim httab As Hashtable = HttpContext.Current.Session("STUD_DET")
    '     Dim vFEE_PERF As FEEPERFORMAINVOICE = httab(STUD_ID)
    '     If Not vFEE_PERF Is Nothing Then
    '         Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
    '         Dim ienum As IEnumerator = arrList.GetEnumerator()
    '         Dim htFeeTypes As New Hashtable
    '         While (ienum.MoveNext())
    '             Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
    '             htFeeTypes(FEE_SUB_DET.FPD_FEE_DESCR) = htFeeTypes(FEE_SUB_DET.FPD_FEE_DESCR) + FEE_SUB_DET.FPD_AMOUNT + FEE_SUB_DET.FPD_ADJ_AMOUNT
    '         End While
    '         Dim iDictEnum As IDictionaryEnumerator = htFeeTypes.GetEnumerator()
    '         While (iDictEnum.MoveNext())
    '             sTemp.Append("<tr>")
    '             'sTemp.Append("<td>" & "1" & "</td>")
    '             sTemp.Append("<td>" & iDictEnum.Key & "</td>")
    '             sTemp.Append("<td>" & iDictEnum.Value & "</td>")
    '             'sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_ID.ToString & "</td>")
    '             'sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_DESCR & "</td>")
    '             'sTemp.Append("<td>" & FEE_SUB_DET.FPD_AMOUNT.ToString & "</td>")
    '             sTemp.Append("</tr>")
    '         End While
    '     End If
    '     sTemp.Append("</table>")

    '     Return sTemp.ToString()
    ' End Function

    Protected Sub btnSearchCAT_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub

    Protected Sub btnSearchCAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub

    Protected Sub btnSearchREGNO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub btnSearchOwned_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub

    Protected Sub btnSearchOpera_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub

    Protected Sub btnSearchAlloc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Transport\tptVeh_Allocation_Edit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))

            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblVID As New Label
            Dim url As String
            Dim viewid As String
            lblVID = TryCast(sender.FindControl("lblVID"), Label)
            viewid = lblVID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Transport\tptVeh_Allocation_Edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '   ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        gridbind(True)
    End Sub
End Class
