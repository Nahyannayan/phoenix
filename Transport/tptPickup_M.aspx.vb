Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptPickup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                  Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("dtPnt") = SetDataTable()
                    ViewState("SelectedRow") = -1
                    If ViewState("datamode") = "add" Then
                        BindLocation()
                        BindSubLocation()
                        If ddlSubLocation.Items.Count = 0 Then
                            ddlSubLocation.Enabled = False
                            txtPickupPoint.ReadOnly = True
                        Else
                            ddlSubLocation.Enabled = True
                            txtPickupPoint.ReadOnly = False
                        End If
                    Else

                        EnableDisableControls(True)
                        Dim editstring As String = Encr_decrData.Decrypt(Request.QueryString("editstring").Replace(" ", "+"))
                        Dim str() As String
                        str = editstring.Split("|")
                        Dim li As New ListItem
                        li.Text = str(1)
                        li.Value = str(0)
                        ddlLocation.Items.Add(li)

                        li = New ListItem
                        li.Text = str(3)
                        li.Value = str(2)
                        ddlSubLocation.Items.Add(li)

                        GetRows()
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

   
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ClearRecords()
            EnableDisableControls(False)
            lblError.Text = ""
            BindLocation()
            BindSubLocation()
            If ddlSubLocation.Items.Count = 0 Then
                ddlSubLocation.Enabled = False
                txtPickupPoint.ReadOnly = True
            Else
                ddlSubLocation.Enabled = True
                txtPickupPoint.ReadOnly = False
            End If
            Session("dtPnt") = SetDataTable()
            gvtptPickup.DataSource = Session("dtPnt")
            gvtptPickup.DataBind()
            gvtptPickup.Columns(3).Visible = True
            gvtptPickup.Columns(4).Visible = True
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                ClearRecords()
                EnableDisableControls(True)
                lblError.Text = ""
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            ClearRecords()
            EnableDisableControls(True)
            ' lblError.Text = "Record saved successfully"
            ViewState("datamode") = "view"
            Session("dtPnt") = SetDataTable()
            GetRows()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            EnableDisableControls(False)
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            BindSubLocation()
            If ddlSubLocation.Items.Count = 0 Then
                ddlSubLocation.Enabled = False
                txtPickupPoint.ReadOnly = True
            Else
                ddlSubLocation.Enabled = True
                txtPickupPoint.ReadOnly = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvtptPickup_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvtptPickup.RowCommand
        Try
            If e.CommandName = "edit" Or e.CommandName = "delete" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvtptPickup.Rows(index), GridViewRow)
                Dim lblPickUpPoint As Label
                Dim lblRate As Label
                Dim lblfromDate As Label
                Dim lblflag As Label


                lblPickUpPoint = selectedRow.FindControl("lblPickupPoint")
                lblRate = selectedRow.FindControl("lblRate")
                lblfromDate = selectedRow.FindControl("lblfromDate")
                lblflag = selectedRow.FindControl("lblflag")

                lblError.Text = ""
                Dim keys() As Object
                ReDim keys(3)
                Dim dt As DataTable
                dt = Session("dtPnt")

                keys(0) = ddlLocation.SelectedValue
                keys(1) = ddlSubLocation.SelectedValue
                keys(2) = lblPickUpPoint.Text
                keys(3) = lblflag.Text

                index = dt.Rows.IndexOf(dt.Rows.Find(keys))

                If e.CommandName = "edit" Then
                    txtPickupPoint.Text = lblPickUpPoint.Text
                    ViewState("SelectedRow") = index
                    btnAddNew.Text = "Update"

                Else

                    If dt.Rows(index).Item(6) = "add" Then
                        dt.Rows(index).Item(6) = "remove"
                    ElseIf dt.Rows(index).Item(6) = "edit" Then
                        If isTripsAllocated(dt.Rows(index).Item(2)) = True Then
                            lblError.Text = "Please remove this pick up point from allocated trips before deleting"
                            Exit Sub
                        End If
                        dt.Rows(index).Item(6) = "delete"
                        dt.Rows(index).Item(7) = "1"
                    End If

                    Session("dtPnt") = dt
                    GridBind(dt)


                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvtptPickup_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvtptPickup.RowDeleting

    End Sub

    Protected Sub gvtptPickup_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvtptPickup.RowEditing

    End Sub
    Protected Sub gvTptPickUp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTptPickUp.PageIndexChanging
        Try
            Dim dt As DataTable
            dt = Session("dtPnt")

            gvtptPickup.PageIndex = e.NewPageIndex
            GridBind(dt)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                AddRows()
            Else
                EditRows()
            End If
            ViewState("SelectedRow") = -1
            btnAddNew.Text = "Add"
            ClearRecords()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindLocation()
        ddlLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select loc_id,loc_description from TRANSPORT.LOCATION_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "loc_description"
        ddlLocation.DataValueField = "loc_id"
        ddlLocation.DataBind()
    End Sub

    Private Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(6) <> "delete" And dt.Rows(i)(6) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvtptPickup.DataSource = dtTemp
        gvtptPickup.DataBind()
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(3)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbl_loc_id"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "pnt_sbl_id"
        dt.Columns.Add(column)
        keys(1) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "pnt_id"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "loc_description"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbl_description"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "pnt_description"
        dt.Columns.Add(column)
        keys(2) = column

      
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "datamode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "flag"
        dt.Columns.Add(column)
        keys(3) = column

        dt.PrimaryKey = keys
        Return dt
    End Function

    Private Sub GetRows()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT sbl_loc_id,pnt_sbl_id,pnt_id,loc_description,sbl_description,pnt_description,pnt_pickup,pnt_dropoff" _
        '                        & " FROM tptpickuppoints_m A, tptsublocation_m B,tptlocation_m C WHERE " _
        '                        & " A.pnt_sbl_id=B.sbl_id AND B.sbl_loc_id=C.loc_id AND pnt_sbl_id=" + ddlSubLocation.SelectedValue.ToString

        Dim str_query As String = "SELECT SBL_LOC_ID,PNT_SBL_ID,PNT_ID,LOC_DESCRIPTION,SBL_DESCRIPTION,PNT_DESCRIPTION " _
                                & "FROM TRANSPORT.SUBLOCATION_M AS A INNER JOIN" _
                                & " TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID = B.PNT_SBL_ID INNER JOIN" _
                                & " TRANSPORT.LOCATION_M AS C ON A.SBL_LOC_ID = C.LOC_ID" _
                                & " WHERE PNT_SBL_ID=" + ddlSubLocation.SelectedValue.ToString _
                                & " AND PNT_BSU_ID='" + Session("SBSUID") + "'" _
                                & " ORDER BY LOC_DESCRIPTION,SBL_DESCRIPTION,pnt_description"

        Dim dt As DataTable
        Dim dr As DataRow
        dt = Session("dtPnt")

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            dr = dt.NewRow
            With dr
                .Item(0) = reader.GetValue(0).ToString
                .Item(1) = reader.GetValue(1).ToString
                .Item(2) = reader.GetValue(2).ToString
                .Item(3) = reader.GetString(3)
                .Item(4) = reader.GetString(4)
                .Item(5) = reader.GetString(5)
                .Item(6) = "edit"
                .Item(7) = "0"
                dt.Rows.Add(dr)
            End With
        End While
        reader.Close()
        gvtptPickup.DataSource = dt
        gvtptPickup.DataBind()
        Session("dtPnt") = dt

     

    End Sub

    Sub AddRows()
        Try
            Dim dr As DataRow
            Dim dt As New DataTable
            dt = Session("dtPnt")

            ''check if the row is already added
            Dim keys As Object()
            ReDim keys(3)
            keys(0) = ddlLocation.SelectedValue
            keys(1) = ddlSubLocation.SelectedValue
            keys(2) = txtPickupPoint.Text
            keys(3) = "0"
            Dim row As DataRow = dt.Rows.Find(keys)
            If Not row Is Nothing Then
                lblError.Text = "This pick up point is already added"
                Exit Sub
            End If
            dr = dt.NewRow
            dr.Item(0) = ddlLocation.SelectedValue
            dr.Item(1) = ddlSubLocation.SelectedValue
            dr.Item(2) = 0
            dr.Item(3) = ddlLocation.SelectedItem.Text
            dr.Item(4) = ddlSubLocation.SelectedItem.Text
            dr.Item(5) = txtPickupPoint.Text
            dr.Item(6) = "add"
            dr.Item(7) = "0"
            dt.Rows.Add(dr)
            Session("dtPnt") = dt
            GridBind(dt)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtPnt")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                .Item(5) = txtPickupPoint.Text.ToString
                If .Item(6) = "edit" Then
                    .Item(6) = "update"
                End If
            End With
            Session("dtPnt") = dt
            GridBind(dt)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub BindSubLocation()
        ddlSubLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select sbl_id,sbl_description from TRANSPORT.SUBLOCATION_M where" _
                                  & " sbl_id not in (select pnt_sbl_id from TRANSPORT.PICKUPPOINTS_M where pnt_bsu_id='" + Session("sbsuid") + "')  and sbl_loc_id=" + ddlLocation.SelectedValue _
                                  & " ORDER BY sbl_description"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubLocation.DataSource = ds
        ddlSubLocation.DataTextField = "sbl_description"
        ddlSubLocation.DataValueField = "sbl_id"
        ddlSubLocation.DataBind()
    End Sub

    Sub ClearRecords()
        txtPickupPoint.Text = ""
    End Sub

    Sub EnableDisableControls(ByVal value As Boolean)
        txtPickupPoint.ReadOnly = value
        ddlLocation.Enabled = Not value
        ddlSubLocation.Enabled = Not value
        gvtptPickup.Enabled = Not value
    End Sub

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String
                Dim dt As DataTable
                Dim flagAudit As Integer
                Dim pntid As Integer
                Dim pntAddIds As String = ""
                Dim pntEditIds As String = ""
                Dim pntDeleteIds As String = ""

                dt = Session("dtPnt")
                Dim dr As DataRow
                For Each dr In dt.Rows
                    With dr
                        If .Item(6).ToString <> "remove" And .Item(6).ToString <> "edit" Then
                            If .Item(6).ToString = "update" Then
                                UtilityObj.InsertAuditdetails(transaction, ViewState("datamode"), "TRANSPORT.PICKUPPOINTS_M", "PNT_ID", "PNT_SBL_ID", "PNT_ID=" + .Item(2).ToString)
                            ElseIf .Item(6).ToString = "delete" Then
                                UtilityObj.InsertAuditdetails(transaction, ViewState("datamode"), "TRANSPORT.PICKUPPOINTS_M", "PNT_ID", "PNT_SBL_ID", "PNT_ID=" + .Item(2).ToString)
                            End If
                            str_query = "exec [TRANSPORT].[SavePICKUPPOINTS] " + .Item(2) + "," + .Item(1) + ",'" + Session("sbsuid").ToString + "'," + Session("clm") + ",'" + .Item(5).Replace("'", "''") + "','" + .Item(6) + "'," + .Item(0)
                            pntid = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                            If .Item(6) = "add" Then
                                If pntAddIds.Length <> 0 Then
                                    pntAddIds += ","
                                End If
                                pntAddIds += pntid.ToString
                            End If

                            If .Item(6) = "update" Then
                                If pntEditIds.Length <> 0 Then
                                    pntEditIds += ","
                                End If
                                pntEditIds += pntid.ToString
                            End If

                            If .Item(6) = "delete" Then
                                If pntDeleteIds.Length <> 0 Then
                                    pntDeleteIds += ","
                                End If
                                pntDeleteIds += pntid.ToString
                            End If
                        End If
                    End With
                Next


                If pntAddIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "PNT_ID(" + pntAddIds + ")", "add", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If pntEditIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "PNT_ID(" + pntEditIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If pntDeleteIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "PNT_ID(" + pntDeleteIds + ")", "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Private Function isTripsAllocated(ByVal pntid As Integer) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select count(tpp_id) from transport.trips_pickup_s where tpp_pnt_id=" + pntid.ToString
        Dim pnt As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If pnt = 0 Then
            Return False
        Else
            Return True
        End If
    End Function



#End Region

  
    
End Class
