Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.IO
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports SmsService
Partial Class Transport_tptStudBBTResendEmail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or ViewState("MainMnu_code") <> "T000280" Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_9.Value = "LI__../Images/operations/like.gif"

                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = "0"
                    ddlGrade.Items.Insert(0, li)




                    Select Case ViewState("MainMnu_code")
                        Case "T000260"
                            lblTitle.Text = "Resend Email For Non Gems Schools"
                    End Select

                    If ddlClm.Items.Count = 1 Then
                        tblTPT.Rows(0).Visible = False
                    End If


                    tblTPT.Rows(4).Visible = False

                    tblTPT.Rows(5).Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else



        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid9(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_9_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString


        Dim str_query As String = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ') " _
                                & " AS STU_NAME,GRM_DISPLAY,SBL_DESCRIPTION,PNT_DESCRIPTION ," _
                                & " BBT_TITLE+ ' ' + ISNULL(BBT_PFIRSTNAME,'')+ ' ' +ISNULL(BBT_PMIDNAME,'')+' '+ISNULL(BBT_PLASTNAME,'') AS BBT_PARENT,BBT_REGFROM ,BBT_MOBILEPHONE," _
                                & " STU_SBL_ID_PICKUP,BBT_EMAIL,BNO_DESCR,STU_FEE_ID," _
                                & " CASE STU_PRIMARYCONTACT WHEN 'F' THEN STS_FUSR_NAME WHEN 'M' THEN STS_MUSR_NAME " _
                                & " ELSE STS_GUSR_NAME END AS PUSER " _
                                & " FROM STUDENT_M AS A" _
                                & " INNER JOIN STUDENT_D AS Y ON A.STU_SIBLING_ID=Y.STS_STU_ID" _
                                & " INNER JOIN STUDENT_BBTREQ_S AS P ON A.STU_ID=P.BBT_STU_ID" _
                                & " INNER JOIN VV_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                                & " INNER JOIN TRANSPORT.SUBLOCATION_M AS C ON A.STU_SBL_ID_PICKUP=C.SBL_ID" _
                                & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS D ON A.STU_PICKUP=D.PNT_ID" _
                                & " LEFT OUTER JOIN TRANSPORT.VV_TRIPS_D AS T ON A.STU_PICKUP_TRP_ID=T.TRP_ID" _
                                & " WHERE STU_BSU_ID='" + Session("SBSUID") + "'"



        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND GRM_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If
        If txtId.Text <> "" Then
            str_query = " AND STU_ID LIKE '%" + txtId.Text + "%'"
        End If


        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedArea As String = ""
        Dim selectedPickup As String = ""

        Dim txtSearch As New TextBox

        If gvStud.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_7.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtArea")
            strSidsearch = h_Selected_menu_8.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SBL_DESCRIPTION", txtSearch.Text, strSearch)
            selectedArea = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtPickup")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("PNT_DESCRIPTION", txtSearch.Text, strSearch)
            selectedPickup = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If

        End If

        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()
        End If

        Dim dt As DataTable = ds.Tables(0)


        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtArea")
        txtSearch.Text = selectedArea

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtPickup")
        txtSearch.Text = selectedPickup


        set_Menu_Img()



    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub SendApproveEmail(ByVal reg As String, ByVal stu_id As String, ByVal parent As String, ByVal emailId As String, ByVal sbl_id As String, ByVal feeId As String, ByVal puser As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec transport.rptStudTransportInfo '" + stu_id + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Dim filepath As String = Server.MapPath("~/Transport/BBTAOApproveEmail.htm")
        Dim fr As System.IO.TextReader
        fr = File.OpenText(filepath)
        Dim sr As String = fr.ReadToEnd
        fr.Close()

        sr = sr.Replace("%parent%", parent)
        While reader.Read
            If Not reader.IsDBNull(4) Then
                sr = sr.Replace("%child%", reader("STU_NAME"))
            End If
            If Not reader.IsDBNull(5) Then
                sr = sr.Replace("%grade%", reader("GRM_DISPLAY"))
            End If
            If Not reader.IsDBNull(8) Then
                sr = sr.Replace("%parea%", reader("PICKUPAREA"))
            Else
                sr = sr.Replace("%parea%", "")
            End If
            If Not reader.IsDBNull(14) Then
                sr = sr.Replace("%pbus%", reader("PICKUPBUS"))
            Else
                sr = sr.Replace("%pbus%", "")
            End If
            If Not reader.IsDBNull(20) Then
                sr = sr.Replace("%ptrip%", reader("PICKUPTRIP"))
            Else
                sr = sr.Replace("%ptrip%", "")
            End If
            If Not reader.IsDBNull(10) Then
                sr = sr.Replace("%pickup%", reader("PICKUP"))
            Else
                sr = sr.Replace("%pickup%", "")
            End If
            If Not reader.IsDBNull(22) Then
                sr = sr.Replace("%ptime%", reader("PICKUPTIME"))
            Else
                sr = sr.Replace("%ptime%", "")
            End If
            If Not reader.IsDBNull(16) Then
                sr = sr.Replace("%pdrv%", reader("PICKUPDRIVER"))
            Else
                sr = sr.Replace("%pdrv%", "")
            End If
            If Not reader.IsDBNull(18) Then
                sr = sr.Replace("%pcon%", reader("PICKUPCONDUCTOR"))
            Else
                sr = sr.Replace("%pcon%", "")
            End If
            If Not reader.IsDBNull(9) Then
                sr = sr.Replace("%darea%", reader("DROPOFFAREA"))
            Else
                sr = sr.Replace("%darea%", "")
            End If
            If Not reader.IsDBNull(15) Then
                sr = sr.Replace("%dbus%", reader("DROPOFFBUS"))
            Else
                sr = sr.Replace("%dbus%", "")
            End If
            If Not reader.IsDBNull(21) Then
                sr = sr.Replace("%dtrip%", reader("DROPOFFTRIP"))
            End If
            If Not reader.IsDBNull(11) Then
                sr = sr.Replace("%dropoff%", reader("DROPOFF"))
            End If
            If Not reader.IsDBNull(23) Then
                sr = sr.Replace("%dtime%", reader("DROPOFFTIME"))
            Else
                sr = sr.Replace("%dtime%", "")
            End If
            If Not reader.IsDBNull(17) Then
                sr = sr.Replace("%ddrv%", reader("DROPOFFDRIVER"))
            Else
                sr = sr.Replace("%ddrv%", "")
            End If
            If Not reader.IsDBNull(19) Then
                sr = sr.Replace("%dcon%", reader("DROPOFFCONDUCTOR"))
            Else
                sr = sr.Replace("%dcon%", "")
            End If
        End While
        reader.Close()

        sr = sr.Replace("%feeid%", feeId)
        sr = sr.Replace("%user%", puser)
        sr = sr.Replace("%password%", feeId)
        sr = sr.Replace("%contactnumber%", "800XXXXX")

        If hfReg.Value = "bbt" Then
            sr = sr.Replace("%lurl%", "www.brightbustransport.com")
        Else
            sr = sr.Replace("%lurl%", "www.schooltransportservices.com")
        End If
        str_conn = ConnectionManger.GetOASISConnectionString
        If hfReg.Value = "bbt" Then
            str_query = "SELECT BSC_HOST,BSC_USERNAME,BSC_PASSWORD,BSC_PORT,BSC_FROMEMAIL FROM BSU_COMMUNICATION_M WHERE BSC_TYPE='BRIGHTBUSREG' AND BSC_BSU_ID='900500'"
            ' str_query = "SELECT BSC_HOST,BSC_USERNAME,BSC_PASSWORD,BSC_PORT FROM BSU_COMMUNICATION_M WHERE BSC_TYPE='HELPDESK' AND BSC_BSU_ID='999998'"
        Else
            str_query = "SELECT BSC_HOST,BSC_USERNAME,BSC_PASSWORD,BSC_PORT,BSC_FROMEMAIL FROM BSU_COMMUNICATION_M WHERE BSC_TYPE='STSREG' AND BSC_BSU_ID='900501'"
            ' str_query = "SELECT BSC_HOST,BSC_USERNAME,BSC_PASSWORD,BSC_PORT FROM BSU_COMMUNICATION_M WHERE BSC_TYPE='HELPDESK' AND BSC_BSU_ID='999998'"
        End If
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim host As String = ""
        Dim fromId As String = ""
        Dim password As String = ""
        Dim userName As String = ""
        Dim port As Integer
        While reader.Read
            host = reader.GetString(0)
            userName = reader.GetString(1)
            password = reader.GetString(2)
            port = reader.GetValue(3)
            fromId = reader.GetString(4)
        End While
        reader.Close()

        str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        str_query = "SELECT TRM_DESCRIPTION,TRM_STARTDATE,TRM_ENDDATE,RTS_AMOUNT  FROM " _
                  & " TRANSPORT.RATES_M AS B  INNER JOIN TRANSPORT.RATES_S AS C ON B.RTM_ID=C.RTS_RTM_ID" _
                  & " INNER JOIN OASIS.DBO.TRM_M AS D ON C.RTS_REF_ID=D.TRM_ID" _
                  & " WHERE RTM_ACD_ID=" + hfACD_ID.Value + " AND RTM_SBL_ID=" + sbl_id + " ORDER BY TRM_STARTDATE"
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim fee As String = ""
        While reader.Read
            If fee <> "" Then
                fee += ","
            End If
            fee += reader("TRM_DESCRIPTION") + "(" + Format(reader("TRM_STARTDATE"), "MMM") + " to " + Format(reader("TRM_ENDDATE"), "MMM") + ") Aed : " + Round(reader("RTS_AMOUNT")).ToString
        End While
        reader.Close()

        sr = sr.Replace("%fee%", fee)


        sr = SetEmailHeader(sr, reg)
        Dim retVal As String = ""
        Dim AlternatreMailBody As AlternateView

        AlternatreMailBody = convertToEmbedResource(sr, "1")
        retVal = SendNewsLetters(fromId, emailId, "Confirmation of transport", AlternatreMailBody, userName, password, host, port, "1", False)
        '  retVal = SendPlainTextEmails(fromId, "dhanyaajith1001@gmail.com", "Confirmation of transport", sr, fromId, password, host, port, "0", False)

        If retVal = "Successfully send" Then
            lblError.Text = "Email sent successfully"
        Else
            lblError.Text = "Email could not be sent"
        End If

    End Sub

    Function SetEmailHeader(ByVal sr As String, ByVal reg As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim address As String
        If reg = "bbt" Then
            str_query = "exec getBsuInFo '900500','logo'"
            sr = sr.Replace("%img%", "Schools/BBT/BRIGHT BUS LOGO.JPG")
            sr = sr.Replace("%regunit%", "Bright Bus Transport")
        Else
            str_query = "exec getBsuInfo '900501','logo'"
            sr = sr.Replace("%img%", "Schools/STS/STSLOGO_ONLY.gif")
            sr = sr.Replace("%regunit%", "School Transport Services")
        End If

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            sr = sr.Replace("%bsu%", reader("BSU_NAME"))
            sr = sr.Replace("%pobox%", "Po Box : " + reader("BSU_POBOX"))
            address = "Tel :" + reader("BSU_TEL") + " ,Fax : " + reader("BSU_FAX") + " ,Email : " + reader("BSU_EMAIL")
            sr = sr.Replace("%address%", address)
            sr = sr.Replace("%url%", reader("BSU_URL"))
        End While
        reader.Close()
        Return sr
    End Function

    Public Shared Function SendNewsLetters(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As AlternateView, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
        Dim RetutnValue = ""
        Try

            Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

            msg.Subject = Subject

            msg.AlternateViews.Add(MailBody)

            msg.Priority = Net.Mail.MailPriority.High

            msg.IsBodyHtml = True


            Dim client As New System.Net.Mail.SmtpClient(Host, Port)

            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            client.Send(msg)

            RetutnValue = "Successfully send"

        Catch ex As Exception
            RetutnValue = "Error : " & ex.Message
        End Try

        Return RetutnValue

    End Function


    Public Function convertToEmbedResource(ByVal emailHtml$, ByVal Templateid As String) As AlternateView

        'This is the website where the resources are located
        Dim ResourceUrl As String = WebConfigurationManager.AppSettings("WebsiteURLResource").ToString
        Dim webSiteUrl$ = ResourceUrl + Templateid + "/"

        ' The first regex finds all the url/src tags.
        Dim matchesCol As MatchCollection = Regex.Matches(emailHtml, "url\(['|\""]+.*['|\""]\)|src=[""|'][^""']+[""|']")

        Dim normalRes As Match


        Dim resCol As AlternateView = AlternateView.CreateAlternateViewFromString("", Nothing, "text/html")

        Dim resId% = 0

        ' Between the findings
        For Each normalRes In matchesCol

            Dim resPath$

            ' Replace it for the new content ID that will be embeded
            If Left(normalRes.Value, 3) = "url" Then
                emailHtml = emailHtml.Replace(normalRes.Value, "url(cid:EmbedRes_" & resId & ")")
            Else
                emailHtml = emailHtml.Replace(normalRes.Value, "src=""cid:EmbedRes_" & resId & """")
            End If

            ' Clean the path
            resPath = Regex.Replace(normalRes.Value, "url\(['|\""]", "")
            resPath = Regex.Replace(resPath, "src=['|\""]", "")
            resPath = Regex.Replace(resPath, "['|\""]\)", "").Replace(webSiteUrl, "").Replace("""", "")


            '' Map it on the server
            resPath = Server.MapPath(resPath)

            resPath = resPath.Replace("%20", " ")
            ' Embed the resource
            If resPath.LastIndexOf("http://") = -1 Then
                Dim theResource As LinkedResource = New LinkedResource(resPath)
                theResource.ContentId = "EmbedRes_" & resId
                resCol.LinkedResources.Add(theResource)
            End If

            ' Next resource ID
            resId = resId + 1

        Next


        ' Create our final object
        Dim finalEmail As AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString(emailHtml, Nothing, "text/html")
        Dim transferResource As LinkedResource

        ' And transfer all the added resources to the output object
        For Each transferResource In resCol.LinkedResources
            finalEmail.LinkedResources.Add(transferResource)
        Next

        Return finalEmail

    End Function

    Sub SendSMS(ByVal stu_id As String, ByVal mobile As String, ByVal area As String, ByVal pickup As String, ByVal busno As String, ByVal reg As String, ByVal feeID As String)
        mobile = mobile.Replace(" ", "")

        Dim z As Char() = {"0"}

        mobile = mobile.TrimStart(z)
        If Not mobile.Substring(0, 3) = "971" Then
            mobile = "971" + mobile
        End If
        Dim message As String = ""
        Dim pwd As String = Encr_decrData.Decrypt(System.Configuration.ConfigurationManager.AppSettings("smspassword").Replace(" ", "+"))
        Dim from As String
        Dim user As String = System.Configuration.ConfigurationManager.AppSettings("smsuser")
        Dim msg As String
        If reg = "bbt" Then
            message += "BBT"
            from = "BBT"
        Else
            message += "STS"
            from = "STS"
        End If
        message += " transportation confirmation : Registration No. : " + feeID + " ,Area : " + area + " ,Pickup Point : " + pickup + " ,BusNO : " + busno
        msg = sms.SendMessage(mobile, message, from, user, pwd)
        If msg.IndexOf("Error") <> -1 Then
            lblError.Text = "Unable to send sms to the number " + mobile
        Else
            lblError.Text = "Sms sent successfully"
        End If



    End Sub
#End Region

    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnArea_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnPickup_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        tblTPT.Rows(4).Visible = True
        tblTPT.Rows(5).Visible = True
        GridBind()
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub


    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        If e.CommandName = "view" Or e.CommandName = "edit" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
            Dim lblStuId As Label
            Dim lblParent As Label
            Dim lblReg As Label
            Dim lblEmail As Label
            Dim lblSblID As Label
            Dim lblBusNo As Label
            Dim lblArea As Label
            Dim lblPickup As Label
            Dim lblMobile As Label
            Dim lblFeeId As Label
            Dim lblPuser As Label
            With selectedRow
                lblStuId = .FindControl("lblStuId")
                lblParent = .FindControl("lblParent")
                lblReg = .FindControl("lblReg")
                lblEmail = .FindControl("lblEmail")
                lblSblID = .FindControl("lblSblID")
                lblFeeId = .FindControl("lblFeeId")
                lblPuser = .FindControl("lblPuser")
            End With
            If e.CommandName = "view" Then
                SendApproveEmail(lblReg.Text, lblStuId.Text, lblParent.Text, lblEmail.Text, lblSblID.Text, lblFeeId.Text, lblPuser.Text)
            ElseIf e.CommandName = "edit" Then
                With selectedRow
                    lblBusNo = .FindControl("lblBusNo")
                    lblArea = .FindControl("lblArea")
                    lblPickup = .FindControl("lblPickup")
                    lblMobile = .FindControl("lblMobile")
                    SendSMS(lblStuId.Text, lblMobile.Text, lblArea.Text, lblPickup.Text, lblBusNo.Text, lblReg.Text, lblFeeId.Text)
                End With
            End If
        End If
    End Sub

    Protected Sub gvStud_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStud.RowDeleting

    End Sub

    Protected Sub gvStud_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStud.RowEditing

    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        Try
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
            Dim li As New ListItem
            li.Text = "All"
            li.Value = "0"
            ddlGrade.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try


    End Sub
End Class
