Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studTransportRequestChange
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100211") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    ddlAcademicYear = PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = 0
                    ddlGrade.Items.Insert(0, li)
                    PopulateSection()
                    callCurrent_BsuShift()
                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvStud.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0

                    If ddlClm.Items.Count = 1 Then
                        ' tbPromote.Rows(0).Visible = False
                    End If
                    BindBusNo()
                    ' BindSublocation(ddlPArea)
                    ' BindSublocation(ddlDArea)



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else

            highlight_grid()

        End If
    End Sub

    Public Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String)
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                        & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        Return ddlAcademicYear
    End Function



    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            PopulateSection()
            If ViewState("norecord") = "1" Then

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            lblError.Text = ""
            GridBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Function GetPromotedYear() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " select top 1 acd_id from academicyear_d where acd_startdt>" _
                                & "(select acd_startdt from academicyear_d where acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                & " and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + ") and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + " order by acd_startdt"
        Dim acdid As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return acdid
    End Function

    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        If ddlGrade.SelectedValue = "0" Then
            ddlSection.Items.Insert(0, li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString & " ORDER BY SCT_DESCR "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub highlight_grid()
        For i As Integer = 0 To gvStud.Rows.Count - 1
            Dim row As GridViewRow = gvStud.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = ""
        '-------------------------- New Query ----------------------------------------------------------------
        Dim ACD_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim SHF_ID As String = String.Empty
        Dim SCT_ID As String = String.Empty

        If ddlAcademicYear.SelectedIndex = -1 Then
            ACD_ID = " AND A.STU_ACD_ID=''"
        Else
            If ddlAcademicYear.SelectedValue = "0" Then
                ACD_ID = " AND A.STU_ACD_ID<>''"
            Else
                ACD_ID = " AND A.STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "'"
            End If
        End If

        If ddlGrade.SelectedIndex = -1 Then
            GRD_ID = " AND A.STU_GRD_ID=''"
        Else
            If ddlGrade.SelectedValue = "0" Then
                GRD_ID = " AND A.STU_GRD_ID<>'0'"
            Else
                GRD_ID = " AND A.STU_GRD_ID='" & ddlGrade.SelectedValue & "'"
            End If
        End If

        If ddlSection.SelectedIndex = -1 Then
            SCT_ID = " AND A.STU_SCT_ID=''"
        Else
            If ddlSection.SelectedValue = "0" Then
                SCT_ID = " AND A.STU_SCT_ID<>'0'"
            Else
                SCT_ID = " AND A.STU_SCT_ID='" & ddlSection.SelectedValue & "'"
            End If
        End If


        If ddlShift.SelectedIndex = -1 Then
            SHF_ID = " AND A.STU_SHF_ID=''"
        Else
            If ddlShift.SelectedValue = "0" Then
                SHF_ID = " AND A.STU_SHF_ID<>'0'"
            Else
                SHF_ID = " AND A.STU_SHF_ID='" & ddlShift.SelectedValue & "'"
            End If

        End If





        Dim str_Sql As String = "" 'SBL_DESCRIPTION
        If rbTransYes.Checked = True Then
            str_query = " SELECT DISTINCT STU_ID,STU_NO,STU_BSU_ID,STU_GRM_ID,STU_ACD_ID,STU_GRD_ID,STU_SCT_ID,STU_SHF_ID,STU_FEE_ID,STU_CURRSTATUS,STU_NAME," & _
                    " (CAST(ISNULL(SBL_DESCRIPTION,'')AS VARCHAR)  + ' - Bus No(' + CAST(ISNULL(BNO_DESCR,'') AS VARCHAR)+ ') ' + CAST(ISNULL(TRP_DESCR,'')AS VARCHAR)) AS PICKUPOLD, " & _
                    " (CAST(ISNULL(STU_SBL_ID_DROPOFF,'')AS VARCHAR)+';'+CAST(ISNULL(STU_DROPOFF_TRP_ID,'')AS VARCHAR)+ ';'+CAST(ISNULL(STU_DROPOFF,'')AS VARCHAR)) AS DROPOLD, " & _
                    "  STU_PICKUP_TRP_ID, STU_PICKUP_BUSNO, STU_PICKUP, STU_DROPOFF_TRP_ID, STU_DROPOFF_BUSNO, STU_DROPOFF, STU_LEAVEDATE, STU_SBL_ID_PICKUP " & _
                    "  STU_SBL_ID_DROPOFF, GRADE, SHF_DESCR, TRP_DESCR, PNT_DESCRIPTION,pickupbus,dropoffbus " & _
                    " from (SELECT  STUDENT_M.STU_ID, STUDENT_M.STU_NO, STUDENT_M.STU_BSU_ID, STUDENT_M.STU_GRM_ID, STUDENT_M.STU_ACD_ID, " & _
                    " STUDENT_M.STU_GRD_ID, STUDENT_M.STU_SCT_ID, STUDENT_M.STU_SHF_ID, STUDENT_M.STU_FEE_ID, STUDENT_M.STU_CURRSTATUS, " & _
                    " rtrim(ltrim(ISNULL(STUDENT_M.STU_FIRSTNAME, ''))) + ' ' + ISNULL(STUDENT_M.STU_MIDNAME, ' ') + ' ' + ISNULL(STUDENT_M.STU_LASTNAME, '') AS STU_NAME, " & _
                    " STUDENT_M.STU_PICKUP_TRP_ID, STUDENT_M.STU_PICKUP_BUSNO,VB.BNO_DESCR,STUDENT_M.STU_PICKUP, STUDENT_M.STU_DROPOFF_TRP_ID, " & _
                    " STUDENT_M.STU_DROPOFF_BUSNO, STUDENT_M.STU_DROPOFF, STUDENT_M.STU_LEAVEDATE, STUDENT_M.STU_SBL_ID_PICKUP," & _
                    " STUDENT_M.STU_SBL_ID_DROPOFF,  VV_GRADE_BSU_M.GRM_DISPLAY +' ' + VV_SECTION_M.SCT_DESCR as GRADE, VV_SHIFTS_M.SHF_DESCR, " & _
                    " TRANSPORT.TRIPS_M.TRP_DESCR, TRANSPORT.PICKUPPOINTS_M.PNT_DESCRIPTION,TRANSPORT.SUBLOCATION_M.SBL_DESCRIPTION,vb.bno_descr as pickupbus,cc.bno_descr as dropoffbus " & _
                    "  FROM   STUDENT_M INNER JOIN  VV_SECTION_M ON STUDENT_M.STU_ACD_ID = VV_SECTION_M.SCT_ACD_ID AND STUDENT_M.STU_SCT_ID = VV_SECTION_M.SCT_ID AND " & _
                    " STUDENT_M.STU_GRD_ID = VV_SECTION_M.SCT_GRD_ID INNER JOIN  VV_GRADE_BSU_M ON STUDENT_M.STU_ACD_ID = VV_GRADE_BSU_M.GRM_ACD_ID AND " & _
                    "  STUDENT_M.STU_GRD_ID = VV_GRADE_BSU_M.GRM_GRD_ID INNER JOIN    VV_SHIFTS_M ON STUDENT_M.STU_SHF_ID = VV_SHIFTS_M.SHF_ID AND STUDENT_M.STU_BSU_ID = VV_SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                    "  TRANSPORT.TRIPS_M ON STUDENT_M.STU_PICKUP_TRP_ID = TRANSPORT.TRIPS_M.TRP_ID " & _
                    "  INNER JOIN   TRANSPORT.VV_BUSES VB ON TRANSPORT.TRIPS_M.TRP_ID =VB.TRP_ID INNER JOIN " & _
                    " TRANSPORT.PICKUPPOINTS_M ON STUDENT_M.STU_PICKUP = TRANSPORT.PICKUPPOINTS_M.PNT_ID INNER JOIN " & _
                    " TRANSPORT.SUBLOCATION_M ON STUDENT_M.STU_SBL_ID_PICKUP=TRANSPORT.SUBLOCATION_M.SBL_ID " & _
                     " left outer join transport.vv_buses as cc on STUDENT_M.stu_dropoff_trp_id=cc.trp_id " & _
                     " )A WHERE     (ISNULL(A.STU_PICKUP, 0) <> 0) AND (ISNULL(A.STU_DROPOFF, 0) <> 0) AND (CONVERT(DATETIME,A.STU_LEAVEDATE) IS NULL OR " & _
                     " CONVERT(DATETIME,A.STU_LEAVEDATE)>getdate()) AND A.STU_CURRSTATUS<>'CN'"

          
            If ddlBusNo.SelectedValue <> "ALL" Then
                If rdOnward.Checked = True Then
                    str_query += " and pickupbus='" + ddlBusNo.SelectedItem.Text + "'"
                Else
                    str_query += " and dropoffbus='" + ddlBusNo.SelectedItem.Text + "'"
                End If
            End If


        ElseIf rbTransNo.Checked = True Then
            str_query = "  select distinct STU_ID,STU_NO,STU_BSU_ID,STU_GRM_ID,STU_ACD_ID,STU_GRD_ID,STU_SCT_ID,STU_SHF_ID,STU_FEE_ID,STU_CURRSTATUS,STU_NAME," & _
                        " '' AS PICKUPOLD, " & _
                        " (CAST(ISNULL(STU_SBL_ID_DROPOFF,'')AS VARCHAR)+';'+CAST(ISNULL(STU_DROPOFF_TRP_ID,'')AS VARCHAR)+ ';'+CAST(ISNULL(STU_DROPOFF,'')AS VARCHAR)) AS DROPOLD, " & _
                        " STU_PICKUP_TRP_ID, STU_PICKUP_BUSNO, STU_PICKUP, STU_DROPOFF_TRP_ID, STU_DROPOFF_BUSNO, STU_DROPOFF, STU_LEAVEDATE, STU_SBL_ID_PICKUP " & _
                        " STU_SBL_ID_DROPOFF,GRADE,SHF_DESCR,TRP_DESCR,PNT_DESCRIPTION from (SELECT  STUDENT_M.STU_ID, STUDENT_M.STU_NO, STUDENT_M.STU_BSU_ID, STUDENT_M.STU_GRM_ID, STUDENT_M.STU_ACD_ID, " & _
                        " STUDENT_M.STU_GRD_ID, STUDENT_M.STU_SCT_ID, STUDENT_M.STU_SHF_ID, STUDENT_M.STU_FEE_ID, STUDENT_M.STU_CURRSTATUS, " & _
                        " rtrim(ltrim(ISNULL(STUDENT_M.STU_FIRSTNAME, ''))) + ' ' + ISNULL(STUDENT_M.STU_MIDNAME, ' ') + ' ' + ISNULL(STUDENT_M.STU_LASTNAME, '') AS STU_NAME, " & _
                        "  STUDENT_M.STU_PICKUP_TRP_ID, STUDENT_M.STU_PICKUP_BUSNO, STUDENT_M.STU_PICKUP, STUDENT_M.STU_DROPOFF_TRP_ID," & _
                        "  STUDENT_M.STU_DROPOFF_BUSNO, STUDENT_M.STU_DROPOFF, STUDENT_M.STU_LEAVEDATE, STUDENT_M.STU_SBL_ID_PICKUP, " & _
                        "  STUDENT_M.STU_SBL_ID_DROPOFF,  VV_GRADE_BSU_M.GRM_DISPLAY +' ' + VV_SECTION_M.SCT_DESCR as GRADE, VV_SHIFTS_M.SHF_DESCR,'' AS TRP_DESCR,'' AS PNT_DESCRIPTION " & _
                        "  FROM  STUDENT_M INNER JOIN   VV_SECTION_M ON STUDENT_M.STU_ACD_ID = VV_SECTION_M.SCT_ACD_ID AND STUDENT_M.STU_SCT_ID = VV_SECTION_M.SCT_ID AND " & _
                        "  STUDENT_M.STU_GRD_ID = VV_SECTION_M.SCT_GRD_ID INNER JOIN   VV_GRADE_BSU_M ON STUDENT_M.STU_ACD_ID = VV_GRADE_BSU_M.GRM_ACD_ID AND " & _
                        "  STUDENT_M.STU_GRD_ID = VV_GRADE_BSU_M.GRM_GRD_ID INNER JOIN   VV_SHIFTS_M ON STUDENT_M.STU_SHF_ID = VV_SHIFTS_M.SHF_ID AND STUDENT_M.STU_BSU_ID = VV_SHIFTS_M.SHF_BSU_ID " & _
                        "  )A WHERE     (ISNULL(A.STU_PICKUP, 0) = 0 AND ISNULL(A.STU_DROPOFF, 0) = 0) AND (CONVERT(DATETIME,A.STU_LEAVEDATE) IS NULL OR CONVERT(DATETIME,A.STU_LEAVEDATE)>getdate())" & _
                        "  AND A.STU_CURRSTATUS<>'CN'"

        End If
        ' " (CAST(ISNULL(STU_SBL_ID_PICKUP,'')AS VARCHAR)  + ';' + CAST(ISNULL(STU_PICKUP_TRP_ID,'')AS VARCHAR)  + ';' + CAST(ISNULL(STU_PICKUP,'') AS VARCHAR)) AS PICKUPOLD, " & _
        '--------------------------End New Query --------------------------------------------------------------

        str_query += ACD_ID + GRD_ID + SHF_ID

       

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""
        ViewState("rowIndex") = 0

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter = " AND " + GetSearchString("A.STU_NAME", txtSearch.Text, strSearch)
            End If
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += " AND " + GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & ACD_ID & GRD_ID & SHF_ID & SCT_ID & " ORDER BY A.STU_NAME")
        gvStud.DataSource = ds


        If ds.Tables(0).Rows.Count = 0 Then
            ViewState("norecord") = "1"
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            btnUpdate.Visible = False
        Else
            ViewState("norecord") = "0"
            gvStud.DataBind()
            btnUpdate.Visible = True
        End If

        If gvStud.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtFeeSearch")
            txtSearch.Text = strFee

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = strName
        End If
    End Sub


    Sub SaveChanges(ByVal stuid As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.saveCHANGEDETAILS " _
                                & stuid + "," _
                                & "'" + Session("sbsuid") + "'," _
                                & ddlAcademicYear.SelectedValue.ToString + "," _
                                & hfPSBL_ID.Value + "," _
                                & hfPTRP_ID.Value + "," _
                                & hfPICKUP.Value + "," _
                                & ddlPArea.SelectedValue.ToString + "," _
                                & ddlPTrip.SelectedValue.ToString + "," _
                                & ddlPickup.SelectedValue.ToString + "," _
                                & hfDSBL_ID.Value + "," _
                                & hfDTRP_ID.Value + "," _
                                & hfDROPOFF.Value + "," _
                                & ddlDArea.SelectedValue.ToString + "," _
                                & ddlDTrip.SelectedValue.ToString + "," _
                                & ddlDropOff.SelectedValue.ToString + "," _
                                & hfROUNDTRIP.Value + "," _
                                & ddlTripType.SelectedValue.ToString
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


    End Sub
    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim i As Integer
        Dim str_query As String
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim ddlPickUp As DropDownList
        Dim ddlDropOff As DropDownList
        Dim ddlDArea As DropDownList
        Dim ddlPArea As DropDownList
        Dim ddlDTrip As DropDownList
        Dim ddlPTrip As DropDownList
        Dim cselect As Boolean = False

        Dim stuNosSaved As String = ""

        Dim stuNosNotSaved As String = ""
        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            lblStuId = gvStud.Rows(i).FindControl("lblStuId")
            Dim radStatus As RadioButtonList = gvStud.Rows(i).FindControl("RadioButtonList1")

            If chkSelect.Checked = True Then
                cselect = True
                If radStatus.SelectedIndex = 0 Then
                    str_query = " exec [TRANSPORT].[saveTRANSPORTCHANGEREQUEST] " _
                              & "'" + Session("sbsuid") + "'," _
                              & ddlAcademicYear.SelectedValue + "," _
                              & "'" + lblStuId.Text.ToString + "'," _
                              & "'false'"
                Else
                    str_query = " exec [TRANSPORT].[saveTRANSPORTCHANGEREQUEST] " _
                              & "'" + Session("sbsuid") + "'," _
                              & ddlAcademicYear.SelectedValue + "," _
                              & "'" + lblStuId.Text.ToString + "'," _
                              & "'true'"
                End If


                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                stuNosSaved = lblStuId.Text
                If stuNosSaved <> "" Then
                    stuNosSaved += ","
                End If
                stuNosSaved += lblStuId.Text
            End If
        Next

        If stuNosNotSaved <> "" Then
            lblError.Text = "The following records " + stuNosNotSaved + " could not be saved as the service facility starting date is within the Fee Closing date"
        ElseIf cselect = True Then
            lblError.Text = "Record saved successfully"
        Else
            lblError.Text = "No records selected"
        End If
    End Sub

    Sub CallReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptTransportReqInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function

    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

    Sub BindSublocation(ByVal ddlSublocation As DropDownList)
        ddlSublocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        '    Dim str_query As String = "SELECT SBL_ID=convert(varchar(100),SBL_ID)+'|" + ViewState("rowIndex").ToString + "',SBL_DESCRIPTION FROM TRANSPORT.VV_SUBLOCATION_M WHERE SBL_BSU_ID='" + Session("SBSUID") + "'"
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A " _
                                 & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID WHERE PNT_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " ORDER BY SBL_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSublocation.DataSource = ds
        ddlSublocation.DataTextField = "SBL_DESCRIPTION"
        ddlSublocation.DataValueField = "SBL_ID"
        ddlSublocation.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSublocation.Items.Insert(0, li)
        ds = Nothing
    End Sub

    'Sub BindPickup(ByVal ddlPickup As DropDownList, ByVal sblId As String)
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.VV_PICKUPOINTS_M WHERE PNT_SBL_ID=" + sblId _
    '                             & " AND PNT_BSU_ID='"+SESSION("SBSUID")+"'"

    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlPickup.DataSource = ds
    '    ddlPickup.DataTextField = "PNT_DESCRIPTION"
    '    ddlPickup.DataValueField = "PNT_ID"
    '    ddlPickup.DataBind()
    '    Dim li As New ListItem
    '    li.Text = "--"
    '    li.Value = "0"
    '    ddlPickup.Items.Insert(0, li)
    '    ds = Nothing

    '    li = New ListItem

    '    For Each li In ddlPickup.Items
    '        li.Attributes.Add("title", li.Text)
    '    Next
    'End Sub

    Sub BindPickup(ByVal ddl As DropDownList, ByVal trdId As String, ByVal sblid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT DISTINCT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M AS A  " _
                                & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.PNT_ID=B.TPP_PNT_ID AND PNT_SBL_ID=" + sblid _
                                & " WHERE TPP_TRP_ID = " + trdId _
                                & " ORDER BY PNT_DESCRIPTION"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "PNT_DESCRIPTION"
        ddl.DataValueField = "PNT_ID"
        ddl.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddl.Items.Insert(0, li)
        ds = Nothing

        li = New ListItem

        For Each li In ddl.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub

    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String, ByVal sblid As String)
        ddlTrip.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = " SELECT DISTINCT TRP_ID AS TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_SBL_ID=" + sblid _
                        & " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT TRP_ID AS TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_SBL_ID=" + sblid _
                 & "  AND TRD_TODATE IS NULL"



        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"

        ddlTrip.Items.Insert(0, li)

        For Each li In ddlTrip.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Function CheckDates() As Boolean
        Dim flag As Boolean = True

        Dim i As Integer
        Dim txtDate As TextBox
        Dim lblErr As Label
        Dim chkSelect As CheckBox
        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                txtDate = gvStud.Rows(i).FindControl("txtDate")
                lblErr = gvStud.Rows(i).FindControl("lblErr")
                If txtDate.Text = "" Then
                    lblErr.Visible = True
                    flag = False
                ElseIf IsDate(txtDate.Text) = False Then
                    lblErr.Visible = True
                    flag = False
                End If
            End If
        Next

        Return flag

    End Function

#End Region

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
          
                Dim lblSTUID As Label

                With e.Row

                  
                    lblSTUID = .FindControl("lblStuid")
                    Dim radStatus As RadioButtonList = .FindControl("RadioButtonList1")
                    Dim lblEdit As LinkButton
                    lblEdit = .FindControl("lblEdit")


                    'To get The Saved recotrds
                    Dim ds As DataSet = GetSaveRecord(lblSTUID.Text)

                    radStatus.SelectedIndex = Convert.ToInt16(ds.Tables(0).Rows(0).Item("Bactive"))
                    If ds.Tables(0).Rows(0).Item("Bactive").ToString.ToLower = "false" Then
                        lblEdit.Enabled = False
                    End If

                    If ds.Tables(0).Rows(0).Item("Bchanged").ToString.ToLower = "true" Then
                        e.Row.BackColor = Drawing.Color.Coral
                    End If

                    'ViewState("rowIndex") += 1
                    'If CheckSaveRecord(lblSTUID.Text) = True Then
                    '    e.Row.BackColor = Drawing.Color.Cornsilk
                    '    e.Row.Enabled = CheckEditRecord()
                    'Else
                    '    e.Row.Enabled = True
                    'End If
                End With
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Private Function CheckSaveRecord(ByVal STU_ID As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT CR_STU_ID FROM TRANSPORT.CR_CHANGEREQUEST WHERE CR_STU_ID='" & STU_ID & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then
            Return True
        Else
            Return False
        End If

    End Function
    Private Function CheckEditRecord() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_bTPTRESPONSEEDIT FROM dbo.BUSINESSUNIT_M WHERE BSU_ID='" & Session("sbsuid") & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then
            Return ds.Tables(0).Rows(0).Item("BSU_bTPTRESPONSEEDIT")
        Else
            Return False
        End If
    End Function
    Private Function GetSaveRecord(ByVal STU_ID As String) As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT isnull(Bactive,'true') as BActive,isnull(cr_Bchanged,'false') as BChanged FROM TRANSPORT.CR_CHANGEREQUEST WHERE CR_STU_ID='" & STU_ID & "' AND CR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Return ds
    End Function

    'Protected Sub ddlPArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        lblError.Text = ""
    '        Dim chkSelect As CheckBox
    '        Dim ddlPsbl As DropDownList = DirectCast(sender, DropDownList)

    '        Dim parea As String() = ddlPsbl.SelectedValue.Split("|")

    '        Dim ddlPickUp As DropDownList
    '        Dim ddlDSbl As DropDownList
    '        Dim ddlDropOff As DropDownList

    '        ddlPickUp = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlPickUp")
    '        ddlDSbl = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlDArea")
    '        ddlDSbl.ClearSelection()
    '        ddlDSbl.Items.FindByValue(ddlPsbl.SelectedValue).Selected = True
    '        Dim darea As String() = ddlDSbl.SelectedValue.Split("|")
    '        ddlDropOff = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlDropOff")
    '        BindPickup(ddlPickUp, parea(0))
    '        BindPickup(ddlDropOff, darea(0))
    '        chkSelect = gvStud.Rows(CType(parea(1), Integer)).FindControl("chkSelect")
    '        chkSelect.Checked = True
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub

    'Protected Sub ddlDArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        lblError.Text = ""
    '        Dim ddlDsbl As DropDownList = DirectCast(sender, DropDownList)

    '        Dim darea As String() = ddlDsbl.SelectedValue.Split("|")

    '        Dim ddlDropOff As DropDownList

    '        ddlDropOff = gvStud.Rows(CType(darea(1), Integer)).FindControl("ddlDropOff")
    '        BindPickup(ddlDropOff, darea(0))
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            SaveData()
            GridBind()
        Catch ex As Exception
            'UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
            Dim li As New ListItem
            li.Text = "All"
            li.Value = 0
            ddlGrade.Items.Insert(0, li)
            PopulateSection()
            If ViewState("norecord") = "1" Then

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
            Dim li As New ListItem
            li.Text = "All"
            li.Value = 0
            ddlGrade.Items.Insert(0, li)
            PopulateSection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callCurrent_BsuShift()
    End Sub

    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                            ddlShift.SelectedIndex = ItemTypeCounter
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblStuId As Label
        Dim lblName As Label
        Dim lblFeeId As Label
        lblStuId = TryCast(sender.findcontrol("lblStuId"), Label)
        lblName = TryCast(sender.findcontrol("lblSName"), Label)
        lblFeeId = TryCast(sender.findcontrol("lblFeeId"), Label)



        lblStudName.Text = lblName.Text
        lblstudNo.Text = lblFeeId.Text
        hfSTU_ID.Value = lblStuId.Text
        GetData(lblStuId.Text)
        MPChangeTransport.Show()

        Dim radStatus As RadioButtonList = TryCast(sender.FindControl("RadioButtonList1"), RadioButtonList)
        Dim lblEdit As LinkButton
        lblEdit = TryCast(sender.FindControl("lblEdit"), LinkButton)


        'To get The Saved recotrds
        'Dim ds As DataSet = GetSaveRecord(lblStuId.Text)

        'radStatus.SelectedIndex = Convert.ToInt16(ds.Tables(0).Rows(0).Item("Bactive"))
        'If ds.Tables(0).Rows(0).Item("Bactive").ToString.ToLower = "false" Then
        '    lblEdit.Enabled = False
        'End If

        'If ds.Tables(0).Rows(0).Item("Bchanged").ToString.ToLower = "true" Then
        '    'e.Row.BackColor = Drawing.Color.Coral
        '    'gvStud.SelectedRow.BackColor = Drawing.Color.Coral

        'End If
    End Sub
    Sub BindBusNo()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BNO_ID,BNO_DESCR FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlBusNo.Items.Insert(0, li)

    End Sub
    Sub GetData(ByVal stuid As String)

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT  distinct  isnull(B.LOC_ID,0),isnull(D.SBL_ID,0),isnull(C.PNT_ID,0)," _
        '                         & " isnull(E.LOC_ID,0),isnull(F.SBL_ID,0),isnull(G.PNT_ID,0)," _
        '                         & " isnull(A.STU_ACD_ID,0),isnull(A.STU_ROUNDTRIP,0),ISNULL(P.TRP_ID) " _
        '                         & " FROM  STUDENT_M AS A " _
        '                         & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
        '                         & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
        '                         & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
        '                         & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
        '                         & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
        '                         & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
        '                         & " LEFT OUTER JOIN TRANSPORT.TRIPS_M AS P ON A.STU_PICKUP_TRP_ID=P.TRP_ID" _
        '                         & " LEFT OUTER JOIN TRANSPORT.TRIPS_M AS Q ON A.STU_DROPOFF_TRP_ID=Q.TRP_ID" _
        '                         & " WHERE STU_ID =" + stuid
        Dim clis(8) As Integer

        Dim str_query As String = "SELECT ISNULL(CR_SBL_ID_PICKUP_NEW,0),ISNULL(CR_SBL_ID_DROPOFF_NEW,0),ISNULL(CR_PICKUP_NEW,0),ISNULL(CR_DROPOFF_NEW,0),ISNULL(CR_TRP_ID_PICKUP_NEW,0),ISNULL(CR_TRP_ID_DROPOFF_NEW,0),ISNULL(CR_ROUNDTRIP_NEW,0)" _
                            & " FROM TRANSPORT.CR_CHANGEREQUEST WHERE ISNULL(BACTIVE,'TRUE')='TRUE' AND ISNULL(CR_bCHANGED,'FALSE')='TRUE'" _
                            & " AND CR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND CR_STU_ID=" + stuid

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read

            clis(0) = reader.GetValue(0)
            clis(1) = reader.GetValue(1)
            clis(2) = reader.GetValue(2)
            clis(3) = reader.GetValue(3)
            clis(4) = reader.GetValue(4)
            clis(5) = reader.GetValue(5)
            clis(6) = reader.GetValue(6)

        End While

        reader.Close()
       

        str_query = "SELECT isnull(STU_SBL_ID_PICKUP,0),isnull(STU_SBL_ID_DROPOFF,0),isnull(STU_PICKUP,0),isnull(STU_DROPOFF,0),isnull(STU_PICKUP_TRP_ID,0),isnull(STU_DROPOFF_TRP_ID,0),isnull(STU_ROUNDTRIP,0)" _
                            & " FROM STUDENT_M WHERE STU_ID=" + stuid
        Dim lis(8) As Integer
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read

            lis(0) = reader.GetValue(0)
            lis(1) = reader.GetValue(1)
            lis(2) = reader.GetValue(2)
            lis(3) = reader.GetValue(3)
            lis(4) = reader.GetValue(4)
            lis(5) = reader.GetValue(5)
            lis(6) = reader.GetValue(6)

            hfPSBL_ID.Value = reader.GetValue(0)
            hfDSBL_ID.Value = reader.GetValue(1)
            hfPICKUP.Value = reader.GetValue(2)
            hfDROPOFF.Value = reader.GetValue(3)
            hfPTRP_ID.Value = reader.GetValue(4)
            hfDTRP_ID.Value = reader.GetValue(5)
            hfROUNDTRIP.Value = reader.GetValue(6)


        End While

        reader.Close()



        If clis(0) <> 0 Then
            BindData(clis)
        Else
            BindData(lis)
        End If


    End Sub

    Sub BindData(ByVal lis As Integer())

        ' lblTripValue.Text = lis(8)
        If lis(6) = 0 Then
            lblTripType.Text = "RoundTrip"
        ElseIf lis(6) = 1 Then
            lblTripType.Text = "Pick Up"
        ElseIf lis(6) = 2 Then
            lblTripType.Text = "Drop Off"
        End If
        ddlTripType.Items.FindByValue(lis(6)).Selected = True


        'ddlPLocation = BindLocation(ddlPLocation)
        'If lis(0) <> 0 Then
        '    ddlPLocation.Items.FindByValue(lis(0)).Selected = True
        '    lblPlocation.Text = ddlPLocation.SelectedItem.Text
        'End If

        hfPSBL_ID.Value = lis(0)
        BindSublocation(ddlPArea)
        If lis(0) <> 0 Then
            ddlPArea.Items.FindByValue(lis(0)).Selected = True
            lblPArea.Text = ddlPArea.SelectedItem.Text

            BindTrip(ddlPTrip, "Onward", lis(0))
            If lis(4) <> 0 Then
                ddlPTrip.Items.FindByValue(lis(4)).Selected = True


                BindPickup(ddlPickup, lis(4), lis(0))
                If lis(2) <> 0 Then
                    ddlPickup.Items.FindByValue(lis(2)).Selected = True
                    lblPickup.Text = ddlPickup.SelectedItem.Text
                End If
            End If


        End If

        hfDSBL_ID.Value = lis(1)
        BindSublocation(ddlDArea)
        If lis(1) <> 0 Then
            ddlDArea.Items.FindByValue(lis(1)).Selected = True
            lblDArea.Text = ddlDArea.SelectedItem.Text
            BindTrip(ddlDTrip, "Return", lis(1))
            If lis(5) <> 0 Then
                ddlDTrip.Items.FindByValue(lis(5)).Selected = True


                BindPickup(ddlDropOff, lis(5), lis(1))
                If lis(3) <> 0 Then
                    ddlDropOff.Items.FindByValue(lis(3)).Selected = True
                    lblDropoff.Text = ddlDropOff.SelectedItem.Text
                End If
            End If
        End If
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        MPChangeTransport.Hide()
        MPChangeTransport.Dispose()
        MPChangeTransport = Nothing
    End Sub

    Protected Sub gvStud_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStud.RowEditing

    End Sub

    
    Protected Sub ddlPArea_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPArea.SelectedIndexChanged
        If ddlPArea.SelectedValue <> "0" Then
            BindTrip(ddlPTrip, "Onward", ddlPArea.SelectedValue.ToString)
            BindPickup(ddlPickup, 0, 0)
        End If
    End Sub

    Protected Sub ddlDArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDArea.SelectedIndexChanged
        If ddlDArea.SelectedValue <> "0" Then
            BindTrip(ddlDTrip, "Return", ddlPArea.SelectedValue.ToString)
            BindPickup(ddlDropOff, 0, 0)
        End If
    End Sub

    Protected Sub ddlPTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPTrip.SelectedIndexChanged
        If ddlPTrip.SelectedValue <> "0" Then
            BindPickup(ddlPickup, ddlPTrip.SelectedValue.ToString, ddlPArea.SelectedValue.ToString)
        End If
    End Sub

    Protected Sub ddlDTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTrip.SelectedIndexChanged
        If ddlDTrip.SelectedValue <> "0" Then
            BindPickup(ddlDropOff, ddlDTrip.SelectedValue.ToString, ddlDArea.SelectedValue.ToString)
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim panel As UpdatePanel = Master.FindControl("UpdatePanel1")
        panel.UpdateMode = UpdatePanelUpdateMode.Conditional
        panel.ChildrenAsTriggers = True
        'If Not IsPostBack Then
        '    Dim smScriptManager As New ScriptManager
        '    smScriptManager = Master.FindControl("ScriptManager1")
        '    smScriptManager.EnablePartialRendering = False
        'End If
    End Sub

   
    Protected Sub btnStudSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStudSave.Click
        If ddlPArea.SelectedValue = 0 Or ddlDArea.SelectedValue = 0 Or ddlPickup.SelectedValue = 0 Or ddlDropOff.SelectedValue = 0 Or ddlPTrip.SelectedValue = 0 Or ddlDTrip.SelectedValue = 0 Then
            lblError.Text = "Please select all the fields"
            Exit Sub
        End If
        SaveChanges(hfSTU_ID.Value)
    End Sub
End Class
