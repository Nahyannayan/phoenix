﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.UI.WebControls
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Imports Telerik.Web.UI
Imports System.Linq
Partial Class Transport_TransportPromotions
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim Latest_refid As Integer = 0
    Dim MainObj As New Mainclass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImpTrRate)
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    'get the data mode from the query string to check if in add or edit mode 
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    'get the menucode to confirm the user is accessing the valid page
                    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                    Page.Title = OASISConstants.Gemstitle
                    Clear()
                    BindBsu()
                    BindAcademicyear()

                    If ViewState("datamode") = "add" Then
                        hfTPT_PROMO_ID.Value = 0
                    Else
                        hfTPT_PROMO_ID.Value = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        GetPromotionDetails(hfTPT_PROMO_ID.Value)
                        DisabledControls()
                    End If
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Page Load: " + ex.Message, "TransportPromotions")
            End Try
        Else

        End If
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            Clear()
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
    End Sub

    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                'lblError.CssClass = "error"
                usrMessageBar.ShowNotification(Message, UserControls_usrMessageBar.WarningType.Danger)
            Else
                'lblError.CssClass = "error"
                usrMessageBar.ShowNotification(Message, UserControls_usrMessageBar.WarningType.Success)
            End If
            'lblError.CssClass = ""
        End If
        'lblError.Text = Message
    End Sub
    Protected Sub GetPromotionDetails(ByVal promo_Id As Integer)
        Try
            Dim conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ds As DataSet
            Dim Param(2) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@TPT_PROMO_ID", promo_Id, SqlDbType.Int)
            Param(1) = Mainclass.CreateSqlParameter("@TYPE", "DETAILS", SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GET_TRANSPORTPROMOTIONS", Param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPromotionName.Text = ds.Tables(0).Rows(0)("TPT_PROMO_NAME").ToString()
                txtPromotionDescr.Text = ds.Tables(0).Rows(0)("TPT_PROMO_DESCR").ToString()
                txtAmount.Text = ds.Tables(0).Rows(0)("AMOUNT").ToString()
                rbtnlstPromotionPeriod.SelectedValue = ds.Tables(0).Rows(0)("TPT_CHARGING_SCHEDULE").ToString()
                rbFeeType.SelectedValue = ds.Tables(0).Rows(0)("TPT_PROMO_TYPE").ToString()

                If ds.Tables(0).Rows(0)("TPT_CHARGING_SCHEDULE").ToString() = "DT" Then
                    txtFromDate.Text = Format(ds.Tables(0).Rows(0)("TPT_PROMO_FROM_DATE"), "dd/MMM/yyyy")
                    txtToDate.Text = Format(ds.Tables(0).Rows(0)("TPT_PROMO_TO_DATE"), "dd/MMM/yyyy")
                    trDates.Visible = True
                    txtDaysWeek.Visible = False
                    Promodays.Visible = False
                ElseIf ds.Tables(0).Rows(0)("TPT_CHARGING_SCHEDULE").ToString() = "DY" Then
                    txtDaysWeek.Text = ds.Tables(0).Rows(0)("TPT_PROMO_PERIOD_VALUE").ToString()
                    trDates.Visible = False
                    txtDaysWeek.Visible = True
                    Promodays.Visible = True
                ElseIf ds.Tables(0).Rows(0)("TPT_CHARGING_SCHEDULE").ToString() = "WK" Then
                    txtDaysWeek.Text = ds.Tables(0).Rows(0)("TPT_PROMO_PERIOD_VALUE").ToString()
                    trDates.Visible = False
                    txtDaysWeek.Visible = True
                    Promodays.Visible = True
                End If
                ddlAcdId.SelectedValue = ds.Tables(0).Rows(0)("ACD_ID")
                ddlBusinessunit.SelectedValue = ds.Tables(0).Rows(0)("TPT_PROMO_BSU_ID")
                If rbFeeType.SelectedValue = "FS" Then
                    Dim GridData As DataSet
                    Dim Param1(2) As SqlParameter
                    Param1(0) = Mainclass.CreateSqlParameter("@TPT_PROMO_ID", promo_Id, SqlDbType.Int)
                    Param1(1) = Mainclass.CreateSqlParameter("@TYPE", "PROMODETAILS", SqlDbType.VarChar)
                    Param1(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    GridData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GET_TRANSPORTPROMOTIONS", Param1)
                    VSgvTrRateImport = GridData.Tables(0)
                    BindTrRateExcel()
                    lblAmount.Visible = False
                    txtAmount.Visible = False
                Else
                    lblAmount.Visible = True
                    txtAmount.Visible = True
                End If
            End If
                btnTrCommit.Visible = False
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetPromotionDetails(): " + ex.Message, "TransportPromotions")
        End Try
    End Sub


    Private Sub PopulateSubNodes(ByVal dt As DataTable,
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("SVB_BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
        Next
    End Sub



    Private Sub PopulateSubLevel(ByVal parentid As String, ByVal parentNode As TreeNode)
        Dim dtTable As DataTable = FeeCommon.SERVICES_BSU_M(Session("sUsr_name"), Session("sBsuid"))
        PopulateSubNodes(dtTable, parentNode.ChildNodes)
    End Sub
    Private Sub Clear()
        txtPromotionName.Text = ""
        txtPromotionDescr.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtDaysWeek.Text = ""
        txtAmount.Text = ""
        rbtnlstPromotionPeriod.SelectedItem.Value = "DT"
        trDates.Visible = True
        txtDaysWeek.Visible = False
        'lblError.Text = ""
        'PopulateTree()
        hfTPT_PROMO_ID.Value = 0
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim BSUParms(1) As SqlParameter
            BSUParms(0) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            BSUParms(1) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            ddlBusinessunit.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "gETUNITSFORTRANSPORTFEES", BSUParms)
            ddlBusinessunit.DataTextField = "BSU_NAME"
            ddlBusinessunit.DataValueField = "SVB_BSU_ID"
            ddlBusinessunit.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindAcademicyear()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim strsql As String = "SELECT  ACY.ACY_DESCR + ' (' + CLM.CLM_DESCR + ')' AS ACY_DESCR, ACD.ACD_ID   FROM    OASIS.dbo.ACADEMICYEAR_D AS ACD WITH (NOLOCK) "
        strsql = strsql + "INNER JOIN OASIS.dbo.ACADEMICYEAR_M AS ACY WITH (NOLOCK) ON ACD.ACD_ACY_ID = ACY.ACY_ID INNER JOIN OASIS.dbo.CURRICULUM_M AS CLM WITH (NOLOCK) ON ACD.ACD_CLM_ID = CLM.CLM_ID "
        strsql = strsql + " WHERE   (ACD.ACD_BSU_ID ='" & ddlBusinessunit.SelectedValue & "') AND (ISNULL(ACD.ACD_bCLOSE, 0) = 0) AND ISNULL(ACY_bSHOW, 0) = 1 AND ACD_ID >= (SELECT MIN(ACD_ID) "
        strsql = strsql + " FROM OASIS.dbo.ACADEMICYEAR_D WITH (NOLOCK) WHERE ACD_BSU_ID ='" & ddlBusinessunit.SelectedValue & "' AND  ISNULL(ACD_CURRENT,0) = 1)     ORDER BY ACD.ACD_STARTDT"
        ddlAcdId.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
        ddlAcdId.DataTextField = "ACY_DESCR"
        ddlAcdId.DataValueField = "ACD_ID"
        ddlAcdId.DataBind()


    End Sub
    Private Sub DisabledControls()
        txtPromotionName.Enabled = False
        txtPromotionDescr.Enabled = False
        txtFromDate.Enabled = False
        txtToDate.Enabled = False
        txtDaysWeek.Enabled = False
        txtAmount.Enabled = False
        rbtnlstPromotionPeriod.Enabled = False
        txtFromDate.Enabled = False
        txtToDate.Enabled = False
        txtDaysWeek.Enabled = False
        btnTrCommit.Enabled = False
        ddlBusinessunit.Enabled = False
        ddlAcdId.Enabled = False
        rbFeeType.Enabled = False



    End Sub
    Private Sub EnabledControls()
        txtPromotionName.Enabled = True
        txtPromotionDescr.Enabled = False
        txtFromDate.Enabled = True
        txtToDate.Enabled = True
        txtDaysWeek.Enabled = True
        txtAmount.Enabled = True
        rbtnlstPromotionPeriod.Enabled = True
        txtFromDate.Enabled = True
        txtToDate.Enabled = True
        txtDaysWeek.Enabled = True

    End Sub
    Protected Sub rbtnlstPromotionPeriod_SelectedIndexChanged(sender As Object, e As EventArgs)
        If rbtnlstPromotionPeriod.SelectedValue = "DT" Then
            trDates.Visible = True
            txtDaysWeek.Visible = False
            txtDaysWeek.Text = ""
            Promodays.Visible = False

        ElseIf rbtnlstPromotionPeriod.SelectedValue = "DY" Then
            trDates.Visible = False
            txtDaysWeek.Visible = True
            'spanDaysWeek.InnerText = "Days"
            txtFromDate.Text = ""
            txtToDate.Text = ""
            Promodays.Text = "Promotion Days"
            Promodays.Visible = True
        ElseIf rbtnlstPromotionPeriod.SelectedValue = "WK" Then
            trDates.Visible = False
            txtDaysWeek.Visible = True
            'spanDaysWeek.InnerText = "Weeks"
            txtFromDate.Text = ""
            txtToDate.Text = ""
            Promodays.Visible = True
            Promodays.Text = "Promotion Weeks"
        End If
    End Sub
    Protected Sub rbtnRateType_SelectedIndexChanged(sender As Object, e As EventArgs)
        If rbFeeType.SelectedValue = "FC" Then
            txtAmount.Visible = True
            lblAmount.Visible = True
            upload.Visible = False
            Import.Visible = True
            ExcelGrid.Visible = False
            If hfAlreadySelected.Value = 1 Then
                BindBsu()
                BindAcademicyear()
            End If
            hfAlreadySelected.Value = 1

        ElseIf rbFeeType.SelectedValue = "FS" Then
            txtAmount.Visible = False
            lblAmount.Visible = False
            upload.Visible = True
            Import.Visible = False
            ExcelGrid.Visible = True
            If hfAlreadySelected.Value = 1 Then
                BindBsu()
                BindAcademicyear()
            End If
            hfAlreadySelected.Value = 1

        End If
    End Sub



    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'lblError.Text = ""
            ViewState("datamode") = "add"
            Clear()
            EnabledControls()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            ShowMessage("Request could not be processed", False)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'lblError.Text = ""
            If hfTPT_PROMO_ID.Value = 0 Then
                'lblError.Text = "No records to edit"
                ShowMessage("No records to edit", True)
                Exit Sub
            End If
            EnabledControls()
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            UtilityObj.beforeLoopingControls(Me.Page)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            ShowMessage("Request could not be processed", False)
        End Try
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("datamode") = "delete"
            If hfTPT_PROMO_ID.Value = 0 Then

                ShowMessage("No records to delete", True)
                Exit Sub
            End If
            SaveData()
            Clear()
            DisabledControls()
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            ShowMessage("Request could not be processed", False)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            hfTPT_PROMO_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                Clear()
                DisabledControls()
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim strans As SqlTransaction = Nothing
        Dim objConn As New SqlConnection(str_conn)
        'objConn.Open()

        Dim param(13) As SqlClient.SqlParameter

        Dim fromdt As String = Nothing
            Dim todt As String = Nothing
            Dim daysORweeksCount As String = Nothing
            Dim weeksCount As String = Nothing
            Dim Amount As Double = 0

            If rbtnlstPromotionPeriod.SelectedValue = "DT" Then
                fromdt = txtFromDate.Text.Trim()
            todt = txtToDate.Text.Trim()
            daysORweeksCount = 0
            ElseIf rbtnlstPromotionPeriod.SelectedValue = "DY" Then
                daysORweeksCount = txtDaysWeek.Text.Trim()
                fromdt = "1900-01-01"
                todt = "1900-01-01"
            ElseIf rbtnlstPromotionPeriod.SelectedValue = "WK" Then
                daysORweeksCount = txtDaysWeek.Text.Trim()
                fromdt = "1900-01-01"
                todt = "1900-01-01"
            End If
            If LTrim(RTrim(txtAmount.Text)) = "" Then
                Amount = 0
            Else
                Amount = LTrim(RTrim(txtAmount.Text))
            End If

            param(0) = Mainclass.CreateSqlParameter("@TPT_PROMO_ID", hfTPT_PROMO_ID.Value, SqlDbType.Int, True)
            param(1) = Mainclass.CreateSqlParameter("@TPT_PROMO_NAME", txtPromotionName.Text.Trim(), SqlDbType.VarChar)
            param(2) = Mainclass.CreateSqlParameter("@TPT_PROMO_DESCR", txtPromotionDescr.Text, SqlDbType.VarChar)
            param(3) = Mainclass.CreateSqlParameter("@TPT_PROMO_PERIOD", rbtnlstPromotionPeriod.SelectedValue, SqlDbType.VarChar)
            param(4) = Mainclass.CreateSqlParameter("@TPT_PROMO_FROM_DATE", fromdt, SqlDbType.VarChar)
        param(5) = Mainclass.CreateSqlParameter("@TPT_PROMO_TO_DATE", todt, SqlDbType.VarChar)
        param(6) = Mainclass.CreateSqlParameter("@TPT_PROMO_PERIOD_VALUE", daysORweeksCount, SqlDbType.Int)
            param(7) = Mainclass.CreateSqlParameter("@TPT_PROMO_AMOUNT", Amount, SqlDbType.Int)
            param(8) = Mainclass.CreateSqlParameter("@TPT_PROMO_CREATED_BY", Session("sUsr_name"), SqlDbType.VarChar)
        param(9) = Mainclass.CreateSqlParameter("@TPT_PROMO_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        param(10) = Mainclass.CreateSqlParameter("@SBSU_ID", ddlBusinessunit.SelectedValue, SqlDbType.VarChar)
        param(11) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcdId.SelectedValue, SqlDbType.Int)
        param(12) = Mainclass.CreateSqlParameter("@MODE", rbFeeType.SelectedValue, SqlDbType.VarChar)
        param(13) = Mainclass.CreateSqlParameter("@BATCH", ViewState("FTI_BATCHNO"), SqlDbType.Int)
        'param(13) = Mainclass.CreateSqlParameter("@ReturnValue", 0, SqlDbType.Int, True)


        'Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETRANSPORTPROMOTIONS_M", param)
                If RetVal = "0" Then
                stTrans.Commit()
                Message = "Record saved successfully."
                DisabledControls()
                ShowMessage(Message, False)
                    SetDataMode("view")
                    GetPromotionDetails(param(0).Value)
                Else
                stTrans.Rollback()
                Message = getErrorMessage("4000")
                    ShowMessage(Message, True)
                End If

            Catch ex As Exception
                Message = getErrorMessage("4000")
            ShowMessage(ex.Message, True)
            UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "TransportPromotions")

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Sub SaveData1()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim param(13) As SqlClient.SqlParameter
            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@TPT_PROMO_ID", SqlDbType.Int, True)
            param(0).Value = hfTPT_PROMO_ID.Value



            param(1) = New SqlClient.SqlParameter("@TPT_PROMO_NAME", SqlDbType.VarChar)
            param(1).Value = txtPromotionName.Text.Trim()
            param(2) = New SqlClient.SqlParameter("@TPT_PROMO_DESCR", SqlDbType.VarChar)
            param(2).Value = txtPromotionDescr.Text
            param(3) = New SqlClient.SqlParameter("@TPT_PROMO_PERIOD", SqlDbType.Char)
            param(3).Value = rbtnlstPromotionPeriod.SelectedValue

            Dim fromdt As String = Nothing
            Dim todt As String = Nothing
            Dim daysORweeksCount As String = Nothing
            Dim weeksCount As String = Nothing
            Dim Amount As Double = 0

            If rbtnlstPromotionPeriod.SelectedValue = "DT" Then
                fromdt = txtFromDate.Text.Trim()
                todt = txtToDate.Text.Trim()
            ElseIf rbtnlstPromotionPeriod.SelectedValue = "DY" Then
                daysORweeksCount = txtDaysWeek.Text.Trim()
                fromdt = "1900-01-01"
                todt = "1900-01-01"
            ElseIf rbtnlstPromotionPeriod.SelectedValue = "WK" Then
                daysORweeksCount = txtDaysWeek.Text.Trim()
                fromdt = "1900-01-01"
                todt = "1900-01-01"
            End If
            If LTrim(RTrim(txtAmount.Text)) = "" Then
                Amount = 0
            Else
                Amount = LTrim(RTrim(txtAmount.Text))
            End If
            param(4) = New SqlClient.SqlParameter("@TPT_PROMO_FROM_DATE", SqlDbType.VarChar)
            param(4).Value = fromdt
            param(5) = New SqlClient.SqlParameter("@TPT_PROMO_TO_DATE", SqlDbType.VarChar)
            param(5).Value = todt
            param(6) = New SqlClient.SqlParameter("@TPT_PROMO_PERIOD_VALUE", SqlDbType.Int)
            param(6).Value = daysORweeksCount

            param(7) = New SqlClient.SqlParameter("@TPT_PROMO_AMOUNT", SqlDbType.Decimal)
            param(7).Value = Amount

            param(8) = New SqlClient.SqlParameter("@TPT_PROMO_CREATED_BY", SqlDbType.VarChar)
            param(8).Value = Session("sUsr_name")

            param(9) = New SqlClient.SqlParameter("@TPT_PROMO_BSU_ID", SqlDbType.VarChar)
            param(9).Value = ddlBusinessunit.SelectedValue

            param(10) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
            param(10).Value = ddlAcdId.SelectedValue

            param(11) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar)
            param(11).Value = rbFeeType.SelectedValue


            param(12) = New SqlClient.SqlParameter("@BATCH", SqlDbType.VarChar)
            param(12).Value = ViewState("FTI_BATCHNO")

            param(13) = New SqlClient.SqlParameter("@ReturnValue", SqlDbType.Int)
            param(13).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "SAVETRANSPORTPROMOTIONS_M", param)
            'ret = SqlCmd.Parameters("@ReturnValue").Value
            If param(13).Value = 0 Then
                strans.Commit()
                If ViewState("datamode") = "add" Then
                    Message = "Record saved successfully."
                ElseIf ViewState("datamode") = "edit" Then
                    Message = "Record updated successfully."
                ElseIf ViewState("datamode") = "delete" Then
                    Message = "Record deleted successfully."
                End If
                If ViewState("datamode") = "add" Then
                    Clear()
                End If
                DisabledControls()
                ShowMessage(Message, False)
                SetDataMode("view")
                GetPromotionDetails(param(0).Value)

            Else
                strans.Rollback()
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(ex.Message, True)
            UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "TransportPromotions")

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Private Sub ddlBusinessunit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        BindAcademicyear()
    End Sub
    Protected Sub btnImpTrRate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim conn As New OleDbConnection
        'Me.lblError.Text = ""
        Try
            Dim str_conn As String = ""
            Dim cmd As New OleDbCommand
            Dim da As New OleDbDataAdapter
            Dim dtXcel As DataTable
            Dim FileName As String = ""
            If (FileUpload2.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload2.FileName).ToString().ToLower()
                'FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload2.FileName.ToString()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload2.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload2.SaveAs(FileName)
                Else
                    ShowMessage("Only Excel files are allowed", False)
                    Exit Try
                End If

                dtXcel = Mainclass.FetchFromExcelIntoDataTable(FileName, 1, 1, 10)
                dtXcel.Columns.Add(New DataColumn("Status", GetType(System.String), ""))

                If Not dtXcel Is Nothing AndAlso dtXcel.Rows.Count > 0 Then
                    dtXcel.AcceptChanges()
                    VSgvTrRateImport = dtXcel.Copy
                    BindTrRateExcel()
                Else
                    ShowMessage("Empty Document!!", False)
                End If

            Else
                ShowMessage("File not Found!!", False)
            End If

            File.Delete(FileName) 'delete the file after copying the contents
        Catch
            ShowMessage(Err.Description, False)
        End Try
    End Sub
    Private Property VSgvTrRateImport() As DataTable
        Get
            Return ViewState("gvTrRateImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvTrRateImport") = value
        End Set
    End Property
    Private Sub ValidateRate(ByVal DT As DataTable)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim Success As Boolean = True
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        ' Dim cmd As New SqlCommand
        Dim dsOldRates As New DataSet
        ViewState("FTI_BATCHNO") = 0
        Try
            If DT.Columns.Contains("SlNo") Then
                DT.Columns.Remove("SlNo")
                DT.AcceptChanges()
            End If
            Dim pParms(7) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Me.ddlBusinessunit.SelectedValue

            pParms(1) = New SqlClient.SqlParameter("@TPR_ACD_ID", SqlDbType.Int)
            pParms(1).Value = Me.ddlAcdId.SelectedValue

            pParms(2) = New SqlClient.SqlParameter("@TPR_USER", SqlDbType.VarChar, 50)
            pParms(2).Value = Session("sUsr_name")

            pParms(3) = New SqlClient.SqlParameter("@TPR_BATCHNO", SqlDbType.BigInt)
            pParms(3).Direction = ParameterDirection.Output

            pParms(4) = New SqlClient.SqlParameter("@DT_RATES", SqlDbType.Structured)
            pParms(4).Value = DT
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue

            Dim retval As String

            dsOldRates = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "[DataImport].[VALIDATE_AND_IMPORT_TPT_PROMO_RATE]", pParms)
            retval = pParms(5).Value
            If retval <> 0 Then
                VSgvTrRateImport = dsOldRates.Tables(0)
                BindTrRateExcel()
                'Me.btntrProceed.Visible = False
                Me.btnTrCommit.Visible = False
                Me.btnImpTrRate.Visible = True
                Me.btnValidate.Visible = True
                stTrans.Rollback()
                ShowMessage("Some issues with Area and location", True)

            Else
                stTrans.Commit()
                ViewState("FTI_BATCHNO") = pParms(3).Value
                Me.btnTrCommit.Visible = True
                Me.btnCancel.Visible = True
                Me.btnImpTrRate.Visible = False
                Me.btnValidate.Visible = False
                VSgvTrRateImport = dsOldRates.Tables(0)
                BindTrRateExcel()
                Import.Visible = True
            End If
        Catch ex As Exception
            'Me.btntrProceed.Visible = False
            Me.btnTrCommit.Visible = False
            Me.btnImpTrRate.Visible = True
            stTrans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Sub ShowRateComparison(ByVal ds As DataSet)
        'If ds.Tables(0).Rows.Count > 0 Then
        '    VSgvTrRateImport = ds.Tables(0).Copy()
        '    'Dim n1 = VSgvTrRateImport.Columns("T1NORMAL_RATE").DataType

        '    Try
        '        Dim columns As List(Of DataControlField) = gvTrRateImport.Columns.Cast(Of DataControlField)().ToList()
        '        columns.Find(Function(col) col.HeaderText = "New Rate").Visible = True
        '        columns.Find(Function(col) col.HeaderText = "Existing Rate").Visible = True
        '        columns.Find(Function(col) col.HeaderText = "Difference").Visible = True
        '    Catch ex As Exception

        '    End Try
        'End If
        BindTrRateExcel()
    End Sub
    Sub BindTrRateExcel()
        If Not VSgvTrRateImport Is Nothing Then
            Me.gvTrRateImport.DataSource = VSgvTrRateImport
            Me.gvTrRateImport.DataBind()
        Else
            'Me.trGvImport.Visible = False
        End If

    End Sub

    Protected Sub gvTrRateImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Me.gvTrRateImport.PageIndex = e.NewPageIndex
        BindTrRateExcel()
    End Sub
    Protected Sub gvTrRateImport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    End Sub

    Private Sub btnValidate_Click(sender As Object, e As EventArgs) Handles btnValidate.Click
        ValidateRate(VSgvTrRateImport)
    End Sub

    Private Sub btnTrCommit_Click(sender As Object, e As EventArgs) Handles btnTrCommit.Click
        Try
            Dim validationsPass As Boolean = True

            If rbFeeType.SelectedValue = "FC" Then
                If (txtAmount.Text = "" Or txtAmount.Text = "0") Then
                    Message = getErrorMessage("4029")
                End If
            End If


            If ((txtPromotionName.Text = "" Or txtPromotionName.Text Is Nothing) Or
                (txtPromotionDescr.Text Is Nothing Or txtPromotionDescr.Text = "")) Then
                Message = getErrorMessage("4029")
                ShowMessage(Message, True)
                validationsPass = False
            ElseIf rbtnlstPromotionPeriod.SelectedValue = "DT" Then
                If ((txtFromDate.Text = "" Or txtFromDate.Text Is Nothing) Or
               (txtToDate.Text = "" Or txtToDate.Text Is Nothing)) Then
                    Message = getErrorMessage("4029")
                    ShowMessage(Message, True)
                    validationsPass = False
                Else

                    Dim promoStDt As Date = DateTime.Parse(txtFromDate.Text.Trim())
                    Dim promoEndDt As Date = DateTime.Parse(txtToDate.Text.Trim())
                    If (promoEndDt < promoStDt) Then
                        Message = "From date should be less than To date"
                        ShowMessage(Message, True)
                        validationsPass = False
                    End If
                End If
            ElseIf rbtnlstPromotionPeriod.SelectedValue = "DY" Or rbtnlstPromotionPeriod.SelectedValue = "WK" Then
                If (txtDaysWeek.Text = "" Or txtDaysWeek.Text Is Nothing) Then
                    Message = getErrorMessage("4029")
                    ShowMessage(Message, True)
                    validationsPass = False
                End If
            End If

            Dim strBusinessUnits As String = ""
            If validationsPass = True Then
                SaveData()
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "TransportPromotions")
        End Try
    End Sub

End Class
