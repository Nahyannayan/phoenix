﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studChangeAreaSameRateBulk
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100550") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = 0
                    ddlGrade.Items.Insert(0, li)
                    PopulateSection()

                    BindArea()
                    BindTrip(ddlTrip, "Onward", ddlArea.SelectedValue.ToString)
                    BindPickup(ddlPickup, ddlTrip.SelectedValue.ToString, ddlArea.SelectedValue.ToString)

                    DisplayRows(False)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else

            highlight_grid()

        End If
    End Sub

#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvStud.Rows.Count - 1
            Dim row As GridViewRow = gvStud.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        If ddlGrade.SelectedValue = "0" Then
            ddlSection.Items.Insert(0, li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindArea()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT DISTINCT A.SBL_ID AS SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A " _
                                 & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                 & " INNER JOIN TRANSPORT.FN_getAREARATE(" + ddlAcademicYear.SelectedValue.ToString + ") AS C ON A.SBL_ID=C.SBL_ID" _
                                 & " WHERE PNT_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " ORDER BY SBL_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlArea.DataSource = ds
        ddlArea.DataTextField = "SBL_DESCRIPTION"
        ddlArea.DataValueField = "SBL_ID"
        ddlArea.DataBind()
    End Sub

    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String, ByVal sblid As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = " SELECT DISTINCT CONVERT(VARCHAR(100),TRP_ID)+'|'+CONVERT(VARCHAR(100),TRD_ID) AS TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_SBL_ID=" + sblid _
                        & " AND TRP_BSU_ID='" + Session("SBSUID") + "'" _
                        & " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT CONVERT(VARCHAR(100),TRP_ID)+'|'+CONVERT(VARCHAR(100),TRD_ID) AS TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_SBL_ID=" + sblid _
                  & " AND TRP_BSU_ID='" + Session("SBSUID") + "'" _
                 & " AND TRD_TODATE IS NULL"

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"

        ddlTrip.Items.Insert(0, li)

        For Each li In ddlTrip.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub


    Sub BindPickup(ByVal ddlPickup As DropDownList, ByVal trdId As String, ByVal sblid As String)
        Dim strtrip As String()
        If trdId <> "0" Then
            strtrip = trdId.Split("|")
            trdId = strtrip(1)
        End If
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M AS A  " _
                                & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.PNT_ID=B.TPP_PNT_ID AND PNT_SBL_ID=" + sblid _
                                & " WHERE TPP_TRD_ID = " + trdId _
                                & " ORDER BY PNT_DESCRIPTION"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPickup.DataSource = ds
        ddlPickup.DataTextField = "PNT_DESCRIPTION"
        ddlPickup.DataValueField = "PNT_ID"
        ddlPickup.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlPickup.Items.Insert(0, li)
        ds = Nothing

        li = New ListItem

        For Each li In ddlPickup.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub

    Sub BindChangeArea()
         Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = " SELECT DISTINCT SBL_ID,AREA FROM TRANSPORT.FN_getAREARATE(" + ddlAcademicYear.SelectedValue.ToString + ") AS A " _
                                & " WHERE AMOUNT=(SELECT AMOUNT FROM TRANSPORT.FN_GETAREARATE(" + ddlAcademicYear.SelectedValue.ToString + ") AS B WHERE SBL_ID=" + ddlArea.SelectedValue.ToString _
                                & " AND A.TERM=B.TERM)AND SBL_ID<>" + ddlArea.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAreaChange.DataSource = ds
        ddlAreaChange.DataTextField = "AREA"
        ddlAreaChange.DataValueField = "SBL_ID"
        ddlAreaChange.DataBind()
    End Sub

    Sub DisplayRows(ByVal value As Boolean)
        tblTPT.Rows(3).Visible = value
        tblTPT.Rows(4).Visible = value
        tblTPT.Rows(5).Visible = value
        tblTPT.Rows(6).Visible = value
        tblTPT.Rows(7).Visible = value
        tblTPT.Rows(8).Visible = value
        tblTPT.Rows(9).Visible = value
        tblTPT.Rows(10).Visible = value
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME," _
                                 & " GRM_DISPLAY,SCT_DESCR,SSV_FROMDATE FROM STUDENT_M AS A" _
                                 & " INNER JOIN STUDENT_SERVICES_D AS B ON A.STU_ID=B.SSV_STU_ID" _
                                 & " INNER JOIN OASIS..GRADE_BSU_M AS C ON A.STU_GRM_ID=C.GRM_ID" _
                                 & " INNER JOIN OASIS..SECTION_M AS D ON A.STU_SCT_ID=D.SCT_ID" _
                                 & " AND A.STU_ACD_ID=B.SSV_ACD_ID AND SSV_TODATE IS NULL" _
                                 & " AND STU_SBL_ID_PICKUP=SSV_SBL_ID" _
                                 & " WHERE SSV_SBL_ID=" + ddlArea.SelectedValue.ToString _
                                 & " AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND SSV_SVC_ID=1"

        Dim strTrip As String()
        Dim trpid As String

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If

        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If


        If ddlTrip.SelectedValue.ToString <> "0" Then
            strTrip = ddlTrip.SelectedValue.Split("|")
            trpid = strTrip(0)
            str_query += " AND SSv_PICKUP_TRP_ID=" + trpid
        End If

        If ddlPickup.SelectedValue.ToString <> "0" Then
            str_query += " AND SSV_PICKUP=" + ddlPickup.SelectedValue.ToString
        End If


        str_query += "ORDER BY GRM_DISPLAY,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim txtDate As TextBox
        Dim lblDate As Label


        Dim strPTrip As String()
        Dim strDTrip As String()

        strPTrip = ddlPTripChange.SelectedValue.Split("|")
        strDTrip = ddlDTripChange.SelectedValue.Split("|")


        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblStuId = gvStud.Rows(i).FindControl("lblStuId")
                txtDate = gvStud.Rows(i).FindControl("txtDate")
                lblDate = gvStud.Rows(i).FindControl("lblDate")

                If CDate(txtDate.Text) >= CDate(lblDate.Text) Then
                    str_query = "exec  [TRANSPORT].[saveCHANGEAREAFORSAMERATE] " _
                                & "@BSU_ID='" + Session("sbsuid") + "'," _
                                & "@STU_ID=" + lblStuId.Text + "," _
                                & "@STU_SBL_ID_PICKUP=" + ddlAreaChange.SelectedValue.ToString + "," _
                                & "@STU_SBL_ID_DROPOFF=" + ddlAreaChange.SelectedValue.ToString + "," _
                                & "@STU_PICKUP=" + ddlPickupChange.SelectedValue.ToString + "," _
                                & "@STU_DROPOFF=" + ddlDropOff.SelectedValue.ToString + "," _
                                & "@STU_PICKUP_TRD_ID=" + strPTrip(1) + "," _
                                & "@STU_DROPOFF_TRD_ID=" + strDTrip(1) + "," _
                                & "@STU_SBL_ID_PICKUP_OLD=NULL," _
                                & "@STU_SBL_ID_DROPOFF_OLD=NULL," _
                                & "@FROMDATE='" + txtDate.Text + "'," _
                                & "@USR='" + Session("susr_name") + "'," _
                                & "@CLM_ID=" + ddlClm.SelectedValue.ToString

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End If
        Next
    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        Dim li As New ListItem
        li.Text = "All"
        li.Value = 0
        ddlGrade.Items.Insert(0, li)
        PopulateSection()
        DisplayRows(False)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
        DisplayRows(False)
    End Sub

    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlArea.SelectedIndexChanged
        BindTrip(ddlTrip, "Onward", ddlArea.SelectedValue.ToString)
        BindPickup(ddlPickup, ddlTrip.SelectedValue.ToString, ddlArea.SelectedValue.ToString)
        DisplayRows(False)
    End Sub

    Protected Sub ddlTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrip.SelectedIndexChanged
        BindPickup(ddlPickup, ddlTrip.SelectedValue.ToString, ddlArea.SelectedValue.ToString)
        DisplayRows(False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindChangeArea()
        BindTrip(ddlPTripChange, "Onward", ddlAreaChange.SelectedValue.ToString)
        BindPickup(ddlPickupChange, ddlPTripChange.SelectedValue.ToString, ddlAreaChange.SelectedValue.ToString)
        BindTrip(ddlDTripChange, "Return", ddlAreaChange.SelectedValue.ToString)
        BindPickup(ddlDropOff, ddlDTripChange.SelectedValue.ToString, ddlAreaChange.SelectedValue.ToString)
        DisplayRows(True)
        GridBind()
    End Sub

    Protected Sub ddlPTripChange_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPTripChange.SelectedIndexChanged
        BindPickup(ddlPickupChange, ddlPTripChange.SelectedValue.ToString, ddlAreaChange.SelectedValue.ToString)
    End Sub

    Protected Sub ddlDTripChange_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTripChange.SelectedIndexChanged
        BindPickup(ddlDropOff, ddlDTripChange.SelectedValue.ToString, ddlAreaChange.SelectedValue.ToString)
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlAreaChange.SelectedValue = "0" Then
            lblError.Text = "Please select the changed area"
            Exit Sub
        End If
        If ddlPTripChange.SelectedValue = "0" Then
            lblError.Text = "Please select the changed pickup trip."
            Exit Sub
        End If
        If ddlPTripChange.SelectedValue = "0" Then
            lblError.Text = "Please select the changed pickup trip."
            Exit Sub
        End If
        If ddlPickupChange.SelectedValue = "0" Then
            lblError.Text = "Please select the changed pickup."
            Exit Sub
        End If
        If ddlDropOff.SelectedValue = "0" Then
            lblError.Text = "Please select the changed dropoff."
            Exit Sub
        End If

        SaveData()
        GridBind()
    End Sub

  

   
    Protected Sub ddlAreaChange_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAreaChange.SelectedIndexChanged
        BindTrip(ddlPTripChange, "Onward", ddlAreaChange.SelectedValue.ToString)
        BindPickup(ddlPickupChange, ddlPTripChange.SelectedValue.ToString, ddlAreaChange.SelectedValue.ToString)
        BindTrip(ddlDTripChange, "Return", ddlAreaChange.SelectedValue.ToString)
        BindPickup(ddlDropOff, ddlDTripChange.SelectedValue.ToString, ddlAreaChange.SelectedValue.ToString)
    End Sub
End Class
