<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptVeh_Allocation_Edit.aspx.vb" Inherits="Transport_tptVeh_Allocation_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function getVehicleDetail(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;

            url = 'tptShowDetail.aspx?id=' + mode;
            document.getElementById('<%=hdn_mode.ClientID%>').value = mode;

            var oWnd = radopen(url, "pop_newitem");

        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();

            if (arg) {
                var mode = document.getElementById('<%=hdn_mode.ClientID%>').value
                //alert(mode);
                if (mode == 'H') {

                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtHyp.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfHyp.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'S') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtSupp.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=hfSupp.ClientID %>").value = NameandCode[1];
                }
                else if (mode == 'C') {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById("<%=txtCat.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfCat.ClientID %>").value = NameandCode[1];
            }

            else if (mode == 'I') {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtInsu.ClientID %>").value = NameandCode[0];
                 document.getElementById("<%=hfInsu.ClientID %>").value = NameandCode[1];
             }
             else if (mode == 'BO') {
                 NameandCode = arg.NameandCode.split('||');
                 document.getElementById("<%=txtBO.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBO.ClientID %>").value = NameandCode[1];
            }
            else if (mode == 'BP') {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtBP.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBP.ClientID %>").value = NameandCode[1];
            }
            else if (mode == 'BA') {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtBA.ClientID %>").value = NameandCode[0];
                 document.getElementById("<%=hfBA.ClientID %>").value = NameandCode[1];
             }
             else if (mode == 'M') {
                 NameandCode = arg.NameandCode.split('||');
                 document.getElementById("<%=txtMake.ClientID %>").value = NameandCode[0];
                 document.getElementById("<%=hfMake.ClientID %>").value = NameandCode[1];
             }
}
}
    </script>

    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_newitem" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Vehicle Allocation"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False" ValidationGroup="Error_Comm" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;Fields Marked with(<span class="text-danger">*</span>)are mandatory</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="title-bg-lite" colspan="4">
                                        <asp:Literal ID="ltLabel" runat="server" Text="Vehicle Allocation"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Order No</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtOrderNo" runat="server" MaxLength="20"></asp:TextBox></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Reg. No</span><span class="text-danger">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtRegNo" runat="server" MaxLength="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRegNo" runat="server" ControlToValidate="txtRegNo"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Reg. No required" ValidationGroup="Error_Comm">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"><span class="field-label">Reg.Date</span><span class="text-danger">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtRegDT" runat="server" AutoPostBack="True" OnTextChanged="txtRegDT_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgRegDT" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRegDT"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Reg. Date required" ForeColor=""
                                            ValidationGroup="Error_Comm">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Reg. Expiry Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtRegExpDT" runat="server" Width="110px"></asp:TextBox>
                                        <asp:ImageButton ID="imgRegExpDT" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Hypothicated To</span></td>
                                    <td align="left" width="30%">  <asp:TextBox ID="txtHyp" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgHyp" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('H');return false;" /></td>
                                </tr>
                               
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Vehicle Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Supplier Name</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtSupp" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgSupp" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('S');return false;" /></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Category</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCat" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCat" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('C');return false;" /></td>
                                    <td align="left"><span class="field-label">Plate Colour</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtPlateColor" runat="server" MaxLength="100">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Make</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMake" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgMake" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('M');return false;" /></td>
                                    <td align="left"><span class="field-label">Model</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtModel" runat="server" MaxLength="100"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Engine No</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtEngineNo" runat="server" MaxLength="100"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Chasis No</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtChasisNo" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Capacity</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCap" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Recommd.Milage</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRecommd" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Revaluation Value</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRev_Value" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Depreciation Value</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDep_value" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Invoice Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Invoice No</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtInvNo" runat="server"></asp:TextBox></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Invoice Value</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtInv_value" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Invoice Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtInvDT" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgInvDT" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Insurance Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Insurance Co</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtInsu" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgInsu" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('I');return false;" /></td>
                                    <td align="left"><span class="field-label">Insurance Value</span></td>
                                    <td align="left">  <asp:TextBox ID="txtIns_value" runat="server"></asp:TextBox></td>
                                </tr>
                              
                                <tr>
                                    <td align="left"><span class="field-label">Start Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtIns_StartDT" runat="server" AutoPostBack="True" OnTextChanged="txtIns_StartDT_TextChanged">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgInsStartDT" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick=";" /></td>
                                    <td align="left"><span class="field-label">Expiry Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtIns_ExpDT" runat="server">
                                        </asp:TextBox><asp:ImageButton ID="imgInsExpDT" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick=";" /></td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Other Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Owned By</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtBO" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgBO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('BO');return false;" />
                                        <asp:RequiredFieldValidator ID="rfvOwnedBy" runat="server" ControlToValidate="txtBO"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Vehicle owned by cannot be left empty"
                                            ForeColor="" ValidationGroup="Error_Comm">*</asp:RequiredFieldValidator></td>
                                    <td align="left"><span class="field-label">From Date</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtBODT" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnBO" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick=";" />
                                        <asp:RequiredFieldValidator ID="rfvOwnedByFromDT" runat="server" ControlToValidate="txtBODT"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                            ValidationGroup="Error_Comm">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Operated By</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtBP" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgBP" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('BP');return false;" /></td>
                                    <td align="left"><span class="field-label">From Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtBPDT" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnBP" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick=";" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Allocated To</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBA" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgBA" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleDetail('BA');return false;" /></td>
                                    <td align="left"><span class="field-label">From Date</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBADT" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnAllocDT" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick=";" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Card Number</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCardNumber" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Fuel Type</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlFuelType" runat="server"></asp:DropDownList>
                                    </td>

                                </tr>

                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Sale Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Sales Invoice No</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtSaleInvNo" runat="server"></asp:TextBox></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Sale Value</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtSaleValue" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Sale Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtSaleDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgSaleDT" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                </tr>

                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Permit Details</td>
                                </tr>


                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:GridView ID="gridPermitDetails" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVEP_ID" runat="server" Text='<%# Bind("VEP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVEP_VPT_ID" runat="server" Text='<%# Bind("VEP_VPT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVPT_DESCR" runat="server" Text='<%# Bind("VPT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlPermitType" AutoPostBack="True" runat="server"></asp:DropDownList>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("VEP_VPT_ID") %>' runat="server" />

                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:DropDownList ID="ddlPermitType" AutoPostBack="True" runat="server"></asp:DropDownList>
                                                        <asp:HiddenField ID="hdnITM_ID" Value='<%# Bind("VEP_VPT_ID") %>' runat="server" />
                                                        <asp:Label ID="lblVEP_VPT_ID" runat="server" Text='<%# Bind("VEP_VPT_ID") %>'></asp:Label>




                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Permit No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVEP_PTYPE_NO" runat="server" Style="text-align: left"
                                                            Text='<%# Bind("VEP_PERMIT_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPERMITNO" runat="server" Style="text-align: right"
                                                            Text='<%# Bind("VEP_PERMIT_NO") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtPERMITNO" runat="server" Style="text-align: right"
                                                            Text='<%# Bind("VEP_PERMIT_NO") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Issue Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChqDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("VEP_ISSDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>



                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtIssDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("VEP_ISSDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgEIssDateDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtIssDate" PopupButtonID="imgEIssDateDate">
                                                        </ajaxToolkit:CalendarExtender>

                                                        <asp:HiddenField ID="h_VEP_Trdate" Value='<%# Bind("VEP_TRDATE") %>' runat="server" />
                                                        <asp:HiddenField ID="h_P_DATE" Value='<%# Bind("P_Date") %>' runat="server" />



                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtIssDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("VEP_ISSDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgFIssDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtIssDate" PopupButtonID="imgFIssDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Expiry Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExpDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("VEP_EXPDATE", "{0:dd-MMM-yyyy}") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtExpDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("VEP_EXPDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgEExpDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtExpDate" PopupButtonID="imgEExpDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtExpDate" runat="server" Style="text-align: right"
                                                            Text='<%# eval("VEP_EXPDATE", "{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgFExpDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd-MMM-yyyy" runat="server"
                                                            TargetControlID="txtExpDate" PopupButtonID="imgFExpDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </FooterTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Renew?">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVPT_RENEW" runat="server" Text='<%# Bind("VEP_RENEW") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlRenew" AutoPostBack="True" runat="server"></asp:DropDownList>

                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:DropDownList ID="ddlRenew" AutoPostBack="True" runat="server"></asp:DropDownList>

                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditQUD" runat="server" CausesValidation="False" CommandName="Edit"
                                                            Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateQUD" runat="server" CausesValidation="True" CommandName="Update"
                                                            Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelQUD" runat="server" CausesValidation="False" CommandName="Cancel"
                                                            Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddQUD" runat="server" CausesValidation="False" CommandName="AddNew"
                                                            Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" OnClick="btnEdit_Click" />
                            <asp:Button ID="btnSave" runat="server"
                                CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="Error_Comm" />
                            <asp:Button ID="btnCancel"
                                runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>




                <ajaxToolkit:CalendarExtender ID="CalendarextenderRegDT" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgRegDT" TargetControlID="txtRegDT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarextenderRegExpDT" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgRegExpDT" TargetControlID="txtRegExpDT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarextenderInvDT" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgInvDT" TargetControlID="txtInvDT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarextenderInsExpDT" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgInsExpDT" TargetControlID="txtIns_ExpDT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderBO" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgbtnBO" TargetControlID="txtBODT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderBP" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgbtnBP" TargetControlID="txtBPDT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderBA" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgbtnAllocDT" TargetControlID="txtBADT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarextenderInsStartDT" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgInsStartDT" TargetControlID="txtIns_StartDT" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarextenderSaleDT" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgSaleDT" TargetControlID="txtSaleDate" CssClass="MyCalendar">
                </ajaxToolkit:CalendarExtender>



                &nbsp;<asp:HiddenField ID="hfHyp" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSupp" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfCat" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfInsu" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfMake" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfBO" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfBP" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_veh_id" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hGridDelete" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hfBA" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdn_mode" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>

