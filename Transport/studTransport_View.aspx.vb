Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studTransport_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                   Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100190") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid"))
                    BindShift()

                    If ddlClm.Items.Count = 1 Then
                        tblTpt.Rows(0).Visible = False
                    End If
                End If

                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else


        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnPBusNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnDBusNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvPick_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvDrop_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private Methods"

    Public Sub PopulateAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=" + ddlClm.SelectedValue.ToString _
                                  & " AND ACD_ACY_ID>=" + Session("Current_ACY_ID")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub
    Sub BindShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SHF_DESCR,SHF_ID FROM VV_SHIFTS_M WHERE SHF_BSU_ID='" + Session("SBSUID") + "' Order By SHF_DESCR DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                   & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR ," _
                   & " IMGPICK=CASE  WHEN STU_PICKUP_TRP_ID IS NULL THEN '~/images/cross.gif' ELSE '~/images/tick.gif' END," _
                   & " IMGDROP=CASE WHEN STU_DROPOFF_TRP_ID  IS NULL THEN '~/images/cross.gif' ELSE '~/images/tick.gif' END," _
                   & " bPICK=CASE  WHEN STU_PICKUP_TRP_ID IS NULL THEN 'TRUE' ELSE 'FALSE' END," _
                   & " bDROP=CASE WHEN STU_DROPOFF_TRP_ID  IS NULL THEN 'TRUE' ELSE 'FALSE' END," _
                   & " D.BNO_DESCR+'-'+D.TRP_DESCR AS PBUSNO,E.BNO_DESCR+'-'+E.TRP_DESCR AS DBUSNO" _
                   & " FROM STUDENT_M AS A INNER JOIN VV_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                   & " LEFT OUTER JOIN VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                   & " LEFT OUTER JOIN TRANSPORT.VV_BUSES AS D ON A.STU_PICKUP_TRP_ID=D.TRP_ID " _
                   & " LEFT OUTER JOIN TRANSPORT.VV_BUSES AS E ON A.STU_DROPOFF_TRP_ID=E.TRP_ID " _
                   & " WHERE STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + " AND STU_SHF_ID=" + ddlShift.SelectedValue.ToString _
                   & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                   & " AND STU_CURRSTATUS<>'CN' AND (STU_SBL_ID_PICKUP IS NOT NULL OR STU_SBL_ID_DROPOFF IS NOT NULL)"

           Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvPick As New DropDownList
        Dim ddlgvDrop As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStudTPT.Rows.Count > 0 Then


            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStudTPT.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text



            ddlgvPick = gvStudTPT.HeaderRow.FindControl("ddlgvPick")
            If ddlgvPick.Text <> "ALL" And ddlgvPick.Text.Trim <> "" Then
                If ddlgvPick.Text = "YES" Then
                    strFilter += " and stu_pickup_trp_id is not null"
                Else
                    strFilter += " and stu_pickup_trp_id is  null"
                End If
            End If
            selectedPick = ddlgvPick.Text

            ddlgvDrop = gvStudTPT.HeaderRow.FindControl("ddlgvDrop")
            If ddlgvDrop.Text <> "ALL" And ddlgvDrop.Text.Trim <> "" Then
                If ddlgvDrop.Text = "YES" Then
                    strFilter += " and stu_dropoff_trp_id is not null"
                Else
                    strFilter += " and stu_dropoff_trp_id is  null"
                End If
            End If
            selectedDrop = ddlgvDrop.Text

            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If
            'Else
            'str_query += " and stu_pickup_trp_id is not null and stu_dropoff_trp_id is not null"

        End If

        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudTPT.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            Try
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0)("bPICK") = False
                ds.Tables(0).Rows(0)("bDROP") = False

                gvStudTPT.DataSource = ds.Tables(0)
                Try
                    gvStudTPT.DataBind()
                Catch ex As Exception
                End Try
                ViewState("norow") = "yes"
                Dim columnCount As Integer = gvStudTPT.Rows(0).Cells.Count
                gvStudTPT.Rows(0).Cells.Clear()
                gvStudTPT.Rows(0).Cells.Add(New TableCell)
                gvStudTPT.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Catch ex As Exception
            End Try
        Else
            gvStudTPT.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        If gvStudTPT.Rows.Count > 0 Then
                ddlgvDrop = gvStudTPT.HeaderRow.FindControl("ddlgvDrop")
            ddlgvPick = gvStudTPT.HeaderRow.FindControl("ddlgvPick")



 
            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = selectedGrade

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtSection")
            txtSearch.Text = selectedSection

            If selectedPick <> "" Then
                ddlgvPick.Text = selectedPick
            End If

            If selectedDrop <> "" Then
                ddlgvDrop.Text = selectedDrop
            End If

        End If
        set_Menu_Img()


    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Function GetStatus(ByVal stu_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT ISNULL(STD_TYPE,'NA'),STD_FROMDATE,STD_TODATE FROM STUDENT_DISCONTINUE_S WHERE STD_STU_ID=" + stu_id _
                                & " AND STD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND Convert(Datetime,'" + Format(Now.Date, "yyyy-MM-dd") + "')<= " _
                                & " CONVERT(DATETIME,STD_TODATE) AND STD_bAPPROVE='TRUE' AND STD_TYPE<>'P' "

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Dim type As String
        Dim fromDate As DateTime
        Dim toDate As DateTime
        While reader.Read
            type = reader.GetString(0)
            If type <> "NA" Then
                fromDate = reader.GetDateTime(1)
                toDate = reader.GetDateTime(2)
            End If
        End While
        reader.Close()

        If type = "NA" Then
            Return "Active"
        ElseIf type = "T" Then
            Return "Transport temporarily discontinued from " + Format(fromDate, "dd/MMM/yyyy") + " to " + Format(toDate, "dd/MMM/yyyy")
        ElseIf type = "S" Then
            Return "Transport suspended  from " + Format(fromDate, "dd/MMM/yyyy") + " to " + Format(toDate, "dd/MMM/yyyy")
        Else
            Return "Active"
        End If


    End Function

    Sub CallReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptStudTransportInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub CallReqReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptTransportReqInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function


    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

#End Region

   
    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudTPT.PageIndexChanging
        Try
            gvStudTPT.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudTPT.RowCommand
        Try
            If e.CommandName = "view" Or e.CommandName = "edititem" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStudTPT.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblStuId As Label = selectedRow.FindControl("lblStuId")
                Dim lblStu_No As Label = selectedRow.FindControl("lblStu_No")
                Dim lblStu_Name As Label = selectedRow.FindControl("lblStu_Name")
                Dim lblGrade As Label = selectedRow.FindControl("lblGrade")
                Dim lblSection As Label = selectedRow.FindControl("lblSection")
                Dim imgPick As Image = selectedRow.FindControl("imgPick")
                Dim imgDrop As Image = selectedRow.FindControl("imgDrop")
                Dim url As String
                If e.CommandName = "view" Then
                    url = String.Format("~\Transport\studTPTEdit_M.aspx?MainMnu_code={0}&datamode={1}&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&shfid=" + Encr_decrData.Encrypt(ddlShift.SelectedValue.ToString) + "&sen=" + Encr_decrData.Encrypt(lblStu_No.Text) + "&name=" + Encr_decrData.Encrypt(lblStu_Name.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                    Response.Redirect(url)
                Else
                    If imgPick.ImageUrl.ToLower = "~/images/cross.gif" And imgDrop.ImageUrl.ToLower = "~/images/cross.gif" Then
                        CallReqReport(lblStuId.Text)
                    Else
                        CallReport(lblStuId.Text)
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

  
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStudTPT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudTPT.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStuId As Label
            Dim lblStatus As Label

            lblStuId = e.Row.FindControl("lblStuId")
            lblStatus = e.Row.FindControl("lblStatus")
            If lblStuId.Text <> "" Then
                lblStatus.Text = GetStatus(lblStuId.Text)
            End If

        End If
    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        Try
            Session("CLM") = ddlClm.SelectedItem.Value
            Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(Session("sBsuid"), Session("CLM"))
                If readerAcademic_Cutoff.HasRows = True Then
                    While readerAcademic_Cutoff.Read
                        Session("Current_ACY_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))
                        Session("Current_ACD_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ID"))
                    End While
                End If
            End Using
            ' PopulateAcademicYear()
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid"))

            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
