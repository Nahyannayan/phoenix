<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptLocation_M.aspx.vb" Inherits="Transport_tptLocation_M" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Location Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">
        <tr>
            <td  >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    HeaderText="You must enter a value in the following fields:" 
                    ValidationGroup="groupM1"  Width="100%" />
                
            </td>
        </tr>
        <tr>
         <td align="center" valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                style="text-align: center"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <table align="center" width="100%">
                                       
                         <tr>
                        <td align="left" width="20%">
                           <span class="field-label">Location Code</span></td>
                       
                        <td align="left" width="30%">
                               <asp:TextBox ID="txtCode" runat="server" TabIndex="2" MaxLength="3"></asp:TextBox>
                      </td>          
                    
                        <td align="left" width="20%">
                           <span class="field-label">Location Name</span></td>
                        
                        <td align="left" width="30%">
                               <asp:TextBox ID="txtLocation" runat="server" TabIndex="2"></asp:TextBox>
                      </td>          
                    </tr>
                                       
                     </table>
               </td>
        </tr>
       
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
        </tr>
        <tr>
            <td valign="bottom">
               <asp:HiddenField ID="hfLOC_ID" runat="server" />
               <asp:RequiredFieldValidator ID="rfLocation" runat="server" ErrorMessage="Please enter the field Location Name" ControlToValidate="txtLocation" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>      
          <asp:RequiredFieldValidator ID="rfCode" runat="server" ErrorMessage="Please enter the field Location Code" ControlToValidate="txtCode" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
          </td>
          
        </tr>
    </table>

            </div>
        </div>
    </div>
</asp:Content>

