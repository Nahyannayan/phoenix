Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
Imports System.Collections
Partial Class Transport_tptStudParent_Consent
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100217" And ViewState("MainMnu_code") <> "T100217") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    If ViewState("MainMnu_code") <> "T100217" Then
                        rdBoth.Visible = False
                    End If
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    BindAcademicYear()
                    BindBusNo()
                    tbl_Grd.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Protected Sub btnSave2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Stu_ID As String = String.Empty
        Dim Consent As String = String.Empty
        Dim comment As String = String.Empty

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Try
            For i As Integer = 0 To gv_BusList.Rows.Count - 1
                Dim row As GridViewRow = gv_BusList.Rows(i)
                Stu_ID = DirectCast(row.FindControl("lblStuId"), Label).Text
                Consent = DirectCast(row.FindControl("ddlConsent"), DropDownList).SelectedItem.Value
                comment = DirectCast(row.FindControl("txtComments"), TextBox).Text

                Dim str_query As String = "exec TRANSPORT.saveParent_Consent " _
                                    & Stu_ID + "," _
                                    & "'" + Consent + "'," _
                                    & "'" + Session("sUsr_name") + "'," _
                                    & "'" + comment + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next
            lblError.Text = "Data Successfully Saved!!!"





        Catch ex As Exception
            lblError.Text = "Error. Please contact admin!!!"
        Finally

        End Try
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        lblError.Text = ""

        gridbind()
    End Sub


#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub gridbind()
        Dim lstrConsent As String

        If rdYes.Checked = True Then
            lstrConsent = "YES"
        ElseIf rdNo.Checked = True Then
            lstrConsent = "NO"
        Else
            lstrConsent = "ALL"
        End If
        Try
            tbl_Grd.Visible = True
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String = "exec TRANSPORT.BUSLIST_PARENT_CONSENT  '" _
                                                & ddlBusNo.SelectedValue.ToString + "','" _
                                                & ddlBsu.SelectedValue.ToString + "'," _
                                                & "'" + ddlAcademicYear.SelectedValue.ToString + "'," _
                                                & "'" + txtSTU_ID.Text.ToString + "'," _
                                                & "'" + txtSTU_NAME.Text.ToString + "','" + lstrConsent + "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim dv As DataView = ds.Tables(0).DefaultView
            gv_BusList.DataSource = dv
            gv_BusList.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_ID,ACY_DESCR FROM ACADEMICYEAR_M WHERE ACY_ID>=((SELECT MIN(ACD_ACY_ID) FROM ACADEMICYEAR_D WHERE ACD_BSU_ID=" + ddlBsu.SelectedValue.ToString _
                                 & " AND ACD_CURRENT=1)-1)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlAcademicYear.Items.Insert(0, li)

        If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACY_ID")) Is Nothing Then
            ddlAcademicYear.Items.FindByValue(Session("Current_ACY_ID")).Selected = True
        End If
        'Dim currentYear As Integer

        'str_query = "SELECT ACD_ACY_ID FROM ACADEMICYEAR_D AS A INNER JOIN BUSINESSUNIT_M AS B ON" _
        '           & " A.ACD_BSU_ID=B.BSU_ID AND A.ACD_CLM_ID=B.BSU_CLM_ID WHERE ACD_CURRENT=1 " _
        '           & " AND BSU_ID=" + ddlBsu.SelectedValue.ToString

        'currentYear = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        'ddlAcademicYear.Items.FindByValue(currentYear).Selected = True
    End Sub

    Sub BindBusNo()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BNO_ID,BNO_DESCR FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + ddlBsu.SelectedValue.ToString + "'" _
                                 & " ORDER BY BNO_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlBusNo.Items.Insert(0, li)

    End Sub




#End Region

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        Try
            BindAcademicYear()
            BindBusNo()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gv_BusList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gv_BusList.PageIndexChanging
        gv_BusList.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gv_BusList_rowdatabound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Dim mytable As New DataTable()
        Dim CNS_CODEcolumn As New DataColumn("CNS_CODE")
        Dim CNS_DESCRcolumn As New DataColumn("CNS_DESCR")

        mytable.Columns.Add(CNS_CODEcolumn)
        mytable.Columns.Add(CNS_DESCRcolumn)

        Dim ds As New DataSet()
        ds = callConsent()


        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Temp_COnsent As String = DirectCast(e.Row.FindControl("lblTemp_Consent"), Label).Text
            Dim ddl As DropDownList = DirectCast(e.Row.FindControl("ddlConsent"), DropDownList)
            ' ddl = FillEmirateDetails(ddl)
            Dim rows As DataRow() = ds.Tables(0).Select

            For Each row As DataRow In rows
                Dim newrow As DataRow = mytable.NewRow()
                newrow("CNS_CODE") = row("CONSENT_CODE")
                newrow("CNS_DESCR") = row("CONSENT_CODE")

                mytable.Rows.Add(newrow)
            Next

            ddl.DataSource = mytable
            ddl.DataTextField = "CNS_CODE"
            ddl.DataValueField = "CNS_DESCR"
            ddl.DataBind()

            ddl.ClearSelection()
            ddl.Items.FindByValue(Temp_COnsent).Selected = True
        End If
    End Sub
    Public Function callConsent() As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_Sql As String = "SELECT 'NO' as CONSENT_CODE UNION SELECT 'YES' as CONSENT_CODE"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Return ds

    End Function
    Protected Sub gv_BusList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_BusList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("popStatusHistory"), AjaxControlToolkit.PopupControlExtender)

            Dim behaviorID As String = "pce_" & Convert.ToString(e.Row.RowIndex)
            pce.BehaviorID = behaviorID

            Dim img As Image = DirectCast(e.Row.FindControl("imgLog"), Image)
            'pce.TargetControlID = img.ClientID
            pce.DynamicControlID = "pnlPopup" 'img.ClientID ' "pce_" & behaviorID 'pnlPopup.ClientID
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)
            pce.Enabled = True
            img.Attributes.Add("onmouseover", OnMouseOverScript)
            img.Attributes.Add("onmouseout", OnMouseOutScript)

        End If

    End Sub
    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Try


            'Return "hollo world"
            Dim constr As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim query As String = " SELECT CONSENT,REPLACE(CONVERT(VARCHAR(20),LOG_DATE,106),' ','/') + RIGHT(CONVERT(VARCHAR(20),LOG_DATE,100),8) LOG_DATE,LOG_USR,STU_Parent_Consent_remark FROM TRANSPORT.PAR_CONSENT_LOG WHERE STU_ID= " & contextKey & " ORDER BY id desc "  '& ",'" & HttpContext.Current.Session("sBSUID") & "'"
            Dim b As New StringBuilder()

            Dim da As New SqlDataAdapter(query, constr)
            Dim Ttable As New DataTable()
            da.Fill(Ttable)

            b.Append("<table style='background-color:#f3f3f3; border:3px solid #336699; ")
            b.Append("width:550px;'>")

            b.Append("<tr><td colspan='4' style='background-color:#336699; color:white;'>")
            b.Append("<b>Status History</b>")
            b.Append("</td></tr>")
            b.Append("<tr style='background-color:#f3f3f3;'><td><b>Status</b></td>")
            b.Append("<td><b>Updated On</b></td>")
            b.Append("<td><b>Updated By</b></td>")
            b.Append("<td><b>Comments</b></td></tr>")
            If Ttable.Rows.Count > 0 Then

                For Each drow As DataRow In Ttable.Rows


                    b.Append("<tr>")
                    b.Append("<td style='align:left'>" & drow("CONSENT").ToString() & "</td>")
                    b.Append("<td style='align:left'>" & drow("LOG_DATE").ToString() & "</td>")
                    b.Append("<td style='align:left'>" & drow("LOG_USR").ToString() & "</td>")
                    b.Append("<td style='align:left'>" & drow("STU_Parent_Consent_remark").ToString() & "</td>")
                    b.Append("</tr>")
                Next
            Else
                b.Append("<tr width='100%'>")
                b.Append("<td colspan='4'>No status added </td>")
                b.Append("</tr>")
            End If
            b.Append("</table>")
            Return b.ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
End Class
