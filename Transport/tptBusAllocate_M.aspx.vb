Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptBusAllocate_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000461") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    BindunallocatedBus()
                    BindallocatedBus()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlbsu.DataSource = ds
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataBind()
    End Sub
    Sub BindunallocatedBus()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select distinct VEH_ID,VEH_REGNO,VEH_ALTO_BSU_ID,VEH_OWN_BSU_ID from dbo.VEHICLE_M where isnull(VEH_ALTO_BSU_ID,'999998')='999998'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstunalloctedBuslist.DataSource = ds
        lstunalloctedBuslist.DataTextField = "VEH_REGNO"
        lstunalloctedBuslist.DataValueField = "VEH_ID"
        lstunalloctedBuslist.DataBind()
    End Sub
    Sub BindallocatedBus()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select distinct VEH_ID,VEH_REGNO,VEH_ALTO_BSU_ID,VEH_OWN_BSU_ID from dbo.VEHICLE_M where isnull(VEH_ALTO_BSU_ID,'999998')='" & ddlbsu.SelectedValue & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstalloctedBuslist.DataSource = ds
        lstalloctedBuslist.DataTextField = "VEH_REGNO"
        lstalloctedBuslist.DataValueField = "VEH_ID"
        lstalloctedBuslist.DataBind()
    End Sub
    Protected Sub ddlbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlbsu.SelectedIndexChanged
        BindunallocatedBus()
        BindallocatedBus()
    End Sub

    Protected Sub btnallocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnallocate.Click
        Try
            Dim li As ListItem
            Dim lstTemp As New ListBox
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            For Each li In lstunalloctedBuslist.Items
                If li.Selected = True Then
                    lstalloctedBuslist.Items.Add(li)
                    lstTemp.Items.Add(li)
                    Dim param(5) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@VEH_ID", li.Value)
                    param(1) = New SqlClient.SqlParameter("@VEH_ALTO_BSU_ID", ddlbsu.SelectedValue)
                    param(2) = New SqlClient.SqlParameter("@STATUS", "Add")
                    param(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                    param(3).Direction = ParameterDirection.Output
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "TRANSPORT.BUSALLOCATION_VEHICLE_M", param)
                    Dim msg As String = param(3).Value.ToString
                    lblError.Text = "Updated Successfully"
                End If
            Next
            For Each li In lstTemp.Items
                lstunalloctedBuslist.Items.Remove(li)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnUnallocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnallocate.Click
        Try
            ViewState("vehilceno") = ""
            Dim li As ListItem
            Dim lstTemp As New ListBox
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            For Each li In lstalloctedBuslist.Items
                If li.Selected = True Then
                    Dim param(5) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@VEH_ID", li.Value)
                    param(1) = New SqlClient.SqlParameter("@VEH_ALTO_BSU_ID", "999998")
                    param(2) = New SqlClient.SqlParameter("@STATUS", "Delete")
                    param(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                    param(3).Direction = ParameterDirection.Output
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "TRANSPORT.BUSALLOCATION_VEHICLE_M", param)
                    Dim msg As String = param(3).Value.ToString
                    If msg = 0 Then
                        ViewState("vehilceno") = li.Text + "," + ViewState("vehilceno")

                        lblError.Text = ViewState("vehilceno") + "These Vehicle is already allocated for a Trip,so it cannot be deallocated"
                    Else
                        lstunalloctedBuslist.Items.Add(li)
                        lstTemp.Items.Add(li)
                        ' lblError.Text = "Updated Successfully"
                    End If

                End If
            Next
            For Each li In lstTemp.Items
                lstalloctedBuslist.Items.Remove(li)
            Next
        Catch ex As Exception

        End Try
    End Sub
End Class
