Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Transport_tptDriveConAllocate_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000290") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    hfBsu.Value = Encr_decrData.Decrypt(Request.QueryString("bsu").Replace(" ", "+"))
                    hfEmp.Value = Encr_decrData.Decrypt(Request.QueryString("emp").Replace(" ", "+"))
                    BindEmpAllocated()
                    BindEmpNotAllocated()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindEmpAllocated()
        lstEmpAllocated.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT a.guid as guid,isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') + ' ('+ EMPNO + ')'  as emp_name," _
                                 & " emp_id FROM employee_m as a inner join oasis_transport.TRANSPORT.DRIVERCON_ALLOC_S as b on " _
                                 & " a.emp_id=b.trc_emp_id where TRC_BSU_ALTO_ID='" + hfBsu.Value + "'" _
                                 & " and trc_emp_type='" + hfEmp.Value + "' ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstEmpAllocated.DataSource = ds
        lstEmpAllocated.DataTextField = "emp_name"
        lstEmpAllocated.DataValueField = "emp_id"
        lstEmpAllocated.DataBind()
    End Sub

    Sub BindEmpNotAllocated()
        lstEmpNotAllocated.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If hfEmp.Value.ToLower = "driver" Then
            str_query = "SELECT '('+BSU_SHORT_RESULT+') '+ isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') + ' ('+ EMPNO + ')' as emp_name," _
                        & "emp_id FROM employee_m  AS A INNER JOIN BUSINESSUNIT_M AS B ON A.EMP_BSU_ID=B.BSU_ID   INNER JOIN OASIS..EMPDESIGNATION_M DES ON EMP_DES_ID=DES_ID AND DES_FLAG='SD'  WHERE DES_GROUP_ID =308 and (EMP_BSU_ID='" + Session("sBsuid") + "' OR EMP_BSU_ID='" + hfBsu.Value + "')" _
                        & " and emp_id not in (select trc_emp_id from oasis_transport.TRANSPORT.DRIVERCON_ALLOC_S) ORDER BY BSU_SHORT_RESULT,EMP_FNAME,EMP_MNAME,EMP_LNAME"
        Else
            'str_query = "SELECT '('+BSU_SHORT_RESULT+') '+ isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
            '          & "emp_id FROM employee_m AS A INNER JOIN BUSINESSUNIT_M AS B ON A.EMP_BSU_ID=B.BSU_ID  WHERE emp_des_id=(select des_id from empdesignation_m where des_descr='Conductor' and des_flag='SD') and (EMP_BSU_ID='" + Session("sBsuid") + "' OR EMP_BSU_ID='" + hfBsu.Value + "')" _
            '          & " and emp_id not in (select trc_emp_id from oasis_transport.TRANSPORT.DRIVERCON_ALLOC_S) ORDER BY BSU_SHORT_RESULT,EMP_FNAME,EMP_MNAME,EMP_LNAME"
            str_query = "SELECT '('+BSU_SHORT_RESULT+') '+ isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
                      & "emp_id FROM employee_m AS A INNER JOIN BUSINESSUNIT_M AS B ON A.EMP_BSU_ID=B.BSU_ID  WHERE EMP_ECT_ID=2 AND EMP_DES_ID<>308 AND EMP_STATUS=1  and (EMP_BSU_ID='" + Session("sBsuid") + "' OR EMP_BSU_ID='" + hfBsu.Value + "')" _
                      & " and emp_id not in (select trc_emp_id from oasis_transport.TRANSPORT.DRIVERCON_ALLOC_S) ORDER BY BSU_SHORT_RESULT,EMP_FNAME,EMP_MNAME,EMP_LNAME"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstEmpNotAllocated.DataSource = ds
        lstEmpNotAllocated.DataTextField = "emp_name"
        lstEmpNotAllocated.DataValueField = "emp_id"
        lstEmpNotAllocated.DataBind()
    End Sub

    Sub AllocateEmployee()
        Dim li As ListItem
        Dim lstTemp As New ListBox
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        For Each li In lstEmpNotAllocated.Items
            If li.Selected = True Then
                lstEmpAllocated.Items.Add(li)
                lstTemp.Items.Add(li)
                str_query = "exec TRANSPORT.saveDRIVERCONALLOC " _
                          & "'" + Session("sbsuid") + "'," _
                          & "'" + hfBsu.Value + " '," _
                          & li.Value.ToString + "," _
                          & "'" + hfEmp.Value + "'," _
                          & "'ADD'"

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next
        For Each li In lstTemp.Items
            lstEmpNotAllocated.Items.Remove(li)
        Next
    End Sub

    Sub DeAllocateEmployee()
        Dim str As String
        Dim li As ListItem
        Dim lstTemp As New ListBox
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        For Each li In lstEmpAllocated.Items
            If li.Selected = True Then
                If IsEmployeeInTrip(li.Value) = False Then
                    lstEmpNotAllocated.Items.Add(li)
                    lstTemp.Items.Add(li)
                    str_query = "exec TRANSPORT.saveDRIVERCONALLOC " _
                              & "'" + Session("sbsuid") + "'," _
                              & "'" + hfBsu.Value + " '," _
                              & li.Value.ToString + "," _
                              & "'" + hfEmp.Value + "'," _
                              & "'DELETE'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                Else
                    If str <> "" Then
                        str += " , "
                    End If
                    str += li.Text
                End If
            End If
        Next
        For Each li In lstTemp.Items
            lstEmpAllocated.Items.Remove(li)
        Next

        If str <> "" Then
            If hfEmp.Value.ToLower = "driver" Then
                lblError.Text = "The following drivers " + str + " could not be deallocated as they are allocated to Buses"
            Else
                lblError.Text = "The following conductors " + str + " could not be deallocated as they are allocated to Buses"
            End If
        End If
    End Sub

    Function IsEmployeeInTrip(ByVal emp_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        If hfEmp.Value.ToLower = "driver" Then
            str_query = "SELECT COUNT(TRD_ID) FROM TRANSPORT.TRIPS_D WHERE TRD_TODATE IS NULL AND TRD_DRIVER_EMP_ID=" + emp_id
        Else
            str_query = "SELECT COUNT(TRD_ID) FROM TRANSPORT.TRIPS_D WHERE TRD_TODATE IS NULL AND TRD_CONDUCTOR_EMP_ID=" + emp_id
        End If

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

    Protected Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight.Click
        AllocateEmployee()
    End Sub

    Protected Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft.Click
        DeAllocateEmployee()
    End Sub
End Class
