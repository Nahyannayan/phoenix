<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptDriveConAllocate_M.aspx.vb" Inherits="Transport_tptDriveConAllocate_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }





    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>Driver/Conductor Allocation Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">Fields Marked with (<span class="text-danger">*</span>) are mandatory      
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <%-- <tr class="subheader_img">
                                    <td align="left" colspan="12" style="height: 16px" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
             Driver/Conductor Allocation Set Up&nbsp;</span></font></td>
                                </tr>--%>
                                <tr>
                                    <td align="center" colspan="18">

                                        <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                            <tr>
                                                <td align="center">
                                                    <asp:ListBox ID="lstEmpNotAllocated" runat="server" Height="290px" Width="305px" SelectionMode="Multiple" Rows="10"></asp:ListBox>
                                                </td>
                                                <td align="center">
                                                    <asp:Button ID="btnRight" runat="server" CssClass="button" Text=">>" /><br />
                                                    <br />
                                                    <asp:Button ID="btnLeft" runat="server" CssClass="button" Text="<<" /></td>

                                                <td align="center">
                                                    <asp:ListBox ID="lstEmpAllocated" runat="server" Height="290px" Width="305px" SelectionMode="Multiple" Rows="10"></asp:ListBox>
                                                </td>

                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                            <input id="lstValues" name="lstValues" runat="server" type="hidden" />
                            <asp:HiddenField ID="hfEmp" runat="server" />
                            <asp:HiddenField ID="hfBsu" runat="server" />


                        </td>
                    </tr>

                </table>


            </div>
        </div>
    </div>

</asp:Content>

