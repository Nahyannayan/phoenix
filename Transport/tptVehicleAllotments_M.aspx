<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tptVehicleAllotments_M.aspx.vb" Inherits="Transport_tptSeatCapacity_M" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <%-- <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script> --%>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <script language="javascript" type="text/javascript"> 

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left">
                        <table width="100%" id="Table3" border="0">
                            <tr>
                                <td align="left" class="title-bg">Vehicle Allotments</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center">
                        <table id="Table1" runat="server" align="center" border="0" cellpadding="0"
                            cellspacing="0" width="100%">
                            <tr>
                                <td colspan="3" align="center" class="matters">

                                    <table id="Table2" runat="server" align="center" border="0"
                                        cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Select Vehicle</span></td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlVehicle" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" width="20%"></td>
                                            <td align="left" width="30%"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Select Shift</span></td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" AppendDataBoundItems="True">
                                                </asp:DropDownList></td>
                                            <td align="left" width="20%"></td>
                                            <td align="left" width="30%">
                                                <asp:RadioButton ID="rdOnward" AutoPostBack="true" runat="server" Checked="True" GroupName="G1" Text="Onward" CssClass="field-label" />
                                                <asp:RadioButton ID="rdReturn" AutoPostBack="true" runat="server" GroupName="G1" Text="Return" CssClass="field-label" /></td>
                                        </tr>

                                        <tr>
                                            <td colspan="4" align="center">
                                                <asp:GridView ID="gvTPT" runat="server"
                                                    AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                    PageSize="20" AllowPaging="True" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Area">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblH1" runat="server" Text="Area"></asp:Label><br />
                                                                <asp:TextBox ID="txtTrip" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnTrip_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnTrip_Search_Click" />
                                                            </HeaderTemplate>

                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrip" runat="server" Text='<%# Bind("trp_descr") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Driver">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblDri" runat="server" Text="Driver"></asp:Label><br />
                                                                <asp:TextBox ID="txtDriver" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnDriver_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnDriver_Search_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDriver" runat="server" Text='<%# Bind("DRV_NAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Conductor">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblCon" runat="server" Text="Conductor"></asp:Label><br />
                                                                <asp:TextBox ID="txtConductor" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnConductor_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnConductor_Search_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblConductor" runat="server" Text='<%# Bind("CON_NAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="griditem" />
                                                    <HeaderStyle />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    <SelectedRowStyle />
                                                    <PagerStyle HorizontalAlign="Left" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                            runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3"
                            runat="server" type="hidden" value="=" />
                    </td>
                </tr>

            </table>


        </div>
    </form>
</body>
</html>
