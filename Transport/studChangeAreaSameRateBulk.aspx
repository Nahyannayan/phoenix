﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studChangeAreaSameRateBulk.aspx.vb" Inherits="Transport_studChangeAreaSameRateBulk" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">

        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Change Area With Same Rate
        </div>
        <div class="card-body">
            <div class="table-responsive">



                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <%--<tr style="font-size: 12pt;">
                        <td width="50%" align="left" class="title">CHANGE AREA WITH SAME RATE</td>
                    </tr>--%>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTPT" runat="server" align="center" border="0" cellpadding="4" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="10%"><span class="field-label">Curriculum</span></td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="10%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>



                                    <td align="left" width="10%"><span class="field-label">Grade</span></td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>


                                    <td align="left" width="10%"><span class="field-label">Section</span></td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>


                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left"><span class="field-label">Student Name</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                                    <td><span class="field-label">Pickup Area</span></td>

                                    <td>
                                        <asp:DropDownList ID="ddlArea" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Pickup Trip</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTrip" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Pickup Point</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPickup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="8"></td>
                                </tr>


                                <tr>
                                    <td align="left" colspan="8" class="title-bg-lite">Change Area To</td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Pickup Area</span></td>

                                    <td colspan="7">
                                        <asp:DropDownList ID="ddlAreaChange" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Pickup Trip</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPTripChange" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Pickup Point</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPickupChange" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Drop Off Trip</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlDTripChange" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Drop Off Point</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlDropOff" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>


                                <tr>
                                    <td align="center" colspan="8">
                                        <asp:Button ID="btnUpdate1" OnClick="btnUpdate_Click" runat="server" Text="Update" CssClass="button" TabIndex="4" ValidationGroup="groupM1" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="8">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>


                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Select
                                                        <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Start Date">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>

                                                        <asp:TextBox ID="txtDate" Text='<%# Bind("SSV_FROMDATE","{0:dd/MMM/yyyy}") %>' runat="server" CausesValidation="true"></asp:TextBox>

                                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />

                                                        <asp:Label ID="lblErr" runat="server" Text="*" CssClass="error" Visible="false"></asp:Label>


                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                            Format="dd/MMM/yyyy" PopupButtonID="imgDate" PopupPosition="BottomLeft" TargetControlID="txtDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Start Date" Visible="false">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblDate" Text='<%# Bind("SSV_FROMDATE","{0:dd/MMM/yyyy}") %>' runat="server" CausesValidation="true"></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="8">
                                        <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" CssClass="button" TabIndex="4" ValidationGroup="groupM1" /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                        </td>
                    </tr>


                </table>

            </div>
        </div>
    </div>
</asp:Content>

