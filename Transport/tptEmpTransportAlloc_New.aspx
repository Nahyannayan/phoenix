<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptEmpTransportAlloc_New.aspx.vb" Inherits="Transport_tptEmpTransportAlloc_New" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 <script type="text/javascript" language ="javascript" >
  function getDate() 
   {     
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx?dt="+document.getElementById('<%=txtStDate.ClientID %>').value,"", sFeatures)
            if (result=='' || result==undefined)
            {
            return false;
            }
           document.getElementById('<%=txtStDate.ClientID %>').value=result; 
           return true;
    }      
    
     function getDate1() 
   {     
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx?dt="+document.getElementById('<%=txtStDate1.ClientID %>').value,"", sFeatures)
            if (result=='' || result==undefined)
            {
            return false;
            }
           document.getElementById('<%=txtStDate1.ClientID %>').value=result; 
           return true;
    }    
    
   </script>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Employee Transport Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <table align="center" width="100%">
        
        <tr>
            <td align="left" colspan="4" class="title-bg">
                Employee Details</td>
        </tr>
        <tr>
            <td align="left" width="20%">
                <span class="field-label">StaffNumber</span></td>
            
            <td align="left" width="30%">
                <asp:Label id="lblStaffNo" runat="server" cssclass="field-value"></asp:Label></td>
            <td align="left" width="20%">
                <span class="field-label">
                Department</span></td>
           
            <td align="left" width="30%">
                <asp:Label id="lblDepartment" runat="server" cssclass="field-value"></asp:Label></td>
        </tr>
                <tr>
            <td align="left">
                <span class="field-label">Staff Name</span></td>
            
            <td align="left">
                <asp:Label id="lblStaffName" runat="server" cssclass="field-value"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Category</span></td>
            
            <td align="left">
                <asp:Label id="lblCategory" runat="server" cssclass="field-value"></asp:Label></td>
            <td align="left">
                <span class="field-label">Designation</span></td>
           
            <td align="left">
                <asp:Label id="lblDesignation" runat="server" cssclass="field-value"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Religion</span></td>
            
            <td align="left">
                <asp:Label id="lblReligion" runat="server" cssclass="field-value"></asp:Label></td>
            <td align="left">
               <span class="field-label"> Nationality</span></td>
            
            <td align="left">
                <asp:Label id="lblnationality" runat="server" cssclass="field-value"></asp:Label></td>
        </tr>

         <tr>
            <td align="left" colspan="4" class="title-bg">
                Transport Allocation Onwards  <asp:CheckBox id="chkReturn" runat="server" Text="Return" OnCheckedChanged="chkReturn_CheckedChanged" AutoPostBack="True">
                </asp:CheckBox></td>
        </tr>
        
         <tr>
            <td align="left" >
                <span class="field-label">Area</span>
                <asp:Label id="Label1" runat="server" cssclass="text-danger" Text="*"></asp:Label></td>
            
            <td align="left">
                <asp:DropDownList id="ddlArea" runat="server" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged"
                   AutoPostBack="True">
                </asp:DropDownList></td>
            <td align="left">
                <span class="field-label">Trip</span>
                <asp:Label id="Label3" runat="server" Text="*" CssClass="text-danger"></asp:Label></td>
            
            <td align="left">
                <asp:DropDownList id="ddlTrip" runat="server" OnSelectedIndexChanged="ddlTrip_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Pick up Point</span>
                <asp:Label id="Label2" runat="server" cssclass="text-danger" Text="*" ></asp:Label></td>
           
            <td align="left">
                <asp:DropDownList id="ddlpickupPoint" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlpickupPoint_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <span class="field-label">Start Date</span></td>
            
            <td align="left">
                <asp:TextBox id="txtStDate" runat="server" AutoPostBack="True" ></asp:TextBox>
                <asp:ImageButton id="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtStDate">
                            </ajaxToolkit:CalendarExtender>
        </tr>
         <tr id="TitlePanel" runat="server">
            <td align="left" colspan="4" class="title-bg">
                <asp:Label id="lblReturn" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
    <asp:Panel id="Panel1" runat="server" Width="100%">
    <table align="center" width="100%">
      
         <tr>
            <td align="left" width="20%">
                <span class="field-label">Area</span>
                <asp:Label id="Label11" runat="server" cssclass="text-danger"  Text="*"></asp:Label></td>
            
            <td align="left" width="30%">
                <asp:DropDownList id="ddlArea1" runat="server" OnSelectedIndexChanged="ddlArea1_SelectedIndexChanged"
                 AutoPostBack="True">
                </asp:DropDownList></td>
            <td align="left" width="20%">
                <span class="field-label">Trip</span>
                <asp:Label id="Label31" runat="server" Text="*" CssClass="text-danger"></asp:Label></td>
            
            <td align="left" width="30%">
                <asp:DropDownList id="ddlTrip1" runat="server" OnSelectedIndexChanged="ddlTrip1_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Drop Off Point</span>
                <asp:Label id="Label21" runat="server" cssclass="text-danger" Text="*" Width="7px"></asp:Label></td>
            
            <td align="left">
                <asp:DropDownList id="ddlpickupPoint1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlpickupPoint1_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <span class="field-label">Start Date</span></td>
           
            <td align="left">
                <asp:TextBox id="txtStDate1" runat="server" AutoPostBack="True">
                </asp:TextBox>
                <asp:ImageButton id="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar1" TargetControlID="txtStDate1">
                            </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
       
        </table>
                </asp:Panel>
                </td>
        </tr>
         <tr>
            <td align="center" colspan="4">
                <asp:Button id="btnSave" runat="server" CssClass="button" tabIndex="26" Text="Save"
                  OnClick="btnSave_Click" />
                <asp:Button id="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    tabIndex="30" Text="Delete" OnClick="btnDelete_Click" />
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    tabIndex="30" Text="Cancel" OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>
    <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label>
    &nbsp;&nbsp;
    <asp:HiddenField id="HIDAllocOwd" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="HidAllocRtn" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="HFDID" runat="server">
    </asp:HiddenField>

                </div>
            </div>
        </div>

</asp:Content>

