<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTransportDiscontinueApprove_M.aspx.vb" Inherits="Transport_studTransportDiscontinueApprove_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>  Student Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">

    <tr>
    <td align="left"  width="100%">
    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
    SkinID="error"></asp:Label>
       </td>
    </tr>
    <tr>
        <td align="left" width="100%">
        
        <asp:ValidationSummary id="valSum" runat="server" CssClass="error" EnableViewState="False"
            HeaderText="You must enter a value in the following fields:" Height="100px"
            ValidationGroup="groupM1"  width="100%">
        </asp:ValidationSummary>&nbsp; &nbsp;
    </td>
    </tr>
    <tr>
    <td align="center" width="100%">
    Fields Marked with (<span class="text-danger">*</span>) are mandatory
    </td>
    </tr>

    <tr>
    <td  width="100%">
    <table align="center"  width="100%" id="tblTpt" runat="server">
      
        <tr>
        <td align="left" width="20%">
            <span class="field-label">Student ID</span></td>
        
        <td align="left" width="30%"><asp:Label ID="lblStuNo" runat="server" CssClass="field-value"></asp:Label></td>
        <td align="left" width="20%">
            <span class="field-label">Name</span></td>
        
        <td width="30%"><asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Grade</span></td>
            
            <td align="left">
                <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
            <td align="left">
               <span class="field-label"> Section</span></td>
           
            <td align="left">
                <asp:Label ID="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>
 <tr>
            <td align="left">
                <span class="field-label">Pickup Area</span></td>
           
            <td align="left">
                <asp:Label ID="lblPArea" runat="server" CssClass="field-value"></asp:Label></td>
            <td align="left">
                <span class="field-label">Pickup Point</span></td>
            
            <td align="left">
                <asp:Label ID="lblPickup" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Pickup Trip</span></td>
           
            <td align="left">
                <asp:Label ID="lblPtrip" runat="server" CssClass="field-value"></asp:Label></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
               <span class="field-label"> Drop Off Area</span></td>
            
            <td align="left">
                <asp:Label ID="lblDArea" runat="server" CssClass="field-value"></asp:Label></td>
            <td align="left">
                <span class="field-label">Drop Off Point</span></td>
           
            <td align="left">
                <asp:Label ID="lblDropoff" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Drop Off Trip</span></td>
            
            <td align="left">
                <asp:Label ID="lblDTrip" runat="server" CssClass="field-value"></asp:Label></td>
            <td></td>
            <td></td>
        </tr>



        <tr>
        <td align="left" colspan="4" class="title-bg">        
            Discontinuation Details</td>
        </tr>

        <tr>

        <td align="left">
           <span class="field-label"> Discontinuation Type</span></td>

        <td align="left">
            <asp:Label ID="lblType" runat="server" CssClass="field-value"></asp:Label></td>
           <td align="left">
           <span class="field-label"> Discontinuation Reason</span></td>
            <td align="left">
            <asp:Label ID="lblreason" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>
        <tr>
        <td align="left">
            <span class="field-label">From</span></td>
        <td align="left">
            <asp:Label ID="lblFrom" runat="server" CssClass="field-value"></asp:Label></td>


        <td align="left" >
            <span class="field-label">To</span></td>

        <td align="left">
           <asp:Label ID="lblTo" runat="server" CssClass="field-value"></asp:Label></td>

        </tr>
        
         <tr>
        <td align="left">
            <span class="field-label">Collect discontinuation charges ?</span></td>
        
        <td align="left" colspan="3" >
       <asp:RadioButton ID="optDisChargesYes" runat="server"  GroupName="optControl" Text="Yes"  />
       <asp:RadioButton ID="optDisChargesNo" runat="server" GroupName="optControl" Text="No" Checked="True" />
       
       </td>
        </tr>
        
        
        
        
        <tr>
        <td align="left">
            <span class="field-label">Request Remarks</span></td>
        
        <td align="left">
        <asp:TextBox id="txtReqRemarks" runat="server" TextMode="MultiLine" ReadOnly="True"></asp:TextBox></td>
        <td></td>
        <td></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Approval Date</span></td>
            
            <td align="left">
                <asp:TextBox id="txtFrom" runat="server" AutoCompleteType="Disabled" Width="112px">
                </asp:TextBox><asp:ImageButton id="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                    tabIndex="4"></asp:ImageButton><asp:RegularExpressionValidator id="revDocDate" runat="server"
                        ControlToValidate="txtFrom" CssClass="error" Display="Dynamic" EnableViewState="False"
                        ErrorMessage="Enter From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                        ForeColor="" style="position: relative" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>
            <td align="left">
                <span class="field-label">Remarks</td>
            
            <td align="left">
                <asp:TextBox id="txtRemarks" runat="server" TextMode="MultiLine" Height="67px" SkinID="MultiText">
                </asp:TextBox></td>
        </tr>
        </table>
        </td>
        </tr>



<tr>
<td align="center" colspan="4">
<asp:Button id="btnApprove" runat="server" CssClass="button" Text="Accept" />
<asp:Button id="btnReject" runat="server" CssClass="button" Text="Reject" />
<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  CausesValidation="False"/>
</td>
</tr>
<tr>
<td>
<asp:HiddenField ID="hfSTU_ID" runat="server" />
<asp:HiddenField ID="hfACD_ID" runat="server" /><asp:RequiredFieldValidator id="rfrEMARKS"
    runat="server" ControlToValidate="txtRemarks" Display="None" ErrorMessage="Please enter the Remarks"
    ValidationGroup="groupM1"></asp:RequiredFieldValidator>
<asp:HiddenField ID="hfType" runat="server" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp;
    <asp:RequiredFieldValidator id="rfFrom" runat="server" ControlToValidate="txtFrom"
        Display="None" ErrorMessage="Please enter the approval date " ValidationGroup="groupM1">
    </asp:RequiredFieldValidator>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="hfSHF_ID" runat="server" EnableViewState="False" />
<asp:HiddenField ID="hfSTD_ID" runat="server" EnableViewState="False" />
</td>
</tr>
</table>

                </div>
            </div>
    </div>

</asp:Content>

