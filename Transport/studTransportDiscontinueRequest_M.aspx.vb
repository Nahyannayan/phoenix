Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studTransportDiscontinueRequest_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000200") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    txtRequest.Text = Format(Now.Date, "dd/MMM/yyyy")
                    hfTCH_ID.Value = "0"
                    LOAD_AUDIT_HEADERS()
                    LOAD_SERVICE_HISTORY()
                    btnSave.Visible = True
                    'GetData()
                    BinddiscontinuReason()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
#Region "Private Methods"

    Sub LOAD_AUDIT_HEADERS()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC TRANSPORT.GET_STUDENT_TRANSPORT_AUDIT_HEADER_DATA @STU_ID = @STUID, @DATATYPE = @DATATYPE")

            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
            pParms(0).Value = hfSTU_ID.Value
            pParms(1) = New SqlClient.SqlParameter("@DATATYPE", SqlDbType.VarChar, 10)
            pParms(1).Value = "STUDENT"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                dtlStudentInfo.HeaderTemplate = New HeaderTemplate(ds.Tables(0).Rows(0)("HEADING").ToString)
                dtlStudentInfo.HeaderStyle.CssClass = "title-bg"
                dtlStudentInfo.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                dtlStudentInfo.DataSource = ds
                dtlStudentInfo.DataBind()
            End If
            'ReDim Preserve pParms(3)
            pParms(1) = New SqlClient.SqlParameter("@DATATYPE", SqlDbType.VarChar, 10)
            pParms(1).Value = "SERVICE"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                dtlStudentServiceInfo.HeaderTemplate = New HeaderTemplate(ds.Tables(0).Rows(0)("HEADING").ToString)
                dtlStudentServiceInfo.HeaderStyle.CssClass = "title-bg"
                dtlStudentServiceInfo.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                dtlStudentServiceInfo.DataSource = ds
                dtlStudentServiceInfo.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub LOAD_SERVICE_HISTORY()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC TRANSPORT.GET_TRANSPORT_SERVICE_HISTORY @STU_ID = @STUID")

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
            pParms(0).Value = hfSTU_ID.Value

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvServiceHistory.DataSource = ds
                gvServiceHistory.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    'Sub GetData()

    '    Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
    '    Dim trdPickup As Integer
    '    Dim trdDropoff As Integer

    '    Dim str_query As String = "SELECT  distinct isnull(D.SBL_DESCRIPTION,''),isnull(C.PNT_DESCRIPTION,'')," _
    '                             & " isnull(F.SBL_DESCRIPTION,''),isnull(G.PNT_DESCRIPTION,'')," _
    '                             & " ISNULL(I.BNO_DESCR,'')+'-'+ISNULL(I.TRP_DESCR,'') AS PICKUPTRIP, " _
    '                             & " ISNULL(J.BNO_DESCR,'')+'-'+ISNULL(J.TRP_DESCR,'') AS DROPOFFTRIP , " _
    '                             & " SSV_FROMDATE " _
    '                             & " FROM  STUDENT_M AS A " _
    '                             & " INNER JOIN OASIS..STUDENT_SERVICES_D AS B ON A.STU_ID=B.SSV_STU_ID AND SSV_TODATE IS NULL AND SSV_SVC_ID=1" _
    '                             & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
    '                             & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
    '                             & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
    '                             & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
    '                             & " LEFT OUTER JOIN TRANSPORT.VV_TRIPS_D AS I ON A.STU_PICKUP_TRP_ID=I.TRP_ID" _
    '                             & " LEFT OUTER JOIN TRANSPORT.VV_TRIPS_D AS J ON A.STU_DROPOFF_TRP_ID=J.TRP_ID" _
    '                             & " WHERE STU_ID =" + hfSTU_ID.Value.ToString




    '    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

    '    While reader.Read
    '        With reader
    '            lblPArea.Text = .GetString(0)
    '            lblPickup.Text = .GetString(1)
    '            lblDArea.Text = .GetString(2)
    '            lblDropoff.Text = .GetString(3)
    '            lblPtrip.Text = .GetString(4)
    '            lblDTrip.Text = .GetString(5)
    '            hfFromDate.Value = Format(.GetDateTime(6), "dd/MMM/yyyy")
    '        End With
    '    End While
    '    reader.Close()

    '    str_query = "SELECT ISNULL(STD_FROMDATE,'01/Jan/1900') FROM STUDENT_DISCONTINUE_S WHERE STD_TYPE='P' " _
    '               & " AND STD_STU_ID=" + hfSTU_ID.Value
    '    reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '    Dim disDate As DateTime
    '    While reader.Read
    '        disDate = reader.GetDateTime(0)
    '    End While
    '    reader.Close()


    'End Sub


    'Sub SaveData()
    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    '    Dim ToDt As String

    '    If ddlType.SelectedValue = "P" Then
    '        ToDt = "NULL"
    '    Else
    '        ToDt = "'" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
    '    End If
    '    Dim str_query As String = "exec TRANSPORT.saveTRANSPORTDISCONTINUEREQ " _
    '                            & hfSTU_ID.Value + "," _
    '                            & hfACD_ID.Value + "," _
    '                            & "'" + ddlType.SelectedValue + "'," _
    '                            & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'," _
    '                            & ToDt + "," _
    '                            & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
    '                            & "'" + Session("sUsr_name") + "'," _
    '                            & "'" + Format(Date.Parse(txtRequest.Text), "yyyy-MM-dd") + "'"
    '    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    'End Sub
    Private Function SAVE_TRANSPORT_DISCONTINUE_REQ() As Integer
        SAVE_TRANSPORT_DISCONTINUE_REQ = 1
        Dim TODATE As New Object
        If IsDate(txtTo.Text) Then
            TODATE = CDate(txtTo.Text)
        Else
            TODATE = DBNull.Value
        End If
        If ddlType.SelectedValue = "P" Then
            TODATE = DBNull.Value
        End If
        Dim conn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        conn.Open()
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction()
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("TRANSPORT.SAVETRANSPORTDISCONTINUEREQ", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@STD_STU_ID", SqlDbType.Int)
            sqlp1.Value = hfSTU_ID.Value
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@STD_ACD_ID", SqlDbType.Int)
            sqlp2.Value = hfACD_ID.Value
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@STD_TYPE", SqlDbType.VarChar, 10)
            sqlp3.Value = ddlType.SelectedValue
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@STD_FROMDATE", SqlDbType.DateTime)
            sqlp4.Value = CDate(txtFrom.Text)
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@STD_TODATE", SqlDbType.DateTime)
            sqlp5.Value = TODATE
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@STD_REQREMARKS", SqlDbType.VarChar, 255)
            sqlp6.Value = txtRemarks.Text.Replace("'", "''")
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@STD_REQUSR", SqlDbType.VarChar, 100)
            sqlp7.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlp7)

            Dim sqlp8 As New SqlParameter("@STD_REQDATE", SqlDbType.DateTime)
            sqlp8.Value = CDate(txtRequest.Text)
            cmd.Parameters.Add(sqlp8)

            Dim sqlp9 As New SqlParameter("@STD_REASON", SqlDbType.Int)
            sqlp9.Value = ddldiscontResons.SelectedValue
            cmd.Parameters.Add(sqlp9)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            SAVE_TRANSPORT_DISCONTINUE_REQ = retSValParam.Value
            If retSValParam.Value = 0 Then
                trans.Commit()
                'lblError.Text = "Request saved successfully"
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
            Else
                trans.Rollback()
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(SAVE_TRANSPORT_DISCONTINUE_REQ), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            trans.Rollback()
            UtilityObj.Errorlog("Error in " & System.Reflection.MethodBase.GetCurrentMethod().Name & ", " & ex.Message, "PHOENIX")
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification("Error : " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ddlType.SelectedItem.Text = "--" Then
                'lblError.Text = "Please select the discontinuation type"
                usrMessageBar.ShowNotification("Please select the discontinuation type", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If ddldiscontResons.SelectedItem.Text = "" Then
                usrMessageBar.ShowNotification("Please select the reason for dicontinuation", UserControls_usrMessageBar.WarningType.Danger)
                ddldiscontResons.Focus()
                Exit Sub
            End If
            Try
                Dim dt = Date.Parse(txtFrom.Text) 
            Catch ex As Exception
                usrMessageBar.ShowNotification("Please select the date", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End Try
            If ddlType.SelectedValue = "P" Or ddlType.SelectedValue = "T" Then
                If studClass.checkFeeClosingDate(Session("sbsuid"), hfACD_ID.Value, Date.Parse(txtFrom.Text)) = False Then
                    'lblError.Text = "The discontinuation strarting date has to be higher than the fee closing date"
                    usrMessageBar.ShowNotification("The discontinuation strarting date has to be higher than the fee closing date", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If

            Try
                If Date.Parse(txtFrom.Text) < Date.Parse(hfFromDate.Value) Then
                    'lblError.Text = "The discontinuation strarting date has to be a date higher than or equal to the service date " + hfFromDate.Value
                    usrMessageBar.ShowNotification("The discontinuation strarting date has to be a date higher than or equal to the service date " + hfFromDate.Value, UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            Catch ex As Exception
            End Try

            If SAVE_TRANSPORT_DISCONTINUE_REQ() = 0 Then
                btnSave.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "P" Then
            rdTo.Enabled = False
            tblTpt0.Rows(0).Cells(4).Visible = False
            tblTpt0.Rows(0).Cells(5).Visible = False
            'tblTpt0.Rows(9).Cells(8).Visible = False
            lblDtFrom.Text = "Bus Service End Date"
        Else
            rdTo.Enabled = True
            tblTpt0.Rows(0).Cells(4).Visible = True
            tblTpt0.Rows(0).Cells(5).Visible = True
            'tblTpt0.Rows(9).Cells(8).Visible = True
            lblDtFrom.Text = "From"
        End If
    End Sub
    Private Sub BinddiscontinuReason()
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString)

            Dim cmd As New SqlCommand("[OASIS_TRANSPORT].dbo.[GetDiscontinueReason]", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = ddlBUnit.SelectedValue.ToString
            'cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = ddlAcademicYear.SelectedValue.ToString
            'cmd.Parameters.Add("@GRADE", SqlDbType.VarChar).Value = ddlgrade.SelectedValue.ToString
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddldiscontResons.DataSource = dr
                    ddldiscontResons.DataTextField = "DISCONT_REASON"
                    ddldiscontResons.DataValueField = "DISCONTR_ID"
                    ddldiscontResons.DataBind()
                    Dim li As New ListItem
                    li.Text = ""
                    li.Value = ""
                    ddldiscontResons.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub
End Class
