Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Transport_tptVehicle_History
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ' btnBack.Attributes.Add("onClick", "return go();")

                btnBsuSave.Visible = True
                Dim VID As String = Request.QueryString("VID")
                txtBSU_From.Attributes.Add("Readonly", "Readonly")
                txtBSU_TO.Attributes.Add("Readonly", "Readonly")
                Dim Sel_Flag As String = Request.QueryString("Sel_Flag")
                If Sel_Flag = "O" Then
                    ltLabel.Text = "Change Owned By"
                    Literal1.Text = "Owned Till Now By"
                ElseIf Sel_Flag = "P" Then
                    ltLabel.Text = "Change Operated By"
                    Literal1.Text = "Operated Till Now By"
                ElseIf Sel_Flag = "A" Then
                    ltLabel.Text = "Change Allocated To"
                    Literal1.Text = "Allocated Till Now To"
                End If

             
                Using VID_reader As SqlDataReader = TransportClass.GetVehicleAlloc_ID(VID, Sel_Flag)
                    If VID_reader.HasRows = True Then
                        gvHistory.DataSource = VID_reader
                        gvHistory.DataBind()
                    Else
                        gvHistory.CssClass = "matters"
                        gvHistory.EmptyDataText = "Currently no record available ."
                        gvHistory.DataBind()
                    End If

                End Using

                Using VID_Change_reader As SqlDataReader = TransportClass.GetVehicleChanged_ID(VID, Sel_Flag)
                    If VID_Change_reader.HasRows = True Then

                        While VID_Change_reader.Read
                            txtBSU_From.Text = Convert.ToString(VID_Change_reader("BSU_NAME"))
                            hfBSU_From.Value = Convert.ToString(VID_Change_reader("BSU_OLD"))
                            hfVAL_ID.Value = Convert.ToString(VID_Change_reader("VAL_ID"))
                            hfVEH_ID.Value = Convert.ToString(VID_Change_reader("VEH_ID"))

                            If IsDate(VID_Change_reader("FROMDT")) = True Then
                                ViewState("FROMDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((VID_Change_reader("FROMDT")))).Replace("01/Jan/1900", "")
                            End If

                        End While
                    Else
                        lblError.Text = "Error Occured While loading data. Please Contact Admin"
                    End If

                End Using

            Catch ex As Exception
                UtilityObj.Errorlog("OASISAUDIT", ex.Message)
            End Try
        End If
    End Sub

    Protected Sub btnBsuSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBsuSave.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If serverDateValidate() = "0" Then

            str_err = calltransaction(errorMessage)
            If str_err = "0" Then
                lblError.Text = "Record Saved Successfully"
                'Response.Write("<script language='javascript'>")
                'Response.Write("window.location.reload(true);")
                'Response.Write("window.returnValue = '" & 1 & "';")
                ''Response.Write("window.close();")
                'Response.Write(" </script>")
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "POPUP", "closepopup()", True)

            Else
                lblError.Text = errorMessage
            End If
        End If

    End Sub
    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFROMDT.Text.Trim <> "" Then
            Dim strfDate As String = txtFROMDT.Text.Trim

            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFROMDT.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFROMDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"

                ElseIf IsDate(dateTime1) And IsDate(ViewState("FROMDT")) Then
                    Dim d1 As Date = ViewState("FROMDT")
                    Dim d2 As Date = dateTime1

                    If d1 >= d2 Then
                        CommStr = CommStr & "<div>From Date entered must be greater than the previous date</div>"
                    End If

                End If
            End If
        End If


        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim status As Integer
        Dim VAL_ID As String = hfVAL_ID.Value
        Dim VEH_ID As String = hfVEH_ID.Value
        Dim BSU_TO As String = hfBSU_TO.Value
        Dim FROMDT As String = txtFROMDT.Text
        Dim VTYPE As String = Request.QueryString("Sel_Flag")

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                status = TransportClass.SAVE_VEHICLE_ALLOC(VAL_ID, VEH_ID, BSU_TO, FROMDT, VTYPE, transaction)

                If Status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(Status)  '"Error in inserting new record"
                    Return "1"
                End If
                  

                txtFROMDT.Text = ""
                txtBSU_From.Text = ""
                txtBSU_TO.Text = ""
                btnBsuSave.Visible = False
                calltransaction = "0"
               
            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
        

    End Function
    

End Class
