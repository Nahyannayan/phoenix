<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="tptDrvConBusAllocate.aspx.vb" Inherits="Transport_tptDrvConBusAllocate" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Bus Driver Conductor Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">


 <table id="tbl_ShowScreen" runat="server" align="center" width="100%">
           <tr>
            <td align="left" width="100%">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" >
                </asp:ValidationSummary>
                </td>
                </tr>
                               
                <tr>
                <td >
    <table align="center"  id="tbPromote" runat="server" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Select Bus No</span></td>
                       
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBusNo"  runat="server" Width="98px" AutoPostBack="True">
                            </asp:DropDownList></td>
                        
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    
                    <tr>
                    <td colspan="4" align="left">
                        </td>
                    </tr>
                      <tr>
                      
                      
                     <td align="left" colspan="4" width="100%">
                     
                     <asp:GridView ID="gvTpt" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                     PageSize="20" Width="100%" AllowSorting="True" >
                     <Columns>
                     
                      

                        <asp:TemplateField HeaderText="TRD_ID" Visible="False" >
                        <ItemTemplate>
                        <asp:Label ID="lblStuId" runat="server" text='<%# Bind("TRD_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="TRD_ID" Visible="False" >
                        <ItemTemplate>
                        <asp:Label ID="lblBnoId" runat="server" text='<%# Bind("TRD_BNO_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        
                          <asp:TemplateField HeaderText="TRD_ID" Visible="False" >
                        <ItemTemplate>
                        <asp:Label ID="lblVehId" runat="server" text='<%# Bind("VEH_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="STU_ID" Visible="False" >
                        <ItemTemplate>
                        <asp:Label ID="lblDrvId" runat="server" text='<%# Bind("DRV_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="STU_ID" Visible="False" >
                        <ItemTemplate>
                        <asp:Label ID="lblDrvMobile" runat="server" text='<%# Bind("DRVMOBILE") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="STU_ID" Visible="False" >
                        <ItemTemplate>
                        <asp:Label ID="lblConMobile" runat="server" text='<%# Bind("CONMOBILE") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="STU_ID" Visible="False" >
                        <ItemTemplate>
                        <asp:Label ID="lblConId" runat="server" text='<%# Bind("CON_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Bus No.">
                        <ItemTemplate>
                        <asp:Label ID="lblBusNo" runat="server" text='<%# Bind("BNO_DESCR") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        
                        <asp:TemplateField HeaderText="Vehicle">
                        <ItemTemplate>
                         <asp:Label ID="lblVehicle" text='<%# Bind("VEH_REGNO") %>' runat="server" ></asp:Label><br />
                        <asp:DropDownList ID="ddlVehicle"  Visible="false"  runat="server" ></asp:DropDownList>
                        
                        </ItemTemplate>
                            <HeaderStyle Width="15%" />
                        </asp:TemplateField>




                        <asp:TemplateField HeaderText="Driver">
                        <ItemTemplate>
                         <asp:Label ID="lblDriver"  text='<%# Bind("DRIVER") %>' runat="server"  ></asp:Label><br />                         
                         <asp:DropDownList ID="ddlDriver" AutoPostBack="true"  OnSelectedIndexChanged="ddlDriver_SelectedIndexChanged"  runat="server" Visible="false" ></asp:DropDownList>

                        <asp:TextBox ID="txtDrvMobile" runat="server" Visible="false" ></asp:TextBox >
                        </ItemTemplate>
                        </asp:TemplateField>


                      
                        <asp:TemplateField HeaderText="Conductor">
                        <ItemTemplate>
                         <asp:Label ID="lblConductor"   text='<%# Bind("CONDUCTOR") %>' runat="server" ></asp:Label>
                         
                         <asp:DropDownList ID="ddlConductor" AutoPostBack="true" Visible="false"  OnSelectedIndexChanged="ddlConductor_SelectedIndexChanged" runat="server" ></asp:DropDownList>
                        
                        <asp:TextBox ID="txtConMobile" Visible="false"  runat="server" ></asp:TextBox >
                        
                        </ItemTemplate>
                        </asp:TemplateField>


                   
                        <asp:TemplateField>
                        <HeaderTemplate>
                        <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                        <ItemTemplate>
                        <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateField>
                          
                       </Columns>  
                         <HeaderStyle CssClass="gridheader_pop" />
                         <RowStyle CssClass="griditem" />
                         <SelectedRowStyle CssClass="Green" />
                         <AlternatingRowStyle CssClass="griditem_alternative" />
                     </asp:GridView>
                     </td></tr>
                    </table>
                    &nbsp;
                    <asp:HiddenField ID="hfACD_ID" runat="server" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server"
                        type="hidden" value="=" /></td></tr>
           
    
</table>
  
    </div>
    
   
    </div>
     </div>

</asp:Content>

