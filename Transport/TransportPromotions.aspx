﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TransportPromotions.aspx.vb" Inherits="Transport_TransportPromotions" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .ltor {
            direction: rtl;
        }
    </style><div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-users"></i>
            <asp:Label Text='Transport Promotions' ID="lblheading" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div id="divHeading" class="divheadingS_CAL">
                    <div>
                        <asp:HiddenField ID="hidlatestrefid" runat="server" />
                    </div>
                </div>

                <table width="100%" align="center" border="0">
                    <tr>
                        <td align="left">
                            <div>
                                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                            </div>

                        </td>
                    </tr>
                </table>
                <table width="100%" align="center" style="border-collapse: collapse">

                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblslctgrade" runat="server" Text="Select Business Unit(s)" CssClass="field-label" /><span style="color: red">*</span>
                        </td>
                        <td  align="left" colspan="3"  >
                            
                            <asp:DropDownList ID="ddlBusinessunit"   runat="server" AutoPostBack="true"  ></asp:DropDownList>
                            
                        </td>
                         <td  align="left"><span class="field-label">Academic Year</span><span style="color: red">*</span>
                        </td>
                        <td colspan="3" align="left">
                            <asp:DropDownList ID="ddlAcdId"  runat="server" ></asp:DropDownList>
                        </td>
                       
                    </tr>
                    <tr>
                     <td  align="left"><span class="field-label">Promotion Name</span><span style="color: red">*</span>
                        </td>
                        <td colspan="8" align="left">
                            <asp:TextBox ID="txtPromotionName" runat="server" autocomplete="off" MaxLength="50" TabIndex="1" required="required" ValidationGroup="mandatory"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                       <td   align="left"><span class="field-label">Promotion Description</span><span style="color: red">*</span>
                        </td>
                        <td    align="left" colspan="8"  >

                            <asp:TextBox ID="txtPromotionDescr" runat="server" autocomplete="off" MaxLength="350" TabIndex="1" required="required" ValidationGroup="mandatory"></asp:TextBox>
                            
                        </td>
                    </tr>

                    <tr>
                        <td align="left" ><span class="field-label">Promotion Period</span><span style="color: red">*</span>
                        </td>
                        <td align="left"  colspan="3" >
                            <asp:RadioButtonList ID="rbtnlstPromotionPeriod" runat="server" RepeatDirection="Horizontal" required="required" ValidationGroup="mandatory" TabIndex="4" AutoPostBack="true" OnSelectedIndexChanged="rbtnlstPromotionPeriod_SelectedIndexChanged">
                                <asp:ListItem Value="DT" Text="Date" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="DY" Text="Days"></asp:ListItem>
                                <asp:ListItem Value="WK" Text="Week"></asp:ListItem>
                            </asp:RadioButtonList>
                            
                        </td>
                        <td   align="left"> <asp:Label id="Promodays" class="field-label" runat="server" Visible="false"  Text="Promotion Days"></asp:Label>
                            
                        </td>
                        
                        <td width="35%" colspan="3"><asp:TextBox Width="100px" ID="txtDaysWeek" autocomplete="off" runat="server" TabIndex="7" required="required" ValidationGroup="mandatory" MaxLength="3" onkeypress="if(event.keyCode<48 || event.keyCode>57)event.returnValue=false;"></asp:TextBox></td>

                    </tr>



                    
                    

                    
                    <tr runat="server" id="trDates" visible="true">
                        <td align="left"><span class="field-label">From Date</span><span style="color: red">*</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtFromDate" runat="server" autocomplete="off" required="required" ValidationGroup="mandatory"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="5" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFromDate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="txtFromDate"
                                    TargetControlID="txtFromDate" Format="dd/MMM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                        </td>
                        <td align="left"><span class="field-label">To Date</span><span style="color: red">*</span>
                        </td>
                        <td  align="left" colspan="3">
                            <asp:TextBox ID="txtToDate" runat="server" autocomplete="off" required="required" ValidationGroup="mandatory"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="6" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                PopupButtonID="imgTo" TargetControlID="txtToDate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="txtToDate"
                                    TargetControlID="txtToDate" Format="dd/MMM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>

                    <tr>

                        <td align="left" width="15%"><span class="field-label">Rate Type</span><span style="color: red">*</span>
                        </td>
                        <td align="left" colspan="3" width="35%" >
                            <asp:RadioButtonList ID="rbFeeType"        runat="server" RepeatDirection="Horizontal" required="required" ValidationGroup="mandatory" TabIndex="4" AutoPostBack="true" OnSelectedIndexChanged="rbtnRateType_SelectedIndexChanged">
                            
                                <asp:ListItem Value="FC" Text="Fixed" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="FS" Text="Location wise" ></asp:ListItem>
                                
                                
                            </asp:RadioButtonList>
                            
                        </td>
                        
                         
                         <td   align="left"> <asp:Label id="lblAmount" class="field-label" runat="server"   Text="Amount"></asp:Label>
                        </td>
                        <td align="left" colspan="3">
                             <asp:TextBox ID="txtAmount" runat="server" onFocus="this.select();"
                                                Style="text-align: right;" autocomplete="off" MaxLength="18" TabIndex="3" required="required" ValidationGroup="mandatory"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtAmount" />
                        </td>
                    </tr>
                    

                     <tr id="upload" runat="server" visible="false" >
                         <td   align="left"><span class="field-label">Select File</span><span style="color: red">*</span>
                        </td>
                        <td    align="left" colspan="6"  >
                         
                                                    <asp:FileUpload ID="FileUpload2" runat="server" UpdateMode="Always" /></td>
                                                <td  align="left" colspan="2" >
                         <asp:Button ID="btnImpTrRate" runat="server" CssClass="button" Text="Import" OnClick="btnImpTrRate_Click"/>
                                                    <asp:Button ID="btnValidate" runat="server" CssClass="button" Text="Validate" />
                                                    </td>
                         

                                            </tr>

                    
                    <tr id="ExcelGrid" runat="server">
                                                <td align="center" colspan="8">
                                                    <asp:GridView runat="server" AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Data"
                                                        CssClass="table table-bordered table-row" Width="100%" ID="gvTrRateImport" OnPageIndexChanging="gvTrRateImport_PageIndexChanging"
                                                        ShowFooter="True" OnRowDataBound="gvTrRateImport_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="SlNo" Visible="false"  HeaderText="SlNo">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Loc" HeaderText="Loc">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Area" DataField="Area">
                                                                <FooterStyle BackColor="#99CCFF" BorderStyle="None" HorizontalAlign="Right" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="RATE" HeaderText="Rate" DataFormatString="{0:0.00}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="Status" HeaderText="Status">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>

                                                          
                                                        </Columns>
                                                        <FooterStyle BackColor="#99CCFF" HorizontalAlign="Right" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>

                    <tr id="Import" runat="server" visible="true">
                                                <td align="center" colspan="8">
                                                    
                                                    <asp:Button ID="btnTrCommit" runat="server" CssClass="button" Text="Save"  />
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  />
                                                    <asp:Button ID="btnAdd" runat="server" visible="false" CausesValidation="False" CssClass="button" Text="Add" TabIndex="9" />

                                                </td>
                                            </tr>

                 
                    <tr>
                        <td colspan="8" align="center">
                            
                           <%-- <asp:Button ID="btnSave" runat="server" CausesValidation="False" CssClass="button"
                                Text="Save" TabIndex="9" />--%>
                            <%--<asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="10" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="mandatory" TabIndex="11" OnClick="btnSave_Click"  />--%>
                            <%--<asp:Button ID="bCancel" runat="server" runat="server" CssClass="button" Text="Cancel"  />--%>
                            <%--<asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"Text="Delete" OnClientClick="return confirm_delete();" TabIndex="13" /></td>--%>
                    </tr>
                </table>
            </div>
        </div>
        <asp:HiddenField ID="hfTPT_PROMO_ID" runat="server" Value="0" />
        <asp:HiddenField ID="hfAlreadySelected" runat="server" Value="0" />
    </div>
    <script type="text/javascript" language="javascript"> 
        
        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>

