<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="tplSplTransportDet.aspx.vb" Inherits="Transport_tplSplTransportDet"
    Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function GetSTUDENTS() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 600px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            //var NameandCode;
            //var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=h_GRD_IDs.ClientID %>').value;

            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
        <%--    var s = document.getElementById('<%=h_STU_IDs.ClientID %>').value
            if (s != '') {
                s = s + "___"
            }--%>
           // result = window.showModalDialog("../Transport/tptPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures);
            var oWnd = radopen("../Transport/tptPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "pop_student");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = s + result; //NameandCode[0];

            }
            else {
                return false;
            }--%>
        }
         function OnClientClose(oWnd, args) {
            var NameandCode;
            var val;
            var arg = args.get_argument();
            var s = document.getElementById('<%=h_STU_IDs.ClientID%>')
            if (s.value != '' && s.value == undefined) {
                s.value = s.value + "___"
            }
            if (arg) {
                NameandCode = arg.arry.split('||');
                val += NameandCode[0];
                s.value = s.value.replace("undefined", "__");
                document.getElementById('<%=h_STU_IDs.ClientID%>').value = s.value + val;
                document.getElementById('<%=txtStudIDs.ClientID%>').value = s.value + val;
                __doPostBack('<%= txtStudIDs.ClientID %>', 'TextChanged');

            }
        }

        function Getstaffs() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 729px; ";
            //sFeatures += "dialogHeight: 600px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            //var NameandCode;
            //var result;

           // result = window.showModalDialog("../Transport/tptPopupForm.aspx?multiselect=true&ID=TEACHER", "", sFeatures);
            var oWnd = radopen("../Transport/tptPopupForm.aspx?multiselect=true&ID=TEACHER", "pop_staff");
           <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_EMP_IDs.ClientID %>').value = result; //NameandCode[0];
            }
            else {
                return false;
            }--%>
        }
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

       
        function OnClientClose3(oWnd, args) {
            
             var NameandCode;
             var val;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.arry.split('||');
                val += NameandCode[0];
                document.getElementById('<%=h_EMP_IDs.ClientID%>').value = val
                document.getElementById('<%=h_EMP_IDs.ClientID %>').value
                  document.getElementById('<%=txtEmpId.ClientID%>').value =  val;
                __doPostBack('<%= txtEmpId.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
      <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="800px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows >
               <telerik:RadWindow ID="pop_staff" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="800px" Height="620px" >
            </telerik:RadWindow>
          </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Special Transport Arrangement Details"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="Table4" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="LinkButton2" runat="server">Add New </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date of Transport Requirement</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDate" runat="server" AutoCompleteType="Disabled"></asp:TextBox><asp:ImageButton
                                            ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Program </span><span style=" color: #800000"></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPgm" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Travelling From</span><span style=" color: #800000"></span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTfrm" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">To reach at (Place & Time)</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtToreach" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Return From (Place)</span><span style=" color: #800000"></span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtReturnfrm" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Name of the Incharge</span><span style=" color: #800000"></span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtIncharge" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Time</span><span style=" color: #800000"></span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTime" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Mobile Number</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMobno" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table id="Table1" runat="server" width="100%">
                                <tr class="title-bg">
                                    <td align="left" colspan="5"  valign="middle">Student / Staff Details 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr id="20">
                                                <td align="left" valign="middle" width="20%"><span class="field-label">Grade</span>
                                                </td>

                                                <td align="left" valign="middle" width="30%">
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="middle" width="20%"><span class="field-label">Section</span>
                                                </td>
                                                <td align="left" valign="middle" width="30%">
                                                    <asp:DropDownList ID="ddlSection" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><span class="field-label">Student</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtStudIDs" runat="server" AutoPostBack="true" Enabled="false"  OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                                                    <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS() ;return false;"
                                                        OnClick="imgStudent_Click"></asp:ImageButton>
                                                    <asp:GridView ID="grdStudent" runat="server" AllowPaging="false" AutoGenerateColumns="False" Width="80%" CssClass="table table-row table-bordered" AllowCustomPaging="true"
                                                        PageSize="10" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Stud. No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader_new" />
                                                    </asp:GridView>
                                                </td>

                                                <td align="left" valign="top"><span class="field-label">Staff</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtEmpId" runat="server"  AutoPostBack="true" Enabled="false" OnTextChanged="txtEmpId_TextChanged"></asp:TextBox>
                                                    <asp:ImageButton ID="imgStaff" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="Getstaffs();return false"
                                                        OnClick="imgStaff_Click"></asp:ImageButton>
                                                    <asp:GridView ID="grdStaff" runat="server" AllowPaging="false" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                        PageSize="5" OnPageIndexChanging="grdStaff_PageIndexChanging" Width="80%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Emp. No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbstf_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DESCR" HeaderText="Emp Name"></asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader_new" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btn" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                        <asp:Button ID="Update" runat="server" CssClass="button" Text="Update" ValidationGroup="groupM1"
                                            Visible="false" />
                                        <asp:Button ID="Print" runat="server" CssClass="button" Text="Print" ValidationGroup="groupM1" />
                                        <asp:Button ID="Close" runat="server" CssClass="button" Text="Close" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trcatGrade" runat="server">
                        <td id="Td1" align="center" runat="server"  >
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" Width="100%"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="15">
                                <RowStyle CssClass="griditem"  />
                                <Columns>
                                    <asp:TemplateField HeaderText="Staff No" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("spl_id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student / Emp No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblregno" runat="server" Text='<%# Bind("stu_regno") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("stu_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                         <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltype" runat="server" Text='<%# Bind("stu_r") %>'></asp:Label>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                         <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgrd" runat="server" Text='<%# Bind("stu_grd") %>'></asp:Label>
                                        </ItemTemplate>
                                       <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                         <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                        <ItemTemplate>

                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                Text="Delete"></asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="You are about to delete this record.Do you want to proceed?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                       <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                         <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="h_EMP_IDs" runat="server" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>
