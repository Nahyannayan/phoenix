Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptPickup_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                  Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

                    GridBind()

                    set_Menu_Img()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnLocation_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSubLocation_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnPickUp_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Transport\tptPickup_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvTptPickUp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTptPickUp.PageIndexChanging
        Try
            gvTptPickUp.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTptPickUp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTptPickUp.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvTptPickUp.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblLocId As Label
                Dim lblSblId As Label
                Dim lblLocation As Label
                Dim lblSubLocation As Label


                With selectedRow
                    lblLocId = .FindControl("lblLocId")
                    lblLocation = .FindControl("lblLocation")
                    lblSblId = .FindControl("lblSblId")
                    lblSubLocation = .FindControl("lblSubLocation")

                End With


                If e.CommandName = "View" Then
                    Dim url As String
                    Dim editstring As String = Encr_decrData.Encrypt(lblLocId.Text + "|" + lblLocation.Text + "|" + lblSblId.Text + "|" + lblSubLocation.Text)
                    url = String.Format("~\Transport\tptpickup_M.aspx?MainMnu_code={0}&datamode={1}&editstring=" + editstring, ViewState("MainMnu_code"), ViewState("datamode"))
                    Response.Redirect(url)
                    'ElseIf e.CommandName = "edit" Then

                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTptPickUp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptPickUp.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTptPickUp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptPickUp.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvTptPickUp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptPickUp.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT pnt_id,pnt_sbl_id,sbl_loc_id,pnt_description,loc_description,sbl_description,pnt_pickup,pnt_dropoff" _
        '                        & " FROM tptpickuppoints_m A, tptsublocation_m B,tptlocation_m C WHERE " _
        '                        & " A.pnt_sbl_id=B.sbl_id AND B.sbl_loc_id=C.loc_id AND pnt_bsu_id='" + Session("sbsuid") + "'"

        Dim str_query As String = "SELECT PNT_ID,SBL_LOC_ID,PNT_SBL_ID, PNT_DESCRIPTION, LOC_DESCRIPTION, SBL_DESCRIPTION " _
                                & "FROM TRANSPORT.SUBLOCATION_M AS A INNER JOIN" _
                                & " TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID = B.PNT_SBL_ID INNER JOIN" _
                                & " TRANSPORT.LOCATION_M AS C ON A.SBL_LOC_ID = C.LOC_ID" _
                                & " WHERE PNT_BSU_ID='" + Session("sbsuid") + "'"

        Dim selectedLocation As String
        Dim txtSearch As New TextBox
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim locSearch As String = ""
        Dim sblSearch As String = ""
        Dim pntSearch As String = ""


        If gvTptPickUp.Rows.Count > 0 Then

            txtSearch = New TextBox
            txtSearch = gvTptPickUp.HeaderRow.FindControl("txtPickup")
            strSidsearch = h_Selected_menu_3.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("pnt_description", txtSearch.Text, strSearch)
            pntSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvTptPickUp.HeaderRow.FindControl("txtLocation")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("loc_description", txtSearch.Text, strSearch)
            locSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvTptPickUp.HeaderRow.FindControl("txtSubLocation")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("sbl_description", txtSearch.Text, strSearch)
            sblSearch = txtSearch.Text

            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If

        str_query += "  ORDER BY LOC_DESCRIPTION,SBL_DESCRIPTION,pnt_description"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptPickUp.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTptPickUp.DataBind()
            Dim columnCount As Integer = gvTptPickUp.Rows(0).Cells.Count
            gvTptPickUp.Rows(0).Cells.Clear()
            gvTptPickUp.Rows(0).Cells.Add(New TableCell)
            gvTptPickUp.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTptPickUp.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTptPickUp.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTptPickUp.DataBind()
        End If

        txtSearch = gvTptPickUp.HeaderRow.FindControl("txtPickup")
        txtSearch.Text = pntSearch

        txtSearch = gvTptPickUp.HeaderRow.FindControl("txtLocation")
        txtSearch.Text = locSearch

        txtSearch = gvTptPickUp.HeaderRow.FindControl("txtSubLocation")
        txtSearch.Text = sblSearch

        set_Menu_Img()

        Dim row As GridViewRow
        Dim lblPntId As Label
        Dim lnkAddTrip As LinkButton
        Dim str As String
        For Each row In gvTptPickUp.Rows
            lblPntId = row.FindControl("lblpntId")
            If Not lblPntId Is Nothing Then
                lnkAddTrip = row.FindControl("lnkAddTrip")
                str = "javascript:return AddtoTrip(" + lblPntId.Text + ");"
                lnkAddTrip.OnClientClick = str
            End If
        Next
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region

    Protected Sub gvTptPickUp_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvTptPickUp.RowEditing

    End Sub

End Class
