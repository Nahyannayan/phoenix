<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptBusAllocate_M.aspx.vb" Inherits="Transport_tptBusAllocate_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Bus Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table  width="100%">
        
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Select Business Unit</span></td>
            
            <td align="left" width="30%" colspan="2">
                <asp:DropDownList ID="ddlbsu" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
            
            <td align="left" width="30%"></td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table width="100%">
                    <tr>
                        <td>
                            <span class="field-label">Unallocated Vehicle List</span>
                            <asp:ListBox ID="lstunalloctedBuslist" runat="server" SelectionMode="Multiple"></asp:ListBox></td>
                        <td>
                            <asp:Button ID="btnallocate" runat="server" CssClass="button" Text=">>" />
                            <br />
                            <br />
                            <asp:Button ID="btnUnallocate" runat="server" CssClass="button" Text="<<" /></td>
                        <td>
                            <span class="field-label">Allocated Vehicle List</span>
                            <asp:ListBox ID="lstalloctedBuslist" runat="server" SelectionMode="Multiple"></asp:ListBox></td>
                    </tr>
                </table>
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
    </table>
                </div>
            </div>
        </div>

</asp:Content>

