<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tptShowDetail.aspx.vb" Inherits="ShowEmpDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <base target="_self" />

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->
    <%--  <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        // function settitle(){
        //alert('fgfg');
        //document.title ='sfsdsd';
        // alert('fgfg');
        //      }


        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>

</head>
<body onload="listen_window();">
    <form id="form1" runat="server">

        <div>
            <div>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvCommInfo" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Employee ID" SortExpression="EMP_ID">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("Code") %>'></asp:LinkButton>&nbsp;
                                                                </ItemTemplate>
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblID" runat="server" EnableViewState="False" Text="Business ID"></asp:Label><br />
                                                                    <asp:TextBox ID="txtcode" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpId_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblName" runat="server" Text="Business Unit Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpName_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Selected"
                                                                        Text='<%# Eval("Descr") %>' OnClick="LinkButton1_Click"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CommID" Visible="False">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCommID" runat="server" Text='<%# bind("CommID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <RowStyle />
                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input type="hidden" id="h_SelectedId" runat="server" value="0" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 95%">
            </table>
        </div>


    </form>

</body>
</html>
