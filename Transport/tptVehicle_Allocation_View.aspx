<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptVehicle_Allocation_View.aspx.vb" Inherits="Transport_tptVehicle_Allocation_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>


    <script language="javascript" type="text/javascript">


        function ShowDetails(url, mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 645px; ";
            sFeatures += "dialogHeight: 350px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var result;

            url = url + '&Sel_Flag=' + mode;
            //alert(url);
            //result = window.showModalDialog(url, "", sFeatures)
            //if (result == "1") {
            //    window.location.reload(true);
            //}
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;

            //return true;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Vehicle Allocation"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <%--  <table id="Table1" border="0" width="100%">
                    <tr style="font-size: 12pt">
                        <td align="left" class="title" width="50%">
                           </td>
                    </tr>
                </table>--%>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="tbl_test" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:GridView ID="gvVehicleAlloc" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="VEH_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("SAL_ID") %>'></asp:TextBox>

                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVID" runat="server" Text='<%# Bind("VEH_ID") %>' __designer:wfdid="w1"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Category">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("GRD_DESCR") %>'></asp:TextBox>

                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblCAT_DESCRH" runat="server" Text="Category"></asp:Label><br />
                                                        <asp:TextBox ID="txtCAT_DESCR" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchCAT_DESCR" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchCAT_DESCR_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCAT_DESCR" runat="server" Text='<%# Bind("CAT_DESCR") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reg.No">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EMP_Name") %>'></asp:TextBox>

                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblREGNOH" runat="server" Text="Reg. No"></asp:Label><br />
                                                        <asp:TextBox ID="txtREGNO" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchREGNO" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchREGNO_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblREGNO" runat="server" Text='<%# Bind("REGNO") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GPS_UNIT_ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGPS_UNIT_ID" runat="server" Text='<%# Bind("GPS_UNIT_ID")%>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="capacity">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w3"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblCAPH" runat="server" Text="Capacity"></asp:Label><br />
                                                        <asp:TextBox ID="txtCAP" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchCAP" OnClick="btnSearchCAP_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                            <asp:Label ID="lblCAP" runat="server" Text='<%# Bind("CAP") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Owned">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblOWNH" runat="server" Text="Owned By"></asp:Label><br />
                                                        <asp:TextBox ID="txtOwned" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchOwned" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" OnClick="btnSearchOwned_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lbtnOwned" runat="server" Text='<%# Bind("OWNED") %>'></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Operated">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblOperaH" runat="server" Text="Operated By"></asp:Label><br />
                                                        <asp:TextBox ID="txtOpera" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchOpera" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" OnClick="btnSearchOpera_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lbtnOperBy" runat="server" Text='<%# Bind("OPRT") %>'></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Allocated">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblAllocH" runat="server" Text="Allocated To"></asp:Label><br />
                                                        <asp:TextBox ID="txtAlloc" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchAlloc" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" OnClick="btnSearchAlloc_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                            <asp:LinkButton ID="lbtnALTO" runat="server" Text='<%# Bind("ALTO") %>' __designer:wfdid="w12"></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>

                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblView" runat="server" OnClick="lblView_Click">View</asp:LinkButton>

                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>

