<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptFuel_Edit.aspx.vb" Inherits="Transport_tptFuel_Edit" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Fuel Entry SetUp
        </div>
        <div class="card-body">
            <div class="table-responsive">


<table id="tbl_ShowScreen" runat="server" align="center" width="100%" >
            
            <tr>
                <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                        </tr>
               
               
      
        
           <tr>
      
            <td align="center" width="100%">
            
  <table id="tblFuel" runat="server" align="center" width="100%" >
                            
         <tr><td colspan="2">
          <table id="Table3" runat="server" align="center" width="100%" >
                      <tr><td align="left" width="20%">
               <span class="field-label">Select Vehicle</span></td>
                 
                 <td align="left" width="30%">
                     <asp:DropDownList AutoPostBack="true" ID="ddlVehicle" runat="server">
                     </asp:DropDownList>
                     </td>
                          <td width="20%"></td>
                          <td width="30%"></td>
                      </tr>
              <tr>
               <td align="left" width="20%">
                    <asp:Label ID="Label2" runat="server" Text="Fill Date From" CssClass="field-label"></asp:Label></td>
                   
                 <td align="left" width="30%">
                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                         <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                           /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom"
                            Display="Dynamic" ErrorMessage="Enter the Trip Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtFrom"
                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date"
                            ValidationGroup="groupM1">*</asp:CustomValidator>
                     <asp:RequiredFieldValidator ID="rfFrom" runat="server" ErrorMessage="Please enter the field trip date from" ControlToValidate="txtFrom" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>        
                  </td>
                       
                              <td align="left" width="20%">
                    <asp:Label ID="Label3" runat="server" Text="To" CssClass="field-label"></asp:Label></td>
                    
                 <td align="left" width="30%">
                 <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                         <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif"
                          /><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTo"
                            Display="Dynamic" ErrorMessage="Enter the Trip Date To in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtTo"
                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date To entered is not a valid date"
                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                     <asp:RequiredFieldValidator ID="rfTo" runat="server" ErrorMessage="Please enter the field trip date to" ControlToValidate="txtTo" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>   
                 </td>  </tr>
              <tr>   
                   <td align="center" colspan="4" >             
                <asp:Button id="btnSearch" runat="server" Text="Search" CssClass="button" ValidationGroup="groupM1" ></asp:Button>
                </td>
             </tr>
                    </table>
         </td></tr>                       
                    
        <tr><td  colspan="4" align="center" width="100%" >
         
         <table id="Table2" runat="server" align="center" width="100%" >
          
              
      
            <tr><td colspan="4" align="center">
                      <asp:GridView ID="gvFuel" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     PageSize="20" BorderStyle="None" >
                     <Columns>
                    
                       <asp:TemplateField  Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblTflId" runat="server" Text='<%# Bind("tfl_id") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                          
                       <asp:TemplateField HeaderText="FillDate" >
                      <ItemStyle HorizontalAlign="center"  />
                      <ItemTemplate  >
                      <asp:TextBox ID="txtFill" ValidationGroup='<%# Eval("tfl_ID") %>' runat="server" CausesValidation="true" Width="75%" Text='<%# Bind("TFL_FILLDATE", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                       <asp:ImageButton
                       ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                       <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                       Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFill">
                      </ajaxToolkit:CalendarExtender>
                      </ItemTemplate>
                      </asp:TemplateField>
                      
                                
                       <asp:TemplateField HeaderText="Previous KM Reading" >
                       <ItemStyle HorizontalAlign="center" />
                       <ItemTemplate>
                       <asp:TextBox ID="txtPrev" ValidationGroup='<%# Eval("tfl_ID") %>' runat="server" CausesValidation="true" Width="60px" Text='<%# Bind("TFL_PREVKM") %>'></asp:TextBox>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                      <asp:TemplateField HeaderText="Current KM Reading" >
                      <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:TextBox ID="txtCurrent" ValidationGroup='<%# Eval("TFL_ID") %>' runat="server" CausesValidation="true" Width="60px" Text='<%# Bind("TFL_CURRENTKM") %>'></asp:TextBox>
                      </ItemTemplate>
                      </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="Gallons" >
                      <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:TextBox ID="txtGallon" ValidationGroup='<%# Eval("TFL_ID") %>' runat="server" CausesValidation="true" Width="60px" Text='<%# Bind("tfl_Gallon") %>'></asp:TextBox>
                      </ItemTemplate>
                      </asp:TemplateField>
                      
                      <asp:TemplateField HeaderText="Amount(AED)" >
                      <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:TextBox ID="txtAmount" ValidationGroup='<%# Eval("TFL_ID") %>' runat="server" CausesValidation="true" Width="60px" Text='<%# Bind("tfl_amount") %>'></asp:TextBox>
                      </ItemTemplate>
                      </asp:TemplateField>
                      
                   
                   </Columns>  
                     <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                          <RowStyle CssClass="griditem" Wrap="False" />
                          <%--<SelectedRowStyle CssClass="Green" Wrap="False" />
                          <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
                          <EmptyDataRowStyle Wrap="False" />
                          <EditRowStyle Wrap="False" />
                     </asp:GridView>
                   </td></tr>
                   </table>
           </td>
           </tr>
            <tr>
            <td align="center" colspan="4">
               
                 <asp:Button ID="btnSave"  runat="server" CssClass="button" Text="Save" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
              
                   
                    </td>
        </tr>
        
           </table>
            
                    </td></tr>
          
        </table>
    <asp:HiddenField ID="hfTRD_ID" runat="server" />

                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                   Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
               </ajaxToolkit:CalendarExtender>
               <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                   Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
               </ajaxToolkit:CalendarExtender>

                </div>
            </div>
         </div>

</asp:Content>

