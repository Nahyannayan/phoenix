<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptPickup_M.aspx.vb" Inherits="Transport_tptPickup_M" MaintainScrollPositionOnPostback="true" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PickUp Points Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" Style="text-align: left" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfPickupPoint" runat="server" ErrorMessage="Please enter the field PickUp Point" ControlToValidate="txtPickupPoint" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>


                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblLocText" runat="server" Text="Select Location" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label2" runat="server" Text="Select Sub Location" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubLocation" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Pickup Point</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPickupPoint" runat="server"></asp:TextBox></td>
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" /></td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                            <tr>
                                                <td align="center" >
                                                    <asp:GridView ID="gvtptPickup" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" >

                                                        <Columns>

                                                            <asp:TemplateField HeaderText="loc_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbllocId" runat="server" Text='<%# Bind("sbl_loc_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="acd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSblId" runat="server" Text='<%# Bind("pnt_sbl_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="acd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPntId" runat="server" Text='<%# Bind("pnt_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Location">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("loc_description")  %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Sub Location">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubLocation" runat="server"  Text='<%# Bind("sbl_description")  %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Pick Up Point">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPickupPoint" runat="server" Text='<%# Bind("pnt_description")  %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="-" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblflag" runat="server" Text='<%# Bind("flag")  %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:ButtonField CommandName="edit" HeaderText="edit" Text="edit">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>

                                                            <asp:ButtonField CommandName="delete" HeaderText="delete" Text="delete">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>

                                                        </Columns>
                                                        <HeaderStyle  />
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative"  />
                                                        <EmptyDataRowStyle  />
                                                        <EditRowStyle  />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" TabIndex="5" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" TabIndex="6" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
</asp:Content>

