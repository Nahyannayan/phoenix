Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Transport_tptSeatCapacity_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try


                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                ViewState("datamode") = "add"

                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                PopulateAcademicYear()
                BindShift()


                GridBind()


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If


    End Sub

    Protected Sub btnArea_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"

    Public Sub PopulateAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=" + Session("clm") _
                                  & " AND ACD_ACY_ID>=" + Session("Current_ACY_ID")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
      
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvTPT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub BindShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "SELECT SHF_ID,SHF_DESCR FROM VV_SHIFTS_M WHERE SHF_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
    End Sub

    Sub GridBind()
        Dim sbl_id As String = Request.QueryString("sbl_id")
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String


        Dim str_query As String = "exec SEAT_CAPACITY_SELECTDAREA " _
                                 & "'" + Session("sbsuid") + "'," _
                                 & ddlAcademicYear.SelectedValue.ToString + "," _
                                 & Session("Current_ACY_ID") + "," _
                                 & ddlShift.SelectedValue.ToString + "," _
                                 & "'" + IIf(rdOnward.Checked = True, "Onward", "Return") + "'," _
                                 & sbl_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim txtSearch As New TextBox

        Dim areaSearch As String = ""

        If gvTPT.Rows.Count > 0 Then
            txtSearch = gvTPT.HeaderRow.FindControl("txtArea")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("SBL_DESCRIPTION", txtSearch.Text, strSearch)
            areaSearch = txtSearch.Text
        End If

        Dim dv As DataView
        dv = ds.Tables(0).DefaultView

        If areaSearch <> "" Then
            dv.RowFilter = strFilter
        End If

        gvTPT.DataSource = dv
        If dv.Table.Rows.Count = 0 Then
            dv.Table.Rows.Add(dv.Table.NewRow())
            gvTPT.DataBind()
            Dim columnCount As Integer = gvTPT.Rows(0).Cells.Count
            gvTPT.Rows(0).Cells.Clear()
            gvTPT.Rows(0).Cells.Add(New TableCell)
            gvTPT.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTPT.DataBind()
        End If

        If ddlAcademicYear.SelectedValue = Session("Current_ACD_ID") Then
            gvTPT.Columns(5).Visible = False
        Else
            gvTPT.Columns(5).Visible = True
        End If
    End Sub


    Sub BindPickup(ByVal gvPickuP As GridView, ByVal sbl_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String


        Dim str_query As String = "exec SEAT_CAPACITY_PICKUP " _
                                 & "'" + Session("sbsuid") + "'," _
                                 & ddlAcademicYear.SelectedValue.ToString + "," _
                                 & Session("Current_ACY_ID") + "," _
                                 & ddlShift.SelectedValue.ToString + "," _
                                 & "'" + IIf(rdOnward.Checked = True, "Onward", "Return") + "'," _
                                 & sbl_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)



        gvPickuP.DataSource = ds
        gvPickuP.DataBind()

       
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region



    Protected Sub rdOnward_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOnward.CheckedChanged
        Try
            If rdOnward.Checked = True Then
                GridBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub rdReturn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdReturn.CheckedChanged
        Try
            If rdReturn.Checked = True Then
                GridBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTPT.PageIndexChanging
        Try
            gvTPT.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTPT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTPT.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblSblId As Label
                Dim gvPickup As GridView
                lblSblId = e.Row.FindControl("lblSblId")
                gvPickup = e.Row.FindControl("gvPickup")
                If gvPickup.Rows.Count = 0 Then
                    BindPickup(gvPickup, lblSblId.Text)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
