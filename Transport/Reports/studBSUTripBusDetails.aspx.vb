﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Transport_Reports_studBSUTripBusDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim OneWayTotal As Integer = 0
    Dim bothwayTotal As Integer = 0
    Dim GrandTotal As Integer = 0

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Dim ss As ScriptManager = New ScriptManager
        ss.RegisterPostBackControl(btnExcel)

        Try
            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000150") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            End If
        Catch ex As Exception

        End Try



    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindStudentTripCount()
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        ParticipantsExcelDownload()
    End Sub


    Protected Sub gvStudentCount_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvStudentCount.PageIndex = e.NewPageIndex
        BindStudentTripCount()
    End Sub


    Private Sub BindStudentTripCount()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As New DataSet

            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Date", txtDate.Value)



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Bsu_Trip_Student_Bus_Details", params)
            If ds.Tables(0).Rows.Count > 0 Then

                gvStudentCount.DataSource = ds.Tables(0)
                gvStudentCount.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                '  ds.Tables(0).Rows(0)(2) = True
                gvStudentCount.DataSource = ds.Tables(0)
                Try
                    gvStudentCount.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStudentCount.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStudentCount.Rows(0).Cells.Clear()
                gvStudentCount.Rows(0).Cells.Add(New TableCell)
                gvStudentCount.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudentCount.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudentCount.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub gvStudentCount_RowDatabound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)


        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim oneWay As String = DirectCast(e.Row.FindControl("lblOneWay"), Label).Text

                OneWayTotal += Convert.ToInt32(oneWay)

                Dim bothway As String = DirectCast(e.Row.FindControl("lblbothWay"), Label).Text
                bothwayTotal += Convert.ToInt32(bothway)

                Dim grandTot As String = DirectCast(e.Row.FindControl("lblTOTAL"), Label).Text
                GrandTotal += Convert.ToInt32(grandTot)

                e.Row.Cells(2).Width = "200px"
            End If

            If e.Row.RowType = DataControlRowType.Footer Then
                Dim onewayTotalTxt As Label = DirectCast(e.Row.FindControl("lblOnewayTotalText"), Label)
                onewayTotalTxt.Text = OneWayTotal

                Dim bothwayTotalTxt As Label = DirectCast(e.Row.FindControl("lblBOTHWAYSTotalText"), Label)
                bothwayTotalTxt.Text = bothwayTotal


                Dim lblTotalText As Label = DirectCast(e.Row.FindControl("lblTotalText"), Label)
                lblTotalText.Text = GrandTotal
            End If
        Catch ex As Exception
            Dim err As String = ex.Message
        End Try

    End Sub

    Protected Sub gvStudentCount_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            Dim headerGrid As GridView = DirectCast(sender, GridView)
            '' GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            Dim HeaderRow As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim Cell_Header As TableCell = New TableCell

            Cell_Header.Text = ""
            Cell_Header.HorizontalAlign = HorizontalAlign.Center

            Cell_Header.ColumnSpan = 2

            HeaderRow.Cells.Add(Cell_Header)

            Cell_Header = New TableCell()
            Cell_Header.Text = "BUS Details"
            Cell_Header.HorizontalAlign = HorizontalAlign.Center

            Cell_Header.ColumnSpan = 3
            HeaderRow.Cells.Add(Cell_Header)

            Cell_Header = New TableCell()
            Cell_Header.Text = "No of Students"
            Cell_Header.HorizontalAlign = HorizontalAlign.Center

            Cell_Header.ColumnSpan = 3


            HeaderRow.Cells.Add(Cell_Header)

            Cell_Header = New TableCell()
            Cell_Header.Text = "Charge"
            Cell_Header.HorizontalAlign = HorizontalAlign.Center

            Cell_Header.ColumnSpan = 6


            HeaderRow.Cells.Add(Cell_Header)

            gvStudentCount.Controls(0).Controls.AddAt(0, HeaderRow)
        End If
    End Sub


    Private Sub ParticipantsExcelDownload()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As New DataSet

            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Date", txtDate.Value)



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Bsu_Trip_Student_Bus_Details", params)

            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim dtEXCEL As New DataTable
            dtEXCEL = ds.Tables(0)

            If dtEXCEL.Rows.Count > 0 Then
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT")
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                ws.Cells(0, 0).Value = "Business Unit ID"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 10000
                ws.Columns.Remove(1)

                ws.Cells(0, 0).Value = "Business Unit"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 12000

                ws.Cells(0, 1).Value = "BUS No"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 5000

                ws.Cells(0, 2).Value = "Category"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 5000

                ws.Cells(0, 3).Value = "Capacity"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 5000

                ws.Cells(0, 4).Value = "ONE WAY Stud"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 5000

                ws.Cells(0, 5).Value = "BOTH WAYS Stud"
                ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 6).Value = "TOTAL Stud"
                ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("G").Width = 7000

                ws.Cells(0, 7).Value = "ONE WAY Charge"
                ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("H").Width = 5000

                ws.Cells(0, 8).Value = "BOTH WAYS Charge"
                ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("I").Width = 5000

                ws.Cells(0, 9).Value = "TOTAL Charge"
                ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("J").Width = 5000

                ws.Cells(0, 10).Value = "Concessions"
                ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("I").Width = 5000

                ws.Cells(0, 11).Value = "Adjustments"
                ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("J").Width = 5000

                ws.Cells(0, 12).Value = "Net Charge"
                ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("K").Width = 5000


                Dim stuFilename As String = "BusinessUnitWise_Trip_" & Today.Now().ToString().Replace("/", "-").Replace(":", "-") & ".xlsx"

                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                Dim pathSave As String
                pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()
            End If

        Catch ex As Exception
            Dim erro As String = ex.Message
        End Try
    End Sub
End Class
