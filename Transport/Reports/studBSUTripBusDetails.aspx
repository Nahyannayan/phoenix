﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studBSUTripBusDetails.aspx.vb" Inherits="Transport_Reports_studBSUTripBusDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../Scripts/jQuery-ui-1.10.3.css" />
    <script src="../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {

            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
        }
    );
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Transport Students Bus Report"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%--<table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr class="title">
            <td style="height: 25px" align="left">
                <asp:Literal ID="ltHeader" runat="server" Text="Transport Students Bus Report"></asp:Literal>
            </td>
        </tr>
    </table>--%>
                <table border="0" cellpadding="2" cellspacing="2" width="100%" >
                    <tr>
                        <td align="left"  valign="bottom">
                            <div id="lblError" runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="tableUser" id="tblCategory" cellspacing="2" cellpadding="2" width="100%">
                                <tr align="left">
                                    <td width="20%"><span class="field-label">Trip Date</span><font color="red">*</font>
                                    </td>
                                    <td width="30%">
                                        <input type="text" id="txtDate" class="datepicker" runat="server" />
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="SEARCH" />                                       
                                       <asp:Button ID="btnExcel" runat="server" CssClass="button" Text="Export to excel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                  
                    <tr id="trGridv">
                        <td align="center">
                            <asp:GridView ID="gvStudentCount" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                EmptyDataText="Record not available !!!" HeaderStyle-Height="30" CssClass="table table-bordered table-row"
                                PageSize="20" ShowFooter="true" Width="100%" OnPageIndexChanging="gvStudentCount_PageIndexChanging"
                                OnRowCreated="gvStudentCount_RowCreated" OnRowDataBound="gvStudentCount_RowDatabound" RowStyle-Wrap="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu_ID" runat="server" Text='<%# Bind("SSV_BSU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." >
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BusinessUnit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBusineeUnit" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BusNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBusno" runat="server" Text='<%# Bind("BNO_DESCR") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CAT_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Capacity">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTexts" runat="server" Text="Total"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ONE WAY">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOneWay" runat="server" Text='<%# Bind("ONEWAY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblOnewayTotalText" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BOTH WAYS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbothWay" runat="server" Text='<%# Bind("BOTHWAYS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblBOTHWAYSTotalText" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOTAL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL" runat="server" Text='<%# Bind("TOTAL") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalText" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ONE WAY">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOneWayC" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblOnewayTotalTextC" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BOTH WAYS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbothWayC" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblBOTHWAYSTotalTextC" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOTAL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTALC" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTextC" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CONCESSION">
                                        <ItemTemplate>
                                            <asp:Label ID="lblConsessC" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ADJUSTMENTS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAdj" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NET CHARGE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetC" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  />
                                        <ItemStyle  HorizontalAlign="Left" Wrap="true" />
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem"   />
                                <SelectedRowStyle/>
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <FooterStyle  HorizontalAlign="Left" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="hdnID" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
