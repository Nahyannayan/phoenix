﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_Reports_studBSUTripCount
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim OneWayTotal As Integer = 0
    Dim bothwayTotal As Integer = 0
    Dim GrandTotal As Integer = 0

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Dim ss As ScriptManager = New ScriptManager
        ss.RegisterPostBackControl(btnExcel)

        Try
            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000149") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            End If
        Catch ex As Exception

        End Try



    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindStudentTripCount()
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        ParticipantsExcelDownload()
    End Sub


    Protected Sub gvStudentCount_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvStudentCount.PageIndex = e.NewPageIndex
        BindStudentTripCount()
    End Sub

    Private Sub BindStudentTripCount()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As New DataSet

            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Date", txtDate.Value)



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Bsu_Trip_Student_Numbers", params)
            If ds.Tables(0).Rows.Count > 0 Then

                gvStudentCount.DataSource = ds.Tables(0)
                gvStudentCount.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                '  ds.Tables(0).Rows(0)(2) = True
                gvStudentCount.DataSource = ds.Tables(0)
                Try
                    gvStudentCount.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStudentCount.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStudentCount.Rows(0).Cells.Clear()
                gvStudentCount.Rows(0).Cells.Add(New TableCell)
                gvStudentCount.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudentCount.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudentCount.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub gvStudentCount_RowDatabound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim oneWay As String = DirectCast(e.Row.FindControl("lblOneWay"), Label).Text

                OneWayTotal += Convert.ToInt32(oneWay)

                Dim bothway As String = DirectCast(e.Row.FindControl("lblbothWay"), Label).Text
                bothwayTotal += Convert.ToInt32(bothway)

                Dim grandTot As String = DirectCast(e.Row.FindControl("lblTOTAL"), Label).Text
                GrandTotal += Convert.ToInt32(grandTot)
            End If

            If e.Row.RowType = DataControlRowType.Footer Then
                Dim onewayTotalTxt As Label = DirectCast(e.Row.FindControl("lblOnewayTotalText"), Label)
                onewayTotalTxt.Text = OneWayTotal

                Dim bothwayTotalTxt As Label = DirectCast(e.Row.FindControl("lblBOTHWAYSTotalText"), Label)
                bothwayTotalTxt.Text = bothwayTotal


                Dim lblTotalText As Label = DirectCast(e.Row.FindControl("lblTotalText"), Label)
                lblTotalText.Text = GrandTotal
            End If
        Catch ex As Exception
            Dim err As String = ex.Message
        End Try
       
    End Sub

    Protected Sub gvStudentCount_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            Dim headerGrid As GridView = DirectCast(sender, GridView)
            '' GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            Dim HeaderRow As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim Cell_Header As TableCell = New TableCell
            Cell_Header.Text = ""
            Cell_Header.HorizontalAlign = HorizontalAlign.Center

            Cell_Header.ColumnSpan = 2

            HeaderRow.Cells.Add(Cell_Header)

            Cell_Header = New TableCell()
            Cell_Header.Text = "No. Of Students"
            Cell_Header.HorizontalAlign = HorizontalAlign.Center

            Cell_Header.ColumnSpan = 3

            HeaderRow.Cells.Add(Cell_Header)

            gvStudentCount.Controls(0).Controls.AddAt(0, HeaderRow)
        End If
    End Sub

    Private Sub ParticipantsExcelDownload()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As New DataSet

            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Date", txtDate.Value)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Bsu_Trip_Student_Numbers_Export", params)

            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim dtEXCEL As New DataTable
            Dim dtExcel2 As New DataTable
            dtEXCEL = ds.Tables(0)
            dtExcel2 = ds.Tables(1)

            If dtEXCEL.Rows.Count > 0 Then
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("STS_DATA_EXPORT")
                Dim ws1 As ExcelWorksheet = ef.Worksheets.Add("BBT_DATA_EXPORT")
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True

                'ws.Cells(0, 0).Value = "Business Unit ID"
                'ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("A").Width = 10000
                'ws.Columns("A").Delete()

                ws.Cells(0, 0).Value = "Business Unit"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 12000

                ws.Cells(0, 1).Value = "ONE WAY"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 5000

                ws.Cells(0, 2).Value = "BOTH WAYS"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 7000

                ws.Cells(0, 3).Value = "TOTAL"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 7000
                Dim Sum As Integer = 0
                For Each dRow As DataRow In ds.Tables(0).Rows
                    Sum += Convert.ToInt32(dRow("TOTAL"))
                Next
                ws.Cells(ds.Tables(0).Rows.Count + 2, 2).Value = "GRAND TOTAL"
                ws.Cells(ds.Tables(0).Rows.Count + 2, 2).Style.Font.Color = Drawing.Color.White

                ws.Cells(ds.Tables(0).Rows.Count + 2, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Cells(ds.Tables(0).Rows.Count + 2, 3).Value = Sum
                'ws.Cells(0, 5).Value = "PROVIDER"
                'ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)

                'ws.Columns("E").Delete()
                'ws.Columns("F").Delete()
                'ws.Columns("G").Delete()
                'ws.Columns("H").Delete()
                'ws.Columns("I").Delete()
                'ws.Columns("J").Delete()
                'ws.Columns("K").Delete()
                ''SETTING COLUMNS FOR bbt SHEET

                If dtExcel2.Rows.Count > 0 Then
                    ws1.InsertDataTable(dtExcel2, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    ' ws1.HeadersFooters.AlignWithMargins = True
                    'ws1.Cells(0, 0).Value = "Business Unit ID"
                    'ws1.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                    'ws1.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    'ws1.Columns("A").Width = 10000
                    'ws1.Columns("A").Delete()

                    ws1.Cells(0, 0).Value = "Business Unit"
                    ws1.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                    ws1.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws1.Columns("A").Width = 12000

                    ws1.Cells(0, 1).Value = "ONE WAY"
                    ws1.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                    ws1.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws1.Columns("B").Width = 5000

                    ws1.Cells(0, 2).Value = "BOTH WAYS"
                    ws1.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                    ws1.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws1.Columns("C").Width = 7000

                    ws1.Cells(0, 3).Value = "TOTAL"
                    ws1.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                    ws1.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)

                    ws1.Columns("D").Width = 7000

                    Dim Sum1 As Integer = 0
                    For Each dRow As DataRow In ds.Tables(1).Rows
                        Sum1 += Convert.ToInt32(dRow("TOTAL"))
                    Next

                    ws1.Cells(ds.Tables(1).Rows.Count + 2, 2).Value = "GRAND TOTAL"
                    ws1.Cells(ds.Tables(1).Rows.Count + 2, 2).Style.Font.Color = Drawing.Color.White
                    'ws1.Cells(ds.Tables(1).Rows.Count + 2, 2).Style.Font.Weight = 2
                    ws1.Cells(ds.Tables(1).Rows.Count + 2, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws1.Cells(ds.Tables(1).Rows.Count + 2, 3).Value = Sum1

                    'ws1.Cells(0, 5).Value = "PROVIDER"
                    'ws1.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                    'ws1.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)


                    'ws1.Columns("E").Delete()
                    'ws1.Columns("F").Delete()
                    'ws1.Columns("G").Delete()
                    'ws1.Columns("H").Delete()
                End If
                ''ENDS HERE bbt SHEET


                Dim stuFilename As String = "BusinessUnitWise_Trip_" & Today.Now().ToString().Replace("/", "-").Replace(":", "-") & ".xlsx"

                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                Dim pathSave As String
                pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()
            End If

        Catch ex As Exception
            Dim erro As String = ex.Message
            UtilityObj.Errorlog(ex.Message, "excel in trip")
            UtilityObj.Errorlog("excel in trip", ex.Message)
        End Try
    End Sub
End Class
