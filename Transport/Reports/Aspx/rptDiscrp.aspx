<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDiscrp.aspx.vb" Inherits="Students_Reports_ASPX_rptDiscrp" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Discrepancy Analysis
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="Table1" runat="server" align="center" style="width: 100%">

                    <tr>
                        <td align="center"></td>
                    </tr>
                    <tr align="left">
                        <td style="width: 557px">
                            <table align="center" 
                                style="width: 100%">

                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Select Business Unit</span></td>
                                    <td align="left"   style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server">
                                        </asp:DropDownList></td>
                                  
                                    <td align="left"  width="20%">Select Report</td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlRpt" runat="server" Width="326px">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left"  colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

