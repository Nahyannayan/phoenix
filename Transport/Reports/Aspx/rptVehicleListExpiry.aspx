<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptVehicleListExpiry.aspx.vb" Inherits="Transport_Reports_Aspx_rptVehicleListExpiry" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);

                __doPostBack("", "");


            }
        }


        function OnTreeClick1(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }


        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }


        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;
            if (parentNodeTable) {
                var checkUncheckSwitch;
                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }
                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) {
                    //check if the child node is an element node
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function UnCheckAll() {
            var oTree = document.getElementById("<%=tvBsu.ClientId %>");
            childChkBoxes = oTree.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = false;
            }

            return true;
        }

        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture = "../../../Accounts/calendar.aspx?nofuture=yes";
            if (val == 0) {
                result = window.showModalDialog(nofuture, "", sFeatures)

            }
            else {
                result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures)
            }
            if (result == '' || result == undefined) {
                return false;
            }


            if (val == 1) {
                document.getElementById('<%=txtFrom.ClientID %>').value = result;
            }
            else if (val == 2) {
                document.getElementById('<%=txtTo.ClientID %>').value = result;
        }




}







    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblRptName" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                &nbsp


    <table id="tbl_ShowScreen" runat="server" align="center" border="0" bordercolor="#1b80b6"
        cellpadding="5"
        cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
                <table id="Table1" runat="server" align="center"
                    cellpadding="5" cellspacing="0" style="width: 100%">

                    <tr >
                        <td align="left"  valign="middle">
                            <%--<font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">--%>
                            <%--</span></font>--%>

                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="width: 100%" colspan="18">

                            <table id="tblTrip" runat="server" align="center"
                                cellpadding="12" cellspacing="0" width="100%">
                                <tr>
                                    <td></td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td >
                                        <table id="tblGroup" runat="server" width="100%">
                                            <tr class="title-bg">
                                                <td align="left"  valign="middle">                                                    
                                        Business Unit</td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                   
                                                   <%-- <asp:TreeView ID="tvBsu" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                        ExpandDepth="1" Height="206px" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                                        ShowCheckBoxes="All" Style="overflow: auto; text-align: left" Width="203px" OnTreeNodeCheckChanged="tvBsu_TreeNodeCheckChanged" PopulateNodesFromClient="False">
                                                        <ParentNodeStyle Font-Bold="False" />
                                                        <HoverNodeStyle Font-Underline="True" />
                                                        <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px"
                                                            Font-Underline="False" HorizontalPadding="3px" VerticalPadding="1px" />
                                                        <NodeStyle HorizontalPadding="5px"
                                                            NodeSpacing="1px" VerticalPadding="2px" />
                                                    </asp:TreeView>--%>
                                                    <div class="checkbox-list">
                                                     <asp:TreeView ID="tvBsu" runat="server" 
                                                        ExpandDepth="1"  MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                                       xOnTreeNodeCheckChanged="tvBsu_TreeNodeCheckChanged" PopulateNodesFromClient="False">
                                                        <%--<ParentNodeStyle Font-Bold="False" />--%>
                                                        <%--<HoverNodeStyle Font-Underline="True" />--%>
                                                        <%--<SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px"--%>
                                                            <%--Font-Underline="False" HorizontalPadding="3px" VerticalPadding="1px" />--%>
                                                        <%--<NodeStyle HorizontalPadding="5px"--%>
                                                            <%--NodeSpacing="1px" VerticalPadding="2px" />--%>
                                                    </asp:TreeView></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td ></td>
                                    <td >
                                        <table id="Table7" runat="server" width="100%" >
                                            <tr class="title-bg">
                                                <td align="left" valign="middle">
                                                    
                                        <asp:Label id="lblCategory" runat="server" Text="Category" ></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">

                                                   <%-- <asp:TreeView ID="tvCategory" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                        ExpandDepth="1" Height="189px" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick1(event);"
                                                        ShowCheckBoxes="All" Style="overflow: auto; text-align: left" Width="202px" PopulateNodesFromClient="False">
                                                        <ParentNodeStyle Font-Bold="False" />
                                                        <HoverNodeStyle Font-Underline="True" />
                                                        <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px"
                                                            Font-Underline="False" HorizontalPadding="3px" VerticalPadding="1px" />
                                                        <NodeStyle  HorizontalPadding="5px"
                                                            NodeSpacing="1px" VerticalPadding="2px" />
                                                    </asp:TreeView>--%>
                                                      <div class="checkbox-list">
                                                          <asp:TreeView ID="tvCategory" runat="server"
                                                              ExpandDepth="1" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick1(event);"
                                                              ShowCheckBoxes="All" PopulateNodesFromClient="False">
                                                              <%-- <ParentNodeStyle Font-Bold="False" />
                                                        <HoverNodeStyle Font-Underline="True" />
                                                        <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px"
                                                            Font-Underline="False" HorizontalPadding="3px" VerticalPadding="1px" />
                                                        <NodeStyle  HorizontalPadding="5px"
                                                            NodeSpacing="1px" VerticalPadding="2px" />--%>
                                                          </asp:TreeView>
                                                          </div>
                                                </td>

                                            </tr>
                                        </table>
                                    </td>
                                    <td >
                                        <table id="Table3" runat="server" width="100%" >
                                            <tr class="title-bg">
                                                <td align="left"  valign="middle">
                                                    
                                        <asp:CheckBox ID="chkVehicle" runat="server" AutoPostBack="True" Text="Vehicle" /></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <div class="checkbox-list">
                                                    <asp:CheckBoxList ID="lstVehicle" runat="server" AutoPostBack="True" 
                                                       >
                                                    </asp:CheckBoxList></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <table id="Table6" runat="server" align="center" 
                                            cellpadding="5" cellspacing="0" style="width: 100%">

                                            <tr>

                                                <td align="left" width="20%">
                                                    <asp:Label ID="lblDttext" runat="server" Text="From" CssClass="field-label"></asp:Label></td>
                                                <%--<td align="left" style="width: 11px;">:</td>--%>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom"
                                                            Display="Dynamic" ErrorMessage="Enter the Trip Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtFrom"
                                                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date"
                                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                    <asp:RequiredFieldValidator ID="rfFrom" runat="server" ErrorMessage="Please enter the field trip date from" ControlToValidate="txtFrom" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                                                </td>



                                                <td align="left" width="20%">
                                                    <asp:Label ID="Label3" runat="server" Text="To" CssClass="field-label" ></asp:Label></td>
                                                <%--<td align="left" style="width: 11px;">:</td>--%>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtTo" runat="server" AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                                        PopupButtonID="imgTo" TargetControlID="txtTo">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTo"
                                                            Display="Dynamic" ErrorMessage="Enter the Trip Date To in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtTo"
                                                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date To entered is not a valid date"
                                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                    <asp:RequiredFieldValidator ID="rfTo" runat="server" ErrorMessage="Please enter the field trip date to" ControlToValidate="txtTo" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>



                                <tr>
                                    <td colspan="4" ></td>
                                </tr>

                                <tr>
                                    <td align="center" ></td>
                                    <td align="center" colspan="3" ></td>

                                </tr>


                                <tr>
                                    <td ></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td >
                                        <asp:RadioButton ID="RdRegistration" runat="server" Text="Registration" GroupName="G2" CssClass="field-label"></asp:RadioButton></td>
                                    <td colspan="3">
                                        <asp:RadioButton ID="RdInsurance" runat="server" Text="Insurance" GroupName="G2" CssClass="field-label"></asp:RadioButton></td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                    <tr>
                        <td valign="bottom" align="center">

                            <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" ></asp:Button>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="hfENDDT" runat="server" />

                <asp:HiddenField ID="hfACY_ID" runat="server" />


                <input id="lstValues" name="lstValues" runat="server" type="hidden"  />


            </td>
        </tr>

    </table>
            </div>
        </div>
    </div>


</asp:Content>

