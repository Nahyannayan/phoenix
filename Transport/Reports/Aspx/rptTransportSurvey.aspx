<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTransportSurvey.aspx.vb" Inherits="Students_Reports_ASPX_rptAdmissionDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Select  School"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" style="width: 100%;">
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">School</span></td>
                                    <td width="60%" colspan="2" align="left">
                                        <div class="checkbox-list">
                                            <asp:TreeView ID="tvBusinessunit" runat="server" onclick="client_OnTreeNodeChecked();"
                                                ShowCheckBoxes="All">
                                                <NodeStyle CssClass="treenode" />
                                            </asp:TreeView>
                                            <asp:DropDownList ID="ddlBSU_ID" runat="server"></asp:DropDownList><br />
                                        </div>
                                    </td>
                                    <td width="20%"></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Continue</span></td>
                                    <td width="30%" align="left">
                                        <asp:Panel ID="Panel1" runat="server">
                                            <asp:RadioButton CssClass="field-label" ID="RadYes" runat="server" GroupName="Tran" Text="Yes" Checked="True"></asp:RadioButton>
                                            <asp:RadioButton CssClass="field-label" ID="radNo" runat="server" GroupName="Tran" Text="NO"></asp:RadioButton>
                                        </asp:Panel>
                                    </td>
                                    <td width="30%"></td>
                                    <td width="20%"></td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

