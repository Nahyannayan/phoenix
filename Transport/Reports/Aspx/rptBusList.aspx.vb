Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
Imports System.Collections
Partial Class Transport_Reports_Aspx_rptBusList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100480" And ViewState("MainMnu_code") <> "T100510" And ViewState("MainMnu_code") <> "T100482") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    If ViewState("MainMnu_code") <> "T100480" Then
                        rdBoth.Visible = False
                    End If
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    BindAcademicYear()
                    BindBusNo()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)


    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            hfbDownload.Value = 0
            CallReport()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_ID,ACY_DESCR FROM ACADEMICYEAR_M WHERE ACY_ID>=((SELECT MIN(ACD_ACY_ID) FROM ACADEMICYEAR_D WHERE ACD_BSU_ID=" + ddlBsu.SelectedValue.ToString _
                                 & " AND ACD_CURRENT=1)-1)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlAcademicYear.Items.Insert(0, li)

        If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACY_ID")) Is Nothing Then
            ddlAcademicYear.Items.FindByValue(Session("Current_ACY_ID")).Selected = True
        End If
        'Dim currentYear As Integer

        'str_query = "SELECT ACD_ACY_ID FROM ACADEMICYEAR_D AS A INNER JOIN BUSINESSUNIT_M AS B ON" _
        '           & " A.ACD_BSU_ID=B.BSU_ID AND A.ACD_CLM_ID=B.BSU_CLM_ID WHERE ACD_CURRENT=1 " _
        '           & " AND BSU_ID=" + ddlBsu.SelectedValue.ToString

        'currentYear = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        'ddlAcademicYear.Items.FindByValue(currentYear).Selected = True
    End Sub

    Sub BindBusNo()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BNO_ID,BNO_DESCR FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + ddlBsu.SelectedValue.ToString + "'" _
                                 & " ORDER BY BNO_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlBusNo.Items.Insert(0, li)

    End Sub


    Sub CallReport()

        Dim param As New Hashtable
        param.Add("@ACY_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@BNO_ID", ddlBusNo.SelectedValue.ToString)
        param.Add("@BSU_ID", ddlBsu.SelectedValue.ToString)
        If rdOnward.Checked = True Then
            param.Add("@JOURNEY", "Onward")
        ElseIf rdReturn.Checked = True Then
            param.Add("@JOURNEY", "Return")
        End If
        param.Add("loginbsu", Session("sbsuid"))

        param.Add("bsu", ddlBsu.SelectedItem.Text)
        param.Add("academicyear", ddlAcademicYear.SelectedItem.Text)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        If chkFuture.Checked = True Then
            param.Add("@bFUTURE", "true")
        Else
            param.Add("@bFUTURE", "false")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If ViewState("MainMnu_code") = "T100480" Then
                If rdBoth.Checked = True Then
                    If chkStaff.Checked = True Then
                        .reportPath = Server.MapPath("../Rpt/rptBusList_ALLWithStaff.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/rptBusList_ALL.rpt")
                    End If
                Else
                    If chkStaff.Checked = True Then
                        .reportPath = Server.MapPath("../Rpt/rptBusListWithStaff.rpt")
                    Else
                        .reportPath = Server.MapPath("../Rpt/rptBusList.rpt")
                    End If
                End If
            ElseIf ViewState("MainMnu_code") = "T100510" Then
                .reportPath = Server.MapPath("../Rpt/rptBuslistContacts.rpt")
            End If
        End With
       
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


#End Region

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        Try
            BindAcademicYear()
            BindBusNo()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub
End Class
