<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTransSURV_Student.aspx.vb" Inherits="Students_Reports_ASPX_rptAdmissionDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../../../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>


    <script language="javascript" type="text/javascript">
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkList") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Transport Request"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td align="center">
                            <table align="center" style="width: 100%;">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Curriculum</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCurr" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurr_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td width="20%" align="left"><span class="field-label">Academic Year</span></td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Shift</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlShift" runat="server" OnSelectedIndexChanged="ddlShift_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Section</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Transport Avail Students</span></td>
                                    <td align="left">
                                        <asp:RadioButton CssClass="field-label" ID="rbTransYes" runat="server" GroupName="Trans" Text="Yes" AutoPostBack="True" OnCheckedChanged="rbTransYes_CheckedChanged"></asp:RadioButton>&nbsp;
                                        <asp:RadioButton CssClass="field-label" ID="rbTransNo" runat="server" GroupName="Trans" Text="No" AutoPostBack="True" OnCheckedChanged="rbTransNo_CheckedChanged"></asp:RadioButton></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" OnClick="btnSearch_Click" />
                                        &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Print" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvTransport" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" DataKeyNames="STU_ID" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            OnPageIndexChanged="gvTransport_PageIndexChanged" PageSize="50" Width="100%" OnPageIndexChanging="gvTransport_PageIndexChanging">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSelectH" runat="server" Text="Select" CssClass="gridheader_text" __designer:wfdid="w64"></asp:Label>
                                                        <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server" __designer:wfdid="w65"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:CheckBox ID="chkList" runat="server" __designer:wfdid="w21"></asp:CheckBox>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="FeeID">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblFEE_IDH" runat="server" Text="Fee Id" CssClass="gridheader_text" __designer:wfdid="w14"></asp:Label><br />
                                                        <asp:TextBox ID="txtStu_Fee_Id" runat="server" Width="75%" __designer:wfdid="w15"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchSTU_FEE_ID" OnClick="btnSearchSTU_FEE_ID_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w16"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_FEE_ID" runat="server" Text='<%# Bind("STU_FEE_ID") %>' __designer:wfdid="w13"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="FileNo">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblfilenoH" runat="server" Text="File No" CssClass="gridheader_text" __designer:wfdid="w18"></asp:Label><br />
                                                        <asp:TextBox ID="txtfileno" runat="server" Width="75%" __designer:wfdid="w19"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchfileNo" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w20"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFILENO" runat="server" Text='<%# Bind("FileNo") %>' __designer:wfdid="w17"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStudentNameH" runat="server" Text="Student Name" CssClass="gridheader_text" __designer:wfdid="w5"></asp:Label><br />
                                                        <asp:TextBox ID="txtSNAME" runat="server" Width="75%" __designer:wfdid="w6"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchSNAME" OnClick="btnSearchSNAME_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w7"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSName" runat="server" Text='<%# Bind("Sname") %>' __designer:wfdid="w4"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade &amp; Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("Grade") %>' __designer:wfdid="w50"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pickup Bus No">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                                        <asp:Label ID="lblSTU_PICKUP_BUSNO" runat="server" Text='<%# Bind("STU_PICKUP_BUSNO") %>' __designer:wfdid="w51"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pickup Point">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPNT_DESCRIPTION" runat="server" Text='<%# Bind("PNT_DESCRIPTION") %>' __designer:wfdid="w54"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trip Descr">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTRP_DESCR" runat="server" Text='<%# Bind("TRP_DESCR") %>' __designer:wfdid="w22"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stu_id" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_ID" runat="server" Text='<%# Bind("STU_ID") %>' __designer:wfdid="w62"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnPrintSelected" runat="server" CssClass="button" Text="Print Selected Records" OnClick="btnPrintSelected_Click" /></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />&nbsp;
                
                <asp:HiddenField ID="hiddenENQIDs" runat="server" />


                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

