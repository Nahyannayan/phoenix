Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
Imports System.Collections
Partial Class Transport_Reports_Aspx_rptBusList_Export
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100485" And ViewState("MainMnu_code") <> "T100510") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    BindAcademicYear()
                    BindBusNo()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    
    Protected Sub btnGenerateReport2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport2.Click
        Try


            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim param(6) As SqlClient.SqlParameter
            param(1) = New SqlClient.SqlParameter("@ACY_ID", ddlAcademicYear.SelectedValue.ToString)
            param(2) = New SqlClient.SqlParameter("@BNO_ID", ddlBusNo.SelectedValue.ToString)
            param(3) = New SqlClient.SqlParameter("@BSU_ID", ddlBsu.SelectedValue.ToString)
            If rdOnward.Checked = True Then
                param(4) = New SqlClient.SqlParameter("@JOURNEY", "Onward")
            Else
                param(4) = New SqlClient.SqlParameter("@JOURNEY", "Return")
            End If

            If chkFuture.Checked = True Then
                param(5) = New SqlClient.SqlParameter("@bFUTURE", "true")
            Else
                param(5) = New SqlClient.SqlParameter("@bFUTURE", "false")
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRANSPORT.rptBUSLIST_Export", param)
            Dim dtEXCEL As New DataTable
            dtEXCEL = ds.Tables(0)

            If dtEXCEL.Rows.Count > 0 Then
                Dim str_err As String = String.Empty
                Dim errorMessage As String = String.Empty
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As New GemBox.Spreadsheet.ExcelFile()
                Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

                'ws.HeadersFooters.AlignWithMargins = True
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_BusList.xlsx")

                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                Dim pathSave As String
                pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()
            Else
                lblError.Text = "No Records To display with this filter condition....!!!"
                lblError.Focus()
            End If
        Catch ex As Exception

        End Try

    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_ID,ACY_DESCR FROM ACADEMICYEAR_M WHERE ACY_ID>=((SELECT MIN(ACD_ACY_ID) FROM ACADEMICYEAR_D WHERE ACD_BSU_ID=" + ddlBsu.SelectedValue.ToString _
                                 & " AND ACD_CURRENT=1)-1)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlAcademicYear.Items.Insert(0, li)

        If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACY_ID")) Is Nothing Then
            ddlAcademicYear.Items.FindByValue(Session("Current_ACY_ID")).Selected = True
        End If
        'Dim currentYear As Integer

        'str_query = "SELECT ACD_ACY_ID FROM ACADEMICYEAR_D AS A INNER JOIN BUSINESSUNIT_M AS B ON" _
        '           & " A.ACD_BSU_ID=B.BSU_ID AND A.ACD_CLM_ID=B.BSU_CLM_ID WHERE ACD_CURRENT=1 " _
        '           & " AND BSU_ID=" + ddlBsu.SelectedValue.ToString

        'currentYear = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        'ddlAcademicYear.Items.FindByValue(currentYear).Selected = True
    End Sub

    Sub BindBusNo()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BNO_ID,BNO_DESCR FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + ddlBsu.SelectedValue.ToString + "'" _
        & " ORDER BY BNO_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlBusNo.Items.Insert(0, li)

    End Sub


    

#End Region

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        Try
            BindAcademicYear()
            BindBusNo()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


End Class
