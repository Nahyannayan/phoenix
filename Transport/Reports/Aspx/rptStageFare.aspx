<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStageFare.aspx.vb" Inherits="Transport_Reports_Aspx_rptStageFare" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Stage Fare
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center"
                    cellpadding="5" width="100%"
                    cellspacing="0">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>

                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="tblRate" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">

                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="16" style="height: 16px; width: 734px;" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
             Stage Fare</span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">SelectBusinessUnit </span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Select AcademicYear</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Curriculum</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td valign="bottom" colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" ValidationGroup="groupM1"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                            <input id="lstValues" name="lstValues" runat="server" type="hidden" />
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

