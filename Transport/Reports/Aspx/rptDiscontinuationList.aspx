<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDiscontinuationList.aspx.vb" Inherits="Students_Reports_ASPX_rptDiscontinuationList" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }



    </script>
    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                      break;
              }
          }
          return false;
      }

      function FnCheckUnselected() {

          var ControlRef = document.getElementById('<%= lstBsu.ClientID%>');



            var CheckBoxListArray = ControlRef.getElementsByTagName('input');
            var spanArray = ControlRef.getElementsByTagName('span');
            var checkedValues = '';
            var nIndex = 0;
            var sValue = '';

            for (var i = 0; i < CheckBoxListArray.length; i++) {
                var checkBoxRef = CheckBoxListArray[i];

                if (checkBoxRef.checked == false) {
                    document.getElementById('<%= chkSelectBsu.ClientID%>').checked = false;
                  }
              }
          }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Select Date Range"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" style="width: 100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Business Unit</span></td>
                                    <td align="left" style="text-align: left; width=30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr id="trBSU" visible="false" runat="server">
                                    <td align="left" class="matters">
                                        <span class="field-label">Service Business Unit</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <div class="checkbox-list">

                                            <asp:CheckBox ID="chkSelectBsu" onclick="javascript:fnSelectAll(this);" runat="server"
                                                Text="Select All" AutoPostBack="True" /><br />
                                            <asp:CheckBoxList ID="lstBsu" onclick="javascript:FnCheckUnselected();" runat="server" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">From Date</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgFrom" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                            runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFromDate"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender5" CssClass="MyCalendar" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgFrom"
                                            TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>

                                        <td align="left">
                                            <span class="field-label">To Date</span></td>
                                        <td align="left" style="text-align: left">
                                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImgToDate" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator
                                                ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDate"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="MyCalendar" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgToDate"
                                                TargetControlID="txtToDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                                PopupButtonID="txtToDate" TargetControlID="txtToDate">
                                            </ajaxToolkit:CalendarExtender>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Discontinuation Type</span></td>
                                    <td align="left">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="lstType" runat="server">
                                                <asp:ListItem Value="T">Temporary Discontinuation</asp:ListItem>
                                                <asp:ListItem Value="S">Suspended for a Week</asp:ListItem>
                                                <asp:ListItem Value="P">Permanent Discontinuation</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Status</span></td>
                        <td align="left" style="text-align: left">
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="lstStatus" runat="server">
                                    <asp:ListItem Value="Approved">Approved</asp:ListItem>
                                    <asp:ListItem Value="Rejected">Rejected</asp:ListItem>
                                    <asp:ListItem Value="Pending">Approval Pending</asp:ListItem>
                                </asp:CheckBoxList>
                            </div>
                        </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Category</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:RadioButtonList ID="rbCategory" runat="server" BorderStyle="Solid" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="All" Selected="True"><span class="field-label">All</span></asp:ListItem>
                                            <asp:ListItem Value="TC"><span class="field-label">TC</span></asp:ListItem>
                                            <asp:ListItem Value="SO"><span class="field-label">SO</span></asp:ListItem>
                                            <asp:ListItem Value="OT"><span class="field-label">OT</span></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">&nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

