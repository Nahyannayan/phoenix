Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Partial Class Students_Reports_ASPX_rptAdmissionDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100470") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    CURRICULUM_BSU_4_Student()
                    h_Selected_menu_1.Value = "LI__../../../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../../../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../../../Images/operations/like.gif"
                    rbTransYes.Checked = True
                    gvTransport.Visible = False
                    btnPrintSelected.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTransport.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvTransport.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTransport.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvTransport.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvTransport.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvTransport.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub btnSearchSTU_FEE_ID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ACD_ID As String = String.Empty
            Dim GRD_ID As String = String.Empty
            Dim SHF_ID As String = String.Empty
            Dim SCT_ID As String = String.Empty
          

            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = " AND A.STU_ACD_ID=''"
            Else
                If ddlAcademicYear.SelectedValue = "0" Then
                    ACD_ID = " AND A.STU_ACD_ID<>''"
                Else
                    ACD_ID = " AND A.STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "'"
                End If

            End If

            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = " AND A.STU_GRD_ID=''"
            Else
                If ddlGrade.SelectedValue = "0" Then
                    GRD_ID = " AND A.STU_GRD_ID<>''"
                Else
                    GRD_ID = " AND A.STU_GRD_ID='" & ddlGrade.SelectedValue & "'"
                End If
            End If
            If ddlSection.SelectedIndex = -1 Then
                SCT_ID = " AND A.STU_SCT_ID=''"
            Else
                If ddlSection.SelectedValue = "0" Then
                    SCT_ID = " AND A.STU_SCT_ID<>''"
                Else
                    SCT_ID = " AND A.STU_SCT_ID='" & ddlSection.SelectedValue & "'"
                End If
            End If


            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = " AND A.STU_SHF_ID=''"
            Else
                If ddlShift.SelectedValue = "0" Then
                    SHF_ID = " AND A.STU_SHF_ID<>''"
                Else
                    SHF_ID = " AND A.STU_SHF_ID='" & ddlShift.SelectedValue & "'"
                End If

            End If

            Dim str_Sql As String = ""
            Dim str_filter_Stu_Fee_Id As String = String.Empty
            Dim str_filter_SNAME As String = String.Empty
            Dim str_filter_fileno As String = String.Empty

            Dim ds As New DataSet
            If rbTransYes.Checked = True Then
                str_Sql = " select distinct STU_ID,STU_NO,STU_BSU_ID,STU_GRM_ID,STU_ACD_ID,STU_GRD_ID,STU_SCT_ID,STU_SHF_ID,STU_FEE_ID,STU_CURRSTATUS,Sname," & _
                " STU_PICKUP_TRP_ID, STU_PICKUP_BUSNO, STU_PICKUP, STU_DROPOFF_TRP_ID, STU_DROPOFF_BUSNO, STU_DROPOFF, STU_LEAVEDATE, STU_SBL_ID_PICKUP " & _
                " STU_SBL_ID_DROPOFF, GRADE, SHF_DESCR, TRP_DESCR, PNT_DESCRIPTION, FILENO " & _
                " from (SELECT STUDENT_M.STU_ID, STUDENT_M.STU_NO, STUDENT_M.STU_BSU_ID, STUDENT_M.STU_GRM_ID, STUDENT_M.STU_ACD_ID, " & _
                      " STUDENT_M.STU_GRD_ID, STUDENT_M.STU_SCT_ID, STUDENT_M.STU_SHF_ID, STUDENT_M.STU_FEE_ID, STUDENT_M.STU_CURRSTATUS, " & _
                      " RTRIM(LTRIM(ISNULL(STUDENT_M.STU_FIRSTNAME, ''))) + ' ' + ISNULL(STUDENT_M.STU_MIDNAME, ' ') + ' ' + ISNULL(STUDENT_M.STU_LASTNAME, " & _
                " '') AS Sname, STUDENT_M.STU_PICKUP_TRP_ID, TRANSPORT.VV_BUSES.BNO_descr as STU_PICKUP_BUSNO, STUDENT_M.STU_PICKUP, " & _
                     "  STUDENT_M.STU_DROPOFF_TRP_ID, STUDENT_M.STU_DROPOFF_BUSNO, STUDENT_M.STU_DROPOFF, STUDENT_M.STU_LEAVEDATE, " & _
                     "  STUDENT_M.STU_SBL_ID_PICKUP, STUDENT_M.STU_SBL_ID_DROPOFF," & _
                      " VV_GRADE_BSU_M.GRM_DISPLAY + ' ' + VV_SECTION_M.SCT_DESCR AS GRADE, VV_SHIFTS_M.SHF_DESCR, " & _
                      " TRANSPORT.PICKUPPOINTS_M.PNT_DESCRIPTION, isnull(STUDENT_M.STU_FILENO,'') AS FILENO, TRANSPORT.VV_BUSES.TRP_DESCR " & _
                      " FROM  STUDENT_M INNER JOIN  VV_SECTION_M ON STUDENT_M.STU_ACD_ID = VV_SECTION_M.SCT_ACD_ID AND STUDENT_M.STU_SCT_ID = VV_SECTION_M.SCT_ID AND " & _
                      " STUDENT_M.STU_GRD_ID = VV_SECTION_M.SCT_GRD_ID INNER JOIN " & _
                      " VV_GRADE_BSU_M ON STUDENT_M.STU_ACD_ID = VV_GRADE_BSU_M.GRM_ACD_ID AND " & _
                      " STUDENT_M.STU_GRD_ID = VV_GRADE_BSU_M.GRM_GRD_ID INNER JOIN " & _
                      " VV_SHIFTS_M ON STUDENT_M.STU_SHF_ID = VV_SHIFTS_M.SHF_ID AND STUDENT_M.STU_BSU_ID = VV_SHIFTS_M.SHF_BSU_ID INNER JOIN " & _
                      " TRANSPORT.PICKUPPOINTS_M ON STUDENT_M.STU_PICKUP = TRANSPORT.PICKUPPOINTS_M.PNT_ID INNER JOIN " & _
                      " OASIS.dbo.STUDENT_SERVICES_D ON STUDENT_M.STU_ID = OASIS.dbo.STUDENT_SERVICES_D.SSV_STU_ID " & _
                      " INNER JOIN " & _
                     "  TRANSPORT.VV_BUSES ON STUDENT_M.STU_PICKUP_TRP_ID = TRANSPORT.VV_BUSES.TRP_ID " & _
" WHERE     (OASIS.dbo.STUDENT_SERVICES_D.SSV_TODATE IS NULL) AND (OASIS.dbo.STUDENT_SERVICES_D.SSV_SVC_ID = 1)" & _
"  AND (OASIS.dbo.STUDENT_SERVICES_D.SSV_bACTIVE=1) AND (CONVERT(DATETIME,STUDENT_M.STU_LEAVEDATE) IS NULL OR " & _
" CONVERT(DATETIME,STUDENT_M.STU_LEAVEDATE)>getdate()) AND STUDENT_M.STU_CURRSTATUS<>'CN' AND (ISNULL(STUDENT_M.STU_PICKUP, 0) <> 0) " & _
" )A  WHERE  A.STU_ID<>'' "


            ElseIf rbTransNo.Checked = True Then
                str_Sql = "  select distinct STU_ID,STU_NO,STU_BSU_ID,STU_GRM_ID,STU_ACD_ID,STU_GRD_ID,STU_SCT_ID,STU_SHF_ID,STU_FEE_ID,STU_CURRSTATUS,Sname," & _
                " STU_PICKUP_TRP_ID, STU_PICKUP_BUSNO, STU_PICKUP, STU_DROPOFF_TRP_ID, STU_DROPOFF_BUSNO, STU_DROPOFF, STU_LEAVEDATE, STU_SBL_ID_PICKUP " & _
" STU_SBL_ID_DROPOFF,GRADE,SHF_DESCR,TRP_DESCR,PNT_DESCRIPTION,FILENO from (SELECT  STUDENT_M.STU_ID, STUDENT_M.STU_NO, STUDENT_M.STU_BSU_ID, STUDENT_M.STU_GRM_ID, STUDENT_M.STU_ACD_ID, " & _
 " STUDENT_M.STU_GRD_ID, STUDENT_M.STU_SCT_ID, STUDENT_M.STU_SHF_ID, STUDENT_M.STU_FEE_ID, STUDENT_M.STU_CURRSTATUS, " & _
  " rtrim(ltrim(ISNULL(STUDENT_M.STU_FIRSTNAME, ''))) + ' ' + ISNULL(STUDENT_M.STU_MIDNAME, ' ') + ' ' + ISNULL(STUDENT_M.STU_LASTNAME, '') AS Sname, " & _
                    "   STUDENT_M.STU_PICKUP_TRP_ID, STUDENT_M.STU_PICKUP_BUSNO, STUDENT_M.STU_PICKUP, STUDENT_M.STU_DROPOFF_TRP_ID," & _
                     "  STUDENT_M.STU_DROPOFF_BUSNO, STUDENT_M.STU_DROPOFF, STUDENT_M.STU_LEAVEDATE, STUDENT_M.STU_SBL_ID_PICKUP, " & _
                      "  STUDENT_M.STU_SBL_ID_DROPOFF,  VV_GRADE_BSU_M.GRM_DISPLAY +' ' + VV_SECTION_M.SCT_DESCR as GRADE, VV_SHIFTS_M.SHF_DESCR,'' AS TRP_DESCR,'' AS PNT_DESCRIPTION ,isnull(STUDENT_M.STU_FILENO,'') AS FILENO " & _
"  FROM  STUDENT_M INNER JOIN   VV_SECTION_M ON STUDENT_M.STU_ACD_ID = VV_SECTION_M.SCT_ACD_ID AND STUDENT_M.STU_SCT_ID = VV_SECTION_M.SCT_ID AND " & _
                      "  STUDENT_M.STU_GRD_ID = VV_SECTION_M.SCT_GRD_ID INNER JOIN   VV_GRADE_BSU_M ON STUDENT_M.STU_ACD_ID = VV_GRADE_BSU_M.GRM_ACD_ID AND " & _
                      "  STUDENT_M.STU_GRD_ID = VV_GRADE_BSU_M.GRM_GRD_ID INNER JOIN   VV_SHIFTS_M ON STUDENT_M.STU_SHF_ID = VV_SHIFTS_M.SHF_ID AND STUDENT_M.STU_BSU_ID = VV_SHIFTS_M.SHF_BSU_ID " & _
"  )A WHERE     (ISNULL(A.STU_PICKUP, 0) = 0)  AND (CONVERT(DATETIME,A.STU_LEAVEDATE) IS NULL OR CONVERT(DATETIME,A.STU_LEAVEDATE)>getdate())" & _
"  AND A.STU_CURRSTATUS<>'CN'"


            End If

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_Stu_Fee_Id As String = String.Empty

            Dim str_SNAME As String = String.Empty
            Dim str_fileno As String = String.Empty

            If gvTransport.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvTransport.HeaderRow.FindControl("txtStu_Fee_Id")
                str_Stu_Fee_Id = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Stu_Fee_Id = " AND A.Stu_Fee_Id LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Stu_Fee_Id = "  AND  NOT A.Stu_Fee_Id LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Stu_Fee_Id = " AND A.Stu_Fee_Id  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Stu_Fee_Id = " AND A.Stu_Fee_Id NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Stu_Fee_Id = " AND A.Stu_Fee_Id LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Stu_Fee_Id = " AND A.Stu_Fee_Id NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvTransport.HeaderRow.FindControl("txtSNAME")
                str_SNAME = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_SNAME = " AND A.SNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_SNAME = "  AND  NOT A.SNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_SNAME = " AND A.SNAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_SNAME = " AND A.SNAME NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_SNAME = " AND A.SNAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_SNAME = " AND A.SNAME NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvTransport.HeaderRow.FindControl("txtfileno")
                str_fileno = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_fileno = " AND A.fileno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_fileno = "  AND  NOT A.fileno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_fileno = " AND A.fileno  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_fileno = " AND A.fileno NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_fileno = " AND A.fileno LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_fileno = " AND A.fileno NOT LIKE '%" & txtSearch.Text & "'"
                End If

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & ACD_ID & GRD_ID & SHF_ID & SCT_ID & str_filter_Stu_Fee_Id & str_filter_SNAME & str_filter_fileno & " ORDER BY A.SNAME")

            If ds.Tables(0).Rows.Count > 0 Then

                gvTransport.DataSource = ds.Tables(0)
                gvTransport.DataBind()
                btnPrintSelected.Visible = True
            Else
                btnPrintSelected.Visible = False
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvTransport.DataSource = ds.Tables(0)
                Try
                    gvTransport.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTransport.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvTransport.Rows(0).Cells.Clear()
                gvTransport.Rows(0).Cells.Add(New TableCell)
                gvTransport.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTransport.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTransport.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            'txtSearch = gvTransport.HeaderRow.FindControl("txtStu_Fee_Id")
            'txtSearch.Text = str_Stu_Fee_Id
            'txtSearch = gvTransport.HeaderRow.FindControl("txtSNAME")
            'txtSearch.Text = str_SNAME
            h_Selected_menu_1.Value = "LI__../../../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../../../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../../../Images/operations/like.gif"
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub CURRICULUM_BSU_4_Student()
        Dim GEMSSchool As String = Session("sBsuid")
        Using CURRICULUM_BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_BSU(GEMSSchool)
            Dim di_CURRICULUM_BSU As ListItem
            ddlCurr.Items.Clear()
            If CURRICULUM_BSU_4_Student_reader.HasRows = True Then
                While CURRICULUM_BSU_4_Student_reader.Read
                    di_CURRICULUM_BSU = New ListItem(CURRICULUM_BSU_4_Student_reader("CLM_DESCR"), CURRICULUM_BSU_4_Student_reader("CLM_ID"))
                    ddlCurr.Items.Add(di_CURRICULUM_BSU)
                End While
                If Not ddlCurr.Items.FindByValue(Session("CLM")) Is Nothing Then
                    ddlCurr.Items.FindByValue(Session("CLM")).Selected = True
                End If

                ddlCurr_SelectedIndexChanged(ddlCurr, Nothing)
            End If
        End Using
    End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim CLM_ID As String = String.Empty
            If ddlCurr.SelectedIndex = -1 Then
                CLM_ID = ""
            Else
                CLM_ID = ddlCurr.SelectedItem.Value
            End If
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), CLM_ID)
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using

            If ddlCurr.SelectedValue = Session("CLM") Then
                If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                    ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If
            End If
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim CLM_ID As String = String.Empty
            If ddlCurr.SelectedIndex = -1 Then
                CLM_ID = ""
            Else
                CLM_ID = ddlCurr.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, CLM_ID)
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While
                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If



            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    'For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                    '    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                    '    If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                    '        ddlShift.SelectedIndex = ItemTypeCounter
                    '    End If
                    'Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    
    Protected Sub ddlCurr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callYEAR_DESCRBind()
        callCurrent_BsuShift()
        gvTransport.Visible = False
        btnPrintSelected.Visible = False
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callGrade_ACDBind()
        gvTransport.Visible = False
        btnPrintSelected.Visible = False
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callGrade_Section()

        gvTransport.Visible = False
        btnPrintSelected.Visible = False
    End Sub
    Protected Sub rbTransYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvTransport.Visible = False
        btnPrintSelected.Visible = False
    End Sub

    Protected Sub rbTransNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvTransport.Visible = False
        btnPrintSelected.Visible = False
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callGrade_Section()
        gvTransport.Visible = False
        btnPrintSelected.Visible = False
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'callCurrent_BsuShift()
        gvTransport.Visible = False
        btnPrintSelected.Visible = False
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then


            If ddlAcademicYear.SelectedIndex = -1 Then
                lblError.Text = "Academic Year required"
            ElseIf ddlGrade.SelectedIndex = -1 Then
                lblError.Text = "Grade required"
            ElseIf ddlSection.SelectedIndex = -1 Then
                lblError.Text = "Section required"
            ElseIf ddlShift.SelectedValue = "0" Then

                lblError.Text = "Shift required"
            Else

                CallReport()

            End If

        End If


    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FLAGENQ_STUD As Boolean = True
        Dim bTANS_AVAIL As Boolean = False

        Dim param As New Hashtable



        param.Add("@STU_ACD_ID", ddlAcademicYear.SelectedValue)
        If ddlGrade.SelectedValue = "0" Then
            param.Add("@STU_GRD_ID", Nothing)
        Else
            param.Add("@STU_GRD_ID", ddlGrade.SelectedItem.Value)
        End If
        If ddlSection.SelectedValue = "0" Then
            param.Add("@STU_SCT_ID", Nothing)
        Else
            param.Add("@STU_SCT_ID", ddlSection.SelectedItem.Value)
        End If

        param.Add("@STU_SHF_ID", ddlShift.SelectedValue)
        param.Add("@STU_IDs", "")
        param.Add("@FLAGENQ_STUD", FLAGENQ_STUD)
        If rbTransYes.Checked = True Then
            param.Add("@bTANS_AVAIL", True)
        Else
            param.Add("@bTANS_AVAIL", False)
        End If


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param

            .reportPath = Server.MapPath("../RPT/rptStudTrans_Survey.rpt")


        End With

       
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
        gvTransport.Visible = True

    End Sub


   
    Protected Sub btnPrintSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Page.IsValid Then



            If ddlAcademicYear.SelectedIndex = -1 Then
                lblError.Text = "Academic Year required"
            ElseIf ddlGrade.SelectedIndex = -1 Then
                lblError.Text = "Grade required"
            ElseIf ddlSection.SelectedIndex = -1 Then
                lblError.Text = "Section required"
            ElseIf ddlShift.SelectedValue = "0" Then

                lblError.Text = "Shift required"
            Else

                Dim chk As CheckBox
                Dim STU_ID As String = String.Empty
                Dim hash As New Hashtable
                Dim vhash As SMS_HASH
                If Not Session("hashCheck_Trans") Is Nothing Then
                    hash = Session("hashCheck_Trans")
                End If
                For Each rowItem As GridViewRow In gvTransport.Rows
                    chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
                    STU_ID = DirectCast(rowItem.FindControl("lblStu_ID"), Label).Text
                    

                    If chk.Checked = True Then
                        If hash.Contains(STU_ID) = False Then
                            vhash = New SMS_HASH
                            vhash.STU_ID = STU_ID
                            hash(STU_ID) = vhash
                        End If
                    Else
                        If hash.Contains(STU_ID) = True Then
                            hash.Remove(STU_ID)
                        End If
                    End If
                Next

                Session("hashCheck_Trans") = hash

                Dim tempHash As Hashtable = Session("hashCheck_Trans")
                Dim i = 0
                Dim rcount = 0
                rcount = tempHash.Count


                Dim str_bsu_ids As New StringBuilder

               

                If rcount > 0 Then


                    For Each vloop As SMS_HASH In tempHash.Values

                        str_bsu_ids.Append(vloop.STU_ID)
                        str_bsu_ids.Append("|")

                    Next
                    tempHash.Clear()
                    Session("hashCheck_Trans") = Nothing
                    CallReport_Seleted(str_bsu_ids.ToString)

                Else
                    lblError.Text = "Select the students to be printed"
                End If


            End If

        End If
    End Sub

    Sub CallReport_Seleted(ByVal str_bsu_ids As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FLAGENQ_STUD As Boolean = True
        Dim bTANS_AVAIL As Boolean = False

        Dim param As New Hashtable

        param.Add("@STU_ACD_ID", ddlAcademicYear.SelectedValue)
        If ddlGrade.SelectedValue = "0" Then
            param.Add("@STU_GRD_ID", Nothing)
        Else
            param.Add("@STU_GRD_ID", ddlGrade.SelectedItem.Value)
        End If
        If ddlSection.SelectedValue = "0" Then
            param.Add("@STU_SCT_ID", Nothing)
        Else
            param.Add("@STU_SCT_ID", ddlSection.SelectedItem.Value)
        End If

        param.Add("@STU_SHF_ID", ddlShift.SelectedValue)
        param.Add("@STU_IDs", str_bsu_ids)
        param.Add("@FLAGENQ_STUD", FLAGENQ_STUD)
        If rbTransYes.Checked = True Then
            param.Add("@bTANS_AVAIL", True)
        Else
            param.Add("@bTANS_AVAIL", False)
        End If



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param

            .reportPath = Server.MapPath("../RPT/rptStudTrans_Survey.rpt")


        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
        ' LoadReports()
    End Sub
    Protected Sub gvTransport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvTransport.PageIndex = e.NewPageIndex
        Dim chk As CheckBox
        Dim STU_ID As String = String.Empty
        
        Dim hash As New Hashtable
        Dim vhash As SMS_HASH
        If Not Session("hashCheck_Trans") Is Nothing Then
            hash = Session("hashCheck_Trans")
        End If
        For Each rowItem As GridViewRow In gvTransport.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            STU_ID = DirectCast(rowItem.FindControl("lblStu_ID"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(STU_ID) = False Then
                    vhash = New SMS_HASH
                    vhash.STU_ID = STU_ID
                    hash(STU_ID) = vhash
                End If
            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If
        Next
        Session("hashCheck_Trans") = hash
        gridbind()
    End Sub



    Protected Sub gvTransport_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As New Hashtable
        If Not Session("hashCheck_Trans") Is Nothing Then
            hash = Session("hashCheck_Trans")
        End If
        Dim chk As CheckBox
        Dim STU_ID As String = String.Empty
        For Each rowItem As GridViewRow In gvTransport.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
            STU_ID = gvTransport.DataKeys(rowItem.RowIndex)("STU_ID").ToString()
            If hash.Contains(STU_ID) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If

        Next
    End Sub
End Class
