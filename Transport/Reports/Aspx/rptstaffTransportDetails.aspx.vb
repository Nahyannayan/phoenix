Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports System.Data
Partial Class Transport_Reports_Aspx_rptstaffTransportDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Try


            If Page.IsPostBack = False Then


                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")


                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or ViewState("MainMnu_code") <> "T100481" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    BindCategory()
                    BindBusNo()
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message.ToString()

        End Try

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If Page.IsValid Then
                CallReport()
            End If
        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
        End Try
    End Sub

    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub
    Sub BindBusNo()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BNO_ID,BNO_DESCR FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + ddlBsu.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlBusNo.Items.Insert(0, li)

    End Sub
    Sub BindCategory()
        Try
            Dim str_Conn = ConnectionManger.GetOASISConnectionString
            Dim strQuery As String = "SELECT ECT_ID,ECT_DESCR FROM EMPCATEGORY_M"

            Dim dsCategory As DataSet
            dsCategory = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                ddlempcategory.DataSource = dsCategory
                ddlempcategory.DataTextField = "ECT_DESCR"
                ddlempcategory.DataValueField = "ECT_ID"
                ddlempcategory.DataBind()
                Dim li As New ListItem
                li.Text = "ALL"
                li.Value = "ALL"
                ddlempcategory.Items.Insert(0, li)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub CallReport()

        Dim param As New Hashtable
        param.Add("@EMP_ECT_ID", ddlempcategory.SelectedValue.ToString)
        param.Add("@BNO_ID", ddlBusNo.SelectedItem.ToString)
        param.Add("@BSU_ID", ddlBsu.SelectedValue.ToString)
        If ddljourneytype.SelectedValue = 1 Then
            param.Add("@JOURNEY", "Onward")
        ElseIf ddljourneytype.SelectedValue = 2 Then
            param.Add("@JOURNEY", "Return")
        Else
            param.Add("@JOURNEY", "ALL")
        End If

        'param.Add("loginbsu", Session("sbsuid"))

        'param.Add("bsu", ddlBsu.SelectedItem.Text)
        ' param.Add("academicyear", ddlAcademicYear.SelectedItem.Text)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If ddljourneytype.SelectedValue = 0 Then
                .reportPath = Server.MapPath("../Rpt/rptstaffTransportDetailsall.rpt")
            Else
                .reportPath = Server.MapPath("../Rpt/rptstaffTransportDetails.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            BindBusNo()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
