<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptNewRequest.aspx.vb" Inherits="Students_Reports_ASPX_rptAdmissionDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                      break;
                  case 1:
                      document.getElementById('<%=txtToDate.ClientID %>').value = result;
                      break;
              }
          }
          return false;
      }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="New Request"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td style="width: 557px;">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td align="right">
                            <table align="center"  style="width: 100%">
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">From Date</span><span class="text-danger">*</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                         <asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return getDate(550, 310, 0)" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender5" CssClass="MyCalendar" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                                            TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="20%" align="left"><span class="field-label">To Date</span><span class="text-danger">*</span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                       <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return getDate(550, 310, 1)" /><asp:RequiredFieldValidator ID="rfvToDate"
                                                runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                                ValidationGroup="dayBook" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="MyCalendar" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                                            TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtToDate" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Search Record Based on</span></td>
                                    <td align="left">
                                        <asp:RadioButton ID="rbEntryDate" CssClass="field-label" runat="server" Text="Entry Date " GroupName="DT"></asp:RadioButton>
                                        <asp:RadioButton ID="rbStartDate" CssClass="field-label" runat="server" Text="Start Date " GroupName="DT"></asp:RadioButton></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td align="right"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

