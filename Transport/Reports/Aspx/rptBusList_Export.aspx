<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptBusList_Export.aspx.vb" Inherits="Transport_Reports_Aspx_rptBusList_Export" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">





    </script>

    &nbsp; &nbsp;

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
           BUS LIST
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" style="width: 100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;
                &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="tblRate" align="center" style="width: 100%">

                                <tr>
                                    <td width="20%"  align="left"><span class="field-label">SelectBusinessUnit </span></td>
                                    <td width="30%" >
                                        <asp:DropDownList ID="ddlBsu" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                               
                                    <td width="20%"  align="left"><span class="field-label">Select AcademicYear</span> </td>
                                    <td width="30%"  align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td   align="left"><span class="field-label">Select Bus No</span></td>
                                    <td   align="left">
                                        <asp:DropDownList ID="ddlBusNo" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                               <td></td>
                                    <td align="left">
                                        <asp:RadioButton CssClass="field-label" ID="rdOnward" runat="server" Checked="True" GroupName="g" Text="Onward" />
                                        <asp:RadioButton CssClass="field-label" ID="rdReturn" runat="server" GroupName="g" Text="Return" />
                                        <asp:CheckBox CssClass="field-label" ID="chkFuture" Text="Promoted Year" runat="server" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">

                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" ValidationGroup="groupM1" Visible="False"></asp:Button>
                                        &nbsp;<asp:Button ID="btnGenerateReport2" runat="server" CssClass="button" Text="Export"
                                            ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
               <input id="lstValues" name="lstValues" runat="server" type="hidden" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px" />
                            &nbsp;
           
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

