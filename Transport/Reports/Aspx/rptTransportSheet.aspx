<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" Debug="true" AutoEventWireup="false" CodeFile="rptTransportSheet.aspx.vb" Inherits="Transport_Reports_Aspx_rptTransportSheet" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);

                __doPostBack("", "");


            }
        }


        function OnTreeClick1(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }


        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }


        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;
            if (parentNodeTable) {
                var checkUncheckSwitch;
                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }
                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) {
                    //check if the child node is an element node
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function UnCheckAll() {
            var oTree = document.getElementById("<%=tvBsu.ClientId %>");
            childChkBoxes = oTree.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = false;
            }

            return true;
        }

        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture = "../../../Accounts/calendar.aspx?nofuture=yes";
            if (val == 0) {
                result = window.showModalDialog(nofuture, "", sFeatures)

            }
            else {
                result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures)
            }
            if (result == '' || result == undefined) {
                return false;
            }


            if (val == 1) {
                document.getElementById('<%=txtFrom.ClientID %>').value = result;
            }
            else if (val == 2) {
                document.getElementById('<%=txtTo.ClientID %>').value = result;
            }




    }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Trip Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>



                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <%--  <tr class="subheader_img">
          <td align="left"  style="height: 16px" valign="middle" >
         <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
          </span></font></td>
         </tr>--%>

                                <tr>
                                    <td align="center">

                                        <table id="tblTrip" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="20%"><span class="field-label">Select Academic Year</span> </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="50%">
                                                    <asp:CheckBox ID="chkTotal" runat="server" Text="Get Total Strength" Visible="False" CssClass="field-label" /></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="30%">
                                                    <table id="tblGroup" runat="server" border="0" width="100%">
                                                        <tr>
                                                            <td align="left" class="title-bg-lite">Business Unit
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <div class="checkbox-list-full">
                                                                    <asp:TreeView ID="tvBsu" runat="server"
                                                                        ExpandDepth="1" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                                                        ShowCheckBoxes="All" Style="overflow: auto; text-align: left" OnTreeNodeCheckChanged="tvBsu_TreeNodeCheckChanged" PopulateNodesFromClient="False">
                                                                        <ParentNodeStyle Font-Bold="False" />
                                                                        <HoverNodeStyle Font-Underline="False" />
                                                                        <SelectedNodeStyle HorizontalPadding="3px" VerticalPadding="1px" />
                                                                        <NodeStyle HorizontalPadding="5px"
                                                                            NodeSpacing="1px" VerticalPadding="2px" />
                                                                    </asp:TreeView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left" width="35%">
                                                    <table id="Table2" runat="server" border="0" width="100%">
                                                        <tr class="title-bg-lite">
                                                            <td align="left">

                                                                <asp:CheckBox ID="chkArea" runat="server" AutoPostBack="True" Text="Area" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <div class="checkbox-list-full">
                                                                    <asp:CheckBoxList ID="lstArea" runat="server" AutoPostBack="True" RepeatLayout="Flow" Style="vertical-align: middle; overflow: auto; text-align: left">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                                <td align="left" width="35%">
                                                    <table id="Table3" runat="server" border="0" width="100%">
                                                        <tr class="title-bg-lite">
                                                            <td align="left">

                                                                <asp:CheckBox ID="chkVehicle" runat="server" AutoPostBack="True" Text="Vehicle" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <div class="checkbox-list-full">
                                                                    <asp:CheckBoxList ID="lstVehicle" runat="server" AutoPostBack="True" RepeatLayout="Flow" Style="vertical-align: middle; overflow: auto; text-align: left">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3">
                                                    <table id="Table6" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                                        <tr>

                                                            <td align="left">
                                                                <asp:Label ID="lblDttext" runat="server" Text="Trip Date From" CssClass="field-label"></asp:Label></td>

                                                            <td align="left">
                                                                <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True"></asp:TextBox>
                                                                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom"
                                                                    Display="Dynamic" ErrorMessage="Enter the Trip Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtFrom"
                                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date"
                                                                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                                <asp:RequiredFieldValidator ID="rfFrom" runat="server" ErrorMessage="Please enter the field trip date from" ControlToValidate="txtFrom" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                                                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>



                                                            <td align="left">
                                                                <asp:Label ID="Label3" runat="server" Text="To" CssClass="field-label"></asp:Label></td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtTo" runat="server" AutoPostBack="True"></asp:TextBox>
                                                                <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTo"
                                                                    Display="Dynamic" ErrorMessage="Enter the Trip Date To in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtTo"
                                                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date To entered is not a valid date"
                                                                        ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                                                <asp:RequiredFieldValidator ID="rfTo" runat="server" ErrorMessage="Please enter the field trip date to" ControlToValidate="txtTo" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                                                    Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>



                                            <tr>
                                                <td colspan="3">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkRegular" runat="server" Text="Regular Trip" AutoPostBack="True" CssClass="field-label" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkExtra" runat="server" Text="Extra Trip" AutoPostBack="True" CssClass="field-label" /></td>

                                                            <td align="left">
                                                                <asp:DropDownList ID="ddlPurpose" AutoPostBack="true" runat="server">
                                                                </asp:DropDownList></td>
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkApproved" runat="server" Text="Chargable" AutoPostBack="True" CssClass="field-label" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkNotApproved" runat="server" Text="Not Chargable" AutoPostBack="True" CssClass="field-label" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="left">
                                                    <table id="Table5" runat="server" border="0" width="100%">

                                                        <tr class="title-bg-lite">
                                                            <td align="left" colspan="4">Trip</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList runat="server" ID="ddlTripType" AutoPostBack="true">
                                                                    <asp:ListItem>All</asp:ListItem>
                                                                    <asp:ListItem>Onward</asp:ListItem>
                                                                    <asp:ListItem>Return</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="20%"></td>
                                                            <td align="left" width="30%"></td>
                                                            <td align="left" width="20%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="4">
                                                                <div id="tvTrip0" runat="server" class="checkbox-list-full">
                                                                    <asp:TreeView ID="tvTrip" runat="server"
                                                                        ExpandDepth="1" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick1(event);"
                                                                        ShowCheckBoxes="All" Style="overflow: auto; text-align: left" PopulateNodesFromClient="False">
                                                                        <ParentNodeStyle Font-Bold="False" />
                                                                        <HoverNodeStyle Font-Underline="False" />
                                                                        <SelectedNodeStyle Font-Underline="False" HorizontalPadding="3px" VerticalPadding="1px" />
                                                                        <NodeStyle HorizontalPadding="5px"
                                                                            NodeSpacing="1px" VerticalPadding="2px" />
                                                                    </asp:TreeView>
                                                                </div>
                                                                <div id="tvTrip10" runat="server" class="checkbox-list-full">
                                                                    <asp:TreeView ID="tvTrip1" runat="server"
                                                                        ExpandDepth="1" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                                                        ShowCheckBoxes="All" Style="overflow: auto; text-align: left" PopulateNodesFromClient="False" Visible="False">
                                                                        <ParentNodeStyle Font-Bold="False" />
                                                                        <HoverNodeStyle Font-Underline="False" />
                                                                        <SelectedNodeStyle Font-Underline="False" HorizontalPadding="3px" VerticalPadding="1px" />
                                                                        <NodeStyle HorizontalPadding="5px"
                                                                            NodeSpacing="1px" VerticalPadding="2px" />
                                                                    </asp:TreeView>
                                                                </div>
                                                            </td>
                                                        </tr>


                                                    </table>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <table id="Table4" runat="server" border="0" width="100%">
                                                        <tr class="title-bg-lite">
                                                            <td align="left">Driver
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="padding: 10px">
                                                                <asp:CheckBox ID="chkDriver" runat="server" AutoPostBack="True" Text="Select All" />

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <div class="checkbox-list-full">
                                                                    <asp:CheckBoxList ID="lstDriver" runat="server" RepeatLayout="Flow" Style="vertical-align: middle; overflow: auto; text-align: left">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="rdBsu" runat="server" GroupName="G1" Text="Business Unit Wise Report" Checked="True" CssClass="field-label" />
                                                    <asp:CheckBox
                                                        ID="chkPageWise" runat="server" Text="Split Page Wise" Visible="False" CssClass="field-label"></asp:CheckBox>

                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdArea" runat="server" Text="Area Wise Report" GroupName="G1" CssClass="field-label" /></td>
                                                <td>
                                                    <asp:RadioButton ID="rdVehicle" runat="server" Text="Vehicle Wise Report" GroupName="G1" CssClass="field-label" />
                                                    <asp:CheckBox
                                                        ID="chkPageWise1" runat="server" Text="Split Page Wise" Visible="False" CssClass="field-label"></asp:CheckBox><br />
                                                    <asp:CheckBox
                                                        ID="chkFuture" runat="server" Text="Promoted Year" Visible="False" CssClass="field-label"></asp:CheckBox></td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="center">

                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"></asp:Button>
                                    </td>
                                </tr>
                            </table>

                            <asp:HiddenField ID="hfENDDT" runat="server" />

                            <asp:HiddenField ID="hfACY_ID" runat="server" />

                            <input id="lstValues" name="lstValues" runat="server" type="hidden" />


                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>


</asp:Content>

