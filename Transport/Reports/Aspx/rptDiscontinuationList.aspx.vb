Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports System.Data
Partial Class Students_Reports_ASPX_rptDiscontinuationList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")


            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or ViewState("MainMnu_code") <> "T100420" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                BindBsu()
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty
            Str_ValidateDate = ValidateDate()
            If Str_ValidateDate = "" Then
                CallReport()
            End If
        End If

    End Sub
  
    'Sub CallReport()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim FromTo_Date As String = ""
    '    Dim TC_Cancel As Boolean
    '    Dim TCSO As String = String.Empty
    '    Dim i As Integer = 0

    '    Dim date1 As String
    '    Dim date2 As String
    '    'Dim str_query As String = "SELECT ACY_DESCR FROM ACADEMICYEAR_M WHERE ACY_ID=" + Session("Current_ACY_ID")
    '    'Dim acy As String = SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_query)
    '    If txtToDate.Text = "" Then
    '        date2 = "2100-01-01"
    '    Else
    '        date2 = Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd")
    '    End If

    '    If txtFromDate.Text = "" Then
    '        date1 = "1900-01-01"
    '    Else
    '        date1 = Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd")
    '    End If

    '    Dim param As New Hashtable
    '    param.Add("@IMG_BSU_ID", Session("sbsuid"))
    '    param.Add("@IMG_TYPE", "LOGO")
    '    param.Add("@BSU_ID", ddlBsu.SelectedValue)
    '    param.Add("@BSU_IDs", ddlBsu.SelectedValue)
    '    param.Add("@DATE2", date2)
    '    param.Add("@DATE1", date1)
    '    If txtToDate.Text <> "" Then
    '        param.Add("to", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
    '    Else
    '        param.Add("to", "")
    '    End If

    '    If txtFromDate.Text <> "" Then
    '        param.Add("from", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
    '    Else
    '        param.Add("from", "")
    '    End If
    '    param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
    '    param.Add("UserName", Session("sUsr_name"))
    '    param.Add("bsu", ddlBsu.SelectedItem.Text)
    '    Dim strStatus As String
    '    Dim strType As String

    '    strStatus = GetStatus()
    '    strType = GetTypes()
    '    If strStatus = "" Then
    '        param.Add("@STATUS", DBNull.Value)
    '    Else
    '        param.Add("@STATUS", strStatus)
    '    End If

    '    If strType = "" Then
    '        param.Add("@TYPES", DBNull.Value)
    '    Else
    '        param.Add("@TYPES", strType)
    '    End If

    '    '  param.Add("AccYear", acy)
    '    ' param.Add("@REASONS", GetReasons)
    '    Dim rptClass As New rptClass

    '    With rptClass
    '        .crDatabase = "Oasis_Transport"
    '        .reportParameters = param
    '        .reportPath = Server.MapPath("../RPT/rptDiscontinuationList.rpt")
    '    End With
    '    Session("rptClass") = rptClass

    '    'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    '    ReportLoadSelection()

    'End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FromTo_Date As String = ""
        Dim TC_Cancel As Boolean
        Dim TCSO As String = String.Empty
        Dim i As Integer = 0

        Dim date1 As String
        Dim date2 As String
        Dim strBsu As String = String.Empty
        'Dim str_query As String = "SELECT ACY_DESCR FROM ACADEMICYEAR_M WHERE ACY_ID=" + Session("Current_ACY_ID")
        'Dim acy As String = SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_query)

        If ddlBsu.SelectedValue = "900501" Or ddlBsu.SelectedValue = "900500" Then
            With lstBsu
                For i = 0 To .Items.Count - 1
                    If .Items(i).Selected = True Then
                        If .Items(i).Value <> "All" Then
                            strBsu += .Items(i).Value + "|"
                        End If

                    End If
                Next
            End With
        Else
            strBsu = ""
        End If

        If txtToDate.Text = "" Then
            date2 = "2100-01-01"
        Else
            date2 = Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd")
        End If

        If txtFromDate.Text = "" Then
            date1 = "1900-01-01"
        Else
            date1 = Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd")
        End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", ddlBsu.SelectedValue)
        param.Add("@DATE2", date2)
        param.Add("@DATE1", date1)
        If txtToDate.Text <> "" Then
            param.Add("to", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
        Else
            param.Add("to", "")
        End If

        If txtFromDate.Text <> "" Then
            param.Add("from", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        Else
            param.Add("from", "")
        End If
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("bsu", ddlBsu.SelectedItem.Text)
        Dim strStatus As String
        Dim strType As String

        strStatus = GetStatus()
        strType = GetTypes()
        If strStatus = "" Then
            param.Add("@STATUS", DBNull.Value)
        Else
            param.Add("@STATUS", strStatus)
        End If

        If strType = "" Then
            param.Add("@TYPES", DBNull.Value)
        Else
            param.Add("@TYPES", strType)
        End If
        param.Add("@CATEGORY", rbCategory.SelectedValue)
        param.Add("@BSU_IDs", strBsu)
        '  param.Add("AccYear", acy)
        ' param.Add("@REASONS", GetReasons)
        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptDiscontinuationList.rpt")
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

    Function GetTypes() As String
        Dim i As Integer
        Dim strType As String = ""
        Dim strType1 As String = ""
        For i = 0 To lstType.Items.Count - 1
            If lstType.Items(i).Selected = True Then
                If strType <> "" Then
                    strType += ","
                End If
                strType += "'" + lstType.Items(i).Value + "'"
            End If

            If strType1 <> "" Then
                strType1 += ","
            End If
            strType1 += "'" + lstType.Items(i).Value + "'"

        Next

        If strType <> "" Then
            Return "(" + strType + ")"
        Else
            Return "(" + strType1 + ")"
        End If
    End Function

    Function GetStatus() As String
        Dim i As Integer
        Dim strStatus As String = ""
        Dim strStatus1 As String = ""
        For i = 0 To lstStatus.Items.Count - 1
            If lstStatus.Items(i).Selected = True Then
                If strStatus <> "" Then
                    strStatus += ","
                End If
                If lstStatus.Items(i).Value = "Approved" Then
                    strStatus += "'1'"
                ElseIf lstStatus.Items(i).Value = "Rejected" Then
                    strStatus += "'0'"
                Else
                    strStatus += "'-'"
                End If
            End If

            If strStatus1 <> "" Then
                strStatus1 += ","
            End If
            If lstStatus.Items(i).Value = "Approved" Then
                strStatus1 += "'0'"
            ElseIf lstStatus.Items(i).Value = "Rejected" Then
                strStatus1 += "'1'"
            Else
                strStatus1 += "'-'"
            End If

        Next
        If strStatus <> "" Then
            Return "(" + strStatus + ")"
        Else
            Return "(" + strStatus1 + ")"
        End If
    End Function

    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDate.Text.Trim <> "" Then

                Dim strfDate As String = txtToDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function
End Class
