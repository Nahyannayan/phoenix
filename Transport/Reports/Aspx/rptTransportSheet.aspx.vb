Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Transport_Reports_Aspx_rptTransportSheet
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(tvBsu)

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000110" And ViewState("MainMnu_code") <> "T000130" _
                And ViewState("MainMnu_code") <> "T100270" And ViewState("MainMnu_code") <> "T100280" _
                And ViewState("MainMnu_code") <> "T100290" And ViewState("MainMnu_code") <> "T100300" _
                And ViewState("MainMnu_code") <> "T100310" And ViewState("MainMnu_code") <> "T100320") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    GetEndDate()

                    txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
                    txtTo.Text = Format(Now.Date, "dd/MMM/yyyy")


                    Select Case ViewState("MainMnu_code")

                        Case "T000110" 'Trip Sheet
                            tvTrip1.Visible = True
                            tvTrip10.Visible = True
                            tvTrip.Visible = False
                            tvTrip0.Visible = False
                            tblTrip.Rows(0).Visible = False
                            rdArea.Text = "Driver Wise"
                            BindBsu()
                            BindVehicle()
                            BindArea()
                            BindPurpose()
                            BindTrip1()
                            BindDriver()

                        Case "T000130" 'Regular Trips Summary
                            tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Cells(1).Visible = False
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                            tblTrip.Rows(4).Cells(0).Style.Add("align", "center")
                            rdArea.Text = "Trip Wise"
                            BindBsu()
                            BindVehicle()
                            BindArea()
                            BindTrip()
                        Case "T100270" 'Seat Capacity
                            tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Visible = False
                            tblTrip.Rows(4).Cells(1).Visible = False
                            tblTrip.Rows(1).Cells(1).ColSpan = 2
                            tblTrip.Rows(5).Visible = False
                            BindBsu()
                            BindArea()
                            tblTrip.Rows(1).Cells(2).Visible = False
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                        Case "T100280" 'Fuel Entry Report
                            tblTrip.Rows(0).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Visible = False
                            tblTrip.Rows(5).Visible = False
                            tblTrip.Rows(4).Cells(1).Visible = False
                            lblDttext.Text = "Fuel entry date from "
                            tblTrip.Rows(1).Cells(1).Visible = False
                            tblTrip.Rows(1).Cells(2).ColSpan = 2
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                            BindBsu()
                            BindVehicle()
                            txtFrom.Text = ""
                        Case "T100290" 'Student wise trip details
                            BindGradeAndSection()
                            BindVehicle()
                            BindArea()
                            BindTrip()
                            tblTrip.Rows(4).Cells(1).Visible = False
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(2).Visible = False

                            rdBsu.Text = "Grade and Section wise report"
                            rdVehicle.Text = "Trip wise"
                            chkPageWise.Visible = True
                            chkPageWise1.Visible = True

                            chkFuture.Visible = True


                        Case "T100300" 'Student waitlist
                            tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Visible = False
                            tblTrip.Rows(1).Cells(2).Visible = False
                            tblTrip.Rows(1).Cells(1).ColSpan = 2
                            tblTrip.Rows(5).Cells(2).Visible = False
                            tblTrip.Rows(5).Cells(1).ColSpan = 2
                            BindGradeAndSection()
                            BindArea()
                            rdBsu.Text = "Grade and Section wise report"

                        Case "T100310" 'Extra Trip Summary
                            BindBsu()
                            BindVehicle()
                            BindArea()
                            BindPurpose()

                            chkRegular.Visible = False
                            chkExtra.Checked = True
                            chkExtra.Visible = False
                            BindTrip()
                            rdArea.Text = "Purpose Wise"
                            tblTrip.Rows(1).Cells(1).Visible = False
                            tblTrip.Rows(4).Cells(1).Visible = False
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                            tblTrip.Rows(5).Cells(2).Visible = False
                            tblTrip.Rows(5).Cells(1).ColSpan = 2

                        Case "T100320" 'List of students not using transport

                            BindGradeAndSection()
                            tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Visible = False
                            tblTrip.Rows(1).Cells(2).Visible = False
                            tblTrip.Rows(1).Cells(1).Visible = False
                            tblTrip.Rows(1).Cells(0).ColSpan = 3
                            tblTrip.Rows(1).Cells(0).Align = "center"
                            tblTrip.Rows(5).Visible = False
                            'tblTrip.Rows(5).Cells(1).ColSpan = 2
                    End Select



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblerror.Text = "Request could not be processed"
            End Try

        End If

        'tvTrip1.Visible = True
        'tvTrip.Visible = True
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetEndDate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACD_ENDDT,ACD_ACY_ID FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            hfENDDT.Value = reader.GetDateTime(0).ToShortDateString
            hfACY_ID.Value = reader.GetValue(1).ToString
        End While
    End Sub
    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_ID,ISNULL(BSU_SHORT_RESULT,'') AS BSU_SHORT_RESULT,SHF_ID,SHF_DESCR FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & " INNER JOIN VV_SHIFTS_M AS C ON A.BSU_ID=C.SHF_BSU_ID " _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_SHORT_RESULT FOR XML AUTO"

        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("BSU_ID", "ID")
        str = str.Replace("BSU_SHORT_RESULT", "TEXT")
        str = str.Replace("SHF_ID", "ID")
        str = str.Replace("SHF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvBsu.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvBsu.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvBsu.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub


    Sub BindVehicle()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        Dim str As String = ""
        Dim i As Integer
        Dim node As TreeNode


        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim ccccNode As TreeNode
        Dim eFor As Boolean = False

        If ViewState("MainMnu_code") <> "T100290" Then
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If str <> "" Then
                                str += ","
                            End If
                            str += "'" + cNode.Value + "'"
                            Exit For
                        End If
                    Next
                Next
            Next

            If str = "" Then
                For Each node In tvBsu.Nodes
                    For Each cNode In node.ChildNodes
                        For Each ccNode In cNode.ChildNodes
                            'If ccNode.Checked = True Then
                            If str <> "" Then
                                str += ","
                            End If
                            str += "'" + cNode.Value + "'"
                            Exit For
                            ' End If
                        Next
                    Next
                Next
            End If
        Else
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        For Each cccNode In ccNode.ChildNodes
                            For Each ccccNode In cccNode.ChildNodes
                                If ccccNode.Checked = True Then
                                    If str <> "" Then
                                        str += ","
                                    End If
                                    str += "'" + cNode.Value + "'"
                                    eFor = True
                                    Exit For
                                End If
                            Next
                            If eFor = True Then
                                Exit For
                            End If
                        Next
                        If eFor = True Then
                            Exit For
                        End If
                    Next
                    eFor = False
                Next
            Next
            If str = "" Then
                For Each node In tvBsu.Nodes
                    For Each cNode In node.ChildNodes
                        For Each ccNode In cNode.ChildNodes
                            For Each cccNode In ccNode.ChildNodes
                                For Each ccccNode In cccNode.ChildNodes
                                    ' If ccccNode.Checked = True Then
                                    If str <> "" Then
                                        str += ","
                                    End If
                                    str += "'" + cNode.Value + "'"
                                    eFor = True
                                    Exit For
                                    'End If
                                Next
                                If eFor = True Then
                                    Exit For
                                End If
                            Next
                            If eFor = True Then
                                Exit For
                            End If
                        Next
                        eFor = False
                    Next
                Next
            End If
        End If



        '  If str <> "" Then
        str_query = "SELECT VEH_ID,VEH_REGNO FROM TRANSPORT.VV_VEHICLE_M WHERE VEH_ALTO_BSU_ID IN(" + str + ")"
        ' Else
        'str_query = "SELECT VEH_ID,VEH_REGNO FROM TRANSPORT.VV_VEHICLE_M WHERE VEH_OPRT_BSU_ID='" + Session("sbsuid") + "'"
        'End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstVehicle.DataSource = ds
        lstVehicle.DataTextField = "VEH_REGNO"
        lstVehicle.DataValueField = "VEH_ID"
        lstVehicle.DataBind()

    End Sub

    Sub BindDriver()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
                                 & "emp_id FROM employee_m as a inner join oasis_transport.transport.trips_d as b " _
                                 & " on a.emp_id=b.trd_driver_emp_id   INNER JOIN OASIS..EMPDESIGNATION_M DES ON EMP_DES_ID=DES_ID AND DES_FLAG='SD' " _
                                 & " WHERE DES_GROUP_ID=308 " _
                                 & " and emp_ID<>0"

        Dim str_bsu As String = ""
        Dim str_trip As String = ""

        Dim node As TreeNode


        Dim cNode As TreeNode
        Dim ccNode As TreeNode


        For Each node In tvTrip1.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        If str_trip <> "" Then
                            str_trip += ","
                        End If
                        str_trip += "'" + ccNode.Value + "'"
                    End If
                Next
            Next
        Next

        If str_trip = "" Then

            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If str_bsu <> "" Then
                                str_bsu += ","
                            End If
                            str_bsu += "'" + cNode.Value + "'"
                            Exit For
                        End If
                    Next
                Next
            Next

            If str_bsu <> "" Then
                str_query += " AND EMP_BSU_ID IN(" + str_bsu + ")"
            End If
        Else
            str_query += " AND trd_trp_ID IN(" + str_trip + ")"
        End If

        str_query += " ORDER BY emp_name,emp_id"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstDriver.DataSource = ds
        lstDriver.DataTextField = "emp_name"
        lstDriver.DataValueField = "emp_id"
        lstDriver.DataBind()
    End Sub
    Sub BindArea()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        Dim str As String = ""
        Dim i As Integer

        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim ccccNode As TreeNode
        Dim eFor As Boolean = False

        If ViewState("MainMnu_code") <> "T100290" Then
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If str <> "" Then
                                str += ","
                            End If
                            str += "'" + cNode.Value + "'"
                            Exit For
                        End If
                    Next
                Next
            Next
        Else
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        For Each cccNode In ccNode.ChildNodes
                            For Each ccccNode In cccNode.ChildNodes
                                If ccccNode.Checked = True Then
                                    If str <> "" Then
                                        str += ","
                                    End If
                                    str += "'" + cNode.Value + "'"
                                    eFor = True
                                    Exit For
                                End If
                            Next
                            If eFor = True Then
                                Exit For
                            End If
                        Next
                        If eFor = True Then
                            Exit For
                        End If
                    Next
                    eFor = False
                Next
            Next
        End If

        If str <> "" Then
            str_query = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A " _
                     & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID" _
                     & " WHERE PNT_BSU_ID IN(" + str + ")"
        Else
            str_query = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstArea.DataSource = ds
        lstArea.DataTextField = "SBL_DESCRIPTION"
        lstArea.DataValueField = "SBL_ID"
        lstArea.DataBind()
    End Sub

    Sub BindPurpose()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT PSE_ID,PSE_DESCR FROM TRANSPORT.TRIP_PURPOSE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPurpose.DataSource = ds
        ddlPurpose.DataTextField = "PSE_DESCR"
        ddlPurpose.DataValueField = "PSE_ID"
        ddlPurpose.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = 0
        ddlPurpose.Items.Insert(0, li)
    End Sub

    Sub BindTrip()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        str_query = GetTripQuery()
        ' Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'lstTrip.DataSource = ds
        'lstTrip.DataTextField = "TRP_DESCR"
        'lstTrip.DataValueField = "TRP_ID"
        'lstTrip.DataBind()
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String = ""
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("BSU_ID", "ID")
        str = str.Replace("BSU_NAME", "TEXT")
        str = str.Replace("TRP_ID", "ID")
        str = str.Replace("TRP_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvTrip.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvTrip.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvTrip.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)
        If Session("sbsuid") <> "900501" And Session("sbsuid") <> "900500" Then
            tvTrip.ExpandAll()
        End If
    End Sub
    Sub BindTrip1()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        str_query = GetTripQuery()
        ' Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'lstTrip.DataSource = ds
        'lstTrip.DataTextField = "TRP_DESCR"
        'lstTrip.DataValueField = "TRP_ID"
        'lstTrip.DataBind()
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String = ""
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("BSU_ID", "ID")
        str = str.Replace("BSU_NAME", "TEXT")
        str = str.Replace("TRP_ID", "ID")
        str = str.Replace("TRP_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvTrip.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvTrip1.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvTrip1.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)

        If Session("sbsuid") <> "900501" And Session("sbsuid") <> "900500" Then
            tvTrip1.ExpandAll()
        End If
    End Sub
    Sub BindGradeAndSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT BSU_ID,BSU_NAME,SHF_ID,SHF_DESCR FROM BUSINESSUNIT_M AS A " _
        '                        & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
        '                        & " INNER JOIN VV_SHIFTS_M AS C ON A.BSU_ID=C.SHF_BSU_ID " _
        '                        & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' FOR XML AUTO"
        Dim str_query As String = "SELECT DISTINCT ISNULL(BSU_SHORT_RESULT,'') AS BSU_SHORT_RESULT,BSU_ID,SHF_DESCR,SHF_ID,GRM_DISPLAY," _
                                 & " GRM_GRD_ID,SCT_DESCR,SCT_ID FROM BUSINESSUNIT_M AS A INNER JOIN " _
                                 & " VV_SHIFTS_M AS B ON A.BSU_ID=B.SHF_BSU_ID" _
                                 & " INNER JOIN GRADE_BSU_M AS C ON B.SHF_ID=C.GRM_SHF_ID " _
                                 & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID " _
                                 & " INNER JOIN OASIS_TRANSPORT.dbo.BSU_TRANSPORT AS F ON A.BSU_ID=F.BST_BSU_ID " _
                                 & " WHERE GRM_ACD_ID IN(SELECT ACD_ID FROM ACADEMICYEAR_D WHERE ACD_ACY_ID=" + hfACY_ID.Value _
                                 & " ) AND BST_BSU_OPRT_ID='" + Session("sbsuid") + "'" _
                                 & " ORDER BY BSU_SHORT_RESULT,SHF_DESCR,GRM_DISPLAY,SCT_DESCR" _
                                 & " FOR XML AUTO"


        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("BSU_ID", "ID")
        str = str.Replace("BSU_SHORT_RESULT", "TEXT")
        str = str.Replace("SHF_ID", "ID")
        str = str.Replace("SHF_DESCR", "TEXT")
        str = str.Replace("GRM_GRD_ID", "ID")
        str = str.Replace("GRM_DISPLAY", "TEXT")
        str = str.Replace("SCT_ID", "ID")
        str = str.Replace("SCT_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvBsu.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvBsu.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvBsu.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)

        If Session("sbsuid") <> "900501" And Session("sbsuid") <> "900500" Then
            tvBsu.ExpandDepth = 3
        End If
    End Sub


    Function GetTripQuery() As String
        Dim str_Reg As String
        Dim str_Extra As String
        Dim strFilter As String = ""
        Dim str_Bsu As String = ""
        Dim str_Bsu1 As String = ""

        Dim str_query As String = "SELECT DISTINCT BSU_ID,BSU_NAME,TRP_ID,TRP_DESCR=CASE WHEN TRP_JOURNEY<>'EXTRA' THEN BNO_DESCR+' - '+TRP_DESCR ELSE TRP_DESCR END FROM BUSINESSUNIT_M AS C" _
                        & " INNER JOIN TRANSPORT.TRIPS_M AS " _
                        & " A ON A.TRP_BSU_ID=C.BSU_ID INNER JOIN TRANSPORT.TRIPS_D AS B ON A.TRP_ID=B.TRD_TRP_ID " _
                        & " LEFT OUTER JOIN TRANSPORT.BUSNOS_M AS T ON B.TRD_BNO_ID=T.BNO_ID "



        Dim i As Integer
        Dim str_Veh As String = ""
        Dim str_Area As String = ""

        '****************area****************************************
        For i = 0 To lstArea.Items.Count - 1
            With lstArea
                If .Items(i).Selected = True Then
                    If str_Area <> "" Then
                        str_Area += ","
                    End If
                    str_Area += .Items(i).Value.ToString
                End If
            End With
        Next
        If str_Area <> "" Then
            str_query += " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS D ON B.TRD_ID=D.TPP_TRD_ID " _
                        & " WHERE TPP_SBL_ID IN(" + str_Area + ") "
        Else
            str_query += " WHERE TRP_ID<>0"
        End If
        If ViewState("MainMnu_code") = "T000130" Or ViewState("MainMnu_code") = "T100290" Then
            str_query += " AND TRD_TODATE IS NULL AND TRP_JOURNEY<>'EXTRA'"
        End If

        If ddlTripType.SelectedValue <> "All" Then
            str_query += " AND TRP_JOURNEY='" + ddlTripType.SelectedValue + "'"
        End If
        '************************business unit **********************
        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim ccccNode As TreeNode
        Dim eFor As Boolean = False

        If ViewState("MainMnu_code") <> "T100290" Then
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    If str_Bsu1 <> "" Then
                        str_Bsu1 += ","
                    End If
                    str_Bsu1 += "'" + cNode.Value + "'"
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If str_Bsu <> "" Then
                                str_Bsu += ","
                            End If
                            str_Bsu += "'" + cNode.Value + "'"
                            Exit For
                        End If
                    Next
                Next
            Next
        Else
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    If str_Bsu1 <> "" Then
                        str_Bsu1 += ","
                    End If
                    str_Bsu1 += "'" + cNode.Value + "'"
                    For Each ccNode In cNode.ChildNodes
                        For Each cccNode In ccNode.ChildNodes
                            For Each ccccNode In cccNode.ChildNodes
                                If ccccNode.Checked = True Then
                                    If str_Bsu <> "" Then
                                        str_Bsu += ","
                                    End If
                                    str_Bsu += "'" + cNode.Value + "'"
                                    eFor = True
                                    Exit For
                                End If
                            Next
                            If eFor = True Then
                                Exit For
                            End If
                        Next
                        If eFor = True Then
                            Exit For
                        End If
                    Next
                    eFor = False
                Next
            Next
        End If

        If str_Bsu = "" Then
            'For Each node In tvBsu.Nodes
            '    For Each cNode In node.ChildNodes
            '        For Each ccNode In cNode.ChildNodes
            '            If str_Bsu <> "" Then
            '                str_Bsu += ","
            '            End If
            '            str_Bsu += "'" + cNode.Value + "'"
            '            Exit For
            '        Next
            '    Next
            'Next
            str_Bsu = str_Bsu1
        End If
        str_query += " AND TRP_BSU_ID IN(" + str_Bsu + ")"


        '***********************vehicle************************

        For i = 0 To lstVehicle.Items.Count - 1
            With lstVehicle
                If .Items(i).Selected = True Then
                    If str_Veh <> "" Then
                        str_Veh += ","
                    End If
                    str_Veh += .Items(i).Value.ToString
                End If
            End With
        Next
        If str_Veh <> "" Then
            str_query += " AND TRD_VEH_ID IN(" + str_Veh + ")"
        End If


        '*********************to check wether the trip is active in the selected period
        str_Extra = "  TRP_JOURNEY='EXTRA' "

        str_Reg = "  TRP_JOURNEY<>'EXTRA' "

        'If txtFrom.Text <> "" Then
        '    str_Extra += " AND '" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "' BETWEEN TRD_FROMDATE AND ISNULL(TRD_TODATE,'2100-01-01')"
        'End If

        'If txtTo.Text <> "" Then
        '    str_Extra += " AND '" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "' BETWEEN TRD_FROMDATE AND ISNULL(TRD_TODATE,'2100-01-01')"
        'End If

        'If txtFrom.Text <> "" And txtTo.Text <> "" Then
        '    str_Reg += " AND ISNULL(TRD_TODATE,'2100-01-01') >='" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'" _
        '                & " AND ISNULL(TRD_TODATE,'2100-01-01') NOT BETWEEN '" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
        'ElseIf txtFrom.Text <> "" Then
        '    str_Reg += " AND ISNULL(TRD_TODATE,'2100-01-01') >='" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'"
        'ElseIf txtTo.Text <> "" Then
        '    str_Reg += " AND ISNULL(TRD_TODATE,'2100-01-01') <='" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
        'End If

        If txtFrom.Text <> "" And txtTo.Text <> "" Then
            str_query += " AND ISNULL(TRD_TODATE,'2100-01-01') >='" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'" _
              & " AND TRD_FROMDATE<='" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
        ElseIf txtFrom.Text <> "" Then
            str_query += " AND ISNULL(TRD_TODATE,'2100-01-01') >='" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'"
        ElseIf txtTo.Text <> "" Then
            str_query += " AND ISNULL(TRD_TODATE,'2100-01-01') <='" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
        End If


        '*******************journey******************************
        If chkExtra.Checked = True Then

            If ddlPurpose.SelectedValue <> 0 Then
                str_Extra += " AND TRP_PSE_ID=" + ddlPurpose.SelectedValue.ToString
            End If


            If chkApproved.Checked = True Then
                If chkNotApproved.Checked = False Then
                    str_Extra += " AND TRP_CHARGABLE='TRUE'"
                End If
            End If

            If chkNotApproved.Checked = True Then
                If chkApproved.Checked = False Then
                    str_Extra += " AND TRP_CHARGABLE='FALSE'"
                End If
            End If

        End If




        'If str_Reg <> "" And str_Extra <> "" Then
        '    str_query = str_query + str_Reg + " UNION ALL " + str_query + str_Extra
        'ElseIf str_Reg <> "" Then
        '    str_query = str_query + str_Reg
        'ElseIf str_Extra <> "" Then
        '    str_query = str_query + str_Extra
        'End If


        '''''NOTE:NOT ABLE TO USE UNION ALL AS XML FORMAT IS NOT PROPERLY GENERATED


        If chkRegular.Checked = True And chkExtra.Checked = False Then
            str_query = str_query + " AND " + str_Reg
        ElseIf chkExtra.Checked = True And chkRegular.Checked = False Then
            str_query = str_query + " AND " + str_Extra
        Else
            If ViewState("MainMnu_code") <> "T000130" And ViewState("MainMnu_code") <> "T100290" Then
                str_query = str_query + "AND((" + str_Reg + " )OR(" + str_Extra + " ))"
            End If
        End If
        Return str_query + " ORDER BY BSU_NAME,TRP_DESCR FOR XML AUTO"
    End Function


    Function Get_rptTripSheetQuery(ByVal str_Trip As String) As String
        Dim str_Reg As String
        Dim str_Extra As String
        Dim strFilter As String = ""
        Dim str_Bsu As String = ""

        Dim str_query As String = ""

        Dim i As Integer
        Dim str_Veh As String = ""
        Dim str_Area As String = ""

        Dim str_Driver As String = ""

        Dim str_query1 As String = "SELECT TRD_DRIVER_EMP_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
                   & " CON_NAME,DRV_NAME,TRP_JOURNEY,ISNULL(SHF_DESCR,'') AS SHF_DESCR," _
                   & " PICKUPPOINTS=(select STUFF((SELECT ' , '+[PNT_DESCRIPTION] FROM " _
                   & " TRANSPORT.PICKUPPOINTS_M AS P " _
                   & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS Q ON P.PNT_ID=Q.TPP_PNT_ID " _
                   & " WHERE TPP_TRD_ID=B.TRD_ID for xml path('')),1,1,'')) ," _
                   & " TRL_STARTTIME,TRL_ENDTIME,TRL_ENDKM,TRL_STARTKM,BSU_NAME,BSU_ID,TRL_TRIPDATE,PSE_DESCR='',BSU_SHORT_RESULT " _
                   & " FROM TRANSPORT.VV_TRIPS_D AS B INNER JOIN TRANSPORT.TRIPLOG_S AS A ON B.TRD_ID=A.TRL_TRD_ID " _
                   & " INNER JOIN BUSINESSUNIT_M AS C ON B.TRD_BSU_ID=C.BSU_ID "

        Dim str_query2 As String = "SELECT TRD_DRIVER_EMP_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR=''," _
                   & " CON_NAME,DRV_NAME,TRP_JOURNEY,SHF_DESCR='Extra Trips'," _
                   & " PICKUPPOINTS=''," _
                   & " TRL_STARTTIME,TRL_ENDTIME,TRL_ENDKM,TRL_STARTKM,BSU_NAME,BSU_ID,TRL_TRIPDATE,PSE_DESCR,BSU_SHORT_RESULT " _
                   & " FROM TRANSPORT.VV_EXTRATRIPS_D AS B INNER JOIN TRANSPORT.TRIPLOG_S AS A ON B.TRD_ID=A.TRL_TRD_ID " _
                   & " INNER JOIN BUSINESSUNIT_M AS C ON B.TRD_BSU_ID=C.BSU_ID " _
                   & " WHERE TRP_ID<>0 "


        'if any trip is selected then search only based on trip and tripdate
        If str_Trip = "" Then

            '***********search with area***********************
            For i = 0 To lstArea.Items.Count - 1
                With lstArea
                    If .Items(i).Selected = True Then
                        If str_Area <> "" Then
                            str_Area += ","
                        End If
                        str_Area += .Items(i).Value.ToString
                    End If
                End With
            Next
            If str_Area <> "" Then
                str_query1 += " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS D ON B.TRD_ID=D.TPP_TRD_ID " _
                            & " WHERE TPP_SBL_ID IN(" + str_Area + ") "
            Else
                str_query1 += " WHERE TRP_ID<>0 "
            End If



            '**********business unit****************
            Dim node As TreeNode

            Dim cNode As TreeNode
            Dim ccNode As TreeNode
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If str_Bsu <> "" Then
                                str_Bsu += ","
                            End If
                            str_Bsu += "'" + cNode.Value + "'"
                            Exit For
                        End If
                    Next
                Next
            Next

            If str_Bsu = "" Then
                For Each node In tvBsu.Nodes
                    For Each cNode In node.ChildNodes
                        For Each ccNode In cNode.ChildNodes
                            If str_Bsu <> "" Then
                                str_Bsu += ","
                            End If
                            str_Bsu += "'" + cNode.Value + "'"
                            Exit For
                        Next
                    Next
                Next
            End If
            str_query += " AND TRD_BSU_ID IN(" + str_Bsu + ")"





            '***********vehicle*********************
            For i = 0 To lstVehicle.Items.Count - 1
                With lstVehicle
                    If .Items(i).Selected = True Then
                        If str_Veh <> "" Then
                            str_Veh += ","
                        End If
                        str_Veh += .Items(i).Value.ToString
                    End If
                End With
            Next

            If str_Veh <> "" Then
                str_query += " AND VEH_ID IN(" + str_Veh + ")"
            End If



            '*********************journey********************************
            If chkExtra.Checked = True Then
                str_Extra = " AND TRP_JOURNEY='EXTRA' "

                If ddlPurpose.SelectedValue <> 0 Then
                    str_Extra += " AND TRP_PSE_ID=" + ddlPurpose.SelectedValue.ToString
                End If


                If chkApproved.Checked = True Then
                    If chkNotApproved.Checked = False Then
                        str_Extra += " AND TRP_CHARGABLE='TRUE'"
                    End If
                End If

                If chkNotApproved.Checked = True Then
                    If chkApproved.Checked = False Then
                        str_Extra += " AND TRP_CHARGABLE='FALSE'"
                    End If
                End If

            End If

            If chkRegular.Checked = True Then
                str_Reg = " AND TRP_JOURNEY<>'EXTRA' "
            End If
        Else
            str_query += " AND TRP_ID IN (" + str_Trip + ")"
        End If


        '**************tripdate*************************************
        If txtFrom.Text <> "" Then
            str_query += " AND TRL_TRIPDATE>='" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'"
        End If

        If txtTo.Text <> "" Then
            str_query += " AND TRL_TRIPDATE<='" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
        End If

        For i = 0 To lstDriver.Items.Count - 1
            If lstDriver.Items(i).Selected = True Then
                If str_Driver <> "" Then
                    str_Driver += ","
                End If
                str_Driver += lstDriver.Items(i).Value.ToString
            End If
        Next

        If str_Driver <> "" Then
            str_query += " AND TRD_DRIVER_EMP_ID IN(" + str_Driver + ")"
        End If


        If str_Reg <> "" And str_Extra <> "" Then
            str_query = str_query1 + str_query + str_Reg + " UNION ALL " + str_query2 + str_query + str_Extra
        ElseIf str_Reg <> "" Then
            str_query = str_query1 + str_query + str_Reg
        ElseIf str_Extra <> "" Then
            str_query = str_query2 + str_query + str_Extra
        Else
            str_query = str_query1 + str_query + " UNION ALL " + str_query2 + str_query
        End If

        Return str_query
    End Function

    Function Get_rptFuelQuery() As String
        Dim str_query As String = "SELECT TFL_VEH_ID,TFL_PREVKM,TFL_CURRENTKM,TFL_GALLON,TFL_AMOUNT,TFL_FILLDATE FROM " _
                                & "  TRANSPORT.TRIP_FUEL_S WHERE TFL_ID<>0"

        Dim str_Veh As String = ""
        Dim str_Date As String = ""
        Dim i As Integer

        For i = 0 To lstVehicle.Items.Count - 1
            If lstVehicle.Items(i).Selected = True Then
                If str_Veh <> "" Then
                    str_Veh += ","
                End If
                str_Veh += lstVehicle.Items(i).Value.ToString
            End If
        Next

        If str_Veh = "" Then
            For i = 0 To lstVehicle.Items.Count - 1
                If str_Veh <> "" Then
                    str_Veh += ","
                End If
                str_Veh += lstVehicle.Items(i).Value.ToString
            Next
        End If


        If str_Veh <> "" Then
            str_query += " AND TFL_VEH_ID IN(" + str_Veh + ")"
        End If

        If txtFrom.Text <> "" And txtTo.Text <> "" Then
            str_Date = " AND TFL_FILLDATE BETWEEN '" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
        ElseIf txtFrom.Text <> "" Then
            str_Date = " AND TFL_FILLDATE>='" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'"
        ElseIf txtTo.Text <> "" Then
            str_Date = " AND TFL_FILLDATE<='" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
        End If

        If str_Date <> "" Then
            str_query += str_Date
        End If

        Return str_query

    End Function

    Sub CallTripSheetReport()
        Dim str_query As String
        Dim str_Trip As String = ""

        Dim i As Integer


        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        For Each node In tvTrip1.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        If str_Trip <> "" Then
                            str_Trip += ","
                        End If
                        str_Trip += "'" + ccNode.Value + "'"
                    End If
                Next
            Next
        Next

        str_query = Get_rptTripSheetQuery(str_Trip)


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("@SEARCHQUERY", str_query)
        If Session("sbsuid") = "900501" Then
            param.Add("showH1", "true")
        Else
            param.Add("showH1", "false")
        End If

        param.Add("from", txtFrom.Text)
        param.Add("to", txtTo.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If rdBsu.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptTripSheet_BSU.rpt")
            ElseIf rdVehicle.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptTripSheet_Vehicle.rpt")
            ElseIf rdArea.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptTripSheet_Driver.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub CallRegulaTripSummaryReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        If Session("sbsuid") = "900501" Then
            param.Add("showH1", "true")
        Else
            param.Add("showH1", "false")
        End If
        param.Add("@ACY_ID", hfACY_ID.Value)
        param.Add("@CLM_ID", Session("clm"))
        param.Add("@BSU_XML", getBsu_XML)

        Dim str_Area As String = getArea_XML()
        If str_Area <> "" Then
            param.Add("@SBL_XML", str_Area)
        Else
            param.Add("@SBL_XML", DBNull.Value)
        End If

        Dim str_Veh As String = getVehicle_XML()
        If str_Veh <> "" Then
            param.Add("@VEH_XML", str_Veh)
        Else
            param.Add("@VEH_XML", DBNull.Value)
        End If

        Dim str_Trip As String = getTrip_XML()
        If str_Trip <> "" Then
            param.Add("@TRP_XML", str_Trip)
        Else
            param.Add("@TRP_XML", DBNull.Value)
        End If

        param.Add("@BSU_OPRT_ID", Session("sbsuid"))

        If chkTotal.Checked = True Then
            param.Add("@bTOTAL", "TRUE")
        Else
            param.Add("@bTOTAL", "FALSE")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param

            If rdBsu.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptRegularTripSummary_Bsu.rpt")
            ElseIf rdVehicle.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptRegularTripSummary_Vehicle.rpt")
            ElseIf rdArea.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptRegularTripSummary_Trip.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub


    Sub CallExtraTripSummaryReport()

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        If Session("sbsuid") = "900501" Then
            param.Add("showH1", "true")
        Else
            param.Add("showH1", "false")
        End If

        Dim str_Bsu As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        For Each node In tvBsu.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID></ID>"
                    End If
                Next
            Next
        Next

        If str_Bsu = "" Then
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID></ID>"
                        Exit For
                    Next
                Next
            Next
        End If

        str_Bsu = "<IDS>" + str_Bsu + "</IDS>"

        param.Add("@BSU_XML", str_Bsu)

        'Dim str_Veh As String = getVehicle_XML()
        'If str_Veh <> "" Then
        '    param.Add("@VEH_XML", str_Veh)
        'Else
        '    param.Add("@VEH_XML", DBNull.Value)
        'End If

        Dim str_Trip As String = getTrip_XML()
        If str_Trip = "" Then
            For Each node In tvTrip.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        str_Trip += "<ID><TRP_ID>" + ccNode.Value + "</TRP_ID></ID>"
                    Next
                Next
            Next
            str_Trip = "<IDS>" + str_Trip + "</IDS>"
        End If
        param.Add("@VEH_XML", DBNull.Value)
        If str_Trip <> "" Then
            param.Add("@TRP_XML", str_Trip)
        Else
            param.Add("@TRP_XML", DBNull.Value)
        End If
        param.Add("@BSU_OPRT_ID", Session("sbsuid"))

        param.Add("from", txtFrom.Text)
        param.Add("to", txtTo.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If rdBsu.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptExtraTripSummary_Bsu.rpt")
            ElseIf rdArea.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptExtraTripSummary_Purpose.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub CallSeatCapacityReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        If Session("sbsuid") = "900501" Then
            param.Add("showH1", "true")
        Else
            param.Add("showH1", "false")
        End If
        param.Add("@ACY_ID", hfACY_ID.Value)
        param.Add("@BSU_XML", getBsu_XML)

        Dim str_Area As String = getArea_XML()
        If str_Area <> "" Then
            param.Add("@SBL_XML", str_Area)
        Else
            param.Add("@SBL_XML", DBNull.Value)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            'If rdBsu.Checked = True Then
            '    '.reportPath = Server.MapPath("../Rpt/rptRegularTripSummary_Bsu.rpt")
            'ElseIf rdVehicle.Checked = True Then
            '    '.reportPath = Server.MapPath("../Rpt/rptRegularTripSummary_Vehicle.rpt")
            'ElseIf rdArea.Checked = True Then
            .reportPath = Server.MapPath("../Rpt/rptSeatCapacity_Area.rpt")
            'End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
    Sub CallFuelEntryReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        If Session("sbsuid") = "900501" Then
            param.Add("showH1", "true")
        Else
            param.Add("showH1", "false")
        End If

        param.Add("@SEARCHQUERY", Get_rptFuelQuery)

        If txtFrom.Text <> "" And txtTo.Text <> "" Then
            param.Add("dateQuery", "Fill date between " + Format(Date.Parse(txtFrom.Text), "dd/MMM/yyyy") + " and " + Format(Date.Parse(txtTo.Text), "dd/MMM/yyyy"))
        ElseIf txtFrom.Text <> "" Then
            param.Add("dateQuery", "Fill date since " + Format(Date.Parse(txtFrom.Text), "dd/MMM/yyyy"))
        ElseIf txtTo.Text <> "" Then
            param.Add("dateQuery", "Fill date as on " + Format(Date.Parse(txtTo.Text), "dd/MMM/yyyy"))
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptFuel.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub CallStudentWisetripReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))



        param.Add("@ACY_ID", hfACY_ID.Value)

        Dim str_Trip As String = ""
        Dim str_Sct As String = ""

        str_Trip = getTrip_XML()
        str_Sct = getSCT_XML()

        If str_Trip = "" Then
            param.Add("@TRP_XML", DBNull.Value)
        Else
            param.Add("@TRP_XML", str_Trip)
        End If

        If str_Sct = "" Then
            param.Add("@SCT_XML", DBNull.Value)
        Else
            param.Add("@SCT_XML", str_Sct)
        End If

        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)

        param.Add("@BSU_OPRT_ID", Session("SBSUID"))

        If rdArea.Checked = True Then
            Dim str_area As String = ""
            str_area = getArea_XML()
            If str_area = "" Then
                param.Add("@SBL_XML", DBNull.Value)
            Else
                param.Add("@SBL_XML", str_area)
            End If
        End If

        If rdBsu.Checked = True Then
            param.Add("pgwise", chkPageWise.Checked)
        End If

        If rdVehicle.Checked = True Then
            param.Add("pgwise", chkPageWise1.Checked)
        End If
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If rdBsu.Checked = True Then

                .reportPath = Server.MapPath("../Rpt/rptStuTransport_GradeAndSection.rpt")

            ElseIf rdVehicle.Checked = True Then
                param.Add("@bFUTURE", chkFuture.Checked)
                .reportPath = Server.MapPath("../Rpt/rptStuTransport_Trip.rpt")
                '.reportPath = Server.MapPath("../Rpt/TEST.rpt")
            ElseIf rdArea.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptStuTransport_Area.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub CallStudWaitListReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        If Session("sbsuid") = "900501" Then
            param.Add("showH1", "true")
        Else
            param.Add("showH1", "false")
        End If

        param.Add("@ACY_ID", hfACY_ID.Value)


        Dim str_Sct As String
        str_Sct = getSCT_XML()

        If str_Sct = "" Then
            param.Add("@SCT_XML", DBNull.Value)
        Else
            param.Add("@SCT_XML", str_Sct)
        End If

        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("@BSU_OPRT_ID", Session("SBSUID"))
        If rdArea.Checked = True Then
            Dim str_area As String = ""
            str_area = getArea_XML()
            If str_area = "" Then
                param.Add("@SBL_XML", DBNull.Value)
            Else
                param.Add("@SBL_XML", str_area)
            End If

            Dim str_Bsu As String = ""
            str_Bsu = getBsuGrade_XML()

            If str_area = "" Then
                param.Add("@BSU_XML", DBNull.Value)
            Else
                param.Add("@BSU_XML", str_Bsu)
            End If
        End If


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If rdBsu.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptStuWaitList_GradeandSection.rpt")
            ElseIf rdArea.Checked = True Then
                .reportPath = Server.MapPath("../Rpt/rptStuWaitList_Area.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub CallNoTransportListReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        Dim str_Sct As String
        str_Sct = getSCT_XML()

        If str_Sct = "" Then
            param.Add("@SCT_XML", DBNull.Value)
        Else
            param.Add("@SCT_XML", str_Sct)
        End If

        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("@BSU_OPRT_ID", Session("SBSUID"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_transport"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptStudNoTransportList.rpt")

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Function getBsuGrade_XML() As String

        Dim str_Bsu As String = ""

        Dim node As TreeNode

        Dim eFor As Boolean = False
        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim ccccNode As TreeNode

        For Each node In tvBsu.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    For Each cccNode In cNode.ChildNodes
                        For Each ccccNode In cNode.ChildNodes
                            If ccNode.Checked = True Then
                                str_Bsu += "<ID><BSU_ID>" + ccNode.Value + "</BSU_ID>"
                                eFor = True
                                Exit For
                            End If
                        Next
                        If eFor = True Then
                            Exit For
                        End If
                    Next
                    If eFor = True Then
                        Exit For
                    End If
                Next
                eFor = False
            Next
        Next


        If str_Bsu = "" Then
            Return ""
        Else
            Return "<IDS>" + str_Bsu + "</IDS>"
        End If
    End Function



    Function getBsu_XML() As String

        Dim str_Bsu As String

        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        For Each node In tvBsu.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID><SHF_ID>" + ccNode.Value + "</SHF_ID></ID>"
                    End If
                Next
            Next
        Next


        If str_Bsu = "" Then

            ''IF A TRIP IS SELECTED THEN SHOW ONLY THOSE BUSINESS UNIT FOR WICH TRIP IS SELECTED
            'For Each node In tvTrip.Nodes
            '    For Each cNode In node.ChildNodes
            '        For Each ccNode In cNode.ChildNodes
            '            str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID><SHF_ID>" + ccNode.Value + "</SHF_ID></ID>"
            '        Next
            '    Next
            'Next

            If str_Bsu = "" Then
                For Each node In tvBsu.Nodes
                    For Each cNode In node.ChildNodes
                        For Each ccNode In cNode.ChildNodes
                            str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID><SHF_ID>" + ccNode.Value + "</SHF_ID></ID>"
                        Next
                    Next
                Next
            End If
            Return "<IDS>" + str_Bsu + "</IDS>"
        Else
            Return "<IDS>" + str_Bsu + "</IDS>"
        End If
    End Function

    Function getVehicle_XML()
        Dim i As Integer
        Dim str As String = ""
        With lstVehicle
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    str += "<ID><VEH_ID>" + .Items(i).Value + "</VEH_ID></ID>"
                End If
            Next
        End With

        If str = "" Then
            Return ""
        Else
            Return "<IDS>" + str + "</IDS>"
        End If

    End Function

    Function getArea_XML()
        Dim i As Integer
        Dim str As String = ""
        With lstArea
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    str += "<ID><SBL_ID>" + .Items(i).Value + "</SBL_ID></ID>"
                End If
            Next
        End With
        If str = "" Then
            Return ""
        Else
            Return "<IDS>" + str + "</IDS>"
        End If

    End Function

    Function getTrip_XML()
        Dim str_Trip As String = ""

        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        For Each node In tvTrip.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        str_Trip += "<ID><TRP_ID>" + ccNode.Value + "</TRP_ID></ID>"
                    End If
                Next
            Next
        Next
        If str_Trip = "" Then
            Return ""
        Else
            Return "<IDS>" + str_Trip + "</IDS>"
        End If

    End Function
    Function getSCT_XML()
        Dim str_Trip As String = ""

        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim ccccNode As TreeNode

        For Each node In tvBsu.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    For Each cccNode In ccNode.ChildNodes
                        For Each ccccNode In cccNode.ChildNodes
                            If ccccNode.Checked = True Then
                                str_Trip += "<ID><SCT_ID>" + ccccNode.Value + "</SCT_ID></ID>"
                            End If
                        Next
                    Next
                Next
            Next
        Next

        If str_Trip = "" Then
            Return ""
        Else
            Return "<IDS>" + str_Trip + "</IDS>"
        End If

    End Function



#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        If ViewState("MainMnu_code") = "T000130" Then
            Using readerPrev_Next As SqlDataReader = AccessStudentClass.getNEXT_CURR_PREV(Session("sBsuid"), Session("CLM"), Session("Current_ACY_ID"))
                If readerPrev_Next.HasRows = True Then
                    While readerPrev_Next.Read
                        Session("next_ACD_ID") = Convert.ToString(readerPrev_Next("next_ACD_ID"))
                    End While
                End If
            End Using
            If ddlAcademicYear.SelectedValue = Session("next_ACD_ID") Then
                chkTotal.Visible = True
            Else
                chkTotal.Visible = False
            End If
        End If
        GetEndDate()
    End Sub

    Protected Sub lstArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstArea.SelectedIndexChanged
        BindTrip()
    End Sub

    Protected Sub lstVehicle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstVehicle.SelectedIndexChanged
        BindTrip()
    End Sub

    Protected Sub chkExtra_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExtra.CheckedChanged
        BindTrip()
    End Sub

    Protected Sub chkRegular_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRegular.CheckedChanged
        BindTrip()
    End Sub

    Protected Sub chkApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkApproved.CheckedChanged
        BindTrip()
    End Sub

    Protected Sub chkNotApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNotApproved.CheckedChanged
        BindTrip()
    End Sub

    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        If chkExtra.Checked = True Then
            BindTrip()
        End If
    End Sub

    Protected Sub imgFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrom.Click
        If txtFrom.Text <> "" Then
            BindTrip()
        End If
    End Sub

    Protected Sub imgTo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTo.Click
        If txtTo.Text <> "" Then
            BindTrip()
        End If
    End Sub

    Protected Sub tvBsu_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvBsu.SelectedNodeChanged

    End Sub

    Protected Sub tvBsu_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBsu.TreeNodeCheckChanged
        '       BindArea()
        ' BindVehicle()
        'BindTrip()

        Select Case ViewState("MainMnu_code")
            Case "T000110" 'Trip Sheet
                BindVehicle()
                BindArea()
                BindTrip()
                BindDriver()
            Case "T000130" 'Regular Trips Summary
                BindVehicle()
                BindArea()
                BindTrip()
            Case "T100270" 'Seat Capacity
                BindArea()
            Case "T100280" 'Fuel Entry Report
                BindVehicle()
            Case "T100290"
                BindArea()
                BindVehicle()
                BindTrip()
        End Select

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case ViewState("MainMnu_code")
            Case "T000110"
                CallTripSheetReport()
            Case "T000130"
                CallRegulaTripSummaryReport()
            Case "T100270"
                CallSeatCapacityReport()
            Case "T100280"
                CallFuelEntryReport()
            Case "T100290"
                CallStudentWisetripReport()
            Case "T100300"
                CallStudWaitListReport()
            Case "T100310"
                CallExtraTripSummaryReport()
            Case "T100320"
                CallNoTransportListReport()
        End Select
    End Sub

    Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom.TextChanged
        BindTrip()
    End Sub

    Protected Sub txtTo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged
        BindTrip()
    End Sub

    Protected Sub tvTrip1_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvTrip1.TreeNodeCheckChanged
        Select Case ViewState("MainMnu_code")
            Case "T000110" 'Trip Sheet
                BindDriver()
            Case "T000130" 'Regular Trips Summary
            Case "T100270" 'Seat Capacity
            Case "T100280" 'Fuel Entry Report
        End Select
    End Sub

    Protected Sub chkArea_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkArea.CheckedChanged
        If chkArea.Checked = True Then
            Dim i As Integer
            For i = 0 To lstArea.Items.Count - 1
                lstArea.Items(i).Selected = True
            Next
        Else
            lstArea.ClearSelection()
        End If
    End Sub

    Protected Sub chkDriver_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDriver.CheckedChanged
        If chkDriver.Checked = True Then
            Dim i As Integer
            For i = 0 To lstDriver.Items.Count - 1
                lstDriver.Items(i).Selected = True
            Next
        Else
            lstDriver.ClearSelection()
        End If
    End Sub

    Protected Sub chkVehicle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkVehicle.CheckedChanged
        If chkVehicle.Checked = True Then
            Dim i As Integer
            For i = 0 To lstVehicle.Items.Count - 1
                lstVehicle.Items(i).Selected = True
            Next
        Else
            lstVehicle.ClearSelection()
        End If
    End Sub

    Protected Sub ddlTripType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTripType.SelectedIndexChanged
        BindTrip()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
