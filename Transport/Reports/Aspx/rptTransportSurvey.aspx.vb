Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Students_Reports_ASPX_rptAdmissionDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            '  radSubscribed.Checked = True
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try-E100010

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S135005") Then
            '    If Not Request.UrlReferrer Is Nothing Then
            '        Response.Redirect(Request.UrlReferrer.ToString())
            '    Else

            '        Response.Redirect("~\noAccess.aspx")
            '    End If

            'Else
            'calling pageright class to get the access rights


            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            If ViewState("MainMnu_code") = "T100491" Then
                If Session("sBsuid") = "999998" Then
                    PopulateTree()
                    ddlBSU_ID.Visible = False
                    'Panel1.Visible = False
                Else
                    PopulateTree()
                    ddlBSU_ID.Visible = False
                    'Panel1.Visible = False
                End If

            ElseIf ViewState("MainMnu_code") = "E100006" Then
                'Edu Shield Daily Collection
                ReportCriteriaSettings()
            End If

            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub

    Private Sub ReportCriteriaSettings()
        GEMS_BSU_4_Student()
        tvBusinessunit.Visible = False
        ddlBSU_ID.Visible = True
        ddlBSU_ID.Enabled = False
        ddlBSU_ID.ClearSelection()
        If Not ddlBSU_ID.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_ID.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
        Panel1.Visible = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub GEMS_BSU_4_Student()
        Using BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_Student()
            Dim di_Grade_BSU As ListItem
            ddlBSU_ID.Items.Clear()
            If BSU_4_Student_reader.HasRows = True Then
                While BSU_4_Student_reader.Read
                    di_Grade_BSU = New ListItem(BSU_4_Student_reader("bsu_name"), BSU_4_Student_reader("bsu_id"))
                    ddlBSU_ID.Items.Add(di_Grade_BSU)
                End While
            End If
        End Using
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
          
            If tvBusinessunit.CheckedNodes.Count = 0 Then

            ElseIf tvBusinessunit.CheckedNodes.Count < 1 Then
                lblError.Text = "At least one school needs to be selected"
            ElseIf tvBusinessunit.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvBusinessunit.CheckedNodes
                    If node.Value.Length < 2 Then
                        lblError.Text = "Atleast one school needs to be selected"
                    Else
                        CallReport()
                    End If
                Next
            End If
        End If

    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim strReportOption As Integer = 0
        Dim i As Integer = 0
        Dim strBusUnits As String = ""

        Dim str_bsu_ids As New StringBuilder

        If tvBusinessunit.CheckedNodes.Count >= 1 Then
            For Each node As TreeNode In tvBusinessunit.CheckedNodes
                If node.Value.Length > 2 Then
                    str_bsu_ids.Append(node.Value)
                    str_bsu_ids.Append("|")
                End If
            Next
        Else

            str_bsu_ids.Append(ddlBSU_ID.SelectedValue)
            str_bsu_ids.Append("|")
        End If
       
        If Session("Current_ACD_ID") Is Nothing Then
            Session("Current_ACD_ID") = "80"
        End If

        If RadYes.Checked = True Then
            strReportOption = 1
        Else
            strReportOption = 0
        End If
        'strBusUnits = "'121009','123006'"

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", str_bsu_ids.ToString())
        param.Add("@BCONTINEW", strReportOption)
        param.Add("@ACD_ID", Session("Current_ACD_ID"))

        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptTransFormResponse.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    
    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String

        'added By Sajesh ---------------------
        Dim strFilter As String
        Dim strUnits As String = ""

        If Session("sBsuid") = "999998" Then
            strFilter = ""
        Else
            Dim tblbUsr_id As String = Session("sUsr_id")
            ' strUnits = AccessRoleUser.GetBusinessUnitsByLogUser(tblbUsr_id)
            'strFilter = " AND BSU_ID IN (" & strUnits & ")"
            Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                If BUnitreader.HasRows = True Then
                    While BUnitreader.Read
                        strUnits = strUnits + "'" + BUnitreader(0) + "',"
                    End While
                End If
            End Using
            strUnits = strUnits.TrimEnd(",")
            strFilter = " AND BSU_ID IN (" & strUnits & ")"


        End If
        'By Sajesh ---------------------

        tvBusinessunit.Nodes.Clear()
        str_Sql = "SELECT  0 AS BSU_ID,'All' AS BSU_NAME,  COUNT (*)  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') " & strFilter & " order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvBusinessunit.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        tvBusinessunit.DataBind()
        tvBusinessunit.CollapseAll()
    End Sub

    Protected Sub tvBusinessunit_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBusinessunit.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim strFilter As String
        Dim strUnits As String = ""

        If Session("sBsuid") = "999998" Then
            strFilter = ""
        Else
            Dim tblbUsr_id As String = Session("sUsr_id")
            'strUnits = AccessRoleUser.GetBusinessUnitsByLogUser(tblbUsr_id)

            Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                If BUnitreader.HasRows = True Then
                    While BUnitreader.Read
                        strUnits = strUnits + "'" + BUnitreader(0) + "',"
                    End While
                End If
            End Using
            strUnits = strUnits.TrimEnd(",")
            strFilter = " AND BSU_ID IN (" & strUnits & ")"
        End If

        If parentid = "0" Then

            str_Sql = "SELECT   BSU_ID,BSU_NAME,  0  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') " & strFilter & " order by bsu_name"
        Else
            str_Sql = "SELECT   BSU_ID,BSU_NAME,  0  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') " & strFilter & " order by bsu_name"
        End If



        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)



    End Sub
   
End Class
