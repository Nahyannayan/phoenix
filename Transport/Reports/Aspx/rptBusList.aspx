<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptBusList.aspx.vb" Inherits="Transport_Reports_Aspx_rptBusList" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">





    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            BUS LIST
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_ShowScreen" runat="server" align="center" style="width: 100%">
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;
                &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="tblRate" class="BlueTableView" runat="server" align="center" style="width: 100%">

                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Select Business Unit </span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td   width="20%"><span class="field-label">Select  AcademicYear</span> </td>
                                    <td   width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td  style="width: 315px;" align="left"><span class="field-label">Select Bus No</span></td>
                                    <td >
                                        <asp:DropDownList ID="ddlBusNo" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                               
                                    <td align="left"><span class="field-label">Include Staff list</span></td>
                                    <td align="left" >
                                        <asp:CheckBox CssClass="field-label" ID="chkStaff" runat="server"></asp:CheckBox></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="left"  colspan="2" >
                                        <asp:RadioButton CssClass="field-label" ID="rdOnward" runat="server" Checked="True" GroupName="g" Text="Onward" />
                                        <asp:RadioButton CssClass="field-label" ID="rdReturn" runat="server" GroupName="g" Text="Return" />
                                        <asp:RadioButton CssClass="field-label" ID="rdBoth" runat="server" GroupName="g" Text="Both" />
                                        <asp:CheckBox    CssClass="field-label" ID ="chkFuture" Text="Promoted Year" runat="server" />
                                    </td>
                                    <td></td>
                                    
                                </tr>

                                <tr>
                                    <td  colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" ValidationGroup="groupM1"></asp:Button>
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Report in PDF"
                                            ValidationGroup="groupM1" /></td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
               <input id="lstValues" name="lstValues" runat="server" type="hidden" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px" />
                            &nbsp;
           
                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>


                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>

</asp:Content>

