
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Transport_Reports_Aspx_rptVehicleListExpiry
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(tvBsu)

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code")) <> "T000430" And ViewState("MainMnu_code") <> "T000432" _
                    And ViewState("MainMnu_code") <> "T000434" Then
                    
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    

                    txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
                    txtTo.Text = Format(Now.Date, "dd/MMM/yyyy")


                    Select Case ViewState("MainMnu_code")

                       
                        Case "T000430" 'Vehicle Expiry Report
                            tblTrip.Rows(0).Visible = False
                            'tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Visible = False
                            tblTrip.Rows(5).Visible = False
                            tblTrip.Rows(4).Cells(1).Visible = False
                            'lblDttext.Text = "Fuel entry date from "
                            tblTrip.Rows(1).Cells(1).Visible = False
                            tblTrip.Rows(1).Cells(2).ColSpan = 2
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                            tblTrip.Rows(1).Cells(2).Visible = False
                            lblRptName.Text = "Vehicle Expiry Report"
                            RdRegistration.Checked = True
                            BindBsu()
                            BindVehicle()
                            'txtFrom.Text = ""
                        Case "T000432" 'Category wise vehicle list
                            tblTrip.Rows(0).Visible = False
                            'tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Visible = False
                            tblTrip.Rows(5).Visible = False
                            tblTrip.Rows(4).Cells(1).Visible = False
                            'lblDttext.Text = "Fuel entry date from "
                            tblTrip.Rows(1).Cells(1).Visible = False
                            tblTrip.Rows(1).Cells(3).Visible = False
                            tblTrip.Rows(1).Cells(2).ColSpan = 2
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                            'tblTrip.Rows(1).Cells(3).Visible = False
                            tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(6).Visible = False
                            lblRptName.Text = "Category wise Vehicle List"
                            'RdRegistration.Checked = True
                            BindBsu()
                            BindVehicle()
                            BindCategory()
                        Case "T000434" 'Vehicle Details
                            tblTrip.Rows(0).Visible = False
                            'tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(3).Visible = False
                            tblTrip.Rows(4).Visible = False
                            tblTrip.Rows(5).Visible = False
                            tblTrip.Rows(4).Cells(1).Visible = False
                            'lblDttext.Text = "Fuel entry date from "
                            tblTrip.Rows(1).Cells(1).Visible = False
                            tblTrip.Rows(1).Cells(2).ColSpan = 2
                            tblTrip.Rows(4).Cells(0).ColSpan = 3
                            'tblTrip.Rows(1).Cells(3).Visible = False
                            tblTrip.Rows(1).Cells(2).Visible = False
                            tblTrip.Rows(2).Visible = False
                            tblTrip.Rows(6).Visible = False
                            lblRptName.Text = "Vehicle Details"
                            'RdRegistration.Checked = True
                            BindBsu()
                            BindVehicle()

                    End Select



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblerror.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    
    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_ID,ISNULL(BSU_SHORT_RESULT,'') AS BSU_SHORT_RESULT,SHF_ID,SHF_DESCR FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & " INNER JOIN VV_SHIFTS_M AS C ON A.BSU_ID=C.SHF_BSU_ID " _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_SHORT_RESULT FOR XML AUTO"

        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("BSU_ID", "ID")
        str = str.Replace("BSU_SHORT_RESULT", "TEXT")
        str = str.Replace("SHF_ID", "ID")
        str = str.Replace("SHF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvBsu.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvBsu.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvBsu.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub


    Sub BindVehicle()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        Dim str As String = ""
        Dim i As Integer
        Dim node As TreeNode


        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim ccccNode As TreeNode
        Dim eFor As Boolean = False

        If ViewState("MainMnu_code") <> "T100290" Then
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If str <> "" Then
                                str += ","
                            End If
                            str += "'" + cNode.Value + "'"
                            Exit For
                        End If
                    Next
                Next
            Next

            If str = "" Then
                For Each node In tvBsu.Nodes
                    For Each cNode In node.ChildNodes
                        For Each ccNode In cNode.ChildNodes
                            'If ccNode.Checked = True Then
                            If str <> "" Then
                                str += ","
                            End If
                            str += "'" + cNode.Value + "'"
                            Exit For
                            ' End If
                        Next
                    Next
                Next
            End If
        Else
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    For Each ccNode In cNode.ChildNodes
                        For Each cccNode In ccNode.ChildNodes
                            For Each ccccNode In cccNode.ChildNodes
                                If ccccNode.Checked = True Then
                                    If str <> "" Then
                                        str += ","
                                    End If
                                    str += "'" + cNode.Value + "'"
                                    eFor = True
                                    Exit For
                                End If
                            Next
                            If eFor = True Then
                                Exit For
                            End If
                        Next
                        If eFor = True Then
                            Exit For
                        End If
                    Next
                    eFor = False
                Next
            Next
            If str = "" Then
                For Each node In tvBsu.Nodes
                    For Each cNode In node.ChildNodes
                        For Each ccNode In cNode.ChildNodes
                            For Each cccNode In ccNode.ChildNodes
                                For Each ccccNode In cccNode.ChildNodes
                                    ' If ccccNode.Checked = True Then
                                    If str <> "" Then
                                        str += ","
                                    End If
                                    str += "'" + cNode.Value + "'"
                                    eFor = True
                                    Exit For
                                    'End If
                                Next
                                If eFor = True Then
                                    Exit For
                                End If
                            Next
                            If eFor = True Then
                                Exit For
                            End If
                        Next
                        eFor = False
                    Next
                Next
            End If
        End If



        '  If str <> "" Then
        str_query = "SELECT VEH_ID,VEH_REGNO FROM TRANSPORT.VV_VEHICLE_M WHERE VEH_ALTO_BSU_ID IN(" + str + ")"
        ' Else
        'str_query = "SELECT VEH_ID,VEH_REGNO FROM TRANSPORT.VV_VEHICLE_M WHERE VEH_OPRT_BSU_ID='" + Session("sbsuid") + "'"
        'End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstVehicle.DataSource = ds
        lstVehicle.DataTextField = "VEH_REGNO"
        lstVehicle.DataValueField = "VEH_ID"
        lstVehicle.DataBind()

    End Sub




    Function getBsu_XML() As String

        Dim str_Bsu As String

        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        For Each node In tvBsu.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID><SHF_ID>" + ccNode.Value + "</SHF_ID></ID>"
                    End If
                Next
            Next
        Next


        If str_Bsu = "" Then

            ''IF A TRIP IS SELECTED THEN SHOW ONLY THOSE BUSINESS UNIT FOR WICH TRIP IS SELECTED
            'For Each node In tvTrip.Nodes
            '    For Each cNode In node.ChildNodes
            '        For Each ccNode In cNode.ChildNodes
            '            str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID><SHF_ID>" + ccNode.Value + "</SHF_ID></ID>"
            '        Next
            '    Next
            'Next

            If str_Bsu = "" Then
                For Each node In tvBsu.Nodes
                    For Each cNode In node.ChildNodes
                        For Each ccNode In cNode.ChildNodes
                            str_Bsu += "<ID><BSU_ID>" + cNode.Value + "</BSU_ID><SHF_ID>" + ccNode.Value + "</SHF_ID></ID>"
                        Next
                    Next
                Next
            End If
            Return "<IDS>" + str_Bsu + "</IDS>"
        Else
            Return "<IDS>" + str_Bsu + "</IDS>"
        End If
    End Function

    Function getVehicle_XML()
        Dim i As Integer
        Dim str As String = ""
        With lstVehicle
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    str += "<ID><VEH_ID>" + .Items(i).Value + "</VEH_ID></ID>"
                End If
            Next
        End With

        If str = "" Then
            Return ""
        Else
            Return "<IDS>" + str + "</IDS>"
        End If

    End Function

    

#End Region

    Protected Sub tvBsu_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvBsu.SelectedNodeChanged

    End Sub

    Protected Sub tvBsu_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBsu.TreeNodeCheckChanged

        Select Case ViewState("MainMnu_code")
           
            Case "T000430" 'Vehcle Expiry Report
                BindVehicle()
            Case "T000434" 'Vehicle Details
                BindVehicle()
            Case "T000432"  'Vehicle list Category wise
                BindCategory()
                tvCategory.ExpandAll()
        End Select

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case ViewState("MainMnu_code")
           
            Case "T000430"
                GetVehicleListExpiryReport()
            Case "T000432"
                GetVehicleCategorywise()
            Case "T000434"
                GetVehicleDetails()
        End Select
    End Sub

    

    Protected Sub chkVehicle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkVehicle.CheckedChanged
        If chkVehicle.Checked = True Then
            Dim i As Integer
            For i = 0 To lstVehicle.Items.Count - 1
                lstVehicle.Items(i).Selected = True
            Next
        Else
            lstVehicle.ClearSelection()
        End If
    End Sub
    Private Sub GetVehicleListExpiryReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim params As New Hashtable
        params.Add("@IMG_BSU_ID", Session("SBSUID"))
        params.Add("@IMG_TYPE", "LOGO")
        params.Add("@BSU_ID", Session("SBSUID"))
        params.Add("UserName", Session("sUsr_name"))
        params.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        Dim str_Veh As String
        str_Veh = getVehicle_XML()
        If str_Veh = "" Then
            params.Add("@SCT_XML", DBNull.Value)
        Else
            params.Add("@SCT_XML", str_Veh)
        End If

        Dim str_Bsu As String = ""
        str_Bsu = getBsu_XML()
        If str_Bsu = "" Then
            params.Add("@BSU_XML", DBNull.Value)
        Else
            params.Add("@BSU_XML", str_Bsu)
        End If
        If txtFrom.Text <> "" And txtTo.Text <> "" Then
            params.Add("@FDATE", Format(Date.Parse(txtFrom.Text), "dd/MMM/yyyy"))

            params.Add("@TDATE", Format(Date.Parse(txtTo.Text), "dd/MMM/yyyy"))

        End If
        
        If txtFrom.Text <> "" And txtTo.Text <> "" Then
            params.Add("dateQuery", "For the period From " + Format(Date.Parse(txtFrom.Text), "dd/MMM/yyyy") + " To " + Format(Date.Parse(txtTo.Text), "dd/MMM/yyyy"))
            
        End If

        Dim rptclass As New rptClass

        With rptclass
            .crDatabase = "Oasis_transport"
            .reportParameters = params

            If RdRegistration.Checked Then
                .reportPath = Server.MapPath("../Rpt/rptVehicleListRegExp.rpt")
            ElseIf RdInsurance.Checked Then
                .reportPath = Server.MapPath("../Rpt/rptVehicleListIncExp.rpt")
            End If
        End With
        Session("rptClass") = rptclass
        '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub BindCategory()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        str_query = GetCategoryListQuery()
        'str_query = "SELECT CAT.CAT_ID,CAT.CAT_DESCRIPTION,VEH.VEH_ID,VEH.VEH_REGNO FROM dbo.CATEGORY_M AS CAT" _
        '             & "INNER JOIN dbo.VEHICLE_M AS VEH ON CAT.CAT_ID=VEH.VEH_CAT_ID"

        
        'Dim i As Integer
        'Dim str_Veh As String = ""
        'Dim str_Area As String = ""
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String = ""
        'Dim ntr As Integer
        While reader.Read
            str += reader.GetString(0)

        End While
        Dim xl As New SqlString

        Str = Str.Replace("CAT_ID", "ID")

        Str = Str.Replace("CAT_DESCRIPTION", "TEXT")
        str = str.Replace("VEH_REGNO", "ID")
        str = str.Replace("VEH_ID", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvCategory.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvCategory.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvCategory.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)
        'If Session("sbsuid") <> "900501" And Session("sbsuid") <> "900500" Then
        'tvCategory.ExpandAll()

        'End If
    End Sub

   
    Private Sub GetVehicleCategorywise()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim i As Integer
        Dim IntCategory As Integer
        Dim str_Category As String = ""
        Dim str_query As String = ""

        Dim params As New Hashtable
        params.Add("@IMG_BSU_ID", Session("SBSUID"))
        params.Add("@IMG_TYPE", "LOGO")
        params.Add("@BSU_ID", Session("SBSUID"))
        params.Add("UserName", Session("sUsr_name"))
        params.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        Dim str_Bsu As String = ""
        str_Bsu = getBsu_XML()
        If str_Bsu = "" Then
            params.Add("@BSU_XML", DBNull.Value)
        Else
            params.Add("@BSU_XML", str_Bsu)
        End If


        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim str_Cat As String = getCategory_XML()
        
        If str_Cat <> "" Then
            params.Add("@VEH_XML", str_Cat)
        Else
            params.Add("@VEH_XML", DBNull.Value)
        End If
        Dim rptclass As New rptClass

        With rptclass
            .crDatabase = "Oasis_transport"
            .reportParameters = params
            .reportPath = Server.MapPath("../Rpt/rptVehicleListCategory.rpt")

        End With
        Session("rptClass") = rptclass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Function getCategory_XML()
        
        Dim str_Trip As String = ""

        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        For Each node In tvCategory.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        str_Trip += "<ID><VEH_REGNO>" + ccNode.Value + "</VEH_REGNO></ID>"
                    End If
                Next
            Next
        Next
        If str_Trip = "" Then
            Return ""
        Else
            Return "<IDS>" + str_Trip + "</IDS>"
        End If
    End Function
    Private Sub GetVehicleDetails()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim i As Integer
        Dim IntCategory As Integer
        Dim str_Category As String = ""
        Dim str_query As String = ""

        Dim params As New Hashtable
        params.Add("@IMG_BSU_ID", Session("SBSUID"))
        params.Add("@IMG_TYPE", "LOGO")
        params.Add("@BSU_ID", Session("SBSUID"))
        params.Add("UserName", Session("sUsr_name"))
        params.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        Dim str_Cat As String
        str_Cat = getVehicle_XML()
        If str_Cat = "" Then
            params.Add("@SCT_XML", DBNull.Value)
        Else
            params.Add("@SCT_XML", str_Cat)
        End If


        Dim str_Bsu As String = ""
        str_Bsu = getBsu_XML()
        If str_Bsu = "" Then
            params.Add("@BSU_XML", DBNull.Value)
        Else
            params.Add("@BSU_XML", str_Bsu)
        End If

        Dim rptclass As New rptClass

        With rptclass
            .crDatabase = "Oasis_transport"
            .reportParameters = params
            .reportPath = Server.MapPath("../Rpt/rptVehicleDetails.rpt")

        End With
        Session("rptClass") = rptclass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
    Function GetCategoryListQuery() As String
        Dim str_Bsu1 As String = ""
        Dim str_Bsu As String = ""

        Dim str_query As String = "SELECT CAT_ID,CAT_DESCRIPTION,VEH.VEH_ID,VEH.VEH_REGNO  " & _
        " FROM dbo.CATEGORY_M INNER JOIN dbo.VEHICLE_M AS VEH ON CAT_ID=VEH.VEH_CAT_ID " & _
        "INNER JOIN  dbo.BUSINESSUNIT_M ON BSU_ID=VEH.VEH_BSU_ID "
        '************************business unit **********************
        Dim node As TreeNode

        Dim cNode As TreeNode
        Dim ccNode As TreeNode
        Dim cccNode As TreeNode
        Dim ccccNode As TreeNode
        Dim eFor As Boolean = False

        If ViewState("MainMnu_code") <> "T100290" Then
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    If str_Bsu1 <> "" Then
                        str_Bsu1 += ","
                    End If
                    str_Bsu1 += "'" + cNode.Value + "'"
                    For Each ccNode In cNode.ChildNodes
                        If ccNode.Checked = True Then
                            If str_Bsu <> "" Then
                                str_Bsu += ","
                            End If
                            str_Bsu += "'" + cNode.Value + "'"
                            Exit For
                        End If
                    Next
                Next
            Next
        Else
            For Each node In tvBsu.Nodes
                For Each cNode In node.ChildNodes
                    If str_Bsu1 <> "" Then
                        str_Bsu1 += ","
                    End If
                    str_Bsu1 += "'" + cNode.Value + "'"
                    For Each ccNode In cNode.ChildNodes
                        For Each cccNode In ccNode.ChildNodes
                            For Each ccccNode In cccNode.ChildNodes
                                If ccccNode.Checked = True Then
                                    If str_Bsu <> "" Then
                                        str_Bsu += ","
                                    End If
                                    str_Bsu += "'" + cNode.Value + "'"
                                    eFor = True
                                    Exit For
                                End If
                            Next
                            If eFor = True Then
                                Exit For
                            End If
                        Next
                        If eFor = True Then
                            Exit For
                        End If
                    Next
                    eFor = False
                Next
            Next
        End If

        If str_Bsu = "" Then
            'For Each node In tvBsu.Nodes
            '    For Each cNode In node.ChildNodes
            '        For Each ccNode In cNode.ChildNodes
            '            If str_Bsu <> "" Then
            '                str_Bsu += ","
            '            End If
            '            str_Bsu += "'" + cNode.Value + "'"
            '            Exit For
            '        Next
            '    Next
            'Next
            str_Bsu = str_Bsu1
        End If
        str_query += " WHERE VEH.VEH_BSU_ID IN(" + str_Bsu + ")"
        str_query += " GROUP BY CAT_ID,CAT_DESCRIPTION,VEH.VEH_ID,VEH.VEH_REGNO ORDER BY CAT_DESCRIPTION,VEH.VEH_REGNO FOR XML AUTO "
        Return str_query
    End Function
End Class

