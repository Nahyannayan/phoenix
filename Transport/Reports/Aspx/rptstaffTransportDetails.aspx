<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstaffTransportDetails.aspx.vb" Inherits="Transport_Reports_Aspx_rptstaffTransportDetails" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Employee Transport Details
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="left" style="width: 100%">
                    <tr>
                         <td align="center">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        <%--<td align="left"></td>--%>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="tblRate" runat="server" align="center" style="width: 100%">
                                <tr>
                                    <td align="left"  style="width: 20%"><span class="field-label">Select Business Unit</span>
                                    </td>
                                    <td style="width: 30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBsu_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                 
                                    <td align="left" width="20%"><span class="field-label">Select Employee Category</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlempcategory" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"  ><span class="field-label">Select Bus No</span></td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlBusNo" runat="server" AutoPostBack="True" Width="102px">
                                        </asp:DropDownList></td>
                               
                                    <td align="left"  ><span class="field-label">Journey Type</span></td>
                                    <td align="left"   >
                                        <asp:DropDownList ID="ddljourneytype" runat="server" AutoPostBack="True" Width="102px">
                                            <asp:ListItem Value="0">ALL</asp:ListItem>
                                            <asp:ListItem Value="1">Onward</asp:ListItem>
                                            <asp:ListItem Value="2">Return</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td  colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="groupM1" OnClick="btnGenerateReport_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>--%>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

