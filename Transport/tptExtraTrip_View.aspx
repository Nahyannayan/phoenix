<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptExtraTrip_View.aspx.vb" Inherits="Transport_tptExtraTrip_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

       
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            EXTRA TRIP SETUP
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" style="width: 100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>

                        <td>

                            <table id="Table1" runat="server" align="center" style="width: 100%">

                                <tr>
                                    <td>

                                        <table id="Table2" runat="server" align="center" style="width: 100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                                <td align="right">
                                                    <asp:LinkButton ID="lbHistory" runat="server" Font-Bold="True" Text="Show History"></asp:LinkButton>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="2" align="center" style="width: 100%">
                                                    <asp:GridView CssClass="table table-bordered table-row" ID="gvTptTripAlloc" runat="server"
                                                        AutoGenerateColumns="False"
                                                        PageSize="20" AllowPaging="True" Width="100%">
                                                        <Columns>


                                                            <asp:TemplateField HeaderText="trd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrdId" runat="server" Text='<%# Bind("TRD_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Trips">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lbltp" runat="server" CssClass="gridheader_text" Text="Trip"></asp:Label><br />
                                                                    <asp:TextBox ID="txtTrip" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnTrip_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnTrip_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrip" runat="server" Text='<%# Bind("TRP_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Vehicle">
                                                                <HeaderTemplate>
                                                                    Vehicle<br />
                                                                    <asp:DropDownList ID="ddlgvVehicle" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvVehicle_SelectedIndexChanged" Width="75%">
                                                                    </asp:DropDownList></td>
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblVehicle" runat="server" Text='<%# Bind("VEH_REGNO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Catergory">
                                                                <HeaderTemplate>
                                                                    Category<br />
                                                                    <asp:DropDownList ID="ddlgvCategory" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvCategory_SelectedIndexChanged" Width="75%">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCat" runat="server" Text='<%# Bind("CAT_DESCRIPTION") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Driver">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblDri" runat="server" CssClass="gridheader_text" Text="Driver"></asp:Label><br />
                                                                    <asp:TextBox ID="txtDriver" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnDriver_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnDriver_Search_Click" /></td>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDriver" runat="server" Text='<%# Bind("DRV_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Conductor">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblCon" runat="server" CssClass="gridheader_text" Text="Conductor"></asp:Label><br />
                                                                    <asp:TextBox ID="txtConductor" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnConductor_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnConductor_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConductor" runat="server" Text='<%# Bind("CON_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Purpose">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Purpose"></asp:Label><br />
                                                                    <asp:TextBox ID="txtPurpose" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnPurpose_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnConductor_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPurpose" runat="server" Text='<%# Bind("PSE_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Remarks">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("TRD_REMARKS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="From">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFrom" runat="server" Text='<%# Bind("TRD_FROMDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="To">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTo" runat="server" Text='<%# Bind("TRD_TODATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>



                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>

                                                        </Columns>
                                                        <RowStyle CssClass="griditem"   />
                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <SelectedRowStyle BackColor="Aqua" />
                                                        <PagerStyle Font-Names="Verdana" HorizontalAlign="Left" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3"
                                runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4"
                                runat="server" type="hidden" value="=" />
                        </td>
                    </tr>

                </table>
              
                <script type="text/javascript">


                    cssdropdown.startchrome("Div1")
                    cssdropdown.startchrome("Div2")
                    cssdropdown.startchrome("Div3")
                    cssdropdown.startchrome("Div4")


                </script>
            </div>
        </div>
    </div>

</asp:Content>

