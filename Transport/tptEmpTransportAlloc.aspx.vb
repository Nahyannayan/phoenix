Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_tptEmpTransportAlloc
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100190") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If

                'Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page
                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ViewState("datamode") = "add"

                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

                set_Menu_Img()
                BindCategory()
                BindGridView("")
                'GridBind()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else


        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")

            smScriptManager.EnablePartialRendering = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindGridView(ByVal Filter As String)

        ' To bind the Gridview with Data values..
        Dim str_Conn = ConnectionManger.GetOASISConnectionString
        'Dim strQuery As String = "SELECT EMP_ID,EMPNO, ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') AS EMPNAME," & _
        '        "EMP_DEPT_DESCR, EMP_DES_DESCR, CATEGORY_DESC, EMP_RLG_ID, EMP_CNTR_DESCR, 0 'Tran Alloted'," & _
        '        "'' As 'TRIP','' As 'AREA','' As 'PICKUP' " & _
        '        "FROM   vw_OSO_EMPLOYEEMASTER WHERE EMP_BSU_ID='" & Session("sBsuid") & "'"

        'strQuery = "SELECT EMP.EMP_ID,EMPNO, ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') AS EMPNAME, " & _
        '            "EMP_DEPT_DESCR, EMP_DES_DESCR, CATEGORY_DESC, EMP_RLG_ID, EMP_CNTR_DESCR, " & _
        '            " 0 'Tran Alloted','' As 'TRIP','' As 'AREA','' As 'PICKUP',PNT.PNT_ID,PNT.PNT_DESCRIPTION,SBL.SBL_DESCRIPTION,SBL.SBL_ID " & _
        '            "FROM   vw_OSO_EMPLOYEEMASTER EMP " & _
        '            "LEFT OUTER JOIN [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP ON EMP.EMP_ID=ETRIP.EMP_ID " & _
        '            "LEFT OUTER JOIN [oasis_transpORT].TRANSPORT.SUBLOCATION_M SBL ON ETRIP.SBL_ID=SBL.SBL_ID " & _
        '            "LEFT OUTER JOIN [oasis_transpORT].TRANSPORT.PICKUPPOINTS_M PNT ON ETRIP.POINT_ID=PNT.PNT_ID " & _
        '            "WHERE emp.EMP_BSU_ID='" & Session("sBsuid") & "'"
        ''changed DISTINCT to TOP 1 by nahyan on 14mar2017 #76740 --error subquery returns more than one val
        Dim strQuery As String = "SELECT distinct (ETRIP.EMP_ID)As EMPID,EMP.EMP_ID,EMPNO, ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') AS EMPNAME," & _
                "EMP_DEPT_DESCR, EMP_DES_DESCR, CATEGORY_DESC, EMP_RLG_ID, EMP_CNTR_DESCR," & _
                    "(SELECT TOP 1 SBL.SBL_DESCRIPTION FROM [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP " & _
                    "RIGHT OUTER JOIN [oasis_transpORT].TRANSPORT.SUBLOCATION_M SBL ON ETRIP.SBL_ID=SBL.SBL_ID " & _
                    "WHERE ETRIP.EMP_ID=EMP.EMP_ID AND TPT_JOURNY='Onward' )AS 'AREAOWN', " & _
                    "(SELECT TOP 1 PNT.PNT_DESCRIPTION FROM [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP " & _
                    "RIGHT OUTER JOIN [oasis_transpORT].TRANSPORT.PICKUPPOINTS_M PNT ON ETRIP.POINT_ID=PNT.PNT_ID  " & _
                    "WHERE ETRIP.EMP_ID=EMP.EMP_ID AND TPT_JOURNY='Onward')AS 'PICKUPOWN', " & _
                    "(SELECT TOP 1 TRP.TRP_DESCR FROM [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP " & _
                    "RIGHT OUTER JOIN [oasis_transpORT].TRANSPORT.TRIPS_M TRP ON ETRIP.TRIP_ID=TRP.TRP_ID " & _
                    "WHERE EMP.EMP_ID=ETRIP.EMP_ID AND TPT_JOURNY='Onward')AS 'TRPOWN', " & _
                    "(SELECT TOP 1 SBL.SBL_DESCRIPTION FROM [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP " & _
                    "RIGHT OUTER JOIN [oasis_transpORT].TRANSPORT.SUBLOCATION_M SBL ON ETRIP.SBL_ID=SBL.SBL_ID " & _
                    "WHERE ETRIP.EMP_ID=EMP.EMP_ID AND TPT_JOURNY='Return' )AS 'AREARTN'," & _
                    "(SELECT TOP 1 PNT.PNT_DESCRIPTION FROM [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP " & _
                    "RIGHT OUTER JOIN [oasis_transpORT].TRANSPORT.PICKUPPOINTS_M PNT ON ETRIP.POINT_ID=PNT.PNT_ID  " & _
                    "WHERE ETRIP.EMP_ID=EMP.EMP_ID AND TPT_JOURNY='Return')AS 'PICKUPRTN', " & _
                    "(SELECT TOP 1 TRP.TRP_DESCR FROM [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP " & _
                    "RIGHT OUTER JOIN [oasis_transpORT].TRANSPORT.TRIPS_M TRP ON ETRIP.TRIP_ID=TRP.TRP_ID " & _
                    "WHERE EMP.EMP_ID=ETRIP.EMP_ID AND  TPT_JOURNY='Return')AS 'TRPRTN'  " & _
                "FROM   vw_OSO_EMPLOYEEMASTER EMP " & _
                "LEFT OUTER JOIN [oasis_transpORT].TRANSPORT.TPTEMPLOYEE_TRIP ETRIP ON EMP.EMP_ID=ETRIP.EMP_ID  " & _
                "LEFT OUTER JOIN [oasis_transpORT].TRANSPORT.SUBLOCATION_M SBL ON ETRIP.SBL_ID=SBL.SBL_ID " & _
                "LEFT OUTER JOIN [oasis_transpORT].TRANSPORT.PICKUPPOINTS_M PNT ON ETRIP.POINT_ID=PNT.PNT_ID " & _
                "WHERE emp.EMP_BSU_ID='" & Session("sBsuid") & "'  AND EMP.EMP_bACTIVE=1 " & Filter

        ''"PNT.PNT_ID,PNT.PNT_DESCRIPTION,SBL.SBL_DESCRIPTION " & _

        'AND PNT_DESCRIPTION LIKE '%AL DUARAH BUILDING%'"
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim StaffNameSearch As String = ""
        Dim StaffNoSearch As String = ""
        Dim CategorySearch As String = ""
        Dim AreaSearch As String = ""
        Dim TripSearch As String = ""
        Dim PickUpSearch As String = ""

        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""

        Dim txtEmpNo As New TextBox
        Dim txtEmpName As New TextBox
        Dim txtArea As New TextBox
        Dim txtPick As New TextBox
        Dim txttrip As New TextBox

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""
        Dim txtSearch As New TextBox

        If gvStudTPT.Rows.Count > 0 Then
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStaffNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("EMPNO", txtSearch.Text, strSearch)
            StaffNoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStaffName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'')", txtSearch.Text, strSearch)
            StaffNameSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtArea1")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SBL_DESCRIPTION", txtSearch.Text, strSearch)
            AreaSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtPickUp1")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("PNT_DESCRIPTION", txtSearch.Text, strSearch)
            PickUpSearch = txtSearch.Text


            ''Return Search ..........
            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtArea2")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SBL_DESCRIPTION", txtSearch.Text, strSearch)
            AreaSearch = txtSearch.Text

            'txtSearch = New TextBox
            'txtSearch = gvStudTPT.HeaderRow.FindControl("txtPickUp2")
            'strSidsearch = h_Selected_menu_2.Value.Split("__")
            'strSearch = strSidsearch(0)
            'strFilter += GetSearchString("PNT_DESCRIPTION", txtSearch.Text, strSearch)
            'PickUpSearch = txtSearch.Text


            'txtSearch = New TextBox
            'txtSearch = gvStudTPT.HeaderRow.FindControl("txtTrip1")
            'strSidsearch = h_Selected_menu_2.Value.Split("__")
            'strSearch = strSidsearch(0)
            'strFilter += GetSearchString("TRIP", txtSearch.Text, strSearch)
            'TripSearch = txtSearch.Text


            If strFilter.Trim <> "" Then
                strQuery = strQuery + strFilter
            End If
            'Else
            'str_query += " and stu_pickup_trp_id is not null and stu_dropoff_trp_id is not null"

        End If

        strQuery += " ORDER BY EMPNO"

        Dim dsEmpTrip As DataSet
        dsEmpTrip = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)
        gvStudTPT.DataSource = dsEmpTrip
        gvStudTPT.DataBind()

        If dsEmpTrip.Tables(0).Rows.Count = 0 Then
            dsEmpTrip.Tables(0).Rows.Add(dsEmpTrip.Tables(0).NewRow())
            gvStudTPT.DataBind()
            Dim columnCount As Integer = gvStudTPT.Rows(0).Cells.Count
            gvStudTPT.Rows(0).Cells.Clear()
            gvStudTPT.Rows(0).Cells.Add(New TableCell)
            gvStudTPT.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudTPT.DataBind()
        End If

        'For Each row As GridViewRow In gvQuestions.Rows
        '    Dim strval As String = DirectCast(row.FindControl("HiddenGType"), HiddenField).Value
        '    If strval = "Add On" Then
        '        DirectCast(row.FindControl("lnkview"), LinkButton).Enabled = False
        '    End If
        'Next
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

#Region " Private Methods and Functions "
    Private Function isPageExpired()

    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

#End Region

    Public Function returnpath(ByVal p_posted As String) As String
        Try
            Dim b_posted As String = p_posted
            If p_posted = "1" Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try
        'returnpath(Container.DataItem("ITF_bPOSTED"))
    End Function

    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudTPT.PageIndexChanging
        Try
            gvStudTPT.PageIndex = e.NewPageIndex
            BindGridView("")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudTPT.RowCommand
        Try
            Dim EncDec As New Encryption64
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudTPT.Rows(index), GridViewRow)

            Dim lblEmpId As Label
            With selectedRow
                lblEmpId = .FindControl("lblEmpId")
            End With

            Dim EMPId As String = lblEmpId.Text
            EMPId = EncDec.Encrypt(EMPId)
            If e.CommandName = "view" Then
                If EMPId.Length >= 0 Then
                    'mInfo = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString()
                    Response.Redirect("tptEmpTransportAlloc_New.aspx?EmpId=" & EMPId & "&MainMnu_code=" & Encr_decrData.Encrypt(ViewState("MainMnu_code").ToString()))
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvStudTPT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudTPT.RowDataBound
        Try
            Dim ingTick As New Image

            'Dim lblEmpId As New Label
            'lblEmpId = TryCast(e.Row.FindControl("lblEmpId"), Label)

            'Dim lblTrip As New Label
            'lblTrip = TryCast(e.Row.FindControl("lblTrip"), Label)
            'lblTrip.Text = GetTrip(CInt(lblEmpId.Text))

            'Dim lblArea As New Label
            'lblArea = TryCast(e.Row.FindControl("lblArea"), Label)
            'lblArea.Text = GetArea(CInt(lblEmpId.Text))

            'Dim lblPickUp As New Label
            'lblPickUp = TryCast(e.Row.FindControl("lblPickup"), Label)
            'lblPickUp.Text = GetPickUppoint(CInt(lblEmpId.Text))

            'If lblPickUp.Text <> "" Then
            '    '"~/Images/tick.gif"
            '    ingTick = TryCast(e.Row.FindControl("imgPick"), Image)
            '    ingTick.ImageUrl = "~/Images/tick.gif"
            '    '
            'End If

        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetTrip(ByVal EMPID As Double) As String
        Try
            Dim str_Conn = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim strQuery As String = "SELECT TRP_DESCR FROM TRANSPORT.VV_TRIPS_D WHERE TRD_ID=" & _
                        "(SELECT TRIP_ID FROM TRANSPORT.TPTEMPLOYEE_TRIP WHERE EMP_ID=" & EMPID & ")"

            Dim dsEmpTrip As DataSet
            dsEmpTrip = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)
            If dsEmpTrip.Tables(0).Rows.Count >= 1 Then
                Return dsEmpTrip.Tables(0).Rows(0).Item("TRP_DESCR").ToString
            Else
                Return ""
            End If

        Catch ex As Exception

        End Try
    End Function

    Private Function GetArea(ByVal EMPID As Double) As String
        Try
            Dim str_Conn = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim strQuery As String = "SELECT EMP_ID,ET.SBL_ID,SL.SBL_DESCRIPTION,POINT_ID,TRIP_ID FROM TRANSPORT.TPTEMPLOYEE_TRIP ET " & _
                " INNER JOIN TRANSPORT.SUBLOCATION_M  SL ON ET.SBL_ID=SL.SBL_ID" & _
                " INNER JOIN TRANSPORT.PICKUPPOINTS_M B ON SL.SBL_ID=B.PNT_SBL_ID" & _
                " WHERE ET.EMP_ID =" & EMPID

            Dim dsArea As DataSet
            dsArea = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)
            If dsArea.Tables(0).Rows.Count >= 1 Then
                Return dsArea.Tables(0).Rows(0).Item("SBL_DESCRIPTION").ToString
            Else
                Return ""
            End If

        Catch ex As Exception

        End Try
    End Function

    Private Function BindCategory()
        Try
            Dim str_Conn = ConnectionManger.GetOASISConnectionString
            Dim strQuery As String = "SELECT ECT_ID,ECT_DESCR FROM EMPCATEGORY_M "

            Dim dsCategory As DataSet
            dsCategory = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                ddlEmpCategory.DataSource = dsCategory
                ddlEmpCategory.DataTextField = "ECT_DESCR"
                ddlEmpCategory.DataValueField = "ECT_ID"
                ddlEmpCategory.DataBind()
                Dim li As New ListItem
                li.Text = "ALL"
                li.Value = "0"
                ddlEmpCategory.Items.Insert(0, li)
            End If

        Catch ex As Exception

        End Try
    End Function

    Private Function GetPickUppoint(ByVal EMPID As Double) As String
        Try
            Dim str_Conn = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim strQuery As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M " & _
                        "WHERE PNT_ID=(SELECT POINT_ID FROM TRANSPORT.TPTEMPLOYEE_TRIP WHERE EMP_ID=" & EMPID & ")"

            Dim dsPickUp As DataSet
            dsPickUp = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)
            If dsPickUp.Tables(0).Rows.Count >= 1 Then
                Return dsPickUp.Tables(0).Rows(0).Item("PNT_DESCRIPTION").ToString
            Else
                Return ""
            End If

        Catch ex As Exception

        End Try

    End Function

    Protected Sub btnSearchstaffNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchstaffName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchTrip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            'BindGridView()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchPickup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchcategory_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStudTPT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim strFilter As String

            If ddlEmpCategory.SelectedValue <> "0" Then
                strFilter = " AND EMP_ECT_ID=" & ddlEmpCategory.SelectedValue & " "
            End If

            If ddlAlloted.SelectedValue = "Yes" Then
                strFilter = strFilter & " AND NOT ETRIP.TPT_JOURNY IS NULL"
            ElseIf ddlAlloted.SelectedValue = "No" Then
                strFilter = strFilter & " AND  ETRIP.TPT_JOURNY IS NULL"
            End If

            BindGridView(strFilter)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearchArea1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchTrip1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchPickup1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchArea2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchTrip2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearchPickup2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGridView("")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
