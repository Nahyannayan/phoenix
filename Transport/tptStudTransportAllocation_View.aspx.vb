Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptStudTransportAllocation_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100180") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    PopulateAcademicYear()
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    BindSublocation()
                    BindPickup()

                    GridBind()

                    If ddlClm.Items.Count = 1 Then
                        Table2.Rows(0).Visible = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub


    Protected Sub ddlgvTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvJourney_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvBus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvVehicle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvTptTripAlloc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTptTripAlloc.PageIndexChanging
        Try
            gvTptTripAlloc.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTptTripAlloc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTptTripAlloc.RowCommand
        Try

            If e.CommandName = "edit" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvTptTripAlloc.Rows(index), GridViewRow)

                Dim lblTrdId As Label
                Dim lblShfId As Label
                Dim lblJourney As Label
                Dim lblAvailable As Label
                Dim lblTrip As Label
                Dim lblBusId As Label
                Dim url As String
                lblTrdId = selectedRow.FindControl("lblTrdId")
                lblShfId = selectedRow.FindControl("lblShfId")
                lblJourney = selectedRow.FindControl("lblJourney")
                lblAvailable = selectedRow.FindControl("lblAvailable")
                lblTrip = selectedRow.FindControl("lblTrip")
                lblBusId = selectedRow.FindControl("lblBusId")


                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Transport\tptstudTransportAllocation_M.aspx?MainMnu_code={0}&datamode={1}&trdId=" + Encr_decrData.Encrypt(lblTrdId.Text) _
                      & "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text) + "&journey=" + Encr_decrData.Encrypt(lblJourney.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                      & "&seats=" + Encr_decrData.Encrypt(lblAvailable.Text) + "&trip=" + Encr_decrData.Encrypt(lblTrip.Text) + "&busno=" + Encr_decrData.Encrypt(lblBusId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvTptTripAlloc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTptTripAlloc.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lbltrpid As New Label
                lbltrpid = e.Row.FindControl("lblTrdId")
                Dim lblCap As Label
                Dim lblAvailable As Label
                Dim lblJourney As Label
                Dim lblTrip As Label
                Dim lblShfId As Label

                lblCap = e.Row.FindControl("lblCap")
                lblAvailable = e.Row.FindControl("lblAvailable")
                lblJourney = e.Row.FindControl("lblJourney")
                Dim gvPickup As New GridView
                gvPickup = e.Row.FindControl("gvPickUp")
                lblTrip = e.Row.FindControl("lblTrip")
                If gvPickup.Rows.Count = 0 Then
                    lblShfId = e.Row.FindControl("lblShfId")
                    If lbltrpid.Text <> "" Then
                        BindPickup(gvPickup, lbltrpid.Text.ToString)
                        BindSeats(lbltrpid.Text, lblCap, lblAvailable, lblJourney)
                    End If

                    'If lbltrpid.Text <> "" Then
                    '    If isStudentwaiting(lbltrpid.Text, lblShfId.Text, lblJourney.Text) = True Then
                    '        '    lblTrip.Text += " <span style=""font-size: 8pt; color: #800000"">*</span>"
                    '        e.Row.BackColor = Drawing.Color.LightSalmon
                    '    Else
                    '        e.Row.Visible = False
                    '    End If
                    'End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnTrip_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
   
#Region "Private Methods"
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTptTripAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptTripAlloc.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub PopulateAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=" + ddlClm.SelectedValue.ToString _
                                  & " AND ACD_ACY_ID>=" + Session("Current_ACY_ID")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()
    End Sub
    Sub BindSeats(ByVal trdId As String, ByVal lblCap As Label, ByVal lblAvailable As Label, ByVal lblJourney As Label)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.getSEATCAPACITY " _
                              & "'" + Session("sbsuid") + "'," _
                              & trdId + "," _
                              & ddlAcademicYear.SelectedValue.ToString + "," _
                              & Session("Current_ACY_ID") + "," _
                              & "'" + lblJourney.Text + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            lblCap.Text = reader.GetValue(0).ToString
            lblAvailable.Text = reader.GetValue(1).ToString
        End While

        reader.Close()
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Private Sub GridBind(Optional ByVal history As Boolean = False)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim str_query As String
        Dim str_query1 As String
        Dim str_query2 As String
        'If ddlPickUp.SelectedValue = "0" Then
        '    If ddlSubLocation.SelectedValue = "0" Then
        '        str_query = "SELECT DISTINCT GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
        '                   & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
        '                   & " FROM TRANSPORT.VV_TRIPS_D WHERE " _
        '                   & " TRD_TODATE IS NULL AND TRD_BSU_ID='" + Session("SBSUID") + "' AND TRP_JOURNEY<>'EXTRA'"
        '    Else
        '        str_query = "SELECT DISTINCT A.GUID AS GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
        '                    & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
        '                    & " FROM TRANSPORT.VV_TRIPS_D AS A INNER JOIN TRANSPORT.TRIPS_PICKUP_S  AS B " _
        '                    & " ON A.TRD_ID=B.TPP_TRD_ID" _
        '                    & " WHERE  TRD_TODATE IS NULL AND TRD_BSU_ID='" + Session("SBSUID") + "'" _
        '                    & " AND TPP_SBL_ID='" + ddlSubLocation.SelectedValue.ToString + "' AND TRP_JOURNEY<>'EXTRA'"
        '    End If

        'Else
        '    str_query = "SELECT DISTINCT A.GUID AS GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
        '               & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
        '               & " FROM TRANSPORT.VV_TRIPS_D AS A " _
        '               & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.TRD_ID=B.TPP_TRD_ID" _
        '               & " WHERE TRD_TODATE Is NULL And TPP_PNT_ID = " + ddlPickUp.SelectedValue.ToString _
        '               & " AND TRD_BSU_ID='" + Session("SBSUID") + "' AND TRP_JOURNEY<>'EXTRA'"
        'End If


        If ddlPickUp.SelectedValue = "0" Then
            If ddlSubLocation.SelectedValue = "0" Then
                str_query1 = "SELECT DISTINCT TRD_ID AS  GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
                           & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
                           & " FROM TRANSPORT.VV_TRIPS_D AS A WHERE " _
                           & " TRD_TODATE IS NULL AND TRD_BSU_ID='" + Session("SBSUID") + "' AND TRP_JOURNEY='ONWARD'"

                str_query2 = "SELECT DISTINCT TRD_ID AS GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
                          & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
                          & " FROM TRANSPORT.VV_TRIPS_D AS A WHERE " _
                          & " TRD_TODATE IS NULL AND TRD_BSU_ID='" + Session("SBSUID") + "' AND TRP_JOURNEY='RETURN'"

            Else
                str_query1 = "SELECT DISTINCT TRD_ID AS GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
                            & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
                            & " FROM TRANSPORT.VV_TRIPS_D AS A INNER JOIN TRANSPORT.TRIPS_PICKUP_S  AS B " _
                            & " ON A.TRD_ID=B.TPP_TRD_ID" _
                            & " WHERE  TRD_TODATE IS NULL AND TRD_BSU_ID='" + Session("SBSUID") + "'" _
                            & " AND TPP_SBL_ID='" + ddlSubLocation.SelectedValue.ToString + "' AND TRP_JOURNEY='ONWARD'"

                str_query2 = "SELECT DISTINCT TRD_ID AS GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
                       & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
                       & " FROM TRANSPORT.VV_TRIPS_D AS A INNER JOIN TRANSPORT.TRIPS_PICKUP_S  AS B " _
                       & " ON A.TRD_ID=B.TPP_TRD_ID" _
                       & " WHERE  TRD_TODATE IS NULL AND TRD_BSU_ID='" + Session("SBSUID") + "'" _
                       & " AND TPP_SBL_ID='" + ddlSubLocation.SelectedValue.ToString + "' AND TRP_JOURNEY='RETURN'"


            End If

        Else
            str_query1 = "SELECT DISTINCT TRD_ID AS GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
                       & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
                       & " FROM TRANSPORT.VV_TRIPS_D AS A " _
                       & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.TRD_ID=B.TPP_TRD_ID" _
                       & " WHERE TRD_TODATE Is NULL And TPP_PNT_ID = " + ddlPickUp.SelectedValue.ToString _
                       & " AND TRD_BSU_ID='" + Session("SBSUID") + "' AND TRP_JOURNEY='ONWARD'"

            str_query2 = "SELECT DISTINCT TRD_ID AS GUID,TRD_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR," _
            & " TRP_JOURNEY,SHF_DESCR,SHF_ID  " _
            & " FROM TRANSPORT.VV_TRIPS_D AS A " _
            & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.TRD_ID=B.TPP_TRD_ID" _
            & " WHERE TRD_TODATE Is NULL And TPP_PNT_ID = " + ddlPickUp.SelectedValue.ToString _
            & " AND TRD_BSU_ID='" + Session("SBSUID") + "' AND TRP_JOURNEY='RETURN'"

        End If

        'SELECT THOSE TRIPS 
        str_query1 += " AND (SELECT COUNT(STU_ID) FROM STUDENT_M  AS P " _
                        & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON P.STU_PICKUP=B.TPP_PNT_ID" _
                        & " WHERE  STU_PICKUP_TRP_ID IS NULL AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND TPP_TRD_ID=A.TRD_ID" _
                        & " AND STU_SHF_ID=A.SHF_ID" _
                        & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                        & " AND STU_CURRSTATUS<>'CN')>0"


        str_query2 += " AND (SELECT COUNT(STU_ID) FROM STUDENT_M  AS P " _
                      & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON P.STU_DROPOFF=B.TPP_PNT_ID" _
                      & " WHERE  STU_DROPOFF_TRP_ID IS NULL AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND TPP_TRD_ID=A.TRD_ID" _
                      & " AND STU_SHF_ID=A.SHF_ID" _
                      & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                      & " AND STU_CURRSTATUS<>'CN')>0"

        str_query = str_query1 + " UNION ALL " + str_query2


        Dim txtTrip As New TextBox
        Dim ddlgvVehicle As New DropDownList
        Dim ddlgvShift As New DropDownList
        Dim ddlgvJourney As New DropDownList
        Dim ddlgvBus As New DropDownList


        Dim selectedTrips As String = ""
        Dim selectedVehicle As String = ""
        Dim selectedShift As String = ""
        Dim selectedJourney As String = ""
        Dim selectedBus As String = ""

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim txtSearch As New TextBox
        Dim trpSearch As String = ""
        If gvTptTripAlloc.Rows.Count > 0 Then

            txtSearch = New TextBox
            txtSearch = gvTptTripAlloc.HeaderRow.FindControl("txtTrip")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("TRP_DESCR", txtSearch.Text.Replace("'", "''"), strSearch)
            trpSearch = txtSearch.Text



            ddlgvVehicle = gvTptTripAlloc.HeaderRow.FindControl("ddlgvVehicle")
            If ddlgvVehicle.Text <> "ALL" Then
                strFilter += " and VEH_REGNO='" + ddlgvVehicle.Text + "'"
                selectedVehicle = ddlgvVehicle.Text
            End If

            ddlgvJourney = gvTptTripAlloc.HeaderRow.FindControl("ddlgvJourney")
            If ddlgvJourney.Text <> "ALL" Then
                strFilter += " and TRP_JOURNEY='" + ddlgvJourney.Text + "'"
                selectedJourney = ddlgvJourney.Text
            End If

            ddlgvShift = gvTptTripAlloc.HeaderRow.FindControl("ddlgvShift")
            If ddlgvShift.Text <> "ALL" Then
                strFilter += " and SHF_DESCR='" + ddlgvShift.Text + "'"
                selectedShift = ddlgvShift.Text
            End If

            ddlgvBus = gvTptTripAlloc.HeaderRow.FindControl("ddlgvBus")
            If ddlgvBus.Text <> "ALL" Then
                strFilter += " and BNO_DESCR='" + ddlgvBus.Text + "'"
                selectedBus = ddlgvBus.Text
            End If



            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If

        str_query += " ORDER BY TRP_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptTripAlloc.DataSource = ds


        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTptTripAlloc.DataBind()
            Dim columnCount As Integer = gvTptTripAlloc.Rows(0).Cells.Count
            gvTptTripAlloc.Rows(0).Cells.Clear()
            gvTptTripAlloc.Rows(0).Cells.Add(New TableCell)
            gvTptTripAlloc.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTptTripAlloc.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTptTripAlloc.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTptTripAlloc.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        If gvTptTripAlloc.Rows.Count > 0 Then
              ddlgvShift = gvTptTripAlloc.HeaderRow.FindControl("ddlgvShift")
            ddlgvJourney = gvTptTripAlloc.HeaderRow.FindControl("ddlgvJourney")
            ddlgvVehicle = gvTptTripAlloc.HeaderRow.FindControl("ddlgvVehicle")
            ddlgvBus = gvTptTripAlloc.HeaderRow.FindControl("ddlgvBus")

            Dim dr As DataRow

       
            ddlgvShift.Items.Clear()
            ddlgvJourney.Items.Clear()
            ddlgvVehicle.Items.Clear()
            ddlgvBus.Items.Clear()
            ddlgvBus.Items.Clear()
           
            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr

                    If ddlgvShift.Items.FindByText(.Item(6)) Is Nothing Then
                        ddlgvShift.Items.Add(.Item(6))
                    End If

                    If ddlgvJourney.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvJourney.Items.Add(.Item(5))
                    End If

                    If ddlgvVehicle.Items.FindByText(.Item(2)) Is Nothing Then
                        ddlgvVehicle.Items.Add(.Item(2))
                    End If

                    If ddlgvBus.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvBus.Items.Add(.Item(4))
                    End If
                End With
            Next


            If ddlgvBus.Items.Count <> 0 Then
                studClass.SortDropDown(ddlgvBus)
            End If

            If ddlgvVehicle.Items.Count <> 0 Then
                studClass.SortDropDown(ddlgvVehicle)
            End If

            If ddlgvShift.Items.Count <> 0 Then
                studClass.SortDropDown(ddlgvShift)
            End If

            ddlgvShift.Items.Insert(0, "ALL")
            ddlgvJourney.Items.Insert(0, "ALL")
            ddlgvVehicle.Items.Insert(0, "ALL")
            ddlgvBus.Items.Insert(0, "ALL")

            txtSearch = New TextBox
            txtSearch = gvTptTripAlloc.HeaderRow.FindControl("txtTrip")
            txtSearch.Text = trpSearch

            If selectedShift <> "" Then
                ddlgvShift.Text = selectedShift
            End If
            If selectedBus <> "" Then
                ddlgvBus.Text = selectedBus
            End If
            If selectedVehicle <> "" Then
                ddlgvVehicle.Text = selectedVehicle
            End If
            If selectedJourney <> "" Then
                ddlgvJourney.Text = selectedJourney
            End If
        End If

    End Sub

    Sub BindPickup(ByVal gvPickup As GridView, ByVal trpid As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT SBL_DESCRIPTION,PNT_DESCRIPTION,TPP_TIME FROM  TRANSPORT.PICKUPPOINTS_M AS B INNER JOIN" _
                                  & " TRANSPORT.TRIPS_PICKUP_S AS A ON B.PNT_ID = A.TPP_PNT_ID " _
                                  & " INNER JOIN TRANSPORT.SUBLOCATION_M AS C ON B.PNT_SBL_ID=C.SBL_ID" _
                                  & " WHERE TPP_TRD_ID='" + trpid + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvPickup.DataSource = ds
        gvPickup.DataBind()
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function


    Sub BindSublocation()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION " _
                                 & " FROM TRANSPORT.SUBLOCATION_M AS A " _
                                 & "  INNER JOIN TRANSPORT.PICKUPPOINTS_M  AS B ON A.SBL_ID=B.PNT_SBL_ID" _
                                 & " WHERE PNT_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubLocation.DataSource = ds
        ddlSubLocation.DataTextField = "SBL_DESCRIPTION"
        ddlSubLocation.DataValueField = "SBL_ID"
        ddlSubLocation.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSubLocation.Items.Insert(0, li)
    End Sub


    Sub BindPickup()

        Dim li As New ListItem

        If Not ddlSubLocation.SelectedValue = 0 Then
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M WHERE PNT_SBL_ID=" + ddlSubLocation.SelectedValue.ToString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlPickUp.DataSource = ds
            ddlPickUp.DataTextField = "PNT_DESCRIPTION"
            ddlPickUp.DataValueField = "PNT_ID"
            ddlPickUp.DataBind()

            li.Text = "ALL"
            li.Value = "0"
            ddlPickUp.Items.Insert(0, li)
        Else
            li.Text = "ALL"
            li.Value = "0"
            ddlPickUp.Items.Add(li)
        End If
    End Sub

    Function isStudentwaiting(ByVal trd_id As String, ByVal shf_id As String, ByVal journey As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = "SELECT COUNT(STU_ID) FROM STUDENT_M  AS A " _
                        & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.STU_PICKUP=B.TPP_PNT_ID" _
                        & " WHERE  STU_PICKUP_TRP_ID IS NULL AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND TPP_TRD_ID=" + trd_id _
                        & " AND STU_SHF_ID=" + shf_id _
                        & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                        & " AND STU_CURRSTATUS<>'CN'"
        Else
            str_query = "SELECT COUNT(STU_ID) FROM STUDENT_M  AS A " _
                      & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.STU_DROPOFF=B.TPP_PNT_ID" _
                      & " WHERE  STU_DROPOFF_TRP_ID IS NULL AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND TPP_TRD_ID=" + trd_id _
                      & " AND STU_SHF_ID=" + shf_id _
                      & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                      & " AND STU_CURRSTATUS<>'CN'"
        End If

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

#End Region

    Protected Sub ddlSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        Try
            BindPickup()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlPickUp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPickUp.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        Try
            PopulateAcademicYear()
            BindSublocation()
            BindPickup()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
