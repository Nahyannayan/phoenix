﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Transport_tptVehicle_Allocation_View2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindGrid(False)
        End If

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

    Public Sub BindGrid(ByVal export As Boolean)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTransportConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim emp_no As String = String.Empty
            Dim emp_name As String = String.Empty
            Dim desg As String = String.Empty

            Dim work_bsu As String = String.Empty
            Dim visa_bsu As String = String.Empty
            Dim alloc_bsu As String = String.Empty
            Dim BSU_ID As String = Session("sBsuid")

            If GridInfo.Rows.Count > 0 Then

                emp_no = DirectCast(GridInfo.HeaderRow.FindControl("txt1"), TextBox).Text.Trim()
                emp_name = DirectCast(GridInfo.HeaderRow.FindControl("txt2"), TextBox).Text.Trim()
                desg = DirectCast(GridInfo.HeaderRow.FindControl("txt3"), TextBox).Text.Trim()
                work_bsu = DirectCast(GridInfo.HeaderRow.FindControl("txt4"), TextBox).Text.Trim()
                visa_bsu = DirectCast(GridInfo.HeaderRow.FindControl("txt5"), TextBox).Text.Trim()
                alloc_bsu = DirectCast(GridInfo.HeaderRow.FindControl("txt6"), TextBox).Text.Trim()

            End If

            If BSU_ID = "900501" Then
                str_Sql = "Select   EMPNO,EMP_STAFFNO,isNULL(EMP_FName,'')+' '+isNULL(EMP_MName,'')+' '+isNULL(EMP_LName,'') as EmpName,DES_DESCR, " & _
                          " B.BSU_NAME as WorkBSU, " & _
                          " C.BSU_NAME as VisaBSU, " & _
                          " K.BSU_NAME as AllocBSU " & _
                          " From OASIS..Employee_M  A " & _
                          " INNER JOIN OASIS..BusinessUnit_M B ON EMP_BSU_ID=B.BSU_ID " & _
                          " INNER JOIN OASIS..BusinessUnit_M C ON EMP_VISA_BSU_ID=C.BSU_ID " & _
                          " INNER JOIN OASIS..EMPDESIGNATION_M E  ON EMP_DES_ID=DES_ID " & _
                          " LEFT JOIN TRANSPORT.DRIVERCON_ALLOC_S D ON A.EMP_ID=TRC_EMP_ID  " & _
                          " LEFT JOIN OASIS..BusinessUnit_M K ON TRC_BSU_ALTO_ID=K.BSU_ID " & _
                          " WHERE DES_GROUP_ID=308 "

            Else
                str_Sql = "Select   EMPNO,EMP_STAFFNO,isNULL(EMP_FName,'')+' '+isNULL(EMP_MName,'')+' '+isNULL(EMP_LName,'') as EmpName,DES_DESCR, " & _
                          " B.BSU_NAME as WorkBSU, " & _
                          " C.BSU_NAME as VisaBSU, " & _
                          " K.BSU_NAME as AllocBSU " & _
                          " From OASIS..Employee_M  A " & _
                          " INNER JOIN OASIS..BusinessUnit_M B ON EMP_BSU_ID=B.BSU_ID " & _
                          " INNER JOIN OASIS..BusinessUnit_M C ON EMP_VISA_BSU_ID=C.BSU_ID " & _
                          " INNER JOIN OASIS..EMPDESIGNATION_M E  ON EMP_DES_ID=DES_ID " & _
                          " LEFT JOIN TRANSPORT.DRIVERCON_ALLOC_S D ON A.EMP_ID=TRC_EMP_ID  " & _
                          " LEFT JOIN OASIS..BusinessUnit_M K ON TRC_BSU_ALTO_ID=K.BSU_ID " & _
                          " WHERE DES_GROUP_ID=308  AND TRC_BSU_ALTO_ID=" & BSU_ID & " "
            End If

            If emp_no <> "" Then
                str_Sql &= " AND EMPNO like '%" & emp_no & "%' "
            End If

            If emp_name <> "" Then
                str_Sql &= " AND EMP_FName like '%" & emp_name & "%' "
            End If

            If desg <> "" Then
                str_Sql &= " AND DES_DESCR like '%" & desg & "%' "
            End If

            If work_bsu <> "" Then
                str_Sql &= " AND B.BSU_NAME like '%" & work_bsu & "%' "
            End If

            If visa_bsu <> "" Then
                str_Sql &= " AND C.BSU_NAME like '%" & visa_bsu & "%' "
            End If

            If alloc_bsu <> "" Then
                str_Sql &= " AND K.BSU_NAME like '%" & alloc_bsu & "%' "
            End If

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            GridInfo.DataSource = ds
            GridInfo.DataBind()

            If export Then
                Dim dt As New DataTable
                dt = ds.Tables(0)
                'dt.Columns.Remove("EMP_STAFFNO")
                ExportExcel(dt)
            End If
            If GridInfo.Rows.Count > 0 Then

                DirectCast(GridInfo.HeaderRow.FindControl("txt1"), TextBox).Text = emp_no
                DirectCast(GridInfo.HeaderRow.FindControl("txt2"), TextBox).Text = emp_name
                DirectCast(GridInfo.HeaderRow.FindControl("txt3"), TextBox).Text = desg
                DirectCast(GridInfo.HeaderRow.FindControl("txt4"), TextBox).Text = work_bsu
                DirectCast(GridInfo.HeaderRow.FindControl("txt5"), TextBox).Text = visa_bsu
                DirectCast(GridInfo.HeaderRow.FindControl("txt6"), TextBox).Text = alloc_bsu

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid(False)
    End Sub

    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "search" Then
            BindGrid(False)
        End If

    End Sub
    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '   ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        BindGrid(True)
    End Sub
End Class
