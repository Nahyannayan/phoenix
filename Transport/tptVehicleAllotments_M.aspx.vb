Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Transport_tptSeatCapacity_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            
            Dim str_sql As String = ""

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

 
            'calling pageright class to get the access rights


            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            ViewState("datamode") = "add"
    
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

            BindVehicle()
            If Not Request.QueryString("vehid") Is Nothing Then
                ddlVehicle.Items.FindByValue(Request.QueryString("vehid")).Selected = True
            End If


            BindShift()

            If Request.QueryString("journey") = "Onward" Then
                rdOnward.Checked = True
                rdReturn.Checked = False
            Else
                rdReturn.Checked = True
                rdOnward.Checked = False
            End If

            GridBind()




        End If


    End Sub

    Protected Sub btnArea_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnDriver_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnConductor_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnTrip_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
#Region "Private Methods"



    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTPT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTPT.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub PopulateAcademicYear()
        ddlVehicle.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=" + Session("clm") _
                                  & " AND ACD_ACY_ID>=" + Session("Current_ACY_ID")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlVehicle.DataSource = ds
        ddlVehicle.DataTextField = "acy_descr"
        ddlVehicle.DataValueField = "acd_id"
        ddlVehicle.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvTPT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub BindShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "SELECT SHF_ID,SHF_DESCR FROM VV_SHIFTS_M WHERE SHF_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String


        Dim str_query As String = "SELECT TRP_DESCR,DRV_NAME,CON_NAME FROM TRANSPORT.VV_TRIPS_D WHERE TRD_TODATE IS NULL " _
                                & " AND VEH_ID=" + ddlVehicle.SelectedValue.ToString + " AND TRP_JOURNEY=" _
                                & IIf(rdOnward.Checked = True, "'Onward'", "'Return'")

        Dim txtSearch As New TextBox

        Dim drvSearch As String = ""
        Dim trpSearch As String = ""
        Dim conSearch As String = ""


        If gvTPT.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvTPT.HeaderRow.FindControl("txtTrip")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("TRP_DESCR", txtSearch.Text, strSearch)
            trpSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvTPT.HeaderRow.FindControl("txtDriver")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("DRV_NAME", txtSearch.Text, strSearch)
            drvSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvTPT.HeaderRow.FindControl("txtConductor")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("CON_NAME", txtSearch.Text, strSearch)
            conSearch = txtSearch.Text
        End If

        str_query += " ORDER BY TRP_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTPT.DataSource = ds


        gvTPT.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTPT.DataBind()
            Dim columnCount As Integer = gvTPT.Rows(0).Cells.Count
            gvTPT.Rows(0).Cells.Clear()
            gvTPT.Rows(0).Cells.Add(New TableCell)
            gvTPT.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTPT.DataBind()
        End If

      
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Private Sub BindVehicle()
        ddlvehicle.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        str_query = "select veh_id,veh_regno from transport.vv_vehicle_m where VEH_ALTO_BSU_ID='" + Session("sbsuid") + "' order by veh_regno "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlVehicle.DataSource = ds
        ddlVehicle.DataTextField = "veh_regno"
        ddlVehicle.DataValueField = "veh_id"
        ddlVehicle.DataBind()
    End Sub


#End Region



    Protected Sub rdOnward_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOnward.CheckedChanged
        If rdOnward.Checked = True Then
            GridBind()
        End If
    End Sub

    Protected Sub rdReturn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdReturn.CheckedChanged
        If rdReturn.Checked = True Then
            GridBind()
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVehicle.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub gvTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTPT.PageIndexChanging
        gvTPT.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
End Class
