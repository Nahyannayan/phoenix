<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptBusNo_M.aspx.vb" Inherits="Transport_tptBusNo_M" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
</script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Bus Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">
        <tr>
            <td>
                <asp:ValidationSummary ID="valSum" runat="server" CssClass="error" EnableViewState="False"
                   HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1"  width="100%"/>
               
            </td>
        </tr>
        <tr>
         <td align="center" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                   width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel1" runat="server" >
                         
                <table align="center" width="100%">  
                    <tr>
                       
                        <td align="left" width="10%">
                           <span class="field-label">Prefix</span></td>                        
                        <td align="left" width="20%">
                               <asp:TextBox ID="txtPrefix" runat="server" TabIndex="1">Bus No.</asp:TextBox>
                      </td>          
                        <td align="left" width="10%">
                             <span class="field-label">Range From</span></td>
                        
                        <td align="left" width="20%">
                               <asp:TextBox ID="txtFrom" runat="server" TabIndex="2" ></asp:TextBox>
                      </td> 
                        <td align="left" width="10%">
                            <span class="field-label"> To</span></td>
                       
                        <td align="left" width="20%">
                               <asp:TextBox ID="txtTo" runat="server" TabIndex="3"></asp:TextBox>
                      </td> 
                    </tr>
                            </table>
                       </asp:Panel>
                       
       </td>
        </tr>
        <tr>
            <td align="left" width="100%">
            
                <asp:Panel ID="Panel2" runat="server">
                         
                <table align="left" width="100%">
                  
                    <tr>
                       
                        <td align="left" width="20%">
                         <span class="field-label">Bus Description</span></td>
                        
                        <td align="left" width="30%">
                               <asp:TextBox ID="txtBusNo" runat="server" TabIndex="2"></asp:TextBox>
                      </td>  
                      
                        <td  width="20%"></td>
                        <td  width="30%"></td>
                            
                      </tr>
                    
                   
                                   
                     </table>
                </asp:Panel>
                       
               </td>
        </tr>
        <tr>
            <td align="center">
            
                <asp:Panel ID="Panel3" runat="server" Visible="False">
                         
                <table align="center" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                    Some of the buses in the sprecified range are already saved.
                    Do you wish to override them?
                      </td>
                    </tr>
                    <tr>
                      <td align="center" valign="middle">
                    <asp:Button ID="btnYes" runat="server" CausesValidation="False" CssClass="button"
                    Text="Yes"/>
                   <asp:Button ID="btnNo" runat="server" CausesValidation="False" CssClass="button"
                    Text="No"/>
                    </td>
                    </tr>
                </table>
                </asp:Panel>
                       
               </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Panel ID="Panel4" runat="server" Height="50px" Width="692px">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" TabIndex="9" OnClientClick="return confirm_delete();" />
                    </asp:Panel>
                    </td>
        </tr>
        <tr>
            <td valign="bottom" style="height: 44px; width: 692px;">
                <asp:RequiredFieldValidator ID="rfPrefix" runat="server" ControlToValidate="txtPrefix"
                    Display="None" ErrorMessage="Please enter data in the field Prefix" ValidationGroup="groupM1" Enabled="False"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="rfFrm" runat="server" ControlToValidate="txtFrom"
                    Display="None" ErrorMessage="Please enter data in the field Range From" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="rfBusNo" runat="server" ControlToValidate="txtBusNo"
                    Display="None" ErrorMessage="Please enter data in the field Bus Decription" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvRange" runat="server" ControlToCompare="txtFrom" ControlToValidate="txtTo"
                    CssClass="error" Display="None" ErrorMessage="Range To should be greater than Range from"
                    ForeColor="Transparent" Height="1px" Operator="GreaterThan" Type="Integer" ValidationGroup="groupM1"
                    Width="46px"></asp:CompareValidator>
                <asp:HiddenField ID="hfBUS_ID" runat="server" />
               
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>
</asp:Content>

