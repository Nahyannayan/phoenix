Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Transport_tptVeh_Allocation_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    txtHyp.Attributes.Add("Readonly", "Readonly")
                    txtSupp.Attributes.Add("Readonly", "Readonly")
                    txtCat.Attributes.Add("Readonly", "Readonly")
                    txtMake.Attributes.Add("Readonly", "Readonly")
                    txtInsu.Attributes.Add("Readonly", "Readonly")
                    txtBO.Attributes.Add("Readonly", "Readonly")
                    txtBP.Attributes.Add("Readonly", "Readonly")
                    txtBA.Attributes.Add("Readonly", "Readonly")
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                        Call Vehicle_M_Details(ViewState("viewid"))
                        Call setState()
                        disablecontrol()
                    ElseIf ViewState("datamode") = "add" Then
                        Call ResetState()
                        txtRegDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                        txtRegExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Year, 1, DateTime.Now)))

                        txtInvDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        txtIns_StartDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        txtIns_ExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Year, 1, DateTime.Now)))
                        txtBODT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        txtBPDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                        txtBADT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                        Call enableControl()
                        Call clearall()
                        ''added by nahyan on 10Dec2013
                        bindFuelTypes()

                        If CurBsUnit <> "900501" Then
                            txtBA.Text = GetBSU_NAME(CurBsUnit)
                            imgBA.Visible = False
                            hfBA.Value = CurBsUnit
                        End If
                    End If

                    fillEmpDetailsGridView(gridPermitDetails, "select '" & IIf(ViewState("viewid") = Nothing, 0, ViewState("viewid")) & "' VEP_VEH_ID,VEP_ID,VEP_VPT_ID,VPT_DESCR,VEP_PERMIT_NO,VEP_ISSDATE,VEP_EXPDATE,VEP_RENEW,CONVERT(VARCHAR(20),dateadd(d,7,VEP_TRDATE),106) VEP_TRDATE,CONVERT(VARCHAR(20),getdate(),106) P_Date from VEHICLE_PERMIT left outer join VEHICLE_PERMIT_M on VPT_ID=VEP_VPT_ID  where VEP_VEH_ID=" & IIf(ViewState("viewid") = Nothing, 0, ViewState("viewid")) & " and VEP_DELETED=0 order by VPT_DESCR")




                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    Sub setState()
        txtBO.Enabled = False
        imgBO.Visible = False
        txtBP.Enabled = False
        imgBP.Visible = False
        txtBA.Enabled = False
        imgBA.Visible = False

        txtBODT.Enabled = False
        imgbtnBO.Visible = False
        txtBPDT.Enabled = False
        imgbtnBP.Visible = False

        txtBADT.Enabled = False
        imgbtnAllocDT.Visible = False


    End Sub
    Sub clearall()
        txtOrderNo.Text = ""
        txtRegNo.Text = ""
        txtRegDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtRegExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Year, 1, DateTime.Now)))

        txtInvDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtIns_StartDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtIns_ExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Year, 1, DateTime.Now)))

        txtBODT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtBPDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

        txtBADT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

        txtHyp.Text = ""

        txtSupp.Text = ""

        txtCat.Text = ""


        txtMake.Text = ""

        txtPlateColor.Text = ""


        txtEngineNo.Text = ""
        txtChasisNo.Text = ""
        txtCap.Text = ""
        txtRecommd.Text = ""
        txtRev_Value.Text = ""
        txtDep_value.Text = ""
        txtInvNo.Text = ""
        txtInv_value.Text = ""

        txtInvDT.Text = ""

        txtInsu.Text = ""

        txtIns_value.Text = ""

        txtBO.Text = ""
        txtBA.Text = ""
        txtBP.Text = ""

        txtSaleDate.Text = ""
        txtSaleInvNo.Text = ""
        txtSaleValue.Text = ""

    End Sub
    Sub enableControl()
        txtOrderNo.Enabled = True
        txtRegNo.Enabled = True
        txtRegDT.Enabled = True
        txtRegExpDT.Enabled = True
        txtHyp.Enabled = True
        imgHyp.Enabled = True
        txtSupp.Enabled = True
        imgSupp.Enabled = True
        txtCat.Enabled = True
        imgCat.Enabled = True
        txtMake.Enabled = True
        imgMake.Enabled = True
        txtPlateColor.Enabled = True
        txtMake.Enabled = True
        imgMake.Enabled = True
        txtEngineNo.Enabled = True
        txtChasisNo.Enabled = True
        txtCap.Enabled = True
        txtRecommd.Enabled = True
        txtRev_Value.Enabled = True
        txtDep_value.Enabled = True
        txtInvNo.Enabled = True
        txtInv_value.Enabled = True
        txtInvDT.Enabled = True
        imgInvDT.Enabled = True
        txtInsu.Enabled = True
        imgInsu.Enabled = True
        txtIns_value.Enabled = True
        txtIns_ExpDT.Enabled = True
        txtIns_StartDT.Enabled = True
        imgInsStartDT.Enabled = True
        imgInsExpDT.Enabled = True
        ddlFuelType.Enabled = True
        txtCardNumber.Enabled = True
        gridPermitDetails.Enabled = True
        txtSaleDate.Enabled = True
        txtSaleInvNo.Enabled = True
        txtSaleValue.Enabled = True
    End Sub
    Sub disablecontrol()
        txtOrderNo.Enabled = False
        txtRegNo.Enabled = False
        txtRegDT.Enabled = False
        txtRegExpDT.Enabled = False
        txtHyp.Enabled = False
        imgHyp.Enabled = False
        txtSupp.Enabled = False
        imgSupp.Enabled = False
        txtCat.Enabled = False
        imgCat.Enabled = False
        txtMake.Enabled = False
        imgMake.Enabled = False
        txtPlateColor.Enabled = False
        txtMake.Enabled = False
        imgMake.Enabled = False
        txtEngineNo.Enabled = False
        txtChasisNo.Enabled = False
        txtCap.Enabled = False
        txtRecommd.Enabled = False
        txtRev_Value.Enabled = False
        txtDep_value.Enabled = False
        txtInvNo.Enabled = False
        txtInv_value.Enabled = False
        txtInvDT.Enabled = False
        imgInvDT.Enabled = False
        txtInsu.Enabled = False
        imgInsu.Enabled = False
        txtIns_value.Enabled = False
        txtIns_ExpDT.Enabled = False
        txtIns_StartDT.Enabled = False
        imgInsStartDT.Enabled = False
        imgInsExpDT.Enabled = False
        ddlFuelType.Enabled = False
        txtCardNumber.Enabled = False
        gridPermitDetails.Enabled = False
        txtSaleDate.Enabled = False
        txtSaleInvNo.Enabled = False
        txtSaleValue.Enabled = False


    End Sub
    Sub ResetState()
        txtBO.Enabled = True
        imgBO.Visible = True
        txtBP.Enabled = True
        imgBP.Visible = True
        txtBA.Enabled = True
        imgBA.Visible = True

        txtBODT.Enabled = True
        imgbtnBO.Visible = True
        txtBPDT.Enabled = True
        imgbtnBP.Visible = True

        txtBADT.Enabled = True
        imgbtnAllocDT.Visible = True


    End Sub
    Sub Vehicle_M_Details(ByVal VEH_ID As String)
        h_veh_id.Value = VEH_ID

        Try

            Using readerVehicle_M As SqlDataReader = TransportClass.GetVehicle_M_Details(ViewState("viewid"))
                If readerVehicle_M.HasRows = True Then
                    While readerVehicle_M.Read
                        txtOrderNo.Text = Convert.ToString(readerVehicle_M("VEH_ORDERNO"))
                        txtRegNo.Text = Convert.ToString(readerVehicle_M("VEH_REGNO"))
                        txtHyp.Text = Convert.ToString(readerVehicle_M("HPM_NAME"))
                        txtSupp.Text = Convert.ToString(readerVehicle_M("ACT_NAME"))
                        txtCat.Text = Convert.ToString(readerVehicle_M("CAT_DESCRIPTION"))
                        txtPlateColor.Text = Convert.ToString(readerVehicle_M("VEH_PLATE_COLOR")) 'new added code
                        txtMake.Text = Convert.ToString(readerVehicle_M("VMK_MAKE"))
                        txtModel.Text = Convert.ToString(readerVehicle_M("VEH_MODEL"))
                        txtEngineNo.Text = Convert.ToString(readerVehicle_M("VEH_ENGINENO"))
                        txtChasisNo.Text = Convert.ToString(readerVehicle_M("VEH_CHASISNO"))
                        txtCap.Text = Convert.ToString(readerVehicle_M("VEH_CAPACITY"))
                        txtRecommd.Text = Convert.ToString(readerVehicle_M("VEH_RECOMMD_MILAGE"))
                        txtRev_Value.Text = Convert.ToString(readerVehicle_M("VEH_REV_VALUE"))
                        txtDep_value.Text = Convert.ToString(readerVehicle_M("VEH_DEPR_VALUE"))
                        txtInvNo.Text = Convert.ToString(readerVehicle_M("VEH_INVOICENO"))
                        txtInv_value.Text = Convert.ToString(readerVehicle_M("VEH_INVOICE_VALUE"))
                        txtInsu.Text = Convert.ToString(readerVehicle_M("INC_NAME"))
                        txtIns_value.Text = Convert.ToString(readerVehicle_M("VEH_INC_VALUE"))
                        txtBO.Text = Convert.ToString(readerVehicle_M("OWNED_BSU"))
                        txtBP.Text = Convert.ToString(readerVehicle_M("OPERA_BSU"))
                        txtBA.Text = Convert.ToString(readerVehicle_M("ALLOC_BSU"))

                        hfCat.Value = Convert.ToString(readerVehicle_M("VEH_CAT_ID"))
                        hfSupp.Value = Convert.ToString(readerVehicle_M("VEH_SUPPLIER_ACT_ID"))
                        hfInsu.Value = Convert.ToString(readerVehicle_M("VEH_INC_ID"))
                        hfHyp.Value = Convert.ToString(readerVehicle_M("VEH_HPM_ID"))
                        hfBO.Value = Convert.ToString(readerVehicle_M("VEH_OWN_BSU_ID"))
                        hfBP.Value = Convert.ToString(readerVehicle_M("VEH_OPRT_BSU_ID"))
                        hfBA.Value = Convert.ToString(readerVehicle_M("VEH_ALTO_BSU_ID"))
                        hfMake.Value = Convert.ToString(readerVehicle_M("VEH_VMK_ID"))


                        If IsDate(readerVehicle_M("VEH_REGDATE")) = True Then
                            txtRegDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VEH_REGDATE"))))
                        End If

                        If IsDate(readerVehicle_M("VEH_REG_EXPDT")) = True Then
                            txtRegExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VEH_REG_EXPDT"))))
                        End If
                        If IsDate(readerVehicle_M("VEH_INVOICE_DATE")) = True Then
                            txtInvDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VEH_INVOICE_DATE"))))
                        End If

                        If IsDate(readerVehicle_M("VEH_INC_STARTDT")) = True Then

                            txtIns_StartDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VEH_INC_STARTDT"))))

                        End If

                        If IsDate(readerVehicle_M("VEH_INC_EXPDT")) = True Then
                            txtIns_ExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VEH_INC_EXPDT"))))
                        End If

                        If IsDate(readerVehicle_M("VAL_FROMDT_O")) = True Then
                            txtBODT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VAL_FROMDT_O"))))
                        End If

                        If IsDate(readerVehicle_M("VAL_FROMDT_A")) = True Then
                            txtBADT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VAL_FROMDT_A"))))
                        End If

                        If IsDate(readerVehicle_M("VAL_FROMDT_P")) = True Then
                            txtBPDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VAL_FROMDT_P"))))
                        End If

                        ''added by nahyan on 10Dec2013 
                        bindFuelTypes()
                        If Convert.ToString(readerVehicle_M("VEH_FUT_ID")) <> "" Then
                            ddlFuelType.Items.FindByValue(readerVehicle_M("VEH_FUT_ID")).Selected = True
                        End If

                        If Convert.ToString(readerVehicle_M("VEH_CARD_NUMBER")) <> "" Then
                            txtCardNumber.Text = Convert.ToString(readerVehicle_M("VEH_CARD_NUMBER"))
                        End If


                        ''added by Mahesh on 7Dec2015

                        If IsDate(readerVehicle_M("VEH_SALE_DATE")) = True Then
                            txtSaleDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerVehicle_M("VEH_SALE_DATE"))))
                        End If
                        txtSaleInvNo.Text = Convert.ToString(readerVehicle_M("VEH_SALE_INVNO"))
                        txtSaleValue.Text = Convert.ToString(readerVehicle_M("VEH_SALE_VALUE"))




                    End While
                End If



            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Property TPTPermitDetails() As DataTable
        Get
            Return ViewState("TPTPermitDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTPermitDetails") = value
        End Set
    End Property
    Private Sub fillEmpDetailsGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTPermitDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTPermitDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTPermitDetails = mtable

        fillGrdView.DataSource = TPTPermitDetails
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub
    Private Sub showNoRecordsFound()
        If TPTPermitDetails.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = gridPermitDetails.Columns.Count - 2
            gridPermitDetails.Rows(0).Cells.Clear()
            gridPermitDetails.Rows(0).Cells.Add(New TableCell())
            gridPermitDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            gridPermitDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub
    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtRegDT.Text.Trim <> "" Then
            Dim strfDate As String = txtRegDT.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtRegDT.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtRegDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If


        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        'If serverDateValidate() = "0" Then

        str_err = calltransaction(errorMessage)







        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            Call disablecontrol()
            clearall()
        Else
            lblError.Text = errorMessage
        End If

        ' End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim VEH_BSU_ID As String = hfBO.Value
        Dim VEH_ID As String = String.Empty
        Dim VEH_CAT_ID As String = hfCat.Value
        Dim VEH_REGNO As String = txtRegNo.Text
        Dim VEH_REGDAT As String = txtRegDT.Text
        Dim VEH_CAPACITY As String = txtCap.Text
        Dim VEH_MODEL As String = txtModel.Text
        Dim VEH_ORDERNO As String = txtOrderNo.Text
        Dim VEH_SPM_ID As String = hfSupp.Value
        Dim VEH_INC_ID As String = hfInsu.Value
        Dim VEH_HPM_ID As String = hfHyp.Value
        Dim VEH_INVOICENO As String = txtInvNo.Text
        Dim VEH_CHASISNO As String = txtChasisNo.Text
        Dim VEH_ENGINENO As String = txtEngineNo.Text
        Dim VEH_INVOICE_DATE As String = txtInvDT.Text
        Dim VEH_INVOICE_VALUE As Double = IIf(txtInv_value.Text = "", 0, txtInv_value.Text)
        Dim VEH_REV_VALUE As Double = IIf(txtRev_Value.Text = "", 0, txtRev_Value.Text)
        Dim VEH_DEPR_VALUE As Double = IIf(txtDep_value.Text = "", 0, txtDep_value.Text)
        Dim VEH_INC_VALUE As Double = IIf(txtIns_value.Text = "", 0, txtIns_value.Text)
        Dim VEH_INC_STARTDT As String = txtIns_StartDT.Text
        Dim VEH_INC_EXPDT As String = txtIns_ExpDT.Text
        Dim VEH_REG_EXPDT As String = txtRegExpDT.Text
        Dim VEH_RECOMMD_MILAGE As Double = IIf(txtRecommd.Text = "", 0, txtRecommd.Text)
        Dim VEH_OWN_BSU_ID As String = hfBO.Value
        Dim VEH_OPRT_BSU_ID As String = hfBP.Value
        Dim VEH_ALTO_BSU_ID As String = hfBA.Value
        Dim VEH_VMK_ID As String = hfMake.Value
        Dim VAL_TYPE_O As Boolean
        Dim VAL_FROMDT_O As String = txtBODT.Text
        Dim VAL_TYPE_P As Boolean
        Dim VAL_FROMDT_P As String = txtBPDT.Text
        Dim VAL_TYPE_A As Boolean
        Dim VAL_FROMDT_A As String = txtBADT.Text
        Dim VEH_PLATE_COLOR As String = txtPlateColor.Text


        Dim bEdit As String

        ''added by nahyan on 10Dec2013
        Dim VEH_FUEL_TYPE As Integer = Convert.ToInt32(ddlFuelType.SelectedValue)
        Dim VEH_CARD_NUMBER As String = txtCardNumber.Text

        ''added by Mahesh on 7Dec2013
        Dim VEH_SALE_INVNO As String = txtSaleInvNo.Text
        Dim VEH_SALE_VALUE As Double = IIf(txtSaleValue.Text = "", 0, txtSaleValue.Text)
        Dim VEH_SALE_DATE As String = IIf(txtSaleDate.Text = "", "1900-01-01", txtSaleDate.Text)








        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer

                    If VEH_OWN_BSU_ID.Trim = "" Or VAL_FROMDT_O.Trim = "" Then
                        VAL_TYPE_O = False
                        VEH_OWN_BSU_ID = ""
                        VAL_FROMDT_O = ""
                    Else
                        VAL_TYPE_O = True

                    End If

                    If VEH_OPRT_BSU_ID.Trim = "" Or VAL_FROMDT_A.Trim = "" Then
                        VAL_TYPE_P = True
                        VEH_OPRT_BSU_ID = VEH_OWN_BSU_ID
                        VAL_FROMDT_P = VAL_FROMDT_O
                    Else
                        VAL_TYPE_P = True
                    End If


                    If VEH_ALTO_BSU_ID.Trim = "" Or VAL_FROMDT_A.Trim = "" Then
                        VAL_TYPE_P = True
                        VEH_ALTO_BSU_ID = VEH_OWN_BSU_ID
                        VAL_FROMDT_A = VAL_FROMDT_O
                    Else
                        VAL_TYPE_A = True
                    End If



                    bEdit = False

                    ''added two new fields VEH_FUEL_TYPE, VEH_CARD_NUMBER,  by nahyan on dec10-2013
                    status = TransportClass.SAVE_VEHICLE_ALLOC_M(VEH_BSU_ID, VEH_ID, VEH_CAT_ID, VEH_REGNO, _
                     VEH_REGDAT, VEH_CAPACITY, VEH_MODEL, VEH_ORDERNO, _
                     VEH_SPM_ID, VEH_PLATE_COLOR, VEH_INC_ID, VEH_HPM_ID, VEH_INVOICENO, _
                     VEH_CHASISNO, VEH_ENGINENO, VEH_INVOICE_DATE, VEH_INVOICE_VALUE, _
                     VEH_REV_VALUE, VEH_DEPR_VALUE, VEH_INC_VALUE, VEH_INC_STARTDT, VEH_INC_EXPDT, _
                     VEH_REG_EXPDT, VEH_RECOMMD_MILAGE, VEH_OWN_BSU_ID, VEH_OPRT_BSU_ID, _
                     VEH_ALTO_BSU_ID, VEH_VMK_ID, VAL_TYPE_O, VAL_FROMDT_O, _
                     VAL_TYPE_P, VAL_FROMDT_P, VAL_TYPE_A, VAL_FROMDT_A, _
                      bEdit, VEH_FUEL_TYPE, VEH_CARD_NUMBER, transaction, VEH_SALE_INVNO, VEH_SALE_VALUE, VEH_SALE_DATE)




                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

                'added by mahesh For Permit Details.
                Try
                    Dim RowCount As Integer, InsCount As Integer = 0
                    Dim VehId As Integer = 0
                    h_veh_id.Value = Mainclass.getDataValue("SELECT VEH_ID FROM VEHICLE_M WHERE VEH_REGNO='" & txtRegNo.Text & "'", "OASIS_TRANSPORTConnectionString")
                    For RowCount = 0 To TPTPermitDetails.Rows.Count - 1
                        Dim iParms(8) As SqlClient.SqlParameter
                        Dim rowState As Integer = ViewState("EntryId")
                        Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, TPTPermitDetails.Rows(RowCount)("VEP_ID"))
                        If TPTPermitDetails.Rows(RowCount).RowState = 8 Then rowState = -1
                        iParms(1) = Mainclass.CreateSqlParameter("@VEP_ID", 0, SqlDbType.Int)
                        iParms(2) = Mainclass.CreateSqlParameter("@VEP_VPT_ID", TPTPermitDetails.Rows(RowCount)("VEP_VPT_ID"), SqlDbType.Int)
                        iParms(3) = Mainclass.CreateSqlParameter("@VEP_VEH_ID", h_veh_id.Value, SqlDbType.Int)
                        iParms(4) = Mainclass.CreateSqlParameter("@VEP_PERMIT_NO", TPTPermitDetails.Rows(RowCount)("VEP_PERMIT_NO"), SqlDbType.VarChar)
                        iParms(5) = Mainclass.CreateSqlParameter("@VEP_ISSDATE", TPTPermitDetails.Rows(RowCount)("VEP_ISSDATE"), SqlDbType.SmallDateTime)
                        iParms(6) = Mainclass.CreateSqlParameter("@VEP_EXPDATE", TPTPermitDetails.Rows(RowCount)("VEP_EXPDATE"), SqlDbType.SmallDateTime)
                        iParms(7) = Mainclass.CreateSqlParameter("@VEP_RENEW", TPTPermitDetails.Rows(RowCount)("VEP_RENEW"), SqlDbType.VarChar)
                        iParms(8) = Mainclass.CreateSqlParameter("@VEP_USER", Session("sUsr_name"), SqlDbType.VarChar)
                        If Not IsDBNull(TPTPermitDetails.Rows(RowCount)("VEP_VPT_ID")) > 0 Then
                            InsCount += 1
                            Dim RetValFooter As String = Mainclass.ExecuteParamQRY(connectionString, "SAVEVEHICLE_PERMIT", iParms)
                        End If
                    Next
                Catch ex As Exception
                    errorMessage = "Error Occured While Saving Permit Details ."
                End Try
                'added by mahesh For Permit Details.
            End Using
        ElseIf ViewState("datamode") = "edit" Then
            If ViewState("viewid") = 0 Then
                calltransaction = "1"
                errorMessage = "Record do not exist for saving the record."
            Else
                Dim transaction As SqlTransaction
                Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        Dim status As Integer
                        VEH_ID = ViewState("viewid")
                        bEdit = True
                        status = TransportClass.SAVE_VEHICLE_ALLOC_M(VEH_BSU_ID, VEH_ID, VEH_CAT_ID, VEH_REGNO, _
         VEH_REGDAT, VEH_CAPACITY, VEH_MODEL, VEH_ORDERNO, _
         VEH_SPM_ID, VEH_PLATE_COLOR, VEH_INC_ID, VEH_HPM_ID, VEH_INVOICENO, _
         VEH_CHASISNO, VEH_ENGINENO, VEH_INVOICE_DATE, VEH_INVOICE_VALUE, _
         VEH_REV_VALUE, VEH_DEPR_VALUE, VEH_INC_VALUE, VEH_INC_STARTDT, VEH_INC_EXPDT, _
         VEH_REG_EXPDT, VEH_RECOMMD_MILAGE, VEH_OWN_BSU_ID, VEH_OPRT_BSU_ID, _
         VEH_ALTO_BSU_ID, VEH_VMK_ID, VAL_TYPE_O, VAL_FROMDT_O, _
         VAL_TYPE_P, VAL_FROMDT_P, VAL_TYPE_A, VAL_FROMDT_A, _
          bEdit, VEH_FUEL_TYPE, VEH_CARD_NUMBER, transaction, VEH_SALE_INVNO, VEH_SALE_VALUE, VEH_SALE_DATE)
                        If status <> 0 Then
                            calltransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                            Return "1"
                        End If
                        ViewState("viewid") = "0"
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        calltransaction = "0"
                        'added by mahesh For Permit Details.
                        Dim RowCount As Integer, InsCount As Integer = 0
                        If TPTPermitDetails.Rows(0)(1) <> -1 Then
                            For RowCount = 0 To TPTPermitDetails.Rows.Count - 1
                                Dim iParms(8) As SqlClient.SqlParameter
                                Dim rowState As Integer = ViewState("EntryId")
                                Dim RowEntryId As Integer = IIf(ViewState("datamode") = "add", 0, TPTPermitDetails.Rows(RowCount)("VEP_ID"))
                                If TPTPermitDetails.Rows(RowCount).RowState = 8 Then rowState = -1
                                iParms(1) = Mainclass.CreateSqlParameter("@VEP_ID", TPTPermitDetails.Rows(RowCount)("VEP_ID"), SqlDbType.Int)
                                iParms(2) = Mainclass.CreateSqlParameter("@VEP_VPT_ID", TPTPermitDetails.Rows(RowCount)("VEP_VPT_ID"), SqlDbType.Int)
                                iParms(3) = Mainclass.CreateSqlParameter("@VEP_VEH_ID", h_veh_id.Value, SqlDbType.Int)
                                iParms(4) = Mainclass.CreateSqlParameter("@VEP_PERMIT_NO", TPTPermitDetails.Rows(RowCount)("VEP_PERMIT_NO"), SqlDbType.VarChar)
                                iParms(5) = Mainclass.CreateSqlParameter("@VEP_ISSDATE", TPTPermitDetails.Rows(RowCount)("VEP_ISSDATE"), SqlDbType.SmallDateTime)
                                iParms(6) = Mainclass.CreateSqlParameter("@VEP_EXPDATE", TPTPermitDetails.Rows(RowCount)("VEP_EXPDATE"), SqlDbType.SmallDateTime)
                                iParms(7) = Mainclass.CreateSqlParameter("@VEP_RENEW", TPTPermitDetails.Rows(RowCount)("VEP_RENEW"), SqlDbType.VarChar)
                                iParms(8) = Mainclass.CreateSqlParameter("@VEP_USER", Session("sUsr_name"), SqlDbType.VarChar)
                                If Not IsDBNull(TPTPermitDetails.Rows(RowCount)("VEP_VPT_ID")) > 0 Then
                                    InsCount += 1
                                    Dim RetValFooter As String = Mainclass.ExecuteParamQRY(connectionString, "SAVEVEHICLE_PERMIT", iParms)
                                    If RetValFooter = "-1" Then
                                        calltransaction = "1"
                                    Else
                                        calltransaction = "0"
                                    End If
                                End If
                            Next
                        End If
                        If hGridDelete.Value <> "" Then
                            Dim deleteEmps() As String = hGridDelete.Value.Split(";")
                            Dim iParms(2) As SqlClient.SqlParameter
                            For RowCount = 0 To deleteEmps.GetUpperBound(0) - 1
                                iParms(1) = Mainclass.CreateSqlParameter("@VEP_ID", deleteEmps(RowCount), SqlDbType.Int)
                                Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "UPDATE VEHICLE_PERMIT SET vep_deleted=1,VEP_DELETED_USER='" & Session("sUsr_name") & "',VEP_DELETED_DATE=getdate() where VEP_ID=@VEP_ID", iParms)

                                If RetValFooter = "-1" Then
                                    calltransaction = "1"
                                Else
                                    calltransaction = "0"
                                End If
                            Next
                        End If
                        ViewState("viewid") = 0
                    Catch ex As Exception
                        calltransaction = "1"
                        errorMessage = "Error Occured While Saving."
                    Finally
                        If calltransaction <> "0" Then
                            UtilityObj.Errorlog(errorMessage)
                            transaction.Rollback()
                        Else
                            errorMessage = ""
                            transaction.Commit()
                        End If
                    End Try
                End Using
            End If
        End If
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        Call ResetState()
        Call enableControl()
        Call clearall()
        Dim BSU_ID As String = Session("sBsuid")

        If BSU_ID <> "900501" Then
            txtBA.Text = GetBSU_NAME(BSU_ID)
            imgBA.Visible = False
            hfBA.Value = BSU_ID
        End If

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Function GetBSU_NAME(ByVal BSU_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim strQuery As String = String.Empty
        strQuery = "select BSU_NAME FROM BUSINESSUNIT_M where BSU_ID='" & BSU_ID & "' "
        GetBSU_NAME = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Call setState()
        Call enableControl()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call disablecontrol()
                Call clearall()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Protected Sub txtRegDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

        If IsDate(txtRegDT.Text) Then
            Dim tempdate As Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtRegDT.Text)
            Dim ExpDate As Date = DateAdd(DateInterval.Year, 1, tempdate)

            txtRegExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", (DateAdd(DateInterval.Day, -1, ExpDate)))
        Else
            lblError.Text = "Invalid Registration Date"
        End If

    End Sub

    Protected Sub txtIns_StartDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(txtIns_StartDT.Text) Then
            Dim tempdate As Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtIns_StartDT.Text)
            Dim Ins_ExpDate As Date = DateAdd(DateInterval.Year, 1, tempdate)

            txtIns_ExpDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", (DateAdd(DateInterval.Day, -1, Ins_ExpDate)))
        Else
            lblError.Text = "Invalid Insurance Start Date"
        End If
    End Sub

    Private Sub bindFuelTypes()
        Try
            Dim CONN As String = ConnectionManger.GetOASISTRANSPORTConnectionString

            Using fuelTypes As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "FUEL.Get_All_Fuel_Types")
                ddlFuelType.Items.Clear()

                If fuelTypes.HasRows = True Then

                    ddlFuelType.DataSource = fuelTypes
                    ddlFuelType.DataTextField = "FUT_DESCRIPTION"
                    ddlFuelType.DataValueField = "FUT_ID"
                    ddlFuelType.DataBind()

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'Added By Mahesh on 13-10-2015
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub
    Protected Sub gridPermitDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gridPermitDetails.RowCancelingEdit
        gridPermitDetails.ShowFooter = True
        gridPermitDetails.EditIndex = -1
        gridPermitDetails.DataSource = TPTPermitDetails
        gridPermitDetails.DataBind()
    End Sub
    Protected Sub gridPermitDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridPermitDetails.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtIssDate As TextBox = gridPermitDetails.FooterRow.FindControl("txtIssDate")
            Dim txtExpDate As TextBox = gridPermitDetails.FooterRow.FindControl("txtExpDate")
            Dim hdnPermitType_ID As HiddenField = gridPermitDetails.FooterRow.FindControl("hdnITM_ID")
            Dim ddlPermit As DropDownList = gridPermitDetails.FooterRow.FindControl("ddlPermitType")
            Dim ddlRenew As DropDownList = gridPermitDetails.FooterRow.FindControl("ddlRenew")
            Dim txtPERMITNO As TextBox = gridPermitDetails.FooterRow.FindControl("txtPERMITNO")
            If txtPERMITNO.Text = "" Or txtIssDate.Text = "" Or txtExpDate.Text = "" Then
                lblError.Text = "Please enter permit Details properly."
            End If
            If CDate(txtIssDate.Text) > CDate(txtExpDate.Text) Then
                lblError.Text = "Issue date should be less than Expiry Date"
            End If
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            If TPTPermitDetails.Rows(0)(1) = -1 Then TPTPermitDetails.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = TPTPermitDetails.NewRow
            mrow("VEP_ID") = 0
            mrow("VEP_VPT_ID") = ddlPermit.SelectedValue
            mrow("VPT_DESCR") = ddlPermit.SelectedItem.Text
            mrow("VEP_ISSDATE") = txtIssDate.Text
            mrow("VEP_EXPDATE") = txtExpDate.Text
            mrow("VEP_PERMIT_NO") = txtPERMITNO.Text
            mrow("VEP_VEH_ID") = 0
            mrow("VEP_RENEW") = ddlRenew.SelectedItem.Text

            TPTPermitDetails.Rows.Add(mrow)
            gridPermitDetails.EditIndex = -1
            gridPermitDetails.DataSource = TPTPermitDetails
            gridPermitDetails.DataBind()
            showNoRecordsFound()
        Else

            Dim txtInv_Rate As TextBox = gridPermitDetails.FooterRow.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = gridPermitDetails.FooterRow.FindControl("txtChqDate")
            Dim txtInv_Amt As TextBox = gridPermitDetails.FooterRow.FindControl("txtAmount")
            Dim ddlPayment As DropDownList = gridPermitDetails.FooterRow.FindControl("ddlPaymentType")
            Dim ddlPType As DropDownList = gridPermitDetails.FooterRow.FindControl("ddlPType")
            Dim txtPayable As TextBox = gridPermitDetails.FooterRow.FindControl("txtPayable")

        End If
    End Sub
    Protected Sub gridPermitDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridPermitDetails.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim txtPERMITNO_F As TextBox = e.Row.FindControl("txtPERMITNO")
                Dim hdnPER_ID_F As HiddenField = e.Row.FindControl("hdnITM_ID")
                Dim imgIssDate As ImageButton = e.Row.FindControl("imgIssDate")
                Dim imgExpDate As ImageButton = e.Row.FindControl("imgExpDate")
                Dim ddlPermitType As DropDownList = e.Row.FindControl("ddlPermitType")
                Dim ddlRenew As DropDownList = e.Row.FindControl("ddlRenew")
                Dim txtIssDate_F As TextBox = e.Row.FindControl("txtIssDate")
                Dim txtExpDate_F As TextBox = e.Row.FindControl("txtExpDate")
            End If
            If gridPermitDetails.ShowFooter Or gridPermitDetails.EditIndex > -1 Then
                Dim ddlPermitType As DropDownList = e.Row.FindControl("ddlPermitType")
                Dim ddlRenew As DropDownList = e.Row.FindControl("ddlRenew")

                If Not ddlPermitType Is Nothing Then
                    Dim sqlStr As String = ""
                    sqlStr &= "select VPT_ID,VPT_DESCR from VEHICLE_PERMIT_M order by VPT_DESCR "
                    fillDropdown(ddlPermitType, sqlStr, "VPT_DESCR", "VPT_ID", False)
                End If

                If Not ddlRenew Is Nothing Then
                    Dim sqlstr1 As String = "select 'YES' ID,'YES' DESCR union all select 'NO','NO'"
                    fillDropdown(ddlRenew, sqlstr1, "DESCR", "ID", False)
                End If
            End If
        End If
    End Sub

    Protected Sub gridPermitDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gridPermitDetails.RowDeleting
        Dim mRow() As DataRow = TPTPermitDetails.Select("ID=" & gridPermitDetails.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= mRow(0)("VEP_ID") & ";"
            TPTPermitDetails.Select("ID=" & gridPermitDetails.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            TPTPermitDetails.AcceptChanges()
        End If
        If TPTPermitDetails.Rows.Count = 0 Then
            TPTPermitDetails.Rows.Add(TPTPermitDetails.NewRow())
            TPTPermitDetails.Rows(0)(1) = -1
        End If

        gridPermitDetails.DataSource = TPTPermitDetails
        gridPermitDetails.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub gridPermitDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gridPermitDetails.RowEditing
        Try
            gridPermitDetails.ShowFooter = False
            gridPermitDetails.EditIndex = e.NewEditIndex
            gridPermitDetails.DataSource = TPTPermitDetails
            gridPermitDetails.DataBind()

            Dim txtIssDate As TextBox = gridPermitDetails.Rows(e.NewEditIndex).FindControl("txtIssDate")
            Dim txtExpDate As TextBox = gridPermitDetails.Rows(e.NewEditIndex).FindControl("txtExpDate")
            Dim hdnPermitType_ID As HiddenField = gridPermitDetails.Rows(e.NewEditIndex).FindControl("hdnITM_ID")
            Dim ddlPermit As DropDownList = gridPermitDetails.Rows(e.NewEditIndex).FindControl("ddlPermitType")
            Dim ddlRenew As DropDownList = gridPermitDetails.Rows(e.NewEditIndex).FindControl("ddlRenew")
            Dim txtPERMITNO As TextBox = gridPermitDetails.Rows(e.NewEditIndex).FindControl("txtPERMITNO")
            Dim imgIssdate As ImageButton = gridPermitDetails.Rows(e.NewEditIndex).FindControl("imgEIssDateDate")
            Dim imgExpdate As ImageButton = gridPermitDetails.Rows(e.NewEditIndex).FindControl("imgEExpDate")
            Dim h_VEP_Trdate As HiddenField = gridPermitDetails.Rows(e.NewEditIndex).FindControl("h_VEP_Trdate")
            Dim h_P_DATE As HiddenField = gridPermitDetails.Rows(e.NewEditIndex).FindControl("h_P_DATE")
            'If h_P_DATE.Value > h_VEP_Trdate.Value Then
            '    ddlPermit.Enabled = False
            '    txtPERMITNO.Enabled = False
            '    imgIssdate.Enabled = False
            '    imgExpdate.Enabled = False
            'End If
            ddlPermit.Items.FindByValue(hdnPermitType_ID.Value).Selected = True
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gridPermitDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gridPermitDetails.RowUpdating
        Dim s As String = gridPermitDetails.DataKeys(e.RowIndex).Value.ToString()
        Dim VEP_id As Label = gridPermitDetails.Rows(e.RowIndex).FindControl("lblVEP_ID")
        Dim ddlPermit As DropDownList = gridPermitDetails.Rows(e.RowIndex).FindControl("ddlPermitType")
        Dim ddlRenew As DropDownList = gridPermitDetails.Rows(e.RowIndex).FindControl("ddlRenew")
        Dim txtIssDate As TextBox = gridPermitDetails.Rows(e.RowIndex).FindControl("txtIssDate")
        Dim txtExpDate As TextBox = gridPermitDetails.Rows(e.RowIndex).FindControl("txtExpDate")
        Dim txtPERMITNO As TextBox = gridPermitDetails.Rows(e.RowIndex).FindControl("txtPERMITNO")
        Dim hdnITM_ID As HiddenField = gridPermitDetails.Rows(e.RowIndex).FindControl("hdnITM_ID")

        If txtPERMITNO.Text = "" Or txtIssDate.Text = "" Or txtExpDate.Text = "" Then
            lblError.Text = "Please enter permit Details properly."
        End If
        If CDate(txtIssDate.Text) > CDate(txtExpDate.Text) Then
            lblError.Text = "Issue date should be lessthan Expiry Date"
        End If
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If
        Dim mrow As DataRow
        mrow = TPTPermitDetails.Select("ID=" & s)(0)
        mrow("VEP_ID") = VEP_id.Text
        mrow("VEP_VPT_ID") = ddlPermit.SelectedValue
        mrow("VPT_DESCR") = ddlPermit.SelectedItem
        mrow("VEP_RENEW") = ddlRenew.SelectedItem

        mrow("VEP_ISSDATE") = txtIssDate.Text
        mrow("VEP_EXPDATE") = txtExpDate.Text
        mrow("VEP_PERMIT_NO") = txtPERMITNO.Text
        mrow("VEP_VEH_ID") = h_veh_id.Value

        gridPermitDetails.EditIndex = -1
        gridPermitDetails.ShowFooter = True
        gridPermitDetails.DataSource = TPTPermitDetails
        gridPermitDetails.DataBind()
    End Sub
End Class
