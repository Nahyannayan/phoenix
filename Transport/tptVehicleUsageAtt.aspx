﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptVehicleUsageAtt.aspx.vb" Inherits="Transport_tptVehicleUsageAtt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>Manual Attendance
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left">Fields Marked with (<span class="text-danger">*</span>) are mandatory
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblBsu" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Curriculum</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblCur" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>

                                <tr>

                                    <td align="left"><span class="field-label">Attendance Date</span> <span class="text-danger">*</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox
                                            ID="txtAtt" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgAtt" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtAtt" Display="Dynamic"
                                            ErrorMessage="Enter the start date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator3" runat="server" ControlToValidate="txtAtt" CssClass="error"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Attendance Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfFrom" runat="server" ControlToValidate="txtAtt"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter the Attendance date"></asp:RequiredFieldValidator>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4" class="title-bg">Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Trip Type</span></td>

                                    <td align="left">
                                        <asp:RadioButtonList ID="rblTrip" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Bus</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBus" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr style="display: none;">
                                    <td align="left"><span class="field-label">Location</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Area</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlArea" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                </tr>


                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:GridView ID="gvAtt" runat="server" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblid" runat="server" Text='<%# Bind("TRP_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltrip" runat="server" Text='<%# Bind("TRP_DESCR")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Students Travelled">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="lblstudNo" runat="server" Text='<%# Bind("vua_stud_tot")%>'></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                                            TargetControlID="lblstudNo">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Staff Travelled">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="lblstaffdNo" runat="server" Text='<%# Bind("vua_staff_tot")%>'></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                                            TargetControlID="lblstaffdNo">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop"></HeaderStyle>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Save" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>




                    <tr>
                        <td valign="bottom">&nbsp;
            
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgAtt" TargetControlID="txtAtt">
                </ajaxToolkit:CalendarExtender>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

