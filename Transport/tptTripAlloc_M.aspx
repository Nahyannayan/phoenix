<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptTripAlloc_M.aspx.vb" Inherits="Transport_tptTripAlloc_M" EnableEventValidation="false" EnableViewState="true" MaintainScrollPositionOnPostback="true" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="true">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function getVehInfo() {
            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;

            url = document.getElementById("<%=hfVehicleUrl.ClientID %>").value

            //window.showModalDialog(url, "", sFeatures);

            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;

        }

        function ShowSeats() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;


            //window.showModalDialog("tptSeatCapacity_AreaPickup_M.aspx", "", sFeatures);
            return ShowWindowWithClose("tptSeatCapacity_AreaPickup_M.aspx", 'search', '55%', '85%')
            return false;
            return false;

        }

        function moveOptionUp() {
            var obj = document.getElementById("<%=lstTrip.ClientId%>")
            if (!hasOptions(obj)) { return; }
            for (i = 0; i < obj.options.length; i++) {
                if (obj.options[i].selected) {
                    if (i != 0 && !obj.options[i - 1].selected) {
                        swapOptions(obj, i, i - 1);
                        obj.options[i - 1].selected = true;
                    }
                }
            }
            persistOptionsList();
        }
        function hasOptions(obj) {
            if (obj != null && obj.options != null) { return true; }
            return false;
        }


        function moveOptionDown() {
            var obj = document.getElementById("<%=lstTrip.ClientId%>")
            if (!hasOptions(obj)) { return; }
            for (i = obj.options.length - 1; i >= 0; i--) {
                if (obj.options[i].selected) {
                    if (i != (obj.options.length - 1) && !obj.options[i + 1].selected) {
                        swapOptions(obj, i, i + 1);
                        obj.options[i + 1].selected = true;
                    }
                }
            }
            persistOptionsList();
        }


        function removeSelectedOptions() {
            var from = document.getElementById("<%=lstTrip.ClientId%>")

            if (!hasOptions(from)) {
                persistOptionsList();
                return;
            }
            if (from.type == "select-one") {
                from.options[from.selectedIndex] = null;
            }
            else {
                for (var i = (from.options.length - 1) ; i >= 0; i--) {
                    var o = from.options[i];
                    if (o.selected) {
                        from.options[i] = null;
                    }
                }
            }
            from.selectedIndex = -1;
            persistOptionsList();
        }


        function RemoveOption() {

        }
        function swapOptions(obj, i, j) {
            var o = obj.options;
            var i_selected = o[i].selected;
            var j_selected = o[j].selected;
            var temp = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
            var temp2 = new Option(o[j].text, o[j].value, o[j].defaultSelected, o[j].selected);
            o[i] = temp2;
            o[j] = temp;
            o[i].selected = j_selected;
            o[j].selected = i_selected;
        }


        //to retain values in the trips listbox after postback the list items are stored in an input box	
        function persistOptionsList() {
            var listBox1 = document.getElementById("<%=lstTrip.ClientId%>");
            var optionsList = '';
            if (listBox1.options.length > 0) {
                for (var i = 0; i < listBox1.options.length; i++) {
                    var optionText = listBox1.options[i].text;
                    var optionValue = listBox1.options[i].value;

                    if (optionsList.length > 0)
                        optionsList += '|';
                    optionsList += optionText + "$" + optionValue;
                }
            }
            document.getElementById("<%=lstValues.ClientId%>").value = optionsList;
            return false;
        }



        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }


        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }


        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;
            if (parentNodeTable) {
                var checkUncheckSwitch;
                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }
                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) {
                    //check if the child node is an element node
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function UnCheckAll() {
            var oTree = document.getElementById("<%=tvLocations.ClientId %>");
            childChkBoxes = oTree.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = false;
            }

            return true;
        }








    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Trip Allocation Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">



                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfTrip" runat="server" ControlToValidate="txtTrip" ValidationGroup="v1"
                                Display="Dynamic" ErrorMessage="Please enter the field Trip" CssClass="error"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:LinkButton ID="lnkSeat" runat="server" CausesValidation="False" OnClientClick="javascript:return ShowSeats();"
                                Text="Seat Capacity"></asp:LinkButton></td>
                    </tr>

                    <tr>
                        <td align="center">Fields Marked with (<span class="text-danger">*</span>) are mandatory      
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">


                                <tr>

                                    <td align="left" width="20%"><span class="field-label">Trip</span><span class="text-danger">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTrip" runat="server"></asp:TextBox></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:RadioButton ID="rdOnward" runat="server" Text="Onward" Checked="True" GroupName="G1" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rdReturn" runat="server" Text="Return" GroupName="G1" CssClass="field-label"></asp:RadioButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Shift</span><span class="text-danger">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlShift" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Bus No.</span><span class="text-danger">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBusNo" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>

                                    <td align="left"><span class="field-label">Vehicle Category</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left"><span class="field-label">Vehicle Registration No.</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList AutoPostBack="true" ID="ddlVehicle" runat="server">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Label ID="lblCapacity" runat="server" Style="text-align: right"></asp:Label>
                                        <asp:LinkButton ID="lnkView" OnClientClick="getVehInfo(); return false;" runat="server">View</asp:LinkButton></td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Driver</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlDriver" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Conductor</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlConductor" runat="server">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                            <tr>
                                                <td align="center" width="40%">
                                                    <div class="checkbox-list">
                                                        <asp:TreeView ID="tvLocations" runat="server" 
                                                            onclick="return OnTreeClick(event);" ShowCheckBoxes="All"
                                                            Style="overflow: auto; text-align: left; padding :0px;"
                                                            MaxDataBindDepth="0" NodeIndent="0" ExpandDepth="0" SkipLinkText="" >                                                           
                                                        </asp:TreeView>
                                                    </div>
                                                </td>
                                                <td align="center" width="10%">
                                                    <asp:Button ID="btnTransfer" runat="server" CssClass="button" Text=">>" CausesValidation="False" />
                                                    <br />
                                                    <br />
                                                </td>

                                                <td align="left" width="40%">
                                                    <div class="checkbox-list">
                                                        <asp:ListBox ID="lstTrip" runat="server" SelectionMode="Multiple" Rows="10" Style="overflow: hidden; min-width: 100% !important;"></asp:ListBox>
                                                    </div>
                                                </td>

                                                <td align="center" width="10%">
                                                    <input id="btnUp" type="button" value="Up" onclick="moveOptionUp()" class="button" />
                                                    <br />
                                                    <br />
                                                    <input id="btnDown" type="button" value="Down" onclick="moveOptionDown()" class="button" />
                                                    <br />
                                                    <br />
                                                    <input id="btnRemove" type="button" value="Remove" onclick="removeSelectedOptions()" class="button" />

                                                </td>

                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" TabIndex="5" OnClientClick="return UnCheckAll();" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" TabIndex="6" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                            TabIndex="7" ValidationGroup="v1" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" OnClientClick="return UnCheckAll();" TabIndex="8" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete"
                                            OnClientClick="return UnCheckAll();return confirm_delete();" TabIndex="8" /></td>
                                </tr>
                            </table>

                            <asp:HiddenField ID="hfTRD_ID" runat="server" />
                            <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                HorizontalSide="Center" TargetControlID="Panel1" VerticalSide="Middle">
                            </ajaxToolkit:AlwaysVisibleControlExtender>
                            <asp:Panel ID="Panel1" CssClass="panel-cover" runat="server" Style="z-index: 10; left: 470px; position: absolute; top: 645px; padding:20px"
                                Visible="false" >
                                <asp:Label ID="lblText" runat="server" Style="left: -4px; position: relative; "
                                    Width="263px"></asp:Label><br />
                                <asp:Button ID="btnYes" runat="server" CssClass="button" 
                                    Text="Yes" />
                                <asp:Button ID="btnNo" runat="server" CssClass="button" 
                                    Text="No" />
                            </asp:Panel>
                            <input id="lstValues" name="lstValues" runat="server" type="hidden" />
                            <asp:HiddenField ID="hfTRP_ID" runat="server" />
                            <asp:HiddenField ID="hfVehicleUrl" runat="server" />
                            <asp:HiddenField ID="hfDRV_ID" runat="server" />
                            <asp:HiddenField ID="hfVEH_ID" runat="server" />
                            <asp:HiddenField ID="hfCON_ID" runat="server" />

                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>


