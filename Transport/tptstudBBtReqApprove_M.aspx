<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptstudBBtReqApprove_M.aspx.vb" Inherits="Transport_tptstudBBtReqApprove_M" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        <%--function UploadPhoto() {
            //        document.forms[0].Submit();
            var filepath = document.getElementById('<%=FUUploadEmpPhoto.ClientID %>').value;
        if ((filepath != '') && (document.getElementById('<%=hfEmpImagePath.ClientID %>').value != filepath)) {
            document.getElementById('<%=hfEmpImagePath.ClientID %>').value = filepath;
            document.forms[0].submit();
        }
    }--%>

        function PShowSeats() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var sbl_id;

            sbl_id = document.getElementById("<%=hfPSBL_ID.ClientID%>").value
            url = 'tptSeatCapacity_SelectArea_M.aspx?sbl_id=' + sbl_id;
            //window.showModalDialog(url, "", sFeatures);
            //return false;
            var oWnd = radopen(url, "pop_PShowSeats");
        }

        function DShowSeats() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var sbl_id;
            sbl_id = document.getElementById("<%=hfDSBL_ID.ClientID%>").value
            url = 'tptSeatCapacity_SelectArea_M.aspx?sbl_id=' + sbl_id;
            //window.showModalDialog(url, "", sFeatures);
            //return false;
            var oWnd = radopen(url, "pop_DShowSeats");
        }

        function PAddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 400px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            window.returnValue = true;
            pnt_id = document.getElementById("<%=hfPPNT_ID.ClientID%>").value
            bsu_id = document.getElementById("<%=hfBSU_ID.ClientID%>").value
            url = 'tptAddPickuptoTripNonGems.aspx?pntid=' + pnt_id + '&bsuid=' + bsu_id;
            //window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_PAddtoTrip");
        }

        function DAddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 400px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            pnt_id = document.getElementById("<%=hfDPNT_ID.ClientID%>").value
            bsu_id = document.getElementById("<%=hfBSU_ID.ClientID%>").value
            url = 'tptAddPickuptoTripNonGems.aspx?pntid=' + pnt_id + '&bsuid=' + bsu_id;
            //window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_DAddtoTrip");
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_PShowSeats" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_DShowSeats" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_PAddtoTrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_DAddtoTrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Transport Registration Approval
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%" cellpadding="0" cellspacing="0">

                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ValidationGroup="groupM1" />

                            <asp:RequiredFieldValidator ID="rfPFirstName" runat="server" ControlToValidate="txtPFirstName"
                                Display="None" ErrorMessage="Please enter the parent first name" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfPlastName" runat="server" ControlToValidate="txtPLastName"
                                Display="None" ErrorMessage="Please enter the parent last name" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfAddress" runat="server" ControlToValidate="txtAddress"
                                Display="None" ErrorMessage="Please enter the address" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfOffph" runat="server" ControlToValidate="txtOffPhone"
                                Display="None" ErrorMessage="Please enter the office phone" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfresPh" runat="server" ControlToValidate="txtResPhone"
                                Display="None" ErrorMessage="Please enter the home phone" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfMob" runat="server" ControlToValidate="txtOffPhone"
                                Display="None" ErrorMessage="Please enter the mobile number" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfEmail" runat="server" ControlToValidate="txtEmail"
                                Display="None" ErrorMessage="Please enter the email address" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regEmail" runat="server" ControlToValidate="txtEmail"
                                Display="None" ErrorMessage="Please enter a valid email address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="groupM1"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfAdminArea" runat="server" ControlToValidate="ddlAreaForAdmin"
                                Display="None" ErrorMessage="Please select a Pickup Area" InitialValue="0"
                                SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator><asp:RequiredFieldValidator ID="rfAdminDropArea" runat="server" ControlToValidate="ddlDropAreaForAdmin"
                                    Display="None" ErrorMessage="Please select a Dropoff Area" InitialValue="0"
                                    SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator><asp:RequiredFieldValidator ID="rfAdminPickUpPoint" runat="server" ControlToValidate="ddlAreaForAdmin"
                                        Display="None" ErrorMessage="Please select a Pickup Point" InitialValue="0"
                                        SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator><asp:RequiredFieldValidator ID="rfAdminDropoffPoint" runat="server" ControlToValidate="ddlDropAreaForAdmin"
                                            Display="None" ErrorMessage="Please select a Dropoff oint" InitialValue="0"
                                            SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:RegularExpressionValidator ID="regAltEmail" runat="server" ControlToValidate="txtAltEmail"
                                Display="None" ErrorMessage="Please enter a valid alternate email address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="groupM1"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfFirstName" runat="server" ControlToValidate="txtFirstName"
                                Display="None" ErrorMessage="Please enter the student first name" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfLastName" runat="server" ControlToValidate="txtLastName"
                                Display="None" ErrorMessage="Please enter the student last name" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfDOB" runat="server" ControlToValidate="txtDob"
                                Display="None" ErrorMessage="Please enter the studentdate of birth" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="left">
                            <table width="100%" border="0" cellpadding="0">
                                <%--<tr class="subheader_img">
                                    <td align="left" colspan="6">TRANSPORT
                               REGISTRATION APPROVAL
                   
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Start Date </span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="4" /></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Pickup Area</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblPickupArea" runat="server" Text="" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left"><span class="field-label">Pickup Point</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblPickupPoint" runat="server" Text="" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Dropoff Area</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblDropoffArea" runat="server" Text="" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Dropoff Point</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblDropoffPoint" runat="server" Text="" CssClass="field-value"></asp:Label></td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <span class="field-label">Detailed location address/map to determine pick-up/drop-off points: </span></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtLocation" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Parent Details</td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RadioButton ID="rdFather" runat="server" EnableTheming="False" Text="Father" Checked="True" GroupName="g1" CssClass="field-label" />
                                        <asp:RadioButton ID="rdMother" runat="server" Text="Mother" GroupName="g1" CssClass="field-label" />
                                        <asp:RadioButton ID="rdGuardian" runat="server" EnableViewState="False" Text="Guardian" GroupName="g1" CssClass="field-label" /></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Title</span> <span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTitle" runat="server">
                                            <asp:ListItem>Mr.</asp:ListItem>
                                            <asp:ListItem>Mrs.</asp:ListItem>
                                            <asp:ListItem>Ms</asp:ListItem>
                                            <asp:ListItem>Dr.</asp:ListItem>
                                            <asp:ListItem>Prof.</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Parent Name</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPFirstName" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPMidName" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPLastName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Po Box No.</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPoBox" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Emirate</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlEmirate" runat="server" Width="94px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">House/Villa street No</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtHouseNo" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Street Address</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>

                                    <td align="left"><span class="field-label">Office Tel </span><span class="text-danger">*</span></td>

                                    <td>
                                        <asp:TextBox ID="txtOffPhone" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Residence Tel </span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtResPhone" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>

                                    <td align="left"><span class="field-label">Father Mobile</span> <span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Mother Mobile</span> <span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMotherMob" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Emergency contact</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmergencyMob" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Email Address</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>

                                    <td align="left"><span class="field-label">Alternate Email</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAltEmail" runat="server"></asp:TextBox></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Student Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">First Name </span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtMidName" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Date of Birth</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDob" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgDob" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="4" /></td>
                                    <td align="left"><span class="field-label">Student Photo</span>
                                        <br />
                                        <asp:FileUpload ID="FUUploadEmpPhoto" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Image ID="imgEmpImage"
                                            Height="105px" runat="server" ImageUrl="~/Images/Photos/no_image.gif"
                                            Width="106px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Gender</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:RadioButton ID="rdMale" runat="server" Checked="True" GroupName="g2" Text="Male" CssClass="field-label" />
                                        <asp:RadioButton ID="rdFemale" runat="server" GroupName="g2" Text="Female" CssClass="field-label" /></td>
                                    <td align="left"><span class="field-label">Academic Year</span> <span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>

                                    <td align="left"><span class="field-label">Grade </span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>

                                    <td align="left"><span class="field-label">Medical Condition/Allergy(If Any)</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMedCond" runat="server"></asp:TextBox>
                                    </td>
                                </tr>



                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Pickup Point &nbsp; &nbsp; &nbsp;
                                        <asp:LinkButton ID="lnkSeat"
                                            runat="server" CausesValidation="False" OnClientClick="javascript: PShowSeats(); return false;"
                                            Text="Seat Capacity"></asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Trip Type</span></td>

                                    <td>
                                        <asp:DropDownList ID="ddlTripType" runat="server" AutoPostBack="true">
                                            <asp:ListItem>--</asp:ListItem>
                                            <asp:ListItem>Round Trip</asp:ListItem>
                                            <asp:ListItem>Pickup Only</asp:ListItem>
                                            <asp:ListItem>Dropoff Only</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Location</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblPlocation" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Area</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAreaForAdmin" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">PickupPoint</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPickUpChoose" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:LinkButton ID="lnKPickup" runat="server" OnClientClick="javascript: PAddtoTrip(); return false;">Add to Trip</asp:LinkButton></td>
                                </tr>


                                <tr>

                                    <td align="left"><span class="field-label">Trip</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlonward" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:GridView ID="gvOnward" runat="server" Width="100%" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Capacity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Available">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle></HeaderStyle>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <SelectedRowStyle></SelectedRowStyle>
                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Drop Off Point &nbsp; &nbsp; &nbsp;       
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False"
                                            OnClientClick="javascript: DShowSeats(); return false;" Text="Seat Capacity">
                                        </asp:LinkButton></td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label9" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>

                                    <td align="left">
                                        <asp:Label ID="lblDlocation" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Area</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlDropAreaForAdmin" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">DropoffPoint</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlDropoffChoose" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:LinkButton ID="lnkDropOff" runat="server" OnClientClick="javascript: DAddtoTrip(); return false;">Add to Trip</asp:LinkButton></td>
                                </tr>


                                <tr>
                                    <td align="left"><span class="field-label">Trip</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReturn" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" colspan="2">
                                        <asp:GridView ID="gvReturn" runat="server" Width="100%" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Capacity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Available">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle></HeaderStyle>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <SelectedRowStyle></SelectedRowStyle>
                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Approval Details</td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Approval Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtApprove" runat="server" AutoPostBack="True">
                                        </asp:TextBox><asp:ImageButton ID="imgApprove" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:CustomValidator
                                            ID="CustomValidator1" runat="server" ControlToValidate="txtApprove" CssClass="error"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Approval Date entered is not a valid date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtApprove"
                                            Display="Dynamic" ErrorMessage="Enter the Approval Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtApprove"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter the approval date">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>


                                <%--<tr>
                            <td align="center" class="matters" colspan="8">
                               
                                
                                <asp:Button ID="btnAccept1" runat="server" CssClass="button"
                                    Text="Accept" />
                                <asp:Button ID="btnReject1" runat="server" CssClass="button"
                                    Text="Reject" />
                                <asp:Button ID="btnCancel1" runat="server"
                                        CausesValidation="False" CssClass="button" Text="Cancel" />
                                        
                                           <asp:Button ID="btnPrint1" runat="server" CssClass="button" Text="Print" />
                                        </td>
                        </tr> --%>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                                        <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfAcaID" runat="server" />
                <asp:HiddenField ID="hfSTU_ID" runat="server" />
                <asp:HiddenField ID="hfFEE_ID" runat="server" />
                <asp:HiddenField ID="hfUser" runat="server" />
                <asp:HiddenField ID="hfGRM_ID" runat="server" />



                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" TargetControlID="txtFrom" PopupButtonID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar" TargetControlID="txtFrom" PopupButtonID="imgFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfReg" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfBBT_ID" runat="server"></asp:HiddenField>
                <br />
                <asp:HiddenField ID="hfBSU_ID" runat="server"></asp:HiddenField>
                <%--<asp:HiddenField ID="hfEmpImagePath" runat="server"></asp:HiddenField>--%>

                <asp:HiddenField ID="hfSNR_ID" runat="server" />
                <asp:HiddenField ID="hfSHF_ID" runat="server" />
                <asp:HiddenField ID="hfPSBL_ID" runat="server" />
                <asp:HiddenField ID="hfDSBL_ID" runat="server" />
                <asp:HiddenField ID="hfPPNT_ID" runat="server" />
                <asp:HiddenField ID="hfDPNT_ID" runat="server" />
                <asp:HiddenField ID="hfParent" runat="server" />
                <asp:HiddenField ID="hfEmail" runat="server" />
                <asp:HiddenField ID="hfSave" runat="server" />
                <asp:HiddenField ID="hfImageName" runat="server" />

                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" TargetControlID="txtDob" PopupButtonID="txtDob" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgApprove" TargetControlID="txtApprove">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtApprove" PopupPosition="TopRight" TargetControlID="txtApprove">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" TargetControlID="txtDob" PopupButtonID="imgDob" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

