Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Transport_tptAddPickuptoTrip
    Inherits System.Web.UI.Page
    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTrip.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvTrip.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvTrip.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvTrip.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub BindSublocation()
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM oasis.oasis_transport.TRANSPORT.SUBLOCATION_M AS A " _
                                 & " INNER JOIN oasis.oasis_transport.TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID WHERE PNT_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " ORDER BY SBL_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubLocation.DataSource = ds
        ddlSubLocation.DataTextField = "SBL_DESCRIPTION"
        ddlSubLocation.DataValueField = "SBL_ID"
        ddlSubLocation.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubLocation.Items.Insert(0, li)
        ds = Nothing
        ScriptManager.GetCurrent(Page).SetFocus(ddlPickup)
    End Sub

    Sub BindPickup(ByVal sblid As String)
        Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM oasis.oasis_transport.TRANSPORT.PICKUPPOINTS_M AS A  " _
                                   & " WHERE PNT_SBL_ID = " + sblid _
                                   & " AND PNT_BSU_ID=" + Session("SBSUID") _
                                   & " ORDER BY PNT_DESCRIPTION"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPickup.DataSource = ds
        ddlPickup.DataTextField = "PNT_DESCRIPTION"
        ddlPickup.DataValueField = "PNT_ID"
        ddlPickup.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlPickup.Items.Insert(0, li)
        ds = Nothing

        li = New ListItem

        For Each li In ddlPickup.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function filldropdown(ByVal report As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim params() As String = report.Split(";")
        Dim sql As String = ";with cte as (SELECT PNT_ID id,case when lat is null then '' else '*' end+PNT_DESCRIPTION descr FROM oasis.oasis_transport.TRANSPORT.PICKUPPOINTS_M AS A "
        sql &= "left outer join [172.25.29.52].sts_database.dbo.pickuppoint on pntid=pnt_id WHERE PNT_SBL_ID = " & params(0) & " AND PNT_BSU_ID='" & params(1) & "') "
        sql &= "Select isnull((STUFF((Select ', ["" '+cast(id as varchar)+'"",'+'""'+descr+'""]' from cte ORDER BY descr FOR XML PATH('')),1,2,'')),'') "

        filldropdown = ""
        Try
            filldropdown = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql)

        Catch ex As Exception
            filldropdown = ex.Message
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function saveGPSdata(ByVal report As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        saveGPSdata = ""

        Try
            Dim reportValues() As String = report.Split(";")
            saveGPSdata = report 'SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql)

            'if @PNT_LOC_ID=0 select top 1 @PNT_LOC_ID=PNT_LOC_ID from TRANSPORT.PICKUPPOINTS_M where PNT_BSU_ID=@PNT_BSU_ID and PNT_SBL_ID=@PNT_SBL_ID
            '[TRANSPORT].[SavePICKUPPOINTS]
            If reportValues(0).Length > 0 Then
                'Dim str_query As String = "exec [TRANSPORT].[SavePICKUPPOINTS] 0," + reportValues(2) + ",'" + reportValues(0) + "'," + reportValues(1) + ",'" + reportValues(3) + "','" + "Add" + "',0"
                'Dim pntid As Int16 = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            End If
            'lblError.Text = str_query & ";" & ddlPoint.SelectedValue
            'Exit Sub

        Catch ex As Exception
            Dim errMsg = ex.Message
        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getTable(ByVal report As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim params() As String = report.Split(";")
        Dim sql As String = ";with bno as (SELECT distinct BNO_DESCR, TPP_PNT_ID "
        sql &= "From oasis.oasis_transport.TRANSPORT.TRIPS_M AS A INNER Join oasis.oasis_transport.TRANSPORT.TRIPS_D AS B inner Join oasis.oasis_transport.TRANSPORT.BUSNOS_M on BNO_ID=TRD_BNO_ID inner Join oasis.oasis_transport.TRANSPORT.TRIPS_PICKUP_S on TRD_ID=TPP_TRD_ID "
        sql &= "On A.TRP_ID=B.TRD_TRP_ID And TRD_TODATE Is NULL "
        sql &= "WHERE TRP_JOURNEY <>'EXTRA' AND TRD_BSU_ID='" & params(1) & "'), pnt as ( "
        sql &= "Select sbl_description, pnt_id, pnt_description, cast(lat As varchar) lat, cast(lon As varchar) lon "
        sql &= "From oasis.oasis_transport.TRANSPORT.SUBLOCATION_M AS A INNER Join oasis.oasis_transport.TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID "
        sql &= "inner Join [172.25.29.52].sts_database.dbo.pickuppoint On pnt_id=pntid Where pnt_bsu_id ='" & params(1) & "' and sbl_id=" & params(0) & "), cte as ( "
        sql &= "Select * from bno inner Join pnt On tpp_pnt_id=pnt_id  union select 0, 0, 'School', 0, bsu_name, lat, lon from businessunit where bsu_id='" & params(1) & "') "
        sql &= "Select isnull((STUFF((Select ', ["" '+cast(count(*) as varchar)+'"",'+'""'+pnt_description+'"",'+cast(lat as varchar)+','+cast(lon as varchar)+']' "
        sql &= "From cte Group By pnt_description, lat, lon FOR XML PATH('')),1,2,'')),'') "
        getTable = ""
        Try
            getTable = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql)

        Catch ex As Exception
            Dim errMsg = ex.Message
        End Try

    End Function

    Sub BindBusNo()
        Dim str_query As String = "Select BNO_DESCR,BNO_ID FROM oasis.oasis_transport.TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + Session("SBSUID") + "'" _
                                & " ORDER BY BNO_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlBusNo.Items.Insert(0, li)
    End Sub

    Sub BindShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SHF_ID,SHF_DESCR FROM oasis_transport.dbo.VV_SHIFTS_M WHERE SHF_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Private Sub GridBind(Optional ByVal history As Boolean = False)
        Dim str_query As String = "SELECT TRD_ID,TRP_ID,TRP_DESCR, TRP_JOURNEY" _
                                 & " FROM oasis.oasis_transport.TRANSPORT.TRIPS_M AS A INNER JOIN oasis.oasis_transport.TRANSPORT.TRIPS_D AS B" _
                                 & " ON A.TRP_ID=B.TRD_TRP_ID AND TRD_TODATE IS NULL " _
                                 & " WHERE TRP_JOURNEY<>'EXTRA' AND TRD_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND TRD_BNO_ID=" + ddlBusNo.SelectedValue.ToString _
                                 & " AND TRP_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                 & " AND TRD_ID NOT IN (SELECT TPP_TRD_ID FROM oasis.oasis_transport.TRANSPORT.TRIPS_PICKUP_S WHERE TPP_PNT_ID=" + ddlPickup.SelectedValue.ToString + ")"
        Dim txtTrip As New TextBox
        Dim ddlgvVehicle As New DropDownList
        Dim ddlgvShift As New DropDownList
        Dim ddlgvJourney As New DropDownList
        Dim ddlgvBus As New DropDownList
        Dim ddlgvCategory As New DropDownList
        Dim selectedTrips As String = ""
        Dim selectedVehicle As String = ""
        Dim selectedShift As String = ""
        Dim selectedJourney As String = ""
        Dim selectedBus As String = ""
        Dim selectedCategory As String = ""

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim txtSearch As New TextBox

        Dim drvSearch As String = ""
        Dim conSearch As String = ""
        Dim trpSearch As String = ""

        If gvTrip.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvTrip.HeaderRow.FindControl("txtTrip")
            strSidsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("TRP_DESCR", txtSearch.Text, strSearch)
            trpSearch = txtSearch.Text

            ddlgvJourney = gvTrip.HeaderRow.FindControl("ddlgvJourney")
            If ddlgvJourney.Text <> "ALL" Then
                strFilter += " and TRP_JOURNEY='" + ddlgvJourney.Text + "'"
                selectedJourney = ddlgvJourney.Text
            End If
            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If

        str_query += " ORDER BY TRP_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTrip.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTrip.DataBind()
            Dim columnCount As Integer = gvTrip.Rows(0).Cells.Count
            gvTrip.Rows(0).Cells.Clear()
            gvTrip.Rows(0).Cells.Add(New TableCell)
            gvTrip.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTrip.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTrip.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTrip.DataBind()
        End If

        Dim dt As DataTable = ds.Tables(0)
        If gvTrip.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvTrip.HeaderRow.FindControl("txtTrip")
            txtSearch.Text = trpSearch
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        Dim chkSelect As CheckBox
        Dim lblTrdId As Label
        Dim lblTrpId As Label
        Dim i As Integer

        For i = 0 To gvTrip.Rows.Count - 1
            With gvTrip.Rows(i)
                chkSelect = .FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblTrdId = .FindControl("lblTrdId")
                    lblTrpId = .FindControl("lblTrpId")
                    str_query = "EXEC oasis_transport.TRANSPORT.savePICKUPTOTRIP " _
                               & "'" + Session("sbsuid") + "'," _
                               & lblTrpId.Text + "," _
                               & lblTrdId.Text + "," _
                               & ddlPickup.SelectedValue.ToString
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

            'hfPNT_ID.Value = Request.QueryString("pntid")
            'If hfPNT_ID.Value = "0" Then
            '    lblError.Text = "No pickup Selected"
            '    btnSave.Visible = False
            '    Exit Sub
            'End If
            'BindBusNo()
            'BindShift()
            BindShift()
            BindSublocation()
            BindPickup(ddlSubLocation.SelectedValue.ToString)
            BindBusNo()
            GridBind()
            hBSU_ID.Value = Session("SBSUID")
            hCLM_ID.Value = Session("clm")
        End If
        'If h_SelectedId.Value <> "Close" Then
        '    Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

        '    Response.Write("} </script>" & vbCrLf)
        'End If
    End Sub

    Protected Sub btnTrip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvJourney_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlBusNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusNo.SelectedIndexChanged
        GridBind()
        ScriptManager.GetCurrent(Page).SetFocus(ddlPickup)
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        GridBind()
        ScriptManager.GetCurrent(Page).SetFocus(ddlPickup)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            GridBind()
            lblError.Text = "Records Saved Successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        BindPickup(ddlSubLocation.SelectedValue.ToString)
    End Sub

    Private Sub ddlPickup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPickup.SelectedIndexChanged
        ScriptManager.GetCurrent(Page).SetFocus(ddlPickup)
    End Sub

    'Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
    '    'if @PNT_LOC_ID=0 select top 1 @PNT_LOC_ID=PNT_LOC_ID from TRANSPORT.PICKUPPOINTS_M where PNT_BSU_ID=@PNT_BSU_ID and PNT_SBL_ID=@PNT_SBL_ID
    '    '[TRANSPORT].[SavePICKUPPOINTS]
    '    Dim str_query As String = "exec [TRANSPORT].[SavePICKUPPOINTS] 0," + ddlSubLocation.SelectedValue + ",'" + Session("sbsuid").ToString + "'," + Session("clm") + ",'" + txtPoint.Text + "','" + "Add" + "',0"
    '    lblError.Text = str_query & ";" & ddlPoint.SelectedValue
    '    Exit Sub
    '    Dim pntid As Int16 = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    'End Sub
End Class
