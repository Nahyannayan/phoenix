<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tptAddAreaPickuptoTrip.aspx.vb" Inherits="Transport_tptAddPickuptoTrip" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Trips</title>
  <base target="_self" />
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
     <script language="javascript" type="text/javascript" src="../../cssfiles/chromejs/chrome.js"></script> 
     <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />

      <script language="javascript" type="text/javascript">     
                      
        var color = ''; 
        function highlight(obj)
        { 
        var rowObject = getParentRow(obj); 
        var parentTable = document.getElementById("<%=gvTrip.ClientID %>"); 
        if(color == '') 
        {
        color = getRowColor(); 
        } 
        if(obj.checked) 
        { 
        rowObject.style.backgroundColor ='#f6deb2'; 
        }
        else 
        {
        rowObject.style.backgroundColor = '';  
        color = ''; 
        }
        // private method

        function getRowColor() 
        {
        if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
        else return rowObject.style.backgroundColor; 
        }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) 
        {  
        do 
        {
        obj = obj.parentElement;
        }
        while(obj.tagName != "TR") 
        return obj; 
        }

        function runMap() {
            //showMap('["W 97731",25.2703,55.3657], ["W 97736",25.2662,55.431], ["V 89677",25.2649,55.4296]');
            //var Area = $get("<%=ddlSubLocation.ClientID %>").value;
            //var e = document.getElementById("ddlSubLocation");
            //var Area = e.options[e.selectedIndex].value
            var area = document.getElementById("ddlSubLocation").value;
            var unit = document.getElementById("hBSU_ID").value;
            PageMethods.filldropdown(area + ';' + unit, CallDropdown, CallFailed);
            //PageMethods.filldropdown('22;125017', CallDropdown, CallFailed);
            PageMethods.getTable(area + ';' + unit, CallSuccess, CallFailed);
        }
          function doUpdate() {              
              var bsu = document.getElementById("hBSU_ID").value;
              var clm = document.getElementById("hCLM_ID").value;
              var area = document.getElementById("ddlSubLocation").value;
              var area = document.getElementById("ddlSubLocation").value;
              var pickup = document.getElementById("ddlPoint").value;
              var Lat = document.getElementById("txtLat").value;
              var Lon = document.getElementById("txtLon").value;
              var point = document.getElementById("txtPoint").value;
              if ((pickup.length < 3 && point == 0) || Lat.length == 0)
                  alert("nothing to save?");
              else
                  if (pickup.length >= 3 && point.length > 0)
                      alert("New point or existing point?");
                  else
                      PageMethods.saveGPSdata(bsu + ';' + clm + ';' + area + ';' + pickup + ';' + point + ';' + Lat + ';' + Lon, CallSave, CallFailed);
          }
          function CallSave(res) {
              alert(res);
          }
          function CallDropdown(res) {
              //var resary = res.split(";");
              //alert(res);
              if (res != null)
                  if (res != "" || res != null) {
                  }
              if (res.length > 0) {
                  var locations = JSON.parse("[" + res + "]");
                  var i;
                  var select1 = document.getElementById('<%= ddlPickup.ClientID %>');
                  select1.length = 0;
                  for (i = 0; i < locations.length; i++) {
                    var option = document.createElement("option");
                    option.value = locations[i][0];
                    option.innerHTML = locations[i][1];
                    select1.appendChild(option);
                  }
                  var select2 = document.getElementById('<%= ddlPoint.ClientID %>');
                  select2.length = 0;
                  var option = document.createElement("option");
                  option.value = "0";
                  option.innerHTML = "NA";
                  select2.appendChild(option);
                  for (i = 0; i < locations.length; i++) {
                      var option = document.createElement("option");
                      option.value = locations[i][0];
                      option.innerHTML = locations[i][1];
                      select2.appendChild(option);
                  }
              }
          }

        function CallSuccess(res) {
              //var resary = res.split(";");
              if (res != null)
                  if (res != "" || res != null) {
                  }
              if (res.length>0) showMap(res);
        }

        function CallFailed(res) {
            alert(res.get_message());
            //alert("fail");
        }

        function showMap(myString) {
            var locations = JSON.parse("[" + myString + "]");
            var map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 13,
                center: new google.maps.LatLng(locations[0][2], locations[0][3]),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            google.maps.event.addListener(map, 'click', function (event) {
                mapZoom = map.getZoom();
                startLocation = event.latLng;
                document.getElementById("txtLat").value = startLocation.lat();
                document.getElementById("txtLon").value = startLocation.lng();
                //setTimeout(placeMarker, 600);
            });

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][2], locations[i][3]),
                    label: locations[i][0],
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][1]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
        function placeMarker() {
            alert('ok');
        }
        function change_chk_state(chkThis)
             {
            var chk_state= ! chkThis.checked ;
             for(i=0; i<document.forms[0].elements.length; i++)
                   {
                   var currentid =document.forms[0].elements[i].id; 
                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
                 {
                   //if (document.forms[0].elements[i].type=='checkbox' )
                      //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                        document.forms[0].elements[i].checked=chk_state;
                         document.forms[0].elements[i].click();//fire the click event of the child element
                     }
                  }
              }
          
             function test(val)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                }
                
                  function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                  }
          //function listen_window() { }
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyC3wIDw5B28tMJe3o5DyeT1_DQU_XawOEA" async="" defer="defer" type="text/javascript"></script>

</head>
<body> 
    
    <form id="form1" runat="server">
<%--<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server"></ajaxToolkit:ToolkitScriptManager>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" EnablePageMethods="true" runat="server"></ajaxToolkit:ToolkitScriptManager>
<asp:ScriptManager ID="ScriptManager2" EnablePageMethods="true" runat="server"></asp:ScriptManager>--%>
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager2" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                                    runat="server" EnablePageMethods="true" EnablePartialRendering="true"></ajaxToolkit:ToolkitScriptManager>
    <div>
        <div id="dropmenu" class="dropmenudiv" style="left: 153px; width: 110px; top: 1px">
            <a href="javascript:test('LI');">
                <img alt="Any where" class="img_left" src="../../Images/operations/like.gif" />
                Any Where</a> <a href="javascript:test('NLI');">
                    <img alt="Not In" class="img_left" src="../../Images/operations/notlike.gif" />
                    Not In</a> <a href="javascript:test('SW');">
                        <img alt="Starts With" class="img_left" src="../../Images/operations/startswith.gif" />
                        Starts With</a> <a href="javascript:test('NSW');">
                            <img alt="Like" class="img_left" src="../../Images/operations/notstartwith.gif" />
                            Not Start With</a> <a href="javascript:test('EW');">
                                <img alt="Like" class="img_left" src="../../Images/operations/endswith.gif" />
                                Ends With</a> <a href="javascript:test('NEW');">
                                    <img alt="Like" class="img_left" src="../../Images/operations/notendswith.gif" />
                                    Not Ends With</a></div>
        <div id="dropmenu1" class="dropmenudiv" style="left: 0px; width: 110px; top: -1px">
            <a href="javascript:test1('LI');">
                <img alt="Like" class="img_left" src="../../Images/operations/like.gif" />
                Any Where</a> <a href="javascript:test1('NLI');">
                    <img alt="Like" class="img_left" src="../../Images/operations/notlike.gif" />
                    Not In</a> <a href="javascript:test1('SW');">
                        <img alt="Like" class="img_left" src="../../Images/operations/startswith.gif" />
                        Starts With</a> <a href="javascript:test1('NSW');">
                            <img alt="Like" class="img_left" src="../../Images/operations/notstartwith.gif" />
                            Not Start With</a> <a href="javascript:test1('EW');">
                                <img alt="Like" class="img_left" src="../../Images/operations/endswith.gif" />
                                Ends With</a> <a href="javascript:test1('NEW');">
                                    <img alt="Like" class="img_left" src="../../Images/operations/notendswith.gif" />
                                    Not Ends With</a></div>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional"><ContentTemplate>
<table align="left" border="0" bordercolor="#1b80b6" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="WIDTH: 120px"></td>
        <td align="left" class="matters" colspan="2" style="width: 567px;" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; color: #0000ff">
            <tr>
                <td style="width: 511px;">

        <table align="left" border="0" bordercolor="#1b80b6" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center" class="matters" colspan="3" style="height: 35px" valign="top">
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td></tr>
            <tr>
                <td align="left" class="matters" style="height: 35px; width: 1011px;" valign="top">Select Area ver 1</td>
                <td style="width: 348px; height: 35px" class="matters">:</td>
                <td align="left" style="height: 35px">
                    <asp:DropDownList ID="ddlSubLocation" runat="server" onblur="runMap();" Width="211px"></asp:DropDownList></td></tr>
            <tr>
                <td align="left" class="matters" style="height: 35px; width: 1011px;" valign="top">Select Pickup</td>
                <td style="width: 348px; height: 35px" class="matters">:</td>
                <td align="left" style="height: 35px">
                    <asp:DropDownList ID="ddlPickup" runat="server" Width="354px"></asp:DropDownList></td></tr>
            <tr>
                <td align="left" class="matters"  style="height: 35px; width: 1011px;" valign="top">Select Bus No </td>
                <td style="width: 348px; height: 35px" class="matters">:</td>
                <td style="height: 35px" align="left">
                    <asp:DropDownList ID="ddlBusNo" runat="server" AutoPostBack="True" Width="168px"></asp:DropDownList></td></tr>
            <tr>
                <td align="left" class="matters" style="height: 35px; width: 1011px;" valign="top">Select Shift</td>
                <td style="width: 348px" class="matters">:</td>
                <td align="left" style="height: 35px">
                    <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" Width="120px"></asp:DropDownList></td></tr>
            <tr>
                <td align="center" class="matters" colspan="6" style="" valign="top">                              
                    <asp:GridView ID="gvTrip" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="80%" CssClass="gridstyle">
                    <Columns>       
                        <asp:TemplateField HeaderText="sbg_id" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblTrpId" runat="server" Text='<%# Bind("TRP_ID") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Left"></ItemStyle></asp:TemplateField>
         
                            <asp:TemplateField HeaderText="sbg_id" Visible="False"><ItemTemplate>
                                <asp:Label ID="lblTrdId" runat="server" Text='<%# Bind("TRD_ID") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Left"></ItemStyle></asp:TemplateField>

                            <asp:TemplateField HeaderText="Select">
                                <EditItemTemplate><asp:CheckBox ID="chkSelect" runat="server"  /></EditItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle Wrap="False" /><ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" /></ItemTemplate>
                                <HeaderTemplate>
                                    <table>
                                        <tr><td align="center" class="matters" >Select </td></tr>
                                        <tr><td align="center"><asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);" ToolTip="Click here to select/deselect all rows" /></td></tr>
                                    </table></HeaderTemplate></asp:TemplateField>

                           <asp:TemplateField HeaderText="Trip" ShowHeader="False">
                                <HeaderTemplate>
                                <table style="width: 100%; height: 100%">
                                    <tr><td align="center" style="width: 100px; height: 14px;"><asp:Label ID="lblParent" runat="server" CssClass="gridheader_text" Text="Trip" Width="214px"></asp:Label></td></tr>
                                    <tr><td style="width: 100px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr><td style="width: 100px; height: 12px"><div id="Div2" class="chromestyle"><ul><li><a href="#" rel="dropmenu1">
                                                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../../Images/operations/like.gif" />
                                                <span style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a></li></ul></div></td>
                                            <td style="width: 100px; height: 12px">
                                                <asp:TextBox ID="txtTrip" runat="server" Width="150px"></asp:TextBox></td>
                                            <td style="width: 100px; height: 12px" valign="middle">
                                                <asp:ImageButton ID="btnTrip" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnTrip_Click" /></td></tr></table></td></tr></table> </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblParent" runat="server"  Text='<%# BIND("TRP_DESCR") %>'    Width="214px"></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Left" /></asp:TemplateField>

                            <asp:TemplateField HeaderText="Journey" >
                                <HeaderTemplate>
                                    <table><tbody>
                                        <tr><td align="center" Class="gridheader_text">Journey</td></tr>
                                        <tr><td align="center">
                                            <asp:DropDownList id="ddlgvJourney" runat="server" CssClass="listbox" Width="70px" AutoPostBack="True" OnSelectedIndexChanged="ddlgvJourney_SelectedIndexChanged">
                                            <asp:ListItem>ALL</asp:ListItem>
                                            <asp:ListItem>Onward</asp:ListItem>
                                            <asp:ListItem>Return</asp:ListItem>
                                            </asp:DropDownList></td></tr></tbody></table></HeaderTemplate>
                                <ItemTemplate><asp:Label ID="lblJourney" runat="server" Text='<%# BIND("TRP_JOURNEY") %>'></asp:Label></ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  /><HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" /></asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Height="25px" /><AlternatingRowStyle CssClass="griditem_alternative" /><RowStyle CssClass="griditem" Height="25px" /></asp:GridView></td></tr></table>
            </td></tr>
            <tr>
              <td align="center" style="width: 511px; height: 12px">
                  <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td></tr></table>
        </td></tr>
        <tr>
        <td align="center" class="matters" colspan="4" style="width: 567px; height: 28px"valign="middle">
            <input id="h_SelectedId" runat="server" type="hidden" value="0" />
            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td></tr>
        </table>
        </ContentTemplate><Triggers>
                    <asp:PostBackTrigger ControlID="ddlSubLocation" /><asp:PostBackTrigger ControlID="ddlPickup" />
                    <asp:PostBackTrigger ControlID="ddlBusNo" /><asp:PostBackTrigger ControlID="ddlShift" /></Triggers></asp:UpdatePanel>
        <div id="map_canvas" style="margin: 0px auto; width: 600px; height: 400px"></div>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 90%; color: #0000ff">
            <tr>
                <td style="WIDTH: 120px"></td><td>
        <table align="left" border="0" bordercolor="#1b80b6" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" class="matters" >Latitude</td>
                <td class="matters">:&nbsp&nbsp&nbsp</td>
                <td class="matters" >                
                    <asp:TextBox ID="txtLat" Enabled="false" runat="server" CssClass="error"></asp:TextBox></td></tr>
            <tr>
                <td align="left" class="matters">Longitude</td>
                <td class="matters">:&nbsp&nbsp&nbsp</td>
                <td class="matters">
                    <asp:TextBox ID="txtLon" Enabled="false" runat="server" CssClass="error"></asp:TextBox></td></tr>
            <tr>
                <td align="left" class="matters">New Pickup Point Name</td>
                <td class="matters">:&nbsp&nbsp&nbsp</td>
                <td class="matters">
                    <asp:TextBox ID="txtPoint" runat="server" Width="250px" CssClass="error"></asp:TextBox></td></tr>

            <tr>
                <td align="left" class="matters">Update existing Pickup Point</td>
                <td class="matters">:&nbsp&nbsp&nbsp</td>
                <td class="matters">
                    <asp:DropDownList ID="ddlPoint" runat="server" Width="211px"></asp:DropDownList></td></tr>

            <tr>
              <td colspan="2"></td>
              <td>&nbsp&nbsp&nbsp&nbsp
                  <asp:Button ID="btnUpdate" OnClientClick="doUpdate();return false;" runat="server" CssClass="button" Text="Save PickupPoint" /></td></tr>

        </table></td></tr></table>
    </div>
    
    <script type="text/javascript">

cssdropdown.startchrome("Div2")

</script>

        <asp:HiddenField ID="hfPNT_ID" runat="server" />
        <asp:HiddenField ID="hBSU_ID" runat="server" />
        <asp:HiddenField ID="hCLM_ID" runat="server" />
        &nbsp;&nbsp;
    </form>
</body>
</html>
