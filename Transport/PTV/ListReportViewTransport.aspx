<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ListReportViewTransport.aspx.vb" Inherits="Common_ListReportViewTransport" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        var myVar;
        function AddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 900px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = 'gpsShowLocation.aspx?';
            window.showModalDialog(url, "", sFeatures);
            return true;
        }

        function process() {
            var chk_state = document.getElementById("chkAL").checked;
            if (myVar) alert('Process in progress');
            else
                if (chk_state) {
                    myVar = setInterval("CallMe();", 10000);
                }
        }
        function CallMe() {
            var bsuid = $get("<%=h_bsuid.ClientID %>").value;
            PageMethods.getTable(bsuid, CallSuccess, CallFailed);
        }
        function CallSuccess(res) {
            //var resary = res.split(";");
            if (res != null)
                if (res != "" || res != null) {
                    //alert(res);
                    document.getElementById("id1").innerHTML = res;
                    if (res == 'Process Complete') clearInterval(myVar);
                }
                else alert('error');
        }
        function CallFailed(res) {
            //alert(res.get_message());
            //alert("fail");
        }
        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
          <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
         <div class="card-body">
            <div class="table-responsive">
    <table align="center" width="100%">
        <tr id="rowUnit" runat="server" visible="false" valign="top" class="input-group">
            <td valign="bottom" width="10%" align="left">
                <asp:Label ID="Label1" runat="server" CssClass="error"></asp:Label>
            </td>
            <td align="left" width="90%">
                <table id="Table1" align="center" width="100%">
                    <tr>
                         <td align="left" width="40%"><span class="field-label"> Businessunit :
                            <asp:Label ID="lblUnit" runat="server" Text=""></asp:Label>
                            <asp:TextBox ID="txtUnit" runat="server" Width="600px" Text="GEMS UNITED INDIAN SCHOOL LLC - ABU DHABI"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2"  class="title-bg">
                <table align="center" width="100%">
                    <tr valign="top">
                        <th align="right" valign="middle" width="100%">
                           <asp:LinkButton ID="btnPrint" runat="server" Visible="False">Print</asp:LinkButton>
                &nbsp;
                <asp:HyperLink ID="btnExport" runat="server">Export</asp:HyperLink>
                        </th>
                    </tr>
                </table>

            </td>
        </tr>
        <tr valign="top">
            <td valign="bottom" width="30%" align="left">
                <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
             
            </td>
            <td align="right" width="100%">
                <table  width="100%">
                    <tr>
                        <td width="0" height="0px">
                            <asp:CheckBox ID="chkSelection1" runat="server" AutoPostBack="True" Visible="False" />
                        </td>
                        <td width="0" height="0px">
                            <asp:CheckBox ID="chkSelection2" runat="server" AutoPostBack="True" Height="16px" />
                        </td>
                        <td height="0px">&nbsp;
                        </td>
                        <td width="0" height="0px">
                            <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                        </td>
                        <td width="0">
                            <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                        </td>
                        <td width="0">
                            <asp:RadioButton ID="rad4" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                        </td>
                        <td width="0">
                            <asp:RadioButton ID="rad5" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                        </td>
                        <td width="0">
                            <asp:RadioButton ID="rad6" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grdPTV" runat="server" AutoGenerateColumns="True" PageSize="5" Width="50%" ShowFooter="true"
                    CaptionAlign="Top" SkinID="GridViewView" CssClass="table table-bordered table-row" DataKeyNames="ID" AllowPaging="True" >
                </asp:GridView>
            </td>
            <td>
                <asp:Button ID="ListButton1" runat="server" CssClass="button" CausesValidation="False" OnClientClick="return process();" Width="142px"
                    OnClick="ListButton1_Click" Visible="False"></asp:Button>
                <asp:Button ID="ListButton2" runat="server" CssClass="button" CausesValidation="False" Width="142px"
                    OnClick="ListButton2_Click" Visible="False"></asp:Button>
                <div id="id1"></div>

            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <table style="margin-left: 0px; height: 1px;" width="0px">
                    <tr>
                        <td width="0" height="0px">
                            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" Visible="False"  CssClass="field-label"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a id='top'></a>
    <table align="center" class="table table-bordered table-row" width="100%">
        <tr>
            <td align="center" valign="top" width="100%" colspan="2">
                <asp:GridView ID="gvDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%"
                    SkinID="GridViewView" AllowPaging="True" PageSize="20">
                    <Columns>
                        <asp:TemplateField HeaderText=" ">
                            <HeaderTemplate>
                                <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                    value="Check All" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <input id="chkControl" runat="server" type="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol1" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col1"></asp:Label>
                                <br />

                                <asp:TextBox ID="txtCol1" runat="server" Width="75%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol2" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col2"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol2" runat="server" Width="75%"></asp:TextBox>
                                
                                                        <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol3" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col3"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol3" runat="server" Width="80%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol4" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col4"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol4" runat="server" Width="80%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />&nbsp;
                                                 
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol5" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col5"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol5" runat="server" Width="100%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />&nbsp;
                                                   
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol6" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col6"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol6" runat="server" Width="100%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />&nbsp;
                                                    
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col7" SortExpression="Col7">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol7" runat="server" Text='<%# Bind("Col7") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol7" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col7"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol7" runat="server" Width="100%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch7" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />&nbsp;
                                                    
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col8" SortExpression="Col8">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol8" runat="server" Text='<%# Bind("Col8") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol8" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col8"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol8" runat="server" Width="100%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch8" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />&nbsp;
                                                    
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col9" SortExpression="Col9">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol9" runat="server" Text='<%# Bind("Col9") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol9" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col9"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol9" runat="server" Width="100%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch9" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />&nbsp;
                                                   
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Col10" SortExpression="Col10">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCol10" runat="server" Text='<%# Bind("Col10") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderTemplate>

                                <asp:Label ID="lblHeaderCol10" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                    Text="Col10"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtCol10" runat="server" Width="100%"></asp:TextBox>

                                <asp:ImageButton ID="btnSearch10" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearch_Click" />&nbsp;
                                                   
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" ShowHeader="False">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <table style="width: 100%;">
                    <tr>
                        <td align="left" width="20%">
                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                Text="Select All" Visible="False" />
                        </td>
                        <td align="center" width="60%"></td>
                        <td width="20%">&nbsp;
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
    </table>
</div>
   </div>
        </div>
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_8" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_9" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_10" runat="server" type="hidden" value="=" />
    <input id="h_SelectedId" runat="server" type="hidden" value="0" />
    <input id="h_bsuid" runat="server" type="hidden" value="" />
</asp:Content>
