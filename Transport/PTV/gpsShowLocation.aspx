﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsShowLocation.aspx.vb" Inherits="Transport_GPS_Tracking_Map_gpsShowLocation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
     <!-- Bootstrap core JavaScript-->
    <script src="/phoenixbeta/vendor/jquery/jquery.min.js"></script>
    <script src="/phoenixbeta/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/phoenixbeta/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>




    <!-- Bootstrap core CSS-->
    <link href="/phoenixbeta/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/phoenixbeta/cssfiles/BSUstyles.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/phoenixbeta/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/phoenixbeta/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/phoenixbeta/cssfiles/sb-admin.css" rel="stylesheet">
    <link href="/phoenixbeta/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/phoenixbeta/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/phoenixbeta/cssfiles/Accordian.css" rel="stylesheet" />

    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="/cssfiles/all-ie-only.css">
    <!--[endif]-->

    <!-- Bootstrap header files ends here -->

<style type="text/css"> 
#map_canvas{
    overflow: hidden;
    position: absolute;
    top: 110px;
    right: 0;
    bottom: 0;
    left: 0;
}

      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map_canvas {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map_canvas #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
            margin-top: 10px;
    height: 40px !important;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }

      table td select, div select, table td input[type=text], table td input[type=date], table td input[type=password], table td textarea {
          padding: 2px !important;
          border:1px solid;
      }
      table td input.button, div input.button, table td div input.button {
          padding:4px !important;
      }
      .error {
          padding:2px !important;
      }
      .padding-0 {
          padding:0px !important;
      }
    </style>

<script type="text/javascript">

    function showMap() {
        var myString = document.getElementById("HiddenJSON").value;
        var locations = JSON.parse("[" + myString + "]");
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 13,
            center: new google.maps.LatLng(locations[0][2], locations[0][3]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        google.maps.event.addListener(map, 'click', function (event) {
            mapZoom = map.getZoom();
            startLocation = event.latLng;
            document.getElementById("txtLat").value = startLocation.lat();
            document.getElementById("txtLon").value = startLocation.lng();
            document.getElementById("Hiddenlat").value = startLocation.lat();
            document.getElementById("Hiddenlng").value = startLocation.lng();
            //setTimeout(placeMarker, 600);
        });

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][2], locations[i][3]),
                label: locations[i][0],
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][1]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }

    function initMap() {
        var myLatLng = { lat: -25.363, lng: 131.044 };

        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 4,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
        });
    }


      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }


</script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyC3wIDw5B28tMJe3o5DyeT1_DQU_XawOEA&libraries=places" async="" defer="defer" type="text/javascript"></script>

</head>
<body onload='showMap();'>
    <form id="form1" runat="server">
    <div><input id="pac-input" class="controls" type="text" placeholder="Search Box" />

<div id="map_canvas" style="width: 100%; height: 100%;"></div>
       </div>


         <%--<div class="card mb-3">
             <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            
        </div>--%>
 <div class="card-body padding-0">
            <div class="table-responsive m-auto">
    <asp:HiddenField ID="Hiddenlat" runat="server" />
    <asp:HiddenField ID="Hiddenlng" runat="server" />
    <asp:HiddenField ID="HiddenJSON" runat="server" />

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-2 col-md-4 col-6">
                            <strong>Sub Locations</strong>
                        <br />
                            <asp:DropDownList ID="ddlSubLocation" AutoPostBack="true" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <strong>Latitude</strong>
                        <br />
                            <asp:TextBox ID="txtLat"  runat="server" CssClass="error"></asp:TextBox>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <strong>Longitude</strong>
                        <br />
                             <asp:TextBox ID="txtLon"  runat="server" CssClass="error"></asp:TextBox>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <strong>New Point</strong>
                        <br />
                            <asp:TextBox ID="txtPoint" runat="server" CssClass="error"></asp:TextBox>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6">
                            <strong>Existing Point</strong>
                        <br />
                            <asp:DropDownList ID="ddlPoint" runat="server"></asp:DropDownList>
                        </div>
                    
                        <div class="col-lg-2 text-center m-auto">
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                        </div>
                    </div>

                </div>

      
                </div>
                  </div>

    </form>
</body>
</html>
