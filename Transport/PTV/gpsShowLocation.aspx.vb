﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_Map_gpsShowLocation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
            If Session("sUsr_name") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("ViewId").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "T000030") 
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    'ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    firstTime()
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        End If
    End Sub

    Private Sub firstTime()
        fillDropdown(ddlSubLocation, "ddlSubLocation", "", "SBL_DESCRIPTION", "PNT_SBL_ID", False)
        ddlSubLocation.SelectedValue = ViewState("ViewId")
        fillPoints()
    End Sub

    Private Sub showMap(sblId As Int16)
        Hiddenlat.Value = Request.QueryString("lat")
        Hiddenlng.Value = Request.QueryString("lng")

        Dim sql As String = ";With bno As (Select distinct BNO_DESCR, TPP_PNT_ID "
        sql &= "From oasis.oasis_transport.TRANSPORT.TRIPS_M As A INNER Join oasis.oasis_transport.TRANSPORT.TRIPS_D As B inner Join oasis.oasis_transport.TRANSPORT.BUSNOS_M On BNO_ID=TRD_BNO_ID inner Join oasis.oasis_transport.TRANSPORT.TRIPS_PICKUP_S On TRD_ID=TPP_TRD_ID "
        sql &= "On A.TRP_ID=B.TRD_TRP_ID And TRD_TODATE Is NULL "
        sql &= "WHERE TRP_JOURNEY <>'EXTRA' AND TRD_BSU_ID='" & Session("SBSUID") & "'), pnt as ( "
        sql &= "Select sbl_description, pnt_id, pnt_description, cast(lat As varchar) lat, cast(lon As varchar) lon "
        sql &= "From oasis.oasis_transport.TRANSPORT.SUBLOCATION_M AS A INNER Join oasis.oasis_transport.TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID "
        sql &= "inner Join [172.25.29.52].sts_database.dbo.pickuppoint On pnt_id=pntid Where pnt_bsu_id ='" & Session("SBSUID") & "' and (sbl_id=" & sblId & " or " & sblId & "=0)), cte as ( "
        sql &= "Select * from bno inner Join pnt On tpp_pnt_id=pnt_id  union select 0, 0, 'School', 0, bsu_name, lat, lon from businessunit where bsu_id='" & Session("SBSUID") & "') "
        sql &= "Select isnull((STUFF((Select ', ["" '+cast(count(*) as varchar)+'"",'+'""'+pnt_description+'"",'+cast(lat as varchar)+','+cast(lon as varchar)+']' "
        sql &= "From cte Group By pnt_description, lat, lon FOR XML PATH('')),1,2,'')),'') "
        Try
            'HiddenJSON.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql)
            HiddenJSON.Value = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "PTV_REPORT '" & Session("SBSUID") & "', " & sblId & ", 'Get Map'")

        Catch ex As Exception
            Dim errMsg = ex.Message
        End Try

    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal sqlFilter As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        ReDim SPParam(2)
        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        SPParam(1) = Mainclass.CreateSqlParameter("@ID", sqlFilter, SqlDbType.VarChar)
        SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", sqlQuery, SqlDbType.VarChar)
        Dim myDataSet As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PTV_REPORT", CType(SPParam, SqlParameter()))
        drpObj.DataSource = myDataSet.Tables(0)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ddlSubLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        fillPoints()
    End Sub

    Private Sub fillPoints()
        fillDropdown(ddlPoint, "ddlPoint", ddlSubLocation.SelectedValue, "PNT_DESCRIPTION", "PNT_ID", False)
        showMap(ddlSubLocation.SelectedValue)
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim sql As String = ""
        If ddlPoint.SelectedValue = 0 And txtPoint.Text.Trim.Length < 3 Then sql = "Enter new point details"
        If ddlPoint.SelectedValue > 0 And txtPoint.Text.Trim.Length > 0 And txtPoint.Text.Trim.Length < 3 Then sql = "Enter new point details"
        If ddlPoint.SelectedValue > 0 And txtPoint.Text.Trim.Length = 0 And Hiddenlat.Value.Length = 0 Then sql = "Nothing to do"
        If txtLat.Text.Length > 0 And Hiddenlat.Value.Length = 0 Then Hiddenlat.Value = txtLat.Text
        If txtLon.Text.Length > 0 And Hiddenlng.Value.Length = 0 Then Hiddenlng.Value = txtLon.Text
        If sql.Length > 0 Then
            Page.ClientScript.RegisterStartupScript(Me.[GetType](), "CallMyMenu", "alert('" & sql & "')", True)
            txtLat.Text = Hiddenlat.Value
            txtLon.Text = Hiddenlng.Value
            Exit Sub
        End If
        'old point and geocode provided
        If ddlPoint.SelectedValue > 0 And Hiddenlat.Value.Length > 0 Then
            If ddlPoint.SelectedItem.Text.Substring(ddlPoint.SelectedItem.Text.Length - 1) = "*" Then
                sql = "insert into [172.25.29.52].sts_database.dbo.pickuppoint (bsuid, pntid, lat, lon, dtime) values ('" & Session("SBSUID") & "'," & ddlPoint.SelectedValue & "," & Hiddenlat.Value & "," & Hiddenlng.Value & ", getdate())"
            Else
                sql = "update [172.25.29.52].sts_database.dbo.pickuppoint set lat='" & Hiddenlat.Value & "', lon='" & Hiddenlng.Value & "', dtime=getdate() where bsuid='" & Session("SBSUID") & "' and pntid='" & ddlPoint.SelectedValue & "' "
            End If
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql)
            If ddlPoint.SelectedItem.Text <> txtPoint.Text And txtPoint.Text.Trim.Length > 1 Then 'need a change of name
                sql = "update oasis_TRANSPORT.TRANSPORT.PICKUPPOINTS_M set PNT_DESCRIPTION='" & ddlPoint.SelectedItem.Text & "' where pnt_id=" & ddlPoint.SelectedItem.Value
                Dim pntid As Int64 = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sql)
            End If
            fillPoints()
        Else
            'new point
            If txtPoint.Text.Trim.Length > 2 Then
                sql = "select count(*) from oasis.oasis_TRANSPORT.TRANSPORT.PICKUPPOINTS_M inner join oasis.oasis_TRANSPORT.TRANSPORT.SUBLOCATION_M on SBL_ID=PNT_SBL_ID "
                sql &= "inner join oasis.oasis_TRANSPORT.TRANSPORT.LOCATION_M on LOC_ID=PNT_LOC_ID where pnt_bsu_id='" + Session("sbsuid").ToString + "' and SBL_ID=" & ddlSubLocation.SelectedValue
                sql &= " and dbo.fn_StripCharacters(pnt_description, '^a-z0-9')=dbo.fn_StripCharacters('" & txtPoint.Text & "', '^a-z0-9')"
                If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql) > 0 Then
                    Page.ClientScript.RegisterStartupScript(Me.[GetType](), "CallMyMenu", "alert('This pick up point already exists')", True)
                    Exit Sub
                End If
                sql = "select top 1 SBL_LOC_ID from oasis.oasis_TRANSPORT.TRANSPORT.PICKUPPOINTS_M inner join oasis.oasis_TRANSPORT.TRANSPORT.SUBLOCATION_M on SBL_ID=PNT_SBL_ID "
                sql &= "inner join oasis.oasis_TRANSPORT.TRANSPORT.LOCATION_M on LOC_ID=PNT_LOC_ID where pnt_bsu_id='" + Session("sbsuid").ToString + "' and SBL_ID=" & ddlSubLocation.SelectedValue
                Dim locid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql)
                sql = "exec [TRANSPORT].[SavePICKUPPOINTS] 0," & ddlSubLocation.SelectedValue & ",'" + Session("sbsuid").ToString + "'," + Session("clm") + ",'" + txtPoint.Text + "','Add'," + locid
                Dim pntid As Int64
                pntid = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sql)
                If Hiddenlat.Value.Length > 0 And pntid > 0 Then
                    sql = "insert into [172.25.29.52].sts_database.dbo.pickuppoint (bsuid, pntid, lat, lon, dtime) values ('" & Session("SBSUID") & "'," & pntid & "," & Hiddenlat.Value & "," & Hiddenlng.Value & ", getdate())"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql)
                    fillPoints()
                End If
            End If
        End If
        showMap(ddlSubLocation.SelectedValue)
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim sql As String = "select count(tpp_id) from oasis.oasis_transport.transport.trips_pickup_s where tpp_pnt_id=" & ddlPoint.SelectedValue
        If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql) > 0 Then
            Page.ClientScript.RegisterStartupScript(Me.[GetType](), "CallMyMenu", "alert('Please remove this pick up point from allocated trips before deleting')", True)
            Exit Sub
        End If
        sql = "update oasis_TRANSPORT.TRANSPORT.PICKUPPOINTS_M set PNT_LOC_ID=0, PNT_OLD=PNT_LOC_ID where pnt_id=" & ddlPoint.SelectedValue
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sql)
    End Sub
End Class
