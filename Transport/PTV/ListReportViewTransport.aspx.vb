Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Partial Class Common_ListReportViewTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Sub BuildListView()
        Try
            Dim strConn, StrSQL, StrSortCol As String
            strConn = "" : StrSQL = "" : StrSortCol = ""
            strConn = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            Select Case MainMenuCode
                Case "T200134"
                    ConnectionString = ConnectionManger.GetOASISTRANSPORTConnectionString
                    CheckBox1Text = "Area"
                    CheckBox2Text = "Home to School"
                    CheckBox3Text = "School to Home"
                    If rad1.Checked Then
                        HeaderTitle = "Area Details"
                        newTable = PageCaption <> "Area Details"
                    ElseIf rad2.Checked Then
                        HeaderTitle = "Home to School"
                        newTable = PageCaption <> "Home to School"
                    Else
                        HeaderTitle = "School to Home"
                        newTable = PageCaption <> "School to Home"
                    End If
                    NoAddingAllowed = True
                    IsStoredProcedure = True
                    StrSQL = "PTV_REPORT"
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", HeaderTitle, SqlDbType.VarChar)
                    EditPagePath = "../PTV/gpsShowLocation.aspx"
                    PageCaption = HeaderTitle

                Case "T200133"
                    ConnectionString = strConn
                    HeaderTitle = "Create a new MasterPlan"
                    If Datamode = "add" Then
                        IsAddNew = True
                        NoAddingAllowed = True
                        IsStoredProcedure = True
                        StrSQL = "PTV_REPORT"
                        ReDim SPParam(2)
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.VarChar)
                        SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "UnitList", SqlDbType.VarChar)
                    ElseIf Datamode = "none" Then
                        CheckBox1Text = "Export to PTV"
                        CheckBox2Text = "Import from PTV"
                        IsAddNew = False
                        If rad1.Checked Then
                            newTable = PageCaption <> "View existing MasterPlans"
                            Session("prf_id") = 0
                            HeaderTitle = "View existing MasterPlans"
                            IsStoredProcedure = True
                            StrSQL = "PTV_REPORT"
                            ReDim SPParam(2)
                            SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                            SPParam(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.VarChar)
                            SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "MasterPlans", SqlDbType.VarChar)
                        Else
                            Session("prf_id") = -1
                            newTable = PageCaption <> "View existing Released Plans"
                            HeaderTitle = "View existing Released Plans"
                            IsStoredProcedure = True
                            StrSQL = "PTV_REPORT"
                            ReDim SPParam(2)
                            SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                            SPParam(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.VarChar)
                            SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "ReleasedPlans", SqlDbType.VarChar)
                        End If
                    Else
                        Dim viewId As String = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        If IsAddNew Or viewId.Length = 6 Then
                            'CheckBox1Text = "New Students"
                            ButtonText1 = "Process Data"
                            HeaderTitle = "Master Plan " & SqlHelper.ExecuteScalar(strConn, CommandType.Text, "Select bsu_shortname from businessunit where bsu_id='" & viewId & "'")
                            NoViewAllowed = True
                            NoAddingAllowed = True
                            ShowGridCheckBox = True
                            h_bsuid.Value = viewId
                            grdPTV.DataSource = SqlHelper.ExecuteDataset(strConn, CommandType.Text, "select Id, Unit, Trip, FromTime, ToTime, fgr FromGrade, tgr ToGrade, dt [Date] from ptv_tripdetails inner join businessunit on bsu_shortname=unit where bsu_id='" & viewId & "'")
                            grdPTV.DataBind()
                            StrSQL = "Declare @today date=getdate(), @bsuid varchar(10)='" & viewId & "', @bsuname varchar(10)"
                            StrSQL &= "Select @bsuname=bsu_shortname from businessunit where bsu_id=@bsuid "
                            StrSQL &= ";with cte as (select OROR_EXTID2 ptvid from [172.25.29.56].ROST_2018_2_STS_Productive.[dbo].[V_RPT_Order] where LBTF_NAME Like @bsuname+'%') "
                            StrSQL &= "select ID, [Student No], [Student Name], [HS BusNo], [SH BusNo], City, Area, PickupPoint, Grade, Operation from (" ' Gender, Sibling
                            StrSQL &= "Select distinct stu_id ID, stu_no [Student No], isnull(stu_firstname,'')+isnull(stu_midname,'')+' '+isnull(stu_lastname,'') [Student Name], stu_currstatus, "
                            StrSQL &= "N.BNO_DESCR [HS BusNo], isnull(B.LOC_DESCRIPTION,'') City,isnull(D.SBL_DESCRIPTION,'') Area,isnull(C.PNT_ID,0) stu_hsid, C.PNT_DESCRIPTION PickupPoint, "
                            StrSQL &= "N.BNO_DESCR [SH BusNo], isnull(E.LOC_DESCRIPTION,'') stu_shcity,isnull(F.SBL_DESCRIPTION,'') stu_sharea,isnull(G.PNT_ID,0) stu_shid, G.PNT_DESCRIPTION stu_shpoint,isnull(A.STU_ACD_ID,0) stu_acdid, "
                            StrSQL &= "STU_GENDER Gender,GRM_GRD_ID Grade,case when STU_SIBLING_ID<>stu_id then STU_SIBLING_ID else '' end Sibling, case when ptvid is null then 'NEW' else 'UPDATE' end Operation "
                            'StrSQL &= "--isnull(L.TRD_ID,0) stu_hsbus,isnull(M.TRD_ID,0) stu_hsbusid,isnull(D.SBL_ID,0) stu_hsbusid,isnull(F.SBL_ID,0) stu_shbusid "
                            StrSQL &= "From  oasis.oasis_transport.dbo.STUDENT_M As A left outer join oasis.oasis.dbo.Student_Services_D S With (NOLOCK) On stu_id=ssv_stu_id "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.PICKUPPOINTS_M As C With (NOLOCK) On C.PNT_ID = A.STU_PICKUP "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.SUBLOCATION_M As D With (NOLOCK) On A.STU_SBL_ID_PICKUP = D.SBL_ID "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.LOCATION_M As B With (NOLOCK) On D.SBL_LOC_ID = B.LOC_ID "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.PICKUPPOINTS_M As G With (NOLOCK) On A.STU_DROPOFF = G.PNT_ID "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.SUBLOCATION_M As F  With (NOLOCK) On F.SBL_ID = A.STU_SBL_ID_DROPOFF "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.LOCATION_M As E With (NOLOCK) On E.LOC_ID = F.SBL_LOC_ID "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.TRIPS_D As L With (NOLOCK) On A.STU_PICKUP_TRP_ID=L.TRD_TRP_ID And L.TRD_TODATE Is NULL "
                            StrSQL &= "Left OUTER JOIN oasis.oasis_transport.TRANSPORT.TRIPS_D As M With (NOLOCK) On A.STU_DROPOFF_TRP_ID=M.TRD_TRP_ID And M.TRD_TODATE Is NULL "
                            StrSQL &= "Left OUTER join oasis.oasis_transport.TRANSPORT.BUSNOS_M N With (NOLOCK) On L.TRD_BSU_ID=N.BNO_BSU_ID And N.BNO_ID=L.TRD_BNO_ID "
                            StrSQL &= "Left OUTER join oasis.oasis_transport.TRANSPORT.BUSNOS_M O With (NOLOCK) On M.TRD_BSU_ID=O.BNO_BSU_ID And O.BNO_ID=M.TRD_BNO_ID "
                            StrSQL &= "INNER JOIN oasis.OASIS.dbo.GRADE_BSU_M As P With(NOLOCK) On STU_GRM_ID=P.GRM_ID  INNER JOIN oasis.OASIS.dbo.GRADE_M As R With(NOLOCK) On P.GRM_GRD_ID=R.GRD_ID  "
                            StrSQL &= "Left OUTER JOIN CTE on ptvid=cast(stu_id as varchar) "
                            StrSQL &= "WHERE SSV_SVC_ID = 1 And isnull(SSV_PICKUP_TRP_ID, SSV_DROPOFF_TRP_ID) Is Not null "
                            StrSQL &= "And @today BETWEEN SSV_FROMDATE And isNULL(SSV_TODATE-1,'01-JAN-2020') and SSV_BSU_ID=@bsuid "
                            StrSQL &= "And isnull(stu_leavedate,'01-JAN-2020')>@today "
                            StrSQL &= "And SSV_ACD_ID IN (SELECT ACD_ID From oasis.oasis.dbo.AcademicYear_D WITH (NOLOCK) WHERE acd_bsu_id=@bsuid And (@today between ACD_STARTDT And ACD_ENDDT Or acd_current=1))"
                            StrSQL &= " ) A WHERE 1=1 "
                            IsStoredProcedure = False
                        Else
                            If Session("prf_id") = 0 Then
                                HeaderTitle = "View existing MasterPlans"
                                CheckBox1Text = "Students"
                                CheckBox2Text = "Vehicles"
                                CheckBox3Text = "PickupPoints"
                                CheckBox4Text = "Audit Log"
                                StrSQL = "Declare @id int=" & viewId
                                HeaderTitle = SqlHelper.ExecuteScalar(strConn, CommandType.Text, "select GTL_PLANNAME from ptv_transfer_log where GTL_ID=" & viewId)
                                If rad1.Checked Then
                                    newTable = PageCaption <> "Student Details"
                                    HeaderTitle &= " Student Details"
                                    IsStoredProcedure = True
                                    StrSQL = "PTV_REPORT"
                                    ReDim SPParam(2)
                                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", viewId, SqlDbType.VarChar)
                                    SPParam(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.VarChar)
                                    SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "Student", SqlDbType.VarChar)
                                ElseIf rad2.Checked Then
                                    newTable = PageCaption <> "Vehicle Details"
                                    HeaderTitle &= " Vehicle Details"
                                    IsStoredProcedure = True
                                    StrSQL = "PTV_REPORT"
                                    ReDim SPParam(2)
                                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", viewId, SqlDbType.VarChar)
                                    SPParam(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.VarChar)
                                    SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "Vehicle", SqlDbType.VarChar)
                                ElseIf rad3.Checked Then
                                    newTable = PageCaption <> "Pickup point Details"
                                    HeaderTitle &= " Pickup point Details"
                                    IsStoredProcedure = True
                                    StrSQL = "PTV_REPORT"
                                    ReDim SPParam(2)
                                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", viewId, SqlDbType.VarChar)
                                    SPParam(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.VarChar)
                                    SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "Pickup", SqlDbType.VarChar)
                                ElseIf rad4.Checked Then
                                    newTable = PageCaption <> "Audit Log"
                                    HeaderTitle &= " Audit Log"
                                    IsStoredProcedure = True
                                    StrSQL = "PTV_REPORT"
                                    ReDim SPParam(2)
                                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", "", SqlDbType.VarChar)
                                    SPParam(1) = Mainclass.CreateSqlParameter("@ID", viewId, SqlDbType.VarChar)
                                    SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "Audit", SqlDbType.VarChar)
                                End If
                                NoViewAllowed = True
                            Else
                                HeaderTitle = "View existing Released Plans"
                                If rad1.Checked Then
                                    newTable = PageCaption <> "Import from PTV Details"
                                    HeaderTitle &= " Import from PTV Details"
                                    IsStoredProcedure = True
                                    StrSQL = "PTV_REPORT"
                                    ReDim SPParam(2)
                                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", "", SqlDbType.VarChar)
                                    SPParam(1) = Mainclass.CreateSqlParameter("@ID", viewId, SqlDbType.VarChar)
                                    SPParam(2) = Mainclass.CreateSqlParameter("@REPORT", "Released", SqlDbType.VarChar)
                                End If
                            End If
                            NoViewAllowed = True
                        End If
                    End If
                    StrSortCol = "ID Desc"
                    EditPagePath = "../PTV/listreportviewTransport.aspx"
                    PageCaption = HeaderTitle
            End Select
            SQLSelect = StrSQL
            SortColString = StrSortCol
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    ' Dim SPParam(10) As SqlParameter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Dim smScriptManager As New ScriptManager
                smScriptManager = Master.FindControl("ScriptManager1")
                smScriptManager.EnablePageMethods = True
                'Session("sBsuid") = "125017"
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/Like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/Like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/Like.gif"

                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    lblError.Text = msgText
                End If
                ShowGridCheckBox = False
                NoAddingAllowed = False
                NoViewAllowed = False
                HideExportButton = False
                IsStoredProcedure = False
                ButtonText1 = ""
                ButtonText2 = ""
                rad1.Checked = True
                CheckBox1Text = ""
                CheckBox2Text = ""
                CheckBox3Text = ""
                CheckBox4Text = ""
                CheckBox5Text = ""
                CheckBox6Text = ""
                FilterText1 = ""
                FilterText2 = ""
                BuildListView()
                gridbind(False)
                btnExport.Visible = Not HideExportButton
                If NoAddingAllowed Then
                    hlAddNew.Visible = False
                Else
                    hlAddNew.Visible = True
                    Dim url As String
                    url = EditPagePath & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    hlAddNew.NavigateUrl = url
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] As varchar),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(ByVal NoReset As Boolean)
        Try
            Dim GridData As New DataTable
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            FilterTextboxString = ""
            If gvDetails.Rows.Count > 0 And Not newTable Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            Else
                newTable = False
            End If
            Dim OrderByStr As String = ""
            If SortColString <> "" Then
                OrderByStr = "order by " & SortColString
            Else
                OrderByStr = "order by  1"
            End If
            If Not IsStoredProcedure Then
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQLSelect & str_Filter & OrderByStr).Tables(0)
            Else
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SQLSelect, CType(SPParam, SqlParameter())).Tables(0)
                str_Filter = " 1 = 1 " & str_Filter
                GridData.Select(str_Filter)
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridData.Copy
                mySelectDT.Rows.Clear()
                For Each mRow In GridData.Select(str_Filter)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If
            HeaderTitle = PageCaption & " (Count:" & GridData.Rows.Count & ")"
            Dim bindDT, myData As New DataTable
            bindDT = GridData.Copy
            myData = GridData.Copy
            If myData.Columns.Count > 0 Then
                myData.Columns.RemoveAt(0)
            End If
            ViewState("GridTable") = GridData
            Session("myData") = myData
            SetExportFileLink()
            GridData.Rows.Clear()
            Dim HeaderLabel As New Label
            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex
            If bindDT.Columns.Count < 10 Then
                For i = 0 To bindDT.Columns.Count
                    gvDetails.Columns(i).Visible = True
                Next
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If
            gvDetails.Columns(0).Visible = ShowGridCheckBox
            gvDetails.Columns(11).Visible = Not NoAddingAllowed
            gvDetails.Columns(1).Visible = False
            gvDetails.Columns(11).Visible = Not NoViewAllowed
            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If

            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            SetHeaderFilterString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label
            lblItemColID = TryCast(e.Row.FindControl("lblItemCol1"), Label)
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            Try
                Dim FloatColIndexArr(), i, ControlName As String
                FloatColIndexArr = FloatColIndex.Split("|")
                Dim lblItemCol As Label
                For Each i In FloatColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsNumeric(lblItemCol.Text) Then
                                lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("####0.00")
                            End If
                        End If
                    End If
                Next
                Dim DateColIndexArr() As String
                DateColIndexArr = DateColIndex.Split("|")
                For Each i In DateColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsDate(lblItemCol.Text) Then
                                lblItemCol.Text = CDate(lblItemCol.Text).ToString("dd/MMM/yyyy")
                            End If
                        End If
                    End If
                Next

            Catch ex As Exception

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind(True)
    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=RouteManagement" '& Encr_decrData.Encrypt(lblTitle.Text)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub rad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rad1.CheckedChanged, rad2.CheckedChanged, rad3.CheckedChanged, rad4.CheckedChanged, rad5.CheckedChanged, rad6.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub chkSelection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelection1.CheckedChanged, chkSelection2.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Dim chkControl As New HtmlInputCheckBox
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                chkControl.Checked = chkSelectAll.Checked
            Next
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub ListButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListButton1.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl IsNot Nothing Then
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                End If
            End If
        Next
        If IDs = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "AST0610"
                PrintBarcode(IDs)
            Case "P153067"
                GeneratePassword(IDs)
            Case "PI02014" '-------------------------------------
                PostPurchase(IDs)
            Case "PI04005", "PI04007"
                ApprovePurchase(IDs)
            Case "T200133"
                ProcessUnits(IDs)
        End Select
    End Sub

    Private Function ProcessUnits(ByVal BSU_IDs As String) As Boolean
        If BSU_IDs <> "" Then
            'If (BSU_IDs.Length - BSU_IDs.Replace("|", "").Length) > 2 Then lblError.Text = "Max 2 units can be selected"
            'Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, BSU_ID() As String = BSU_IDs.Split("|")
            'Dim dr1() As DataRow = Session("myData").Select("id='" & BSU_ID(0) & "'")
            'Dim dr2() As DataRow = Session("myData").Select("id='" & BSU_ID(1) & "'")
            'If dr1(0)("zone") <> dr2("zone") Then lblError.Text = "units must belong to same zone"
            'Page.ClientScript.RegisterStartupScript(Me.[GetType](), "CallMyMenu", "alert('ok')", True)
            For Each ID In BSU_ID
                'Dim strApprover() As String = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApprover(" & ID & ")").ToString.Split(";")
                'If strApprover(0) = 100 Then 'finally approved
                '    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set PRF_STATUS='C', PRF_STATUS_STR='Buyer:Print PO', PRF_APR_ID=" & strApprover(0) & " where PRF_ID=" & ID)
                '    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "Approval" & "','" & "Approvals complete, sent to buyer for raising Purchase Order" & "'," & ID & ",0,'" & "B" & "')")
                'Else
                '    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set PRF_STATUS_STR='Approval:with " & strApprover(1) & "', PRF_STATUS=case when PRF_STATUS='N' then 'S' else PRF_STATUS end, PRF_APR_ID=" & strApprover(0) & " where PRF_ID=" & ID)
                '    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "Approval" & "','" & "Sent for Approval to " & strApprover(1) & "'," & ID & "," & strApprover(0) & ",'" & "S" & "')")
                'End If
            Next
            Dim strConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            SqlHelper.ExecuteNonQuery(strConn, CommandType.Text, "if (select count(*) from PTV_PROCESS where prc_end is null)=0 insert into PTV_PROCESS (prc_unit) select bsu_shortname from businessunit where bsu_id='" & h_bsuid.Value & "'")
            'gridbind(True)
        Else
            lblError.Text = "select atleast one unit"
        End If
    End Function

    Private Function ApprovePurchase(ByVal PRF_IDs As String) As Boolean
        If PRF_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, PRF_ID() As String = PRF_IDs.Split("|")
            For Each ID In PRF_ID
                Dim strApprover() As String = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApprover(" & ID & ")").ToString.Split(";")
                If strApprover(0) = 100 Then 'finally approved
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set PRF_STATUS='C', PRF_STATUS_STR='Buyer:Print PO', PRF_APR_ID=" & strApprover(0) & " where PRF_ID=" & ID)
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "Approval" & "','" & "Approvals complete, sent to buyer for raising Purchase Order" & "'," & ID & ",0,'" & "B" & "')")
                Else
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set PRF_STATUS_STR='Approval:with " & strApprover(1) & "', PRF_STATUS=case when PRF_STATUS='N' then 'S' else PRF_STATUS end, PRF_APR_ID=" & strApprover(0) & " where PRF_ID=" & ID)
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) values ('" & Session("sUsr_name") & "','" & "Approval" & "','" & "Sent for Approval to " & strApprover(1) & "'," & ID & "," & strApprover(0) & ",'" & "S" & "')")
                End If
            Next
            gridbind(True)
        End If
    End Function

    Private Function PostPurchase(ByVal PRF_IDs As String) As Boolean
        If PRF_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@GRN_IDs", PRF_IDs.Replace("|", ","), SqlDbType.VarChar)
            Mainclass.ExecuteParamQRY(strConn, "PostGRN", sqlParam)
            SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) select '" & Session("sUsr_name") & "','" & "Invoice" & "','" & "Invoice Generated" & "',GRN_PRF_ID,0,'I' from GRN_H where GRN_ID in (" & PRF_IDs.Replace("|", ",") & ")")
            gridbind(True)
        End If
    End Function

    Protected Sub ListButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListButton2.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl.Checked Then
                lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
            End If
        Next
        If IDs = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "P153067"
                PrintPasswords(IDs)
        End Select
    End Sub
    Private Function PrintBarcode(ByVal ASM_IDs As String) As Boolean
        Try
            If ASM_IDs <> "" Then
                Dim sqlParam(0) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@ASM_IDs", ASM_IDs, SqlDbType.VarChar)
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                Dim dtBarcode As New DataTable
                dtBarcode = Mainclass.getDataTable("[ASSETS].[RPT_PrintAssetBarcode]", sqlParam, ConnectionManger.GetOASIS_ASSETConnectionString)
                Dim mRow As DataRow
                For Each mRow In dtBarcode.Rows
                    barcode.Number = mRow("ASM_CODE")
                    barcode.CustomText = mRow("ASM_CODE")
                    barcode.ChecksumAdd = True
                    barcode.NarrowBarWidth = 3
                    barcode.Height = 400
                    barcode.FontHeight = 0.2F
                    barcode.ForeColor = Drawing.Color.Black
                    Dim b As Byte()
                    ReDim b(barcode.Render(ImageType.Png).Length)
                    b = barcode.Render(ImageType.Png)
                    mRow("ImgBarCode") = b
                Next
                Session("ReportClassSource") = Nothing
                Dim RepClass As New ReportClass
                Dim strResourseName As String = String.Empty
                strResourseName = "../Asset/Reports/Rpt/rptPrintAssetBarcode.rpt"
                RepClass.ResourceName = strResourseName
                RepClass.ReportData = dtBarcode
                Session("ReportClassSource") = RepClass
                Response.Redirect("PrintViewer.aspx", True)
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function GeneratePassword(ByVal EMP_IDs As String) As Boolean
        If EMP_IDs <> "" Then
            Dim sql_Str As String
            Dim EMP_ID(), ID, Password, EncrPassword As String
            EMP_ID = EMP_IDs.Split("|")
            Dim passwordEncr As New Encryption64
            Dim strConn As String = ConnectionManger.GetOASISConnectionString
            For Each ID In EMP_ID
                sql_Str = "GeneratePasswordForEmployee '" & ID & "'"
                Password = Mainclass.getDataValue(sql_Str, "OASISConnectionString")
                If Password <> "" Then
                    EncrPassword = passwordEncr.Encrypt(Password)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@EMP_ID", ID, SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@Password", EncrPassword, SqlDbType.VarChar)
                    Mainclass.ExecuteParamQRY(strConn, "UpdatePasswordForEmployee", sqlParam)
                End If
            Next
            lblError.Text = "Password Generated For Selected Employees !!!!"
            gridbind(True)
        End If
    End Function
    Private Function PrintPasswords(ByVal EMP_IDs As String) As Boolean
        If EMP_IDs <> "" Then
            Try
                Dim sqlParam(0) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@EMP_Ids", EMP_IDs, SqlDbType.VarChar)
                Dim dtPasswords As New DataTable
                dtPasswords = Mainclass.getDataTable("RPT_PrintEmployeePasswords", sqlParam, ConnectionManger.GetOASISConnectionString)
                Dim mRow As DataRow
                Dim passwordEncr As New Encryption64
                For Each mRow In dtPasswords.Rows
                    mRow("EMP_PASSWORD") = passwordEncr.Decrypt(Convert.ToString(mRow("EMP_PASSWORD")).Replace(" ", "+"))
                Next
                Dim RepClass As New ReportClass
                Dim strResourseName As String = String.Empty
                Dim params As New Hashtable




                strResourseName = "../Payroll/Reports/Rpt/rptPrintEmpPwd.rpt"
                RepClass.ResourceName = strResourseName
                RepClass.ReportData = dtPasswords
                Session("ReportClassSource") = RepClass
                Response.Redirect("PrintViewer.aspx", True)
            Catch ex As Exception
                'Response.Redirect(ViewState("ReferrerUrl"))
            End Try
        End If
    End Function
    Private Property newTable() As Boolean
        Get
            Return ViewState("newtable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("newtable") = value
        End Set
    End Property
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property FilterString() As String
        Get
            Return ViewState("FilterString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property SQLSelect() As String
        Get
            Return ViewState("SQLSelect")
        End Get
        Set(ByVal value As String)
            ViewState("SQLSelect") = value
        End Set
    End Property
    Private Property SortColString() As String
        Get
            Return ViewState("SortColString")
        End Get
        Set(ByVal value As String)
            ViewState("SortColString") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property EditPagePath() As String
        Get
            Return ViewState("EditPagePath")
        End Get
        Set(ByVal value As String)
            ViewState("EditPagePath") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property
    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property
    Private Property PageCaption() As String
        Get
            Return ViewState("PageCaption")
        End Get
        Set(ByVal value As String)
            ViewState("PageCaption") = value
        End Set
    End Property
    Private Property HeaderTitle() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            Page.Title = value
            lblTitle.Text = value
        End Set
    End Property
    Private Property ShowGridCheckBox() As Boolean
        Get
            Return ViewState("ShowGridCheckBox")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowGridCheckBox") = value
        End Set
    End Property
    Private Property NoAddingAllowed() As Boolean
        Get
            Return ViewState("NoAddingAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoAddingAllowed") = value
        End Set
    End Property
    Private Property NoViewAllowed() As Boolean
        Get
            Return ViewState("NoViewAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAllowed") = value
        End Set
    End Property
    Private Property HideExportButton() As Boolean
        Get
            Return ViewState("HideExportButton")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideExportButton") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property IsAddNew() As Boolean
        Get
            Return ViewState("IsAddNew")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsAddNew") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Private Property FilterText1() As String
        Get
            Return chkSelection1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection1.Text = value
                chkSelection1.Visible = True
            Else
                chkSelection1.Visible = False
                chkSelection1.Text = ""
            End If
        End Set
    End Property
    Private Property FilterText2() As String
        Get
            Return chkSelection2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection2.Text = value
                chkSelection2.Visible = True
            Else
                chkSelection2.Visible = False
                chkSelection2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox1Text() As String
        Get
            Return rad1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad1.Text = value
                rad1.Visible = True
            Else
                rad1.Visible = False
                rad1.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox2Text() As String
        Get
            Return rad2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad2.Text = value
                rad2.Visible = True
            Else
                rad2.Visible = False
                rad2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox3Text() As String
        Get
            Return rad3.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad3.Text = value
                rad3.Visible = True
            Else
                rad3.Visible = False
                rad3.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox4Text() As String
        Get
            Return rad4.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad4.Text = value
                rad4.Visible = True
            Else
                rad4.Visible = False
                rad4.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox5Text() As String
        Get
            Return rad5.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad5.Text = value
                rad5.Visible = True
            Else
                rad5.Visible = False
                rad5.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox6Text() As String
        Get
            Return rad6.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad6.Text = value
                rad6.Visible = True
            Else
                rad6.Visible = False
                rad6.Text = ""
            End If
        End Set
    End Property
    Private Property ButtonText1() As String
        Get
            Return ListButton1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton1.Text = value
                ListButton1.Visible = True
            Else
                ListButton1.Visible = False
                ListButton1.Text = ""
            End If
        End Set
    End Property
    Private Property ButtonText2() As String
        Get
            Return ListButton2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton2.Text = value
                ListButton2.Visible = True
            Else
                ListButton2.Visible = False
                ListButton2.Text = ""
            End If
        End Set
    End Property

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Try
        '    If Session("sModule") = "SS" Then
        '        Me.MasterPageFile = "../../mainMasterPageSS.master"
        '    Else
        '        Me.MasterPageFile = "../../mainMasterPage.master"
        '    End If
        'Catch ex As Exception

        'End Try
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function getTable(ByVal report As String) As String
        Dim strConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        getTable = SqlHelper.ExecuteScalar(strConn, CommandType.Text, "select isnull((select 'Processing:'+prc_unit+', Start Time:'+cast(isnull(prc_start,getdate()) as varchar)+', Total Minutes:'+cast(datediff(minute,isnull(prc_start,getdate()),isnull(prc_end,getdate())) as varchar) from ptv_process where prc_end is null),'Process Complete')")
    End Function
End Class

