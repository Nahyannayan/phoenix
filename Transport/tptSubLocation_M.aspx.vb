Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptSubLocation_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000040") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("dtSbl") = SetDataTable()
                    ' gvtptSubLocation.DataSource = Session("dtSbl")
                    ' gvtptSubLocation.DataBind()
                    ViewState("SelectedRow") = -1
                    If ViewState("datamode") = "add" Then
                        BindLocation()
                    Else
                        Dim li As New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("location").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("locid").Replace(" ", "+"))
                        ddlLocation.Items.Add(li)
                        GetRows()
                        txtSubLocation.ReadOnly = True
                        gvtptSubLocation.Enabled = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""

            ddlLocation.Enabled = True
            txtSubLocation.ReadOnly = False
            gvtptSubLocation.Enabled = True
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Session("dtSbl") = SetDataTable()
            gvtptSubLocation.DataSource = Session("dtSbl")
            gvtptSubLocation.DataBind()
            ddlLocation.Enabled = True
            txtSubLocation.ReadOnly = False
            gvtptSubLocation.Enabled = True
            lblError.Text = ""
            BindLocation()
            txtSubLocation.Text = ""
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ' hfSBL_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                ddlLocation.Enabled = False
                txtSubLocation.ReadOnly = True
                gvtptSubLocation.Enabled = False
                lblError.Text = ""
                Session("dtSbl") = SetDataTable()
                gvtptSubLocation.DataSource = Session("dtSbl")
                gvtptSubLocation.DataBind()

                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            ddlLocation.Enabled = False
            txtSubLocation.ReadOnly = True
            gvtptSubLocation.Enabled = False
            SaveData()
            ddlLocation.Enabled = False
            txtSubLocation.ReadOnly = True
            txtSubLocation.Text = ""
            lblError.Text = "Record saved successfully"
            ViewState("datamode") = "view"
            Session("dtSbl") = SetDataTable()
            GetRows()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            If ViewState("SelectedRow") = -1 Then
                AddRows()
            Else
                EditRows()
            End If
            ViewState("SelectedRow") = -1
            btnAddNew.Text = "Add"
            ddlLocation.Enabled = True
            txtSubLocation.Text = ""
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvtptSubLocation_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvtptSubLocation.PageIndexChanging
        Try
            gvtptSubLocation.PageIndex = e.NewPageIndex
            gvtptSubLocation.DataSource = Session("dtSbl")
            gvtptSubLocation.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvtptSubLocation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvtptSubLocation.RowCommand
        Try
            If e.CommandName = "edit" Or e.CommandName = "delete" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvtptSubLocation.Rows(index), GridViewRow)
                Dim lblSubLocation As New Label
                Dim lblflag As Label

                lblSubLocation = selectedRow.Cells(3).FindControl("lblSubLocation")
                lblflag = selectedRow.FindControl("lblflag")
                lblError.Text = ""
                Dim keys() As Object
                ReDim keys(2)
                Dim dt As DataTable
                dt = Session("dtSbl")

                '   keys(0) = ddlLocation.SelectedValue
                Dim lbllocid As Label = selectedRow.FindControl("lbllocId")
                keys(0) = lbllocid.Text
                keys(1) = lblSubLocation.Text
                keys(2) = lblflag.Text

                index = dt.Rows.IndexOf(dt.Rows.Find(keys))

                If e.CommandName = "edit" Then
                    txtSubLocation.Text = lblSubLocation.Text
                    ViewState("SelectedRow") = index
                    btnAddNew.Text = "Update"
                Else

                    If dt.Rows(index).Item(4) = "add" Then
                        dt.Rows(index).Item(4) = "remove"
                    ElseIf dt.Rows(index).Item(4) = "edit" Then
                        If isPickupPointsExists(dt.Rows(index).Item(0)) = True Then
                            lblError.Text = "Please delete the pickup points under this sublocation before deleting the sublocation"
                            Exit Sub
                        End If
                        dt.Rows(index).Item(4) = "delete"
                        dt.Rows(index).Item(5) = 1
                    End If

                    Session("dtSbl") = dt
                    GridBind(dt)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvtptSubLocation_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvtptSubLocation.RowDeleting

    End Sub

    Protected Sub gvtptSubLocation_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvtptSubLocation.RowEditing

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        lblError.Text = ""
    End Sub

#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindLocation()
        ddlLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select loc_id,loc_description from TRANSPORT.LOCATION_M where  " _
                                  & " loc_id NOT IN( SELECT sbl_loc_id from TRANSPORT.SUBLOCATION_M)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "loc_description"
        ddlLocation.DataValueField = "loc_id"
        ddlLocation.DataBind()
    End Sub

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(2)

      

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbl_id"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbl_loc_id"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "loc_description"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "sbl_description"
        dt.Columns.Add(column)
        keys(1) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "datamode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "flag"
        dt.Columns.Add(column)
        keys(2) = column

        ' dt.PrimaryKey = keys
        Return dt
    End Function

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String
                Dim sblId As Integer
                Dim sblAddIds As String = ""
                Dim sblEditIds As String = ""
                Dim sblDeleteIds As String = ""
                Dim dt As DataTable
                Dim flagAudit As Integer
                dt = Session("dtSbl")
                Dim dr As DataRow
                For Each dr In dt.Rows
                    With dr
                        If .Item(4).ToString <> "remove" And .Item(4).ToString <> "edit" Then
                            If .Item(4).ToString = "update" Then
                                UtilityObj.InsertAuditdetails(transaction, "edit", "TRANSPORT.SUBLOCATION_M", "SBL_ID", "SBL_LOC_ID", "SBL_ID=" + .Item(0).ToString)
                            ElseIf .Item(4).ToString = "delete" Then
                                UtilityObj.InsertAuditdetails(transaction, "delete", "TRANSPORT.SUBLOCATION_M", "SBL_ID", "SBL_LOC_ID", "SBL_ID=" + .Item(0).ToString)
                            End If

                            str_query = "exec [TRANSPORT].[SaveSUBLOCATION] " + .Item(0).ToString + "," + .Item(1).ToString + ",'" + .Item(3).Replace("'", "''") + "','" + .Item(4) + "'"
                            sblId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                            If .Item(4) = "add" Then
                                If sblAddIds.Length <> 0 Then
                                    sblAddIds += ","
                                End If
                                sblAddIds += sblId.ToString
                            End If

                            If .Item(4) = "update" Then
                                If sblEditIds.Length <> 0 Then
                                    sblEditIds += ","
                                End If
                                sblEditIds += sblId.ToString
                            End If

                            If .Item(4) = "delete" Then
                                If sblDeleteIds.Length <> 0 Then
                                    sblDeleteIds += ","
                                End If
                                sblDeleteIds += sblId.ToString
                            End If
                        End If
                    End With
                Next

                If sblAddIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SBL_ID(" + sblAddIds + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If sblEditIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SBL_ID(" + sblEditIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If sblDeleteIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SBL_ID(" + sblDeleteIds + ")", "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub AddRows()
        Try
            Dim dr As DataRow
            Dim dt As New DataTable
            dt = Session("dtSbl")

            ''check if the row is already added
            Dim keys As Object()
            ReDim keys(2)
            keys(0) = ddlLocation.SelectedValue
            keys(1) = txtSubLocation.Text
            keys(2) = "0"
            Dim row As DataRow = dt.Rows.Find(keys)
            If Not row Is Nothing Then
                lblError.Text = "This sublocation is already added"
                Exit Sub
            End If
            dr = dt.NewRow
            dr.Item(0) = 0
            dr.Item(1) = ddlLocation.SelectedValue
            dr.Item(2) = ddlLocation.SelectedItem.Text
            dr.Item(3) = txtSubLocation.Text
            dr.Item(4) = "add"
            dr.Item(5) = "0"
            dt.Rows.Add(dr)
            Session("dtSbl") = dt
            GridBind(dt)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtSbl")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                .Item(3) = txtSubLocation.Text.ToString
                If .Item(4) = "edit" Then
                    .Item(4) = "update"
                End If
            End With
            gvtptSubLocation.DataSource = dt
            gvtptSubLocation.DataBind()
            Session("dtSbl") = dt
            GridBind(dt)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(4) <> "delete" And dt.Rows(i)(4) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvtptSubLocation.DataSource = dtTemp
        gvtptSubLocation.DataBind()
    End Sub
    Private Sub GetRows()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT sbl_loc_id,sbl_id,loc_description,sbl_description from tptsublocation_m A,tptlocation_m B" _
        '                          & " where A.sbl_loc_id=B.loc_id and sbl_loc_id=" + ddlLocation.SelectedValue

        Dim str_query As String = "SELECT SBL_ID, SBL_LOC_ID,LOC_DESCRIPTION,SBL_DESCRIPTION " _
                                & "FROM TRANSPORT.LOCATION_M AS A INNER JOIN " _
                                & " TRANSPORT.SUBLOCATION_M AS B ON A.LOC_ID = B.SBL_LOC_ID" _
                                & " WHERE SBL_LOC_ID=" + ddlLocation.SelectedValue _
                                & " ORDER BY SBL_DESCRIPTION "
        Dim dt As DataTable
        Dim dr As DataRow
        dt = Session("dtSbl")

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            dr = dt.NewRow
            With dr
                .Item(0) = reader.GetValue(0).ToString
                .Item(1) = reader.GetValue(1).ToString
                .Item(2) = reader.GetString(2)
                .Item(3) = reader.GetString(3)
                .Item(4) = "edit"
                .Item(5) = "0"
                dt.Rows.Add(dr)
            End With
        End While
        reader.Close()
        gvtptSubLocation.DataSource = dt
        gvtptSubLocation.DataBind()
        Session("dtSbl") = dt

        gvtptSubLocation.Columns(2).Visible = False
    End Sub

    Private Function isPickupPointsExists(ByVal sblid As Integer) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select count(pnt_id) from TRANSPORT.pickuppoints_m where pnt_sbl_id=" + sblid.ToString
        Dim loc As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If loc = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

#End Region
End Class
