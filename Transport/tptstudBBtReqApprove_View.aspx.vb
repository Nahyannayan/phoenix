Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Partial Class Transport_tptstudBBtReqApprove_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ' hfBSU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("bsuid").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000260" And ViewState("MainMnu_code") <> "T000270") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"


                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_9.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_10.Value = "LI__../Images/operations/like.gif"

                    If ViewState("MainMnu_code") = "T000260" And (Session("SBSUID") = "900500" Or Session("SBSUID") = "900501") Then
                        BindBsu()
                    Else
                        Dim li As New ListItem
                        li.Text = Session("sbsuid")
                        li.Value = Session("sbsuid")
                        ddlBsu.Items.Insert(0, li)
                        tblTPT.Rows(1).Visible = False
                    End If


                    If Not ddlBsu.SelectedValue = 0 Then
                        If tblTPT.Rows(2).Visible = True Then
                            BindCurriculum(ddlBsu.SelectedValue)
                            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue)
                            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                        Else
                            BindAcademicYear()
                            BindGrade()
                        End If
                        

                        Dim li As New ListItem
                        li.Text = "All"
                        li.Value = "0"
                        ddlGrade.Items.Insert(0, li)
                    Else
                        ddlClm.Items.Clear()
                        tblTPT.Rows(2).Visible = False
                        ddlAcademicYear.Items.Clear()
                        ddlGrade.Items.Clear()
                        Dim li As New ListItem
                        li.Text = "All"
                        li.Value = "0"
                        ddlGrade.Items.Insert(0, li)

                        li = New ListItem
                        li.Text = "All"
                        li.Value = "0"
                        ddlAcademicYear.Items.Insert(0, li)
                    End If



                    Select Case ViewState("MainMnu_code")
                        Case "T000260"
                            lblTitle.Text = "TRANSPORT REGISTRATION APPROVAL-STS"
                        Case "T000270"
                            lblTitle.Text = "TRANSPORT REGISTRATION APPROVAL-AO"
                            tblTPT.Rows(2).Visible = False
                    End Select
                    tblTPT.Rows(6).Visible = False

                    tblTPT.Rows(7).Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else



        End If
    End Sub


    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnArea_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnPickup_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnAcademicYear_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid9(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_9_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid10(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_9_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT BBT_BSU_ID,BBT_ID,ISNULL(BBT_STU_FIRSTNAME,'')+' '+ISNULL(BBT_STU_MIDNAME,'')+' '+ISNULL(BBT_STU_LASTNAME,' ') " _
        '                        & " AS STU_NAME,GRM_DISPLAY,SBL_DESCRIPTION,PNT_DESCRIPTION,ACY_DESCR,BSU_SHORT_RESULT FROM STUDENT_BBTREQ_S AS A" _
        '                        & " INNER JOIN VV_GRADE_BSU_M AS B ON A.BBT_GRM_ID=B.GRM_ID" _
        '                        & " INNER JOIN TRANSPORT.SUBLOCATION_M AS C ON A.BBT_SBL_ID=C.SBL_ID" _
        '                        & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS D ON A.BBT_PNT_ID=D.PNT_ID" _
        '                        & " INNER JOIN OASIS..ACADEMICYEAR_M AS E ON A.BBT_ACY_ID=E.ACY_ID" _
        '                        & " INNER JOIN BUSINESSUNIT_M AS F ON A.BBT_BSU_ID=F.BSU_ID" _
        '                        & " WHERE BBT_id<>0"

        Dim str_query As String = "SELECT BBT_BSU_ID,BBT_ID,ISNULL(BBT_STU_FIRSTNAME,'')+' '+ISNULL(BBT_STU_MIDNAME,'')+' '+ISNULL(BBT_STU_LASTNAME,' ')  " _
                                  & " AS STU_NAME,GRM_DISPLAY,ISNULL(SBL_DESCRIPTION,'') AS SBL_DESCRIPTION,ISNULL(PNT_DESCRIPTION,'') AS PNT_DESCRIPTION,ACY_DESCR,BSU_SHORT_RESULT FROM STUDENT_BBTREQ_S AS A" _
                                  & " INNER JOIN VV_GRADE_BSU_M AS B ON A.BBT_GRM_ID=B.GRM_ID " _
                                  & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS D ON A.BBT_PNT_ID=D.PNT_ID " _
                                  & "LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS C ON D.PNT_SBL_ID=C.SBL_ID " _
                                  & "LEFT OUTER JOIN OASIS..ACADEMICYEAR_M AS E ON A.BBT_ACY_ID=E.ACY_ID " _
                                  & " INNER JOIN BUSINESSUNIT_M AS F ON A.BBT_BSU_ID=F.BSU_ID " _
                                  & " WHERE BBT_id <> 0 "

        'If ViewState("MainMnu_code") = "T000260" Then
        'If rdApplied.Checked = True Then
        '    str_query += " AND BBT_bSTSAPPROVE IS NULL"
        'Else
        '    str_query += " AND BBT_bSTSAPPROVE='FALSE'"
        'End If
        'ElseIf ViewState("MainMnu_code") = "T000270" Then
        'If rdApplied.Checked = True Then
        '    str_query += " AND ISNULL(BBT_bSTSAPPROVE,'FALSE')='TRUE' AND BBT_bAOAPPROVE IS NULL"
        'Else
        '    str_query += " AND ISNULL(BBT_bSTSAPPROVE,'FALSE')='TRUE' AND BBT_bAOAPPROVE='FALSE'"
        'End If
        'End If

        If rdApplied.Checked = True Then
            str_query += " AND BBT_bAOAPPROVE IS NULL"
        Else
            str_query += " AND BBT_bAOAPPROVE='FALSE'"
        End If

        If ViewState("MainMnu_code") = "T000260" Then
            If ddlBsu.SelectedValue <> "0" Then
                str_query += " AND BBT_BSU_ID='" + ddlBsu.SelectedValue + "'"
            End If
        Else
            str_query += " AND BBT_BSU_ID='" + Session("sbsuid") + "'"
        End If

        If ddlAcademicYear.SelectedIndex <> 0 Then
            str_query += " AND GRM_ACD_ID= '" + ddlAcademicYear.SelectedItem.Value + "'"
        End If

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND GRM_GRD_ID= '" + ddlGrade.SelectedItem.Value + "'"
        End If
        If txtId.Text <> "" Then
            str_query += " AND BBT_ID LIKE '%" + txtId.Text + "%'"
        End If


        If txtName.Text <> "" Then
            str_query += " AND ISNULL(BBT_STU_FIRSTNAME,'')+' ' + ISNULL(BBT_STU_MIDNAME,'')+' '+ISNULL(BBT_STU_LASTNAME,'') LIKE '%" + txtName.Text + "%' "
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedArea As String = ""
        Dim selectedPickup As String = ""
        Dim selectedYear As String = ""

        Dim txtSearch As New TextBox

        If gvStud.Rows.Count > 0 Then

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtAcademicYear")
            strSidsearch = h_Selected_menu_10.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ACY_DESCR", txtSearch.Text, strSearch)
            selectedYear = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(BBT_STU_FIRSTNAME,'')+' '+ISNULL(BBT_STU_MIDNAME,'')+' '+ISNULL(BBT_STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_7.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtArea")
            strSidsearch = h_Selected_menu_8.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SBL_DESCRIPTION", txtSearch.Text, strSearch)
            selectedArea = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtPickup")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("PNT_DESCRIPTION", txtSearch.Text, strSearch)
            selectedPickup = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If

        End If

        str_query += " ORDER BY bbt_STU_FIRSTNAME,bbt_STU_MIDNAME,bbt_STU_LASTNAME"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()
        End If

        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtAcademicYear")
        txtSearch.Text = selectedYear

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtArea")
        txtSearch.Text = selectedArea

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtPickup")
        txtSearch.Text = selectedPickup

        gvStud.Visible = True
        set_Menu_Img()


    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                  & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID " _
                                  & " WHERE BSU_TYPE<>'O' AND BST_BSU_OPRT_ID='" + Session("SBSUID") + "'" _
                                  & " and ISNULL(BSU_bGEMSSCHOOL,'FALSE')='FALSE' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlBsu.Items.Insert(0, li)
    End Sub
    Sub BindCurriculum(ByVal bsu_id As String)
        ddlClm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CLM_ID,CLM_DESCR FROM CURRICULUM_M AS A" _
                                & " INNER JOIN ACADEMICYEAR_D AS B ON A.CLM_ID=B.ACD_CLM_ID" _
                                & " WHERE ACD_BSU_ID='" + bsu_id + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClm.DataSource = ds
        ddlClm.DataTextField = "CLM_DESCR"
        ddlClm.DataValueField = "CLM_ID"
        ddlClm.DataBind()
        If ddlClm.Items.Count = 1 Then
            tblTPT.Rows(2).Visible = False
        Else
            tblTPT.Rows(2).Visible = True
        End If
    End Sub

#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        tblTPT.Rows(6).Visible = True
        tblTPT.Rows(7).Visible = True
        GridBind()
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        If e.CommandName = "view" Then
            Dim url As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
            Dim lblAcdYear As Label = selectedRow.FindControl("lblAcademicYear")
            Dim acdYear As String = lblAcdYear.Text
            Dim lblBBTId As Label
            Dim lblBsuId As Label
            lblBBTId = selectedRow.FindControl("lblBBTId")
            lblBsuId = selectedRow.FindControl("lblBsuId")
            If ViewState("MainMnu_code") = "T000260" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                url = String.Format("~\Transport\tptstudBBtReqApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(acdYear) + "&bbtid=" + Encr_decrData.Encrypt(lblBBTId.Text) + "&bsuid=" + Encr_decrData.Encrypt(lblBsuId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            Else
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                url = String.Format("~\Transport\tptstudBBTAOApprove.aspx?MainMnu_code={0}&datamode={1}&bbtid=" + Encr_decrData.Encrypt(lblBBTId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If

        End If
    End Sub

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + ddlBsu.SelectedValue.ToString + "'" _
                                  & " ORDER BY ACY_ID DESC"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        ddlAcademicYear.Items.Insert(0, New ListItem("ALL", "0"))

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + ddlBsu.SelectedValue.ToString + "' "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then

            Dim li As New ListItem
            li.Text = ds.Tables(0).Rows(0).Item(0)
            li.Value = ds.Tables(0).Rows(0).Item(1)
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        End If
        gvStud.Visible = False
    End Sub

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged

        If Not ddlBsu.SelectedValue = 0 Then
            BindCurriculum(ddlBsu.SelectedValue)
            'changed By Minu -- added the true/false condition
            If tblTPT.Rows(2).Visible = True Then
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sBsuid"))
                ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
            Else
                BindAcademicYear()
                BindGrade()
            End If
            gvStud.Visible = False
            'Dim li As New ListItem
            'li.Text = "All"
            'li.Value = "0"
            'ddlGrade.Items.Insert(0, li)
        Else
            ddlClm.Items.Clear()
            tblTPT.Rows(2).Visible = False
            ddlAcademicYear.Items.Clear()
            ddlGrade.Items.Clear()
            Dim li As New ListItem
            li.Text = "All"
            li.Value = "0"
            ddlGrade.Items.Insert(0, li)

            li = New ListItem
            li.Text = "All"
            li.Value = "0"
            ddlAcademicYear.Items.Insert(0, li)
        End If

    End Sub

    

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT GRM_ID,GRM_DISPLAY FROM GRADE_BSU_M AS A " _
                                & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY GRD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_ID"
        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, New ListItem("ALL", "0"))
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
    End Sub

End Class
