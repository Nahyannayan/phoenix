Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Partial Class Transport_tptstudTransportAllocation_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100180") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvAllot.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0

                    hfJourney.Value = Encr_decrData.Decrypt(Request.QueryString("journey").Replace(" ", "+"))
                    hfTRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("trdid").Replace(" ", "+"))
                    hfSHF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))
                    hfSeats.Value = Encr_decrData.Decrypt(Request.QueryString("seats").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    lblTrip.Text = Encr_decrData.Decrypt(Request.QueryString("trip").Replace(" ", "+")) + " (" + Encr_decrData.Decrypt(Request.QueryString("busno").Replace(" ", "+")) + ")"

                    ' ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlTrip.Enabled = False
                    GridBind()
                    gvSeats.Visible = False
                    BindSeats1()
                    If hfJourney.Value = "Onward" Then
                        chkEnable.Text = "Allocate return trip"
                    Else
                        chkEnable.Text = "Allocate onward trip"
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else

            highlight_grid()



        End If
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
  
    Protected Sub ddlgvStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub BindTrip()

        Dim str_PickUp As String = ""
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = " SELECT DISTINCT TRD_ID,TRP_DESCR+' ('+BNO_DESCR+')' AS TRP_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                                & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                                & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID INNER JOIN" _
                                & " TRANSPORT.TRIPS_PICKUP_S AS D ON C.TPP_PNT_ID = D.TPP_PNT_ID INNER JOIN" _
                                & " TRANSPORT.BUSNOS_M AS E ON B.TRD_BNO_ID=E.BNO_ID " _
                                & " WHERE TRD_TODATE IS NULL AND D.TPP_TRD_ID=" + hfTRD_ID.Value _
                                & " AND A.TRP_JOURNEY='" + IIf(hfJourney.Value = "Onward", "Return", "Onward") + "'" _
                                & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()
    End Sub

    Sub BindPickUps()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select STUFF((SELECT ' , '+ PNT_DESCRIPTION FROM transport.PICKUPPOINTS_M as a inner join transport.TRIPS_PICKUP_S " _
                              & " as b on a.pnt_id=b.tpp_pnt_id  where TPP_TRD_ID = " + ddlTrip.SelectedValue.ToString _
                              & " for xml path('')),1,1,'')"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
         lblPickupPoints.Text = "Pickup points : "
        While reader.Read
            lblPickupPoints.Text += reader.GetString(0)
        End While
        reader.Close()
    End Sub

    Sub BindSeats()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT TRD_VEH_CAPACITY,TRD_VEH_AVAILABLE=TRD_VEH_CAPACITY-TRD_VEH_ALLOTED" _
        '                         & " FROM TRANSPORT.TRIPS_D WHERE TRD_ID=" + ddlTrip.SelectedValue.ToString
        Dim str_query As String = "exec TRANSPORT.getSEATCAPACITY " _
                               & "'" + Session("sbsuid") + "'," _
                               & ddlTrip.SelectedValue.ToString + "," _
                               & hfACD_ID.Value + "," _
                               & Session("Current_ACY_ID") + "," _
                               & "'" + IIf(hfJourney.Value = "Onward", "Return", "Onward") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSeats.DataSource = ds
        gvSeats.DataBind()
    End Sub
    Sub BindSeats1()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT TRD_VEH_CAPACITY,TRD_VEH_AVAILABLE=TRD_VEH_CAPACITY-TRD_VEH_ALLOTED" _
        '                         & " FROM TRANSPORT.TRIPS_D WHERE TRD_ID=" + ddlTrip.SelectedValue.ToString
        Dim str_query As String = "exec TRANSPORT.getSEATCAPACITY " _
                               & "'" + Session("sbsuid") + "'," _
                               & hfTRD_ID.Value + "," _
                               & hfACD_ID.Value + "," _
                               & Session("Current_ACY_ID") + "," _
                               & "'" + hfJourney.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSeat1.DataSource = ds
        gvSeat1.DataBind()
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = " AND " + "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = " AND " + "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvAllot.Rows.Count - 1
            Dim row As GridViewRow = gvAllot.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        If hfJourney.Value = "Onward" Then
            'str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
            '         & " STU_PICKUP_BUSNO,STU_DROPOFF_BUSNO,GRM_DISPLAY,SCT_DESCR,SSV_RATE " _
            '         & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
            '         & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
            '         & " INNER JOIN STUDENT_SERVICES_D AS D ON A.STU_ID=D.SSV_STU_ID AND SSV_SVC_ID=1 AND SSV_TODATE IS NULL AND SSV_ACD_ID=" + hfACD_ID.Value _
            '         & " WHERE STU_PICKUP IS NOT NULL AND STU_PICKUP<>'' AND STU_PICKUP_TRV_ID IS NULL"

            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,'')), " _
                       & " C.PNT_DESCRIPTION AS PICKUP,D.PNT_DESCRIPTION AS DROPOFF,STU_PICKUP,STU_DROPOFF,SSV_FROMDATE " _
                       & " FROM STUDENT_M  AS A " _
                       & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.STU_PICKUP=B.TPP_PNT_ID" _
                       & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON A.STU_PICKUP=C.PNT_ID " _
                       & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS D ON A.STU_DROPOFF=D.PNT_ID " _
                       & " INNER JOIN OASIS.DBO.STUDENT_SERVICES_D AS E ON A.STU_ID=E.SSV_STU_ID AND E.SSV_SVC_ID=1 AND E.SSV_bACTIVE='TRUE' " _
                       & " WHERE  STU_PICKUP_TRP_ID IS NULL AND STU_ACD_ID=" + hfACD_ID.Value + " AND TPP_TRD_ID=" + hfTRD_ID.Value _
                       & " AND STU_SHF_ID=" + hfSHF_ID.Value _
                       & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                       & " AND STU_CURRSTATUS<>'CN'"



        Else
            'str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
            '          & " STU_PICKUP_BUSNO,STU_DROPOFF_BUSNO,GRM_DISPLAY,SCT_DESCR,SSV_RATE " _
            '          & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
            '          & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
            '          & " INNER JOIN STUDENT_SERVICES_D AS D ON A.STU_ID=D.SSV_STU_ID AND SSV_SVC_ID=1 AND SSV_TODATE IS NULL AND SSV_ACD_ID=" + hfACD_ID.Value _
            '          & " WHERE STU_DROPOFF IS NOT NULL AND STU_DROPOFF<>'' AND STU_DROPOFF_TRV_ID IS NULL"
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,'')), " _
                               & " C.PNT_DESCRIPTION AS PICKUP,D.PNT_DESCRIPTION AS DROPOFF,STU_PICKUP,STU_DROPOFF,SSV_FROMDATE  " _
                               & " FROM STUDENT_M AS A " _
                               & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.STU_DROPOFF=B.TPP_PNT_ID" _
                               & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON A.STU_PICKUP=C.PNT_ID " _
                               & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS D ON A.STU_DROPOFF=D.PNT_ID " _
                               & " INNER JOIN OASIS.DBO.STUDENT_SERVICES_D AS E ON A.STU_ID=E.SSV_STU_ID AND E.SSV_SVC_ID=1 AND E.SSV_bACTIVE='TRUE' " _
                               & " WHERE STU_DROPOFF_TRP_ID IS NULL AND STU_ACD_ID=" + hfACD_ID.Value + " AND TPP_TRD_ID=" + hfTRD_ID.Value _
                               & " AND STU_SHF_ID=" + hfSHF_ID.Value _
                               & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                               & " AND STU_CURRSTATUS<>'CN'"


        End If



        ' Dim ddlgvGrade As New DropDownList

        '  Dim selectedGrade As String = ""
          Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox

        If gvAllot.Rows.Count > 0 Then


            txtSearch = gvAllot.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,'')", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
           
                strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

            'ddlgvGrade = gvAllot.HeaderRow.FindControl("ddlgvGrade")
            'If ddlgvGrade.Text <> "ALL" And ddlgvGrade.Text <> "" Then
            '    strFilter += " and grm_display='" + ddlgvGrade.Text + "'"
            '    selectedGrade = ddlgvGrade.Text
            'End If
            If strFilter <> "" Then
                str_query += strFilter
            End If
          


        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dv As DataView = ds.Tables(0).DefaultView
    

        Dim dt As DataTable
        gvAllot.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ViewState("norecord") = "1"
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvAllot.DataBind()
            Dim columnCount As Integer = gvAllot.Rows(0).Cells.Count
            gvAllot.Rows(0).Cells.Clear()
            gvAllot.Rows(0).Cells.Add(New TableCell)
            gvAllot.Rows(0).Cells(0).ColumnSpan = columnCount
            gvAllot.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvAllot.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            ViewState("norecord") = "0"
            gvAllot.DataBind()
        End If


        dt = ds.Tables(0)
        If gvAllot.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = strName

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtFeeSearch")
            txtSearch.Text = strFee


            'ddlgvGrade = gvAllot.HeaderRow.FindControl("ddlgvGrade")

            'Dim dr As DataRow

            'ddlgvGrade.Items.Clear()
            'ddlgvGrade.Items.Add("ALL")



            'For Each dr In dt.Rows
            '    If dr.Item(0) Is DBNull.Value Then
            '        Exit For
            '    End If
            '    With dr
            '        If ddlgvGrade.Items.FindByText(.Item(5)) Is Nothing Then
            '            ddlgvGrade.Items.Add(.Item(5))
            '        End If


            '    End With

            'Next
            'If selectedGrade <> "" Then
            '    ddlgvGrade.Text = selectedGrade
            'End If


        End If



        set_Menu_Img()
    End Sub


    Function SaveData() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        Dim lblAvailable As Label
        Dim journey As String

        If chkEnable.Checked = True Then
            journey = "ALL"
        Else
            journey = hfJourney.Value
        End If

        Dim trdPickup As String = ""
        Dim trdDropOff As String = ""
        If journey = "ALL" Then
            If hfJourney.Value = "Onward" Then
                trdPickup = hfTRD_ID.Value
                trdDropOff = ddlTrip.SelectedValue.ToString
            Else
                trdDropOff = hfTRD_ID.Value
                trdPickup = ddlTrip.SelectedValue.ToString
            End If
        ElseIf journey = "Onward" Then
            trdPickup = hfTRD_ID.Value
            trdDropOff = "NULL"
        ElseIf journey = "Return" Then
            trdPickup = "NULL"
            trdDropOff = hfTRD_ID.Value
        End If


        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblPickId As Label
        Dim lblDropId As Label

        Dim lblStuId As Label

        Dim chkflag As Boolean = False
        Dim count As Integer
        Dim lblDate As Label
        Dim flag As Boolean = True
        Dim stuNosSaved As String = ""
        For i = 0 To gvAllot.Rows.Count - 1
            With gvAllot.Rows(i)
                chkSelect = .FindControl("chkSelect")
                lblPickId = .FindControl("lblPickId")
                lblDropId = .FindControl("lblDropId")
                lblDate = .FindControl("lblDate")

              
                If chkSelect.Checked = True Then
                    If journey = "ALL" Then
                        str_query = "SELECT COUNT(TPP_ID) FROM TRANSPORT.TRIPS_PICKUP_S WHERE TPP_TRD_ID=" + ddlTrip.SelectedValue.ToString _
                                  & " AND TPP_PNT_ID=" + IIf(hfJourney.Value = "Onward", lblDropId.Text, lblPickId.Text)

                        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                        If count = 0 Then
                            journey = hfJourney.Value
                            flag = False
                        End If
                    End If
                    lblStuId = .FindControl("lblStuId")
                    str_query = "exec TRANSPORT.saveSTUDTRANSPORT " _
                              & trdPickup + "," _
                              & trdDropOff + "," _
                              & "'" + journey + "'," _
                              & lblStuId.Text + "," _
                              & lblPickId.Text + "," _
                              & lblDropId.Text + "," _
                              & "'" + Format(Date.Parse(lblDate.Text), "yyyy-MM-dd") + "'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    hfSeats.Value = Val(hfSeats.Value - 1)
                    If chkEnable.Checked = True Then
                        lblAvailable = gvSeats.Rows(0).FindControl("lblAvailable")
                        lblAvailable.Text = Val(lblAvailable.Text - 1)
                    End If
                    chkflag = True

                    If stuNosSaved <> "" Then
                        stuNosSaved += ","
                    End If
                    stuNosSaved += lblStuId.Text

                End If
            End With
        Next


        If chkflag = False Then
            lblError.Text = "No records selected"
            Return False
        End If

        If stuNosSaved <> "" Then
            CallReport(stuNosSaved)
        End If
        lblError.Text = "Record Saved Successfully"

        If flag = False Then
            If hfJourney.Value = "Onward" Then
                lblError.Text = "The drop off point requested by some students are not covered by the selected return trip." _
                               & " Please allocate the retrurn trip seperately for those students."
                Return True
            Else
                lblError.Text = "The pickup point requested by some students are not covered by the selected onward trip." _
                                & " Please allocate the onward trip seperately for those students."
                Return True
            End If
        End If
        Return True


    End Function

   

    Public Sub UserMsgBox(ByVal sMsg As String, ByVal frmMe As Object)

        Dim sb As New StringBuilder()
        Dim oFormObject As New System.Web.UI.Control
        If sMsg = "" Then
            Exit Sub
        End If
        sMsg = sMsg.Replace("'", "\'")
        sMsg = sMsg.Replace(Chr(34), "\" & Chr(34))
        sMsg = sMsg.Replace(vbCrLf, "\n")
        '  sMsg = "<script language=javascript>if(confirm(""" & sMsg & """)==true){ return true ; }else { return false; }</script>"
        'sMsg = "<script language=javascript>confirm(""" & sMsg & """)</script>"
        sMsg = "<script language=javascript>return confirm_enroll();</script>"
        'sb = New StringBuilder()
        ' sb.Append(sMsg)
        'sb.AppendLine("<script language=javascript type=text/javascript>")
        'sb.AppendLine("if(confirm(""" & sMsg & """)==true)")
        'sb.AppendLine("return true ;")
        'sb.AppendLine("else")
        'sb.AppendLine("return false;")

        For Each oFormObject In frmMe.Controls
            If TypeOf oFormObject Is HtmlForm Then
                Exit For
            End If
        Next

        ' Add the javascript after the form object so that the 
        ' message doesn't appear on a blank screen.
        oFormObject.Controls.AddAt(oFormObject.Controls.Count, New LiteralControl(sb.ToString()))

    End Sub

    Sub CallReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptStudTransportInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function


    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

#End Region

    Protected Sub chkEnable_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEnable.CheckedChanged
        Try
            If chkEnable.Checked = True Then
                ddlTrip.Enabled = True
                BindTrip()
                If ddlTrip.Items.Count > 0 Then
                    BindSeats()
                    BindPickUps()
                End If
                gvSeats.Visible = True
            Else
                ddlTrip.Items.Clear()
                ddlTrip.Enabled = False
                gvSeats.Visible = False
            End If
            If ViewState("norecord") = "1" Then
                GridBind()  'for the display to be proper
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAllot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllot.Click
        Try
            lblError.Text = ""
            Dim str As String = CheckSeats()
            If str <> "" Then
                gvAllot.Enabled = False
                btnAllot.Enabled = False
                Panel1.Visible = True
                lblText.Text = str
                Exit Sub
            End If
            If SaveData() = True Then
                GridBind()
                If chkEnable.Checked = True Then
                    BindSeats()
                End If
                BindSeats1()

            End If
            If ViewState("norecord") = "1" Then
                GridBind()  'for the display to be proper
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    'Sub ShowMessage(ByVal message As String)
    '    Dim newBox As New MessageBox
    '    newBox.MessageBoxButton = 2

    '    newBox.MessageBoxTop = 350

    '    newBox.MessageBoxLeft = 400

    '    newBox.MessageBoxWidth = 300

    '    newBox.MessageBoxHeight = 150

    '    newBox.MessageBoxButtonWidth = 50

    '    newBox.MessageBoxIDYes = "yesno"

    '    newBox.MessageBoxIDNo = "yesno"

    '    newBox.MessageBoxIDCancel = "yesno"

    '    newBox.MessageBoxButtonYesText = "Yes"

    '    newBox.MessageBoxButtonNoText = "No"

    '    newBox.MessageBoxButtonCancelText = "Cancel"

    '    newBox.MessageBoxTitle = "Student Transport Allocation."

    '    newBox.MessageBoxMessage = message

    '    newBox.MessageBoxImage = "~/Images/icon-question.gif"

    '    PlaceHolder1.Controls.Add(newBox)


    'End Sub

    Function CheckSeats() As String

        Dim chkSelect As CheckBox
        Dim seatsReqd As Integer = 0
        Dim lblAvailable As Label
        Dim seat1 As Integer = hfSeats.Value
        Dim seat2 As Integer = 0

        Dim strText As String = ""

        Dim i As Integer
        For i = 0 To gvAllot.Rows.Count - 1
            chkSelect = gvAllot.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                seatsReqd += 1
            End If
        Next

        If seatsReqd > seat1 Then
            strText = lblTrip.Text
        End If

        If chkEnable.Checked = True Then
            lblAvailable = gvSeats.Rows(0).FindControl("lblAvailable")
            seat2 = Val(lblAvailable.Text)
            If seatsReqd > seat2 Then
                If strText <> "" Then
                    strText += " and "
                End If
                strText += ddlTrip.SelectedItem.Text
            End If
        End If

        If strText <> "" Then
            Return "The selected students has exceeded the seats available for " + strText _
                          & " .Do you wish to proceed?"

        End If
        Return ""

    End Function


    Protected Sub gvAllot_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAllot.PageIndexChanging
        Try
            gvAllot.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    'Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
    '    GridBind()
    'End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            gvAllot.Enabled = True
            btnAllot.Enabled = True
            Panel1.Visible = False
            SaveData()
            GridBind()
            If chkEnable.Checked = True Then
                BindSeats()
            End If
            BindSeats1()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Try
            gvAllot.Enabled = True
            btnAllot.Enabled = True
            Panel1.Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrip.SelectedIndexChanged
        Try
            lblError.Text = ""
            BindSeats()
            BindPickUps()
            If ViewState("norecord") = "1" Then
                GridBind()  'for the display to be proper
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
