﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptPromotions_view.aspx.vb" Inherits="Transport_tptPromotions_view" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>Promotions Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>

                        <td align="center">

                            <table id="Table1" runat="server" align="center" width="100%">



                                <tr>
                                    <td align="center">

                                        <table id="Table2" runat="server" align="center" width="100%">

                                            <tr>
                                                <td colspan="3" align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3" align="center">
                                                    <asp:GridView ID="gvTptPromotions" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="grd_id" Visible="False">

                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTptPromoId" runat="server" Text='<%# Bind("TPT_PROMO_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Promotion Name">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblPromoName" runat="server" Text="Promotion Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtPromoName" runat="server" Width="75%" autocomplete="off"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnPromoName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnPromoName_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPromoName" runat="server" Text='<%# Bind("TPT_PROMO_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Prmotion Description">                                                                
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPromoDescr" runat="server" Text='<%# Bind("TPT_PROMO_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           <%-- <asp:TemplateField HeaderText="Prmotion Amount" ItemStyle-HorizontalAlign="Right">                                                                
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPromoAmt" runat="server" Text='<%# Bind("TPT_PROMO_AMOUNT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Prmotion Period">                                                                
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPromoPeriod" runat="server" Text='<%# Bind("PROMO_PERIOD") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>

                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                                        <RowStyle CssClass="griditem" Wrap="False" />
                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
</asp:Content>

