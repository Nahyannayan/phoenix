Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Partial Class Transport_tptStudTransportBulkAllocation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100450") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvAllot.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0


                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid"))
                    PopulateGrade()
                    BindShift()

                    BindBusNo(ddlBusNo)

                    If ddlClm.Items.Count = 1 Then
                        tblTpt.Rows(0).Visible = False
                    End If

                    tblTpt.Rows(6).Visible = False
                    tblTpt.Rows(7).Visible = False
                    tblTpt.Rows(8).Visible = False
                    tblTpt.Rows(9).Visible = False
                    tblTpt.Rows(10).Visible = False
                    tblTpt.Rows(11).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else
            highlight_grid()
        End If
    End Sub
    Sub PopulateGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRM_DISPLAY,GRD_DISPLAYORDER FROM GRADE_BSU_M AS A " _
                               & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                               & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " ORDER BY GRD_DISPLAYORDER "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnArea_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub gvAllot_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAllot.PageIndexChanging
        Try
            gvAllot.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvBno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = " AND " + "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = " AND " + "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvAllot.Rows.Count - 1
            Dim row As GridViewRow = gvAllot.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub
    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,'')), " _
                       & " C.PNT_DESCRIPTION AS PICKUP,D.PNT_DESCRIPTION AS DROPOFF,STU_PICKUP,STU_DROPOFF,SSV_FROMDATE, " _
                       & " STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME,BNO_DESCR, " _
                       & " IMGBNO=CASE  WHEN STU_PICKUP_TRP_ID IS NULL THEN '~/images/cross.gif' ELSE '~/images/tick.gif' END," _
                       & " bPICK=CASE  WHEN STU_PICKUP_TRP_ID IS NULL THEN 'TRUE' ELSE 'FALSE' END,SBL_DESCRIPTION" _
                       & " FROM STUDENT_M  AS A " _
                       & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.STU_PICKUP=B.TPP_PNT_ID" _
                       & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON A.STU_PICKUP=C.PNT_ID " _
                       & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS D ON A.STU_DROPOFF=D.PNT_ID " _
                       & " INNER JOIN TRANSPORT.SUBLOCATION_M AS P ON A.STU_SBL_ID_PICKUP=P.SBL_ID" _
                       & " INNER JOIN OASIS.DBO.STUDENT_SERVICES_D AS E ON A.STU_ID=E.SSV_STU_ID AND E.SSV_SVC_ID=1 AND E.SSV_bACTIVE='TRUE' " _
                       & " LEFT OUTER JOIN TRANSPORT.VV_BUSES AS F ON A.STU_PICKUP_TRP_ID=F.TRP_ID" _
                       & " WHERE   STU_ACD_ID=" + hfACD_ID.Value _
                       & " AND STU_SHF_ID=" + hfSHF_ID.Value _
                       & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                       & " AND STU_CURRSTATUS<>'CN' AND SSV_ACD_ID=" + hfACD_ID.Value


        If ddlBusNo.SelectedValue <> "0" Then
            str_query += " AND TRD_ID IN (SELECT TRD_ID FROM TRANSPORT.TRIPS_D WHERE TRD_TODATE IS NULL AND TRD_BNO_ID=" + ddlBusNo.SelectedValue.ToString + ")"
        End If

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID ='" & ddlGrade.SelectedValue.ToString & "'"
        End If


        If rdSameShift.Checked = True Then
            str_query += "   AND STU_PICKUP_TRP_ID " _
                        & " IN(SELECT TRP_ID FROM TRANSPORT.TRIPS_M WHERE TRP_BSU_ID='" + Session("SBSUID") + "' AND " _
                        & " TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRP_JOURNEY='ONWARD')"
        ElseIf rdDifferentShift.Checked = True Then
            str_query += "   AND STU_PICKUP_TRP_ID " _
                                  & " IN(SELECT TRP_ID FROM TRANSPORT.TRIPS_M WHERE TRP_BSU_ID='" + Session("SBSUID") + "' AND " _
                                  & " TRP_SHF_ID<>" + hfSHF_ID.Value + " AND TRP_JOURNEY='ONWARD')"
        End If


          Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim strArea As String = ""
        Dim txtSearch As New TextBox

        Dim ddlgvBno As New DropDownList

        Dim selectedBno As String = ""

        If gvAllot.Rows.Count > 0 Then


            txtSearch = gvAllot.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,'')", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text


            txtSearch = gvAllot.HeaderRow.FindControl("txtArea")
            strSidsearch = h_Selected_menu_3.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("sbl_description", txtSearch.Text, strSearch)
            strArea = txtSearch.Text



            ddlgvBno = gvAllot.HeaderRow.FindControl("ddlgvBno")
            If ddlgvBno.Text <> "ALL" And ddlgvBno.Text.Trim <> "" Then
                If ddlgvBno.Text = "X" Then
                    strFilter += " and stu_pickup_trp_id is  null"
                Else
                    strFilter += " and bno_descr='" + ddlgvBno.Text + "'"
                End If
            End If
            selectedBno = ddlgvBno.Text

           
            If strFilter <> "" Then
                str_query += strFilter
            End If

        End If

        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dv As DataView = ds.Tables(0).DefaultView

        Dim dt As DataTable
        gvAllot.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ViewState("norecord") = "1"
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            ds.Tables(0).Rows(0)("bPICK") = False
            gvAllot.DataSource = ds.Tables(0)
            gvAllot.DataBind()
            Dim columnCount As Integer = gvAllot.Rows(0).Cells.Count
            gvAllot.Rows(0).Cells.Clear()
            gvAllot.Rows(0).Cells.Add(New TableCell)
            gvAllot.Rows(0).Cells(0).ColumnSpan = columnCount
            gvAllot.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvAllot.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            ViewState("norecord") = "0"
            gvAllot.DataBind()
        End If


        dt = ds.Tables(0)
        If gvAllot.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = strName

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtFeeSearch")
            txtSearch.Text = strFee

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtArea")
            txtSearch.Text = strArea

            ddlgvBno = gvAllot.HeaderRow.FindControl("ddlgvBno")
            

            Dim dr As DataRow

            For Each dr In dt.Rows
                If Not dr.Item(11) Is DBNull.Value Then
                    With dr
                        If ddlgvBno.Items.FindByText(.Item(11)) Is Nothing Then
                            ddlgvBno.Items.Add(.Item(11))
                        End If
                    End With
                End If
            Next

            If ddlgvBno.Items.Count <> 0 Then
                studClass.SortDropDown(ddlgvBno)
            End If

            ddlgvBno.Items.Insert(0, "ALL")
            ddlgvBno.Items.Insert(1, "X")

            If selectedBno <> "" Then
                ddlgvBno.Text = selectedBno
            End If

        End If

      

        set_Menu_Img()
    End Sub
    Sub BindShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SHF_ID,SHF_DESCR FROM VV_SHIFTS_M WHERE SHF_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
    End Sub
    Sub BindBusNo(ByVal ddlBus As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT BNO_DESCR,BNO_ID FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + Session("SBSUID") + "'" _
        '                        & " ORDER BY BNO_DESCR "

        Dim str_query As String = "SELECT DISTINCT BNO_DESCR+ ' - '+ TRP_DESCR AS BNO_DESCR,BNO_ID FROM TRANSPORT.VV_BUSES WHERE TRD_BSU_ID='" + Session("SBSUID") + "'" _
                                & " ORDER BY BNO_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBus.DataSource = ds
        ddlBus.DataTextField = "BNO_DESCR"
        ddlBus.DataValueField = "BNO_ID"
        ddlBus.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlBus.Items.Insert(0, li)
    End Sub

    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        ddlTrip.Items.Clear()

        If ddlBusNo.SelectedValue <> "0" Then
            str_query = "SELECT DISTINCT BNO_DESCR+ ' - '+ TRP_DESCR AS TRP_DESCR,TRP_ID FROM TRANSPORT.VV_BUSES WHERE TRD_BSU_ID='" + Session("SBSUID") + "'" _
                  & " AND TRP_JOURNEY='" + journey + "' AND SHF_ID=" + ddlShift.SelectedValue.ToString _
                  & " AND BNO_ID=" + ddlBusNo.SelectedValue.ToString
        Else
            str_query = "SELECT DISTINCT BNO_DESCR+ ' - '+ TRP_DESCR  AS TRP_DESCR,TRP_ID FROM TRANSPORT.VV_BUSES WHERE TRD_BSU_ID='" + Session("SBSUID") + "'" _
                & " AND TRP_JOURNEY='" + journey + "' AND SHF_ID=" + ddlShift.SelectedValue.ToString

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRP_ID"
        ddlTrip.DataBind()

        If ddlBusNo.SelectedValue = "0" Then
            Dim li As New ListItem
            li.Text = "All"
            li.Value = 0
            ddlTrip.Items.Insert(0, li)
        End If

    End Sub

    Sub SaveData()
        Dim str_query As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i = 0 To gvAllot.Rows.Count - 1
                    chkSelect = gvAllot.Rows(i).FindControl("chkSelect")
                    lblStuId = gvAllot.Rows(i).FindControl("lblStuId")
                    If chkSelect.Checked = True Then
                        str_query = "exec TRANSPORT.saveSTUDBULKTRANSPORTALLOCATION " _
                                  & lblStuId.Text + "," _
                                  & ddlOnward.SelectedValue.ToString + "," _
                                  & ddlReturn.SelectedValue.ToString
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End If
                Next

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
             
                lblError.Text = "Request could not be processed"

                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

#End Region

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Try
            BindBusNo(ddlBusNo)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

   

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            lblError.Text = ""


            tblTpt.Rows(6).Visible = True
            tblTpt.Rows(7).Visible = True
            tblTpt.Rows(8).Visible = True
            tblTpt.Rows(9).Visible = True
            tblTpt.Rows(10).Visible = True
            tblTpt.Rows(11).Visible = True

            hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
            hfSHF_ID.Value = ddlShift.SelectedValue.ToString


            GridBind()

            BindTrip(ddlOnward, "Onward")
            BindTrip(ddlReturn, "Return")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

   
    Protected Sub btnAllot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllot.Click
        Try
            If ddlOnward.SelectedValue = "0" Then
                lblError.Text = "Please select the onward trip"
                Exit Sub
            End If
            If ddlReturn.SelectedValue = "0" Then
                lblError.Text = "Please select the return trip"
                Exit Sub
            End If
            SaveData()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

  

    Protected Sub btnAllot1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllot1.Click
        Try
            If ddlOnward.SelectedValue = "0" Then
                lblError.Text = "Please select the onward trip"
                Exit Sub
            End If
            If ddlReturn.SelectedValue = "0" Then
                lblError.Text = "Please select the return trip"
                Exit Sub
            End If
            SaveData()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

   
    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        Try
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade()
    End Sub
End Class
