Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studNewTransportReq
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100212") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = 0
                    ddlGrade.Items.Insert(0, li)
                    PopulateSection()
                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvStud.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0

                    If ddlClm.Items.Count = 1 Then
                        tbPromote.Rows(0).Visible = False
                    End If

                    'tbPromote.Rows(4).Visible = False
                    tbPromote.Rows(5).Visible = False
                    tbPromote.Rows(6).Visible = False
                    tbPromote.Rows(7).Visible = False
                    tbPromote.Rows(8).Visible = False
                    tbPromote.Rows(9).Visible = False
                    tbPromote.Rows(10).Visible = False
                    tbPromote.Rows(11).Visible = False

                    txtReqDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                    BindBusAreas()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else

            highlight_grid()

        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            PopulateSection()
            If ViewState("norecord") = "1" Then

                tbPromote.Rows(3).Visible = False
                tbPromote.Rows(4).Visible = False
                tbPromote.Rows(5).Visible = False
                tbPromote.Rows(6).Visible = False
                tbPromote.Rows(7).Visible = False
                tbPromote.Rows(8).Visible = False
                tbPromote.Rows(9).Visible = False
                tbPromote.Rows(10).Visible = False
                tbPromote.Rows(11).Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim i As Integer
            Dim ddlTrips As DropDownList
            lblError.Text = ""


            tbPromote.Rows(4).Visible = True
            tbPromote.Rows(5).Visible = True
            tbPromote.Rows(6).Visible = True
            tbPromote.Rows(7).Visible = True
            tbPromote.Rows(8).Visible = True
            tbPromote.Rows(9).Visible = True
            tbPromote.Rows(10).Visible = True
            tbPromote.Rows(11).Visible = True
            hfACD_ID.Value = ddlAcademicYear.SelectedValue
            GridBind()
            For i = 0 To gvStud.Rows.Count - 1
                ddlTrips = gvStud.Rows(i).FindControl("ddlTrips")
                If Not ddlTrips Is Nothing AndAlso Not (ddlTrips.Items.FindByText("RoundTrip")) Is Nothing Then
                    ddlTrips.Items.FindByText("RoundTrip").Selected = True
                End If


            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "PrivateMethods"

    Sub BindBusAreas()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String = "SELECT BNO_DESCR,AREA=STUFF((SELECT  distinct  ', ' + SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M  AS E" _
                                    & "  INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS D ON E.SBL_ID=D.TPP_SBL_ID " _
                                    & " WHERE TPP_TRP_ID=A.TRP_ID  ORDER BY ', ' + SBL_DESCRIPTION for xml path('')),1,1,'') FROM TRANSPORT.VV_BUSES AS A" _
                                    & " WHERE TRD_BSU_ID='" + Session("sbsuid") + "' AND TRP_JOURNEY='ONWARD' ORDER BY BNO_DESCR"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvAreas.DataSource = ds
            gvAreas.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Function GetPromotedYear() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " select top 1 acd_id from academicyear_d where acd_startdt>" _
                                & "(select acd_startdt from academicyear_d where acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                & " and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + ") and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + " order by acd_startdt"
        Dim acdid As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return acdid
    End Function

    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        If ddlGrade.SelectedValue = "0" Then
            ddlSection.Items.Insert(0, li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub highlight_grid()
        For i As Integer = 0 To gvStud.Rows.Count - 1
            Dim row As GridViewRow = gvStud.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT STU_ID ,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')),STU_SHF_ID  , replace(CONVERT(varchar,Convert(dateTime,STU_DOJ) ,106),' ','/')  as STU_FROM_DOJ" _
                                 & " FROM STUDENT_M WHERE  STU_PICKUP IS NULL AND  STU_SBL_ID_PICKUP IS NULL  " _
                                 & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                                 & " AND STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND STU_ID  NOT IN(SELECT SNR_STU_ID FROM STUDENT_TRANSPORTREQ_S WHERE SNR_bAPPROVED IS NULL)"


        If ddlGrade.SelectedValue <> "0" Then
            str_query += "AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If
        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString + ""
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""
        ViewState("rowIndex") = 0

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter = " AND " + GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            End If
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += " AND " + GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If
        End If
        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds


        If ds.Tables(0).Rows.Count = 0 Then
            ViewState("norecord") = "1"
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            ViewState("norecord") = "0"
            gvStud.DataBind()
        End If

        If gvStud.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtFeeSearch")
            txtSearch.Text = strFee

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = strName
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim i As Integer
        Dim str_query As String
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim lblStuNo As Label
        Dim ddlTrips As DropDownList
        Dim ddlPickUp As DropDownList
        Dim ddlDropOff As DropDownList
        Dim ddlDArea As DropDownList
        Dim ddlPArea As DropDownList
        Dim ddlDTrip As DropDownList
        Dim ddlPTrip As DropDownList
        Dim cselect As Boolean = False


        Dim stuNosSaved As String = ""

        Dim stuNosNotSaved As String = ""
        Dim txtDate As TextBox
        For i = 0 To gvStud.Rows.Count - 1
            Dim PArea As Integer = 0
            Dim DArea As Integer = 0
            Dim DTrip As Integer = 0
            Dim PTrip As Integer = 0
            Dim PickUp As Integer = 0
            Dim DropOff As Integer = 0
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            lblStuId = gvStud.Rows(i).FindControl("lblStuId")
            ddlPickUp = gvStud.Rows(i).FindControl("ddlPickUp")
            ddlDropOff = gvStud.Rows(i).FindControl("ddlDropOff")
            ddlDArea = gvStud.Rows(i).FindControl("ddlDArea")
            ddlPArea = gvStud.Rows(i).FindControl("ddlPArea")
            ddlDTrip = gvStud.Rows(i).FindControl("ddlDTrip")
            ddlPTrip = gvStud.Rows(i).FindControl("ddlPTrip")
            txtDate = gvStud.Rows(i).FindControl("txtDate")
            lblStuNo = gvStud.Rows(i).FindControl("lblFeeId")
            ddlTrips = gvStud.Rows(i).FindControl("ddlTrips")

            '   Dim pArea As String() = ddlPArea.SelectedValue.Split("|")
            ' Dim dArea As String() = ddlDArea.SelectedValue.Split("|")
            If chkSelect.Checked = True Then
                'If ddlPickUp.Items.Count <> 0 And ddlDropOff.Items.Count <> 0 Then
                ' If ddlPickUp.SelectedValue <> "0" And ddlDropOff.SelectedValue <> "0" Then
                If ddlTrips.SelectedItem.Value = "0" Then    'Round Trip
                    If ddlPickUp.Items.Count <> 0 And ddlDropOff.Items.Count <> 0 Then
                        If ddlPickUp.SelectedValue <> "0" And ddlDropOff.SelectedValue <> "0" Then
                            cselect = True
                            PArea = ddlPArea.SelectedItem.Value
                            DArea = ddlDArea.SelectedItem.Value
                            DTrip = ddlDTrip.SelectedItem.Value
                            PTrip = ddlPTrip.SelectedItem.Value
                            PickUp = ddlPickUp.SelectedItem.Value
                            DropOff = ddlDropOff.SelectedItem.Value
                        End If
                    End If
                ElseIf ddlTrips.SelectedItem.Value = "1" Then   'Pick Up
                    If ddlPickUp.Items.Count <> 0 And ddlPickUp.SelectedValue <> "0" Then
                        cselect = True
                        PArea = ddlPArea.SelectedItem.Value
                        PTrip = ddlPTrip.SelectedItem.Value
                        PickUp = ddlPickUp.SelectedItem.Value
                        DArea = ddlPArea.SelectedItem.Value
                        DTrip = ddlPTrip.SelectedItem.Value
                        DropOff = ddlPickUp.SelectedItem.Value
                    End If
                ElseIf ddlTrips.SelectedItem.Value = "2" Then   'Drop Off
                    If ddlDropOff.Items.Count <> 0 And ddlDropOff.SelectedValue <> "0" Then
                        cselect = True
                        DArea = ddlDArea.SelectedItem.Value
                        DTrip = ddlDTrip.SelectedItem.Value
                        DropOff = ddlDropOff.SelectedItem.Value
                        PArea = ddlDArea.SelectedItem.Value
                        PTrip = ddlDTrip.SelectedItem.Value
                        PickUp = ddlDropOff.SelectedItem.Value
                    End If
                End If

                If cselect = True Then
                    If studClass.checkFeeClosingDate(Session("sbsuid"), ddlAcademicYear.SelectedValue, Date.Parse(txtDate.Text)) = True Then

                        'str_query = " exec [TRANSPORT].[saveSTUDNEWTRANSPORTREQUEST] " _
                        '           & lblStuId.Text + "," _
                        '           & ddlPArea.SelectedValue.ToString + "," _
                        '           & ddlPickUp.SelectedValue.ToString + "," _
                        '           & ddlDArea.SelectedValue.ToString + "," _
                        '           & ddlDropOff.SelectedValue.ToString + "," _
                        '           & "'" + txtDate.Text + "'," _
                        '           & "'" + txtReqDate.Text + "'," _
                        '           & ddlPTrip.SelectedValue.ToString + "," _
                        '           & ddlDTrip.SelectedValue.ToString + "," _
                        '           & "'" + Session("sUsr_name") + "'"


                        str_query = " exec [TRANSPORT].[saveSTUDNEWTRANSPORTREQ] " _
                                   & lblStuId.Text + "," _
                                   & PArea.ToString() + "," _
                                   & PickUp.ToString() + "," _
                                   & DArea.ToString() + "," _
                                   & DropOff.ToString() + "," _
                                   & "'" + txtDate.Text + "'," _
                                   & "'" + txtReqDate.Text + "'," _
                                   & PTrip.ToString() + "," _
                                   & DTrip.ToString() + "," _
                                   & "'" + Session("sUsr_name") + "'," _
                                   & "" + ddlTrips.SelectedItem.Value + ""


                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                        If stuNosSaved <> "" Then
                            stuNosSaved += ","
                        End If
                        stuNosSaved += lblStuId.Text

                    Else
                        If stuNosNotSaved <> "" Then
                            stuNosNotSaved += ","
                        End If
                        stuNosNotSaved += lblStuNo.Text
                    End If
                End If
                'End If
            End If
        Next

        If stuNosSaved <> "" Then
            CallReport(stuNosSaved)
        End If

        If stuNosNotSaved <> "" Then
            lblError.Text = "The following records " + stuNosNotSaved + " could not be saved as the service facility starting date is within the Fee Closing date"
        ElseIf cselect = True Then
            lblError.Text = "Record saved successfully"
        Else
            lblError.Text = "No records selected"
        End If
    End Sub

    Sub CallReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptTransportReqInfo.rpt")
            '.reportPath = Server.MapPath("../Transport/Reports/RPT/rptTransportReqInformation.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function


    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

    Sub BindSublocation(ByVal ddlSublocation As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        '    Dim str_query As String = "SELECT SBL_ID=convert(varchar(100),SBL_ID)+'|" + ViewState("rowIndex").ToString + "',SBL_DESCRIPTION FROM TRANSPORT.VV_SUBLOCATION_M WHERE SBL_BSU_ID='" + Session("SBSUID") + "'"
        Dim str_query As String = "SELECT DISTINCT A.SBL_ID AS SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A " _
                                 & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                                                 & " WHERE PNT_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " ORDER BY SBL_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSublocation.DataSource = ds
        ddlSublocation.DataTextField = "SBL_DESCRIPTION"
        ddlSublocation.DataValueField = "SBL_ID"
        ddlSublocation.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSublocation.Items.Insert(0, li)
        ds = Nothing
    End Sub


    'Sub BindPickup(ByVal ddlPickup As DropDownList, ByVal sblId As String)
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.VV_PICKUPOINTS_M WHERE PNT_SBL_ID=" + sblId _
    '                             & " AND PNT_BSU_ID='"+SESSION("SBSUID")+"'"

    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlPickup.DataSource = ds
    '    ddlPickup.DataTextField = "PNT_DESCRIPTION"
    '    ddlPickup.DataValueField = "PNT_ID"
    '    ddlPickup.DataBind()
    '    Dim li As New ListItem
    '    li.Text = "--"
    '    li.Value = "0"
    '    ddlPickup.Items.Insert(0, li)
    '    ds = Nothing

    '    li = New ListItem

    '    For Each li In ddlPickup.Items
    '        li.Attributes.Add("title", li.Text)
    '    Next
    'End Sub


    Sub BindPickup(ByVal ddlPickup As DropDownList, ByVal trdId As String, ByVal sblid As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M AS A  " _
                                & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.PNT_ID=B.TPP_PNT_ID AND PNT_SBL_ID=" + sblid _
                                & " WHERE TPP_TRD_ID = " + trdId _
                                & " ORDER BY PNT_DESCRIPTION"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPickup.DataSource = ds
        ddlPickup.DataTextField = "PNT_DESCRIPTION"
        ddlPickup.DataValueField = "PNT_ID"
        ddlPickup.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlPickup.Items.Insert(0, li)
        ds = Nothing

        li = New ListItem

        For Each li In ddlPickup.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub

    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String, ByVal shift As String, ByVal sblid As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_SBL_ID=" + sblid _
                        & " AND A.TRP_SHF_ID=" + shift + " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_SBL_ID=" + sblid _
                 & " AND A.TRP_SHF_ID=" + shift + " AND TRD_TODATE IS NULL"



        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"

        ddlTrip.Items.Insert(0, li)

        For Each li In ddlTrip.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Function CheckDates() As Boolean
        Dim flag As Boolean = True
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim i As Integer
        Dim txtDate As TextBox
        Dim lblErr As Label
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim lstrValid As String
        For i = 0 To gvStud.Rows.Count - 1
            chkSelect = gvStud.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                txtDate = gvStud.Rows(i).FindControl("txtDate")
                lblStuId = gvStud.Rows(i).FindControl("lblStuId")
                lblErr = gvStud.Rows(i).FindControl("lblErr")
                If txtDate.Text = "" Then
                    lblErr.Visible = True
                    flag = False
                ElseIf IsDate(txtDate.Text) = False Then
                    lblErr.Visible = True
                    flag = False
                ElseIf txtDate.Text <> "" Then
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STU_ID", lblStuId.Text)
                    pParms(1) = New SqlClient.SqlParameter("@TPT_START", Format(Date.Parse(txtDate.Text), "dd-MMM-yyyy"))
                    Using readerStudent As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[GET_VALID_TPTREQ]", pParms)
                        If readerStudent.HasRows = True Then
                            While readerStudent.Read
                                lstrValid = Convert.ToString(readerStudent("STU_VALID"))
                                If lstrValid <> "0" Then
                                    lblErr.Visible = True
                                    flag = False
                                End If
                            End While
                        End If
                    End Using
                End If
            End If
        Next

        Return flag

    End Function
#End Region

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        'Try
        '    If e.Row.RowType = DataControlRowType.DataRow Then
        '        Dim ddlPArea As DropDownList
        '        Dim ddlDArea As DropDownList
        '        Dim ddlPickUp As DropDownList
        '        Dim ddlDropOff As DropDownList



        '        With e.Row


        '            ddlPArea = .FindControl("ddlPArea")
        '            ddlDArea = .FindControl("ddlDArea")
        '            ddlPickUp = .FindControl("ddlPickUp")
        '            ddlDropOff = .FindControl("ddlDropOff")


        '            BindSublocation(ddlPArea)
        '            BindSublocation(ddlDArea)

        '            Dim parea As String() = ddlPArea.SelectedValue.Split("|")

        '            BindPickup(ddlPickUp, parea(0))

        '            Dim darea As String() = ddlDArea.SelectedValue.Split("|")

        '            BindPickup(ddlDropOff, darea(0))

        '            ViewState("rowIndex") += 1
        '        End With
        '    End If
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    lblError.Text = "Request could not be processed"
        'End Try

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlPArea As DropDownList
                Dim ddlDArea As DropDownList
                Dim ddlPickUp As DropDownList
                Dim ddlDropOff As DropDownList
                Dim ddlPTrip As DropDownList
                Dim ddlDTrip As DropDownList

                Dim lblShfId As Label

                With e.Row


                    ddlPArea = .FindControl("ddlPArea")
                    ddlDArea = .FindControl("ddlDArea")
                    ddlPTrip = .FindControl("ddlPTrip")
                    ddlDTrip = .FindControl("ddlDTrip")
                    lblShfId = .FindControl("lblShfId")
                    ' ddlPickUp = .FindControl("ddlPickUp")
                    ' ddlDropOff = .FindControl("ddlDropOff")

                    If lblShfId.Text <> "" Then
                        BindSublocation(ddlPArea)
                        BindSublocation(ddlDArea)

                        'Dim parea As String() = ddlPArea.SelectedValue.Split("|")
                        BindTrip(ddlPTrip, "Onward", lblShfId.Text, ddlPArea.SelectedValue.ToString)

                        'BindTrip(
                        '  BindPickup(ddlPickUp, parea(0))

                        '  Dim darea As String() = ddlDArea.SelectedValue.Split("|")
                        BindTrip(ddlDTrip, "Return", lblShfId.Text, ddlDArea.SelectedValue.ToString)

                        '  BindPickup(ddlDropOff, darea(0))
                    End If
                    ViewState("rowIndex") += 1
                End With
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    'Protected Sub ddlPArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        lblError.Text = ""
    '        Dim chkSelect As CheckBox
    '        Dim ddlPsbl As DropDownList = DirectCast(sender, DropDownList)

    '        Dim parea As String() = ddlPsbl.SelectedValue.Split("|")

    '        Dim ddlPickUp As DropDownList
    '        Dim ddlDSbl As DropDownList
    '        Dim ddlDropOff As DropDownList

    '        ddlPickUp = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlPickUp")
    '        ddlDSbl = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlDArea")
    '        ddlDSbl.ClearSelection()
    '        ddlDSbl.Items.FindByValue(ddlPsbl.SelectedValue).Selected = True
    '        Dim darea As String() = ddlDSbl.SelectedValue.Split("|")
    '        ddlDropOff = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlDropOff")
    '        BindPickup(ddlPickUp, parea(0))
    '        BindPickup(ddlDropOff, darea(0))
    '        chkSelect = gvStud.Rows(CType(parea(1), Integer)).FindControl("chkSelect")
    '        chkSelect.Checked = True
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub

    'Protected Sub ddlDArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        lblError.Text = ""
    '        Dim ddlDsbl As DropDownList = DirectCast(sender, DropDownList)

    '        Dim darea As String() = ddlDsbl.SelectedValue.Split("|")

    '        Dim ddlDropOff As DropDownList

    '        ddlDropOff = gvStud.Rows(CType(darea(1), Integer)).FindControl("ddlDropOff")
    '        BindPickup(ddlDropOff, darea(0))
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub


    Protected Sub ddlPArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblError.Text = ""
            Dim chkSelect As CheckBox
            Dim ddlPsbl As DropDownList = DirectCast(sender, DropDownList)

            '  Dim parea As String() = ddlPsbl.SelectedValue.Split("|")

            ' Dim ddlPickUp As DropDownList
            Dim ddlDSbl As DropDownList
            ' Dim ddlDropOff As DropDownList

            Dim ddlPTrip As DropDownList
            Dim ddlDTrip As DropDownList

            Dim lblShfId As Label

            'ddlPickUp = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlPickUp")
            ddlPTrip = TryCast(sender.FindControl("ddlPTrip"), DropDownList)
            ddlDTrip = TryCast(sender.FindControl("ddlDtrip"), DropDownList)
            ddlDSbl = TryCast(sender.FindControl("ddlDArea"), DropDownList)
            lblShfId = TryCast(sender.FindControl("lblShfId"), Label)


            ddlDSbl.ClearSelection()
            ddlDSbl.Items.FindByValue(ddlPsbl.SelectedValue).Selected = True
            ' Dim darea As String() = ddlDSbl.SelectedValue.Split("|")
            ' ddlDropOff = gvStud.Rows(CType(parea(1), Integer)).FindControl("ddlDropOff")
            '  BindPickup(ddlPickUp, parea(0))
            ' BindPickup(ddlDropOff, darea(0))

            BindTrip(ddlPTrip, "Onward", lblShfId.Text, ddlPsbl.SelectedValue.ToString)
            BindTrip(ddlDTrip, "Return", lblShfId.Text, ddlDSbl.SelectedValue.ToString)

            chkSelect = TryCast(sender.FindControl("chkSelect"), CheckBox)
            chkSelect.Checked = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlDArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblError.Text = ""
            Dim ddlDsbl As DropDownList = DirectCast(sender, DropDownList)
            Dim lblShfId As Label
            Dim ddlDTrip As DropDownList


            lblShfId = TryCast(sender.FindControl("lblShfId"), Label)
            ddlDTrip = TryCast(sender.FindControl("ddlDTrip"), DropDownList)
            BindTrip(ddlDTrip, "Return", lblShfId.Text, ddlDsbl.SelectedValue.ToString)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlPTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlOnward As DropDownList = DirectCast(sender, DropDownList)
        Dim ddlPickup As DropDownList
        Dim ddlPArea As DropDownList
        ddlPickup = TryCast(sender.FindControl("ddlPickup"), DropDownList)
        ddlPArea = TryCast(sender.FindControl("ddlPArea"), DropDownList)
        BindPickup(ddlPickup, ddlOnward.SelectedValue.ToString, ddlPArea.SelectedValue.ToString)
    End Sub


    Protected Sub ddlDTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlReturn As DropDownList = DirectCast(sender, DropDownList)
        Dim ddlDropoff As DropDownList
        Dim ddlDArea As DropDownList

        ddlDropoff = TryCast(sender.FindControl("ddlDropOff"), DropDownList)
        'ddlDArea = TryCast(sender.FindControl("ddlPArea"), DropDownList)
        ddlDArea = TryCast(sender.FindControl("ddlDArea"), DropDownList)
        BindPickup(ddlDropoff, ddlReturn.SelectedValue.ToString, ddlDArea.SelectedValue.ToString)
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If CheckDates() = True Then
                SaveData()
                GridBind()

            Else
                lblError.Text = "Please enter a valid date in the fields marked with *"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Dim i As Integer
            Dim chkSelect As CheckBox
            Dim txtDate As TextBox
            Dim ddlTrips As DropDownList
            'Dim lblTrip As Label
            'Dim lblTripVal As Label
            For i = 0 To gvStud.Rows.Count - 1
                chkSelect = gvStud.Rows(i).FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    txtDate = gvStud.Rows(i).FindControl("txtDate")
                    txtDate.Text = txtFrom.Text
                    ddlTrips = gvStud.Rows(i).FindControl("ddlTrips")
                    ddlTrips.SelectedIndex = -1
                    ddlTrips.Items.FindByText(ddlTripType.SelectedItem.Text).Selected = True
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
            Dim li As New ListItem
            li.Text = "All"
            li.Value = 0
            ddlGrade.Items.Insert(0, li)
            PopulateSection()
            If ViewState("norecord") = "1" Then
                'tbPromote.Rows(4).Visible = False
                tbPromote.Rows(5).Visible = False
                tbPromote.Rows(6).Visible = False
                tbPromote.Rows(7).Visible = False
                tbPromote.Rows(8).Visible = False
                tbPromote.Rows(9).Visible = False
                tbPromote.Rows(10).Visible = False
                tbPromote.Rows(11).Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnUpdate1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate1.Click
        Try
            If CheckDates() = True Then
                SaveData()
                GridBind()

            Else
                lblError.Text = "Please enter a valid date in the fields marked with *"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        Try
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
            Dim li As New ListItem
            li.Text = "All"
            li.Value = 0
            ddlGrade.Items.Insert(0, li)
            PopulateSection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
