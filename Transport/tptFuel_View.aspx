<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptFuel_View.aspx.vb" Inherits="Transport_tptFuel_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">

                
function switchViews(obj,row)
        {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            
            if (div.style.display=="none")
                {
                    div.style.display = "inline";
                    if (row=='alt')
                       {
                           img.src="../Images/expand_button_white_alt_down.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_Button_white_Down.jpg" ;
                       }
                   img.alt = "Click to close";
               }
           else
               {
                   div.style.display = "none";
                   if (row=='alt')
                       {
                           img.src="../Images/Expand_button_white_alt.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_button_white.jpg" ;
                       }
                   img.alt = "Click to expand";
               }
       }

</script>


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Fuel Entry
        </div>
        <div class="card-body">
            <div class="table-responsive">


<table id="tbl_ShowScreen" runat="server" align="center" width="100%" >
            
            <tr >    <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    HeaderText="You must enter a value in the following fields:"   ValidationGroup="groupM1" />
                    
         </td>  
                        </tr>
               
               
      
        
           <tr>
      
            <td align="center" width="100%">
            
  <table id="Table1" runat="server" align="center" width="100%" >
         
              
         <tr><td width="100%">
          <table id="Table3" runat="server" align="center" width="100%" >
                    <tr>
            
               <td align="left" width="20%">
                    <asp:Label ID="Label2" runat="server" CssClass="field-label" Text="Fill Date From"></asp:Label></td>
                    
                 <td align="left" width="30%" >
                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                         <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                           /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom"
                            Display="Dynamic" ErrorMessage="Enter the Trip Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtFrom"
                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date"
                            ValidationGroup="groupM1">*</asp:CustomValidator>
                     <asp:RequiredFieldValidator ID="rfFrom" runat="server" ErrorMessage="Please enter the field trip date from" ControlToValidate="txtFrom" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>        
                                      </td>
                      
                        
                              <td align="left" width="20%" >
                    <asp:Label ID="Label3" runat="server" Text="To" CssClass="field-label"></asp:Label></td>
                    
                 <td align="left" width="30%">
                 <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                         <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTo"
                            Display="Dynamic" ErrorMessage="Enter the Trip Date To in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtTo"
                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date To entered is not a valid date"
                            ValidationGroup="groupM1">*</asp:CustomValidator>
                     <asp:RequiredFieldValidator ID="rfTo" runat="server" ErrorMessage="Please enter the field trip date to" ControlToValidate="txtTo" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>   
                 </td>     
                </tr>
              <tr>
                   <td align="center" colspan="4" >             
                <asp:Button id="btnSearch" runat="server" Text="Search" CssClass="button" ValidationGroup="groupM1" ></asp:Button>
                </td>
             </tr>
                    </table>
         
         </td></tr>     
          
         <tr class="subheader_img">
         <td align="left" colspan="4" class="title-bg">
            Details</td>
         </tr>                       
                    
        <tr><td  colspan="4" align="center" width="100%">
         
         <table id="Table2" runat="server" align="center" width="100%" >
          
            <tr><td colspan="4" align="center" width="100%">
           <asp:GridView ID="gvTptTrip" runat="server" CssClass="table table-bordered table-row"
                    AutoGenerateColumns="False" OnRowDataBound="gvTptTrip_RowDataBound" PageSize="20"  AllowPaging="True" Width="100%" >
           <Columns>
           <asp:TemplateField>
           <ItemTemplate>
              <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide "  src="../Images/expand_button_white.jpg" />
               </a>
            </ItemTemplate>
            <AlternatingItemTemplate>
             <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
              <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide "  src="../Images/expand_button_white_alt.jpg" />
              </a>
            </AlternatingItemTemplate>
           </asp:TemplateField>
                 
                    <asp:TemplateField HeaderText="veh_id" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblvehId" runat="server" Text='<%# Bind("veh_id") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
         
                       <asp:TemplateField HeaderText="Trips">
                             <HeaderTemplate>                        
                            Vehicle<br />
                         <asp:DropDownList ID="ddlgvVehicle" runat="server" AutoPostBack="True" CssClass="listbox"
                          OnSelectedIndexChanged="ddlgvVehicle_SelectedIndexChanged" Width="120px">
                          </asp:DropDownList>
                         </HeaderTemplate>
                       <ItemTemplate>
                      <asp:Label ID="lblVehicle" runat="server" text='<%# Bind("VEH_REGNO") %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                                    
           <asp:ButtonField CommandName="Add" HeaderText="Add" Text="Add"  >
           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
           <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
           </asp:ButtonField>  
           
          <asp:TemplateField  >
           <ItemTemplate>
            </td></tr>
             <tr >
            <td colspan="100%" align="left">
           
            <div id="div<%# Eval("GUID") %>" style="display:none;position:relative;left:20px;" >
           <asp:GridView ID="gvDetails" runat="server" Width="95%"
           AutoGenerateColumns="false" EmptyDataText="" OnRowCommand="gvDetails_RowCommand" OnRowDeleting="gvDetails_RowDeleting" >
            <Columns>
            
            
                <asp:TemplateField HeaderText="trv_id" Visible="False">
                <ItemStyle HorizontalAlign="Left" />
                 <ItemTemplate>
                 <asp:Label ID="lblTflId" runat="server" Text='<%# Bind("TFL_ID") %>'></asp:Label>
                 </ItemTemplate>
                  </asp:TemplateField>
                       
            
                 <asp:TemplateField HeaderText="veh_id" Visible="False">
                 <ItemStyle HorizontalAlign="Left" />
                  <ItemTemplate>
                  <asp:Label ID="lbltflvehId" runat="server" Text='<%# Bind("TFL_VEH_ID") %>'></asp:Label>
                 </ItemTemplate>
                 </asp:TemplateField>
                 
                  <asp:TemplateField HeaderText="vehicle" Visible="False">
                 <ItemStyle HorizontalAlign="Left" />
                  <ItemTemplate>
                  <asp:Label ID="lblveh" runat="server"   text='<%# Bind("VEH_REGNO") %>'></asp:Label>
                 </ItemTemplate>
                 </asp:TemplateField>
                 
                       
               <asp:TemplateField HeaderText="Fill Date" Visible="False">
               <ItemStyle HorizontalAlign="Left" />
               <ItemTemplate>
               <asp:Label ID="lblFillDate" runat="server" Text='<%# Bind("TFL_FILLDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
                  
              <asp:BoundField DataField="TFL_PREVKM" HeaderText="Previous Km. Reading"  HtmlEncode="False" ControlStyle-Width="70px" >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField> 
           
           <asp:BoundField DataField="TFL_CURRENTKM" HeaderText="Current Km. Reading"  HtmlEncode="False" >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField>  
           <asp:BoundField DataField="TFL_GALLON" HeaderText="No.Of Gallons"  HtmlEncode="False" >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField>     
            <asp:BoundField DataField="TFL_AMOUNT" HeaderText="Amount(AED)"  HtmlEncode="False" >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField>
          
           
              <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="delete"  >
           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
           <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
           </asp:ButtonField>
           
           </Columns>
          <RowStyle CssClass="griditem" Height="20px" Font-Names="Verdana"  /> 
           <HeaderStyle CssClass="gridheader_pop" Height="25px" /> 
         </asp:GridView>
          </div>
          </td>
           </tr>
          </ItemTemplate>
         </asp:TemplateField>
         
        
              </Columns>
                   <RowStyle CssClass="griditem" Height="20px" Font-Names="Verdana"  />
                            <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                            <SelectedRowStyle BackColor="Aqua" />
                            <PagerStyle Font-Names="Verdana" Font-Size="7pt" HorizontalAlign="Left"  />                        
               </asp:GridView>   
        </td></tr>
        </table>
                   
           </td>
           </tr>
           </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom" PopupPosition="TopRight">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtTo" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo" PopupPosition="TopRight">
                </ajaxToolkit:CalendarExtender>
               
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="hfTripDate" runat="server" type="hidden"  />
               
                  
                </td></tr>
        
        </table>


    
    </div>
      </div>
        </div>

</asp:Content>

