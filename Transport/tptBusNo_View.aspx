<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptBusNo_View.aspx.vb" Inherits="Transport_tptBusNo_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Bus No Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center" width="100%">
            
            <tr>    <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                        </tr>
               
               
      
        
           <tr>
      
            <td align="center">
            
  <table id="Table1" runat="server" align="center" width="100%">
           
                                
                    
        <tr><td align="center" >
         
         <table id="Table2" runat="server" align="center"  width="100%">
          
           <tr><td align="left" >          
            <asp:LinkButton ID="lbAddNew" runat="server" >Add New</asp:LinkButton>
            </td></tr>
            
            <tr><td colspan="3" align="center">
                      <asp:GridView ID="gvTptBusNo" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     PageSize="20" Width="100%">
                     <Columns>
                    
                      <asp:TemplateField HeaderText="bno" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblBusId" runat="server" Text='<%# Bind("bno_id") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                         <asp:TemplateField HeaderText="Bus Description">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblBusDescr" runat="server" Text='<%# Bind("bno_descr") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                      
                     
                       <asp:ButtonField CommandName="View" HeaderText="View" Text="View"  >
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                     
                       </Columns>  
                     <HeaderStyle  Wrap="False" />
                          <RowStyle Wrap="False" />
                          <SelectedRowStyle  Wrap="False" />
                          <AlternatingRowStyle Wrap="False" />
                          <EmptyDataRowStyle Wrap="False" />
                          <EditRowStyle Wrap="False" />
                     </asp:GridView>
                   </td></tr>
                   </table>
           </td>
           </tr>
           </table>
             </td></tr>
          
        </table>

            </div>
        </div>
    </div>
 
</asp:Content>

