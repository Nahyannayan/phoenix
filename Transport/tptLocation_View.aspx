<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptLocation_View.aspx.vb" Inherits="Transport_tptLocation_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Location Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center" width="100%">
            
            <tr>    <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                        </tr>
           
           <tr>
      
            <td align="center">
            
  <table id="Table1" runat="server" align="center" width="100%" >
                             
                                
                    
        <tr><td align="center" >
         
         <table id="Table2" runat="server" align="center" width="100%" >
          
           <tr><td  colspan="3" align="left" >          
            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
            </td></tr>
            
            <tr><td colspan="3" align="center">
                      <asp:GridView ID="gvTptLocation" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     PageSize="20" Width="100%"  BorderStyle="None">
                     <Columns>
                      <asp:TemplateField HeaderText="grd_id" Visible="False">
                    
                      <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblLocId" runat="server" Text='<%# Bind("loc_id") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                 <asp:TemplateField HeaderText="Location Code">
                                <HeaderTemplate>
                                 <asp:Label ID="lblLocCode" runat="server" Text="Location Code"></asp:Label><br />
                                 <asp:TextBox ID="txtCode" runat="server" Width="75%"></asp:TextBox>
                                 <asp:ImageButton ID="btnCode_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnCode_Search_Click" />
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblCode" runat="server" text='<%# Bind("loc_code") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                    <asp:TemplateField HeaderText="Location">
                                <HeaderTemplate>
                                <asp:Label ID="lblLoc" runat="server" CssClass="gridheader_text" Text="Location"></asp:Label><br />
                                <asp:TextBox ID="txtLocation" runat="server" Width="75%"></asp:TextBox>
                               <asp:ImageButton ID="btnLocation_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnLocation_Search_Click" />
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblLocation" runat="server" text='<%# Bind("loc_description") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                                 
                     
                       <asp:ButtonField CommandName="View" HeaderText="View" Text="View"  >
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                     
                       </Columns>  
                     <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                          <RowStyle CssClass="griditem"  Wrap="False" />
                          <SelectedRowStyle CssClass="Green" Wrap="False" />
                          <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                          <EmptyDataRowStyle Wrap="False" />
                          <EditRowStyle Wrap="False" />
                     </asp:GridView>
                   </td></tr>
                   </table>
                       <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
           </td>
           </tr>
           </table>
           </td></tr>
          
        </table>
        
                </div>
            </div>
         </div>

</asp:Content>

