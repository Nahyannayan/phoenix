Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_tptEmpTransportAlloc_New
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")

            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                Try
                    Dim str_sql As String = ""
                    Dim CurBsUnit As String = Session("sBsuid")
                    Dim USR_NAME As String = Session("sUsr_name")

                    'collect the url of the file to be redirected in view state

                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If

                    'get the data mode from the query string to check if in add or edit mode 
                    ViewState("EMPID") = Encr_decrData.Decrypt(Request.QueryString("EmpId").Replace(" ", "+"))
                    'get the menucode to confirm the user is accessing the valid page
                    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                    'check for the usr_name and the menucode are valid otherwise redirect to login page
                    'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100190") Then
                    '    If Not Request.UrlReferrer Is Nothing Then
                    '        Response.Redirect(Request.UrlReferrer.ToString())
                    '    Else

                    '        Response.Redirect("~\noAccess.aspx")
                    '    End If
                    'Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    lblReturn.Text = ""
                    Panel1.Visible = False
                    TitlePanel.Visible = False
                    ViewState("return") = "0"
                    If IsTransportAlloted(ViewState("EMPID")) = True Then
                        ViewState("datamode") = "edit"
                        HFDID.Value = ViewState("EMPID")
                        BindSublocation(ddlArea)
                        GetTripOnward()
                        DispLayEmployeeDetails(ViewState("EMPID"))
                        DisplayTransportDet(ViewState("EMPID"))
                        btnDelete.Enabled = True
                    Else
                        ViewState("datamode") = "add"
                        btnDelete.Enabled = False
                        HFDID.Value = "0"
                        HIDAllocOwd.Value = "0"
                        HidAllocRtn.Value = "0"
                        DispLayEmployeeDetails(ViewState("EMPID"))
                        BindSublocation(ddlArea)
                    End If

                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    lblError.Text = "Request could not be processed"
                End Try
            Else
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Function isPageExpired()
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSublocation(ByVal DDlArea As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION " _
                                 & " FROM TRANSPORT.SUBLOCATION_M AS A " _
                                 & "  INNER JOIN TRANSPORT.PICKUPPOINTS_M  AS B ON A.SBL_ID=B.PNT_SBL_ID" _
                                 & " WHERE PNT_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DDlArea.DataSource = ds
        DDlArea.DataTextField = "SBL_DESCRIPTION"
        DDlArea.DataValueField = "SBL_ID"
        DDlArea.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        DDlArea.Items.Insert(0, li)


    End Sub
    Sub BindPickup(ByVal ddlpickup As DropDownList, ByVal ddlAreas As DropDownList)

        Dim li As New ListItem
        If Not ddlArea.SelectedValue = 0 Then
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M WHERE PNT_SBL_ID=" + ddlAreas.SelectedValue.ToString + " AND PNT_BSU_ID='" + Session("sBsuid") + "'"


            ddlpickup.Items.Clear()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlpickup.DataSource = ds
            ddlpickup.DataTextField = "PNT_DESCRIPTION"
            ddlpickup.DataValueField = "PNT_ID"
            ddlpickup.DataBind()

            li.Text = "ALL"
            li.Value = "0"
            ddlpickup.Items.Insert(0, li)
        Else
            li.Text = "ALL"
            li.Value = "0"
            ddlpickup.Items.Add(li)
        End If
    End Sub
    Sub BindTripWIsePickup(ByVal ddlpickup As DropDownList, ByVal ddlTrip As DropDownList)

        Dim li As New ListItem
        If Not ddlArea.SelectedValue = 0 Then
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M  WHERE PNT_ID IN " & _
                                      "(SELECT TPP_PNT_ID FROM TRANSPORT.TRIPS_PICKUP_S WHERE TPP_ID=" & ddlTrip.SelectedValue.ToString & ")"

            str_query = "SELECT DISTINCT PNT_ID,PNT_DESCRIPTION FROM  TRANSPORT.PICKUPPOINTS_M AS B " & _
                       "INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS A ON B.PNT_ID = A.TPP_PNT_ID " & _
                       "INNER JOIN TRANSPORT.SUBLOCATION_M AS C ON B.PNT_SBL_ID=C.SBL_ID " & _
                       " WHERE A.TPP_TRP_ID = " & ddlTrip.SelectedValue.ToString & " AND PNT_SBL_ID=" & ddlArea.SelectedValue.ToString()

            ddlpickup.Items.Clear()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlpickup.DataSource = ds
            ddlpickup.DataTextField = "PNT_DESCRIPTION"
            ddlpickup.DataValueField = "PNT_ID"
            ddlpickup.DataBind()

            li.Text = "ALL"
            li.Value = "0"
            ddlpickup.Items.Insert(0, li)
        Else
            li.Text = "ALL"
            li.Value = "0"
            ddlpickup.Items.Add(li)
        End If
    End Sub


    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlArea.SelectedValue = 0 Then
                ddlpickupPoint.Items.Clear()
                ddlTrip.Items.Clear()
            Else
                BindPickup(ddlpickupPoint, ddlArea)
                GetTripOnward()
                GetTripReturn()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub DispLayEmployeeDetails(ByVal EMPID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT EMPNO, ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'')AS EMPNAME," & _
                    "EMP_DEPT_DESCR,EMP_DES_DESCR,CATEGORY_DESC,EMP_RLG_ID,EMP_CNTR_DESCR " & _
                    " FROM vw_OSO_EMPLOYEEMASTER WHERE EMP_BSU_ID=" & Session("sBsuid") & " AND EMP_ID=" & EMPID

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count >= 1 Then
            lblStaffNo.Text = ds.Tables(0).Rows(0).Item("EMPNO")
            lblStaffName.Text = ds.Tables(0).Rows(0).Item("EMPNAME")
            lblCategory.Text = ds.Tables(0).Rows(0).Item("CATEGORY_DESC")
            lblDepartment.Text = ds.Tables(0).Rows(0).Item("EMP_DEPT_DESCR")
            lblDesignation.Text = ds.Tables(0).Rows(0).Item("EMP_DES_DESCR")
            lblnationality.Text = ds.Tables(0).Rows(0).Item("EMP_CNTR_DESCR")
            lblReligion.Text = ds.Tables(0).Rows(0).Item("EMP_RLG_ID")
        End If
    End Sub

    Private Function GetTripOnward() As String
        Try
            Dim str_Conn = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim strFilter As String = ""
            Dim strQuery As String

            If ddlpickupPoint.SelectedValue <> 0 Then
                strFilter = " AND S.TPP_PNT_ID=" & ddlpickupPoint.SelectedValue
            End If

            strQuery = "SELECT DISTINCT(B.TRP_ID),(B.BNO + ' - ' + B.TRP_DESCR) AS TRP_DESCR FROM  TRANSPORT.VV_BUSES B " & _
                       "INNER JOIN TRANSPORT.TRIPS_PICKUP_S S ON B.TRP_ID=S.TPP_TRP_ID " & _
                       "INNER JOIN TRANSPORT.TRIPS_D D ON B.TRD_ID= D.TRD_ID " & _
                       "INNER JOIN TRANSPORT.TRIPS_M M ON D.TRD_TRP_ID=M.TRP_ID WHERE B.TRP_JOURNEY='ONWARD' " & _
                       "AND B.TRD_BSU_ID='" & Session("sBsuid") & "' AND S.TPP_SBL_ID=" & ddlArea.SelectedValue & "  " & strFilter

            Dim dsEmpTrip As DataSet
            dsEmpTrip = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)

            ddlTrip.DataSource = dsEmpTrip
            ddlTrip.DataTextField = "TRP_DESCR"
            ddlTrip.DataValueField = "TRP_ID"
            ddlTrip.DataBind()

            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlTrip.Items.Insert(0, li)

        Catch ex As Exception

        End Try
    End Function

    Private Function GetTripReturn() As String
        Try
            Dim str_Conn = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim strFilter As String
            Dim strQuery As String
            If ddlpickupPoint1.SelectedValue.ToString <> "" And ddlpickupPoint1.SelectedValue <> "0" Then
                strFilter = " AND B.TPP_PNT_ID=" & ddlpickupPoint1.SelectedValue
            End If

            strQuery = "SELECT DISTINCT(B.TRP_ID),(B.BNO + ' - ' + B.TRP_DESCR) AS TRP_DESCR FROM  TRANSPORT.VV_BUSES B " & _
                       "INNER JOIN TRANSPORT.TRIPS_PICKUP_S S ON B.TRP_ID=S.TPP_TRP_ID " & _
                       "INNER JOIN TRANSPORT.TRIPS_D D ON B.TRD_ID= D.TRD_ID " & _
                       "INNER JOIN TRANSPORT.TRIPS_M M ON D.TRD_TRP_ID=M.TRP_ID WHERE B.TRP_JOURNEY='RETURN' " & _
                       "AND B.TRD_BSU_ID='" & Session("sBsuid") & "' AND S.TPP_SBL_ID=" & ddlArea1.SelectedValue & "  "


            Dim dsEmpTrip As DataSet
            dsEmpTrip = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)
            ddlTrip1.Items.Clear()
            ddlTrip1.DataSource = dsEmpTrip
            ddlTrip1.DataTextField = "TRP_DESCR"
            ddlTrip1.DataValueField = "TRP_ID"
            ddlTrip1.DataBind()

            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlTrip1.Items.Insert(0, li)

        Catch ex As Exception

        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            '
            'Validations --------------------------------------------
            If ddlArea.SelectedValue = "0" Then
                lblError.Text = "Please select the area"
                Exit Sub
            End If
            If ddlTrip.SelectedValue = "0" Then
                lblError.Text = "Please select the trip"
                Exit Sub
            End If

            If ddlpickupPoint.SelectedValue = "0" Then
                lblError.Text = "Please select the pickup point"
                Exit Sub
            End If
            If txtStDate.Text.Trim = "" Then
                lblError.Text = "Please enter date."
                Exit Sub
            End If

            If chkReturn.Checked = True Then
                If ddlArea1.SelectedValue = "0" Then
                    lblError.Text = "Please select the return area"
                    Exit Sub
                End If
                If ddlTrip1.SelectedValue = "0" Then
                    lblError.Text = "Please select the return trip"
                    Exit Sub
                End If

                If ddlpickupPoint1.SelectedValue = "0" Then
                    lblError.Text = "Please select the return pickup point"
                    Exit Sub
                End If
                If txtStDate1.Text.Trim = "" Then
                    lblError.Text = "Please enter the return date."
                    Exit Sub
                End If
            End If

            'End Validations ----------------------------------------
            SaveData()

            'Move to List Form
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & ViewState("datamode").ToString()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect("tptEmpTransportAlloc.aspx" & mInfo)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveData()

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim AllocationDate As Date = txtStDate.Text

                Dim str_query As String = "exec TRANSPORT.saveEMPTRANSPORTS " + HIDAllocOwd.Value + "," + ViewState("EMPID") + "," + ddlArea.SelectedValue + "," & _
                 "" + ddlpickupPoint.SelectedValue + "," + ddlTrip.SelectedValue + ",1,'" + Session("sBsuid") + "','" + AllocationDate + "'," & _
                "'Onward','" + ViewState("datamode").ToString + "'"

                HFDID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                If HFDID.Value = "" Then
                    HFDID.Value = 0
                End If

                ' To Save the Return Trip Entry
                Dim ReturnDate As Date
                If chkReturn.Checked = True Then
                    ReturnDate = txtStDate1.Text
                    'Dim ReturnDate As Date = txtStDate1.Text
                    If ViewState("datamode") = "edit" Then
                        If ViewState("return") = "add" Then
                            HidAllocRtn.Value = "0"
                            Dim str_queryRtnEdit As String = "exec TRANSPORT.saveEMPTRANSPORTS " + HidAllocRtn.Value + "," + ViewState("EMPID") + "," + ddlArea1.SelectedValue + "," & _
                                "" + ddlpickupPoint1.SelectedValue + "," + ddlTrip1.SelectedValue + ",1,'" + Session("sBsuid") + "','" + ReturnDate + "'," & _
                                "'Return','add'"
                            HFDID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_queryRtnEdit)
                        End If
                    End If

                    Dim str_queryRtn As String = "exec TRANSPORT.saveEMPTRANSPORTS " + HidAllocRtn.Value + "," + ViewState("EMPID") + "," + ddlArea1.SelectedValue + "," & _
                          "" + ddlpickupPoint1.SelectedValue + "," + ddlTrip1.SelectedValue + ",1,'" + Session("sBsuid") + "','" + ReturnDate + "'," & _
                          "'Return','" + ViewState("datamode").ToString + "'"

                    HFDID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_queryRtn)
                Else
                    ViewState("datamode") = "Delete"

                    ' If The Return is un Checked .. Return Entry to be deleted , If  Exists.
                    Dim str_queryDel As String = "exec TRANSPORT.saveEMPTRANSPORTS " + HidAllocRtn.Value + "," + ViewState("EMPID") + ",0," & _
                                              "0,0,1,'" + Session("sBsuid") + "','" + ReturnDate + "'," & _
                                              "'Return','" + ViewState("datamode").ToString + "'"

                    HFDID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_queryDel)
                End If

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EMPTRPID(" + HFDID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully "

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
            End Try
        End Using
    End Sub

    Private Function IsTransportAlloted(ByVal EMPID As Integer) As Boolean
        Try
            Dim str_Conn = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim strQuery As String = "SELECT EMP_ID FROM TRANSPORT.TPTEMPLOYEE_TRIP WHERE EMP_ID=" & EMPID

            Dim dsEmpTrip As DataSet
            dsEmpTrip = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)

            If dsEmpTrip.Tables(0).Rows.Count >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function DisplayTransportDet(ByVal EMPID As Integer)
        Try
            Dim str_Conn = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim strQuery As String = "SELECT EMPTRPID,EMP_ID,SBL_ID,POINT_ID,TRIP_ID,EMP_TPTALLOCATED,BSU_ID,TPTALLOCDATE,TPT_JOURNY " & _
                                    " FROM TRANSPORT.TPTEMPLOYEE_TRIP WHERE EMP_ID=" & EMPID

            Dim dsEmpTrip As DataSet
            dsEmpTrip = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, strQuery)

            If dsEmpTrip.Tables(0).Rows.Count >= 1 Then

                With dsEmpTrip.Tables(0)
                    Dim dVOnward As New DataView(dsEmpTrip.Tables(0), "TPT_JOURNY='Onward'", "EMPTRPID", DataViewRowState.Unchanged)
                    If dVOnward.Count >= 1 Then
                        HIDAllocOwd.Value = dVOnward.Item(0).Row.Item("EMPTRPID")
                        HFDID.Value = dVOnward.Item(0).Row.Item("EMP_ID")
                        ddlArea.SelectedValue = dVOnward.Item(0).Row.Item("SBL_ID")
                        BindPickup(ddlpickupPoint, ddlArea)
                        GetTripOnward()
                        ddlpickupPoint.SelectedValue = dVOnward.Item(0).Row.Item("POINT_ID")
                        ddlTrip.SelectedValue = dVOnward.Item(0).Row.Item("TRIP_ID")
                        txtStDate.Text = Format(dVOnward.Item(0).Row.Item("TPTALLOCDATE"), "dd/MMM/yyyy")

                    End If

                    Dim dVRetutn As New DataView(dsEmpTrip.Tables(0), "TPT_JOURNY='Return'", "EMPTRPID", DataViewRowState.Unchanged)
                    If dVRetutn.Count >= 1 Then
                        chkReturn.Checked = True
                        Panel1.Visible = True
                        TitlePanel.Visible = True
                        lblReturn.Text = "Transport Allocation Return"
                        BindSublocation(ddlArea1)
                        HidAllocRtn.Value = dVRetutn.Item(0).Row.Item("EMPTRPID")
                        ddlArea1.SelectedValue = dVRetutn.Item(0).Row.Item("SBL_ID")
                        'BindPickup(ddlpickupPoint1, ddlArea1)
                        GetTripReturn()
                        ddlTrip1.SelectedValue = IIf(dVRetutn.Item(0).Row.Item("TRIP_ID").ToString = "", 0, dVRetutn.Item(0).Row.Item("TRIP_ID"))
                        If ddlTrip1.SelectedValue = "" Or ddlTrip1.SelectedValue = "0" Then
                            BindPickup(ddlpickupPoint1, ddlArea1)
                            ddlpickupPoint1.SelectedValue = dVRetutn.Item(0).Row.Item("POINT_ID")
                            GetTripReturn()
                        Else
                            BindTripWIsePickup(ddlpickupPoint1, ddlTrip1)
                            ddlpickupPoint1.SelectedValue = dVRetutn.Item(0).Row.Item("POINT_ID")
                            'GetTripReturn()
                        End If

                        txtStDate1.Text = Format(dVRetutn.Item(0).Row.Item("TPTALLOCDATE"),"dd/MMM/yyyy")
                        ddlpickupPoint1.SelectedValue = dVRetutn.Item(0).Row.Item("POINT_ID")
                    Else
                        ViewState("return") = "add"
                    End If

                End With

            End If
        Catch ex As Exception

        End Try

    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("datamode") = "none"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & ViewState("datamode").ToString()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect("tptEmpTransportAlloc.aspx" & mInfo)
            ' End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlpickupPoint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GetTripOnward()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlpickupPoint.SelectedValue = 0 Then
                BindTripWIsePickup(ddlpickupPoint, ddlTrip)
            Else
            End If
            If ddlTrip.SelectedValue = 0 Then
                BindPickup(ddlpickupPoint, ddlArea)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkReturn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkReturn.Checked = True Then
            lblReturn.Text = "Transport Allocation Return"
            Panel1.Visible = True
            TitlePanel.Visible = True
            BindSublocation(ddlArea1)
            If ViewState("datamode") = "add" Then
                If ddlArea.SelectedValue <> "0" Then
                    ddlArea1.SelectedValue = ddlArea.SelectedValue
                    BindPickup(ddlpickupPoint1, ddlArea1)
                    GetTripReturn()
                    'BindTripWIsePickup(ddlpickupPoint1, ddlArea1)

                    If ddlpickupPoint.SelectedValue <> "0" Then
                        'ddlpickupPoint1.SelectedValue = ddlpickupPoint.SelectedValue
                    End If


                    If ddlTrip.SelectedValue <> "0" Then
                        'ddlTrip1.SelectedValue = ddlTrip.SelectedValue
                    End If
                    If txtStDate.Text.Trim <> "" Then
                        txtStDate1.Text = txtStDate.Text
                    End If
                Else
                    BindPickup(ddlpickupPoint1, ddlArea1)
                End If

            End If

        Else
            lblReturn.Text = ""
            Panel1.Visible = False
            TitlePanel.Visible = False

        End If
    End Sub

    Protected Sub ddlArea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlArea1.SelectedValue = 0 Then
                ddlpickupPoint1.Items.Clear()
                ddlTrip1.Items.Clear()
            Else
                BindPickup(ddlpickupPoint1, ddlArea1)
                'GetTripOnward()
                GetTripReturn()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlpickupPoint1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlTrip1.SelectedValue = "0" Then
                GetTripReturn()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlTrip1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If ddlpickupPoint1.SelectedValue = "0" Then
            '    BindPickup(ddlpickupPoint1, ddlArea1)
            'Else
            '    BindTripWIsePickup(ddlpickupPoint1, ddlTrip1)
            'End If
            If ddlpickupPoint1.SelectedValue = 0 Then
                BindTripWIsePickup(ddlpickupPoint1, ddlTrip1)
            Else
            End If
            If ddlTrip1.SelectedValue = 0 Then
                BindPickup(ddlpickupPoint1, ddlArea1)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlArea1_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlArea1.SelectedIndexChanged

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("datamode") = "DeleteAll"
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim AllocationDate As Date = txtStDate.Text

                    Dim str_query As String = "exec TRANSPORT.saveEMPTRANSPORTS " + HIDAllocOwd.Value + "," + ViewState("EMPID") + "," + ddlArea.SelectedValue + "," & _
                     "" + ddlpickupPoint.SelectedValue + "," + ddlTrip.SelectedValue + ",1,'" + Session("sBsuid") + "','" + AllocationDate + "'," & _
                    "'Onward','" + ViewState("datamode").ToString + "'"

                    HFDID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EMPTRPID(" + HFDID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully "

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                End Try
            End Using

            'Move to List Form
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & ViewState("datamode").ToString()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect("tptEmpTransportAlloc.aspx" & mInfo)
        Catch ex As Exception

        End Try
    End Sub
End Class
