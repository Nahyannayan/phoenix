﻿<%@ WebService Language="VB"  Class="STU_Parent_Consent_Service" %>
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Data

<WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Script.Services.ScriptService()> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class STU_Parent_Consent_Service
    Inherits System.Web.Services.WebService
    
    <WebMethod()> _
    Public Function GetDynamicContent(ByVal contextKey As String) As String
        Dim constr As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim query As String = " SELECT CONSENT,REPLACE(CONVERT(VARCHAR(20),LOG_DATE,106),' ','/') + RIGHT(CONVERT(VARCHAR(20),LOG_DATE,100),8) LOG_DATE,LOG_USR,STU_Parent_Consent_remark FROM TRANSPORT.PAR_CONSENT_LOG WHERE STU_ID= " & contextKey & " ORDER BY id desc "  '& ",'" & HttpContext.Current.Session("sBSUID") & "'"
        Dim b As New StringBuilder()

        Dim da As New SqlDataAdapter(query, constr)
        Dim Ttable As New DataTable()
        da.Fill(Ttable)

        b.Append("<table style='background-color:#f3f3f3; border: #336699 3px solid; ")
        b.Append("width:550px; font-size:8pt; font-family:Verdana;' cellspacing='0' cellpadding='3'>")

        b.Append("<tr><td colspan='5' style='background-color:#336699; color:white;'>")
        b.Append("<b>Status History</b>")
        b.Append("</td></tr>")
        b.Append("<tr><td style='width:80px;align:center'><b>Status</b></td>")
        b.Append("<td style='align:center'><b>Updated On</b></td>")
        b.Append("<td><b>Updated By</b></td>")
        b.Append("<td><b>Comments</b></td></tr>")
        If Ttable.Rows.Count > 0 Then

            For Each drow As DataRow In Ttable.Rows


                b.Append("<tr>")
                b.Append("<td style='align:left'>" & drow("CONSENT").ToString() & "</td>")
                b.Append("<td style='align:left'>" & drow("LOG_DATE").ToString() & "</td>")
                b.Append("<td style='align:left'>" & drow("LOG_USR").ToString() & "</td>")
                b.Append("<td style='align:left'>" & drow("STU_Parent_Consent_remark").ToString() & "</td>")
                b.Append("</tr>")
            Next
        Else
            b.Append("<tr width='100%'>")
            b.Append("<td colspan='5'>No status added </td>")
            b.Append("</tr>")
        End If
        b.Append("</table>")
        Return b.ToString()
        

    End Function

End Class
