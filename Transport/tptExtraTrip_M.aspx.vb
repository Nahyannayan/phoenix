Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Transport_tptExtraTrip_M
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100230") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfTRD_ID.Value = 0


                    BindCategory()

                    BindDriver()
                    BindConductor()
                    BindPurpose()
                    If ViewState("datamode") = "view" Then
                        Dim editString As String = Encr_decrData.Decrypt(Request.QueryString("editString").Replace(" ", "+"))
                        BindData(editString)
                        EnableDisableControls(False)
                    Else
                        hfTRD_ID.Value = 0
                        hfTRP_ID.Value = 0
                        BindVehicle()
                        GetTripName()
                    End If


                    If ddlVehicle.SelectedValue <> 0 Then
                        GetCapaCity()
                    End If



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindData(ByVal editString As String)
        Dim editstrings As String() = editString.Split("|")
        hfTRD_ID.Value = editstrings(0)

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TRD_TRP_ID,ISNULL(TRD_DRIVER_EMP_ID,0),ISNULL(TRD_CONDUCTOR_EMP_ID,0),ISNULL(TRD_VEH_ID,0),isnull(TRD_REMARKS,''),VEH_CAT_ID,TRD_FROMDATE,ISNULL(TRD_TODATE,'01/Jan/1900')," _
                                  & "ISNULL(TRP_PSE_ID,0),ISNULL(TRP_DESTINATION,'') FROM TRANSPORT.TRIPS_D AS A INNER JOIN VEHICLE_M AS B " _
                                  & " ON A.TRD_VEH_ID=B.VEH_ID " _
                                  & " INNER JOIN TRANSPORT.TRIPS_M AS C ON A.TRD_TRP_ID=C.TRP_ID" _
                                  & " WHERE TRD_ID=" + editstrings(0)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
         While reader.Read
            hfTRP_ID.Value = reader.GetValue(0)
            txtTrip.Text = editstrings(1)

            ddlDriver.ClearSelection()
            ddlDriver.Items.FindByValue(reader.GetValue(1)).Selected = True

            ddlConductor.ClearSelection()
            ddlConductor.Items.FindByValue(reader.GetValue(2)).Selected = True

            ddlCategory.ClearSelection()
            ddlCategory.Items.FindByValue(reader.GetValue(5)).Selected = True

            BindVehicle()

            ddlVehicle.ClearSelection()
            ddlVehicle.Items.FindByValue(reader.GetValue(3)).Selected = True


            txtRemarks.Text = reader.GetString(4)

            ddlCategory.ClearSelection()
            ddlCategory.Items.FindByValue(reader.GetValue(5)).Selected = True

            txtFrom.Text = Format(reader.GetDateTime(6), "dd/MMM/yyyy")
            txtTo.Text = Format(reader.GetDateTime(7), "dd/MMM/yyyy").Replace("01/Jan/1900", "")

            If reader.GetValue(8) <> 0 Then
                ddlPurpose.Items.FindByValue(reader.GetValue(8)).Selected = True
            End If
            txtDestination.Text = reader.GetString(9)

        End While
        reader.Close()

       End Sub

    Private Function SaveData() As Boolean
        Dim datamode As String = ""
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim reader As SqlDataReader
        Dim str_query As String
        Dim flag As Boolean = True
        Dim i As Integer

           Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            'Try

            'save trips_m
            Dim journey As String
          
            str_query = "exec [TRANSPORT].[SaveEXTRATRIP_M] " _
                        & hfTRP_ID.Value + "," _
                        & "'" + Session("sbsuid") + "'," _
                        & "'" + txtTrip.Text + "'," _
                        & ddlPurpose.SelectedValue.ToString + "," _
                        & "'" + txtRemarks.Text + "'," _
                        & "'" + txtDestination.Text + "'," _
                        & "'" + ViewState("datamode") + "'"
            hfTRP_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            If ViewState("datamode") = "edit" Then

                str_query = "SELECT TRD_TRP_ID,isnull(TRD_VEH_ID,0),isnull(TRD_DRIVER_EMP_ID,0),isnull(TRD_CONDUCTOR_EMP_ID,0)" _
                          & " FROM TRANSPORT.TRIPS_D  WHERE TRD_ID =" + hfTRD_ID.Value
                reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
                While reader.Read
                    With reader
                        If .GetValue(1) <> CType(ddlVehicle.SelectedValue, Integer) Then
                            flag = False
                        End If
                        If .GetValue(2) <> CType(ddlDriver.SelectedValue, Integer) Then
                            flag = False
                        End If
                        If .GetValue(3) <> CType(ddlConductor.SelectedValue, Integer) Then
                            flag = False
                        End If
                       
                    End With
                End While

                reader.Close()

                If flag = False Then
                    datamode = "edit"
                Else
                    datamode = "none"
                End If
            ElseIf ViewState("datamode") = "add" Then
                datamode = "add"
            ElseIf ViewState("datamode") = "delete" Then
                datamode = "delete"
            End If

            Dim trd_previd As String

            If ViewState("datamode") = "edit" Then
                trd_previd = hfTRD_ID.Value.ToString
            Else
                trd_previd = 0
            End If

            If datamode <> "none" Then
                If datamode = "edit" Or datamode = "delete" Then
                    UtilityObj.InsertAuditdetails(transaction, datamode, "TRANSPORT.TRIPS_D", "TRD_ID", "TRD_TRP_ID", "TRD_ID=" + hfTRD_ID.Value.ToString)
                End If
                str_query = "exec [TRANSPORT].[SaveEXTRATRIP_D] '" _
                           & Session("sbsuid") + "'," _
                           & hfTRD_ID.Value + "," _
                           & hfTRP_ID.Value + "," _
                           & ddlVehicle.SelectedValue.ToString + "," _
                           & ddlDriver.SelectedValue.ToString + "," _
                           & IIf(ddlConductor.SelectedValue = 0, "NULL", ddlConductor.SelectedValue.ToString) + "," _
                           & "'" + txtRemarks.Text.ToString + " '," _
                           & "'" + txtFrom.Text + "'," _
                           & IIf(txtTo.Text = "", "NULL", "'" + txtTo.Text + "'") + "," _
                           & "'" + datamode + " '"
                hfTRD_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
            End If

         
            transaction.Commit()
            lblError.Text = "Record Saved Successfully"
            Dim flagAudit As Integer
            If datamode <> "edit" Then
                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TRD_ID(" + hfTRD_ID.Value.ToString + ")", datamode, Page.User.Identity.Name.ToString, Me.Page)
            Else
                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TRD_ID(" + trd_previd.ToString + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TRD_ID(" + hfTRD_ID.Value.ToString + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
            End If
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

            'Catch myex As ArgumentException
            '    transaction.Rollback()
            '    lblError.Text = myex.Message
            'Catch ex As Exception
            '    transaction.Rollback()
            '    lblError.Text = "Record could not be Saved"

            'End Try
        End Using
        Return True
    End Function

    Sub GetTripName()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.GETTRIPNAME"
        txtTrip.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub
    Sub BindCategory()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT CAT_ID,CAT_DESCRIPTION FROM TRANSPORT.VV_CATEGORY_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "CAT_DESCRIPTION"
        ddlCategory.DataValueField = "CAT_ID"
        ddlCategory.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = 0
        ddlCategory.Items.Insert(0, li)
    End Sub
    Private Sub BindVehicle()
        ddlvehicle.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If ddlCategory.SelectedValue <> 0 Then
            str_query = "select veh_id,veh_regno from transport.vv_vehicle_m where VEH_ALTO_BSU_ID='" + Session("sbsuid") + "' " _
                                       & " and cat_id=" + ddlCategory.SelectedValue.ToString
        Else
            str_query = "select veh_id,veh_regno from transport.vv_vehicle_m where VEH_ALTO_BSU_ID='" + Session("sbsuid") + "' "
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlVehicle.DataSource = ds
        ddlVehicle.DataTextField = "veh_regno"
        ddlVehicle.DataValueField = "veh_id"
        ddlVehicle.DataBind()
        If ddlCategory.SelectedValue = 0 Then
            Dim li As New ListItem
            li.Text = "--"
            li.Value = 0
            ddlVehicle.Items.Insert(0, li)
        End If
    End Sub

    Sub GetCapaCity()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT VEH_CAPACITY FROM VEHICLE_M WHERE VEH_ID=" + ddlVehicle.SelectedValue.ToString
        lblCapacity.Text = "Capacity : " + SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
    End Sub

    Sub BindDriver()
        ddlDriver.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
        '                         & "emp_id FROM employee_m WHERE emp_des_id=(select des_id from empdesignation_m where des_descr='Driver' and des_flag='SD') and emp_bsu_id='" + Session("sBsuid") + "'"

        Dim str_query As String = "SELECT  emp_name,emp_id FROM VV_DRIVER where bsu_alto_id='" + Session("sBsuid") + "'" _
                                 & " ORDER BY EMP_NAME"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDriver.DataSource = ds
        ddlDriver.DataTextField = "emp_name"
        ddlDriver.DataValueField = "emp_id"
        ddlDriver.DataBind()
    End Sub

    Sub BindConductor()
        ddlConductor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
        '                         & "emp_id FROM employee_m WHERE emp_des_id=(select des_id from empdesignation_m where des_descr='Conductor' and des_flag='SD') and emp_bsu_id='" + Session("sBsuid") + "'"

        Dim str_query As String = "SELECT emp_name,emp_id FROM VV_CONDUCTOR where bsu_alto_id='" + Session("sBsuid") + "'" _
                                    & " ORDER BY EMP_NAME"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlConductor.DataSource = ds
        ddlConductor.DataTextField = "emp_name"
        ddlConductor.DataValueField = "emp_id"
        ddlConductor.DataBind()
    End Sub


    Private Sub EnableDisableControls(ByVal value As Boolean)
        ddlVehicle.Enabled = value
        ddlDriver.Enabled = value
        ddlConductor.Enabled = value
        ddlCategory.Enabled = value
        ddlPurpose.Enabled = value
        txtFrom.Enabled = value
        imgFrom.Enabled = value
        txtTo.Enabled = value
        imgTo.Enabled = value
        txtRemarks.Enabled = value
        txtDestination.Enabled = value
    End Sub

    Sub BindPurpose()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT PSE_ID,PSE_DESCR FROM TRANSPORT.TRIP_PURPOSE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPurpose.DataSource = ds
        ddlPurpose.DataTextField = "PSE_DESCR"
        ddlPurpose.DataValueField = "PSE_ID"
        ddlPurpose.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0
        ddlPurpose.Items.Insert(0, li)
    End Sub
  
#End Region

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            GetTripName()
            EnableDisableControls(True)
            txtRemarks.Text = ""
            ViewState("datamode") = "add"
            hfTRD_ID.Value = 0
            hfTRP_ID.Value = 0
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ViewState("datamode") <> "none" Then
                If txtTo.Text <> "" Then
                    Dim dtFrom As Date = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    Dim dtTo As Date = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    If DateTime.Compare(dtFrom, dtTo) > 0 Then
                        lblError.Text = "From date as to be  date less that To date"
                        Exit Sub
                    End If
                End If
                If ddlDriver.SelectedValue = 0 Then
                    lblError.Text = "Please select a driver"
                    Exit Sub
                End If
                If ddlVehicle.SelectedValue = 0 Then
                    lblError.Text = "Please select a vehicle"
                    Exit Sub
                End If
                SaveData()
                EnableDisableControls(False)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            ViewState("datamode") = "edit"
            EnableDisableControls(True)

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfTRD_ID.Value = 0
               If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                  'ddlVehicle.Items(0).Selected = True
                'ddlTrip.Items(0).Selected = True
                'ddlBusNo.Items(0).Selected = True
                'ddlDriver.Items(0).Selected = True
                'ddlConductor.Items(0).Selected = True
                txtRemarks.Text = ""
                EnableDisableControls(False)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ViewState("datamode") = "delete"
            If hfTRD_ID.Value = 0 Then
                lblError.Text = "No records to delete"
                Exit Sub
            End If
            SaveData()
            ViewState("datamode") = "none"
            txtRemarks.Text = ""
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlVehicle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVehicle.SelectedIndexChanged
        Try
            If ddlVehicle.SelectedValue <> 0 Then
                GetCapaCity()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            BindVehicle()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
