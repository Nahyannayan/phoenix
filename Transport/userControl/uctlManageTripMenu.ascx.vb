﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO

Public Structure SessionValues
    Dim DataMode As String
    Dim MainMenuCode As String
End Structure

Partial Class TransportV2_userControl_uctlManageTripMenu
    Inherits System.Web.UI.UserControl
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            SetActiveMenu()
            'BindTrip_Count()
        End If
    End Sub

    Sub SetActiveMenu()
        Dim currentpage As String = Path.GetFileName(Page.Request.Path).ToString
        If currentpage = "tripMainDetails.aspx" Then
            lbtnTripDetails.Attributes.Add("class", "cssStepBtnActive")
            lbtnAddBSU.Attributes.Add("class", "cssStepBtnInActive")
            lbtnPickupPoints.Attributes.Add("class", "cssStepBtnInActive")
            lbtnVehicleDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnDriverDetails.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "tripAddBSU.aspx" Then
            lbtnTripDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnAddBSU.Attributes.Add("class", "cssStepBtnActive")
            lbtnPickupPoints.Attributes.Add("class", "cssStepBtnInActive")
            lbtnVehicleDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnDriverDetails.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "tripPickupPoints.aspx" Then
            lbtnTripDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnAddBSU.Attributes.Add("class", "cssStepBtnInActive")
            lbtnPickupPoints.Attributes.Add("class", "cssStepBtnActive")
            lbtnVehicleDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnDriverDetails.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "tripVehicleDetails.aspx" Then
            lbtnTripDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnAddBSU.Attributes.Add("class", "cssStepBtnInActive")
            lbtnPickupPoints.Attributes.Add("class", "cssStepBtnInActive")
            lbtnVehicleDetails.Attributes.Add("class", "cssStepBtnActive")
            lbtnDriverDetails.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "tripDriverDetails.aspx" Then
            lbtnTripDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnAddBSU.Attributes.Add("class", "cssStepBtnInActive")
            lbtnPickupPoints.Attributes.Add("class", "cssStepBtnInActive")
            lbtnVehicleDetails.Attributes.Add("class", "cssStepBtnInActive")
            lbtnDriverDetails.Attributes.Add("class", "cssStepBtnActive")
        End If
    End Sub

    Sub BindTrip_Count()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ds As New DataSet
            Dim param(2) As SqlClient.SqlParameter
            Dim trpId As Integer = 0
            If Convert.ToString(Session("TRP_M_ID")) <> "" Then
                trpId = Session("TRP_M_ID").ToString
            End If
            param(0) = New SqlParameter("@TRIP_ID", trpId)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRANSPORT.GET_TRIPS_COUNTS_BY_TRIP_ID", param)
            Dim currentpage As String = Path.GetFileName(Page.Request.Path).ToString
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt1 As New DataTable
                dt1 = ds.Tables(0)
                If currentpage <> "tripMainDetails.aspx" Then
                    If Convert.ToInt16(dt1.Rows(0)("mcount").ToString) > 0 Then
                        lbtnTripDetails.Attributes.Add("class", "")
                        lbtnTripDetails.Attributes.Add("class", "cssStepBtnComp")
                    End If
                End If
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                Dim dt2 As New DataTable
                dt2 = ds.Tables(1)
                If currentpage <> "tripPickupPoints.aspx" Then
                    If Convert.ToInt16(dt2.Rows(0)("pcount").ToString) > 0 Then
                        lbtnPickupPoints.Attributes.Add("class", "")
                        lbtnPickupPoints.Attributes.Add("class", "cssStepBtnComp")
                    End If
                End If
            End If
            If ds.Tables(2).Rows.Count > 0 Then
                Dim dt3 As New DataTable
                dt3 = ds.Tables(2)
                If currentpage <> "tripVehicleDetails.aspx" Then
                    If Convert.ToInt16(dt3.Rows(0)("vcount").ToString) > 0 Then
                        lbtnVehicleDetails.Attributes.Add("class", "")
                        lbtnVehicleDetails.Attributes.Add("class", "cssStepBtnComp")
                    End If
                End If
            End If
            If ds.Tables(3).Rows.Count > 0 Then
                Dim dt4 As New DataTable
                dt4 = ds.Tables(3)
                If currentpage <> "tripDriverDetails.aspx" Then
                    If Convert.ToInt16(dt4.Rows(0)("dcount").ToString) > 0 Then
                        lbtnDriverDetails.Attributes.Add("class", "")
                        lbtnDriverDetails.Attributes.Add("class", "cssStepBtnComp")
                    End If
                End If
            End If
            If ds.Tables(4).Rows.Count > 0 Then
                Dim dt5 As New DataTable
                dt5 = ds.Tables(4)
                If currentpage <> "tripAddBSU.aspx" Then
                    If Convert.ToInt16(dt5.Rows(0)("scount").ToString) > 0 Then
                        lbtnAddBSU.Attributes.Add("class", "")
                        lbtnAddBSU.Attributes.Add("class", "cssStepBtnComp")
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbtnTripDetails_Click(sender As Object, e As EventArgs) Handles lbtnTripDetails.Click
        If Convert.ToString(Request.QueryString("datamode")) <> "" Then
            Dim DataMode As String
            DataMode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If DataMode = "edit" Then
                Dim values As SessionValues = GetSessionValues()
                Dim Url = String.Format("~\Transport\tripMainDetails.aspx?MainMnu_code={0}&datamode={1}", values.MainMenuCode, values.DataMode)
                Response.Redirect(Url, False)
            End If
        End If
    End Sub

    Protected Sub lbtnAddBSU_Click(sender As Object, e As EventArgs) Handles lbtnAddBSU.Click
        If Convert.ToString(Request.QueryString("datamode")) <> "" Then
            Dim DataMode As String
            DataMode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If DataMode = "edit" Then
                Dim values As SessionValues = GetSessionValues()
                Dim Url = String.Format("~\Transport\tripAddBSU.aspx?MainMnu_code={0}&datamode={1}", values.MainMenuCode, values.DataMode)
                Response.Redirect(Url, False)
            End If
        End If
    End Sub

    Protected Sub lbtnPickupPoints_Click(sender As Object, e As EventArgs) Handles lbtnPickupPoints.Click
        If Convert.ToString(Request.QueryString("datamode")) <> "" Then
            Dim DataMode As String
            DataMode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If DataMode = "edit" Then
                Dim values As SessionValues = GetSessionValues()
                Dim Url = String.Format("~\Transport\tripPickupPoints.aspx?MainMnu_code={0}&datamode={1}", values.MainMenuCode, values.DataMode)
                Response.Redirect(Url, False)
            End If
        End If
    End Sub

    Protected Sub lbtnVehicleDetails_Click(sender As Object, e As EventArgs) Handles lbtnVehicleDetails.Click
        If Convert.ToString(Request.QueryString("datamode")) <> "" Then
            Dim DataMode As String
            DataMode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If DataMode = "edit" Then
                Dim values As SessionValues = GetSessionValues()
                Dim Url = String.Format("~\Transport\tripVehicleDetails.aspx?MainMnu_code={0}&datamode={1}", values.MainMenuCode, values.DataMode)
                Response.Redirect(Url, False)
            End If
        End If
    End Sub

    Protected Sub lbtnDriverDetails_Click(sender As Object, e As EventArgs) Handles lbtnDriverDetails.Click
        If Convert.ToString(Request.QueryString("datamode")) <> "" Then
            Dim DataMode As String
            DataMode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If DataMode = "edit" Then
                Dim values As SessionValues = GetSessionValues()
                Dim Url = String.Format("~\Transport\tripDriverDetails.aspx?MainMnu_code={0}&datamode={1}", values.MainMenuCode, values.DataMode)
                Response.Redirect(Url, False)
            End If
        End If
    End Sub

#Region "Get Session Values"
    Private Function GetSessionValues() As SessionValues
        Dim values As New SessionValues
        If Convert.ToString(Session("Data_Mode")) <> "" Then
            values.DataMode = Session("Data_Mode")
        End If
        If Convert.ToString(Session("Main_Menu_Code")) <> "" Then
            values.MainMenuCode = Session("Main_Menu_Code")
        End If
        Return values
    End Function
#End Region
End Class
