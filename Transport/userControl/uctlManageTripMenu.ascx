﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uctlManageTripMenu.ascx.vb" Inherits="TransportV2_userControl_uctlManageTripMenu" %>
<div align="left" class="matters" style="direction: none;">
    <asp:LinkButton ID="lbtnTripDetails" runat="server" CssClass="">                
        <div class="">           
            <div class="cssStepInfo" style="padding-top:5px; text-align:center;">Trip Details</div>     
        </div>  
    </asp:LinkButton>
    <asp:LinkButton ID="lbtnAddBSU" runat="server" CssClass="">                
        <div class="">           
            <div class="cssStepInfo" style="padding-top:5px; text-align:center;">Add Shared BSU</div>     
        </div>  
    </asp:LinkButton>
    <asp:LinkButton ID="lbtnPickupPoints" runat="server" CssClass="">                
        <div class="">           
            <div class="cssStepInfo" style="padding-top:5px; text-align:center;">Pick up points</div>     
        </div>
    </asp:LinkButton>
    <asp:LinkButton ID="lbtnVehicleDetails" runat="server" CssClass="">                
        <div class="">           
            <div class="cssStepInfo" style="padding-top:5px; text-align:center;">Vehicle Details</div>     
        </div>
    </asp:LinkButton>
    <asp:LinkButton ID="lbtnDriverDetails" runat="server" CssClass="">                
        <div class="">           
            <div class="cssStepInfo" style="padding-top:5px; text-align:center;">Driver Details</div>     
        </div>
    </asp:LinkButton>
</div>
