<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptSubLocation_M.aspx.vb" MaintainScrollPositionOnPostback="true" Inherits="Transport_tptSubLocation_M" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript" >
   function confirm_delete()
{
  
  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
   return true;
  else
    return false;
   
 } 
</script>

      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Area Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center" width="100%">
            
            <tr >
               <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:RequiredFieldValidator ID="rfsublocation" runat="server" ErrorMessage="Please enter the field Sub Location" ControlToValidate="txtSubLocation" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
               </td>  
                        </tr>
            <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                   HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" Width="100%" Height="1px" />
                
                </td>
                
        </tr>            
                        
           <tr><td align="center"  >
         <table id="Table1" runat="server" align="center" width="100%" >
        
         
                    
                    <tr><td>
                    
             <table id="Table2" runat="server" align="center" width="100%" >         
            <tr>
             <td align="left" width="20%">
                    <asp:Label ID="lblLocText" runat="server" CssClass="field-label" Text="Select Location"></asp:Label>
             </td>
                    <td align="left" width="30%">
                    <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                <td width="20%"></td>
                 <td width="30%"></td>
                           </tr>
                    
            </table>
            </td></tr>  
                      
            <tr>
           <td align="left" class="title-bg" >
           Details
            </td>
             </tr>
             
            <tr>
            <td align="center">
          
             <table id="Table3" runat="server" align="center" width="100%" > 
                      
                           <tr>
                       <td align="left" width="20%">
                    <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Sub Location"></asp:Label></td>
                 <td align="left" width="30%">
                     <asp:TextBox ID="txtSubLocation" runat="server"></asp:TextBox></td>
                     
                   
                        
                   <td align="left" width="20%">
                       <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1"/></td>
                 
                 <td width="30%"></td>
                   
              </tr>
        
              
               </table>
                  
                  </td>
                  </tr>
                  
             <tr>
             <td align="left">    
          
            <table id="Table4" runat="server" align="center" width="100%" >         
                                       
            <tr><td align="center"  >
             <asp:GridView ID="gvtptSubLocation" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     PageSize="20"  BorderStyle="None">
                     <Columns>
                     
                      <asp:TemplateField HeaderText="loc_id" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lbllocId" runat="server" Text='<%# Bind("sbl_loc_id") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="acd_id" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblSblId" runat="server" Text='<%# Bind("sbl_id") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                         <asp:TemplateField HeaderText="Location"  >
                      <ItemTemplate>
                      <asp:Label ID="lblLocation" runat="server"  text='<%# Bind("loc_description")  %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>   
                                  
                      <asp:TemplateField HeaderText="Sub Location"  >
                      <ItemTemplate>
                      <asp:Label ID="lblSubLocation" runat="server" text='<%# Bind("sbl_description")  %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                     
                      <asp:TemplateField HeaderText="-" visible="false"  >
                      <ItemTemplate>
                      <asp:Label ID="lblflag" runat="server" text='<%# Bind("flag")  %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                                        
                       <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="edit"  >
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                     
                         <asp:ButtonField CommandName="delete"  HeaderText="Delete" Text="delete"  >
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                       
                       </Columns>  
                         <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                          <RowStyle CssClass="griditem" Wrap="False" />
                          <SelectedRowStyle CssClass="Green" Wrap="False" />
                          <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                          <EmptyDataRowStyle Wrap="False" />
                          <EditRowStyle Wrap="False" />
                     </asp:GridView>
                     </td></tr> 
          </table>
           
           </td></tr>
           
            <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" CausesValidation="False" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
               </td>
        </tr>
                      </table>
               <asp:HiddenField ID="hfGRM_ID" runat="server" /><asp:HiddenField ID="hfDisplay" runat="server" /><asp:HiddenField ID="hfGrm_MaxCapacity" runat="server" />
           
           </td></tr>
          
        </table>

            </div>
        </div>
    </div>
</asp:Content>

