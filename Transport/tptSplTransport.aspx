<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptSplTransport.aspx.vb" Inherits="Transport_tptSplTransport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Special Transport Arrangement Details"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters">
                            <asp:LinkButton ID="lnkButton" runat="server">Add New</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>

                    <tr>

                        <td colspan="4">
                            <asp:GridView ID="gvComments" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="25" Width="100%" AllowPaging="true">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CMTID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="cmtId" runat="server" Text='<%# bind("STA_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Date"></asp:Label><br />
                                            <asp:TextBox ID="txtEnqDate" runat="server"  ></asp:TextBox>
                                            <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnEnq_Date_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server"  Text='<%# Bind("STA_DATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle  HorizontalAlign="Center" ></HeaderStyle>
                                        <ItemStyle   HorizontalAlign="Center" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Program">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblAccHeader" runat="server" Text="Program" CssClass="gridheader_text"></asp:Label><br />
                                            <asp:TextBox ID="txtAccSearch" runat="server"  ></asp:TextBox>
                                            <asp:ImageButton ID="btnAcc_Search" OnClick="btnAcc_Search_Click" runat="server"
                                                ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblclm" runat="server"   Text='<%# Bind("STA_PGM") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField CommandName="Edit" Text="Edit" HeaderText="Edit">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:ButtonField CommandName="Print" Text="Print" HeaderText="Print">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:ButtonField CommandName="Delete" Text="Delete" HeaderText="Delete">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" Height="30px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>

                        </td>
                    </tr>

                </table>
                &nbsp;
               <asp:HiddenField ID="hfTRD_ID" runat="server" />
                &nbsp; &nbsp;&nbsp;<asp:HiddenField ID="hfTRP_ID" runat="server" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input
                    id="h_Selected_menu_12" runat="server" type="hidden" value="=" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

