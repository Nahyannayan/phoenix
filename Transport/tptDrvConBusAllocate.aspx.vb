Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_tptDrvConBusAllocate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000460") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    BindBusNo()
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub
    Protected Sub ddlDriver_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlDriver As DropDownList = DirectCast(sender, DropDownList)
        Dim txtDrvMobile As TextBox = TryCast(sender.FindControl("txtDrvMobile"), TextBox)
        txtDrvMobile.Text = GetEmpMobilNo(ddlDriver.SelectedValue.ToString)
    End Sub
    Protected Sub ddlConductor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlConductor As DropDownList = DirectCast(sender, DropDownList)
        Dim txtConMobile As TextBox = TryCast(sender.FindControl("txtConMobile"), TextBox)
        txtConMobile.Text = GetEmpMobilNo(ddlConductor.SelectedValue.ToString)
    End Sub
    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)

            Dim lblTrdId As Label = TryCast(sender.FindControl("lblTrdId"), Label)
            Dim lblDrvId As Label = TryCast(sender.FindControl("lblDrvId"), Label)
            Dim lblConId As Label = TryCast(sender.FindControl("lblConId"), Label)
            Dim lblDrvMobile As Label = TryCast(sender.FindControl("lblDrvMobile"), Label)
            Dim lblConMobile As Label = TryCast(sender.FindControl("lblConMobile"), Label)
            Dim lblVehId As Label = TryCast(sender.FindControl("lblVehId"), Label)
            Dim lblBnoId As Label = TryCast(sender.FindControl("lblBnoId"), Label)

            Dim lblDriver As Label = TryCast(sender.FindControl("lblDriver"), Label)
            Dim lblConductor As Label = TryCast(sender.FindControl("lblConductor"), Label)
            Dim lblVehicle As Label = TryCast(sender.FindControl("lblVehicle"), Label)

            Dim ddlDriver As DropDownList = TryCast(sender.FindControl("ddlDriver"), DropDownList)
            Dim ddlConductor As DropDownList = TryCast(sender.FindControl("ddlConductor"), DropDownList)
            Dim ddlVehicle As DropDownList = TryCast(sender.FindControl("ddlVehicle"), DropDownList)

            Dim txtDrvMobile As TextBox = TryCast(sender.FindControl("txtDrvMobile"), TextBox)
            Dim txtConMobile As TextBox = TryCast(sender.FindControl("txtConMobile"), TextBox)


            If lblEdit.Text = "Edit" Then
                lblEdit.Text = "Update"

                ddlDriver.Visible = True
                ddlConductor.Visible = True
                ddlVehicle.Visible = True
                txtDrvMobile.Visible = True
                txtConMobile.Visible = True

                BindDriver(ddlDriver)
                If Not ddlDriver.Items.FindByValue(lblDrvId.Text) Is Nothing Then
                    ddlDriver.Items.FindByValue(lblDrvId.Text).Selected = True
                End If

                BindConductor(ddlConductor)
                If Not ddlConductor.Items.FindByValue(lblConId.Text) Is Nothing Then
                    ddlConductor.Items.FindByValue(lblConId.Text).Selected = True
                End If

                BindVehicle(ddlVehicle)
                If Not ddlVehicle.Items.FindByValue(lblVehId.Text) Is Nothing Then
                    ddlVehicle.Items.FindByValue(lblVehId.Text).Selected = True
                End If

                txtDrvMobile.Text = lblDrvMobile.Text
                txtConMobile.Text = lblConMobile.Text
            Else
                lblEdit.Text = "Edit"

                SaveData(lblBnoId.Text, ddlDriver.SelectedValue.ToString, _
                         ddlConductor.SelectedValue.ToString, ddlVehicle.SelectedItem.Value, _
                         txtDrvMobile.Text, txtConMobile.Text)

                lblDriver.Text = ddlDriver.SelectedItem.Text + "(Mob# " + txtDrvMobile.Text + ")"
                lblConductor.Text = ddlConductor.SelectedItem.Text + "(Mob# " + txtConMobile.Text + ")"
                lblDrvMobile.Text = txtDrvMobile.Text
                lblConMobile.Text = txtConMobile.Text
                lblVehicle.Text = ddlVehicle.SelectedItem.Text

                ddlDriver.Visible = False
                ddlConductor.Visible = False
                ddlVehicle.Visible = False
                txtDrvMobile.Visible = False
                txtConMobile.Visible = False
                lblError.Text = "Record Saved Successfully"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "PrivateMethods"

    Function GetEmpMobilNo(ByVal empId As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "select isnull(EMD_CUR_MOBILE,'') from employee_d where emd_emp_id=" + empId
        Dim mobile As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return mobile
    End Function
    Private Sub BindVehicle(ByVal ddlVehicle As DropDownList)
        ddlVehicle.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        str_query = "select veh_id,veh_regno from transport.vv_vehicle_m where VEH_ALTO_BSU_ID='" + Session("sbsuid") + "' " _
                  & " order by veh_regno"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlVehicle.DataSource = ds
        ddlVehicle.DataTextField = "veh_regno"
        ddlVehicle.DataValueField = "veh_id"
        ddlVehicle.DataBind()

    End Sub

    Sub BindDriver(ByVal ddlDriver As DropDownList)
        ddlDriver.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  emp_name,emp_id FROM VV_DRIVER where bsu_alto_id='" + Session("sBsuid") + "'" _
                                 & " ORDER BY EMP_NAME"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDriver.DataSource = ds
        ddlDriver.DataTextField = "emp_name"
        ddlDriver.DataValueField = "emp_id"
        ddlDriver.DataBind()
    End Sub

    Sub BindConductor(ByVal ddlConductor As DropDownList)
        ddlConductor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT emp_name,emp_id FROM VV_CONDUCTOR where bsu_alto_id='" + Session("sBsuid") + "'" _
                                    & " ORDER BY EMP_NAME"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlConductor.DataSource = ds
        ddlConductor.DataTextField = "emp_name"
        ddlConductor.DataValueField = "emp_id"
        ddlConductor.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT DISTINCT BNO_DESCR+'-'+REPLACE(TRP_DESCR,'-Onward','')  AS BNO_DESCR," _
                                 & " DRV_NAME+'(Mob# '+DRVMOBILE+')' AS DRIVER,CON_NAME+'(Mob# '+CONMOBILE+')' AS CONDUCTOR," _
                                 & " TRD_DRIVER_EMP_ID AS DRV_ID,TRD_CONDUCTOR_EMP_ID AS CON_ID,VEH_REGNO,VEH_ID," _
                                 & " TRD_ID,TRD_BNO_ID,DRVMOBILE,CONMOBILE FROM TRANSPORT.VV_TRIPS_D WHERE TRP_JOURNEY='ONWARD' AND TRD_BSU_ID='" + Session("SBSUID") + "'"

        If ddlBusNo.SelectedValue <> "0" Then
            str_query += " AND TRD_BNO_ID=" + ddlBusNo.SelectedValue.ToString
        End If

        str_query += " ORDER BY BNO_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTpt.DataSource = ds
        gvTpt.DataBind()
    End Sub

    Sub BindBusNo()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BNO_DESCR,BNO_ID FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + Session("SBSUID") + "'" _
                                & " ORDER BY BNO_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlBusNo.Items.Insert(0, li)
    End Sub

    Sub SaveData(ByVal bnoid As String, ByVal drvid As String, ByVal conid As String, ByVal vehid As String, ByVal drvmobile As String, ByVal conmobile As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.saveBUSALLOCATION " _
                               & bnoid + "," _
                               & drvid + "," _
                               & conid + "," _
                               & vehid + "," _
                               & "'" + drvmobile + "'," _
                               & "'" + conmobile + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
#End Region

    Protected Sub ddlBusNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusNo.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub gvTpt_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTpt.PageIndexChanging
        gvTpt.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
End Class
