Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class ShowEmpDetail
    Inherits System.Web.UI.Page
    'Protected PageTitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim SearchMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Try
                SearchMode = Request.QueryString("id")
                'If SearchMode = "H" Then
                'ElseIf SearchMode = "B" Then
                '    Page.Title = "Business Unit Info"

                'ElseIf SearchMode = "C" Then
                '    Page.Title = "Category Info"
                'ElseIf SearchMode = "D" Then
                '    Page.Title = "Deduction Info"

                'End If
                'Page.DataBind()

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                gridbind()
            Catch ex As Exception

            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvCommInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvCommInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTransportConnection.ConnectionString
            Dim str_Sql As String = ""
            SearchMode = Request.QueryString("id")
            Dim str_filter_code As String
            Dim str_filter_Descr As String

            'Get all Bank details
            If SearchMode = "H" Then
                str_Sql = "Select CommID, Code,Descr from(SELECT [HPM_ID] as CommID,[HPM_CODE] as Code,[HPM_NAME] as Descr FROM [OASIS_TRANSPORT].[TRANSPORT].[VW_HYPOTHICATED_M])a where a.CommID<>''"
                'Getting all the supplier
            ElseIf SearchMode = "S" Then
                str_Sql = "Select CommID, Code,Descr from(SELECT [ACT_ID] as CommID,[ACT_ID] as Code,[ACT_name] as Descr FROM [OASIS_TRANSPORT].[dbo].[VW_OSF_Suppliers])a where a.CommID<>''"
                'Get all the BSU
            ElseIf SearchMode = "BO" Or SearchMode = "BP" Or SearchMode = "BA" Then
                str_Sql = "Select CommID, Code,Descr from(SELECT [BSU_ID] as CommID,[BSU_SHORTNAME]as Code,[BSU_NAME]as Descr  FROM [OASIS_TRANSPORT].[dbo].[VW_BUSINESSUNIT_M])a where a.CommID<>''"

                'Get Category
            ElseIf SearchMode = "C" Then
                str_Sql = "Select CommID, Code,Descr from(SELECT [CAT_ID] as CommID ,[CAT_ID] as Code,[CAT_DESCRIPTION] as Descr FROM [OASIS_TRANSPORT].[TRANSPORT].[VV_CATEGORY_M])a where a.CommID<>''"
                'Get all the Make
            ElseIf SearchMode = "M" Then
                str_Sql = "Select CommID, Code,Descr from(SELECT [VMK_ID] as CommID ,[VMK_CODE] as Code,[VMK_MAKE] as Descr  FROM [OASIS_TRANSPORT].[TRANSPORT].[VW_VMK_MAKE])a where a.CommID<>'' "
                'Get Insurance
            ElseIf SearchMode = "I" Then
                str_Sql = "Select CommID, Code,Descr from(SELECT [INC_ID] as CommID,[INC_CODE] as Code,[INC_NAME] as Descr FROM [OASIS_TRANSPORT].[TRANSPORT].[VW_INSURANCE_M])a where a.CommID<>''"
            End If

            Dim ds As New DataSet

            Dim lblCode As New Label
            Dim lblDescr As New Label
            Dim txtSearch As New TextBox
            Dim str_txtCode, str_txtDescr As String
            Dim str_search As String
            str_txtCode = ""
            str_txtDescr = ""

            str_filter_code = ""
            str_filter_Descr = ""

            If gvCommInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvCommInfo.HeaderRow.FindControl("txtcode")
                str_txtCode = txtSearch.Text
                ''code

                If str_search = "LI" Then
                    str_filter_code = " AND a.Code LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND a.Code NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND a.Code LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND a.Code NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND a.Code LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND a.Code NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvCommInfo.HeaderRow.FindControl("txtName")
                str_txtDescr = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Descr = " AND a.Descr LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Descr = "  AND  NOT a.Descr LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Descr = " AND a.Descr  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Descr = " AND a.Descr  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Descr = " AND a.Descr LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Descr = " AND a.Descr NOT LIKE '%" & txtSearch.Text & "'"
                End If

            End If



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_Descr & "order by a.Descr")



            gvCommInfo.DataSource = ds.Tables(0)
            ' gvEmpInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCommInfo.DataBind()
                Dim columnCount As Integer = gvCommInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvCommInfo.Rows(0).Cells.Clear()
                gvCommInfo.Rows(0).Cells.Add(New TableCell)
                gvCommInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCommInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCommInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvEmpInfo.HeaderRow.Visible = True
            Else
                gvCommInfo.DataBind()


            End If



            If SearchMode = "BO" Or SearchMode = "BP" Or SearchMode = "BA" Then
                lblCode = gvCommInfo.HeaderRow.FindControl("lblCode")
                lblCode.Text = "Business ID"
                lblDescr = gvCommInfo.HeaderRow.FindControl("lblDescr")
                lblDescr.Text = "Business Unit Name"

            ElseIf SearchMode = "H" Then

                lblCode = gvCommInfo.HeaderRow.FindControl("lblCode")
                lblCode.Text = "Bank Code"
                lblDescr = gvCommInfo.HeaderRow.FindControl("lblDescr")
                lblDescr.Text = "Bank Name"
            ElseIf SearchMode = "C" Then

                lblCode = gvCommInfo.HeaderRow.FindControl("lblCode")
                lblCode.Text = "Category ID"
                lblDescr = gvCommInfo.HeaderRow.FindControl("lblDescr")
                lblDescr.Text = "Category Name"

            ElseIf SearchMode = "S" Then

                lblCode = gvCommInfo.HeaderRow.FindControl("lblCode")
                lblCode.Text = "Supplier Code"
                lblDescr = gvCommInfo.HeaderRow.FindControl("lblDescr")
                lblDescr.Text = "Supplier Name"
            ElseIf SearchMode = "M" Then

                lblCode = gvCommInfo.HeaderRow.FindControl("lblCode")
                lblCode.Text = "Make Code"
                lblDescr = gvCommInfo.HeaderRow.FindControl("lblDescr")
                lblDescr.Text = "Description"
            ElseIf SearchMode = "I" Then

                lblCode = gvCommInfo.HeaderRow.FindControl("lblCode")
                lblCode.Text = "Insurance Code"
                lblDescr = gvCommInfo.HeaderRow.FindControl("lblDescr")
                lblDescr.Text = "Description"
            End If


            txtSearch = gvCommInfo.HeaderRow.FindControl("txtcode")
            txtSearch.Text = str_txtCode
            txtSearch = gvCommInfo.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtDescr
            set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            ' Errorlog(ex.Message)
        End Try





    End Sub


    Protected Sub btnSearchEmpId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub



    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCommID As New Label
        Dim javaSr As String
        Dim lbClose As New LinkButton

        lbClose = sender

        lblCommID = sender.Parent.FindControl("lblCommID")

        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "||" & lblCommID.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")

        l_Str_Msg = UtilityObj.CleanupStringForJavascript(l_Str_Msg)
        l_Str_Msg = l_Str_Msg.Replace(vbCrLf, "")
        l_Str_Msg = l_Str_Msg.Replace(vbCr, "")
        l_Str_Msg = l_Str_Msg.Replace(vbLf, "")

        If (Not lblCommID Is Nothing) Then
            ' '' ''   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")


            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"


            javaSr = "function listen_window(){"
            javaSr += " var oArg = new Object();"
            javaSr += "oArg.NameandCode ='" & l_Str_Msg & "' ; "
            javaSr += "var oWnd = GetRadWindow('" & l_Str_Msg & "');"
            javaSr += "oWnd.close(oArg);"
            javaSr += "}"
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "POPUP", javaSr, True)

            h_SelectedId.Value = "Close"
        End If
    End Sub


    Protected Sub gvEmpInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCommInfo.PageIndexChanging
        gvCommInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

   
    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim javaSr As String
        Dim lbClose As New LinkButton


        lbClose = sender.Parent.FindControl("LinkButton1")


        lblcode = sender.Parent.FindControl("lblCommID")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "||" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")

        l_Str_Msg = UtilityObj.CleanupStringForJavascript(l_Str_Msg)
        l_Str_Msg = l_Str_Msg.Replace(vbCrLf, "")
        l_Str_Msg = l_Str_Msg.Replace(vbCr, "")
        l_Str_Msg = l_Str_Msg.Replace(vbLf, "")

        If (Not lblcode Is Nothing) Then
            ' ''   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")


            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"

            javaSr = "function listen_window(){"
            javaSr += " var oArg = new Object();"
            javaSr += "oArg.NameandCode ='" & l_Str_Msg & "' ; "
            javaSr += "var oWnd = GetRadWindow('" & l_Str_Msg & "');"
            javaSr += "oWnd.close(oArg);"
            javaSr += "}"
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "POPUP", javaSr, True)

            h_SelectedId.Value = "Close"
        End If
    End Sub

End Class
