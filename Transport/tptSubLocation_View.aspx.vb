Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptSubLocation_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                  Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000040") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
   
    Protected Sub btnLocation_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSubLocation_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
   
    Protected Sub ddlgvLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Transport\tptSubLocation_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvTptSubLocation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTptSubLocation.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvTptSubLocation.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblLocId As Label
                Dim lblLocation As Label
                With selectedRow
                    lblLocId = .Cells(1).FindControl("lblLocId")
                    lblLocation = .Cells(3).FindControl("lblLocation")
                End With
                Dim url As String
                url = String.Format("~\Transport\tptSubLocation_M.aspx?MainMnu_code={0}&datamode={1}&locid=" + Encr_decrData.Encrypt(lblLocId.Text) + "&location=" + Encr_decrData.Encrypt(lblLocation.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvTptSubLocation_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTptSubLocation.PageIndexChanging
        Try
            gvTptSubLocation.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTptSubLocation.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptSubLocation.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTptSubLocation.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptSubLocation.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT sbl_id,sbl_loc_id,loc_description,sbl_description" _
        '                        & " FROM tptsublocation_m A,tptlocation_m B WHERE " _
        '                        & " A.sbl_loc_id=B.loc_id AND  loc_bsu_id='" + Session("sbsuid") + "'"

        Dim str_query As String = "SELECT SBL_ID, SBL_LOC_ID,LOC_DESCRIPTION,SBL_DESCRIPTION " _
                                & "FROM TRANSPORT.LOCATION_M AS A INNER JOIN " _
                                & " TRANSPORT.SUBLOCATION_M AS B ON A.LOC_ID = B.SBL_LOC_ID" _
                                & " WHERE SBL_LOC_ID=LOC_ID "


        Dim selectedLocation As String
        Dim ddlgvLocation As New DropDownList
        Dim txtSearch As New TextBox
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim locSearch As String = ""
        Dim sblSearch As String = ""

        If gvTptSubLocation.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvTptSubLocation.HeaderRow.FindControl("txtLocation")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("loc_description", txtSearch.Text, strSearch)
            locSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvTptSubLocation.HeaderRow.FindControl("txtSubLocation")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("sbl_description", txtSearch.Text, strSearch)
            sblSearch = txtSearch.Text

            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If

        str_query += " ORDER BY LOC_DESCRIPTION,SBL_DESCRIPTION "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptSubLocation.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTptSubLocation.DataBind()
            Dim columnCount As Integer = gvTptSubLocation.Rows(0).Cells.Count
            gvTptSubLocation.Rows(0).Cells.Clear()
            gvTptSubLocation.Rows(0).Cells.Add(New TableCell)
            gvTptSubLocation.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTptSubLocation.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTptSubLocation.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTptSubLocation.DataBind()
        End If
        txtSearch = gvTptSubLocation.HeaderRow.FindControl("txtLocation")
        txtSearch.Text = locSearch

        txtSearch = gvTptSubLocation.HeaderRow.FindControl("txtSubLocation")
        txtSearch.Text = sblSearch

        set_Menu_Img()
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
 

#End Region
End Class
