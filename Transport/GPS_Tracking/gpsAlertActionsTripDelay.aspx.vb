﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_gpsAlertTdb
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("tmsection") = 0 Then

            Dim checkc = False
            For Each row As GridViewRow In gridInfo.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("CheckAction"), CheckBox)
                If check.Checked Then
                    checkc = True
                End If
            Next

            If checkc = False Then
                BindGrid()
                cpindex.Value = 1
            End If

            'If cp.Checked = False Then
            '    BindGrid()
            '    cpindex.Value = 1
            'End If


        Else

            If Request.QueryString("cpindex") <> Request.QueryString("tpage") Then
                cpindex.Value = Convert.ToInt16(Request.QueryString("cpindex")) + 1
                gridInfo.PageIndex = Request.QueryString("cpindex")
            Else
                cpindex.Value = 0
                gridInfo.PageIndex = Request.QueryString("cpindex")
            End If


            Dim checkc = False
            For Each row As GridViewRow In gridInfo.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("CheckAction"), CheckBox)
                If check.Checked Then
                    checkc = True
                End If
            Next

            If checkc = False Then
                BindGrid()
            End If



        End If



    End Sub

    Public Sub BindGrid()
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
         Dim pParms(6) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", Today.ToString("dd/MMM/yyyy"))

            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Request.QueryString("Bsu").Trim())


            Dim FromHr = "00"
            Dim FromMins = "00"
            Dim ToHr = "23"
            Dim ToMins = "59"


            Dim ds1 As DataSet = GetTripDetails(Request.QueryString("tripid").Trim())
            FromHr = ds1.Tables(0).Rows(0).Item("TRIP_FROM_HR").ToString()
            FromMins = ds1.Tables(0).Rows(0).Item("TRIP_FROM_MINS").ToString()
            ToHr = ds1.Tables(0).Rows(0).Item("TRIP_TO_HR").ToString()
            ToMins = ds1.Tables(0).Rows(0).Item("TRIP_TO_MINS").ToString()

            pParms(2) = New SqlClient.SqlParameter("@FromHr", FromHr)
            pParms(3) = New SqlClient.SqlParameter("@FromMins", FromMins)
            pParms(4) = New SqlClient.SqlParameter("@ToHr", ToHr)
            pParms(5) = New SqlClient.SqlParameter("@ToMins", ToMins)
            pParms(6) = New SqlClient.SqlParameter("@UNIT_ID", Request.QueryString("vehid"))

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_TRIP_DELAY_REPORT", pParms)

            gridInfo.DataSource = ds
            gridInfo.DataBind()

            Dim pval As Double = ds.Tables(0).Rows.Count / gridInfo.PageSize
            Dim pfval As Integer = 1
            If pval.ToString.Contains(".") Then

                If pval.ToString.Split(".")(1) > 0 Then
                    pfval = pval.ToString.Split(".")(0) + 1
                Else
                    pfval = pval.ToString.Split(".")(0)
                End If

            Else
                pfval = pval
            End If

            tpage.Value = pfval

        Catch ex As Exception

        End Try

    End Sub

    Public Function GetTripDetails(ByVal tipid As String) As DataSet

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 5)
        pParms(1) = New SqlClient.SqlParameter("@GPS_TRIP_ID", tipid)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)


        Return ds
    End Function


    Protected Sub gridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridInfo.PageIndexChanging
        gridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnaction.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        For Each row As GridViewRow In gridInfo.Rows
            Dim check As CheckBox = DirectCast(row.FindControl("CheckAction"), CheckBox)
            If check.Checked Then
                Dim BARCODE = DirectCast(row.FindControl("HiddenBARCODE"), HiddenField).Value
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@LCD_RECORD_ID", 0)
                pParms(1) = New SqlClient.SqlParameter("@REMARKS", txtaction.Text.Trim())
                pParms(2) = New SqlClient.SqlParameter("@TYPE", Request.QueryString("Type"))
                pParms(3) = New SqlClient.SqlParameter("@ACTION_BY", Request.QueryString("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@UNIT_ID", BARCODE)
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_ACTION_LOG", pParms)

            End If

        Next
        BindGrid()
    End Sub

End Class
