﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsSendSms.aspx.vb" Inherits="Transport_GPS_Tracking_gpsSendSms" %>

<%@ Register Src="UserControls/usrStudentdetails.ascx" TagName="usrStudentdetails" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Send SMS</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <base target="_self" />
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->
</head>
<body>
    <form id="form1" runat="server">


        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>

        <div align="center">


            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Send SMS
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="100%">
                            <tr>

                                <td align="left" colspan="2">
                                    <uc1:usrStudentdetails ID="usrStudentdetails1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="30%">
                                    <span class="field-label">SMS Text</span></td>
                                <td align="left" width="70%">
                                    <asp:TextBox ID="txtmessage" runat="server" EnableTheming="False" MaxLength="100" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="30%">
                                    <span class="field-label">Mobile Number</span></td>
                                <td align="left" width="70%">
                                    <asp:TextBox ID="txtmobile" runat="server"></asp:TextBox>
                                    <br />
                                    <span style="font-size: smaller;">Country Code+Mobile Number ( eg: 971xxxxxxxxx)</span>

                                </td>
                            </tr>
                            <tr>

                                <td align="center" colspan="2">
                                    <asp:Button ID="btnsend" runat="server" Text="Send" CssClass="button"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:FilteredTextBoxExtender ID="F1" FilterType="Numbers" TargetControlID="txtmobile" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
        </div>


        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
            ControlToValidate="txtmessage" Display="None"
            ErrorMessage="Please enter SMS text." SetFocusOnError="True"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
            ControlToValidate="txtmobile" Display="None"
            ErrorMessage="Please enter valid mobile number" SetFocusOnError="True"></asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server"
            ShowMessageBox="True" ShowSummary="False" />


    </form>
</body>
</html>
