﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsBarcodePickDropAlertOnwardShow.aspx.vb"
    Inherits="Transport_GPS_Tracking_gpsBarcodePickDropAlertShow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>On Ward Alert</title>
  <%--  <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <base target="_self" />


    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

</head>
<script type="text/javascript">
    window.setTimeout('setRefresh()', 10000);


    function setRefresh() {

        window.location = window.location
    }

</script>
<body>
    <form id="form1" runat="server">
        <div align="center">


            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Onward Trip Absent Alert
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="GridData" runat="server" EmptyDataText="No Records Found" CssClass="table table-bordered table-row"
                            AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Business unit
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("BSU_NAME") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Count
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="LinkShow" Text='<%#Eval("T_COUNT") %>' OnClientClick='<%#Eval("link") %>' runat="server"></asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle  />
                            <SelectedRowStyle  />
                            <HeaderStyle  />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>



        </div>

    </form>
</body>
</html>
