﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsLCDAlertActions.aspx.vb"
    Inherits="Transport_GPS_Tracking_gpsLCDAlertActions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%--    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <title>Actions</title>
    <base target="_self" />

    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />
</head>
<body>
    <script type="text/javascript">

        function ActionHistory(unit_id) {

            var actionvalue = '?unit_id=' + unit_id;
            //window.showModalDialog('gpsLCDAlertActionsList.aspx' + actionvalue, '', 'dialogHeight:600px;dialogWidth:800px;scroll:no;resizable:no;');
            return ShowWindowWithClose('gpsLCDAlertActionsList.aspx' + actionvalue, 'search', '55%', '85%')
            return false;

        }

    </script>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div>
                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                        <tr class="title-bg-lite">
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td align="left"><span class="field-label">Actions</span>  </td>
                                        <td align="right">
                                            <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/Images/close_red.gif"
                                                OnClientClick="javascript:window.close();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GridBusListing" runat="server" AutoGenerateColumns="False" ShowFooter="true" CssClass="table table-bordered table-row"
                                    Width="100%" AllowPaging="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Check">
                                            <HeaderTemplate>
                                                Check
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <asp:CheckBox ID="CheckAction" runat="server" />
                                                    <asp:HiddenField ID="HiddenLogid" Value='<%# Eval("RECORD_ID") %>' runat="server" />
                                                </center>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <center>
                                                    <asp:Button ID="btnActions" runat="server" CommandName="actions" CssClass="button"
                                                        Text="Action" />
                                                </center>
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reg.No.">
                                            <HeaderTemplate>
                                                Reg.No.
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%# Eval("VEH_REGNO") %>
                                                </center>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bus Type">
                                            <HeaderTemplate>
                                                Type
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%# Eval("CAT_DESCRIPTION") %>
                                                </center>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Unit">
                                            <HeaderTemplate>
                                                BSU
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%#Eval("BSU_SHORTNAME")%>
                                                </center>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Driver">
                                            <HeaderTemplate>
                                                Driver
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("Driver")%>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Conductor">
                                            <HeaderTemplate>
                                                Conductor
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("Conductor") %>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Today's Distance Travelled">
                                            <HeaderTemplate>
                                                Distance Travelled
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("kM")%>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Current Speed">
                                            <HeaderTemplate>
                                                Current Speed
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%#Eval("SPEED")%>
                                                </center>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Today's Max Speed">
                                            <HeaderTemplate>
                                                Max Speed
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%# Eval("maxspeed")%>
                                                </center>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Today's Panic Time">
                                            <HeaderTemplate>
                                                Panic Time
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%# Eval("PANIC_ALERT_TIME")%>
                                                </center>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Today's Speed Alert">
                                <HeaderTemplate>
                                    <span style="font-size: small;">Speed Alert</span>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                       
                                        <asp:Image ID="ImageSpeedAlert" runat="server" Height="20px" ImageAlign="Top" 
                                            ImageUrl="~/Images/alertRed.gif" Width="20px" />
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Panic Alert">
                                <HeaderTemplate>
                                    <span style="font-size: small;">Panic Alert</span>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:Image ID="ImagePanicAlert" runat="server" Height="20px" ImageAlign="Top" 
                                            ImageUrl="~/Images/alertRed.gif" Width="20px" />
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Today's Geo Fence Alert">
                                            <HeaderTemplate>
                                                Geo Fence Alert
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <asp:Image ID="ImageGeoAlert" runat="server" Height="20px" Visible='<%# Eval("VisibleGEO")%>'
                                                        ImageAlign="Top" ImageUrl="~/Images/GPS_Images/Geo.png" Width="20px" />
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkHistory" Visible='<%# Eval("HistoryVisible")%>' OnClientClick='<%# Eval("ActionHistory")%>' runat="server">History</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle />
                                    <RowStyle CssClass="griditem" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <SelectedRowStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <EmptyDataRowStyle />
                                    <EditRowStyle />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <asp:Panel ID="PanelTitleAdd" runat="server" CssClass="panel-cover"
                        Style="display: none">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" class="title-bg-lite">Action &amp; Remarks
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Remarks</span>             </td>

                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtremarks" runat="server" EnableTheming="False" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            <td align="left" width="20%"><span class="field-label">Action Type</span>  </td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddactiontype" runat="server">
                                                    <asp:ListItem Text="Max Speed" Value="MAXSPEED" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Panic" Value="PANIC"></asp:ListItem>
                                                    <asp:ListItem Text="Geo Fence" Value="GEOFENCE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" colspan="4">
                                                <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnok" runat="server" CssClass="button" OnClick="btnok_Click" Text="Ok" ValidationGroup="s" />
                                                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" OnClick="btncancel_Click" /><br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                </div>
                <asp:Label ID="Linktemp" runat="server" Text=""></asp:Label>
                <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="True" TargetControlID="Linktemp"
                    PopupControlID="PanelTitleAdd" DynamicServicePath="" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>

                <asp:HiddenField ID="HiddenBsuid" runat="server" />
                <asp:HiddenField ID="Hiddenemp_id" runat="server" />
                <asp:HiddenField ID="Hiddensearch" runat="server" />
                <%--<asp:HiddenField ID="Hiddenfromspeed" runat="server" />
    <asp:HiddenField ID="Hiddentospeed" runat="server" />
    <asp:HiddenField ID="Hiddenpanic" runat="server" />
    <asp:HiddenField ID="Hiddengeofence" runat="server" />--%>
            </ContentTemplate>
        </asp:UpdatePanel>


        <script type="text/javascript" lang="javascript">
            function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                $.fancybox({
                    type: 'iframe',
                    //maxWidth: 300,
                    href: gotourl,
                    //maxHeight: 600,
                    fitToView: true,
                    padding: 6,
                    width: w,
                    height: h,
                    autoSize: false,
                    openEffect: 'none',
                    showLoading: true,
                    closeClick: true,
                    closeEffect: 'fade',
                    'closeBtn': true,
                    afterLoad: function () {
                        this.title = '';//ShowTitle(pageTitle);
                    },
                    helpers: {
                        overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                        title: { type: 'inside' }
                    },
                    onComplete: function () {
                        $("#fancybox-wrap").css({ 'top': '90px' });

                    },
                    onCleanup: function () {
                        var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                        if (hfPostBack == "Y")
                            window.location.reload(true);
                    }
                });

                return false;
            }
        </script>



    </form>
</body>
</html>
