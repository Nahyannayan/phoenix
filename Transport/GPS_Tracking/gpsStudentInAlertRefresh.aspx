﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsStudentInAlertRefresh.aspx.vb" Inherits="Transport_GPS_Tracking_gpsStudentInAlertRefresh" %>

<%@ Register Src="UserControls/gpsStudentInAlertRefresh.ascx" TagName="gpsStudentInAlertRefresh" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Student- Status In Monitoring</title>
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
</head>

<body>
    <form id="form1" runat="server">
        <div>

            <div class="card mb-3">
                <div class="card-header letter-space">
                    <i class="fa fa-users mr-3"></i>
                    Student Alert (Status IN)
                </div>
                <div class="card-body">
                    <div class="table-responsive m-auto">


                        <uc1:gpsStudentInAlertRefresh ID="gpsStudentInAlertRefresh1" runat="server" />
                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
