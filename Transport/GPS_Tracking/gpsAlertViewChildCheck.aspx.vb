﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Partial Class Transport_GPS_Tracking_gpsAlertViewChildCheck
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScriptManager1 As ScriptManager = Page.FindControl("ScriptManager1")
        ScriptManager1.RegisterPostBackControl(CheckPause)
        ScriptManager1.RegisterPostBackControl(btnSearch)

        If Not IsPostBack Then
            btnActionALL.Attributes.Add("disabled", "true")
            Hiddenbsuid.Value = Session("sBsuid")
            BindBanner()
            BindBsu()
            ' BindGrid()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If
    End Sub
    Public Sub BindBanner()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = "select BST_BSU_OPRT_ID from dbo.BSU_TRANSPORT " & _
                        " where BST_BSU_ID='" & Hiddenbsuid.Value & "' and BST_BSU_OPRT_ID<>'" & Hiddenbsuid.Value & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim bsu_id = ds.Tables(0).Rows(0).Item("BST_BSU_OPRT_ID").ToString()

            If bsu_id = "900501" Then ''STS

                Image1.ImageUrl = "~/Images/Schools/STS/sts_headerbaord_bg.jpg"

            ElseIf bsu_id = "900500" Then ''Bright Bus

                Image1.ImageUrl = "~/Images/Schools/BBT/BrightBus.jpg"

            Else ''STS

                Image1.ImageUrl = "~/Images/Schools/STS/sts_headerbaord_bg.jpg"

            End If

        Else ''STS

            Image1.ImageUrl = "~/Images/Schools/STS/sts_headerbaord_bg.jpg"

        End If


    End Sub

    Public Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@pDATE", System.DateTime.Now.ToShortDateString())


            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", IIf(ddlbsu.SelectedValue = "0" Or ddlbsu.SelectedValue = "", DBNull.Value, ddlbsu.SelectedValue.ToString()))
            pParms(2) = New SqlClient.SqlParameter("@VEH_REGNO", IIf(txtVehRegNo.Text = "", DBNull.Value, txtVehRegNo.Text))
            pParms(3) = New SqlClient.SqlParameter("@FK_UNITID", IIf(txtUnitId.Text = "", DBNull.Value, txtUnitId.Text))
            pParms(4) = New SqlClient.SqlParameter("@ACTION", IIf(ddlActionTaken.SelectedValue = "2", DBNull.Value, ddlActionTaken.SelectedValue.ToString()))

            pParms(5) = New SqlClient.SqlParameter("@pOption", "1")
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS_GPS].[ALRT].[MISSING_CHILD_CHECK]", pParms)
            GridBusListing.DataSource = ds
            GridBusListing.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If PnlActiontaken.Visible = False And CheckPause.Checked = False Then
            If GridBusListing.CurrentPageIndex = GridBusListing.PageCount - 1 Then
                GridBusListing.CurrentPageIndex = 0
            Else
                GridBusListing.CurrentPageIndex = GridBusListing.CurrentPageIndex + 1
            End If
            BindGrid()
        End If

    End Sub

#Region "search"
    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME +'('+ isnull(BSU_shortname,'') +')' as BSU_NAME  FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlbsu.DataSource = dsBsu
            ddlbsu.DataTextField = "BSU_NAME"
            ddlbsu.DataValueField = "BSU_ID"
            ddlbsu.DataBind()

            Dim list As New RadComboBoxItem
            list.Text = "All"
            list.Value = "0"
            ddlbsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME +'('+ isnull(BSU_shortname,'') +')' as BSU_NAME  FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlbsu.DataSource = dsBsu
            ddlbsu.DataTextField = "BSU_NAME"
            ddlbsu.DataValueField = "BSU_ID"
            ddlbsu.DataBind()
        End If


    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub
    Protected Sub btnSearchClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchClear.Click
        clearSearch()
    End Sub
    Sub clearSearch()
        ddlbsu.SelectedValue = "0"
        txtVehRegNo.Text = ""
        txtUnitId.Text = ""
        ddlActionTaken.SelectedValue = "0"
    End Sub
#End Region
    Protected Sub lnkbtnAction_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkselect As CheckBox = TryCast(sender.parent.FindControl("chkselect"), CheckBox)
        chkselect.Checked = True
        txtReasonAct.Text = ""
        PnlActiontaken.Visible = True
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim UNITID As New StringBuilder()
        Try
            UNITID.Append("<GPS>")
            For Each gv_row As GridDataItem In GridBusListing.Items
                Dim chkselect As CheckBox = TryCast(gv_row.FindControl("chkselect"), CheckBox)
                Dim HF_FK_UNITID As HiddenField = TryCast(gv_row.FindControl("HF_FK_UNITID"), HiddenField)
                Dim HF_REP100_POS As HiddenField = TryCast(gv_row.FindControl("HF_REP100_POS"), HiddenField)
                Dim HF_DELAYED_BY As HiddenField = TryCast(gv_row.FindControl("HF_DELAYED_BY"), HiddenField)
                If chkselect.Checked = True Then
                    Dim GpsLog As String = " <GPS_LOGS FK_UNITID='" + HF_FK_UNITID.Value + "' POS_LOG='" + HF_REP100_POS.Value + "' DELAYED_BY='" + HF_DELAYED_BY.Value + "' ></GPS_LOGS> "
                    UNITID.Append(GpsLog)
                    'UNITID.Append("|")
                End If
            Next
            UNITID.Append("</GPS>")
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString)
                conn.Open()
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim pParms(8) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@UNIT_ID", UNITID.ToString())
                    pParms(1) = New SqlClient.SqlParameter("@LOG_ACTION_USER", Session("sUsr_name"))
                    pParms(2) = New SqlClient.SqlParameter("@LOG_REMARKS", txtReasonAct.Text)
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[OASIS_GPS].[ALRT].[SAVE_LOG_CHILD_CHECK]", pParms)
                    transaction.Commit()
                    lblerror.Text = "Saved sucessfully."
                    PnlActiontaken.Visible = False
                    BindGrid()
                Catch ex As Exception
                    lblerrorAction.Text = "Save failed."
                    lblerror.Text = "Save failed."
                    transaction.Rollback()
                Finally
                    conn.Close()
                End Try
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lbtnActionClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnActionClose.Click
        PnlActiontaken.Visible = False
    End Sub

    Protected Sub GridBusListing_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GridBusListing.NeedDataSource
        BindGrid()
    End Sub
    Protected Sub GridBusListing_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles GridBusListing.ItemDataBound

        Dim a As String = e.Item.ItemType.ToString()
        If e.Item.ItemType = GridItemType.AlternatingItem Or e.Item.ItemType = GridItemType.Item Then

            Dim lnkbtnAction As LinkButton = DirectCast(e.Item.FindControl("lnkbtnAction"), LinkButton)
            Dim ScriptManager1 As ScriptManager = Page.FindControl("ScriptManager1")
            ' ScriptManager1.RegisterPostBackControl(lnkbtnAction)

            Dim HF_LOG_ACTION As HiddenField = DirectCast(e.Item.FindControl("HF_LOG_ACTION"), HiddenField)
            Dim chkselect As CheckBox = TryCast(e.Item.FindControl("chkselect"), CheckBox)
            Dim lnkActionView As LinkButton = TryCast(e.Item.FindControl("lnkActionView"), LinkButton)
            Dim pnlActionView As Panel = TryCast(e.Item.FindControl("pnlActionView"), Panel)

            If HF_LOG_ACTION.Value = "1" Then
                lnkbtnAction.Visible = False
                lnkActionView.Visible = True
                chkselect.Enabled = False
                pnlActionView.Visible = True

            Else
                lnkbtnAction.Visible = True
                lnkActionView.Visible = False
                pnlActionView.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnActionALL_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtReasonAct.Text = ""
        PnlActiontaken.Visible = True
    End Sub
End Class
