﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsTripDelayReport.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsTripDelayReport" %>

<%@ Register Src="UserControls/gpsTripDelayReport.ascx" TagName="gpsTripDelayReport" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Trip Delay Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc1:gpsTripDelayReport ID="gpsTripDelayReport1" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>
