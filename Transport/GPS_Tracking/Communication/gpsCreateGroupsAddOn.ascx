﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsCreateGroupsAddOn.ascx.vb"
    Inherits="Transport_GPS_Tracking_SMS_gpsCreateGroupsAddOn" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->



<script type="text/javascript">

    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

    function change_chk_stateg2(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch2") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>
<div>


    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Groups/Add On Creation</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Group Description</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdesc" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">User Type</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddusertype" runat="server" AutoPostBack="True">
                                <asp:ListItem Text="STUDENT" Value="STUDENT"></asp:ListItem>
                                <asp:ListItem Text="STAFF" Value="STAFF"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Type</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddtype" runat="server" AutoPostBack="True">
                                <asp:ListItem Text="Transport" Value="T"></asp:ListItem>
                                <asp:ListItem Text="Non-Transport" Value="NT"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr id="TR1" runat="server">
                        <td align="left" width="20%"><span class="field-label">Bus No</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddvehRegNo" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="TR2" runat="server">
                        <td align="left" width="20%"><span class="field-label">GPS Unit Id</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddgpsunit" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="TR3" runat="server">
                        <td align="left" width="20%"><span class="field-label">Trip Onward</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddtriponward" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="TR4" runat="server">
                        <td align="left" width="20%"><span class="field-label">Trip Return</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddtripreturn" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Group Type</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddGropType" runat="server">
                                <asp:ListItem Text="Groups" Value="G"></asp:ListItem>
                                <asp:ListItem Text="Add On" Value="AD"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>

                        <td align="center" colspan="4">
                            <asp:Button ID="btnpreview" runat="server" CssClass="button" Text="Preview" />
                        </td>
                    </tr>
                    <tr>

                        <td align="left" colspan="4">
                            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>

    <br />

    <table id="TBL2" border="0" cellpadding="0" cellspacing="0"
        width="100%" runat="server">
        <tr>
            <td align="left" class="title-bg-lite">User List</td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" ShowFooter="true" CssClass="table table-bordered table-row"
                    AutoGenerateColumns="false" EmptyDataText="Information not available."
                    Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Check
                                       <br />
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                    ToolTip="Click here to select/deselect all rows" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:CheckBox ID="ch1" runat="server" />
                                </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                            <asp:Button ID="btnadd" runat="server" CssClass="button" CommandName="add" Text="Add" />
                        </center>

                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User ID
                                      <br />
                                <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />



                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("USR_ID")%>
                                <asp:HiddenField ID="HiddenUserid" Value='<%# Eval("USR_ID")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User Name
                                              <br />
                                <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageSearch2" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Details
                                               <br />
                                <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageSearch3" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                           <%# Eval("DETAILS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact Name
                                               <br />
                                <asp:TextBox ID="Txt4" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageSearch4" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("CONTACT_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact No
                                               <br />
                                <asp:TextBox ID="Txt5" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageSearch5" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                            <%# Eval("MOBILE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Email Id
                                               <br />
                                <asp:TextBox ID="Txt6" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageSearch6" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("EMAIL")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>

                <br />

                <asp:GridView ID="GridInfo0" runat="server" ShowFooter="true" CssClass="table table-bordered table-row"
                    AutoGenerateColumns="false"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Check
                                       <br />
                                <asp:CheckBox ID="chkAll0" runat="server" onclick="javascript:change_chk_stateg2(this);"
                                    ToolTip="Click here to select/deselect all rows" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:CheckBox ID="ch2" runat="server" />
                                </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                            <asp:Button ID="btndelete" runat="server" CssClass="button" CommandName="deleting" Text="Delete" />
                        </center>

                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User ID
                              
                              
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("USR_ID")%>
      <asp:HiddenField ID="HiddenUserid" Value='<%# Eval("USR_ID")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User Name
                                         

                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Details
                                         

                                     
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                           <%# Eval("DETAILS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact Name
                                            

                                      
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("CONTACT_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact No
                                             

                                   
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                            <%# Eval("MOBILE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Email Id
                                           

                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("EMAIL")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table>

</div>
