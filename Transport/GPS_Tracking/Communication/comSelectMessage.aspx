﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSelectMessage.aspx.vb"
    Inherits="Transport_GPS_Tracking_Communication_comSelectMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Templates</title>
    <base target="_self" />
    <%--    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript">

        function Openss(a) {
            window.returnValue = a;
            window.close();

        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">SMS Templates
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="GridMSMS" AutoGenerateColumns="false" Width="100%" runat="server" CssClass="table table-bordered table-row"
                            AllowPaging="True" EmptyDataText="No SMS templates added yet (or) Search query did not produce any results"
                            OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="10">
                            <Columns>
                                <asp:TemplateField HeaderText="Sl No">
                                    <HeaderTemplate>
                                        Sl No
                                                <br />
                                        <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="Hiddenid" Value='<%# Eval("CMS_ID") %>' runat="server" />
                                        <center>
                                            <%# Eval("CMS_ID") %></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sms Text">
                                    <HeaderTemplate>
                                        Sms Text
                                                <br />
                                        <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                        <asp:Panel ID="T12Panel1" runat="server" Height="50px">
                                            <%#Eval("cms_sms_text")%>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                            CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                            TextLabelID="T12lblview">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Select">
                                    <HeaderTemplate>

                                        <br />
                                        Select
                                                <br />
                                        <br />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="lnksend" OnClientClick='<%#Eval("OPENW")%>' runat="server">Select</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
