﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSendLog.aspx.vb" Inherits="Transport_GPS_Tracking_Communication_xml_comSendLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />
    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->
    <%-- <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">


        function Opnewindow(logid) {

            window.showModalDialog("comPreviewSentData.aspx?logid=" + logid, "", "dialogWidth:800px; dialogHeight:600px; center:yes");


        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg">Send Report &nbsp;
                    <asp:LinkButton ID="Linkexport" runat="server">Export</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                            PageSize="20" EmptyDataText="Information not available." Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        ID
                                                <br />
                                        <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                            ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("SEND_UNIQUE_ID")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Name
                                                <br />
                                        <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                            ID="ImageSearch2" CausesValidation="false" runat="server" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("NAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        To
                                                <br />
                                        <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                            ID="ImageSearch3" CausesValidation="false" runat="server" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("SEND_MESSAGE_TO")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Response
                                                <br />
                                        <asp:DropDownList ID="Dropresponce" AutoPostBack="true" OnSelectedIndexChanged="BindGridsearch" runat="server">
                                            <asp:ListItem Value="-1" Text="ALL"></asp:ListItem>
                                            <asp:ListItem Value="ERROR" Text="ERROR"></asp:ListItem>
                                            <asp:ListItem Value="SUCCESS" Text="SUCCESS"></asp:ListItem>
                                        </asp:DropDownList>

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("SHOW_STATUS")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Date Time
                                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("SEND_DATE")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Message
                                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="LinkView" ToolTip='<%#Eval("SEND_RESPONCE")%>' OnClientClick='<%#Eval("Opnewindow")%>' runat="server">View</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <SelectedRowStyle Wrap="False" />
                            <HeaderStyle Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
