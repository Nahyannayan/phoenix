<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSMSDataAdd.ascx.vb"
    Inherits="masscom_UserControls_comExcelSMSDataAdd" %>
<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>


<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />


<script type="text/javascript">

    function vali_Filetype() {

        var id_value = document.getElementById('<%= FileUpload1.ClientID %>').value;

        if (id_value != '') {
            var valid_extensions = /(.xls|.xlsx|.XLS|.XLSX)$/i;

            if (valid_extensions.test(id_value)) {

                return true;
            } else {
                alert('Please upload a document in one of the following formats:  .xls,.xlsx')
                document.getElementById('<%= FileUpload1.ClientID %>').focus()
                return false;
            }
        }

    }

</script>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label><br />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" class="title-bg-lite">Select a Title 
            <asp:DropDownList ID="ddType" runat="server" AutoPostBack="True">
            </asp:DropDownList>
            &nbsp;
            <asp:LinkButton ID="LinkAddTitle" runat="server">Add New Template</asp:LinkButton>
        </td>
    </tr>
</table>
<br />
<asp:Panel ID="Panel1" Visible="False" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"
        runat="server" visible="false">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="30%">
                            <span class="field-label">Title</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="30%">
                            <span class="field-label">Type </span>
                        </td>
                        <td align="left">
                            <asp:RadioButtonList ID="RadioType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="Student" Value="STUDENT"></asp:ListItem>
                                <asp:ListItem Text="Staff" Value="STAFF"></asp:ListItem>
                                <asp:ListItem Text="Others" Value="OTHERS"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <asp:LinkButton ID="Linksavechanges" runat="server" Visible="False" OnClick="Linksavechanges_Click">Save Changes</asp:LinkButton>||<asp:LinkButton
                    ID="Linkdeleterecords" runat="server" Visible="False" OnClick="Linkdeleterecords_Click">Delete Template</asp:LinkButton>
            </td>
        </tr>
    </table>
    <ajaxToolkit:ConfirmButtonExtender ID="CFD1" ConfirmText="Are you sure, Do you want to delete this template?"
        TargetControlID="Linkdeleterecords" runat="server" Enabled="True">
    </ajaxToolkit:ConfirmButtonExtender>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Records in Selected Title
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridEdit" AutoGenerateColumns="False" runat="server" AllowPaging="True" CssClass="table table-bordered table-row"
                    ShowFooter="True" OnRowEditing="GridEdit_RowEditing" OnRowCancelingEdit="GridEdit_RowCancelingEdit"
                    OnRowUpdating="GridEdit_RowUpdating" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Unique ID.
                                            <br />
                                <asp:TextBox ID="txtUniqueId" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                    ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("ID")%></center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center class="matters">
                                    <asp:TextBox ID="txtUniqueIdAdd" runat="server"></asp:TextBox>
                                </center>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <center>
                                    <asp:HiddenField ID="HiddenRecordid" Value='<%#Eval("RECORD_ID")%>' runat="server" />
                                    <asp:TextBox ID="txtUniqueIdEdit" Text='<%#Eval("ID")%>' runat="server"></asp:TextBox>
                                </center>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Mobile No.
                                            <br />
                                <asp:TextBox ID="txtdata" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                    ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("MOBILE_NO")%></center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center class="matters">
                                    <asp:TextBox ID="txtDataAdd" runat="server"></asp:TextBox>
                                </center>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <center>
                                    <asp:TextBox ID="txtDataEdit" Text='<%#Eval("MOBILE_NO")%>' runat="server"></asp:TextBox>
                                </center>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Status
                                            <br />
                                <asp:DropDownList ID="ddstatus" AutoPostBack="true" OnSelectedIndexChanged="ddstatusEdit"
                                    runat="server">
                                    <asp:ListItem Value="ALL" Text="ALL" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="ERROR" Text="ERROR"></asp:ListItem>
                                </asp:DropDownList>

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:Image ID="Image1" ImageUrl='<%#Eval("NO_ERROR_FLAG")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                                    <asp:Button ID="btnAdd" Width="100%" CssClass="button" CommandName="Add" runat="server"
                                        Text="Add" />
                                </center>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>

                                <br />
                                Edit
                                            <br />
                                <br />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkEdit" CommandName="Edit" CommandArgument='<%#Eval("RECORD_ID")%>'
                                        runat="server">Edit</asp:LinkButton>
                                </center>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkUpdate" CommandName="Update" CommandArgument='<%#Eval("RECORD_ID")%>'
                                        runat="server">Update</asp:LinkButton>
                                    <asp:LinkButton ID="LinkCancel" CommandName="Cancel" CommandArgument='<%#Eval("RECORD_ID")%>'
                                        runat="server">Cancel</asp:LinkButton>
                                </center>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>

                                <br />
                                Delete
                                            <br />
                                <br />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkDelete" CommandName="deleting" CommandArgument='<%#Eval("RECORD_ID")%>'
                                        runat="server"> Delete</asp:LinkButton>
                                    <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" TargetControlID="LinkDelete"
                                        ConfirmText="Are you sure, Do you want this record to be deleted ?">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
<div class="matters">
    <asp:Panel ID="PanelTitleAdd" runat="server" CssClass="panel-cover"
        Style="display: none">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">New Title
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="field-label">Title</span>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txttitleadd" runat="server" ValidationGroup="s"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="T1" runat="server" visible="false">
                                    <td align="left" width="30%">
                                        <span class="field-label">Type</span>
                                    </td>

                                    <td align="left">
                                        <asp:RadioButtonList ID="RadioTypeAdd" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Text="Student" Value="STUDENT"></asp:ListItem>
                                            <asp:ListItem Text="Staff" Value="STAFF"></asp:ListItem>
                                            <asp:ListItem Text="Others" Value="OTHERS"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="field-label">Excel</span>
                                    </td>

                                    <td align="left">
                                        <asp:FileUpload ID="FileUpload1" onBlur="javascript:vali_Filetype();" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnok" runat="server" CssClass="button" OnClick="btnok_Click" Text="Ok"
                                            ValidationGroup="s" />
                                        <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /><br />
                                        (Excel Not Mandatory)
                                        <br />
                                        Sheet Name:Sheet1
                                        <br />
                                        Field Names:ID,MobileNo,Name(Not Mandatory)
                                    </td>
                                </tr>
                            </table>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttitleadd"
                                Display="None" ErrorMessage="Please Enter Title" SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                            <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUpload1"
                                                        Display="None" ErrorMessage="Please upload a Excel document (*.xls)." Style="position: static"
                                                        ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.xls|.xlsx|.XLS|.XLSX)$"
                                                        ValidationGroup="s"></asp:RegularExpressionValidator>--%>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                                ShowSummary="False" ValidationGroup="s" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnexport" runat="server" CausesValidation="False" CssClass="button"
        Text="Export" Visible="False" />
</div>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="btncancel" DropShadow="True" PopupControlID="PanelTitleAdd"
    TargetControlID="LinkAddTitle" DynamicServicePath="" Enabled="True">
</ajaxToolkit:ModalPopupExtender>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="HiddenTempid" runat="server" />
