﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"
    CodeFile="gpsListGroupsAddOn.aspx.vb" Inherits="Transport_GPS_Tracking_SMS_gpsListGroupsAddOn" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <!-- Bootstrap core JavaScript-->
    <script src="../../../vendor/jquery/jquery.min.js"></script>
    <script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Bootstrap core CSS-->
    <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Groups
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">



                <asp:LinkButton ID="lnkadd" runat="server">New Group</asp:LinkButton>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg-lite">Group List
                <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" PageSize="20" ShowFooter="true" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="false" EmptyDataText="Information not available." Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Group ID
                                            <br />
                                            <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("GROUP_ID")%>
                                    <asp:HiddenField ID="Hiddengroupid" Value='<%# Eval("GROUP_ID")%>' runat="server" />
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Group Name
                                            <br />
                                            <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch2" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("GROUP_DESC")%> &nbsp;(<%# Eval("GROUP_TYPE")%>)
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Transport Type
                                            <br />
                                            <asp:DropDownList ID="ddTransportType" AutoPostBack="true" OnSelectedIndexChanged="search" runat="server">
                                                <asp:ListItem Text="ALL" Value="-1"></asp:ListItem>
                                                <asp:ListItem Text="Transport" Value="T"></asp:ListItem>
                                                <asp:ListItem Text="Non-Transport" Value="NT"></asp:ListItem>
                                            </asp:DropDownList>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("TRANSPORT_TYPE")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Vehicle Reg
                                            <br />
                                            <asp:TextBox ID="Txt4" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch4" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("VEH_REGNO")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            GPS Unit Id
                                            <br />
                                            <asp:TextBox ID="Txt5" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch5" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("GPS_UNIT_ID")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Trip Onward
                                            <br />
                                            <asp:TextBox ID="Txt6" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch6" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("ONWARD_TRIP")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Trip Return
                                            <br />
                                            <asp:TextBox ID="Txt7" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch7" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("RETURN_TRIP")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            User Type
                                            <br />
                                            <asp:DropDownList ID="ddUserType" AutoPostBack="true" OnSelectedIndexChanged="search" runat="server">
                                                <asp:ListItem Text="ALL" Value="-1"></asp:ListItem>
                                                <asp:ListItem Text="STUDENT" Value="STUDENT"></asp:ListItem>
                                                <asp:ListItem Text="STAFF" Value="STAFF"></asp:ListItem>
                                            </asp:DropDownList>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("USER_TYPE")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            BSU
                                       
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("BSU_SHORTNAME")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            List
                                      
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkList" CommandName="list" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server">List</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Delete
                                       
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkdelete" CommandName="deleting" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server">Delete</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Send
                                     
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnksms" CommandName="sms" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server">SMS</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Send
                                      
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkemail" CommandName="email" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server">Email</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle />
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <EditRowStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
