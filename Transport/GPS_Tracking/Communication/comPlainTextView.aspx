﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comPlainTextView.aspx.vb" EnableTheming="false" Inherits="masscom_TabPages_comPlainTextView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->
</head>
<body>
    <form id="form1" runat="server">
    <div id="template" runat="server" >
    
    </div>
    <asp:GridView ID="GrdAttachment" runat="server" AutoGenerateColumns="false"  CssClass="table table-bordered table-row"
        OnRowCommand="GrdAttachment_RowCommand" ShowHeader="false" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" 
                        CommandName="select" Text=' <%# Eval("Name") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Size (Bytes)">
                <ItemTemplate>
                    <div align="right">
                        -(Size
                                                    <%#Eval("length")%>
                                                    Bytes)</div>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
    </form>
</body>
</html>
