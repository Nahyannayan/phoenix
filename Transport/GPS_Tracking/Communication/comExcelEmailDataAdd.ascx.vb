Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class masscom_UserControls_comExcelEmailDataAdd
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Hiddenbsuid.Value = Session("sBsuid")
            BindTitle()
            Dim logid = Request.QueryString("DataId").ToString()

            If logid <> 0 Then
                ddType.SelectedValue = logid
                ddType_SelectedIndexChanged(ddType, Nothing)

                LinkAddTitle.Visible = False
                Linkdeleterecords.Visible = False

                ddType.Enabled = False
            End If

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnok)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindTitle()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select LOG_ID,TITLE from COM_EXCEL_EMAIL_DATA_MASTER where BSU_ID='" & Hiddenbsuid.Value & "'  ORDER BY TITLE"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddType.DataSource = ds
        ddType.DataTextField = "TITLE"
        ddType.DataValueField = "LOG_ID"
        ddType.DataBind()
        Dim list As New ListItem
        list.Text = "Select a Title"
        list.Value = "-1"
        ddType.Items.Insert(0, list)

    End Sub

    Public Sub BindGridEdit(ByVal export As Boolean)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select RECORD_ID,ID,EMAIL_ID, (CASE NO_ERROR_FLAG WHEN 'True' Then '~/Images/tick.gif' ELSE '~/Images/cross.png' END )NO_ERROR_FLAG from COM_EXCEL_EMAIL_DATA_LOG " & _
                        " where LOG_ID='" & ddType.SelectedValue & "' "

        Dim txtUniqueId As String
        Dim txtdata As String
        Dim ddstatus As String
        lblmessage.Text = ""

        If GridEdit.Rows.Count > 0 Then
            txtUniqueId = DirectCast(GridEdit.HeaderRow.FindControl("txtUniqueId"), TextBox).Text.Trim()
            txtdata = DirectCast(GridEdit.HeaderRow.FindControl("txtdata"), TextBox).Text.Trim()
            ddstatus = DirectCast(GridEdit.HeaderRow.FindControl("ddstatus"), DropDownList).SelectedValue


            If txtUniqueId.Trim() <> "" Then
                str_query &= " and replace(ID,' ','') like '%" & txtUniqueId.Replace(" ", "") & "%' "
            End If

            If txtdata.Trim() <> "" Then
                str_query &= " and replace(EMAIL_ID,' ','') like '%" & txtdata.Replace(" ", "") & "%' "
            End If

            If ddstatus <> "ALL" Then
                str_query &= " and NO_ERROR_FLAG='False'"
            End If

        End If

        str_query &= " order by ID"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("RECORD_ID")
            dt.Columns.Add("ID")
            dt.Columns.Add("EMAIL_ID")
            dt.Columns.Add("NO_ERROR_FLAG")

            Dim dr As DataRow = dt.NewRow()
            dr("RECORD_ID") = ""
            dr("ID") = ""
            dr("EMAIL_ID") = ""
            dr("NO_ERROR_FLAG") = ""

            dt.Rows.Add(dr)
            GridEdit.DataSource = dt
            GridEdit.DataBind()

            DirectCast(GridEdit.Rows(0).FindControl("LinkEdit"), LinkButton).Visible = False
            DirectCast(GridEdit.Rows(0).FindControl("LinkDelete"), LinkButton).Visible = False
            DirectCast(GridEdit.Rows(0).FindControl("Image1"), Image).Visible = False
            btnexport.Visible = False

        Else
            GridEdit.DataSource = ds
            GridEdit.DataBind()

            btnexport.Visible = True

            If export Then

                Dim dt As DataTable

                dt = ds.Tables(0)

                dt.Columns.Remove("RECORD_ID")
                dt.Columns.Remove("NO_ERROR_FLAG")

                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile

                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ws.InsertDataTable(dt)
                ws.HeadersFooters.AlignWithMargins = True
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
                'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                Dim pathSave As String
                pathSave = "Data.xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If

        End If

        If GridEdit.Rows.Count > 0 Then

            DirectCast(GridEdit.HeaderRow.FindControl("txtUniqueId"), TextBox).Text = txtUniqueId
            DirectCast(GridEdit.HeaderRow.FindControl("txtdata"), TextBox).Text = txtdata
            DirectCast(GridEdit.HeaderRow.FindControl("ddstatus"), DropDownList).SelectedValue = ddstatus


        End If

    End Sub
    Protected Sub ddType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddType.SelectedIndexChanged
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select TITLE,TYPE  from COM_EXCEL_EMAIL_DATA_MASTER where LOG_ID='" & ddType.SelectedValue.Trim() & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ddType.SelectedIndex > 0 Then
            Panel1.Visible = True
        Else
            Panel1.Visible = False
        End If

        If ds.Tables(0).Rows.Count > 0 Then
            txttitle.Text = ds.Tables(0).Rows(0).Item("TITLE").ToString()
            RadioType.SelectedValue = ds.Tables(0).Rows(0).Item("TYPE").ToString().Trim()
            Linksavechanges.Visible = True
            Linkdeleterecords.Visible = True

        Else
            txttitle.Text = ""
            Linksavechanges.Visible = False
            Linkdeleterecords.Visible = False
        End If

        BindGridEdit(False)

    End Sub

    Protected Sub GridEdit_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridEdit.PageIndexChanging
        GridEdit.PageIndex = e.NewPageIndex
        BindGridEdit(False)

    End Sub

    Protected Sub GridEdit_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridEdit.RowCommand
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""

        If e.CommandName = "search" Then
            BindGridEdit(False)
        End If

        If e.CommandName = "Add" Then


            Dim txtUniqueIdAdd = DirectCast(GridEdit.FooterRow.FindControl("txtUniqueIdAdd"), TextBox).Text
            Dim txtDataAdd = DirectCast(GridEdit.FooterRow.FindControl("txtDataAdd"), TextBox).Text
            Dim errorflag As Boolean


            If txtUniqueIdAdd <> "" And txtDataAdd <> "" Then

                If isEmail(txtDataAdd) Then
                    errorflag = True
                Else
                    errorflag = False
                End If

                str_query = "insert into  COM_EXCEL_EMAIL_DATA_LOG (LOG_ID,ID,EMAIL_ID,NO_ERROR_FLAG) values('" & ddType.SelectedValue & "','" & txtUniqueIdAdd & "','" & txtDataAdd & "','" & errorflag & "') "
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                ''Track that it is modified from original excel sheet
                str_query = "Update COM_EXCEL_EMAIL_DATA_MASTER  SET MODIFIED='True' where LOG_ID='" & ddType.SelectedValue & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                BindGridEdit(False)

                lblmessage.Text = "Successfully Added"
            End If

        End If


        If e.CommandName = "deleting" Then
            If CheckSend() Then
                str_query = "delete COM_EXCEL_EMAIL_DATA_LOG where RECORD_ID='" & e.CommandArgument & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                BindGridEdit(False)

                ''Panel1.Visible = False
                lblmessage.Text = "Successfully Deleted"
            End If

        End If



    End Sub

    Public Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else

            Dim pattern As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" '"(.+@.+\.[a-z]+)"
            Dim expression As Regex = New Regex(pattern)
            Return expression.IsMatch(inputEmail)


        End If

    End Function



    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

    Protected Sub ddstatusEdit(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGridEdit(False)
    End Sub

    Protected Sub GridEdit_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        GridEdit.EditIndex = e.NewEditIndex
        BindGridEdit(False)
    End Sub


    Protected Sub GridEdit_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs)
        GridEdit.EditIndex = "-1"
        BindGridEdit(False)

    End Sub


    Protected Sub GridEdit_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        Dim errorflag As Boolean

        Dim txtUniqueIdEdit = DirectCast(GridEdit.Rows(e.RowIndex).FindControl("txtUniqueIdEdit"), TextBox).Text.Trim()
        Dim txtDataEdit = DirectCast(GridEdit.Rows(e.RowIndex).FindControl("txtDataEdit"), TextBox).Text.Trim()
        Dim Record_id = DirectCast(GridEdit.Rows(e.RowIndex).FindControl("HiddenRecordid"), HiddenField).Value.Trim()

        If txtUniqueIdEdit <> "" And txtDataEdit <> "" Then
            If isEmail(txtDataEdit) Then
                errorflag = True
            Else
                errorflag = False
            End If

            str_query = "update COM_EXCEL_EMAIL_DATA_LOG set ID='" & txtUniqueIdEdit & "', EMAIL_ID='" & txtDataEdit & "', NO_ERROR_FLAG='" & errorflag & "' where RECORD_ID='" & Record_id & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            ''Track that it is modified from original excel sheet
            str_query = "Update COM_EXCEL_EMAIL_DATA_MASTER  SET MODIFIED='True' where LOG_ID='" & ddType.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            GridEdit.EditIndex = "-1"

            BindGridEdit(False)

            lblmessage.Text = "Successfully Updated"


        End If



    End Sub
    Public Function CheckPreviousTemplateName(ByVal optioncheck As String) As Boolean
        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_text = ""
        If optioncheck = "1" Then
            sql_text = "SELECT * FROM COM_EXCEL_EMAIL_DATA_MASTER WHERE TITLE='" & txttitleadd.Text.Trim() & "' AND BSU_ID='" & Hiddenbsuid.Value & "'"
        End If
        If optioncheck = "2" Then
            sql_text = "SELECT * FROM COM_EXCEL_EMAIL_DATA_MASTER WHERE TITLE='" & txttitle.Text.Trim() & "' AND BSU_ID='" & Hiddenbsuid.Value & "' AND LOG_ID != '" & ddType.SelectedValue & "'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_text)
        Dim Returnvalue = True
        If ds.Tables(0).Rows.Count > 0 Then
            lblmessage.Text = "Title already exists. Please enter a new title."
            Returnvalue = False
        End If
        Return Returnvalue
    End Function


    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If CheckPreviousTemplateName("1") Then

            Dim filename As String = ""
            Dim savepath As String = ""

            If FileUpload1.HasFile Then
                Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                filename = FileUpload1.FileName.ToString()
                savepath = path + "NewsLettersEmails/" + Session("EmployeeId") + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)
                FileUpload1.SaveAs(savepath)
            End If


            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()

            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TITLE", txttitleadd.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@TYPE", RadioType.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@EXCEL_PATH", savepath)
                pParms(3) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)

                Dim Logid = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "COM_EXCEL_EMAIL_DATA_MASTER_INSERT", pParms)

                If FileUpload1.HasFile Then

                    Dim myDataset As New DataSet()

                    Dim strConnE As String = ""
                    Dim ext = GetExtension(filename)

                    If ext = "xls" Then
                        strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savepath & ";Extended Properties=""Excel 8.0;"""
                    ElseIf ext = "xlsx" Then
                        strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savepath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                    End If


                    Try
                        Dim myData As New OleDbDataAdapter("SELECT ID,EmailID,Name FROM [Sheet1$] ", strConnE)
                        myData.TableMappings.Add("Table", "ExcelTest")
                        myData.Fill(myDataset)

                    Catch ex As Exception

                        Dim myData As New OleDbDataAdapter("SELECT ID,EmailID, '' as Name FROM [Sheet1$] ", strConnE)
                        myData.TableMappings.Add("Table", "ExcelTest")
                        myData.Fill(myDataset)

                    End Try


                    Dim i = 0

                    Dim student_id As String
                    Dim EMAIL_ID As String
                    Dim errorflag As Boolean
                    Dim str_query As String
                    Dim name As String


                    For i = 0 To myDataset.Tables(0).Rows.Count - 1

                        student_id = myDataset.Tables(0).Rows(i).Item("ID").ToString()
                        EMAIL_ID = myDataset.Tables(0).Rows(i).Item("EmailID").ToString().Replace("'", "").Trim()
                        name = myDataset.Tables(0).Rows(i).Item("Name").ToString().Replace("'", "")

                        If student_id <> "" And EMAIL_ID <> "" Then
                            If isEmail(EMAIL_ID) Then
                                errorflag = True
                            Else
                                errorflag = False
                            End If

                            str_query = "insert into  COM_EXCEL_EMAIL_DATA_LOG (LOG_ID,ID,EMAIL_ID,NAME,NO_ERROR_FLAG) values('" & Logid & "','" & student_id & "','" & EMAIL_ID & "','" & name & "','" & errorflag & "') "
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                        End If


                    Next


                End If

                transaction.Commit()

                BindTitle()
                BindGridEdit(False)

                Panel1.Visible = False

                lblmessage.Text = "New Template Added successfully."

            Catch ex As Exception

                lblmessage.Text = "Error : " & ex.Message

            End Try

        End If

    End Sub
    Protected Sub Linkdeleterecords_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkdeleterecords.Click
        If CheckSend() Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "Delete COM_EXCEL_EMAIL_DATA_MASTER  where LOG_ID='" & ddType.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            str_query = "Delete COM_EXCEL_EMAIL_DATA_LOG  where LOG_ID='" & ddType.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            BindTitle()
            BindGridEdit(False)

            Panel1.Visible = False
            lblmessage.Text = "Record(s) deleted successfully."
        End If


    End Sub
    Public Function CheckSend() As Boolean
        Dim returnval = True
        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_query = " SELECT SENDING_ID FROM dbo.COM_EXCEL_EMAIL_SENDING_LOG WHERE DATA_LOG_ID='" & ddType.SelectedValue & "' " & _
        '                " UNION " & _
        '                " SELECT RECORD_ID FROM  dbo.COM_MANAGE_EMAIL_SCHEDULE WHERE DATA_LOG_ID='" & ddType.SelectedValue & "' "
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'If ds.Tables(0).Rows.Count > 0 Then
        '    returnval = False
        '    lblmessage.Text = "Message has been sent , Record cannot be deleted."
        'End If

        Return returnval
    End Function

    Protected Sub Linksavechanges_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If CheckPreviousTemplateName("2") Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "update COM_EXCEL_EMAIL_DATA_MASTER set TITLE='" & txttitle.Text.Trim() & "', TYPE='" & RadioType.SelectedValue & "', MODIFIED='True' where LOG_ID='" & ddType.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            BindTitle()
            BindGridEdit(False)

            Panel1.Visible = False
            lblmessage.Text = "Title information updated successfully."
        Else
            lblmessage.Text = "Title already Exists."
        End If


    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        BindGridEdit(True)
    End Sub

End Class
