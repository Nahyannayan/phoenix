<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="comCommunicationReportPage.aspx.vb" Inherits="Transport_GPS_Tracking_Communication_comCommunicationReportPage" %>

<%@ Register Src="comCommunicationReport.ascx" TagName="comCommunicationReport" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Communication Reports"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:comCommunicationReport ID="ComCommunicationReport1" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>
