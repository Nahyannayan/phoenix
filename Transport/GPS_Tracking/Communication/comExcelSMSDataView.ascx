<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSMSDataView.ascx.vb"
    Inherits="masscom_UserControls_comExcelSMSDataView" %>
<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>

<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />



<script type="text/javascript">

    function openPopup(id) {
        var strOpen;
        var tempid = document.getElementById('<%= HiddenTempid.ClientID %>').value;
            var module = document.getElementById('<%= HiddenModule.ClientID %>').value;
            strOpen = 'comSendMessagePage.aspx'
            strOpen += "?module=" + module + "&messagetype=SMS&grpid=&logid=" + id + "&templateid=" + tempid;
            //window.showModalDialog(strOpen, "", "dialogWidth:350px;dialogHeight:350px;center:yes");
            //window.close();
            return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
            return false;
        }

</script>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:Label ID="lblsmessage2" runat="server" CssClass="error"></asp:Label>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Template Collections
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView" AutoGenerateColumns="False" runat="server" AllowPaging="True" CssClass="table table-bordered table-row"
                Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Title
                                        <br />
                            <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenLogid" Value='<% #Eval("LOG_ID")%>' runat="server" />
                            <%#Eval("TITLE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Type
                                        <br />
                            <asp:TextBox ID="txttype" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("TYPE")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Count
                                        <br />
                            Total/Valid/Error
                                        <br />
                            <br />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:Label ID="lbltotal" Text='<% #Eval("TOTAL")%>' runat="server"></asp:Label>
                                <b>/</b>
                                <asp:Label ID="lblvalid" Text='<% #Eval("VALID")%>' runat="server"></asp:Label>
                                <b>/</b>
                                <asp:Label ID="lblerror" Text='<% #Eval("ERROR")%>' runat="server"></asp:Label>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>

                            <br />
                            Send
                                        <br />
                            <br />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkSent" OnClientClick='<%#Eval("OPENW")%>' runat="server">Proceed</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="HiddenTempid" runat="server" />
<asp:HiddenField ID="HiddenModule" runat="server" />

<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>
