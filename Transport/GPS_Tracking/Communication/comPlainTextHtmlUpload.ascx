﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comPlainTextHtmlUpload.ascx.vb" Inherits="masscom_UserControls_comPlainTextHtmlUpload" %>

<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />


<br />
<table border="0" cellpadding="0" cellspacing="0" 
    width="100%">
    <tr>
        <td align="left" class="title-bg-lite">
            Upload Zip File (Html File and its supporting files)&nbsp;</td>
    </tr>
    <tr>
        <td align="center" >
<asp:FileUpload ID="FileUploadNewsletters" runat="server" />
&nbsp;<asp:Button ID="btnupload" runat="server" CssClass="button" Text="Upload"  />
&nbsp;&nbsp;<br />
&nbsp;<asp:Label ID="lblmessage" runat="server" Text="" CssClass="error"></asp:Label>
            <br />
            <span class="error">
               Note: Please upload a zip file which contains the html file and its supporting 
            files (images, css files etc). Once uploaded you can see the preview of the html 
            file. Below you can find the html tags which will be usefull to create the plain 
            text email.
            </span>
         
            
            </td>
    </tr>
    <tr>
        <td align="left">



<div id="template" runat="server">
</div>
        </td>
    </tr>
    <tr>
        <td align="left">
<asp:TextBox ID="txthtmlcode" TextMode="MultiLine"  EnableTheming="false" runat="server" 
                Height="230px" Width="926px"></asp:TextBox>
        </td>
    </tr>
</table>



