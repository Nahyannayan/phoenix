﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO


Partial Class Transport_GPS_Tracking_SMS_gpsCreateGroupsAddOn
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            TBL2.Visible = False
            BindBsu()
        End If

    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindVehReg()

    End Sub

    Public Sub BindgpsUnit()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 13)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

        ddgpsunit.DataSource = ds
        ddgpsunit.DataTextField = "VEH_UNITID"
        ddgpsunit.DataValueField = "VEH_UNITID"
        ddgpsunit.DataBind()

        Dim list As New ListItem
        list.Text = "Select GPS unit"
        list.Value = "-1"
        ddgpsunit.Items.Insert(0, list)
    End Sub

    Public Sub BindVehReg()

        BindgpsUnit()

        ddvehRegNo.Items.Clear()
        If ddBsu.SelectedValue <> "-1" Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter


            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 10)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                ddvehRegNo.DataTextField = "ONWRET"
                ddvehRegNo.DataValueField = "VEH_ID"
                ddvehRegNo.DataSource = ds
                ddvehRegNo.DataBind()
            End If
        End If

        Dim list As New ListItem
        list.Text = "Onward/Return"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)

        BindTrips()

    End Sub

    Public Sub BindTrips()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 11)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        If ddvehRegNo.SelectedValue <> "-1" Then
            pParms(2) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)
 
        ddtriponward.DataSource = ds
        ddtriponward.DataTextField = "Trp_descr"
        ddtriponward.DataValueField = "trip_onw_id"
        ddtriponward.DataBind()

        Dim list As New ListItem
        list.Text = "Select Onward Trip"
        list.Value = "-1"
        ddtriponward.Items.Insert(0, list)


        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 12)


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

        ddtripreturn.DataSource = ds
        ddtripreturn.DataTextField = "Trp_descr"
        ddtripreturn.DataValueField = "trip_ret_id"
        ddtripreturn.DataBind()

        Dim list2 As New ListItem
        list2.Text = "Select Return Trip"
        list2.Value = "-1"
        ddtripreturn.Items.Insert(0, list2)


    End Sub

    Public Sub HideRows()
        If ddtype.SelectedValue = "T" And ddusertype.SelectedValue = "STUDENT" Then
            TR1.Visible = True
            TR2.Visible = True
            TR3.Visible = True
            TR4.Visible = True
        Else
            TR1.Visible = False
            TR2.Visible = False
            TR3.Visible = False
            TR4.Visible = False
        End If
    End Sub

    Protected Sub ddtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtype.SelectedIndexChanged
        HideRows()
    End Sub

    Protected Sub ddvehRegNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddvehRegNo.SelectedIndexChanged
        BindTrips()
    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindVehReg()

    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If Validate() Then


            lblmessage.Text = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(12) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OPTION ", 3)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

            If ddtype.SelectedValue = "T" Then
                If ddvehRegNo.SelectedValue <> "-1" Then
                    pParms(2) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
                End If

                If ddgpsunit.SelectedValue <> "-1" Then
                    pParms(3) = New SqlClient.SqlParameter("@GPS_ID", ddgpsunit.SelectedValue)
                End If

                If ddtriponward.SelectedValue <> "-1" Then
                    pParms(4) = New SqlClient.SqlParameter("@TRIP_ONWARD", ddtriponward.SelectedValue)
                End If

                If ddtripreturn.SelectedValue <> "-1" Then
                    pParms(5) = New SqlClient.SqlParameter("@TRIP_RETURN", ddtripreturn.SelectedValue)
                End If
            End If

            pParms(6) = New SqlClient.SqlParameter("@TRANSPORT_TYPE", ddtype.SelectedValue)
            pParms(7) = New SqlClient.SqlParameter("@GROUP_DESC", txtdesc.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@GROUP_TYPE", ddGropType.SelectedValue)
            pParms(9) = New SqlClient.SqlParameter("@USER_TYPE", ddusertype.SelectedValue)

            Dim flag = 0
            Dim flag2 = 0
            Dim hash As New Hashtable
            If Session("smsids") Is Nothing Then

            Else

                hash = Session("smsids")
                Dim val = ""
                For Each Item As Object In hash
                    If val = "" Then
                        val = Item.Value
                    Else
                        val &= "," & Item.Value
                    End If
                    flag = 1
                Next

                If val <> "" Then
                    pParms(10) = New SqlClient.SqlParameter("@ADDON_ID", val)
                End If

            End If

            If ddGropType.SelectedValue = "G" Then
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_GROUP_USER", pParms)
                flag2 = 1
            End If

            If ddGropType.SelectedValue = "AD" And flag = 1 Then
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_GROUP_USER", pParms)
                flag2 = 1
            Else
                lblmessage.Text = "Please select add on students"
            End If

            If flag2 = 1 Then
                If lblmessage.Text.IndexOf("exists") > -1 Then

                Else
                    hash.Clear()
                    Session("smsids") = hash
                    Response.Redirect("~/Transport/GPS_Tracking/Communication/gpsListGroupsAddOn.aspx")
                End If

            End If


        End If

    End Sub

    Public Sub subBindPreviewGrid()

        If Validate() Then
            TBL2.Visible = True
            lblmessage.Text = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(13) As SqlClient.SqlParameter

            Dim user_id = ""
            Dim user_name = ""
            Dim details = ""
            Dim contactname = ""
            Dim contactno = ""
            Dim email = ""

            If GridInfo.Rows.Count > 0 Then
                user_id = DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
                user_name = DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
                details = DirectCast(GridInfo.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()
                contactname = DirectCast(GridInfo.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim()
                contactno = DirectCast(GridInfo.HeaderRow.FindControl("Txt5"), TextBox).Text.Trim()
                email = DirectCast(GridInfo.HeaderRow.FindControl("Txt6"), TextBox).Text.Trim()
            End If

            If ddtype.SelectedValue = "T" Then
                pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
            Else
                pParms(0) = New SqlClient.SqlParameter("@OPTION ", 1)
            End If

            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

            If ddvehRegNo.SelectedValue <> "-1" Then
                pParms(2) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
            End If

            If ddgpsunit.SelectedValue <> "-1" Then
                pParms(3) = New SqlClient.SqlParameter("@GPS_ID", ddgpsunit.SelectedValue)
            End If

            If ddtriponward.SelectedValue <> "-1" Then
                pParms(4) = New SqlClient.SqlParameter("@TRIP_ONWARD", ddtriponward.SelectedValue)
            End If

            If ddtripreturn.SelectedValue <> "-1" Then
                pParms(5) = New SqlClient.SqlParameter("@TRIP_RETURN", ddtripreturn.SelectedValue)
            End If

            If user_id <> "" Then
                pParms(6) = New SqlClient.SqlParameter("@USR_ID", user_id)
            End If

            If user_name <> "" Then
                pParms(7) = New SqlClient.SqlParameter("@NAME", user_name)
            End If

            If details <> "" Then
                pParms(8) = New SqlClient.SqlParameter("@DETAILS", details)
            End If

            If contactname <> "" Then
                pParms(9) = New SqlClient.SqlParameter("@CONTACT_NAME", contactname)
            End If

            If contactno <> "" Then
                pParms(10) = New SqlClient.SqlParameter("@MOBILE", contactno)
            End If

            If email <> "" Then
                pParms(11) = New SqlClient.SqlParameter("@EMAIL", email)
            End If

            pParms(12) = New SqlClient.SqlParameter("@USER_TYPE", ddusertype.SelectedValue)


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_GROUP_USER", pParms)

            GridInfo.DataSource = ds
            GridInfo.DataBind()

            If GridInfo.Rows.Count > 0 Then

                DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text = user_id
                DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text = user_name
                DirectCast(GridInfo.HeaderRow.FindControl("Txt3"), TextBox).Text = details
                DirectCast(GridInfo.HeaderRow.FindControl("Txt4"), TextBox).Text = contactname
                DirectCast(GridInfo.HeaderRow.FindControl("Txt5"), TextBox).Text = contactno
                DirectCast(GridInfo.HeaderRow.FindControl("Txt6"), TextBox).Text = email

                If ddGropType.SelectedValue = "G" Then
                    GridInfo.Columns(0).Visible = False
                Else
                    GridInfo.Columns(0).Visible = True
                End If

            End If

        End If

    End Sub

    Public Function Validate() As Boolean
        Dim rval = True
        lblmessage.Text = ""

        If txtdesc.Text = "" Then
            rval = False
            lblmessage.Text = "Please enter Group description"
        End If

        If ddBsu.SelectedValue = "-1" Then
            rval = False
            lblmessage.Text &= "<br>Please select business unit"
        End If

        Return rval
    End Function

    Protected Sub btnpreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpreview.Click
        subBindPreviewGrid()
    End Sub


    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        subBindPreviewGrid()
    End Sub



    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "search" Then
            subBindPreviewGrid()
        End If

        If e.CommandName = "add" Then

            Dim hash As New Hashtable

            If Session("smsids") Is Nothing Then
            Else
                hash = Session("smsids")
            End If

            For Each row As GridViewRow In GridInfo.Rows

                If DirectCast(row.FindControl("ch1"), CheckBox).Checked = True Then
                    Dim id = DirectCast(row.FindControl("HiddenUserid"), HiddenField).Value.Trim()

                    If Not hash.ContainsKey(id) Then
                        hash.Add(id, "'" & id & "'")
                    End If

                End If

            Next

            Session("smsids") = hash

            AddonGridBind()

        End If

    End Sub


    Public Sub AddonGridBind()
        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(7) As SqlClient.SqlParameter


        If ddtype.SelectedValue = "T" Then
            pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        Else
            pParms(0) = New SqlClient.SqlParameter("@OPTION ", 1)
        End If

        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        If ddvehRegNo.SelectedValue <> "-1" Then
            pParms(2) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
        End If

        If ddgpsunit.SelectedValue <> "-1" Then
            pParms(3) = New SqlClient.SqlParameter("@GPS_ID", ddgpsunit.SelectedValue)
        End If

        If ddtriponward.SelectedValue <> "-1" Then
            pParms(4) = New SqlClient.SqlParameter("@TRIP_ONWARD", ddtriponward.SelectedValue)
        End If

        If ddtripreturn.SelectedValue <> "-1" Then
            pParms(5) = New SqlClient.SqlParameter("@TRIP_RETURN", ddtripreturn.SelectedValue)
        End If

        pParms(6) = New SqlClient.SqlParameter("@USER_TYPE", ddusertype.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_GROUP_USER", pParms)

        Dim hash As Hashtable
        hash = Session("smsids")
        Dim val = ""
        For Each Item As Object In hash
            If val = "" Then
                val = Item.Value
            Else
                val &= "," & Item.Value
            End If

        Next
        If val <> "" Then
            Dim filter = "USR_ID in (" & val & ")"
            Dim dvwView As New DataView
            dvwView = ds.Tables(0).DefaultView
            dvwView.RowFilter = filter

            GridInfo0.DataSource = dvwView
            GridInfo0.DataBind()
        Else
            GridInfo0.DataSource = Nothing
            GridInfo0.DataBind()
        End If


    End Sub

    Protected Sub GridInfo0_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo0.RowCommand


        If e.CommandName = "deleting" Then

            Dim hash As Hashtable
            hash = Session("smsids")
            For Each row As GridViewRow In GridInfo0.Rows
                If DirectCast(row.FindControl("ch2"), CheckBox).Checked = True Then
                    Dim id = DirectCast(row.FindControl("HiddenUserid"), HiddenField).Value.Trim()
                    hash.Remove(id)
                End If

            Next
            Session("smsids") = hash
            AddonGridBind()

        End If


    End Sub

    Protected Sub ddusertype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddusertype.SelectedIndexChanged
        HideRows()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click

        Dim hash As New Hashtable
        hash.Clear()
        Session("smsids") = hash
        Response.Redirect("~/Transport/GPS_Tracking/Communication/gpsListGroupsAddOn.aspx")

    End Sub

End Class
