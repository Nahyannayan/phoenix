Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib
Partial Class masscom_UserControls_comCreatePlainText
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnsaveordinary)

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            BindTemplate()
            BindDefaultValues()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)


    End Sub

    Public Sub BindDefaultValues()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select * from BSU_COMMUNICATION_M where BSC_BSU_ID='" & Hiddenbsuid.Value & "' AND BSC_TYPE='COM'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            txtFrom.Text = ds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
            txthost.Text = ds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            txtport.Text = ds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
            txtusername.Text = ds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
            HiddenPassword.value = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
            txtpassword.Text = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
            btnsaveordinary.Visible = True
        Else
            lblmessage.Text = "Default email configuration settings not set by the administrator. Please contact the administrator."
            btnsaveordinary.Visible = False
        End If


    End Sub
    Public Sub BindTemplate()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select MERGE_ID,MERGE_TITLE_DES from COM_MERGE_TABLES where MERGE_BSU='" & Hiddenbsuid.Value & "' AND MERGE_TYPE='EMAIL'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtemplate.DataSource = ds
            ddtemplate.DataTextField = "MERGE_TITLE_DES"
            ddtemplate.DataValueField = "MERGE_ID"
            ddtemplate.DataBind()

            Dim item As New ListItem
            item.Text = "Select a Template"
            item.Value = "-1"
            ddtemplate.Items.Insert(0, item)
            Fields()
        Else
            TRDynamic.Visible = False
        End If

    


    End Sub
    Public Sub Fields()
        ''Clear the Previous message
        txtEmailText.Content = ""
     

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select MERGE_FIELDS from COM_MERGE_TABLES where MERGE_ID='" & ddtemplate.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim fields As String = ds.Tables(0).Rows(0).Item("MERGE_FIELDS").ToString()

            Dim Afields() As String = fields.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("Data")
            dt.Columns.Add("Value")

            Dim i = 0
            For i = 0 To Afields.Length - 1
                Dim cval As String = Afields(i)


                Dim csplit() As String = cval.Split(">")

                Dim dr As DataRow = dt.NewRow()

                dr("Value") = "$" & csplit(0) & "$"
                dr("Data") = csplit(1).Replace("$", "").Replace("<", "")

                dt.Rows.Add(dr)
            Next

            ddfields.DataSource = dt
            ddfields.DataTextField = "Data"
            ddfields.DataValueField = "Value"
            ddfields.DataBind()
            ddfields.Visible = True
            btninsert.Visible = True
        Else
            ddfields.Visible = False
            btninsert.Visible = False
        End If



    End Sub

    
    Protected Sub btnsaveordinary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsaveordinary.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "insert into COM_MANAGE_EMAIL (EML_GUID) values(NEWID()) SELECT scope_identity() as id "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim attachment = False
        Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
        Dim val As String = ""
        If ds.Tables(0).Rows(0).Item("id").ToString() <> "" Then
            val = ds.Tables(0).Rows(0).Item("id").ToString()
        Else
            val = "1"
        End If

        Directory.CreateDirectory(serverpath + val)
        Directory.CreateDirectory(serverpath + val + "/Attachments")
        Directory.CreateDirectory(serverpath + val + "/News Letters")


        If FileUploadOrdinaryFile.HasFile Then
            attachment = True
            Dim filen As String()
            Dim FileName As String = FileUploadOrdinaryFile.PostedFile.FileName
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)
            FileUploadOrdinaryFile.SaveAs(serverpath + val + "/Attachments/" + FileName)
            attachment = True
        End If

        Dim pParms(19) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EML_ID", val)
        pParms(1) = New SqlClient.SqlParameter("@EML_NEWS_LETTER", "False")
        pParms(2) = New SqlClient.SqlParameter("@EML_BODY", txtEmailText.Content)
        pParms(3) = New SqlClient.SqlParameter("@EML_ATTACHMENT", attachment)
        pParms(4) = New SqlClient.SqlParameter("@EML_FROM", txtFrom.Text.Trim())
        pParms(5) = New SqlClient.SqlParameter("@EML_TITLE", txtTitle.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@EML_SUBJECT", txtsubject.Text.Trim())
        pParms(7) = New SqlClient.SqlParameter("@EML_DISPLAY", txtdisplay.Text.Trim())

        pParms(8) = New SqlClient.SqlParameter("@EML_USERNAME", txtusername.Text.Trim())
        If txtpassword.Text.Trim() <> "" Then
            pParms(9) = New SqlClient.SqlParameter("@EML_PASSWORD", txtpassword.Text.Trim())
        Else
            pParms(9) = New SqlClient.SqlParameter("@EML_PASSWORD", HiddenPassword.value)
        End If


        pParms(10) = New SqlClient.SqlParameter("@EML_HOST", txthost.Text.Trim())
        pParms(11) = New SqlClient.SqlParameter("@EML_PORT", txtport.Text.Trim())
        pParms(12) = New SqlClient.SqlParameter("@EML_BSU_ID", Hiddenbsuid.Value)
        pParms(13) = New SqlClient.SqlParameter("@EML_EMP_ID", Session("EmployeeId"))
        pParms(19) = New SqlClient.SqlParameter("@EML_MODULE", "STS")

        If ddtemplate.SelectedIndex > 0 Then
            pParms(14) = New SqlClient.SqlParameter("@EML_MERGE_ID", ddtemplate.SelectedValue)
        End If


        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_INSERT", pParms)

        txtEmailText.Content = ""
        txtFrom.Text = ""
        txtTitle.Text = ""
        txtsubject.Text = ""
        txtdisplay.Text = ""
        txtusername.Text = ""
        txtpassword.Text = ""
        txthost.Text = ""
        txtport.Text = ""

        lblmessage.Text = "Plain Text Email Template Successfully Created."
       
    End Sub

    
    Protected Sub ddtemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Fields()
    End Sub

    Protected Sub btninsert_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtEmailText.Content = txtEmailText.Content & ddfields.SelectedValue
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Redirect("comEditPlainText.aspx")
    End Sub

End Class
