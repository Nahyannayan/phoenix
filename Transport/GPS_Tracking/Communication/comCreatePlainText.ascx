<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comCreatePlainText.ascx.vb"
    Inherits="masscom_UserControls_comCreatePlainText" %>
<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="/cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">

    function opneWindow() {

        var sFeatures;
        sFeatures = "dialogWidth: 900px; ";
        sFeatures += "dialogHeight: 700px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "comMergerDocument.aspx?Type=EMAIL"

        var result;
        //result = window.showModalDialog(strOpen, "", sFeatures);
        //window.location.reload(true);
        return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
        return false;
    }

    function opnehtmlWindow() {

        var sFeatures;
        sFeatures = "dialogWidth: 1000px; ";
        sFeatures += "dialogHeight: 700px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "comPlainTextHtmlUpload.aspx"

        var result;
        //result = window.showModalDialog(strOpen, "", sFeatures);
        //window.location.reload(true);
        return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
        return false;
    }

</script>
<div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblmessage" runat="server" CssClass="error" ></asp:Label>
            <br />
            <asp:LinkButton ID="lnkhtmlupload" OnClientClick="javascript:opnehtmlWindow(); return false;"
                runat="server">Plain Text with image ? Please click here.</asp:LinkButton>
            <br />
            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Enter Email Text
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td align="left" rowspan="2">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="30%"><span class="field-label">Title</span>
                                            </td>

                                            <td align="left">
                                                <asp:TextBox ID="txtTitle" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="30%"><span class="field-label">From Email Id</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtFrom" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="30%"><span class="field-label">Subject</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtsubject" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="30%"><span class="field-label">Display</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtdisplay" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="Tr1" runat="server" visible="false">
                                            <td align="left" width="30%"><span class="field-label">Host</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txthost" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="Tr2" runat="server" visible="false">
                                            <td align="left" width="30%"><span class="field-label">Port</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtport" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="Tr3" runat="server" visible="false">
                                            <td align="left" width="30%"><span class="field-label">Username</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtusername" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="Tr4" runat="server" visible="false">
                                            <td align="left" width="30%"><span class="field-label">Password</span>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" Width="50%"></asp:TextBox>
                                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="* Please provide if any change in Password"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>
                                                <telerik:RadEditor ID="txtEmailText" runat="server" EditModes="All" Height="600px"
                                                    ToolsFile="xml/FullSetOfTools.xml" Width="750px">
                                                </telerik:RadEditor>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>
                                                <div align="right">
                                                    <asp:LinkButton ID="Linkmerge" runat="server" CausesValidation="false" OnClientClick="opneWindow()">Upload Merge Document</asp:LinkButton>
                                                </div>
                                                <br />
                                                <br />
                                                <div id="TRDynamic" runat="server">
                                                    <asp:LinkButton ID="LinkDynamic" runat="server" OnClientClick="javascript:return false;">Dynamic Text</asp:LinkButton>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="Panel1" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td colspan="1">Template
                                                                        </td>

                                                                        <td>
                                                                            <asp:DropDownList ID="ddtemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddtemplate_SelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="1">Fields
                                                                        </td>

                                                                        <td>
                                                                            <asp:DropDownList ID="ddfields" runat="server">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td align="center">
                                                                            <asp:Button ID="btninsert" runat="server" CssClass="button" OnClick="btninsert_Click"
                                                                                Text="Insert" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkDynamic" Collapsed="true"
                                                                CollapsedSize="0" CollapsedText="Dynamic Text" ExpandControlID="LinkDynamic"
                                                                ExpandedSize="100" ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1"
                                                                TextLabelID="LinkDynamic">
                                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="30%"><span class="field-label">Attachments</span>
                                            </td>

                                            <td>
                                                <asp:FileUpload ID="FileUploadOrdinaryFile" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>
                                                <asp:Button ID="btnsaveordinary" runat="server" CssClass="button" Text="Save" />
                                                <asp:Button ID="btncancel" runat="server" CssClass="button" CausesValidation="false" Text="Cancel" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
        Display="None" ErrorMessage="Please enter email title" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFrom"
        Display="None" ErrorMessage="Please enter from address" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtsubject"
        Display="None" ErrorMessage="Please enter email subject" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtdisplay"
        Display="None" ErrorMessage="Please enter email display name" SetFocusOnError="True"></asp:RequiredFieldValidator>

    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmailText"
        Display="None" ErrorMessage="Please Enter Email Text" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:HiddenField ID="HiddenPassword" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
</div>

<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>
