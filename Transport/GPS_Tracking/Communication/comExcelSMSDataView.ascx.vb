Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.Oledb
Partial Class masscom_UserControls_comExcelSMSDataView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenTempid.Value = Request.QueryString("templateid").ToString().Trim()
            HiddenModule.Value = Request.QueryString("module").ToString().Trim()
            Hiddenbsuid.Value = Session("sBsuid")
            BindGridView()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindGridView()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " Select BSU_ID,A.LOG_ID,TITLE,TYPE,TOTAL,ISNULL(VALID,0)VALID,ISNULL(ERROR,0)ERROR,'openPopup('''+ convert(varchar, A.LOG_ID) +''')' as OPENW " & _
                        "  from COM_EXCEL_SMS_DATA_MASTER A " & _
                        "  inner join(select log_id,count(*)TOTAL from COM_EXCEL_SMS_DATA_LOG group by log_id) b on b.log_id =a.log_id " & _
                        "  left join(select log_id,count(*)VALID from COM_EXCEL_SMS_DATA_LOG where NO_ERROR_FLAG='True' group by log_id) c on c.log_id =a.log_id " & _
                        "  left join(select log_id,count(*)ERROR from COM_EXCEL_SMS_DATA_LOG where NO_ERROR_FLAG='False' group by log_id) d on d.log_id =a.log_id " & _
                        "  where BSU_ID='" & Hiddenbsuid.Value & "'"


        Dim txttitle As String = ""
        Dim txttype As String = ""


        If GridView.Rows.Count > 0 Then
            txttitle = DirectCast(GridView.HeaderRow.FindControl("txttitle"), TextBox).Text.Trim()
            txttype = DirectCast(GridView.HeaderRow.FindControl("txttype"), TextBox).Text.Trim()

            If txttitle.Trim() <> "" Then
                str_query &= " and replace(TITLE,' ','') like '%" & txttitle.Replace(" ", "") & "%' "
            End If

            If txttype.Trim() <> "" Then
                str_query &= " and replace(TYPE,' ','') like '%" & txttype.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by LOG_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("LOG_ID")
            dt.Columns.Add("TITLE")
            dt.Columns.Add("TYPE")
            dt.Columns.Add("TOTAL")
            dt.Columns.Add("VALID")
            dt.Columns.Add("ERROR")
            dt.Columns.Add("OPENW")

            Dim dr As DataRow = dt.NewRow()
            dr("LOG_ID") = ""
            dr("TITLE") = ""
            dr("TYPE") = ""
            dr("TOTAL") = ""
            dr("VALID") = ""
            dr("ERROR") = ""
            dr("OPENW") = ""
            dt.Rows.Add(dr)
            GridView.DataSource = dt
            GridView.DataBind()
        Else
            GridView.DataSource = ds
            GridView.DataBind()
        End If

        If GridView.Rows.Count > 0 Then

            DirectCast(GridView.HeaderRow.FindControl("txttitle"), TextBox).Text = txttitle
            DirectCast(GridView.HeaderRow.FindControl("txttype"), TextBox).Text = txttype

        End If

    End Sub


    Protected Sub GridView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView.PageIndexChanging
        GridView.PageIndex = e.NewPageIndex
        BindGridView()
    End Sub

    Protected Sub GridView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView.RowCommand
        If e.CommandName = "search" Then
            BindGridView()
        End If
    End Sub


End Class
