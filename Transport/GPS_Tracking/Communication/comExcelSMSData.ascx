<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comExcelSMSData.ascx.vb" Inherits="masscom_UserControls_comExcelSMSData" %>
<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>


<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />



  <script type="text/javascript" >
   
    window.setTimeout('setpathExt()',100);
    
     function setpathExt()
        {
           var path = window.location.href
           var Rpath=path.substring(path.indexOf('?'),path.length)
           var objFrame
           
           //Student
           objFrame=document.getElementById("FEx1"); 
           objFrame.src="comExcelSMSDataView.aspx" + Rpath
           
           //Staff
           objFrame=document.getElementById("FEx2"); 
           objFrame.src="comExcelSMSDataAdd.aspx" + Rpath

        }
   
       </script> 

<div >
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                <ajaxToolkit:TabPanel ID="HT1" runat="server">
                    <ContentTemplate>
                      <iframe id="FEx1" height="500" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="700"></iframe>
               
           
                    </ContentTemplate>
                    <HeaderTemplate>
                        Template Collections
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="HT2" runat="server">
                    <ContentTemplate>
                    <iframe id="FEx2" height="500" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="700"></iframe>
              
                    </ContentTemplate>
                    <HeaderTemplate>
                    Add/Edit/Delete Template 
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
