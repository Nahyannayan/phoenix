﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comMergerDocument.ascx.vb"
    Inherits="masscom_UserControls_comMergerDocument" %>


<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>

<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />

<div align="center">
    <center><asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label></center>

    <br />

    <asp:Panel ID="Panel2" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Step 1 :
            Merge Document Upload</td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td align="left" width="30%">
                                <span class="field-label">Title</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txttitlte" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="30%">
                                <span class="field-label">Upload Excel</span>
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                            </td>
                        </tr>
                        <tr>

                            <td align="center" colspan="2">
                                <asp:Button ID="btnExtract" runat="server" CssClass="button" Text="Upload" Width="100px" />
                                <asp:Button ID="btnClose" runat="server" OnClientClick="javascript:window.close();" CausesValidation="False"
                                    CssClass="button" Text="Close" Width="100px" />
                            </td>
                        </tr>

                    </table>
                    <span class="error"><i>Note: Uploading Excel sheet must 
            contain a field named &quot;ID&quot;.The records in the &quot;ID&quot; field must match with the 
            OASIS Unique Number or Uploading Contact Excel Sheet.</i></span>
                    <asp:RadioButtonList ID="RadioPrimaryId" runat="server" Visible="False">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="Panel1" runat="server" Visible="false">

        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" class="title-bg-lite">Step 2 : Select Primary Field &amp; Confirm&nbsp;the data</td>
            </tr>
            <tr>
                <td align="left">


                    <table width="100%">

                        <tr>
                            <td align="left">
                                <span class="error">Please confirm the data below and save the data.</span>
                                <asp:GridView ID="GridData" runat="server" CssClass="table table-bordered table-row" EnableTheming="false" Width="100%">
                                    <HeaderStyle BackColor="LightGray" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" CausesValidation="False" />
                                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            </td>
                        </tr>
                    </table>



                </td>
            </tr>
        </table>

    </asp:Panel>
</div>
<br />
<asp:HiddenField ID="HiddenData" runat="server" />
<asp:HiddenField ID="HiddenBsu" runat="server" />
<asp:HiddenField ID="Hiddenemp_id" runat="server" />
<asp:HiddenField ID="Hiddentype" runat="server" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
    ControlToValidate="txttitlte" Display="None" ErrorMessage="Please enter Title"
    SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
    ControlToValidate="FileUpload1" Display="None"
    ErrorMessage="Please upload an Excel File" SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server"
    ShowMessageBox="True" ShowSummary="False" />

