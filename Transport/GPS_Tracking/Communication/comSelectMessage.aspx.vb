﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Transport_GPS_Tracking_Communication_comSelectMessage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " select cms_id,cms_date, '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(cms_sms_text,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,cms_sms_text,'Openss('''+ convert(varchar, cms_id) +'''); return false;' as OPENW " & _
                        " from com_manage_sms where 1=1 AND CMS_BSU_ID ='" & Session("sBsuid") & "' AND ISNULL(CMS_DELETED,'False')='False' "

        Dim Txt1 As String
        Dim Txt3 As String

        If GridMSMS.Rows.Count > 0 Then

            Txt1 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            Txt3 = DirectCast(GridMSMS.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()

            If Txt1.Trim() <> "" Then
                str_query &= " and replace(cms_id,' ','') like '%" & Txt1.Replace(" ", "") & "%' "
            End If

            If Txt3.Trim() <> "" Then
                str_query &= " and replace(cms_sms_text,' ','') like '%" & Txt3.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by CMS_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("cms_id")
            dt.Columns.Add("cms_date")
            dt.Columns.Add("hide")
            dt.Columns.Add("tempview")
            dt.Columns.Add("cms_sms_text")


            Dim dr As DataRow = dt.NewRow()
            dr("cms_id") = ""
            dr("cms_date") = ""
            dr("hide") = ""
            dr("tempview") = ""
            dr("cms_sms_text") = ""

            dt.Rows.Add(dr)
            GridMSMS.DataSource = dt
            GridMSMS.DataBind()

            DirectCast(GridMSMS.Rows(0).FindControl("Image1"), Image).Visible = False
        Else
            GridMSMS.DataSource = ds
            GridMSMS.DataBind()

        End If

        If GridMSMS.Rows.Count > 0 Then

            DirectCast(GridMSMS.HeaderRow.FindControl("Txt1"), TextBox).Text = Txt1
            DirectCast(GridMSMS.HeaderRow.FindControl("Txt3"), TextBox).Text = Txt3

        End If


    End Sub

    Protected Sub GridMSMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridMSMS.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridMSMS_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMSMS.RowCommand

        If e.CommandName = "search" Then
            BindGrid()
        End If

    End Sub
End Class
