﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_Communication_comSendMessage
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            TBL2.Visible = False
            notification()
            If Request.QueryString("templateid").Trim() <> "" Then
                HiddenTemplateid.Value = Request.QueryString("templateid").Trim()

            End If
            If Request.QueryString("grpid").Trim() <> "" Then
                HiddenGroupid.Value = Request.QueryString("grpid").Trim()
            End If

            If Request.QueryString("logid").Trim() <> "" Then
                Hiddenlogid.Value = Request.QueryString("logid").Trim()
            End If

            HiddenMessagetype.Value = Request.QueryString("messagetype").Trim()
            HiddenModule.Value = Request.QueryString("module").Trim()
            Hiddenbsuid.Value = Session("sbsuid")

            BindHrsMins()

        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub notification()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select DATEDIFF(second,SCHEDULE_ON_TIME,GETDATE())SEC from COM_NOTIFICATIONS"

        Dim sec As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If sec > 5 Then
            lblsmessage.Text = "Messaging System is offline.Please contact the administrator."
            btnok2.Visible = False
        Else
            btnok2.Visible = True
        End If

    End Sub

    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub

    Protected Sub RadioSend_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioSend.SelectedIndexChanged
        lblsmessage.Text = ""
        If RadioSend.SelectedValue = "1" Then
            TR1.Visible = False
        Else
            TR1.Visible = True
        End If
    End Sub

    Public Function ValidateControls() As Boolean
        Dim rval = True

        If Not IsDate(txtdate.Text.Trim) And RadioSend.SelectedValue = "2" Then
            lblsmessage.Text = "Please enter valid date"
            rval = False
        End If

        Return rval
    End Function

    Protected Sub btnok2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok2.Click
        btnok2.Enabled = False
        lblsmessage.Text = ""
        If ValidateControls() Then
            Dim commit = True
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@MODULE_TYPE", HiddenModule.Value)
            pParms(1) = New SqlClient.SqlParameter("@MESSGE_TYPE", HiddenMessagetype.Value)
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)

            'pParms(4) = New SqlClient.SqlParameter("@MERGE_ID", 0)

            If HiddenGroupid.Value <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@GROUP_ID", Convert.ToInt16(HiddenGroupid.Value))
            End If

            If Hiddenlogid.Value <> "" Then
                pParms(6) = New SqlClient.SqlParameter("@EXCEL_LOG_ID", Convert.ToInt16(Hiddenlogid.Value))
            End If


            If RadioSend.SelectedValue = "2" Then

                Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())
                Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
                Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
                Dim hours = ts.TotalHours
                If hours < 0 Then
                    pParms(7) = New SqlClient.SqlParameter("@SCHEDULE", "True")
                    pParms(8) = New SqlClient.SqlParameter("@SEND_DATE_TIME", dt)
                Else
                    commit = False
                    lblsmessage.Text = "Date time is past"
                End If
            Else
                pParms(7) = New SqlClient.SqlParameter("@SCHEDULE", "False")
            End If

            pParms(9) = New SqlClient.SqlParameter("@TEMPLATE_ID", Convert.ToInt16(HiddenTemplateid.Value))

            If commit Then
                Dim sendid = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_SEND_MASTER", pParms)
                btnok2.Visible = False
                If RadioSend.SelectedValue = "2" Then
                    lblsmessage.Text = "Schedule done successfully"
                Else
                    TBL1.Visible = False
                    TBL2.Visible = True
                    HiddenSendingId.Value = sendid
                    'lblsmessage.Text = "Sending in progress....."
                End If
            Else
                btnok2.Visible = True
            End If


        End If

    End Sub

End Class
