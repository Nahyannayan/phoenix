﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_UserControls_comChangeActions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenRecordid.Value = Request.QueryString("sendid")
            BindHrsMins()
            CheckMessageSending()
            checkProgress()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub


    Public Function CheckMessageSending() As Boolean
        lblsmessage.Text = ""
        Dim returnsent As Boolean = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim sql_query = "SELECT top 1 * from GPS_MESSAGE_SEND_LOG WHERE SEND_ID='" & HiddenRecordid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
            returnsent = True
        End If

        If returnsent = True Then
            Panel1.Visible = False
            lblsmessage.Text = "Message has been sent."
            btnok0.Visible = True
        Else
            btnok1.Visible = False
            Panel1.Visible = True
        End If

        Return returnsent
    End Function

    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
        btnok.Enabled = False
        If CheckMessageSending() = False Then

            If IsDate(txtdate.Text.Trim) Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
                Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

                Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
                Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
                Dim hours = ts.TotalHours
                If hours < 0 Then
                    ''Update Record
                    Dim sql_query = "UPDATE GPS_MESSAGE_SEND_MASTER SET SEND_DATE_TIME= CONVERT(DATETIME,'" & dt & "') WHERE SEND_ID='" & HiddenRecordid.Value & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
                    lblsmessage.Text = "Message schedule changed successfully."
                    Panel1.Visible = False
                    btnok0.Visible = True

                Else
                    lblsmessage.Text = "Date time is past"
                    Panel1.Visible = True
                End If
            Else
                lblsmessage.Text = "Enter valid date time"
            End If

        End If

    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If CheckMessageSending() = False Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim sql_query = "DELETE GPS_MESSAGE_SEND_MASTER WHERE SEND_ID='" & HiddenRecordid.Value & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
            lblsmessage.Text = "Message schedule deleted successfully."
            Panel1.Visible = False
            btnok0.Visible = True
        End If

    End Sub

    Public Function checkProgress() As Boolean
        Dim rval = True
        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        'Dim sql_query = "SELECT FINISHED FROM  GPS_MESSAGE_SEND_MASTER WHERE SEND_ID='" & HiddenRecordid.Value & "'"
        'Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_query)
        'If val = "False" Then
        '    rval = False
        '    btnok1.Visible = False
        'Else
        '    btnok1.Visible = True
        'End If
        Return rval
    End Function

    Protected Sub btnok1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok1.Click

        If checkProgress() Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim sql_query = "UPDATE GPS_MESSAGE_SEND_MASTER SET RESEND='True',FINISHED='False' WHERE SEND_ID='" & HiddenRecordid.Value & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
            lblsmessage.Text = "Message will be resent to non sent recepinets.Please see the log for updates."
            btnok1.Visible = False
        Else
            lblsmessage.Text = "Message sending is still on progress.Please wait for to finish the process."
        End If

    End Sub

End Class
