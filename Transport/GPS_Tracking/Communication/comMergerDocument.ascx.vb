﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class masscom_UserControls_comMergerDocument
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsu.Value = Session("sBsuid")
            Hiddenemp_id.Value = Session("EmployeeId")
            Hiddentype.Value = Request.QueryString("Type")
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Protected Sub btnExtract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExtract.Click
        lblmessage.Text = ""
        Try
            Dim myDataset As New DataSet()

            If FileUpload1.HasFile Then

                Dim strConnE As String = ""
                Dim filenam As String = FileUpload1.PostedFile.FileName

                Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                Dim filename = FileUpload1.FileName.ToString()
                Dim savepath = path + "NewsLettersEmails/" + Session("EmployeeId") + "_MERGE_DATA_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)
                FileUpload1.SaveAs(savepath)

                HiddenData.Value = savepath

                Try
                    strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savepath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                    Dim conn As New OleDbConnection(strConnE)
                    Dim myData As New OleDbDataAdapter
                    myData.SelectCommand = New OleDbCommand("SELECT * FROM [Sheet1$] ", conn)

                    myData.TableMappings.Add("Table", "ExcelTest")
                    myData.Fill(myDataset)

                Catch ex As Exception
                    strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savepath & ";Extended Properties=""Excel 8.0;"""
                    Dim conn As New OleDbConnection(strConnE)
                    Dim myData As New OleDbDataAdapter
                    myData.SelectCommand = New OleDbCommand("SELECT * FROM [Sheet1$] ", conn)

                    myData.TableMappings.Add("Table", "ExcelTest")
                    myData.Fill(myDataset)

                End Try


                Dim temp As String = ""

                Dim val = myDataset.Tables(0).Columns.Count
                Dim i = 0
                Dim spaceflag = 0
                Dim spaceval As String = ""

                For i = 0 To val - 1

                    Dim columnName As String = myDataset.Tables(0).Columns(i).ColumnName.Trim()
                    If columnName.Contains(" ") Then
                        spaceval = columnName
                        spaceflag = 1
                        Exit For
                    End If
                    If i = 0 Then
                        temp = columnName
                    Else
                        temp &= "," & columnName
                    End If

                Next


                If spaceflag = 0 Then

                    Dim split As String() = temp.Split(",")

                    Dim dt As New DataTable


                    dt.Columns.Add("TEXT")
                    dt.Columns.Add("VALUE")

                    Dim idflag = 0

                    For i = 0 To split.Length - 1
                        Dim dr As DataRow = dt.NewRow()
                        dr("TEXT") = split(i)
                        dr("VALUE") = "<$" & split(i) & "$>"
                        dt.Rows.Add(dr)

                        If split(i) = "ID" Then
                            idflag = 1
                        End If

                    Next

                    If idflag = 0 Then
                        lblmessage.Text = "Uploaded excel sheet dosent have any ""ID"" field. Please include it with valid data defined in OASIS."
                        Panel1.Visible = False
                        If System.IO.File.Exists(savepath) Then
                            System.IO.File.Delete(savepath)
                        End If
                    Else

                        RadioPrimaryId.DataSource = dt
                        RadioPrimaryId.DataTextField = "TEXT"
                        RadioPrimaryId.DataValueField = "VALUE"
                        RadioPrimaryId.DataBind()

                        Panel1.Visible = True
                        Panel2.Visible = False
                        Dim ds As New DataSet
                        Dim sReader As New System.IO.StringReader(myDataset.GetXml())
                        ds.ReadXml(sReader)
                        GridData.DataSource = ds
                        GridData.DataBind()
                        sReader.Close()
                        sReader.Dispose()
                    End If



                Else
                    lblmessage.Text = "Space in the column : " & spaceval & " . Please remove space. "
                    If System.IO.File.Exists(savepath) Then
                        System.IO.File.Delete(savepath)
                    End If

                    Panel1.Visible = False
                End If
            Else
                lblmessage.Text = "Please upload an Excel file"

            End If

        Catch ex As Exception
            lblmessage.Text = "Error : " & ex.Message
        End Try
    End Sub

    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

    Public Function CheckDuplicates() As Boolean
        Dim returnval = True
        Dim strConnE = ""
        Dim mydataset As New DataSet
        Try
            strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & HiddenData.Value & "; Extended Properties=""Excel 12.0;HDR=YES;"""
            Dim conn As New OleDbConnection(strConnE)
            Dim myData As New OleDbDataAdapter
            myData.SelectCommand = New OleDbCommand("SELECT * FROM [Sheet1$] ", conn)

            myData.TableMappings.Add("Table", "ExcelTest")
            myData.Fill(mydataset)

        Catch ex As Exception
            strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & HiddenData.Value & ";Extended Properties=""Excel 8.0;"""
            Dim conn As New OleDbConnection(strConnE)
            Dim myData As New OleDbDataAdapter
            myData.SelectCommand = New OleDbCommand("SELECT * FROM [Sheet1$] ", conn)

            myData.TableMappings.Add("Table", "ExcelTest")
            myData.Fill(mydataset)

        End Try
        Dim I = 0
        ''Check Duplicate records
        For i = 0 To mydataset.Tables(0).Rows.Count - 1
            Dim myTable As New DataTable
            myTable = mydataset.Tables(0)

            Dim strcond = " " & "ID" & " = " & mydataset.Tables(0).Rows(i).Item("ID").ToString()
            Dim drw As DataRow()
            drw = myTable.Select(strcond)

            If drw.Length > 1 Then
                returnval = False
                Panel2.Visible = True
                Panel1.Visible = False
                lblmessage.Text = "Duplicate values found in the - " & "ID" & " - column. " & "ID" & " :  " & mydataset.Tables(0).Rows(i).Item("ID").ToString()
                lblmessage.Text &= ". <br> Please remove duplicate values and upload the excel file"
                Exit For
            End If

        Next

        ''Check Duplicate columns
        Dim hash As New Hashtable
        For i = 0 To mydataset.Tables(0).Columns.Count - 1

            Dim columnName As String = mydataset.Tables(0).Columns(i).ColumnName.Trim()
            If hash.ContainsKey(columnName) = False Then
                hash.Add(columnName, columnName)
            Else
                returnval = False
                Panel2.Visible = True
                Panel1.Visible = False
                lblmessage.Text = "Duplicate Column found. Column Name : " & columnName
                Exit For
            End If

        Next

        Return returnval

    End Function
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        If CheckDuplicates() Then
            Try
                Dim val = ""
                For Each item As ListItem In RadioPrimaryId.Items
                    If val = "" Then
                        val = item.Text & ">" & item.Value
                    Else
                        val &= "," & item.Text & ">" & item.Value
                    End If
                Next

                If val <> "" Then
                    Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
                    Dim transaction As SqlTransaction
                    connection.Open()
                    transaction = connection.BeginTransaction()
                    lblmessage.Text = ""
                    Try
                        Dim pParms(7) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@MERGE_TITLE_DES", txttitlte.Text.Trim().ToUpper())
                        pParms(1) = New SqlClient.SqlParameter("@MERGE_TABLE", HiddenData.Value)
                        pParms(2) = New SqlClient.SqlParameter("@MERGE_UNIQUE_FIELD_NAME", "ID")
                        pParms(3) = New SqlClient.SqlParameter("@MERGE_FIELDS", val)
                        pParms(4) = New SqlClient.SqlParameter("@MERGE_BSU", HiddenBsu.Value)
                        pParms(5) = New SqlClient.SqlParameter("@MERGE_USER_ID", Hiddenemp_id.Value)
                        pParms(6) = New SqlClient.SqlParameter("@MERGE_TYPE", Hiddentype.Value)
                        lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "COM_MERGE_TABLES_INSERT", pParms)
                        transaction.Commit()

                        Panel2.Visible = True
                        txttitlte.Text = ""
                    Catch ex As Exception
                        transaction.Rollback()

                    Finally
                        connection.Close()

                    End Try
                Else
                    lblmessage.TabIndex = "Please check the excel sheet."

                End If

            Catch ex As Exception
                lblmessage.Text = "Error : " & ex.Message

            End Try
            Panel1.Visible = False
        End If



    End Sub

  
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Panel1.Visible = False
        Panel2.Visible = True
        txttitlte.Text = ""
    End Sub
End Class
