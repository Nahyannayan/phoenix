Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_comViewSmsText
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            Hiddenmessageid.Value = Request.QueryString("cmsid").Trim()
            BindTemplate()
            bind()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If

    End Sub

    Public Sub BindTemplate()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select MERGE_ID,MERGE_TITLE_DES from COM_MERGE_TABLES where MERGE_BSU='" & Hiddenbsuid.Value & "' AND MERGE_TYPE='SMS'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtemplate.DataSource = ds
            ddtemplate.DataTextField = "MERGE_TITLE_DES"
            ddtemplate.DataValueField = "MERGE_ID"
            ddtemplate.DataBind()

            Dim item As New ListItem
            item.Text = "Select a Template"
            item.Value = "-1"
            ddtemplate.Items.Insert(0, item)
            Fields()
        Else
            TRDynamic.Visible = False
        End If


    End Sub

    Public Sub Fields()
        

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select MERGE_FIELDS from COM_MERGE_TABLES where MERGE_ID='" & ddtemplate.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim fields As String = ds.Tables(0).Rows(0).Item("MERGE_FIELDS").ToString()

            Dim Afields() As String = fields.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("Data")
            dt.Columns.Add("Value")

            Dim i = 0
            For i = 0 To Afields.Length - 1
                Dim cval As String = Afields(i)


                Dim csplit() As String = cval.Split(">")

                Dim dr As DataRow = dt.NewRow()

                dr("Value") = "<#" & csplit(0) & "#>"
                dr("Data") = csplit(1).Replace("#", "").Replace("<", "")

                dt.Rows.Add(dr)
            Next

            ddfields.DataSource = dt
            ddfields.DataTextField = "Data"
            ddfields.DataValueField = "Value"
            ddfields.DataBind()
            ddfields.Visible = True
            btninsert.Visible = True
        Else
            ddfields.Visible = False
            btninsert.Visible = False
        End If



    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
  
    Public Sub bind()
        txtsmstext.ReadOnly = True
        'txtusername.ReadOnly = True
        'txtpassword.ReadOnly = True
        txtfrom.ReadOnly = True

        Panel2.Visible = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim query As String = "select * from COM_MANAGE_SMS where CMS_ID=" & Hiddenmessageid.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        HiddenCount.Value = "160" ''original count
        txtsmstext.Text = ds.Tables(0).Rows(0).Item("CMS_SMS_TEXT").ToString()
        'txtusername.Text = ds.Tables(0).Rows(0).Item("CMS_USER_NAME").ToString()
        'txtpassword.Text = ds.Tables(0).Rows(0).Item("CMS_PASSWORD").ToString()
        txtfrom.Text = ds.Tables(0).Rows(0).Item("CMS_FROM").ToString()
        HiddenUTF16.Value = ds.Tables(0).Rows(0).Item("CMS_UTF_16").ToString()
        txttaken.Text = txtsmstext.Text.Length

        If ds.Tables(0).Rows(0).Item("CMS_ARABIC") = True Then
            CheckArabic.Checked = True
            txtsmstext.Attributes.Add("style", "direction:rtl")
        End If

        CheckArabic.Enabled = False
        Hiddensmstext.Value = ds.Tables(0).Rows(0).Item("CMS_SMS_TEXT").ToString()
        txtcount.Text = HiddenCount.Value - txtsmstext.Text.Length


        Dim mergetemplate = ds.Tables(0).Rows(0).Item("CMS_MERGE_ID").ToString()

        If mergetemplate <> "" Then
            ddtemplate.SelectedValue = mergetemplate
            Fields()
        End If

    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("comCreateSmsText.aspx")

    End Sub
    Public Function VisibleEditOption(ByVal cmsid As String) As Boolean
        Dim ReturnValue = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select count(*) from COM_LOG_SMS_TABLE where LOG_CMS_ID='" & cmsid & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If val = 0 Then
            ReturnValue = True
        End If

        If Session("sBusper") = "True" Then
            ReturnValue = True
        End If

        Return ReturnValue
    End Function
    Protected Sub btnedit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnedit.Click
        If VisibleEditOption(Hiddenmessageid.Value) Then
            lblmessage.Text = ""
            txtsmstext.ReadOnly = False
            'txtusername.ReadOnly = False
            'txtpassword.ReadOnly = False
            txtfrom.ReadOnly = False
            Panel1.Visible = False
            Panel2.Visible = True
        Else
            lblmessage.Text = "You cannot edit this message.Message has been sent."
        End If

    End Sub

    Protected Sub btnecancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnecancel.Click
        lblmessage.Text = ""
        Panel1.Visible = True
        Panel2.Visible = False
        txtsmstext.Text = Hiddensmstext.Value
        txtcount.Text = HiddenCount.Value - txtsmstext.Text.Length
        txtsmstext.ReadOnly = True
        'txtusername.ReadOnly = True
        'txtpassword.ReadOnly = True
        txtfrom.ReadOnly = True
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        lblmessage.Text = ""
        txtsmstext.Text = Hiddensmstext.Value
        txtcount.Text = HiddenCount.Value - txtsmstext.Text.Length
        txtsmstext.ReadOnly = False
        'txtusername.ReadOnly = False
        'txtpassword.ReadOnly = False
        txtfrom.ReadOnly = False
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click

        Response.Write("<script language='javascript'>")
        Response.Write("window.returnValue = '" & 1 & "';")
        Response.Write("window.close();")
        Response.Write(" </script>")
        ''Response.Write("<script type='text/javascript' language='JavaScript'>window.close();</script>")

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CMS_SMS_TEXT", txtsmstext.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@CMS_ID", Hiddenmessageid.Value)
        pParms(2) = New SqlClient.SqlParameter("@CMS_USER_NAME", WebConfigurationManager.AppSettings("smsUsername").ToString())
        pParms(3) = New SqlClient.SqlParameter("@CMS_PASSWORD", WebConfigurationManager.AppSettings("smspwd").ToString())
        pParms(4) = New SqlClient.SqlParameter("@CMS_FROM", txtfrom.Text.Trim())

        pParms(5) = New SqlClient.SqlParameter("@CMS_UTF_16", HiddenUTF16.Value.Replace(" ", ""))

        If ddtemplate.SelectedIndex > 0 Then
            pParms(6) = New SqlClient.SqlParameter("@CMS_MERGE_ID", ddtemplate.SelectedValue)
        End If

        If CheckArabic.Checked Then
            pParms(7) = New SqlClient.SqlParameter("@CMS_ARABIC", True)
        End If



        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_UPDATE", pParms)
        lblmessage.Text = "SMS Template Updated."
        bind()
        Panel1.Visible = True
        txtsmstext.ReadOnly = True
        'txtusername.ReadOnly = True
        'txtpassword.ReadOnly = True
        txtfrom.ReadOnly = True
    End Sub

    Protected Sub ddtemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ''Clear the Previous message
        txtsmstext.Text = ""
        txttaken.Text = ""
        txtcount.Text = ""
        Fields()
    End Sub

    Public Function CheckNotSend() As Boolean
        Dim rtv = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim query = "SELECT * FROM COM_LOG_SMS_TABLE  WHERE LOG_CMS_ID='" & Hiddenmessageid.Value & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        If ds.Tables(0).Rows.Count > 0 Then
            rtv = False
        End If
        If Session("sBusper") = "True" Then
            rtv = True
        End If

        Return rtv
    End Function
    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click

        If CheckNotSend() Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim query = "UPDATE COM_MANAGE_SMS SET CMS_DELETED='True' where CMS_ID='" & Hiddenmessageid.Value & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, query)
            lblmessage.Text = "Message deleted successfully"
        Else
            lblmessage.Text = "Message has been sent . Message cant be deleted."
        End If

    End Sub

End Class
