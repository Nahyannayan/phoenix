<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comEditPlainText.ascx.vb"
    Inherits="masscom_UserControls_comEditPlainText" %>
<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />


<script type="text/javascript">

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false;

    }


    function opennew() {

        window.open("comCreatePlainText.aspx", "", "dialogWidth:800px; dialogHeight:600px; center:yes");
        window.location.reload = window.location.reload
    }

    function Openss(id) {

        var strOpen;
        var gpid = document.getElementById('<%= HiddenGroupdid.ClientID %>').value;
        if (gpid != "") {
            strOpen = 'comSendMessagePage.aspx'
            strOpen += "?module=GPS&messagetype=EMAIL&grpid=" + gpid + "&logid=&templateid=" + id;
            //window.showModalDialog(strOpen, "", "dialogWidth:350px;dialogHeight:350px;center:yes");
            return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
            return false;
        }
        else {
            strOpen = 'comExcelEmailData.aspx'
            strOpen += "?module=GPS&Tab=0&DataId=0&templateid=" + id;
            //window.showModalDialog(strOpen, "", "dialogWidth:800px;dialogHeight:600px;center:yes");
            return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
            return false;
        }

    }

</script>
<div>
    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
    <asp:Panel ID="Panel3" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Email Plain Text Templates
                    <asp:LinkButton ID="Linkadd" runat="server">Create New Template</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GrdEmailText" runat="server" EmptyDataText="Email Plain Text not added yet." CssClass="table table-bordered table-row"
                        Width="100%" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Template ID">
                                <HeaderTemplate>
                                    Template&nbsp;ID
                                                <br />
                                    <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                        ImageUrl="~/Images/forum_search.gif" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                    <center>
                                        <%# Eval("EML_ID") %></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <HeaderTemplate>
                                    Date
                                                <br />
                                    <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                        ImageUrl="~/Images/forum_search.gif" />
                                    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                        TargetControlID="Txt2">
                                    </ajaxToolkit:CalendarExtender>

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email Text">
                                <HeaderTemplate>
                                    Email Title
                                                <br />
                                    <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                        ImageUrl="~/Images/forum_search.gif" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkView" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Attachments">
                                <HeaderTemplate>

                                    <br />
                                    Attachments
                                                <br />
                                    <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:GridView ID="GrdAttachment" runat="server" AutoGenerateColumns="false" OnRowCommand="GrdTextAttachment_RowCommand"
                                        ShowHeader="false" Width="100%" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" CommandName="select"
                                                        Text=' <%# Eval("Name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Size (Bytes)">
                                                <ItemTemplate>
                                                    <div align="right">
                                                        -(Size
                                                        <%#Eval("length")%>
                                                        Bytes)
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Remove">Delete</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this attachment?"
                                                        TargetControlID="lnkdelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <HeaderTemplate>

                                    <br />
                                    Delete
                                                <br />
                                    <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                            CommandName="RemoveDir">Delete</asp:LinkButton></center>
                                    <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this template?"
                                        TargetControlID="lnkdelete">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <HeaderTemplate>

                                    <br />
                                    Edit
                                                <br />
                                    <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                            CommandName="Editing">Edit</asp:LinkButton></center>
                                    <ajaxToolkit:ConfirmButtonExtender ID="C2" runat="server" ConfirmText="Are you sure you want to edit this template?"
                                        TargetControlID="lnkEdit">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Send">
                                <HeaderTemplate>

                                    <br />
                                    Send
                                                <br />
                                    <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="lnksend" OnClientClick='<%#Eval("OPENW")%>' runat="server">Send</asp:LinkButton>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <EmptyDataRowStyle />
                        <EditRowStyle />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel4" runat="server" Visible="False">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" class="title-bg-lite">Update Plain Text Templates
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Title</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">From Email Id</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtFrom" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Subject</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtsubject" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Display</span>
                                        </td>

                                        <td align="left">
                                            <asp:TextBox ID="txtdisplay" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Host</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txthost" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Port</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtport" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Username</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtusername" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Password</span>
                                        </td>

                                        <td align="left">
                                            <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:Label ID="Label1" runat="server" CssClass="error" Text="* Please provide if any change in Password"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td align="left" colspan="2">
                                            <telerik:RadEditor ID="txtEditEmailText" runat="server" EditModes="All" Height="600px"
                                                ToolsFile="xml/FullSetOfTools.xml" Width="750px">
                                            </telerik:RadEditor>
                                            <div id="TRDynamic" runat="server">
                                                <asp:LinkButton ID="LinkDynamic" OnClientClick="javascript:return false;" runat="server">Dynamic Text</asp:LinkButton>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel1" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" width="30%">
                                                                        <span class="field-label">Template</span>
                                                                    </td>

                                                                    <td>
                                                                        <asp:DropDownList ID="ddtemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddtemplate_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" width="30%">
                                                                        <span class="field-label">Fields</span>
                                                                    </td>

                                                                    <td>
                                                                        <asp:DropDownList ID="ddfields" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="2">
                                                                        <asp:Button ID="btninsert" runat="server" CssClass="button" Text="Insert" OnClick="btninsert_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkDynamic" Collapsed="true"
                                                            CollapsedSize="0" CollapsedText="Dynamic Text" ExpandControlID="LinkDynamic"
                                                            ExpandedSize="100" ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1"
                                                            TextLabelID="LinkDynamic">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <span class="field-label">Attachments</span>
                                        </td>

                                        <td align="left">
                                            <asp:FileUpload ID="FileUploadEditEmailtext" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btneditplaintextsave" runat="server" CssClass="button" Text="Save"
                                    ValidationGroup="EdText" />
                                <asp:Button ID="btnplaintextcancel" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEditEmailText"
            Display="None" ErrorMessage="Please Enter Email Text" SetFocusOnError="True"
            ValidationGroup="EdText"></asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
            ShowSummary="False" ValidationGroup="EdText" />
    </asp:Panel>
    <asp:HiddenField ID="HiddenFieldTemplateid" runat="server" />
    <asp:HiddenField ID="HiddenPassword" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenGroupdid" runat="server" />
</div>

<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>