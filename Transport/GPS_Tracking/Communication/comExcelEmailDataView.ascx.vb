Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.Oledb
Partial Class masscom_UserControls_comExcelEmailDataView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTempid.Value = Request.QueryString("templateid").ToString().Trim()
            HiddenModule.Value = Request.QueryString("module").ToString().Trim()
            Hiddenbsuid.Value = Session("sBsuid")
            BindGridView()
            BindHrsMins()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindHrsMins()
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddhour.Items.Insert(i, "0" & i.ToString())
            Else
                ddhour.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddmins.Items.Insert(i, "0" & i.ToString())
            Else
                ddmins.Items.Insert(i, i)
            End If
        Next
    End Sub


    Public Sub BindGridView()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_query = "Select LOG_ID,TITLE,TYPE " & _
        '                ",(select count(*) from COM_EXCEL_EMAIL_DATA_LOG B where B.LOG_ID=A.LOG_ID ) TOTAL " & _
        '                ",(select count(*) from COM_EXCEL_EMAIL_DATA_LOG C where C.LOG_ID=A.LOG_ID AND NO_ERROR_FLAG='True' ) VALID " & _
        '                ",(select count(*) from COM_EXCEL_EMAIL_DATA_LOG D where D.LOG_ID=A.LOG_ID AND NO_ERROR_FLAG='False') ERROR " & _
        '                ",'openPopup('''+ convert(varchar, LOG_ID) +''')' OPENW" & _
        '                ",'openPopup2('''+ convert(varchar, LOG_ID) +''')' OPENW2" & _
        '                " from COM_EXCEL_EMAIL_DATA_MASTER A where BSU_ID='" & Hiddenbsuid.Value & "' "
        'Dim str_query = "Select LOG_ID,TITLE,TYPE " & _
        '                      ",'' TOTAL " & _
        '                      ",''VALID " & _
        '                      ",'' ERROR " & _
        '                      ",'openPopup('''+ convert(varchar, LOG_ID) +''')' OPENW" & _
        '                      ",'openPopup2('''+ convert(varchar, LOG_ID) +''')' OPENW2" & _
        '                      " from COM_EXCEL_EMAIL_DATA_MASTER A where BSU_ID='" & Hiddenbsuid.Value & "' "

        Dim str_query = " Select BSU_ID,A.LOG_ID,TITLE,TYPE,TOTAL,ISNULL(VALID,0)VALID,ISNULL(ERROR,0)ERROR,'openPopup('''+ convert(varchar, A.LOG_ID) +''')' as OPENW " & _
                       "  from COM_EXCEL_EMAIL_DATA_MASTER A " & _
                       "  inner join(select log_id,count(*)TOTAL from COM_EXCEL_EMAIL_DATA_LOG group by log_id) b on b.log_id =a.log_id " & _
                       "  left join(select log_id,count(*)VALID from COM_EXCEL_EMAIL_DATA_LOG where NO_ERROR_FLAG='True' group by log_id) c on c.log_id =a.log_id " & _
                       "  left join(select log_id,count(*)ERROR from COM_EXCEL_EMAIL_DATA_LOG where NO_ERROR_FLAG='False' group by log_id) d on d.log_id =a.log_id " & _
                       "  where BSU_ID='" & Hiddenbsuid.Value & "'"


        Dim txttitle As String
        Dim txttype As String


        If GridView.Rows.Count > 0 Then
            txttitle = DirectCast(GridView.HeaderRow.FindControl("txttitle"), TextBox).Text.Trim()
            txttype = DirectCast(GridView.HeaderRow.FindControl("txttype"), TextBox).Text.Trim()

            If txttitle.Trim() <> "" Then
                str_query &= " and replace(TITLE,' ','') like '%" & txttitle.Replace(" ", "") & "%' "
            End If

            If txttype.Trim() <> "" Then
                str_query &= " and replace(TYPE,' ','') like '%" & txttype.Replace(" ", "") & "%' "
            End If

        End If

        str_query &= " order by LOG_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("LOG_ID")
            dt.Columns.Add("TITLE")
            dt.Columns.Add("TYPE")
            dt.Columns.Add("TOTAL")
            dt.Columns.Add("VALID")
            dt.Columns.Add("ERROR")
            dt.Columns.Add("OPENW")


            Dim dr As DataRow = dt.NewRow()
            dr("LOG_ID") = ""
            dr("TITLE") = ""
            dr("TYPE") = ""
            dr("TOTAL") = ""
            dr("VALID") = ""
            dr("ERROR") = ""
            dr("OPENW") = ""
            dt.Rows.Add(dr)
            GridView.DataSource = dt
            GridView.DataBind()

        Else
            GridView.DataSource = ds
            GridView.DataBind()

        End If

        If GridView.Rows.Count > 0 Then

            DirectCast(GridView.HeaderRow.FindControl("txttitle"), TextBox).Text = txttitle
            DirectCast(GridView.HeaderRow.FindControl("txttype"), TextBox).Text = txttype

        End If

    End Sub



    Protected Sub GridView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView.PageIndexChanging
        GridView.PageIndex = e.NewPageIndex
        BindGridView()
    End Sub

    Protected Sub GridView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView.RowCommand
        If e.CommandName = "search" Then
            BindGridView()
        End If

        If e.CommandName = "Schedule" Then
            lblsmessage2.Text = ""
            HiddenDatalogid.Value = e.CommandArgument
            MO2.Show()

        End If

    End Sub


    Protected Sub btnok2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnok2.Enabled = False
        Dim Encr_decrData As New Encryption64
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())

        Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(ddhour.SelectedValue), Convert.ToInt16(ddmins.SelectedValue), 0)
        Dim ts As TimeSpan = DateTime.Now.Subtract(dt)
        Dim hours = ts.TotalHours
        If hours < 0 Then

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EML_ID", HiddenTempid.Value)
            pParms(1) = New SqlClient.SqlParameter("@EXCEL_PATH", "Using Previous Template") '' We are taking data log id data
            pParms(2) = New SqlClient.SqlParameter("@SCHEDULE_DATE_TIME", dt)
            pParms(3) = New SqlClient.SqlParameter("@CSE_ID", 0)
            pParms(4) = New SqlClient.SqlParameter("@CGR_ID", 0)
            pParms(5) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Hiddenbsuid.Value)
            pParms(6) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(7) = New SqlClient.SqlParameter("@DATA_LOG_ID", HiddenDatalogid.Value)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_EMAIL_SCHEDULE_INSERT", pParms)

            lblsmessage2.Text = "Schedule has been successfully done"
            lblsmessage.Text = ""
            txtdate.Text = ""

            MO2.Hide()
        Else
            lblsmessage.Text = "Date time is past"
            MO2.Show()
        End If


    End Sub

    Protected Sub btncancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO2.Hide()
    End Sub

End Class
