﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comChangeActions.ascx.vb" Inherits="masscom_UserControls_comChangeActions" %>

<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<div align="center">

    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <center>
               <br />
               <asp:Label ID="lblsmessage" runat="server" ></asp:Label>
               <br />
               <asp:Button ID="btnok1" runat="server" CssClass="button" 
                   CausesValidation="false"  
                   Text="Resend" 
                    />
                            <asp:Button ID="btnok0" runat="server" CausesValidation="false" 
                   CssClass="button" OnClientClick="javascript:window.close(); return false;" 
                   Text="Cancel" Visible="False"  />
                            </center>
            <asp:Panel ID="Panel1" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg">Set Schedule Time</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="s"></asp:TextBox>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Hour</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddhour" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Mins</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddmins" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnok" runat="server" CssClass="button" Text="Ok"
                                            ValidationGroup="s" />
                                        <asp:Button ID="btncancel" runat="server" OnClientClick="javascript:window.close();" CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btndelete" runat="server" CssClass="button" Text="Delete" />
                                        <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Are you sure ?" TargetControlID="btndelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                    </td>
                                </tr>

                            </table>
                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="Image1" TargetControlID="txtdate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="txtdate" Display="None" ErrorMessage="Please Enter Date"
                                SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server"
                                ShowMessageBox="True" ShowSummary="False" ValidationGroup="s" />
                            <asp:HiddenField ID="HiddenRecordid" runat="server" />


                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>

