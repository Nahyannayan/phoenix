﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_SMS_gpsListGroupsAddOn
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
            BindGrid()
        End If

    End Sub

    Public Sub BindBsu()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

    End Sub

    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim groupid = ""
        Dim groupname = ""
        Dim transporttype = "-1"
        Dim vehno = ""
        Dim gps_unitid = ""
        Dim trip_onward = ""
        Dim trip_return = ""
        Dim use_type = "-1"

        If GridInfo.Rows.Count > 0 Then
            groupid = DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            groupname = DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            transporttype = DirectCast(GridInfo.HeaderRow.FindControl("ddTransportType"), DropDownList).SelectedValue
            vehno = DirectCast(GridInfo.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim()
            gps_unitid = DirectCast(GridInfo.HeaderRow.FindControl("Txt5"), TextBox).Text.Trim()
            trip_onward = DirectCast(GridInfo.HeaderRow.FindControl("Txt6"), TextBox).Text.Trim()
            trip_return = DirectCast(GridInfo.HeaderRow.FindControl("Txt7"), TextBox).Text.Trim()
            use_type = DirectCast(GridInfo.HeaderRow.FindControl("ddUserType"), DropDownList).SelectedValue
        End If

        Dim str_query = " select * from (SELECT GROUP_ID,A.BSU_ID,F.BSU_SHORTNAME,GROUP_DESC,TRANSPORT_TYPE AS TRANS_TYPE,CASE TRANSPORT_TYPE WHEN 'T' THEN 'TRANSPORT' ELSE 'NON TRANSPORT' END TRANSPORT_TYPE, " & _
                        " B.VEH_REGNO,GPS_UNIT_ID,D.TRP_DESCR AS ONWARD_TRIP,E.TRP_DESCR AS RETURN_TRIP,USER_TYPE,GROUP_TYPE  " & _
                        " FROM dbo.GPS_MESSAGE_GROUPS_MASTER A " & _
                        " LEFT JOIN   dbo.VW_VEH_DRIVER B ON A.VEH_ID=B.VEH_ID " & _
                        " LEFT JOIN dbo.VV_TRIPS_D D ON D.TRP_ID=A.TRIP_ONWARD " & _
                        " LEFT JOIN dbo.VV_TRIPS_D E ON E.TRP_ID=A.TRIP_RETURN " & _
                        " INNER JOIN VV_BUSINESSUNIT_M F ON F.BSU_ID=A.BSU_ID " & _
                        " where DELETED='false') AAA where 1=1"

        If ddBsu.SelectedValue <> "-1" Then
            str_query &= " AND BSU_ID='" & ddBsu.SelectedValue & "'"
        End If


        If groupid <> "" Then
            str_query &= " AND GROUP_ID LIKE '%" & groupid & "%'"
        End If

        If groupname <> "" Then
            str_query &= " AND GROUP_DESC LIKE '%" & groupname & "%'"
        End If

        If transporttype <> "-1" Then
            str_query &= " AND TRANS_TYPE = '" & transporttype & "'"
        End If

        If vehno <> "" Then
            str_query &= " AND VEH_REGNO LIKE '%" & vehno & "%'"
        End If

        If gps_unitid <> "" Then
            str_query &= " AND GPS_UNIT_ID LIKE '%" & gps_unitid & "%'"
        End If

        If trip_onward <> "" Then
            str_query &= " AND ONWARD_TRIP LIKE '%" & trip_onward & "%'"
        End If

        If trip_return <> "" Then
            str_query &= " AND RETURN_TRIP LIKE '%" & trip_return & "%'"
        End If


        If use_type <> "-1" Then
            str_query &= " AND USER_TYPE = '" & use_type & "'"
        End If

        str_query &= " ORDER BY GROUP_ID DESC"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridInfo.DataSource = ds
        GridInfo.DataBind()

        If GridInfo.Rows.Count > 0 Then
            DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text = groupid
            DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text = groupname
            DirectCast(GridInfo.HeaderRow.FindControl("ddTransportType"), DropDownList).SelectedValue = transporttype
            DirectCast(GridInfo.HeaderRow.FindControl("Txt4"), TextBox).Text = vehno
            DirectCast(GridInfo.HeaderRow.FindControl("Txt5"), TextBox).Text = gps_unitid
            DirectCast(GridInfo.HeaderRow.FindControl("Txt6"), TextBox).Text = trip_onward
            DirectCast(GridInfo.HeaderRow.FindControl("Txt7"), TextBox).Text = trip_return
            DirectCast(GridInfo.HeaderRow.FindControl("ddUserType"), DropDownList).SelectedValue = use_type
        End If


    End Sub

    Public Sub search(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub

    Protected Sub lnkadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkadd.Click
        Response.Redirect("~/Transport/GPS_Tracking/Communication/gpsCreateGroupsAddOnPage.aspx")
    End Sub


    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "search" Then
            BindGrid()
        End If

        If e.CommandName = "list" Then
            Response.Redirect("~/Transport/GPS_Tracking/Communication/gpsListGroupsAddOnList.aspx?gpid=" & e.CommandArgument)
        End If

        If e.CommandName = "deleting" Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

            Dim query = "UPDATE GPS_MESSAGE_GROUPS_MASTER SET DELETED='True' where GROUP_ID='" & e.CommandArgument & "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, query)

        End If


        If e.CommandName = "sms" Then
            Response.Redirect("~/Transport/GPS_Tracking/Communication/comManageSMS.aspx?gpid=" & e.CommandArgument)
        End If

        If e.CommandName = "email" Then
            Response.Redirect("~/Transport/GPS_Tracking/Communication/comEditPlainText.aspx?gpid=" & e.CommandArgument)
        End If

    End Sub



    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
