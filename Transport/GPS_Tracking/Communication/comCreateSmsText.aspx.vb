Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class masscom_comCreateSmsText
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sBsuid")
            HiddenCount.Value = "160"
            txtcount.Text = HiddenCount.Value
            BindTemplate()
            HiddenRefresh.Value = 0
        End If

        If HiddenRefresh.Value = 1 Then
            BindTemplate()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        'AssignRights()
    End Sub


    Public Function BindTemplate() As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select MERGE_ID,MERGE_TITLE_DES from COM_MERGE_TABLES where MERGE_BSU='" & Hiddenbsuid.Value & "' AND MERGE_TYPE='SMS'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtemplate.DataSource = ds
            ddtemplate.DataTextField = "MERGE_TITLE_DES"
            ddtemplate.DataValueField = "MERGE_ID"
            ddtemplate.DataBind()

            Dim item As New ListItem
            item.Text = "Select a Template"
            item.Value = "-1"
            ddtemplate.Items.Insert(0, item)
            Fields()
        Else
            TRDynamic.Visible = False
        End If

        Return ""

    End Function

    Public Sub Fields()
        ''Clear the Previous message
        txtsmstext.Text = ""
        txttaken.Text = ""
        txtcount.Text = ""

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select MERGE_FIELDS from COM_MERGE_TABLES where MERGE_ID='" & ddtemplate.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim fields As String = ds.Tables(0).Rows(0).Item("MERGE_FIELDS").ToString()

            Dim Afields() As String = fields.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("Data")
            dt.Columns.Add("Value")

            Dim i = 0
            For i = 0 To Afields.Length - 1
                Dim cval As String = Afields(i)


                Dim csplit() As String = cval.Split(">")

                Dim dr As DataRow = dt.NewRow()

                dr("Value") = "$" & csplit(0) & "$"
                dr("Data") = csplit(1).Replace("$", "").Replace("<", "")

                dt.Rows.Add(dr)
            Next

            ddfields.DataSource = dt
            ddfields.DataTextField = "Data"
            ddfields.DataValueField = "Value"
            ddfields.DataBind()
            ddfields.Visible = True
            btninsert.Visible = True
        Else
            ddfields.Visible = False
            btninsert.Visible = False
        End If



    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(19) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@CMS_SMS_TEXT", txtsmstext.Text.Trim())

        pParms(1) = New SqlClient.SqlParameter("@CMS_USER_NAME", WebConfigurationManager.AppSettings("smsUsername").ToString())
        pParms(2) = New SqlClient.SqlParameter("@CMS_PASSWORD", WebConfigurationManager.AppSettings("smspwd").ToString())

        pParms(3) = New SqlClient.SqlParameter("@CMS_FROM", txtfrom.Text.Trim())

        pParms(4) = New SqlClient.SqlParameter("@CMS_BSU_ID", Hiddenbsuid.Value)
        pParms(5) = New SqlClient.SqlParameter("@CMS_EMP_ID", Session("EmployeeId"))

        If ddtemplate.SelectedIndex > 0 And CheckArabic.Checked = False Then
            pParms(6) = New SqlClient.SqlParameter("@CMS_MERGE_ID", ddtemplate.SelectedValue)
        End If

        pParms(7) = New SqlClient.SqlParameter("@CMS_UTF_16", HiddenUTF16.Value.Replace(" ", ""))

        If CheckArabic.Checked Then
            pParms(8) = New SqlClient.SqlParameter("@CMS_ARABIC", True)
        End If
        pParms(11) = New SqlClient.SqlParameter("@CMS_MODULE", "TRANSPORT")


        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "COM_MANAGE_SMS_INSERT", pParms)
        lblmessage.Text = "SMS Template Created."

    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        txtsmstext.Text = ""
        'txtusername.Text = ""
        'txtpassword.Text = ""
        txtfrom.Text = ""
        txtcount.Text = HiddenCount.Value
        HiddenUTF16.Value = ""
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Write("<script language='javascript'>")
        Response.Write("window.returnValue = '" & 1 & "';")
        Response.Write("window.close();")
        Response.Write(" </script>")

    End Sub

    Protected Sub CheckArabic_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckArabic.CheckedChanged
        txtsmstext.Text = ""
        txttaken.Text = ""
        txtcount.Text = ""
        lblmessage.Text = ""
        If CheckArabic.Checked Then
            TRDynamic.Visible = False
            txtsmstext.Attributes.Add("style", "direction:rtl")
            TRDynamic.Visible = False

        Else
            TRDynamic.Visible = True
            txtsmstext.Attributes.Add("style", "direction:ltr")

            BindTemplate()
        End If

    End Sub

    Protected Sub ddtemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtemplate.SelectedIndexChanged
        Fields()
    End Sub

End Class
