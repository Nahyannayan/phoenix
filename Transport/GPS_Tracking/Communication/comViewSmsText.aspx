<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comViewSmsText.aspx.vb" Inherits="masscom_comViewSmsText" %>


<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>SMS Text</title>
    <%--  <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/cssfiles/Popup.css" rel="stylesheet" />

    <script type="text/javascript" language="JavaScript">


        function textCounter() {
            var field = document.getElementById("txtsmstext");
            var countfield = document.getElementById("txtcount");
            var maxlimit = document.getElementById("HiddenCount").value;
            if (field.value.length > maxlimit) // if too long...trim it!
                field.value = field.value.substring(0, maxlimit);
                // otherwise, update 'characters left' counter
            else
                countfield.value = maxlimit - field.value.length;

            document.getElementById("<%=txttaken.ClientID %>").value = field.value.length
        }


    </script>
    <script type="text/javascript">
        function dec2hex(textString) {
            return (textString + 0).toString(16).toUpperCase();
        }

        function dec2hex4(textString) {
            var hexequiv = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
            return hexequiv[(textString >> 12) & 0xF] + hexequiv[(textString >> 8) & 0xF] + hexequiv[(textString >> 4) & 0xF] + hexequiv[textString & 0xF];
        }
        function convertCP2UTF16() {
            var outputString = "";

            var textString = convertChar2CP(document.getElementById("txtsmstext").value);

            textString = textString.replace(/^\s+/, '');



            if (textString.length == 0) { return ""; }
            textString = textString.replace(/\s+/g, ' ');

            var listArray = textString.split(' ');



            for (var i = 0; i < listArray.length; i++) {
                var n = parseInt(listArray[i], 16);



                if (i > 0) { outputString += ' '; }

                if (n <= 0xFFFF) {
                    outputString += dec2hex4(n);

                } else if (n <= 0x10FFFF) {
                    n -= 0x10000
                    outputString += dec2hex4(0xD800 | (n >> 10)) + ' ' + dec2hex4(0xDC00 | (n & 0x3FF));
                } else {
                    outputString += 'HAI BOSS' + dec2hex(n) + '!';
                }
            }
            document.getElementById("HiddenUTF16").value = outputString
            return (outputString);
        }
        function convertChar2CP(textString) {
            var outputString = "";
            var haut = 0;
            var n = 0;
            for (var i = 0; i < textString.length; i++) {
                var b = textString.charCodeAt(i);  // alert('b:'+dec2hex(b));
                if (b < 0 || b > 0xFFFF) {
                    outputString += 'HAI BOSS' + dec2hex(b) + '!';
                }
                if (haut != 0) {
                    if (0xDC00 <= b && b <= 0xDFFF) {
                        outputString += dec2hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00)) + ' ';
                        haut = 0;
                        continue;
                    } else {
                        outputString += 'HAI BOSS' + dec2hex(haut) + '!';
                        haut = 0;
                    }
                }
                if (0xD800 <= b && b <= 0xDBFF) {
                    haut = b;
                } else {
                    outputString += dec2hex(b) + ' ';
                }
            }
            return (outputString.replace(/ $/, ''));
        }


        function point() {

            var tempval;
            tempval = document.getElementById('<%=txtsmstext.ClientID %>').value

            tempval = tempval + document.getElementById('<%=ddfields.ClientID %>').value
            document.getElementById('<%=txtsmstext.ClientID %>').value = tempval
            textCounter();

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <center>
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>

            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left">
                        <div>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Message Text</td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:CheckBox ID="CheckArabic" runat="server" AutoPostBack="True" Text="Arabic" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtsmstext" runat="server" Height="200px" TextMode="MultiLine" onKeyDown="textCounter();" onKeyUp="textCounter();" EnableTheming="False"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr id="TRDynamic" runat="server">
                                    <td colspan="4">
                                        <asp:LinkButton ID="LinkDynamic" runat="server" OnClientClick="javascript:return false;">Dynamic Text</asp:LinkButton>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="Panel1T" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td align="left" width="20%"><span class="field-label">Template</span>
                                                            </td>

                                                            <td align="left">
                                                                <asp:DropDownList ID="ddtemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddtemplate_SelectedIndexChanged">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" width="20%"><span class="field-label">Fields</span></td>

                                                            <td align="left">
                                                                <asp:DropDownList ID="ddfields" runat="server">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>

                                                            <td align="center" colspan="2">
                                                                <asp:Button ID="btninsert" runat="server" CssClass="button" OnClientClick="point(); return false;"
                                                                    Text="Insert" /></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>

                                                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkDynamic"
                                                    Collapsed="true" CollapsedSize="0" CollapsedText="Dynamic Text" ExpandControlID="LinkDynamic"
                                                    ExpandedSize="100" ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1T"
                                                    TextLabelID="LinkDynamic">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">Characters Left</td>

                                    <td align="left" colspan="3">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txttaken" runat="server" ReadOnly="True"></asp:TextBox>
                                                <asp:TextBox ID="txtcount" runat="server" ReadOnly="True"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <%--            <tr  >
                <td align="right" style="width: 100px">
                    User Name</td>
                <td align="center" style="width: 2px">
                    :</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtusername" runat="server" Width="121px"></asp:TextBox></td>
            </tr>
            <tr >
                <td align="right" style="width: 100px">
                    Password</td>
                <td align="center" style="width: 2px">
                    :</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtpassword" runat="server" Width="121px"></asp:TextBox></td>
            </tr>--%>
                                <tr>
                                    <td align="left" width="20%">From
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtfrom" runat="server"></asp:TextBox></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <center>
                                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" />
                                                <asp:Button ID="btnedit" runat="server" Text="Edit" CausesValidation="False" CssClass="button" />
                                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="button" />
                                                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                                            </asp:Panel>
                                            <asp:Panel ID="Panel2" runat="server" Width="100%">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="convertCP2UTF16 ()" CssClass="button" /><asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="button" /><asp:Button ID="btnecancel" runat="server" Text="Cancel" CssClass="button" />
                                            </asp:Panel>
                                            &nbsp;
                                        </center>
                                        <center>
                                            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>&nbsp;</center>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <input id="HiddenCount" runat="server" type="hidden" />
                        <input id="Hiddensmstext" runat="server" type="hidden" /></td>
                </tr>
            </table>

        </center>

        <asp:HiddenField ID="Hiddenmessageid" runat="server" />
        <asp:HiddenField ID="HiddenUTF16" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtsmstext"
            Display="None" ErrorMessage="Please enter message text" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:RequiredFieldValidator
                ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtfrom" Display="None"
                ErrorMessage="Please enter From details" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" DisplayMode="List" ShowMessageBox="True"
                    ShowSummary="False" />

    </form>
</body>
</html>
