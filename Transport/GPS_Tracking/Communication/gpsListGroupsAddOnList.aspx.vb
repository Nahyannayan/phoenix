﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_SMS_gpsListGroupsAddOnList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            BindGrid(Request.QueryString("gpid").ToString())

        End If

        If GridInfo.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.FooterRow.FindControl("btnexport"), Button))
        End If

    End Sub


    Public Sub BindGrid(ByVal Groupid As String, Optional ByVal export As Boolean = False)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(7) As SqlClient.SqlParameter

        Dim query = " select * from GPS_MESSAGE_GROUPS_MASTER where GROUP_ID='" & Groupid & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        If ds.Tables(0).Rows.Count > 0 Then

            If ds.Tables(0).Rows(0).Item("TRANSPORT_TYPE").ToString.Trim() = "T" Then

                pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
            Else
                pParms(0) = New SqlClient.SqlParameter("@OPTION ", 1)
            End If

            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ds.Tables(0).Rows(0).Item("BSU_ID").ToString.Trim())

            If ds.Tables(0).Rows(0).Item("VEH_ID").ToString.Trim() <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@VEH_ID", ds.Tables(0).Rows(0).Item("VEH_ID").ToString.Trim())
            End If

            If ds.Tables(0).Rows(0).Item("GPS_UNIT_ID").ToString.Trim() <> "" Then
                pParms(3) = New SqlClient.SqlParameter("@GPS_ID", ds.Tables(0).Rows(0).Item("GPS_UNIT_ID").ToString.Trim())
            End If

            If ds.Tables(0).Rows(0).Item("TRIP_ONWARD").ToString.Trim() <> "" Then
                pParms(4) = New SqlClient.SqlParameter("@TRIP_ONWARD", ds.Tables(0).Rows(0).Item("TRIP_ONWARD").ToString.Trim())
            End If

            If ds.Tables(0).Rows(0).Item("TRIP_RETURN").ToString.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@TRIP_RETURN", ds.Tables(0).Rows(0).Item("TRIP_RETURN").ToString.Trim())
            End If

            pParms(6) = New SqlClient.SqlParameter("@USER_TYPE", ds.Tables(0).Rows(0).Item("USER_TYPE").ToString.Trim())

            Dim gtype = ds.Tables(0).Rows(0).Item("GROUP_TYPE").ToString.Trim()
            Dim aonid = ds.Tables(0).Rows(0).Item("ADDON_ID").ToString.Trim()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_GROUP_USER", pParms)

            If gtype = "AD" Then

                Dim filter = "USR_ID in (" & aonid & ")"
                Dim dvwView As New DataView
                dvwView = ds.Tables(0).DefaultView
                dvwView.RowFilter = filter

                GridInfo.DataSource = dvwView
                GridInfo.DataBind()
                lbltotal.Text = " Total Count :" & dvwView.ToTable.Rows.Count

                If export Then
                    Dim dt As DataTable = dvwView.ToTable
                    ExportExcel(dt)
                End If

            Else

                GridInfo.DataSource = ds
                GridInfo.DataBind()
                lbltotal.Text = " Total Count :" & ds.Tables(0).Rows.Count

                If export Then
                    Dim dt As DataTable = ds.Tables(0)
                    ExportExcel(dt)
                End If

            End If

        End If

    End Sub


    Public Sub exporting(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid(Request.QueryString("gpid").ToString(), True)
    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt)
        ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub


    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid(Request.QueryString("gpid").ToString())
    End Sub

 
End Class
