﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comCommunicationReport.ascx.vb"
    Inherits="Transport_GPS_Tracking_Communication_comCommunicationReport" %>

<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="/cssfiles/Popup.css" rel="stylesheet" />
<script type="text/javascript">

    function openWindowActions(sendid) {

        var sFeatures;
        sFeatures = "dialogWidth: 300px; ";
        sFeatures += "dialogHeight: 300px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "comChangeActions.aspx?sendid=" + sendid

        var result;
        //result = window.showModalDialog(strOpen, "", sFeatures);
        //window.location.reload(true)
        return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
        return false;

    }

    function openLog(sendid, usertype) {
        window.open("comSendLog.aspx?send_id=" + sendid + '&usertype=' + usertype, "", "Width=1000,Height=600,scrollbars=1");
    }

    window.setTimeout('window.location.reload(true)', 30000)

</script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" width="20%"><span class="field-label">Reports </span></td>
        <td align="left" colspan="3">
            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td align="left" width="20%"><span class="field-label">Type </span></td>
        <td align="left" width="30%">
            <asp:DropDownList ID="DropType" runat="server" AutoPostBack="True">
                <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                <asp:ListItem Text="SMS" Value="SMS"></asp:ListItem>
                <asp:ListItem Text="EMAIL" Value="EMAIL"></asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" width="20%"></td>
        <td align="left" width="30%"></td>
    </tr>
    <tr>
        <td align="left" colspan="4">
            <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                EmptyDataText="Information not available." Width="100%" PageSize="20">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Send ID
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("SEND_ID")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            BSU
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("BSU_SHORTNAME")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Module/Type
                                   
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%# Eval("MODULE_TYPE")%>/<%# Eval("MESSGE_TYPE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Group
                                   
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%# Eval("GROUP_DESC")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Message
                                   
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%# Eval("MESSAGE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Schedule/Send Date
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("SEND_DATE_TIME")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            End Date
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("END_DATE_TIME")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("TOTAL_DATA")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Success
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("SUCCESS")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Failed
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("FAILED")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Status
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("STATUS")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Sent By
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("NAME")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Action
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkAction" OnClientClick='<%# Eval("openWindowActions")%>' runat="server">Action</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Log
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkLog" OnClientClick='<%# Eval("openLog")%>' runat="server">Log</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle Wrap="False" />
                <HeaderStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>


<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>
