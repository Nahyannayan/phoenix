<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsBusListing.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsBusListing" %>

<%@ Register Src="UserControls/gpsBusListing.ascx" TagName="gpsBusListing" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Bus Tracking"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc1:gpsBusListing ID="GpsBusListing1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

