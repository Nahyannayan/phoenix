﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_gpsBarcodePickDropAlertReturnShow
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        BindBsu()
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim str_query = "GPS_GET_BASIC_DATA"
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        pParms(1) = New SqlClient.SqlParameter("@LOG_JOURNEY", "Return")
        pParms(2) = New SqlClient.SqlParameter("@OPTION", 7)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, pParms)
        GridData.DataSource = ds
        GridData.DataBind()

    End Sub
End Class
