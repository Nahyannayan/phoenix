<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsSpeedReportFilter.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_Report_Pages_gpsSpeedReportFilter" %>

<%@ Register Src="~/Transport/GPS_Tracking/Report/UserControls/gpsSpeedReportFilter.ascx" TagName="gpsSpeedReportFilter"
    TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Speed Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc1:gpsSpeedReportFilter ID="GpsSpeedReportFilter1" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>



