﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsReports.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsReports" %>
<%@ Register Src="../../Javascript/TreeNodeScript.ascx" TagName="TreeNodeScript"
    TagPrefix="uc1" %>
<uc1:TreeNodeScript ID="TreeNodeScript1" runat="server" />

<div>
    <table  cellpadding="5" cellspacing="0" width="100%">
        <%-- <tr>
            <td class="subheader_img">
                GPS Reports
            </td>
        </tr>--%>
        <tr>
            <td align="left">
                <table width="100%">
                      <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">
                            <span class="field-label">Report</span>
                        </td>

                        <td width="30%">
                            <asp:DropDownList ID="DropType" runat="server" AutoPostBack="True">
                                <asp:ListItem Text="Speed" Value="SPEED"></asp:ListItem>
                                <asp:ListItem Text="Panic" Value="PANIC"></asp:ListItem>
                                <asp:ListItem Text="Idle" Value="IDLE"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="20%">
                            <span class="field-label">BusinessUnit </span></td>

                        <td width="30%">
                            <asp:DropDownList ID="ddBsu1" runat="server" OnSelectedIndexChanged="ddBsu1_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr id="trignore2" runat="server">
                        <td width="20%">  <span class="field-label">Vehicle Type</span>
                        </td>                       
                        <td width="30%">
                            <asp:DropDownList ID="ddlVehicleType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged">
                                <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                <asp:ListItem Text="BIG BUS" Value="2"></asp:ListItem>
                                <asp:ListItem Text="MEDIUM BUS" Value="4"></asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">  <span class="field-label">Vehicles</span>
                        </td>
                     
                        <td width="30%">
                            
                                <asp:CheckBox ID="CheckAll" runat="server" AutoPostBack="True" Text="All" Checked="True" />
                            <div class="checkbox-list"> 
                                <asp:CheckBoxList ID="CheckVhe" runat="server">
                                </asp:CheckBoxList> </div>
                           
                        </td>
                    </tr>
                    <tr id="TR1" runat="server">
                        <td width="20%"> <span class="field-label">Speed</span>&lt;
                        </td>
                     
                        <td width="30%">
                            <asp:TextBox ID="txtspeed" runat="server" Text="80"  MaxLength="3"></asp:TextBox>
                            Km/Hr
                        </td>
                    </tr>
                    <tr id="TR2" runat="server">
                        <td><span class="field-label">IdleTime&lt;</span>
                        </td>
                       
                        <td width="30%">
                            <asp:TextBox ID="txtidle" runat="server" Text="10"  MaxLength="4"></asp:TextBox>
                            Mins
                        </td>
                    </tr>
                    <tr id="trignore" runat="server">
                        <td width="20%"><span class="field-label">Ignore vehicles exceeding limit 1 or 2 times in a day</span>
                        </td>
                        
                        <td width="30%">
                            <asp:CheckBox ID="chkIgnoreexceed" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td width="20%"> <span class="field-label">Date</span>
                        </td>
                        
                        <td width="30%">
                            <asp:TextBox ID="txtfromdate" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <%--  <tr>
                        <td >
                            ToDate
                        </td>
                        <td>
                            :
                        </td>
                        <td width="100%">
                            <asp:TextBox ID="txttodate" runat="server"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                       
                        <td width="100%" colspan="4" align="center">
                            <asp:Button ID="btnReport" runat="server" Text="Generate Report"  CssClass="button"
                                 />

                            <asp:Button ID="btnExcelExport" runat="server" CssClass="button" Text="Export to Excel" Visible="false" />

                        </td>
                    </tr>
                  
                </table>
            </td>
        </tr>
    </table>
    <ajaxToolkit:FilteredTextBoxExtender ID="F1" FilterType="Numbers" TargetControlID="txtspeed"
        runat="server">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="F2" FilterType="Numbers" TargetControlID="txtidle"
        runat="server">
    </ajaxToolkit:FilteredTextBoxExtender>
</div>
<ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtfromdate"
    PopupPosition="TopLeft" TargetControlID="txtfromdate">
</ajaxToolkit:CalendarExtender>
<%--<ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txttodate"
    PopupPosition="TopLeft" TargetControlID="txttodate">
</ajaxToolkit:CalendarExtender>--%>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfromdate"
    Display="None" ErrorMessage="Please Enter From Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttodate"
    Display="None" ErrorMessage="Please Enter To Date" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" />

