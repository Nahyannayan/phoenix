﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports InfosoftGlobal
Imports System.Web.Configuration
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports System.IO

Partial Class Transport_GPS_Tracking_UserControls_gpsReports
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ShowHide()
            BindBsu()
            'Dim hash As New Hashtable
            'If Session("sBsuid") = "900501" Then  '' STS LOGIN
            '    hash("BSU") = "All"
            'Else
            '    hash("BSU") = Session("sBsuid")
            'End If

            'hash("EXPAND") = False


        End If

    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu1.DataSource = dsBsu
            ddBsu1.DataTextField = "BSU_NAME"
            ddBsu1.DataValueField = "BSU_ID"
            ddBsu1.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "0"
            ddBsu1.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu1.DataSource = dsBsu
            ddBsu1.DataTextField = "BSU_NAME"
            ddBsu1.DataValueField = "BSU_ID"
            ddBsu1.DataBind()
        End If

        BindVehReg()


    End Sub

    Public Sub BindVehReg()

        'CheckVhe.Items.Clear()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString

        '' included by nahyan on 17jan2017 for 53925
        Dim vehicle_type As String = String.Empty

        If ddlVehicleType.SelectedValue <> "0" Then
            vehicle_type = ddlVehicleType.SelectedValue
        Else
            vehicle_type = "0"
        End If

        If ddBsu1.SelectedValue <> "0" Then
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, " select CONVERT(VARCHAR,VEH_REGNO) + ' -- ' +  CONVERT(VARCHAR,VEH_UNITID)  as DATA , VEH_UNITID from  OASIS_TRANSPORT.dbo.VW_VEH_DRIVER WHERE VEH_ALTO_BSU_ID='" & ddBsu1.SelectedValue & "' AND VEH_CAT_ID = IIF( '" & vehicle_type & "'=0,VEH_CAT_ID, '" & vehicle_type & "') ORDER BY VEH_REGNO , VEH_UNITID")
        Else
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, " select CONVERT(VARCHAR,VEH_REGNO) + ' -- ' +  CONVERT(VARCHAR,VEH_UNITID)  as DATA , VEH_UNITID from  OASIS_TRANSPORT.dbo.VW_VEH_DRIVER where  VEH_CAT_ID =  IIF( '" & vehicle_type & "'=0,VEH_CAT_ID, '" & vehicle_type & "')  ORDER BY VEH_REGNO , VEH_UNITID")
        End If


        If ds.Tables(0).Rows.Count > 0 Then
            CheckVhe.DataTextField = "DATA"
            CheckVhe.DataValueField = "VEH_UNITID"
            CheckVhe.DataSource = ds
            CheckVhe.DataBind()
        End If
        'Dim list As New ListItem
        'list.Text = "All"
        'list.Value = "-1"
        'CheckVhe.Items.Insert(0, list)
        If CheckAll.Checked Then
            For Each check As ListItem In CheckVhe.Items
                check.Selected = True
            Next
        End If


    End Sub

    Public Sub ShowHide()
        If DropType.SelectedValue = "SPEED" Then
            TR1.Visible = True
            TR2.Visible = False
            trignore.Visible = True
            trignore2.Visible = True
            btnExcelExport.Visible = True
        End If


        If DropType.SelectedValue = "PANIC" Then
            TR1.Visible = False
            trignore.Visible = False
            trignore2.Visible = False
            TR2.Visible = False
            btnExcelExport.Visible = False
        End If


        If DropType.SelectedValue = "IDLE" Then
            TR1.Visible = False
            TR2.Visible = True
            trignore.Visible = False
            trignore2.Visible = False
            btnExcelExport.Visible = False
        End If
    End Sub
    Protected Sub DropType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropType.SelectedIndexChanged

        ShowHide()

    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click


        Dim param As New Hashtable

        Dim search = ""

        If DropType.SelectedValue = "SPEED" Then
            If txtspeed.Text.Trim() = "" Then
                txtspeed.Text = "80"
            End If
            param.Add("@SPEED", txtspeed.Text.Trim())
            param.Add("@REPORT_TYPE", "REPORT")
            search = "Speed greater than : " & txtspeed.Text.Trim() & " Km/Hr "
        End If

        If DropType.SelectedValue = "IDLE" Then
            If txtidle.Text.Trim() = "" Then
                txtidle.Text = "10"
            End If
            param.Add("@IDLE", txtidle.Text.Trim())
            search = "Idle time greater than : " & txtidle.Text.Trim() & " Mins "

        End If

        If DropType.SelectedValue = "PANIC" Then
            search = "Panic Report "
        End If

        search &= " - Date : " & txtfromdate.Text.Trim() ''& " To Date : " & txttodate.Text.Trim() & ""

        ''Default Parameters 


        param.Add("@HEADER", "Reports")
        param.Add("@SEARCH", search)

        param.Add("@UNIT_ID", SelectedNode)


        If txtfromdate.Text.Trim() = "" Then
            param.Add("@FROM_DATE", DBNull.Value)
            param.Add("@TO_DATE", DBNull.Value)
        Else
            param.Add("@FROM_DATE", txtfromdate.Text.Trim())
            param.Add("@TO_DATE", txtfromdate.Text.Trim())
        End If
        param.Add("@IMG_BSU_ID", Session("sBsuid")) '
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name")) '

        If DropType.SelectedValue = "SPEED" Then
            If chkIgnoreexceed.Checked Then
                param.Add("@ignorelimit", 3)
            Else
                param.Add("@ignorelimit", 0)
            End If
        End If





        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_GPS"
            .crInstanceName = "172.25.29.51"
            .crUser = "GPS"
            .crPassword = "mango123"
            .reportParameters = param

            If DropType.SelectedValue = "SPEED" Then
                .reportPath = Server.MapPath("~/Transport/GPS_Tracking/Report/CrystalReports/CR_Speed.rpt")
            End If

            If DropType.SelectedValue = "PANIC" Then
                .reportPath = Server.MapPath("~/Transport/GPS_Tracking/Report/CrystalReports/CR_Panic.rpt")
            End If

            If DropType.SelectedValue = "IDLE" Then
                .reportPath = Server.MapPath("~/Transport/GPS_Tracking/Report/CrystalReports/CR_Idle.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()


    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Public Function SelectedNode() As String

        Dim Value As String = ""

        For Each check As ListItem In CheckVhe.Items

            If check.Selected Then
                Value &= check.Value & "|"
            End If
        Next

        Return Value

    End Function

    Protected Sub ddBsu1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindVehReg()
    End Sub

    Protected Sub ddlVehicleType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindVehReg()
    End Sub

    Protected Sub CheckAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckAll.CheckedChanged
        BindVehReg()
    End Sub


    Protected Sub btnExcelExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcelExport.Click
        CallReport()
    End Sub

    Sub CallReport()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer



        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SPEED", txtspeed.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@UNIT_ID", SelectedNode)

        If txtfromdate.Text.Trim() = "" Then

            pParms(2) = New SqlClient.SqlParameter("@FROM_DATE", DBNull.Value)
            pParms(3) = New SqlClient.SqlParameter("@TO_DATE", DBNull.Value)
        Else
            pParms(2) = New SqlClient.SqlParameter("@FROM_DATE", Format(Date.Parse(txtfromdate.Text.Trim), "dd/MMM/yyyy"))
            pParms(3) = New SqlClient.SqlParameter("@TO_DATE", Format(Date.Parse(txtfromdate.Text.Trim), "dd/MMM/yyyy"))
        End If

        pParms(4) = New SqlClient.SqlParameter("@REPORT_TYPE", "EXPORT")
        If chkIgnoreexceed.Checked Then
            pParms(5) = New SqlClient.SqlParameter("@ignorelimit", 3)
        Else
            pParms(5) = New SqlClient.SqlParameter("@ignorelimit", 0)
        End If

        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_SPEED_REPORT", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

            '  ws.HeadersFooters.AlignWithMargins = True

            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If

    End Sub


End Class
