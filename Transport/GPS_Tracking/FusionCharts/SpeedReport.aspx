<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SpeedReport.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_FusionCharts_SpeedReport" %>

<%@ Register Src="UserControls/SpeedReport.ascx" TagName="SpeedReport" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Speed Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <uc1:SpeedReport ID="SpeedReport1" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>





