Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports InfoSoftGlobal
Imports System.Web.Configuration

Partial Class Transport_GPS_Tracking_FusionCharts_UserControls_SpeedReport
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBsu()
            BindPage()
            FCLiteral.Text = CreateCharts()
        End If

    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindVehReg()


    End Sub

    Public Sub BindVehReg()
        ddVehReg.Items.Clear()
        Dim ds As DataSet

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "select BSU_SHORTNAME +'-- ' + VEH_REGNO DATA,VEH_UNITID from  VW_VEH_DRIVER  WHERE VEH_ALTO_BSU_ID='" & ddBsu.SelectedValue & "'" & " order by VEH_REGNO")

        If ds.Tables(0).Rows.Count > 0 Then
            ddVehReg.DataTextField = "DATA"
            ddVehReg.DataValueField = "VEH_UNITID"
            ddVehReg.DataSource = ds
            ddVehReg.DataBind()
        Else
            Dim list As New ListItem
            list.Text = "Vehicle"
            list.Value = "-1"
            ddVehReg.Items.Insert(0, list)
        End If

        BindPage()
        ddPage_SelectedIndexChanged(ddPage, Nothing)

    End Sub

    Public Function CreateCharts() As String
        Dim strXML As String
        strXML = ""
        strXML = strXML & "<graph caption='Speed Report' xAxisName='Time' yAxisName='Speed (Km/Hr)' decimalPrecision='0' rotateNames='1' formatNumberScale='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC'>"
        strXML = strXML & xml().ToString()
        strXML = strXML & "</graph>"


        Return FusionCharts.RenderChartHTML("swf/" & ddReportList.SelectedValue, "", strXML, "myNext", "600", "400", False)

    End Function

    Public Sub BindPage()
        ddPage.Items.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@DATE", txtdate.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@UNIT_ID", ddVehReg.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@OPTION", 3)

        Dim page = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

        Dim i = 0
        For i = 1 To page
            Dim list As New ListItem
            list.Text = i
            list.Value = i
            ddPage.Items.Add(list)
        Next




    End Sub

    Public Function xml() As StringBuilder

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim strXML As New StringBuilder

            Dim pParms(4) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@DATE", txtdate.Text.Trim())
            pParms(1) = New SqlClient.SqlParameter("@UNIT_ID", ddVehReg.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@ROW_ID", ddPage.SelectedIndex)
            pParms(3) = New SqlClient.SqlParameter("@OPTION", 4)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)



            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                strXML.Append("<set name='" & ds.Tables(0).Rows(i).Item("PositionLogDateTime").ToString() & "' value='" & ds.Tables(0).Rows(i).Item("speed").ToString() & "' color='A186BE' />")
            Next

            Return strXML
        Catch ex As Exception

        End Try



    End Function


    Protected Sub ddPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddPage.SelectedIndexChanged
        FCLiteral.Text = CreateCharts()
    End Sub

    Protected Sub imgnext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgnext.Click

        If ddPage.SelectedValue < ddPage.Items.Count Then
            ddPage.SelectedIndex = ddPage.SelectedIndex + 1
            ddPage_SelectedIndexChanged(ddPage, Nothing)
        End If


    End Sub


    Protected Sub imgPrev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrev.Click
        If ddPage.SelectedValue <= ddPage.Items.Count Then
            ddPage.SelectedIndex = ddPage.SelectedIndex - 1
            ddPage_SelectedIndexChanged(ddPage, Nothing)
        End If

    End Sub

    Protected Sub ddReportList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddReportList.SelectedIndexChanged
        BindPage()
        ddPage_SelectedIndexChanged(ddPage, Nothing)
    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindVehReg()
    End Sub

    Protected Sub ddVehReg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddVehReg.SelectedIndexChanged
        BindPage()
        ddPage_SelectedIndexChanged(ddPage, Nothing)
    End Sub

    Protected Sub txtdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdate.TextChanged
        BindPage()
        ddPage_SelectedIndexChanged(ddPage, Nothing)
    End Sub

End Class
