<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SpeedReport.ascx.vb" Inherits="Transport_GPS_Tracking_FusionCharts_UserControls_SpeedReport" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg">Speed Report</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Vehicle</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddVehReg" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdate" runat="server" AutoPostBack="True"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Report Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddReportList" runat="server" AutoPostBack="True">
                                <asp:ListItem Text="Line" Value="FCF_Line.swf" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Area2D" Value="FCF_Area2D.swf"></asp:ListItem>
                                <asp:ListItem Text="Column2D" Value="FCF_Column2D.swf"></asp:ListItem>
                                <asp:ListItem Text="Column3D" Value="FCF_Column3D.swf"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">

                <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>
                <br />
                <asp:ImageButton ID="imgPrev" runat="server" ImageUrl="~/Images/button_previous.jpg"></asp:ImageButton>&nbsp;
        <asp:DropDownList ID="ddPage" runat="server" AutoPostBack="True">
        </asp:DropDownList>&nbsp;
                <asp:ImageButton ID="imgnext" runat="server" ImageUrl="~/Images/button_next.jpg"></asp:ImageButton>

            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" PopupPosition="TopLeft" Format="dd/MMM/yyyy" PopupButtonID="txtdate" TargetControlID="txtdate"></ajaxToolkit:CalendarExtender>
</div>
