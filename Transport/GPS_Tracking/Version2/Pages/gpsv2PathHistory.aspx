﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsv2PathHistory.aspx.vb"
    Inherits="Transport_GPS_Tracking_Version2_Pages_gpsv2PathHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <%--<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />--%>
    <meta http-equiv="X-UA-Compatible" content="IE=11" /> 
    <title>Path History</title>
    	
<%--  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAftxP7-851yNhYPbe3r0_3Xqdyq7l47fI&callback=initMap">
    </script>--%>
   <%--  <script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyAftxP7-851yNhYPbe3r0_3Xqdyq7l47fI&callback=initMap">
    </script>
     --%>
 <%--   <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAeuBSnop-n2H2ynhKh8hcexTGfOK6oiJyuK2ZbZJplrYQarsorhS5wa-ITLWd56UFHGhxW3tuA5veDQ"
        type="text/javascript"></script>--%>
   <%-- <script src="../Javascript/PathHistory.js" type="text/javascript"></script>--%>


    <script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyA1iYoEmhopACwr8vAOLOO3KSOHvMERD-E&callback=initMap">
    </script>

    <style type="text/css">
        #map
        {
            height: 900px;
            width: 99%;
            border: 1px solid gray;
            margin-top: 8px;
            margin-left: 8px;
            overflow: hidden;
        }
        
        
        table.BlueTable
        {
            border-right: #1b80b6 1pt solid;
            padding: 4px;
            border-collapse: collapse;
            border-bottom: #1b80b6 1pt solid;
            font-size: 10px;
            -moz-border-radius: 0;
            width: 100%;
        }
        table.BlueTable td
        {
            border-width: 1px;
            padding: 4px;
            border-left: #1b80b6 1pt solid;
            border-top: #1b80b6 1pt solid;
            border-bottom: #1b80b6 1pt solid;
            -moz-border-radius: 0;
        }
        
        table.BlueTable select
        {
            font-size: 10px;
        }
        table.BlueTable input
        {
            font-size: 10px;
        }
        table.BlueTable table td
        {
            border-width: 0pt;
            padding: 1px;
        }
        
    </style>
    <script type="text/javascript">

        function calendarShown(sender, args) { sender._popupBehavior._element.style.zIndex = 10005; }

    </script>
</head>
<body onload="initMap()">
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:Image ID="Image1" ImageUrl="https://school.gemsoasis.com/images/schools/sts/sts_headerbaord_bg.jpg"
        Height="120px" Width="100%" runat="server" />
    <table class="BlueTable">
        <tr>
            <td valign="top">
                
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    Business&nbsp;Unit
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddBsu" runat="server" EnableTheming="false" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Vehicle&nbsp;Reg&nbsp;No
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddvehRegNo" runat="server" EnableTheming="false" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Trips
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddtrips" EnableTheming="false" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Date
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                <div>

                                    <asp:TextBox ID="txtDate" runat="server" Width="70px" EnableTheming="false" ValidationGroup="T1"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" OnClientShown="calendarShown" PopupButtonID="txtDate"
                                        PopupPosition="BottomLeft" TargetControlID="txtDate">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                                </td>
                                <td>
                                    Options
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckShowLines" runat="server" EnableTheming="false" Text="Show&nbsp;Lines" />
                                    <asp:CheckBox ID="CheckGeoFence" runat="server" EnableTheming="false" Text="Geo&nbsp;Fence"
                                       />
                                </td>
                                <td valign="middle">
                                    <asp:ImageButton ID="ImageSearch" ImageUrl="~/Images/GPS_Images/Search.png" OnClientClick="javascript:Validate();initMap();return false;"
                                        ToolTip="Search" Width="30" Height="27" runat="server" />
                                </td>
                                <td valign="top">
                                    &nbsp;&nbsp;<asp:ImageButton ID="ImageStop" ImageUrl="~/Images/GPS_Images/Stop.png"
                                        OnClientClick="haltAnim();return false;" ToolTip="Stop Animation" Width="20"
                                        Height="20" runat="server" />
                                    &nbsp;&nbsp;<asp:ImageButton ID="ImageContinue" ImageUrl="~/Images/GPS_Images/Play.png"
                                        OnClientClick="carryOn();return false;" ToolTip="Continue Animation" Width="20"
                                        Height="20" runat="server" />
                                        <asp:ImageButton ID="ImageAssignGeofence" ImageUrl="~/Images/GPS_Images/actions.png"
                                        OnClientClick="AssignGeofencing();return false;" ToolTip="Assign Geofence" Width="20"
                                        Height="20" runat="server" />
                                    <%--&nbsp;&nbsp;<asp:ImageButton ID="ImageRefresh" ImageUrl="~/Images/GPS_Images/Play_Again.png" OnClientClick="playAgain();return false;" ToolTip="Play Again" Width="20" Height="20" runat="server" />--%>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div id="map">
                </div>
                <br />
                <div id="Info" style=" font-size:medium "></div>
            </td>
        </tr>
    </table>
    <center>
        <b> <asp:Label ID="lblpower" runat="server" Text="Powered by GEMS IT"></asp:Label></b></center>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddBsu"
        Display="None" ErrorMessage="Please Select Business Unit" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddvehRegNo"
        Display="None" ErrorMessage="Please Select Vehicle Reg No." InitialValue="-1"
        SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDate"
        Display="None" ErrorMessage="Please Enter Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
    </form>
</body>
</html>
<script type="text/javascript">
    //if (window.ActiveXObject) {
    //    alert('hi');
    //    XMLHTTPRequestObject = new ActiveXObject('Microsoft.XMLHTTP');
    //   // XMLHTTPRequestObject = new XMLHttpRequest();
    //}
    if (window.XMLHttpRequest) {
        // code for modern browsers
        XMLHTTPRequestObject = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        XMLHTTPRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // Global variables 

    var map, route;
    var points = [];
    var gmarkers = [];
    var count = 0;
    var stopClick = false;
    var points1 = [];

    function addClickevent(marker) { // Add a click listener to the markers

        google.maps.event.addListener(marker, "click", function () {
            var infowindow = new google.maps.InfoWindow();
            infowindow.setContent(marker.content);
            infowindow.open(map, this);
            // marker.openInfoWindowHtml(marker.content);
            /* Change count to continue from the last manually clicked marker
            *  Better syntax since Javascript 1.6 - Unfortunately not implemented in IE.
            *  count = gmarkers.indexOf(marker);
            */
            count = marker.nr;
            stopClick = true;
        });
        return marker;
    }


    function Validate() {
        if (window.document.getElementById("ddBsu").value == -1) {
            alert('Please Select Business Unit.'); return false;
        }
        if (window.document.getElementById("ddvehRegNo").value == -1) {
            alert('Please Select Vehicle Reg No.'); return false;
        }
        if (window.document.getElementById("txtDate").value == '') {
            alert('Please Enter Date'); return false;
        }

    }
    function initMap() {

        if (route != null) {

            window.clearTimeout(route)
        }

        //  if (GBrowserIsCompatible()) {
        //Load Map
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(25.104565, 55.17231),
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scaleControl: false
        });
        //Center Map
        // map.setCenter(new GLatLng(25.104565, 55.17231), 14);
        map.setCenter(new google.maps.LatLng(25.104565, 55.17231));
        //Load Controls
        //var customUI = map.getDefaultUI();
        //customUI.controls.scalecontrol = false;
        //map.setUI(customUI);

        //Reverse Geocode
        google.maps.event.addListener(map, "click", getAddress);
        geocoder = new google.maps.Geocoder();

        //Show Geoffence
        if (document.getElementById("CheckGeoFence").checked) {

            try {
                SetGeoFence(map);
            }
            catch (ErrorMessage) {
                ErrorMessage.message;
            }
        }


        //Search Strings
        var SearchString = '';
        SearchString = window.document.getElementById("ddBsu").value;
        SearchString += '$' + window.document.getElementById("ddvehRegNo").value;
        SearchString += '$' + window.document.getElementById("ddtrips").value;
        SearchString += '$' + window.document.getElementById("txtDate").value;

        // alert(SearchString);

        var datasource = '../../WebServices/gpsGenerateXml.asmx/V2_PathHistory?SearchString=' + SearchString; // + window.document.getElementById("HiddenUnitId").value

        XMLHTTPRequestObject.open('GET', datasource, true);
        var xmlObj = ''

        XMLHTTPRequestObject.onreadystatechange = function () {

            if (XMLHTTPRequestObject.readyState == 4 && XMLHTTPRequestObject.status == 200) {

                var xmlDoc = XMLHTTPRequestObject.responseXML;

                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");



                xmlDocc.load(xmlDoc);
                xmlObj = xmlDocc.documentElement;
                //  alert(xmlObj);

                points = [];
                points1 = [];
                for (var i = 0; i < xmlObj.childNodes.length; i++) {

                    // Light blue marker icons
                    //var icon = new google.maps.();
                    //icon.image = xmlObj.childNodes.item(i).childNodes.item(3).text;
                    //addIcon(icon);

                    points[i] = {
                        position: new google.maps.LatLng(parseFloat(xmlObj.childNodes.item(i).childNodes.item(1).text), parseFloat(xmlObj.childNodes.item(i).childNodes.item(2).text)),
                        map: map,

                        icon: xmlObj.childNodes.item(i).childNodes.item(3).text

                    };

                    //   points[i] = new GLatLng(parseFloat(xmlObj.childNodes.item(i).childNodes.item(1).text), parseFloat(xmlObj.childNodes.item(i).childNodes.item(2).text));
                    points1[i] = new google.maps.LatLng(parseFloat(xmlObj.childNodes.item(i).childNodes.item(1).text), parseFloat(xmlObj.childNodes.item(i).childNodes.item(2).text));

                    gmarkers[i] = new google.maps.Marker(points[i]);

                    // Store data attributes as property of gmarkers
                    var html = xmlObj.childNodes.item(i).childNodes.item(4).text;
                    gmarkers[i].content = html;
                    gmarkers[i].nr = i;
                    addClickevent(gmarkers[i]);
                    gmarkers[i].setMap(map);
                }


                if (xmlObj.childNodes.length > 0) {

                    // Draw polylines between marker points
                    if (document.getElementById("CheckShowLines").checked) {
                        //var poly = new GPolyline(points, "#2554C7", 5, .8);
                        //map.addOverlay(poly);


                        //var polyLineOptions = {
                        //    path: gmarkers,
                        //    strokeColor: '#2554C7',
                        //    strokeOpacity: 0.8,
                        //    strokeWeight: 5,
                        //    map:map
                        //};



                        //directionsDisplay.setOptions(new google.maps.Polyline(polyLineOptions));
                        //polyline.setMap(map);

                        var polyline = new google.maps.Polyline({
                            path: points1,
                            strokeColor: '#2554C7',
                            strokeOpacity: 0.8,
                            strokeWeight: 5,
                            geodesic: true
                        });

                        polyline.setMap(map);




                        //directionsService.route(request, function (response, status) {
                        //    alert('hi');

                        //    if (status == google.maps.DirectionsStatus.OK) {

                        //        var polyLineOptions = {
                        //            path: points,
                        //            strokeColor: '#2554C7',
                        //            strokeOpacity: 0.8,
                        //            strokeWeight: 5
                        //        };
                        //        var options = {};
                        //        options.directions = response;
                        //        options.map = map;
                        //        options.polylineOptions = polyLineOptions;
                        //        //options.suppressMarkers = true;
                        //        directionsDisplay.setOptions(options);// = new google.maps.DirectionsRenderer(options);
                        //        polyLine.setMap(map);

                        //       //directionsDisplay.setDirections(response);
                        //    }
                        //});
                    }
                    var infowindow = new google.maps.InfoWindow();
                    infowindow.setContent(gmarkers[0].content);
                    infowindow.open(map, this);

                    // Open infowindow of first marker
                    //gmarkers[0].openInfoWindowHtml(gmarkers[0].content);
                }
                else {
                    clearTimeout(route); count = 0; route = null;
                    if (window.document.getElementById("ddvehRegNo").value > -1) {
                        alert('Information not available.Please redefine your search.')
                    }


                }


            }

        }

        XMLHTTPRequestObject.send(null);

        route = setTimeout("anim()", 3600);

        // }




    }


    function showAddress(response) {
        if (!response || response.Status.code != 200) {
            alert("Status Code:" + response.Status.code);
        }
        else {
            place = response.Placemark[0];
            point = new google.maps.LatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
            var val =
            '&nbsp;<b>Address : </b>' + place.address;

            var obj = document.getElementById('Info');
            obj.innerHTML = val
        }
    }
    function addIcon(icon) { // Add icon properties

        //icon.shadow = "http://www.google.com/mapfiles/shadow50.png";
        icon.iconSize = new GSize(32, 32);
        //icon.shadowSize = new GSize(37, 34);
        icon.iconAnchor = new GPoint(15, 34);
        icon.infoWindowAnchor = new GPoint(19, 2);
        //icon.infoShadowAnchor = new GPoint(18, 25);
        //map.getPane(G_MAP_FLOAT_SHADOW_PANE).style.visibility = "hidden"; 

    }

    function getAddress(overlay, latlng) {
        if (latlng != null) {
            address = latlng;
            geocoder.getLocations(latlng, showAddress);
        }
    }
    function SetGeoFence(map) {

        XMLHTTPRequestObject = new ActiveXObject('Microsoft.XMLHTTP');
        var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        var datasource = '../../WebServices/gpsGenerateXml.asmx/GetGeoFencingPoints?UnitId=' + window.document.getElementById("ddvehRegNo").value
        // alert(datasource);
        var xmlDoc = '';
        XMLHTTPRequestObject.open('GET', datasource, false);
        XMLHTTPRequestObject.onreadystatechange = function () {

            if (XMLHTTPRequestObject.readystate == 4 && XMLHTTPRequestObject.status == 200) {
                xmlDoc = XMLHTTPRequestObject.responseXML;
                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDocc.load(xmlDoc);
                xmlObj = xmlDocc.documentElement
            }
            else {
            }
        }
        XMLHTTPRequestObject.send(null);

        if (xmlObj.childNodes.length > 0) {

            var markers = xmlDoc.documentElement.getElementsByTagName("marker");
            //alert(markers.length);
            var pointsg = [];
            for (var k = 0; k < markers.length - 1; k++) {

                pointsg[k] = new google.maps.LatLng(parseFloat(markers[k].getAttribute("lat")), parseFloat(markers[k].getAttribute("lng")));

            }

            //var polygon = new GPolygon(pointsg, '#FF0000', 0.1, 0.4, '#FF0000', 0.4);
            ////alert(map);
            //map.addOverlay(polygon);

            directionsService.route(request, function (response, status) {
                alert("op");
                if (status == google.maps.DirectionsStatus.OK) {
                    alert("op1");
                    var polyLineOptions = {
                        strokeColor: '#FF0000'
                    };
                    var options = {};
                    options.directions = response;
                    options.map = map;
                    options.polylineOptions = polyLineOptions;
                    //options.suppressMarkers = true;
                    directionsDisplay.setOptions(options);// = new google.maps.DirectionsRenderer(options);
                    polyLine.setMap(map);
                    //directionsDisplay.setDirections(response);
                }
            });

        }


    }


    function anim() {

        count++;
        if (count < points.length) {
            // Use counter as array index
            map.panTo(points[count]);
            var infowindow = new google.maps.InfoWindow();
            infowindow.setContent(gmarkers[count].content);
            infowindow.open(map, this);
            // gmarkers[count].openInfoWindowHtml(gmarkers[count].content);
            var delay = 3400;
            if ((count + 1) != points.length)
                var dist = points[count].distanceFrom(points[count + 1]);

            // Adjust delay
            if (dist < 10000) {
                delay = 2000;
            }
            if (dist > 80000) {
                delay = 4200;
            }
            route = setTimeout("anim()", delay);
        }
        else {
            clearTimeout(route);
            count = 0;
            route = null;
        }
    }
    </script>
