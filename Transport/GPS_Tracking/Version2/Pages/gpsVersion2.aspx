﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsVersion2.aspx.vb" Inherits="Transport_GPS_Tracking_Version2_Pages_gpsVersion2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GEMS OASIS Fleet</title>
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
   <div align="left">
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="HT1"  runat="server">
                <ContentTemplate>
                <iframe id="FEx1" height="800" scrolling="auto"  marginwidth="0px"   frameborder="0"  width="800"></iframe>         
                </ContentTemplate>
                <HeaderTemplate>
                Live Tracking 
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>

               <ajaxToolkit:TabPanel ID="HT4" runat="server">
                <ContentTemplate>
                    <iframe id="FEx2" height="800px" scrolling="auto"  marginwidth="0px" src="gpsv2PathHistory.aspx" frameborder="0"  width="700px"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                 Track Path 
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
        </ajaxToolkit:TabContainer>
        
        <asp:HiddenField ID="Hiddenempid" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        
    </div>
    </form>
</body>
</html>
