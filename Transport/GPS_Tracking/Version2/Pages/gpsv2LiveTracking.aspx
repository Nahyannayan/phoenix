﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsv2LiveTracking.aspx.vb"
    Inherits="Transport_GPS_Tracking_Version2_Pages_gpsv2LiveTracking" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <meta http-equiv="X-UA-Compatible"  content="IE=11" />
    <title>Live Tracking</title>
    <%--<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGlQYfrpDbqE6Xk5sKDv-S38jCLZQqgao&callback=initMap">
    </script>--%>

   <script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyA1iYoEmhopACwr8vAOLOO3KSOHvMERD-E&callback=initMap" >
    </script>
    <%--<script 
    src="http://maps.google.com/maps?file=api&amp;key=AIzaSyBcJMSA2h3Hc6Q2i1V0wGZBye_eKZmMlOI" type="text/javascript">
    </script>--%>
    <script src="../Javascript/elabel.js" type="text/javascript"></script>

   
   <%-- <script src="../Javascript/LiveTracking.js" type="text/javascript"></script>--%>
    <style type="text/css">
        #map
        {
            height: 900px;
            width: 99%;
            border: 1px solid gray;
            margin-top: 8px;
            margin-left: 8px;
            overflow: hidden;
        }
        
        
        table.BlueTable
        {
            border-right: #1b80b6 1pt solid;
            padding: 4px;
            border-collapse: collapse;
            border-bottom: #1b80b6 1pt solid;
            font-size: 10px;
            -moz-border-radius: 0;
            width: 100%;
        }
        table.BlueTable td
        {
            border-width: 1px;
            padding: 4px;
            border-left: #1b80b6 1pt solid;
            border-top: #1b80b6 1pt solid;
            border-bottom: #1b80b6 1pt solid;
            -moz-border-radius: 0;
        }
        
        table.BlueTable select
        {
            font-size: 10px;
        }
        table.BlueTable input
        {
            font-size: 10px;
        }
        table.BlueTable table td
        {
            border-width: 0pt;
            padding: 1px;
        }
        .style1
        {
            background-color: #ffffff;
            font-weight: bold;
            border: 2px #006699 solid;
        }
      .labels {
  color: white;
  background-color: red;
  font-family: "Lucida Grande", "Arial", sans-serif;
  font-size: 10px;
  text-align: center;
  width: 30px;
  white-space: nowrap;
}
      
    </style>
  
</head>
<body onload="initMap()"  >
    <form id="form1" runat="server">
    <ajaxtoolkit:toolkitscriptmanager ID="ScriptManager1" runat="server">
    </ajaxtoolkit:toolkitscriptmanager>
    <asp:Image ID="Image1" ImageUrl="https://school.gemsoasis.com/images/schools/sts/sts_headerbaord_bg.jpg"
        Height="120px" Width="100%" runat="server" />
    <table class="BlueTable">
        <tr>
            <td valign="top">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    Business&nbsp;Unit
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddBsu" runat="server" EnableTheming="false" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="lblselection" runat="server" Text="Label"></asp:Label>  
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <img id="img1" src="../../../../Images/GPS_Images/expand.png" alt="Expand/Collapse"
                                        onclick="javascript:CloseExpand(this);return false;" style="width: 16px; height: 16px" />
                                    <div id="Tab1" style="visibility: hidden; background-color: white; position: absolute;
                                        float: right; border: 3; border-color: Black;z-index: +20">
                                        <asp:CheckBox ID="CkAll" Checked="true" Text="All" runat="server" AutoPostBack="True" />
                                        <br />
                                        <asp:CheckBoxList ID="CheckvehRegNo" BorderColor="Black" BorderWidth="1"  runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </td>
                                <td valign="middle">
                                    <asp:ImageButton ID="ImageSearch" ImageUrl="~/Images/GPS_Images/Search.png" OnClientClick="javascript:Validate();checkBoxListOnClick('CheckvehRegNo');initMap();return false;"
                                        ToolTip="Search" Width="30" Height="27" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div id="map" >
                </div>
                <br />
                <div id="Info" style=" font-size:medium "></div>
            </td>
        </tr>
    </table>
    <center>
        <b>
            <asp:Label ID="lblpower" runat="server" Text="Powered by GEMS IT"></asp:Label></b></center>
    <asp:HiddenField ID="HiddenUnitId" runat="server" />
    </form>
</body>
</html>
<script type="text/javascript">

    function CloseExpand(imgobj) {
        var imgpath = imgobj.src;
        if (imgpath.indexOf("expand") > -1) {
            imgobj.src = "../../../../Images/GPS_Images/Close.png"
            document.getElementById("Tab1").style.visibility = 'visible'
        }
        else {
            imgobj.src = "../../../../Images/GPS_Images/expand.png"
            document.getElementById("Tab1").style.visibility = 'hidden'
        }


    }
    //if (window.ActiveXObject) {
    //    XMLHTTPRequestObject = new ActiveXObject('Microsoft.XMLHTTP');
    //}

    if (window.XMLHttpRequest) {
        // code for modern browsers
        XMLHTTPRequestObject = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        XMLHTTPRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // Global variables 
    var map, route, geocoder, marker;
    var points = [];
    var gmarkers = [];
    var count = 0;
    var stopClick = false;
    var Fentry = 0;
    var points1 = [];

    function initMap() {

        if (route != null) {

            window.clearTimeout(route)
        }


        //if (GBrowserIsCompatible()) {
        if (Fentry == '0') {

            try {

                map = new google.maps.Map(document.getElementById('map'), {
                    center: new google.maps.LatLng(25.104565, 55.17231),
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scaleControl: false
                });

                Fentry = 1;
                try {
                    google.maps.event.addListener(map, 'click', getAddress);

                }
                catch (errMsg) {
                    alert(errMsg.message);
                }

                //geocoder = new google.maps.Geocoder();
                //alert("3");
                //map.setCenter(new google.maps.LatLng(25.104565, 55.17231), 12);
                //alert("4");
                ////Reverse Geocode
                //google.maps.event.addListener(map, 'click', getAddress);
                //alert("5");
                //geocoder = new google.maps.Geocoder();
                //alert("6");
                // marker = new google.maps.Marker({
                //    position: map,
                //    map: map,
                //    title: 'Hello World!'
                //});
            }
            catch (errMsg) {


                errMsg.message;
            }
        }//Fentry=0
        else {

            var center = map.getCenter();
            try {
                map.setCenter(new google.maps.LatLng(center.lat(), center.lng()));
            }
            catch (errorMessage) {

                alert(errorMessage.message);
            }


        }

        //Search Strings
        var SearchString = '';
        SearchString = window.document.getElementById("ddBsu").value;
        SearchString += '$' + window.document.getElementById("HiddenUnitId").value;

        var datasource = '../../WebServices/gpsGenerateXml.asmx/V2_LiveTracking?SearchString=' + SearchString;
        // alert(datasource);
        XMLHTTPRequestObject.open('GET', datasource, true);
        var xmlObj = ''
        XMLHTTPRequestObject.onreadystatechange = function () {

            if (XMLHTTPRequestObject.readystate == 4 && XMLHTTPRequestObject.status == 200) {

                var xmlDoc = XMLHTTPRequestObject.responseXML;
                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDocc.load(xmlDoc);
                xmlObj = xmlDocc.documentElement

                points = [];
                points1 = [];
                try {
                    if (gmarkers.length > 0) {

                        for (var i = 0; i < gmarkers.length; i++) {
                            //  gmarkers[i] = new google.maps.Marker(null);

                            gmarkers[i].setMap(null);

                        }
                    }
                }
                catch (ErrorMessage) {
                    alert(ErrorMessage.message);
                }

                for (var i = 0; i < xmlObj.childNodes.length; i++) {


                    try {

                        points[i] = {
                            position: new google.maps.LatLng(parseFloat(xmlObj.childNodes.item(i).childNodes.item(1).text), parseFloat(xmlObj.childNodes.item(i).childNodes.item(2).text)),
                            map: map,
                            label: { text: xmlObj.childNodes.item(i).childNodes.item(9).text, fontSize: "9px" },

                            icon: xmlObj.childNodes.item(i).childNodes.item(3).text

                        };
                        points1[i] = new google.maps.LatLng(parseFloat(xmlObj.childNodes.item(i).childNodes.item(1).text), parseFloat(xmlObj.childNodes.item(i).childNodes.item(2).text));

                    }
                    catch (ErrorMessage) {
                        alert(ErrorMessage.message);
                    }
                    gmarkers[i] = new google.maps.Marker(points[i]);


                    // Store data attributes as property of gmarkers
                    var html = xmlObj.childNodes.item(i).childNodes.item(4).text;

                    //alert(html);
                    gmarkers[i].content = html;
                    gmarkers[i].nr = i;

                    try {

                        addClickevent(gmarkers[i]);

                    }
                    catch (ErrorMessage) {
                        alert(ErrorMessage.message);
                    }
                    // map.addOverlay(gmarkers[i]);
                    try {

                        gmarkers[i].setMap(map);
                    }
                    catch (ErrorMessage) {
                        alert(ErrorMessage.message);
                    }

                    //


                    var stuff, label;
                    stuff = '<div style="width:55px;">' + xmlObj.childNodes.item(i).childNodes.item(5).text + '</div>';
                    //label = new ELabel(points1[i], stuff, null, new google.maps.Size(20, 10), 100, true);

                    //  map.removeOverlay(label)
                    //label.pixelOffset = new google.maps.Size(20, 10);

                    //gmarkers[i].setMap(label);

                    //var infowindow = new google.maps.InfoWindow({
                    //    content: stuff,
                    //    pixelOffset: new google.maps.Size(20, 10),
                    //    zIndex: -1000,
                    //    maxWidth : 60

                    //});
                    //infowindow.open(map, gmarkers[i]);


                    // gmarkers[i].setMap(label);
                    //alert(label);
                    //  label.pixelOffset = new google.maps.Size(20, 10);
                    //alert(points[i]);
                    //try
                    //{
                    //    label = new ELabel(points[i], stuff, null, null, 100);
                    //    label.pixelOffset = new google.maps.Size(20, 10);
                    //}
                    //catch (ErrorMessage) {
                    //    alert(ErrorMessage.message);
                    //}


                    //  map.removeOverlay(label);

                    // 

                    // label.pixelOffset = new google.maps.Size(20, 10);

                    // label.pixelOffset = s;

                    //try
                    //{
                    //    gmarkers[i].setMap(label);
                    //}
                    //catch (ErrorMessage) {
                    //    alert(ErrorMessage.message);
                    //}
                    // alert("ds130");

                }

                if (xmlObj.childNodes.length == 0) {
                    alert('Information not available.Please redefine your search.')
                }


            }

        }

        XMLHTTPRequestObject.send(null);

        route = setTimeout("initMap()", 20000);
        return false;
        /// } //gbrowseriscompatib
    }

    function addIcon(icon) { // Add icon properties

        //icon.shadow = "http://www.google.com/mapfiles/shadow50.png";
        icon.iconSize = new GSize(32, 32);
        //icon.shadowSize = new GSize(37, 34);
        icon.iconAnchor = new GPoint(15, 34);
        icon.infoWindowAnchor = new GPoint(19, 2);
        //icon.infoShadowAnchor = new GPoint(18, 25);
        //map.getPane(G_MAP_FLOAT_SHADOW_PANE).style.visibility = "hidden"; 

    }

    function addClickevent(marker) { // Add a click listener to the markers

        google.maps.event.addListener(marker, "click", function () {
            var infowindow = new google.maps.InfoWindow();
            infowindow.setContent(marker.content);
            infowindow.open(map, this);
            //marker.openInfoWindowHtml(marker.content);
            /* Change count to continue from the last manually clicked marker
            *  Better syntax since Javascript 1.6 - Unfortunately not implemented in IE.
            *  count = gmarkers.indexOf(marker);
            */
            count = marker.nr;
            stopClick = true;
        });
        return marker;
    }
    function getAddress() {
        alert("420");
    }
    function getAddress(overlay, latlng) {
        alert("443");
        if (latlng != null) {
            alert("444");
            address = latlng;
            geocoder.geocode(latlng, showAddress);
            alert("44");
        }
    }

    function showAddress(response) {
        if (!response || response.Status.code != 200) {
            alert("Status Code:" + response.Status.code);
        }
        else {
            alert("449");
            place = response.Placemark[0];
            point = new google.maps.LatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
            var val =
            '&nbsp;<b>Address : </b>' + place.address;

            var obj = document.getElementById('Info');
            obj.innerHTML = val
        }
    }



    function handleError() {

        return true;
    }
    window.onerror = handleError;

    function Validate() {
        //    if (window.document.getElementById("ddBsu").value == -1) {
        //        alert('Please Select Business Unit.');return false;
        //    }
    }


    function checkBoxListOnClick(checkboxlistID) {

        var scount = 0;
        window.document.getElementById("HiddenUnitId").value = '-1';
        if (document.getElementById("CkAll").checked == '0') {
            window.document.getElementById("HiddenUnitId").value = '';
            if (document.getElementById(checkboxlistID) != null) {
                var tableBody = document.getElementById(checkboxlistID).childNodes[0];
                for (var i = 0; i < tableBody.childNodes.length; i++) {
                    var currentTd = tableBody.childNodes[i].childNodes[0];
                    var listControl = currentTd.childNodes[0];
                    if (listControl.checked == true) {
                        window.document.getElementById("HiddenUnitId").value += ',' + currentTd.childNodes[1].innerText.split('-')[1];
                        scount = scount + 1;
                    }
                }
            }

            if (scount > 20) {
                alert('You may select 20 records at a time.')
                window.document.getElementById("HiddenUnitId").value = ''

            }

        }

    }

    google.maps.event.addDomListener(window, 'load', initMap);
</script>
