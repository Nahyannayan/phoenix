﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_Version2_Pages_gpsv2PathHistory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
            BinndDate()
            HideNonGemsObject(lblpower)
        End If

    End Sub

    Public Sub HideNonGemsObject(ByVal Obj As Object)
        Dim NoGemObj As Boolean = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim query = "SELECT isnull(BSU_bGEMSSCHOOL,'False')BSU_bGEMSSCHOOL FROM OASIS.dbo.BUSINESSUNIT_M where BSU_ID='" + Session("sbsuid") + "'"
        NoGemObj = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)
        If NoGemObj Then
            Obj.Visible = False
        Else
            Obj.Visible = True
        End If

    End Sub

    Public Sub BinndDate()
        txtDate.Text = Today.ToString("dd/MMM/yyyy")
    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindVehReg()

    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, " select CONVERT(VARCHAR,VEH_REGNO) + ' -- ' +  CONVERT(VARCHAR,VEH_UNITID)  as DATA , VEH_UNITID from  OASIS_TRANSPORT.dbo.VW_VEH_DRIVER WHERE VEH_ALTO_BSU_ID='" & ddBsu.SelectedValue & "' ORDER BY VEH_REGNO , VEH_UNITID")

        If ds.Tables(0).Rows.Count > 0 Then
            ddvehRegNo.DataTextField = "DATA"
            ddvehRegNo.DataValueField = "VEH_UNITID"
            ddvehRegNo.DataSource = ds
            ddvehRegNo.DataBind()
        End If
        Dim list As New ListItem
        list.Text = "Vehicle"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)

        BindTrips()

    End Sub

    Public Sub BindTrips()
        ddtrips.Items.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtrips.DataSource = ds
            ddtrips.DataTextField = "TRIP"
            ddtrips.DataValueField = "GPS_TRIP_ID"
            ddtrips.DataBind()

        End If

        Dim list As New ListItem
        list.Text = "Select a Trip"
        list.Value = "-1"
        ddtrips.Items.Insert(0, list)

    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindVehReg()
    End Sub

    Protected Sub ddvehRegNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddvehRegNo.SelectedIndexChanged
        BindTrips()
    End Sub
End Class
