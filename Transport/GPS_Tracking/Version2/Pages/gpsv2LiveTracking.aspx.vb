﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_Version2_Pages_gpsv2LiveTracking
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
            BindBsucheck()
            lblselection.Text = "Business Unit"
            HideNonGemsObject(lblpower)
        End If

    End Sub
    Public Sub HideNonGemsObject(ByVal Obj As Object)
        Dim NoGemObj As Boolean = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim query = "SELECT isnull(BSU_bGEMSSCHOOL,'False')BSU_bGEMSSCHOOL FROM OASIS.dbo.BUSINESSUNIT_M where BSU_ID='" + Session("sbsuid") + "'"
        NoGemObj = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)
        If NoGemObj Then
            Obj.Visible = False
        Else
            Obj.Visible = True
        End If

    End Sub
    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)

        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindVehReg()

    End Sub

    Public Sub BindVehReg()

        CheckvehRegNo.Items.Clear()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, " select CONVERT(VARCHAR,VEH_REGNO) + '-' +  CONVERT(VARCHAR,VEH_UNITID)  as DATA , VEH_UNITID from  OASIS_TRANSPORT.dbo.VW_VEH_DRIVER WHERE VEH_ALTO_BSU_ID='" & ddBsu.SelectedValue & "' ORDER BY VEH_REGNO , VEH_UNITID")

        If ds.Tables(0).Rows.Count > 0 Then
            CheckvehRegNo.DataTextField = "DATA"
            CheckvehRegNo.DataValueField = "VEH_UNITID"
            CheckvehRegNo.DataSource = ds
            CheckvehRegNo.DataBind()
        End If
        BindChecked()
    End Sub

    Public Sub BindChecked()

        For Each item As ListItem In CheckvehRegNo.Items
            If CkAll.Checked Then
                item.Selected = True
            Else
                item.Selected = False
            End If
        Next

    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        If ddBsu.SelectedValue = "-1" Then
            lblselection.Text = "Business Unit"
            BindBsucheck()
        Else
            lblselection.Text = "Vehicle Reg No"
            BindVehReg()
        End If

    End Sub

    Protected Sub CkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CkAll.CheckedChanged
        BindChecked()
    End Sub

    Public Sub BindBsucheck()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            CheckvehRegNo.DataSource = dsBsu
            CheckvehRegNo.DataTextField = "BSU_NAME"
            CheckvehRegNo.DataValueField = "BSU_ID"
            CheckvehRegNo.DataBind()
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            CheckvehRegNo.DataSource = dsBsu
            CheckvehRegNo.DataTextField = "BSU_NAME"
            CheckvehRegNo.DataValueField = "BSU_ID"
            CheckvehRegNo.DataBind()
        End If
        BindChecked()
    End Sub
End Class
