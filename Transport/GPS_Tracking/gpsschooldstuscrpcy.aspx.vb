﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_gpsschooldstuscrpcy
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            txtDate.Text = Today.ToString("dd/MMM/yyyy")
            BindBsu()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReportExcel)
    End Sub


    Public Function getacd_id() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "  select top 1 ACD_ID from [OASIS].[dbo].[ACADEMICYEAR_D] where ACD_BSU_ID='" & ddBsu.SelectedValue & "'  AND ACD_CURRENT=1 "

        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    End Function
    Sub bindCLM()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim acd_id As String = getacd_id()

        '        str_query = " select clm_descr,clm_id from dbo.CURRICULUM_M where clm_id in " & _
        '" (select distinct acd_clm_id from academicyear_d where acd_acy_id='" & acd_id & "' and acd_bsu_id='" & ddBsu.SelectedValue & "')"

        str_query = " select clm_id,clm_descr from dbo.curriculum_M inner join  academicyear_d on acd_clm_id=clm_id" & _
                    " where acd_current=1 and acd_bsu_id='" & ddBsu.SelectedValue & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCurri.DataSource = ds
        ddlCurri.DataTextField = "clm_descr"
        ddlCurri.DataValueField = "clm_id"
        ddlCurri.DataBind()

        If ddlCurri.Items.Count = 0 Then
            ddlCurri.Items.Add(New ListItem("ALL", "0"))
            ddlCurri.ClearSelection()
            ddlCurri.Items.FindByText("ALL").Selected = True
        End If
        ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim clm As String = ddlCurri.SelectedValue
        Dim acy_id As String = getacd_id()

        Dim ds As DataSet

        If clm <> "0" Then
            ' str_query = " SELECT     distinct GRADE_BSU_M.GRM_GRD_ID, GRADE_BSU_M.GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER " & _
            '" FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
            '" WHERE(GRADE_BSU_M.GRM_BSU_ID = '" & ddBsu.SelectedValue & "') AND (GRADE_BSU_M.GRM_ACY_ID = '" & acy_id & "') AND (GRADE_BSU_M.GRM_ACD_ID = " & _
            '" (SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID ='" & clm & "') AND " & _
            '"  (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & ddBsu.SelectedValue & "'))) order by GRADE_M.GRD_DISPLAYORDER "

            str_query = "select distinct grm_grd_id,grm_display,grd_displayorder from dbo.grade_bsu_m inner join " & _
    " academicyear_d on grm_acd_id=acd_id inner join grade_m on grm_grd_id=grd_id " & _
    " where acd_current=1 and acd_bsu_id='" & ddBsu.SelectedValue & "' and acd_clm_id='" & ddlCurri.SelectedValue & "' order by grd_displayorder "


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "grm_grd_id"
            ddlGrade.DataBind()
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = True
        Else
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = False
        End If


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub

    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim clm As String = ddlCurri.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim acy_id As String = getacd_id()
        If clm <> "0" Then
            ' Dim str_query As String = " SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE  SCT_GRD_ID= '" & GRD_ID & "'" & _
            '" AND SCT_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & clm & "') AND " & _
            ' " (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & ddBsu.SelectedValue & "')) ORDER BY SCT_DESCR"


            Dim str_query As String = " SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE  SCT_GRD_ID= '01' " & _
    " AND SCT_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & ddlCurri.SelectedValue & "')  " & _
" and acd_current=1 AND (ACD_BSU_ID = '" & ddBsu.SelectedValue & "') ) ORDER BY SCT_DESCR "


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = True
        Else
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = False
        End If

    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        If ddBsu.SelectedValue <> "-1" Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter


            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

            pParms(1) = New SqlClient.SqlParameter("@DATE", txtDate.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 6)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                ddvehRegNo.DataTextField = "DATA"
                ddvehRegNo.DataValueField = "VEH_UNITID"
                ddvehRegNo.DataSource = ds
                ddvehRegNo.DataBind()
            End If
        End If

        Dim list As New ListItem
        list.Text = "Vehicle"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)

        BindTrips()

    End Sub

    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        ddBsu_SelectedIndexChanged(ddBsu, Nothing)

    End Sub
    Public Sub BindTrips()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtrips.DataSource = ds
            ddtrips.DataTextField = "TRIP"
            ddtrips.DataValueField = "GPS_TRIP_ID"
            ddtrips.DataBind()

            Dim list As New ListItem
            list.Text = "Select a Trip"
            list.Value = "-1"

            ddtrips.Items.Insert(0, list)
            ddtrips.Visible = True
        Else
            ddtrips.Visible = False
        End If




    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        bindCLM()
        BindVehReg()

    End Sub

    Public Function GetTripDetails(ByVal tipid As String) As DataSet

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 5)
        pParms(1) = New SqlClient.SqlParameter("@GPS_TRIP_ID", tipid)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)


        Return ds
    End Function


    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        BindGrade()
    End Sub

    Public Sub BindGrid(Optional ByVal export As Boolean = False)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(12) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())
        If txtstuname.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@STU_NAME", txtstuname.Text.Trim())
        End If
        If txtBusNo.Text.Trim() <> "" Then
            pParms(3) = New SqlClient.SqlParameter("@BUS_NO", txtBusNo.Text.Trim())
        End If
        'If ddBsu.SelectedValue <> "-1" Then
        '    pParms(4) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        'End If
        If ddvehRegNo.SelectedValue <> "-1" Then
            pParms(5) = New SqlClient.SqlParameter("@FK_UnitID", ddvehRegNo.SelectedValue)
        End If
        If txtStudentNumber.Text.Trim() <> "" Then
            pParms(6) = New SqlClient.SqlParameter("@STU_NO", txtStudentNumber.Text.Trim())
        End If
        If ddlGrade.SelectedValue <> "0" Then
            pParms(7) = New SqlClient.SqlParameter("@GRM_DISPLAY", ddlGrade.SelectedItem.Text.Trim())
        End If

        If ddlSection.SelectedValue <> "0" Then
            pParms(8) = New SqlClient.SqlParameter("@SCT_DESCR", ddlSection.SelectedItem.Text.Trim())
        End If


        If ddriskarea.SelectedValue <> "0" Then
            If ddriskarea.SelectedValue = "O" Then
                pParms(9) = New SqlClient.SqlParameter("@O_HAZARD", "True")
            Else
                pParms(9) = New SqlClient.SqlParameter("@R_HAZARD", "True")
            End If
        End If




        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU_ATT_DISCREPANCY", pParms)

        GridInfo.DataSource = ds
        GridInfo.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then

            If export Then
                Dim dt As DataTable = ds.Tables(0)
                'dt.Columns.Remove("GPS_TRIP_ID")
                'dt.Columns.Remove("viewdetails")
                ' Create excel file.
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
                Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                '   ws.HeadersFooters.AlignWithMargins = True
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
                'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                Dim pathSave As String
                pathSave = "Data.xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            End If

        End If





    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        BindGrid()
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReportExcel.Click
        BindGrid(True)
    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
