<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsGoogleMap_PlotPath.aspx.vb" Inherits="Transport_GPS_Tracking_Map_gpsGoogleMap_PlotPath" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GPS Path History</title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAeuBSnop-n2H2ynhKh8hcexTGfOK6oiJyuK2ZbZJplrYQarsorhS5wa-ITLWd56UFHGhxW3tuA5veDQ

" type="text/javascript"></script>
    <script src="../Javascript/GPS_Show_Path_History.js" type="text/javascript"></script>
    <script src="../Javascript/GPS_Assign_Geofencingv2.js" type="text/javascript"></script>

     <!-- Bootstrap core JavaScript-->
    <script src="../../../vendor/jquery/jquery.min.js"></script>
    <script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../../cssfiles/Popup.css" rel="stylesheet" />
    
</head>
<body onload="buildMap();load()" onunload="GUnload()" style="margin-left:-1px; margin-right:-1px;margin-top:-1px;margin-bottom:-1px;">
    <form id="form1"  runat="server">
   
        <table width="100%">
            <tr>
                <td align="center" valign="top">
                    <input type="button" onclick="closePoly();" class="button" value="Close Polyshape"/>
                    <asp:Button ID="btndeletefence" OnClientClick="ok();"  CssClass="button" runat="server" Text="Delete Geofence"  />
                     <asp:Button ID="btnGeoFencing" OnClientClick="ok();"  CssClass="button" runat="server" Text="Assign Geofence"  />
                </td>
            </tr>
            <tr>
                <td valign="top">
                   <div id="map" style="width: 1020px; position: static; height: 620px;">
        </div>  </td>
            </tr>
        </table>
   
       
        <div id="Info" style="background-color:InfoBackground">
        </div>
        <asp:HiddenField ID="HiddenUnitId"  runat="server" />
        <input id="DDATE" type="hidden" runat="server" />
        <input id="FHRS" type="hidden" runat="server" />
        <input id="FMINS" type="hidden" runat="server" />
        <input id="THRS" type="hidden" runat="server" />
        <input id="TMINS" type="hidden" runat="server" />
       
        
    <input type="radio" name="drawMode" id="drawMode_polyline" title="PolyLine" value="polyline" style="visibility:hidden" checked="checked" onclick="toggleDrawMode();" />
    <input type="radio" name="drawMode" id="drawMode_polygon" title="Polygon" value="polygon" style="visibility:hidden" onclick="toggleDrawMode();"/> 
    <input type="button" onclick="deleteLastPoint();" style="visibility:hidden" class="button" value="Delete Last Point"/>
   
    <input type="button" onclick="editlines();" style="visibility:hidden; display:none" class="button" value="Edit lines"/>
    <input type="button" onclick="clearMap();" style="visibility:hidden; display:none" class="button" value="Clear Map"/>
  

     <textarea  id="coords" runat="server" ></textarea>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="HiddenField2" runat="server" />
    <asp:HiddenField ID="HiddenBsu" runat="server" />
    <asp:HiddenField ID="HiddenTrips" runat="server" />
    <asp:HiddenField ID="HiddenDate" runat="server" />

        <asp:HiddenField ID="HiddenUserId"  runat="server" />
        <input id="HiddenLat" value="25.128128" type="hidden" />
        <input id="Hiddenlog" value="55.211222" type="hidden" />
        <input id="HiddenZoomLevel" value="12" type="hidden" />
        <input id="HiddenDirections" type="hidden" runat="server" />
        <input id="HiddenGeoFence" type="hidden" runat="server" />
          
    </form>
</body>
</html>
