Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
Imports System.IO
Imports System.Data
Partial Class Transport_GPS_Tracking_Map_gpsGoogleMap_PlotPath
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

            HiddenUnitId.Value = Request.QueryString("UnitId")
            HiddenUserId.Value = Request.QueryString("User")
            HiddenDirections.Value = Request.QueryString("Directions")
            HiddenGeoFence.Value = Request.QueryString("GeoFence")
            DDATE.Value = Request.QueryString("DDATE")
            FHRS.Value = Request.QueryString("FHRS")
            FMINS.Value = Request.QueryString("FMINS")
            THRS.Value = Request.QueryString("THRS")
            TMINS.Value = Request.QueryString("TMINS")

            ''Check If data avalable in database

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@DATE", DDATE.Value)
            pParms(1) = New SqlClient.SqlParameter("@FHRS", FHRS.Value)
            pParms(2) = New SqlClient.SqlParameter("@FMINS", FMINS.Value)
            pParms(3) = New SqlClient.SqlParameter("@THRS", THRS.Value)
            pParms(4) = New SqlClient.SqlParameter("@TMINS", TMINS.Value)
            pParms(5) = New SqlClient.SqlParameter("@UNIT_ID", HiddenUnitId.Value)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_PATH_HISTORY", pParms)

            If ds.Tables(0).Rows.Count = 0 Then
                Response.Write("<script type='text/javascript'> alert('Information not available for selected search'); window.close(); </script> ")
            End If

        End If
    End Sub

    Protected Sub btnGeoFencing_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGeoFencing.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim sql_query = ""

        ''Delete Previous assigned Geo Fencing Points
        sql_query = "Delete GEO_FENCING_POINTS where UNIT_ID ='" & HiddenUnitId.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)

        Dim allcaord As String()
        allcaord = coords.Value.Split("),")


        Dim i = 0
        For i = 0 To allcaord.Length - 1
            Dim lat, lng

            Dim split As String()
            allcaord(i) = allcaord(i).Replace("(", "").Replace(")", "")
            split = allcaord(i).Split(",")

            If split.Length = 2 Then

                split(0) = split(0).Replace(",", "")
                lat = split(0)
                split(1) = split(1).Replace(",", "")
                lng = split(1)
            End If
            If split.Length = 3 Then

                split(1) = split(1).Replace(",", "")
                lat = split(1)
                split(2) = split(2).Replace(",", "")
                lng = split(2)
            End If

            If split.Length = 2 Or split.Length = 3 Then
                sql_query = "insert into GEO_FENCING_POINTS (UNIT_ID,LATITUDE,LONGITUDE,GEO_ORDER) VALUES ('" & HiddenUnitId.Value & "','" & lat & "','" & lng & "','" & i + 1 & "')"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)
            End If
            ''Update data in GPS_LCD
            sql_query = "UPDATE dbo.GPS_LCD SET GEO_FENCE_TIME =NULL where  " & _
                        " REPLACE(CONVERT(VARCHAR(11), ENTRY_DATE, 106), ' ', '/') = REPLACE(CONVERT(VARCHAR(11), GETDATE(), 106), ' ', '/')  " & _
                        " AND UNIT_ID='" & HiddenUnitId.Value & "' "
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)

            Response.Write("<script>window.close();</script>")
        Next

    End Sub


    Protected Sub btndeletefence_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeletefence.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim sql_query = ""

        ''Delete Previous assigned Geo Fencing Points
        sql_query = "Delete GEO_FENCING_POINTS where UNIT_ID ='" & HiddenUnitId.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)

        ''Update data in GPS_LCD
        sql_query = "UPDATE dbo.GPS_LCD SET GEO_FENCE_TIME =NULL where  " & _
                    " REPLACE(CONVERT(VARCHAR(11), ENTRY_DATE, 106), ' ', '/') = REPLACE(CONVERT(VARCHAR(11), GETDATE(), 106), ' ', '/')  " & _
                    " AND UNIT_ID='" & HiddenUnitId.Value & "' "
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sql_query)


        Response.Write("<script> window.close();</script>")

    End Sub


End Class
