<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsGetInfo.aspx.vb" Inherits="Transport_GPS_Tracking_Map_gpsGetInfo" %>

<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <%--  <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core JavaScript-->
    <script src="../../../vendor/jquery/jquery.min.js"></script>
    <script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../../cssfiles/Popup.css" rel="stylesheet" />

    <title>Get Info</title>

    <script type="text/javascript">

        function openPopUp() {
            var UnitId = window.document.getElementById("HiddenUnitId").value
            var User = window.document.getElementById("HiddenUserId").value

            var DDATE = window.document.getElementById("txtFromdate").value
            var FHRS = window.document.getElementById("ddFHrs").value
            var FMINS = window.document.getElementById("ddFmin").value
            var THRS = window.document.getElementById("ddTHrs").value
            var TMINS = window.document.getElementById("ddTmin").value



            var Directions = 'False'
            var GeoFence = 'False'
            if (document.getElementById("CheckDirection").checked) {
                Directions = 'True'
            }
            if (document.getElementById("CheckGeoFence").checked) {
                GeoFence = 'True'
            }


            window.open('../Map/gpsGoogleMap_PlotPath.aspx?UnitId=' + UnitId + '&User=' + User + '&DDATE=' + DDATE + '&FHRS=' + FHRS + '&FMINS=' + FMINS + '&THRS=' + THRS + '&TMINS=' + TMINS + '&Directions=' + Directions + '&GeoFence=' + GeoFence, '', 'dialogHeight:1024px;dialogWidth:768px;scroll:no;resizable:no;'); return false;
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table width="100%" style="background-image: url(../../../Images/GPS_Images/gps_locator1.jpg); background-position: center center; background-attachment: fixed; background-repeat: repeat;">
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Date</span></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtFromdate" runat="server"></asp:TextBox>
                                <asp:ImageButton
                                    ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;" /></td>

                            <td align="left" width="20%"></td>
                            <td align="left" width="30%"></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">From Time</span></td>
                            <td align="left" width="30%">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="10%"><span class="field-label">Hrs</span></td>

                                        <td align="left" width="40%">
                                            <asp:DropDownList ID="ddFHrs" runat="server">
                                            </asp:DropDownList></td>
                                        <td align="left" width="10%"><span class="field-label">Sec</span></td>

                                        <td align="left" width="40%">
                                            <asp:DropDownList ID="ddFmin" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                            <td align="left" width="20%"><span class="field-label">To Time</span></td>
                            <td align="left" width="30%">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="10%"><span class="field-label">Hrs</span></td>

                                        <td align="left" width="40%">
                                            <asp:DropDownList ID="ddTHrs" runat="server">
                                            </asp:DropDownList></td>
                                        <td align="left" width="10%"><span class="field-label">Sec</span></td>

                                        <td align="left" width="40%">
                                            <asp:DropDownList ID="ddTmin" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>


                        <%--  <tr >
                                        <td>
                                            Map Type</td>
                                        <td colspan="7">
                                            <asp:DropDownList ID="DropMapType" runat="server">
                                                <asp:ListItem Selected="true" Value="G_PHYSICAL_MAP">Relief</asp:ListItem>
                                                <asp:ListItem Value="G_NORMAL_MAP">Map</asp:ListItem>
                                                <asp:ListItem Value="G_SATELLITE_MAP">Satellite</asp:ListItem>
                                 <asp:ListItem Value="G_HYBRID_MAP">Hybrid</asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>--%>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Directions</span> </td>
                            <td align="left" width="30%">
                                <asp:CheckBox ID="CheckDirection" runat="server" Text="Directions" CssClass="field-label" /></td>
                            <td align="left" width="20%"><span class="field-label">GeoFence</span></td>
                            <td align="left" width="30%">
                                <asp:CheckBox ID="CheckGeoFence" runat="server" Text="GeoFence" CssClass="field-label" />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="ButtonShow" runat="server" CssClass="button" Text="Get Data" />

                            </td>
                        </tr>
                    </table>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy"
                        PopupButtonID="ImageButton1" TargetControlID="txtFromdate">
                    </ajaxToolkit:CalendarExtender>
                    <asp:HiddenField ID="HiddenUnitId" runat="server" />
                    <asp:HiddenField ID="HiddenUserId" runat="server" />
                    <asp:HiddenField ID="HiddenOpen" Value="0" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </form>
</body>
</html>
<script type="text/javascript">

    if (document.getElementById("HiddenOpen").value == '1') {

        openPopUp();
    }


</script>
