Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
Imports System.IO
Imports System.Data

Partial Class Transport_GPS_Tracking_Map_gpsTrackVechile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
                Response.Cache.SetAllowResponseInBrowserHistory(False)
                Dim hash As Hashtable = Session("Vhashtable")
                Dim val As String = ""
                Dim i = 0

                Dim idictenum As IDictionaryEnumerator
                idictenum = hash.GetEnumerator()

                While (idictenum.MoveNext())
                    Dim key = idictenum.Key
                    Dim stuid = idictenum.Value
                    If i = 0 Then
                        val = idictenum.Value
                        i = 1
                    Else
                        val = val & "," & idictenum.Value
                    End If

                End While

                HiddenUnitId.Value = val

                ''Check If data avalable in database
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
                Dim pParms(1) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@UNITS", HiddenUnitId.Value)
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_VECHILE_CURRENT_LOCATION_FOR_GIVEN_UNITS", pParms)

                hash.Clear()
                Session("Vhashtable") = hash

                If ds.Tables(0).Rows.Count = 0 Then

                    Response.Write("<script type='text/javascript'> alert('Information not available for selected search'); window.close(); </script> ")

                End If



            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
End Class
