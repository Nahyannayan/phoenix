Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Xml
Imports System.IO
Partial Class Transport_GPS_Tracking_Map_gpsGetInfo
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

            HiddenUnitId.Value = Request.QueryString("UnitId")
            HiddenUserId.Value = Request.QueryString("User")
            BinndDate()
            BindMinSec()
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(ButtonShow)

    End Sub
    Public Sub BinndDate()
        txtFromdate.Text = Today.ToString("MM/dd/yyyy")
    End Sub
    Public Sub BindMinSec()
        ''Bind Mins
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddFHrs.Items.Insert(i, "0" & i.ToString())
                ddTHrs.Items.Insert(i, "0" & i.ToString())
            Else
                ddFHrs.Items.Insert(i, i)
                ddTHrs.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddFmin.Items.Insert(i, "0" & i.ToString())
                ddTmin.Items.Insert(i, "0" & i.ToString())
            Else
                ddFmin.Items.Insert(i, i)
                ddTmin.Items.Insert(i, i)
            End If
        Next
        ddTHrs.SelectedValue = "23"
        ddTmin.SelectedValue = "59"
    End Sub

    Protected Sub ButtonShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonShow.Click
        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        'Dim pParms(5) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@DATE", txtFromdate.Text.Trim())
        'pParms(1) = New SqlClient.SqlParameter("@FHRS", ddFHrs.SelectedItem.Text.Trim())
        'pParms(2) = New SqlClient.SqlParameter("@FMINS", ddFmin.SelectedItem.Text.Trim())
        'pParms(3) = New SqlClient.SqlParameter("@THRS", ddTHrs.SelectedItem.Text.Trim())
        'pParms(4) = New SqlClient.SqlParameter("@TMINS", ddTmin.SelectedItem.Text.Trim())
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GENERATE_DATA_FOR_XML", pParms)
        'GenerateXMLForMap(ds)
        HiddenOpen.Value = "1"
    End Sub
    Public Sub GenerateXMLForMap(ByVal ds As DataSet)
        Try
            Dim writer As New XmlTextWriter(HttpContext.Current.Server.MapPath("~/Transport/GPS_Tracking/Xml/" & HiddenUserId.Value & ".xml"), System.Text.Encoding.UTF8)
            writer.Formatting = Formatting.Indented
            writer.Indentation = 6
            writer.WriteStartDocument()
            writer.WriteStartElement("markers")
            If ds.Tables(0).Rows.Count >= 1 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim flag = 0
                    Dim Tripid = "Trip1"
                    Dim NextTripId = ""
                    If (i = ds.Tables(0).Rows.Count - 1) Then
                        flag = 1
                    ElseIf i < ds.Tables(0).Rows.Count - 1 Then
                        NextTripId = Tripid ''ds.Tables(0).Rows(i + 1).Item("TRIP_ID").ToString()
                    Else
                        NextTripId = Tripid
                    End If


                    Dim Html As New StringBuilder

                    If Tripid <> NextTripId Or flag = 1 Then
                        Html.Append("Vehicle Number :" & ds.Tables(0).Rows(i).Item("FK_UnitID").ToString())
                        Html.Append("<br>Latitude :" & ds.Tables(0).Rows(i).Item("Latitude").ToString())
                        Html.Append("<br>Longitude :" & ds.Tables(0).Rows(i).Item("Longitude").ToString())
                        Html.Append("<br>Speed :" & ds.Tables(0).Rows(i).Item("Speed").ToString())
                        Html.Append("<br>Direction :" & ds.Tables(0).Rows(i).Item("Direction").ToString())
                        Html.Append("<br>Altitude :" & ds.Tables(0).Rows(i).Item("Altitude").ToString())

                        Dim val As String = "javascript:openPopUp('"
                        val = val & ds.Tables(0).Rows(i).Item("FK_UnitID").ToString()
                        val = val & "','" & ds.Tables(0).Rows(i).Item("Latitude").ToString()
                        val = val & "','" & ds.Tables(0).Rows(i).Item("Longitude").ToString()
                        val = val & "')"

                        Html.Append("<br><input id='Button1' onclick=" & val & "  type='button' value='More Info' />")
                    End If

                    WriteItem(writer, ds.Tables(0).Rows(i).Item("Latitude").ToString(), ds.Tables(0).Rows(i).Item("Longitude").ToString(), Html, "Trip1", ds.Tables(0).Rows(i).Item("FK_UnitID").ToString(), ds.Tables(0).Rows(i).Item("PositionLogDateTime").ToString(), ds.Tables(0).Rows(i).Item("Speed").ToString(), ds.Tables(0).Rows(i).Item("Direction").ToString())
                Next
            End If
            writer.WriteEndElement()
            writer.Flush()
            writer.Close()

        Catch ex As Exception
            'Throw ex
        Finally
            ds = Nothing
        End Try
    End Sub
    Public Function AreaLocation(ByVal lat, ByVal lng) As String
        Dim ReturnValue As String = ""
        Dim ServicePath = "http://ws.geonames.org/findNearbyPlaceName?lat=" & lat & "&lng=" & lng & ""
        Dim myWebClient As New System.Net.WebClient
        Dim responseArray As Byte() = myWebClient.DownloadData(ServicePath)
        ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)
        Return ReturnValue
    End Function
    Protected Function WriteItem(ByVal writer As XmlTextWriter, ByVal Y As String, ByVal X As String, ByVal des As StringBuilder, ByVal Trip_ID As String, ByVal Bus_ID As String, ByVal datetime As String, ByVal speed As String, ByVal directions As String) As XmlTextWriter
        writer.WriteStartElement("marker")
        writer.WriteAttributeString("lat", Y + ",")
        writer.WriteAttributeString("lng", X + ",")
        writer.WriteAttributeString("Date", Convert.ToDateTime(datetime).ToString("dd/MM/yyyy HH:mm:ss"))
        writer.WriteAttributeString("info", des.ToString())
        writer.WriteAttributeString("Speed", speed)
        writer.WriteAttributeString("Directions", directions)
        writer.WriteAttributeString("Trip_ID", Trip_ID)
        writer.WriteAttributeString("Bus_ID", Bus_ID)
        writer.WriteEndElement()
        Return writer

    End Function
End Class
