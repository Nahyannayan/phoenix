﻿
Partial Class Transport_GPS_Tracking_Map_gpsShowLocation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

            Hiddenlat.Value = Request.QueryString("lat")
            Hiddenlng.Value = Request.QueryString("lng")

        End If
    End Sub
End Class
