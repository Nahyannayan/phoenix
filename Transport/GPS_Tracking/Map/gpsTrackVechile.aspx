<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsTrackVechile.aspx.vb" Inherits="Transport_GPS_Tracking_Map_gpsTrackVechile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>GPS Tracking</title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAeuBSnop-n2H2ynhKh8hcexTGfOK6oiJyuK2ZbZJplrYQarsorhS5wa-ITLWd56UFHGhxW3tuA5veDQ

"
        type="text/javascript"></script>
    <script src="../Javascript/GPS_Tracking.js" type="text/javascript"></script>
    <script src="../Javascript/labeledmarker.js" type="text/javascript"></script>
    <!-- Bootstrap core JavaScript-->
    <script src="../../../vendor/jquery/jquery.min.js"></script>
    <script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../../cssfiles/Popup.css" rel="stylesheet" />


</head>
<body onload="buildMap()" onunload="GUnload()" style="margin-left: -1px; margin-right: -1px; margin-top: -1px; margin-bottom: -1px;">
    <form id="form1" runat="server">


        <div id="map" style="width: 1020px; position: static; height: 660px;">
        </div>
        <div id="Info" style="background-color: InfoBackground">
        </div>


        <asp:HiddenField ID="HiddenUserId" runat="server" />
        <asp:HiddenField ID="HiddenUnitId" runat="server" />
        <input id="HiddenLat" value="25.128128" type="hidden" />
        <input id="Hiddenlog" value="55.211222" type="hidden" />
        <input id="HiddenZoomLevel" value="12" type="hidden" />

    </form>
</body>
</html>
