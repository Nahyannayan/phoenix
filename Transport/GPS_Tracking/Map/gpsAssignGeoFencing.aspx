<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsAssignGeoFencing.aspx.vb" Inherits="Transport_GPS_Tracking_Map_gpsAssignGeoFencing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GPS Assign Geo Fencing</title>
    <base target="_self" />
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAeuBSnop-n2H2ynhKh8hcexTGfOK6oiJyuK2ZbZJplrYQarsorhS5wa-ITLWd56UFHGhxW3tuA5veDQ

"
        type="text/javascript"></script>

    <%--    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script src="../Javascript/GPS_Assign_Geofencing.js" type="text/javascript"></script>

    <!-- Bootstrap core JavaScript-->
    <script src="../../../vendor/jquery/jquery.min.js"></script>
    <script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../../cssfiles/Popup.css" rel="stylesheet" />

</head>
<body onload="load()" onunload="GUnload()" style="margin-left: -1px; margin-right: -1px; margin-top: -1px; margin-bottom: -1px;">
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td align="left">
                        <input type="radio" name="drawMode" id="drawMode_polyline" value="polyline" checked="checked" onclick="toggleDrawMode();" />PolyLine
                        <input type="radio" name="drawMode" id="drawMode_polygon" value="polygon" onclick="toggleDrawMode();" />
                        Polygon
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <input type="button" onclick="deleteLastPoint();" class="button" value="Delete Last Point" />
                        <input type="button" onclick="closePoly();" class="button" value="Close Polyshape" />
                        <input type="button" onclick="editlines();" style="visibility: hidden; display: none" class="button" value="Edit lines" />
                        <input type="button" onclick="clearMap();" style="visibility: hidden; display: none" class="button" value="Clear Map" />
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <asp:Button ID="btndeletefence" OnClientClick="ok();" CssClass="button" runat="server" Text="Delete Previous Geofence" />
                        <asp:Button ID="btnGeoFencing" OnClientClick="closePoly();ok();" CssClass="button" runat="server" Text="Assign Geofence" />
                    </td>
                </tr>
            </table>





            <div id="map" style="width: 1024px; height: 768px"></div>
            <textarea id="coords" rows="100" cols="100" runat="server"></textarea>
            <asp:HiddenField ID="HiddenUserId" runat="server" />
            <asp:HiddenField ID="HiddenUnitId" runat="server" />
            <asp:HiddenField ID="HiddenBsu" runat="server" />
            <asp:HiddenField ID="HiddenTrips" runat="server" />
            <asp:HiddenField ID="HiddenDate" runat="server" />

        </div>
    </form>
</body>
</html>
