﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsShowLocation.aspx.vb" Inherits="Transport_GPS_Tracking_Map_gpsShowLocation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
          <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAeuBSnop-n2H2ynhKh8hcexTGfOK6oiJyuK2ZbZJplrYQarsorhS5wa-ITLWd56UFHGhxW3tuA5veDQ" type="text/javascript"></script>--%>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1iYoEmhopACwr8vAOLOO3KSOHvMERD-E&callback=initMap">
    </script>
<script type="text/javascript">


    function plot() {

        var gmarkers;
        try
        {
            var   map = new google.maps.Map(document.getElementById('map'), {
                center: new google.maps.LatLng(25.104565, 55.17231),
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scaleControl: false
            });
            var lat = document.getElementById("Hiddenlat").value
            var lng = document.getElementById("Hiddenlng").value
      
            if (lat != '' && lng != '') {
                map.setCenter(new google.maps.LatLng(lat, lng));
           
               
               var points = {
                    position: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
                    map: map
                   

                };
                gmarkers = new google.maps.Marker(points);
                gmarkers.setMap(map);
            }

        }
        catch (errMsg)
        {
            alert(errMsg.message);
        }
    }

   
</script>

</head>
<body onload='plot();'>
    <form id="form1" runat="server">
    <div>
       <div id="map" style=" width:512px; height:512px"> </div>
       </div>
    <asp:HiddenField ID="Hiddenlat" runat="server" />
    <asp:HiddenField ID="Hiddenlng" runat="server" />
    </form>
</body>
</html>
