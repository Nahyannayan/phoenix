﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="gpsschooldstuscrpcy.aspx.vb" Inherits="Transport_GPS_Tracking_gpsschooldstuscrpcy" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Absentee Discrepancies"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <div>


                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">Student Discrepancy List Search
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table width="100%">
                                    <%--  <tr>
                                <td colspan="6">
                                    <em style="color: #0000FF">For fast response , please specify filter conditions more
                                        precise.</em>
                                </td>
                            </tr>--%>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                                        </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Curriculum</span>
                                        </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlCurri_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Vehicle Reg No</span></td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddvehRegNo" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Grade</span>
                                        </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Trips</span>
                                        </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddtrips" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Section</span>
                                        </td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddlSection" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Student Number</span></td>

                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtStudentNumber" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Bus No</span></td>

                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtBusNo" runat="server"></asp:TextBox>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Risk Area</span></td>

                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddriskarea" runat="server">
                                                <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Onward" Value="O"></asp:ListItem>
                                                <asp:ListItem Text="Return" Value="R"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label"></span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtstuname" runat="server" Visible="False"></asp:TextBox>
                                        </td>


                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Date</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtDate"
                                                PopupPosition="TopLeft" TargetControlID="txtDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td align="left" width="20%"></td>
                                        <td align="left" width="30%"></td>

                                    </tr>
                                    <%-- <tr>
                            <td>
                                Time From
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:DropDownList ID="DropFromHr" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="DropFromMins" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                Time To
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:DropDownList ID="DropToHr" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="DropToMins" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>--%>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnview" runat="server" CssClass="button" Text="View"
                                                ValidationGroup="T1" />
                                            <asp:Button ID="btnReportExcel" runat="server" CssClass="button" Text="Excel Report"
                                                ValidationGroup="T1" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>


                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">Student Discrepancy List</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row"
                                    AutoGenerateColumns="false" EmptyDataText="Information not available."
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Student No  
                                                      
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                <%# Eval("STU_NO")%>
                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Name
                                                        
                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <%# Eval("STUDNAME")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Grade Section
                                                       
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                               <%# Eval("GRD_SEC") %> 
                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Onward/Return Bus No
                                                       
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                               <%# Eval("ONWARDBUS")%> /<%# Eval("RETURNBUS") %> 
                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <EmptyDataRowStyle />
                                    <SelectedRowStyle />
                                    <HeaderStyle />
                                    <EditRowStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>

                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="ddBsu" Display="None" ErrorMessage="Please select BSU"
                    InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtDate" Display="None" ErrorMessage="Please enter date"
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                    ShowMessageBox="True" ShowSummary="False" />

            </div>
        </div>
    </div>


</asp:Content>
