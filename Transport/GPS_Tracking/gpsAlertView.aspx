﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="gpsAlertView.aspx.vb" Inherits="Transport_GPS_Tracking_gpsAlertView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Alerts"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left" colspan="4" class="title-bg-lite">GPS Alerts </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Alerts</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddl_alerts" runat="server">
                            </asp:DropDownList>
                        </td>

                        <td align="left" colspan="2">
                            <asp:Button ID="btn_showAlerts" runat="server" CssClass="button" Text="Show"
                                OnClientClick="javascript:OpenListings();" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        function OpenListings() {
            var User = "";
            var e = document.getElementById("<%=ddl_alerts.ClientID %>").value;
            if (e == '1') {
                var win = window.open("../GPS_Tracking/gpsAlertViewBusStatusCheck.aspx", "", "toolbar=0,menubar=0,height=800,width=1200,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }
            else if (e == '4') {
                var win = window.open("../GPS_Tracking/gpsAlertViewChildCheck.aspx", "", "toolbar=0,menubar=0,height=800,width=1200,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }
            else if (e == '3') {
                var win = window.open("../GPS_Tracking/gpsAlertViewChildStillOnBoard.aspx", "", "toolbar=0,menubar=0,height=800,width=1200,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }
            else if (e == '5') {
                var win = window.open("../GPS_Tracking/gpsAlertViewGPSNotWorking.aspx", "", "toolbar=0,menubar=0,height=800,width=1200,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }
            else if (e == '6') {
                var win = window.open("../GPS_Tracking/gpsAlertViewStuDiffBusoNWARD.aspx", "", "toolbar=0,menubar=0,height=800,width=1200,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }
            else if (e == '7') {
                var win = window.open("../GPS_Tracking/gpsAlertViewStuDiffBusReturn.aspx", "", "toolbar=0,menubar=0,height=800,width=1200,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }
            else if (e == '8') {
                var win = window.open("../GPS_Tracking/gpsAlertViewTripDelay.aspx", "", "toolbar=0,menubar=0,height=800,width=1200,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>

