﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="gpsAttendanceNonCard.aspx.vb" Inherits="Transport_GPS_Tracking_gpsAttendanceNonCard" %>

<%@ Register Src="UserControls/gpsAttendanceNonCard.ascx" TagName="gpsAttendanceNonCard" TagPrefix="uc1" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Attendance (User Id card Lost)"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc1:gpsAttendanceNonCard ID="gpsAttendanceNonCard1" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>


