﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_gpsAlertTdb
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("tmsection") = 0 Then

            BindGrid()
            cpindex.Value = 1

        Else

            If Request.QueryString("cpindex") <> Request.QueryString("tpage") Then
                cpindex.Value = Convert.ToInt16(Request.QueryString("cpindex")) + 1
                gridInfo.PageIndex = Request.QueryString("cpindex")
            Else
                cpindex.Value = 0
                gridInfo.PageIndex = Request.QueryString("cpindex")
            End If

            BindGrid()

        End If



    End Sub

    Public Sub BindGrid()
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            If Request.QueryString("Bsu").Trim() <> "-1" Then
                If Request.QueryString("Bsu").Trim() <> "900501" Then
                    pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Request.QueryString("Bsu"))
                End If
            End If
            pParms(1) = New SqlClient.SqlParameter("@ALERT", "True")
            pParms(2) = New SqlClient.SqlParameter("@SPEED_LOW_ALERT_STU_IN", "True")
            pParms(3) = New SqlClient.SqlParameter("@SPEED_LIMIT", Request.QueryString("speed"))
            pParms(4) = New SqlClient.SqlParameter("@STU_COUNT_ALERT", Request.QueryString("scount"))

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BARCODE_LOG_STUDENT_IN_ALERT_REFRESH", pParms)


            gridInfo.DataSource = ds
            gridInfo.DataBind()

            Dim pval As Double = ds.Tables(0).Rows.Count / gridInfo.PageSize
            Dim pfval As Integer = 1
            If pval.ToString.Contains(".") Then

                If pval.ToString.Split(".")(1) > 0 Then
                    pfval = pval.ToString.Split(".")(0) + 1
                Else
                    pfval = pval.ToString.Split(".")(0)
                End If

            Else
                pfval = pval
            End If

            tpage.Value = pfval

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridInfo.PageIndexChanging
        gridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    ''Protected Sub btnaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnaction.Click
    ''    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

    ''    For Each row As GridViewRow In gridInfo.Rows
    ''        Dim check As CheckBox = DirectCast(row.FindControl("CheckAction"), CheckBox)
    ''        If check.Checked Then
    ''            Dim BARCODE = DirectCast(row.FindControl("HiddenBARCODE"), HiddenField).Value
    ''            Dim pParms(5) As SqlClient.SqlParameter
    ''            pParms(0) = New SqlClient.SqlParameter("@LCD_RECORD_ID", 0)
    ''            pParms(1) = New SqlClient.SqlParameter("@REMARKS", txtaction.Text.Trim())
    ''            pParms(2) = New SqlClient.SqlParameter("@TYPE", Request.QueryString("Type"))
    ''            pParms(3) = New SqlClient.SqlParameter("@ACTION_BY", Request.QueryString("EmployeeId"))
    ''            pParms(4) = New SqlClient.SqlParameter("@BARCODE", BARCODE)
    ''            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_ACTION_LOG", pParms)

    ''        End If

    ''    Next
    ''    BindGrid()
    ''End Sub

End Class
