﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsStudentAttendanceVehicleMessage.aspx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsStudentAttendanceVehicleMessage" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<script type="text/javascript" language="javascript">
    function CloseWindowYesNo() {
        if (confirm('Are you sure?'))
            window.close();
    }
    function CloseWindow() {
        window.close();
    }





</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
      <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <base target="_self" />
    <title>Message Text</title>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

</head>
<body>
    <form id="form1" runat="server">
        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left">
                    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server">
                    </asp:ScriptManager>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">

                        <tr>
                            <td align="left" class="title-bg-lite">Send Message
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:TextBox ID="txtmessage" runat="server" EnableTheming="False" TextMode="MultiLine"></asp:TextBox>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" OnClientClick="CloseWindowYesNo();" />

                </td>
            </tr>

            <tr>

                <td align="left">
                    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                </td>
            </tr>
        </table>
    </form>




</body>
</html>
