<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsLCDAlertListingsJavascript.aspx.vb"
    Inherits="Transport_GPS_Tracking_gpsLCDAlertListingsJavascript" %>

<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
<head id="Head1" runat="server">
    <base target="_self" />
    <title></title>

    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />


    <script type="text/javascript" src="/Transport/GPS_Tracking/Javascript/GPS_LCDView.js"></script>
    <style type="text/css">
        body {
            background: url("../../Images/GRIDHEAD.gif");
            background-attachment: fixed;
            background-repeat: repeat-x;
            background-position: bottom;
        }
    </style>
</head>
<body onload="onloadFunction()">
    <form id="form1" runat="server">
        <div class="matters">
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <%--<asp:Image ID="Image1" ImageUrl="~/Images/Schools/STS/sts_headerbaord_bg.jpg" Width="100%" runat="server" />--%>

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr class="title-bg-lite">
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td>Online Vehicle Tracking
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="ImageClose" OnClientClick="javascript:window.close();" runat="server"
                                        ImageUrl="~/Images/close_red.gif" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="GridBusListing" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField HeaderText="Vehicle Id">
                                    <HeaderTemplate>
                                        Sl.No.
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("VEH_ID") %>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reg.No.">
                                    <HeaderTemplate>
                                        Reg.No.
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("VEH_REGNO") %>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bus Type">
                                    <HeaderTemplate>
                                        Type
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("CAT_DESCRIPTION") %>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Business Unit">
                                    <HeaderTemplate>
                                        BSU
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("BSU_SHORTNAME")%>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Driver">
                                    <HeaderTemplate>
                                        Driver
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("Driver")%>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Conductor">
                                    <HeaderTemplate>
                                        Conductor
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Conductor") %>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Route">
                                <HeaderTemplate>
                                    <span style="font-size: small;">Route</span>
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <%# Eval("Route") %>
                                 </ItemTemplate>
                            </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Today's Distance Travelled">
                                    <HeaderTemplate>
                                        Distance Travelled
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("DistanceTraveled")%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Speed">
                                    <HeaderTemplate>
                                        Current Speed
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("SPEED")%>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Today's Max Speed">
                                    <HeaderTemplate>
                                        Max Speed
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("SPEED") %>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Today's Panic Time">
                                    <HeaderTemplate>
                                        Panic Time
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("TodayPanic")%>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Today's Speed Alert">
                                    <HeaderTemplate>
                                        Speed Alert
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("TodaySpeed") %>
                                            <asp:Image ID="ImageSpeedAlert" ImageUrl="~/Images/GPS_Images/Speed.png" Height="20px" Width="20px"
                                                ImageAlign="Top" runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Panic Alert">
                                    <HeaderTemplate>
                                        Panic Alert
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("TodayPanic") %>
                                            <asp:Image ID="ImagePanicAlert" ImageUrl="~/Images/GPS_Images/Panic.png" Height="20px" Width="20px"
                                                ImageAlign="Top" runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Today's Geo Fence Alert">
                                    <HeaderTemplate>
                                        Geo Fence Alert
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("TodayGeoFence") %>
                                            <asp:Image ID="ImageGeoAlert" ImageUrl="~/Images/GPS_Images/Geo.png" Height="20px" Width="20px"
                                                ImageAlign="Top" runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" VerticalAlign="Middle" HorizontalAlign="Center" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                    </td>
                </tr>
            </table>

            <table width="100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/Images/button_previous.jpg"
                            OnClientClick="javascript:Prevpage();return false;" Visible="true" runat="server" />
                        <asp:TextBox ID="txtpaging" Text="1" runat="server" Style="min-width: 10% !important; width: 10%;"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton2" ImageUrl="~/Images/button_next.jpg"
                            OnClientClick="javascript:Nextpage();return false;" Visible="true" runat="server" />
                        <img id="img1" src="../../Images/GPS_Images/expand.png" alt="Expand/Collapse" onclick="javascript:CloseExpand(this);return false;" style="width: 16px; height: 16px" />
                    </td>
                </tr>
            </table>


            <ajaxToolkit:FilteredTextBoxExtender ID="F1" TargetControlID="txtpaging" FilterType="Numbers"
                runat="server">
            </ajaxToolkit:FilteredTextBoxExtender>




            <table id="Tab1" style="visibility: hidden" width="100%" class="panel-cover">
                <tr>
                    <td align="left" width="10%"><span class="field-label">Date</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                    </td>
                    <td align="left" width="10%"><span class="field-label">Max Speed</span>
                    </td>

                    <td align="left" width="30%">

                        <table width="100%">
                            <tr>
                                <td align="left" width="50%">
                                    <asp:DropDownList ID="DropMaxSpeedFrom" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="50%">
                                    <asp:DropDownList ID="DropMaxSpeedTo" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>


                    </td>
                    <td align="left" width="20%"></td>

                </tr>
                <tr>
                    <td align="left"></td>
                    <td align="left">
                        <asp:CheckBox ID="CheckPanic" runat="server" Text="Panic Alert" CssClass="field-label" />
                        <asp:CheckBox ID="CheckGeo" runat="server" Text="Geo Fence Alert" CssClass="field-label" />
                    </td>

                    <td align="left" colspan="2">
                        <asp:ImageButton ID="ImageButton3" ImageUrl="~/Images/forum_Search.gif" ToolTip="Search" OnClientClick="javascript:search(); return false;"
                            runat="server" />
                        &nbsp;&nbsp;
                <asp:ImageButton ID="ImageButton4" ImageUrl="~/Images/actions.png" ToolTip="Actions" OnClientClick="javascript:actions(); return false;"
                    runat="server" />
                    </td>
                    <td align="left" width="20%"></td>
                </tr>
                <tr>
                    <td align="left" width="10%"><span class="field-label"></span></td>

                    <td align="left" width="30%">
                        <input id="CheckPause" onclick="moveent();" type="checkbox" /><span class="field-label">Pause</span></td>
                    <td align="left" width="10%"></td>
                    <td align="left" width="30%"></td>
                    <td align="left" width="20%"></td>
                </tr>
            </table>


            <ajaxToolkit:CalendarExtender ID="CE1" TargetControlID="txtDate" PopupPosition="TopRight" Format="dd/MMM/yyyy"
                runat="server">
            </ajaxToolkit:CalendarExtender>

            <asp:HiddenField ID="HiddenSearchOptions" runat="server" />
            <asp:HiddenField ID="HiddenSmsMobileNumbers" runat="server" />
            <asp:HiddenField ID="HiddenRefreshCount" runat="server" />
            <asp:HiddenField ID="HiddenRefresh" runat="server" />
            <asp:HiddenField ID="HiddenSendSms" runat="server" />
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />
            <asp:HiddenField ID="HiddenPaging" Value="0" runat="server" />
            <asp:HiddenField ID="HiddenStartFlag" Value="0" runat="server" />
            <asp:HiddenField ID="Hiddenemp_id" runat="server" />
            <br />
        </div>
        <embed src="../../Images/GPS_Images/panic.wav" autostart="false" width="0" height="0" name="sound1" enablejavascript="true">
    <embed src="../../Images/GPS_Images/Speed.wav" autostart=false width=0 height=0 name="sound2" enablejavascript="true">
    </form>
</body>
</html>

<script type="text/javascript">

    function actions() {

        var actionvalue = "?Actions=" + window.document.getElementById("txtDate").value;
        if (actionvalue != "") {
            var actionvaluefrom = window.document.getElementById("DropMaxSpeedFrom").value;
            var actionvalueto = window.document.getElementById("DropMaxSpeedTo").value;

            if (parseInt(actionvaluefrom) <= parseInt(actionvalueto)) {
                actionvalue = actionvalue + "$" + actionvaluefrom;
                actionvalue = actionvalue + "$" + actionvalueto;
                actionvalue = actionvalue + "$" + window.document.getElementById("CheckPanic").checked;
                actionvalue = actionvalue + "$" + window.document.getElementById("CheckGeo").checked;
                actionvalue = actionvalue + '&bsu=' + window.document.getElementById("Hiddenbsuid").value + '&emp_id=' + window.document.getElementById("Hiddenemp_id").value;
                //alert(actionvalue);
                //window.showModalDialog('gpsLCDAlertActions.aspx' + actionvalue, '', 'dialogHeight:600px;dialogWidth:1000px;scroll:no;resizable:no;');
                return ShowWindowWithClose('gpsLCDAlertActions.aspx' + actionvalue, 'search', '55%', '85%')
                return false;
                moveent()
            }
            else {
                alert("Please Enter Correct Max Speed Range")
            }
        }
        else {
            alert("Please Enter Date")
        }

    }


</script>

<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>
