﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsStudentAttendance.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsStudentAttendance" %>

<%@ Register Src="UserControls/gpsStudentAttendance.ascx" TagName="gpsStudentAttendance" TagPrefix="uc1" %>
<%@ Register Src="UserControls/gpsStudentAttendanceVehicle.ascx" TagName="gpsStudentAttendanceVehicle" TagPrefix="uc2" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Student Log Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc2:gpsStudentAttendanceVehicle ID="gpsStudentAttendanceVehicle1" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>






