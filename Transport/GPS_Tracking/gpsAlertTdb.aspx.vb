﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_gpsAlertTdb
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub

    Public Sub BindGrid()
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(16) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", Today.ToString("dd/MMM/yyyy"))

            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Request.QueryString("Bsu"))

            Dim FromHr = "00"
            Dim FromMins = "00"
            Dim ToHr = "23"
            Dim ToMins = "59"

            pParms(6) = New SqlClient.SqlParameter("@ALERT_TBD_UNIT_ID", Request.QueryString("vehid"))
            pParms(7) = New SqlClient.SqlParameter("@FromHr", FromHr)
            pParms(8) = New SqlClient.SqlParameter("@FromMins", FromMins)
            pParms(9) = New SqlClient.SqlParameter("@ToHr", ToHr)
            pParms(10) = New SqlClient.SqlParameter("@ToMins", ToMins)
            pParms(14) = New SqlClient.SqlParameter("@ALERT_TBD", "True")
            pParms(15) = New SqlClient.SqlParameter("@NOT_SAME_BUS", "True")


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BARCODE_LOG_STUDENT", pParms)


            gridInfo.DataSource = ds
            gridInfo.DataBind()



        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridInfo.PageIndexChanging
        gridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnaction.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        For Each row As GridViewRow In gridInfo.Rows
            Dim check As CheckBox = DirectCast(row.FindControl("CheckAction"), CheckBox)
            If check.Checked Then
                Dim logid = DirectCast(row.FindControl("HiddenLogpkid"), HiddenField).Value
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@LCD_RECORD_ID", 0)
                pParms(1) = New SqlClient.SqlParameter("@REMARKS", txtaction.Text.Trim())
                pParms(2) = New SqlClient.SqlParameter("@TYPE", "TDB")
                pParms(3) = New SqlClient.SqlParameter("@ACTION_BY", Request.QueryString("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@PK_LOG_ID", logid)
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_ACTION_LOG", pParms)

            End If

        Next
        BindGrid()
    End Sub

End Class
