﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsDefineTrips.aspx.vb" MasterPageFile="~/mainMasterPage.master"  Inherits="Transport_GPS_Tracking_gpsDefineTrips" %>

<%@ Register src="UserControls/gpsDefineTrips.ascx" tagname="gpsDefineTrips" tagprefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
 <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
 <link href="/cssfiles/sb-admin.css" rel="stylesheet">

 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Define Trips
        </div>
        <div class="card-body">
            <div class="table-responsive">

  <uc1:gpsDefineTrips ID="gpsDefineTrips1" runat="server" />

                

                </div>
            </div>
     </div>

</asp:Content> 
 
          
 