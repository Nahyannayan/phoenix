﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsMessagHistory.aspx.vb" Inherits="Transport_GPS_Tracking_gpsMessagHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Message History</title>
    <%--   <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <base target="_self" />
</head>
<body>
    <script type="text/javascript">

        //Javascript Error Handling

        function handleError() {

            return true;
        }
        window.onerror = handleError;


    </script>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div align="center">           

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Message History</td>
                </tr>
                <tr>
                    <td align="left">

                        <asp:GridView ID="GridData" runat="server" AllowPaging="True" EmptyDataText="No Records found" CssClass="table table-bordered table-row"
                            AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Date">
                                    <HeaderTemplate>
                                        Date
                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("ENTRY_DATE")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Message">
                                    <HeaderTemplate>
                                        Message
                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Elblview1" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                        <asp:Panel ID="E4Panel11" runat="server" Height="50px">
                                            <%#Eval("MESSAGE")%>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2"
                                            runat="server" AutoCollapse="False" AutoExpand="False"
                                            CollapseControlID="Elblview1" Collapsed="true" CollapsedSize="0"
                                            CollapsedText='<%#Eval("tempview")%>' ExpandControlID="Elblview1"
                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true"
                                            TargetControlID="E4Panel11" TextLabelID="Elblview1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sent To">
                                    <HeaderTemplate>
                                        Sent To
                          
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("SENT_TO")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <HeaderTemplate>
                                        Status
                          
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("STATUS")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sent By">
                                    <HeaderTemplate>
                                        Sent By
                          
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("SENT_BY")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>




                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
