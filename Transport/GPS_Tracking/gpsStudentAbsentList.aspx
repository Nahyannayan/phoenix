﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsStudentAbsentList.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsStudentAbsentList" %>

<%@ Register Src="UserControls/gpsStudentAbsentList.ascx" TagName="gpsStudentAbsentList" TagPrefix="uc1" %>



<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Transport Absentee/TWC List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <uc1:gpsStudentAbsentList ID="gpsStudentAbsentList1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>


