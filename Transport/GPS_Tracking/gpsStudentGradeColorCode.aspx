﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsStudentGradeColorCode.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsStudentGradeColorCode" %>

<%@ Register src="UserControls/gpsStudentGradeColorCode.ascx" tagname="gpsStudentGradeColorCode" tagprefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Define Grade Color
        </div>
        <div class="card-body">
            <div class="table-responsive">

      <uc1:gpsStudentGradeColorCode ID="gpsStudentGradeColorCode1" runat="server" />
    
                            
                </div>
            </div>
     </div>
</asp:Content> 

  
