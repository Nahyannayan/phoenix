﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsTripConsolidatedReport.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsTripConsolidatedReport" %>

<%@ Register Src="UserControls/gpsTripConsolidatedReport.ascx" TagName="gpsTripConsolidatedReport" TagPrefix="uc1" %>

<asp:Content ID="c1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Trip Consolidated Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc1:gpsTripConsolidatedReport ID="gpsTripConsolidatedReport1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>


