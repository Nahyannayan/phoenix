Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_gpsLCDAlertListingsJavascript
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenemp_id.Value = Session("EmployeeId")
            BindControl()
            Hiddenbsuid.Value = Session("sBsuid")
            BindGrid()
            BindBanner()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

        End If

    End Sub

    Public Sub BindControl()
        txtDate.Text = Today.ToString("dd/MMM/yyyy")
        Dim i = 0
        For i = 0 To 250
            DropMaxSpeedFrom.Items.Insert(i, i)
            DropMaxSpeedTo.Items.Insert(i, i)
        Next
        DropMaxSpeedFrom.SelectedValue = 80
        DropMaxSpeedTo.SelectedValue = 250

        HiddenSearchOptions.Value = txtDate.Text.Trim()
        HiddenSearchOptions.Value &= "$" & DropMaxSpeedFrom.SelectedValue
        HiddenSearchOptions.Value &= "$" & DropMaxSpeedTo.SelectedValue
        HiddenSearchOptions.Value &= "$" & CheckPanic.Checked
        HiddenSearchOptions.Value &= "$" & CheckPanic.Checked

    End Sub


    Public Sub BindBanner()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = "select BST_BSU_OPRT_ID from dbo.BSU_TRANSPORT " & _
                        " where BST_BSU_ID='" & Hiddenbsuid.Value & "' and BST_BSU_OPRT_ID<>'" & Hiddenbsuid.Value & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim bsu_id = ds.Tables(0).Rows(0).Item("BST_BSU_OPRT_ID").ToString()

            If bsu_id = "900501" Then ''STS

                'Image1.ImageUrl = "~/Images/Schools/STS/sts_headerbaord_bg.jpg"

            ElseIf bsu_id = "900500" Then ''Bright Bus

                'Image1.ImageUrl = "~/Images/Schools/BBT/BrightBus.jpg"

            Else ''STS

                'Image1.ImageUrl = "~/Images/Schools/STS/sts_headerbaord_bg.jpg"

            End If

        Else ''STS

            'Image1.ImageUrl = "~/Images/Schools/STS/sts_headerbaord_bg.jpg"

        End If


    End Sub
    Public Sub BindGrid()

        Dim dt As New DataTable
        dt.Columns.Add("VEH_ID")
        dt.Columns.Add("VEH_REGNO")
        dt.Columns.Add("CAT_DESCRIPTION")
        dt.Columns.Add("BSU_SHORTNAME")
        dt.Columns.Add("Driver")
        dt.Columns.Add("Conductor")
        dt.Columns.Add("Route")
        dt.Columns.Add("DistanceTraveled")
        dt.Columns.Add("SPEED")
        dt.Columns.Add("TodaySpeed")
        dt.Columns.Add("TodayPanic")
        dt.Columns.Add("TodayGeoFence")

        Dim i = 1
        For i = 1 To 10
            Dim dr As DataRow = dt.NewRow()
            dr.Item("VEH_ID") = ""
            dr.Item("VEH_REGNO") = ""
            dr.Item("CAT_DESCRIPTION") = ""
            dr.Item("BSU_SHORTNAME") = ""
            dr.Item("Driver") = ""
            dr.Item("Conductor") = ""
            dr.Item("Route") = ""
            dr.Item("DistanceTraveled") = ""
            dr.Item("SPEED") = ""
            dr.Item("TodaySpeed") = ""
            dr.Item("TodayPanic") = ""
            dr.Item("TodayGeoFence") = ""
            dt.Rows.Add(dr)
        Next

        GridBusListing.DataSource = dt
        GridBusListing.DataBind()


    End Sub
End Class
