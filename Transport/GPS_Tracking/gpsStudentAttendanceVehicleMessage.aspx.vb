﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_UserControls_gpsStudentAttendanceVehicleMessage
    Inherits System.Web.UI.Page
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim Studentids As String = ""
        Try
            If Session("StudentIDs") <> "" Then
                Studentids = Session("StudentIDs")
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Session("SelBsuid"))
                pParms(2) = New SqlClient.SqlParameter("@CMS_SMS_TEXT", LTrim(RTrim(txtmessage.Text)))
                pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@UNIQUE_IDS", Studentids)
                pParms(5) = New SqlClient.SqlParameter("@OPTION  ", 1)
                Dim sendid = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "PUSH_MESSAGE_APP", pParms)
                lblmessage.Text = "Saved Successfully. Message sending in progress"
            Else
                lblmessage.Text = "There is no data for selected filter."
            End If

        Catch ex As Exception
            lblmessage.Text = ex.Message

        End Try
        




    End Sub
End Class
