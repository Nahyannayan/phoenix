﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsSpare.aspx.vb" Inherits="Transport_GPS_Tracking_gpsSpare" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Spare Bus</title>
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <base target="_self" />
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Assign Spare Vehicle
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Business Unit</span>
                                </td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Vehicle Reg No</span></td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddvehRegNo" runat="server"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>

                                <td align="center" colspan="4">
                                    <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="button" />
                                    <asp:Button ID="btnreset" runat="server" Text="Reverse/Reset"  CssClass="button" />
                                </td>
                            </tr>
                            <tr>

                                <td align="center" colspan="4">
                                    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
