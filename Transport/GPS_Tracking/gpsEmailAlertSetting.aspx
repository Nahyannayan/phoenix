﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="gpsEmailAlertSetting.aspx.vb" Inherits="Transport_GPS_Tracking_gpsEmailAlertSetting" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<link href="css/GpsStyle.css" rel="stylesheet" type="text/css" />--%>
    <script src="Javascript/jquery-1.8.2.min.js" type="text/javascript"></script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Email Alert Setting"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table width="100%">
                    <tr class="title-bg">
                        <td colspan="4">GPS Email Alert Setting</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">School</span><span class="text-danger">*</span> </td>
                        <td align="left" colspan="3">
                            <%-- Filter="Contains" --%>
                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="chkBsu" runat="server">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Alert</span><span class="text-danger">*</span> </td>
                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="ddl_alerts" runat="server" Width="100%">
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="RV_alert" runat="server" ErrorMessage="Select alert" ControlToValidate="ddl_alerts" InitialValue="Select" ValidationGroup="save"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Email id</span><span class="text-danger">*</span>  </td>
                        <td align="left" colspan="3">
                            <telerik:RadTextBox ID="txt_EmailIds" runat="server" Width="100%"
                                EmptyMessage="Add Email Ids separated by commas"
                                TextMode="MultiLine">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RV_alert0" runat="server"
                                ErrorMessage="Enter email address" ControlToValidate="txt_EmailIds"
                                ValidationGroup="save"></asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Contact Number</span><span class="text-danger">*</span> </td>
                        <td align="left" colspan="3">
                            <telerik:RadTextBox ID="txt_mobnumbers" runat="server" Width="100%"
                                EmptyMessage="Add contact numbers separated by commas"
                                TextMode="MultiLine">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RV_alert1" runat="server"
                                ErrorMessage="Enter contact number" ControlToValidate="txt_mobnumbers"
                                ValidationGroup="save"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="save" />&nbsp;
             <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="false" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblerror" runat="server" Text="" EnableViewState="false" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <telerik:RadGrid ID="gv_emailalerts" runat="server" AutoGenerateColumns="False"
                                CellSpacing="0" GridLines="None" CssClass="table table-bordered table-row"
                                AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True">
                                <PagerStyle AlwaysVisible="True" />
                                <MasterTableView GroupLoadMode="Server">
                                    <CommandItemSettings></CommandItemSettings>

                                    <RowIndicatorColumn Visible="True">
                                        <HeaderStyle></HeaderStyle>
                                    </RowIndicatorColumn>

                                    <ExpandCollapseColumn Visible="True">
                                        <HeaderStyle></HeaderStyle>
                                    </ExpandCollapseColumn>

                                    <Columns>
                                        <telerik:GridBoundColumn
                                            UniqueName="column" HeaderText="BSU" DataField="BSU_SHORTNAME">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn
                                            UniqueName="column1" HeaderText="Alert" DataField="GAS_DES">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn
                                            UniqueName="column2" HeaderText="Email Ids" DataField="GEA_EMAIL_IDS">
                                            <ItemStyle></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn
                                            UniqueName="Mobno" HeaderText="Mob No" DataField="GEA_CONTACT_NO">
                                            <ItemStyle></ItemStyle>
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Edit"
                                            Groupable="false" UniqueName="TemplateColumn" AllowFiltering="False">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_Edit" runat="server" OnClick="lnk_Edit_Click" Text="Edit"></asp:LinkButton>
                                                <asp:HiddenField ID="HF_GEA_ID" runat="server" Value='<%#Bind("GEA_ID")%>' />
                                                <asp:HiddenField ID="HF_GAS_ID" runat="server" Value='<%#Bind("GEA_GAS_ID")%>' />
                                                <asp:HiddenField ID="HF_BSU_ID" runat="server" Value='<%#Bind("GEA_BSU_ID")%>' />
                                                <asp:HiddenField ID="HF_GEA_EMAIL_IDS" runat="server" Value='<%#Bind("GEA_EMAIL_IDS")%>' />
                                                <asp:HiddenField ID="HF_GEA_CONTACT_NO" runat="server" Value='<%#Bind("GEA_CONTACT_NO")%>' />

                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <EditFormSettings>
                                        <EditColumn></EditColumn>
                                    </EditFormSettings>
                                    <PagerStyle AlwaysVisible="True" />
                                </MasterTableView>
                                <ClientSettings AllowGroupExpandCollapse="True" ReorderColumnsOnClient="True" AllowDragToGroup="True"
                                    EnableRowHoverStyle="true" Selecting-AllowRowSelect="true" AllowColumnsReorder="True">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                </ClientSettings>
                                <GroupingSettings ShowUnGroupButton="true"></GroupingSettings>
                                <FilterMenu EnableImageSprites="False"></FilterMenu>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            var id = "#<%=chkBsu.ClientID %>_0";
            var checkboxlistid = "#<%=chkBsu.ClientID %>";
            $(id).click(function () {
                $("#<%=chkBsu.ClientID %> input:checkbox").attr('checked', this.checked);
            });
            $(checkboxlistid + " input:checkbox").click(function () {
                if ($(id).attr('checked') == true && this.checked == false) {
                    $(id).attr('checked', false);
                }
                else
                    CheckSelectAll();
            });

            function CheckSelectAll() {
                $(checkboxlistid + " input:checkbox").each(function () {
                    var checkedcount = $(checkboxlistid + " input[type=checkbox]:checked").length;
                    var checkcondition = $(checkboxlistid + " input[type=checkbox]").length - 1
                    if (checkedcount > checkcondition)
                        $(id).attr('checked', true);
                    else
                        $(id).attr('checked', false);
                });
            }
        });

    </script>
</asp:Content>

