﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsAlertActionsTripDelay.aspx.vb" Inherits="Transport_GPS_Tracking_gpsAlertTdb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            //window.setTimeout('loadpage();', 20000);

            function loadpage() {
                if (document.getElementById("<%=cp.ClientID %>").checked == false) {

                var path = window.location.href
                if (path.indexOf('?') != '-1') {
                    var Rpath = ''
                    var Fpath = ''
                    var hpath = ''
                    Rpath = path.substring(path.indexOf('?'), path.length)
                    Rpath = Rpath.replace('tmsection=0', 'tmsection=1')
                    Fpath = path.substring(0, path.indexOf('?'))
                    var cpindex = document.getElementById("<%=cpindex.ClientID %>").value;
                    var tpage = document.getElementById("<%=tpage.ClientID %>").value;
                    Rpath = Rpath.substring(0, Rpath.indexOf('tmsection=1')) + 'tmsection=1';
                    //alert(Rpath)
                    hpath = Fpath + Rpath + '&cpindex=' + cpindex + '&tpage=' + tpage;
                    //alert(hpath);
                    window.location.href = hpath;

                }

                //window.location = window.location
            }
            else {
                window.setTimeout('loadpage();', 20000);
            }
        }

        function change_chk_stateg(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckAction") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }



        </script>
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Actions
                    <asp:CheckBox ID="cp" Text="Pause" runat="server" />
                        &nbsp;&nbsp;
                    <asp:ImageButton ID="ImageClose" ToolTip="Close" OnClientClick='window.close();'
                        ImageUrl="../../Images/close_red1.png" runat="server" Height="16px" Width="16px" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="gridInfo" runat="server" AllowPaging="True" PageSize="20" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                            EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                            Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Reg&nbsp;No">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("VEH_REGNO")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Business&nbsp;Unit">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("BSU_SHORTNAME")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Driver">
                                    <ItemTemplate>
                                        <%#Eval("Driver")%>
                                        <br />

                                        <%#Eval("DrivMob")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Conductor">
                                    <ItemTemplate>
                                        <%# Eval("Conductor") %>
                                        <br />

                                        <%#Eval("CndMob")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="GPS&nbsp;Unit">
                                    <ItemTemplate>

                                        <center>
                                            <%# Eval("GPS_UNIT_ID")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trip Name">
                                    <ItemTemplate>

                                        <%# Eval("TRIPNAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trip Time">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("starttime") %>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trip Scan-Start time">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("P_Datetime")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trip Scan-End time">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("N_Datetime") %>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mins Diff">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("minsdiff") %>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Action
                                   <center>
                                       <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                           ToolTip="Click here to select/deselect all rows" /></center>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:CheckBox ID="CheckAction" runat="server" />
                                            <asp:HiddenField ID="HiddenBARCODE" Value='<%# Eval("GPS_UNIT_ID") %>' runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                        <asp:TextBox ID="txtaction" runat="server" Style="min-width: 10% !important; width: 10% !important;"></asp:TextBox>
                        <asp:Button ID="btnaction" runat="server" Text="Save Action" />
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="HiddenEmpId" runat="server" />
            <asp:HiddenField ID="cpindex" runat="server" />
            <asp:HiddenField ID="tpage" runat="server" />
            <asp:HiddenField ID="HiddenAlertType" runat="server" />
        </div>
    </form>
</body>
</html>
