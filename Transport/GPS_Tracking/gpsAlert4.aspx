﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsAlert4.aspx.vb" Inherits="Transport_GPS_Tracking_gpsAlertTdb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%-- <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            window.setTimeout('loadpage();', 20000);

            function loadpage() {
                if (document.getElementById("<%=cp.ClientID %>").checked == false) {

                    var path = window.location.href
                    if (path.indexOf('?') != '-1') {
                        var Rpath = ''
                        var Fpath = ''
                        var hpath = ''
                        Rpath = path.substring(path.indexOf('?'), path.length)
                        Rpath = Rpath.replace('tmsection=0', 'tmsection=1')
                        Fpath = path.substring(0, path.indexOf('?'))
                        var cpindex = document.getElementById("<%=cpindex.ClientID %>").value;
                    var tpage = document.getElementById("<%=tpage.ClientID %>").value;
                    Rpath = Rpath.substring(0, Rpath.indexOf('tmsection=1')) + 'tmsection=1';
                    //alert(Rpath)
                    hpath = Fpath + Rpath + '&cpindex=' + cpindex + '&tpage=' + tpage;
                    //alert(hpath);
                    window.location.href = hpath;

                }

                    //window.location = window.location
            }
            else {
                window.setTimeout('loadpage();', 20000);
            }
        }

        function openlist(a, b, c) {

            if (a == 1) {

                var win = window.open("gpsAlertTdb.aspx?vehid=" + b + "&Bsu=" + c + "&tmsection=0")

            }
            if (a == 2) {
                var win = window.open("gpsStudentInAlert.aspx?vehid=" + b + "&Bsu=" + c + "&tmsection=0")
            }

        }


        </script>
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Alert
                    <asp:CheckBox ID="cp" Text="Pause" runat="server" />
                        &nbsp;&nbsp;
                    <asp:ImageButton ID="ImageClose" ToolTip="Close" OnClientClick='window.close();'
                        ImageUrl="../../Images/close_red1.png" runat="server" Height="16px" Width="16px" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="gridInfo" runat="server" AllowPaging="True" PageSize="20" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                            EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                            Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Vehicle No">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("VEH_REGNO")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BSU">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("bsu_shortname")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Driver">
                                    <ItemTemplate>
                                        <%# Eval("DRIVER")%>
                                        <br />
                                        <%# Eval("Drivmob")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Conductor">
                                    <ItemTemplate>
                                        <%# Eval("CONDUCTOR")%>
                                        <br />
                                        <%# Eval("cndmob")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("cat_description")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Speed">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("speed")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Student In">
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("INCOUNT")%>
                                            <br />
                                            <asp:ImageButton ID="imgstuin" ImageUrl="~/Images/alertRed.gif" runat="server" />
                                            <%--<asp:LinkButton ID="lnk2"  OnClientClick='<%# Eval("STU_IN")%>' Visible='<%# Eval("STU_IN_ALERT")%>' runat="server">View</asp:LinkButton>--%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                        <asp:TextBox ID="txtaction" runat="server" Visible="false" Style="min-width: 10% !important; width: 10% !important;"></asp:TextBox>
                        <asp:Button ID="btnaction" runat="server" Visible="false" Text="Save Action" />
                        <asp:Label ID="lblmessage" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="HiddenEmpId" runat="server" />
            <asp:HiddenField ID="cpindex" runat="server" />
            <asp:HiddenField ID="tpage" runat="server" />
            <asp:HiddenField ID="HiddenAlertType" runat="server" />
        </div>
    </form>
</body>
</html>
