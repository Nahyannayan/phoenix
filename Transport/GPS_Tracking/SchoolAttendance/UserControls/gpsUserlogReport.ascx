﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsUserlogReport.ascx.vb"
    Inherits="Transport_GPS_Tracking_SchoolAttendance_UserControls_gpsUserlogReport" %>
<script type="text/javascript">

    function ShowMap(link) {

        window.showModalDialog(link)

    }


</script>
<div>


<table width="100%">
    <tr>
        <td class="title-bg">
            Search
        </td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
                <tr>
                    <td align="left" width="20%">
                        <span class="field-label">Business Unit</span>
                    </td>
                    
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                
                    <td align="left" width="20%">
                        <span class="field-label">Name</span>
                    </td>
                   
                    <td align="left" width="30%">
                        <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        <span class="field-label">User No.</span>
                    </td>
                    
                    <td>
                        <asp:TextBox ID="txtuserno" runat="server" Width="188px"></asp:TextBox>
                    </td>
                
                    <td>
                        <span class="field-label">Location</span>
                    </td>
                   
                    <td>
                        <asp:DropDownList ID="ddlocation" runat="server">
                        </asp:DropDownList>
                    </td>
                        </tr>
                <tr>
                    <td>
                        <span class="field-label">Card Type</span>
                    </td>
                    
                    <td>
                        <asp:DropDownList ID="ddcardtype" runat="server">
                        </asp:DropDownList>
                    </td>
                
                    <td>
                        <span class="field-label">User Type</span>
                    </td>
                   
                    <td>
                        <asp:DropDownList ID="ddusertype" runat="server">
                            <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="Student" Value="STUDENT"></asp:ListItem>
                            <asp:ListItem Text="Employee" Value="EMPLOYEE"></asp:ListItem>
                            <asp:ListItem Text="Unknown" Value="UNKNOWN"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    </tr>
                   <tr>
                    <td>
                        <span class="field-label">Date</span>
                    </td>
                    
                    <td>
                        <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                    </td>
                       <td></td>
                       <td></td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnview" runat="server" CssClass="button" Text="View Log" />
                        <asp:Button ID="btnview0" runat="server" CssClass="button" Text="Export Excel" 
                         />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<ajaxToolkit:CalendarExtender ID="CL1" TargetControlID="txtdate" PopupButtonID="txtdate"
    Format="dd/MMM/yyyy" runat="server">
</ajaxToolkit:CalendarExtender>
<br />
<table width="100%">
    <tr>
        <td class="title-bg">
            User Logs
            <asp:Label ID="lblmessage0" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridInfo" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" PageSize="25" AutoGenerateColumns="false"
                EmptyDataText="Information not available." Width="100%">
                <Columns>
                          <asp:TemplateField>
                        <HeaderTemplate>
                                        Sl No.
                        </HeaderTemplate>
                        <ItemTemplate>
                         <center>  <%# Eval("LOG_ID")%> </center> 
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>                           
                                        User No
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblno" runat="server" Visible='<%# Eval("Profilevis") %>' Text='<%# Eval("BARCODE") %>'></asp:Label>
                            <br />
                            <center>
                            
                            <asp:Image ID="Image1" Visible='<%# Eval("diffbsu") %>' ImageUrl="~/Images/alertRed.gif" runat="server" />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>                            
                                        User Name                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblname" runat="server" Visible='<%# Eval("Profilevis") %>' Text='<%# Eval("USR_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>                           
                                        User Type
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("USR_TYPE")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                                        <asp:TemplateField>
                        <HeaderTemplate>                           
                                        User Info
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("USER_INFO")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                                    Location 1
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <br />
                                <asp:LinkButton ID="lnklocation1" Text='<%# Eval("P_LOCATION") %>' OnClientClick='<%# Eval("Location1") %>'
                                    runat="server"></asp:LinkButton>
                                <br />
                                <br />
                                 <%# Eval("P_Datetime")%>
                               
                                 
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                                       Location 2                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <br />
                                 <asp:LinkButton ID="lnklocation2" Text='<%# Eval("N_LOCATION") %>' Visible='<%# Eval("N_Visible") %>'
                                    OnClientClick='<%# Eval("Location2") %>' runat="server"></asp:LinkButton>
                                <br />
                                <br />                               
                                    <asp:Label ID="lbllocation2" runat="server" Visible='<%# Eval("N_Visible") %>' Text='<%# Eval("N_Datetime") %>'></asp:Label>
                                                            
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>                         
                                        Time Difference
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>                               
                                    <%# Eval("Minsdiff")%>
                                    <%# Eval("Secdiff")%>                            
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <%--<SelectedRowStyle CssClass="Green" Wrap="False" />--%>
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <%--<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
    ControlToValidate="ddBsu" Display="None" 
    ErrorMessage="Please select bussiness unit" InitialValue="-1" 
    SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
    ControlToValidate="txtdate" Display="None" ErrorMessage="Please enter date" 
    SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" 
    ShowMessageBox="True" ShowSummary="False" />

</div>