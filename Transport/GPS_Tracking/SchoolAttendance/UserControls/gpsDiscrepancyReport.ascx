﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsDiscrepancyReport.ascx.vb" Inherits="Transport_GPS_Tracking_SchoolAttendance_UserControls_gpsDiscrepancyReport" %>
<%@ Register src="../../UserControls/usrStudentdetails.ascx" tagname="usrStudentdetails" tagprefix="uc1" %>
<script type="text/javascript">

    function ShowMap(link) {

        window.showModalDialog(link)

    }


</script>
<div class="matters" >


<table width="100%">
    <tr>
        <td class="title-bg">
            Search
        </td>
    </tr>
    <tr>
        <td align="left">
            <table class="style1">
                <tr>
                    <td align="left" width="20%">
                            <span class="field-label">Business Unit</span>
                    </td>
                    
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                
                    <td align="left" width="20%">
                                <span class="field-label">Vehicle Reg No</span></td>
                    
                    <td align="left" width="30%">
                                <asp:DropDownList ID="ddvehRegNo" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Name</span>
                    </td>
                    
                    <td>
                        <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <span class="field-label">User No.</span>
                    </td>
                    
                    <td>
                        <asp:TextBox ID="txtuserno" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                       <span class="field-label">Date From</span></td>
                    
                    <td>
                        <asp:TextBox ID="txtdate1" runat="server"></asp:TextBox>
<ajaxToolkit:CalendarExtender ID="CL1" TargetControlID="txtdate1" PopupButtonID="txtdate1"
    Format="dd/MMM/yyyy" runat="server">
</ajaxToolkit:CalendarExtender>
                    </td>
                    <td>
                        <span class="field-label">Date To</span></td>
                    
                    <td>
                        <asp:TextBox ID="txtdate2" runat="server"></asp:TextBox>
<ajaxToolkit:CalendarExtender ID="CL2" TargetControlID="txtdate2" PopupButtonID="txtdate2"
    Format="dd/MMM/yyyy" runat="server">
</ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnview" runat="server" CssClass="button" Text="View Log" />
                        <asp:Button ID="btnview0" runat="server" CssClass="button" Text="Export Excel" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />

<table width="100%">
    <tr>
        <td class="title-bg">
            Student Discrepancy Log
            <asp:Label ID="lblmessage0" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridData" runat="server" AllowPaging="True" 
                AutoGenerateColumns="false" cssclass="table table-bordered table-row"
                EmptyDataText="&lt;center&gt;No Records Found&lt;/center&gt;" PageSize="20" 
                Width="100%">
                <Columns>
                    
                    <asp:TemplateField HeaderText="Student Details">
                    <ItemTemplate>
                       <uc1:usrStudentdetails ID="usrStudentdetails1" Visible='<%# Eval("P_Visible") %>' StudentDetails_ID='<%# Eval("BARCODE") %>' runat="server" />
                    </ItemTemplate>
                     </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Time">
                    <ItemTemplate>
                     <%# Eval("P_Datetime_c") %>
                    </ItemTemplate>                
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Veh Reg.">
                    <ItemTemplate>
                    <center>
                      <%# Eval("VEH_REGNO")%>
                    </center>
                    
                    </ItemTemplate>
                      
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Onward/Return">
                    <ItemTemplate>
                    <center>
                     <%# Eval("ONW")%> /  <%# Eval("RET") %>
                    </center>
                    
                    </ItemTemplate>
                      
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                    <center>
                     <asp:LinkButton ID="L1" CommandName="Location" CommandArgument='<%# Eval("PK_PositionLogID")%>' runat="server">Location</asp:LinkButton>
                    </center>
                    
                    </ItemTemplate>
                      
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Map">
                     <ItemTemplate>
                     <center>
                      <asp:LinkButton ID="L2" OnClientClick='<%# Eval("map")%>' runat="server">Map</asp:LinkButton>
                     </center>
                    </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <RowStyle CssClass="griditem" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>

        <asp:Label ID="Label2" runat="server"></asp:Label><asp:Panel ID="PanelShow" runat="server"
            BackColor="white" CssClass="modalPopup" Style="display: none" Width="512">
            <table width="500">
                <tr>
                    <td class="title-bg">
                        Location of the Bus
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <table width="500">
                                    <tbody>
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="btncancel" runat="server" Text="OK" CssClass="button" CausesValidation="false"
                                                   ></asp:Button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
            CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelShow" RepositionMode="RepositionOnWindowResizeAndScroll"
            TargetControlID="Label2">
        </ajaxToolkit:ModalPopupExtender>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
    ErrorMessage="Please select Bussiness Unit" ControlToValidate="ddBsu" 
    Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
    ErrorMessage="Please enter from date" ControlToValidate="txtdate1" 
    Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
    ErrorMessage="Please enter to date" ControlToValidate="txtdate2" Display="None" 
    SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" 
    ShowMessageBox="True" ShowSummary="False" />
    </div>