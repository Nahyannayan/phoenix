﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_SchoolAttendance_UserControls_gpsDiscrepancyReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
            BinndDate()
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnview0)


    End Sub


    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindVehReg()

    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        If ddBsu.SelectedValue <> "-1" Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim Query = "SELECT  VEH_UNITID,VEH_REGNO FROM dbo.VW_VEH_DRIVER WHERE VEH_ALTO_BSU_ID='" & ddBsu.SelectedValue & "' order by VEH_REGNO"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)

            If ds.Tables(0).Rows.Count > 0 Then
                ddvehRegNo.DataTextField = "VEH_REGNO"
                ddvehRegNo.DataValueField = "VEH_UNITID"
                ddvehRegNo.DataSource = ds
                ddvehRegNo.DataBind()
            End If
        End If

        Dim list As New ListItem
        list.Text = "Vehicle"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)

    End Sub
    Public Sub BinndDate()
        txtdate1.Text = Today.ToString("dd/MMM/yyyy")
        txtdate2.Text = Today.ToString("dd/MMM/yyyy")
    End Sub

    Protected Sub GridData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridData.RowCommand
        Try


            If e.CommandName = "Location" Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@PK_PositionLogID", e.CommandArgument)
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_LAST_PK_LOG_ID", pParms)


                Dim lat, lng As String


                If ds.Tables(0).Rows.Count > 0 Then
                    lat = ds.Tables(0).Rows(0).Item("Latitude").ToString()
                    lng = ds.Tables(0).Rows(0).Item("Longitude").ToString()

                    Dim ReturnValue As String = ""


                    Dim ServicePath = "http://ws.geonames.org/findNearbyPlaceName?lat=" & lat & "&lng=" & lng & ""
                    Dim myWebClient As New System.Net.WebClient
                    Dim responseArray As Byte() = myWebClient.DownloadData(ServicePath)
                    ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)
                    Dim startindex = ReturnValue.IndexOf("<name>")
                    Dim endindex = ReturnValue.IndexOf("</name>")
                    ReturnValue = ReturnValue.Substring(startindex + 6, (endindex - (startindex + 6)))
                    Label1.Text = ReturnValue.Replace("?", "")
                Else
                    Label1.Text = "Unit Information not available in our database."
                End If

                MO1.Show()

            End If
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        BindGrid(False)
    End Sub


    Public Sub BindGrid(ByVal export As Boolean)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime1", txtdate1.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@PositionLogDateTime2", txtdate2.Text.Trim())
        If ddBsu.SelectedValue <> "-1" Then
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        End If
        If ddvehRegNo.SelectedValue <> "-1" Then
            pParms(3) = New SqlClient.SqlParameter("@FK_UnitID", ddvehRegNo.SelectedValue)
        End If

        If txtname.Text.Trim() <> "" Then
            pParms(4) = New SqlClient.SqlParameter("@USR_NAME", txtname.Text.Trim())
        End If

        If txtuserno.Text.Trim() <> "" Then
            pParms(5) = New SqlClient.SqlParameter("@USR_NO", txtuserno.Text.Trim())
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BARCODE_LOG_STUDENT_DISCP", pParms)
        GridData.DataSource = ds
        GridData.DataBind()

        If export Then
            Dim dt As DataTable = ds.Tables(0)
            dt.Columns.Remove("P_Visible")
            dt.Columns.Remove("PK_PositionLogID")
            dt.Columns.Remove("map")
            ' Create excel file.
            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
            Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
            ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '  ws.HeadersFooters.AlignWithMargins = True
            'Response.ContentType = "application/vnd.ms-excel"
            'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
            'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "Data.xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If

    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindVehReg()
    End Sub

    Protected Sub btnview0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview0.Click
        BindGrid(True)
    End Sub

End Class
