﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsSetGPSUnitsIdInLocation.ascx.vb"
    Inherits="Transport_GPS_Tracking_SchoolAttendance_UserControls_gpsSetGPSUnitsIdInLocation" %>
<div>
    <table align="center" width="100%">
        <tr>
            <td class="title-bg">
                Define Locations
            </td>
        </tr>
        <tr>
            <td align="left" width="100%">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Location Type</span>
                        </td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddType" runat="server">
                            </asp:DropDownList>
                        </td>
                    
                        <td align="left" width="20%">
                            <span class="field-label">Location Description</span>
                        </td>
                       
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtLocationdesc" runat="server" Width="199px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">GPS Unit Id</span>
                        </td>
                        
                        <td>
                            <asp:TextBox ID="txtLocationUnitid" runat="server" Width="199px"></asp:TextBox>
                        </td>
                    
                        <td>
                           <span class="field-label"> Flow</span>
                        </td>
                        
                        <td>
                            <asp:DropDownList ID="ddFlow" runat="server">
                                <asp:ListItem Text="IN & OUT-(IO)" Value="IO"></asp:ListItem>
                                <asp:ListItem Text="Only IN-(I)" Value="I"></asp:ListItem>
                                <asp:ListItem Text="Only OUT-(O)" Value="O"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        
                        <td align="center" colspan="4">
                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                        </td>
                    </tr>
                    <tr>
                        
                        <td align="center" colspan="4">
                            <asp:Label ID="lblmessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<br />
<table  width="100%">
    <tr>
        <td class="title-bg">
            Locations
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridInfo" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="false" Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Type
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("LOCATION_TYPE_DES")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Location Description
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("LOCATION_DES")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        GPS Unit id
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("GPS_UNIT_ID")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                           
                                        Flow
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("LOCATION_FLOW")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Delete
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnkdelete" CommandName="deleting" CommandArgument='<%# Eval("LOCATION_ID")%>'
                                    runat="server">Delete</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
               <%-- <SelectedRowStyle CssClass="Green" Wrap="False" />--%>
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
               <%-- <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
            </asp:GridView>
            <ajaxToolkit:FilteredTextBoxExtender ID="F1" FilterType="Numbers" TargetControlID="txtLocationUnitid"
                runat="server">
            </ajaxToolkit:FilteredTextBoxExtender>
        </td>
    </tr>
</table>
