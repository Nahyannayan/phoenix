﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_SchoolAttendance_UserControls_gpsSetGPSUnitsIdInLocation
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocationtype()
            BindGrid()
        End If
    End Sub


    Public Sub BindLocationtype()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim query As String = "SELECT * FROM GPS_LOCATION_TYPE"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        ddType.DataSource = ds
        ddType.DataTextField = "LOCATION_TYPE_DES"
        ddType.DataValueField = "LOCATION_TYPE_ID"
        ddType.DataBind()

    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query As String = " SELECT * FROM dbo.GPS_LOCATION_SET  A INNER JOIN dbo.GPS_LOCATION_TYPE B ON A.LOCATION_TYPE_ID=B.LOCATION_TYPE_ID " & _
                              "WHERE BSU_ID='" & Session("sbsuid") & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        GridInfo.DataSource = ds
        GridInfo.DataBind()


    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim pParms(5) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@LOCATION_TYPE_ID", ddType.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@GPS_UNIT_ID", txtLocationUnitid.Text.Trim())
        pParms(2) = New SqlClient.SqlParameter("@LOCATION_DES", txtLocationdesc.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@LOCATION_FLOW", ddFlow.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))

        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_LOCATION_SET_INSERT_EDIT", pParms)

        BindGrid()

    End Sub

    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand
        lblmessage.Text = ""
        If e.CommandName = "deleting" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim query As String = "DELETE GPS_LOCATION_SET WHERE LOCATION_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, query)
            lblmessage.Text = "Record deleted successfully."
            BindGrid()
        End If

    End Sub

End Class
