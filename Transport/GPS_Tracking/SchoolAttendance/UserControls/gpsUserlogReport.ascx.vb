﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_SchoolAttendance_UserControls_gpsUserlogReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
            txtdate.Text = Today.ToString("dd/MMM/yyyy")
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnview0)
    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindLoctionAndCardType()

    End Sub

    Public Sub BindLoctionAndCardType()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query As String = " SELECT GPS_UNIT_ID,LOCATION_DES + ' - ( ' + LOCATION_FLOW + ' )' AS LOCATION_DES FROM dbo.GPS_LOCATION_SET  A INNER JOIN dbo.GPS_LOCATION_TYPE B ON A.LOCATION_TYPE_ID=B.LOCATION_TYPE_ID " & _
                              "WHERE BSU_ID='" & ddBsu.SelectedValue & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        ddlocation.DataSource = ds
        ddlocation.DataTextField = "LOCATION_DES"
        ddlocation.DataValueField = "GPS_UNIT_ID"
        ddlocation.DataBind()

        Dim list As New ListItem
        list.Text = "All"
        list.Value = "-1"

        ddlocation.Items.Insert(0, list)

        str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        query = " SELECT  ID_CT_ID, CAT_DES FROM STU_CARD_CATEGORY where BSU_ID= '" & ddBsu.SelectedValue & "' "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        ddcardtype.DataSource = ds
        ddcardtype.DataTextField = "CAT_DES"
        ddcardtype.DataValueField = "ID_CT_ID"
        ddcardtype.DataBind()

        ddcardtype.Items.Insert(0, list)

    End Sub

    Public Sub BindGrid(ByVal export As Boolean)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(11) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", txtdate.Text.Trim())
        If ddBsu.SelectedValue <> "-1" Then
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        End If
        If txtname.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@USR_NAME", txtname.Text.Trim())
        End If
        If ddlocation.SelectedValue <> "-1" Then
            pParms(3) = New SqlClient.SqlParameter("@FK_UnitID", ddlocation.SelectedValue)
        End If
        If txtuserno.Text.Trim() <> "" Then
            pParms(4) = New SqlClient.SqlParameter("@USR_NO", txtuserno.Text.Trim())
        End If
        If ddcardtype.SelectedValue <> "-1" Then
            pParms(5) = New SqlClient.SqlParameter("@CRD_TYPE", ddcardtype.SelectedValue)
        End If
        If ddusertype.SelectedValue <> "-1" Then
            pParms(6) = New SqlClient.SqlParameter("@USR_TYPE", ddusertype.SelectedValue)
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BARCODE_LOG_USER_BSU", pParms)
        GridInfo.DataSource = ds
        GridInfo.DataBind()

        lblmessage0.Text = " : Total Records :  " & ds.Tables(0).Rows.Count

        If export Then
            Dim dt As DataTable = ds.Tables(0)
            dt.Columns.Remove("Profilevis")
            dt.Columns.Remove("N_Visible")
            dt.Columns.Remove("Location1")
            dt.Columns.Remove("Location2")
            ' Create excel file.
            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
            Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
            ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '  ws.HeadersFooters.AlignWithMargins = True
            'Response.ContentType = "application/vnd.ms-excel"
            'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
            'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "Data.xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If

    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        BindGrid(False)
    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindLoctionAndCardType()
    End Sub

    Protected Sub btnview0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview0.Click
        BindGrid(True)
    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid(False)
    End Sub
End Class
