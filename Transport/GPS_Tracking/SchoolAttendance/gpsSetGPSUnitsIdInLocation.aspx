﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsSetGPSUnitsIdInLocation.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_SchoolAttendance_gpsSetGPSUnitsIdInLocation" %>

<%@ Register src="UserControls/gpsSetGPSUnitsIdInLocation.ascx" tagname="gpsSetGPSUnitsIdInLocation" tagprefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Define Scanner Locations
        </div>
        <div class="card-body">
            <div class="table-responsive">

        <uc1:gpsSetGPSUnitsIdInLocation ID="gpsSetGPSUnitsIdInLocation1" 
            runat="server" />

            </div>
        </div>
    </div>

</asp:Content> 

 