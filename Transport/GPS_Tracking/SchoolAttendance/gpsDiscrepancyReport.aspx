﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsDiscrepancyReport.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_SchoolAttendance_gpsDiscrepancyReport" %>

<%@ Register src="UserControls/gpsDiscrepancyReport.ascx" tagname="gpsDiscrepancyReport" tagprefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Student Discrepancy report
        </div>
        <div class="card-body">
            <div class="table-responsive">

  <uc1:gpsDiscrepancyReport ID="gpsDiscrepancyReport1" runat="server" />

                </div>
        </div>
    </div>

</asp:Content> 
 