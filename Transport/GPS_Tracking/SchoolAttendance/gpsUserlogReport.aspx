﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsUserlogReport.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_SchoolAttendance_gpsUserlogReport" %>

<%@ Register src="UserControls/gpsUserlogReport.ascx" tagname="gpsUserlogReport" tagprefix="uc1" %>

 <asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
 
     <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> School Location Scanner Log
        </div>
        <div class="card-body">
            <div class="table-responsive">

  <uc1:gpsUserlogReport ID="gpsUserlogReport1" runat="server" />

            </div>
        </div>
    </div>
 
 </asp:Content> 
    
       
 