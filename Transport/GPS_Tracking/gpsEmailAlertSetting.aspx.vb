﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Partial Class Transport_GPS_Tracking_gpsEmailAlertSetting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "T000516" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
                'Else
                '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            BindBsu()
            BindAlerts()
            BindGrid()
        End If
    End Sub
    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            chkBsu.DataSource = dsBsu
            chkBsu.DataTextField = "BSU_NAME"
            chkBsu.DataValueField = "BSU_ID"
            chkBsu.DataBind()


            Dim list As New ListItem
            list.Text = "All"
            list.Value = "0"
            chkBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            chkBsu.DataSource = dsBsu
            chkBsu.DataTextField = "BSU_NAME"
            chkBsu.DataValueField = "BSU_ID"
            chkBsu.DataBind()
        End If


    End Sub
    Sub BindAlerts()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ALRT.GET_GPSALERTS")

            ddl_alerts.DataSource = ds
            ddl_alerts.DataValueField = "GAS_ID"
            ddl_alerts.DataTextField = "GAS_DES"
            ddl_alerts.DataBind()
            ddl_alerts.Items.Insert(0, New RadComboBoxItem("Select", "0"))



        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim sb As New StringBuilder()

            For Each item As ListItem In chkBsu.Items
                If item.Selected = True Then
                    sb.Append(item.Value)
                    sb.Append("|")
                End If
            Next

            If sb.ToString() <> "" And ddl_alerts.SelectedValue <> "0" Then
                Dim transaction As SqlTransaction
                Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString)
                    conn.Open()
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        Dim pParms(8) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@BSU_IDS", sb.ToString())
                        pParms(1) = New SqlClient.SqlParameter("@GAS_ID", ddl_alerts.SelectedValue)
                        pParms(2) = New SqlClient.SqlParameter("@EMAIL_IDS", txt_EmailIds.Text.Trim())

                        pParms(3) = New SqlClient.SqlParameter("@USER_NAME", Session("sUsr_name"))
                        pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
                        pParms(5) = New SqlClient.SqlParameter("@GEA_ID", IIf(ViewState("HF_GEA_ID") = "", DBNull.Value, ViewState("HF_GEA_ID")))
                        pParms(6) = New SqlClient.SqlParameter("@CONTACT_NO", txt_mobnumbers.Text.Trim())

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[OASIS_GPS].[ALRT].[SAVE_GPS_EMAILALERTS]", pParms)
                        transaction.Commit()
                        lblerror.Text = "Saved Sucessfully."
                        clear()
                        BindGrid()
                    Catch ex As Exception
                        transaction.Rollback()
                        lblerror.Text = "Failed to save."
                    Finally
                        conn.Close()
                    End Try
                End Using
            Else
                lblerror.Text = "Please select school(s)"
            End If
        Catch ex As Exception
            'lblerror.Text = "Failed to save."
        End Try
    End Sub
    Sub clear()
        txt_EmailIds.Text = ""
        txt_mobnumbers.Text = ""
        'chkBsu.ClearCheckedItems()
        chkBsu.Enabled = True
        BindBsu()
        ddl_alerts.ClearSelection()
        ddl_alerts.Enabled = True
        ViewState("HF_GEA_ID") = ""
    End Sub

    Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim ds As DataSet
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ALRT.GETGPS_EMAILALERTS", pParms)
            gv_emailalerts.DataSource = ds
            gv_emailalerts.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gv_emailalerts_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_emailalerts.NeedDataSource
        BindGrid()
    End Sub

    Protected Sub lnk_Edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim HF_GEA_ID As HiddenField = TryCast(sender.parent.FindControl("HF_GEA_ID"), HiddenField)
            Dim HF_GAS_ID As HiddenField = TryCast(sender.parent.FindControl("HF_GAS_ID"), HiddenField)
            Dim HF_BSU_ID As HiddenField = TryCast(sender.parent.FindControl("HF_BSU_ID"), HiddenField)
            Dim HF_GEA_EMAIL_IDS As HiddenField = TryCast(sender.parent.FindControl("HF_GEA_EMAIL_IDS"), HiddenField)

            Dim HF_GEA_CONTACT_NO As HiddenField = TryCast(sender.parent.FindControl("HF_GEA_CONTACT_NO"), HiddenField)

            ViewState("HF_GEA_ID") = HF_GEA_ID.Value

            ddl_alerts.SelectedValue = HF_GAS_ID.Value
            ddl_alerts.Enabled = False
            chkBsu.SelectedValue = HF_BSU_ID.Value
            'Dim item1 As New RadComboBoxItem()
            'item1 = chkBsu.Items.FindItemByValue(HF_BSU_ID.Value)
            'item1.Checked = True
            chkBsu.Enabled = False
            txt_mobnumbers.Text = HF_GEA_CONTACT_NO.Value
            txt_EmailIds.Text = HF_GEA_EMAIL_IDS.Value
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clear()

    End Sub
End Class
