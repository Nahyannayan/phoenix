﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsTripTimeHazardMaster.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsTripTimeHazardMaster" %>

<%@ Register src="UserControls/gpsTripTimeHazardMaster.ascx" tagname="gpsTripTimeHazardMaster" tagprefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Define Trip Time & Hazard Locations
        </div>
        <div class="card-body">
            <div class="table-responsive">

<uc1:gpsTripTimeHazardMaster ID="gpsTripTimeHazardMaster1" runat="server" />

            </div>
        </div>
        </div>
    


</asp:Content> 
 