﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Transport_GPS_Tracking_gpsLCDAlertActions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

            HiddenBsuid.Value = Request.QueryString("bsu")
            Hiddenemp_id.Value = Request.QueryString("emp_id")
            Hiddensearch.Value = Request.QueryString("Actions")
            BindGrid(Hiddensearch.Value)

        End If


    End Sub


    Public Sub BindGrid(ByVal SearchOptions As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(8) As SqlClient.SqlParameter


        If HiddenBsuid.Value <> "900501" Then
            pParms(1) = New SqlClient.SqlParameter("@bsu_id", HiddenBsuid.Value)
        End If

        If SearchOptions <> "" Then

            ''Search Parameters
            Dim val As String() = SearchOptions.Split("$")
            Dim dateselected = val(0).Trim()
            Dim maxspeedFrom = val(1).Trim()
            Dim maxspeedTo = val(2).Trim()
            Dim panicalert = val(3).Trim()
            Dim geoalert = val(4).Trim()

            pParms(2) = New SqlClient.SqlParameter("@date", dateselected)
            pParms(3) = New SqlClient.SqlParameter("@MaxSpeedFrom", maxspeedFrom)
            pParms(4) = New SqlClient.SqlParameter("@MaxSpeedTo", maxspeedTo)

            If panicalert = "true" Then
                pParms(5) = New SqlClient.SqlParameter("@Panic_Alert", panicalert)
            End If

            If geoalert = "true" Then
                pParms(6) = New SqlClient.SqlParameter("@GeoFenceAlert", geoalert)
            End If


        End If

        pParms(7) = New SqlClient.SqlParameter("@option", 2)


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_LCD_DISPLAY", pParms)


        GridBusListing.DataSource = ds
        GridBusListing.DataBind()


    End Sub



    Protected Sub GridBusListing_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridBusListing.PageIndexChanging
        GridBusListing.PageIndex = e.NewPageIndex
        BindGrid(Hiddensearch.Value)
    End Sub

    Protected Sub GridBusListing_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridBusListing.RowCommand

        If e.CommandName = "actions" Then
            lblmessage.Text = ""
            MO1.Show()
        End If

    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        For Each row As GridViewRow In GridBusListing.Rows
            Dim check As CheckBox = DirectCast(row.FindControl("CheckAction"), CheckBox)
            If check.Checked Then

                Dim logid = DirectCast(row.FindControl("HiddenLogid"), HiddenField).Value

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@LCD_RECORD_ID", logid)
                pParms(1) = New SqlClient.SqlParameter("@REMARKS", txtremarks.Text.Trim())
                pParms(2) = New SqlClient.SqlParameter("@TYPE", ddactiontype.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@ACTION_BY", Hiddenemp_id.Value)
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_ACTION_LOG", pParms)

            End If

        Next

        MO1.Show()

    End Sub
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid(Hiddensearch.Value)
        MO1.Hide()
    End Sub
End Class
