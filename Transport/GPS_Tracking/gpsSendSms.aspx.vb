﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO


Partial Class Transport_GPS_Tracking_gpsSendSms
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
            BindMobileNo()
            usrStudentdetails1.StudentDetails_ID = Request.QueryString("stu_no")
            BindText()

        End If

    End Sub

    Public Sub BindMobileNo()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", Request.QueryString("stu_id"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            txtmobile.Text = ds.Tables(0).Rows(0).Item("STU_EMGCONTACT").ToString()
        End If



    End Sub


    Public Function AreaLocation(ByVal pk_id As String) As String
        Dim ReturnValue As String = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 1)
        pParms(1) = New SqlClient.SqlParameter("@PK_ID", pk_id)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)
        If ds.Tables(0).Rows.Count > 0 Then

            Dim lat = ds.Tables(0).Rows(0).Item("Latitude").ToString()
            Dim lng = ds.Tables(0).Rows(0).Item("Longitude").ToString()
            Try
                Dim ServicePath = "http://ws.geonames.org/findNearbyPlaceName?lat=" & lat & "&lng=" & lng & ""
                Dim myWebClient As New System.Net.WebClient
                Dim responseArray As Byte() = myWebClient.DownloadData(ServicePath)
                ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)
                Dim startindex = ReturnValue.IndexOf("<name>")
                Dim endindex = ReturnValue.IndexOf("</name>")
                ReturnValue = ReturnValue.Substring(startindex + 6, (endindex - (startindex + 6)))

            Catch ex As Exception
                ReturnValue = "Service not available"
            End Try

        Else
            ReturnValue = "Record not found.."
        End If



        Return ReturnValue.Replace("?", "")
    End Function

    Public Sub BindText()
        Dim type = Request.QueryString("type")

        If type = "LOCATION" Then
            txtmessage.Text = "Your child's current location of the bus : " & AreaLocation(Request.QueryString("Pk_id"))
        End If
        If type = "ABSENT" Then
            txtmessage.Text = "This is for your kind information that your child has been absent for the day :" & Request.QueryString("adate")
        End If



    End Sub

    Protected Sub btnsend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsend.Click

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(7) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OPTION ", 1)
            pParms(1) = New SqlClient.SqlParameter("@MEM_TYPE", Request.QueryString("mem_type"))
            pParms(2) = New SqlClient.SqlParameter("@USER_ID", Request.QueryString("stu_id"))
            pParms(3) = New SqlClient.SqlParameter("@ACTION_TYPE", Request.QueryString("type"))
            pParms(4) = New SqlClient.SqlParameter("@MESSAGE", txtmessage.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@MOBILE_NO", txtmobile.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@SENT_BY_ID", Session("EmployeeId"))

            Dim RECORD_ID As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_INSERT_MESSAGE_LOG", pParms)

            If RECORD_ID.Trim() <> "" Then
                Dim usrname = WebConfigurationManager.AppSettings("smsUsername")
                Dim password = WebConfigurationManager.AppSettings("smspwd")

                Dim servicevalue = SmsService.sms.SendMessage(txtmobile.Text, txtmessage.Text, "STS-ADMIN", usrname, password)

                pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
                pParms(1) = New SqlClient.SqlParameter("@STATUS", servicevalue)
                pParms(2) = New SqlClient.SqlParameter("@RECORD_ID", RECORD_ID.Trim())
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_INSERT_MESSAGE_LOG", pParms)

            Else
                lblmessage.Text = "Error while saving record.Please try again later.."
            End If

        Catch ex As Exception
            lblmessage.Text = "Error :" & ex.Message
        End Try



    End Sub


End Class
