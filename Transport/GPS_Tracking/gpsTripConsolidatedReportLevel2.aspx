﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsTripConsolidatedReportLevel2.aspx.vb" Inherits="Transport_GPS_Tracking_gpsTripConsolidatedReportLevel2" %>

<%@ Register src="UserControls/gpsTripConsolidatedReportLevel2.ascx" tagname="gpsTripConsolidatedReportLevel2" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>List of Buses</title>
    <base target="_self" /> 
     <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:gpsTripConsolidatedReportLevel2 ID="gpsTripConsolidatedReportLevel21" 
            runat="server" />
    
    </div>
    </form>
</body>
</html>
