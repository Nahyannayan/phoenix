﻿// JScript File
//<![CDATA[
function openPopUp(s,lat,lng)
{
window.showModalDialog('../gpsBusLocationDetails.aspx?BusInfo=' + s +'&lat='+ lat +'&lng='+ lng , '','dialogHeight:1024px;dialogWidth:800px;scroll:no;resizable:no;'); return false;
}

var map, actual;
var timeout;
var XMLHTTPRequestObject =false;
var bit =0;
var geocoder;
var address;

 if(window.ActiveXObject)
    {
    XMLHTTPRequestObject =new ActiveXObject('Microsoft.XMLHTTP');
    }


function createMarker(point, icon, soundnr,htmlcontent) 
{

 //Set draggable markers
 var marker = new GMarker(point, {icon:icon, draggable:false, bouncy:false, dragCrossMove:false});
 marker.content = soundnr;

         GEvent.addListener(marker, "dragstart", function() 
         {
          //Close infowindow when dragging a marker
          map.closeInfoWindow();
         }
         );

         GEvent.addListener(marker, "mouseover", function() 
                     {
                   
                      // Make the variable html a bit more flexible 
                      var html = makeHTML(htmlcontent);
                      // Open infowindow only if it's closed or the mouse moves over another marker
                      if(map.getInfoWindow().isHidden() || actual != marker ) 
                      {   
                        marker.openInfoWindowHtml(html);
                      } 
                     
                      actual = marker;
                     }
         );

 return marker;
}

function makeHTML(htmlcontent) 
{
 var html ="<div class='infowindow'>";
 html += htmlcontent + "</div>";
 return html;
}


  
function buildMap() 
{

  if (timeout != null)
  {
  //Clear the previous timeout
  window.clearTimeout(timeout)
  }

 
 if(GBrowserIsCompatible()) {
    
         // Find the Map Div
         map=new GMap2(document.getElementById("map"),{draggableCursor:'auto',draggingCursor:'move'});
            
         GEvent.addListener(map, "click", getAddress);
         geocoder = new GClientGeocoder();
         //Installs keyboard event handler for the map passed as argument
         new GKeyboardHandler(map); 
     
         //Setting Zoom Level and Map Type
         var zoom = parseInt(window.document.getElementById("HiddenZoomLevel").value) 
       
         var lat =window.document.getElementById("HiddenLat").value
         var log =window.document.getElementById("Hiddenlog").value
      
         map.setCenter(new GLatLng(lat,log),zoom, G_PHYSICAL_MAP);
         
         var mapControl = new GMapTypeControl();
         map.addControl(mapControl);
         
         //Adding Controls.
         map.addControl(new GLargeMapControl ());
         
    var DDATE =window.document.getElementById("DDATE").value
    var FHRS=window.document.getElementById("FHRS").value
    var FMINS=window.document.getElementById("FMINS").value
    var THRS=window.document.getElementById("THRS").value
    var TMINS=window.document.getElementById("TMINS").value



    GDownloadUrl("../Xml/data.xml", function(data) {

        var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");

        var datasource = '../WebServices/gpsGenerateXml.asmx/GetPathHistory?UnitId=' + window.document.getElementById("HiddenUnitId").value + '&DDATE=' + DDATE + '&FHRS=' + FHRS + '&FMINS=' + FMINS + '&THRS=' + THRS + '&TMINS=' + TMINS

        XMLHTTPRequestObject.open('GET', datasource, false);
        XMLHTTPRequestObject.onreadystatechange = function() {

            if (XMLHTTPRequestObject.readystate == 4 && XMLHTTPRequestObject.status == 200) {
                var xmlDoc = XMLHTTPRequestObject.responseXML;
                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDocc.load(xmlDoc);
                xmlObj = xmlDocc.documentElement

            }
            else {
                //alert(XMLHTTPRequestObject.status)
            }
        }

        XMLHTTPRequestObject.send(null);


        var xml = GXml.parse(xmlObj.xml);

        var markers = xml.documentElement.getElementsByTagName("marker");

        var points = [];
        var Buspoint;
        var SearchDate;
        var startflat = 0;
        var StartPoint;
        var DirectionFlag = 0;

        for (var k = 0; k < markers.length; k++) {

            if (startflat == 0) {
                StartPoint = k;
                startflat = 1;
            }
            Buspoint = k
            points[k] = new GLatLng(parseFloat(markers[k].getAttribute("lat")), parseFloat(markers[k].getAttribute("lng")));

            // Direction Start
            if (window.document.getElementById("HiddenDirections").value == 'True') {
                DirectionFlag = DirectionFlag + 1;

                if (DirectionFlag == 2) {
                    DirectionFlag = 0;
                    var direction = markers[k].getAttribute("Directions")
                    var tinyIcon = new GIcon();

                    if ((direction > 330 && direction <= 360) || (direction >= 0 && direction < 30)) //North
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_N_s6.gif";
                    }
                    if (direction > 60 && direction < 120) //West
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_W_s6.gif";
                    }
                    if (direction > 150 && direction < 210) //South
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_S_s6.gif";
                    }
                    if (direction > 240 && direction < 300) //East
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_E_s6.gif";
                    }
                    if (direction >= 30 && direction <= 60) //North East
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_NE_s6.gif";

                    }
                    if (direction >= 120 && direction <= 150) //South East
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_SE_s6.gif";
                    }
                    if (direction >= 210 && direction <= 240) //South West
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_SW_s6.gif";

                    }
                    if (direction >= 300 && direction <= 330) //North West
                    {
                        tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/Directions/dir_NW_s6.gif";
                    }
                    tinyIcon.iconSize = new GSize(12, 20);
                    tinyIcon.shadowSize = new GSize(22, 20);
                    tinyIcon.iconAnchor = new GPoint(6, 20);
                    tinyIcon.infoWindowAnchor = new GPoint(5, 1);
                    // Set up our GMarkerOptions object literal
                    markerOptions = { icon: tinyIcon };
                    var Dpoint = new GLatLng(parseFloat(markers[k].getAttribute("lat")), parseFloat(markers[k].getAttribute("lng")));
                    map.addOverlay(new GMarker(Dpoint, markerOptions));

                }

            }
            // Direction End

        }

        // Draw polylines between marker points
        var colr = "#FF0000"
        if (window.document.getElementById("HiddenGeoFence").value == 'True') {
            colr = '#0000FF'
        }
        var poly = new GPolyline(points, colr , 5, .5);
        map.addOverlay(poly);

        //Show the Start Image 
        var Sicon = new GIcon();
        Sicon.image = "https://school.gemsoasis.com/images/GPS_Images/startind.png";
        addIcon(Sicon);
        var Spoint = new GLatLng(parseFloat(markers[StartPoint].getAttribute("lat")), parseFloat(markers[StartPoint].getAttribute("lng")));
        markerOptions = { icon: Sicon };
        //var Shtml = markers[StartPoint].getAttribute("info")                                       
        //map.addOverlay(createMarker(Spoint, Sicon, "Map",Shtml));
        map.addOverlay(new GMarker(Spoint, markerOptions));

        //Show the Bus Image 
        var icon = new GIcon();
        icon.image = "https://school.gemsoasis.com/images/GPS_Images/bus.gif";
        addIcon(icon);
        var point = new GLatLng(parseFloat(markers[Buspoint].getAttribute("lat")), parseFloat(markers[Buspoint].getAttribute("lng")));
        var html = markers[Buspoint].getAttribute("info")
        map.addOverlay(createMarker(point, icon, "Map", html));
        ShowGeofence(map);
    }

                        );

                                                         
  }
 
}

function getAddress(overlay, latlng) 
 {
      if (latlng != null) 
      {
        address = latlng;
        geocoder.getLocations(latlng, showAddress);
      }
 }
 
 function showAddress(response) 
 {
      if (!response || response.Status.code != 200) 
      {
        alert("Status Code:" + response.Status.code);
      } 
      else 
      {
        place = response.Placemark[0];
        point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
        //        marker = new GMarker(point);
        //        map.addOverlay(marker);
        //        marker.openInfoWindowHtml(
        //        '<b>orig latlng:</b>' + response.name + '<br/>' + 
        //        '<b>latlng:</b>' + place.Point.coordinates[0] + "," + place.Point.coordinates[1] + '<br>' +
        //        '<b>Status Code:</b>' + response.Status.code + '<br>' +
        //        '<b>Status Request:</b>' + response.Status.request + '<br>' +
        //        '<b>Address:</b>' + place.address + '<br>' +
        //        '<b>Accuracy:</b>' + place.AddressDetails.Accuracy + '<br>' +
        //        '<b>Country code:</b> ' + place.AddressDetails.Country.CountryNameCode);

        var val=
        '<b>Address:</b>' + place.address + '<br>' +
        '<b>Country code:</b> ' + place.AddressDetails.Country.CountryNameCode 
        var obj = document.getElementById('Info');
        obj.innerHTML=val
      }
 }

function addIcon(icon) 
{ 
 // Add icon attributes
 icon.shadow= "https://school.gemsoasis.com/images/GPS_Images/mm_20_shadow.png";
 icon.iconSize = new GSize(30, 30);
 icon.shadowSize = new GSize(25, 25);
 icon.iconAnchor = new GPoint(6, 20);
 icon.infoWindowAnchor = new GPoint(5, 1);
}


//]]>

//Javascript Error Handling

function handleError() 
{

	return true;
}
window.onerror = handleError;


function ShowGeofence(map) {

    if (window.document.getElementById("HiddenGeoFence").value == 'True') {
       
        SetGeoFence(map)
    }


}



function SetGeoFence(map) {
//alert('Vehicle Track with GeoFence')
    GDownloadUrl("../Xml/data.xml", function(data) {

        var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        var datasource = '../WebServices/gpsGenerateXml.asmx/GetGeoFencingPoints?UnitId=' + window.document.getElementById("HiddenUnitId").value

        XMLHTTPRequestObject.open('GET', datasource, false);
        XMLHTTPRequestObject.onreadystatechange = function() {

            if (XMLHTTPRequestObject.readystate == 4 && XMLHTTPRequestObject.status == 200) {
                var xmlDoc = XMLHTTPRequestObject.responseXML;
                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDocc.load(xmlDoc);
                xmlObj = xmlDocc.documentElement
            }
            else {
                //alert(XMLHTTPRequestObject.status)
            }
        }

        XMLHTTPRequestObject.send(null);


        var xml = GXml.parse(xmlObj.xml);
        var markers = xml.documentElement.getElementsByTagName("marker");

        var points = [];
        //Clear the over lay before plotting points
        //map.clearOverlays();
        for (var k = 0; k < markers.length - 1; k++) {

            points[k] = new GLatLng(parseFloat(markers[k].getAttribute("lat")), parseFloat(markers[k].getAttribute("lng")));

        }

        var polygon = new GPolygon(points, '#FF0000', 0.1, 0.4, '#FF0000', 0.4);
        map.addOverlay(polygon);
        

    }

                        );



}