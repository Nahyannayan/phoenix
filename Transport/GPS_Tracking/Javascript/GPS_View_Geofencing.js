﻿// JScript File
//<![CDATA[

var map, actual;
var timeout;
var XMLHTTPRequestObject =false;
var bit =0;

 if(window.ActiveXObject)
    {
    XMLHTTPRequestObject =new ActiveXObject('Microsoft.XMLHTTP');
    }

  
function buildMap() 
{

  if (timeout != null)
  {
  //Clear the previous timeout
  window.clearTimeout(timeout)
  }
 
 if(GBrowserIsCompatible()) 
 {
 
         if (bit==0)
         {
          // Find the Map Div
          map=new GMap2(document.getElementById("map"),{draggableCursor:'auto',draggingCursor:'move'});
          
          bit =1
         }
         else
         {
        
          var center = map.getCenter();
          window.document.getElementById("HiddenLat").value=center.lat()
          window.document.getElementById("Hiddenlog").value=center.lng()
          window.document.getElementById("HiddenZoomLevel").value=map.getZoom()
         }
                
         //Installs keyboard event handler for the map passed as argument
         new GKeyboardHandler(map); 
     
         //Get Zoom Level,latlng informations
          var zoom = parseInt(window.document.getElementById("HiddenZoomLevel").value) 
          var lat =window.document.getElementById("HiddenLat").value
          var log =window.document.getElementById("Hiddenlog").value
          map.setCenter(new GLatLng(lat,log),zoom, G_PHYSICAL_MAP)
          
         //Adding Controls.
         map.addControl(new GLargeMapControl ());
     

          GDownloadUrl("../Xml/data.xml", function(data) 
                            { 
                      
                                        var xmlDoc = new ActiveXObject("Microsoft.XMLDOM"); 
                                        var datasource ='../WebServices/gpsGenerateXml.asmx/GetGeoFencingPoints?UnitId='+ window.document.getElementById("HiddenUnitId").value                                       
                                      
                                        XMLHTTPRequestObject.open('GET',datasource,false);
                                        XMLHTTPRequestObject.onreadystatechange =function()
                                        {
                                       
                                                if(XMLHTTPRequestObject.readystate==4 && XMLHTTPRequestObject.status==200)
                                                {
                                                var xmlDoc =XMLHTTPRequestObject.responseXML;
                                                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM"); 
                                                xmlDocc.load(xmlDoc);  
                                                xmlObj=xmlDocc.documentElement
                                                }
                                                else
                                                {
                                                alert(XMLHTTPRequestObject.status)
                                                }
                                        } 
                                       
                                       XMLHTTPRequestObject.send(null);
                                     

                                        var xml = GXml.parse(xmlObj.xml);
                                        var markers = xml.documentElement.getElementsByTagName("marker");
                                         
                                        var points = [];
                                        //Clear the over lay before plotting points
                                        map.clearOverlays();   
                                                      for (var k=0;k<markers.length-1;k++)
                                                      {   

                                                            points[k] = new GLatLng(parseFloat(markers[k].getAttribute("lat")), parseFloat(markers[k].getAttribute("lng")));
                                                        
                                                      }
                                                    
                                                     var polygon = new GPolygon(points,'#FF0000',0.1,0.4,'#FF0000',0.4);
                                                     map.addOverlay(polygon);
                                                     
                                                 var tinyIcon = new GIcon();  
                                                 if (markers[k].getAttribute("Status")=='True')  
                                                 {
                                                 tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/startind.png";    
                                                 }
                                                 else
                                                 {
                                                  tinyIcon.image = "https://school.gemsoasis.com/images/GPS_Images/icon_alert.gif";    
                                          
                                                 }    
                                                 tinyIcon.iconSize = new GSize(30, 30);
                                                 tinyIcon.shadowSize = new GSize(22, 20);
                                                 tinyIcon.iconAnchor = new GPoint(6, 20);
                                                 tinyIcon.infoWindowAnchor = new GPoint(5, 1);

                                                 // Set up our GMarkerOptions object literal
                                                 markerOptions = { icon:tinyIcon };
                                                 var Dpoint = new GLatLng(parseFloat(markers[k].getAttribute("lat")), parseFloat(markers[k].getAttribute("lng")));
                                                 map.addOverlay(new GMarker(Dpoint,markerOptions));     
                                                     
                                                     
                                                     
                                                     

                            }
                             
                        );
                        
                                          
  }

  timeout =  window.setTimeout('buildMap();', 20000); //20 sec

}

function addIcon(icon) 
{ 
 // Add icon attributes
 icon.shadow= "https://school.gemsoasis.com/images/GPS_Images/mm_20_shadow.png";
 icon.iconSize = new GSize(30, 30);
 icon.shadowSize = new GSize(25, 25);
 icon.iconAnchor = new GPoint(6, 20);
 icon.infoWindowAnchor = new GPoint(5, 1);
}

//]]>

//Javascript Error Handling

function handleError() 
{

	return true;
}
window.onerror = handleError;
