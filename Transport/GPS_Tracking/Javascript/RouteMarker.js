// This Marker is a scalable Polyline overlay that shows a route on the map
// Bill Chadwick

function RouteMarker(points, color, opacity, widthKm) {
  this.points_ = points;
  this.color_ = color || "#888888";
  this.opacity_ = opacity || 0.5;
  this.widthKm_ = widthKm || 1.0;
  this.polyLine_ = null;
}

RouteMarker.prototype = new GOverlay();

RouteMarker.prototype.initialize = function(map) {
  //save for later
  this.map_ = map;
}

// Remove the line from the map pane
RouteMarker.prototype.remove = function() {
  if(this.polyLine_ != null){
	this.map_.removeOverlay(this.polyLine_);
      this.polyLine_ = null;
  }
}

// Copy our data to a new RouteMarker
RouteMarker.prototype.copy = function() {
  return new RouteMarker(this.points_, this.color_, this.opacity_, this.widthKm_);
}

// Redraw the line based on the current projection and zoom level
RouteMarker.prototype.redraw = function(force) {

  this.remove();
  //find pixels perkm
  var sz = this.map_.getSize();
  var bnds = this.map_.getBounds();
  var pxDiag = Math.sqrt((sz.width*sz.width) + (sz.height*sz.height));
  var mDiagKm = bnds.getNorthEast().distanceFrom(bnds.getSouthWest()) / 1000.0;
  var pxPerKm = pxDiag/mDiagKm;
  var wPx = Math.round(this.widthKm_ * pxPerKm);
  if(this.widthKm_ > mDiagKm){
	return;//dont plot when width of route more than diagonal of map
	}
  this.polyLine_ = new GPolyline(this.points_,this.color_,wPx ,this.opacity_);
  this.map_.addOverlay(this.polyLine_);
}





