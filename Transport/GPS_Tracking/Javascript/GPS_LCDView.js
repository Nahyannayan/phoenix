﻿// JScript File

var XMLHTTPRequestObject = false;
var timeout;
var page = 0;
var PanicRegNo = '';
var showNewPAlert = '';
var playPSound = 0;
var MaxSpeedRegNo = '';
var showSpeedAlert = '';


//    if(window.XMLHttpRequest)
//    {
//    XMLHTTPRequestObject =new XMLHTTPRequest();
//    }

//if(window.ActiveXObject)
//{
//XMLHTTPRequestObject =new ActiveXObject('Microsoft.XMLHTTP');
//}

if (window.XMLHttpRequest) {
    // code for modern browsers
    XMLHTTPRequestObject = new XMLHttpRequest();

} else {
    // code for IE6, IE5
    XMLHTTPRequestObject = new ActiveXObject("Microsoft.XMLHTTP");

}

function onloadFunction() {
    var gridBusListing = document.getElementById('<%=GridBusListing.ClientID%>');
    //alert(gridBusListing);
    //var GridObject =document.getElementById('GridBusListing').childNodes[0];
    var GridObject = document.getElementById('GridBusListing').rows;
  

    for (var i = 1; i <= GridObject.length; i++) {
        var image;
        if (i < 10) {
            image = 'GridBusListing_ctl0' + i
        }
        if (i == 10) {
            image = 'GridBusListing_ctl10';
        }
        if (i == 11) {
            image = 'GridBusListing_ctl11';
        }


        var image2 = image + '_ImageSpeedAlert';
        var image3 = image + '_ImagePanicAlert';
        var image4 = image + '_ImageGeoAlert';
        //alert(image3 + '  --->  ' + document.getElementById(image3))
        if (document.getElementById(image2) != null) {
            document.getElementById(image2).style.visibility = "hidden";
        }
        if (document.getElementById(image3) != null) {
            document.getElementById(image3).style.visibility = "hidden";
        }
        if (document.getElementById(image4) != null) {
            document.getElementById(image4).style.visibility = "hidden";
        }


        if (i < 11) {

            //GridObject.childNodes[i].childNodes[0].innerHTML=''
            //GridObject.childNodes[i].childNodes[1].innerHTML=''
            //GridObject.childNodes[i].childNodes[2].innerHTML=''
            //GridObject.childNodes[i].childNodes[3].innerHTML=''
            //GridObject.childNodes[i].childNodes[4].innerHTML=''
            //GridObject.childNodes[i].childNodes[5].innerHTML=''
            //GridObject.childNodes[i].childNodes[6].innerHTML=''
            //GridObject.childNodes[i].childNodes[7].innerHTML=''
            //GridObject.childNodes[i].childNodes[8].innerHTML=''
            //GridObject.childNodes[i].childNodes[9].innerHTML=''
            GridObject[i].childNodes[0].innerHTML = ''
            GridObject[i].childNodes[1].innerHTML = ''
            GridObject[i].childNodes[2].innerHTML = ''
            GridObject[i].childNodes[3].innerHTML = ''
            GridObject[i].childNodes[4].innerHTML = ''
            GridObject[i].childNodes[5].innerHTML = ''
            GridObject[i].childNodes[6].innerHTML = ''
            GridObject[i].childNodes[7].innerHTML = ''
            GridObject[i].childNodes[8].innerHTML = ''
            GridObject[i].childNodes[9].innerHTML = ''

        }

    }

    if (document.getElementById('HiddenStartFlag').value == 0) {

        BindGrid()
    }
}


//Javascript Error Handling

function handleError() {

    return true;
}
window.onerror = handleError;

function Prevpage() {
    var Tpage = ''
    document.getElementById('HiddenStartFlag').value = 0;
    if (page > 0) {
        Tpage = (document.getElementById('txtpaging').value) - (2)
        document.getElementById('HiddenPaging').value = (Tpage) * (10)
        page = ((Tpage) * (10)) / 10

        document.getElementById('txtpaging').value = page
        BindGrid()
    }
    else {
        BindGrid()
    }

}

function Nextpage() {
    var Tpage = ''
    document.getElementById('HiddenStartFlag').value = 0;
    if (page > 0) {
        Tpage = document.getElementById('txtpaging').value
        document.getElementById('HiddenPaging').value = (Tpage) * (10)
        BindGrid()
        page = (document.getElementById('HiddenPaging').value) / 10
        document.getElementById('txtpaging').value = page

    }
    else {
        BindGrid()
    }

}

function BindGrid() {
    if (document.getElementById("CheckPause").checked == '0') {

      

        if (timeout != null) {

            window.clearTimeout(timeout)
        }
        var paging = document.getElementById('HiddenPaging').value
        var bsu_id = document.getElementById('Hiddenbsuid').value
        var searchvalues = document.getElementById('HiddenSearchOptions').value;

        playPSound = 0;

        document.getElementById('HiddenStartFlag').value = 1;
        var datasource = 'https://school.gemsoasis.com/Transport/GPS_Tracking/Webservices/gpsLCDviewJavascript.asmx/GpsLcdDisplay?paging=' + paging + '&bsu_id=' + bsu_id + '&SearchOptions=' + searchvalues
        var GridObject = document.getElementById('GridBusListing').rows;

        XMLHTTPRequestObject.open('GET', datasource, true);
        XMLHTTPRequestObject.onreadystatechange = function () {
            if (XMLHTTPRequestObject.readyState == 4 && XMLHTTPRequestObject.status == 200) {

                var xmlDoc = XMLHTTPRequestObject.responseXML;
                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDocc.load(xmlDoc);
                xmlObj = xmlDocc.documentElement

                if (xmlObj.childNodes.length == 0) {
                    document.getElementById('HiddenPaging').value = 0;

                    page = 0
                }
                else {
                    //Clear before inserting
                    onloadFunction()
                    document.getElementById('txtpaging').value = parseInt(document.getElementById('HiddenPaging').value);
                    document.getElementById('HiddenPaging').value = parseInt(document.getElementById('HiddenPaging').value) + (10) //xmlObj.childNodes.item((xmlObj.childNodes.length-1)).childNodes.item(0).text  // //parseInt(document.getElementById('HiddenPaging').value) + (1);

                    page = page + (1)
                    document.getElementById('txtpaging').value = page;
                }



                for (var i = 0; i < xmlObj.childNodes.length; i++) {

                    var VEH_ID = xmlObj.childNodes.item(i).childNodes.item(0).text;

                    var VEH_REGNO = xmlObj.childNodes.item(i).childNodes.item(5).text + '<br>(' + xmlObj.childNodes.item(i).childNodes.item(20).text + ')';

                    var CAT_DESCRIPTION = xmlObj.childNodes.item(i).childNodes.item(3).text;

                    var BSU_SHORTNAME = xmlObj.childNodes.item(i).childNodes.item(2).text

                    var Driver = xmlObj.childNodes.item(i).childNodes.item(7).text + '<br>' + xmlObj.childNodes.item(i).childNodes.item(8).text;

                    var Conductor = xmlObj.childNodes.item(i).childNodes.item(9).text + '<br>' + xmlObj.childNodes.item(i).childNodes.item(10).text;

                    var SPEED = xmlObj.childNodes.item(i).childNodes.item(11).text

                    var PANIC_ALERT_TIME = xmlObj.childNodes.item(i).childNodes.item(13).text

                    var PANIC_ALERT = xmlObj.childNodes.item(i).childNodes.item(14).text

                    var maxspeed = xmlObj.childNodes.item(i).childNodes.item(15).text

                    var maxspeedtime = xmlObj.childNodes.item(i).childNodes.item(18).text

                    var geoalert = xmlObj.childNodes.item(i).childNodes.item(16).text

                    var Km = xmlObj.childNodes.item(i).childNodes.item(17).text

                    var max_speed_alert = xmlObj.childNodes.item(i).childNodes.item(19).text

                    //Display Image Alerts
                    var image;
                    if (i < 8) {
                        image = 'GridBusListing_ctl0' + (i + 2)
                    }
                    else {
                        image = 'GridBusListing_ctl' + (i + 2);
                    }


                    var image2 = image + '_ImageSpeedAlert';
                    var image3 = image + '_ImagePanicAlert';
                    var image4 = image + '_ImageGeoAlert';
                    //alert(image3 + '  --->  ' + document.getElementById(image3))
                    //alert(maxspeed)
                    //alert(max_speed_alert)
                    if (max_speed_alert == 'True') {
                        if (document.getElementById(image2) != null) {
                            document.getElementById(image2).style.visibility = 'visible';
                        }
                    }

                    if (PANIC_ALERT != '--') {
                        if (document.getElementById(image3) != null) {
                            document.getElementById(image3).style.visibility = 'visible';
                        }
                    }
                    if (geoalert == '0') {
                        if (document.getElementById(image4) != null) {
                            document.getElementById(image4).style.visibility = 'visible';
                        }
                    }


                    //Accumilate Veh Reg no If speed
                    showSpeedAlert = '';
                    if (max_speed_alert == 'True') {

                        if (MaxSpeedRegNo.indexOf(VEH_REGNO + maxspeedtime) < 0) {
                            MaxSpeedRegNo = MaxSpeedRegNo + ',' + VEH_REGNO + maxspeedtime
                            showSpeedAlert = '-(New)';
                            playPSound = 2;
                        }

                    }

                    //Accumilate Veh Reg no If Panic
                    showNewPAlert = '';
                    if (PANIC_ALERT != '--') {

                        if (PanicRegNo.indexOf(VEH_REGNO + PANIC_ALERT_TIME) < 0) {

                            PanicRegNo = PanicRegNo + ',' + VEH_REGNO + PANIC_ALERT_TIME
                            showNewPAlert = '-(New)';
                            playPSound = 1;
                        }

                    }




                    GridObject[i + 1].childNodes[0].innerHTML = VEH_ID
                    GridObject[i + 1].childNodes[1].innerHTML = VEH_REGNO
                    GridObject[i + 1].childNodes[2].innerHTML = CAT_DESCRIPTION
                    GridObject[i + 1].childNodes[3].innerHTML = BSU_SHORTNAME
                    GridObject[i + 1].childNodes[4].innerHTML = Driver
                    GridObject[i + 1].childNodes[5].innerHTML = Conductor
                    GridObject[i + 1].childNodes[6].innerHTML = Km
                    GridObject[i + 1].childNodes[7].innerHTML = SPEED
                    if (max_speed_alert == 'True') {
                        GridObject[i + 1].childNodes[8].innerHTML = maxspeed + maxspeedtime + showSpeedAlert
                    }
                    GridObject[i + 1].childNodes[9].innerHTML = PANIC_ALERT_TIME + showNewPAlert


                }

                if (playPSound == 1) {
                    //Play sound per page
                    var thissound = eval("document.sound1");
                    thissound.Play();
                }
                if (playPSound == 2) {
                    //Play sound per page
                    var thissound = eval("document.sound2");
                    thissound.Play();
                }

            }
            else {

                if (XMLHTTPRequestObject.status == 503 || XMLHTTPRequestObject.status == 504) {
                    BindGrid()

                }

            }
        }

        XMLHTTPRequestObject.send(null);
        timeout = window.setTimeout('BindGrid();', 20000); // 20 sec
        return false;
    }

}


function moveent() {
    Nextpage()
}




function search() {

    var searchvalue = window.document.getElementById("txtDate").value;
    if (searchvalue != "") {
        var sfrom = window.document.getElementById("DropMaxSpeedFrom").value;
        var sto = window.document.getElementById("DropMaxSpeedTo").value;

        if (parseInt(sfrom) <= parseInt(sto)) {
            searchvalue = searchvalue + "$" + sfrom;
            searchvalue = searchvalue + "$" + sto;
            searchvalue = searchvalue + "$" + window.document.getElementById("CheckPanic").checked;
            searchvalue = searchvalue + "$" + window.document.getElementById("CheckGeo").checked;
            window.document.getElementById("HiddenSearchOptions").value = searchvalue;
            CloseExpand(window.document.getElementById("img1"));
            //Call the log
            document.getElementById('HiddenPaging').value = 0;
            page = 0;
            document.getElementById('HiddenStartFlag').value = 0;
            onloadFunction()

        }
        else {
            alert("Please Enter Correct Max Speed Range")
        }
    }
    else {
        alert("Please Enter Date")
    }
}

function CloseExpand(imgobj) {
    var imgpath = imgobj.src;
    if (imgpath.indexOf("expand") > -1) {

        imgobj.src = "../../Images/GPS_Images/Close.png"
        document.getElementById("Tab1").style.visibility = 'visible'
    }
    else {
        imgobj.src = "../../Images/GPS_Images/expand.png"
        document.getElementById("Tab1").style.visibility = 'hidden'
    }


}