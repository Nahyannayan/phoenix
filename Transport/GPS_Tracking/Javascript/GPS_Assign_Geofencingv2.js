﻿// JScript File
if (window.ActiveXObject) {
    XMLHTTPRequestObject = new ActiveXObject('Microsoft.XMLHTTP');
}


var map;
var polyShape;
var polyShape2;
var polygonMode = null;
var circlemode = null;
var headfoot = true;
var mylistener;
var editlistener = null;
var polygonDepth = "0";
var polyPoints = new Array();
var polyPointsnew = new Array();
var holePoints = new Array();
var marker;
var geocoder = null;
var holemode = null;
//var colormode = null;

var fillColor = "#FF0000"; // red fill
var fillColorcur = "#FF0000";

var lineColor = "#000000";  // red line
var lineColorcur = "#000000";

var polygonlineColor = "#FF0000";
var polygonlineColorcur = "#FF0000";

var lineColor2 = "#000000"; // black line
var lineopacity = .4;
var lineopacitycur = .4;
var fillopacity = .4;
var fillopacitycur = .4;


var lineWeight = 8;
var lineWeightcur = 8;
var polygonLineWeight = 0.1; //not used
var polygonLineWeightcur = 0.1;



var centerMarker = null;
var radiusMarker = null;

var points = [];
var gmarkers = [];




function load() {
    if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map"));
        map.setCenter(new GLatLng(25.151188, 55.22558), 14);
        map.addControl(new GLargeMapControl());
        map.addControl(new GMapTypeControl());
        map.addMapType(G_PHYSICAL_MAP);
        map.enableScrollWheelZoom();
        var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(10, 40));
        map.addControl(new GScaleControl(), pos);
        mylistener = GEvent.addListener(map, 'click', mapClick);
        geocoder = new GClientGeocoder();
        GEvent.addListener(map, "mousemove", function (point) {
            var LnglatStr6 = point.lng().toFixed(6) + ', ' + point.lat().toFixed(6);
            var latLngStr6 = point.lat().toFixed(6) + ', ' + point.lng().toFixed(6);
            //document.getElementById("over").options[0].text = LnglatStr6;
            //document.getElementById("over").options[1].text = latLngStr6;
        });
        GEvent.addListener(map, "zoomend", mapzoom);
        GEvent.addListener(map, "moveend", mapcenter);
        //ShowPathHistory(map)
    }
}

// mapClick - Handles the event of a user clicking anywhere on the map
// Adds a new point to the map and draws either a new line from the last point
// or a new polygon section depending on the drawing mode.
function mapClick(marker, clickedPoint) {
    var mapNormalProj = G_NORMAL_MAP.getProjection();
    var mapZoom = map.getZoom();
    var clickedPixel = mapNormalProj.fromLatLngToPixel(clickedPoint, mapZoom);
    var polyPixel = new GPoint(clickedPixel.x, clickedPixel.y);
    var polyPoint = mapNormalProj.fromPixelToLatLng(polyPixel, mapZoom);
    if (polyPoint) {
        if (holemode) {

            // Push onto polypoints of existing poly, building inner boundary
            holePoints.push(polyPoint);
        } else {

            polyPoints.push(polyPoint);
        }

        drawCoordinates();
    }
}


function mapzoom() {
    var mapZoom = map.getZoom();
    //document.getElementById("zoom").value = mapZoom;
}

function mapcenter() {
    var mapCenter = map.getCenter();
    var latLngStr6 = mapCenter.lat().toFixed(6) + ', ' + mapCenter.lng().toFixed(6);
    //document.getElementById("center").value = latLngStr6;
}

// drawCoordinates
function drawCoordinates() {


    if (polyPoints.length > 0) {
        // Re-create Polyline/Polygon
        if (polygonMode) { // polygon
            if (holePoints.length > 0) { // let's create a polygon with a hole
                var polywithholePoints = new Array();
                // join outer boundary coordinates and inner boundary coordinates
                polywithholePoints = polyPoints.concat(holePoints);
                // this is the polygon with a hole
                polyShape = new GPolygon(polywithholePoints, "#ffffff", 0, lineopacitycur, fillColorcur, fillopacitycur);
            } else { // normal polygon
                polyShape = new GPolygon(polyPoints, polygonlineColorcur, polygonLineWeightcur, lineopacitycur, fillColorcur, fillopacitycur);
            }
        } else { // polyline


            if (polyPoints.length > 2) {
                polyPointsnew = [];
                polyPointsnew.push(polyPoints[polyPoints.length - 1]);
                polyPointsnew.push(polyPoints[polyPoints.length - 2]);
            }
            else {
                polyPointsnew = polyPoints;
            }

            polyShape = new GPolyline(polyPointsnew, lineColorcur, lineWeightcur, lineopacitycur);
            if (holePoints.length > 0) { // polyline intended to be the hole border
                polyShape2 = new GPolyline(holePoints, lineColor2, lineWeightcur, lineopacitycur);
            }
        }
        // clear the map
        //map.clearOverlays();
        //ShowPathHistory(map) 
        // put marker(s) on the map
        marker = new GMarker(polyPoints[0]); // marker always at startpoint
        map.addOverlay(marker);
        if (holePoints.length > 0) {
            marker = new GMarker(holePoints[0]); // marker always at startpoint
            map.addOverlay(marker);
        }
        // add the updated shape(s)
        map.addOverlay(polyShape);
        if (holePoints.length > 0) {
            // polyShape2 will never be a polygon, just building lines until complete
            if (!polygonMode) {
                map.addOverlay(polyShape2);
            }
        }

    }
    document.getElementById("coords").value = polyPoints;
}

// the "Edit lines" button has been pressed
function editlines() {
    // editing is set to be possible only in polyline draw mode
    // same code in togglehole()
    document.getElementById("drawMode_polygon").checked = false;
    document.getElementById("drawMode_polyline").checked = true;
    polygonMode = document.getElementById("drawMode_polygon").checked;
    drawCoordinates(); // draw and log in polyline mode
    if (!polygonMode) {
        polyShape.enableEditing({ onEvent: "mouseover" });
        polyShape.disableEditing({ onEvent: "mouseout" });
        GEvent.addListener(polyShape, 'lineupdated', updateCoordinates);
        if (polyShape2 != null) {
            polyShape2.enableEditing({ onEvent: "mouseover" });
            GEvent.addListener(polyShape2, 'lineupdated', updateCoordinates);
        }
        // document.getElementById("coords").value =  "Drag a point. Points will be visible when you move the mouse to a line.";
    }

}

// when editing lines, the points arrays are updated with this function
function updateCoordinates() {
    var j = polyShape.getVertexCount(); // get the amount of points
    for (var i = 0; i < j; i++) {
        polyPoints[i] = polyShape.getVertex(i); // update polyPoints array
    }
    if (holePoints.length > 0) {
        j = polyShape2.getVertexCount(); // get the amount of points
        for (var i = 0; i < j; i++) {
            holePoints[i] = polyShape2.getVertex(i); // update holePoints array
        }
    }

}




// let start and end meet
function closePoly() {
    if (!circlemode) { // in circlemode this has been done in function drawCircle
        // Push onto polypoints of existing polyline
        if (holemode) {
            if (holePoints.length > 2) {
                holePoints.push(holePoints[0]);
                drawCoordinates();
            }
        } else {
            // Push onto polypoints of existing polyline/polygon
            if (polyPoints.length > 2) {
                polyPoints.push(polyPoints[0]);
                drawCoordinates();
            }
        }
    }
}

// Clear current Map
function clearMap() {
    map.clearOverlays();
    polyShape = null;
    polyShape2 = null;
    centerMarker = null;
    radiusMarker = null;
    polyPoints = [];
    holePoints = [];
    holemode = null;

}

// Toggle from Polygon PolyLine mode
function toggleDrawMode() {
    map.clearOverlays();
    polygonMode = document.getElementById("drawMode_polygon").checked;
    if (polyPoints.length > 0) {
        drawCoordinates();
    }
}


// Delete last Point
// This function removes the last point from the Polyline/Polygon and redraws
// map.
function deleteLastPoint() {
    if (!circlemode) { // do not allow delete last point in a circle
        if (!holemode) {
            map.removeOverlay(polyShape);
            // pop last element of polyPoints array
            polyPoints.pop();
            drawCoordinates();
        } else {
            map.removeOverlay(polyShape2);
            // pop last element of holePoints array
            holePoints.pop();
            drawCoordinates();
        }
    }
}


function ok() {
    drawCoordinates();
}

//Javascript Error Handling

function handleError() {

    return true;
}
window.onerror = handleError;




////Track Path


//function ShowPathHistory(map) {

//    //Search Strings
//    var SearchString = '';
//    SearchString = window.document.getElementById("HiddenBsu").value;
//    SearchString += '$' + window.document.getElementById("HiddenUnitId").value;
//    SearchString += '$' + window.document.getElementById("HiddenTrips").value;
//    SearchString += '$' + window.document.getElementById("HiddenDate").value;

//    var datasource = '../WebServices/gpsGenerateXml.asmx/V2_PathHistory?SearchString=' + SearchString; // + window.document.getElementById("HiddenUnitId").value

//    XMLHTTPRequestObject.open('GET', datasource, true);
//    var xmlObj = ''
//    XMLHTTPRequestObject.onreadystatechange = function () {
//        if (XMLHTTPRequestObject.readystate == 4 && XMLHTTPRequestObject.status == 200) {
//            var xmlDoc = XMLHTTPRequestObject.responseXML;
//            var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");
//            xmlDocc.load(xmlDoc);
//            xmlObj = xmlDocc.documentElement
//           
//            points = [];
//           
//            for (var i = 0; i < xmlObj.childNodes.length; i++) {

//                // Light blue marker icons
//                //var icon = new GIcon();
//                //icon.image = xmlObj.childNodes.item(i).childNodes.item(3).text;
//               // addIcon(icon);

//                points[i] = new GLatLng(parseFloat(xmlObj.childNodes.item(i).childNodes.item(1).text), parseFloat(xmlObj.childNodes.item(i).childNodes.item(2).text));
////                gmarkers[i] = new GMarker(points[i], icon);

////                // Store data attributes as property of gmarkers
////                var html = xmlObj.childNodes.item(i).childNodes.item(4).text;
////                gmarkers[i].content = html;
////                gmarkers[i].nr = i;
////                addClickevent(gmarkers[i]);
////                map.addOverlay(gmarkers[i]);
//            }

//            if (xmlObj.childNodes.length > 0) {
//                // Draw polylines between marker points

//                var poly = new GPolyline(points, "#2554C7", 5, .8);
//                map.addOverlay(poly);


//                // Open infowindow of first marker
//                //gmarkers[0].openInfoWindowHtml(gmarkers[0].content);
//            }

//        }

//    }

//    XMLHTTPRequestObject.send(null);


//}




