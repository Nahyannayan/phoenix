﻿// JScript File
//<![CDATA[

var map, actual;
var timeout;
var XMLHTTPRequestObject =false;
var bit =0;
var geocoder;
var address;

 if(window.ActiveXObject)
    {
    XMLHTTPRequestObject =new ActiveXObject('Microsoft.XMLHTTP');
    }

  
function buildMap() 
{

  if (timeout != null)
  {
  //Clear the previous timeout
  window.clearTimeout(timeout)
  }
 
 if(GBrowserIsCompatible()) 
 {
 
         if (bit==0)
         {
          // Find the Map Div
          map=new GMap2(document.getElementById("map"),{draggableCursor:'auto',draggingCursor:'move'});
          
          bit =1
          GEvent.addListener(map, "click", getAddress);
          geocoder = new GClientGeocoder();

         }
         else
         {
        
          var center = map.getCenter();
          window.document.getElementById("HiddenLat").value=center.lat()
          window.document.getElementById("Hiddenlog").value=center.lng()
          window.document.getElementById("HiddenZoomLevel").value=map.getZoom()
         }
                
          //Installs keyboard event handler for the map passed as argument
          new GKeyboardHandler(map); 
     
          //Get Zoom Level,latlng informations
          var zoom = parseInt(window.document.getElementById("HiddenZoomLevel").value) 
          var lat =window.document.getElementById("HiddenLat").value
          var log =window.document.getElementById("Hiddenlog").value
          map.setCenter(new GLatLng(lat,log),zoom, G_PHYSICAL_MAP)
          
         //Adding Controls.
         map.addControl(new GLargeMapControl ());
         
        
          GDownloadUrl("../Xml/data.xml", function(data) 
                            { 
                      
                                        var xmlDoc = new ActiveXObject("Microsoft.XMLDOM"); 
                                        var datasource ='../WebServices/gpsGenerateXml.asmx/GetCurrentLocations?UnitIds='+ window.document.getElementById("HiddenUnitId").value                                       
                                      
                                        XMLHTTPRequestObject.open('GET',datasource,false);
                                        XMLHTTPRequestObject.onreadystatechange =function()
                                        {
                                       
                                                if(XMLHTTPRequestObject.readystate==4 && XMLHTTPRequestObject.status==200)
                                                {
                                                var xmlDoc =XMLHTTPRequestObject.responseXML;
                                                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM"); 
                                                xmlDocc.load(xmlDoc);  
                                                xmlObj=xmlDocc.documentElement
                                                }
                                                else
                                                {
                                                //alert(XMLHTTPRequestObject.status)
                                                }
                                        } 
                                       
                                       XMLHTTPRequestObject.send(null);
                                     

                                        var xml = GXml.parse(xmlObj.xml);
                                        var markers = xml.documentElement.getElementsByTagName("marker");
                                         
                                         //Clear the over lay before plotting points
                                                 map.clearOverlays();     
                                                      
                                                      
                                                      for (var k=0;k<markers.length;k++)
                                                      {   
                                                        
                                                        var icon = new GIcon();
                                                        icon.image = 'https://school.gemsoasis.com/images/GPS_Images/greencirclemarker.png';
                                                        icon.iconSize = new GSize(32, 32);
                                                        icon.iconAnchor = new GPoint(16, 16);
                                                        icon.infoWindowAnchor = new GPoint(25, 7);
                                                        var lbl= '<div style="font-size: xx-small"> <b>' + markers[k].getAttribute("Bsu_regid") + '</b></div>'
                                                        opts = {   "icon": icon,  "clickable": true,  "labelText": lbl ,  "labelOffset": new GSize(-6, -10)};
                                                        var latlng = new GLatLng(parseFloat(markers[k].getAttribute("lat")), parseFloat(markers[k].getAttribute("lng")));
                                                       
                                                        var marker = new LabeledMarker(latlng, opts);
                                                        //var info =markers[k].getAttribute("info")
                                                     
                                                        //GEvent.addListener(marker, "click", function() {  marker.openInfoWindowHtml(info);});
                                                        map.addOverlay(marker);
                                                        
                                                                
                                                      }
                                                                    
                                                     

                            }
                             
                        );
                        
                                          
  }

  timeout =  window.setTimeout('buildMap();', 20000); // 20 sec

}

    
 function getAddress(overlay, latlng) 
 {
      if (latlng != null) 
      {
        address = latlng;
        geocoder.getLocations(latlng, showAddress);
      }
 }
 
 function showAddress(response) 
 {
      if (!response || response.Status.code != 200) 
      {
        alert("Status Code:" + response.Status.code);
      } 
      else 
      {
        place = response.Placemark[0];
        point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
        //        marker = new GMarker(point);
        //        map.addOverlay(marker);
        //        marker.openInfoWindowHtml(
        //        '<b>orig latlng:</b>' + response.name + '<br/>' + 
        //        '<b>latlng:</b>' + place.Point.coordinates[0] + "," + place.Point.coordinates[1] + '<br>' +
        //        '<b>Status Code:</b>' + response.Status.code + '<br>' +
        //        '<b>Status Request:</b>' + response.Status.request + '<br>' +
        //        '<b>Address:</b>' + place.address + '<br>' +
        //        '<b>Accuracy:</b>' + place.AddressDetails.Accuracy + '<br>' +
        //        '<b>Country code:</b> ' + place.AddressDetails.Country.CountryNameCode);

        var val=
        '<b>Address:</b>' + place.address + '<br>' +
        '<b>Country code:</b> ' + place.AddressDetails.Country.CountryNameCode 
        var obj = document.getElementById('Info');
        obj.innerHTML=val
      }
 }
    
function addIcon(icon) 
{ 
 // Add icon attributes
 icon.shadow= "https://school.gemsoasis.com/images/GPS_Images/mm_20_shadow.png";
 icon.iconSize = new GSize(30, 30);
 icon.shadowSize = new GSize(25, 25);
 icon.iconAnchor = new GPoint(6, 20);
 icon.infoWindowAnchor = new GPoint(5, 1);
}

//]]>

//Javascript Error Handling

function handleError() 
{

	return true;
}
window.onerror = handleError;
