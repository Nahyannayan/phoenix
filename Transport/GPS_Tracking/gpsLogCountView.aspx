﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="gpsLogCountView.aspx.vb" Inherits="Transport_GPS_Tracking_gpsLogCountView" %>

<%@ Register Src="UserControls/gpsLogCountView.ascx" TagName="gpsLogCountView" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Student Log Daily Summary
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:gpsLogCountView ID="gpsLogCountView1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>



