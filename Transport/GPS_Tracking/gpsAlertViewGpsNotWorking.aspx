﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsAlertViewGpsNotWorking.aspx.vb" Inherits="Transport_GPS_Tracking_gpsAlertViewGpsNotWorking" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head">
    <title></title>
    <%--  <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
  <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>

    <style type="text/css">
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>

    <script src="Javascript/GPS_LCDAlertView.js" type="text/javascript"></script>
    <script src="Javascript/jquery-1.8.2.min.js" type="text/javascript"></script>

    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->


    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/cssfiles/Popup.css" rel="stylesheet" />

    <script type="text/javascript">
        function popUpDetails(val) {
            window.open(val);
        }

        var allCheckBoxSelector = '#<%=GridBusListing.ClientID%> input[id*="chkselectAll"]:checkbox';
        var checkBoxSelector = '#<%=GridBusListing.ClientID%> input[id*="chkselect"]:checkbox';

        function ToggleCheckUncheckAllOptionAsNeeded() {
            var totalCheckboxes = $(checkBoxSelector),
             checkedCheckboxes = totalCheckboxes.filter(":checked"),
             noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
             allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);


            var btnActionALL = $('#<%= btnActionALL.ClientID %>');
            btnActionALL.attr('disabled', true);

            if (checkedCheckboxes.length == 0) {
                btnActionALL.attr('disabled', true);
            }
            else {
                btnActionALL.removeAttr('disabled');
            }


            $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
        }

        $(document).ready(function () {

            $(allCheckBoxSelector).live('click', function () {
                $(checkBoxSelector).attr('checked', $(this).is(':checked'));
                ToggleCheckUncheckAllOptionAsNeeded();
            });
            $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
            ToggleCheckUncheckAllOptionAsNeeded();

        });



    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>


            <asp:Image ID="Image1" ImageUrl="~/Images/Schools/STS/sts_headerbaord_bg.jpg" Width="100%" runat="server" />
            <table width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">
                        <table width="100%">
                            <tr>
                                <td align="left"><span class="field-label">Alerts - GPS not working</span></td>
                                <td align="right">
                                    <asp:ImageButton ID="ImageClose" OnClientClick="javascript:window.close();" runat="server"
                                        ImageUrl="~/Images/close_red.gif" />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Timer ID="Timer1" runat="server" Interval="10000">
                                </asp:Timer>

                                <telerik:RadGrid ID="GridBusListing" runat="server" AllowFilteringByColumn="True" CssClass="table table-bordered table-row"
                                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                    CellSpacing="0" GridLines="None" EnableTheming="False">
                                    <GroupingSettings CaseSensitive="false" />
                                    <ClientSettings AllowColumnsReorder="True">
                                    </ClientSettings>
                                    <MasterTableView>
                                        <CommandItemSettings></CommandItemSettings>

                                        <RowIndicatorColumn Visible="True">
                                            <HeaderStyle></HeaderStyle>
                                        </RowIndicatorColumn>

                                        <ExpandCollapseColumn Visible="True">
                                            <HeaderStyle></HeaderStyle>
                                        </ExpandCollapseColumn>

                                        <Columns>
                                            <telerik:GridTemplateColumn AllowFiltering="False"
                                                Groupable="False"
                                                HeaderText="Select" UniqueName="TemplateColumn1">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkselectAll" runat="server" />
                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="BSU_SHORTNAME"
                                                HeaderText="BSU"
                                                UniqueName="column">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FK_UNITID"
                                                HeaderText="Unit Id"
                                                UniqueName="column1">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="VEH_REGNO"
                                                HeaderText="Bus RegNo"
                                                UniqueName="column2">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DRV_NAME"
                                                HeaderText="Driver"
                                                UniqueName="column4">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DRVMOBILE"
                                                HeaderText="Driver Mob"
                                                UniqueName="column5">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="GPS_PROBLEM"
                                                HeaderText="Problem" UniqueName="column6">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn AllowFiltering="False"
                                                Groupable="False"
                                                HeaderText="Action" UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtnAction" runat="server" Text="Action"
                                                        OnClick="lnkbtnAction_Click"></asp:LinkButton>

                                                    <asp:LinkButton ID="lnkActionView" runat="server" Text="View"
                                                        OnClientClick="javascript:return false;"></asp:LinkButton>
                                                    <ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="server" TargetControlID="lnkActionView" HoverCssClass="popupHover" PopupControlID="pnlActionView" PopupPosition="Left">
                                                    </ajaxToolkit:HoverMenuExtender>
                                                    <asp:Panel ID="pnlActionView" runat="server" Width="250px" Height="100px" CssClass="popupMenu" BorderColor="BlueViolet" BorderWidth="1px" ScrollBars="Auto">
                                                        <table>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Remarks</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%#Bind("LOG_REMARKS")%>' CssClass="field-value"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Action by</span></td>

                                                                <td>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("LOG_ACTION_USER")%>' CssClass="field-value"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>

                                                    <asp:HiddenField ID="HF_LOG_ACTION" runat="server" Value='<%#Bind("LOG_ACTION")%>' />
                                                    <asp:HiddenField ID="HF_FK_UNITID" runat="server"
                                                        Value='<%#Bind("FK_UnitID")%>' />
                                                    <asp:HiddenField ID="HF_MAXPOSLOGID" runat="server"
                                                        Value='<%#Bind("MAXPOSLOGID")%>' />
                                                    <asp:HiddenField ID="HF_LOG_GPS_PROBLEM" runat="server"
                                                        Value='<%#Bind("GPS_PROBLEM")%>' />

                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>

                                        <EditFormSettings>
                                            <EditColumn></EditColumn>
                                        </EditFormSettings>
                                    </MasterTableView>

                                    <FilterMenu></FilterMenu>
                                </telerik:RadGrid>
                                <asp:Button ID="btnActionALL" runat="server" CssClass="button" Text="Take Action" OnClick="btnActionALL_Click" />

                                <asp:Panel ID="PnlActiontaken" runat="server" class="darkPanlAlumini" Visible="false">
                                    <%--style="display:none" --%>
                                    <div class="panel-cover inner_darkPanlAlumini" style="width: 400px">
                                        <div style="display: block; overflow: hidden; padding: 5px; margin: 5px 0;">
                                            <div style="float: left; width: 88%;"><span class="title-bg">Action Taken </span></div>
                                            <div style="float: right; vertical-align: top; height: 12px;">
                                                <asp:LinkButton ForeColor="red" ID="lbtnActionClose" ToolTip="click here to close" CssClass="" runat="server" Text="X" Font-Underline="false" CausesValidation="false">X</asp:LinkButton>

                                            </div>
                                        </div>
                                        <table>
                                            <tr>
                                                <td><span class="field-label">Remarks</span></td>

                                                <td>
                                                    <asp:TextBox ID="txtReasonAct" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RFV_txtReasonAct" runat="server" ControlToValidate="txtReasonAct" ErrorMessage="Remarks Required" ValidationGroup="save"></asp:RequiredFieldValidator></td>
                                            </tr>
                                            <tr>
                                                <td></td>

                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save Action" ValidationGroup="save" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="lblerrorAction" runat="server" Text="" EnableViewState="false" CssClass="error"></asp:Label>


                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="upProgGv" runat="server">
                            <ProgressTemplate>
                                <asp:Panel ID="pnlProgress" CssClass="screenCenter" runat="server">
                                    <br />
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /><br />
                                    Please Wait....
                                </asp:Panel>
                                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" HorizontalOffset="10"
                                    HorizontalSide="Center" ScrollEffectDuration=".1" TargetControlID="pnlProgress"
                                    VerticalOffset="10" VerticalSide="Middle">
                                </ajaxToolkit:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblerror" runat="server" Text="" EnableViewState="false" CssClass="error"></asp:Label>
            <br />
            Search<img id="img1" src="../../Images/GPS_Images/expand.png" alt="Expand/Collapse" onclick="javascript:CloseExpand(this);return false;" style="width: 16px; height: 16px" />
            <table id="Tab1" style="visibility: hidden" width="100%">
                <tr>
                    <td align="left" width="20%"><span class="field-label">Pause Paging</span></td>

                    <td align="left" width="30%">
                        <asp:CheckBox ID="CheckPause" runat="server" />
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>

                <tr>
                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                    <td align="left" width="30%">
                        <telerik:RadComboBox ID="ddlbsu" runat="server" Filter="Contains">
                        </telerik:RadComboBox>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Vehicle RegNo.</span></td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtVehRegNo" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Unit Id</span></td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtUnitId" runat="server"></asp:TextBox>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Bus No.</span></td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtBusNo" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Action Taken</span></td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddlActionTaken" runat="server">
                            <%-- <asp:ListItem Value="2">ALL</asp:ListItem>--%>
                            <asp:ListItem Value="2">Yes</asp:ListItem>
                            <asp:ListItem Value="1" Selected="True">No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" />
                        &nbsp;
                    <asp:Button ID="btnSearchClear" runat="server" CssClass="button" Text="Clear" />
                    </td>
                </tr>

            </table>



            <br />
        </div>
        <asp:HiddenField ID="HiddenSearchOptions" runat="server" />
        <asp:HiddenField ID="HiddenSmsMobileNumbers" runat="server" />
        <asp:HiddenField ID="HiddenRefreshCount" runat="server" />
        <asp:HiddenField ID="HiddenRefresh" runat="server" />
        <asp:HiddenField ID="HiddenSendSms" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        <asp:HiddenField ID="HiddenPaging" Value="0" runat="server" />
        <asp:HiddenField ID="HiddenStartFlag" Value="0" runat="server" />
        <asp:HiddenField ID="Hiddenemp_id" runat="server" />
    </form>
</body>
</html>
