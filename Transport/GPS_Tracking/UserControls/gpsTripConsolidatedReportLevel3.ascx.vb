﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_UserControls_gpsTripConsolidatedReportLevel3
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindGrid(False)
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If

    End Sub


    Public Sub BindGrid(ByVal export As Boolean)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", Request.QueryString("Date"))
        pParms(1) = New SqlClient.SqlParameter("@gps_trip_id", Request.QueryString("Tripid"))
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", Request.QueryString("Bsu"))
        pParms(3) = New SqlClient.SqlParameter("@FK_UnitID", Request.QueryString("uniid"))


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_TRIP_CONSOLIDATED_RPT_LEVEL_3", pParms)
        GridData.DataSource = ds
        GridData.DataBind()
        If export Then
            Dim dt As DataTable = ds.Tables(0)
            'dt.Columns.Remove("GPS_TRIP_ID")
            'dt.Columns.Remove("viewdetails")
            ' Create excel file.
            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
            Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
            ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '  ws.HeadersFooters.AlignWithMargins = True
            'Response.ContentType = "application/vnd.ms-excel"
            'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
            'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "Data.xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If

    End Sub

    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        GridData.PageIndex = e.NewPageIndex
        BindGrid(False)
    End Sub


    Protected Sub btnExport_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        BindGrid(True)
    End Sub

End Class
