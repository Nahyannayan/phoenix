﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsTripConsolidatedReportLevel3.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsTripConsolidatedReportLevel3" %>
 <br />
    <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export" 
    Width="100px" />
    <br />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" class="title-bg-lite">
            List of Students
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridData" runat="server" AllowPaging="True" EmptyDataText="No data found" AutoGenerateColumns="false"
                Width="100%" CssClass="table table-bordered table-row">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                           
                                        Student No
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("STU_NO")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                       Name
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("STU_PASPRTNAME")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                           
                                       Grade
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("GRM_DISPLAY")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                           
                                       Section
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("SCT_DESCR")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Onward/Return No
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("ONWARDBUS")%>/
                                <%# Eval("RETURNBUS")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
