﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsAttendanceNonCard.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsAttendanceNonCard" %>

<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />


<script type="text/javascript">

    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }



</script>

<div align="left">


    <table border="0" cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Student Search</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBsusearch" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student No</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtstudentno" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Name</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Section</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Pick Up</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtpickup" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Drop Off</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdropup" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Onward Bus No</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddbus1" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Return Bus No</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddbus2" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>

                        <td align="left" width="20%">
                            <asp:CheckBox ID="CheckAbsent" Text="Not Scanned" Checked="true" runat="server" CssClass="field-label" />
                        </td>
                        <td align="left" width="30%"></td>
                        <td align="left" width="20%"><span class="field-label">Event</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddevent" runat="server">
                                <%--            <asp:ListItem Text="ALL" Value="0" ></asp:ListItem>
            <asp:ListItem Text="Absent" Value="1" ></asp:ListItem>
            <asp:ListItem Text="Travelled without card" Value="2" ></asp:ListItem>--%>
                                <asp:ListItem Text="Faulty GPS" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Faulty Scanner" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Student List
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" PageSize="40" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    EmptyDataText="Information not available." Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Stu No
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("STU_NO")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Name
                                        
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("STU_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Grade
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("GRM_DISPLAY")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Section
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("SCT_DESCR")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Pick Up
                                     
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("ONWARDBUS")%> - <%# Eval("PICKUP")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Drop off
                                     
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("RETURNBUS")%> - <%# Eval("DROPOFF")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Remarks
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:TextBox ID="txtremarks" Text="Travelled without ID card " runat="server"></asp:TextBox>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                Last Status
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                              
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:CheckBox ID="ch1" runat="server" />
                                <asp:HiddenField ID="Hiddenid" Value='<%# Eval("STU_NO")%>' runat="server" />
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>



                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <table id="Tsave" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Save Log</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Vehicle Business Unit</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Bus No</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddvehRegNo" runat="server"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Trips</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddtrips" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Time</span></td>

                        <td align="left" width="30%">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:DropDownList ID="ddTHrs" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:DropDownList ID="ddTmin" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Type</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddtype" runat="server">
                                <asp:ListItem Text="Select Type" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Pick" Value="PICK"></asp:ListItem>
                                <asp:ListItem Text="Drop" Value="DROP"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblmessage" runat="server" CssClass="error" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <ajaxToolkit:CalendarExtender ID="CL1" TargetControlID="txtdate" PopupButtonID="txtdate" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>

</div>
