﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class Transport_GPS_Tracking_UserControls_gpsDefineTrips
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuid.Value = Session("sBsuid")
            BindBsu()
            BindMinSec()
            BindGrid()
        End If
    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        
    End Sub
    Public Sub BindMinSec()
        ''Bind Mins
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddFHrs.Items.Insert(i, "0" & i.ToString())
                ddTHrs.Items.Insert(i, "0" & i.ToString())
            Else
                ddFHrs.Items.Insert(i, i)
                ddTHrs.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddFmin.Items.Insert(i, "0" & i.ToString())
                ddTmin.Items.Insert(i, "0" & i.ToString())
            Else
                ddFmin.Items.Insert(i, i)
                ddTmin.Items.Insert(i, i)
            End If
        Next
        ddTHrs.SelectedValue = "23"
        ddTmin.SelectedValue = "59"
    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 1)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        GridData.DataSource = ds
        GridData.DataBind()

    End Sub

    Public Function CheckTime() As Boolean

        Dim returnval = False

        If ddTHrs.SelectedValue > ddFHrs.SelectedValue Then
            returnval = True
        Else
            lblmessage.Text = "To Hrs must be greater than From Hrs"
        End If

        Return returnval

    End Function

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        If CheckTime() Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(7) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OPTION ", 3)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@TRIP_NAME ", txtname.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@TRIP_FROM_HR", ddFHrs.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@TRIP_FROM_MINS", ddFmin.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@TRIP_TO_HR", ddTHrs.SelectedValue)
            pParms(6) = New SqlClient.SqlParameter("@TRIP_TO_MINS", ddTmin.SelectedValue)

            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)
            BindGrid()
        End If

    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub GridData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridData.RowCommand

        If e.CommandName = "deleting" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OPTION ", 4)
            pParms(1) = New SqlClient.SqlParameter("@GPS_TRIP_ID", e.CommandArgument)

            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)
            BindGrid()
        End If

    End Sub
End Class
