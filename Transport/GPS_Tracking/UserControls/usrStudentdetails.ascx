﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrStudentdetails.ascx.vb" Inherits="UserControls_usrStudentdetails" %>

<table width="100%">
    <tr>
        <td rowspan="5" valign="top">
            <asp:Image ID="img_stud" runat="server" Height="70px" Width="70px" />
        </td>
        <td align="left"><span class="field-label">Student No</span></td>
        <td align="left">
            <asp:Literal ID="Ltl_Stu_no" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td align="left"><span class="field-label">Student Name</span></td>
        <td align="left">
            <asp:Literal ID="Ltl_stuName" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td align="left"><span class="field-label">Grade / Section</span></td>
        <td align="left">
            <asp:Literal ID="Ltl_grd_sec" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td align="left"><span class="field-label">Primary Contact Name</span></td>
        <td align="left">
            <asp:Literal ID="Ltl_contname" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td align="left"><span class="field-label">Mobile Number</span></td>
        <td align="left">
            <asp:Literal ID="Ltl_mobnum" runat="server"></asp:Literal>
        </td>
    </tr>



</table>
