﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsStudentAbsentList.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsStudentAbsentList" %>

<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">


    function SendSMS(stu_id, adate, stu_no) {
        var actionvalue = '?stu_id=' + stu_id + '&adate=' + adate + '&stu_no=' + stu_no + '&type=ABSENT&mem_type=STUDENT';
        //window.showModalDialog('gpsSendSms.aspx' + actionvalue, '', 'dialogHeight:400px;dialogWidth:700px;scroll:no;resizable:no;');
        return ShowWindowWithClose('gpsSendSms.aspx' + actionvalue, 'search', '55%', '85%')
        return false;
    }

    function SMSHistory(stu_id) {
        var actionvalue = '?stu_id=' + stu_id + '&type=ABSENT&mem_type=STUDENT';
        //window.showModalDialog('gpsMessagHistory.aspx' + actionvalue, '', 'dialogHeight:600px;dialogWidth:800px;scroll:no;resizable:no;');
        return ShowWindowWithClose('gpsMessagHistory.aspx' + actionvalue, 'search', '55%', '85%')
        return false;
    }

    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }


    function gettemp() {
        //var rval = window.showModalDialog('Communication/comSelectMessage.aspx', '', 'dialogHeight:600px;dialogWidth:1000px;scroll:no;resizable:no;');
        return ShowWindowWithClose('Communication/comSelectMessage.aspx', 'search', '55%', '85%')
        return false;
          <%-- if (typeof rval == "undefined") {
               alert('Template not selected')
           }
           else {

               document.getElementById("<%= Hiddentempid.ClientID %>").value = rval;
               alert('Template select ID:' + rval)
           }--%>
    }

</script>
<div>


    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title">Student Absent Log Search</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <em>For fast response , please specify filter conditions more precise.</em>
                        </td>

                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <%-- <tr>
                            <td>
                                Vehicle Reg No
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddvehRegNo" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                GPS Unit Id
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:TextBox ID="txtgpsunit" runat="server"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="txtgpsunit_CalendarExtender" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="txtDate" PopupPosition="TopLeft" TargetControlID="txtgpsunit">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student Number</span> </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudentNumber" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date</span> </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtDate"
                                PopupPosition="TopLeft" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>

                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student Name</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtstuname" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Bus No</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBusNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Trips</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddtrips" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Option</span></td>

                        <td align="left" width="30%">

                            <asp:DropDownList ID="ddoption" runat="server">
                                <%--  <asp:ListItem Text="ALL" Value="0" ></asp:ListItem>--%>
                                <asp:ListItem Text="Absent List" Value="1"></asp:ListItem>
                                <%--<asp:ListItem Text="Travelled without card" Value="2" ></asp:ListItem>--%>
                                <asp:ListItem Text="Faulty GPS" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Faulty Scanner" Value="4"></asp:ListItem>
                            </asp:DropDownList>


                        </td>
                    </tr>
                    <tr>

                        <td align="left" width="20%"><span class="field-label">Hazard</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddhazard" runat="server">
                                <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Onward" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Return" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="CheckPicture" runat="server" Text="Show Picture" CssClass="field-label" /></td>

                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnview" runat="server" CssClass="button" Text="View"
                                ValidationGroup="T1" />
                            <%--<asp:Button ID="btnReport" runat="server" CssClass="button" Text="View Report" ValidationGroup="T1" />--%>
                            <asp:Button ID="btnReport" runat="server" CssClass="button" Text="View Report"
                                ValidationGroup="T1" />
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4" align="left">Note: Please update the trip onward start time of each trip..</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">List of Students Absent
                   
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblmessagetotal" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridData" AutoGenerateColumns="false" CssClass="table table-bordered table-row" Width="100%" ShowFooter="true" EmptyDataText="<center>No Records Found</center>"
                    runat="server" AllowPaging="True" PageSize="10">
                    <Columns>
                        <asp:TemplateField HeaderText="Student Details">
                            <ItemTemplate>
                                <table class="STU_STYLE">
                                    <tr>
                                        <td rowspan="5" valign="top">
                                            <asp:Image ID="img_stud" runat="server" ImageUrl='<%#Eval("STU_IMAGE") %>' Height="70px" Width="70px" />
                                        </td>
                                        <td>Student No
                                        </td>

                                        <td>
                                            <%# Eval("STU_NO")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student Name
                                        </td>

                                        <td>
                                            <%# Eval("STU_NAME")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Grade / Section
                                        </td>

                                        <td>
                                            <%# Eval("GRM_DISPLAY")%> / <%# Eval("SCT_DESCR")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Primary Contact Name
                                        </td>

                                        <td>
                                            <%# Eval("PARENTNAME")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile Number
                                        </td>

                                        <td>
                                            <%# Eval("STU_EMGCONTACT")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pick&nbsp;Up">
                            <ItemTemplate>
                                <%# Eval("ONWARDBUS")%>-<%# Eval("PICKUP")%><br />
                                Area : <%# Eval("PAREA")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Drop&nbsp;Off">
                            <ItemTemplate>
                                <%# Eval("RETURNBUS")%>-<%# Eval("DROPOFF")%><br />
                                Area : <%# Eval("DAREA")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MESSAGE">
                            <HeaderTemplate>
                                <center>
                                            MESSAGE
                                            <br />
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                ToolTip="Click here to select/deselect all rows" /></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                            <asp:CheckBox ID="ch1"   runat="server" />
                                            <asp:HiddenField ID="Hiddenid" Value='<%# Eval("STU_NO")%>' runat="server" />
                                        </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                                            <asp:LinkButton ID="Linktemplate" OnClientClick="gettemp(); return false;" runat="server">Template</asp:LinkButton>
                                            <br />
                                            <asp:Button ID="btnsend" runat="server" OnClick="SendSms" CssClass="button" Text="Send" />
                                        </center>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="SMS">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Linksend" OnClientClick='<%# Eval("SendSMS")%>' runat="server">Send</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="SMS">
                                <ItemTemplate>
                                   <asp:LinkButton ID="LinkHistory" visible='<%# Eval("HistoryVisible")%>' OnClientClick='<%# Eval("SMSHistory")%>' runat="server">History</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                    </Columns>
                    <HeaderStyle />
                    <RowStyle CssClass="griditem" />
                    <SelectedRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <EmptyDataRowStyle />
                    <EditRowStyle />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <em>Note(Send Messages): If &quot;MESSAGE&quot; checkbox not selected, message will be sent to all students to above filter conditions. </em>
    <br />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="Hiddentempid" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
        ControlToValidate="txtDate" Display="None" ErrorMessage="Please Enter  Date"
        SetFocusOnError="True" ValidationGroup="T1"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"
        ShowMessageBox="True" ShowSummary="False" ValidationGroup="T1" />

</div>


<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>
