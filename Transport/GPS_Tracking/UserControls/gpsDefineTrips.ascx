﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsDefineTrips.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsDefineTrips" %>




<div>
    <table width="100%">

        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Business Unit</span>
                        </td>

                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                        <td align="left" width="20%">
                            <span class="field-label">Trip Name</span>
                        </td>

                        <td align="left" width="20%">
                            <asp:TextBox ID="txtname" runat="server" CssClass="field-value"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Time</span>
                        </td>

                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddFHrs" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddFmin" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddTHrs" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddTmin" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>

                        <td align="center" colspan="5">
                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" s ValidationGroup="trip_name" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5">
                            <asp:Label ID="lblmessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<p>
    <br />
    <table width="100%">
        <tr>
            <td>
                <asp:GridView ID="GridData" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="false" EmptyDataText="&lt;center&gt;No Records Found&lt;/center&gt;"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Trip Name">
                            <ItemTemplate>
                                <center>
                                    <%# Eval("TRIP_NAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="From Hrs">
                            <ItemTemplate>
                                <center>
                                    <%# Eval("TRIP_FROM_HR")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="From Mins">
                            <ItemTemplate>
                                <center>
                                    <%# Eval("TRIP_FROM_MINS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To Hrs">
                            <ItemTemplate>
                                <center>
                                    <%# Eval("TRIP_TO_HR")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To Mins">
                            <ItemTemplate>
                                <center>
                                    <%# Eval("TRIP_TO_MINS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="Linkdelete" CommandName="deleting" CausesValidation="false"  CommandArgument='<%# Eval("GPS_TRIP_ID")%>' runat="server">Delete</asp:LinkButton>
                               <ajaxToolkit:ConfirmButtonExtender ID="CF1" ConfirmText="Do you wish to continue?" TargetControlID="Linkdelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <%--<SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</p>
<asp:HiddenField ID="HiddenBsuid" runat="server" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
    ErrorMessage="Please a trip name " ControlToValidate="txtname"
    Display="None" SetFocusOnError="True" ValidationGroup="trip_name"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server"
    ShowMessageBox="True" ShowSummary="False" ValidationGroup="trip_name" />



