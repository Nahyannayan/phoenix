﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsStudentAttendanceVehicle.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsStudentAttendanceVehicle" %>

<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">

    function ShowMap(link) {

        //window.showModalDialog(link)
        return ShowWindowWithClose(link, 'search', '55%', '85%')
        return false;
    }

    function ShowMessageForm() {

        url = "gpsStudentAttendanceVehicleMessage.aspx";

        //window.showModalDialog(url)
        return ShowWindowWithClose(url, 'search', '55%', '85%')
        return false;

    }

    function SendSMS(stu_id, pk_id, stu_no) {

        var actionvalue = '?stu_id=' + stu_id + '&Pk_id=' + pk_id + '&stu_no=' + stu_no + '&type=LOCATION&mem_type=STUDENT';
        //window.showModalDialog('gpsSendSms.aspx' + actionvalue, '', 'dialogHeight:400px;dialogWidth:700px;scroll:no;resizable:no;');
        return ShowWindowWithClose('gpsSendSms.aspx' + actionvalue, 'search', '55%', '85%')
        return false;

    }

    function SMSHistory(stu_id) {

        var actionvalue = '?stu_id=' + stu_id + '&type=LOCATION&mem_type=STUDENT';
        //window.showModalDialog('gpsMessagHistory.aspx' + actionvalue, '', 'dialogHeight:600px;dialogWidth:800px;scroll:no;resizable:no;');
        return ShowWindowWithClose('gpsMessagHistory.aspx' + actionvalue, 'search', '55%', '85%')
        return false;

    }

    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }


    function gettemp() {
        //var rval = window.showModalDialog('Communication/comSelectMessage.aspx', '', 'dialogHeight:600px;dialogWidth:1000px;scroll:no;resizable:no;');
        return ShowWindowWithClose('Communication/comSelectMessage.aspx', 'search', '55%', '85%')
        return false;

       <%-- if (typeof rval == "undefined") {
            alert('Template not selected')
        }
        else {

            document.getElementById("<%= Hiddentempid.ClientID %>").value = rval;
            alert('Template select ID:' + rval)
        }--%>

    }


</script>
<div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Student Attendance Log Search
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="100%">
                            <%--  <tr>
                                <td colspan="6">
                                    <em style="color: #0000FF">For fast response , please specify filter conditions more
                                        precise.</em>
                                </td>
                            </tr>--%>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Business Unit</span> </td>

                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Curriculum</span> </td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlCurri_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Vehicle Reg No</span></td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddvehRegNo" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Grade</span> </td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Trips</span> </td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddtrips" runat="server">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddtriptype" runat="server">
                                        <asp:ListItem Text="--" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Onward" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Return" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Section</span> </td>
                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddlSection" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Student Number</span></td>
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtStudentNumber" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" width="20%"><span class="field-label">GPS Unit Id</span>
                                </td>
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtgpsunit" runat="server"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="txtgpsunit_CalendarExtender" runat="server" Format="dd/MMM/yyyy"
                                        PopupButtonID="txtDate" PopupPosition="TopLeft" TargetControlID="txtgpsunit">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                            </tr>
                            <tr>

                                <td align="left" width="20%"><span class="field-label">Bus No</span> </td>

                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtBusNo" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Student Name</span>
                                </td>

                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtstuname" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td align="left" width="20%"><span class="field-label">Date</span>
                                </td>

                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtDate"
                                        PopupPosition="TopLeft" TargetControlID="txtDate">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Risk Area</span>
                                </td>

                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddriskarea" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Onward" Value="O"></asp:ListItem>
                                        <asp:ListItem Text="Return" Value="R"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Last Known Status</span></td>

                                <td align="left" width="30%">
                                    <asp:DropDownList ID="DropCheckStatus" runat="server">
                                        <asp:ListItem Selected="True" Text="--" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="IN" Value="IN"></asp:ListItem>
                                        <asp:ListItem Text="OUT" Value="OUT"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Card Type</span>
                                </td>

                                <td align="left" width="30%">
                                    <asp:DropDownList ID="ddcardtype" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:CheckBox ID="TDB" runat="server" Text="TDB" CssClass="field-label" />
                                    <asp:CheckBox ID="CheckPicture" runat="server" Text="Show Picture" CssClass="field-label" />
                                </td>

                                <td align="left" colspan="2">

                                    <asp:CheckBox ID="chkOT" runat="server" Text="Own Transport" CssClass="field-label" />
                                </td>

                            </tr>
                            <%-- <tr>
                            <td>
                                Time From
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:DropDownList ID="DropFromHr" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="DropFromMins" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                Time To
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:DropDownList ID="DropToHr" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="DropToMins" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>--%>
                            <tr>
                                <td align="left" colspan="4">
                                    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnview" runat="server" CssClass="button" Text="View Attendance" ValidationGroup="T1" />
                                    <asp:Button ID="btnReport" runat="server" CssClass="button" Text="View Report" ValidationGroup="T1" />
                                    <asp:Button ID="btnReportExcel" runat="server" CssClass="button" Text="Excel Report -Students" ValidationGroup="T1" />
                                    <asp:Button ID="btnReportExcel_Staff" runat="server" CssClass="button" Text="Excel Report -Staff" ValidationGroup="T1" />
                                    <asp:Button ID="btnPushToApp" runat="server" CssClass="button" Text="Push to App" ValidationGroup="T1" />




                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Student Attendance Log
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblmessage0" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="GridData" AutoGenerateColumns="false" Width="100%" ShowFooter="true" CssClass="table table-bordered table-row"
                            EmptyDataText="<center>No Records Found</center>" runat="server" AllowPaging="True"
                            PageSize="10">
                            <Columns>
                                <asp:TemplateField HeaderText="Student Details" HeaderStyle-Width="30%" >
                                    <ItemTemplate>
                                        <asp:Panel ID="Panel1" BackColor='<%#System.Drawing.Color.FromName(DataBinder.Eval(Container, "DataItem.STUDENT_COLOR_CODE") ) %>' runat="server">
                                            <br />
                                        </asp:Panel>

                                        <div class="container-fluid">
                                            <div class="row-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3" style="margin: auto;">
                                                        <asp:Image ID="img_stud" runat="server" ImageUrl='<%#Eval("STU_IMAGE") %>' Height="70px"
                                                            Width="70px" />
                                                    </div>
                                                    <div class="col-9 col-lg-9 col-md-9 col-md-9">
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                Student No
                                                            </div>

                                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                                <%# Eval("BARCODE")%>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                Student Name
                                                            </div>

                                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                                <%# Eval("STU_NAME")%>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                Grade / Section
                                                            </div>

                                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                                <%# Eval("GRM_DISPLAY")%>
                                                    /
                                                    <%# Eval("SCT_DESCR")%>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                Primary Contact Name
                                                            </div>

                                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                                <%# Eval("PARENTNAME")%>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                                Mobile Number
                                                            </div>

                                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                                <%# Eval("STU_EMGCONTACT")%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <%--<ItemStyle BackColor='<%#System.Drawing.Color.FromName(DataBinder.Eval(Container, "DataItem.STUDENT_COLOR_CODE") ) %>' />--%>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IN">
                                    <ItemTemplate>
                                        <%--<center>
                                            <table class="IN_STYLE">
                                                <tr>
                                                    <td rowspan="4" valign="top">--%>
                                        <%--<asp:Image ID="img_In" ImageUrl="~/Images/GPS_Images/IN.png" runat="server" Height="40px" Width="40px" />--%>
                                        <%-- </td>
                                                    <td >--%>
                                        <asp:LinkButton ID="inLocation" OnClientClick='<%# Eval("InLink")%>' runat="server">Map</asp:LinkButton>
                                        <br />
                                        <%--</td>
                                                </tr>
                                                <tr>
                                                    <td >--%>
                                        <asp:LinkButton ID="inmap" CommandName="Location" CommandArgument='<%# Eval("PK_PositionLogID")%>'
                                            runat="server">Location</asp:LinkButton>
                                        <br />
                                        <%--  </td>
                                                </tr>
                                                <tr>
                                                    <td >--%>
                                        <%# Eval("VEH")%>
                                        <br />
                                        <%--</td>
                                                </tr>
                                                <tr>
                                                    <td >--%>
                                        <%# Eval("P_Datetime")%>
                                        <%--</td>
                                                </tr>
                                            </table>
                                        </center>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OUT">
                                    <ItemTemplate>
                                        <%--<center>
                                            <table border="0" visible='<%# Eval("Visible")%>' runat="server">
                                                <tr>
                                                    <td rowspan="4" valign="top">--%>
                                        <%--<asp:Image ID="img_out" runat="server" ImageUrl='~/Images/GPS_Images/OUT.png' Height="40px" Width="40px" />--%>
                                        <%--</td>
                                                    <td >--%>
                                        <asp:LinkButton ID="outLocation" OnClientClick='<%# Eval("OutLink")%>' runat="server">Map</asp:LinkButton>
                                        <br />
                                        <%--</td>
                                                </tr>
                                                <tr>
                                                    <td >--%>
                                        <asp:LinkButton ID="outmap" CommandName="Location" CommandArgument='<%# Eval("Next_PK_PositionLogID")%>'
                                            runat="server">Location</asp:LinkButton>
                                        <br />
                                        <%--</td>
                                                </tr>
                                                <tr>
                                                    <td >--%>
                                        <%# Eval("VEH")%>
                                        <br />
                                        <%--</td>
                                                </tr>
                                                <tr>
                                                    <td >--%>
                                        <%# Eval("N_Datetime")%>
                                        <%-- </td>
                                                </tr>
                                            </table>
                                        </center>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Card Type">
                                    <ItemTemplate>
                                        <%# Eval("CARD_TYPE_DESC")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pick&nbsp;Up">
                                    <ItemTemplate>
                                        <%# Eval("ONWARDBUS")%>-<%# Eval("PICKUP")%><br />
                                        Area :
                                        <%# Eval("PAREA")%>
                                        <center>
                                            <asp:Image ID="ImageRiskO" Visible='<%# Eval("PICK_UP_HAZARD")%>' ImageUrl="~/Images/alertRed.gif"
                                                runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Drop&nbsp;Off">
                                    <ItemTemplate>
                                        <%# Eval("RETURNBUS")%>-<%# Eval("DROPOFF")%><br />
                                        Area :
                                        <%# Eval("DAREA")%>
                                        <center>
                                            <asp:Image ID="ImageRiskR" Visible='<%# Eval("DROP_OFF_HAZARD")%>' ImageUrl="~/Images/alertRed.gif"
                                                runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TDB">
                                    <ItemTemplate>
                                        <center>
                                        <asp:Image ID="ImageTDB" Visible='<%# Eval("NOT_SAME_BUS")%>' ImageUrl="~/Images/alertRed.gif"
                                                runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Color&nbsp;Code">
                                    <ItemTemplate>
                                        <center>
                                         <asp:TextBox ID="TextStatus" Width="50px"  BorderWidth="0"  EnableTheming="false" BackColor='<%#System.Drawing.Color.FromName(DataBinder.Eval(Container, "DataItem.STUDENT_COLOR_CODE") ) %>'  ReadOnly="true"   runat="server"></asp:TextBox>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="MESSAGE">
                                    <HeaderTemplate>
                                        <center>
                                            MESSAGE
                                            <br />
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                ToolTip="Click here to select/deselect all rows" /></center>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:CheckBox ID="ch1" Visible='<%# Eval("VisibleMSG")%>' runat="server" />
                                            <asp:HiddenField ID="Hiddenid" Value='<%# Eval("BARCODE")%>' runat="server" />
                                        </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                            <asp:LinkButton ID="Linktemplate" OnClientClick="gettemp(); return false;" runat="server">Template</asp:LinkButton>
                                            <br />
                                            <asp:Button ID="btnsend" runat="server" OnClick="SendSms" CssClass="button" Text="Send" />
                                        </center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="SMS">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Linksend" visible='<%# Eval("SmsVisible")%>' OnClientClick='<%# Eval("SendSMS")%>' runat="server">Send</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="SMS">
                                <ItemTemplate>
                                   <asp:LinkButton ID="LinkHistory" visible='<%# Eval("HistoryVisible")%>' OnClientClick='<%# Eval("SMSHistory")%>' runat="server">History</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblmessage1" runat="server" CssClass="error"></asp:Label>
            <br />
            <em>Note(Send Messages): If &quot;MESSAGE&quot; checkbox not
                selected, message will be sent to all students to above filter conditions. </em>
            <br />
            <asp:Label ID="Label2" runat="server"></asp:Label>
            <br />
            <asp:Panel ID="PanelShow" runat="server"
                BackColor="white" CssClass="panel-cover" Style="display: none" Width="512">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Location of the Bus
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="Label1" runat="server" CssClass="field-label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Button ID="btncancel" runat="server" Text="OK" CssClass="button" CausesValidation="false"></asp:Button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelShow" RepositionMode="RepositionOnWindowResizeAndScroll"
                TargetControlID="Label2">
            </ajaxToolkit:ModalPopupExtender>
            <asp:HiddenField ID="HiddenBsuid" runat="server" />
            <asp:HiddenField ID="Hiddentempid" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                Display="None" ErrorMessage="Please Enter  Date" SetFocusOnError="True" ValidationGroup="T1"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" ValidationGroup="T1" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>


<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>
