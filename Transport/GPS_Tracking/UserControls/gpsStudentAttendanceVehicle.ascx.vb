﻿Imports system
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports System.IO

Partial Class Transport_GPS_Tracking_UserControls_gpsStudentAttendanceVehicle
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuid.Value = Session("sBsuid")
            BindBsu()

            BinndDate()
            Crdtype()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReportExcel)
    End Sub

    Public Function getacd_id() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "  select top 1 ACD_ID from [OASIS].[dbo].[ACADEMICYEAR_D] where ACD_BSU_ID='" & ddBsu.SelectedValue & "'  AND ACD_CURRENT=1 "

        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    End Function
    Sub bindCLM()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim acd_id As String = getacd_id()

        '        str_query = " select clm_descr,clm_id from dbo.CURRICULUM_M where clm_id in " & _
        '" (select distinct acd_clm_id from academicyear_d where acd_acy_id='" & acd_id & "' and acd_bsu_id='" & ddBsu.SelectedValue & "')"

        str_query = " select clm_id,clm_descr from dbo.curriculum_M inner join  academicyear_d on acd_clm_id=clm_id" & _
                    " where acd_current=1 and acd_bsu_id='" & ddBsu.SelectedValue & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCurri.DataSource = ds
        ddlCurri.DataTextField = "clm_descr"
        ddlCurri.DataValueField = "clm_id"
        ddlCurri.DataBind()

        If ddlCurri.Items.Count = 0 Then
            ddlCurri.Items.Add(New ListItem("ALL", "0"))
            ddlCurri.ClearSelection()
            ddlCurri.Items.FindByText("ALL").Selected = True
        End If
        ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim clm As String = ddlCurri.SelectedValue
        Dim acy_id As String = getacd_id()

        Dim ds As DataSet

        If clm <> "0" Then
            ' str_query = " SELECT     distinct GRADE_BSU_M.GRM_GRD_ID, GRADE_BSU_M.GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER " & _
            '" FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
            '" WHERE(GRADE_BSU_M.GRM_BSU_ID = '" & ddBsu.SelectedValue & "') AND (GRADE_BSU_M.GRM_ACY_ID = '" & acy_id & "') AND (GRADE_BSU_M.GRM_ACD_ID = " & _
            '" (SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID ='" & clm & "') AND " & _
            '"  (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & ddBsu.SelectedValue & "'))) order by GRADE_M.GRD_DISPLAYORDER "

            str_query = "select distinct grm_grd_id,grm_display,grd_displayorder from dbo.grade_bsu_m inner join " & _
    " academicyear_d on grm_acd_id=acd_id inner join grade_m on grm_grd_id=grd_id " & _
    " where acd_current=1 and acd_bsu_id='" & ddBsu.SelectedValue & "' and acd_clm_id='" & ddlCurri.SelectedValue & "' order by grd_displayorder "


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "grm_grd_id"
            ddlGrade.DataBind()
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = True
        Else
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = False
        End If


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub

    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim clm As String = ddlCurri.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim acy_id As String = getacd_id()
        If clm <> "0" Then
            ' Dim str_query As String = " SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE  SCT_GRD_ID= '" & GRD_ID & "'" & _
            '" AND SCT_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & clm & "') AND " & _
            ' " (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & ddBsu.SelectedValue & "')) ORDER BY SCT_DESCR"


            Dim str_query As String = " SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE  SCT_GRD_ID= '01' " & _
    " AND SCT_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & ddlCurri.SelectedValue & "')  " & _
" and acd_current=1 AND (ACD_BSU_ID = '" & ddBsu.SelectedValue & "') ) ORDER BY SCT_DESCR "


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = True
        Else
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = False
        End If

    End Sub


    Public Sub Crdtype()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query = " select * from GPS_CARD_TYPES where MODULE='GPS' ORDER BY CARD_TYPE_DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        ddcardtype.DataSource = ds
        ddcardtype.DataTextField = "CARD_TYPE_DESC"
        ddcardtype.DataValueField = "ID"
        ddcardtype.DataBind()
        Dim list As New ListItem
        list.Text = "Select Card Type"
        list.Value = "-1"
        ddcardtype.Items.Insert(0, list)


    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If
        ddBsu_SelectedIndexChanged(ddBsu, Nothing)




   
    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        If ddBsu.SelectedValue <> "-1" Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter


            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

            pParms(1) = New SqlClient.SqlParameter("@DATE", txtDate.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 6)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                ddvehRegNo.DataTextField = "DATA"
                ddvehRegNo.DataValueField = "VEH_UNITID"
                ddvehRegNo.DataSource = ds
                ddvehRegNo.DataBind()
            End If
        End If

        Dim list As New ListItem
        list.Text = "Vehicle"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)

        BindTrips()

    End Sub
    Public Sub BinndDate()
        txtDate.Text = Today.ToString("dd/MMM/yyyy")
    End Sub

    Public Sub BindTrips()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtrips.DataSource = ds
            ddtrips.DataTextField = "TRIP"
            ddtrips.DataValueField = "GPS_TRIP_ID"
            ddtrips.DataBind()

            Dim list As New ListItem
            list.Text = "Select a Trip"
            list.Value = "-1"

            ddtrips.Items.Insert(0, list)
            ddtrips.Visible = True
        Else
            ddtrips.Visible = False
        End If




    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        bindCLM()
        BindVehReg()

    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        BindGrind()
    End Sub

    Public Function GetTripDetails(ByVal tipid As String) As DataSet

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 5)
        pParms(1) = New SqlClient.SqlParameter("@GPS_TRIP_ID", tipid)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)


        Return ds
    End Function

    Public Function BindGrind() As DataSet

        lblmessage.Text = ""
        lblmessage1.Text = ""
        Dim ds As New DataSet
        If txtStudentNumber.Text.Trim() <> "" Or ddBsu.SelectedValue <> "-1" Or txtstuname.Text.Trim() <> "" Then
            Dim unitid = ""

            If ddvehRegNo.SelectedValue <> "-1" Then
                unitid = ddvehRegNo.SelectedValue
            Else
                unitid = txtgpsunit.Text.Trim()
            End If

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(20) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())
            If txtstuname.Text.Trim() <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@STU_NAME", txtstuname.Text.Trim())
            End If
            If txtBusNo.Text.Trim() <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@BUS_NO", txtBusNo.Text.Trim())
            End If
            If ddBsu.SelectedValue <> "-1" Then
                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            End If
            If unitid.ToString().Trim() <> "" Then
                pParms(4) = New SqlClient.SqlParameter("@FK_UnitID", unitid)
            End If
            If txtStudentNumber.Text.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@STU_NO", txtStudentNumber.Text.Trim())
            End If

            If DropCheckStatus.SelectedValue <> "-1" Then
                pParms(6) = New SqlClient.SqlParameter("@CHECKSTATUS", DropCheckStatus.SelectedValue)
            End If

            Dim FromHr = "00"
            Dim FromMins = "00"
            Dim ToHr = "23"
            Dim ToMins = "59"

            If ddtrips.SelectedIndex > 0 Then
                Dim ds1 As DataSet = GetTripDetails(ddtrips.SelectedValue)
                FromHr = ds1.Tables(0).Rows(0).Item("TRIP_FROM_HR").ToString()
                FromMins = ds1.Tables(0).Rows(0).Item("TRIP_FROM_MINS").ToString()
                ToHr = ds1.Tables(0).Rows(0).Item("TRIP_TO_HR").ToString()
                ToMins = ds1.Tables(0).Rows(0).Item("TRIP_TO_MINS").ToString()
            End If
            pParms(7) = New SqlClient.SqlParameter("@FromHr", FromHr)
            pParms(8) = New SqlClient.SqlParameter("@FromMins", FromMins)
            pParms(9) = New SqlClient.SqlParameter("@ToHr", ToHr)
            pParms(10) = New SqlClient.SqlParameter("@ToMins", ToMins)

            If ddcardtype.SelectedValue <> "-1" Then
                pParms(11) = New SqlClient.SqlParameter("@CARD_TYPE", ddcardtype.SelectedValue)
            End If

            If ddlGrade.SelectedValue <> "0" Then
                pParms(12) = New SqlClient.SqlParameter("@GRM_DISPLAY", ddlGrade.SelectedItem.Text.Trim())
            End If

            If ddlSection.SelectedValue <> "0" Then
                pParms(13) = New SqlClient.SqlParameter("@SCT_DESCR", ddlSection.SelectedItem.Text.Trim())
            End If


            If ddriskarea.SelectedValue <> "0" Then
                If ddriskarea.SelectedValue = "O" Then
                    pParms(14) = New SqlClient.SqlParameter("@O_HAZARD", "True")
                Else
                    pParms(14) = New SqlClient.SqlParameter("@R_HAZARD", "True")
                End If
            End If

            If TDB.Checked Then
                pParms(15) = New SqlClient.SqlParameter("@NOT_SAME_BUS", TDB.Checked)
            End If

            If ddtriptype.SelectedValue <> "-1" Then
                pParms(16) = New SqlClient.SqlParameter("@TRIP_TYPE", ddtriptype.SelectedValue)
            End If
            Dim lstrSP As String
            If chkOT.Checked Then
                lstrSP = "GPS_GET_BARCODE_LOG_STUDENT_OT"
            Else
                lstrSP = "GPS_GET_BARCODE_LOG_STUDENT"
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, lstrSP, pParms)
            GridData.DataSource = ds
            GridData.DataBind()

            lblmessage0.Text = " : Total Records :  " & ds.Tables(0).Rows.Count



        Else
            lblmessage.Text = "Please specify any one of the following, <br> Business Unit , Student Number or Student Name."
        End If


        Return ds

    End Function

    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Public Function CheckboxListValues(ByVal Grid As GridView, ByVal CheckboxId As String, ByVal hiddenPrimaryId As String, ByVal beforePageChange As Boolean) As Hashtable

        Dim hash As New Hashtable

        If Session("selection") Is Nothing Then
        Else
            hash = Session("selection")
        End If

        For Each row As GridViewRow In Grid.Rows

            Dim HiddenPrimanyIdC As HiddenField = DirectCast(row.FindControl(hiddenPrimaryId), HiddenField)
            Dim CheckboxIdC As CheckBox = DirectCast(row.FindControl(CheckboxId), CheckBox)
            Dim ID = HiddenPrimanyIdC.Value.Trim()

            If CheckboxIdC.Visible = True Then
                If beforePageChange Then
                    If CheckboxIdC.Checked Then
                        If Not hash.ContainsKey(ID) Then
                            hash.Add(ID, "'" & ID & "'")
                        End If
                    Else
                        If hash.ContainsKey(ID) Then
                            hash.Remove(ID)
                        End If
                    End If
                Else

                    If hash.ContainsKey(ID) Then
                        CheckboxIdC.Checked = True
                    Else
                        CheckboxIdC.Checked = False
                    End If

                End If

            End If
        Next
        Session("selection") = hash
        Return hash
    End Function
   
    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        CheckboxListValues(GridData, "ch1", "Hiddenid", True)
        GridData.PageIndex = e.NewPageIndex
        BindGrind()

    End Sub

    Protected Sub GridData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridData.RowCommand
        lblmessage.Text = ""
        Try


            If e.CommandName = "Location" Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@PK_PositionLogID", e.CommandArgument)
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_LAST_PK_LOG_ID", pParms)


                Dim lat, lng As String


                If ds.Tables(0).Rows.Count > 0 Then
                    lat = ds.Tables(0).Rows(0).Item("Latitude").ToString()
                    lng = ds.Tables(0).Rows(0).Item("Longitude").ToString()

                    Dim ReturnValue As String = ""


                    Dim ServicePath = "http://ws.geonames.org/findNearbyPlaceName?lat=" & lat & "&lng=" & lng & ""
                    Dim myWebClient As New System.Net.WebClient
                    Dim responseArray As Byte() = myWebClient.DownloadData(ServicePath)
                    ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)
                    Dim startindex = ReturnValue.IndexOf("<name>")
                    Dim endindex = ReturnValue.IndexOf("</name>")
                    ReturnValue = ReturnValue.Substring(startindex + 6, (endindex - (startindex + 6)))
                    Label1.Text = ReturnValue.Replace("?", "")
                Else
                    Label1.Text = "Unit Information not available in our database."
                End If

                MO1.Show()

            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        If txtStudentNumber.Text.Trim() <> "" Or ddBsu.SelectedValue <> "-1" Or txtstuname.Text.Trim() <> "" Then
            Dim param As New Hashtable


            Dim unitid As String = ""

            If ddvehRegNo.SelectedValue <> "-1" Then
                unitid = ddvehRegNo.SelectedValue
            Else
                unitid = txtgpsunit.Text.Trim()
            End If

            param.Add("UserName", Session("sUsr_name"))
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@PositionLogDateTime", txtDate.Text.Trim()) 'Format(Date.Parse(txtDate.Text.Trim()), "MM/dd/yyyy")

            Dim FromHr = "00"
            Dim FromMins = "00"
            Dim ToHr = "23"
            Dim ToMins = "59"

            If ddtrips.SelectedIndex > 0 Then
                Dim ds1 As DataSet = GetTripDetails(ddtrips.SelectedValue)
                FromHr = ds1.Tables(0).Rows(0).Item("TRIP_FROM_HR").ToString()
                FromMins = ds1.Tables(0).Rows(0).Item("TRIP_FROM_MINS").ToString()
                ToHr = ds1.Tables(0).Rows(0).Item("TRIP_TO_HR").ToString()
                ToMins = ds1.Tables(0).Rows(0).Item("TRIP_TO_MINS").ToString()
            End If
  
            param.Add("@FromHr", FromHr)
            param.Add("@FromMins", FromMins)
            param.Add("@ToHr", ToHr)
            param.Add("@ToMins", ToMins)

          

            If txtstuname.Text.Trim() <> "" Then
                param.Add("@STU_NAME", txtstuname.Text.Trim())
            Else
                param.Add("@STU_NAME", DBNull.Value)
            End If

            If txtBusNo.Text.Trim() <> "" Then
                param.Add("@BUS_NO", txtBusNo.Text.Trim())
            Else
                param.Add("@BUS_NO", DBNull.Value)
            End If

            If ddBsu.SelectedValue <> "-1" Then
                param.Add("@BSU_ID", ddBsu.SelectedValue.ToString())
            Else
                param.Add("@BSU_ID", DBNull.Value)
            End If

            If unitid.ToString().Trim() <> "" Then
                param.Add("@FK_UnitID", unitid)
            Else
                param.Add("@FK_UnitID", DBNull.Value)
            End If

            If txtStudentNumber.Text.Trim() <> "" Then
                param.Add("@STU_NO", txtStudentNumber.Text.Trim())
            Else
                param.Add("@STU_NO", DBNull.Value)
            End If

            If DropCheckStatus.SelectedValue <> "-1" Then
                param.Add("@CHECKSTATUS", DropCheckStatus.SelectedValue)
            Else
                param.Add("@CHECKSTATUS", DBNull.Value)
            End If

            If ddcardtype.SelectedValue <> "-1" Then
                param.Add("@CARD_TYPE", ddcardtype.SelectedValue)
            Else
                param.Add("@CARD_TYPE", DBNull.Value)
            End If

            If ddlGrade.SelectedValue <> "0" Then
                param.Add("@GRM_DISPLAY", ddlGrade.SelectedItem.Text.Trim())
            Else
                param.Add("@GRM_DISPLAY", DBNull.Value)
            End If

            If ddlSection.SelectedValue <> "0" Then
                param.Add("@SCT_DESCR", ddlSection.SelectedItem.Text.Trim())
            Else
                param.Add("@SCT_DESCR", DBNull.Value)
            End If

            If ddriskarea.SelectedValue <> "0" Then
                If ddriskarea.SelectedValue = "O" Then
                    param.Add("@O_HAZARD", "True")
                    param.Add("@R_HAZARD", "False")
                Else
                    param.Add("@O_HAZARD", "False")
                    param.Add("@R_HAZARD", "True")
                End If

            Else
                param.Add("@O_HAZARD", DBNull.Value)
                param.Add("@R_HAZARD", DBNull.Value)
            End If

            If TDB.Checked Then
                param.Add("@NOT_SAME_BUS", TDB.Checked)
            Else
                param.Add("@NOT_SAME_BUS", DBNull.Value)
            End If

            param.Add("@ALERT_TBD", DBNull.Value)
            param.Add("@ALERT_TBD_UNIT_ID", DBNull.Value)

            If ddtriptype.SelectedValue <> "-1" Then
                param.Add("@TRIP_TYPE", ddtriptype.SelectedValue)
            Else
                param.Add("@TRIP_TYPE", DBNull.Value)
            End If

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "OASIS_GPS"
                .crInstanceName = "172.25.29.51"
                .crUser = "GPS"
                .crPassword = "mango123"
                .reportParameters = param
                .reportPath = Server.MapPath("../Gps_Tracking/Report/CrystalReports/CR_Student_Log2.rpt")
            End With
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Else
            lblmessage.Text = "Please specify any one of the following, <br> Business Unit , Student Number or Student Name."
        End If

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub GridData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridData.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            If CheckPicture.Checked Then
                DirectCast(e.Row.FindControl("img_stud"), Image).Visible = True
            Else
                DirectCast(e.Row.FindControl("img_stud"), Image).Visible = False
            End If
        End If


    End Sub

    Protected Sub GridData_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridData.PageIndexChanged
        CheckboxListValues(GridData, "ch1", "Hiddenid", False)
    End Sub


    Protected Sub SendSms(ByVal sender As Object, ByVal e As System.EventArgs)

        If Hiddentempid.Value <> "" Then
            Dim hash As New Hashtable
            hash = CheckboxListValues(GridData, "ch1", "Hiddenid", True)

            If hash.Count = 0 Then
                Dim ds As DataSet = BindGrind()

                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ID = ds.Tables(0).Rows(i).Item("BARCODE").ToString().Trim()
                    If Not hash.ContainsKey(ID) Then
                        hash.Add(ID, "'" & ID & "'")
                    End If
                Next

            End If

            Dim val = ""
            For Each Item As Object In hash
                If val = "" Then
                    val = Item.Value
                Else
                    val &= "," & Item.Value
                End If

            Next

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@MODULE_TYPE", "GPS")
            pParms(1) = New SqlClient.SqlParameter("@MESSGE_TYPE", "SMS")
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", HiddenBsuid.Value)
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
            pParms(5) = New SqlClient.SqlParameter("@SCHEDULE", "False")
            pParms(6) = New SqlClient.SqlParameter("@UNIQUE_IDS", val)
            pParms(8) = New SqlClient.SqlParameter("@TEMPLATE_ID", Convert.ToInt16(Hiddentempid.Value))
            Dim sendid = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_SEND_MASTER", pParms)
            lblmessage1.Text = "Message sending in progress. Please check the report."
            hash.Clear()
            Session("selection") = hash

        Else
            lblmessage1.Text = "Message template not selected.Please select template."
        End If


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        BindGrade()
    End Sub

    Protected Sub btnReportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReportExcel.Click
        CallReport_New()
        Exit Sub

        lblmessage.Text = ""
        Dim dt As New DataTable
        Dim ds As DataSet = BindGrind()
        If ds.Tables.Count > 0 Then

            dt = ds.Tables(0)
            dt.Columns.Remove("VisibleMSG")
            dt.Columns.Remove("Visible")
            dt.Columns.Remove("SmsVisible")
            dt.Columns.Remove("Next_PK_PositionLogID")
            dt.Columns.Remove("STU_IMAGE")
            dt.Columns.Remove("SMSHistory")
            dt.Columns.Remove("SendSMS")
            dt.Columns.Remove("OutLink")
            dt.Columns.Remove("InLink")
            dt.Columns.Remove("HistoryVisible")
            dt.Columns.Remove("PK_PositionLogID")
            'dt.Columns.Remove("PICK_UP_HAZARD")
            dt.Columns.Remove("DROP_OFF_HAZARD")
            dt.Columns.Remove("PICK_UP_HAZARD")
            'dt.Columns.Remove("DROP_OFF_HAZARD")
            ExportExcel(dt)

        End If

    End Sub


    Protected Sub btnReportExcel_Staff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReportExcel_Staff.Click
        CallReport()
    End Sub

    Sub CallReport()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer
        Dim BSU_ID
        If ddBsu.SelectedValue <> "-1" Then
            BSU_ID = ddBsu.SelectedItem.Value
        Else
            BSU_ID = DBNull.Value
        End If


        Dim pParms(10) As SqlClient.SqlParameter
       
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        pParms(2) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STAFF_LOG_EXPORT", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)
        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT_")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            ' ws.HeadersFooters.AlignWithMargins = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else

        End If

    End Sub


    Sub CallReport_New()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer
        Dim BSU_ID
        If ddBsu.SelectedValue <> "-1" Then
            BSU_ID = ddBsu.SelectedItem.Value
        Else
            BSU_ID = DBNull.Value
        End If



        Dim unitid = ""

        If ddvehRegNo.SelectedValue <> "-1" Then
            unitid = ddvehRegNo.SelectedValue
        Else
            unitid = txtgpsunit.Text.Trim()
        End If


        Dim pParms(20) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())
        If txtstuname.Text.Trim() <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@STU_NAME", txtstuname.Text.Trim())
        End If
        If txtBusNo.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@BUS_NO", txtBusNo.Text.Trim())
        End If
        If ddBsu.SelectedValue <> "-1" Then
            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        End If
        If unitid.ToString().Trim() <> "" Then
            pParms(4) = New SqlClient.SqlParameter("@FK_UnitID", unitid)
        End If
        If txtStudentNumber.Text.Trim() <> "" Then
            pParms(5) = New SqlClient.SqlParameter("@STU_NO", txtStudentNumber.Text.Trim())
        End If

        If DropCheckStatus.SelectedValue <> "-1" Then
            pParms(6) = New SqlClient.SqlParameter("@CHECKSTATUS", DropCheckStatus.SelectedValue)
        End If

        Dim FromHr = "00"
        Dim FromMins = "00"
        Dim ToHr = "23"
        Dim ToMins = "59"

        If ddtrips.SelectedIndex > 0 Then
            Dim ds2 As DataSet = GetTripDetails(ddtrips.SelectedValue)
            FromHr = ds2.Tables(0).Rows(0).Item("TRIP_FROM_HR").ToString()
            FromMins = ds2.Tables(0).Rows(0).Item("TRIP_FROM_MINS").ToString()
            ToHr = ds2.Tables(0).Rows(0).Item("TRIP_TO_HR").ToString()
            ToMins = ds2.Tables(0).Rows(0).Item("TRIP_TO_MINS").ToString()
        End If
        pParms(7) = New SqlClient.SqlParameter("@FromHr", FromHr)
        pParms(8) = New SqlClient.SqlParameter("@FromMins", FromMins)
        pParms(9) = New SqlClient.SqlParameter("@ToHr", ToHr)
        pParms(10) = New SqlClient.SqlParameter("@ToMins", ToMins)

        If ddcardtype.SelectedValue <> "-1" Then
            pParms(11) = New SqlClient.SqlParameter("@CARD_TYPE", ddcardtype.SelectedValue)
        End If

        If ddlGrade.SelectedValue <> "0" Then
            pParms(12) = New SqlClient.SqlParameter("@GRM_DISPLAY", ddlGrade.SelectedItem.Text.Trim())
        End If

        If ddlSection.SelectedValue <> "0" Then
            pParms(13) = New SqlClient.SqlParameter("@SCT_DESCR", ddlSection.SelectedItem.Text.Trim())
        End If


        If ddriskarea.SelectedValue <> "0" Then
            If ddriskarea.SelectedValue = "O" Then
                pParms(14) = New SqlClient.SqlParameter("@O_HAZARD", "True")
            Else
                pParms(14) = New SqlClient.SqlParameter("@R_HAZARD", "True")
            End If
        End If

        If TDB.Checked Then
            pParms(15) = New SqlClient.SqlParameter("@NOT_SAME_BUS", TDB.Checked)
        End If

        If ddtriptype.SelectedValue <> "-1" Then
            pParms(16) = New SqlClient.SqlParameter("@TRIP_TYPE", ddtriptype.SelectedValue)
        End If
        Dim lstrSP As String


        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_STUDENT_LOG_EXPORT_ALL", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)
        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT_")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            ' ws.HeadersFooters.AlignWithMargins = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else

        End If

    End Sub
    Protected Sub btnPushToApp_Click(sender As Object, e As EventArgs) Handles btnPushToApp.Click
        BindStudentIds()
    End Sub
    Public Function BindStudentIds() As DataSet
        lblmessage.Text = ""
        lblmessage1.Text = ""
        Dim StudentNos As String = ""
        Dim ds As New DataSet
        If txtStudentNumber.Text.Trim() <> "" Or ddBsu.SelectedValue <> "-1" Or txtstuname.Text.Trim() <> "" Then
            Dim unitid = ""

            If ddvehRegNo.SelectedValue <> "-1" Then
                unitid = ddvehRegNo.SelectedValue
            Else
                unitid = txtgpsunit.Text.Trim()
            End If

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(20) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())
            If txtstuname.Text.Trim() <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@STU_NAME", txtstuname.Text.Trim())
            End If
            If txtBusNo.Text.Trim() <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@BUS_NO", txtBusNo.Text.Trim())
            End If
            If ddBsu.SelectedValue <> "-1" Then
                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            End If
            If unitid.ToString().Trim() <> "" Then
                pParms(4) = New SqlClient.SqlParameter("@FK_UnitID", unitid)
            End If
            If txtStudentNumber.Text.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@STU_NO", txtStudentNumber.Text.Trim())
            End If

            If DropCheckStatus.SelectedValue <> "-1" Then
                pParms(6) = New SqlClient.SqlParameter("@CHECKSTATUS", DropCheckStatus.SelectedValue)
            End If

            Dim FromHr = "00"
            Dim FromMins = "00"
            Dim ToHr = "23"
            Dim ToMins = "59"

            If ddtrips.SelectedIndex > 0 Then
                Dim ds1 As DataSet = GetTripDetails(ddtrips.SelectedValue)
                FromHr = ds1.Tables(0).Rows(0).Item("TRIP_FROM_HR").ToString()
                FromMins = ds1.Tables(0).Rows(0).Item("TRIP_FROM_MINS").ToString()
                ToHr = ds1.Tables(0).Rows(0).Item("TRIP_TO_HR").ToString()
                ToMins = ds1.Tables(0).Rows(0).Item("TRIP_TO_MINS").ToString()
            End If
            pParms(7) = New SqlClient.SqlParameter("@FromHr", FromHr)
            pParms(8) = New SqlClient.SqlParameter("@FromMins", FromMins)
            pParms(9) = New SqlClient.SqlParameter("@ToHr", ToHr)
            pParms(10) = New SqlClient.SqlParameter("@ToMins", ToMins)

            If ddcardtype.SelectedValue <> "-1" Then
                pParms(11) = New SqlClient.SqlParameter("@CARD_TYPE", ddcardtype.SelectedValue)
            End If

            If ddlGrade.SelectedValue <> "0" Then
                pParms(12) = New SqlClient.SqlParameter("@GRM_DISPLAY", ddlGrade.SelectedItem.Text.Trim())
            End If

            If ddlSection.SelectedValue <> "0" Then
                pParms(13) = New SqlClient.SqlParameter("@SCT_DESCR", ddlSection.SelectedItem.Text.Trim())
            End If


            If ddriskarea.SelectedValue <> "0" Then
                If ddriskarea.SelectedValue = "O" Then
                    pParms(14) = New SqlClient.SqlParameter("@O_HAZARD", "True")
                Else
                    pParms(14) = New SqlClient.SqlParameter("@R_HAZARD", "True")
                End If
            End If

            If TDB.Checked Then
                pParms(15) = New SqlClient.SqlParameter("@NOT_SAME_BUS", TDB.Checked)
            End If

            If ddtriptype.SelectedValue <> "-1" Then
                pParms(16) = New SqlClient.SqlParameter("@TRIP_TYPE", ddtriptype.SelectedValue)
            End If
            Dim lstrSP As String
            If chkOT.Checked Then
                lstrSP = "GPS_GET_BARCODE_LOG_STUDENT_OT"
            Else
                lstrSP = "GPS_GET_BARCODE_LOG_STUDENT"
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, lstrSP, pParms)
            Dim i As Integer = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                StudentNos = StudentNos & "'" + ds.Tables(0).Rows(i)("BARCODE").ToString() + "',"
            Next

            Session("StudentIDs") = StudentNos
            Session("SelBsuid") = ddBsu.SelectedValue
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "text", "ShowMessageForm()", True)


        Else
            lblmessage.Text = "Please specify any one of the following, <br> Business Unit , Student Number or Student Name."
        End If


        Return ds


    End Function



End Class
