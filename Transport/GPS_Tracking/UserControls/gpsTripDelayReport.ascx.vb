﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_UserControls_gpsTripDelayReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            txtDate.Text = Today.ToString("dd/MMM/yyyy")
            BindBsu()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)
    End Sub

    Public Sub BindTrips()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddbsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtrips.DataSource = ds
            ddtrips.DataTextField = "TRIP"
            ddtrips.DataValueField = "GPS_TRIP_ID"
            ddtrips.DataBind()

            Dim list As New ListItem
            list.Text = "Select a Trip"
            list.Value = "-1"

            ddtrips.Items.Insert(0, list)
            ddtrips.Visible = True
        Else
            ddtrips.Visible = False
        End If

    End Sub

    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddbsu.DataSource = dsBsu
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()

            Dim list As New ListItem
            list.Text = "All"
            list.Value = "0"
            ddbsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddbsu.DataSource = dsBsu
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
        End If

        BindTrips()
    End Sub

    Public Function GetTripDetails(ByVal tipid As String) As DataSet

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 5)
        pParms(1) = New SqlClient.SqlParameter("@GPS_TRIP_ID", tipid)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)


        Return ds
    End Function

    Public Sub BindGrid(Optional ByVal export As Boolean = False)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())

        If ddbsu.SelectedValue <> "0" Then
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddbsu.SelectedValue)
        End If

        Dim FromHr = "00"
        Dim FromMins = "00"
        Dim ToHr = "23"
        Dim ToMins = "59"

        If ddtrips.SelectedIndex > 0 Then
            Dim ds1 As DataSet = GetTripDetails(ddtrips.SelectedValue)
            FromHr = ds1.Tables(0).Rows(0).Item("TRIP_FROM_HR").ToString()
            FromMins = ds1.Tables(0).Rows(0).Item("TRIP_FROM_MINS").ToString()
            ToHr = ds1.Tables(0).Rows(0).Item("TRIP_TO_HR").ToString()
            ToMins = ds1.Tables(0).Rows(0).Item("TRIP_TO_MINS").ToString()
        End If
        pParms(2) = New SqlClient.SqlParameter("@FromHr", FromHr)
        pParms(3) = New SqlClient.SqlParameter("@FromMins", FromMins)
        pParms(4) = New SqlClient.SqlParameter("@ToHr", ToHr)
        pParms(5) = New SqlClient.SqlParameter("@ToMins", ToMins)


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_TRIP_DELAY_REPORT", pParms)

        GridBusListing.DataSource = ds
        GridBusListing.DataBind()

        If export Then
            Dim dt As DataTable
            dt = ds.Tables(0)
            ExportExcel(dt)
        End If

    End Sub


    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        BindGrid()
    End Sub

    Protected Sub GridBusListing_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridBusListing.PageIndexChanging
        GridBusListing.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        BindGrid(True)
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindTrips()
    End Sub
End Class
