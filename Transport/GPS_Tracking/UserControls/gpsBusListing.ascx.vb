Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class Transport_GPS_Tracking_UserControls_gpsBusListing
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenRedirect.Value = 0
            HiddenBsuid.Value = Session("sBsuid")
            HiddenUserId.Value = Session("sUsr_name")
            BindBsu()
            BindGrid()
        End If

        If GridBusListing.Rows.Count > 0 Then
            Dim BtnTrackShow As Button = DirectCast(GridBusListing.FooterRow.FindControl("BtnTrackShow"), Button)

            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(BtnTrackShow)
        End If
    End Sub

    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddbsu.DataSource = dsBsu
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()

            Dim list As New ListItem
            list.Text = "All"
            list.Value = "0"
            ddbsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddbsu.DataSource = dsBsu
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
        End If


    End Sub

    Public Sub BindGrid()
        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim pParms(5) As SqlClient.SqlParameter

        If ddbsu.SelectedValue <> 0 Then
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddbsu.SelectedValue)
        End If

        If txtvehicleid.Text.Trim() <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@VEH_REGNO", txtvehicleid.Text.Trim())
        End If

        If txtgpsunit.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@UNIT_ID", txtgpsunit.Text.Trim())
        End If

        If txtbusno.Text.Trim() <> "" Then
            pParms(3) = New SqlClient.SqlParameter("@BUS_NO", txtbusno.Text.Trim())
        End If

        pParms(4) = New SqlClient.SqlParameter("@OPTION", 5)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)
        GridBusListing.DataSource = ds
        GridBusListing.DataBind()

        lbltotal.Text = "Total : " & ds.Tables(0).Rows.Count

    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged

        BindGrid()

    End Sub

    Protected Sub GridBusListing_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridBusListing.PageIndexChanging
        Dim hash As New Hashtable
        If Session("Vhashtable") Is Nothing Then
        Else
            hash = Session("Vhashtable")
        End If
        ''Dim i = 0
        For Each row As GridViewRow In GridBusListing.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("CheckTrack"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("HiddenUnitId"), HiddenField)
            Dim key = GridBusListing.PageIndex & "_" & Hid.Value
            If ch.Checked Then
                If hash.ContainsKey(key) Then
                Else
                    hash.Add(key, Hid.Value)
                End If

            Else

                If hash.ContainsKey(key) Then
                    hash.Remove(key)
                Else
                End If
            End If
        Next

        Session("Vhashtable") = hash

        GridBusListing.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub BtnTrackShow_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As New Hashtable

        If Session("Vhashtable") Is Nothing Then
        Else
            hash = Session("Vhashtable")
        End If

        For Each row As GridViewRow In GridBusListing.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("CheckTrack"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("HiddenUnitId"), HiddenField)
            Dim key = GridBusListing.PageIndex & "_" & Hid.Value
            If ch.Checked Then
                If hash Is Nothing Then
                    hash.Add(key, Hid.Value)
                End If

                If hash.Contains(key) Then
                Else
                    hash.Add(key, Hid.Value)
                End If

            Else

                If hash.ContainsKey(key) Then
                    hash.Remove(key)
                Else
                End If
            End If
        Next

        Session("Vhashtable") = hash

        HiddenRedirect.Value = 1

    End Sub

    Protected Sub GridBusListing_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridBusListing.PageIndexChanged

        Dim hash As New Hashtable

        If Session("Vhashtable") Is Nothing Then
        Else
            hash = Session("Vhashtable")
        End If

        For Each row As GridViewRow In GridBusListing.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("CheckTrack"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("HiddenUnitId"), HiddenField)
            Dim key = GridBusListing.PageIndex & "_" & Hid.Value


            If hash.Contains(key) Then
                ch.Checked = True
            Else
                ch.Checked = False
            End If

        Next

    End Sub

    Protected Sub GridBusListing_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridBusListing.RowCommand
        Try

       
            If e.CommandName = "Location" Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@FK_UnitID", e.CommandArgument)
                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_LAST_PK_LOG_ID", pParms)


                Dim lat, lng As String


                If ds.Tables(0).Rows.Count > 0 Then
                    lat = ds.Tables(0).Rows(0).Item("Latitude").ToString()
                    lng = ds.Tables(0).Rows(0).Item("Longitude").ToString()

                    Dim ReturnValue As String = ""


                    Dim ServicePath = "http://ws.geonames.org/findNearbyPlaceName?lat=" & lat & "&lng=" & lng & ""
                    Dim myWebClient As New System.Net.WebClient
                    Dim responseArray As Byte() = myWebClient.DownloadData(ServicePath)
                    ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)
                    Dim startindex = ReturnValue.IndexOf("<name>")
                    Dim endindex = ReturnValue.IndexOf("</name>")
                    ReturnValue = ReturnValue.Substring(startindex + 6, (endindex - (startindex + 6)))
                    Label1.Text = ReturnValue.Replace("?", "")
                Else
                    Label1.Text = "Unit Information not available in our database."
                End If

                
                MO1.Show()
            End If

            If e.CommandName = "speed" Then

                Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
                Dim str_query = ""
                For Each row As GridViewRow In GridBusListing.Rows

                    Dim unitid = DirectCast(row.FindControl("HiddenUnitId"), HiddenField).Value
                    Dim speed = DirectCast(row.FindControl("txtspeed"), TextBox).Text.Trim()

                    If speed <> "" Then
                        str_query = "UPDATE VEHICLE_M set VEH_SPEED_LIMIT='" & speed & "' WHERE VEH_UNITID='" & unitid & "'"
                    Else
                        str_query = "UPDATE VEHICLE_M set VEH_SPEED_LIMIT=NULL WHERE VEH_UNITID='" & unitid & "'"
                    End If

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                Next
                BindGrid()
                lblmessage.Text = "Speed limit updated successfully..."

            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        BindGrid()
    End Sub

End Class
