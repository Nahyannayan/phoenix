﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsStudentGradeColorCode.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsStudentGradeColorCode" %>
<%@ Register namespace="Telerik.Web.UI" tagprefix="UI" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
   <script type="text/javascript">

       function client_OnTreeNodeChecked() {
           var obj = window.event.srcElement;
           var treeNodeFound = false;
           var checkedState;
           if (obj.tagName == "INPUT" && obj.type == "checkbox") {
               var treeNode = obj;
               checkedState = treeNode.checked;
               do {
                   obj = obj.parentElement;
               } while (obj.tagName != "TABLE")
               var parentTreeLevel = obj.rows[0].cells.length;
               var parentTreeNode = obj.rows[0].cells[0];
               var tables = obj.parentElement.getElementsByTagName("TABLE");
               var numTables = tables.length
               if (numTables >= 1) {
                   for (i = 0; i < numTables; i++) {
                       if (tables[i] == obj) {
                           treeNodeFound = true;
                           i++;
                           if (i == numTables) {
                               return;
                           }
                       }
                       if (treeNodeFound == true) {
                           var childTreeLevel = tables[i].rows[0].cells.length;
                           if (childTreeLevel > parentTreeLevel) {
                               var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                               var inputs = cell.getElementsByTagName("INPUT");
                               inputs[0].checked = checkedState;
                           }
                           else {
                               return;
                           }
                       }
                   }
               }
           }
       }

       function change_chk_state(chkThis) {
           var ids = chkThis.id

           var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
           var state = false
           if (chkThis.checked) {
               state = true
           }
           else {
               state = false
           }

           for (i = 0; i < document.forms[0].elements.length; i++) {
               var currentid = document.forms[0].elements[i].id;
               if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                   document.forms[0].elements[i].checked = state;
               }
           }
           return false;
       }


       function uncheckall(chkThis) {
           var ids = chkThis.id
           var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
           var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
           var state = false
           if (chkThis.checked) {
               state = true
           }
           else {
               state = false
           }
           var uncheckflag = 0

           for (i = 0; i < document.forms[0].elements.length; i++) {
               var currentid = document.forms[0].elements[i].id;
               if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                   if (currentid.indexOf(value) == -1) {
                       if (document.forms[0].elements[i].checked == false) {
                           uncheckflag = 1
                       }
                   }
               }
           }

           if (uncheckflag == 1) {
               // uncheck parent
               for (i = 0; i < document.forms[0].elements.length; i++) {
                   var currentid = document.forms[0].elements[i].id;
                   if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                       document.forms[0].elements[i].checked = false;
                   }
               }
           }
           else {
               // Check parent
               for (i = 0; i < document.forms[0].elements.length; i++) {
                   var currentid = document.forms[0].elements[i].id;
                   if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                       document.forms[0].elements[i].checked = true;
                   }
               }
           }


           return false;
       }
       function change_chk_stateg(chkThis) {
           var chk_state = !chkThis.checked;
           for (i = 0; i < document.forms[0].elements.length; i++) {
               var currentid = document.forms[0].elements[i].id;
               if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                   document.forms[0].elements[i].checked = chk_state;
                   document.forms[0].elements[i].click();
               }
           }
       }
    </script>

<style type="text/css">
    .rcpRoundedRight {
        /*height: -webkit-fill-available !important;*/
    }
    .RadColorPicker ul, .RadColorPicker ul li {
        padding:0 5px !important;
        display:table-cell !important;
    }

</style>

<table width="100%">
    
    <tr>
        <td align="left">
          
          <table width="100%">
    <tr>
        <td align="left" width="20%">
            <span class="field-label">Business Unit</span></td>
        
        <td align="left" width="30%">
            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True" >
            </asp:DropDownList>
        </td>
        <td align="left" width="20%">
            <span class="field-label">Academic Year</span></td>
        
        <td align="left" width="30%">
            <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True" >
            </asp:DropDownList>
        </td>
    </tr>
    <tr id="trCurr" runat="server">
        <td align="left" width="20%">
            <span class="field-label">Curriculum</span></td>        
        <td align="left" width="30%">
            <asp:DropDownList ID="ddlCLM_ID" runat="server" AutoPostBack="True" >
            </asp:DropDownList>
        </td>
        <td align="left" width="20%"></td>
        <td align="left" width="30%"></td>
    </tr>
    <tr id="trshf" runat="server">
        <td align="left" width="20%">
            <span class="field-label">Shift</span></td>
        
        <td align="left" width="20%">
            <asp:DropDownList ID="ddlSHF_ID" runat="server" AutoPostBack="True" >
            </asp:DropDownList>
        </td>
    </tr>
    <tr id="trstm" runat="server">
        <td align="left" width="20%">
            <span class="field-label">Stream</span></td>
       
        <td align="left" width="30%">
            <asp:DropDownList ID="ddlSTM_ID" runat="server" AutoPostBack="True" >
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td align="left" width="20%">
           <span class="field-label"> Grade &amp; Section</span></td>
        
        <td align="left" width="20%">
            <div class="checkbox-list">
            <asp:Panel ID="plGrade" runat="server">
                <asp:TreeView ID="tvGrade" runat="server" ExpandDepth="1" 
                    onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="All">
                    <DataBindings>
                        <asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand" 
                            TextField="Text" ValueField="ValueField" />
                    </DataBindings>
                </asp:TreeView>
            </asp:Panel>
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" width="20%"></td>
        <td align="left" width="30%">
           <telerik:RadColorPicker ID="RadColorPicker5" runat="server" PaletteModes="HSV" />
        </td>
        <td align="left" width="20%"></td>
        <td align="left" width="30%"></td>
    </tr>
    <tr>
        <td colspan="4" align="center">
            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Update" />
        </td>
    </tr>
    <tr>
        <td colspan="4" align="center">
            <asp:Label ID="lblmessage" runat="server" cssclass="text-danger" Text=""></asp:Label>
        </td>
    </tr>
</table>
          
          </td>
    </tr>
</table>