<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsUnitChecking.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsUnitChecking" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core JavaScript-->
<script src="../../../vendor/jquery/jquery.min.js"></script>
<script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../../cssfiles/Popup.css" rel="stylesheet" />



<div>

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Enter GPS Unit ID</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">GPS Unit</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtunitid" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Only Barcode</span></td>
                        <td>
                            <asp:CheckBox ID="Checkbarcode" runat="server" CssClass="field-label" />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>

                        <td align="center" colspan="4">
                            <asp:Button ID="BtnInfo" runat="server" CssClass="button" Text="Get Information" />
                            &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Export to Excel" />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Total Data In Temp : </span>
                <asp:Label ID="lbltemp" runat="server" CssClass="field-label"></asp:Label></td>
        </tr>
    </table>

    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
        Format="dd/MMM/yyyy" PopupButtonID="txtdate" TargetControlID="txtdate">
    </ajaxToolkit:CalendarExtender>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Unit Data Information</td>
        </tr>

        <tr>
            <td align="left">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>

        <tr>
            <td align="left">
                <asp:GridView ID="GridInfo" runat="server" CssClass="table table-bordered table-row" AllowPaging="True"
                    EmptyDataText="Infomation not available for entered unit id. Please Configure the GPS device correctly and try again later." AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Unit ID
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%#Eval("FK_UnitID")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Date Time
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%#Eval("PositionLogDateTime")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Latitude 
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                               <%# Eval("Latitude") %> 
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Longitude
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                           <%#Eval("Longitude")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Speed
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                            <%# Eval("Speed") %></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Report ID
                                    
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                            <%# Eval("Report") %></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                BarCode 
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                            <%#Eval("BarCode")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>


</div>
