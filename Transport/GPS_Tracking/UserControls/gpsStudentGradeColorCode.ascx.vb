﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Drawing
Partial Class Transport_GPS_Tracking_UserControls_gpsStudentGradeColorCode
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBsu()
        End If
    End Sub


    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            'Dim list As New ListItem
            'list.Text = "Select Business Unit"
            'list.Value = "-1"
            'ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        ddBsu_SelectedIndexChanged(ddBsu, Nothing)




    End Sub

    Sub acadamicyear_bind()


        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim BSU_ID As String = ddBsu.SelectedItem.Value
        Dim currACY_ID As String = String.Empty
        str_Sql = "SELECT  distinct   ACADEMICYEAR_D.ACD_ACY_ID, ACADEMICYEAR_M.ACY_DESCR," & _
" (select top 1 ACD_ACY_ID from ACADEMICYEAR_D where ACD_BSU_ID = '" & BSU_ID & "' and acd_current=1) as CurrACY_ID " & _
" FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID " & _
" = ACADEMICYEAR_M.ACY_ID WHERE(ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')" & _
" ORDER BY ACADEMICYEAR_D.ACD_ACY_ID "

        Using readerAcademic As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If readerAcademic.HasRows = True Then
                While readerAcademic.Read
                    ddlACD_ID.Items.Add(New ListItem(readerAcademic("ACY_DESCR"), readerAcademic("ACD_ACY_ID")))
                    currACY_ID = readerAcademic("CurrACY_ID")
                End While
            End If
        End Using
        ddlACD_ID.ClearSelection()
        ddlACD_ID.Items.FindByValue(currACY_ID).Selected = True
        ddlACD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)

    End Sub

    Protected Sub ddlACD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACD_ID.SelectedIndexChanged
        CURRICULUM_BSU_4_Student()
    End Sub

    Sub CURRICULUM_BSU_4_Student()
 

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
        Dim BSU_ID As String = ddBsu.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_Count As Integer
            str_Sql = "SELECT DISTINCT ACADEMICYEAR_D.ACD_CLM_ID, CURRICULUM_M.CLM_DESCR," & _
"  (select count(distinct acd_clm_id) from academicyear_d where acd_acy_id " & _
" =ACADEMICYEAR_D.ACD_ACY_ID and acd_bsu_id= '" & BSU_ID & "') as tot_count " & _
" FROM ACADEMICYEAR_D INNER JOIN  CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = " & _
" CURRICULUM_M.CLM_ID WHERE(ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "') And " & _
" (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')"
            ddlCLM_ID.Items.Clear()

            Using readerCLM As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerCLM.HasRows = True Then
                    While readerCLM.Read
                        ddlCLM_ID.Items.Add(New ListItem(readerCLM("CLM_DESCR"), readerCLM("ACD_CLM_ID")))
                        CLM_Count = readerCLM("tot_count")
                    End While
                End If
            End Using
            If CLM_Count > 1 Then
                ddlCLM_ID.Items.Add(New ListItem("ALL", "0"))
                ddlCLM_ID.ClearSelection()
                ddlCLM_ID.Items.FindByValue("0").Selected = True
            Else

            End If
            If ddlCLM_ID.Items.Count = 1 Then
                trCurr.Visible = False
            Else
                trCurr.Visible = True
            End If

            ddlCLM_ID_SelectedIndexChanged(ddlCLM_ID, Nothing)
       
    End Sub


    Protected Sub ddlCLM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCLM_ID.SelectedIndexChanged
        bind_Shift()
        bind_Stream()
        BindTree_GradeSection()

    End Sub
    Sub bind_Shift()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim BSU_ID As String = ddBsu.SelectedValue
        Dim ACY_ID As String = ddlACD_ID.SelectedValue
        Dim CLM_ID As String = ddlCLM_ID.SelectedValue
        ddlSHF_ID.Items.Clear()
        If CLM_ID <> "0" Then
            str_Sql = " SELECT   distinct  SHIFTS_M.SHF_ID, SHIFTS_M.SHF_DESCR " & _
" FROM  SHIFTS_M INNER JOIN  GRADE_BSU_M ON SHIFTS_M.SHF_ID = GRADE_BSU_M.GRM_SHF_ID " & _
" and GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d " & _
" where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "')"

            Using readerSHF As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerSHF.HasRows = True Then
                    While readerSHF.Read
                        ddlSHF_ID.Items.Add(New ListItem(readerSHF("SHF_DESCR"), readerSHF("SHF_ID")))
                    End While
                End If
            End Using
            If ddlSHF_ID.Items.Count > 1 Then
                ddlSHF_ID.Items.Add(New ListItem("ALL", "0"))
                ddlSHF_ID.ClearSelection()
                ddlSHF_ID.Items.FindByValue("0").Selected = True
            End If

        Else
            ddlSHF_ID.Items.Add(New ListItem("ALL", "0"))
            ddlSHF_ID.ClearSelection()
            ddlSHF_ID.Items.FindByValue("0").Selected = True
        End If

        If ddlSHF_ID.Items.Count = 1 Then
            trshf.Visible = False
        Else
            trshf.Visible = True
        End If


    End Sub

    Sub bind_Stream()

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddBsu.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_ID As String = ddlCLM_ID.SelectedValue
            ddlSTM_ID.Items.Clear()
            If CLM_ID <> "0" Then
                str_Sql = "SELECT DISTINCT STREAM_M.STM_ID, STREAM_M.STM_DESCR " & _
               " FROM GRADE_BSU_M INNER JOIN  STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID " & _
               " and GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d " & _
               " where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "')"


                Using readerSTM As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    If readerSTM.HasRows = True Then
                        While readerSTM.Read
                            ddlSTM_ID.Items.Add(New ListItem(readerSTM("STM_DESCR"), readerSTM("STM_ID")))
                        End While
                    End If
                End Using
                If ddlSTM_ID.Items.Count > 1 Then
                    ddlSTM_ID.Items.Add(New ListItem("ALL", "0"))
                    ddlSTM_ID.ClearSelection()
                    ddlSTM_ID.Items.FindByValue("0").Selected = True
                End If

            Else
                ddlSTM_ID.Items.Add(New ListItem("ALL", "0"))
                ddlSTM_ID.ClearSelection()
                ddlSTM_ID.Items.FindByValue("0").Selected = True

            End If

            If ddlSTM_ID.Items.Count = 1 Then
                trstm.Visible = False
            Else
                trstm.Visible = True
            End If

      
    End Sub


    Sub BindTree_GradeSection()
        Dim dsData As DataSet = Nothing
        Dim dtTable As DataTable = Nothing

        Dim ACY_ID As String = ddlACD_ID.SelectedValue
        Dim BSU_ID As String = ddBsu.SelectedValue
        Dim CLM_ID As String = ddlCLM_ID.SelectedValue
        Dim SHF_ID As String = ddlSHF_ID.SelectedValue
        Dim STM_ID As String = ddlSTM_ID.SelectedValue
        Dim SHF_STM As String = String.Empty
        SHF_STM = " AND GRADE_BSU_M.GRM_ID<>'' "
        If SHF_ID <> "0" Then
            SHF_STM += " AND GRADE_BSU_M.GRM_SHF_ID = '" & SHF_ID & "'"
        End If
        If STM_ID <> "0" Then
            SHF_STM += " AND GRADE_BSU_M.GRM_STM_ID = '" & STM_ID & "'"

        End If



        If CLM_ID <> "0" Then

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = " with CTE_GRD (GRD_DESCR, SCT_DESCR, GRD_ID, GRM_ID,SCT_ID,DISPLAYORDER)" & _
 "  AS ( SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR,  " & _
 "  GRADE_BSU_M.GRM_GRD_ID as  GRD_ID,GRM_ID,SECTION_M.SCT_ID as SCT_ID,GRADE_M.GRD_DISPLAYORDER as " & _
 " DISPLAYORDER FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = " & _
 " SECTION_M.SCT_ACD_ID AND  GRADE_BSU_M.GRM_ID = SECTION_M.SCT_GRM_ID INNER JOIN  " & _
 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID  where  GRADE_BSU_M.GRM_ACD_ID= " & _
 " (select acd_id from academicyear_d where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and " & _
 " acd_bsu_id='" & BSU_ID & "') AND SECTION_M.SCT_DESCR<>'TEMP'  " & SHF_STM & ") " & _
 " SELECT GRD_ID,GRD_DESCR, SCT_ID,SCT_DESCR,GRM_ID FROM   CTE_GRD ORDER BY DISPLAYORDER"




                dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            End Using
            If Not dsData Is Nothing Then
                dtTable = dsData.Tables(0)
            End If

            Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("All Grades", "ALL")
            Dim drGRD_DESCR As DataRow
            For i As Integer = 0 To dtTable.Rows.Count - 1
                drGRD_DESCR = dtTable.Rows(i)
                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                        contains = True
                    End If
                End While
                Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRM_ID"))
                If contains Then
                    Continue For
                End If
                'Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
                'drGRD_DESCR("GRD_DESCR") & "'"
                'Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
                'Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
                'While (ienumGRADE_SECT.MoveNext())
                '    Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                '    Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                '    trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
                'End While
                trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
            Next

            tvGrade.Nodes.Clear()
            tvGrade.Nodes.Add(trSelectAll)
            tvGrade.DataBind()

        Else
            Dim trGRDAll As New TreeNode("All Grades", "ALL")
            tvGrade.Nodes.Clear()
            tvGrade.Nodes.Add(trGRDAll)
            tvGrade.DataBind()


        End If

    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        Call acadamicyear_bind()
    End Sub

    Protected Sub ddlSHF_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHF_ID.SelectedIndexChanged
        BindTree_GradeSection()
    End Sub

    Protected Sub ddlSTM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTM_ID.SelectedIndexChanged
        BindTree_GradeSection()
    End Sub


    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = ""
        Dim color As String = ColorTranslator.ToHtml(RadColorPicker5.SelectedColor)

        For Each node As TreeNode In tvGrade.CheckedNodes
            If Not node Is Nothing Then

                If node.Value <> "ALL" Then
                    str_Sql = "UPDATE GRADE_BSU_M SET GRM_COLOR_CODE='" & color & "' WHERE GRM_ID='" & node.Value & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_Sql)
                    lblmessage.text = "Update successfully"
                End If


            End If
        Next


    End Sub

End Class
