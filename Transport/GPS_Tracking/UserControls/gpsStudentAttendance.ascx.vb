﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_UserControls_gpsStudentAttendance
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuid.Value = Session("sBsuid")
            acadamicyear_bind()
            BindBsu()
            BinndDate()
            BindMinSec()

        End If

    End Sub

    Sub acadamicyear_bind()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddBsu.SelectedValue
            Dim currACY_ID As String = String.Empty
            str_Sql = "SELECT  distinct   ACADEMICYEAR_D.ACD_ACY_ID, ACADEMICYEAR_M.ACY_DESCR," & _
" (select top 1 ACD_ACY_ID from ACADEMICYEAR_D where ACD_BSU_ID = '" & BSU_ID & "' and acd_current=1) as CurrACY_ID " & _
" FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID " & _
" = ACADEMICYEAR_M.ACY_ID WHERE(ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')" & _
" ORDER BY ACADEMICYEAR_D.ACD_ACY_ID "

            Using readerAcademic As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerAcademic.HasRows = True Then
                    While readerAcademic.Read
                        ddAccYear.Items.Add(New ListItem(readerAcademic("ACY_DESCR"), readerAcademic("ACD_ACY_ID")))
                        currACY_ID = readerAcademic("CurrACY_ID")
                    End While
                End If
            End Using
            ddAccYear.ClearSelection()
            ddAccYear.Items.FindByValue(currACY_ID).Selected = True

        Catch ex As Exception

        End Try
    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindVehReg()


    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, " select CONVERT(VARCHAR,VEH_REGNO) + ' -- ' +  CONVERT(VARCHAR,VEH_UNITID)  as DATA , VEH_ID from  OASIS_TRANSPORT.dbo.VW_VEH_DRIVER WHERE VEH_ALTO_BSU_ID='" & ddBsu.SelectedValue & "' ORDER BY VEH_REGNO , VEH_UNITID")

        If ds.Tables(0).Rows.Count > 0 Then
            ddvehRegNo.DataTextField = "DATA"
            ddvehRegNo.DataValueField = "VEH_ID"
            ddvehRegNo.DataSource = ds
            ddvehRegNo.DataBind()
        End If
        Dim list As New ListItem
        list.Text = "Vehicle"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)

        BindTrip()

    End Sub


    Public Sub BindTrip()
        ddTripNo.Items.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim qyery = " select TRP_ID, BNO_DESCR + ' - ' + TRP_DESCR AS TRIPDES from OASIS_TRANSPORT.TRANSPORT.VV_TRIPS_D where VEH_ID='" & ddvehRegNo.SelectedValue & "' AND TRP_JOURNEY='" & ddJourney.SelectedValue & "' ORDER BY BNO_DESCR , TRP_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, qyery)
        If ds.Tables(0).Rows.Count > 0 Then
            ddTripNo.DataTextField = "TRIPDES"
            ddTripNo.DataValueField = "TRP_ID"
            ddTripNo.DataSource = ds
            ddTripNo.DataBind()
        End If
        Dim list As New ListItem
        list.Text = "Vehicle Trips"
        list.Value = "-1"
        ddTripNo.Items.Insert(0, list)

    End Sub

    Public Sub BinndDate()
        'txtDate.Text = Today.ToString("dd/MMM/yyyy")
    End Sub
    Public Sub BindMinSec()
        ''Bind Mins
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                DropFromHr.Items.Insert(i, "0" & i.ToString())
                DropToHr.Items.Insert(i, "0" & i.ToString())
            Else
                DropFromHr.Items.Insert(i, i)
                DropToHr.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                DropFromMins.Items.Insert(i, "0" & i.ToString())
                DropToMins.Items.Insert(i, "0" & i.ToString())
            Else
                DropFromMins.Items.Insert(i, i)
                DropToMins.Items.Insert(i, i)
            End If
        Next

        DropToHr.SelectedValue = "23"
        DropToMins.SelectedValue = "59"

    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        acadamicyear_bind()
        BindVehReg()

    End Sub

    Protected Sub ddvehRegNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddvehRegNo.SelectedIndexChanged
        BindTrip()

    End Sub

    Protected Sub ddJourney_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddJourney.SelectedIndexChanged
        BindTrip()

    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click

        BindGrid()

    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        Dim pParms(12) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@STU_BSU_ID", ddBsu.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@ACD_ACY_ID", ddAccYear.SelectedValue)

        If txtStudentNumber.Text <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@STU_NO", txtStudentNumber.Text.Trim())
        End If

        If txtStudentName.Text <> "" Then
            pParms(3) = New SqlClient.SqlParameter("@STU_NAME", txtStudentName.Text.Trim())
        End If

        If ddJourney.SelectedValue <> "-1" Then
            If ddJourney.SelectedValue = "Onward" Then
                If ddTripNo.SelectedValue <> "-1" Then
                    pParms(4) = New SqlClient.SqlParameter("@STU_PICKUP_TRP_ID", ddTripNo.SelectedValue)
                End If
            End If
            If ddJourney.SelectedValue = "Return" Then
                If ddTripNo.SelectedValue <> "-1" Then
                    pParms(5) = New SqlClient.SqlParameter("@STU_DROPOFF_TRP_ID", ddTripNo.SelectedValue)
                End If
            End If
        End If

        If ddvehRegNo.SelectedValue <> "-1" Then
            pParms(6) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
        End If

        pParms(7) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())
        pParms(8) = New SqlClient.SqlParameter("@FromHr", DropFromHr.SelectedValue)
        pParms(9) = New SqlClient.SqlParameter("@FromMins", DropFromMins.SelectedValue)
        pParms(10) = New SqlClient.SqlParameter("@ToHr", DropToHr.SelectedValue)
        pParms(11) = New SqlClient.SqlParameter("@ToMins", DropToMins.SelectedValue)


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BARCODE_LOG_TRIP", pParms)

        GridData.DataSource = ds
        GridData.DataBind()



    End Sub

    Protected Sub ddAccYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAccYear.SelectedIndexChanged

    End Sub

    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        GridData.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
