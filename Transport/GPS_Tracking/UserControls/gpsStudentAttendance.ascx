﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsStudentAttendance.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsStudentAttendance" %>
<%@ Register Src="usrStudentdetails.ascx" TagName="usrStudentdetails" TagPrefix="uc1" %>


<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" class="title-bg-lite">Student Barcode Trip Attendance
        </td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
                <tr>
                    <td align="left" width="20%"><span class="field-label">Business Unit</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Vehicle Reg No.</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddvehRegNo" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddAccYear" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Journey</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddJourney" runat="server" AutoPostBack="True">
                            <asp:ListItem Text="Select Journey" Value="-1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Onward" Value="Onward"></asp:ListItem>
                            <asp:ListItem Text="Return" Value="Return"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Student Number</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtStudentNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Date</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Student Name</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Time From</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="DropFromHr" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropFromMins" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Time To</span>
                    </td>

                    <td align="left" width="30%">
                        <table width="100%">
                            <tr>
                                <td align="left" width="50%">
                                    <asp:DropDownList ID="DropToHr" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left" width="50%">
                                    <asp:DropDownList ID="DropToMins" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                        </table>


                    </td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Trip</span>
                    </td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddTripNo" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="btnview" runat="server" CssClass="button" Text="View Attendance" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Student Barcode Trip Attendance
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridData" CssClass="table table-bordered table-row" AutoGenerateColumns="false" Width="100%" EmptyDataText="<center>No Records Found</center>"
                runat="server" AllowPaging="True" PageSize="10">
                <Columns>

                    <asp:TemplateField HeaderText="Student Details">
                        <ItemTemplate>
                            <uc1:usrStudentdetails ID="usrStudentdetails1" StudentDetails_ID='<%#Eval("stu_id")%>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Pick Up">
                        <ItemTemplate>
                            <%#Eval("PICK_UP")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Drop Off">
                        <ItemTemplate>
                            <%#Eval("DROP_OFF")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Known Status">
                        <ItemTemplate>
                            <center>
                                <table>
                                    <tr>
                                        <td>
                                            <%#Eval("VEH_REGNO")%>
                                            <br />
                                            <span style="font-size: x-small">(
                                                <%#Eval("BSU_SHORTNAME")%>
                                                -
                                                <%#Eval("FK_UnitID")%>
                                                )</span>
                                        </td>
                                        <td>
                                            <%#Eval("ENTRY_DATE")%>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="LinkMap" OnClientClick='<%#Eval("link")%>' CausesValidation="false"
                                                Visible='<%#Eval("VisibleData")%>' runat="server">Map</asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image1" ImageUrl='<%#Eval("STATUS")%>' Visible='<%#Eval("VisibleData")%>'
                                                Width="30px" Height="30px" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle />
                <RowStyle CssClass="griditem" />
                <SelectedRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
                <EmptyDataRowStyle />
                <EditRowStyle />
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuid" runat="server" />
<ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtDate"
    PopupPosition="TopLeft" TargetControlID="txtDate">
</ajaxToolkit:CalendarExtender>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddBsu"
    Display="None" ErrorMessage="Please Select Business Unit" SetFocusOnError="True"
    InitialValue="-1"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
    Display="None" ErrorMessage="Please Enter a Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" />


