﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Transport_GPS_Tracking_UserControls_gpsTripConsolidatedReport_
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExport)
        If Not IsPostBack Then
            BindBsu()
            txtdate.Text = Today.Date.ToString("dd/MMM/yyyy")
            BindGrid(False)
        End If
    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindTrips()

    End Sub
    Public Sub BindTrips()
        ddtrips.Items.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtrips.DataSource = ds
            ddtrips.DataTextField = "TRIP"
            ddtrips.DataValueField = "GPS_TRIP_ID"
            ddtrips.DataBind()

        End If

        Dim list As New ListItem
        list.Text = "Select a Trip"
        list.Value = "-1"
        ddtrips.Items.Insert(0, list)

    End Sub
    Public Sub BindGrid(ByVal export As Boolean)


        If IsDate(txtdate.Text) Then
            Dim unitid = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", txtdate.Text.Trim())

            If ddBsu.SelectedValue <> "-1" Then
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            End If
            If ddtrips.SelectedValue <> "-1" Then
                pParms(2) = New SqlClient.SqlParameter("@GPS_TRIP_ID", ddtrips.SelectedValue)
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_TRIP_CONSOLIDATED_RPT_LEVEL_1", pParms)
            GridData.DataSource = ds
            GridData.DataBind()

            If export Then
                Dim dt As DataTable = ds.Tables(0)
                dt.Columns.Remove("GPS_TRIP_ID")
                dt.Columns.Remove("viewdetails")
                ' Create excel file.
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
                Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                '  ws.HeadersFooters.AlignWithMargins = True
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
                'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                Dim pathSave As String
                pathSave = "Data.xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()




                'ef.SaveXls(cvVirtualPath & pathSave)
                'Dim path = cvVirtualPath & pathSave
                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()

            End If

        End If

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        BindGrid(False)
    End Sub

    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        GridData.PageIndex = e.NewPageIndex
        BindGrid(False)
    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindTrips()
    End Sub


    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        BindGrid(True)
    End Sub
End Class
