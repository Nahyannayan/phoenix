﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsStudentInAlertRefresh.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskMonitor" %>
<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<script type="text/javascript">

    window.setTimeout('loadpage();', 20000);

    function loadpage() {
        if (document.getElementById("<%=cp.ClientID %>").checked == false) {
            window.location = window.location
        }
        else {
            window.setTimeout('loadpage();', 20000);
        }
    }
</script>--%>

<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="/cssfiles/Popup.css" rel="stylesheet" />



<div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Student- Status In Monitoring
                <asp:CheckBox ID="cp" Text="Pause" Visible="false" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridTaskList" runat="server" AllowPaging="True" PageSize="20" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                    EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Task ID">
                            <HeaderTemplate>
                                STU ID
                                            <br />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                      <%# Eval("STU_NO")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BSU">
                            <HeaderTemplate>
                                BSU                                         
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("BSU_SHORTNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority">
                            <HeaderTemplate>
                                Name                                         
                                      
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("STU_NAME")%>
                                <br />
                                <%# Eval("GRM_DISPLAY")%>/  <%#Eval("SCT_DESCR")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Route Tab">
                            <HeaderTemplate>
                                Contact Name                                           
                                     
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("PARENTNAME")%>
                                <br />
                                <%# Eval("STU_EMGCONTACT")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                                Vehicle
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("VEH_REGNO")%>
                                    <br />
                                     <%# Eval("ONW")%>/ <%# Eval("RET")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                                Driver Info
                                           
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("Driver")%> - <%# Eval("DrivMob")%>
                                <br />
                                <%# Eval("Conductor")%> - <%# Eval("CndMob")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Action
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                 <asp:CheckBox ID="CheckAction" runat="server" />
                                 <asp:HiddenField ID="HiddenLogpkid" Value='<%# Eval("PK_PositionLogID") %>'  runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                    <HeaderStyle />
                    <RowStyle CssClass="griditem" />
                    <SelectedRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <EmptyDataRowStyle />
                    <EditRowStyle />
                </asp:GridView>
                <asp:TextBox ID="txtaction" runat="server" Style="min-width: 10% !important; width: 10% !important;"></asp:TextBox>
                <asp:Button ID="btnaction" runat="server" Text="Save Action" />
                <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenEmpId" runat="server" />
</div>
