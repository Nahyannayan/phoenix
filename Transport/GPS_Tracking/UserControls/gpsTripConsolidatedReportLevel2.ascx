﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsTripConsolidatedReportLevel2.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsTripConsolidatedReportLevel2" %>

    <script type="text/javascript">

        function ViewDetails(a, b, c, d) {

            window.open('gpsTripConsolidatedReportLevel3.aspx?Tripid=' + a + '&Date=' + b + '&Bsu=' + c + '&uniid=' + d, '', 'dialogHeight:500px;dialogWidth:500px;scroll:auto;resizable:no;'); return false;


        }

    
    
    </script>

    <br />
    <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export" 
    Width="100px" />
    <br />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" class="title-bg-lite">
            List of Buses
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridData" runat="server" AllowPaging="True" EmptyDataText="No data found" AutoGenerateColumns="false"
                Width="100%"  CssClass="table table-bordered table-row">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Veh Reg.
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("VEH_REGNO")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                           
                                        Onward/Return No
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("ONW")%>/
                                <%# Eval("RET")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        GpsId
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("fk_unitid")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                           
                                        Total Students
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkShow" runat="server" OnClientClick='<%#Eval("viewdetails") %>' Text='<%#Eval("total_students") %>'></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem"  Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
