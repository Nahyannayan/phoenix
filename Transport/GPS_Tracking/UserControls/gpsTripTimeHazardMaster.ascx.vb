﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_UserControls_gpsTripTimeHazardMaster
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
            BindVehReg()
            BindTrip(ddtrip)
            BindArea(ddarea)
            BindPickupDropName(ddpickdrop)

        End If

    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            'Dim list As New ListItem
            'list.Text = "Select Business Unit"
            'list.Value = "-1"
            'ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If



    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        If ddBsu.SelectedValue <> "-1" Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter


            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 10)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                ddvehRegNo.DataTextField = "veh_regno"
                ddvehRegNo.DataValueField = "VEH_ID"
                ddvehRegNo.DataSource = ds
                ddvehRegNo.DataBind()
            End If
        End If

        Dim list As New ListItem
        list.Text = "Vehicle"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)

       
    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        If ddBsu.SelectedValue <> "-1" Then
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        End If
        If ddvehRegNo.SelectedValue <> "-1" Then
            pParms(2) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
        End If
        If ddtrip.SelectedValue <> "-1" Then
            pParms(3) = New SqlClient.SqlParameter("@TRIP_ID", ddtrip.SelectedValue)
        End If
        If ddarea.SelectedValue <> "-1" Then
            pParms(4) = New SqlClient.SqlParameter("@AREA_ID", ddarea.SelectedValue)
        End If
        If ddpickdrop.SelectedValue <> "-1" Then
            pParms(5) = New SqlClient.SqlParameter("@PICK_ID", ddpickdrop.SelectedValue)
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRIP_ACTIVITY_MASTER_TRAN", pParms)
        GridInfo.DataSource = ds
        GridInfo.DataBind()


    End Sub


    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        lblmessage.Text = ""
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindVehReg()
    End Sub

    Protected Sub ddvehRegNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddvehRegNo.SelectedIndexChanged
        BindTrip(ddtrip)
        BindArea(ddarea)
        BindPickupDropName(ddpickdrop)
    End Sub

    Protected Sub ddtrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtrip.SelectedIndexChanged
        BindArea(ddarea)
        BindPickupDropName(ddpickdrop)
    End Sub

    Protected Sub ddarea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddarea.SelectedIndexChanged
        BindPickupDropName(ddpickdrop)
    End Sub


    Public Sub BindTrip(ByVal trip As DropDownList)
        trip.Items.Clear()

        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@OPTION", 14)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            trip.DataTextField = "TRIPNAME"
            trip.DataValueField = "TRIPID"
            trip.DataSource = ds
            trip.DataBind()
        End If

        Dim list As New ListItem
        list.Text = "Trips"
        list.Value = "-1"
        trip.Items.Insert(0, list)

    End Sub


    Public Sub BindArea(ByVal Area As DropDownList)
        Area.Items.Clear()

        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@TRIP_ID", ddtrip.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@OPTION", 15)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            Area.DataTextField = "AREA"
            Area.DataValueField = "AREAID"
            Area.DataSource = ds
            Area.DataBind()
        End If

        Dim list As New ListItem
        list.Text = "Area"
        list.Value = "-1"
        Area.Items.Insert(0, list)
    End Sub

    Public Sub BindPickupDropName(ByVal PickupDrop As DropDownList)
        PickupDrop.Items.Clear()

        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(5) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@VEH_ID", ddvehRegNo.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@TRIP_ID", ddtrip.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@AREA_ID", ddarea.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@OPTION", 16)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            PickupDrop.DataTextField = "PICKUPNAME"
            PickupDrop.DataValueField = "PICKUPID"
            PickupDrop.DataSource = ds
            PickupDrop.DataBind()
        End If

        Dim list As New ListItem
        list.Text = "Pick/Drop"
        list.Value = "-1"
        PickupDrop.Items.Insert(0, list)

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        lblmessage.Text = ""
        BindGrid()
    End Sub

    Public Function Validatetime(ByVal time As String) As Boolean
        Return IsDate(time)
    End Function


    Public Sub savedata(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""

        For Each row As GridViewRow In GridInfo.Rows

            Dim tripid = ""
            Dim trip_start_time As TextBox
            Dim trip_end_time As TextBox

            Dim pickup_id = ""
            Dim pickup_time As TextBox
            Dim hazard = ""

            trip_start_time = DirectCast(row.FindControl("txttripstarttime"), TextBox)
            trip_end_time = DirectCast(row.FindControl("txttripendtime"), TextBox)

            If DirectCast(row.FindControl("txttripstarttime"), TextBox).Visible Then

                tripid = DirectCast(row.FindControl("Hiddentripid"), HiddenField).Value
                trip_start_time.BackColor = Drawing.Color.White
                trip_end_time.BackColor = Drawing.Color.White

                If Validatetime(trip_start_time.Text.Trim()) And Validatetime(trip_end_time.Text.Trim()) Then

                    Dim t1 As DateTime = Convert.ToDateTime(trip_start_time.Text.Trim())
                    Dim t2 As DateTime = Convert.ToDateTime(trip_end_time.Text.Trim())


                    If t2.TimeOfDay.Ticks > t1.TimeOfDay.Ticks Then
                        str_query = "update TRANSPORT.TRIPS_M  set TRP_FROM_TIME='" & trip_start_time.Text.Trim() & "' WHERE TRP_ID='" & tripid & "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                        str_query = "update TRANSPORT.TRIPS_M  set TRP_TO_TIME='" & trip_end_time.Text.Trim() & "' WHERE TRP_ID='" & tripid & "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Else
                        trip_start_time.BackColor = Drawing.Color.Yellow
                        trip_end_time.BackColor = Drawing.Color.Yellow
                    End If

                Else
                    trip_start_time.BackColor = Drawing.Color.Red
                    trip_end_time.BackColor = Drawing.Color.Red
                End If


            End If

            pickup_time = DirectCast(row.FindControl("txtpdtime"), TextBox)

            If Validatetime(trip_start_time.Text.Trim()) And Validatetime(trip_end_time.Text.Trim()) Then
                Dim t1 As DateTime = Convert.ToDateTime(trip_start_time.Text.Trim())
                Dim t2 As DateTime = Convert.ToDateTime(trip_end_time.Text.Trim())


                pickup_id = DirectCast(row.FindControl("Hiddenpickupid"), HiddenField).Value
                hazard = DirectCast(row.FindControl("CheckHazard"), CheckBox).Checked

                pickup_time.BackColor = Drawing.Color.White
                If Validatetime(pickup_time.Text.Trim()) Then
                    Dim t3 As DateTime = Convert.ToDateTime(pickup_time.Text.Trim())
                    If t3 > t1 And t3 < t2 Then
                        str_query = "update TRANSPORT.TRIPS_PICKUP_S  set TPP_TIME='" & pickup_time.Text.Trim() & "' WHERE TPP_ID='" & pickup_id & "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Else
                        pickup_time.BackColor = Drawing.Color.Yellow
                    End If


                Else
                    pickup_time.BackColor = Drawing.Color.Red
                End If

            Else

                pickup_time.BackColor = Drawing.Color.Red

            End If



            str_query = "update TRANSPORT.TRIPS_PICKUP_S  set TPP_HAZARD ='" & hazard & "' WHERE TPP_ID='" & pickup_id & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)




        Next
        lblmessage.Text = "Updation done succesfully except the following," & _
                          "<br>Note:If textbox marked in red, then please enter valid time." & _
                          "<br>If textbox marked in yellow, then please enter valid time range"

        'BindGrid()

    End Sub

End Class
