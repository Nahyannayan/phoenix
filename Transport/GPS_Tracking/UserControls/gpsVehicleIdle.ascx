﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsVehicleIdle.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsVehicleIdle" %>


<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->
<div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Get Idle Report
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Idle Time</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtidle" runat="server"></asp:TextBox>
                            Mins
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>

                        <td align="center" colspan="4">
                            <asp:Button ID="btnview" runat="server" CssClass="button" Text="Show" />
                            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Vehicle Idling
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" AutoGenerateColumns="false" PageSize="20" CssClass="table table-bordered table-row"
                    EmptyDataText="Information not available." Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Unit ID
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:Label ID="Label1" runat="server" Visible='<%# Eval("visible")%>' Text='<%# Eval("GPS_UNIT_ID")%>'></asp:Label>  
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Veh RegNo
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                              <asp:Label ID="Label2" runat="server" Visible='<%# Eval("visible")%>' Text='<%# Eval("VEH_REGNO")%>'></asp:Label>  
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Info
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Panel ID="P1" Visible='<%# Eval("visible")%>' runat="server">
                                    <%# Eval("DRIVER")%>
                                    <br />
                                    <%# Eval("CONDUCTOR")%>
                                    <br />
                                    <%# Eval("BSU_SHORTNAME")%>
                                </asp:Panel>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Trip
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("TRIP") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Trip Start Time
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("StartDate")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Trip End Time
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("EndTime") %>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Mins
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("mindiff") %>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>


</div>
<ajaxToolkit:FilteredTextBoxExtender ID="F1" runat="server"
    FilterType="Numbers" TargetControlID="txtidle">
</ajaxToolkit:FilteredTextBoxExtender>
<ajaxToolkit:CalendarExtender ID="CL1" runat="server" Format="dd/MMM/yyyy"
    PopupButtonID="txtdate" TargetControlID="txtdate">
</ajaxToolkit:CalendarExtender>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
    ControlToValidate="ddBsu" Display="None" ErrorMessage="Please select Bsu"
    InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
    ControlToValidate="txtDate" Display="None" ErrorMessage="Please enter date"
    SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
    ControlToValidate="txtidle" Display="None" ErrorMessage="Please enter mins"
    SetFocusOnError="True"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server"
    ShowMessageBox="True" ShowSummary="False" />

