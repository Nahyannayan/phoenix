﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_UserControls_gpsLogCountView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            txtDate.Text = Today.ToString("dd/MMM/yyyy")
            HiddenBsuid.Value = Session("sBsuid")
            BindBsu()
        End If
    End Sub


    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindTrips()
       
    End Sub


    Public Sub BindTrips()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtrips.DataSource = ds
            ddtrips.DataTextField = "TRIP"
            ddtrips.DataValueField = "GPS_TRIP_ID"
            ddtrips.DataBind()

            Dim list As New ListItem
            list.Text = "Select a Trip"
            list.Value = "-1"

            ddtrips.Items.Insert(0, list)
            ddtrips.Visible = True
        Else
            ddtrips.Visible = False
        End If





    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@DATE", txtDate.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        If ddtrips.SelectedIndex > 0 Then
            pParms(2) = New SqlClient.SqlParameter("@GPS_TRIP_ID", ddtrips.SelectedValue)
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_BSU_STU_BARCODE_COUNT", pParms)

        GridData.DataSource = ds
        GridData.DataBind()

        lblmessage.Text = "Total Vehicle : " & ds.Tables(0).Rows.Count

        If ds.Tables(0).Rows.Count > 0 Then
            'lblmessage.Text &= ", Total Students : " & ds.Tables(0).Compute("SUM(TOTAL)", String.Empty)
            lblmessage.Text &= ", In : " & ds.Tables(0).Compute("SUM(STU_IN)", String.Empty)
            lblmessage.Text &= ", Out : " & ds.Tables(0).Compute("SUM(STU_OUT)", String.Empty)
            lblmessage.Text &= ", Still On Board : " & ds.Tables(0).Compute("SUM(SOB)", String.Empty)
            lblmessage.Text &= ", Defects : " & ds.Tables(0).Compute("SUM(DEFECT)", String.Empty)
        End If

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        BindGrid()
    End Sub


    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        GridData.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


    Protected Sub btnView0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView0.Click


        Dim header = ""

        header = "BSU :-" & ddBsu.SelectedItem.Text & ",  Date :- " & txtDate.Text.Trim()

        If ddtrips.SelectedIndex > 0 Then
            header &= ", Trip :- " & ddtrips.SelectedItem.Text
        End If

        Dim param As New Hashtable

        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@DATE", txtDate.Text.Trim())
        param.Add("@BSU_ID", ddBsu.SelectedValue)

        If ddtrips.SelectedIndex > 0 Then
            param.Add("@GPS_TRIP_ID", ddtrips.SelectedValue)
        Else
            param.Add("@GPS_TRIP_ID", DBNull.Value)
        End If

        param.Add("@Header", header)
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_GPS"
            .crInstanceName = "172.25.29.51"
            .crUser = "GPS"
            .crPassword = "mango123"
            .reportParameters = param
            .reportPath = Server.MapPath("../Gps_Tracking/Report/CrystalReports/CR_Bsu_Barcode_Count.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindTrips()
    End Sub
End Class
