<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsBusListing.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsBusListing" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="/cssfiles/Popup.css" rel="stylesheet" />


<script type="text/javascript">

    function MapShow(unitid) {

        //window.showModalDialog('../GPS_Tracking/Map/GoogleMap.aspx?Unit_Id=' + unitid +'&Date='+ date , '','dialogHeight:1024px;dialogWidth:800px;scroll:no;resizable:no;'); return false;
        var User = document.getElementById("<%=HiddenUserId.ClientID %>").value;
        //window.showModalDialog('../GPS_Tracking/Map/gpsGetInfo.aspx?UnitId=' + unitid + '&User=' + User, '', 'dialogHeight:250px;dialogWidth:250px;scroll:no;resizable:no;'); return false;
        return ShowWindowWithClose('../GPS_Tracking/Map/gpsGetInfo.aspx?UnitId=' + unitid + '&User=' + User, 'search', '55%', '85%')
        return false;

    }
    function AssignGeofencing(unitid) {

        //window.showModalDialog('../GPS_Tracking/Map/GoogleMap.aspx?Unit_Id=' + unitid +'&Date='+ date , '','dialogHeight:1024px;dialogWidth:800px;scroll:no;resizable:no;'); return false;
        var User = document.getElementById("<%=HiddenUserId.ClientID %>").value;
        //window.open('../GPS_Tracking/Map/gpsAssignGeoFencing.aspx?UnitId=' + unitid + '&User=' + User, '', 'dialogHeight:1024px;dialogWidth:768px;scroll:no;resizable:no;'); return false;
        return ShowWindowWithClose('../GPS_Tracking/Map/gpsAssignGeoFencing.aspx?UnitId=' + unitid + '&User=' + User, 'search', '55%', '85%')
        return false;
    }

    function ViewGeofencing(unitid) {

        //window.showModalDialog('../GPS_Tracking/Map/GoogleMap.aspx?Unit_Id=' + unitid +'&Date='+ date , '','dialogHeight:1024px;dialogWidth:800px;scroll:no;resizable:no;'); return false;
        var User = document.getElementById("<%=HiddenUserId.ClientID %>").value;
        //window.open('../GPS_Tracking/Map/gpsViewGeoFencing.aspx?UnitId=' + unitid + '&User=' + User, '', 'dialogHeight:1024px;dialogWidth:768px;scroll:no;resizable:no;'); return false;
        return ShowWindowWithClose('../GPS_Tracking/Map/gpsViewGeoFencing.aspx?UnitId=' + unitid + '&User=' + User, 'search', '55%', '85%')
        return false;
    }

    function spare(unitid) {

        //window.showModalDialog('../GPS_Tracking/gpsSpare.aspx?UnitId=' + unitid, '', 'dialogHeight:200px;dialogWidth:600px;scroll:no;resizable:no;'); return false;

        //window.showModalDialog('../GPS_Tracking/gpsSpare.aspx?UnitId=' + unitid, "", "dialogWidth:650px; dialogHeight:400px; center:yes");
        return ShowWindowWithClose('../GPS_Tracking/gpsSpare.aspx?UnitId=' + unitid, 'search', '55%', '85%')
        return false;
    }

    function openPopUp() {

        //window.open('../GPS_Tracking/Map/gpsTrackVechile.aspx', '', 'dialogHeight:1024px;dialogWidth:768px;scroll:no;resizable:no;'); return false;
        return ShowWindowWithClose('../GPS_Tracking/Map/gpsTrackVechile.aspx', 'search', '55%', '85%')
        return false;
    }

    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckTrack") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>

<div>

    <asp:LinkButton ID="LinkSearch" runat="server" OnClientClick="javascript:return false;">Search </asp:LinkButton>
    <asp:Panel ID="Panel1" runat="server">
        <table width="100%">
            <tr>
                <td align="left" width="20%"><span class="field-label">Bus No</span></td>

                <td align="left" width="30%">
                    <asp:TextBox ID="txtbusno" runat="server"></asp:TextBox>
                </td>
                <td align="left" width="20%"><span class="field-label">Reg No.</span></td>

                <td align="left" width="30%">
                    <asp:TextBox ID="txtvehicleid" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" width="20%"><span class="field-label">GPS Unit</span></td>

                <td align="left" width="30%">
                    <asp:TextBox ID="txtgpsunit" runat="server"></asp:TextBox></td>
                <td align="left" colspan="2">
                    <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="Search" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkSearch" Collapsed="true"
        CollapsedSize="0" CollapsedText="Search" ExpandControlID="LinkSearch" ExpandedSize="50"
        ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1" TextLabelID="LinkSearch">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite" colspan="4">GPS Tracking </td>
        </tr>
        <tr>
            <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

            <td align="left" width="30%">
                <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
            <td align="left" colspan="2">
                <asp:Label ID="lbltotal" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GridBusListing" AutoGenerateColumns="false" ShowFooter="true" PageSize="10" AllowPaging="true" runat="server" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Track">
                            <HeaderTemplate>
                                <center> <asp:CheckBox ID="chkAll"  runat="server"  onclick="javascript:change_chk_stateg(this);"
                         ToolTip="Click here to select/deselect all rows" /></center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:CheckBox ID="CheckTrack"  runat="server" />
                            </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                            <asp:Button ID="BtnTrackShow" runat="server"  OnClick="BtnTrackShow_Click" CssClass="button" Text="Show" />
                        </center>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reg&nbsp;No">
                            <ItemTemplate>
                                <center>
                                <%# Eval("VEH_REGNO") %>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bus&nbsp;Type">
                            <ItemTemplate>
                                <center>
                                <%# Eval("CAT_DESCRIPTION") %>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Business&nbsp;Unit">
                            <ItemTemplate>
                                <center>
                                <%#Eval("BSU_SHORTNAME")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver">
                            <ItemTemplate>
                                <%#Eval("Driver")%>
                                <br />
                                <center>
                                <%#Eval("DrivMob")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Conductor">
                            <ItemTemplate>
                                <%# Eval("Conductor") %>
                                <br />
                                <center>
                                <%#Eval("CndMob")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GPS&nbsp;Unit">
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenUnitId" Value='<%# Eval("VEH_UNITID") %>' runat="server" />
                                <center>
                                <%# Eval("VEH_UNITID") %>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Onward">
                            <ItemTemplate>
                                <center>
                                <%# Eval("ONW")%>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Return">
                            <ItemTemplate>
                                <center>
                                <%# Eval("RET") %>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Speed&nbsp;Limit">
                            <ItemTemplate>

                                <center>
                                <asp:TextBox ID="txtspeed" Width="50px" Text='<%# Eval("VEH_SPEED_LIMIT") %>' runat="server"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="F1"  FilterType="Numbers" TargetControlID="txtspeed" runat="server"></ajaxToolkit:FilteredTextBoxExtender> 
                            </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                         <asp:Button ID="btnspeed" runat="server"  CommandName="speed" CssClass="button" Text="Set" />
                        </center>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Set">
                            <ItemTemplate>

                                <center>
                            <%# Eval("SPARE_REG") %> -   <%# Eval("SPARE_BSU") %>
                            <br />
                            <asp:LinkButton ID="Linkspare"  OnClientClick='<%# Eval("spare") %>' runat="server">Spare</asp:LinkButton>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>

                                <center>
                            <asp:LinkButton ID="LinkView" CommandArgument='<%# Eval("VEH_UNITID") %>' CommandName="Location" runat="server">View</asp:LinkButton>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--<asp:TemplateField HeaderText="Track&nbsp;Path">
                        <ItemTemplate>
                            <center>
                               
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Geo&nbsp;Fencing">
                            <ItemTemplate>
                                <center>
                                <%--<asp:LinkButton ID="lnkAssign" Text="Assign" OnClientClick='<%# Eval("AssignGeofencing") %>' runat="server"></asp:LinkButton>--%>
                                 <asp:LinkButton ID="lnkView" Text="Assign " OnClientClick='<%# Eval("MapShow") %>' runat="server"></asp:LinkButton>
                               <br />
                          
                                <asp:LinkButton ID="lnkGeofnc" Text="View" OnClientClick='<%# Eval("ViewGeofencing") %>' runat="server"></asp:LinkButton>
                            </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle />
                    <RowStyle CssClass="griditem" />
                    <SelectedRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <EmptyDataRowStyle />
                    <EditRowStyle />
                </asp:GridView>
                <br />
                <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenUserId" runat="server" />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />

    <asp:HiddenField ID="HiddenRedirect" runat="server" />

    <script type="text/javascript">

        if (document.getElementById("<%=HiddenRedirect.ClientID %>").value == '1') {
            openPopUp();
        }


    </script>

    <asp:Label ID="Label2"
        runat="server"></asp:Label>

    <asp:Panel ID="PanelShow" runat="server"
        CssClass="panel-cover" Style="display: none" Width="512">

        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Location of the Bus</td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label1" runat="server" CssClass="field-label"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><%--<asp:Button ID="btnok" CssClass="button" runat="server" Text="Ok" Width="80px" ValidationGroup="s" OnClick="btnok_Click" />--%><asp:Button ID="btncancel" runat="server" Text="OK" CssClass="button"></asp:Button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelShow"
        RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label2">
    </ajaxToolkit:ModalPopupExtender>
</div>


<script type="text/javascript" lang="javascript">
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });

            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            }
        });

        return false;
    }
</script>
