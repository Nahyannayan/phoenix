﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsLogCountView.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsLogCountView" %>
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
<%--<style type="text/css">
    .style1 {
        width: 100%;
    }
</style>--%>
<div>
    <table cellpadding="5" cellspacing="0" width="100%">
        <%--<tr>
        <td class="subheader_img">
            Student Log Daily Summary
        </td>
    </tr>--%>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td width="20%"><span class="field-label">Business Unit</span>
                        </td>

                        <td width="30%">
                            <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td width="20%"><span class="field-label">Date</span>
                        </td>

                        <td width="30%">
                            <asp:TextBox ID="txtDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtDate"
                                PopupPosition="TopLeft" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                  
                    <tr>
                        <td width="20%"><span class="field-label">Trips</span>
                        </td>

                        <td width="30%">
                            <asp:DropDownList ID="ddtrips" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>

                        <td colspan="4" align="center">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" />
                            <asp:Button ID="btnView0" runat="server" CssClass="button" Text="Report" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <br />

    <table cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg">Daily Summary :-
            <asp:Label ID="lblmessage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridData" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    EmptyDataText="&lt;center&gt;No Records Found&lt;/center&gt;" PageSize="60" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Reg No">
                            <ItemTemplate>
                                <center>
                         <%# Eval("VEH_REGNO")%>
                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <center>
                          <%# Eval("CAT_DESCRIPTION")%>
                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver">
                            <ItemTemplate>
                                <%# Eval("Driver")%>
                                <br />
                                <%# Eval("DrivMob")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Conductor">
                            <ItemTemplate>
                                <%# Eval("Conductor")%>
                                <br />
                                <%# Eval("CndMob")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="BusNo<br>(onw/ret)">
                            <ItemTemplate>
                                <center>
                            <%#Eval("BusNos1")%>
                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="In">
                            <ItemTemplate>
                                <center>
                            <%#Eval("STU_IN")%>
                        
                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Out">
                            <ItemTemplate>
                                <center>
                            <%# Eval("STU_OUT")%>
                        
                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Still&nbsp;on&nbsp;Board">
                            <ItemTemplate>
                                <center>
                            <%#Eval("SOB")%>
                        
                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Defect">
                            <ItemTemplate>
                                <center>
                           <%# Eval("DEFECT_DISPLAY")%>
                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <SelectedRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
        ControlToValidate="ddBsu" Display="None" ErrorMessage="Please Specify BSU"
        InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
        ControlToValidate="txtDate" Display="None" ErrorMessage="Please Enter Date"
        SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"
        ShowMessageBox="True" ShowSummary="False" />

</div>
