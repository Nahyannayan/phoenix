﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdTaskMonitor
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            monitor()
        End If

    End Sub

    Public Sub monitor()
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString()
            Dim pParms(2) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Request.QueryString("Bsu").Trim())
            pParms(1) = New SqlClient.SqlParameter("@FK_UnitID", Request.QueryString("vehid"))

            Dim ds As DataSet = Nothing

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BARCODE_LOG_STUDENT_IN_ALERT_REFRESH", pParms)


            GridTaskList.DataSource = ds
            GridTaskList.DataBind()

      
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub GridTaskList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskList.PageIndexChanging
        GridTaskList.PageIndex = e.NewPageIndex
        monitor()
    End Sub


    Protected Sub btnaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnaction.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        'code change to handle EmployeeID
        Dim EmployeeId As String = Session("EmployeeId")
        For Each row As GridViewRow In GridTaskList.Rows
            Dim check As CheckBox = DirectCast(row.FindControl("CheckAction"), CheckBox)
            If check.Checked Then
                Dim logid = DirectCast(row.FindControl("HiddenLogpkid"), HiddenField).Value
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@LCD_RECORD_ID", 0)
                pParms(1) = New SqlClient.SqlParameter("@REMARKS", txtaction.Text.Trim())
                pParms(2) = New SqlClient.SqlParameter("@TYPE", "STU_IN")
                pParms(3) = New SqlClient.SqlParameter("@ACTION_BY", EmployeeId) 'Request.QueryString("EmployeeId"))
                pParms(4) = New SqlClient.SqlParameter("@PK_LOG_ID", logid)
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_ACTION_LOG", pParms)

            End If

        Next
        monitor()
    End Sub

End Class
