Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports SmsService


Partial Class Transport_GPS_Tracking_UserControls_SentDetails
    Inherits System.Web.UI.UserControl

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Session("Vhashtable") = Nothing
            HiddenShowFlag.Value = 0
            Label1.Text = ""

            Dim str_conn1 = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString


            Dim query = "select * from TRANSPORT.vw_OSO_STU_BUS " & _
                        " where stu_bsu_id ='" & DropBsu.SelectedValue & " ' and stu_fee_id='" & txtfeeid.Text.Trim() & "' "

            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, query)

            Dim UnitId
            Dim message
            If ds1.Tables(0).Rows.Count > 0 Then
                UnitId = ds1.Tables(0).Rows(0).Item("veh_unitid").ToString()

                message = "Fee ID: " & ds1.Tables(0).Rows(0).Item("stu_fee_id").ToString()
                message = message & ",Name : " & ds1.Tables(0).Rows(0).Item("stu_pasprtname").ToString()
                message = message & ",Bus No. " & ds1.Tables(0).Rows(0).Item("onwardbus").ToString()

            End If



            If UnitId <> "" Then
                Dim str_query = "select Longitude,Latitude from IntelliXDB_V2.dbo.PositionLog where " & _
                                           " PK_PositionLogID=(select max(PK_PositionLogID) from IntelliXDB_V2.dbo.PositionLog " & _
                                           "  where FK_UnitID='" & UnitId & "') "


                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                Dim lat, lng As String


                If ds.Tables(0).Rows.Count > 0 Then
                    lat = ds.Tables(0).Rows(0).Item("Latitude").ToString()
                    lng = ds.Tables(0).Rows(0).Item("Longitude").ToString()
                End If

                Dim ReturnValue As String = ""


                Dim ServicePath = "http://ws.geonames.org/findNearbyPlaceName?lat=" & lat & "&lng=" & lng & ""
                ''Dim ServicePath = "http://ws.geonames.org/findNearbyWikipedia?lat=" & lat & "&lng=" & lng & ""
                Dim myWebClient As New System.Net.WebClient
                Dim responseArray As Byte() = myWebClient.DownloadData(ServicePath)
                ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)
                Dim startindex = ReturnValue.IndexOf("<name>")
                Dim endindex = ReturnValue.IndexOf("</name>")
                ReturnValue = ReturnValue.Substring(startindex + 6, (endindex - (startindex + 6)))


                message = message & " Current Location : " & ReturnValue


                sms.SendMessage(txtmobilenumber.Text.Trim(), message, "STS-Helpdesk", "gemseducation", "manoj")
                HiddenUnitid.Value = UnitId
                HiddenShowFlag.Value = 1

                Dim hash As New Hashtable

                Dim key = "Id"
                hash.Add(key, UnitId)

                Session("Vhashtable") = hash

            Else
                Label1.Text = "Bus info not Found.Please check the fee ID "
            End If


        Catch ex As Exception

        End Try


    End Sub

    Public Sub bINDBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim query = "select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        DropBsu.DataSource = ds
        DropBsu.DataTextField = "BSU_NAME"
        DropBsu.DataValueField = "BSU_ID"

        DropBsu.DataBind()



    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bINDBsu()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(Button1)
    End Sub
End Class
