﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class Transport_GPS_Tracking_UserControls_gpsAttendanceNonCard
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Tsave.Visible = False
            BinndDate()
            BindBsu()
            BindBsulog()
            BindMinSec()


        End If

    End Sub

    Public Sub BindGrade()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query = ""
        query = " select DISTINCT GRM_DISPLAY from VW_STUDTRANSPORTINFO where stu_bsu_id='" & ddBsusearch.SelectedValue & "' ORDER BY GRM_DISPLAY "

        ddGrade.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        ddGrade.DataTextField = "GRM_DISPLAY"
        ddGrade.DataValueField = "GRM_DISPLAY"
        ddGrade.DataBind()

        Dim list As New ListItem
        list.Text = "Select Grade"
        list.Value = "-1"
        ddGrade.Items.Insert(0, list)

        BindSection()
        BindBusNo()
    End Sub

    Public Sub BindSection()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query = ""
        query = " select DISTINCT SCT_DESCR from VW_STUDTRANSPORTINFO where stu_bsu_id='" & ddBsusearch.SelectedValue & "' AND GRM_DISPLAY='" & ddGrade.SelectedValue & "' ORDER BY SCT_DESCR "

        ddSection.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        ddSection.DataTextField = "SCT_DESCR"
        ddSection.DataValueField = "SCT_DESCR"
        ddSection.DataBind()

        Dim list As New ListItem
        list.Text = "Select Section"
        list.Value = "-1"
        ddSection.Items.Insert(0, list)

    End Sub

    Public Sub BindBusNo()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query = ""
        query = " select DISTINCT ONWARDBUS from VW_STUDTRANSPORTINFO where stu_bsu_id='" & ddBsusearch.SelectedValue & "' ORDER BY ONWARDBUS "
        ddbus1.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        ddbus1.DataTextField = "ONWARDBUS"
        ddbus1.DataValueField = "ONWARDBUS"
        ddbus1.DataBind()

        query = " select DISTINCT RETURNBUS  from VW_STUDTRANSPORTINFO where stu_bsu_id='" & ddBsusearch.SelectedValue & "' ORDER BY RETURNBUS  "
        ddbus2.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        ddbus2.DataTextField = "RETURNBUS"
        ddbus2.DataValueField = "RETURNBUS"
        ddbus2.DataBind()

        Dim list As New ListItem
        list.Text = "--"
        list.Value = "-1"
        ddbus1.Items.Insert(0, list)

        Dim list2 As New ListItem
        list2.Text = "--"
        list2.Value = "-1"
        ddbus2.Items.Insert(0, list2)

    End Sub


    Public Sub BindMinSec()
        ''Bind Mins
        Dim i = 0
        For i = 0 To 23
            If i < 10 Then
                ddTHrs.Items.Insert(i, "0" & i.ToString())
            Else
                ddTHrs.Items.Insert(i, i)
            End If

        Next

        For i = 0 To 59
            If i < 10 Then
                ddTmin.Items.Insert(i, "0" & i.ToString())
            Else
                ddTmin.Items.Insert(i, i)
            End If
        Next

        Dim list As New ListItem
        list.Text = "--"
        list.Value = "-1"
        ddTHrs.Items.Insert(0, list)
        ddTmin.Items.Insert(0, list)

    End Sub
    Public Sub BindBsulog()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        BindVehReg()

    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        If ddBsu.SelectedValue <> "-1" Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 10)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                ddvehRegNo.DataTextField = "ONWRET"
                ddvehRegNo.DataValueField = "VEH_UNITID"
                ddvehRegNo.DataSource = ds
                ddvehRegNo.DataBind()
            End If

        End If

        Dim list As New ListItem
        list.Text = "Onward/Return"
        list.Value = "-1"
        ddvehRegNo.Items.Insert(0, list)



    End Sub
    Public Sub BinndDate()
        txtDate.Text = Today.ToString("dd/MMM/yyyy")
    End Sub

    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsusearch.DataSource = dsBsu
            ddBsusearch.DataTextField = "BSU_NAME"
            ddBsusearch.DataValueField = "BSU_ID"
            ddBsusearch.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsusearch.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsusearch.DataSource = dsBsu
            ddBsusearch.DataTextField = "BSU_NAME"
            ddBsusearch.DataValueField = "BSU_ID"
            ddBsusearch.DataBind()
        End If

        BindGrade()

    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query = ""
        Dim ds As New DataSet



        If CheckAbsent.Checked Then
            query = " Select * from VW_STUDTRANSPORTINFO A LEFT join " & _
                " (SELECT DISTINCT isnull(BARCODE,'') COLLATE Latin1_General_CI_AI as BARCODELOG FROM IntelliXDB_V2.dbo.PositionLog) b ON  a.STU_NO=b.BARCODELOG " & _
                " where isnull(BARCODELOG,'')='' "
        Else
            query = "Select * from VW_STUDTRANSPORTINFO WHERE 1=1 "
        End If


        If ddBsusearch.SelectedValue <> "-1" Then
            query &= " AND STU_BSU_ID='" & ddBsusearch.SelectedValue & "'"
        End If

        If txtstudentno.Text <> "" Then
            query &= " AND STU_NO LIKE '%" & txtstudentno.Text.Trim() & "%'"
        End If

        If txtname.Text <> "" Then
            query &= " AND STU_NAME LIKE '%" & txtname.Text.Trim() & "%'"
        End If

        If ddGrade.SelectedValue <> "-1" Then
            query &= " AND GRM_DISPLAY LIKE '%" & ddGrade.SelectedValue & "%'"
        End If

        If ddSection.SelectedValue <> "-1" Then
            query &= " AND SCT_DESCR LIKE '%" & ddSection.SelectedValue & "%'"
        End If

        If txtpickup.Text <> "" Then
            query &= " AND PICKUP LIKE '%" & txtpickup.Text.Trim() & "%'"
        End If

        If txtdropup.Text <> "" Then
            query &= " AND DROPOFF LIKE '%" & txtdropup.Text.Trim() & "%'"
        End If

        If ddbus1.SelectedValue <> "-1" Then
            query &= " AND ONWARDBUS LIKE '%" & ddbus1.SelectedValue & "%'"
        End If

        If ddbus2.SelectedValue <> "-1" Then
            query &= " AND RETURNBUS LIKE '%" & ddbus2.SelectedValue & "%'"
        End If


        query &= " order by pickup,onwardbus,dropoff,returnbus "

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        GridInfo.DataSource = ds
        GridInfo.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then

            For Each row As GridViewRow In GridInfo.Rows

                If ddevent.SelectedItem.Text = "ALL" Then
                    DirectCast(row.FindControl("txtremarks"), TextBox).Text = ""
                Else
                    DirectCast(row.FindControl("txtremarks"), TextBox).Text = ddevent.SelectedItem.Text
                End If
            Next

            Tsave.Visible = True
        Else
            Tsave.Visible = False
        End If

    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindVehReg()
    End Sub

    Public Function ValidateC() As Boolean
        Dim rval = True
        lblmessage.Text = ""

        If ddBsu.SelectedValue = "-1" Then
            lblmessage.Text &= "Select Business unit"
            rval = False
        End If
        If ddvehRegNo.SelectedValue = "-1" Then
            lblmessage.Text &= "<br>Select Vehicle Reg No"
            rval = False
        End If
        If txtdate.Text.Trim() = "" Then
            lblmessage.Text &= "<br>Enter Date"
            rval = False
        End If
        If ddtype.SelectedValue = "-1" Then
            lblmessage.Text &= "<br>Select Type"
            rval = False
        End If

        Return rval
    End Function

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If ValidateC() Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(7) As SqlClient.SqlParameter
            Dim datatime As DateTime = Convert.ToDateTime(txtdate.Text.Trim())
            Dim hrs = Today.Hour
            Dim mins = Today.Minute

            If ddTHrs.SelectedValue <> "-1" And ddTmin.SelectedValue <> "-1" Then
                hrs = ddTHrs.SelectedValue
                mins = ddTmin.SelectedValue
            End If

            Dim dt As DateTime = New DateTime(datatime.Year, datatime.Month, datatime.Day, Convert.ToInt16(hrs), Convert.ToInt16(mins), 0)

            For Each row As GridViewRow In GridInfo.Rows
                Dim chck As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                If chck.Checked Then

                    Dim sid = DirectCast(row.FindControl("Hiddenid"), HiddenField).Value
                    Dim remarks = DirectCast(row.FindControl("txtremarks"), TextBox).Text.Trim()
                    pParms(0) = New SqlClient.SqlParameter("@GPS_UNIT_ID", ddvehRegNo.SelectedValue)
                    pParms(1) = New SqlClient.SqlParameter("@BARCODE", sid)
                    pParms(2) = New SqlClient.SqlParameter("@ENTRY_DATE_TIME", dt)
                    pParms(3) = New SqlClient.SqlParameter("@ENTRY_BY", Session("EmployeeId"))
                    pParms(4) = New SqlClient.SqlParameter("@TYPE", ddtype.SelectedValue)

                    If remarks <> "" Then
                        pParms(5) = New SqlClient.SqlParameter("@REMARKS", remarks)
                    End If
                    pParms(6) = New SqlClient.SqlParameter("@EVENT_ID", ddevent.SelectedValue)

                    lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_ATTENDANCE_NO_CARD_INSERT", pParms)

                End If
            Next
        End If

    End Sub



    Protected Sub ddBsusearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsusearch.SelectedIndexChanged
        BindGrade()
    End Sub


    Protected Sub ddGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddGrade.SelectedIndexChanged
        BindSection()
    End Sub
End Class
