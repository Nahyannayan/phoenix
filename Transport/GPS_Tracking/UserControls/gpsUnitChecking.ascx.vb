Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports System.IO

Partial Class Transport_GPS_Tracking_UserControls_gpsUnitChecking
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            GetTemp()
            BinndDate()
        End If

    End Sub


    Public Sub GetTemp()

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim sql_query As String = "SELECT COUNT(*)TEMP_COUNT FROM IntelliXDB_V2.dbo.TempIn"

            lbltemp.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_query)
        Catch ex As Exception

        End Try

    End Sub

    Public Sub BinndDate()
        txtdate.Text = Today.ToString("dd/MMM/yyyy")
    End Sub
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim sql_query As String = ""

        If Today.ToString("dd/MMM/yyyy").Trim() = txtdate.Text.Trim() Then
            sql_query = "SELECT FK_UnitID, TempInDateTime as  PositionLogDateTime,Latitude, Longitude,Speed,ReportID Report, BarCode  FROM IntelliXDB_V2.dbo.positionlog  WHERE FK_UnitID='" & txtunitid.Text.Trim() & "' "

        Else
            sql_query = "SELECT FK_UnitID, TempInDateTime as PositionLogDateTime,Latitude, Longitude,Speed,ReportID Report, BarCode  FROM IntelliXDB_V2.dbo.positionlog_Backup  WHERE FK_UnitID='" & txtunitid.Text.Trim() & "' "
        End If

        If txtdate.Text <> "" Then
            sql_query &= " AND  REPLACE(CONVERT(VARCHAR(11), PositionLogDateTime, 106), ' ', '/') = '" & txtdate.Text & "'"
        End If
        If Checkbarcode.Checked Then
            sql_query &= " AND  isnull(BarCode,'') <> '' "
        End If

        sql_query &= " ORDER BY PK_PositionLogID DESC "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        GridInfo.DataSource = ds
        GridInfo.DataBind()

        GetTemp()

    End Sub


    Protected Sub BtnInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInfo.Click
        BindGrid()
    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Sub CallReport()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile


        Dim str_query As String
        Dim lstrExportType As Integer




        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim sql_query As String = ""

        If Today.ToString("dd/MMM/yyyy").Trim() = txtdate.Text.Trim() Then
            sql_query = "SELECT FK_UnitID, TempInDateTime as  PositionLogDateTime,Latitude, Longitude,Speed,ReportID Report, BarCode  FROM IntelliXDB_V2.dbo.positionlog  WHERE FK_UnitID='" & txtunitid.Text.Trim() & "' "

        Else
            sql_query = "SELECT FK_UnitID, TempInDateTime as PositionLogDateTime,Latitude, Longitude,Speed,ReportID Report, BarCode  FROM IntelliXDB_V2.dbo.positionlog_Backup  WHERE FK_UnitID='" & txtunitid.Text.Trim() & "' "
        End If

        If txtdate.Text <> "" Then
            sql_query &= " AND  REPLACE(CONVERT(VARCHAR(11), PositionLogDateTime, 106), ' ', '/') = '" & txtdate.Text & "'"
        End If
        If Checkbarcode.Checked Then
            sql_query &= " AND  isnull(BarCode,'') <> '' "
        End If

        sql_query &= " ORDER BY PK_PositionLogID DESC "

        Dim ds1 As DataSet
        ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim title3 = String.Empty
            title3 = txtunitid.Text
            If title3.Length > 30 Then
                title3 = txtunitid.Text.Substring(0, 30)
            Else
                title3 = txtunitid.Text
            End If

            Dim ws As ExcelWorksheet = ef.Worksheets.Add(title3)
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

            ' ws.HeadersFooters.AlignWithMargins = True

            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If

    End Sub
End Class
