﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_UserControls_gpsEmailAlertsGroupList
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindGrind()
        End If

    End Sub



    Public Sub BindGrind()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim str_query = "select * from GPS_EMAIL_ALERT_GROUPS where ENTRY_BSU_ID='" & Session("sBsuid") & "' ORDER BY GROUP_ID DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridGroups.DataSource = ds
        GridGroups.DataBind()

    End Sub

    Protected Sub GridGroups_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridGroups.PageIndexChanging
        GridGroups.PageIndex = e.NewPageIndex
        BindGrind()
    End Sub

    Protected Sub GridGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridGroups.RowCommand
        If e.CommandName = "deleting" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim str_query = ""
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@GROUP_ID", e.CommandArgument)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 3)
            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GPS_INSERT_UPDATE_DELETE_GPS_EMAIL_ALERT_GROUPS", pParms)
            BindGrind()
        End If

        If e.CommandName = "Editing" Then
            Response.Redirect("gpsEmailAlertsGroupCreation.aspx?Groupid=" & e.CommandArgument)
        End If

    End Sub

    Protected Sub linkalert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkalert.Click

        Response.Redirect("gpsEmailAlertsGroupCreation.aspx")

    End Sub

End Class
