﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO


Partial Class Transport_GPS_Tracking_UserControls_gpsStudentAbsentList
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuid.Value = Session("sBsuid")
            BindBsu()
            BinndDate()
            BindTrips()
        End If
    End Sub


    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

        'BindVehReg()


    End Sub

    'Public Sub BindVehReg()

    '    ddvehRegNo.Items.Clear()
    '    Dim ds As DataSet
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, " select CONVERT(VARCHAR,VEH_REGNO) + ' -- ' +  CONVERT(VARCHAR,VEH_UNITID)  as DATA , VEH_UNITID from  OASIS_TRANSPORT.dbo.VW_VEH_DRIVER WHERE VEH_ALTO_BSU_ID='" & ddBsu.SelectedValue & "' ORDER BY VEH_REGNO , VEH_UNITID")

    '    If ds.Tables(0).Rows.Count > 0 Then
    '        ddvehRegNo.DataTextField = "DATA"
    '        ddvehRegNo.DataValueField = "VEH_UNITID"
    '        ddvehRegNo.DataSource = ds
    '        ddvehRegNo.DataBind()
    '    End If
    '    Dim list As New ListItem
    '    list.Text = "Vehicle"
    '    list.Value = "-1"
    '    ddvehRegNo.Items.Insert(0, list)



    'End Sub
    Public Sub BinndDate()
        txtDate.Text = Today.ToString("dd/MMM/yyyy")
    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        Bindgrid()
    End Sub


    Public Function Bindgrid() As DataSet
        lblmessage.Text = ""
        Dim ds As DataSet
        If txtStudentNumber.Text.Trim() <> "" Or ddBsu.SelectedValue <> "-1" Or txtstuname.Text.Trim() <> "" Then
            Dim unitid = ""

            'If ddvehRegNo.SelectedValue <> "-1" Then
            '    unitid = ddvehRegNo.SelectedValue
            'Else
            '    unitid = txtgpsunit.Text.Trim()
            'End If

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(11) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@PositionLogDateTime", txtDate.Text.Trim())
            If txtstuname.Text.Trim() <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@STU_NAME", txtstuname.Text.Trim())
            End If
            If txtBusNo.Text.Trim() <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@BUS_NO", txtBusNo.Text.Trim())
            End If
            If ddBsu.SelectedValue <> "-1" Then
                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            End If
            If ddtrips.SelectedIndex > 0 Then
                pParms(4) = New SqlClient.SqlParameter("@GPS_TRIP_ID", ddtrips.SelectedValue)
            End If
            If txtStudentNumber.Text.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@STU_NO", txtStudentNumber.Text.Trim())
            End If

            If ddoption.SelectedValue <> "0" Then
                pParms(6) = New SqlClient.SqlParameter("@S_OPTION", ddoption.SelectedValue)
            End If

            If ddhazard.SelectedValue = "0" Then
                pParms(7) = New SqlClient.SqlParameter("@PICK_UP_HAZARD", "True")
            End If

            If ddhazard.SelectedValue = "1" Then
                pParms(8) = New SqlClient.SqlParameter("@DROP_OFF_HAZARD", "True")
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BARCODE_LOG_STUDENT_ABSENT", pParms)
            GridData.DataSource = ds
            GridData.DataBind()

            lblmessagetotal.Text = "-Total Records : " & ds.Tables(0).Rows.Count

        Else
            lblmessage.Text = "Please specify any one of the following, <br> Business Unit , Student Number or Student Name."
        End If

        Return ds
    End Function


    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        GridData.PageIndex = e.NewPageIndex
        Bindgrid()
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        If txtStudentNumber.Text.Trim() <> "" Or ddBsu.SelectedValue <> "-1" Or txtstuname.Text.Trim() <> "" Then
            Dim param As New Hashtable

            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@PositionLogDateTime", txtDate.Text.Trim()) 'Format(Date.Parse(txtDate.Text.Trim()), "MM/dd/yyyy")
            param.Add("UserName", Session("sUsr_name"))

            If txtstuname.Text.Trim() <> "" Then
                param.Add("@STU_NAME", txtstuname.Text.Trim())
            Else
                param.Add("@STU_NAME", DBNull.Value)
            End If

            If txtBusNo.Text.Trim() <> "" Then
                param.Add("@BUS_NO", txtBusNo.Text.Trim())
            Else
                param.Add("@BUS_NO", DBNull.Value)
            End If

            If ddBsu.SelectedValue <> "-1" Then
                param.Add("@BSU_ID", ddBsu.SelectedValue.ToString())
            Else
                param.Add("@BSU_ID", DBNull.Value)
            End If
 
            If txtStudentNumber.Text.Trim() <> "" Then
                param.Add("@STU_NO", txtStudentNumber.Text.Trim())
            Else
                param.Add("@STU_NO", DBNull.Value)
            End If

            param.Add("@FK_UnitID", DBNull.Value)

            If ddtrips.SelectedIndex > 0 Then
                param.Add("@GPS_TRIP_ID", ddtrips.SelectedValue)
            Else
                param.Add("@GPS_TRIP_ID", DBNull.Value)
            End If

            If ddoption.SelectedValue <> "0" Then
                param.Add("@S_OPTION", ddoption.SelectedValue)
            Else
                param.Add("@S_OPTION", DBNull.Value)
            End If

            If ddhazard.SelectedValue = "0" Then
                param.Add("@PICK_UP_HAZARD", "True")
            Else
                param.Add("@PICK_UP_HAZARD", DBNull.Value)
            End If

            If ddhazard.SelectedValue = "1" Then
                param.Add("@DROP_OFF_HAZARD", "True")
            Else
                param.Add("@DROP_OFF_HAZARD", DBNull.Value)
            End If


            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "OASIS_GPS"
                .crInstanceName = "172.25.29.51"
                .crUser = "GPS"
                .crPassword = "mango123"
                .reportParameters = param
                .reportPath = Server.MapPath("../Gps_Tracking/Report/CrystalReports/CR_Student_Absent.rpt")
            End With
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Else
            lblmessage.Text = "Please specify any one of the following, <br> Business Unit , Student Number or Student Name."
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub GridData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridData.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            If CheckPicture.Checked Then
                DirectCast(e.Row.FindControl("img_stud"), Image).Visible = True
            Else
                DirectCast(e.Row.FindControl("img_stud"), Image).Visible = False
            End If
        End If


    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindTrips()
    End Sub


    Public Sub BindTrips()
        ddtrips.Items.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 2)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GPS_BSU_TRIPS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            ddtrips.DataSource = ds
            ddtrips.DataTextField = "TRIP"
            ddtrips.DataValueField = "GPS_TRIP_ID"
            ddtrips.DataBind()
        End If

        Dim list As New ListItem
        list.Text = "Select a Trip"
        list.Value = "-1"

        ddtrips.Items.Insert(0, list)
        ddtrips.Visible = True


    End Sub

    Public Function CheckboxListValues(ByVal Grid As GridView, ByVal CheckboxId As String, ByVal hiddenPrimaryId As String, ByVal beforePageChange As Boolean) As Hashtable

        Dim hash As New Hashtable

        If Session("selection") Is Nothing Then
        Else
            hash = Session("selection")
        End If

        For Each row As GridViewRow In Grid.Rows

            Dim HiddenPrimanyIdC As HiddenField = DirectCast(row.FindControl(hiddenPrimaryId), HiddenField)
            Dim CheckboxIdC As CheckBox = DirectCast(row.FindControl(CheckboxId), CheckBox)
            Dim ID = HiddenPrimanyIdC.Value.Trim()

            If CheckboxIdC.Visible = True Then
                If beforePageChange Then
                    If CheckboxIdC.Checked Then
                        If Not hash.ContainsKey(ID) Then
                            hash.Add(ID, "'" & ID & "'")
                        End If
                    Else
                        If hash.ContainsKey(ID) Then
                            hash.Remove(ID)
                        End If
                    End If
                Else

                    If hash.ContainsKey(ID) Then
                        CheckboxIdC.Checked = True
                    Else
                        CheckboxIdC.Checked = False
                    End If

                End If

            End If
        Next
        Session("selection") = hash
        Return hash
    End Function


    Protected Sub SendSms(ByVal sender As Object, ByVal e As System.EventArgs)

        If Hiddentempid.Value <> "" Then
            Dim hash As New Hashtable
            hash = CheckboxListValues(GridData, "ch1", "Hiddenid", True)

            If hash.Count = 0 Then
                Dim ds As DataSet = Bindgrid()

                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ID = ds.Tables(0).Rows(i).Item("STU_NO").ToString().Trim()
                    If Not hash.ContainsKey(ID) Then
                        hash.Add(ID, "'" & ID & "'")
                    End If
                Next

            End If

            Dim val = ""
            For Each Item As Object In hash
                If val = "" Then
                    val = Item.Value
                Else
                    val &= "," & Item.Value
                End If

            Next

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@MODULE_TYPE", "GPS")
            pParms(1) = New SqlClient.SqlParameter("@MESSGE_TYPE", "SMS")
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", HiddenBsuid.Value)
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
            pParms(5) = New SqlClient.SqlParameter("@SCHEDULE", "False")
            pParms(6) = New SqlClient.SqlParameter("@UNIQUE_IDS", val)
            pParms(8) = New SqlClient.SqlParameter("@TEMPLATE_ID", Convert.ToInt16(Hiddentempid.Value))
            Dim sendid = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "GET_TRAN_MESSAGE_SEND_MASTER", pParms)
            lblmessage.Text = "Message sending in progress. Please check the report."
            hash.Clear()
            Session("selection") = hash

        Else
            lblmessage.Text = "Message template not selected.Please select template."
        End If


    End Sub


End Class
