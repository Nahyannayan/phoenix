﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsTripTimeHazardMaster.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsTripTimeHazardMaster" %>

<div>
    <table  width="100%">
        <tr>
            <td class="title-bg">
                Search Filter</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td width="20%" align="left">
                            <span class="field-label">Business Unit</span></td>
                        <td width="30%" align="left">
                                    <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>                   
                        <td width="20%" align="left">
                            <span class="field-label">Veh Reg No</span></td>
                        <td align="left" width="30%">
                                    <asp:DropDownList ID="ddvehRegNo" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                        </tr>
                    <tr>
                        <td>
                            <span class="field-label">Trip Name</span></td>
                        <td>
                            <asp:DropDownList ID="ddtrip" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    
                        <td>
                            <span class="field-label">Area</span> </td>
                        <td>
                            <asp:DropDownList ID="ddarea" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        </tr>
                    <tr>
                        <td>
                            <span class="field-label">Pick/Drop Name</span></td>
                        <td>
                            <asp:DropDownList ID="ddpickdrop" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        
                        <td align="center" colspan="4">
                            <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="Search" />
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<table width="100%">
    <tr>
        <td class="title-bg">
            Trip Activity Master
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridInfo" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" ShowFooter="true" PageSize="50"
                AutoGenerateColumns="false" EmptyDataText="Information not available." 
                Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                                        BSU 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:HiddenField ID="Hiddentripid" Value='<%#Eval("TRIPID")%>' runat="server" />
                                <asp:HiddenField ID="Hiddenpickupid" Value='<%#Eval("PICKUP_S_HAZARD_ID")%>' runat="server" />

                                <asp:Label ID="Label1" runat="server" Visible='<%#Eval("TRIP_VISIBLE")%>' Text='<%#Eval("BSU_SHORTNAME")%>'></asp:Label>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                           Veh Reg No.
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:Label ID="Label2" runat="server" Visible='<%#Eval("TRIP_VISIBLE")%>' Text='<%# Eval("VEH_REGNO")%>'></asp:Label>   
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                                        Trip Name
                        </HeaderTemplate>
                        <ItemTemplate>
                          
                                <asp:Label ID="Label3" runat="server"  Visible='<%#Eval("TRIP_VISIBLE")%>' Text='<%# Eval("TRIPNAME") %>'></asp:Label> 
                               
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                                       Trip Start Time
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:TextBox ID="txttripstarttime"  Visible='<%#Eval("TRIP_VISIBLE")%>' Text='<%# Eval("STARTTIME") %>' runat="server"></asp:TextBox>

                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                                         Trip End Time
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                           <asp:TextBox ID="txttripendtime"  Visible='<%#Eval("TRIP_VISIBLE")%>' Text='<%# Eval("ENDTIME") %>' runat="server"></asp:TextBox>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                                        Area
                        </HeaderTemplate>
                        <ItemTemplate>
                          
                                <asp:Label ID="Label4" runat="server"  Visible='<%#Eval("AREA_VISIBLE")%>' Text='<%# Eval("AREA")%>'></asp:Label> 
                               
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                             Pick/Drop Name
                        </HeaderTemplate>
                        <ItemTemplate>
                           
                            <%#Eval("PICKUPNAME")%>
                               
                        </ItemTemplate>
                    </asp:TemplateField>
                             <asp:TemplateField>
                        <HeaderTemplate>
                            Type
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%# Eval("PICKUPTYPE")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            Time
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>

                              <asp:TextBox ID="txtpdtime" Text='<%# Eval("PICKUPTIME")%>' runat="server"></asp:TextBox>

                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                           Hazard
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:CheckBox ID="CheckHazard" Checked='<%# Eval("TPP_HAZARD")%>'  runat="server" />
                                </center>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnsave" runat="server" CssClass="button" OnClick="savedata" Text="Save" />
                        </FooterTemplate>
                    </asp:TemplateField>

                </Columns>
                <RowStyle CssClass="griditem"  Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <%--<SelectedRowStyle CssClass="Green" Wrap="False" />--%>
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <%--<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
            </asp:GridView>
        </td>
    </tr>
</table>

