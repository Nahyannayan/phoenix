﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsTripConsolidatedReport.ascx.vb"
    Inherits="Transport_GPS_Tracking_UserControls_gpsTripConsolidatedReport_" %>

<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<style type="text/css">
    .style1 {
        width: 100%;
    }

    .button {
    }
</style>

<script type="text/javascript">

    function ViewDetails(a, b, c) {

        window.open('gpsTripConsolidatedReportLevel2.aspx?Tripid=' + a + '&Date=' + b + '&Bsu=' + c, '', 'dialogHeight:500px;dialogWidth:500px;scroll:auto;resizable:no;'); return false;


    }



</script>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" class="title-bg-lite">Search</td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
                <tr>
                    <td align="left" width="20%"><span class="field-label">Date</span></td>
                    <td align="left" width="30%">
                        <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True"
                            EnableTheming="false">
                        </asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td align="left" width="20%"><span class="field-label">Trip</span></td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddtrips" runat="server" EnableTheming="false">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>
                <tr>

                    <td align="center" colspan="4">
                        <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="Show" />
                        <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtDate"
    PopupPosition="TopLeft" TargetControlID="txtDate">
</ajaxToolkit:CalendarExtender>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Trip Consolidated Report</td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridData" runat="server" AllowPaging="True" PageSize="40" EmptyDataText="No records found" AutoGenerateColumns="false"
                CssClass="table table-bordered table-row" Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Business Unit                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>   <%#Eval("BSU_SHORTNAME") %></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total Number of students as per OASIS                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("total_students")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total Number of Buses Deployed in Schools                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("total_bus") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total Capacity                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("veh_capacity") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total Student Absent                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("stu_absent") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Trip Name                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("TRIP_NAME") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Time                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("TRIP_FROM_HR")%>&nbsp;:&nbsp;<%# Eval("TRIP_FROM_MINS")%>&nbsp;to&nbsp;<%# Eval("TRIP_TO_HR")%>&nbsp;:&nbsp;<%#Eval("TRIP_TO_MINS") %></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Number of Buses                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkShow" runat="server" OnClientClick='<%#Eval("viewdetails") %>' Text='<%#Eval("total_bus_trip") %>'></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Number of Students                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("total_stu_trip") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Duplicate Card                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("dup_card_scan") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Number of Staffs                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("total_emp_trip") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>
