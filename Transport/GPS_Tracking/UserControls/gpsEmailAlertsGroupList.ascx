﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsEmailAlertsGroupList.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsEmailAlertsGroupList" %>
<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<div>


    <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>


    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg">GPS Alert Groups&nbsp;&nbsp;                                
                <asp:LinkButton ID="linkalert" runat="server">Add New Email Alert</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="left">


                <asp:GridView ID="GridGroups" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                Group&nbsp;Name
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                    <%#Eval("GROUP_NAME")%>
                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                Alert&nbsp;Type
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                    <%#Eval("ALERT_TYPE")%>
                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time Interval">
                            <HeaderTemplate>
                                Time&nbsp;Interval
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                    <%#Eval("TIME_INTERVAL")%> &nbsp;Mins
                </center>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Start Time">
                            <HeaderTemplate>
                                Start&nbsp;Time
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                    <%#Eval("START_TIME")%>
                </center>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Notify Count">
                            <HeaderTemplate>
                                Notified&nbsp;Count
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
               <%#Eval("NOTIFY_COUNT")%>
            </center>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Active">
                            <HeaderTemplate>
                                Active
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
               <asp:CheckBox ID="Checkactive" Enabled="false" Checked='<%#Eval("ACTIVE")%>' runat="server" />
            </center>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                View/Edit
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                    <asp:LinkButton ID="LinkEdit" runat="server" CommandName="Editing" CommandArgument='<%#Eval("GROUP_ID")%>' CausesValidation="false" >View/Edit</asp:LinkButton>
                </center>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Delete
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                    <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("GROUP_ID")%>'
                        CommandName="deleting"> Delete</asp:LinkButton>
                    <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Are you sure, Do you want this record to be deleted ?"
                        TargetControlID="LinkDelete">
                    </ajaxToolkit:ConfirmButtonExtender>
                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle Wrap="False" />
                    <HeaderStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>



            </td>
        </tr>
    </table>


</div>
