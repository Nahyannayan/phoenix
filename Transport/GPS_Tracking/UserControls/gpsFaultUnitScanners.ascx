﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsFaultUnitScanners.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsFaultUnitScanners" %>

<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" class="title-bg-lite">Search</td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
                <tr>
                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Date</span></td>
                    <td align="left" width="30%">
                        <asp:TextBox ID="txtDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server"
                            Format="dd/MMM/yyyy" PopupButtonID="txtDate"
                            PopupPosition="TopLeft" TargetControlID="txtDate">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                </tr>

                <tr>
                    <td align="left" width="20%"><span class="field-label">Not Working</span></td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddfaulty" runat="server">
                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="GPS Device" Value="D"></asp:ListItem>
                            <asp:ListItem Text="Scanner" Value="S"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>
                <tr>

                    <td align="center" colspan="4">
                        <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="button" />
                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="button" />
                    </td>
                </tr>
                <tr>

                    <td align="left" colspan="4">
                        <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Faulty GPS Device and Scanners</td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row"
                AutoGenerateColumns="false" EmptyDataText="Information not available."
                Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            BSU
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("BSU_SHORTNAME")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Unit ID
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("GPS_ID")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Veh Reg
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("VEH_REGNO")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Driver
                                  
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%# Eval("Driver")%>
                            <br />
                            <%# Eval("DrivMob")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Conductor
                                   
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%# Eval("Conductor")%>
                            <br />
                            <%# Eval("CndMob")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Device
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%# Eval("DEVICE")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Scanner
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%# Eval("SCANNER") %>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>

