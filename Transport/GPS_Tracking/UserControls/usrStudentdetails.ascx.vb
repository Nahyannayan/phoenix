﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj
Partial Class UserControls_usrStudentdetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Private stuid As String = ""
    Public Property StudentDetails_ID() As String
        Get
            Return stuid
        End Get
        Set(ByVal value As String)
            stuid = value
            bindStudetails(stuid)
        End Set
    End Property

    Sub bindStudetails(ByVal STU_ID As String)
        Dim strconn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString()
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        Using studReader As SqlDataReader = SqlHelper.ExecuteReader(strconn, CommandType.StoredProcedure, "GPS_STUDENTDETAILS", param)
            If studReader.HasRows = True Then
                While studReader.Read
                    Ltl_Stu_no.Text = Convert.ToString(studReader("STU_NO"))
                    Ltl_stuName.Text = Convert.ToString(studReader("STUDNAME"))
                    Ltl_grd_sec.Text = Convert.ToString(studReader("GRD_SEC"))
                    Ltl_contname.Text = Convert.ToString(studReader("PARENT_NAME"))
                    Ltl_mobnum.Text = Convert.ToString(studReader.Item("CONTACT_NO"))
                    img_stud.ImageUrl = Convert.ToString(studReader.Item("STU_PHOTOPATH"))
                End While
            End If
        End Using
    End Sub
End Class
