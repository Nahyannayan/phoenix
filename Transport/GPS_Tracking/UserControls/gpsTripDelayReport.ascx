﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsTripDelayReport.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsTripDelayReport" %>

   <!-- Bootstrap core JavaScript-->
 <script src="../../../vendor/jquery/jquery.min.js"></script>
    <script src="../../../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Search</td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
                <tr>
                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Date </span></td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CL1" TargetControlID="txtdate" PopupButtonID="txtdate" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>

                    </td>
                </tr>

                <tr>
                    <td align="left" width="20%"><span class="field-label">Trip Time </span></td>

                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddtrips" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>

                <tr>

                    <td align="center" colspan="4">
                        <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="View" />
                        <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<br />

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" class="title-bg-lite">Trip Delay Report</td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridBusListing" AutoGenerateColumns="false" ShowFooter="true" PageSize="20" AllowPaging="true" Width="100%" runat="server" CssClass="table table-bordered table-row">
                <Columns>

                    <asp:TemplateField HeaderText="Reg No">
                        <ItemTemplate>
                            <center>
                                <%# Eval("VEH_REGNO")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Business Unit">
                        <ItemTemplate>
                            <center>
                                <%# Eval("BSU_SHORTNAME")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Driver">
                        <ItemTemplate>
                            <%#Eval("Driver")%>
                            <br />

                            <%#Eval("DrivMob")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Conductor">
                        <ItemTemplate>
                            <%# Eval("Conductor") %>
                            <br />

                            <%#Eval("CndMob")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="GPS Unit">
                        <ItemTemplate>

                            <center>
                                <%# Eval("GPS_UNIT_ID")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trip Name">
                        <ItemTemplate>

                            <%# Eval("TRIPNAME")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trip Time">
                        <ItemTemplate>
                            <center>
                                <%# Eval("starttime") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trip Scan-Start time">
                        <ItemTemplate>
                            <center>
                                <%# Eval("P_Datetime")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trip Scan-End time">
                        <ItemTemplate>
                            <center>
                                <%# Eval("N_Datetime") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mins Diff">
                        <ItemTemplate>
                            <center>
                                <%# Eval("minsdiff") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
                <HeaderStyle />
                <RowStyle CssClass="griditem" />
                <SelectedRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
                <EmptyDataRowStyle />
                <EditRowStyle />
            </asp:GridView>
            <br />
            <asp:Label ID="lblmessage" runat="server" CssClass="error"></asp:Label>
        </td>
    </tr>
</table>

