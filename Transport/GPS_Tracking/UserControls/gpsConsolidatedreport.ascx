﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsConsolidatedreport.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsConsolidatedreport" %>


<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" colspan="2" class="title-bg-lite">Summary Report</td>
    </tr>
    <tr>
        <td align="left" width="30%">
            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox></td>
        <td align="left" width="70%">
            <asp:Button ID="btnview" CssClass="button" runat="server" Text="View" />
            <asp:Button ID="btnreport" CssClass="button" runat="server" Text="Export" />

        </td>


    </tr>
    <tr>
        <td align="left" colspan="2">
            <asp:GridView ID="GridInfo" runat="server"
                AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                EmptyDataText="Information not available."
                Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            BSU
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("BSU_NAME")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total Vehicle 
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("T_BUS_OASIS")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total Capacity 
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("Total_Capacity ")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField>
                        <HeaderTemplate>
                            Total Student 
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                               <%# Eval("STU_TRAN_TOTAL")%> 
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Student Travelled
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                           <%# Eval("STU_TRAVELLED")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Staff Travelled
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%# Eval("STAFF_TRAVELLED")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Duplicate Card Scanned
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%# Eval("TRAV_NO_CARD")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Student Absent
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%#Eval("STU_ABSENT")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Manual Attendance Staff 
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%#Eval("MANUAL_STAFF_ATT")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Manual Attendance Student
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%#Eval("MANUAL_STUD_ATT")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            % Bus Usage
                                
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                            <%#Eval("PER_USAGE")%>
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>
<ajaxToolkit:CalendarExtender ID="CL1" TargetControlID="txtdate" PopupButtonID="txtdate" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
