﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_gpsLCDAlertActionsList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
            BindGrid()

        End If
    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION ", 1)
        pParms(1) = New SqlClient.SqlParameter("@UNIT_ID", Request.QueryString("unit_id"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_ACTION_LOG_LIST", pParms)

        GridData.DataSource = ds
        GridData.DataBind()

    End Sub



    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging

        GridData.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub
End Class
