<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="gpsLOG_EXPORT.aspx.vb" Inherits="gpsLOG_EXPORT" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">



    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtAsOnDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                        break;
                }
            }
            return false;
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Student Log Export"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" colspan="4" class="title-bg-lite">
                                        <asp:Label ID="lblCaption" runat="server" Text="Select Date Range"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAsOnDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            />
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtAsOnDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="As on Date required"
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span style="font-size: 7pt">(dd/mmm/yyyy)</span>

                                        <ajaxToolkit:CalendarExtender ID="Calendarextender1" runat="server" Format="dd/MMM/yyyy"
            PopupButtonID="imgFromDate" TargetControlID="txtAsOnDate" CssClass="MyCalendar">
        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgToate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                             />
                                        <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="As on Date required"
                                            ValidationGroup="dayBook2">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span style="font-size: 7pt">(dd/mmm/yyyy)</span>

                                         <ajaxToolkit:CalendarExtender ID="Calendarextender2" runat="server" Format="dd/MMM/yyyy"
            PopupButtonID="imgToate" TargetControlID="txtToDate" CssClass="MyCalendar">
        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Export" ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

