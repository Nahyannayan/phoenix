﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsEmailAlertsGroupList.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsEmailAlertsGroupList" %>

<%@ Register Src="UserControls/gpsEmailAlertsGroupList.ascx" TagName="gpsEmailAlertsGroupList" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Speed / Panic Email Alert Settings
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:gpsEmailAlertsGroupList ID="gpsEmailAlertsGroupList1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>




