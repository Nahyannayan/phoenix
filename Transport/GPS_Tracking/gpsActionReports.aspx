﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsActionReports.aspx.vb"
    MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsActionReports" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="GPS Alert Reports"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg-lite">Define Trips
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Report</span>  </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddgpsalerts" runat="server">
                                            <asp:ListItem Text="Max Speed" Value="MAXSPEED"></asp:ListItem>
                                            <asp:ListItem Text="Panic" Value="PANIC"></asp:ListItem>
                                            <asp:ListItem Text="Geo Fence" Value="GEOFENCE"></asp:ListItem>
                                            <asp:ListItem Text="Student Travelled in Different Bus" Value="TDB"></asp:ListItem>
                                            <asp:ListItem Text="Hazard Location - Student Absent- Onward" Value="H_PICK"></asp:ListItem>
                                            <asp:ListItem Text="Hazard Location - Student Absent- Return" Value="H_DROP"></asp:ListItem>
                                            <asp:ListItem Text="Hazard Location - Student Not Dropped on time" Value="H_NOT_DROP"></asp:ListItem>
                                            <asp:ListItem Text="Student In" Value="STU_IN"></asp:ListItem>
                                            <asp:ListItem Text="Trip Delay" Value="TRIP_DELAY"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>

                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtFromDate"
                                            PopupPosition="TopLeft" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtToDate"
                                            PopupPosition="TopLeft" TargetControlID="txtToDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>

                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Excel Export " />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
