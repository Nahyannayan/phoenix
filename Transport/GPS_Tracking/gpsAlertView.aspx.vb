﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Transport_GPS_Tracking_gpsAlertView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "T000515" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
                'Else
                '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            BindAlerts()
        End If
    End Sub
    Sub BindAlerts()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ALRT.GET_GPSALERTS")

            ddl_alerts.DataSource = ds
            ddl_alerts.DataValueField = "GAS_ID"
            ddl_alerts.DataTextField = "GAS_DES"
            ddl_alerts.DataBind()
            ddl_alerts.Items.Insert(0, New ListItem("--Select", "0"))

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btn_showAlerts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_showAlerts.Click

    End Sub


End Class
