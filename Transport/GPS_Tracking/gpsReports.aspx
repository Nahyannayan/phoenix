<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsReports.aspx.vb" EnableEventValidation="false" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsReports" %>

<%@ Register Src="~/Transport/GPS_Tracking/Report/UserControls/gpsReports.ascx" TagName="gpsReports" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="GPS Reports"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:gpsReports ID="gpsReports1" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>



