﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsBarcodePickDropAlert.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsBarcodePickDropAlert" %>




<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <script type="text/javascript">


        function openwindow(a) {

            var file = ''
            if (a == 1) {
                file = 'gpsBarcodePickDropAlertOnwardShow.aspx'
            }
            else {
                file = 'gpsBarcodePickDropAlertReturnShow.aspx'
            }

            window.open(file, '', "dialogWidth:1000px; dialogHeight:800px;scroll:yes;center:yes");

        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Student Trip Log Alert/Actions"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <center>

    <asp:Button ID="btnonward" CssClass="button" OnClientClick='javascript:openwindow(1);return false;' runat="server" Text="On Ward" />
    <asp:Button ID="btnReturn" CssClass="button" OnClientClick='javascript:openwindow(2);return false;'  runat="server" Text="Return" /> 
 
  </center>

            </div>
        </div>
    </div>
</asp:Content>



