<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsMastersettings.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_gpsMastersettings" %>

<%@ Register Src="UserControls/gpsUnitChecking.ascx" TagName="gpsUnitChecking" TagPrefix="uc2" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Unit Checking"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc2:gpsUnitChecking ID="GpsUnitChecking1" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>


