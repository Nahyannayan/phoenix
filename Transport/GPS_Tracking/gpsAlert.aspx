﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="gpsAlert.aspx.vb" Inherits="Transport_GPS_Tracking_gpsAlert" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/cssfiles/Popup.css" rel="stylesheet" />


    <script type="text/javascript">
        //window.setTimeout('loadpage();', 200);

        function loadpage(a) {

            var EmployeeId = document.getElementById("<%=Hiddenempid.ClientID %>").value;
            var sBusper = document.getElementById("<%=Hiddensuper.ClientID %>").value;
            var bsu = document.getElementById("<%=ddBsu.ClientID %>").value;
            if (a == 1) {
                var win = window.open("gpsAlert2.aspx?Bsu=" + bsu + "&EmployeeId=" + EmployeeId + "&sBusper=" + sBusper + "&tmsection=0")//, "", "toolbar=0,menubar=0,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
            }

            if (a == 12) {
                var speed = document.getElementById("<%=txtspeed.ClientID %>").value;
                var scount = document.getElementById("<%=txtstucount.ClientID %>").value;
                if (speed != "" && scount != "") {

                    var win = window.open("gpsAlert4.aspx?Bsu=" + bsu + "&speed=" + speed + "&scount=" + scount + "&EmployeeId=" + EmployeeId + "&sBusper=" + sBusper + "&tmsection=0")//, "", "toolbar=0,menubar=0,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
                }
                else {
                    alert("Please enter both speed and student count")
                }
            }


            var tripid = document.getElementById("<%=ddtrips.ClientID %>").value;

            if (a == 2) {
                if (tripid != "-1") {
                    var win = window.open("gpsAlert3.aspx?Type=H_PICK&Bsu=" + bsu + "&EmployeeId=" + EmployeeId + "&sBusper=" + sBusper + "&tripid=" + tripid + "&tmsection=0")//, "", "toolbar=0,menubar=0,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
                }
                else {
                    alert('Please select trip');
                }
            }

            if (a == 3) {
                if (tripid != "-1") {
                    var win = window.open("gpsAlert3.aspx?Type=H_DROP&Bsu=" + bsu + "&EmployeeId=" + EmployeeId + "&sBusper=" + sBusper + "&tripid=" + tripid + "&tmsection=0")//, "", "toolbar=0,menubar=0,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
                }
                else {
                    alert('Please select trip');
                }
            }
            if (a == 4) {
                if (tripid != "-1") {
                    var win = window.open("gpsAlert3.aspx?Type=H_NOT_DROP&Bsu=" + bsu + "&EmployeeId=" + EmployeeId + "&sBusper=" + sBusper + "&tripid=" + tripid + "&tmsection=0")//, "", "toolbar=0,menubar=0,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
                }
                else {
                    alert('Please select trip');
                }
            }
            if (a == 5) {

                if (tripid != "-1") {
                    var win = window.open("gpsAlert3.aspx?Type=TRIP_DELAY&Bsu=" + bsu + "&EmployeeId=" + EmployeeId + "&sBusper=" + sBusper + "&tripid=" + tripid + "&tmsection=0")//, "", "toolbar=0,menubar=0,scrollbars=yes,resizable=0,directories=0,status=0,fullscreen,left=100,top=100");
                }
                else {
                    alert('Please select trip');
                }
            }


        }






    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="GPS Hazard Location Alerts"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg-lite">GPS Alerts</td>
                    </tr>
                    <tr>
                        <td align="left">

                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td><span class="field-label">GPS Alert- TDB &amp; Student In</span></td>
                                    <td align="left" width="30%">
                                        <asp:LinkButton ID="LinkButton22" OnClientClick="loadpage(1);return false;" runat="server">View Alert</asp:LinkButton>
                                    </td>
                                </tr>




                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student In at low speed - Speed</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtspeed" Text="10" runat="server"></asp:TextBox>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Student Count</span>
                                    </td>
                                    <td align="left" width="30%">

                                        <asp:TextBox ID="txtstucount" Text="10" runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:LinkButton ID="LinkButton27" OnClientClick="loadpage(12);return false;"
                                            runat="server">View Alert</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddtrips" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Hazard Location - Student Absent- Onward</span></td>
                                    <td align="left" width="30%">
                                        <asp:LinkButton ID="LinkButton23" OnClientClick="loadpage(2);return false;" runat="server">View Alert</asp:LinkButton>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Hazard Location - Student Absent- Return</span></td>
                                    <td align="left" width="30%">
                                        <asp:LinkButton ID="LinkButton25" OnClientClick="loadpage(3);return false;"
                                            runat="server">View Alert</asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Hazard Location - Student Not Dropped on time</span></td>
                                    <td align="left" width="30%">
                                        <asp:LinkButton ID="LinkButton24" OnClientClick="loadpage(4);return false;" runat="server">View Alert</asp:LinkButton>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Trip Delay</span></td>
                                    <td align="left" width="30%">
                                        <asp:LinkButton ID="LinkButton26" OnClientClick="loadpage(5);return false;"
                                            runat="server">View Alert</asp:LinkButton>
                                    </td>
                                </tr>


                            </table>

                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="Hiddenempid" runat="server" />

                <asp:HiddenField ID="Hiddensuper" runat="server" />
                <asp:HiddenField ID="HiddenBsu" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

