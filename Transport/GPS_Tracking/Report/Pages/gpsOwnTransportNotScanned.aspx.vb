﻿Imports System
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports System.IO
Partial Class Transport_GPS_Tracking_Report_Pages_gpsOwnTransportNotScanned
    Inherits System.Web.UI.Page

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'HiddenBsuid.Value = Session("sBsuid")
            BindBsu()
            txtStartDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy")
        End If
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReportExcel)
    End Sub
#End Region

#Region "View Button Click"
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim dsTrip As New DataSet
        Dim param() As SqlClient.SqlParameter
        param = GetParametersForGrid()
        If ddlReportType.SelectedValue = "1" Then
            Try
                dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptOwnTransportUsersNotScanned", param)
            Catch ex As Exception
            End Try
        ElseIf ddlReportType.SelectedValue = "2" Then
            Try
                dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptOwnTransportUsersSingleScan", param)
            Catch ex As Exception
            End Try
        ElseIf ddlReportType.SelectedValue = "3" Then
                Try
                    dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptBusUsersScannedAtSchool", param)
                Catch ex As Exception
                End Try
        ElseIf ddlReportType.SelectedValue = "4" Then
                Try
                    dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptOwnTransportLateComers", param)
                Catch ex As Exception
                End Try
        End If
        If (dsTrip.Tables.Count > 0) Then
            GridData.DataSource = dsTrip.Tables(0)
            GridData.DataBind()
            If ddlReportType.SelectedValue = "1" Then
                GridData.Columns(5).Visible = False
                GridData.Columns(6).Visible = False
            Else
                GridData.Columns(5).Visible = True
                GridData.Columns(6).Visible = True
            End If
            lblmessage.Text = dsTrip.Tables(0).Rows.Count.ToString() + " Records Found"
        Else
            lblmessage.Text = "No Records Found"
        End If
    End Sub
#End Region

#Region "Report Button Click"
    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim param As New Hashtable
        param = GetParametersForReport()
        Dim rptClass As New rptClass
        rptClass.crDatabase = "OASIS_TRANSPORT"
        rptClass.reportParameters = param
        If ddlReportType.SelectedValue = "1" Then
            rptClass.reportPath = Server.MapPath("../CrystalReports/CR_OT_Student_Not_Scanned.rpt")
        ElseIf ddlReportType.SelectedValue = "2" Then

        ElseIf ddlReportType.SelectedValue = "3" Then

        ElseIf ddlReportType.SelectedValue = "4" Then

        End If
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

#End Region

#Region "Excel Button Click"
    Protected Sub btnReportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReportExcel.Click
        Dim dsTrip As New DataSet
        Dim dtTrip As New DataTable
        Dim param() As SqlClient.SqlParameter
        param = GetParametersForGrid()
        If ddlReportType.SelectedValue = "1" Then
            Try
                dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptOwnTransportUsersNotScanned", param)
            Catch ex As Exception
            End Try
        ElseIf ddlReportType.SelectedValue = "2" Then
            Try
                dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptOwnTransportUsersSingleScan", param)
            Catch ex As Exception
            End Try
        ElseIf ddlReportType.SelectedValue = "3" Then
            Try
                dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptBusUsersScannedAtSchool", param)
            Catch ex As Exception
            End Try
        ElseIf ddlReportType.SelectedValue = "4" Then
            Try
                dsTrip = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.rptOwnTransportLateComers", param)
            Catch ex As Exception
            End Try
        End If
        If (dsTrip.Tables.Count > 0) Then
            dtTrip = dsTrip.Tables(0)
            If (dtTrip.Rows.Count > 0) Then
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
                Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ws.InsertDataTable(dtTrip, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
                'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                Dim pathSave As String
                pathSave = "Data.xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
            Else
                lblmessage.Text = "No Records Found"
            End If
        Else
            lblmessage.Text = "No Records Found"
        End If
    End Sub
#End Region

#Region "Get Filter Parameters for Grid"
    Protected Function GetParametersForGrid() As SqlClient.SqlParameter()
        Dim param(9) As SqlClient.SqlParameter
        If ddlBsu.SelectedValue <> "" Then
            param(0) = New SqlParameter("@BSU_ID", ddlBsu.SelectedValue)
        Else
            param(0) = New SqlParameter("@BSU_ID", DBNull.Value)
        End If
        If ddlCurriculum.SelectedValue <> "" Then
            param(1) = New SqlParameter("@CLM_ID", ddlCurriculum.SelectedValue)
        Else
            param(1) = New SqlParameter("@CLM_ID", DBNull.Value)
        End If
        If ddlSection.SelectedValue <> "" And ddlSection.SelectedValue <> "0" Then
            param(2) = New SqlParameter("@SCT_ID", Convert.ToInt32(ddlSection.SelectedValue))
        Else
            param(2) = New SqlParameter("@SCT_ID", DBNull.Value)
        End If
        If txtStudentNumber.Text.Trim() <> "" Then
            param(3) = New SqlParameter("@STU_NO", txtStudentNumber.Text)
        Else
            param(3) = New SqlParameter("@STU_NO", DBNull.Value)
        End If
        If txtGpsUnit.Text.Trim() <> "" Then
            param(4) = New SqlParameter("@UNIT_ID", txtGpsUnit.Text)
        Else
            param(4) = New SqlParameter("@UNIT_ID", DBNull.Value)
        End If
        If ddlGrade.SelectedValue <> "" And ddlGrade.SelectedValue <> "0" Then
            param(5) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
        Else
            param(5) = New SqlParameter("@GRD_ID", DBNull.Value)
        End If
        If txtStudentName.Text.Trim() <> "" Then
            param(6) = New SqlParameter("@STU_NAME", txtStudentName.Text)
        Else
            param(6) = New SqlParameter("@STU_NAME", DBNull.Value)
        End If
        If txtStartDate.Text.Trim() <> "" Then
            param(7) = New SqlParameter("@START_DATE", Convert.ToDateTime(txtStartDate.Text))
        Else
            param(7) = New SqlParameter("@START_DATE", DBNull.Value)
        End If
        Return param
    End Function
#End Region

#Region "Get Filter Parameters for Report"
    Protected Function GetParametersForReport() As Hashtable
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", ddlBsu.SelectedValue)
        param.Add("@IMG_TYPE", "LOGO")
        If ddlBsu.SelectedValue <> "" Then
            param.Add("@BSU_ID", ddlBsu.SelectedValue)
        Else
            param.Add("@BSU_ID", DBNull.Value)
        End If
        If ddlCurriculum.SelectedValue <> "" Then
            param.Add("@CLM_ID", ddlCurriculum.SelectedValue)
        Else
            param.Add("@CLM_ID", DBNull.Value)
        End If
        If ddlSection.SelectedValue <> "" And ddlSection.SelectedValue <> "0" Then
            param.Add("@SCT_ID", Convert.ToInt32(ddlSection.SelectedValue))
        Else
            param.Add("@SCT_ID", DBNull.Value)
        End If
        If txtStudentNumber.Text.Trim() <> "" Then
            param.Add("@STU_NO", txtStudentNumber.Text)
        Else
            param.Add("@STU_NO", DBNull.Value)
        End If
        If txtGpsUnit.Text.Trim() <> "" Then
            param.Add("@UNIT_ID", txtGpsUnit.Text)
        Else
            param.Add("@UNIT_ID", DBNull.Value)
        End If
        If ddlGrade.SelectedValue <> "" And ddlGrade.SelectedValue <> "0" Then
            param.Add("@GRD_ID", ddlGrade.SelectedValue)
        Else
            param.Add("@GRD_ID", DBNull.Value)
        End If
        If txtStudentName.Text.Trim() <> "" Then
            param.Add("@STU_NAME", txtStudentName.Text)
        Else
            param.Add("@STU_NAME", DBNull.Value)
        End If
        If txtStartDate.Text.Trim() <> "" Then
            param.Add("@START_DATE", Convert.ToDateTime(txtStartDate.Text))
        Else
            param.Add("@START_DATE", DBNull.Value)
        End If
        Return param
    End Function
#End Region

#Region "Get ACD_ID"
    Public Function getacd_id() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "  select top 1 ACD_ID from [OASIS].[dbo].[ACADEMICYEAR_D] where ACD_BSU_ID='" & ddlBsu.SelectedValue & "'  AND ACD_CURRENT=1 "
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Function
#End Region

#Region "Bind Curriculum Dropdown"
    Sub bindCLM()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim acd_id As String = getacd_id()

        str_query = " select clm_id,clm_descr from dbo.curriculum_M inner join  academicyear_d on acd_clm_id=clm_id" & _
                    " where acd_current=1 and acd_bsu_id='" & ddlBsu.SelectedValue & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCurriculum.DataSource = ds
        ddlCurriculum.DataTextField = "clm_descr"
        ddlCurriculum.DataValueField = "clm_id"
        ddlCurriculum.DataBind()

        If ddlCurriculum.Items.Count = 0 Then
            ddlCurriculum.Items.Add(New ListItem("ALL", "0"))
            ddlCurriculum.ClearSelection()
            ddlCurriculum.Items.FindByText("ALL").Selected = True
        End If
        ddlCurriculum_SelectedIndexChanged(ddlCurriculum, Nothing)
    End Sub
#End Region

#Region "Curriculum Dropdown SelectedIndexChanged Event"
    Protected Sub ddlCurriculum_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurriculum.SelectedIndexChanged
        BindGrade()
    End Sub
#End Region

#Region "Bind Grade Dropdown"
    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim clm As String = ddlCurriculum.SelectedValue
        Dim acy_id As String = getacd_id()
        Dim ds As DataSet
        If clm <> "0" Then
            str_query = "select distinct grm_grd_id,grm_display,grd_displayorder from dbo.grade_bsu_m inner join " & _
                " academicyear_d on grm_acd_id=acd_id inner join grade_m on grm_grd_id=grd_id " & _
                " where acd_current=1 and acd_bsu_id='" & ddlBsu.SelectedValue & "' and acd_clm_id='" & ddlCurriculum.SelectedValue & "' order by grd_displayorder "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "grm_grd_id"
            ddlGrade.DataBind()
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = True
        Else
            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = False
        End If
        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
    End Sub
#End Region

#Region "Grade Dropdown SelectedIndexChanged Event"
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub
#End Region

#Region "Bind Section Dropdown"
    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim clm As String = ddlCurriculum.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim acy_id As String = getacd_id()
        If clm <> "0" Then
            Dim str_query As String = " SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE  SCT_GRD_ID= '" & ddlGrade.SelectedValue & "'" & _
                " AND SCT_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & ddlCurriculum.SelectedValue & "')  " & _
                " and acd_current=1 AND (ACD_BSU_ID = '" & ddlBsu.SelectedValue & "') ) ORDER BY SCT_DESCR "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = True
        Else
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = False
        End If
    End Sub
#End Region

#Region "Bind BSU Dropdown"
    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlBsu.DataSource = dsBsu
            ddlBsu.DataTextField = "BSU_NAME"
            ddlBsu.DataValueField = "BSU_ID"
            ddlBsu.DataBind()
            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddlBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlBsu.DataSource = dsBsu
            ddlBsu.DataTextField = "BSU_NAME"
            ddlBsu.DataValueField = "BSU_ID"
            ddlBsu.DataBind()
        End If
        ddlBsu_SelectedIndexChanged(ddlBsu, Nothing)
    End Sub
#End Region

#Region "BSU Dropdown SelectedIndexChanged Event"
    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        bindCLM()
    End Sub
#End Region





End Class
