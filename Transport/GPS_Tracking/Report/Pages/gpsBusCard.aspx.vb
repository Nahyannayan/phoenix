﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_Report_Pages_gpsBusCard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindCard()
        End If
    End Sub

    Public Sub BindCard()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim query = "SELECT ID,CARD_DESC FROM GPS_BUS_CARD ORDER BY CARD_DESC"

        ddcards.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        ddcards.DataTextField = "CARD_DESC"
        ddcards.DataValueField = "ID"
        ddcards.DataBind()

    End Sub

 
End Class
