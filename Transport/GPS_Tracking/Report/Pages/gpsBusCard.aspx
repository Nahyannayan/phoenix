﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="gpsBusCard.aspx.vb" Inherits="Transport_GPS_Tracking_Report_Pages_gpsBusCard" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Bus Card"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg">Select Bus Card</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddcards" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="70%">
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" OnClientClick="openwindow(); return false;" Text="Print" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="HiddenFlag" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    <script type="text/javascript">

        function openwindow() {

            var sFeatures;
            sFeatures = "dialogWidth: 800px; ";
            sFeatures += "dialogHeight: 600px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";


            var qstng = "Id=" + document.getElementById("<%=ddcards.ClientID %>").value;
            var result;
            result = window.open('../../../../CardPrinter/NewApprovedCard/BusCardBarcode.aspx?' + qstng)

        }


    </script>

</asp:Content>



