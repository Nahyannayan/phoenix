<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gpsdailyViolationFilter.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Transport_GPS_Tracking_Report_Pages_gpsdailyViolationFilter" Title="Untitled Page" %>

<%@ Register Src="~/Transport/GPS_Tracking/Report/UserControls/gpsSpeedReportFilter.ascx" TagName="gpsSpeedReportFilter"
    TagPrefix="uc1" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Violation - Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table cellpadding="5" cellspacing="0" width="100%">
                    <%-- <tr>
            <td class="subheader_img" style="width: 650px">
               Violation - Report
            </td>
        </tr>--%>
                    <tr>
                        <td align="center">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromdate" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;"></asp:ImageButton></td>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>

                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnView" runat="server" CssClass="button" OnClick="btnView_Click"
                                            Text="View" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtFromdate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="HiddenMenuCode" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>

</asp:Content>






