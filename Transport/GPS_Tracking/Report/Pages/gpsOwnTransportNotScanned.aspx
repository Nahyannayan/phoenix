﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="gpsOwnTransportNotScanned.aspx.vb" Inherits="Transport_GPS_Tracking_Report_Pages_gpsOwnTransportNotScanned" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Own Transport Scans

        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td class="title-bg">
                             Students who has not scanned (In or Out)
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td width="20%"><span class="field-label">Report Type</span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlReportType" runat="server">
                                            <asp:ListItem Text="Own Transport Students who has not scanned (In or Out)" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Students who has scanned only once a day (In or Out)" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Bus users scanned at school (In  or Out)" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Late Comer Students" Value="4"></asp:ListItem>
                                        </asp:DropDownList></td>

                                    <td width="20%"><span class="field-label">Business Unit</span>
                                    </td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Student Number</span></td>
                                    <td>
                                        <asp:TextBox ID="txtStudentNumber" runat="server"></asp:TextBox>
                                    </td>
                                    <td><span class="field-label">GPS Unit Id</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtGpsUnit" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td><span class="field-label">Curriculum</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCurriculum" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td><span class="field-label">Grade</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Section</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td><span class="field-label">Date</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStartDate" runat="server" ValidationGroup="T1"></asp:TextBox>
                                        <asp:ImageButton ID="imgStartDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgStartDate"
                                            PopupPosition="TopLeft" TargetControlID="txtStartDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Student Name</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="T1"/>
                                        <asp:Button ID="btnReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="T1"/>
                                        <asp:Button ID="btnReportExcel" runat="server" CssClass="button" Text="Generate Excel" ValidationGroup="T1"/>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="100%">
                    <tr>
                        <td class="title-bg">Own Transport Users Not Scanned -
            <asp:Label ID="lblmessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridData" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                                EmptyDataText="&lt;center&gt;No Records Found&lt;/center&gt;" PageSize="60" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID" Visible="false">
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("STU_ID")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <ItemTemplate>
                                            <center>
                                    <%#Eval("STU_NO")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <%# Eval("STU_NAME")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <center>
                                    <%#Eval("GRD_DISPLAY")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("SCT_DESCR")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GPS Unit">
                                        <ItemTemplate>
                                            <%# Eval("LOCATION_DES")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Time">
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("TempInDateTime")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

