﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="gpsReports.ascx.vb" Inherits="Transport_GPS_Tracking_UserControls_gpsReports" %>
<%@ Register Src="../../Javascript/TreeNodeScript.ascx" TagName="TreeNodeScript"
    TagPrefix="uc1" %>
<uc1:TreeNodeScript ID="TreeNodeScript1" runat="server" />

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="/cssfiles/sb-admin.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->
<div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg">GPS Reports
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Report</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="DropType" runat="server" AutoPostBack="True">
                                <asp:ListItem Text="Speed" Value="SPEED"></asp:ListItem>
                                <asp:ListItem Text="Panic" Value="PANIC"></asp:ListItem>
                                <asp:ListItem Text="Idle" Value="IDLE"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddBsu1" runat="server" OnSelectedIndexChanged="ddBsu1_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trignore2" runat="server">
                        <td align="left" width="20%"><span class="field-label">Vehicle Type</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlVehicleType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged">
                                <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                <asp:ListItem Text="BIG BUS" Value="2"></asp:ListItem>
                                <asp:ListItem Text="MEDIUM BUS" Value="4"></asp:ListItem>

                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Vehicles</span>
                        </td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list">
                                <asp:Panel ID="Panel1" ScrollBars="Auto" runat="server">
                                    <asp:CheckBox ID="CheckAll" runat="server" AutoPostBack="True" Text="All" Checked="True" />
                                    <asp:CheckBoxList ID="CheckVhe" runat="server">
                                    </asp:CheckBoxList>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                    <tr id="TR1" runat="server">
                        <td align="left" width="20%"><span class="field-label">Speed&nbsp;&lt;</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtspeed" runat="server" Text="80" MaxLength="3"></asp:TextBox>
                            &nbsp;Km/Hr
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="TR2" runat="server">
                        <td align="left" width="20%"><span class="field-label">Idle&nbsp;Time&nbsp;&lt;</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtidle" runat="server" Text="10" MaxLength="4"></asp:TextBox>
                            &nbsp;Mins
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trignore" runat="server">
                        <td align="left" colspan="2"><span class="field-label">Ignore vehicles exceeding limit 1 or 2 times in a day</span>
                        </td>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="chkIgnoreexceed" runat="server" CssClass="field-label" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtfromdate" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <%--  <tr>
                        <td width="200px">
                            To&nbsp;Date
                        </td>
                        <td>
                            :
                        </td>
                        <td width="100%">
                            <asp:TextBox ID="txttodate" runat="server"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>

                        <td align="center" colspan="4">
                            <asp:Button ID="btnReport" runat="server" Text="Generate Report" CssClass="button" />

                            &nbsp;<asp:Button ID="btnExcelExport" runat="server" CssClass="button" Text="Export to Excel" Visible="false" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ajaxToolkit:FilteredTextBoxExtender ID="F1" FilterType="Numbers" TargetControlID="txtspeed"
        runat="server">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="F2" FilterType="Numbers" TargetControlID="txtidle"
        runat="server">
    </ajaxToolkit:FilteredTextBoxExtender>
</div>
<ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtfromdate"
    PopupPosition="TopLeft" TargetControlID="txtfromdate">
</ajaxToolkit:CalendarExtender>
<%--<ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txttodate"
    PopupPosition="TopLeft" TargetControlID="txttodate">
</ajaxToolkit:CalendarExtender>--%>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfromdate"
    Display="None" ErrorMessage="Please Enter From Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttodate"
    Display="None" ErrorMessage="Please Enter To Date" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" />

