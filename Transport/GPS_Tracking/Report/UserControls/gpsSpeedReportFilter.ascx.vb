Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class Transport_GPS_Tracking_UserControls_gpsSpeedReportFilter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

            If Session("sbsuid") = "900501" Then ''STS
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "select BSU_SHORTNAME +'-- ' + VEH_REGNO DATA,VEH_UNITID from  VW_VEH_DRIVER  ORDER BY VEH_ALTO_BSU_ID")
            Else
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "select BSU_SHORTNAME +'-- ' + VEH_REGNO DATA,VEH_UNITID from  VW_VEH_DRIVER  WHERE VEH_ALTO_BSU_ID='" & Session("sbsuid") & "'")
            End If
            DropDownList1.DataTextField = "DATA"
            DropDownList1.DataValueField = "VEH_UNITID"
            DropDownList1.DataSource = ds
            DropDownList1.DataBind()
        End If

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim param As New Hashtable
        ''Default Parameters 
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))


        param.Add("@UNIT_ID", DropDownList1.SelectedValue.ToString())
        param.Add("UNITS", DropDownList1.SelectedValue.ToString())
        param.Add("@STARTDATE", txtFromdate.Text.Trim())
        param.Add("@ENDDATE", txtTodate.Text.ToString())
        param.Add("@ReportHeader", "Speed/Time Report")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_GPS"
            .crUser = "GPS"
            .crPassword = "mango123"
            .crInstanceName = "172.25.29.51"
            .reportParameters = param
            .reportPath = Server.MapPath("~/Transport/GPS_Tracking/Report/CrystalReports/CR_Speed_Vs_Time.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
