﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="gpsVehicleIdle.aspx.vb" Inherits="Transport_GPS_Tracking_gpsVehicleIdle" %>

<%@ Register Src="UserControls/gpsVehicleIdle.ascx" TagName="gpsVehicleIdle" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Vehicle Idle Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc1:gpsVehicleIdle ID="gpsVehicleIdle1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

