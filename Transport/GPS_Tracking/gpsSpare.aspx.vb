﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Transport_GPS_Tracking_gpsSpare
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
        End If

    End Sub



    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            'Dim list As New ListItem
            'list.Text = "Select Business Unit"
            'list.Value = "-1"
            'ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If
        ddBsu_SelectedIndexChanged(ddBsu, Nothing)





    End Sub

    Public Sub BindVehReg()

        ddvehRegNo.Items.Clear()
        If ddBsu.SelectedValue <> "-1" Then
            Dim ds As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter


            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 10)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_BASIC_DATA", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                ddvehRegNo.DataTextField = "veh_regno"
                ddvehRegNo.DataValueField = "VEH_UNITID"
                ddvehRegNo.DataSource = ds
                ddvehRegNo.DataBind()
            End If
        End If

        'Dim list As New ListItem
        'list.Text = "Vehicle"
        'list.Value = "-1"
        'ddvehRegNo.Items.Insert(0, list)



    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindVehReg()
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        save()
    End Sub

    Public Sub save(Optional ByVal Reset As Boolean = False)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_query = ""
        Dim unitid = ddvehRegNo.SelectedValue

        If Reset Then
            unitid = Request.QueryString("unitid")
        End If

        If unitid <> Request.QueryString("unitid") Then
            str_query = "UPDATE VEHICLE_M set VEH_SPARE_GPS_UNIT_ID='" & unitid & "' WHERE VEH_UNITID='" & Request.QueryString("unitid") & "'"
        Else
            str_query = "UPDATE VEHICLE_M set VEH_SPARE_GPS_UNIT_ID=NULL WHERE VEH_UNITID='" & unitid & "'"
        End If

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString

        str_query = "INSERT INTO GPS_VEH_SPARE_LOG (GPS_UNIT_ID,SPARE_UNIT_ID) VALUES ('" & Request.QueryString("unitid") & "','" & unitid & "')"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        lblmessage.Text = "Data updated successfully..."


    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        save(True)
    End Sub

End Class
