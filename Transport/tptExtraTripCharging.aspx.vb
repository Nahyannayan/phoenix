Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptExtraTripCharging
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                 Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100240") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    '  Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    btnSave.Visible = False
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()
                    txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
                    txtTo.Text = Format(Now.Date, "dd/MMM/yyyy")
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Protected Sub btnTrip_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnPurpose_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)

        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
     
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTrip.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTrip.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTrip.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTrip.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TRP_ID,TRD_ID,TRP_DESCR,TRP_DESTINATION,TRD_REMARKS,PSE_DESCR,TRD_FROMDATE,TRD_TODATE," _
                                 & " PAY=isnull(CASE TRP_CHARGABLE WHEN 'TRUE' THEN 'TRUE' ELSE 'FALSE' END,'false')," _
                                 & " NOPAY=isnull(CASE TRP_CHARGABLE WHEN 'TRUE' THEN 'FALSE' ELSE 'TRUE' END,'false') " _
                                 & " FROM TRANSPORT.TRIPS_M AS A INNER JOIN TRANSPORT.TRIPS_D AS B ON " _
                                 & " A.TRP_ID=B.TRD_TRP_ID INNER JOIN TRANSPORT.TRIP_PURPOSE_M AS C " _
                                 & " ON A.TRP_PSE_ID=C.PSE_ID" _
                                 & " WHERE TRD_bACTIVE='TRUE' AND TRP_JOURNEY='EXTRA' AND TRD_FROMDATE BETWEEN " _
                                 & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "'"
     
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim txtSearch As New TextBox

        Dim trpSearch As String = ""
        Dim pseSearch As String = ""


        If gvTrip.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvTrip.HeaderRow.FindControl("txtTrip")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("trp_descr", txtSearch.Text, strSearch)
            trpSearch = txtSearch.Text


            txtSearch = New TextBox
            txtSearch = gvTrip.HeaderRow.FindControl("txtPurpose")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("PSE_DESCR", txtSearch.Text, strSearch)
            pseSearch = txtSearch.Text

            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)



        If ds.Tables(0).Rows.Count = 0 Then
            gvTrip.DataBind()
            'ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            'gvTrip.DataBind()
            'Dim columnCount As Integer = gvTrip.Rows(0).Cells.Count
            'gvTrip.Rows(0).Cells.Clear()
            'gvTrip.Rows(0).Cells.Add(New TableCell)
            'gvTrip.Rows(0).Cells(0).ColumnSpan = columnCount
            'gvTrip.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            'gvTrip.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTrip.DataSource = ds
            gvTrip.DataBind()
        End If

       
        If ds.Tables(0).Rows.Count <> 0 Then
            btnSave.Visible = True

            txtSearch = New TextBox
            txtSearch = gvTrip.HeaderRow.FindControl("txtTrip")
            txtSearch.Text = trpSearch

            txtSearch = New TextBox
            txtSearch = gvTrip.HeaderRow.FindControl("txtPurpose")
            txtSearch.Text = pseSearch
            set_Menu_Img()
        Else
            btnSave.Visible = False
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        Dim lblTrpId As Label
        Dim rdPay As RadioButton

        Dim i As Integer
        Dim str As String
        For i = 0 To gvTrip.Rows.Count - 1
            lblTrpId = gvTrip.Rows(i).FindControl("lblTrpId")
            rdPay = gvTrip.Rows(i).FindControl("rdPay")
            str = rdPay.Checked.ToString
            str_query = "UPDATE TRANSPORT.TRIPS_M SET TRP_CHARGABLE='" + str + "' WHERE TRP_ID =" + lblTrpId.Text.ToString
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            GridBind()
            lblError.Text = "Record saved successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
