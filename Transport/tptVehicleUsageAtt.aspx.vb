﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_tptVehicleUsageAtt
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000181") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    txtAtt.Text = Format(Now.Date, "dd/MMM/yyyy")

                    lblBsu.Text = GetBsuName()
                    lblCur.Text = GetCurriculum()
                    BIND_TRIP_CATEGORY()
                    bind_busnos()
                    ' BindLocation(ddlLocation)
                    ' BindSubLocation(ddlLocation, ddlArea)
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function
    Function GetCurriculum() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT CLM_DESCR FROM dbo.CURRICULUM_M  WHERE CLM_ID='" + Session("clm") + "'"
        Dim cur As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return cur
    End Function
    Sub BIND_TRIP_CATEGORY()
        rblTrip.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT TC_TRIP_DESCR FROM transport.TRIP_CATEGORY"
                               
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        rblTrip.DataSource = ds
        rblTrip.DataTextField = "TC_TRIP_DESCR"
        rblTrip.DataValueField = "TC_TRIP_DESCR"
        rblTrip.DataBind()

        rblTrip.Items(0).Selected = True

    End Sub
    Sub bind_busnos()
        ddlBus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT BNO_ID,BNO_DESCR FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + Session("sbsuid") + "' ORDER BY CAST(BNO_DESCR AS INT) " 

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlBus.DataSource = ds
        ddlBus.DataTextField = "BNO_DESCR"
        ddlBus.DataValueField = "BNO_ID"
        ddlBus.DataBind()
        ddlBus.Items.Add(New ListItem("ALL", "0"))
        ddlBus.Items.FindByText("ALL").Selected = True

    End Sub
    Function BindLocation(ByVal ddlLocation As DropDownList)
        ddlLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT DISTINCT SBL_LOC_ID, LOC.LOC_DESCRIPTION  FROM Transport.TRIPS_D  A " _
                              & " INNER JOIN Transport.TRIPS_M B ON TRD_TRP_ID=TRP_ID " _
                              & " INNER JOIN Transport.TRIPS_PICKUP_S C ON C.TPP_TRD_ID=TRD_ID " _
                              & " INNER JOIN Transport.SUBLOCATION_M D ON C.TPP_SBL_ID=SBL_ID " _
                              & " INNER JOIN Transport.LOCATION_M LOC ON D.SBL_LOC_ID=LOC_ID " _
                              & " WHERE B.TRP_BSU_ID='" + Session("sbsuid") + "' " _
                              & " And TRD_TODATE Is NULL " _
                              & " AND B.TRP_JOURNEY='" + rblTrip.SelectedValue + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "LOC_DESCRIPTION"
        ddlLocation.DataValueField = "SBL_LOC_ID"
        ddlLocation.DataBind()
        Return ddlLocation
    End Function
    Function BindSubLocation(ByVal ddlLocation As DropDownList, ByVal ddlArea As DropDownList)


        ddlArea.Items.Clear()
        If ddlLocation.SelectedValue <> "" Then
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_Sql As String = "SELECT DISTINCT C.TPP_SBL_ID,D.SBL_DESCRIPTION   FROM Transport.TRIPS_D  A " _
                                 & " INNER JOIN Transport.TRIPS_M B ON TRD_TRP_ID=TRP_ID " _
                                 & " INNER JOIN Transport.TRIPS_PICKUP_S C ON C.TPP_TRD_ID=TRD_ID " _
                                 & " INNER JOIN Transport.SUBLOCATION_M D ON C.TPP_SBL_ID=SBL_ID " _
                                 & " INNER JOIN Transport.LOCATION_M LOC ON D.SBL_LOC_ID=LOC_ID " _
                                 & " WHERE B.TRP_BSU_ID='" + Session("sbsuid") + "' " _
                                 & " And TRD_TODATE Is NULL " _
                                 & " AND B.TRP_JOURNEY='" + rblTrip.SelectedValue + "'" _
                                 & " AND D.SBL_LOC_ID=" + ddlLocation.SelectedValue



            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlArea.DataSource = ds
            ddlArea.DataTextField = "SBL_DESCRIPTION"
            ddlArea.DataValueField = "TPP_SBL_ID"
            ddlArea.DataBind()
        Else
            ddlArea = Nothing
        End If
        Return ddlArea
    End Function
    Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
            pParms(1) = New SqlClient.SqlParameter("@trp_cat", rblTrip.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@vua_date", txtAtt.Text)
            pParms(3) = New SqlClient.SqlParameter("@busno", ddlBus.SelectedValue)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "transport.get_Vehicle_User_Usage_att", pParms)
            If ds.Tables(0).Rows.Count > 0 Then
                gvAtt.DataSource = ds.Tables(0)
                gvAtt.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvAtt.DataSource = ds.Tables(0)
                Try
                    gvAtt.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvAtt.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvAtt.Rows(0).Cells.Clear()
                gvAtt.Rows(0).Cells.Add(New TableCell)
                gvAtt.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAtt.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAtt.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindSubLocation(ddlLocation, ddlArea)
        gridbind()
    End Sub

    Protected Sub rblTrip_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblTrip.SelectedIndexChanged
        BindLocation(ddlLocation)
        BindSubLocation(ddlLocation, ddlArea)
        gridbind()
    End Sub

    Protected Sub ddlArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlArea.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        lblError.Text = ""
        If Convert.ToDateTime(txtAtt.Text) > Now.Date Then
            lblError.Text = "Do not allow entering of future dates."
            Exit Sub
        End If
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim i As Integer
        i = 0
        For Each row As GridViewRow In gvAtt.Rows
            Dim lblid As New Label
            Dim lblstudNo As New TextBox
            Dim lblstaffdNo As New TextBox

            lblid = row.FindControl("lblid")
            lblstudNo = row.FindControl("lblstudNo")
            lblstaffdNo = row.FindControl("lblstaffdNo")

            If ((lblstudNo.Text = "0") And (lblstaffdNo.Text = "0")) Then
                i = i + 1
            End If
            Dim pParms(6) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@VUA_BSU_ID", Session("sbsuid"))
            pParms(1) = New SqlClient.SqlParameter("@VUA_DATE", txtAtt.Text)
            pParms(2) = New SqlClient.SqlParameter("@VUA_TRP_ID", lblid.Text)
            pParms(3) = New SqlClient.SqlParameter("@VUA_STUD_TOT", lblstudNo.Text)
            pParms(4) = New SqlClient.SqlParameter("@VUA_STAFF_TOT", lblstaffdNo.Text)
            pParms(5) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "transport.SAVE_VEHICLE_USER_ATT", pParms)


        Next
        If gvAtt.Rows.Count = i Then
            lblError.Text = "Please enter Value"
        Else
            lblError.Text = "Record Saved sucessfully"
            gridbind()
        End If
    End Sub

   
    Protected Sub txtAtt_TextChanged(sender As Object, e As EventArgs) Handles txtAtt.TextChanged
        gridbind()
    End Sub

    Protected Sub gvAtt_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvAtt.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblstudNo As TextBox = e.Row.FindControl("lblstudNo")
            Dim lblstaffdNo As TextBox = e.Row.FindControl("lblstaffdNo")
            If lblstudNo.Text <> "0" Then
                lblstudNo.BackColor = Drawing.Color.LightGreen
            End If
            If lblstaffdNo.Text <> "0" Then
                lblstaffdNo.BackColor = Drawing.Color.LightGreen
            End If
        End If
    End Sub

    Protected Sub ddlBus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBus.SelectedIndexChanged
        gridbind()
    End Sub
End Class
