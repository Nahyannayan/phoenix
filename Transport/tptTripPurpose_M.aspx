<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptTripPurpose_M.aspx.vb" Inherits="Transport_tptTripPurpose_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
</script>

        <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Trip Purpose Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">
        <tr>
            <td >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" style="text-align: left" Width="342px" />
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
         <td align="center" width="100%">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" ></asp:Label></td>
        </tr>
        <tr>
            <td width="100%">
                <table align="center" width="100%">
                                        
                         <tr>
                        <td align="left" width="15%">
                            <span class="field-label">Purpose</span></td>
                       
                        <td align="left" width="40%">
                               <asp:TextBox ID="txtPurpose" runat="server" TabIndex="2"></asp:TextBox>
                      </td>
                             <td align="left" width="15%"></td>          
                             <td align="left" width="30%"></td>
                    </tr>
                                       
                     </table>
               </td>
        </tr>
       
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
        </tr>
        <tr>
            <td valign="bottom"><asp:HiddenField ID="hfPSE_ID" runat="server" />
               <asp:RequiredFieldValidator ID="rfPurpose" runat="server" ErrorMessage="Please enter the field Trip Purpose" ControlToValidate="txtPurpose" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>      
           </td>
          
        </tr>
    </table>


            </div>
        </div>
    </div>

</asp:Content>

