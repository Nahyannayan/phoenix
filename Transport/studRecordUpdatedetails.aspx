<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studRecordUpdatedetails.aspx.vb" Inherits="Students_stuContactdetails" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">

    <title>::GEMS OASIS::Update Student Details:: </title>

<script language ="javascript"  type ="text/javascript" >
function closew()
{
    if(confirm('Do you want to exit')==true)
    {
    window.returnValue =true
    window.close()
   }
   else
   {
   return false 
   }
}
</script>
  <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
   <%-- <style type="text/css">
        .style1
        {
            border-style: none;
            border-color: inherit;
            border-width: 0;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            background-image: url('../Images/bgblue.gif');
            background-repeat: repeat-x;
            font-weight: bold;
            color: BLACK;
            height: 16px;
        }
     table.BlueTable_SS
{
	BORDER-RIGHT: #1b80b6 1pt solid;
	padding: 2px; border-collapse:collapse;
	BORDER-BOTTOM: #1b80b6 1pt solid; 
	-moz-border-radius:0;
} 
table.BlueTable_SS td 
{
	border-width: 1px;padding: 2px; 	
	border-left: #1b80b6 1pt solid; border-top: #1b80b6 1pt solid;
	border-bottom : #1b80b6 1pt solid;
	-moz-border-radius:0;
}
</style>--%>
</head>
<base target="_self" />
<body >
     <form id="form1" runat="server">
         <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
         </ajaxToolkit:ToolkitScriptManager>
    <div ><table align="left" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" class="title-bg">
                            <span class="field-label">Student Profile</span></td>
                    </tr>
                    <tr>
                        <td align="left" style="text-align: center;">
                            <table id="Table2" align="left" cellpadding="0" cellspacing="0"
                                style="width: 100%"  runat="server">
                                <tr>
                                    <td align="left">
                                       <span class="field-label"> Name</span></td>
                                    
                                    <td align="left" colspan="4">
                                       <span class="field-value"><asp:Literal ID="ltStudName" runat="server"></asp:Literal></span></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Student ID</span> </td>
                                    
                                    <td align="left" >
                                       <span class="field-value"><asp:Literal ID="ltStudId" runat="server"></asp:Literal></span></td> 
                                    <td align="left">
                                        <span class="field-label">Grade</span></td>
                                   
                                    <td colspan="0" align="left">
                                        <span class="field-value"><asp:Literal ID="ltGrd" runat="server"></asp:Literal></span></td>
                                    <td align="left">
                                        <span class="field-label">Section</span></td>
                                    
                                    <td align="left">
                                        <span class="field-value"><asp:Literal ID="ltSct" runat="server"></asp:Literal></span></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="6">
                    <table id="table1" runat="server" class="table table-bordered" width="100%">
                        <tr class="title-bg-small">
                            <td>Parent Details
                            </td>                           
                            <td>
                                Father</td>
                            <td>
                                Mother</td>
                            <td>
                                Guardian</td>
                             <td align="center">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">First Name</span></td>
                            
                            <td>
                                <asp:textbox ID="Ltl_Fname" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_Mname" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_Gname" runat="server"></asp:textbox></td>
                            <td></td>
                         
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Middle Name</span></td>
                            
                            <td>
                                <asp:textbox ID="txtFmiddlename" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="txtMmiddlename" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="txtGmiddlename" runat="server"></asp:textbox></td>
                            <td></td>
                            
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Last Name</span></td>
                            
                            <td>
                                <asp:TextBox ID="txtFLastname" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMlastname" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGlastname" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                            <tr>
                            <td>
                                <span class="field-label">Nationality</span></td>
                           
                            <td>
                                <asp:DropDownList ID="ddlFnationality" runat="server">
                                </asp:DropDownList>
                                </td>
                            <td>
                                <asp:DropDownList ID="ddMnationality" runat="server">
                                </asp:DropDownList>
                                </td>
                            <td>
                                <asp:DropDownList ID="ddGnationality" runat="server">
                                </asp:DropDownList>
                                </td>
                                <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Communication Address Line 1</span></td>
                            
                            <td>
                                <asp:TextBox ID="txtFaddress1" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMaddress1" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGaddress1" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Communication Street</span></td>
                            
                            <td>
                                <asp:TextBox ID="txtFstreet" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMstreet" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGstreet" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                               <span class="field-label"> Communication Area</span></td>
                           
                            <td>
                                <asp:TextBox ID="txtFarea" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtmarea" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGarea" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Communication Building</span></td>
                           
                            <td>
                                <asp:TextBox ID="txtFBldg" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMBldg" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGBldg" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Communication Apartment No</span></td>
                            
                            <td>
                                <asp:TextBox ID="txtFAPPno" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMAPPno" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGAPPno" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                    
                        <tr>
                            <td>
                                <span class="field-label">Communication POBox</span></td>
                            
                            <td>
                                <asp:textbox ID="Ltl_Fpob" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_Mpob" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_Gpob" runat="server"></asp:textbox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">City/Emirate</span></td>
                            
                            <td>
                                <asp:DropDownList ID="ddlFemirate" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMemirate" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlGemirate" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                               <span class="field-label"> Country</span></td>
                            
                            <td>
                                <asp:DropDownList ID="ddlFcountry" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMcountry" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlGcountry" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                               <span class="field-label"> Phone Res</span></td>
                           
                            <td>
                                <asp:textbox ID="Ltl_FPhoneRes" runat="server" ></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_MPhoneRes" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_GPhoneRes" runat="server"></asp:textbox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Office Phone</span></td>
                            
                            <td>
                                <asp:textbox ID="Ltl_FOfficePhone" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_MOfficePhone" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_GOfficePhone" runat="server"></asp:textbox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">Mobile</span></td>
                           
                            <td>
                                <asp:textbox ID="Ltl_FMobile" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_MMobile" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_GMobile" runat="server"></asp:textbox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">International Address Line 1</span></td>
                            
                            <td>
                                <asp:TextBox ID="txtFInteradd" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMInteradd" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGInteradd" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">International City</span></td>
                            
                            <td>
                                <asp:TextBox ID="txtFIntercity" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMIntercity" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGIntercity" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">International Country</span></td>
                           
                            <td>
                                <asp:DropDownList ID="ddlFintercountry" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMintercountry" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlGintercountry" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="field-label">International Phone</span></td>
                            
                            <td>
                                <asp:TextBox ID="txtFinterPhone" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMinterPhone" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtGinterPhone" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                               <span class="field-label"> Email</span></td>
                            
                            <td>
                                <asp:textbox ID="Ltl_FEmail" runat="server"></asp:textbox>
                                <br />
                                                    </td>
                            <td>
                                <asp:textbox ID="Ltl_MEmail" runat="server"></asp:textbox>
                                <br />
                                                    </td>
                            <td>
                                <asp:textbox ID="Ltl_GEmail" runat="server"></asp:textbox>
                                <br />
                                                    </td>
                            <td></td>
                        </tr>
                        <tr>
                        
                            <td>
                                <span class="field-label">Company</span></td>
                            
                            <td>
                                <asp:textbox ID="Ltl_FCompany" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_MCompany" runat="server"></asp:textbox></td>
                            <td>
                                <asp:textbox ID="Ltl_GCompany" runat="server"></asp:textbox></td>
                            <td></td>
                        </tr>
                        <hr />
                        <tr>
                        
                            <td>
                                <span class="field-label">Emergency Contact No</span></td>
                            
                            <td >
                                <asp:textbox ID="txtEmgcontact" runat="server"></asp:textbox>   
                            </td>
                             <td>
                                <span class="field-label">Emergency Contact Name</span></td>
                            <td>
                                <asp:textbox ID="txtEmgcontactNAme" runat="server"></asp:textbox></td>
                            <td></td>
                        </tr>
                        
                        
                        
                         <tr>
                        
                            <td>
                                <span class="field-label">Comments</span></td>
                           
                            <td colspan="3">
                                <asp:TextBox ID="txtComments" runat="server" TabIndex="22" TextMode="MultiLine"
                                             Rows="4"></asp:TextBox></td>
                             <td></td>
                           
                        </tr>
                        
                        
                        
                        <tr>
                        
                            <td align="center" colspan="5">
                                <asp:Label ID="lblerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                        
                            <td  align="center" colspan="5">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                                    ValidationGroup="gr" />
&nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" onclientclick="return closew();" 
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>   
        <asp:HiddenField ID="HF_stuid" runat="server" /> 
        <asp:HiddenField ID="HF_SibId" runat="server" /> 
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                    ControlToValidate="Ltl_FEmail" 
                                    ErrorMessage="Not a valid email id" ForeColor="" 
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="gr" EnableViewState="False" Display="None"></asp:RegularExpressionValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="RegularExpressionValidator1_ValidatorCalloutExtender" 
                                                runat="server" Enabled="True" TargetControlID="RegularExpressionValidator1">
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                    ControlToValidate="Ltl_MEmail" 
                                    ErrorMessage="Not a valid email id" ForeColor="" 
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="gr" EnableViewState="False" Display="None"></asp:RegularExpressionValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="RegularExpressionValidator2_ValidatorCalloutExtender" 
                                                runat="server" Enabled="True" TargetControlID="RegularExpressionValidator2">
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                    ControlToValidate="Ltl_GEmail" 
                                    ErrorMessage="Not a valid email id" ForeColor="" 
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="gr" EnableViewState="False" Display="None"></asp:RegularExpressionValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="RegularExpressionValidator3_ValidatorCalloutExtender" 
                                                runat="server" Enabled="True" TargetControlID="RegularExpressionValidator3">
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
    
    
    </div>
      
    </form>
</body>
</html>
