<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="tptstudBBtReqApprove_View.aspx.vb" Inherits="Transport_tptstudBBtReqApprove_View"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
       
                 
                             
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">



                <table id="tbl_ShowScreen" width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <table id="tblTPT" runat="server" align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" colspan="4"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Business Unit</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Curriculum</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Ref. Number</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtId" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left"><span class="field-label">Student Name</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3">
                                        <asp:RadioButton ID="rdApplied" runat="server" Checked="True" GroupName="g1" Text="Applied Students" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rdReject" runat="server" GroupName="g1" Text="Rejected Students" CssClass="field-label"></asp:RadioButton></td>
                                    <td align="left">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="4"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Ref. Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBBTId" runat="server" Text='<%# Bind("BBT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="bsuid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBsuId" runat="server" Text='<%# Bind("BBT_BSU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Business Unit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblbSU" runat="server" Text='<%# Bind("bsu_short_result") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Academic Year" SortExpression="DESCR">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblAccH" runat="server">Academicyear</asp:Label><br />
                                                        <asp:TextBox ID="txtAcademicYear" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAcademicYear" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnAcademicYear_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAcademicYear" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label><br />
                                                        <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchStuName_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH12" runat="server" Text="Grade"></asp:Label><br />
                                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnGrade_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Area">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH123" runat="server" Text="Area"></asp:Label><br />
                                                        <asp:TextBox ID="txtArea" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnArea_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnArea_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("sbl_description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pickup point">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH1235" runat="server" Text="Pickup Point"></asp:Label><br />
                                                        <asp:TextBox ID="txtPickup" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnPickup_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnPickup_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPickup" runat="server" Text='<%# Bind("pnt_description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="view" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_9" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_10" runat="server" type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfBSU_ID" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>
