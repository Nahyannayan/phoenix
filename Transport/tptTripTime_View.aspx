<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptTripTime_View.aspx.vb" Inherits="Transport_tptTripTime_View"  Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">

       
function switchViews(obj,row)
        {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            
            if (div.style.display=="none")
                {
                    div.style.display = "inline";
                    if (row=='alt')
                       {
                           img.src="../Images/expand_button_white_alt_down.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_Button_white_Down.jpg" ;
                       }
                   img.alt = "Click to close";
               }
           else
               {
                   div.style.display = "none";
                   if (row=='alt')
                       {
                           img.src="../Images/Expand_button_white_alt.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_button_white.jpg" ;
                       }
                   img.alt = "Click to expand";
               }
       }

</script>

         <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Trip Timings
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center" width="100%">
                
            <tr>    <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                        </tr>
               
                
           <tr>
      
            <td align="center" width="100%">
            
  <table id="Table1" runat="server" align="center" width="100%">
          
                                
                    
        <tr><td align="center" >
         
         <table id="Table2" runat="server" align="center" width="100%" >

            <tr><td align="center">
           <asp:GridView ID="gvTptTripAlloc" runat="server" 
                    AutoGenerateColumns="False" class="table table-bordered table-row"
                     OnRowDataBound="gvTptTripAlloc_RowDataBound" PageSize="20"  AllowPaging="True" Width="100%" >
           <Columns>
           <asp:TemplateField>
           <ItemTemplate>
              <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
               </a>
            </ItemTemplate>
            <AlternatingItemTemplate>
             <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
              <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
              </a>
            </AlternatingItemTemplate>
           </asp:TemplateField>
           
           
            <asp:TemplateField HeaderText="trv_id" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblTrdId" runat="server" Text='<%# Bind("TRD_ID") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                  
                       
                     <asp:TemplateField HeaderText="Trips">
                        <HeaderTemplate>
                                 <asp:Label ID="lbltp" runat="server" CssClass="gridheader_text" Text="Trip"></asp:Label><br />
                            <asp:TextBox ID="txtTrip" runat="server" Width="75%"></asp:TextBox>
                            <asp:ImageButton ID="btnTrip_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnTrip_Search_Click" />
                      </HeaderTemplate>
                       <ItemTemplate>
                      <asp:Label ID="lblTrip" runat="server" text='<%# Bind("TRP_DESCR") %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                      
                           
                     <asp:TemplateField HeaderText="Shift">
                             <HeaderTemplate>
                        Shift<br />
                         <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True"
                          OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" Width="100%">
                          </asp:DropDownList>
                         </HeaderTemplate>
                       <ItemTemplate>
                      <asp:Label ID="lblShift" runat="server" text='<%# Bind("SHF_DESCR") %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                      
                    
                       <asp:TemplateField HeaderText="Journey">
                             <HeaderTemplate>
                       
                     Journey<br />
                         <asp:DropDownList ID="ddlgvJourney" runat="server" AutoPostBack="True"
                          OnSelectedIndexChanged="ddlgvJourney_SelectedIndexChanged" Width="100%">
                          <asp:ListItem >ALL</asp:ListItem>
                          <asp:ListItem>Onward</asp:ListItem>
                          <asp:ListItem>Return</asp:ListItem>
                          
                          </asp:DropDownList>
                         </HeaderTemplate>
                       <ItemTemplate>
                      <asp:Label ID="lblJourney" runat="server" text='<%# Bind("TRP_JOURNEY") %>'></asp:Label>
                      </ItemTemplate>
                      </asp:TemplateField>
                      
                                            
                     <asp:TemplateField HeaderText="Vehicle">
                         <HeaderTemplate>
                        
                     Vehicle<br />
                         <asp:DropDownList ID="ddlgvVehicle" runat="server" AutoPostBack="True"
                          OnSelectedIndexChanged="ddlgvVehicle_SelectedIndexChanged" Width="100%">
                          </asp:DropDownList>
                         </HeaderTemplate>
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblVehicle" runat="server" Text='<%# Bind("VEH_REGNO") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="Catergory">
                         <HeaderTemplate>
                        
                     Category<br />
                         <asp:DropDownList ID="ddlgvCategory" runat="server" AutoPostBack="True"
                          OnSelectedIndexChanged="ddlgvCategory_SelectedIndexChanged" Width="100%">
                          </asp:DropDownList>
                         </HeaderTemplate>
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblCat" runat="server" Text='<%# Bind("CAT_DESCRIPTION") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                                            
                       <asp:TemplateField HeaderText="Bus No">
                             <HeaderTemplate>
                       
                      Bus No<br />
                         <asp:DropDownList ID="ddlgvBus" runat="server" AutoPostBack="True" CssClass="listbox"
                          OnSelectedIndexChanged="ddlgvBus_SelectedIndexChanged" Width="100%">
                          </asp:DropDownList>
                         </HeaderTemplate>
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblBusId" runat="server"  Text='<%# Bind("BNO_DESCR") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                       
                          <asp:TemplateField HeaderText="Driver">
                                <HeaderTemplate>
                                
                                  <asp:Label ID="lblDri" runat="server" CssClass="gridheader_text" Text="Driver"></asp:Label><br />
                                  <asp:TextBox ID="txtDriver" runat="server" Width="75%"></asp:TextBox>
                               <asp:ImageButton ID="btnDriver_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnDriver_Search_Click" />
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblDriver" runat="server" text='<%# Bind("DRV_NAME") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                    
                      <asp:TemplateField HeaderText="Conductor">
                                <HeaderTemplate>
                                <asp:Label ID="lblCon" runat="server" CssClass="gridheader_text" Text="Conductor"></asp:Label><br />
                                <asp:TextBox ID="txtConductor" runat="server" Width="75%"></asp:TextBox>
                                <asp:ImageButton ID="btnConductor_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnConductor_Search_Click" />
                      </HeaderTemplate>
                      <ItemTemplate>
                      <asp:Label ID="lblConductor" runat="server" text='<%# Bind("CON_NAME") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
               
                        <asp:TemplateField HeaderText="From" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblFrom" runat="server" Text='<%# Bind("TRD_FROMDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="To" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblTo" runat="server" Text='<%# Bind("TRD_TODATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
                         <asp:TemplateField HeaderText="Remarks" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("TRD_REMARKS") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                       
             
                               
                  <asp:ButtonField CommandName="Select" HeaderText="Set Time" Text="Set Time"  >
           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
           <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
           </asp:ButtonField>  
             
          <asp:TemplateField >
           <ItemTemplate>
            </td></tr>
             <tr>
            <td colspan="100%">
            <div id="div<%# Eval("GUID") %>" style="display:none;position:relative;" >
           <asp:GridView ID="gvPickUp" runat="server" Width="100%"
           AutoGenerateColumns="false" EmptyDataText="No pickup points for this." >
            <Columns>
                <asp:BoundField DataField="SBL_DESCRIPTION" HeaderText="Area"  HtmlEncode="False" >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField>
           <asp:BoundField DataField="PNT_DESCRIPTION" HeaderText="PickUp Points"  HtmlEncode="False" >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField>
             <asp:BoundField DataField="TPP_TIME" HeaderText="Arrival Time"  HtmlEncode="False"  >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField>
            
          </Columns>
          <RowStyle CssClass="griditem"  /> 
           <HeaderStyle CssClass="gridheader_pop"/> 
         </asp:GridView>
          </div>
          </td>
           </tr>
          </ItemTemplate>
         </asp:TemplateField>
              </Columns>
                   <RowStyle CssClass="griditem"  />
                            <HeaderStyle CssClass="gridheader_pop" />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                            <SelectedRowStyle />
                            <PagerStyle HorizontalAlign="Left"  />                        
               </asp:GridView>   
        </td></tr>
        </table>
                   
           </td>
           </tr>
           </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                   <input id="h_Selected_menu_3"
                    runat="server" type="hidden" value="=" />
                    </td></tr>
          
        </table>


                </div>
            </div>
        </div>

</asp:Content>

