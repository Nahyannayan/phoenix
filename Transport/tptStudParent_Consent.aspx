<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptStudParent_Consent.aspx.vb" Inherits="Transport_tptStudParent_Consent" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Update Parent Consent
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_ShowScreen" runat="server" align="center" width="100%">

                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>


                    <tr>
                        <td align="center" width="100%">
                            <table id="tblRate" runat="server" align="center" width="100%">

                                


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select BusinessUnit</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                                                   
                                    <td align="left" width="20%"><span class="field-label">Select AcademicYear</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Bus No</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBusNo" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                               
                                    <td align="left" width="20%"><span class="field-label">Student No.</span></td>
                                    
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtSTU_ID" runat="server">
                                        </asp:TextBox>

                                    </td>
                                </tr>

                                <tr id="Stud_Name" runat="server" visible="false">
                                    <td align="left" visible="false"><span class="field-label">Student Name</span></td>                                    
                                    <td colspan="2" align="left">
                                        <asp:TextBox ID="txtSTU_NAME" runat="server">
                                        </asp:TextBox>
                                    </td>
                                </tr>



                                <tr id="tr_Staff" runat="server" visible="false">
                                    <td align="left"><span class="field-label">Include Staff list</span></td>
                                    
                                    <td align="left" colspan="2">
                                        <asp:CheckBox ID="chkStaff" runat="server" CssClass="field-label"></asp:CheckBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Consent Given</span></td>
                                    
                                    <td align="left" colspan="2">
                                        <asp:RadioButton ID="rdNo" runat="server" CssClass="field-label" Checked="True" GroupName="g"
                                        Text="No" />
                                        <asp:RadioButton ID="rdYes" runat="server" CssClass="field-label" GroupName="g" Text="Yes" />
                                        <asp:RadioButton ID="rdBoth" runat="server" CssClass="field-label" GroupName="g" Text="All" />

                                    </td>
                                </tr>

                                <tr id="tr1" runat="server" visible="false">
                                    <td>
                                        <asp:CheckBox ID="chkFuture" Text="Promoted Year" CssClass="field-label" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" colspan="4" align="center">

                                        <asp:Button ID="btnGenerateReport" runat="server" Text="List" CssClass="button"></asp:Button>

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>




                    <tr runat="server" id="tbl_Grd">
                        <td align="center">
                            <table id="Table1" class="BlueTableView" runat="server" align="center" width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gv_BusList" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_BusList_rowdatabound"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            Width="100%" AllowPaging="True" PageSize="50">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Log">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgLog" runat="server" ImageUrl="~/Images/master/Remark.png" />
                                                        <ajaxToolkit:PopupControlExtender ID="popStatusHistory" runat="server" PopupControlID="pnlPopup"
                                                            TargetControlID="imgLog" DynamicContextKey='<%# Eval("STU_ID")%>' DynamicControlID="pnlPopup"
                                                            DynamicServicePath="STU_Parent_Consent_Service.asmx"
                                                            DynamicServiceMethod="GetDynamicContent" Position="Right">
                                                        </ajaxToolkit:PopupControlExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Temp_Consent" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTemp_Consent" runat="server" Text='<%# Bind("STU_Parent_Consent") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STU_NO" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="STUDENT NAME" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="GRADE" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_GRADE" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="AREA" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_AREA" runat="server" Text='<%# Bind("DAREA") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="BUS NO" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_BUSNO" runat="server" Text='<%# Bind("RETURNBUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="PARENT NAME" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_PARENTNAME" runat="server" Text='<%# Bind("PARENTNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="PARENT CONTACT" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_PARENTCONTACT" runat="server" Text='<%# Bind("PARENTCONTACT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="CONSENT" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlConsent" runat="server"></asp:DropDownList>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="COMMENTS" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtComments" runat="server" Text='<%# Bind("STU_Parent_Consent_remark")%>' TextMode="MultiLine"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="LAST UPDATE DATE" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUpdatedate" runat="server" Text='<%# Bind("STU_Parent_Consent_Date", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>


                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <RowStyle CssClass="griditem" />
                                            <%--<SelectedRowStyle CssClass="Green" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />--%>
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                            <tr>
                                <td align="center" colspan="4">

                                    <asp:Button ID="btnSave2" runat="server" Text="Save Data" CssClass="button" OnClick="btnSave2_Click"></asp:Button>

                                </td>
                            </tr>
                        </td>
                    </tr>





                </table>

                <asp:Panel ID="pnlPopup" runat="server" CssClass="panel-cover">
                    <div id="InnDiv" style="overflow: auto; width: 650px; background-color:#ffffff;" align="left">
                        <table id="MainTable" style="cursor: hand;">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>

