<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" CodeFile="tptstudTransportAllocation_M.aspx.vb" Inherits="Transport_tptstudTransportAllocation_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">



        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvAllot.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Transport Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%--   <table width="100%" id="Table1" border="0">
                    <tr style="font-size: 12pt;">
                        <td align="left" class="title" style="height: 45px; width: 46%;">&nbsp;TRANSPORT ALLOCATION</td>
                    </tr>
                </table>--%>
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">Fields Marked with (<span class="text-danger">*</span>) are mandatory
                        </td>
                    </tr>

                    <tr>
                        <td align="left">
                            <table align="center" id="tbPromote" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="10%">
                                                    <span class="field-label">Trip </span>
                                                </td>
                                                <td align="left" width="40%">
                                                    <asp:Label ID="lblTrip" runat="server" Text="Label" CssClass="field-value"></asp:Label>

                                                </td>
                                                <td align="left" width="50%">
                                                    <asp:GridView ID="gvSeat1" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records" CssClass="table table-bordered table-row"
                                                        PageSize="1" Width="100%" AllowPaging="True">
                                                        <RowStyle CssClass="griditem" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Capacity">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Available">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle />
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>

                                    <td align="left" width="20%">
                                        <asp:CheckBox ID="chkEnable" runat="server" AutoPostBack="True" CssClass="field-label" /></td>


                                    <td align="left" width="30%"><span class="field-label">Select Trip</span></td>

                                    <td align="left" colspan="2" width="50%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlTrip" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    <br />
                                                </td>
                                                <td align="left">
                                                    <asp:GridView ID="gvSeats" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records" CssClass="table table-bordered table-row"
                                                        PageSize="1" Width="100%" AllowPaging="True">
                                                        <RowStyle CssClass="griditem" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Capacity">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Available">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle />
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="lblPickupPoints" runat="server" Text="PickpPoints :"></asp:Label></td>


                                </tr>
                                <tr>


                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvAllot" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Select
                                                        <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="SL.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Stud No.">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblFeeHeader" runat="server" Text="Stud. No"></asp:Label><br />
                                                        <asp:TextBox ID="txtFeeSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnFeeId_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnFeeId_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label><br />
                                                        <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pick Up">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPickUp" runat="server" Text='<%# Bind("PICKUP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Drop Off">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDropOff" runat="server" Text='<%# Bind("DROPOFF") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Starting Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("SSV_FROMDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pick Up" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPickId" runat="server" Text='<%# Bind("STU_PICKUP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Drop Off" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDropId" runat="server" Text='<%# Bind("STU_DROPOFF") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <HeaderStyle />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAllot" runat="server" Text="Allot" CssClass="button" TabIndex="4" />
                                    </td>


                                </tr>
                            </table>

                            <asp:HiddenField ID="hfSHF_ID" runat="server" />
                            <asp:Panel ID="Panel1" Visible="false" Style="z-index: 10; left: 493px; position: absolute; top: 693px;" runat="server" BackColor="AliceBlue" Height="99px" Width="274px" BorderStyle="Groove">
                                <asp:Label ID="lblText" runat="server"
                                    ForeColor="#1B80B6" Style="left: -4px; position: relative; top: 22px" Height="27px" Width="263px" CssClass="matters"></asp:Label>
                                <asp:Button ID="btnYes" runat="server" Text="Yes" Style="left: 5px; position: relative; top: 36px" CssClass="button" />
                                <asp:Button ID="btnNo" runat="server" Text="No" Style="left: 0px; position: relative; top: 36px" CssClass="button" />
                            </asp:Panel>
                            <asp:HiddenField ID="hfTRD_ID" runat="server" />
                            &nbsp;&nbsp;
                    <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                        HorizontalSide="Center" TargetControlID="Panel1" VerticalSide="Middle">
                    </ajaxToolkit:AlwaysVisibleControlExtender>
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfSeats" runat="server" />
                            <asp:HiddenField ID="hfPickups" runat="server" />
                            <asp:HiddenField ID="hfJourney" runat="server" />
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>


                </table>





            </div>
        </div>
    </div>

</asp:Content>

