﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Transport_tptSplTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim st As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100500") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_12.Value = "LI__../Images/operations/like.gif"

                    hfTRD_ID.Value = 0
                    PopulateAcademicYear()

                    gridbind()



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAcc_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            gridbind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_12.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvComments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvComments.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvComments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvComments.HeaderRow.FindControl("mnu_12_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet

            Dim strFilter As String = ""
            Dim strSidsearch As String()
            Dim strSearch As String


            Dim nameSearch As String = ""
            Dim dateSearch As String = ""
            Dim txtSearch As New TextBox


            str_Sql = " SELECT  STA_ID,STA_DATE,STA_PGM from dbo.SPLTRANSPORT_m WHERE STA_BSUID='" & Session("sBsuid") & "' and sta_acd_id=" & ddlAca_Year.SelectedValue



            If gvComments.Rows.Count > 0 Then




                txtSearch = gvComments.HeaderRow.FindControl("txtAccSearch")
                strSidsearch = h_Selected_menu_12.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("STA_PGM", txtSearch.Text, strSearch)
                nameSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvComments.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,STA_DATE,106)", txtSearch.Text.Replace("/", " "), strSearch)
                dateSearch = txtSearch.Text




                If strFilter.Trim <> "" Then
                    str_Sql = str_Sql + strFilter
                End If


            End If



            str_Sql += " order by STA_DATE desc"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvComments.DataSource = ds.Tables(0)
                gvComments.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvComments.DataSource = ds.Tables(0)
                Try
                    gvComments.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvComments.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvComments.Rows(0).Cells.Clear()
                gvComments.Rows(0).Cells.Add(New TableCell)
                gvComments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvComments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvComments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            txtSearch = New TextBox
            txtSearch = gvComments.HeaderRow.FindControl("txtAccSearch")
            txtSearch.Text = nameSearch

            txtSearch = New TextBox
            txtSearch = gvComments.HeaderRow.FindControl("txtEnqDate")
            txtSearch.Text = dateSearch

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub CallReport(ByVal s As Integer)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@st_id", s)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_transport"
            .reportParameters = param
            .reportPath = Server.MapPath("../Transport/Reports/Rpt/rptSplTransArrangement.rpt")

        End With
        'If hfbDownload.Value = 1 Then
        'Dim rptDownload As New ReportDownload
        'rptDownload.LoadReports(rptClass, rs)
        'rptDownload = Nothing
        'Else
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
        ' End If

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub gvComments_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvComments.PageIndexChanging
        Try
            gvComments.PageIndex = e.NewPageIndex

            gridbind()


        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gvComments_Rowcommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvComments.RowCommand


        '  If IsPostBack = False Then
        Try
            '    Dim url As String

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvComments.Rows(index), GridViewRow)
            Dim lblid As New Label

            lblid = selectedRow.FindControl("cmtId")
            st = Val(lblid.Text)

            If e.CommandName = "Edit" Then

                ViewState("datamode") = "edit"
                'Encrypt the data that needs to be send through Query String
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                Dim url As String = String.Format("~\Transport\tplSplTransportDet.aspx?MainMnu_code={0}&datamode={1}&spl_id=" & st & "&acd_id=" + ddlAca_Year.SelectedValue, ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If

            If e.CommandName = "Print" Then

                CallReport(st)

            End If

            If e.CommandName = "Delete" Then
                Dim connection As String = ConnectionManger.GetOASISTRANSPORTConnectionString
                Dim con As SqlConnection = New SqlConnection(connection)
                con.Open()

                Dim sqltran As SqlTransaction
                sqltran = con.BeginTransaction("trans")
                Try
                    Dim param(10) As SqlParameter

                    param(0) = New SqlParameter("@STA_ID", st)


                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "dbo.delete_spltransport_M", param)


                    sqltran.Commit()
                    lblError.Text = "Deleted..."
                Catch ex As Exception

                    sqltran.Rollback()

                Finally
                    If con.State = ConnectionState.Open Then
                        con.Close()
                    End If
                End Try

            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        ' End If
    End Sub


    Protected Sub lnkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkButton.Click

        ViewState("datamode") = "add"
        'Encrypt the data that needs to be send through Query String
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        Dim url As String = String.Format("~\Transport\tplSplTransportDet.aspx?MainMnu_code={0}&datamode={1}&acd_id=" + ddlAca_Year.SelectedValue, ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub

    Protected Sub gvComments_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvComments.RowDeleting

    End Sub


    'Protected Sub gvGroup_Rowcommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup.RowCommand


    '    '  If IsPostBack = False Then
    '    Try
    '        '    Dim url As String

    '        'Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        'Dim selectedRow As GridViewRow = DirectCast(gvGroup.Rows(index), GridViewRow)

    '        'If e.CommandName = "Edit" Then
    '        '    Dim lblid As New Label
    '        '    Dim txtregno As New TextBox
    '        '    lblid = selectedRow.FindControl("cmtId")






    '        'End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    '    ' End If
    'End Sub

    Protected Sub gvComments_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvComments.RowEditing

    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        gridbind()

    End Sub
    Sub PopulateAcademicYear()
        ddlAca_Year.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sBSUID") + "' " _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAca_Year.DataSource = ds
        ddlAca_Year.DataTextField = "acy_descr"
        ddlAca_Year.DataValueField = "acd_id"
        ddlAca_Year.DataBind()
        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                               & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("sBSUID") + "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAca_Year.Items(ddlAca_Year.Items.IndexOf(li)).Selected = True
    End Sub
End Class
