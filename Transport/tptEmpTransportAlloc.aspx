<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptEmpTransportAlloc.aspx.vb" Inherits="Transport_tptEmpTransportAlloc" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i>Staff Transport Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%">

                    <tr>
                        <td align="center">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label></td>                                    
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Transport Alloted </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAlloted" runat="server">
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>Yes</asp:ListItem>
                                            <asp:ListItem>No</asp:ListItem>
                                        </asp:DropDownList>&nbsp;
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Employee Category</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlEmpCategory" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" OnClick="btnSearch_Click" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStudTPT" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            Width="100%" PageSize="20" OnSelectedIndexChanged="gvStudTPT_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField Visible="False" HeaderText="HideID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpId" runat="server" Text='<%# Bind("EMP_ID") %>' __designer:wfdid="w1"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Staff No">
                                                    <HeaderTemplate>
                                                       <asp:Label ID="lblStu_NoH4" runat="server" __designer:wfdid="w2">Staff No</asp:Label><br />
                                                       <asp:TextBox ID="txtStaffNo" runat="server" Width="75%" __designer:wfdid="w3"></asp:TextBox></td>
                                                       <asp:ImageButton ID="btnSearchstaffNo" OnClick="btnSearchstaffNo_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w4"></asp:ImageButton>
                                                                                  
                                                    </HeaderTemplate>

                                                    <ItemStyle></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("EMPNO") %>'  __designer:wfdid="w1"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="DESCR" HeaderText="Staff Name">
                                                    <HeaderTemplate>
                                                       <asp:Label ID="lblStu_NoH5" runat="server" __designer:wfdid="w349">Staff Name</asp:Label><br />
                                                       <asp:TextBox ID="txtStaffName" runat="server" Width="75%" __designer:wfdid="w350"></asp:TextBox>
                                                       
                                                       <asp:ImageButton ID="btnSearchstaffName" OnClick="btnSearchstaffName_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w351"></asp:ImageButton>
                                                                                   
                                                    </HeaderTemplate>

                                                    <ItemStyle></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("EMPNAME") %>' __designer:wfdid="w348"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Onwards">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="border-bottom: buttontext 1px solid" valign="top" colspan="3">Onwards</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Area</td>
                                                                    <td>Trip</td>
                                                                    <td>PickUp</td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="txtArea1" runat="server" Width="75%" __designer:wfdid="w13"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearchArea1" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w14" OnClick="btnSearchArea1_Click"></asp:ImageButton>
                                                                                   
                                                                    </td>
                                                                    <td valign="top">
                                                                       <asp:TextBox ID="txtTrip1" runat="server" Width="75%" __designer:wfdid="w15"></asp:TextBox>
                                                                       <asp:ImageButton ID="btnSearchTrip1" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w16" OnClick="btnSearchTrip1_Click"></asp:ImageButton>
                                                                                    
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="txtpickup1" runat="server" Width="75%" __designer:wfdid="w17"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearchPickup1" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w18" OnClick="btnSearchPickup1_Click"></asp:ImageButton>
                                                                                   
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        
                                                                        <asp:Label ID="lblO1" runat="server" Text='<%# Bind("AREAOWN") %>' Width="32%" __designer:wfdid="w10"></asp:Label>
                                                                    
                                                                        <asp:Label ID="lblO2" runat="server" Text='<%# Bind("TRPOWN") %>' Width="32%" __designer:wfdid="w11"></asp:Label>
                                                                  
                                                                        <asp:Label ID="lblO3" runat="server" Text='<%# Bind("PICKUPOWN") %>' Width="32%" __designer:wfdid="w12"></asp:Label>
                                                                
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Return">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="border-bottom: buttontext 1px solid" valign="top" colspan="3">Return</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Area</td>
                                                                    <td>Trip</td>
                                                                    <td>PickUp</td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                       <asp:TextBox ID="txtArea2" runat="server" Width="75%" __designer:wfdid="w22"></asp:TextBox>
                                                                       <asp:ImageButton ID="btnSearchArea2" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w23" OnClick="btnSearchArea2_Click"></asp:ImageButton>
                                                                                    
                                                                    </td>
                                                                    <td valign="top">
                                                                            <asp:TextBox ID="txtTrip2" runat="server" Width="75%" __designer:wfdid="w24"></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchTrip2" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w25" OnClick="btnSearchTrip2_Click"></asp:ImageButton>
                                                                                    
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="txtPickup3" runat="server" Width="75%" __designer:wfdid="w26"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearchPickup2" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w27" OnClick="btnSearchPickup2_Click"></asp:ImageButton>
                                                                                    
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </HeaderTemplate>

                                                    <ItemTemplate>
                                                        
                                                                        <asp:Label ID="lblR1" runat="server" Text='<%# Bind("AREARTN") %>' Width="32%" __designer:wfdid="w19"></asp:Label>
                                                                    
                                                                        <asp:Label ID="lblR2" runat="server" Text='<%# Bind("TRPRTN") %>' Width="32%" __designer:wfdid="w20"></asp:Label>
                                                                   
                                                                        <asp:Label ID="lblR3" runat="server" Text='<%# Bind("PICKUPRTN") %>' Width="32%" __designer:wfdid="w21" Font-Overline="False"></asp:Label>
                                                                
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField HeaderText="View" Text="View" CommandName="view">
                                                    <ItemStyle  HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

