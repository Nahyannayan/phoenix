﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="tptVehicle_Allocation_View2.aspx.vb"
    Inherits="Transport_tptVehicle_Allocation_View2" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    
     <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Allocated units of Drivers/Conductors
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table align="center" width="100%">
        
        <tr>
            <td align="left" width="100%">
                <asp:GridView ID="GridInfo" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" PageSize="20"
                    AutoGenerateColumns="false" EmptyDataText="Information not available." Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Emp No
                                            <br />
                                              <asp:TextBox ID="txt1" runat="server" Width="75%"></asp:TextBox>
                                              <asp:ImageButton ID="img1" runat="server" ImageAlign="Middle" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("EMPNO")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Emp Name<br />
                                              <asp:TextBox ID="txt2" runat="server" Width="75%"></asp:TextBox>
                                              <asp:ImageButton ID="img2" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>
                               
                                <%# Eval("EmpName")%>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                               Designation<br />
                                              <asp:TextBox ID="txt3" runat="server" Width="75%"></asp:TextBox>
                                              <asp:ImageButton ID="img3" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                               <%# Eval("DES_DESCR")%> 
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                 Work BSU<br />
                                              <asp:TextBox ID="txt4" runat="server" Width="75%"></asp:TextBox>
                                              <asp:ImageButton ID="img4" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>
                               
                           <%# Eval("WorkBSU")%>
                               
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                               Visa BSU<br />
                                              <asp:TextBox ID="txt5" runat="server" Width="75%"></asp:TextBox>
                                              <asp:ImageButton ID="img5" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>
                               
                            <%# Eval("VisaBSU")%>
                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Allocated BSU<br />
                                              <asp:TextBox ID="txt6" runat="server" Width="75%"></asp:TextBox>
                                              <asp:ImageButton ID="img6" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                            </HeaderTemplate>
                            <ItemTemplate>
                              
                            <%# Eval("AllocBSU") %>
                            
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <%--<SelectedRowStyle CssClass="Green" Wrap="False" />--%>
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <%--<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
                </asp:GridView>
            </td>
        </tr>
    <tr>
        <td align="center" colspan="4" >
<asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export" /></td>
    </tr>
    </table>

            </div>
        </div>
    </div>
</asp:Content> 
