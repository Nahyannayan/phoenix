﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System

Partial Class Transport_tptTransportApproval_EnqAndExistStudents

    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        hfApprovePrintStatus.Value = ""
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                hfSave.Value = "0"
                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                gvStud.Attributes.Add("bordercolor", "#1b80b6")
                gvOnward.Attributes.Add("bordercolor", "#1b80b6")
                gvReturn.Attributes.Add("bordercolor", "#1b80b6")
                tr2.Visible = False
                BindBSU()
                'studTable.Rows(2).Visible = False
                'studTable.Rows(1).Cells()
                studTable.Rows(2).Cells(0).Visible = False
                studTable.Rows(2).Cells(1).Visible = False
                'studTable.Rows(1).Cells(5).Visible = False
                'studTable.Rows(1).Cells(6).ColSpan = "7"
                BindStage()
                'ddlAcademicYear.Items.Insert(0, New ListItem("--", "0"))
                ddlGrade.Items.Insert(0, New ListItem("ALL", "0"))
                ddlOnward.Items.Insert(0, New ListItem("--", "0"))
                ddlReturn.Items.Insert(0, New ListItem("--", "0"))
                rdReqType.SelectedIndex = 0

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub



#Region "Private methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    ReadOnly Property conStr() As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString
        End Get
    End Property

    ReadOnly Property conStrT() As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ToString
        End Get
    End Property

#End Region

    Sub BindStage()

        Dim str_query As String = "SELECT PRO_ID,PRO_DESCRIPTION FROM PROCESSFO_SYS_M where PRO_ID<=7"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStr, CommandType.Text, str_query)
        ddlStage.DataSource = ds
        ddlStage.DataTextField = "PRO_DESCRIPTION"
        ddlStage.DataValueField = "PRO_ID"
        ddlStage.DataBind()
        ddlStage.Items.Insert(0, New ListItem("ALL", "0"))
    End Sub

    Sub BindBSU()

        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_TYPE <> 'O' AND BSU_BSCHOOL = '1' AND BSU_BGEMSSCHOOL = '1' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStr, CommandType.Text, str_query)
        ddlBSU.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBSU.DataValueField = "BSU_ID"
        ddlBSU.DataBind()
        ddlBsu.Items.Insert(0, New ListItem("--", "0"))

    End Sub

    Sub BindAcademicYearForCurriculum()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + ddlBsu.SelectedItem.Value + "' AND ACD_CLM_ID=" + ddlCurriculum.SelectedItem.Value _
                                  & " ORDER BY ACY_ID DESC"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + ddlBsu.SelectedItem.Value + "' AND ACD_CLM_ID=" + ddlCurriculum.SelectedItem.Value
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then


            Dim li As New ListItem
            li.Text = ds.Tables(0).Rows(0).Item(0)
            li.Value = ds.Tables(0).Rows(0).Item(1)
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        End If

        BindGrade()

    End Sub


    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + ddlBsu.SelectedValue.ToString + "'" _
                                  & " ORDER BY ACY_ID DESC"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(conStr, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()

        Dim str_query1 As String = "SELECT ACY_DESCR,ACD_ID FROM OASIS..ACADEMICYEAR_M INNER JOIN " _
                                & " OASIS..ACADEMICYEAR_D ON ACD_ACY_ID = ACY_ID WHERE ACD_BSU_ID ='" + ddlBsu.SelectedItem.Value + "' AND ACD_CURRENT = 1"
        Dim ds1 As DataSet
        ds1 = SqlHelper.ExecuteDataset(conStr, CommandType.Text, str_query1)
        Dim acadYr As String
        Dim acadId As String
        With ds1.Tables(0).Rows(0)
            acadYr = .Item(0)
            acadId = .Item(1)
        End With
        If Not ddlAcademicYear.Items.FindByValue(acadId) Is Nothing Then
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(acadId).Selected = True
        End If
        BindGrade()
    End Sub

    Sub BindPickupArea(ByVal bsuid As String, ByVal defaultArea As String)
        'Dim strBaseDescr As String
        'Dim strBaseID As String
        Dim counter As Integer = 0
        ddlPickupArea.Items.Clear()
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A INNER JOIN " _
                                & " TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                & " WHERE PNT_BSU_ID='" + bsuid + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStrT, CommandType.Text, str_query)
        ddlPickupArea.DataSource = ds
        ddlPickupArea.DataTextField = "SBL_DESCRIPTION"
        ddlPickupArea.DataValueField = "SBL_ID"
        ddlPickupArea.DataBind()
        ddlPickupArea.Items.Insert(0, New ListItem("--", "0"))
        'If defaultArea <> "" Then

        '    If Not ddlPickupArea.Items.FindByValue(hfPSBL_ID.Value) Is Nothing Then
        '        ddlPickupArea.Items.FindByValue(hfPSBL_ID.Value).Selected = True
        '    End If
        '    'Using AreaReader As SqlDataReader = GetAreaForAdmin(defaultArea)
        '    '    While AreaReader.Read
        '    '        strBaseDescr = Convert.ToString(AreaReader("SBL_DESCRIPTION"))
        '    '        strBaseID = Convert.ToString(AreaReader("SBL_ID"))

        '    '        ddlPickupArea.Items.Insert(1, New ListItem(strBaseDescr, strBaseID))
        '    '        ddlPickupArea.SelectedIndex = 1
        '    '    End While
        '    'End Using
        '    'For i = 0 To ddlPickupArea.Items.Count - 1
        '    '    If ddlPickupArea.Items.Item(i).Value = strBaseID Then
        '    '        counter = counter + 1
        '    '        If counter = 2 Then
        '    '            ddlPickupArea.Items.RemoveAt(i)
        '    '            Exit For
        '    '        End If

        '    '    End If
        '    'Next i

        'End If

        Dim count As Integer
        count = ddlPickupArea.Items.Count
        ddlPickupArea.Items.Insert(count, New ListItem("Other", "1"))
    End Sub


    Sub BindDropoffArea(ByVal bsuid As String, ByVal defaultArea As String)
        Dim strBaseDescr As String
        Dim strBaseID As String
        Dim counter As Integer = 0
        ddlDropoffArea.Items.Clear()
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A INNER JOIN " _
                                & " TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                & " WHERE PNT_BSU_ID='" + bsuId + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStrT, CommandType.Text, str_query)
        ddlDropoffArea.DataSource = ds
        ddlDropoffArea.DataTextField = "SBL_DESCRIPTION"
        ddlDropoffArea.DataValueField = "SBL_ID"
        ddlDropoffArea.DataBind()
        ddlDropoffArea.Items.Insert(0, New ListItem("--", "0"))

        If defaultArea <> "" Then

            If Not ddlDropoffArea.Items.FindByValue(hfDSBL_ID.Value) Is Nothing Then
                ddlDropoffArea.Items.FindByValue(hfDSBL_ID.Value).Selected = True
            End If
            'Using AreaReader As SqlDataReader = GetAreaForAdmin(defaultArea)
            '    While AreaReader.Read
            '        strBaseDescr = Convert.ToString(AreaReader("SBL_DESCRIPTION"))
            '        strBaseID = Convert.ToString(AreaReader("SBL_ID"))

            '        ddlDropoffArea.Items.Insert(1, New ListItem(strBaseDescr, strBaseID))
            '        ddlDropoffArea.SelectedIndex = 1
            '    End While
            'End Using
            'For i = 0 To ddlDropoffArea.Items.Count - 1
            '    If ddlDropoffArea.Items.Item(i).Value = strBaseID Then
            '        counter = counter + 1
            '        If counter = 2 Then
            '            ddlDropoffArea.Items.RemoveAt(i)
            '            Exit For
            '        End If

            '    End If
            'Next i
        End If
        Dim count As Integer
        count = ddlDropoffArea.Items.Count
        ddlDropoffArea.Items.Insert(count, New ListItem("Other", "1"))
    End Sub


    Sub BindPickupDropoffPoints(ByVal ddlPickDropPoints As DropDownList, ByVal area As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        ddlPickDropPoints.Items.Clear()
        Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M WHERE PNT_BSU_ID='" + hfBsuID.Value + "'" _
                        & " AND PNT_SBL_ID=" + area
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPickDropPoints.DataSource = ds
        ddlPickDropPoints.DataTextField = "PNT_DESCRIPTION"
        ddlPickDropPoints.DataValueField = "PNT_ID"
        ddlPickDropPoints.DataBind()
        ddlPickDropPoints.Items.Insert(0, New ListItem("--", "0"))
    End Sub

    Public Shared Function GetAreaForAdmin(ByVal defaultArea As String) As SqlDataReader

        Dim sqlArea As String = "SELECT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M " _
                                   & " WHERE SBL_DESCRIPTION = '" & defaultArea & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection()
        Dim command As SqlCommand = New SqlCommand(sqlArea, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Sub BindCurriculum(ByVal bsu_id As String)
        ddlCurriculum.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CLM_ID,CLM_DESCR FROM CURRICULUM_M AS A" _
                                & " INNER JOIN ACADEMICYEAR_D AS B ON A.CLM_ID=B.ACD_CLM_ID" _
                                & " WHERE ACD_BSU_ID='" + bsu_id + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurriculum.DataSource = ds
        ddlCurriculum.DataTextField = "CLM_DESCR"
        ddlCurriculum.DataValueField = "CLM_ID"
        ddlCurriculum.DataBind()
        If ddlCurriculum.Items.Count = 1 Then
            studTable.Rows(2).Cells(0).Visible = False
            studTable.Rows(2).Cells(1).Visible = False
            'studTable.Rows(1).Cells(5).Visible = False
            'studTable.Rows(1).Cells(8).ColSpan = "7"
            BindAcademicYear()
        Else
            studTable.Rows(2).Cells(0).Visible = True
            studTable.Rows(2).Cells(1).Visible = True
            'studTable.Rows(1).Cells(5).Visible = True
            'studTable.Rows(1).Cells(8).ColSpan = "4"
            BindAcademicYearForCurriculum()
        End If
    End Sub


    Sub GetData(ByVal id As String)
        'pnlDetails.ScrollBars = "Vertical"
        trSummativeError.Visible = False
        btnEdit.Enabled = True
        btnSave.Enabled = True
        trSaveOk.Visible = False
        'trPhoto.Visible = True
        tdAppPhotoText.Visible = True
        tdAppPhoto.Visible = True
        'trOtherDropoff.Visible = False
        'trOtherPickup.Visible = False
        trPArea.Visible = False
        trPPoint.Visible = False
        trDArea.Visible = False
        trDPoint.Visible = False
        'trRemarks.Visible = False
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ID", id)
        param(1) = New SqlParameter("@REQTYPE", hfREQ_Type.Value)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStrT, CommandType.StoredProcedure, "[TRANSPORT].[GETENQDATAFORAPPROVAL]", param)
        btnSave.Visible = False
        If ds.Tables(0).Rows.Count <> 0 Then
            With (ds.Tables(0).Rows(0))

                txtFrom.Text = Format(CDate(.Item(0)), "dd-MMM-yyyy")
                hfBsuID.Value = .Item(1)
                BindPickupArea(hfBsuID.Value, .Item(2))
                hfPSBL_ID.Value = .Item(3)
                hfPPNT_ID.Value = .Item(5)
                BindPickupDropoffPoints(ddlPickupPoint, hfPSBL_ID.Value)
                lblPlocation.Text = .Item(6)
                BindDropoffArea(hfBsuID.Value, .Item(7))
                lblDropoffArea.Text = .Item(7)
                hfDSBL_ID.Value = .Item(8)
                'lblDropoffPoint.Text = .Item(9)
                hfDPNT_ID.Value = .Item(10)
                BindPickupDropoffPoints(ddlDropoffPoint, hfDSBL_ID.Value)
                lblDlocation.Text = .Item(11)
                Dim otherPickupArea As String = .Item(12)
                Dim otherDropoffArea As String = .Item(13)
                hfBBT_ID.Value = .Item(14)

                If .Item(15) = 0 Then
                    ddlTripType.SelectedIndex = 1
                ElseIf .Item(15) = 1 Then
                    ddlTripType.SelectedIndex = 2
                ElseIf .Item(15) = 2 Then
                    ddlTripType.SelectedIndex = 3
                End If

                hfAcdID.Value = .Item(16)
                hfGRM_ID.Value = .Item(17)
                hfSHF_ID.Value = .Item(18)
                lblAreaDescription.Text = .Item(19)

                hfOnwardTripId.Value = .Item(20)
                hfReturnTripId.Value = .Item(21)

                If .Item(22) = "0" Then
                    chkAutoApprove.Checked = False
                Else
                    chkAutoApprove.Checked = True
                End If

                Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepathVirtual").ConnectionString
                If .Item(23) <> "" Then

                    'trPhoto.Visible = True
                    tdAppPhotoText.Visible = True
                    tdAppPhoto.Visible = True

                    If rdReqType.SelectedIndex = 0 Then
                        Dim str As String = .Item(23)
                        Dim strR As String = str.Replace("\", "/")
                        imgEmpImage.ImageUrl = str_img + strR
                        lblStuPhoto.Text = "Student Photo"
                    Else
                        Dim length As Integer
                        Dim str As String = .Item(23).ToString
                        length = str.Length
                        Dim imagePath As String = str_img + str.Substring(48, length - 48).ToString
                        imagePath = imagePath.Replace("\", "/")
                        imgEmpImage.ImageUrl = imagePath
                        lblStuPhoto.Text = "Applicant Photo"
                    End If
                Else
                    imgEmpImage.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgEmpImage.AlternateText = "No Image found"
                End If

                If Not String.IsNullOrEmpty(.Item(24).ToString) Then
                    hfEQS_Id.Value = .Item(24)
                End If

                If Not ddlPickupArea.Items.FindByValue(hfPSBL_ID.Value) Is Nothing Then
                    ddlPickupArea.ClearSelection()
                    ddlPickupArea.Items.FindByValue(hfPSBL_ID.Value).Selected = True
                End If

                If Not ddlDropoffArea.Items.FindByValue(hfDSBL_ID.Value) Is Nothing Then
                    ddlDropoffArea.ClearSelection()
                    ddlDropoffArea.Items.FindByValue(hfDSBL_ID.Value).Selected = True
                End If

                If Not ddlPickupPoint.Items.FindByValue(hfPPNT_ID.Value) Is Nothing Then
                    ddlPickupPoint.ClearSelection()
                    ddlPickupPoint.Items.FindByValue(hfPPNT_ID.Value).Selected = True
                End If

                If Not ddlDropoffPoint.Items.FindByValue(hfDPNT_ID.Value) Is Nothing Then
                    ddlDropoffPoint.ClearSelection()
                    ddlDropoffPoint.Items.FindByValue(hfDPNT_ID.Value).Selected = True
                End If

                gvOnward.Visible = False
                gvReturn.Visible = False
                tdEnqOnward.Visible = False
                tdEnqReturn.Visible = False
                tdReturn.Visible = True
                tdOnward.Visible = True

                If ddlOnward.Items.Count <> 0 Then
                    BindCapacity(gvOnward, ddlOnward.SelectedValue.ToString, "Onward")
                End If
                If ddlReturn.Items.Count <> 0 Then
                    BindCapacity(gvReturn, ddlReturn.SelectedValue.ToString, "Return")
                End If
                lblReference.Text = hfBBT_ID.Value
                If .Item(12) <> "" Then
                    lblPickupArea.Text = .Item(12)
                    lblPickupArea.Visible = True
                Else
                    lblPickupArea.Text = ""
                End If
                If .Item(13) <> "" Then
                    lblDropoffArea.Text = .Item(13)
                    lblDropoffArea.Visible = True
                Else
                    lblDropoffArea.Text = ""
                End If
                'If .Item(2) = "OTHER" Then

                '    If .Item(12) <> "" Then
                '        'lblPickupArea.BackColor = Drawing.Color.Red
                '        'lblPickupArea.ForeColor = Drawing.Color.White
                '        lblPickupArea.Text = .Item(12)
                '        'lblPickupPoint.Text = ""
                '        'trOtherPickup.Visible = True
                '    Else
                '        ddlTripType.SelectedIndex = 3
                '        lblPickupArea.Text = "DROPOFFONLY"
                '        lblPickupPoint.Text = "DROPOFFONLY"
                '        trOtherPickup.Visible = True
                '    End If

                'Else
                '    trOtherPickup.Visible = False
                'End If

                'If .Item(7) = "OTHER" Then
                '    If .Item(13) <> "" Then
                '        lblDropoffArea.Text = .Item(13)
                '        lblDropoffPoint.Text = ""
                '        trOtherDropoff.Visible = True
                '    Else
                '        ddlTripType.SelectedIndex = 2
                '        lblDropoffArea.Text = "PICKUPONLY"
                '        lblDropoffPoint.Text = "PICKUPONLY"
                '    End If
                'Else
                '    trOtherDropoff.Visible = False
                'End If


                If ddlPickupPoint.SelectedValue.ToString = "" Then
                    ddlOnward.Items.Clear()
                    ddlOnward.Items.Insert(0, New ListItem("--", "0"))
                Else
                    BindTrip(ddlOnward, "Onward")
                End If

                If ddlDropoffPoint.SelectedValue.ToString = "" Then
                    ddlReturn.Items.Clear()
                    ddlReturn.Items.Insert(0, New ListItem("--", "0"))
                Else
                    BindTrip(ddlReturn, "Return")
                End If

                If Not ddlOnward.Items.FindByValue(hfOnwardTripId.Value) Is Nothing Then
                    ddlOnward.ClearSelection()
                    ddlOnward.Items.FindByValue(hfOnwardTripId.Value).Selected = True
                End If

                If Not ddlReturn.Items.FindByValue(hfReturnTripId.Value) Is Nothing Then
                    ddlReturn.ClearSelection()
                    ddlReturn.Items.FindByValue(hfReturnTripId.Value).Selected = True
                End If


                EnableDisableControls(False)
            End With
        End If


    End Sub

    Sub EnableDisableControls(ByVal value As Boolean)
        txtFrom.Enabled = value
        imgFrom.Enabled = value
        lblAppNo.Enabled = value
        lblAppName.Enabled = value
        lblPlocation.Enabled = value
        ddlPickupArea.Enabled = value
        ddlPickupPoint.Enabled = value
        ddlOnward.Enabled = value
        lnkPickup.Enabled = value
        ddlDropoffArea.Enabled = value
        ddlDropoffPoint.Enabled = value
        ddlReturn.Enabled = value
        lnkDropOff.Enabled = value
        ddlTripType.Enabled = value

        txtRemarks.Enabled = value
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_query As String = "SELECT GRM_ID,GRM_DISPLAY FROM GRADE_BSU_M AS A " _
                                & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY GRD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStr, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_ID"
        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, New ListItem("ALL", "0"))
    End Sub

    Function getChkImage(ByVal status As Integer) As String
        If status = "0" Then
            Return "~/Images/buttons/cross-icon.png"
        Else
            Return "~/Images/buttons/check-icon.png"
        End If
    End Function

    Function getSaveImage(ByVal status As Integer) As String
        If status = "0" Then
            Return "~/Images/buttons/cross-icon.png"
        Else
            Return "~/Images/buttons/check-icon.png"
        End If
    End Function


    Sub BindGrid()
        Dim str_query As String
        Dim rdlReqType As RadioButtonList = Page.FindControl("rdReqType")
        gvOnward.Visible = False
        gvReturn.Visible = False
        If rdReqType.SelectedIndex = 0 Then
            gvStud.Columns(10).Visible = False
        Else
            gvStud.Columns(10).Visible = True
        End If

        'gvStud.Columns(10).Visible = False

        For i As Integer = 0 To rdReqType.Items.Count - 1
            If rdReqType.Items(i).Selected Then
                If i = 0 Then
                    str_query = "SELECT BBT_ID,STU_NO AS 'STU_ID','' AS EQS_APPLNO," _
                    & " ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS 'STU_NAME', " _
                    & " '' AS 'STAGE', " _
                    & " CASE BBT_BAOAPPROVE " _
                    & " WHEN 1 THEN 'APPROVED' WHEN 0 THEN 'REJECTED' ELSE ISNULL((CONVERT(VARCHAR(20),BBT_BAOAPPROVE)),'PENDING') END AS 'STATUS'," _
                    & " ISNULL(BBT_STUID,'') AS 'STUDENT_ID' , ISNULL(BBT_bAutoApprove,'0') AS 'CHKAPRSTATUS', ISNULL(BBT_bSave,'0') AS 'SAVED'," _
                    & " STU_GRD_ID AS 'GRADE' FROM STUDENT_BBTREQ_GEMS_S " _
                    & " INNER JOIN OASIS..STUDENT_M with(nolock) ON STU_ID = BBT_STUID" _
                    & " WHERE STU_BSU_ID = '" + ddlBsu.SelectedItem.Value + "' AND BBT_ACD_ID = '" + ddlAcademicYear.SelectedItem.Value + "'" _
                    & " AND BBT_REQTYPE='STUDENTREQ'"
                    If txtApplicantName.Text <> "" Then
                        str_query = str_query + " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtApplicantName.Text + "%'"
                    End If
                    'If txtApplicationNO.Text <> "" Then
                    '    str_query = str_query + " AND STU_ID LIKE '%" + txtApplicationNO.Text + "%'"
                    'End If
                    'added by Mahesh on 24-Sep-2020
                    If txtApplicationNO.Text <> "" Then
                        str_query = str_query + " AND STU_NO LIKE '%" + txtApplicationNO.Text + "%'"
                    End If

                    If ddlGrade.SelectedIndex <> 0 Then
                        str_query = str_query + " AND STU_GRM_ID = '" + ddlGrade.SelectedItem.Value + "'"
                    End If
                    gvStud.Columns(1).Visible = False
                    gvStud.Columns(2).Visible = True
                    gvStud.Columns(5).Visible = False
                    hfREQ_Type.Value = "Student"
                ElseIf i = 1 Then
                    str_query = "SELECT BBT_ID,BBT_STUID AS 'STU_ID',EQS_APPLNO,(ISNULL(EQM_APPLFIRSTNAME,'')+' '+ISNULL(EQM_APPLMIDNAME,'')+' '+ISNULL(EQM_APPLLASTNAME,'')) AS 'STU_NAME'," _
                    & " PRO_DESCRIPTION AS 'STAGE' , CASE BBT_BAOAPPROVE" _
                    & " WHEN 1 THEN 'APPROVED' WHEN 0 THEN 'REJECTED' ELSE ISNULL((CONVERT(VARCHAR(20),BBT_BAOAPPROVE)),'PENDING')" _
                    & " END AS 'STATUS', ISNULL(BBT_STUID,'') AS 'STUDENT_ID', ISNULL(BBT_bAutoApprove,'0') AS 'CHKAPRSTATUS', ISNULL(BBT_bSave,'0') AS 'SAVED'," _
                    & " EQS_GRD_ID AS 'GRADE' FROM OASIS_TRANSPORT..STUDENT_BBTREQ_GEMS_S" _
                    & " INNER JOIN OASIS..ENQUIRY_SCHOOLPRIO_S ON BBT_EQSID = EQS_ID" _
                    & " INNER JOIN OASIS..ENQUIRY_M ON EQS_EQM_ENQID = EQM_ENQID" _
                    & " INNER JOIN OASIS..PROCESSFO_SYS_M ON PRO_ID = EQS_CURRSTATUS" _
                    & " WHERE EQS_BSU_ID = '" + ddlBsu.SelectedItem.Value.ToString + "'" _
                    & " AND EQS_ACD_ID = '" + ddlAcademicYear.SelectedItem.Value + "'" _
                    & " AND BBT_REQTYPE='ENQUIRY'"

                    If txtApplicantName.Text <> "" Then
                        str_query = str_query + " AND ISNULL(EQM_APPLFIRSTNAME,'')+' '+ISNULL(EQM_APPLMIDNAME,'')+' '+ISNULL(EQM_APPLLASTNAME,'') LIKE '%" + txtApplicantName.Text + "%'"
                    End If
                    If txtApplicationNO.Text <> "" Then
                        str_query = str_query + " AND EQS_APPLNO LIKE '%" + txtApplicationNO.Text + "%'"
                    End If
                    gvStud.Columns(1).Visible = True
                    gvStud.Columns(2).Visible = False
                    gvStud.Columns(5).Visible = True
                    hfREQ_Type.Value = "Enquiry"
                    If ddlGrade.SelectedIndex <> 0 Then
                        str_query = str_query + " AND EQS_GRM_ID = '" + ddlGrade.SelectedItem.Value + "'"
                    End If
                    If ddlStage.SelectedIndex <> 0 Then
                        str_query = str_query + " AND EQS_CURRSTATUS = '" + ddlStage.SelectedItem.Value + "'"
                    End If
                End If
            End If
        Next


        If rbtnList.SelectedIndex <> 0 Then
            If rbtnList.SelectedIndex = 1 Then
                str_query = str_query + " AND BBT_BAOAPPROVE IS NULL "
            ElseIf rbtnList.SelectedIndex = 2 Then
                str_query = str_query + " AND BBT_BAOAPPROVE = 1"
            ElseIf rbtnList.SelectedIndex = 3 Then
                str_query = str_query + " AND BBT_BAOAPPROVE = 0"
            End If

        End If

        If txtRef.Text <> "" Then
            str_query = str_query + " AND BBT_ID LIKE '%" + txtRef.Text + "%'"
        End If

        Dim ddlgvChkStatus As DropDownList
        Dim strChkStatus As String = ""

        Dim ddlgvSaveStatus As DropDownList
        Dim strSaveStatus As String = ""

        If gvStud.Rows.Count > 0 Then

            If rdReqType.SelectedIndex = 1 Then
                gvStud.Columns(10).Visible = True
            End If
            gvStud.Columns(11).Visible = True
            ddlgvChkStatus = gvStud.HeaderRow.FindControl("ddlgvChkStatus")
            If ddlgvChkStatus.SelectedIndex = 1 Then
                str_query += " AND ISNULL(BBT_bAutoApprove,'')='1'"
            ElseIf ddlgvChkStatus.SelectedIndex = 2 Then
                str_query += " AND ISNULL(BBT_bAutoApprove,'0')='0'"
            End If
            strChkStatus = ddlgvChkStatus.SelectedItem.Text

            ddlgvSaveStatus = gvStud.HeaderRow.FindControl("ddlgvSaveStatus")
            If ddlgvSaveStatus.SelectedIndex = 1 Then
                str_query += " AND ISNULL(BBT_bSave,'')='1'"
            ElseIf ddlgvSaveStatus.SelectedIndex = 2 Then
                str_query += " AND ISNULL(BBT_bSave,'0')='0'"
            End If
            strSaveStatus = ddlgvSaveStatus.SelectedItem.Text
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStrT, CommandType.Text, str_query)
        gvStud.Columns(7).Visible = False
        gvStud.DataSource = ds
        'added by mahesh 24-Sep-2020
        StudentDetails = ds.Tables(0)
        gvStud.DataBind()


        If gvStud.Rows.Count > 0 Then
            If strChkStatus <> "" Then
                ddlgvChkStatus = gvStud.HeaderRow.FindControl("ddlgvChkStatus")
                ddlgvChkStatus.ClearSelection()
                ddlgvChkStatus.Items.FindByText(strChkStatus).Selected = True
            End If
            If strSaveStatus <> "" Then
                ddlgvSaveStatus = gvStud.HeaderRow.FindControl("ddlgvSaveStatus")
                ddlgvSaveStatus.ClearSelection()
                ddlgvSaveStatus.Items.FindByText(strSaveStatus).Selected = True
            End If



        End If

        tr2.Visible = True
        gvStud.Visible = True


        If ds.Tables(0).Rows.Count <> 0 Then
            With ds.Tables(0).Rows(0)
                hfStudentID.Value = .Item(6)
                If .Item(7) = "0" Then
                    getChkImage("0")
                Else
                    getChkImage("1")
                End If
                If .Item(8) = "0" Then
                    getSaveImage("0")
                Else
                    getSaveImage("1")
                End If
            End With

        End If
        'For Each dcfColumn As DataControlField In gvStud.Columns
        '    If dcfColumn.HeaderText = "Status" Then
        '        dcfColumn.Visible = False
        '    End If
        'Next
        For i As Integer = 0 To rdReqType.Items.Count - 1
            If rdReqType.Items(1).Selected Then
                If ds.Tables(0).Rows.Count <> 0 Then
                    With ds.Tables(0).Rows(0)

                        If .Item(4) <> "ENROLLMENT" Then
                            chkAutoApprove.Visible = True
                            chkAutoDetails.Visible = True
                        Else

                            chkAutoApprove.Visible = False
                            chkAutoDetails.Visible = False
                        End If

                    End With
                End If
            Else
                chkAutoApprove.Visible = False
                chkAutoDetails.Visible = False
            End If


        Next
    End Sub

    Protected Sub btnGet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGet.Click

        BindGrid()

    End Sub

    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_PNT_ID=" + ddlPickupPoint.SelectedValue.ToString _
                        & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_PNT_ID=" + ddlDropoffPoint.SelectedValue.ToString _
                 & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL" _
                 & " ORDER BY BNO_DESCR,TRP_DESCR"


        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"

        ddlTrip.Items.Insert(0, li)

    End Sub

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        BindCurriculum(ddlBsu.SelectedItem.Value)
        gvStud.Visible = False
        tr2.Visible = False
    End Sub

    Protected Sub ddlgvChkStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
    End Sub


    Protected Sub lnkbtnView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MPViewRequests.Show()
        btnApprove.Visible = True
        btnReject.Visible = True
        btnSave.Visible = True
        btnEdit.Visible = True
        hfViewBtn.Value = "0"
    End Sub

    Protected Sub lnkbtnViewBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MPViewRequests.Show()
        btnApprove.Visible = False
        btnReject.Visible = False
        btnSave.Visible = False
        btnEdit.Visible = False
        hfViewBtn.Value = "1"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        MPViewRequests.Hide()
    End Sub

    Protected Sub gvStud_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvStud.PageIndexChanging

        'added by mahesh 24-Sep-2020
        gvStud.PageIndex = e.NewPageIndex
        gvStud.DataSource = StudentDetails
        gvStud.DataBind()
    End Sub

    Private Property StudentDetails() As DataTable
        Get
            Return ViewState("StudentDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("StudentDetails") = value
        End Set
    End Property


    Protected Sub gvStud_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStud.RowEditing

        Dim index As Integer = e.NewEditIndex
        Dim mySelectedRow As GridViewRow = gvStud.Rows(index)
        Dim stageValue As Label
        Dim statusValue As LinkButton
        statusValue = mySelectedRow.FindControl("lnkbtnView")
        If statusValue.Enabled Then
            btnPrint.Visible = False
        Else
            btnPrint.Visible = True
        End If
        stageValue = mySelectedRow.FindControl("lblStage")
        If rdReqType.SelectedIndex = 1 Then
            If stageValue.Text <> "ENROLLMENT" Then
                btnApprove.Visible = False
                btnReject.Visible = False
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Else
            btnApprove.Visible = True
            btnReject.Visible = True
        End If

        If hfViewBtn.Value = "1" Then
            btnApprove.Visible = False
            btnReject.Visible = False
            btnSave.Visible = False
            btnEdit.Visible = False

        End If

        Dim idValue As Label
        Dim nameValue As Label
        Dim stuId As Label
        stuId = mySelectedRow.FindControl("lblstudentid")
        hfStudentID.Value = stuId.Text
        Dim stuNo As Label = mySelectedRow.FindControl("lblStudNo")
        hfSTU_ID.Value = stuNo.Text


        For i As Integer = 0 To rdReqType.Items.Count - 1
            If rdReqType.Items(i).Selected Then
                If i = 0 Then
                    idValue = mySelectedRow.FindControl("lblStudNo")
                    nameValue = mySelectedRow.FindControl("lblName")
                    lblAppNo.Text = idValue.Text
                    lblAppName.Text = nameValue.Text
                    lblPopupId.Text = "Student ID"
                    lblPopupName.Text = "Student Name"
                ElseIf i = 1 Then
                    lblPopupId.Text = "Application No"
                    lblPopupName.Text = "Applicant Name"
                    idValue = mySelectedRow.FindControl("lblAppl")
                    nameValue = mySelectedRow.FindControl("lblName")
                    lblAppNo.Text = idValue.Text
                    lblAppName.Text = nameValue.Text
                End If
            End If
        Next

        If rdReqType.SelectedIndex = 0 Then
            GetData(hfStudentID.Value)
        Else
            GetData(lblAppNo.Text)
        End If
    End Sub



    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        btnSave.Visible = True
        EnableDisableControls(True)
        hfSave.Value = "0"
        MPViewRequests.Show()

    End Sub

    Function SaveEditData() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim primaryContact As String = ""
        Dim gender As String = ""

        Dim pickupArea As String
        Dim pickupPoint As String
        Dim dropoffArea As String
        Dim dropoffPoint As String
        Dim ptrdId As String
        Dim dtrdId As String

        If ddlPickupArea.SelectedValue.ToString = "" Then
            pickupArea = 1
            pickupPoint = 0
        Else
            pickupArea = ddlPickupArea.SelectedValue
            pickupPoint = ddlPickupPoint.SelectedValue

        End If

        If ddlDropoffArea.SelectedValue.ToString = "" Then
            dropoffArea = 1
            dropoffPoint = 0
        Else
            dropoffArea = ddlDropoffArea.SelectedValue
            dropoffPoint = ddlDropoffPoint.SelectedValue
        End If

        If ddlOnward.SelectedIndex = 0 Then
            ptrdId = ""
        Else
            ptrdId = ddlOnward.SelectedItem.Value
        End If

        If ddlReturn.SelectedIndex = 0 Then
            dtrdId = ""
        Else
            dtrdId = ddlReturn.SelectedItem.Value
        End If

        Dim bautoAppr As Integer
        If chkAutoApprove.Visible Then
            If chkAutoApprove.Checked = True Then
                bautoAppr = 1
            Else
                bautoAppr = 0
            End If
        Else
            bautoAppr = 0
        End If

        Dim tripType As Integer
        If ddlTripType.SelectedIndex = 1 Then
            tripType = 0
        ElseIf ddlTripType.SelectedIndex = 2 Then
            tripType = 1
        ElseIf ddlTripType.SelectedIndex = 3 Then
            tripType = 2
        End If

        Dim str_query As String = "exec updateGEMSSTUDBBTREQ " _
                               & "'" + hfBBT_ID.Value + "'," _
                               & "'" + pickupArea + "'," _
                               & "'" + pickupPoint + "'," _
                               & "'" + dropoffArea + "'," _
                               & "'" + dropoffPoint + "'," _
                               & "'" + bautoAppr.ToString + "'," _
                               & "'" + ptrdId + "'," _
                               & "'" + dtrdId + "'," _
                               & "'" + txtFrom.Text + "'," _
                               & "'" + tripType.ToString + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Return True
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If ddlTripType.SelectedIndex = 1 Then
                If ddlPickupArea.SelectedIndex = 0 Then
                    trPArea.Visible = True
                Else
                    trPArea.Visible = False
                End If

                If ddlPickupPoint.SelectedIndex = 0 Then
                    trPPoint.Visible = True
                Else
                    trPPoint.Visible = False
                End If

                If ddlDropoffArea.SelectedIndex = 0 Then
                    trDArea.Visible = True
                Else
                    trDArea.Visible = False
                End If
                If ddlDropoffPoint.SelectedIndex = 0 Then
                    trDPoint.Visible = True
                Else
                    trDPoint.Visible = False
                End If
            ElseIf ddlTripType.SelectedIndex = 2 Then
                trDArea.Visible = False
                trDPoint.Visible = False
                If ddlPickupArea.SelectedIndex = 0 Then
                    trPArea.Visible = True
                Else
                    trPArea.Visible = False
                End If

                If ddlPickupPoint.SelectedIndex = 0 Then
                    trPPoint.Visible = True
                Else
                    trPPoint.Visible = False
                End If
            ElseIf ddlTripType.SelectedIndex = 3 Then
                trPArea.Visible = False
                trPPoint.Visible = False
                If ddlDropoffArea.SelectedIndex = 0 Then
                    trDArea.Visible = True
                Else
                    trDArea.Visible = False
                End If
                If ddlDropoffPoint.SelectedIndex = 0 Then
                    trDPoint.Visible = True
                Else
                    trDPoint.Visible = False
                End If
            End If

            If trPArea.Visible Or trPPoint.Visible Or trDArea.Visible Or trDPoint.Visible Then
                MPViewRequests.Show()
                Exit Sub
            End If
            'If txtRemarks.Text = "" Then
            '    trRemarks.Visible = True
            '    MPViewRequests.Show()
            '    Exit Sub
            'Else
            '    trRemarks.Visible = False
            'End If

            If SaveEditData() Then
                trSaveOk.Visible = True
                BindGrid()
            End If
            hfSave.Value = "1"
            EnableDisableControls(False)
            btnApprove.Enabled = True
            btnReject.Enabled = True
            btnEdit.Enabled = False
            MPViewRequests.Show()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblFinalError.Text = "Data could not be saved"
        End Try


    End Sub

    Protected Sub ddlPickupArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPickupArea.SelectedIndexChanged
        BindPickupDropoffPoints(ddlPickupPoint, ddlPickupArea.SelectedItem.Value)
        ddlOnward.Items.Clear()
        ddlOnward.Items.Insert(0, New ListItem("--", "0"))
        MPViewRequests.Show()
    End Sub

    Sub SendApprovalEmail()


        Dim bsuID As String = ""
        Dim grade As String = ""
        Dim section As String = ""
        Dim stuName As String = ""
        Dim reqType As String = "APPROVAL"
        Dim regValue As String = ""
        Dim refID As Integer
        Dim Parent As String = ""
        Dim toEmail As String = ""
        Dim onwAreaPoint As String = ""
        Dim retAreaPoint As String = ""
        Dim stuType As String = "GEMS"
        Dim bsuName As String = ""
        Dim startDate As String = ""
        Dim reqDate As String = ""
        Dim regNum As String = ""
        Dim ID As String
        If rdReqType.SelectedIndex = 0 Then
            ID = hfStudentID.Value
        Else
            ID = lblAppNo.Text
        End If

        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ID", ID)
        param(1) = New SqlParameter("@REQTYPE", hfREQ_Type.Value)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStrT, CommandType.StoredProcedure, "[TRANSPORT].[GETDATAFORAPPROVAL]", param)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.count > 0 Then
                bsuID = ds.Tables(0).Rows(0).item(0)
                grade = ds.Tables(0).ROws(0).item(1)
                If hfREQ_Type.Value = "Enquiry" Then
                    section = ""
                Else
                    section = ds.Tables(0).ROws(0).item(2)
                End If
                stuName = ds.Tables(0).ROws(0).item(3)
                reqType = ds.Tables(0).ROws(0).item(4)
                regValue = ds.Tables(0).ROws(0).item(5)
                refID = ds.Tables(0).ROws(0).item(6)
                Parent = ds.Tables(0).ROws(0).item(7)
                toEmail = ds.Tables(0).ROws(0).item(8)
                onwAreaPoint = ds.Tables(0).ROws(0).item(9)
                retAreaPoint = ds.Tables(0).ROws(0).item(10)
                bsuName = ds.Tables(0).Rows(0).Item(12)
                startDate = ds.Tables(0).Rows(0).Item(13)
                reqDate = ds.Tables(0).Rows(0).Item(14)
            End If

        End If

        Dim str_query As String = "exec [TRANSPORT].[TRANSPORT_APPROVAL_EMAIL] " _
                               & "'" + bsuID + "'," _
                               & "'" + grade + "'," _
                               & "'" + section + "'," _
                               & "'" + stuName + "'," _
                               & "'" + reqType + "'," _
                               & "'" + regValue + "'," _
                               & "'" + refID.ToString + "'," _
                               & "'" + Parent + "'," _
                               & "'" + toEmail + "'," _
                               & "'" + onwAreaPoint + "'," _
                               & "'" + retAreaPoint + "'," _
                               & "'GEMS'," _
                               & "'" + bsuName + "'," _
                               & "'" + startDate + "'," _
                               & "'" + reqDate + "'"


        Dim retValue As Integer = SqlHelper.ExecuteScalar(conStrT, CommandType.Text, str_query)



    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnApprove.Click
        Try
            If ddlTripType.SelectedIndex = 1 Then
                If ddlPickupArea.SelectedIndex = 0 Then
                    trPArea.Visible = True
                Else
                    trPArea.Visible = False
                End If

                If ddlPickupPoint.SelectedIndex = 0 Then
                    trPPoint.Visible = True
                Else
                    trPPoint.Visible = False
                End If

                If ddlDropoffArea.SelectedIndex = 0 Then
                    trDArea.Visible = True
                Else
                    trDArea.Visible = False
                End If
                If ddlDropoffPoint.SelectedIndex = 0 Then
                    trDPoint.Visible = True
                Else
                    trDPoint.Visible = False
                End If
            ElseIf ddlTripType.SelectedIndex = 2 Then
                trDArea.Visible = False
                trDPoint.Visible = False
                If ddlPickupArea.SelectedIndex = 0 Then
                    trPArea.Visible = True
                Else
                    trPArea.Visible = False
                End If

                If ddlPickupPoint.SelectedIndex = 0 Then
                    trPPoint.Visible = True
                Else
                    trPPoint.Visible = False
                End If
            ElseIf ddlTripType.SelectedIndex = 3 Then
                trPArea.Visible = False
                trPPoint.Visible = False
                If ddlDropoffArea.SelectedIndex = 0 Then
                    trDArea.Visible = True
                Else
                    trDArea.Visible = False
                End If
                If ddlDropoffPoint.SelectedIndex = 0 Then
                    trDPoint.Visible = True
                Else
                    trDPoint.Visible = False
                End If
            End If

            If trPArea.Visible Or trPPoint.Visible Or trDArea.Visible Or trDPoint.Visible Then
                MPViewRequests.Show()
                Exit Sub
            End If

            'If txtRemarks.Text = "" Then
            '    trRemarks.Visible = True
            '    MPViewRequests.Show()
            '    Exit Sub
            'Else
            '    trRemarks.Visible = False
            'End If

            'Dim transaction As SqlTransaction
            btnSave.Enabled = False
            Dim retValue As Integer
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            'Dim conn As New SqlConnection(conStrT)
            'conn.Open()
            'transaction = conn.BeginTransaction("SampleTransaction")
            Dim reqType As String
            If rdReqType.SelectedIndex = 0 Then
                reqType = "Student"
            Else
                reqType = "Enquiry"
            End If
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BBT_ID", hfBBT_ID.Value)
            param(1) = New SqlParameter("@REQTYPE", reqType)
            retValue = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "[transport].[CHECKAPPROVALSTATUS]", param)
            'transaction.Commit()
            trSaveOk.Visible = False
            SendApprovalEmail()
            If retValue = 0 Then
                If studClass.checkFeeClosingDate(Session("sbsuid"), CType(hfAcdID.Value, Integer), Date.Parse(txtFrom.Text)) = True Then
                    If Approve() = True Then
                        SendApprovalEmail()
                        trSummativeError.Visible = True
                        lblFinalError.Text = "Record Saved Successfully"
                        btnPrint.Enabled = True
                        btnCancel.Enabled = True
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                        btnEdit.Enabled = False
                        BindGrid()
                        MPViewRequests.Show()
                    Else
                        trSummativeError.Visible = True
                        'lblFinalError.Text = "Request could not be processed"
                        MPViewRequests.Show()
                        Exit Sub
                    End If
                Else
                    trSummativeError.Visible = True
                    lblFinalError.Text = "The service strarting date has to be higher than the fee closing date"
                End If
            Else
                trSummativeError.Visible = True
                lblFinalError.Text = "Transport service has already been allocated to the specified Student"

            End If
            MPViewRequests.Show()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            trSummativeError.Visible = True
            lblFinalError.Text = "Request could not be processed"
        End Try
    End Sub


    Function Approve() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Try

            If hfSave.Value = "0" Then
                SaveEditData()
            End If
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim str_query As String = " EXEC [transport].saveSTUDNEWTRANSPORTREQAPPROVALONLINE " _
                                              & "'" + hfStudentID.Value + "'," _
                                              & "'" + hfBBT_ID.Value + "'," _
                                              & "'1'," _
                                              & "'" + ddlOnward.SelectedItem.Value + "'," _
                                              & "'" + ddlReturn.SelectedItem.Value + "'," _
                                              & "'" + Session("sUsr_name") + "'," _
                                              & "'" + hfAcdID.Value + "'," _
                                              & "'" + hfGRM_ID.Value + "'," _
                                              & "''"

                    Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, str_query)
                    '   Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    transaction.Commit()
                    Return True
                Catch ex As Exception
                    transaction.Rollback()
                    trSummativeError.Visible = True
                    lblFinalError.Text = ex.Message
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    Return False
                End Try
            End Using

        Catch myex As ArgumentException
            trSummativeError.Visible = True
            lblFinalError.Text = myex.Message
            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return False
        Catch ex As Exception
            trSummativeError.Visible = True
            lblFinalError.Text = ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return False
        End Try
        'End Using
    End Function

    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand

    End Sub

    Protected Sub ddlTripType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlTripType.SelectedIndex = 1 Then
            If ddlPickupArea.Enabled = False Then
                ddlPickupArea.Enabled = True
                ddlOnward.Enabled = True
                ddlPickupPoint.Enabled = True
                lnkPickup.Enabled = True
                'rfAdminArea.Enabled = True
                'rfAdminPickUpPoint.Enabled = True
                BindPickupArea(hfBsuID.Value, "")
                'BindPickupDropoffPoints(ddlPickupPoint, hfPSBL_ID.Value)
                lblPlocation.Enabled = True
            End If
            If ddlDropoffArea.Enabled = False Then
                ddlDropoffArea.Enabled = True
                ddlReturn.Enabled = True
                ddlDropoffPoint.Enabled = True
                lnkDropOff.Enabled = True
                'rfAdminDropArea.Enabled = True
                'rfAdminDropoffPoint.Enabled = True
                BindDropoffArea(hfBsuID.Value, "")
                'BindPickupDropoffPoints(ddlDropoffPoint, hfDSBL_ID.Value)
                lblDlocation.Enabled = True
            End If
        ElseIf ddlTripType.SelectedIndex = 2 Then
            If ddlPickupArea.Enabled = False Then
                ddlPickupArea.Enabled = True
                ddlOnward.Enabled = True
                ddlPickupPoint.Enabled = True
                lnkPickup.Enabled = True
                'rfAdminArea.Enabled = True
                'rfAdminPickUpPoint.Enabled = True
                BindPickupArea(hfBsuID.Value, "")
                'BindPickupDropoffPoints(ddlDropoffPoint, hfPSBL_ID.Value)
                lblPlocation.Enabled = True
            End If
            If ddlDropoffArea.Enabled = True Then
                ddlDropoffArea.Enabled = False
                ddlReturn.Enabled = False
                ddlDropoffPoint.Enabled = False
                lnkDropOff.Enabled = False
                'rfAdminDropArea.Enabled = False
                'rfAdminDropoffPoint.Enabled = False
                ddlDropoffArea.SelectedIndex = 0
                ddlDropoffPoint.SelectedIndex = 0
                ddlReturn.SelectedIndex = 0
                lblDlocation.Text = ""
            End If
        ElseIf ddlTripType.SelectedIndex = 3 Then
            If ddlPickupArea.Enabled = True Then
                ddlPickupArea.Enabled = False
                ddlOnward.Enabled = False
                ddlPickupPoint.Enabled = False
                lnkPickup.Enabled = False
                'rfAdminArea.Enabled = False
                'rfAdminPickUpPoint.Enabled = False
                ddlPickupArea.SelectedIndex = 0
                ddlPickupPoint.SelectedIndex = 0
                ddlOnward.SelectedIndex = 0
                lblPlocation.Enabled = False
                lblPlocation.Text = ""
            End If
            If ddlDropoffArea.Enabled = False Then
                ddlDropoffArea.Enabled = True
                ddlReturn.Enabled = True
                ddlDropoffPoint.Enabled = True
                lnkDropOff.Enabled = True
                'rfAdminDropArea.Enabled = True
                'rfAdminDropoffPoint.Enabled = True
                BindDropoffArea(hfBsuID.Value, "")
                'BindPickupDropoffPoints(ddlDropoffPoint, hfDSBL_ID.Value)
                lblDlocation.Enabled = True
                lblDlocation.Text = ""
            End If

        Else
            trSummativeError.Visible = True
            lblFinalError.Text = "Choose a trip type"
            Exit Sub
        End If
        MPViewRequests.Show()
    End Sub

    Protected Sub ddlDropoffArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDropoffArea.SelectedIndexChanged
        BindPickupDropoffPoints(ddlDropoffPoint, ddlDropoffArea.SelectedItem.Value)
        MPViewRequests.Show()
        ddlReturn.Items.Clear()
        ddlReturn.Items.Insert(0, New ListItem("--", "0"))
    End Sub

    Protected Sub ddlDropoffPoint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDropoffPoint.SelectedIndexChanged
        BindTrip(ddlReturn, "Return")
        MPViewRequests.Show()
    End Sub

    Protected Sub ddlPickupPoint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPickupPoint.SelectedIndexChanged
        BindTrip(ddlOnward, "Onward")
        MPViewRequests.Show()
    End Sub

    Protected Sub ddlOnward_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOnward.SelectedIndexChanged

        If rdReqType.SelectedIndex = 1 Then
            If btnApprove.Visible = False Then
                tdEnqOnward.Visible = True
                tdOnward.Visible = False
                BindEnqOnwardCapacity(gvEnqOnward, ddlOnward.SelectedValue, "Onward")
            Else
                tdEnqOnward.Visible = False
                tdOnward.Visible = True
                BindCapacity(gvOnward, ddlOnward.SelectedValue, "Onward")
            End If
        Else
            tdEnqOnward.Visible = False
            tdOnward.Visible = True
            BindCapacity(gvOnward, ddlOnward.SelectedValue, "Onward")
        End If

        MPViewRequests.Show()
        gvOnward.Visible = True
    End Sub

    Protected Sub ddlReturn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReturn.SelectedIndexChanged
        If rdReqType.SelectedIndex = 1 Then
            If btnApprove.Visible = False Then
                tdEnqReturn.Visible = True
                tdReturn.Visible = False
                BindEnqReturnCapacity(gvEnqReturn, ddlReturn.SelectedValue, "Return")
            Else
                tdEnqReturn.Visible = False
                tdReturn.Visible = True
                BindCapacity(gvReturn, ddlReturn.SelectedValue, "Return")
            End If
        Else
            tdEnqReturn.Visible = False
            tdReturn.Visible = True
            BindCapacity(gvReturn, ddlReturn.SelectedValue, "Return")
        End If
        BindCapacity(gvReturn, ddlReturn.SelectedValue, "Return")
        MPViewRequests.Show()
        gvReturn.Visible = True
    End Sub

    Protected Sub lnkPickup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPickup.Click
        If ddlPickupPoint.SelectedValue.ToString = "" Then
            ddlOnward.Items.Clear()
            ddlOnward.Items.Insert(0, New ListItem("--", "0"))
        Else
            BindTrip(ddlOnward, "Onward")
        End If
        MPViewRequests.Show()
    End Sub

    Protected Sub lnkDropOff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDropOff.Click
        If ddlDropoffPoint.SelectedValue.ToString = "" Then
            ddlReturn.Items.Clear()
            ddlReturn.Items.Insert(0, New ListItem("--", "0"))
        Else
            BindTrip(ddlReturn, "Return")
        End If
        MPViewRequests.Show()
    End Sub


    Protected Sub rdReqType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdReqType.SelectedIndexChanged
        'Dim tr As HtmlTableRow = studTable.FindControl("trDetails")
        'Dim tr1 As HtmlTableRow = studTable.FindControl("trDetails1")
        tr2.Visible = False
        If rdReqType.SelectedIndex = 0 Then
            lblApplicationNO.Text = "Student ID"
            lblApplicantName.Text = "Student Name"
            'tdStudId.Visible = True
            'tdStudidBox.Visible = True
            'tdStuName.Visible = True
            'tdStudNameBox.Visible = True
            tdAppName.Visible = True
            tdAppNameBox.Visible = True
            tdAppNo.Visible = True
            tdAppNoBox.Visible = True
        Else
            lblApplicationNO.Text = "Application Number"
            lblApplicantName.Text = "Applicant Name"
            tdAppName.Visible = True
            tdAppNameBox.Visible = True
            tdAppNo.Visible = True
            tdAppNoBox.Visible = True
            'tdStudId.Visible = False
            'tdStudidBox.Visible = False
            'tdStuName.Visible = False
            'tdStudNameBox.Visible = False
        End If
    End Sub

    Sub BindEnqReturnCapacity(ByVal gvCap As GridView, ByVal trdId As String, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(*) AS 'SEAT_ALLOTED' FROM STUDENT_BBTREQ_GEMS_S" _
                                & " WHERE BBT_RETURNTRIPID = " + ddlReturn.SelectedItem.Value + " AND BBT_REQTYPE='ENQUIRY' " _
                                & " AND BBT_bAOAPPROVE IS NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvCap.DataSource = ds
        gvCap.DataBind()
    End Sub

    Sub BindEnqOnwardCapacity(ByVal gvCap As GridView, ByVal trdId As String, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(*) AS 'SEAT_ALLOTED' FROM STUDENT_BBTREQ_GEMS_S" _
                                & " WHERE BBT_ONWARDTRIPID = " + ddlOnward.SelectedItem.Value + " AND BBT_REQTYPE='ENQUIRY' " _
                                & " AND BBT_bAOAPPROVE IS NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvCap.DataSource = ds
        gvCap.DataBind()
    End Sub


    Sub BindCapacity(ByVal gvCap As GridView, ByVal trdId As String, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.getSEATCAPACITY " _
                                & "'" + Session("sbsuid") + "'," _
                                & trdId + "," _
                                & hfAcdID.Value + "," _
                                & Session("Current_ACY_ID") + "," _
                                & "'" + journey + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvCap.DataSource = ds
        gvCap.DataBind()

    End Sub

    Sub SaveAOReject()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "update student_bbtreq_gems_s set BBT_bAOAPPROVE = 'FALSE', " _
                                 & " BBT_AOAPRDATE=GETDATE()," _
                                 & " BBT_AOREMARKS='" + txtRemarks.Text + "'," _
                                 & " BBT_AOAPRUSER='" + Session("sUsr_name") + "'" _
                                 & " where bbt_id=" + hfBBT_ID.Value
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            SaveAOReject()
            btnApprove.Enabled = False
            btnSave.Enabled = False
            btnReject.Enabled = False
            btnEdit.Enabled = False
            lblFinalError.Text = "The transport request has been rejected!"
            MPViewRequests.Show()
        Catch ex As Exception
            trSummativeError.Visible = True
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblFinalError.Text = "Rejection Process Could Not be Completed"
            MPViewRequests.Show()
        End Try
    End Sub

    Function GetEmpName(ByVal designation As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function

    Sub CallReportEnq(ByVal Eqs_ID As String)

        Dim param As New Hashtable
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@EQS_ID", Eqs_ID)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptEnqTransportInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Sub CallReport(ByVal STU_IDS As String)

        'Dim param As New Hashtable
        'param.Add("@IMG_BSU_ID", Session("sbsuid"))
        'param.Add("@IMG_TYPE", "LOGO")
        'param.Add("UserName", Session("sUsr_name"))
        'param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        'param.Add("AO", GetEmpName("ADMIN OFFICER"))
        'param.Add("@STU_IDS", STU_IDS)
        'param.Add("BSU", GetBsuName)

        Dim param As New Hashtable
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptStudTransportInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If rdReqType.SelectedIndex = 0 Then
            CallReport(hfStudentID.Value)
        Else
            CallReportEnq(hfEQS_Id.Value)
        End If

        MPViewRequests.Show()
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim drv As DataRowView = e.Row.DataItem
            Dim lnkbtnEdit As LinkButton = CType(e.Row.FindControl("lnkbtnView"), LinkButton)
            If drv("Status").ToString = "APPROVED" Then
                lnkbtnEdit.Enabled = False

            Else
                lnkbtnEdit.Enabled = True

            End If
        End If
        'Dim lnkbtn As LinkButton = e.Row.FindControl("lnkbtnview")
        'If e.Row.Cells(0).Text = "APPROVED" Then
        '    lnkbtn.Enabled = False
        'End If
        'LinkButton lbClose = (LinkButton)e.Row.Cells[5].FindControl("lbClose");

    End Sub

    Protected Sub ddlCurriculum_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurriculum.SelectedIndexChanged
        BindAcademicYearForCurriculum()
    End Sub

    Protected Sub ddlgvSaveStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub

End Class

