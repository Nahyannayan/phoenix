Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptTripDetails_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Num2Words As New Num2Words
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000120") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    hfVEH_ID.Value = Encr_decrData.Decrypt(Request.QueryString("vehicleid").Replace(" ", "+"))
                    txtVehNo.Text = Encr_decrData.Decrypt(Request.QueryString("vehicle").Replace(" ", "+"))
                    If ViewState("datamode") = "add" Then
                        txtTripDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                        ShowEmptyGrid()
                        lbAddNew.Visible = False
                    Else
                        hfTRL_ID.Value = Encr_decrData.Decrypt(Request.QueryString("trlId").Replace(" ", "+"))
                        BindGrid()
                        lbAddNew.Visible = False
                        rdExtra.Visible = False
                        rdRegular.Visible = False

                        ' txtTripDate.ReadOnly = True
                        ' imgBtnTripDate.Enabled = False
                        ' gvTptTrip.Enabled = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    'Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    '    lblError.Text = ""
    '    If ViewState("datamode") <> "add" Then
    '        txtTripDate.ReadOnly = True
    '        imgBtnTripDate.Enabled = False
    '        gvTptTrip.Enabled = False
    '        txtTripDate.Text = Format(Now.Date, "dd/MMM/yyyy")
    '        ShowEmptyGrid()
    '    End If
    '    ViewState("datamode") = "add"
    '    hfTRL_ID.Value = 0
    '    txtTripDate.ReadOnly = False
    '    imgBtnTripDate.Enabled = True
    '    gvTptTrip.Enabled = True
    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    'End Sub

    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    lblError.Text = ""
    '    If hfTRL_ID.Value = 0 Then
    '        lblError.Text = "No record to edit"
    '        Exit Sub
    '    End If
    '    txtTripDate.ReadOnly = False
    '    imgBtnTripDate.Enabled = True
    '    gvTptTrip.Enabled = True
    '    ViewState("datamode") = "edit"
    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    'End Sub



    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                txtTripDate.ReadOnly = True
                imgBtnTripDate.Enabled = False
                gvTptTrip.Enabled = False
                txtTripDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                ShowEmptyGrid()
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                hfTRL_ID.Value = 0
                ' Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ShowEmptyGrid()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TRL_ID='0',TRL_PASSENGERS='',TRD_ID,TRP_REMARKS,TRD_TRP_ID AS TRP_ID,CASE TRP_JOURNEY WHEN 'EXTRA' THEN TRP_DESCR ELSE TRP_DESCR+'('+BNO_DESCR+')' END AS TRP_DESCR ,TRL_STARTTIME='',TRL_ENDTIME=''," _
                                & " TRL_STARTKM='',TRL_ENDKM='',MINKM=ISNULL((SELECT TOP 1 TRL_ENDKM FROM" _
                                & " TRANSPORT.TRIPLOG_S WHERE TRL_TRD_ID=B.TRD_ID AND TRL_TRIPDATE<'" + Format(Date.Parse(txtTripDate.Text), "yyyy-MM-dd") + "'" _
                                & " ORDER BY TRL_TRIPDATE DESC),0),MAXKM=ISNULL((SELECT TOP 1 TRL_ENDKM FROM" _
                                & " TRANSPORT.TRIPLOG_S WHERE TRL_TRD_ID=B.TRD_ID AND TRL_TRIPDATE>'" + Format(Date.Parse(txtTripDate.Text), "yyyy-MM-dd") + "'" _
                                & " ORDER BY TRL_TRIPDATE),0) FROM TRANSPORT.TRIPS_M  AS A INNER JOIN" _
                                & " TRANSPORT.TRIPS_D AS B ON A.TRP_ID = B.TRD_TRP_ID LEFT OUTER JOIN" _
                                & " TRANSPORT.BUSNOS_M AS C ON B.TRD_BNO_ID=C.BNO_ID " _
                                & " WHERE TRD_VEH_ID=" + hfVEH_ID.Value.ToString + " AND TRP_ID NOT IN (SELECT TRL_TRP_ID " _
                                & " FROM TRANSPORT.TRIPLOG_S WHERE CONVERT(VARCHAR,TRL_TRIPDATE,101)='" + Format(Date.Parse(txtTripDate.Text), "MM/dd/yyyy") + "')"

        If rdRegular.Checked = True Then
            str_query += " AND TRP_JOURNEY<>'EXTRA' AND TRD_TODATE IS NULL"
        Else
            str_query += " AND TRP_JOURNEY='EXTRA' AND '" + Format(Date.Parse(txtTripDate.Text), "yyyy-MM-dd") + "' BETWEEN TRD_FROMDATE AND isnull(TRD_TODATE,'2099-01-01') "
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptTrip.DataSource = ds
        gvTptTrip.DataBind()
        If rdRegular.Checked = True Then
            gvTptTrip.Columns(4).Visible = False
            gvTptTrip.Columns(9).Visible = False
        Else
            gvTptTrip.Columns(4).Visible = True
            gvTptTrip.Columns(9).Visible = True
        End If

        Session("dtLog") = ds.Tables(0)
    End Sub

    Private Sub BindGrid()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TRL_ID,TRL_TRD_ID AS TRD_ID,TRL_TRP_ID AS TRP_ID,CASE TRP_JOURNEY WHEN 'EXTRA' THEN TRP_DESCR ELSE TRP_DESCR+'('+BNO_DESCR+')' END AS TRP_DESCR,TRL_TRIPDATE,TRP_JOURNEY,TRL_STARTTIME, " _
                                 & " TRL_ENDTIME,TRL_TRIPDATE,TRL_STARTKM,TRL_ENDKM,ISNULL(TRP_REMARKS,'') AS TRP_REMARKS, " _
                                 & " TRL_PASSENGERS, MINKM=ISNULL((SELECT TOP 1 TRL_ENDKM FROM" _
                                 & " TRANSPORT.TRIPLOG_S WHERE TRL_TRD_ID=B.TRD_ID AND TRL_TRIPDATE<A.TRL_TRIPDATE" _
                                 & " ORDER BY TRL_TRIPDATE DESC),0),MAXKM=ISNULL((SELECT TOP 1 TRL_ENDKM FROM" _
                                 & " TRANSPORT.TRIPLOG_S WHERE TRL_TRD_ID=B.TRD_ID AND TRL_TRIPDATE>A.TRL_TRIPDATE" _
                                 & " ORDER BY TRL_TRIPDATE),0) FROM  TRANSPORT.TRIPS_D AS B INNER JOIN" _
                                 & " TRANSPORT.TRIPS_M ON B.TRD_TRP_ID = TRANSPORT.TRIPS_M.TRP_ID LEFT OUTER JOIN" _
                                 & " TRANSPORT.BUSNOS_M AS C ON B.TRD_BNO_ID=C.BNO_ID " _
                                 & " TRANSPORT.TRIPLOG_S AS A ON B.TRD_ID = A.TRL_TRD_ID" _
                                 & " WHERE TRL_ID=" + hfTRL_ID.Value

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptTrip.DataSource = ds
        gvTptTrip.DataBind()
        txtTripDate.Text = Format(Date.Parse(ds.Tables(0).Rows(0).Item(4)), "dd/MMM/yyyy")
        If ds.Tables(0).Rows(0).Item(5).ToString.ToUpper = "EXTRA" Then
            rdExtra.Checked = True
            gvTptTrip.Columns(4).Visible = True
            gvTptTrip.Columns(9).Visible = True
        Else
            gvTptTrip.Columns(4).Visible = False
            gvTptTrip.Columns(9).Visible = False
        End If
      
    End Sub

    Private Function SaveData() As Boolean
        Dim lblTrdId As Label
        Dim lblTrpId As Label
        Dim lblTrip As Label
        Dim txtStart As TextBox
        Dim txtEnd As TextBox
        Dim txtStartKm As TextBox
        Dim txtEndKm As TextBox
        Dim txtPassenger As TextBox
        Dim ddlStart As DropDownList
        Dim ddlEnd As DropDownList


        Dim str_query As String
        Dim errors As String = ""
        Dim err As String = ""
        Dim trlId As Integer = 0
        Dim trlIds As String = ""
        Dim btSave As Boolean = False
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                If ViewState("datamode") = "add" Then
                    Dim i As Integer

                    With gvTptTrip
                        For i = 0 To .Rows.Count - 1
                            If rdRegular.Checked = True Then
                                err = ValidateRow(.Rows(i), i)
                                If err <> "" Then
                                    errors += err
                                    If i <> .Rows.Count - 1 Then
                                        errors += "<br/>"
                                    End If
                                End If
                            ElseIf (rdExtra.Checked = True) Then
                                lblTrpId = .Rows(i).FindControl("lblTrpId")
                                If lblTrpId.Text <> 0 Then
                                    err = ValidateRow(.Rows(i), i)
                                    If err <> "" Then
                                        errors += err
                                        If i <> .Rows.Count - 1 Then
                                            errors += "<br/>"
                                        End If
                                    End If
                                End If
                            End If

                        Next
                        If errors <> "" Then
                            ltError.Text = errors
                            transaction.Dispose()
                            Return False
                        Else
                            ltError.Text = ""
                        End If

                        For i = 0 To .Rows.Count - 1

                            lblTrdId = .Rows(i).FindControl("lblTrdId")
                            lblTrpId = .Rows(i).FindControl("lblTrpId")
                            lblTrip = .Rows(i).FindControl("lblTrip")
                            txtStart = .Rows(i).FindControl("txtStart")
                            txtEnd = .Rows(i).FindControl("txtEnd")
                            txtStartKm = .Rows(i).FindControl("txtStartKm")
                            txtEndKm = .Rows(i).FindControl("txtEndKm")
                            txtPassenger = .Rows(i).FindControl("txtPassenger")
                            ddlStart = .Rows(i).FindControl("ddlStart")
                            ddlEnd = .Rows(i).FindControl("ddlEnd")
                            If rdExtra.Checked = True Then
                                If txtStart.Text.Trim <> "" And txtEnd.Text.Trim <> "" And txtStartKm.Text.Trim <> "" And txtEndKm.Text.Trim <> "" And txtPassenger.Text.Trim <> "" Then
                                    btSave = True
                                End If
                            Else
                                If txtStart.Text.Trim <> "" And txtEnd.Text.Trim <> "" And txtStartKm.Text.Trim <> "" And txtEndKm.Text.Trim <> "" Then
                                    btSave = True
                                End If
                            End If
                            If btSave = True Then
                                str_query = "exec TRANSPORT.saveTRIPLOG 0," _
                                            & "'" + Session("sbsuid") + "'," _
                                            & IIf(lblTrdId.Text = "", "'0'", lblTrdId.Text) + "," _
                                            & hfVEH_ID.Value + "," _
                                            & IIf(lblTrpId.Text = "", "'0'", lblTrpId.Text) + ",'" _
                                            & Format(Date.Parse(txtTripDate.Text), "yyyy-MM-dd") + "','" _
                                            & txtStart.Text.Trim + " " + ddlStart.Text + "','" _
                                            & txtEnd.Text.Trim + " " + ddlEnd.Text + "'," _
                                            & txtStartKm.Text.Trim + "," _
                                            & txtEndKm.Text.Trim + "," _
                                            & IIf(txtPassenger.Text.Trim = "", "'0'", txtPassenger.Text.Trim) + "," _
                                            & "'" + lblTrip.Text.Replace("'", "") + "'," _
                                            & Session("Current_ACY_ID") + "," _
                                            & "'Add'"
                                trlId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                                If trlIds.Length <> 0 Then
                                    trlIds += ","
                                End If
                                trlIds += trlId.ToString
                                btSave = False
                            End If
                        Next
                    End With
                ElseIf ViewState("datamode") = "edit" Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "TRANSPORT.TRIPLOG_S", "TRL_ID", "TRL_TRD_ID", "TRL_ID=" + hfTRL_ID.Value.ToString)
                    With gvTptTrip
                        err = ValidateRow(.Rows(0), 0)
                        If err <> "" Then
                            ltError.Text = err
                            transaction.Dispose()
                            Return False
                        Else
                            ltError.Text = ""
                        End If
                        txtStart = .Rows(0).FindControl("txtStart")
                        txtEnd = .Rows(0).FindControl("txtEnd")
                        txtStartKm = .Rows(0).FindControl("txtStartKm")
                        txtEndKm = .Rows(0).FindControl("txtEndKm")
                        txtPassenger = .Rows(0).FindControl("txtPassenger")
                        ddlStart = .Rows(0).FindControl("ddlStart")
                        ddlEnd = .Rows(0).FindControl("ddlEnd")
                        'str_query = "exec tptSAVETRIPDETILS " + hfTRL_ID.Value.ToString + ",0,0,0,'" _
                        '                                    & Format(Date.Parse(txtTripDate.Text), "yyyy-MM-dd") + "','" + txtStart.Text + "','" + txtEnd.Text + "'," _
                        '                                    & txtStartKm.Text + "," + txtEndKm.Text + "," + txtFuel.Text + ",'Edit'"
                        str_query = "exec TRANSPORT.saveTRIPLOG " _
                                        & hfTRL_ID.Value + "," _
                                        & "'" + Session("sbsuid") + "'," _
                                        & "0,0,0," _
                                        & "'" + Format(Date.Parse(txtTripDate.Text), "yyyy-MM-dd") + "','" _
                                        & txtStart.Text.Trim + " " + ddlStart.Text + "','" _
                                        & txtEnd.Text.Trim + " " + ddlEnd.Text + "'," _
                                        & txtStartKm.Text.Trim + "," _
                                        & txtEndKm.Text.Trim + "," _
                                        & IIf(txtPassenger.Text.Trim = "", "'0'", txtPassenger.Text.Trim) + "," _
                                        & "''," _
                                        & Session("Current_ACY_ID") + "," _
                                        & "'Edit'"

                        trlId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                        trlIds = trlId.ToString
                    End With
                End If


                Dim flagAudit As Integer
                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TRL_ID(" + trlIds + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                transaction.Commit()
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function

    Private Function ValidateRow(ByVal gRow As GridViewRow, ByVal rowindex As Integer) As String
        Dim txtStart As TextBox
        Dim txtEnd As TextBox
        Dim lblMinKm As Label
        Dim lblMaxKm As Label
        Dim txtStartKm As TextBox
        Dim txtEndKm As TextBox
        Dim txtPassenger As TextBox

        Dim ddlStart As DropDownList
        Dim ddlEnd As DropDownList

        Dim flag As Boolean = True
        Dim str As String = ""
        Dim str1 As String = ""

        Dim flag1 As Boolean = True
        Dim flag2 As Boolean = True
        Dim flag3 As Boolean = True
        Dim flag4 As Boolean = True

        With gRow
            txtStart = .FindControl("txtStart")
            txtEnd = .FindControl("txtEnd")
            txtStartKm = .FindControl("txtStartKm")
            txtEndKm = .FindControl("txtEndKm")
            txtPassenger = .FindControl("txtPassenger")
            lblMinKm = .FindControl("lblMinKm")
            lblMaxKm = .FindControl("lblMaxKm")
            ddlStart = .FindControl("ddlStart")
            ddlEnd = .FindControl("ddlEnd")
            If txtStart.Text.Trim <> "" Or txtEnd.Text.Trim <> "" Or txtStartKm.Text.Trim <> "" Or txtEndKm.Text.Trim <> "" Or txtPassenger.Text.Trim <> "" Then

                If txtStart.Text.Trim.Trim = "" Then
                    str1 += "Start Time"
                    flag = False
                    flag1 = False
                End If
                If txtEnd.Text.Trim.Trim = "" Then
                    If str1.Length <> 0 Then
                        str1 += ", "
                    End If
                    str1 += "End Time"
                    flag = False
                    flag2 = False
                End If
                If txtStartKm.Text.Trim.Trim = "" Then
                    If str1.Length <> 0 Then
                        str1 += ", "
                    End If
                    str1 += "Start Km."
                    flag = False
                    flag3 = False
                End If
                If txtEndKm.Text.Trim.Trim = "" Then
                    If str1.Length <> 0 Then
                        str1 += ", "
                    End If
                    str1 += "End km"
                    flag = False
                    flag4 = False
                End If
                If rdExtra.Checked = True Then
                    If txtPassenger.Text.Trim.Trim = "" Then
                        If str1.Length <> 0 Then
                            str1 += ", "
                        End If
                        str1 += "Passenger"
                        flag = False
                    End If
                End If
                If str1 <> "" Then
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""5px"">.</font>" _
                            & "Please enter data to the following fields :- " + str1 + " in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            Else
                flag1 = False
                flag2 = False
                flag3 = False
                flag4 = False
            End If

            If txtStart.Text.Trim <> "" Then
                If IsDate(txtStart.Text + " " + ddlStart.Text) = False Then
                    If str <> "" Then
                        str += "<br/>"
                    End If
                    flag = False
                    flag1 = False
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""5px"">.</font>" _
                           & "Please enter a valid start time in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            End If

            If txtEnd.Text.Trim <> "" Then
                If IsDate(txtEnd.Text + " " + ddlEnd.Text) = False Then
                    If str <> "" Then
                        str += "<br/>"
                    End If
                    flag = False
                    flag2 = False
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""5px"">.</font>" _
                           & "Please enter a valid end time in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            End If

            If txtStartKm.Text.Trim <> "" Then
                If isValidNumber(txtStartKm.Text.Trim) = False Then
                    If str <> "" Then
                        str += "<br/>"
                    End If
                    flag = False
                    flag3 = False
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""5px"">.</font>" _
                            & "Please enter a valid start km in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            End If

            If txtEndKm.Text.Trim <> "" Then
                If isValidNumber(txtEndKm.Text.Trim) = False Then
                    If str <> "" Then
                        str += "<br/>"
                    End If
                    flag = False
                    flag4 = False
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""5px"">.</font>" _
                            & " Please enter a valid End km in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            End If

            If txtPassenger.Text <> "" Then
                If isValidNumber(txtPassenger.Text.Trim) = False Then
                    If str <> "" Then
                        str += "<br/>"
                    End If
                    flag = False
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""8px"">.</font>" _
                           & "Please enter a valid Fuel in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            End If

            If flag1 = True And flag2 = True Then
                If DateTime.Compare(Date.Parse(txtEnd.Text), Date.Parse(txtStart.Text)) < 0 Then
                    If str <> "" Then
                        str += "<br/>"
                    End If
                    flag = False
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""8px"">.</font>" _
                           & "The start time should be less than end time in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            End If

            If flag3 = True Then

                If lblMinKm.Text <> "0" And lblMaxKm.Text <> "0" Then
                    If CType(txtStartKm.Text, Double) < CType(lblMinKm.Text, Double) Or CType(txtStartKm.Text, Double) > CType(lblMaxKm.Text, Double) Then
                        If str <> "" Then
                            str += "<br/>"
                        End If
                        flag = False
                        str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""8px"">.</font>" _
                               & "The start Km should be between " + lblMinKm.Text + " and " + lblMaxKm.Text + " in the " + Num2Words.convert((rowindex + 1), "") + " row "
                    End If
                ElseIf lblMinKm.Text <> "0" Then
                    If CType(txtStartKm.Text, Double) < CType(lblMinKm.Text, Double) Then
                        If str <> "" Then
                            str += "<br/>"
                        End If
                        flag = False
                        str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""8px"">.</font>" _
                               & "The start Km should be higher than " + lblMinKm.Text + " in the " + Num2Words.convert((rowindex + 1), "") + " row "
                    End If
                ElseIf lblMaxKm.Text <> "0" Then
                    If CType(txtStartKm.Text, Double) > CType(lblMaxKm.Text, Double) Then
                        If str <> "" Then
                            str += "<br/>"
                        End If
                        flag = False
                        str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""8px"">.</font>" _
                               & "The start Km should be less than " + lblMaxKm.Text + " in the " + Num2Words.convert((rowindex + 1), "") + " row "
                    End If
                End If

            End If

            If flag3 = True And flag4 = True Then
                If CType(txtEndKm.Text, Double) < CType(txtStartKm.Text, Double) Then
                    If str <> "" Then
                        str += "<br/>"
                    End If
                    flag = False
                    str += "<font bold=""true"" face=""Arial, Helvetica, sans-serif"" size=""8px"">.</font>" _
                          & "The start Km should be less than end Km in the " + Num2Words.convert((rowindex + 1), "") + " row "
                End If
            End If
        End With
        If flag = False Then
            Return str
        End If
        Return ""
    End Function

    Sub AddNewTrip()
        Dim dt As DataTable
        dt = Session("dtLog")
        Dim i As Integer = 0
        Dim txtStart As TextBox
        Dim txtEnd As TextBox
        Dim txtStartKm As TextBox
        Dim txtEndKm As TextBox
        Dim txtPassenger As TextBox

        Dim ddlStart As DropDownList
        Dim ddlEnd As DropDownList

        Dim dr As DataRow
        For i = 0 To gvTptTrip.Rows.Count - 1
            With gvTptTrip.Rows(i)
                txtStart = .FindControl("txtStart")
                txtEnd = .FindControl("txtEnd")
                txtStartKm = .FindControl("txtStartKm")
                txtEndKm = .FindControl("txtEndKm")
                txtPassenger = .FindControl("txtPassenger")
                ddlStart = .FindControl("ddlStart")
                ddlEnd = .FindControl("ddlEnd")
            End With
            dr = dt.Rows(i)
            With dr
                .Item(6) = txtStart.Text + " " + ddlStart.Text
                .Item(7) = txtEnd.Text + " " + ddlEnd.Text
                .Item(8) = txtStartKm.Text
                .Item(9) = txtEndKm.Text
                .Item(1) = txtPassenger.Text
            End With
        Next
        dr = dt.NewRow
        dr.Item(0) = 0
        dr.Item(1) = ""
        dr.Item(2) = 0
        dr.Item(3) = ""
        dr.Item(4) = 0
        dr.Item(5) = GetTripName()
        dr.Item(6) = ""
        dr.Item(7) = ""
        dr.Item(8) = ""
        dr.Item(9) = ""
        dr.Item(10) = 0
        dr.Item(11) = 0
        dt.Rows.Add(dr)
        gvTptTrip.DataSource = dt
        gvTptTrip.DataBind()
    End Sub

    Function GetTripName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.GETTRIPNAME"
        Dim tripName As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return tripName
    End Function

    Function isValidNumber(ByVal number As String)
        Dim i As Integer = number.Length
        Dim numbers As String() = number.Split(".")
        If numbers.Length > 2 Then
            Return False
        End If
        Dim j As Integer = 0
        While j < i
            If Not number(j) = "." Then
                If Not Char.IsNumber(number(j)) Then
                    Return False
                End If
            End If
            j += 1
        End While
        Return True
    End Function
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If SaveData() = True Then
                lblError.Text = "Record saved successfully"
                txtTripDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                ' ShowEmptyGrid()
                'txtTripDate.ReadOnly = True
                ' imgBtnTripDate.Enabled = False
                ' gvTptTrip.Enabled = False
                Response.Redirect(ViewState("ReferrerUrl"))
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                hfTRL_ID.Value = 0
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        Try
            'If ViewState("datamode") = "add" Then
            lblError.Text = ""
            If IsDate(txtTripDate.Text) = True Then
                lblError.Text = ""
                ShowEmptyGrid()
            End If
            ' End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub rdRegular_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdRegular.CheckedChanged
        Try
            If rdRegular.Checked = True Then
                ShowEmptyGrid()
                lbAddNew.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub rdExtra_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdExtra.CheckedChanged
        Try
            If rdExtra.Checked = True Then
                ShowEmptyGrid()
                If ViewState("datamode") = "add" Then
                    lbAddNew.Visible = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            AddNewTrip()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    
    Protected Sub gvTptTrip_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTptTrip.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtStart As TextBox
                Dim ddlStart As DropDownList
                Dim txtEnd As TextBox
                Dim ddlEnd As DropDownList
                txtStart = e.Row.FindControl("txtStart")
                ddlStart = e.Row.FindControl("ddlStart")
                txtEnd = e.Row.FindControl("txtEnd")
                ddlEnd = e.Row.FindControl("ddlEnd")

                If txtStart.Text <> "" Then
                    If txtStart.Text.Trim.Substring(txtStart.Text.Length - 3, 1) = "A" Or txtStart.Text.Trim.Substring(txtStart.Text.Length - 2, 1) = "A" Then
                        ddlStart.Text = "AM"
                        txtStart.Text = txtStart.Text.Replace("AM", "").Trim
                        txtStart.Text = txtStart.Text.Replace("am", "").Trim
                    Else
                        txtStart.Text = txtStart.Text.Replace("PM", "").Trim
                        txtStart.Text = txtStart.Text.Replace("pm", "").Trim
                        ddlStart.Text = "PM"
                    End If
                End If

                If txtEnd.Text <> "" Then
                    If txtEnd.Text.Trim.Substring(txtEnd.Text.Length - 3, 1) = "A" Or txtEnd.Text.Trim.Substring(txtEnd.Text.Length - 2, 1) = "A" Then
                        ddlEnd.Text = "AM"
                        txtEnd.Text = txtEnd.Text.Replace("AM", "").Trim
                        txtEnd.Text = txtEnd.Text.Replace("am", "").Trim
                    Else
                        txtEnd.Text = txtEnd.Text.Replace("PM", "").Trim
                        txtEnd.Text = txtEnd.Text.Replace("pm", "").Trim
                        ddlEnd.Text = "PM"
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


End Class
