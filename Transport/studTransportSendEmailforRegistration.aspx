﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studTransportSendEmailforRegistration.aspx.vb" Inherits="Transport_studTransportSendEmailforRegistration"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                    if (chkstate = true) {

                    }
                }
            }
        }
               
        
            
           
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Transport Registration Send Email
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" width="100%">
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table id="tblClm" runat="server" align="center" width="100%">
                    
                    <tr id="trBsu" runat="server" width="10%">
                        <td align="left" id="td1" runat="server">
                            <span class="field-label">Business Unit</span>
                        </td>
                        
                        <td align="left" id="td3" runat="server" width="23%">
                            <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True" Width="408px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" id="td4" runat="server" width="10%">
                            <span class="field-label">Curriculum</span>
                        </td>
                        
                        <td align="left" id="td6" runat="server" width="23%">
                            <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True" Width="100px">
                            </asp:DropDownList>
                        </td>
                        <td width="10%"></td>
                        <td width="24%"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Academic Year</span>
                        </td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Grade</span>
                        </td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:RadioButton ID="rdEnquiry" Text="Enquiry" CssClass="field-label" AutoPostBack="true"  Checked="true" runat="server" GroupName="g1" />
                            <asp:RadioButton ID="rdStudent" Text="Student" CssClass="field-label" AutoPostBack="true" runat="server" GroupName="g1" />
                        </td>
                        <td align="left">
                            <asp:RadioButton ID="rdMailNotSend" Text="Mail Not Send" CssClass="field-label" Checked="true" runat="server"
                                GroupName="g2" />
                            <asp:RadioButton ID="rdMailSend" Text="Mail Sent" runat="server" CssClass="field-label" GroupName="g2" />
                            <asp:RadioButton ID="rdAll" Text="All" runat="server" CssClass="field-label" GroupName="g2" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Section</span>
                        </td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlSection" runat="server" Width="80">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Date of Join From</span>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtDojFrom" runat="server"> </asp:TextBox>
                            <asp:ImageButton ID="imgApplyStart" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                        </td>
                        <td align="left">
                            <span class="field-label">Date of Join To</span>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtDojTo" runat="server" > </asp:TextBox>
                            <asp:ImageButton ID="imgApplyEnd" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                           <span class="field-label"> Select Emirate</span></td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlEmirate" runat="server" AutoPostBack="True" 
                               >
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Select Residetial Area</span></td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlArea" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblIdText" Text="Application Number" runat="server" CssClass="field-label"></asp:Label>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtStudentID" runat="server"> </asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblIdName" Text="Applicant Name" runat="server" CssClass="field-label"></asp:Label>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtName" runat="server"> </asp:TextBox>
                        </td>
                         <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                              />
                        </td>
                    </tr>
                    <tr id="trGrid" runat="server">
                        <td align="center" colspan="6">
                            <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Available">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                           Select<br />
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server">
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHId" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("APPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHName" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("APPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date of Join ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("DOJ", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Primary Contact Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPname" runat="server" Text='<%# Bind("PNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPmobile" runat="server" Text='<%# Bind("PMOBILE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPemail" runat="server" Text='<%# Bind("PEMAIL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Area">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPArea" runat="server" Text='<%# Bind("AREA") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderTemplate>
                                            Status<br />
                                                        <asp:DropDownList ID="ddlgvStatus" runat="server"  AutoPostBack="True"
                                                             OnSelectedIndexChanged="ddlgvStatus_SelectedIndexChanged">
                                                            <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="ENQUIRY" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="APPLICANT" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="REGISTERED" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="OFFERED" Value="6"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("STATUS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mail">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSend" OnClick="lnkSend_Click" runat="server" Text='Send'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSend" runat="server">
                        <td align="center" colspan="6" >
                           <asp:Button ID="btnSend" Text="Send Email" runat="server" CssClass="button" /></td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
            </td>
        </tr>
    </table>

    </div>
</div>
    </div>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgApplyStart" TargetControlID="txtDojFrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
       PopupButtonID="imgApplyEnd" TargetControlID="txtDojTo">
    </ajaxToolkit:CalendarExtender>
</asp:Content>
