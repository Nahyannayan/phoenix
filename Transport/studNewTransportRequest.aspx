<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studNewTransportRequest.aspx.vb" Inherits="Transport_studNewTransportRequest" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

        function AddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 500px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = 'tptAddAreaPickuptoTrip.aspx?';
            //window.showModalDialog(url, "", sFeatures);
            //return false;
            var oWnd = radopen(url, "pop_AddtoTrip");
        }
        function call() {
            alert('')
        }


        function ShowPanel() {



            var div1 = document.getElementById('Panel1')

            if (div1.style.display == 'none') {

                div1.style.display = 'inline'
            }
            else {
                div1.style.display = 'none'
            }
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_AddtoTrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>New Transport Requests     
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <%--<table id="Table1" border="0" width="100%">
                    <tr style="font-size: 12pt;">
                        <td align="left" class="title" style="height: 45px; width: 46%;">NEW TRANSPORT REQUESTS&nbsp;</td>
                    </tr>
                </table>--%>
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:"
                                Style="text-align: left" ValidationGroup="groupM1"></asp:ValidationSummary>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table align="center" id="tbPromote" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Curriculum</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Select Academic Year</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>


                                <tr>

                                    <td align="left"><span class="field-label">Select Grade</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Select Section</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>

                                    <%-- <td align="left" colspan="4">
                                        <table width="100%">
                                            <tr>--%>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStuNo" runat="server" AutoCompleteType="Disabled"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Student Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server" AutoCompleteType="Disabled">
                                        </asp:TextBox></td>

                                    <%--   </tr>
                                        </table>

                                    </td>--%>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled">
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="4"></asp:ImageButton>
                                    </td>
                                    <td align="left" colspan="2" width="50%">
                                        <asp:Button ID="btnApply" runat="server" Text="Apply to All" CssClass="button" TabIndex="4" OnClick="btnApply_Click" /></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Request Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtReqDate" runat="server" AutoCompleteType="Disabled">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgReqDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            TabIndex="4"></asp:ImageButton><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtReqDate"
                                                Display="Dynamic" ErrorMessage="Enter the Request Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                    ID="CustomValidator1" runat="server" ControlToValidate="txtReqDate" CssClass="error"
                                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Request Date  entered is not a valid date"
                                                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator><asp:RequiredFieldValidator
                                                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReqDate" Display="None" ErrorMessage="Please enter the field request date"
                                                        ValidationGroup="groupM1"></asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:LinkButton ID="lnkDropOff" runat="server" CausesValidation="False" OnClientClick="javascript: AddtoTrip(); return false;">Add Pickup Point to Trip</asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="lnkBuses" OnClientClick="javascript:ShowPanel();return false;" runat="server">Show Bus Areas</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnUpdate1" runat="server" Text="Update" CssClass="button" TabIndex="4" ValidationGroup="groupM1" /></td>
                                </tr>
                                <tr>


                                    <td align="left" colspan="4">

                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20" Width="100%" AllowSorting="True">
                                            <Columns>

                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Select
                                                        <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShfId" runat="server" Text='<%# Bind("STU_SHF_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stud No.">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblFeeHeader" runat="server" Text="Student ID"></asp:Label><br />
                                                        <asp:TextBox ID="txtFeeSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnFeeId_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnFeeId_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label><br />
                                                        <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRADE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pick Up Area">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlPArea" OnSelectedIndexChanged="ddlPArea_SelectedIndexChanged" AutoPostBack="true" Width="100%" runat="server"></asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Pick Up Point" HeaderStyle-Width="15%">
                                                    <ItemStyle HorizontalAlign="left" Wrap="true" />
                                                    <ItemTemplate>                                                        
                                                            <asp:DropDownList ID="ddlPTrip" AutoPostBack="true" OnSelectedIndexChanged="ddlPTrip_SelectedIndexChanged" runat="server" style="width:100%!important;"></asp:DropDownList>                                                        
                                                            <asp:DropDownList ID="ddlPickup" runat="server" style="width:100%!important;"></asp:DropDownList>
                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Drop Off Area">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlDArea" runat="server" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="ddlDArea_SelectedIndexChanged"></asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="DropOff  Point" HeaderStyle-Width="15%">
                                                    <ItemStyle HorizontalAlign="left" Wrap="true" />
                                                    <ItemTemplate>                                                      
                                                            <asp:DropDownList ID="ddlDTrip" AutoPostBack="true" OnSelectedIndexChanged="ddlDTrip_SelectedIndexChanged" runat="server" style="width:100%!important;"></asp:DropDownList>                                                      
                                                            <asp:DropDownList ID="ddlDropOff" runat="server" style="width:100%!important;"></asp:DropDownList>                                                 

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="From Date" HeaderStyle-Width="14%">
                                                    <ItemStyle HorizontalAlign="left" Wrap="true" />
                                                    <ItemTemplate>

                                                        <asp:TextBox ID="txtDate" runat="server" CausesValidation="true"></asp:TextBox>

                                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />

                                                        <asp:Label ID="lblErr" runat="server" Text="*" CssClass="error" Visible="false"></asp:Label>

                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                            Format="dd/MMM/yyyy" PopupButtonID="imgDate" PopupPosition="BottomLeft" TargetControlID="txtDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                        <asp:Label ID="lbl_FromDate" runat="server" Text='<%# Bind("STU_FROM_DOJ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <HeaderStyle />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" TabIndex="4" ValidationGroup="groupM1" />
                                    </td>


                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                        Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                        Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                    </ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                        Format="dd/MMM/yyyy" PopupButtonID="txtReqDate" TargetControlID="txtReqDate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar"
                        Format="dd/MMM/yyyy" PopupButtonID="imgReqDate" TargetControlID="txtReqDate">
                    </ajaxToolkit:CalendarExtender>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>


                </table>

                <%--     <asp:Panel ID="Panel1" runat="server" BackColor="AliceBlue" BorderStyle="Groove"
                   Height="110px" Style="z-index: 10; left: 470px; position: absolute; top: 645px; "
                   Width="274px">
    </asp:Panel>--%>
                <div id="Panel1" class="panel-cover" style="z-index: 10; left: 386px; position: absolute; top: 439px; display: none; height: 328px; width: 453px; overflow: auto;">
                    <table width="100%">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnClose" OnClientClick="javascript:ShowPanel();return false;" runat="server" Text="X" CssClass="button" Width="25px"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvAreas" runat="server" AllowPaging="FALSE" AutoGenerateColumns="False"
                                    CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                    PageSize="20" Width="100%" AllowSorting="True">
                                    <Columns>


                                        <asp:TemplateField HeaderText="Bus No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBno" runat="server" Text='<%# Bind("BNO_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Area">
                                            <ItemTemplate>
                                                <asp:Label ID="lblArea" runat="server" Text='<%# Bind("AREA") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <HeaderStyle />
                                    <RowStyle CssClass="griditem_alternative" />
                                    <SelectedRowStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>

                <%--    <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                   HorizontalSide="Center" TargetControlID="Panel1" VerticalSide="Middle">
               </ajaxToolkit:AlwaysVisibleControlExtender>--%>
            </div>
        </div>
    </div>

</asp:Content>

