﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.IO
Imports System.Linq.Enumerable
Partial Class PromotionUpload
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
    Public ImgUpload As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnUpload)
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim USR_NAME As String = Session("sUsr_name")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim Fyear As String = Session("F_YEAR")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200900") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"

        ElseIf mode = "add" Then

            If ViewState("MainMnu_code") = "T200900" Then
                mDisable = False
                ViewState("datamode") = "add"
                txtPromotion.Text = ""
                h_EntryId.Value = 0

            Else
                mDisable = False
                ViewState("datamode") = "add"
                ClearDetails()
            End If


        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"

        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        txtPromotion.Enabled = EditAllowed
        btnDocumentDelete.Enabled = EditAllowed
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnDelete.Visible = mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable

        btnUpload.Enabled = Not mDisable

        

    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then
                btnUpload.Visible = True
                UploadDocPhoto.Visible = True
            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select *from document ")
                strSql.Append("where doc_id=" & h_EntryId.Value)

                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtPromotion.Text = dt.Rows(0)("DOC_NAME")
                    h_str_img.Value = dt.Rows(0)("DOC_PATH")
                    btnDocumentLink.Text = "Attachment"

                    If h_str_img.Value <> "" Then
                        btnDocumentDelete.Visible = True
                        btnDocumentLink.Visible = True
                        btnUpload.Visible = False
                        UploadDocPhoto.Visible = False
                    Else
                        btnDocumentDelete.Visible = False
                        btnDocumentLink.Visible = False
                        btnUpload.Visible = True
                        UploadDocPhoto.Visible = True
                    End If
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If

            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Sub ClearDetails()

        txtPromotion.Text = ""
        
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SavedataNew(1)
    End Sub

    Private Sub SavedataNew(ByVal Act As Integer)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        Try
            Dim Errormessage As String = ""
            Dim pParms(5) As SqlParameter
            pParms(0) = Mainclass.CreateSqlParameter("@DOC_ID", h_EntryId.Value, SqlDbType.Int, True)
            pParms(1) = Mainclass.CreateSqlParameter("@DOCNAME", txtPromotion.Text, SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@DOCUSER", "SYSTEM", SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@DOCPATH", h_str_img.Value, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@Option", Act, SqlDbType.Int)
            pParms(5) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
            pParms(5).Direction = ParameterDirection.Output

            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVEPROMOTIONDOCUMENT", pParms)
            Dim ErrorMSG As String = pParms(5).Value.ToString()

            If ErrorMSG = "" Then
                ViewState("EntryId") = pParms(0).Value
            Else
                'lblError.Text = "Unexpected Error !!!"
                'DeleteUploadedDocument()
                usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            End If
            stTrans.Commit()
            
        Catch ex As Exception
            DeleteUploadedDocument()
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

        SetDataMode("view")
        setModifyvalues(ViewState("EntryId"))
        If Act = 1 Then
            usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        ElseIf Act = 2 Then
            usrMessageBar.ShowNotification("Promotion Deleted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        Else
            usrMessageBar.ShowNotification("Document Deleted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        End If


    End Sub
    Private Sub Savedata(ByVal Act As Integer)
        Dim transaction As SqlTransaction
        btnSave.Enabled = False
        Dim conn As New SqlConnection(connectionString)
        conn.Open()
        transaction = conn.BeginTransaction("UploadPro")
        Try
            Dim param1(5) As SqlParameter
            param1(0) = New SqlParameter("@DOC_ID", h_EntryId.Value, SqlDbType.Int, True)
            param1(1) = New SqlParameter("@DOCNAME", txtPromotion.Text)
            param1(2) = New SqlParameter("@DOCUSER", "SYSTEM")
            param1(3) = New SqlParameter("@DOCPATH", h_str_img.Value)
            param1(4) = New SqlParameter("@Option", Act)
            param1(5) = New SqlParameter("@RET_VAL", SqlDbType.Int)
            param1(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVEPROMOTIONDOCUMENT", param1)
            If param1(5).Value = 0 Then
                Try
                    transaction.Commit()
                    If h_EntryId.Value = 0 Then
                        ViewState("EntryId") = param1(0).Value
                    Else
                        ViewState("EntryId") = h_EntryId.Value
                    End If

                    If ImgUpload = True Then
                        WritetoAuit("", h_FileName.Value, "Upload", Page.User.Identity.Name.ToString, Me.Page, h_FileName.Value)
                    End If
                Catch ex As Exception

                End Try
            Else
                transaction.Rollback()
                DeleteUploadedDocument()
                Exit Sub
            End If
        Catch myex As ArgumentException
            transaction.Rollback()
            DeleteUploadedDocument()
            'lblError.Text = myex.Message
            usrMessageBar.ShowNotification(myex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Catch ex As Exception
            transaction.Rollback()
            DeleteUploadedDocument()
            'lblError.Text = "Record could not be Saved"
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            DeleteUploadedDocument()
            Exit Sub

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

        End Try
        SetDataMode("view")
        setModifyvalues(ViewState("EntryId"))
        If Act = 1 Then
            'lblError.Text = "Data Saved Successfully !!!"
            usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        Else
            'lblError.Text = "Data Deleted Successfully !!!"
            usrMessageBar.ShowNotification("Data Deleted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        End If

    End Sub
    Private Sub DeleteUploadedDocument()
        Dim FullPath As String = h_str_img.Value

        If File.Exists(FullPath) Then
            File.Delete(FullPath)
            btnDocumentLink.Text = ""
            btnDocumentLink.Visible = True
        Else
            'lblError.Text = "File Not found"
            usrMessageBar.ShowNotification("File Not found", UserControls_usrMessageBar.WarningType.Danger)

        End If
    End Sub
    Public Shared Function WritetoAuit(ByVal aud_form As String, _
 ByVal aud_docno As String, ByVal aud_action As String, _
 Optional ByVal userInfo As String = "", _
 Optional ByVal ocontrol As Control = Nothing, _
 Optional ByVal aud_remarks As String = "") As Integer
        Dim conn1 As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Dim strHostName As String
        'Dim strIPAddress As String
        strHostName = System.Net.Dns.GetHostName()
        'strIPAddress = System.Net.Dns.GetHostEntry(strHostName).AddressList(0).ToString()
        Dim ClientIP As String = HttpContext.Current.Request.UserHostAddress()
        conn1.Open()
        Try
            Using conn1
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@Aud_user", ClientIP)
                pParms(1) = New SqlClient.SqlParameter("@Aud_bsu_id", "OL")
                pParms(2) = New SqlClient.SqlParameter("@Aud_module", "T0")
                pParms(3) = New SqlClient.SqlParameter("@Aud_form", "\Transport\PromotionUpload.aspx.vb")
                pParms(4) = New SqlClient.SqlParameter("@Aud_docNo", aud_docno)
                pParms(5) = New SqlClient.SqlParameter("@Aud_action", "Upload")
                pParms(6) = New SqlClient.SqlParameter("@Aud_remarks", aud_remarks)
                pParms(7) = New SqlClient.SqlParameter("@AUD_HOST", HttpContext.Current.Request.UserHostAddress.ToString()) 'host.HostName)
                pParms(8) = New SqlClient.SqlParameter("@AUD_WINUSER", userInfo)
                pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(conn1, CommandType.StoredProcedure, "dbo.SaveAudit", pParms)
                Dim ReturnFlag As Integer = pParms(9).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            conn1.Close()
        End Try
        conn1.Close()

    End Function
    Private Sub UploadImage()
        'If UploadDocPhoto.FileName <> "" Then

        '    Dim str_img As String = WebConfigurationManager.ConnectionStrings("UploadTransportPromo").ConnectionString
        '    'Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("TempFileFolder").ConnectionString
        '    'Dim str_img As String = WebConfigurationManager.AppSettings.Item("UploadTransportPromo").ConnectionString

        '    Dim str_tempfilename As String = UploadDocPhoto.FileName
        '    'h_tempFilepath.Value = str_imgvirtual & UploadDocPhoto.FileName

        '    'h_str_img.Value = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\"
        '    Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename
        '    h_str_img.Value = strFilepath

        '    Try
        '        UploadDocPhoto.PostedFile.SaveAs(strFilepath)
        '        h_fileNamewithPath.Value = strFilepath
        '        h_FileName.Value = UploadDocPhoto.FileName
        '        btnDocumentLink.Text = str_tempfilename
        '        btnDocumentLink.Visible = True
        '        hdnFileName.Value = strFilepath
        '        ImgUpload = True

        '    Catch ex As Exception
        '        ImgUpload = False
        '        Exit Sub
        '    End Try

        'ElseIf Not ViewState("imgAsset") Is Nothing Then

        'Else
        '    ViewState("imgAsset") = Nothing
        'End If
    End Sub
    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property


    
    Private Sub UploadCodument(ByVal TRNO As String)
       
        'If Not IO.File.Exists(h_fileNamewithPath.Value) Then
        '    lblError.Text = "Invalid FilePath!!!!"
        '    Exit Sub
        'Else
        '    Dim ContentType As String = getContentType(h_fileNamewithPath.Value)
        '    Dim FileExt As String = h_tempFilepath.Value.Substring(h_tempFilepath.Value.Length - 4)
        '    If FileExt.Substring(0, 1) <> "." Then FileExt = h_tempFilepath.Value.Substring(h_tempFilepath.Value.Length - 5)
        '    If FileExt.Substring(0, 1) <> "." Then FileExt = h_tempFilepath.Value.Substring(h_tempFilepath.Value.Length - 6)
        '    Try
        '        IO.File.Move(h_fileNamewithPath.Value, h_str_img.Value & TRNO & "_" & Session("sBsuid") & FileExt)
        '        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "insert into TPTHiring_I (THI_CONTENTTYPE, THI_DOCNO, THI_BSU_ID, THI_NAME, THI_DATE, THI_USERNAME, THI_FILENAME, THI_DELETE) values ('" & ContentType & "','" & txtPromotion.Text & "','" & Session("sBSUID") & "','" & h_FileName.Value & "',getdate(),'" & Session("sUsr_name") & "','" & txtPromotion.Text & "_" & Session("sBsuid") & FileExt & "',0)")
        '        hdnFileName.Value = txtPromotion.Text & "_" & Session("sBsuid") & FileExt
        '        hdnContentType.Value = ContentType
        '        btnDocumentLink.Text = UploadDocPhoto.FileName
        '        btnDocumentLink.Visible = True
        '        btnDocumentDelete.Visible = True
        '        UploadDocPhoto.Visible = False
        '        btnUpload.Visible = False
        '    Catch ex As Exception

        '    End Try
        'End If

    End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select
    End Function
    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then

            Dim str_img As String = WebConfigurationManager.AppSettings.Item("UploadTransportPromo")
            'Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("TempFileFolder").ConnectionString
            'Dim str_img As String = WebConfigurationManager.AppSettings.Item("UploadTransportPromo").ConnectionString

            Dim str_tempfilename As String = UploadDocPhoto.FileName
            'h_tempFilepath.Value = str_imgvirtual & UploadDocPhoto.FileName

            'h_str_img.Value = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\"
            Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
                h_fileNamewithPath.Value = strFilepath
                h_FileName.Value = UploadDocPhoto.FileName
                h_str_img.Value = strFilepath
                btnDocumentLink.Text = h_FileName.Value
                btnDocumentLink.Visible = True
                hdnFileName.Value = UploadDocPhoto.FileName
                btnUpload.Enabled = False
                ImgUpload = True


            Catch ex As Exception
                ImgUpload = False
                Exit Sub
            End Try
          
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub
    

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        SavedataNew(2)
    End Sub

    Protected Sub btnDocumentDelete_Click(sender As Object, e As EventArgs) Handles btnDocumentDelete.Click
        DeleteUploadedDocument()
        btnDocumentDelete.Visible = False
        btnUpload.Visible = True
        SavedataNew(3)

    End Sub
End Class