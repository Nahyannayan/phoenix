<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudRecordView.aspx.vb" Inherits="StudRecordView" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <script language="javascript" type="text/javascript">

        function popup_new(Sibid, stuid) {

            var sFeatures;
            sFeatures = "dialogWidth: 950px; ";
            sFeatures += "dialogHeight: 850px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // alert(Sibid+','+stuid)
            //            
            url = '../Transport/studRecordUpdatedetails.aspx?Stu_id=' + stuid + '&Sib_id=' + Sibid + '';
            //alert(status) 

            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_rel");
            //if (result == '' || result == undefined) {
            //    return false;
            //}

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Update Student Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table width="100%">
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:HiddenField ID="HF_stuid" runat="server" Visible="False" />
            </td>
        </tr>
        <tr>
            <td align="left" width="25%"><span class="field-label">Select Academic year</span> </td>
            <td align="left" width="35%"><asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
            </asp:DropDownList>
            </td>
            <td></td>
            <td></td>

        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:GridView ID="gvStudentRecord" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    PageSize="20" Width="100%">
                    <RowStyle CssClass="griditem" />
                    <Columns>
                        <asp:TemplateField HeaderText="-" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("eqs_id") %>' __designer:wfdid="w10"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fee Id">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox9" runat="server"
                                    Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w17"></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderTemplate>
                                <asp:Label ID="lblSTUD_IDH" runat="server" Text="FEE ID" CssClass="gridheader_text" __designer:wfdid="w18"></asp:Label>
                                <br />       
                                <asp:TextBox ID="txtSTUD_ID" runat="server" Width="75%" __designer:wfdid="w19"></asp:TextBox>
                                <asp:ImageButton ID="btnSearchStud_ID" OnClick="btnSearchStud_ID_Click"
                                                                    runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle"
                                                                    __designer:wfdid="w20"></asp:ImageButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSTUD_ID" runat="server" Text='<%# Bind("STU_FEE_ID") %>'
                                    __designer:wfdid="w16"></asp:Label>
                            </ItemTemplate>

                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("STU_No") %>' __designer:wfdid="w22"></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderTemplate>
                                <asp:Label ID="lblStud_NameH" runat="server" Text="Student Name" CssClass="gridheader_text" __designer:wfdid="w23"></asp:Label>
                                <br /><asp:TextBox ID="txtStud_Name" runat="server" Width="75%" __designer:wfdid="w24"></asp:TextBox>
                                <asp:ImageButton ID="btnSearchStud_Name" OnClick="btnSearchStud_Name_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" __designer:wfdid="w25"></asp:ImageButton>
                                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lblStud_Name" OnClick="lblStud_Name_Click" runat="server"
                                    Text='<%# Bind("STU_PASPRTNAME") %>' CommandName="Select" OnClientClick="<%# &quot;popup_new('&quot; & Container.DataItem(&quot;STU_SIBLING_ID&quot;)& &quot;','&quot; & Container.DataItem(&quot;stu_id&quot;) & &quot;');return false;&quot; %>"></asp:LinkButton>
                            </ItemTemplate>

                            <HeaderStyle Wrap="False"></HeaderStyle>

                            <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("SName") %>' __designer:wfdid="w27"></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderTemplate>
                                <asp:Label ID="lblGradeH" runat="server" Text="Grade Section" CssClass="gridheader_text" __designer:wfdid="w28"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtStud_Grade" runat="server" Width="75%" __designer:wfdid="w29"></asp:TextBox>
                                <asp:ImageButton ID="btnSearchGrade" OnClick="btnSearchGrade_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" __designer:wfdid="w30"></asp:ImageButton>
                                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="lblGradeH" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' __designer:wfdid="w4"></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                            <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Updated">
                            <HeaderTemplate>
                               Updated
                               <br />
                               <asp:DropDownList ID="ddlgvDoc" runat="server" CssClass="listbox" AutoPostBack="True" Width="75%" __designer:wfdid="w32" OnSelectedIndexChanged="ddlgvDoc_SelectedIndexChanged">
                                                    <asp:ListItem>ALL</asp:ListItem>
                                                    <asp:ListItem>YES</asp:ListItem>
                                                    <asp:ListItem>NO</asp:ListItem>
                                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Image ID="imgDoc" runat="server" ImageUrl='<%# BIND("STU_bUPDATEDIMG") %>' HorizontalAlign="Center" __designer:wfdid="w31"></asp:Image>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Updated Date">
                            <ItemTemplate>
                                <asp:Label ID="lblUpdatedDate" Text='<%# Bind("STU_UPDATED_DATE") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Emergancy Contact">
                            <ItemTemplate>
                                <asp:Label ID="lblEMGCONTACT" Text='<%# Bind("STU_EMGCONTACT") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student ID" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("stu_id") %>' __designer:wfdid="w38"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStudentID" runat="server" Text='<%# Bind("stu_id") %>' __designer:wfdid="w37"></asp:Label>
                                <asp:Label ID="lblSIBLING_ID" runat="server"
                                    Text='<%# Bind("STU_SIBLING_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#d7f9ae" />
                    <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
            </td>
        </tr>
    </table>
    
    </div>
    </div>
    </div>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="800px" Height="700px" >
            </telerik:RadWindow>
        </Windows>
       </telerik:RadWindowManager>

</asp:Content>

