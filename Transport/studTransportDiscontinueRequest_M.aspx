<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studTransportDiscontinueRequest_M.aspx.vb" Inherits="Transport_studTransportDiscontinueRequest_M"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Transport service discontinuation request
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="vertical-align: top; width: 100%">
                    <tr>
                        <td align="left" colspan="2">
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <%-- <tr>
                            </tr>
                            <td style="width: 692px"></td>--%>
                            <asp:ValidationSummary ID="valSum" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                SkinID="error" Style="text-align: left" ValidationGroup="groupM1"></asp:ValidationSummary>

                        </td>
                    </tr>
                    <tr class="title-bg-lite">
                        <td align="left" colspan="2">Transport services history</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:GridView ID="gvServiceHistory" runat="server" Width="100%" EmptyDataText="No Services to show" CssClass="table table-bordered table-row"></asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 50%">
                            <asp:DataList ID="dtlStudentInfo" runat="server" RepeatDirection="Vertical" ShowHeader="true" Width="100%">
                                <ItemTemplate>
                                    <asp:Label ID="lblLHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LHS")%>'></asp:Label>&nbsp;
                            <asp:Label ID="lblRHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RHS")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                        <td style="vertical-align: top; width: 50%">
                            <asp:DataList ID="dtlStudentServiceInfo" runat="server" RepeatDirection="Vertical" ShowHeader="true" Width="100%">
                                <ItemTemplate>
                                    <asp:Label ID="lblLHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LHS")%>'></asp:Label>&nbsp;
                            <asp:Label ID="lblRHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RHS")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:DataList></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%;" id="tblTpt" runat="server">
                                <%-- <tr>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblStuNo" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Section</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Pickup Area</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblPArea" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Pickup Point</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblPickup" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Pickup Trip</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblPtrip" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Drop Off Area</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblDArea" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Drop Off Point</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblDropoff" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Drop Off Trip</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblDTrip" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>--%>
                                <tr>
                                    <td align="left"><span class="field-label">Request Date</span></td>

                                    <td align="left">
                                        <asp:TextBox
                                            ID="txtRequest" runat="server" AutoPostBack="True">
                                        </asp:TextBox><asp:ImageButton ID="imgRequest" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtRemarks" Display="Dynamic"
                                            ErrorMessage="Enter the RequestDate From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator1" runat="server" ControlToValidate="txtRequest" CssClass="error"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Request Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRequest"
                                                    CssClass="error" Display="Dynamic" ErrorMessage="Please enter the request date"></asp:RequiredFieldValidator></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Discontinuation Details</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <table id="tblTpt0" width="100%" runat="server">
                                            <tr>
                                                <td align="left"><span class="field-label">Discontinuation Type</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True">
                                                        <asp:ListItem Value="--">--</asp:ListItem>
                                                        <asp:ListItem Value="T">Temporary Discontinuation</asp:ListItem>
                                                        <asp:ListItem Value="S">Transport Suspension</asp:ListItem>
                                                        <asp:ListItem Value="P">Permanent Discontinuation</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>


                                                <td align="left">
                                                    <asp:Label ID="lblDtFrom" runat="server" Text="From" CssClass="field-label"></asp:Label></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled"></asp:TextBox><asp:ImageButton
                                                        ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton><asp:RegularExpressionValidator
                                                            ID="revDocDate" runat="server" ControlToValidate="txtFrom" CssClass="error" Display="Dynamic"
                                                            EnableViewState="False" ErrorMessage="Enter From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                            ForeColor="" Style="position: relative" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>


                                                <td align="left"><span class="field-label">To</span></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtTo" runat="server" AutoCompleteType="Disabled"></asp:TextBox><asp:ImageButton
                                                        ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton><asp:RegularExpressionValidator
                                                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTo" CssClass="error"
                                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                            ForeColor="" Style="position: relative" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Discontinuation Reason</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddldiscontResons" runat="server">
                                                    </asp:DropDownList></td>
                                                <td align="left">&nbsp;</td>
                                                <td align="left">&nbsp;</td>
                                                <td align="left">&nbsp;</td>
                                                <td align="left">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Comments</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine">
                                        </asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">&nbsp;
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfDbusNo" runat="server" />
                            <asp:HiddenField ID="hfTCH_ID" runat="server" />
                            <asp:RequiredFieldValidator ID="rfrEMARKS" runat="server" ControlToValidate="txtRemarks" Display="None"
                                ErrorMessage="Please enter the Remarks" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfPbusNo" runat="server" />
                            <asp:HiddenField ID="hfFromDate" runat="server" />
                            <asp:RequiredFieldValidator ID="rfFrom"
                                runat="server" ControlToValidate="txtFrom" Display="None" ErrorMessage="Please enter the starting date "
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rdTo" runat="server" ControlToValidate="txtTo" Display="None"
                                ErrorMessage="Please enter the end date" ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>
                            &nbsp;
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                            </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="hfSHF_ID" runat="server" EnableViewState="False" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

