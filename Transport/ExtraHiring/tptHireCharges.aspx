<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptHireCharges.aspx.vb" Inherits="Transport_ExtraHiring_tptHireCharges" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style type="text/css">
        .autocomplete_highlightedListItem_S1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }

        /*.RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }*/
        .RadComboBox_Default .rcbReadOnly {
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbDisabled {
            background-color: rgba(0,0,0,0.01) !important;
        }

            .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                background-color: transparent !important;
                border-radius: 0px !important;
                border: 0px !important;
                padding: initial !important;
                box-shadow: none !important;
            }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function parseTime(s) {
            var c = s.split(':');
            return parseInt(c[0]) * 60 + parseInt(c[1]);
        }

        function calc(Type, rate) {
            if (Type == "T")

                //&& (document.getElementById("<%=txtRentPM.ClientID %>").value != "") &&(document.getElementById("<%=txtDiscount.ClientID %>").value != "")
            {
                document.getElementById("<%=txtDiscountedRatePM.ClientID %>").value = eval(document.getElementById("<%=txtRentPM.ClientID %>").value) - eval(document.getElementById("<%=txtDiscount.ClientID %>").value);
                document.getElementById("<%=txtTotalAmount.ClientID %>").value = eval(document.getElementById("<%=txtDiscountedRatePM.ClientID %>").value) + eval(document.getElementById("<%=txtSalik.ClientID %>").value) + eval(document.getElementById("<%=txtFuel.ClientID %>").value);
                document.getElementById("<%=txttotalBillAmount.ClientID %>").value = eval(document.getElementById("<%=txtTotalAmount.ClientID %>").value) + eval(document.getElementById("<%=txtExcessKMCharge.ClientID %>").value);
                var VATAmt = (eval(document.getElementById("<%=txttotalBillAmount.ClientID %>").value) * eval(document.getElementById("<%=h_VATPerc.ClientID %>").value)) / 100;
                document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
                document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txttotalBillAmount.ClientID %>").value)).toFixed(2);

            }
            else if (Type == "K") {
                document.getElementById("<%=txtTotalKM.ClientID %>").value = eval(document.getElementById("<%=txtEndingKM.ClientID %>").value) - eval(document.getElementById("<%=txtStartingKM.ClientID %>").value);
                if ((eval(document.getElementById("<%=txtTotalKM.ClientID %>").value)) > (eval(document.getElementById("<%=txtUsageLimit.ClientID %>").value))) {
                    document.getElementById("<%=txtAddKM.ClientID %>").value = eval(document.getElementById("<%=txtTotalKM.ClientID %>").value) - eval(document.getElementById("<%=txtUsageLimit.ClientID %>").value);
                    document.getElementById("<%=txtExcessKMCharge.ClientID %>").value = eval(document.getElementById("<%=txtAddKM.ClientID %>").value) * eval(document.getElementById("<%=txtExcessKMRate.ClientID %>").value);
                    document.getElementById("<%=txttotalBillAmount.ClientID %>").value = eval(document.getElementById("<%=txtTotalAmount.ClientID %>").value) + eval(document.getElementById("<%=txtExcessKMCharge.ClientID %>").value);
                    var VATAmt = (eval(document.getElementById("<%=txttotalBillAmount.ClientID %>").value) * eval(document.getElementById("<%=h_VATPerc.ClientID %>").value)) / 100;
                    document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
                    document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txttotalBillAmount.ClientID %>").value)).toFixed(2);
                }
                else {
                    document.getElementById("<%=txtAddKM.ClientID %>").value = 0
                    document.getElementById("<%=txtExcessKMCharge.ClientID %>").value = eval(document.getElementById("<%=txtAddKM.ClientID %>").value) * eval(document.getElementById("<%=txtExcessKMRate.ClientID %>").value);
                    document.getElementById("<%=txttotalBillAmount.ClientID %>").value = eval(document.getElementById("<%=txtTotalAmount.ClientID %>").value) + eval(document.getElementById("<%=txtExcessKMCharge.ClientID %>").value);
                    var VATAmt = (eval(document.getElementById("<%=txttotalBillAmount.ClientID %>").value) * eval(document.getElementById("<%=h_VATPerc.ClientID %>").value)) / 100;
                    document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
                    document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txttotalBillAmount.ClientID %>").value)).toFixed(2);
                }
            }

    }


    function ChangeAllCheckBoxStates(checkState) {

        var chk_state = document.getElementById("chkAL").checked;

        if (chk_state) {

            document.getElementById("<%=txtSelectedAmt.ClientID %>").value = document.getElementById("<%=hdngross.ClientID %>").value;
        }
        else {
            document.getElementById("<%=txtSelectedAmt.ClientID %>").value = '0.00';

        }

        for (i = 0; i < document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }
    }





    function confirm_delete() {

        if (confirm("You are about to delete this record.Do you want to proceed?") == true)
            return true;
        else
            return false;
    }




    </script>
    

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Vehicle Charges
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr id="ErrId" visible="false" >
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">
                            <table align="center" width="100%">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="27" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Vehicle Charges</span></font>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="3%"><span class="field-label">Date</span>
                                                </td>
                                                <td width="5%">
                                                    <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                        TargetControlID="txtDate" PopupButtonID="lnkDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                                <td width="20%">
                                                    <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Load.." />
                                                </td>
                                                <td width="65%"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th>Charging Unit
                                                </th>
                                                <th>Reg.No
                                                </th>
                                                <th>Vehicle Type
                                                </th>
                                                <th>Model
                                                </th>
                                                <th>Unit Reg No.
                                                </th>
                                                <th>User Name
                                                </th>
                                                <th>DAX Code
                                                </th>
                                                <th>Rent P.M
                                                </th>
                                                <th>Discount
                                                </th>
                                                <th>Discounted Rate P.M
                                                </th>
                                                <th>Salik(@4 AED) P.M
                                                </th>
                                                <th>Fuel P.M
                                                </th>
                                                <th>Total Amount
                                                </th>
                                                <th>Start KM
                                                </th>
                                                <th>Ending KM
                                                </th>
                                                <th>Total KM
                                                </th>
                                                <th>Usage Limit
                                                </th>
                                                <th>Additional KM
                                                </th>
                                                <th>Excess KM Rate
                                                </th>
                                                <th>Excess KM Charge
                                                </th>
                                                <th>Total Billed Amount
                                                </th>
                                                <th>VAT Code
                                                </th>
                                                <th>Emirate
                                                </th>
                                                <th>VAT Amount
                                                </th>
                                                <th>Net Amount
                                                </th>
                                                <th align="left">Remarks
                                                </th>
                                                <th align="left">Notes
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtTSRSNo" runat="server" Width="10px" Visible="false"></asp:TextBox>
                                                    <telerik:RadComboBox ID="RadCBSU" Width="350" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRegno" runat="server" Width="100px" AutoPostBack="true" TabIndex="1"
                                                        align="left"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVehicleType" runat="server" TabIndex="2" Width="200px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtModel" runat="server" Width="100px" TabIndex="3" align="left"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUnitRegno" runat="server" Width="100px" TabIndex="4" align="left"></asp:TextBox>
                                                </td>


                                                <td>
                                                    <asp:TextBox ID="txtUser" runat="server" Width="200px" TabIndex="5"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadDAXCodes" Width="375" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtRentPM" Width="100px" Style="text-align: right" runat="server" TabIndex="6"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDiscount" Width="100px" Style="text-align: right" runat="server" TabIndex="7"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDiscountedRatePM" Width="100px" Style="text-align: right" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSalik" Width="100px" Style="text-align: right" runat="server" TabIndex="8"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFuel" Width="100px" Style="text-align: right" runat="server" TabIndex="9"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalAmount" Width="100px" Style="text-align: right" runat="server" Enabled="False"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtStartingKM" Width="100px" Style="text-align: right" runat="server" TabIndex="10"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndingKM" Width="100px" Style="text-align: right" runat="server" TabIndex="11"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalKM" Width="100px" Style="text-align: right" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUsageLimit" Width="100px" Style="text-align: right" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddKM" Width="100px" Style="text-align: right" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtExcessKMRate" Width="100px" Style="text-align: right" runat="server" TabIndex="13"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtExcessKMCharge" Width="100px" Style="text-align: right" runat="server" Enabled="False"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txttotalBillAmount" Width="100px" Style="text-align: right" runat="server" Enabled="False"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="radVAT" Width="350" runat="server" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadEmirate" Width="250" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVATAmount" Width="100px" Style="text-align: right" Enabled="false" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNetAmount" Width="100px" Style="text-align: right" Enabled="false" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>

                                                <%--  <td >
                               <asp:TextBox ID="txtInvNo" runat="server"   ></asp:TextBox></td>--%>
                                                <td>
                                                    <asp:TextBox ID="txtRemarks" runat="server" Width="200px" TabIndex="14"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNotes" runat="server" Width="200px" TabIndex="15"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <%--</td>
                    </tr>--%>
                    <tr>
                        <td colspan="3" align="left">
                            <table width="100%">
                                <tr>
                                    <td width="20%">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button m-0" Text="Save" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button m-0" Text="Cancel" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnCopy1" runat="server" CssClass="button m-0" Text="Copy" />
                                        <asp:Button ID="btnPost1" runat="server" CssClass="button m-0" Text="Post" />
                                        <asp:HyperLink ID="btnExport" runat="server">Export to Excel</asp:HyperLink>
                                        <asp:HiddenField ID="hdngross" runat="server" />
                                    </td>
                                    <td width="40%"></td>
                                    <td width="40%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                     <td width="4%">
                                        <asp:Label ID="Postingdate" Text="Posting Date" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:TextBox ID="txtPostingDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgPosting" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtPostingDate" PopupButtonID="ImgPosting">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>

                                    <td width="4%">
                                        <asp:Label ID="lblSelAmount" Text="Selected Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:TextBox ID="txtSelectedAmt" Style="text-align: right" runat="server"></asp:TextBox>
                                    </td>

                                    <td width="3%">
                                        <asp:Label ID="Label1" Text="VAT Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:TextBox ID="txtSelVAt" Style="text-align: right" runat="server"></asp:TextBox>
                                    </td>

                                    <td width="3%">
                                        <asp:Label ID="Label2" Text="Net Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:TextBox ID="txtSelNet" Style="text-align: right" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="72%"></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="8%">
                                        <asp:RadioButtonList ID="rblSelection" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="0"><span class="field-label"> Not Posted </span></asp:ListItem>
                                            <asp:ListItem Value="1"><span class="field-label">Posted </span></asp:ListItem>
                                            <asp:ListItem Value="2"><span class="field-label">All </span></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td width="8%">
                                        <telerik:RadComboBox ID="RadFilterBSU" Width="600" AllowCustomText="true" Filter="Contains" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>
                                    </td>  
                                    <td></td>                                 
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row mb-0"
                                Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="ID">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <%--<input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All"   />                                --%>
                                            <asp:CheckBox ID="chkAL" AutoPostBack="true" runat="server" OnCheckedChanged="chkAL_CheckedChanged"></asp:CheckBox>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                        </ItemTemplate>



                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("ID") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TRDATE" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Work Unit" HeaderText="Working Unit" />
                                    <asp:BoundField DataField="Type of Vehicle" HeaderText="Vehicle Type" />
                                    <asp:BoundField DataField="Reg No." HeaderText="Reg.No." />
                                    <asp:BoundField DataField="DAX_DESCR" HeaderText="DAX CODE" />
                                    <asp:BoundField DataField="Model" HeaderText="Model" />
                                    <asp:BoundField DataField="Unit Reg" HeaderText="Unit Reg." />
                                    <asp:BoundField DataField="User" HeaderText="User Name" />
                                    <asp:BoundField DataField="Rent per month" HeaderText="Rent PM" />
                                    <asp:BoundField DataField="Discount" HeaderText="Discount" />
                                    <asp:BoundField DataField="Discounted Rate per month" HeaderText="Discounted Rent PM." />
                                    <asp:BoundField DataField="SALIK (@AED 4) p.m." HeaderText="Salik" />
                                    <asp:BoundField DataField="Fuel p.m." HeaderText="Fuel" />
                                    <asp:BoundField DataField="Starting KM" HeaderText="Starting KM" />
                                    <asp:BoundField DataField="Ending KM" HeaderText="Ending KM" />
                                    <asp:BoundField DataField="Usage Limit" HeaderText="Usage Limit" />
                                    <asp:BoundField DataField="Additional KM Travelled" HeaderText="Excess KM" />
                                    <asp:BoundField DataField="Excess KM Rate" HeaderText="Add.KM Rate" />
                                    <asp:BoundField DataField="Excess KM Charges" HeaderText="Add.KM Amount" />
                                    <%--<asp:BoundField DataField="Total Amount to be billed" HeaderText="Total Billed Amount" />--%>

                                    <asp:TemplateField HeaderText="Total Billed Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Eval("Total Amount to be billed") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="THC_TAX_CODE" HeaderText="VAT Code" />
                                    <asp:BoundField DataField="EMIRATE" HeaderText="Emirate" />
                                    <%--<asp:BoundField DataField="THC_TAX_AMOUNT" HeaderText="VAT Amount" />
                        <asp:BoundField DataField="THC_TAX_NET_AMOUNT" HeaderText="Net Amount" />--%>

                                    <asp:TemplateField HeaderText="VAT Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVATAmt" runat="server" Text='<%# Eval("THC_TAX_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetAmt" runat="server" Text='<%# Eval("THC_TAX_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Invoice No" HeaderText="Invoice" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                    <asp:BoundField DataField="Login User" HeaderText="Loged in User Name" />
                                    <asp:BoundField DataField="IJV NO" HeaderText="IJV NO" />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTHCID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" OnClick="lnkPrint_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTHC_invNo" runat="server" Text='<%# Eval("Invoice No") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPOstingDate" runat="server" Text='<%# Eval("THC_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmail" runat="server" Text="Email" OnClick="lnkEmail_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblHC_invNo" runat="server" Text='<%# Eval("Invoice No") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPDate" runat="server" Text='<%# Eval("THC_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>                                    
                                    <td align="left" width="20%">
                                        <asp:Button ID="btnCopy" runat="server" CssClass="button m-0" Text="Copy" />
                                        <asp:Button ID="btnPost" runat="server" CssClass="button m-0" Text="Post" />
                                    </td>
                                    <td width="40%"></td>
                                    <td width="40%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="bottom" align="center">
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_mimimum" runat="server" />
                            <asp:HiddenField ID="h_TEI_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_VehicleType" runat="server" />
                            <asp:HiddenField ID="h_VATPerc" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
