﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Transport_ExtraHiring_tptPostingExGratiaNES
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200219") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            BindDAXCodes()
            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                If ViewState("datamode") = "add" Then
                    SetDataMode("add")
                Else
                    SetDataMode("view")
                    showNoRecordsFound()
                End If
                setModifyvalues(ViewState("EntryId"))
            End If

            showNoRecordsFound()
            grdInvoiceDetails.DataSource = TPTNESExgratiaInvoiceDetails
            grdInvoiceDetails.DataBind()
            showNoRecordsFound()
        Else
            calcTotals()
        End If

    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        grdInvoiceDetails.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnSAVE.Visible = False
        btnPrint.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim Amount(3) As String
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = "0" Then
            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                If ViewState("datamode") = "add" Then
                    'hdnNetTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(NetAmount),0) [NET AMOUNT] FROM  (select EGN_BSU_ID,EGN_DRIVERTYPE, CASE WHEN EGN_DRIVERTYPE ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN EGN_DRIVERTYPE ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN EGN_DRIVERTYPE ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END [NetAmount]  ,EGN_EMP_ID,EGN_TRDATE from TPTEXGRATIACHARGESNES WHERE EGn_EGI_ID = 0    group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE) Net ", "OASIS_TRANSPORTConnectionString")
                    'hdnNesTotal.Value = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES WHERE EGn_EGI_ID = 0    and EGN_DELETED=0 and EGN_DRIVERTYPE=1 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)NES  ", "OASIS_TRANSPORTConnectionString")
                    'hdnVillaTotal.Value = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES WHERE EGn_EGI_ID = 0    and EGN_DELETED=0 and EGN_DRIVERTYPE=2 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")
                    'hdnIITTotal.Value = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES WHERE EGn_EGI_ID = 0    and EGN_DELETED=0 and EGN_DRIVERTYPE=3 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)IIT  ", "OASIS_TRANSPORTConnectionString")
                    'hdnOutTotal.Value = Mainclass.getDataValue("select isnull(sum(EGN_OUTSTATION),0) from (select MAX(EGN_OUTSTATION*25)  [EGN_OUTSTATION] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and EGN_DELETED=0 and left(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OUTSTATION ", "OASIS_TRANSPORTConnectionString")
                    'hdnOverNight.Value = Mainclass.getDataValue(" select isnull(sum(EGN_OVERNIGHTSTAY),0) from (select case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end  [EGN_OVERNIGHTSTAY] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and EGN_DELETED=0 and left(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OVERNIGHTSTAY ", "OASIS_TRANSPORTConnectionString")

                    'hdnNetTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(NetAmount),0) [NET AMOUNT] FROM  (select EGN_BSU_ID,EGN_DRIVERTYPE, CASE WHEN EGN_DRIVERTYPE ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN EGN_DRIVERTYPE ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN EGN_DRIVERTYPE ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END [NetAmount]  ,EGN_EMP_ID,EGN_TRDATE from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0 and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0   group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE) Net ", "OASIS_TRANSPORTConnectionString")
                    hdnNetTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(NetAmount),0) [NET AMOUNT] FROM  (select EGN_BSU_ID,EGN_DRIVERTYPE,CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1 THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 72 ELSE 0  END  ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 150 ELSE 0 END END END END [NetAmount],EGN_EMP_ID,EGN_TRDATE from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0 and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0   group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE) Net ", "OASIS_TRANSPORTConnectionString")
                    hdnNesTotal.Value = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1 THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END end [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0 and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and egN_emp_id<>0   and EGN_DELETED=0 and EGN_DRIVERTYPE=1 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)NES  ", "OASIS_TRANSPORTConnectionString")
                    hdnVillaTotal.Value = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 and EGN_DRIVERTYPE=2 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")
                    hdnIITTotal.Value = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_DELETED=0 and EGN_DRIVERTYPE=3 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)IIT  ", "OASIS_TRANSPORTConnectionString")
                    hdnOutTotal.Value = Mainclass.getDataValue("select isnull(sum(EGN_OUTSTATION),0) from (select MAX(EGN_OUTSTATION*25)  [EGN_OUTSTATION] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and  LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0  group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OUTSTATION ", "OASIS_TRANSPORTConnectionString")
                    hdnOverNight.Value = Mainclass.getDataValue(" select isnull(sum(EGN_OVERNIGHTSTAY),0) from (select case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end  [EGN_OVERNIGHTSTAY] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 and left(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OVERNIGHTSTAY ", "OASIS_TRANSPORTConnectionString")

                    lblbatchstr.Text = p_Modifyid
                    txtBatchNo.Text = "New"
                    txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
                    lblTrDate.Text = Now.ToString("dd/MMM/yyyy")
                    h_EntryId.Value = 0
                    fillGridView(grdInvoiceDetails, "select replace(convert(varchar(30), EGN_TRDATE, 106),' ','/') [DATE],sum(cast(EGN_TTOTAL as numeric(12,2)))[Total_Hrs],max(case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA' else case when EGN_DRIVERTYPE=3 then 'IIT'  end end end)  DriverType,max(DRIVER)DRIVER,max(DRIVER_EMPNO) DRIVER_EMPNO,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN max(EGN_DRIVERTYPE) ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END EGN_AMOUNT, case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end  EGN_OVERNIGHTSTAY,MAX(EGN_OUTSTATION*25)  [EGN_OUTSTATION],MAX(EGN_OUTSTATION*25)+CASE WHEN max(EGN_DRIVERTYPE) ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END+case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end EGN_TOTAL    FROM TPTEXGRATIACHARGESNES  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGN_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID where LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "'  and EGN_EGI_ID =0  and EGN_deleted=0 and egN_emp_id<>0 group by EGN_EMP_ID,EGN_TRDATE,EGN_drivertype")

                Else
                    dt = MainObj.getRecords("SELECT ENI_ID,isnull(ENI_TRNO,'')ENI_TRNO, replace(convert(varchar(30), ENI_DATE, 106),' ','/') [DATE],ENI_NET, ENI_USER_NAME, ENI_POSTED,ENI_REMARKS, ENI_DATE, ENI_POSTING_DATE,ENI_BSU_ID,ENI_STRNO,ENI_DATE,ENI_JHD_DOCNO,isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(ENI_PRGM_DIM_CODE,'0') DAX_ID FROM TPTEXGRATIANES_INV left outer join dbo.VV_DAX_CODES on ID=ENI_PRGM_DIM_CODE  WHERE ENI_ID ='" & p_Modifyid & "' and ENI_BSU_ID='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                    If dt.Rows.Count > 0 Then
                        txtBatchDate.Text = Format(IIf(IsDBNull(dt.Rows(0)("ENI_POSTING_DATE")), Now.Date.ToString, dt.Rows(0)("ENI_POSTING_DATE")), "dd/MMM/yyyy")
                        txtBatchNo.Text = dt.Rows(0)("ENI_TRNO")
                        txtremarks.Text = dt.Rows(0)("ENI_REMARKS")
                        lblbatchstr.Text = dt.Rows(0)("ENI_STRNO")
                        lblTrDate.Text = dt.Rows(0)("ENI_DATE")
                        txtNetTotal.Text = dt.Rows(0)("ENI_NET")
                        txtNesAmount.Text = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1 THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END end  [NetAmount] from TPTEXGRATIACHARGESNES WHERE EGN_EGI_ID='" & p_Modifyid & "' and EGN_DELETED=0 and EGN_DRIVERTYPE=1 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)NES  ", "OASIS_TRANSPORTConnectionString")
                        txtVillaAmount.Text = Mainclass.getDataValue("select isnull(sum(villaAmount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end  [villaAmount] from TPTEXGRATIACHARGESNES WHERE EGN_EGI_ID='" & p_Modifyid & "' and EGN_deleted=0 and EGN_DRIVERTYPE=2 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")
                        txtIITAmount.Text = Mainclass.getDataValue("select isnull(sum(IITAmount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end  [IITAmount] from TPTEXGRATIACHARGESNES WHERE EGN_EGI_ID='" & p_Modifyid & "' and EGN_deleted=0 and EGN_DRIVERTYPE=3 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")
                        txtOutStattion.Text = Mainclass.getDataValue("select isnull(sum(OutStation),0) from (select  MAX(EGN_OUTSTATION*25)  [OutStation] from TPTEXGRATIACHARGESNES WHERE EGN_EGI_ID='" & p_Modifyid & "' and EGN_deleted=0 and EGN_DRIVERTYPE=3 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")
                        txtOverNightStay.Text = Mainclass.getDataValue("select isnull(sum(OverNIghtStay),0) from (select  case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end OverNightStay from TPTEXGRATIACHARGESNES WHERE EGN_EGI_ID='" & p_Modifyid & "' and EGN_deleted=0 and EGN_DRIVERTYPE=3 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")


                        lblJHDDocNo.Text = dt.Rows(0)("ENI_JHD_DOCNO")
                        txtremarks.Enabled = False
                        btnPost.Visible = False
                        lnkBatchDate.Enabled = False
                        txtBatchDate.Enabled = False
                        lblStatus.Text = "Posted"
                        h_EntryId.Value = dt.Rows(0)("ENI_ID")
                        fillGridView(grdInvoiceDetails, "select replace(convert(varchar(30), EGN_TRDATE, 106),' ','/') [DATE],sum(cast(EGN_TTOTAL as numeric(12,2)))[Total_Hrs],max(case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA' else case when EGN_DRIVERTYPE=3 then 'IIT'  end end end)  DriverType,max(DRIVER)DRIVER,max(DRIVER_EMPNO) DRIVER_EMPNO,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN max(EGN_DRIVERTYPE) ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END EGN_TOTAL,0 EGN_AMOUNT,0 EGN_OUTSTATION,0 EGN_OVERNIGHTSTAY  FROM TPTEXGRATIACHARGESNES  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGN_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID where EGN_EGI_ID='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 group by EGN_EMP_ID,EGN_TRDATE")
                        RadDAXCodes.SelectedItem.Text = dt.Rows(0)("DAX_DESCR")
                        RadDAXCodes.SelectedValue = dt.Rows(0)("DAX_ID")
                        RadDAXCodes.Enabled = False
                        showNoRecordsFound()
                    End If
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Property TPTNESExgratiaInvoiceDetails() As DataTable
        Get
            Return ViewState("TPTNESExgratiaInvoiceDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTNESExgratiaInvoiceDetails") = value
        End Set
    End Property

    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTNESExgratiaInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTNESExgratiaInvoiceDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTNESExgratiaInvoiceDetails = mtable
        fillGrdView.DataSource = TPTNESExgratiaInvoiceDetails
        fillGrdView.DataBind()
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
        txtBatchNo.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property EGCFooter() As DataTable
        Get
            Return ViewState("EGCFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EGCFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Sub showNoRecordsFound()
        If Not EGCFooter Is Nothing AndAlso EGCFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoiceDetails.Columns.Count - 2
            grdInvoiceDetails.Rows(0).Cells.Clear()
            grdInvoiceDetails.Rows(0).Cells.Add(New TableCell())
            grdInvoiceDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoiceDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdInvoiceDetails.PageIndexChanging
        grdInvoiceDetails.PageIndex = e.NewPageIndex
        grdInvoiceDetails.DataSource = TPTNESExgratiaInvoiceDetails
        grdInvoiceDetails.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoiceDetails.RowCancelingEdit
        grdInvoiceDetails.ShowFooter = True
        grdInvoiceDetails.EditIndex = -1
        grdInvoiceDetails.DataSource = EGCFooter
        grdInvoiceDetails.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub grdInvoiceDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoiceDetails.RowDataBound
        If ViewState("datamode") <> "add" Then
            grdInvoiceDetails.Columns(0).Visible = False
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        Dim Empnos As String = ""
        Dim chkControl As New HtmlInputCheckBox
        Dim total As Double = 0.0
        Dim gross As Double = 0.0
        Dim salik As Double = 0.0
        Dim net As Double = 0.0

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblEmpnoID As Label = TryCast(gvr.FindControl("lblEmpNo"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmount"), Label)
            If ChkBxItem.Checked = True Then

                Empnos &= IIf(Empnos <> "", "|", "") & lblEmpnoID.Text
                total += Val(lblNet.Text)
            End If
        Next
        'net = (total)
        If Empnos = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(14) As SqlParameter
        Dim pParms1(1) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ENI_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@ENI_STRNO", lblbatchstr.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@ENI_NET", Val(txtVillaAmount.Text), SqlDbType.Float)
        pParms(6) = Mainclass.CreateSqlParameter("@ENI_USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@ENI_REMARKS", txtremarks.Text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@ENI_DATE", lblTrDate.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@ENI_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@ENI_POSTING_DATE", txtBatchDate.Text, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@ENI_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(13) = Mainclass.CreateSqlParameter("@EMPNOS", Empnos, SqlDbType.VarChar)
        pParms(14) = Mainclass.CreateSqlParameter("@ENI_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostExGratiaNESCharges", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(lblbatchstr.Text)
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As String = CType(h_EntryId.Value, String)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTInvoiceExGratiaNES"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ENI_ID", h_EntryId.Value, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@sBSUID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            Dim InvNo As String = ""
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "NES EXGRATIA CHARGES INVOICE"
            params("LPO") = ""
            params("InvNo") = txtBatchNo.Text
            params("InvDate") = txtBatchDate.Text
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            If Session("sBsuid") = "900501" Then
                repSource.ResourceName = "../../Transport/ExtraHiring/Reports/rptTPTInvoiceExGratiaNES.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/Reports/rptTPTInvoiceExGratiaNESBBT.rpt"
            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            ' Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub UpdateAddress()
    End Sub
    Sub calcTotals()
        Dim NeSTotal As Decimal = 0, VillaTotal As Decimal = 0, IITTotal As Decimal = 0, OutStationTotal As Decimal = 0, OverNightStay As Decimal = 0
        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmount"), Label)
            Dim lblOut As Label = TryCast(gvr.FindControl("lblOutStation"), Label)
            Dim lblOVerNight As Label = TryCast(gvr.FindControl("lblOverNightstay"), Label)

            Dim lblDriverType As Label = TryCast(gvr.FindControl("lblDrivertype"), Label)
            If ChkBxItem.Checked = True Then

                If lblDriverType.Text = "NES" Then
                    NesTotal += Val(lblNet.Text)
                ElseIf lblDriverType.Text = "VILLA" Then
                    VillaTotal += Val(lblNet.Text)
                Else
                    IITTotal += Val(lblNet.Text)
                End If
                OutStationTotal += Val(lblOut.Text)
                OverNightStay += Val(lblOVerNight.Text)

            End If
        Next
        txtNesAmount.Text = NeSTotal
        txtVillaAmount.Text = VillaTotal
        txtIITAmount.Text = IITTotal
        txtOutStattion.Text = OutStationTotal
        txtOverNightStay.Text = OverNightStay
        'txtNetTotal.Text = NeSTotal + VillaTotal + IITTotal + OutStationTotal + OverNightStay
        txtNetTotal.Text = NeSTotal + VillaTotal + IITTotal

    End Sub

End Class



