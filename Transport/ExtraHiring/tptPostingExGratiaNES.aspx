﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptPostingExGratiaNES.aspx.vb" Inherits="Transport_ExtraHiring_tptPostingExGratiaNES" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
     .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function onChange() { alert('ok'); }
        function calculate() {
            document.getElementById("<%=txtNetTotal.ClientID %>").value = total;
        }




        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;



            if (chk_state) {

                //                document.getElementById("<%=txtNetTotal.ClientID %>").value = document.getElementById("<%=hdnNetTotal.ClientID %>").value;
                document.getElementById("<%=txtNesAmount.ClientID%>").value = document.getElementById("<%=hdnnesTotal.ClientID%>").value;
                document.getElementById("<%=txtVillaAmount.ClientID%>").value = document.getElementById("<%=hdnvillaTotal.ClientID%>").value;
                document.getElementById("<%=txtIITAmount.ClientID%>").value = document.getElementById("<%=hdniitTotal.ClientID%>").value;
                document.getElementById("<%=txtOutStattion.ClientID%>").value = document.getElementById("<%=hdnOutTotal.ClientID%>").value;
                document.getElementById("<%=txtOverNightStay.ClientID%>").value = document.getElementById("<%=hdnOverNight.ClientID%>").value;

                document.getElementById("<%=txtNetTotal.ClientID %>").value = parseInt(document.getElementById("<%=hdnnesTotal.ClientID%>").value) + parseInt(document.getElementById("<%=hdnvillaTotal.ClientID%>").value) + parseInt(document.getElementById("<%=hdniitTotal.ClientID%>").value) + parseInt(document.getElementById("<%=hdnOutTotal.ClientID%>").value) + parseInt(document.getElementById("<%=txtOverNightStay.ClientID%>").value);


            }
            else {
                document.getElementById("ctl00_cphMasterpage_txtNetTotal").value = '0.00';
                document.getElementById("ctl00_cphMasterpage_txtNesAmount").value = '0.00';
                document.getElementById("ctl00_cphMasterpage_txtVillaAmount").value = '0.00';
                document.getElementById("ctl00_cphMasterpage_txtIITAmount").value = '0.00';
                document.getElementById("ctl00_cphMasterpage_txtOutStattion").value = '0.00';
                document.getElementById("ctl00_cphMasterpage_txtOverNightStay").value = '0.00';

            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }

        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            ExGratia Charges Posting
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <tr valign="bottom">
                        <td align="center" valign="bottom" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table align="left" cellpadding="5" cellspacing="0" style="width: 100%">
                                <%--  <tr class="subheader_img">
                            <td align="center" colspan="5" valign="middle">
                                <div align="center">
                                    ExGratia Charges Posting</div>
                            </td>
                        </tr>--%>
                                <tr>
                                    <td  width="20%">
                                        <asp:Label ID="lblQUONo" runat="server" Text="Batch Number" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td  align="left" width="30%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="50%">
                                        <asp:TextBox ID="txtBatchNo" runat="server" Enabled="false"  ></asp:TextBox>
                                                </td>
                                                <td align="left" width="50%">
                                        <asp:Label ID="Label4" runat="server" Text="Batch:" Enabled="false"></asp:Label>
                                        <asp:Label ID="lblbatchstr" runat="server" Enabled="false"></asp:Label><br />
                                        <asp:Label ID="Label3" runat="server" Text="Tr.Date:" Enabled="false"></asp:Label>
                                        <asp:Label ID="lblTrDate" runat="server" Enabled="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table> 
                                    </td>
                                    <td  width="20%"><span class="field-label">Batch Date</span>
                                    </td>
                                    <td  align="left"  width="30%">
                                        <asp:TextBox ID="txtBatchDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="lnkBatchDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtBatchDate" PopupButtonID="lnkBatchDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Amount</td>
                                </tr>
                                <tr>
                                    <td  colspan="8">

                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <span class="field-label">Net Amount</span>
                                                </td>
                                                <td>
                                                     <asp:TextBox ID="txtNetTotal" Text="0.00" runat="server" Enabled="false" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Text="NES AMOUNT" Font-Bold="true"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNesAmount" Text="0.00" runat="server" Enabled="false"  ></asp:TextBox>
                                                </td>
                                            
                                                <td>
                                                     <asp:Label ID="Label6" runat="server" Text="VILLA AMT." Font-Bold="true"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVillaAmount" Text="0.00" runat="server" Enabled="false"  ></asp:TextBox>
                                                </td>
                                            
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" Text="IIT AMT." Font-Bold="true"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtIITAmount" Text="0.00" runat="server" Enabled="false"  ></asp:TextBox>
                                                </td>
                                            
                                                <td>
                                                      <asp:Label ID="Label8" runat="server" Text="OUTSTATION" Font-Bold="true"> </asp:Label>
                                                </td>
                                                <td>
                                                     <asp:TextBox ID="txtOutStattion" Text="0.00" runat="server" Enabled="false"  ></asp:TextBox>
                                                </td>
                                           
                                                <td>
                                                    <asp:Label ID="Label9" runat="server" Text="ONS AMT." Font-Bold="true"> </asp:Label>
                                                </td>
                                                <td>
                                                     <asp:TextBox ID="txtOverNightStay" Text="0.00" runat="server" Enabled="false"  ></asp:TextBox>
                                                </td>
                                                
                                            </tr>
                                        </table>
                                    </td>
                                    
                                        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                                        <asp:HiddenField ID="hdnNetTotal" runat="server" />
                                        <asp:HiddenField ID="hdnNesTotal" runat="server" />
                                        <asp:HiddenField ID="hdnVillaTotal" runat="server" />
                                        <asp:HiddenField ID="hdnIITTotal" runat="server" />
                                        <asp:HiddenField ID="hdnOutTotal" runat="server" />
                                        <asp:HiddenField ID="hdnOverNight" runat="server" />

                                 </tr>
                                <tr>

                                    <td  align="left" width="20%">
                                        <asp:Label ID="Label2" runat="server" Text="DAX Code" CssClass="field-label"> </asp:Label>
                                    </td>
                                    <td  align="left" width="30%">

                                        <telerik:RadComboBox ID="RadDAXCodes" Width="325" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td  width="20%">
                                        <asp:Label ID="Label1" runat="server" Text="Document Number" CssClass="field-label"> </asp:Label>
                                    </td>
                                    <td  width="30%">
                                        <span class="text-danger">
                                            <asp:Label ID="lblJHDDocNo" runat="server" Text=""></asp:Label>
                                            <br />
                                            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td  width="20%"><span class="field-label">Remarks</span>
                                    </td>
                                    <td  align="left" colspan="3">
                                        <asp:TextBox ID="txtremarks" runat="server"  TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                    

                                </tr>

                                <tr>
                                    <td colspan="4" align="left" >
                                        <asp:GridView ID="grdInvoiceDetails" runat="server" AutoGenerateColumns="False" PageSize="100"
                                            Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                            DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField HeaderText=" ">
                                                    <HeaderTemplate>
                                                        <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                            value="Check All" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DATE" HeaderText="Date" />
                                                <asp:BoundField DataField="DRIVER_EMPNO" HeaderText="Driver OASIS No." />
                                                <asp:TemplateField HeaderText="Empno" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("DRIVER_EMPNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Driver" HeaderText="Driver" />
                                                <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit Name" />
                                                <asp:BoundField DataField="DriverType" HeaderText="Driver Type" />
                                                <asp:TemplateField HeaderText="DriverType" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDrivertype" runat="server" Text='<%# Eval("DriverType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Total_Hrs" HeaderText="Total Hours" />
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNetAmount" runat="server" Text='<%# Eval("EGN_AMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OutStation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOutStation" runat="server" Text='<%# Eval("EGN_OUTSTATION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OverNightStay">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOverNightstay" runat="server" Text='<%# Eval("EGN_OVERNIGHTSTAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Net Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("EGN_TOTAL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">
                                        <asp:Button ID="btnSAVE" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Save" Visible="False" />
                                        <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Post" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Print" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_QUDGridDelete" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
