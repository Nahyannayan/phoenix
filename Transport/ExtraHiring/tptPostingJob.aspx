<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptPostingJob.aspx.vb" Inherits="Transport_ExtraHiring_tptPostingJob" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function calculate() {
        }



        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>Invoice Posting
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <tr valign="bottom">
                        <td align="center"  valign="bottom" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table align="left" cellpadding="5" cellspacing="0"
                                style="width: 100%">
                                <%-- <tr class="subheader_img">
                            <td align="center" colspan="4" valign="middle">
                               <div align="center">
                                    Invoice Posting</div>
                            </td> 
                             </tr> --%>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblQUONo" runat="server" Text="Batch Number" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td  align="left" width="30%">
                                        <asp:TextBox ID="txtBatchNo" runat="server" Enabled="false" 
                                            ></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Batch Date</span>
                                    </td>
                                    <td  align="left" width="30%">
                                        <asp:TextBox ID="txtBatchDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="lnkBatchDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtBatchDate" PopupButtonID="lnkBatchDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Supplier</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="50%" class="p-0"><asp:TextBox ID="txtSupplierCode" runat="server"  ></asp:TextBox></td>
                                                <td align="left" width="50%" class="p-0"><asp:TextBox ID="txtSupplier" runat="server"  ></asp:TextBox></td>
                                            </tr>
                                        </table>
                                        
                                        <asp:Label ID="lblLoc" runat="server" Text=""></asp:Label>


                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Business Category</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <telerik:RadComboBox ID="RadDAXCodes" Width="100%" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>




                                    </td>
                                </tr>

                                <%--<tr id="trVAT" visible="false"  >
                            <td >
                                VAT Code
                            </td>
                            <td  align="left">
                                
                                <telerik:RadComboBox ID="radVat" Width="350" runat="server"   AutoPostBack="true">
                </telerik:RadComboBox>
                                
                                
                            </td>
                            <td >
                            Emirate
                            </td>
                            
                            <td  colspan ="5" >
                            <telerik:RadComboBox ID="radEmirate" Width="150" runat="server">
                </telerik:RadComboBox>
                            
                            
                            
                                
                            </td>
                        </tr>--%>



                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Address</span>
                                    </td>
                                    <td  align="left" width="30%">
                                        <asp:TextBox ID="txtAddress" runat="server" 
                                            TextMode="MultiLine" ></asp:TextBox>

                                    </td>
                                    <td  align="left" width="20%"><span class="field-label">Document No.</span>
                                      
                                    </td>
                                    <td  align="left" width="30%">
                                        <span class="text-danger">
                                            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></span>
                                    </td>


                                </tr>



                                <tr>
                                    <td colspan="4" align="left" >
                                        <asp:GridView ID="grdInvoiceDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="True" CaptionAlign="Top"  DataKeyNames="ID" AllowPaging="True">
                                            <Columns>
                                                <asp:BoundField DataField="batchNo" HeaderText="Batch Number" />
                                                <asp:BoundField DataField="BatchDate" HeaderText="Batch Date" />
                                                <asp:BoundField DataField="JobNo" HeaderText="Job Number" />
                                                <asp:BoundField DataField="Client" HeaderText="Client" />
                                                <asp:BoundField DataField="Amount" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" />

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                                        <asp:Label ID="lblBatchNo" runat="server" Text='<%# Eval("batchNo") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkView" runat="server" Text="View" OnClick="lnkView_CLick"></asp:LinkButton>
                                                        <asp:Label ID="lblJobNo" runat="server" Text='<%# Eval("JobNo") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>

                            <table width="100%">
                                 <tr>
                                    <td align="left" width="35%"></td>
                                    <td align="right" width="30%">
                                        <asp:Label ID="Label2" runat="server" Text="Total Amount" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left"  width="20%">

                                        <asp:TextBox ID="txtTotal" ReadOnly="true" runat="server"  width="100%"  Style="text-align: right" ></asp:TextBox>
                                    </td>

                                    <td align="left"  width="15%">


                                    </td>
                                </tr>

                                <tr>
                                     <td></td>
                                    <td align="right"  >
                                        <asp:Label ID="Label4" runat="server" Text="VAT Amount" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtVatAmount" runat="server" width="100%" Style="text-align: right" ></asp:TextBox></td>
                                    <td align="left">
                                        <asp:Label ID="lblTaxCode" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                     <td ></td>
                                    <td align="right"  >
                                        <asp:Label ID="Label5" runat="server" Text="Net Amount" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNetAmount" runat="server" width="100%"  Style="text-align: right" ></asp:TextBox>
                                     </td>
                                    <td align="left">
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">

                                        <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button" Text="Post" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                                        <asp:Button ID="btnEmail" runat="server" CausesValidation="False" CssClass="button" Text="Email" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Batch ?');" />
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_QUDGridDelete" runat="server" />
                                        <asp:HiddenField ID="h_VATPerc" runat="server" />
                                        <asp:HiddenField ID="h_location" runat="server" />
                                        <asp:HiddenField ID="h_taxCode" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
