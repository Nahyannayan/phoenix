﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Transport_tptContractRate
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200120" And ViewState("MainMnu_code") <> "T200130" And ViewState("MainMnu_code") <> "T200140") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                hGridRefresh.Value = 0
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    'UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
            If hGridRefresh.Value = 1 Then
                'grdBO.DataSource = BOFooter
                'grdBO.DataBind()
                hGridRefresh.Value = 0
                showNoRecordsFound()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        txtCnh_Noshow.Attributes.Add("onkeypress", " return Numeric_Only();")

        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            'pnlJobOrder.Visible = False
        ElseIf mode = "edit" Then
            mDisable = False
            'pnlJobOrder.Visible = True
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        txtCnh_Date.Enabled = EditAllowed
        txtCnh_Effective_Date.Enabled = EditAllowed
        txtCnh_Noshow.Enabled = EditAllowed
        txtCnh_Print.Enabled = EditAllowed

        'imgClient.Enabled = EditAllowed And mode = "add"
        ImgVehType.Enabled = EditAllowed And mode = "add"
        ImgSeason.Enabled = EditAllowed And mode = "add"
        imgbtnAddSalesManID.Enabled = EditAllowed
        imgCnh_Date.Enabled = EditAllowed
        imgCnh_Effective_Date.Enabled = EditAllowed


        grdRates.Columns(6).Visible = Not mDisable
        grdRates.Columns(7).Visible = Not mDisable
        grdRates.ShowFooter = Not mDisable

        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnCopy.Visible = mDisable
        btnDelete.Visible = mDisable
        btnPrint.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                'strSql.Append("select TPTCONTRACT_H.*, isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') SalesManName, replace(convert(varchar(30), Cnh_Date, 106),' ','/') CnhDATE, ")
                strSql.Append("select CNH_ID, CNH_CLIENT_ID, CNH_DATE, CNH_ID_FROM, CNH_SALESMAN_ID, CNH_EFFECTIVE_DATE, CNH_REFERENCE, CNH_PRINT, CNH_SYSDATE, CNH_NOSHOW, CNH_NUMBER, CNH_NUMBERSTR, CNH_VTYP_ID, CNH_SEAS_ID, isnull(CNH_DESCRIPTion,'')CNH_DESCRIPTION, CNH_TRL_ID")
                strSql.Append(", isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') SalesManName, replace(convert(varchar(30), Cnh_Date, 106),' ','/') CnhDATE, ")
                strSql.Append("replace(convert(varchar(30), CNH_EFFECTIVE_DATE, 106),' ','/') CnhEffectiveDATE, ISNULL(ACT_NAME,'Standard Rate') ClientName ")
                strSql.Append(",VTYP_DESCRIPTION [VEHTYPE],SEAS_NAME [SEASON] ")
                strSql.Append("from TPTCONTRACT_H left outer join OASIS.dbo.EMPLOYEE_M on EMP_ID=CNH_SALESMAN_ID ")
                'strSql.Append("LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=CNH_Client_id ")
                strSql.Append("LEFT OUTER JOIN(select ACT_ID, ACT_NAME FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M union SELECT '00000000', 'STANDARD CONTRACT' UNION SELECT '00000001', 'THIRD PARTY 1 SET ' UNION SELECT '00000002', 'THIRD PARTY 2 SET') a on CNH_CLIENT_ID=ACT_ID ")
                strSql.Append("LEFT OUTER JOIN  tptSeason on seas_id=CNH_SEAS_ID left join VEHTYPE_M on VTYP_ID=CNH_VTYP_ID ")
                strSql.Append("where CNH_ID=" & h_EntryId.Value)

                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtCnh_Id.Text = dt.Rows(0)("Cnh_numberstr")
                    txtCnh_Date.Text = dt.Rows(0)("CnhDATE")
                    txtCnh_Effective_Date.Text = dt.Rows(0)("CnhEffectiveDATE")
                    'txtClientName.Text = dt.Rows(0)("ClientName")
                    txtCnh_Noshow.Text = dt.Rows(0)("Cnh_Noshow")
                    txtCnh_Print.Text = dt.Rows(0)("Cnh_Print")
                    'txtClientCode.Text = dt.Rows(0)("Cnh_Client_ID")
                    'txtClientName.Text = dt.Rows(0)("ClientName")
                    DisplayAccouneCodes(dt.Rows(0)("Cnh_Client_ID"))
                    txtSalesMan.Text = dt.Rows(0)("SalesManName")
                    hdnSalesMan.Value = dt.Rows(0)("Cnh_SalesMan_id")
                    txt_Season_Descr.Text = dt.Rows(0)("SEASON")
                    txt_veh_CatDescr.Text = dt.Rows(0)("VEHTYPE")
                    txtDescription.Text = dt.Rows(0)("CNH_DESCRIPTION")

                    hdnCNH_TRL_ID.Value = dt.Rows(0)("CNH_TRL_ID")
                    hdnCNH_CLIENT_ID.Value = dt.Rows(0)("Cnh_Client_ID")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
            fillGridView(grdRates, "select CND_ID, PROD_NAME, CND_CNH_ID, CND_PROD_ID, CND_RATE, CND_HRS, CND_KM from TPTCONTRACT_D inner JOIN TPTHiringProduct on PROD_ID=CND_PROD_ID where CND_CNH_ID=" & h_EntryId.Value & " order by Cnd_Id")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub DisplayAccouneCodes(ByVal ActIDs As String)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select ACT_ID,ACT_NAME  from oasisfin..ACCOUNTS_M  where ACT_ID in(select ID  from oasis.dbo.fnSplitMe('" & ActIDs & "',','))").Tables(0)
        rptImages.DataSource = dt
        rptImages.DataBind()

    End Sub


    Private Property TPTRates() As DataTable
        Get
            Return ViewState("TPTRates")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTRates") = value
        End Set
    End Property

    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTRates = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTRates)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTRates = mtable
        fillGrdView.DataSource = TPTRates
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub

    Private Sub showNoRecordsFound()
        If TPTRates.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdRates.Columns.Count - 2
            grdRates.Rows(0).Cells.Clear()
            grdRates.Rows(0).Cells.Add(New TableCell())
            grdRates.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdRates.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub
    Sub ClearDetails()
        'txtCnh_Id.Visible = False
        txtCnh_Date.Text = Now.ToString("dd/MMM/yyyy")
        txtCnh_Effective_Date.Text = Now.ToString("dd/MMM/yyyy")
        txtCnh_Id.Text = ""
        'txtClientName.Text = ""
        txtCnh_Noshow.Text = "0"
        txtCnh_Print.Text = ""
        'txtClientCode.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdRates.DataSource = TPTRates
        grdRates.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdRates.DataSource = TPTRates
        grdRates.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            If h_EntryId.Value = 1 Then
                lblError.Text = "contract delete standard contract"
            ElseIf SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM TPTHiring inner JOIN TPTHiringInvoice on TRN_no=INV_TRN_NO inner JOIN TPTCONTRACT_H on CNH_ID=inv_cn_id WHERE CNH_ID=" & h_EntryId.Value & " and TPT_JobType=1") > 0 Then
                lblError.Text = "contract in use, unable to delete"
            Else
                Dim objConn As New SqlConnection(connectionString)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim sqlStr As String = "delete from TPTCONTRACT_H where CNH_ID=@TRNO"
                    sqlStr &= " delete from TPTCONTRACT_D where CND_CNH_ID=@TRNO"
                    Dim pParms(1) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@TRNO", h_EntryId.Value, SqlDbType.Int, True)
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                    stTrans.Commit()
                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                    Exit Sub
                Finally
                    objConn.Close()
                End Try
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                'If flagAudit <> 0 Then
                '    Throw New ArgumentException("Could not process your request")
                'End If
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        If txtCnh_Date.Text.Trim.Length = 0 Then strMandatory.Append("Date,")
        'If hTPT_Client_ID.Value = "0" Then strMandatory.Append("Client's Name,")
        If txtCnh_Effective_Date.Text.Trim.Length = 0 Then strMandatory.Append("Effective Date,")
        If Not IsDate(txtCnh_Date.Text) Then strError.Append("invalid date,")
        If Not IsDate(txtCnh_Effective_Date.Text) Then strError.Append("invalid effective date,")
        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(9) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@Cnh_id", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@Cnh_date", txtCnh_Date.Text, SqlDbType.SmallDateTime)
        pParms(3) = Mainclass.CreateSqlParameter("@Cnh_Effective_Date", txtCnh_Effective_Date.Text, SqlDbType.SmallDateTime)
        pParms(4) = Mainclass.CreateSqlParameter("@Cnh_Client_id", hdnCNH_CLIENT_ID.Value, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@Cnh_Print", txtCnh_Print.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@Cnh_Noshow", txtCnh_Noshow.Text, SqlDbType.Decimal)
        pParms(7) = Mainclass.CreateSqlParameter("@Cnh_SalesMan_Id", hdnSalesMan.Value, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@Cnh_Reference", "", SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@Cnh_id_From", 0, SqlDbType.Int)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTContract_h", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            Dim RowCount As Integer
            For RowCount = 0 To TPTRates.Rows.Count - 1
                Dim iParms(6) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                Dim rowVersion As System.Data.DataRowVersion = DataRowVersion.Default
                If TPTRates.Rows(RowCount).RowState = 8 Then
                    rowState = -1
                    rowVersion = DataRowVersion.Original
                End If
                iParms(1) = Mainclass.CreateSqlParameter("@Cnd_Id", IIf(addMode, 0, TPTRates.Rows(RowCount)("Cnd_Id", rowVersion)), SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@Cnd_Cnh_Id", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@CND_HRS", TPTRates.Rows(RowCount)("Cnd_hrs", rowVersion), SqlDbType.Decimal)
                iParms(4) = Mainclass.CreateSqlParameter("@Cnd_Km", TPTRates.Rows(RowCount)("Cnd_km", rowVersion), SqlDbType.Decimal)
                iParms(5) = Mainclass.CreateSqlParameter("@Cnd_RATE", TPTRates.Rows(RowCount)("Cnd_RATE", rowVersion), SqlDbType.Decimal)
                iParms(6) = Mainclass.CreateSqlParameter("@Cnd_PROD_ID", TPTRates.Rows(RowCount)("Cnd_PROD_ID", rowVersion), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTContract_d", iParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next
            If hGridDelete.Value <> "0" Then
                Dim deleteId() As String = hGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@Cnd_Id", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@Cnd_Cnh_Id", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from TPTCONTRACT_D where Cnd_Cnh_Id=@Cnd_Cnh_Id and Cnd_Id=@Cnd_Id", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If
            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub grdRates_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdRates.RowCancelingEdit
        grdRates.ShowFooter = True
        grdRates.EditIndex = -1
        grdRates.DataSource = TPTRates
        grdRates.DataBind()
    End Sub
    Protected Sub grdRates_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdRates.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtCnd_Rate As TextBox = grdRates.FooterRow.FindControl("txtCnd_Rate")
            Dim txtCnd_Hrs As TextBox = grdRates.FooterRow.FindControl("txtCnd_Hrs")
            Dim txtCnd_Km As TextBox = grdRates.FooterRow.FindControl("txtCnd_Km")
            Dim ddlProducts As DropDownList = grdRates.FooterRow.FindControl("ddlProducts")
            lblError.Text = ValidTPTRates(txtCnd_Rate.Text.Trim, txtCnd_Hrs.Text.Trim, txtCnd_Km.Text.Trim)
            If ddlProducts.SelectedIndex = 0 Then lblError.Text &= " Select Product"
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            If TPTRates.Rows(0)(1) = -1 Then TPTRates.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = TPTRates.NewRow
            mrow("Cnd_id") = 0
            mrow("Prod_Name") = ddlProducts.SelectedItem.Text
            mrow("Cnd_PROD_ID") = ddlProducts.SelectedValue
            mrow("Cnd_Rate") = txtCnd_Rate.Text
            mrow("Cnd_Hrs") = txtCnd_Hrs.Text
            mrow("Cnd_Km") = txtCnd_Km.Text
            TPTRates.Rows.Add(mrow)
            grdRates.EditIndex = -1
            grdRates.DataSource = TPTRates
            grdRates.DataBind()
            showNoRecordsFound()
        End If
    End Sub
    Protected Sub grdRates_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRates.RowDataBound
        If (grdRates.ShowFooter And e.Row.RowType = DataControlRowType.Footer) Or (e.Row.RowType = DataControlRowType.DataRow And grdRates.EditIndex > -1) Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim ddlProducts As DropDownList = e.Row.FindControl("ddlProducts")
            If Not ddlProducts Is Nothing Then
                Dim hProducts As HiddenField = e.Row.FindControl("hProducts")
                fillDropdown(ddlProducts, "select 0 ddlId, 'Select Product' ddlDescr union select prod_id, prod_name from TPTHiringProduct", "ddlDescr", "ddlId", False)
                ddlProducts.SelectedValue = hProducts.Value
            End If
            Dim txtCnd_Rate As TextBox = e.Row.FindControl("txtCnd_Rate")
            txtCnd_Rate.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
            Dim txtCnd_Hrs As TextBox = e.Row.FindControl("txtCnd_Hrs")
            txtCnd_Hrs.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
            Dim txtCnd_Km As TextBox = e.Row.FindControl("txtCnd_Km")
            txtCnd_Km.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
        End If
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub
    Protected Sub grdRates_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdRates.RowDeleting
        Dim mRow() As DataRow = TPTRates.Select("ID=" & grdRates.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= ";" & mRow(0)("Cnd_Id")
            TPTRates.Select("ID=" & grdRates.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            TPTRates.AcceptChanges()
        End If
        If TPTRates.Rows.Count = 0 Then
            TPTRates.Rows.Add(TPTRates.NewRow())
            TPTRates.Rows(0)(1) = -1
        End If

        grdRates.DataSource = TPTRates
        grdRates.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub grdRates_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdRates.RowEditing
        Try
            grdRates.ShowFooter = False
            grdRates.EditIndex = e.NewEditIndex
            grdRates.DataSource = TPTRates
            grdRates.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub grdRates_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdRates.RowUpdating
        Dim s As String = grdRates.DataKeys(e.RowIndex).Value.ToString()
        Dim lblCnd_ID As Label = grdRates.Rows(e.RowIndex).FindControl("lblCnd_ID")
        Dim txtCnd_Rate As TextBox = grdRates.Rows(e.RowIndex).FindControl("txtCnd_Rate")
        Dim txtCnd_Hrs As TextBox = grdRates.Rows(e.RowIndex).FindControl("txtCnd_Hrs")
        Dim txtCnd_Km As TextBox = grdRates.Rows(e.RowIndex).FindControl("txtCnd_Km")
        Dim ddlProducts As DropDownList = grdRates.Rows(e.RowIndex).FindControl("ddlProducts")

        lblError.Text = ValidTPTRates(txtCnd_Rate.Text.Trim, txtCnd_Hrs.Text.Trim, txtCnd_Km.Text.Trim)
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If
        Dim mrow As DataRow
        mrow = TPTRates.Select("ID=" & s)(0)
        mrow("Cnd_ID") = lblCnd_ID.Text
        mrow("Prod_Name") = ddlProducts.SelectedItem.Text
        mrow("Cnd_PROD_ID") = ddlProducts.SelectedValue
        mrow("Cnd_Rate") = txtCnd_Rate.Text
        mrow("Cnd_Hrs") = txtCnd_Hrs.Text
        mrow("Cnd_Km") = txtCnd_Km.Text
        grdRates.EditIndex = -1
        grdRates.ShowFooter = True
        grdRates.DataSource = TPTRates
        grdRates.DataBind()
    End Sub
    Function ValidTPTRates(ByVal txtCnd_Rate As String, ByVal txtCnd_Hrs As String, ByVal txtCnd_Km As String) As String
        ValidTPTRates = ""
        If txtCnd_Rate.Length = 0 Then ValidTPTRates &= "Rate"
        If txtCnd_Hrs.Length = 0 Then ValidTPTRates &= IIf(ValidTPTRates.Length = 0, "", ",") & "Qty"
        If txtCnd_Km.Length = 0 Then ValidTPTRates &= IIf(ValidTPTRates.Length = 0, "", ",") & "Km"
    End Function

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        h_EntryId.Value = 0
        ClearDetails()
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTContract"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@CNH_Id", h_EntryId.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@Client_Id", ClientID, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "Transport Hiring Quotation"
            
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTContract.rpt"

            Dim subRep(0) As MyReportClass
            subRep(0) = New MyReportClass
            Dim subcmd1 As New SqlCommand

            subcmd1.CommandText = "rptTPTContractDetails"
            Dim sqlParam1(0) As SqlParameter
            sqlParam1(0) = Mainclass.CreateSqlParameter("@CND_CNH_ID", h_EntryId.Value, SqlDbType.Int)
            subcmd1.Parameters.Add(sqlParam1(0))
            subcmd1.CommandType = Data.CommandType.StoredProcedure
            subcmd1.Connection = New SqlConnection(str_conn)
            subRep(0).Command = subcmd1
            subRep(0).ResourceName = "wBTA_Sector"
            repSource.IncludeBSUImage = True
            repSource.SubReport = subRep
            Session("ReportSource") = repSource
            ' Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub btnDisable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisable.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "Update TPTCONTRACT_H set cnh_noshow=1 where CNH_CLIENT_ID=@CLIETID and CNH_TRL_ID=@TRL_ID"

                Dim pParms(2) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@CLIETID", hdnCNH_CLIENT_ID.Value, SqlDbType.VarChar, True)
                pParms(2) = Mainclass.CreateSqlParameter("@TRL_ID", hdnCNH_TRL_ID.Value, SqlDbType.Int, True)

                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub
End Class
