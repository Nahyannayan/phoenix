﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
Imports System.Collections
Partial Class Transport_ExtraHiring_rptExgratiaDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                txtFDate.Text = Now.ToString("dd/MMM/yyyy")
                txtTDate.Text = Now.ToString("dd/MMM/yyyy")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200280" And ViewState("MainMnu_code") <> "T200300" And ViewState("MainMnu_code") <> "T200301" And ViewState("MainMnu_code") <> "T200330") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                If ViewState("MainMnu_code") <> "T200280" Then
                    Purpose.Visible = False
                    Bsuid.Visible = False
                Else
                    trBSUnit.Visible = False
                End If
                If ViewState("MainMnu_code") = "T200300" Then
                    chkSummary.Visible = True
                Else
                    chkSummary.Visible = False
                End If


                BindBsu()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Else
        End If
    End Sub
    Sub BindBsu()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_Sql1 As String = "SELECT  '' as VehType union all select 'BB' VehType UNION ALL SELECT 'MB' VehType UNION ALL SELECT 'VAN' VehType order by VehType "
        Dim str_Sql As String = "SELECT  '---ALL---' as TMS_DESCR union all select  TMS_DESCR from VW_TPT_PURPOSE order by TMS_DESCR "
        Dim BSUParms(2) As SqlParameter
        BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
        BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        ddlBsu.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForExGratia", BSUParms)
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
        ddlBsu1.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForExGratia", BSUParms)
        ddlBsu1.DataTextField = "BSU_NAME"
        ddlBsu1.DataValueField = "BSU_ID"
        ddlBsu1.DataBind()
        ddlPurpose.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql, BSUParms)
        ddlPurpose.DataTextField = "TMS_DESCR"
        ddlPurpose.DataValueField = "TMS_DESCR"
        ddlPurpose.DataBind()
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            callReport()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region
    Sub callReport()
        Try
            If ViewState("MainMnu_code") = "T200280" Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
                Dim cmd As New SqlCommand
                cmd.CommandText = "rptExGratiaDetails"
                Dim sqlParam(3) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@Fromdate", txtFDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@Todate", txtTDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = Mainclass.CreateSqlParameter("@BSUId", ddlBsu.SelectedValue, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(2))
                sqlParam(3) = Mainclass.CreateSqlParameter("@EGC_PURPOSE", ddlPurpose.SelectedValue, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(3))
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("Businessunit") = ddlBsu.SelectedItem.Text
                params("month") = Month(txtFDate.Text)
                params("DateRange") = txtFDate.Text & " To " & txtTDate.Text
                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptExgratiaDetails.rpt"
                params("reportCaption") = "ExGratia Details"
                Dim subRep(0) As MyReportClass
                Dim sqlParam1(1) As SqlParameter
                repSource.IncludeBSUImage = True
                repSource.SubReport = subRep
                Session("ReportSource") = repSource
            ElseIf ViewState("MainMnu_code") = "T200301" Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
                Dim cmd As New SqlCommand
                cmd.CommandText = "rptExGratiaDetailsNES"
                Dim sqlParam(3) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@Fromdate", txtFDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@Todate", txtTDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = Mainclass.CreateSqlParameter("@BSUId", ddlBsu1.SelectedValue, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(2))
                sqlParam(3) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(3))


                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("Businessunit") = ddlBsu1.SelectedItem.Text
                params("month") = Month(txtFDate.Text)
                params("DateRange") = txtFDate.Text & " To " & txtTDate.Text
                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptExgratiaDetailsNES.rpt"
                params("reportCaption") = "ExGratia Details (NES/VILLA/IIT)"
                Dim subRep(0) As MyReportClass
                Dim sqlParam1(1) As SqlParameter
                repSource.IncludeBSUImage = True
                repSource.SubReport = subRep
                Session("ReportSource") = repSource
            ElseIf ViewState("MainMnu_code") = "T200330" Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
                Dim cmd As New SqlCommand
                cmd.CommandText = "rptExGratiaDetailsSDET"
                Dim sqlParam(3) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@Fromdate", txtFDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@Todate", txtTDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = Mainclass.CreateSqlParameter("@BSUId", ddlBsu1.SelectedValue, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(2))
                sqlParam(3) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(3))
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("Businessunit") = ddlBsu1.SelectedItem.Text
                params("month") = Month(txtFDate.Text)
                params("DateRange") = txtFDate.Text & " To " & txtTDate.Text
                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptExgratiaDetailsSDET.rpt"
                params("reportCaption") = "SDET - ExGratia Details"
                Dim subRep(0) As MyReportClass
                Dim sqlParam1(1) As SqlParameter
                repSource.IncludeBSUImage = True
                repSource.SubReport = subRep
                Session("ReportSource") = repSource
            Else
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
                Dim cmd As New SqlCommand
                If chkSummary.Checked = True Then
                    cmd.CommandText = "EXGRATIACONSOLIDATEDSUMMARY"
                Else
                    cmd.CommandText = "EXGRATIACONSOLIDATED"
                End If
                Dim sqlParam(3) As SqlParameter
                Dim SelBsuIds As String = ""
                sqlParam(0) = Mainclass.CreateSqlParameter("@EGC_FDATE", txtFDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@EGC_TDATE", txtTDate.Text, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                sqlParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBsu1.SelectedValue, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(2))
                sqlParam(3) = Mainclass.CreateSqlParameter("@LoginBSU_ID", Session("sbsuid"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(3))

                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                If ddlBsu1.SelectedValue = 0 Then
                    params("Businessunit") = "ALL SCHOOLS"
                Else
                    params("Businessunit") = ddlBsu1.SelectedItem.Text
                End If

                params("month") = Month(txtFDate.Text)
                params("DateRange") = txtFDate.Text & " To " & txtTDate.Text
                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params

                If chkSummary.Checked = True Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptEXGRATIACONSOLIDATEDSUMMARY.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptEXGRATIACONSOLIDATED.rpt"
                End If
                params("reportCaption") = "ExGratia Details" & txtFDate.Text & " To " & txtTDate.Text
                Dim subRep(0) As MyReportClass
                Dim sqlParam1(1) As SqlParameter
                repSource.IncludeBSUImage = True
                repSource.SubReport = subRep
                Session("ReportSource") = repSource
            End If
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub CallReport1()
        Dim param As New Hashtable
        param.Add("loginbsu", Session("sbsuid"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If ViewState("MainMnu_code") = "T200155" Then
            ElseIf ViewState("MainMnu_code") = "T200155" Then
                .reportPath = Server.MapPath("../Rpt/rptBuslistContacts.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
End Class


