﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.Collections.Generic


Partial Class Transport_ExtraHiring_tptSDETProjectMaster
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200107") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If

                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    'UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        'txtAmount.Attributes.Add("onkeypress", " return Numeric_Only();")
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        'txtAmount.Enabled = EditAllowed
        txtproject.Enabled = EditAllowed
        txtFromdate.Enabled = EditAllowed
        txtTodate.Enabled = EditAllowed
        txtAmount.Enabled = EditAllowed
        txtProjoectDetails.Enabled = EditAllowed
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = EditAllowed
        grdStaff.Enabled = EditAllowed
        btnCancel.Visible = Not mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select SP_ID , SP_DESCR ,isnull(SP_MAX_AMOUNT,0)SP_MAX_AMOUNT,isnull(SP_FROMDATE,'')SP_FROMDATE,isnull(SP_TODATE,'')SP_TODATE,isnull(SP_STATUS,'O')SP_STATUS,isnull(SP_PRJ_DETAILS,'')SP_PRJ_DETAILS from SDETPROJECTMASTER where SP_ID=" & h_EntryId.Value)
                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtproject.Text = dt.Rows(0)("SP_DESCR")
                    txtFromdate.Text = dt.Rows(0)("SP_FROMDATE")
                    txtTodate.Text = dt.Rows(0)("SP_TODATE")
                    txtAmount.Text = dt.Rows(0)("SP_MAX_AMOUNT")
                    txtProjoectDetails.Text = dt.Rows(0)("SP_PRJ_DETAILS")
                    If dt.Rows(0)("SP_STATUS") = "C" Then
                        btnClose.Visible = False
                    Else
                        btnClose.Visible = True
                    End If
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
            fillGridView(grdStaff, "select SPS_ID,SPS_SP_ID,SPS_EMP_ID,SPS_AMOUNT,SPS_DELETED,DRIVER STAFFNAME from SDETPROJECTMASTER_STAFFLIST inner join VW_DRIVER_SDE with(nolock)on DRIVER_EMPID=SPS_EMP_ID where SPS_SP_ID=" & h_EntryId.Value & " and isnull(SPS_DELETED,0)=0 order by SPS_Id")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Property TPTSDETSTAFF() As DataTable
        Get
            Return ViewState("TPTSDETSTAFF")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTSDETSTAFF") = value
        End Set
    End Property
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTSDETSTAFF = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTSDETSTAFF)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTSDETSTAFF = mtable
        fillGrdView.DataSource = TPTSDETSTAFF
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub
    Private Sub showNoRecordsFound()
        If TPTSDETSTAFF.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdStaff.Columns.Count - 2
            grdStaff.Rows(0).Cells.Clear()
            grdStaff.Rows(0).Cells.Add(New TableCell())
            grdStaff.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdStaff.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub
    Sub ClearDetails()
        txtproject.Text = ""
        txtAmount.Text = "0.0"
        txtFromdate.Text = Now.ToString("dd/MMM/yyyy")
        txtTodate.Text = Now.ToString("dd/MMM/yyyy")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'TPTSDETSTAFF.Clear()
        ''TPTSDETSTAFF = Nothing
        'grdStaff.DataSource = TPTSDETSTAFF
        'grdStaff.DataBind()
        'showNoRecordsFound()
        fillGridView(grdStaff, "select SPS_ID,SPS_SP_ID,SPS_EMP_ID,SPS_AMOUNT,SPS_DELETED,DRIVER STAFFNAME from SDETPROJECTMASTER_STAFFLIST inner join VW_DRIVER_SDE with(nolock)on DRIVER_EMPID=SPS_EMP_ID where SPS_SP_ID=0 and isnull(SPS_DELETED,0)=0 order by SPS_Id")
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdStaff.DataSource = TPTSDETSTAFF
        grdStaff.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            SaveData("D")
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData(IIf(h_EntryId.Value > 0, "U", "A"))

    End Sub

    Private Sub SaveData(ByVal Action As String)
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        Dim AddDelete As String = ""

        If Action = "A" Then
            AddDelete = "Add"
        ElseIf Action = "U" Then
            AddDelete = "Update"
        Else
            AddDelete = "Delete"
        End If
        If txtproject.Text.Trim.Length = 0 Then strMandatory.Append("Project Name,")
        h_GridTotal.Value = Convert.ToDouble(TPTSDETSTAFF.Compute("sum(SPS_AMOUNT)", "")).ToString("######0.00")

        If h_GridTotal.Value > Val(txtAmount.Text) Then strMandatory.Append("Staff AMount is exceeding the Project Amount,")
        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(8) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@SP_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@SP_DESCR", txtproject.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@SP_FROMDATE", txtFromdate.Text, SqlDbType.DateTime)
        pParms(4) = Mainclass.CreateSqlParameter("@SP_AMOUNT", txtAmount.Text, SqlDbType.Decimal)
        pParms(5) = Mainclass.CreateSqlParameter("@SP_TODATE", txtTodate.Text, SqlDbType.DateTime)
        pParms(6) = Mainclass.CreateSqlParameter("@SP_USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@SP_ACTION", Action, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@SP_PRJ_DETAILS", txtProjoectDetails.Text, SqlDbType.VarChar)


        'pParms(8) = New SqlParameter("@return", SqlDbType.Int, 0, ParameterDirection.ReturnValue)


        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTSDETPROJECT", pParms)
            'Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveTPTSDETPROJECT", pParms)
            If RetVal = "-1" Then
                'If pParms(8).Value = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If


            Dim RowCount As Integer
            For RowCount = 0 To TPTSDETSTAFF.Rows.Count - 1
                Dim iParms(7) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If TPTSDETSTAFF.Rows(RowCount).RowState = 8 Then rowState = -1
                iParms(1) = Mainclass.CreateSqlParameter("@SPS_ID", TPTSDETSTAFF.Rows(RowCount)("SPS_Id"), SqlDbType.Int)
                iParms(2) = Mainclass.CreateSqlParameter("@SPS_SP_ID", rowState, SqlDbType.Int)
                iParms(3) = Mainclass.CreateSqlParameter("@SPS_EMP_ID", TPTSDETSTAFF.Rows(RowCount)("SPS_EMP_Id"), SqlDbType.Int)
                iParms(4) = Mainclass.CreateSqlParameter("@SPS_AMOUNT", TPTSDETSTAFF.Rows(RowCount)("SPS_AMOUNT"), SqlDbType.VarChar)
                iParms(5) = Mainclass.CreateSqlParameter("@SPS_FROMDATE", txtFromdate.Text, SqlDbType.DateTime)
                iParms(6) = Mainclass.CreateSqlParameter("@SPS_TODATE", txtTodate.Text, SqlDbType.DateTime)
                iParms(7) = Mainclass.CreateSqlParameter("@SPS_DELETED", TPTSDETSTAFF.Rows(RowCount)("SPS_DELETED"), SqlDbType.Decimal)

                'Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SDETPROJECT_STAFFLIST", iParms)
                Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SDETPROJECT_STAFFLIST", iParms)

                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub

                End If
            Next

            If hGridDelete.Value <> "0" Then
                Dim deleteId() As String = hGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@Inv_Id", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@Inv_Trn_No", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "update SDETPROJECTMASTER_STAFFLIST set SPS_DELETED=1 WHERE SPS_ID=@Inv_Id and SPS_SP_ID=@Inv_Trn_No", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("SDET PROJECT MASTER", h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub grdStaff_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdStaff.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtAmount As TextBox = grdStaff.FooterRow.FindControl("txtAmount")
            Dim txtStaff As TextBox = grdStaff.FooterRow.FindControl("txtStaff")
            Dim txtEmpID As HiddenField = grdStaff.FooterRow.FindControl("h_emp_id")



            If txtStaff.Text = "" Then lblError.Text &= " Select Staff"
            If txtAmount.Text = "" Then lblError.Text &= " Enter Amount"
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            If TPTSDETSTAFF.Rows(0)(1) = -1 Then TPTSDETSTAFF.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = TPTSDETSTAFF.NewRow
            mrow("SPS_id") = 0
            mrow("STAFFNAME") = txtStaff.Text
            mrow("SPS_EMP_ID") = txtEmpID.Value
            mrow("SPS_AMOUNT") = txtAmount.Text
            TPTSDETSTAFF.Rows.Add(mrow)
            grdStaff.EditIndex = -1
            grdStaff.DataSource = TPTSDETSTAFF
            grdStaff.DataBind()
            showNoRecordsFound()

        Else
            Dim txtAmount As TextBox = grdStaff.FooterRow.FindControl("txtAmount")
            Dim txtStaff As TextBox = grdStaff.FooterRow.FindControl("txtStaff")
            Dim txtEmpID As HiddenField = grdStaff.FooterRow.FindControl("h_emp_id")
            
            'If txtStaff.Text = "" Then lblError.Text &= " Select Staff"
            'If txtAmount.Text = "" Then lblError.Text &= " Enter Amount"

        End If

    End Sub

    Protected Sub grdStaff_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStaff.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim txtAmount As TextBox = e.Row.FindControl("txtAmount")
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim txtAmounT_F As TextBox = e.Row.FindControl("txtAmount")
                Dim txtSTAFF_F As TextBox = e.Row.FindControl("txtStaff")
                Dim imgSubLedger As ImageButton = e.Row.FindControl("imgSubLedger")
                Dim h_emp_Id_F As HiddenField = e.Row.FindControl("h_EMP_id")
                'Dim lblSP_ID As Label = e.Row.FindControl("lblSPS_ID")
                'Dim lblSP_SPS_ID As Label = e.Row.FindControl("lblSPS_SP_ID")
                imgSubLedger.Attributes.Add("OnClick", "javascript:getStaff('" & txtSTAFF_F.ClientID & "','" & h_emp_Id_F.ClientID & "'); return false;")
            End If

            
        End If
    End Sub
    Protected Sub grdStaff_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdStaff.RowDeleting
        Dim mRow() As DataRow = TPTSDETSTAFF.Select("ID=" & grdStaff.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= ";" & mRow(0)("SPS_ID")
            TPTSDETSTAFF.Select("ID=" & grdStaff.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            TPTSDETSTAFF.AcceptChanges()

        End If
        If TPTSDETSTAFF.Rows.Count = 0 Then
            TPTSDETSTAFF.Rows.Add(TPTSDETSTAFF.NewRow())
            TPTSDETSTAFF.Rows(0)(1) = -1
            'txtAMOunt.Text = "0"
        Else
            'txtBillingTotal.Text = TPTSDETSTAFF.Compute("sum(inv_amt)", "")
            h_GridTotal.Value = TPTSDETSTAFF.Compute("sum(SPS_AMOUNT)", "")
        End If
        grdStaff.DataSource = TPTSDETSTAFF
        grdStaff.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdStaff_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdStaff.RowEditing
        Try
            grdStaff.ShowFooter = False
            grdStaff.EditIndex = e.NewEditIndex
            grdStaff.DataSource = TPTSDETSTAFF
            grdStaff.DataBind()
            Dim txtSTAFF_F As TextBox = grdStaff.Rows(e.NewEditIndex).FindControl("txtStaff")
            Dim h_emp_id As HiddenField = grdStaff.Rows(e.NewEditIndex).FindControl("h_emp_id")
            Dim txtAmount_F As TextBox = grdStaff.FooterRow.FindControl("txtAmount")
            Dim imgSubLedger As ImageButton = grdStaff.Rows(e.NewEditIndex).FindControl("imgSubLedger_e")
            imgSubLedger.Visible = True
            imgSubLedger.Attributes.Add("OnClick", "javascript:getStaff('" & txtSTAFF_F.ClientID & "','" & h_emp_id.ClientID & "'); return false;")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdStaff_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdStaff.RowUpdating
        Dim s As String = grdStaff.DataKeys(e.RowIndex).Value.ToString()
        Dim lblSpS_ID As Label = grdStaff.Rows(e.RowIndex).FindControl("lblSPS_ID")
        Dim lblSpS_SP_ID As Label = grdStaff.Rows(e.RowIndex).FindControl("lblSPS_SP_ID")
        Dim txtAmount As TextBox = grdStaff.Rows(e.RowIndex).FindControl("txtAmount")
        Dim txtstaff As TextBox = grdStaff.Rows(e.RowIndex).FindControl("STAFFNAME")
        Dim txtStaffID As HiddenField = grdStaff.Rows(e.RowIndex).FindControl("h_emp_id")
        If txtAmount.Text = "" Then lblError.Text &= " Enter Amount"
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If
        Dim mrow As DataRow
        mrow = TPTSDETSTAFF.Select("ID=" & s)(0)
        mrow("SPS_ID") = lblSpS_ID.Text
        mrow("SPS_EMP_ID") = txtStaffID.Value
        mrow("SPS_AMOUNT") = txtAmount.Text
        grdStaff.EditIndex = -1
        grdStaff.ShowFooter = True
        grdStaff.DataSource = TPTSDETSTAFF
        grdStaff.DataBind()

    End Sub
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim iParms(1) As SqlClient.SqlParameter
        iParms(0) = Mainclass.CreateSqlParameter("@SP_ID", ViewState("EntryId"), SqlDbType.Int)

        Try
            Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update SDETPROJECTMASTER set SP_STATUS='C' WHERE SP_ID=@SP_ID", iParms)
        Catch ex As Exception
            lblError.Text = ex.Message
        Finally
            lblError.Text = "Project Closed Sucessfully....!"
            btnClose.Visible = False
        End Try







    End Sub
End Class

