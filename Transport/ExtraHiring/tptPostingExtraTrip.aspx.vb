﻿
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports Telerik.Web.UI

Partial Class Transport_ExtraHiring_tptPostingExtraTrip
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200142") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            txtDiscount.Attributes.Add("onblur", " return calculate();")

            BindDAXCodes()
            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                If ViewState("datamode") = "add" Then
                    SetDataMode("add")
                Else
                    SetDataMode("view")
                    showNoRecordsFound()
                End If
                setModifyvalues(ViewState("EntryId"))
            End If

            showNoRecordsFound()
            grdInvoiceDetails.DataSource = TPTInvoiceDetails
            grdInvoiceDetails.DataBind()
            showNoRecordsFound()
        Else
            calcTotals()
        End If

    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim strsql3 As String = ""

            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            strsql3 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

            radVat.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            radVat.DataTextField = "DESCR"
            radVat.DataValueField = "ID"
            radVat.DataBind()

            radEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql3)
            radEmirate.DataTextField = "DESCR"
            radEmirate.DataValueField = "ID"
            radEmirate.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")

        grdInvoiceDetails.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode
        'btnPost.Visible = mDisable
        btnSAVE.Visible = False
        btnPrint.Visible = mDisable
        btnEmail.Visible = mDisable


        txtDiscount.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtsalik.Attributes.Add("onblur", " return formatme('" & txtDiscount.ClientID & "');")
        txtNet.Attributes.Add("onblur", " return formatme('" & txtDiscount.ClientID & "');")
        txtNetTotal.Attributes.Add("onblur", " return formatme('" & txtDiscount.ClientID & "');")
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim Amount(3) As String
            Dim bsu_City As String = ""
            Dim bsu_ID As String = ""
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = "0" Then

            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                Dim dtVATUserAccess As New DataTable


                dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")
                If dtVATUserAccess.Rows.Count > 0 Then
                    If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                        radVat.Enabled = True
                        radEmirate.Enabled = True
                    Else
                        radVat.Enabled = False
                        radEmirate.Enabled = False
                    End If
                Else
                    radVat.Enabled = False
                    radEmirate.Enabled = False
                End If
                bsu_ID = Mainclass.getDataValue("select distinct TEX_BSU_ID   from TPTEXTRA   LEFT OUTER JOIN TPTMASTER ON TMS_ID =TEX_DUTY  LEFT OUTER JOIN VW_DRIVER on DRIVER_EMPID=TEX_DRIVER LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TEX_BSU_ID where LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME='" & p_Modifyid & "'  and TEX_TEI_ID =0", "OASIS_TRANSPORTConnectionString")

                dt = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','EXTRA_TRIP','" & Now.ToString("dd/MMM/yyyy") & "'," & txtNetTotal.Text & ",'" & bsu_ID & "')", "OASIS_TRANSPORTConnectionString")


                If dt.Rows.Count > 0 Then
                    h_VATPerc.Value = dt.Rows(0)("tax_perc_value")
                    'h_VATPerc.Value = 5
                    Dim lstDrp As New RadComboBoxItem

                    lstDrp = radVat.Items.FindItemByValue(dt.Rows(0)("tax_code"))

                    If Not lstDrp Is Nothing Then
                        radVat.SelectedValue = lstDrp.Value
                    Else
                        radVat.SelectedValue = 0
                    End If

                    'txtVatAmount.Text = (((Val(txtNetTotal.Text) - (Val(txtsalik.Text) + Val(txtDiscount.Text))) * h_VATPerc.Value) / 100.0).ToString("####0.000")
                    txtVatAmount.Text = (((GetDoubleVal(txtgross.Text) - GetDoubleVal(txtDiscount.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")
                    txtNewNetAmount.Text = (GetDoubleVal(txtNetTotal.Text) + GetDoubleVal(txtVatAmount.Text)).ToString("####0.000")
                End If

                If ViewState("datamode") = "add" Then
                    'hdngross.Value = Mainclass.getDataValue("SELECT isnull(SUM(TEX_GROSS),0) FROM TPTEXTRA INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=TEX_BSU_ID WHERE LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND TEX_TEI_ID=0 GROUP BY  LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")
                    'hdnsalik.Value = Mainclass.getDataValue("SELECT isnull((SUM(TEX_SALIK)*4),0) FROM TPTEXTRA INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=TEX_BSU_ID WHERE LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND TEX_TEI_ID=0  GROUP BY  LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")

                    hdngross.Value = Mainclass.getDataValue("SELECT isnull(SUM(case when TEX_CBSU_ID IN('900500','900501') then 0 else TEX_GROSS END ),0) FROM TPTEXTRA INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=TEX_BSU_ID WHERE LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND TEX_TEI_ID=0 GROUP BY  LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")
                    hdnsalik.Value = Mainclass.getDataValue("SELECT isnull((SUM(TEX_SALIK)*4),0) FROM TPTEXTRA INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=TEX_BSU_ID WHERE LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND TEX_TEI_ID=0  GROUP BY  LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")

                    bsu_City = Mainclass.getDataValue("SELECT case when case when max(BSU_CITY)='ALN' then 'AUH' else max(BSU_CITY) end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when max(BSU_CITY)='ALN' then 'AUH' else max(BSU_CITY) end end   BSU_CITY   FROM TPTEXTRA INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=TEX_BSU_ID WHERE LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND TEX_TEI_ID=0  GROUP BY  LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")
                    Dim lstDrp As New RadComboBoxItem
                    lstDrp = radEmirate.Items.FindItemByValue(bsu_City)
                    If Not lstDrp Is Nothing Then
                        radEmirate.SelectedValue = lstDrp.Value
                    Else
                        radEmirate.SelectedValue = "NA"
                    End If

                    hdnNet.Value = GetDoubleVal(hdngross.Value) + GetDoubleVal(hdnsalik.Value)
                    lblbatchstr.Text = p_Modifyid
                    txtBatchNo.Text = "New"
                    txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
                    lblTrDate.Text = Now.ToString("dd/MMM/yyyy")
                    'btnPrint.Visible = False
                    txtDiscount.Enabled = True
                    h_EntryId.Value = 0
                    hdnDiscount.Value = "0.00"
                    hdnNetTotal.Value = GetDoubleVal(hdnNet.Value) - GetDoubleVal(hdnDiscount.Value)
                    'fillGridView(grdInvoiceDetails, "select TEX_ID,TEX_TEI_ID,replace(convert(varchar(30), tex_date, 106),' ','/') [DATE],CASE WHEN TEX_VEH_TYPE=1 THEN 'Medium Bus' ELSE CASE WHEN TEX_VEH_TYPE =2 THEN 'Big Bus' ELSE 'Van' END END VehicleType,DRIVER ,TMS_DESCR PURPOSE ,TEX_OSTART STARTKM,TEX_OEND ENDKM,TEX_TSTART STARTTIME,TEX_TEND ENDTIME,TEX_GROSS GROSS,TEX_SALIK SALIK ,TEX_NET NET   from TPTEXTRA   LEFT OUTER JOIN TPTMASTER ON TMS_ID =TEX_DUTY  LEFT OUTER JOIN VW_DRIVER on DRIVER_EMPID=TEX_DRIVER LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TEX_BSU_ID where LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME='" & p_Modifyid & "'  and TEX_TEI_ID =0")
                    fillGrid(grdInvoiceDetails, "SP_GET_EXTRAIPDETAILS", p_Modifyid, "A")
                Else
                    dt = MainObj.getRecords("SELECT TEI_ID,isnull(TEI_TRNO,'')TEI_TRNO, TEI_GROSS,TEI_SALIK,TEI_NET, TEI_USER_NAME, TEI_POSTED,TEI_REMARKS, TEI_DATE, TEI_POSTING_DATE,TEI_BSU_ID,TEI_STRNO,TEI_DATE,TEI_DISCOUNT,TEI_JHD_DOCNO,isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(TEI_PRGM_DIM_CODE,'0') DAX_ID,isnull(TEI_TAX_AMOUNT,0)TEI_TAX_AMOUNT,isnull(TEI_TAX_CODE,'NA')TEI_TAX_CODE,isnull(TEI_TAX_NET_AMOUNT,0)TEI_TAX_NET_AMOUNT,isnull(TEI_EMR_CODE,'NA') TEI_EMR_CODE FROM TPTEXTRA_INV  left outer join dbo.VV_DAX_CODES on ID=TEI_PRGM_DIM_CODE WHERE TEI_ID ='" & p_Modifyid & "' and TEI_BSU_ID='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                    If dt.Rows.Count > 0 Then
                        txtBatchDate.Text = Format(IIf(IsDBNull(dt.Rows(0)("TEI_POSTING_DATE")), Now.Date.ToString, dt.Rows(0)("TEI_POSTING_DATE")), "dd/MMM/yyyy")
                        txtBatchNo.Text = dt.Rows(0)("TEI_TRNO")
                        txtsalik.Text = Format(dt.Rows(0)("TEI_SALIK"), "#,##0.00")
                        txtgross.Text = dt.Rows(0)("TEI_GROSS")
                        txtNet.Text = Format(dt.Rows(0)("TEI_NET"), "#,##0.00")
                        txtremarks.Text = dt.Rows(0)("TEI_REMARKS")
                        lblbatchstr.Text = dt.Rows(0)("TEI_STRNO")
                        lblTrDate.Text = dt.Rows(0)("TEI_DATE")
                        txtDiscount.Text = Format(dt.Rows(0)("TEI_DISCOUNT"), "#,##0.00")
                        lblJHDDocNo.Text = dt.Rows(0)("TEI_JHD_DOCNO")
                        txtVatAmount.Text = dt.Rows(0)("TEI_TAX_AMOUNT")
                        txtNewNetAmount.Text = (dt.Rows(0)("TEI_NET") - dt.Rows(0)("TEI_DISCOUNT")) + dt.Rows(0)("TEI_TAX_AMOUNT")
                        radVat.SelectedValue = dt.Rows(0)("TEI_TAX_CODE")
                        txtremarks.Enabled = False
                        btnPost.Visible = False
                        lnkBatchDate.Enabled = False
                        txtNetTotal.Enabled = False
                        txtBatchDate.Enabled = False
                        txtDiscount.Enabled = False
                        lblStatus.Text = "Posted"
                        h_EntryId.Value = dt.Rows(0)("TEI_ID")
                        RadDAXCodes.SelectedItem.Text = dt.Rows(0)("DAX_DESCR")
                        RadDAXCodes.SelectedValue = dt.Rows(0)("DAX_ID")
                        RadDAXCodes.Enabled = False
                        radVat.Enabled = False
                        radEmirate.Enabled = False

                        Dim lstDrp As New RadComboBoxItem
                        lstDrp = radEmirate.Items.FindItemByValue(dt.Rows(0)("TEI_EMR_CODE"))

                        If Not lstDrp Is Nothing Then
                            radEmirate.SelectedValue = lstDrp.Value
                        Else
                            radEmirate.SelectedValue = "NA"
                        End If
                        radEmirate.Enabled = False
                        txtNetTotal.Text = Format((GetDoubleVal(txtgross.Text) + GetDoubleVal(txtsalik.Text)) - (GetDoubleVal(txtDiscount.Text)), "###,###,##0.00")

                        'fillGridView(grdInvoiceDetails, "select TEX_ID,TEX_TEI_ID,replace(convert(varchar(30), tex_date, 106),' ','/') [DATE],CASE WHEN TEX_VEH_TYPE=1 THEN 'Medium Bus' ELSE CASE WHEN TEX_VEH_TYPE =2 THEN 'Big Bus' ELSE 'Van' END END VehicleType,DRIVER ,TMS_DESCR PURPOSE ,TEX_OSTART STARTKM,TEX_OEND ENDKM,TEX_TSTART STARTTIME,TEX_TEND ENDTIME,TEX_GROSS GROSS,TEX_SALIK*4.0 SALIK ,TEX_NET NET   from TPTEXTRA   LEFT OUTER JOIN TPTMASTER ON TMS_ID =TEX_DUTY  LEFT OUTER JOIN VW_DRIVER on DRIVER_EMPID=TEX_DRIVER LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TEX_BSU_ID where TEX_TEI_ID=" & p_Modifyid)
                        fillGrid(grdInvoiceDetails, "SP_GET_EXTRAIPDETAILS", p_Modifyid, "P")
                        showNoRecordsFound()
                    End If
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function


    Private Property TPTInvoiceDetails() As DataTable
        Get
            Return ViewState("InvoiceDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("InvoiceDetails") = value
        End Set
    End Property

    Private Sub fillGrid(ByRef fillGrd As GridView, ByVal fillSQL As String, Optional id As String = "", Optional Status As String = "")

        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
        PARAM(1) = New SqlParameter("@ID", id)
        PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuID"))
        PARAM(3) = New SqlParameter("@Status", Status)




        TPTInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, fillSQL, PARAM).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("TEX_ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTInvoiceDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTInvoiceDetails = mtable
        fillGrd.DataSource = TPTInvoiceDetails
        fillGrd.DataBind()
    End Sub

    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("TEX_ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTInvoiceDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTInvoiceDetails = mtable
        fillGrdView.DataSource = TPTInvoiceDetails
        fillGrdView.DataBind()
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
        txtBatchNo.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property QUDFooter() As DataTable
        Get
            Return ViewState("QUDFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("QUDFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Sub showNoRecordsFound()
        If Not QUDFooter Is Nothing AndAlso QUDFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoiceDetails.Columns.Count - 2
            grdInvoiceDetails.Rows(0).Cells.Clear()
            grdInvoiceDetails.Rows(0).Cells.Add(New TableCell())
            grdInvoiceDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoiceDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdInvoiceDetails.PageIndexChanging
        grdInvoiceDetails.PageIndex = e.NewPageIndex
        grdInvoiceDetails.DataSource = TPTInvoiceDetails
        grdInvoiceDetails.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoiceDetails.RowCancelingEdit
        grdInvoiceDetails.ShowFooter = True
        grdInvoiceDetails.EditIndex = -1
        grdInvoiceDetails.DataSource = QUDFooter
        grdInvoiceDetails.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCMID As New Label
        Dim TrnNo As Integer
        lblCMID = TryCast(sender.FindControl("lblJobNo"), Label)
        TrnNo = Mainclass.getDataValue("select trn_no from tpthiring where TRN_JobnumberStr='" & lblCMID.Text & "'", "OASIS_TRANSPORTConnectionString")
        Response.Redirect("../ExtraHiring/tptHiringBilling.aspx?MainMnu_code=" & Encr_decrData.Encrypt("T200140") & "&datamode=" & Encr_decrData.Encrypt("view") & "&viewid=" & Encr_decrData.Encrypt(TrnNo))
    End Sub

    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblJobNo As New Label
        Dim lblBatchNo As New Label
        lblJobNo = TryCast(sender.FindControl("lblJobNo"), Label)
        lblBatchNo = TryCast(sender.FindControl("lblBatchNo"), Label)

        If lblStatus.Text = "Posted" Then
            MsgBox("This Batch is posted You can not Delete....!")
        Else
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TRN_BATCH=NULL,TRN_BATCHDATE=NULL,TRN_BATCHSTR=NULL,TRN_STATUS='S'  where TRN_JobnumberStr='" & lblJobNo.Text & "' and tpt_bsu_id='" & Session("sBsuid") & "'")
            showNoRecordsFound()
            fillGridView(grdInvoiceDetails, "select TRN_BATCHSTR BatchNo,TRN_BATCHDATE BatchDate,TRN_JobnumberStr JobNo,ACT_NAME Client,isnull(SUM(INV_Amt),0) Amount from TPTHiring LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id LEFT OUTER JOIN TPTHiringInvoice ON TRN_no =INV_TRN_NO where trn_batchstr ='" & lblBatchNo.Text & "' and tpt_bsu_id='" & Session("sBsuid") & "' group by TRN_BATCHSTR,TRN_BATCHDATE,TRN_JobnumberStr,ACT_NAME")
            If grdInvoiceDetails.Rows.Count - 1 = 0 Then
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub

    Protected Sub grdInvoiceDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoiceDetails.RowDataBound
        If ViewState("datamode") <> "add" Then
            grdInvoiceDetails.Columns(0).Visible = False
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox
        Dim total As Double = 0.0
        Dim gross As Double = 0.0
        Dim salik As Double = 0.0
        Dim net As Double = 0.0

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblGross"), Label)
            Dim lblID As Label = TryCast(gvr.FindControl("lblTEXID"), Label)
            Dim lblSalik As Label = TryCast(gvr.FindControl("lblSalik"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            If ChkBxItem.Checked = True Then
                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                total += GetDoubleVal(lblNet.Text)
                gross += GetDoubleVal(lblGross.Text)
                'salik += GetDoubleVal(lblSalik.Text) * 4
                salik += GetDoubleVal(lblSalik.Text)
            End If
        Next

        'gross = total + salik
        net = (total)
        If IDs = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        If RadDAXCodes.SelectedValue = "0" Then
            lblError.Text = "Please Select DAX Posting Code....!"
            Exit Sub
        End If

        If radEmirate.SelectedValue = "NA" Then
            lblError.Text = "Please Select Emirate....!"
            Exit Sub
        End If



        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If

        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(18) As SqlParameter
        Dim pParms1(1) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@TEI_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@TEI_STRNO", lblbatchstr.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@TEI_GROSS", gross, SqlDbType.Float)
        pParms(4) = Mainclass.CreateSqlParameter("@TEI_SALIK", salik, SqlDbType.Float)
        pParms(5) = Mainclass.CreateSqlParameter("@TEI_NET", net, SqlDbType.Float)
        pParms(6) = Mainclass.CreateSqlParameter("@TEI_USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@TEI_REMARKS", txtremarks.Text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@TEI_DATE", lblTrDate.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@TEI_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@TEI_POSTING_DATE", txtBatchDate.Text, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@TEI_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@TEI_DISCOUNT", txtDiscount.Text, SqlDbType.Float)
        pParms(13) = Mainclass.CreateSqlParameter("@TEX_IDS", IDs, SqlDbType.VarChar)
        pParms(14) = Mainclass.CreateSqlParameter("@TEI_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
        pParms(15) = Mainclass.CreateSqlParameter("@TEI_TAX_AMOUNT", txtVatAmount.Text, SqlDbType.Float)
        pParms(16) = Mainclass.CreateSqlParameter("@TEI_TAX_NET_AMOUNT", txtNewNetAmount.Text, SqlDbType.Float)
        pParms(17) = Mainclass.CreateSqlParameter("@TEI_TAX_CODE", radVat.SelectedValue, SqlDbType.VarChar)
        pParms(18) = Mainclass.CreateSqlParameter("@TEI_EMR_CODE", radEmirate.SelectedValue, SqlDbType.VarChar)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostExtraTrip", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblbatchstr.Text, "Posting", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            'setModifyvalues(ViewState("EntryId"))
            setModifyvalues(lblbatchstr.Text)
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Sub PostDeleteJob(ByVal trnno As String, ByVal action As String)
        Dim pParms(5) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@TRN_no", trnno, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@action", action, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@sBSUID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@PostingDate", txtBatchDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@YEAR", Session("F_YEAR"), SqlDbType.VarChar)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostJob", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                stTrans.Commit()
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            UpdateAddress()
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As String = CType(h_EntryId.Value, String)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTInvoiceExtraTrips"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TEIID", h_EntryId.Value, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@sBSUID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            Dim InvNo As String = ""
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "INVOICE"
            params("LPO") = ""
            params("InvNo") = txtBatchNo.Text
            params("InvDate") = txtBatchDate.Text
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            If Session("sBsuid") = "900501" Then
                ' Format(CDate(txtBatchDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
                If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTInvoiceExtraTrips.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoiceExtraTrips.rpt"
                End If
            Else
                'If Format(CDate(txtBatchDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
                If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTInvoiceExtraTripsBBT.rpt"
                Else
                    'repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoiceExtraTripsBBT.rpt"
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoiceExtraTrips.rpt"
                End If

            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            ' Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub UpdateAddress()
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TPT_ADDRESS='" & txtremarks.Text.Replace("'", "`") & "' where TRN_BATCHSTR='" & txtBatchNo.Text & "'")
    End Sub
    Sub calcTotals()
        Dim dt As New DataTable
        Dim dtVATUserAccess As New DataTable
        Dim NetTotal As Decimal = 0, Gross As Decimal = 0, Salik As Decimal = 0
        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblGross"), Label)
            Dim lblSalik As Label = TryCast(gvr.FindControl("lblSalik"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            If ChkBxItem.Checked = True Then
                NetTotal += GetDoubleVal(lblNet.Text)
                Gross += GetDoubleVal(lblGross.Text)
                'Salik += GetDoubleVal(lblSalik.Text) * 4
                Salik += GetDoubleVal(lblSalik.Text)
            End If
        Next
        txtgross.Text = Gross
        txtsalik.Text = Salik
        txtNet.Text = Gross + Salik
        txtNetTotal.Text = GetDoubleVal(txtNet.Text) - GetDoubleVal(txtDiscount.Text)
        'txtVatAmount.Text = (((Val(txtNetTotal.Text) - Val(txtsalik.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")
        txtVatAmount.Text = (((GetDoubleVal(txtgross.Text) - GetDoubleVal(txtDiscount.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")
        txtNewNetAmount.Text = (GetDoubleVal(txtNetTotal.Text) + GetDoubleVal(txtVatAmount.Text)).ToString("####0.000")

    End Sub
    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmail.Click
        Dim bEmailSuccess As Boolean
        Try
            SendEmail(CType(h_EntryId.Value, String), bEmailSuccess)

            'If bEmailSuccess = True Then
            '    lblError.Text = "Email send  Sucessfully...!"
            'Else
            '    lblError.Text = "Error while sending email.."
            'End If

        Catch ex As Exception
            lblError.Text = "Error while sending email.."
        End Try
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""
        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("userName", "SYSTEM")
        param.Add("reportCaption", "INVOICE")
        param("LPO") = ""
        param("InvNo") = txtBatchNo.Text
        param("InvDate") = txtBatchDate.Text
        param("@TEIID") = h_EntryId.Value
        param("@sBSUID") = HttpContext.Current.Session("sBsuid")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        'param.Add("@IMG_TYPE", "LOGO")
        If HttpContext.Current.Session("sBsuid") = "900501" Then
            'If Format(CDate(txtBatchDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExtraTripsEmail.rpt")
            Else
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExtraTripsEmailTAX.rpt")
            End If

        Else
            'If Format(CDate(txtBatchDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExtraTripsBBTEmail.rpt")
            Else
                'RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExtraTripsBBTEmailTAX.rpt")
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExtraTripsEmailTAX.rpt")

            End If


        End If
        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = LTrim(RTrim(txtBatchNo.Text))
        rptDownload.InvType = "EXTRATRIP"
        rptDownload.FCO_ID = 0
        'UtilityObj.Errorlog("Calling Loadreports", "BBT_TEST")
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  'Extra Trip Invoice Email Sent Sucessfully','Extra Trip'" & ",'Extra Trip Invoice','" & Session("sUsr_name") & "','" & RecNo & "'")
            lblError.Text = "Email send  Sucessfully...!"
        Else
            lblError.Text = "Error while sending email.."
        End If
    End Function
    Protected Sub radVat_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVat.SelectedIndexChanged
        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVat.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            'txtVatAmount.Text = (((Val(txtNetTotal.Text) - (Val(txtsalik.Text) + Val(txtDiscount.Text))) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtVatAmount.Text = (((GetDoubleVal(txtgross.Text) - GetDoubleVal(txtDiscount.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNewNetAmount.Text = Convert.ToDouble(GetDoubleVal(txtNetTotal.Text) + GetDoubleVal(txtVatAmount.Text)).ToString("####0.000")
        Else
            radVat.SelectedValue = 0
            h_VATPerc.Value = radVat.SelectedValue
            'txtVatAmount.Text = (((Val(txtNetTotal.Text) - (Val(txtsalik.Text) + Val(txtDiscount.Text))) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtVatAmount.Text = (((GetDoubleVal(txtgross.Text) - GetDoubleVal(txtDiscount.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNewNetAmount.Text = Convert.ToDouble(GetDoubleVal(txtNetTotal.Text) + GetDoubleVal(txtVatAmount.Text)).ToString("####0.000")
        End If
    End Sub

    'Protected Sub radVat_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radVat.TextChanged
    '    Dim dtVATPerc As New DataTable
    '    dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVat.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

    '    If dtVATPerc.Rows.Count > 0 Then
    '        h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
    '        txtVatAmount.Text = ((Val(txtNet.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
    '        txtNetTotal.Text = Convert.ToDouble(Val(txtNet.Text) + Val(txtVatAmount.Text)).ToString("####0.000")
    '    Else
    '        radVat.SelectedValue = 0
    '        h_VATPerc.Value = radVat.SelectedValue
    '        txtVatAmount.Text = ((Val(txtNet.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
    '        txtNewNetAmount.Text = Convert.ToDouble(Val(txtNetTotal.Text) + Val(txtVatAmount.Text)).ToString("####0.000")
    '    End If
    'End Sub
    Protected Sub RadDAXCodes_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles RadDAXCodes.SelectedIndexChanged

    End Sub
End Class


