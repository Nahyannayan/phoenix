Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class tptHiringBilling
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnUpload)
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim USR_NAME As String = Session("sUsr_name")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim Fyear As String = Session("F_YEAR")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200125" And ViewState("MainMnu_code") <> "T200130" And ViewState("MainMnu_code") <> "T200140" And ViewState("MainMnu_code") <> "T200126") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            txtQty.Visible = False
            LblQty.Visible = False

        ElseIf mode = "add" Then

            If ViewState("MainMnu_code") = "T200125" Then
                mDisable = False
                ViewState("datamode") = "add"
                txtTrn_No.Text = ""
                h_EntryId.Value = 0
                txtTrdate.Text = Format(Now, "dd-MMM-yyyy")
            Else
                mDisable = False
                ViewState("datamode") = "add"
                ClearDetails()
            End If
            hTPT_Loc_ID.Value = 1
            txtLocation.Text = "DUBAI"
            txtQty.Visible = True
            LblQty.Visible = True
            txtQty.Text = 1


        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            txtQty.Text = 1
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        txtLPO_No.Enabled = EditAllowed
        txtSTS_No.Enabled = EditAllowed
        txtTPT_Date.Enabled = EditAllowed
        txtLocation.Enabled = EditAllowed
        txtClientName.Enabled = EditAllowed
        txtService.Enabled = EditAllowed
        txtTPT_Description.Enabled = EditAllowed
        txtPickupPoint.Enabled = EditAllowed
        txtDropOffPoint.Enabled = EditAllowed
        txtCapacity.Enabled = EditAllowed
        rblJobType.Enabled = EditAllowed
        txtTRN_Veh_CatId.Enabled = EditAllowed
        txtTRN_SalesMan.Enabled = EditAllowed
        txtTPT_JobSt_Time.Enabled = EditAllowed
        txtTPT_JOBCl_Time.Enabled = EditAllowed
        txtDecline.Enabled = EditAllowed
        txtCancelDescr.Enabled = EditAllowed
        imgClient.Enabled = EditAllowed
        imgLocation.Enabled = EditAllowed
        imgService.Enabled = EditAllowed
        imgTPT_Date.Enabled = EditAllowed
        btnSave.Visible = Not ItemEditMode And Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnConfirm.Visible = mDisable
        btnDecline.Visible = mDisable
        btnPrint.Visible = mDisable
        btnDelete.Visible = mDisable And ViewState("MainMnu_code") = "T200126"
        btnEdit.Visible = mDisable And ViewState("MainMnu_code") <> "T200126"
        btnAdd.Visible = mDisable And ViewState("MainMnu_code") <> "T200126"

        rbCash.Enabled = EditAllowed
        rbCredit.Enabled = EditAllowed
        rbEmail.Enabled = EditAllowed
        btnUpload.Enabled = Not mDisable

        Dim Yearend As Date
        Dim BsuFreezdt As Date
        Yearend = Mainclass.getDataValue("select fyr_todt from oasis..financialyear_s where fyr_id='" & Session("F_YEAR") & "'", "OASIS_TRANSPORTConnectionString")
        BsuFreezdt = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")

        If BsuFreezdt <= Yearend Then
            btnAdd.Visible = True
        Else
            btnAdd.Visible = False
        End If


    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If ViewState("MainMnu_code") <> "T200126" Then
                trCancelRemarks.Visible = False
                trtxtCancelDescr.Visible = False
                txtDecline.Enabled = True
            Else
                txtCancelDescr.Enabled = True
            End If
            btnUpload.Visible = True
            UploadDocPhoto.Visible = True


            If p_Modifyid = 0 Then
                If Not ("900500900501").ToString.Contains(Session("sBsuid")) Then
                    rblJobType.Items(0).Selected = False
                    rblJobType.Items(1).Selected = True
                    hTPT_Client_ID.Value = Session("sBsuid")
                    txtClientName.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select bsu_name from oasis.dbo.BUSINESSUNIT_M where bsu_id='" & Session("sBsuid") & "'")
                End If
            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select TRN_no, LPO_no, STS_No, TPT_date, TPT_Loc_id, TPT_Veh_id, TPT_Drv_id, TPT_Client_id, TPT_Service_id, TPT_Description, TPT_STSSt_Time, TPT_JobSt_Time, TPT_STSSt_km, TPT_JOBCl_Time, TPT_STSArr_Time, TPT_JobSt_km, TPT_STSArr_km, TPT_JOBCl_km, TPT_Client_RefNo, TPT_Remarks, TPT_Add_Service, TPT_Xtra_Hrs, TPT_Feedback, TPT_QuoteFlag, TPT_QuoteDate, TPT_JobFlag, TPT_JobDate, TPT_InvFlag, TPT_InvDate, TPT_Bsu_id, TPT_BllId, TPT_JobType, TRN_number, TRN_numberstr, TRN_Veh_CatId, TRN_JobnumberStr, TRN_Status, TRN_Salesman, TPT_PickupPoint, TPT_DropOffPoint, TPT_Capacity, TRN_SEAS_ID, TPT_ENQUIRY, TRN_BATCH, TRN_TRL_ID, TPT_SUPPLIER, TPT_DRIVER, TPT_VEHREGNO, TPT_DATA, TPT_SC_VEHTYPE, TPT_SC_CAPACITY, TRN_BATCHDATE, TRN_BATCHSTR, TPT_ADDRESS, TPT_JHD_DOCNO, TPT_FYEAR, TPT_STAFFNAME, TPT_Deleted, isnull(TPT_Cancel_Remarks,'')TPT_Cancel_Remarks , ")
                strSql.Append("replace(convert(varchar(30), TPT_JobDate, 106),' ','/') TPT_JOBDATE,replace(convert(varchar(30), TPT_Date, 106),' ','/') TPTDATE, LOC_DESCRIPTION Location, isnull(TPT_BllId,0) h_TPT_BllId, ")
                strSql.Append("isnull(ACT_NAME,BSU_NAME) ClientName, isnull(TSM_DESCR,'') [SERVICE],isnull(SEAS_NAME,'') [SEASON], case when TPT_JobDate is null then 0 else 1 end jobflag, case when TPT_InvDate is null then 0 else 1 end invflag, isnull(SLSM_NAME,'') SALESMAN, isnull(VTYP_DESCRIPTION,'') VEH_CATEGORY  ")
                strSql.Append(",isnull(TRL_DESCR,'') [RATETYPE],isnull(TRL_DESCR,'') TPTRL_DESCR,isnull(TPT_Declined_Remarks,'')TPT_Declined_Remarks,isnull(TPT_JobSt_Time,'00:00')TPT_JobSt_Time,isnull(TPT_JobCl_Time,'00:00')TPT_JobCl_Time FROM TPTHiring ")
                strSql.Append("inner JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID ")
                strSql.Append("LEFT OUTER JOIN TPTSALESMAN ON SLSM_ID=TRN_Salesman  ")
                strSql.Append("LEFT OUTER JOIN VEHTYPE_M ON VTYP_ID=TRN_Veh_CatId ")
                strSql.Append("LEFT OUTER JOIN TPTSeason ON SEAS_ID=TRN_SEAS_ID ")
                strSql.Append("left outer JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id ")
                strSql.Append("LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id ")
                strSql.Append("LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID ")
                strSql.Append("LEFT OUTER JOIN TPT_RATELIST on TRL_ID=TRN_TRL_ID ")
                strSql.Append("where TRN_No=" & h_EntryId.Value)

                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtTrn_No.Text = dt.Rows(0)("trn_jobNumberStr")
                    txtSTS_No.Text = dt.Rows(0)("STS_No")
                    txtLPO_No.Text = dt.Rows(0)("LPO_No")
                    txtTPT_Date.Text = Format(dt.Rows(0)("TPT_JOBDATE"), "dd/MMM/yyyy")
                    txtLocation.Text = dt.Rows(0)("Location")
                    hTPT_Client_ID.Value = dt.Rows(0)("TPT_Client_ID")
                    txtDropOffPoint.Text = dt.Rows(0)("TPT_DropOffPoint")
                    hTPT_Loc_ID.Value = dt.Rows(0)("TPT_Loc_ID")
                    hTPT_Service_ID.Value = dt.Rows(0)("TPT_Service_ID")
                    h_TPT_BllId.Value = dt.Rows(0)("h_TPT_BllId")
                    txtClientName.Text = dt.Rows(0)("ClientName")
                    txtCapacity.Text = dt.Rows(0)("TPT_Capacity")
                    txtService.Text = dt.Rows(0)("Service")
                    txtTPT_Description.Text = dt.Rows(0)("TPT_Description")
                    txtSeason.Text = dt.Rows(0)("SEASON").ToString
                    hSeason.Value = dt.Rows(0)("TRN_SEAS_ID").ToString
                    txtPickupPoint.Text = dt.Rows(0)("TPT_PickupPoint")
                    txtDropOffPoint.Text = dt.Rows(0)("TPT_DropOffPoint")
                    rblJobType.Items(0).Selected = (dt.Rows(0)("TPT_JobType") = "1")
                    rblJobType.Items(1).Selected = (dt.Rows(0)("TPT_JobType") = "0")
                    txtTRN_SalesMan.Text = dt.Rows(0)("salesman")
                    hdnTRN_SalesMan.Value = dt.Rows(0)("TRN_Salesman")
                    txtTRN_Veh_CatId.Text = dt.Rows(0)("veh_Category")
                    hdnTRN_Veh_CatId.Value = dt.Rows(0)("TRN_Veh_CatId")
                    h_TRN_Status.Value = dt.Rows(0)("TRN_Status")
                    h_EnqMethod.Value = dt.Rows(0)("TPT_ENQUIRY").ToString
                    hRateType.Value = dt.Rows(0)("TRN_TRL_ID").ToString
                    txtRateType.Text = dt.Rows(0)("RATETYPE").ToString
                    txtTrdate.Text = dt.Rows(0)("TPTDATE")
                    txtCancelDescr.Text = dt.Rows(0)("TPT_Cancel_Remarks")
                    txtDecline.Text = dt.Rows(0)("TPT_Declined_Remarks")
                    txtTPT_JobSt_Time.Text = dt.Rows(0)("TPT_JobSt_Time")
                    txtTPT_JOBCl_Time.Text = dt.Rows(0)("TPT_JobCl_Time")
                    ReadRBEnqMethosValues()
                    btnUpload.Visible = True
                    UploadDocPhoto.Visible = True
                    If ViewState("MainMnu_code") = "T200125" Then
                        trDeclineRemarks.Visible = True
                        trDeclineDescr.Visible = True
                        If dt.Rows(0)("TRN_Status") = "N" Then
                            btnConfirm.Visible = True
                            btnDecline.Visible = True
                        Else
                            btnConfirm.Visible = False
                            btnDecline.Visible = False
                            btnEdit.Visible = False
                        End If
                    Else
                        btnConfirm.Visible = False
                        btnDecline.Visible = False
                    End If
                    btnConfirm.Visible = False
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If

                If dt.Rows(0)("TRN_Status") = "C" Then
                    btnEdit.Visible = False
                    btnAdd.Visible = False
                    btnDelete.Text = "Delete Job"
                    btnDelete.Visible = False
                Else
                    btnDelete.Text = "Cancel Job"
                End If
                Dim iji_filename As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "select THI_FILENAME+';'+THI_CONTENTTYPE+';'+THI_NAME from TPTHiring_I where THI_DOCNO='" & txtTrn_No.Text & "' and THI_DELETE=0")
                If Not iji_filename Is Nothing Then
                    hdnFileName.Value = iji_filename.Split(";")(0)
                    hdnContentType.Value = iji_filename.Split(";")(1)
                    btnDocumentLink.Text = iji_filename.Split(";")(2)
                    btnDocumentLink.Visible = True

                    btnDocumentDelete.Visible = True
                    btnUpload.Visible = False
                    UploadDocPhoto.Visible = False
                Else
                    btnUpload.Visible = True
                    UploadDocPhoto.Visible = True
                End If

            End If




            'btnEdit.Visible = (h_TPT_BllId.Value = 0)
            'btnDelete.Visible = (h_TPT_BllId.Value = 0)

            'btnEdit.Visible = (ViewState("MainMnu_code") <> "T200140")
            'btnPrint.Visible = (ViewState("MainMnu_code") <> "T200140")
            'btnAdd.Visible = (ViewState("MainMnu_code") <> "T200140")
            'btnDelete.Visible = (ViewState("MainMnu_code") <> "T200140")

            'txtTotalA.Text = TPTHiringInvoice.Compute("sum(Trv_TotalExp)", "")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ClearDetails()
        txtTPT_Date.Text = Now.ToString("dd/MMM/yyyy")

        txtTrn_No.Text = ""
        txtSTS_No.Text = ""
        txtLPO_No.Text = ""
        txtLocation.Text = ""

        hTPT_Client_ID.Value = "0"
        hTPT_Loc_ID.Value = "0"
        hTPT_Service_ID.Value = "0"

        txtClientName.Text = ""
        txtCapacity.Text = "0"
        txtService.Text = ""
        txtTPT_Description.Text = ""

        txtPickupPoint.Text = ""
        txtDropOffPoint.Text = ""

        rblJobType.Items(0).Selected = True
        txtTRN_SalesMan.Text = ""
        txtTRN_Veh_CatId.Text = ""
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'ClearDetails()
        'setModifyvalues(0)
        SetDataMode("add")
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Dim Yearend As Date
            Dim BsuFreezdt As Date
            Yearend = Mainclass.getDataValue("select fyr_todt from oasis..financialyear_s where fyr_id='" & Session("F_YEAR") & "'", "OASIS_TRANSPORTConnectionString")
            BsuFreezdt = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")

            If BsuFreezdt <= Yearend Then
                btnAdd.Visible = True
            Else
                btnAdd.Visible = False
            End If

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim i As Integer
        If txtTPT_Date.Text.Trim.Length = 0 Then strMandatory.Append("Date,")
        If hTPT_Loc_ID.Value = "0" Then strMandatory.Append("Location,")
        If hTPT_Client_ID.Value = "0" Then strMandatory.Append("Client's Name,")
        If hTPT_Service_ID.Value = "0" Then strMandatory.Append("Service not selected,")
        If hSeason.Value = "0" Then strMandatory.Append("Season not selected,")
        If hdnTRN_SalesMan.Value = "0" Then strMandatory.Append("Salesman  not selected,")
        If hdnTRN_Veh_CatId.Value = "0" Then strMandatory.Append("Vehcile Category  not selected,")
        If txtTPT_Description.Text.Trim.Length = 0 Then strMandatory.Append("Job Description,")
        If txtTPT_JobSt_Time.Text = "" Then strMandatory.Append("Job Starting Time,")
        If txtTPT_JOBCl_Time.Text = "" Then strMandatory.Append("Job Closing Time,")
        If Not IsDate(txtTPT_Date.Text) Then strError.Append("invalid date,")
        If ViewState("add") = True Then
            If Not IsNumeric(txtQty.Text) = True Then strError.Append("invalid Quantity,")
        End If

        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        For i = 1 To Val(txtQty.Text)
            Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
            Dim pParms(22) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TRN_No", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TPT_Bsu_id", Session("sBsuid"), SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@LPO_no", txtLPO_No.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@STS_No", txtSTS_No.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TPT_date", txtTPT_Date.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@TPT_Loc_id", hTPT_Loc_ID.Value, SqlDbType.Int)
            pParms(7) = Mainclass.CreateSqlParameter("@TPT_Client_id", hTPT_Client_ID.Value, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@TPT_Service_id", hTPT_Service_ID.Value, SqlDbType.Int)
            pParms(9) = Mainclass.CreateSqlParameter("@TPT_Description", txtTPT_Description.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@TPT_PickupPoint", txtPickupPoint.Text, SqlDbType.VarChar)
            pParms(11) = Mainclass.CreateSqlParameter("@TPT_DropOffPoint", txtDropOffPoint.Text, SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@TPT_JobType", rblJobType.Text, SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@TRN_Veh_CatId", hdnTRN_Veh_CatId.Value, SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@TRN_Status", "N", SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@TRN_Salesman", hdnTRN_SalesMan.Value, SqlDbType.VarChar)
            pParms(16) = Mainclass.CreateSqlParameter("@TPT_Capacity", txtCapacity.Text, SqlDbType.VarChar)
            pParms(17) = Mainclass.CreateSqlParameter("@TRN_SEAS_ID", hSeason.Value, SqlDbType.Int)
            pParms(18) = Mainclass.CreateSqlParameter("@TPT_ENQUIRY", h_EnqMethod.Value, SqlDbType.VarChar)
            pParms(19) = Mainclass.CreateSqlParameter("@TRN_TRL_ID", hRateType.Value, SqlDbType.VarChar)
            pParms(20) = Mainclass.CreateSqlParameter("@TPT_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(21) = Mainclass.CreateSqlParameter("@TPT_JobSt_Time", txtTPT_JobSt_Time.Text, SqlDbType.VarChar)
            pParms(22) = Mainclass.CreateSqlParameter("@TPT_JobCl_Time", txtTPT_JOBCl_Time.Text, SqlDbType.VarChar)


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTJob", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
                Try

                Catch ex As Exception
                    lblError.Text = ex.Message
                    stTrans.Rollback()
                    Exit Sub
                End Try

                stTrans.Commit()
                writeWorkFlowTPTH(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), "Open Job Order", "")
            End If
        Next
        SetDataMode("view")
        setModifyvalues(ViewState("EntryId"))
        Try
            UploadCodument(txtTrn_No.Text)
        Catch ex As Exception
            lblError.Text = "Job Order Created sucessfully and error while uploading Document..!!"
        End Try

        Dim iji_filename As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "select THI_FILENAME+';'+THI_CONTENTTYPE+';'+THI_NAME from TPTHiring_I where THI_DOCNO='" & txtTrn_No.Text & "' and THI_DELETE=0")
        If Not iji_filename Is Nothing Then
            hdnFileName.Value = iji_filename.Split(";")(0)
            hdnContentType.Value = iji_filename.Split(";")(1)
            btnDocumentLink.Text = iji_filename.Split(";")(2)
            btnDocumentLink.Visible = True

            btnDocumentDelete.Visible = True
            btnUpload.Visible = False
            UploadDocPhoto.Visible = False
        Else
            btnUpload.Visible = True
            UploadDocPhoto.Visible = True
        End If

        lblError.Text = "Data Saved Successfully !!!"
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        If h_TRN_Status.Value = "C" Then
            lblError.Text = ""
            If h_EntryId.Value > 0 Then
                Try
                    Dim sqlStr As String = "update TPTHiring set TPT_Deleted=1 where TRN_No=@TRNO"
                    Dim pParms(1) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@TRNO", h_EntryId.Value, SqlDbType.Int, True)
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                    stTrans.Commit()
                    writeWorkFlowTPTH(ViewState("EntryId"), "Deleted", "Open Job Order", "D")
                    Response.Redirect(ViewState("ReferrerUrl"))
                Catch ex As Exception
                    stTrans.Rollback()
                    'Errorlog(ex.Message)
                    Exit Sub
                Finally
                    objConn.Close()
                    Response.Redirect(ViewState("ReferrerUrl"))
                End Try
            End If
        Else
            If txtCancelDescr.Text = "" Then
                lblError.Text = "Please enter the description."
                Exit Sub
            End If

            Try
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='C',TPT_Cancel_Remarks='" & txtCancelDescr.Text & "' where TRN_No=" & h_EntryId.Value)
                stTrans.Commit()
                writeWorkFlowTPTH(ViewState("EntryId"), "Cancelled", "Open Job Order", "C")
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
                Response.Redirect(ViewState("ReferrerUrl"))
            End Try
        End If
    End Sub


    Private Property TPTHiringInvoice() As DataTable
        Get
            Return ViewState("TPTHiringInvoice")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTHiringInvoice") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As Integer = h_EntryId.Value
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTHiring"
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TRN_NO", TRN_NO, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("reportCaption") = "Transport Hiring Job " & TRN_NO
            params("reportCaption") = "Job Order Details" & IIf(h_TRN_Status.Value = "C", " - Cancelled", "")
            params("@TRN_NO") = TRN_NO
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTJob.rpt"

            Dim subRep(0) As MyReportClass

            subRep(0) = New MyReportClass
            Dim subcmd1 As New SqlCommand
            subcmd1.CommandText = "rptTPTHiringInvoice"
            Dim sqlParam1(1) As SqlParameter
            sqlParam1(0) = Mainclass.CreateSqlParameter("@TRN_NO", TRN_NO, SqlDbType.Int)
            subcmd1.Parameters.Add(sqlParam1(0))
            subcmd1.CommandType = Data.CommandType.StoredProcedure
            subcmd1.Connection = New SqlConnection(str_conn)
            subRep(0).Command = subcmd1
            subRep(0).ResourceName = "wBTA_Sector"

            repSource.IncludeBSUImage = True
            repSource.SubReport = subRep
            'End If
            Session("ReportSource") = repSource
            ' Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function writeWorkFlowTPTH(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal strStatus As String) As Integer
        Dim iParms(6) As SqlClient.SqlParameter
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TRN_NO", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveWorkFlowTPTH", iParms)
        If RetVal = "-1" Then
            lblError.Text = "Unexpected Error !!!"
            stTrans.Rollback()
            Exit Function
        Else
            stTrans.Commit()
        End If
    End Function

    Protected Sub rbCredit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCredit.CheckedChanged
        setRBEnqMethodCheckValues()
    End Sub
    Protected Sub rbEmail_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEmail.CheckedChanged
        setRBEnqMethodCheckValues()
    End Sub
    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged, rbCredit.CheckedChanged, rbEmail.CheckedChanged
        setRBEnqMethodCheckValues()
    End Sub
    Private Sub setRBEnqMethodCheckValues()
        If rbCash.Checked = True Then
            h_EnqMethod.Value = "CASH"
        ElseIf rbCredit.Checked = True Then
            h_EnqMethod.Value = "CARD"
        Else
            h_EnqMethod.Value = "EMAIL"
        End If
    End Sub

    Private Sub ReadRBEnqMethosValues()
        If h_EnqMethod.Value = "CASH" Then
            rbCash.Checked = True
        ElseIf h_EnqMethod.Value = "CARD" Then
            rbCredit.Checked = True
        Else
            rbEmail.Checked = True
        End If

    End Sub
    Protected Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        If txtDecline.Text = "" Then
            lblError.Text = "Please enter the Decline remarks"
            Exit Sub
        End If
        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='R',TPT_Declined_Remarks='" & txtDecline.Text & "' where TRN_No=" & h_EntryId.Value)
            stTrans.Commit()
            writeWorkFlowTPTH(ViewState("EntryId"), "Declined", "Open Job Order", "R")
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Exit Sub
        Finally
            objConn.Close()
            'Response.Redirect(ViewState("ReferrerUrl"))
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Job Order Declined..."
        End Try

    End Sub
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='' where TRN_No=" & h_EntryId.Value)
            stTrans.Commit()
            writeWorkFlowTPTH(ViewState("EntryId"), "Confirmed", "Open Job Order", "")
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Exit Sub
        Finally
            objConn.Close()
            'Response.Redirect(ViewState("ReferrerUrl"))
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Job Order Confirmed Successfully !!!"
        End Try
    End Sub
    Private Sub UploadCodument(ByVal TRNO As String)
        'If UploadDocPhoto.FileName <> "" Then
        'Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\"
        'Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
        'Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename

        'Try
        '    UploadDocPhoto.PostedFile.SaveAs(strFilepath)
        'Catch ex As Exception
        '    Exit Sub
        'End Try

        If Not IO.File.Exists(h_fileNamewithPath.Value) Then
            lblError.Text = "Invalid FilePath!!!!"
            Exit Sub
        Else
            Dim ContentType As String = getContentType(h_fileNamewithPath.Value)
            Dim FileExt As String = h_tempFilepath.Value.Substring(h_tempFilepath.Value.Length - 4)
            If FileExt.Substring(0, 1) <> "." Then FileExt = h_tempFilepath.Value.Substring(h_tempFilepath.Value.Length - 5)
            If FileExt.Substring(0, 1) <> "." Then FileExt = h_tempFilepath.Value.Substring(h_tempFilepath.Value.Length - 6)
            Try
                IO.File.Move(h_fileNamewithPath.Value, h_str_img.Value & TRNO & "_" & Session("sBsuid") & FileExt)
                SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "insert into TPTHiring_I (THI_CONTENTTYPE, THI_DOCNO, THI_BSU_ID, THI_NAME, THI_DATE, THI_USERNAME, THI_FILENAME, THI_DELETE) values ('" & ContentType & "','" & txtTrn_No.Text & "','" & Session("sBSUID") & "','" & h_FileName.Value & "',getdate(),'" & Session("sUsr_name") & "','" & txtTrn_No.Text & "_" & Session("sBsuid") & FileExt & "',0)")
                hdnFileName.Value = txtTrn_No.Text & "_" & Session("sBsuid") & FileExt
                hdnContentType.Value = ContentType
                btnDocumentLink.Text = UploadDocPhoto.FileName
                btnDocumentLink.Visible = True
                btnDocumentDelete.Visible = True
                UploadDocPhoto.Visible = False
                btnUpload.Visible = False
            Catch ex As Exception

            End Try
        End If

    End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select
    End Function
    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            h_tempFilepath.Value = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            h_str_img.Value = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\"
            Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
                h_fileNamewithPath.Value = strFilepath
                h_FileName.Value = UploadDocPhoto.FileName
                btnDocumentLink.Text = strFilepath
                btnDocumentLink.Visible = True
                hdnFileName.Value = strFilepath


            Catch ex As Exception
                Exit Sub
            End Try
            'If hdnAssetImagePath.Value <> "" Then
            'If Not IO.File.Exists(strFilepath) Then
            '    lblError.Text = "Invalid FilePath!!!!"
            '    Exit Sub
            'Else
            '    Dim ContentType As String = getContentType(strFilepath)
            '    Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
            '    If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
            '    If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
            '    Try
            '        IO.File.Move(strFilepath, str_img & txtTrn_No.Text & "_" & Session("sBsuid") & FileExt)
            '        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "insert into TPTHiring_I (THI_CONTENTTYPE, THI_DOCNO, THI_BSU_ID, THI_NAME, THI_DATE, THI_USERNAME, THI_FILENAME, THI_DELETE) values ('" & ContentType & "','" & txtTrn_No.Text & "','" & Session("sBSUID") & "','" & UploadDocPhoto.FileName & "',getdate(),'" & Session("sUsr_name") & "','" & txtTrn_No.Text & "_" & Session("sBsuid") & FileExt & "',0)")
            '        hdnFileName.Value = txtTrn_No.Text & "_" & Session("sBsuid") & FileExt
            '        hdnContentType.Value = ContentType
            '        btnDocumentLink.Text = UploadDocPhoto.FileName
            '        btnDocumentLink.Visible = True
            '        btnDocumentDelete.Visible = True
            '        UploadDocPhoto.Visible = False
            '        btnUpload.Visible = False
            '    Catch ex As Exception

            '    End Try
            'End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub
    Private Sub btnDocumentDelete_Click(sender As Object, e As EventArgs) Handles btnDocumentDelete.Click
        If Not btnSave.Visible Then
            lblError.Text = "Cannot delete file"
        Else
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "update TPTHiring_I set THI_DELETE=1 where THI_DOCNO='" & txtTrn_No.Text & "' and THI_bsu_id='" & Session("sBSUID") & "'")
            Dim fileToDelete As New FileInfo(WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\" & hdnFileName.Value)
            Try

                fileToDelete.Delete()
                writeWorkFlowTPTH(h_EntryId.Value, "DELETE", "Open Job Order-Attachement Deleted File Name:" & hdnFileName.Value, "D")
                hdnFileName.Value = ""
                hdnContentType.Value = ""
                btnDocumentLink.Visible = False
                btnDocumentDelete.Visible = False
                btnUpload.Visible = True
                UploadDocPhoto.Visible = True

            Catch ex As Exception

            End Try



        End If
    End Sub
End Class