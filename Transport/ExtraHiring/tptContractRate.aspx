﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptContractRate.aspx.vb" Inherits="Transport_tptContractRate" Title="Rate List" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }





        function getSalesMan() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTSALESMANNEW"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=hdnSalesMan.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtSalesMan.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=hdnSalesMan.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtSalesMan.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                __doPostBack('<%= txtSalesMan.ClientID%>', 'TextChanged');
            }
        }



        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function getVehicleCategory() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTVEHICLECATEGORY"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up2");
          <%--  if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
              document.getElementById("<%=txt_veh_CatDescr.ClientID %>").value = NameandCode[1];
              document.getElementById("<%=hdnTRN_Veh_CatId.ClientID %>").value = NameandCode[0];--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txt_veh_CatDescr.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hdnTRN_Veh_CatId.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txt_veh_CatDescr.ClientID%>', 'TextChanged');
            }
        }

        function getSeason() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTSEASON"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up3");
   <%-- if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('___');
    document.getElementById("<%=txt_Season_descr.ClientID %>").value = NameandCode[1];
    document.getElementById("<%=hdnTRN_Season_Id.ClientID %>").value = NameandCode[0];--%>
}

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txt_Season_descr.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hdnTRN_Season_Id.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txt_Season_Descr.ClientID%>', 'TextChanged');
            }
        }



    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Extra Trips Rates and Contracts"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle"><asp:Label runat="server" id="rptHeader" Text="Extra Trips Rates and Contracts"></asp:Label></td>
                    </tr>--%>
                                <asp:Panel runat="server" ID="pnlJobOrder">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Contract No</span></td>

                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="txtCnh_Id" Enabled="false" runat="server"></asp:TextBox></td>
                                        <td align="left" width="20%"><span class="field-label">Contract Date</span><span style="color: red">*</span></td>

                                        <td align="left" style="width: 25%">
                                            <asp:TextBox ID="txtCnh_Date" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgCnh_Date" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtCnh_Date" PopupButtonID="imgCnh_Date">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Sales Man</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSalesMan" Enabled="false" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                        <asp:LinkButton ID="lnlbtnAddSalesManID" runat="server" Enabled="False">Add</asp:LinkButton>
                                        <asp:ImageButton ID="imgbtnAddSalesManID" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getSalesMan(); return false;" TabIndex="8" /><asp:HiddenField ID="hdnSalesMan" runat="server" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Effective Date</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCnh_Effective_Date" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCnh_Effective_Date" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtCnh_Effective_Date" PopupButtonID="imgCnh_Effective_Date">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Client's Name</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <%-- <asp:TextBox ID="txtClientCode" Enabled="false" Width="80px" runat="server" style="color: red"></asp:TextBox>
                            <asp:TextBox ID="txtClientName" Enabled="false"  runat="server" style="color: red"></asp:TextBox>
                            <asp:LinkButton ID="lnlbtnAddClientID" runat="server" Enabled="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif" 
                                            OnClientClick="return getClientName();" TabIndex="8" />--%>
                                       <div class="checkbox-list">
                                        <asp:Repeater ID="rptImages" runat="server">
                                            <ItemTemplate>
                                                <asp:Label ID="lblacid" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>

                                                <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("ACT_NAME") %>' Visible="true"></asp:Label>
                                                <br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                       </div>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">No Show(%)</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCnh_Noshow" runat="server" Style="text-align: right" TabIndex="7" Text="0"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Vehicle Type</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txt_veh_CatDescr" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Enabled="False">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImgVehType" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleCategory(); return false;" TabIndex="8" /></td>

                                    <td align="left" width="20%"><span class="field-label">Season</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txt_Season_Descr" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="LinkButton2" runat="server" Enabled="False">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImgSeason" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getSeason(); return false;" TabIndex="8" /></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">List Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDescription" Enabled="false" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Button ID="btnDisable" runat="server" CausesValidation="False" CssClass="button" Text="Disable" />
                                    </td>

                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="grdRates" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true" FooterStyle-HorizontalAlign="Left"
                                            CaptionAlign="Top" PageSize="15" CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCnd_ID" runat="server" Text='<%# Bind("Cnd_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCnd_Cnh_Id" runat="server" Text='<%# Bind("Cnd_Cnh_Id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="44%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtProduct" Enabled="false" runat="server" Text='<%# Eval("Prod_Name") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlProducts" runat="server"></asp:DropDownList><asp:HiddenField ID="hProducts" runat="server" Value='<%# Eval("Cnd_Prod_id") %>' />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:DropDownList ID="ddlProducts" runat="server"></asp:DropDownList><asp:HiddenField ID="hProducts" runat="server" Value='<%# Eval("Cnd_Prod_id") %>' />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rate" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCnd_Rate" Style="text-align: right" Enabled="false" runat="server" Text='<%# Eval("Cnd_Rate","{0:0.00}") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCnd_Rate" Style="text-align: right" runat="server" Text='<%# Eval("Cnd_Rate") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtCnd_Rate" Style="text-align: right" runat="server" Text='<%# Eval("Cnd_Rate") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Hrs/Day" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCnd_Hrs" Style="text-align: right" Enabled="false" runat="server" Text='<%# Eval("Cnd_Hrs") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCnd_Hrs" Style="text-align: right" runat="server" Text='<%# Eval("Cnd_Hrs") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtCnd_Hrs" Style="text-align: right" runat="server" Text='<%# Eval("Cnd_Hrs") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Kms/Day" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCnd_Km" Style="text-align: right" runat="server" Enabled="false" Text='<%# Eval("Cnd_Km") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCnd_Km" Style="text-align: right" runat="server" Text='<%# Eval("Cnd_Km") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtCnd_Km" Style="text-align: right" runat="server" Text='<%# Eval("Cnd_Km") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateRates" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelRates" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddRates" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditRates" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCnh_Print" TextMode="MultiLine" runat="server" TabIndex="23"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnCopy" runat="server" CausesValidation="False" CssClass="button"
                                Text="Copy" TabIndex="27" />
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="hGridDelete" Value="0" runat="server" />
                            <asp:HiddenField ID="hdnTRN_Veh_CatId" runat="server" />
                            <asp:HiddenField ID="hdnTRN_Season_Id" runat="server" />
                            <asp:HiddenField ID="hdnCNH_TRL_ID" runat="server" />
                            <asp:HiddenField ID="hdnCNH_CLIENT_ID" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
