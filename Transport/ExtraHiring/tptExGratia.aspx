<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptExGratia.aspx.vb" Inherits="Transport_tptExGratia" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<style type="text/css">
        .autocomplete_highlightedListItem_S1
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }
    </style>--%>
    <style>
        .RadComboBox_Default .rcbReadOnly {
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbDisabled {
            background-color: rgba(0,0,0,0.01) !important;
        }

            .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                background-color: transparent !important;
                border-radius: 0px !important;
                border: 0px !important;
                padding: initial !important;
                box-shadow: none !important;
            }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length - 1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function parseTime(s) {
            var c = s.split(':');
        }
        function NetAmount() {
            var a = document.getElementById("<%=txtTotalAmount.ClientID %>").value;
            var b = document.getElementById("<%=txtOutStation.ClientID %>").value;
            var c = document.getElementById("<%=txtoOTAmt.ClientID %>").value;

            var ot1;
            var ot2;


            document.getElementById("<%=txtNetAmount.ClientID %>").value = Number(a) + Number(b) + Number(c);

        }


        function calc(Type, rate) {
            a = document.getElementById("<%=txtTStart.ClientID %>").value;
            b = document.getElementById("<%=txtTEnd.ClientID %>").value;
            e = document.getElementById("<%=txtTStart.ClientID %>").value;
            var Rate = document.getElementById("<%=h_Rate.ClientID %>").value;
            var Rate1 = document.getElementById("<%=h_Rate1.ClientID %>").value;




            var OT1Hrs = document.getElementById("<%=h_OT1_Hrs.ClientID %>").value;
            var OT2Hrs = document.getElementById("<%=h_OT2_Hrs.ClientID %>").value;



            var Drivertype = document.getElementById("<%=h_DriverType.ClientID %>").value;
            p = "1/1/1970 ";
            var hol = document.getElementById("<%=h_Holiday.ClientID%>").value;
            //if (a.substring(0, 2) < 18) {
            if (a.substring(0, 2) < Number(OT1Hrs.substring(0, OT1Hrs.length - 3))) {
                if (hol == 1) {
                    a
                }
                else {
                    //a = "18:00"
                    a = OT1Hrs
                }
            }
            else {
                a
            }
            //if (b.substring(0, 2) < 18) {
            if (b.substring(0, 2) < Number(OT1Hrs.substring(0, OT1Hrs.length - 3))) {
                if (hol == 1) {
                    b
                }
                else {
                    if (b.substring(0, 2) < 12) {
                        if (e.substring(0, 2) > 11) {
                            b
                        }
                        else {
                            //b = "18:00"
                            b = OT1Hrs
                        }
                    }
                        //else { b = "18:00" }
                    else { b = OT1Hrs }

                }
            }
            else {
                b
            }

            //if ((a.substring(0, 2) < 21) && (b.substring(0, 2) >= 21)) {
            if ((a.substring(0, 2) < Number(OT2Hrs.substring(0, OT2Hrs.length - 3))) && (b.substring(0, 2) >= Number(OT2Hrs.substring(0, OT2Hrs.length - 3)))) {


                var Ot1;
                Ot1 = b
                //b = "21:00"
                b = OT2Hrs

                difference = new Date(new Date(p + b) - new Date(p + a)).toUTCString().split(" ")[4];
                var diff = new Date(new Date(p + b) - new Date(p + a));

                var diff1 = new Date(new Date(p + Ot1) - new Date(p + b));
                var hours = Math.floor(diff / (1000 * 60 * 60));
                var hours1 = Math.floor(diff1 / (1000 * 60 * 60));
                diff -= hours * (1000 * 60 * 60);
                diff1 -= hours1 * (1000 * 60 * 60);
                var mins = Math.floor(diff / (1000 * 60));
                var mins1 = Math.floor(diff1 / (1000 * 60));
                diff -= mins * (1000 * 60);
                diff1 -= mins1 * (1000 * 60);

                if (mins > 30) {
                    hours = hours + 1;
                }
                else if (mins > 0) {
                    hours = hours + 0.5;
                }
                else {
                    hours = hours + 0.0;
                }
                //Ot Calculations
                if (mins1 > 30) {
                    hours1 = hours1 + 1;
                }
                else if (mins1 > 0) {
                    hours1 = hours1 + 0.5;
                }
                else {
                    hours1 = hours1 + 0.0;
                }

                document.getElementById("<%=txtTTotal.ClientID %>").value = hours + hours1;
                var otAmt1 = hours * Rate;
                //                                if (Drivertype == 1) {
                //                                    Rate1 = 16
                //                                }
                //                                else { Rate1 = 12 }
                var otAmt2 = hours1 * Rate1;
                document.getElementById("<%=txtTotalAmount.ClientID %>").value = otAmt1 + otAmt2;
                var c = document.getElementById("<%=txtTotalAmount.ClientID %>").value
                var d = document.getElementById("<%=txtOutStation .ClientID %>").value
                var e = document.getElementById("<%=txtoOTAmt.ClientID %>").value;
                var tot = Number(c) + Number(d) + Number(e);
                document.getElementById("<%=txtNetAmount.ClientID %>").value = tot
            }


                //else if ((a.substring(0, 2) >= 21) && (b.substring(0, 2) < 21)) {
            else if ((a.substring(0, 2) >= Number(OT2Hrs.substring(0, OT2Hrs.length - 3))) && (b.substring(0, 2) < Number(OT2Hrs.substring(0, OT2Hrs.length - 3)))) {
                var Ot2;
                Ot2 = b
                //b = "21:00"
                b = OT2Hrs
                difference = new Date(new Date(p + b) - new Date(p + a)).toUTCString().split(" ")[4];
                var diff2 = new Date(new Date(p + b) - new Date(p + a));
                var diff3 = new Date(new Date(p + Ot2) - new Date(p + b));
                var hours2 = Math.floor(diff2 / (1000 * 60 * 60));
                var hours3 = Math.floor(diff3 / (1000 * 60 * 60));
                diff3 -= hours3 * (1000 * 60 * 60);
                diff2 -= hours2 * (1000 * 60 * 60);
                var mins2 = Math.floor(diff2 / (1000 * 60));
                var mins3 = Math.floor(diff3 / (1000 * 60));
                diff2 -= mins2 * (1000 * 60);
                diff3 -= mins3 * (1000 * 60);
                if (mins2 > 30) {
                    hours2 = hours2 + 1;
                }
                else if (mins2 > 0) {
                    hours2 = hours2 + 0.5;
                }
                else {
                    hours2 = hours2 + 0.0;
                }
                if (mins3 > 30) {
                    hours3 = hours3 + 1;
                }
                else if (mins3 > 0) {
                    hours3 = hours3 + 0.5;
                }
                else {
                    hours3 = hours3 + 0.0;
                }

                if (a > Ot2) {
                    hours3 = 24 + hours3
                }

                if (hours2 < 0) {
                    hours3 = hours3 + hours2;
                    hours2 = 0;
                }
                document.getElementById("<%=txtTTotal.ClientID %>").value = hours2 + hours3;
                    var OtAmt2 = hours2 * Rate;
                    //                            if (Drivertype == 1) {
                    //                                Rate1 = 16
                    //                            }
                    //                            else { Rate1 = 12 }

                    var OtAmt3 = hours3 * Rate1;
                    document.getElementById("<%=txtTotalAmount.ClientID %>").value = OtAmt2 + OtAmt3;
                    var c = document.getElementById("<%=txtTotalAmount.ClientID %>").value
                    var d = document.getElementById("<%=txtOutStation .ClientID %>").value
                    var e = document.getElementById("<%=txtoOTAmt.ClientID %>").value;
                    var tot = Number(c) + Number(d) + Number(e);

                    document.getElementById("<%=txtNetAmount.ClientID %>").value = tot
                }

                    //else if ((a.substring(0, 2) >= 21) && (b.substring(0, 2) >= 21)) {
                else if ((a.substring(0, 2) >= Number(OT2Hrs.substring(0, OT2Hrs.length - 3))) && (b.substring(0, 2) >= Number(OT2Hrs.substring(0, OT2Hrs.length - 3)))) {


                    difference = new Date(new Date(p + b) - new Date(p + a)).toUTCString().split(" ")[4];
                    var diff = new Date(new Date(p + b) - new Date(p + a));
                    var hours = Math.floor(diff / (1000 * 60 * 60));
                    diff -= hours * (1000 * 60 * 60);
                    var mins = Math.floor(diff / (1000 * 60));
                    diff -= mins * (1000 * 60);
                    if (mins > 30) {
                        hours = hours + 1;
                    }
                    else if (mins > 0) {
                        hours = hours + 0.5;
                    }
                    else {
                        hours = hours + 0.0;
                    }
                    if (a > b) {
                        hours = 24 + hours
                    }

                    document.getElementById("<%=txtTTotal.ClientID %>").value = hours;

                    var otAmt1 = hours * Rate;
                    //                    if (Drivertype == 1) {
                    //                        Rate1 = 16
                    //                    }
                    //                    else { Rate1 = 12 }
                    document.getElementById("<%=txtTotalAmount.ClientID %>").value = hours * Rate1;
                            var c = document.getElementById("<%=txtTotalAmount.ClientID %>").value
                    var d = document.getElementById("<%=txtOutStation .ClientID %>").value
                    var e = document.getElementById("<%=txtoOTAmt.ClientID %>").value;
                    var tot = Number(c) + Number(d) + Number(e);

                    document.getElementById("<%=txtNetAmount.ClientID %>").value = tot
                        }
                        else {

                            if ((a == b) || (a < b)) {
                                var diff6 = new Date(new Date(p + b) - new Date(p + a));
                                var hours6 = Math.floor(diff6 / (1000 * 60 * 60));
                                diff6 -= hours6 * (1000 * 60 * 60);
                                var mins6 = Math.floor(diff6 / (1000 * 60));
                                diff6 -= mins6 * (1000 * 60);

                                if (mins6 > 30) {
                                    hours6 = hours6 + 1;
                                }
                                else if (mins6 > 0) {
                                    hours6 = hours6 + 0.5;
                                }
                                else {
                                    hours6 = hours6 + 0.0;
                                }
                                document.getElementById("<%=txtTTotal.ClientID %>").value = hours6;
                        var OtAmt6 = hours6 * Rate;
                        document.getElementById("<%=txtTotalAmount.ClientID %>").value = OtAmt6;
                        var TAmount = document.getElementById("<%=txtTotalAmount.ClientID %>").value
                        var TOutStation = document.getElementById("<%=txtOutStation .ClientID %>").value
                        var e = document.getElementById("<%=txtoOTAmt.ClientID %>").value;
                        var totAmt = Number(TAmount) + Number(TOutStation) + Number(e);
                        document.getElementById("<%=txtNetAmount.ClientID %>").value = totAmt;


                    }
                    else {

                        var Ot3;
                        Ot3 = b
                        //b = "21:00"
                        b = OT2Hrs

                        difference = new Date(new Date(p + b) - new Date(p + a)).toUTCString().split(" ")[4];
                        var diff4 = new Date(new Date(p + b) - new Date(p + a));
                        var diff5 = new Date(new Date(p + Ot3) - new Date(p + b));
                        var hours4 = Math.floor(diff4 / (1000 * 60 * 60));
                        var hours5 = Math.floor(diff5 / (1000 * 60 * 60));
                        diff4 -= hours4 * (1000 * 60 * 60);
                        diff5 -= hours5 * (1000 * 60 * 60);
                        var mins4 = Math.floor(diff4 / (1000 * 60));
                        var mins5 = Math.floor(diff5 / (1000 * 60));
                        diff4 -= mins4 * (1000 * 60);
                        diff5 -= mins5 * (1000 * 60);
                        if (mins4 > 30) {
                            hours4 = hours4 + 1;
                        }
                        else if (mins4 > 0) {
                            hours4 = hours4 + 0.5;
                        }
                        else {
                            hours4 = hours4 + 0.0;
                        }


                        if (mins5 > 30) {
                            hours5 = hours5 + 1;
                        }
                        else if (mins5 > 0) {
                            hours5 = hours5 + 0.5;
                        }
                        else {
                            hours5 = hours5 + 0.0;
                        }

                        if (a > Ot3) {
                            hours5 = 24 + hours5
                        }

                        if ((hours4 < 0) || (hours5 < 0)) {
                            hours5 = hours5 + hours4;
                            hours4 = 0;
                        }

                        document.getElementById("<%=txtTTotal.ClientID %>").value = hours4 + hours5;
                        var OtAmt4 = hours4 * Rate;
                        //                    if (Drivertype == 1) {
                        //                        Rate1 = 16
                        //                    }
                        //                    else { Rate1 = 12 }

                        var OtAmt5 = hours5 * Rate1;
                        document.getElementById("<%=txtTotalAmount.ClientID %>").value = OtAmt4 + OtAmt5;
                        var c = document.getElementById("<%=txtTotalAmount.ClientID %>").value
                        var d = document.getElementById("<%=txtOutStation .ClientID %>").value
                        var e = document.getElementById("<%=txtoOTAmt.ClientID %>").value;
                        var tot = Number(c) + Number(d) + Number(e);
                        document.getElementById("<%=txtNetAmount.ClientID %>").value = tot

                    }


                    //                           <%-- difference = new Date(new Date(p + b) - new Date(p + a)).toUTCString().split(" ")[4];
//                            var diff = new Date(new Date(p + b) - new Date(p + a));
//                            var hours = Math.floor(diff / (1000 * 60 * 60));
//                            diff -= hours * (1000 * 60 * 60);
//                            var mins = Math.floor(diff / (1000 * 60));
//                            diff -= mins * (1000 * 60);
//                                if (mins > 30) {
//                                    hours = hours + 1;
//                                        }
//                                else if (mins > 0) {
//                                    hours = hours + 0.5;
//                                        }
//                                else {
//                                    hours = hours + 0.0;
//                                    }

//                                if (a > b) {
//                                    hours = 24 + hours
//                                }
//                                document.getElementById("<%=txtTTotal.ClientID %>").value = hours;
//                                        
//                                if (Drivertype = 1) {
//                                    Rate1 = 16
//                                }
//                                else { Rate1 = 12 }
//                           
//                    document.getElementById("<%=txtTotalAmount.ClientID %>").value = hours * Rate;
//                                var c = document.getElementById("<%=txtTotalAmount.ClientID %>").value
//                                var d = document.getElementById("<%=txtOutStation .ClientID %>").value
//                                var tot = Number(c) +Number(d);
//                                document.getElementById("<%=txtNetAmount.ClientID %>").value = tot--%>
                }





        }

        //function ChangeAllCheckBoxStates(checkState) {
        //    var chk_state = document.getElementById("chkAL").checked;
        //    var Total = 0;
        //    if (chk_state) {
        //    }
        //    else {

        //    }

        //    for (i = 0; i < document.forms[0].elements.length; i++) {
        //        if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
        //            if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {

        //                if (document.forms[0].elements[i].type == 'checkbox') {
        //                    if (document.forms[0].elements[i].disabled == false) {
        //                        document.forms[0].elements[i].checked = chk_state;
        //                        alert(document.forms[0].elements[15].value);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}


        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;



            if (chk_state) {

            }
            else {
                
            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkOutStation'){
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            if (document.forms[0].elements[i].disabled == false)
                            {
                                document.forms[0].elements[i].checked = chk_state;
                            }
                            
                        }
                        }
                    }
                }
            }
        }


        function getDriversName() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;


            pMode = "TPTDRIVERSCHOOL";
            
            url = "../../common/PopupSelect.aspx?id=" + pMode;

            result = radopen(url, "pop_up");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=h_Emp_id.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtOasis.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];--%>

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=h_Emp_id.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtOasis.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];
                __doPostBack('<%= txtOasis.ClientID%>', 'TextChanged');
            }
        }



        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Ex Gratia Charges
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">
                            <table align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="15" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">Ex Gratia Charges</span></font>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="5%"><span class="field-label">Date </span></td>
                                                <td width="40%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="30%">
                                                                <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                                <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                                    TargetControlID="txtDate" PopupButtonID="lnkDate">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                            <td width="60%">
                                                                <asp:DropDownList ID="ddlBusinessUnit" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="55%"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table width="100%" class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th>Oasis No
                                                </th>
                                                <th>Driver Name
                                                </th>

                                                <th>Driver Type
                                                </th>

                                                <th>Charging Unit
                                                </th>

                                                <th>Purpose
                                                </th>

                                                <th>OutStation?
                                                </th>

                                                <th>Other OT Amt.
                                                </th>

                                                <th>Time In
                                                </th>
                                                <th>Time Out
                                                </th>
                                                <th>Total Hrs.
                                                </th>
                                                <th>Total Amount
                                                </th>
                                                <th>Out Station
                                                </th>

                                                <th>Net Amount
                                                </th>


                                                <th align="left">Remarks
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtOasis" runat="server" Width="100px" AutoPostBack="true" align="left"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ImageDriver" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getDriversName();return false;" Height="16px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtDriverName" runat="server" Width="200px" Enabled="false" align="left"></asp:TextBox>

                                                </td>

                                                <td>
                                                    <asp:RadioButtonList ID="rblDriverType" runat="server" AutoPostBack="true"
                                                        RepeatDirection="Horizontal">
                                                        <asp:ListItem Selected="True" Value="1">Bus</asp:ListItem>
                                                        <asp:ListItem Value="2">Car</asp:ListItem>
                                                        <asp:ListItem Value="3">Van</asp:ListItem>
                                                        <asp:ListItem Value="4">Cond./SS</asp:ListItem>
                                                    </asp:RadioButtonList>

                                                </td>



                                                <td>
                                                    <%--<asp:TextBox ID="txtCBSU" runat="server" Width="60px"   align="left"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlBSU" Width="150px" runat="server"></asp:DropDownList>

                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadPurpose" runat="server" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                    <%--<asp:DropDownList ID="RadPurpose" runat="server" width="250px"></asp:DropDownList>--%>
                                                </td>

                                                <td>
                                                    <asp:CheckBox ID="chkOutStation" AutoPostBack="true" runat="server" Checked="false"></asp:CheckBox>

                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtoOTAmt" runat="server" Width="100px" Style="text-align: right;" Enabled="true"></asp:TextBox>
                                                </td>


                                                <td>
                                                    <asp:TextBox ID="txtTStart" runat="server" Width="100px" TabIndex="1"></asp:TextBox>
                                                </td>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtTStart" Mask="99:99" MaskType="Time" CultureName="en-us" MessageValidatorTip="true" runat="server"></cc1:MaskedEditExtender>
                                                <td>
                                                    <asp:TextBox ID="txtTEnd" runat="server" Width="100px" TabIndex="2"></asp:TextBox>
                                                </td>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtTEnd" Mask="99:99" MaskType="Time" CultureName="en-us" MessageValidatorTip="true" runat="server"></cc1:MaskedEditExtender>






                                                <td>
                                                    <asp:TextBox ID="txtTTotal" runat="server" Enabled="false" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                </td>



                                                <td>
                                                    <asp:TextBox ID="txtTotalAmount" runat="server" Width="100px" Style="text-align: right;" Enabled="false"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtOutStation" runat="server" Width="100px" Style="text-align: right;" Enabled="false"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtNetAmount" runat="server" Width="100px" Style="text-align: right;" Enabled="false"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TxtRemarks" runat="server" Width="200px" TabIndex="3"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="20%">
                                        <asp:Button ID="btnPost1" runat="server" CssClass="button m-0" Text="Post" Visible="false" />

                                        <asp:HiddenField ID="hdngross" runat="server" />
                                    </td>
                                    <td width="5%">
                                        <asp:Label ID="Postingdate" Text="Posting Date" Visible="false" runat="server"></asp:Label>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtPostingDate" runat="server" Visible="false"></asp:TextBox>
                                        <asp:ImageButton ID="ImgPosting" runat="server" Visible="false" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtPostingDate" PopupButtonID="ImgPosting">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="5%">
                                        <asp:Label ID="lblSelAmount" Visible="false" Text="Selected Amount" runat="server"></asp:Label>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtSelectedAmt" Visible="false" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="50%" align="right">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button m-0" Text="Save" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="bottom">
                            <table width="100%">
                                <tr>
                                    <td width="20%">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button m-0" Text="Load.." />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button m-0" Text="Print" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button m-0" Text="Cancel" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnRecall" runat="server" CausesValidation="False" CssClass="button m-0" Text="Re Call" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="button m-0" Text="Send for Approval" UseSubmitBehavior="False" />
                                        <asp:HyperLink ID="btnExport" runat="server">Export to Excel</asp:HyperLink>
                                    </td>
                                    <td width="3%">
                                        <span class="field-label">FromDate</span>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtFromDate" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFromDate" PopupButtonID="imgFromDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="3%">
                                        <span class="field-label">ToDate</span>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtToDate" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtToDate" PopupButtonID="imgToDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="50%"></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row mb-0"
                                Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="EGC_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText=" " >
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL"   onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                 
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("[EGC_ID]") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EGC_TRDATE" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle Width="80px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BSU_NAME" ItemStyle-Width="220" HeaderText="School" />
                                    <asp:BoundField DataField="EGC_C_BSU_SHORTNAME" HeaderText="Charging Unit" />
                                    <asp:BoundField DataField="DRIVER_EMPNO" HeaderText="Oasis ID" />
                                    <asp:BoundField DataField="DRIVER" ItemStyle-Width="180" HeaderText="Driver Name" />
                                    <asp:BoundField DataField="DriverType" HeaderText="Driver Type" />
                                    <asp:BoundField DataField="TMS_DESCR" HeaderText="Purpose" />
                                    <asp:BoundField DataField="EGC_OUTSTATION" HeaderText="Outstation Allowance?" />
                                    <asp:BoundField DataField="EGC_OOTAMOUNT" HeaderText="Other OT Amt." />
                                    <asp:BoundField DataField="EGC_TSTART" HeaderText="Time In" />
                                    <asp:BoundField DataField="EGC_TEND" HeaderText="Time Out" />
                                    <asp:BoundField DataField="EGC_TTOTAL" HeaderText="Total Time" />
                                    <asp:BoundField DataField="EGC_NETAMOUNT" HeaderText="Total Amount" />
                                    <asp:BoundField DataField="EGC_REMARKS" HeaderText="Remarks" />
                                    <asp:BoundField DataField="EGC_EGI_ID" HeaderText="Posted?" />
                                    <asp:BoundField DataField="EGC_FYEAR" HeaderText="Year" />
                                    <asp:BoundField DataField="EGC_USER" HeaderText="User" />

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblEGCID" runat="server" Text='<%# Eval("[EGC_ID]") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" OnClick="lnkPrint_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblEGC_invNo" runat="server" Text='<%# Eval("EGC_EGI_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="40%"></td>
                                    <td width="20%">
                                        <asp:Button ID="btnPost" runat="server" CssClass="button m-0" Text="Post" Visible="false" />
                                    </td>
                                    <td width="40%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="3">
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_EGC_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_rate1" runat="server" />
                            <asp:HiddenField ID="h_Emp_id" runat="server" Value="0" />
                            <asp:HiddenField ID="h_Holiday" runat="server" Value="0" />
                            <asp:HiddenField ID="h_DriverType" runat="server" Value="1" />
                            <asp:HiddenField ID="h_OT1_Hrs" runat="server" Value="" />
                            <asp:HiddenField ID="h_OT2_Hrs" runat="server" Value="" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />

</asp:Content>


