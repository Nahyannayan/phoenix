﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptDriversLeave.aspx.vb" Inherits="Transport_ExtraHiring_tptDriversLeave" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function getDriverName() {
          
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            

            var RB1 = document.getElementById("<%=rblData.ClientID%>");
            var radio = RB1.getElementsByTagName("input");
            var label = RB1.getElementsByTagName("label");
            for (var x = 0; x < radio.length; x++) {
                if (radio[x].checked) {
                    var jobTypeSelected = label[x].innerHTML;
                }
            }
            if (jobTypeSelected == 'School')
                pMode = "TPTDRIVERSCHOOL";
            else
                pMode = "TPTDRIVERNEW"

            url = "../../common/PopupSelect.aspx?id=" + pMode + "&menu=T200123";
            result = radopen(url, "pop_up");
          <%--  if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtEmpNo.ClientID %>").value = NameandCode[1];
    document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[2];
            document.getElementById("<%=h_emp_id.ClientID %>").value = NameandCode[0];--%>
        }



        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtEmpNo.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtEmpName.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=h_emp_id.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID %>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Driver Leave Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle"><asp:Label runat="server" id="rptHeader" Text="Driver Leave Details"></asp:Label></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Job Type</span></td>

                                    <td align="left" style="width: 30%">
                                        <asp:RadioButtonList ID="rblData" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="0">NES</asp:ListItem>
                                            <asp:ListItem Value="1">School</asp:ListItem>
                                            <asp:ListItem Value="2">SubCon</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>


                                    <td align="left" width="20%"><span class="field-label">Driver Name</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEmpNo" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtEmpName" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getDriverName(); return false;" TabIndex="8" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFromDate" PopupButtonID="imgFromDate">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtToDate" PopupButtonID="imgToDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="h_emp_id" Value="" runat="server" />
                            <asp:HiddenField ID="hGridDelete" Value="0" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
