﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptExgratiaNES.aspx.vb" Inherits="Transport_ExtraHiring_NESExgratiaDetails" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<style type="text/css">
        .autocomplete_highlightedListItem_S1
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }
    </style>--%>
    
    <script language="javascript" type="text/javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length - 1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function parseTime(s) {
            var c = s.split(':');
            return parseInt(c[0]) * 60 + parseInt(c[1]);
        }


        function calc(Type, rate) {
            a = document.getElementById("<%=txtTStart.ClientID %>").value;
            b = document.getElementById("<%=txtTEnd.ClientID %>").value;
            e = document.getElementById("<%=txtTStart.ClientID %>").value;
            var Rate = document.getElementById("<%=h_Rate.ClientID %>").value;
            var Rate1 = 0;

            var Drivertype = document.getElementById("<%=h_DriverType.ClientID %>").value;
            p = "1/1/1970 ";
            var hol = document.getElementById("<%=h_Holiday.ClientID%>").value;
            var TotalHrs = document.getElementById("<%=h_TotalHrs.ClientID%>").value;
            if (b == "24:00") {
                b = '23:59'
            }
            //                else{b}
            difference = new Date(new Date(p + b) - new Date(p + a)).toUTCString().split(" ")[4];
            var diff = new Date(new Date(p + b) - new Date(p + a));
            var hours = Math.floor(diff / (1000 * 60 * 60));
            diff -= hours * (1000 * 60 * 60);
            var mins = Math.floor(diff / (1000 * 60));
            diff -= mins * (1000 * 60);

            if (b == "23:59") {
                mins = mins + 1
            }

            if (mins > 30) {
                hours = hours + 1;
            }
            else if (mins > 0) {
                hours = hours + 0.5;
            }
            else {
                hours = hours + 0.0;
            }
            if (a > b) {
                hours = 24 + hours
            }
            document.getElementById("<%=txtTTotal.ClientID %>").value = hours;

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            var Total = 0;
            if (chk_state) {
            }
            else {

            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {

                        if (document.forms[0].elements[i].type == 'checkbox') {
                            if (document.forms[0].elements[i].disabled == false) {
                                document.forms[0].elements[i].checked = chk_state;
                                alert(document.forms[0].elements[15].value);
                            }
                        }
                    }
                }
            }
        }







        function getDriversName() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;


            pMode = "TPTDRIVERSCHOOL";

            url = "../../common/PopupSelect.aspx?id=" + pMode;

            result = radopen(url, "pop_up");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=h_Emp_id.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtOasis.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];--%>

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=h_Emp_id.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtOasis.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];
                __doPostBack('<%= txtOasis.ClientID %>', 'TextChanged');
            }
        }

        function getPurposeList() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            var DriverType;


            pMode = "TPTPURPOSE";
            DriverType = document.getElementById("<%=h_DriverType.ClientID %>").value;


            url = "../../common/PopupSelect.aspx?id=" + pMode + "&DriverType=" + DriverType;

            result = radopen(url, "pop_up2");
                   <%-- if (result == '' || result == undefined) {
                        return false;
                    }
                    NameandCode = result.split('___');
                    document.getElementById("<%=txtPurpose.ClientID %>").value = NameandCode[2];--%>
        }


        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtPurpose.ClientID %>").value = NameandCode[2];
                __doPostBack('<%= txtPurpose.ClientID%>', 'TextChanged');
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Ex Gratia Charges (NES/VILLA/IIT)
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="13" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Ex Gratia Charges (NES/VILLA/IIT)</span></font>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="10%"><span class="field-label">Date </span></td>
                                                <td width="20%">
                                                    <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"
                                                        Width="16px"></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                        TargetControlID="txtDate" PopupButtonID="lnkDate">
                                                    </ajaxToolkit:CalendarExtender>

                                                </td>
                                                <td width="70%"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table width="100%" class="table table-bordered table-row">
                                            <tr>
                                                <th>Oasis No
                                                </th>
                                                <th>Driver Name
                                                </th>

                                                <th>Driver Type
                                                </th>
                                                <th>Purpose
                                                </th>

                                                <th>Holiday?
                                                </th>
                                                <th>Out Station ?
                                                </th>
                                                <th>Over Night Stay ?
                                                </th>

                                                <th>Time In
                                                </th>
                                                <th>Time Out
                                                </th>
                                                <th>Total Hrs.
                                                </th>
                                                <%--                        
                        <td >
                            Net Amount
                        </td>
                                                --%>

                                                <th align="left">Remarks
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtOasis" runat="server" AutoPostBack="true" align="left"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageDriver" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getDriversName();return false;" />
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtDriverName" runat="server" Width="180px" Enabled="false" align="left"></asp:TextBox>

                                                </td>

                                                <td>
                                                    <%-- <asp:RadioButtonList ID="rblDriverType" runat="server" AutoPostBack="true" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="1">NES</asp:ListItem>
                    <asp:ListItem Value="2">VILLA</asp:ListItem>
                    <asp:ListItem Value="3">IIT</asp:ListItem>
                                                    --%>
                                                    <%--</asp:RadioButtonList>--%>
                                                    <table>
                                                        <td>
                                                            <asp:RadioButton ID="rbNES" Text="NES" runat="server" GroupName="Rad" AutoPostBack="True" Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <asp:RadioButton ID="rbVILLA" Text="VILLA" runat="server" GroupName="Rad" AutoPostBack="True" Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <asp:RadioButton ID="rbIIT" Text="IIT" runat="server" GroupName="Rad" AutoPostBack="True" Enabled="false" />
                                                        </td>
                                                    </table>




                                                </td>

                                                <td>
                                                    <%--<telerik:RadComboBox ID="RadPurpose" runat="server" AutoPostBack ="true" >
                  </telerik:RadComboBox>--%>
                                                    <asp:TextBox ID="txtPurpose" runat="server" AutoPostBack="true" align="left"></asp:TextBox>
                                                    <asp:ImageButton ID="ImagePurpose" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getPurposeList();return false;" />
                                                </td>

                                                <td>
                                                    <asp:CheckBox ID="chkHoliday" AutoPostBack="true" runat="server" Checked="false"></asp:CheckBox>

                                                </td>

                                                <td>
                                                    <asp:CheckBox ID="chkOutStation" AutoPostBack="true" runat="server" Checked="false"></asp:CheckBox>

                                                </td>

                                                <td>
                                                    <asp:CheckBox ID="chkNightStay" AutoPostBack="true" runat="server" Checked="false"></asp:CheckBox>

                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtTStart" runat="server" Width="100px" TabIndex="1"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtTStart" Mask="99:99" MaskType="Time" CultureName="en-us" MessageValidatorTip="true" runat="server"></cc1:MaskedEditExtender>
                                                </td>


                                                <td>
                                                    <asp:TextBox ID="txtTEnd" runat="server" Width="100px" TabIndex="2"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtTEnd" Mask="99:99" MaskType="Time" CultureName="en-us" MessageValidatorTip="true" runat="server"></cc1:MaskedEditExtender>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtTTotal" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                                </td>

                                                <%--   <td >
                            <asp:TextBox ID="txtNetAmount" runat="server"  Enabled ="false"  ></asp:TextBox>
                        </td>--%>

                                                <td>
                                                    <asp:TextBox ID="TxtRemarks" runat="server" Width="200px" TabIndex="3"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnPost1" runat="server" CssClass="button" Text="Post" Visible="false" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />

                                        <asp:HiddenField ID="hdngross" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Postingdate" Text="Posting Date" Visible="false" runat="server" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtPostingDate" runat="server" Visible="false"></asp:TextBox>
                                        <asp:ImageButton ID="ImgPosting" runat="server" Visible="false" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtPostingDate" PopupButtonID="ImgPosting">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSelAmount" Visible="false" Text="Selected Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSelectedAmt" Visible="false" runat="server"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <table width="100%">
                                <tr>
                                    <td width="30%">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Load.." />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnRecall" runat="server" CausesValidation="False" CssClass="button m-0" Text="Re Call" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="button m-0" Text="Send for Approval" UseSubmitBehavior="False" />
                                        

                                        <asp:HyperLink ID="btnExport" runat="server">Export to Excel</asp:HyperLink>
                                    </td>
                                    <td width="5%"><span class="field-label">FromDate</span>
                                    </td>
                                    <td width="15%">
                                        <asp:TextBox ID="txtFromDate" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFromDate" PopupButtonID="imgFromDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="5%"><span class="field-label">ToDate</span>
                                    </td>
                                    <td width="15%">
                                        <asp:TextBox ID="txtToDate" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtToDate" PopupButtonID="imgToDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="30%"></td>                                   
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row mb-0"
                                Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="EGN_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText=" " Visible="true">
                                        <HeaderTemplate>

                                            <asp:CheckBox ID="chkAL" AutoPostBack="true" runat="server" Visible="True" OnCheckedChanged="chkAL_CheckedChanged"></asp:CheckBox>
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" AutoPostBack="true" Visible="True" runat="server"></asp:CheckBox>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("[EGN_ID]") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EGN_TRDATE" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BSU_NAME" ItemStyle-Width="150" HeaderText="School" />
                                    <asp:BoundField DataField="DRIVER_EMPNO" HeaderText="Oasis ID" />
                                    <asp:BoundField DataField="DRIVER" ItemStyle-Width="220" HeaderText="Driver Name" />
                                    <asp:BoundField DataField="DriverType" HeaderText="Driver Type" />
                                    <asp:BoundField DataField="EGN_OUTSTATION" HeaderText="OutStation" />
                                    <asp:BoundField DataField="EGN_OVERNIGHTSTAY" HeaderText="OverNightStay" />
                                    <asp:BoundField DataField="TMS_DESCR" HeaderText="Purpose" />
                                    <asp:BoundField DataField="EGN_TSTART" HeaderText="Time In" />
                                    <asp:BoundField DataField="EGN_TEND" HeaderText="Time Out" />
                                    <asp:BoundField DataField="EGN_TTOTAL" HeaderText="Total Time" />
                                    <%--<asp:BoundField DataField="EGN_NETAMOUNT" HeaderText="Total Amount" />--%>
                                    <asp:BoundField DataField="EGN_REMARKS" HeaderText="Remarks" />
                                    <asp:BoundField DataField="EGN_EGI_ID" HeaderText="Posted?" />
                                    <asp:BoundField DataField="EGN_FYEAR" HeaderText="Year" />
                                    <asp:BoundField DataField="EGN_USER" HeaderText="User" />

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblEGCID" runat="server" Text='<%# Eval("[EGN_ID]") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" OnClick="lnkPrint_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblEGC_invNo" runat="server" Text='<%# Eval("EGN_EGI_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr> 
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="40%"></td>
                                    <td width="20%" align="center">
                                        <asp:Button ID="btnPost" runat="server" CssClass="button m-0" Text="Post" Visible="false" />
                                    </td>
                                    <td width="40%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_EGN_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_Emp_id" runat="server" Value="0" />
                            <asp:HiddenField ID="h_Holiday" runat="server" Value="0" />
                            <asp:HiddenField ID="h_TotalHrs" runat="server" Value="0" />
                            <asp:HiddenField ID="h_DriverType" runat="server" Value="1" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
</asp:Content>


