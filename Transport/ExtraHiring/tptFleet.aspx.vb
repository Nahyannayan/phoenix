Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Net
Imports System.Xml
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Text
Imports Telerik.Web.UI

Partial Class tptFleet
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim Discounted As Boolean = True
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = CObj(Page.Master).FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnUpload)

            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim USR_NAME As String = Session("sUsr_name")
                Dim CurBsUnit As String = Session("sBsuid")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200120" And ViewState("MainMnu_code") <> "T200130" And ViewState("MainMnu_code") <> "T200135" And ViewState("MainMnu_code") <> "T200140" And ViewState("MainMnu_code") <> "T200141" And ViewState("MainMnu_code") <> "T200190") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                bindVATCodes()


                If Request.QueryString("viewid") <> "" Then
                    If ViewState("MainMnu_code") = "T200140" Then
                        SetDataMode("view")
                        setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    Else
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        SetDataMode("edit")
                        setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    End If
                    grdInvoice.DataSource = TPTHiringInvoice
                    grdInvoice.DataBind()
                    showNoRecordsFound()
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If
            End If
            If hGridRefresh.Value = 1 Then
                grdInvoice.DataSource = TPTHiringInvoice
                grdInvoice.DataBind()
                hGridRefresh.Value = 0
            End If
            showNoRecordsFound()
            hGridRefresh.Value = 0

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Sub BindVATCodes()
        Dim strsql2 As String = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
        Dim strsql1 As String = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        radVat.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
        radVat.DataTextField = "DESCR"
        radVat.DataValueField = "ID"
        radVat.DataBind()

        RadEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql1)
        RadEmirate.DataTextField = "DESCR"
        RadEmirate.DataValueField = "ID"
        RadEmirate.DataBind()

    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            txtTrn_No.Text = ""
            h_EntryId.Value = 0
            txtTPT_Date.Text = Format(Now, "dd-MMM-yyyy")
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"


        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        lblTotalKm.Enabled = False
        lblTotalTime.Enabled = False
        txtTPT_JobSt_km.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtTPT_JOBCl_km.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtTPT_JobSt_km.Attributes.Add("onblur", " return calcKm();")
        txtTPT_JOBCl_km.Attributes.Add("onblur", " return calcKm();")
        txtTPT_STSArr_km.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtTPT_STSSt_km.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtTPT_JOBCl_Time.Attributes.Add("onblur", " return calcTime();")
        txtTPT_JobSt_Time.Attributes.Add("onblur", " return calcTime();")
        txtLPO_No.Enabled = EditAllowed
        txtSTS_No.Enabled = EditAllowed
        txtTPT_Date.Enabled = EditAllowed
        txtLocation.Enabled = EditAllowed
        txtVehicleNo.Enabled = EditAllowed
        txtDriversName.Enabled = EditAllowed
        txtClientName.Enabled = EditAllowed
        txtService.Enabled = EditAllowed
        txtTPT_Description.Enabled = EditAllowed
        rblJobType.Enabled = EditAllowed

        txtSupplier.Enabled = EditAllowed
        txtDriver.Enabled = EditAllowed
        txtVehRegNo.Enabled = EditAllowed

        txtPickupPoint.Enabled = EditAllowed
        txtDropOffPoint.Enabled = EditAllowed
        txtTPT_STSSt_Time.Enabled = EditAllowed
        txtTPT_JobSt_Time.Enabled = EditAllowed
        txtTPT_STSSt_km.Enabled = EditAllowed
        txtTPT_JOBCl_Time.Enabled = EditAllowed
        txtTPT_STSArr_Time.Enabled = EditAllowed
        txtTPT_JobSt_km.Enabled = EditAllowed
        txtTPT_STSArr_km.Enabled = EditAllowed
        txtTPT_JOBCl_km.Enabled = EditAllowed

        txtTPT_Client_RefNo.Enabled = EditAllowed
        txtTPT_Remarks.Enabled = EditAllowed
        txtTPT_Add_Service.Enabled = EditAllowed
        txtTPT_Xtra_Hrs.Enabled = EditAllowed
        txtTPT_Feedback.Enabled = EditAllowed
        txtTRN_Veh_CatId.Enabled = False
        txtTRN_SalesMan.Enabled = EditAllowed
        txtSeason.Enabled = EditAllowed
        imgClient.Enabled = EditAllowed
        imgDriver.Enabled = EditAllowed
        imgLocation.Enabled = EditAllowed
        imgService.Enabled = EditAllowed
        imgTPT_Date.Enabled = EditAllowed
        imgVehicle.Enabled = EditAllowed
        imgVehType.Enabled = EditAllowed
        ImgSeason.Enabled = EditAllowed
        grdInvoice.Columns(8).Visible = Not mDisable
        grdInvoice.Columns(7).Visible = Not mDisable
        grdInvoice.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnPrint.Visible = mDisable
        btnPProforma.ValidationGroup = mDisable
        btnSave.Visible = Not ItemEditMode And Not mDisable And ViewState("MainMnu_code") <> "T200135"
        btnFOC.Visible = Not mDisable And ViewState("MainMnu_code") = "T200135"
        btnEdit.Visible = mDisable
        'Dim Yearend As Date
        'Dim BsuFreezdt As Date
        'Yearend = Mainclass.getDataValue("select fyr_todt from oasis..financialyear_s where fyr_id='" & Session("F_YEAR") & "'", "OASIS_TRANSPORTConnectionString")
        'BsuFreezdt = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")

        'If BsuFreezdt <= Yearend Then
        'btnAdd.Visible = True
        'Else
        'btnAdd.Visible = False
        'End If
        btnAdd.Visible = False




    End Sub
    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If ViewState("MainMnu_code") <> "T200135" Then
                trfoc.Visible = False
                trfoc1.Visible = False
            End If

            'Dim Yearend As Date
            'Dim BsuFreezdt As Date
            'Yearend = Mainclass.getDataValue("select fyr_todt from oasis..financialyear_s where fyr_id='" & Session("F_YEAR") & "'", "OASIS_TRANSPORTConnectionString")
            'BsuFreezdt = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            'If BsuFreezdt <= Yearend Then
            'btnAdd.Visible = True
            'Else
            'btnAdd.Visible = False
            'End If
            btnAdd.Visible = False


            If Not ("900500900501").ToString.Contains(Session("sBsuid")) Then
                rblJobType.Items(0).Selected = False
                rblJobType.Items(1).Selected = True
                rblJobType.Enabled = False
                txtClientName.Enabled = False
                imgClient.Enabled = False
                lblSalesMan.Text = "CC"
            End If

            If p_Modifyid = 0 Then
                If Not ("900500900501").ToString.Contains(Session("sBsuid")) Then
                    hTPT_Client_ID.Value = Session("sBsuid")
                    Dim bsu_Details() As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select bsu_name+';'+isnull(cast(LOC_ID AS varchar),'')+';'+isnull(LOC_DESCRIPTION,'') from oasis.dbo.BUSINESSUNIT_M left OUTER JOIN TRANSPORT.LOCATION_M on BSU_CITY=LOC_CODE where bsu_id='" & Session("sBsuid") & "'").ToString.Split(";")
                    txtClientName.Text = bsu_Details(0)
                    hTPT_Loc_ID.Value = bsu_Details(1)
                    txtLocation.Text = bsu_Details(2)
                    txtTRN_SalesMan.Text = Session("sUsr_name")
                End If

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                'strSql.Append("select Distinct replace(convert(varchar(30), TPT_JobDate, 106),' ','/') TPT_JOBDATE,replace(convert(varchar(30), TPT_Date, 106),' ','/') TPTDATE, LOC_DESCRIPTION Location, isnull(isnull(TPTVEHICLE_M.VEH_REGNO,VEHICLE_M.VEH_REGNO),'') VehicleNo, case when tpt_data=2 then '' else case when TPT_DATA=1 THEN ISNULL(CAT_DESCRIPTION,'')  else case when TPT_DATA=0 then isnull(VEH_TYPE,'') else '' end end end  VehicleName , case when TPT_DATA=1 THEN ISNULL(VEHICLE_M.VEH_CAPACITY,0)  else isnull(TPTVEHICLE_M.VEH_CAPACITY,0) end  Capacity, isnull(isnull(TPTD_NAME,DRIVER),'') DriversName, isnull(TPT_BllId,0) h_TPT_BllId, ")
                'strSql.Append("select Distinct TPTHiring.*, replace(convert(varchar(30), TPT_JobDate, 106),' ','/') TPT_JOBDATE,replace(convert(varchar(30), TPT_Date, 106),' ','/') TPTDATE, LOC_DESCRIPTION Location, isnull(isnull(TPTVEHICLE_M.VEH_REGNO,VEHICLE_M.VEH_REGNO),'') VehicleNo, case when tpt_data=2 then '' else case when TPT_DATA=1 THEN ISNULL(CAT_DESCRIPTION,'')  else case when TPT_DATA=0 then isnull(VEH_TYPE,'') else '' end end end  VehicleName , case when TPT_DATA=1 THEN ISNULL(VEHICLE_M.VEH_CAPACITY,0)  else isnull(TPTVEHICLE_M.VEH_CAPACITY,0) end  Capacity, isnull(isnull(TPTD_NAME,DRIVER),'') DriversName, isnull(TPT_BllId,0) h_TPT_BllId, ")
                strSql.Append("select Distinct TRN_no, LPO_no, STS_No, TPT_date, TPT_Loc_id, TPT_Veh_id, TPT_Drv_id, TPT_Client_id, TPT_Service_id, TPT_Description, TPT_STSSt_Time, TPT_JobSt_Time, TPT_STSSt_km, TPT_JOBCl_Time, TPT_STSArr_Time, TPT_JobSt_km, TPT_STSArr_km, TPT_JOBCl_km, TPT_Client_RefNo, TPT_Remarks, TPT_Add_Service, TPT_Xtra_Hrs, TPT_Feedback, TPT_QuoteFlag, TPT_QuoteDate, TPT_JobFlag, TPT_JobDate, TPT_InvFlag, TPT_InvDate, TPT_Bsu_id, TPT_BllId, TPT_JobType, TRN_number, TRN_numberstr, TRN_Veh_CatId, TRN_JobnumberStr, TRN_Status, TRN_Salesman, TPT_PickupPoint, TPT_DropOffPoint, TPT_Capacity, TRN_SEAS_ID, TPT_ENQUIRY, TRN_TRL_ID, TPT_SUPPLIER, TPT_DRIVER, TPT_VEHREGNO, TPT_DATA, TPT_SC_VEHTYPE, TPT_SC_CAPACITY, TRN_BATCHDATE, TRN_BATCHSTR, TRN_BATCH, TPT_ADDRESS, TPT_JHD_DOCNO, TPT_STAFFNAME, TPT_FYEAR, TPT_Deleted, TPT_Cancel_Remarks, TPT_bEMAILED, TPT_Declined_Remarks, TPT_PRGM_DIM_CODE, isnull(TPT_TAX_AMOUNT,0)TPT_TAX_AMOUNT, isnull(TPT_TAX_NET_AMOUNT,0)TPT_TAX_NET_AMOUNT, TPT_TAX_CODE, TPT_EMR_CODE,")
                strSql.Append("replace(convert(varchar(30), TPT_JobDate, 106),' ','/') TPT_JOBDATE,replace(convert(varchar(30), TPT_Date, 106),' ','/') TPTDATE, LOC_DESCRIPTION Location,")
                'strSql.Append("isnull(isnull(TPTVEHICLE_M.VEH_REGNO,VEHICLE_M.VEH_REGNO),'') VehicleNo, case when tpt_data=2 then '' else case when TPT_DATA=1 THEN ISNULL(CAT_DESCRIPTION,'')  else case when TPT_DATA=0 then isnull(VEH_TYPE,'') else '' end end end  VehicleName , case when TPT_DATA=1 THEN ISNULL(VEHICLE_M.VEH_CAPACITY,0)  else isnull(TPTVEHICLE_M.VEH_CAPACITY,0) end  Capacity, ")
                strSql.Append("isnull(VEHICLE_M.VEH_REGNO,'')VehicleNo,ISNULL(CAT_DESCRIPTION,'')VehicleName,ISNULL(VEHICLE_M.VEH_CAPACITY,0)Capacity,")
                strSql.Append("isnull(ACT_NAME,BSU_NAME) ClientName, isnull(TSM_DESCR,'') [SERVICE],isnull(SEAS_NAME,'') [SEASON], case when TPT_JobDate is null then 0 else 1 end jobflag, case when TPT_InvDate is null then 0 else 1 end invflag, isnull((select act_name from oasisfin..accounts_m where act_id=tpt_supplier),'') TPT_SUPPLIERNAME, isnull(isnull(TPTD_NAME,DRIVER),'') DriversName, isnull(TPT_BllId,0) h_TPT_BllId, ")
                strSql.Append("isnull((select sum(inv_amt) from TPTHiringInvoice where inv_trn_no=trn_no),0) totalbillingamt, isnull(SLSM_NAME,'') SALESMAN, isnull(VTYP_DESCRIPTION,'') VEH_CATEGORY ")
                strSql.Append(",isnull(TRL_DESCR,'') [RATETYPE],case when isnull(TPT_EMR_CODE,'')='' then case when isnull(ACT_EMR_CODE,'')='' then 'NA' else ACT_EMR_CODE end else TPT_EMR_CODE end  TPT_EMR_CODENEW,isnull(TPT_TAX_CODE,'NA') [TPT_TAX_CODENEW] FROM TPTHiring ")
                strSql.Append("LEFT OUTER JOIN TPTVEHICLE_M ON VEH_ID=TPT_Veh_id ")
                strSql.Append("LEFT OUTER JOIN oasis.TAX.VW_TAX_EMIRATES on EMR_CODE=TPT_EMR_CODE ")
                strSql.Append("LEFT OUTER JOIN VEHICLE_M on VEHICLE_M.VEH_ID=TPT_VEH_ID ")
                strSql.Append("LEFT OUTER JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID ")
                strSql.Append("LEFT OUTER JOIN TPTDRIVER_M ON TPTD_ID=TPT_Drv_id ")
                strSql.Append("LEFT OUTER JOIN (SELECT  distinct driver,DRIVER_EMPID FROM VW_DRIVER ) AS tmp ON TPTHiring.TPT_Drv_id = tmp.DRIVER_EMPID ")
                strSql.Append("LEFT OUTER JOIN TPTSALESMAN ON SLSM_ID=TRN_Salesman ")
                strSql.Append("LEFT OUTER JOIN VEHTYPE_M ON VTYP_ID=TRN_Veh_CatId  ")
                strSql.Append("LEFT OUTER JOIN TPTSeason ON SEAS_ID=TRN_SEAS_ID ")
                strSql.Append("LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id ")
                strSql.Append("LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID ")
                strSql.Append("LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id ")
                strSql.Append("LEFT OUTER JOIN TPT_RATELIST on TRL_ID=TRN_TRL_ID ")
                strSql.Append("LEFT OUTER JOIN CATEGORY_M ON CAT_ID=VEHICLE_M.VEH_CAT_ID ")
                strSql.Append("where TRN_No=" & h_EntryId.Value)
                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtTrn_No.Text = dt.Rows(0)("TRN_JobnumberStr")
                    txtSTS_No.Text = dt.Rows(0)("STS_No")
                    txtLPO_No.Text = dt.Rows(0)("LPO_No")
                    txtTPT_Date.Text = Format(dt.Rows(0)("TPT_JOBDATE"), "dd/MMM/yyyy")
                    lblTrDate.Text = "Tr.Date:" + dt.Rows(0)("TPTDATE")
                    txtLocation.Text = dt.Rows(0)("Location")
                    hTPT_Client_ID.Value = dt.Rows(0)("TPT_Client_ID")
                    txtDropOffPoint.Text = dt.Rows(0)("TPT_DropOffPoint")
                    hTPT_Drv_ID.Value = dt.Rows(0)("TPT_Drv_ID")
                    hTPT_Loc_ID.Value = dt.Rows(0)("TPT_Loc_ID")
                    txtPickupPoint.Text = dt.Rows(0)("TPT_PickupPoint")
                    hTPT_Service_ID.Value = dt.Rows(0)("TPT_Service_ID")
                    hTPT_Veh_ID.Value = dt.Rows(0)("TPT_Veh_ID")
                    h_TPT_BllId.Value = dt.Rows(0)("h_TPT_BllId")
                    txtDriversName.Text = dt.Rows(0)("DriversName")
                    txtSupplier.Text = dt.Rows(0)("TPT_SUPPLIERNAME")
                    hdnSupplier.Value = dt.Rows(0)("TPT_SUPPLIER")
                    txtDriver.Text = dt.Rows(0)("TPT_DRIVER")
                    txtVehRegNo.Text = dt.Rows(0)("TPT_VEHREGNO")
                    txtVehType.Text = dt.Rows(0)("TPT_SC_VEHTYPE")
                    txtVehCapacity.Text = dt.Rows(0)("TPT_SC_CAPACITY")
                    txtVehicleNo.Text = dt.Rows(0)("VehicleNo")
                    txtVehicleName.Text = dt.Rows(0)("VehicleName")
                    txtCapacity.Text = dt.Rows(0)("Capacity")
                    txtCapacityStr.Text = dt.Rows(0)("TPT_Capacity")
                    txtClientName.Text = dt.Rows(0)("ClientName")
                    txtService.Text = dt.Rows(0)("Service")
                    txtTPT_Description.Text = dt.Rows(0)("TPT_Description")
                    txtSeason.Text = dt.Rows(0)("SEASON").ToString
                    hSeason.Value = dt.Rows(0)("TRN_SEAS_ID").ToString
                    h_EnqMethod.Value = dt.Rows(0)("TPT_ENQUIRY").ToString
                    ReadRBEnqMethosValues()
                    'lblref.Text = "Ref. No :" & dt.Rows(0)("LPO_no").ToString
                    txtLPO_No.Text = dt.Rows(0)("LPO_no").ToString
                    txtSTS_No.Text = dt.Rows(0)("STS_No").ToString
                    rblData.Text = dt.Rows(0)("TPT_DATA").ToString
                    txtPickupPoint.Text = dt.Rows(0)("TPT_PickupPoint")
                    txtDropOffPoint.Text = dt.Rows(0)("TPT_DropOffPoint")
                    txtTPT_STSSt_Time.Text = dt.Rows(0)("TPT_STSSt_Time")
                    txtTPT_JobSt_Time.Text = dt.Rows(0)("TPT_JobSt_Time")
                    txtTPT_STSSt_km.Text = dt.Rows(0)("TPT_STSSt_km")
                    txtTPT_JOBCl_Time.Text = dt.Rows(0)("TPT_JOBCl_Time")
                    txtTPT_STSArr_Time.Text = dt.Rows(0)("TPT_STSArr_Time")
                    txtTPT_JobSt_km.Text = dt.Rows(0)("TPT_JobSt_km")
                    txtTPT_STSArr_km.Text = dt.Rows(0)("TPT_STSArr_km")
                    txtTPT_JOBCl_km.Text = dt.Rows(0)("TPT_JOBCl_km")
                    rblJobType.Items(0).Selected = (dt.Rows(0)("TPT_JobType") = "1")
                    rblJobType.Items(1).Selected = (dt.Rows(0)("TPT_JobType") = "0")
                    txtTPT_Client_RefNo.Text = dt.Rows(0)("TPT_Client_RefNo")
                    txtTPT_Remarks.Text = dt.Rows(0)("TPT_Remarks")
                    txtTPT_Add_Service.Text = dt.Rows(0)("TPT_Add_Service")
                    txtTPT_Xtra_Hrs.Text = dt.Rows(0)("TPT_Xtra_Hrs")
                    txtTPT_Feedback.Text = dt.Rows(0)("TPT_Feedback")
                    txtBillingTotal.Text = dt.Rows(0)("totalbillingamt")
                    txtTRN_SalesMan.Text = dt.Rows(0)("salesman")
                    hdnTRN_SalesMan.Value = dt.Rows(0)("TRN_Salesman")
                    txtTRN_Veh_CatId.Text = dt.Rows(0)("veh_Category")
                    hdnTRN_Veh_CatId.Value = dt.Rows(0)("TRN_Veh_CatId")
                    h_TrnStatus.Value = dt.Rows(0)("TRN_STATUS")

                    Dim lstemirate As New RadComboBoxItem
                    lstemirate = RadEmirate.Items.FindItemByValue(dt.Rows(0)("TPT_EMR_CODENEW"))
                    If Not lstemirate Is Nothing Then
                        RadEmirate.SelectedValue = lstemirate.Value
                    Else
                        RadEmirate.SelectedValue = "NA"
                    End If

                    Dim lstVAT As New RadComboBoxItem
                    lstVAT = radVat.Items.FindItemByValue(dt.Rows(0)("TPT_TAX_CODENEW"))
                    If Not lstVAT Is Nothing Then
                        radVat.SelectedValue = lstVAT.Value
                    Else
                        radVat.SelectedValue = "NA"
                    End If



                    If dt.Rows(0)("TRN_TRL_ID").ToString = "" Then
                        hRateType.Value = Mainclass.getDataValue("SELECT  top 1 CNH_TRL_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & hTPT_Client_ID.Value & ",%'", "OASIS_TRANSPORTConnectionString")
                        txtRateType.Text = Mainclass.getDataValue("select TRL_DESCR FROM TPT_RATELIST WHERE TRL_ID=" & hRateType.Value & "", "OASIS_TRANSPORTConnectionString")
                    Else
                        hRateType.Value = dt.Rows(0)("TRN_TRL_ID").ToString
                        txtRateType.Text = dt.Rows(0)("RATETYPE").ToString
                    End If

                    If dt.Rows(0)("TPT_JobSt_Time").ToString.Length > 0 Then
                        Dim stTime As Integer = dt.Rows(0)("TPT_JobSt_Time").split(":")(0) * 60 + dt.Rows(0)("TPT_JobSt_Time").split(":")(1)
                        Dim clTime As Integer = dt.Rows(0)("TPT_JobCl_Time").split(":")(0) * 60 + dt.Rows(0)("TPT_JobCl_Time").split(":")(1)
                        lblTotalKm.Text = dt.Rows(0)("TPT_JOBCl_km") - dt.Rows(0)("TPT_JobSt_km")
                        lblTotalTime.Text = Format(CType((clTime - stTime) / 60.0, Integer), "00") & ":" & Format((clTime - stTime) - CType((clTime - stTime) / 60.0, Integer) * 60.0, "00")
                        FormatFigures()

                    Else 'conversion from job order 
                        txtClientName.Enabled = False
                        txtLocation.Enabled = False
                        imgLocation.Enabled = False

                    End If
                    btnUpload.Visible = True
                    UploadDocPhoto.Visible = True

                    If h_TrnStatus.Value = "F" Then
                        btnFOC.Visible = False
                    End If




                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
            If rblJobType.Items(0).Selected Then
                fillGridView(grdInvoice, "select INV_Id, PROD_NAME, INV_CN_ID, INV_TRN_NO, INV_PROD_ID, INV_Rate, INV_Qty, INV_Amt from TPTHiringInvoice INNER JOIN TPTHiringProduct ON INV_PROD_ID=PROD_ID where INV_TRN_NO=" & h_EntryId.Value & " order by INV_Id")
            Else
                fillGridView(grdInvoice, "select INV_Id, 'Rate per km' PROD_NAME, INV_CN_ID, INV_TRN_NO, INV_PROD_ID, INV_Rate, INV_Qty, INV_Amt from TPTHiringInvoice INNER JOIN TPTCONTRACT_B ON INV_PROD_ID=CNB_ID where INV_TRN_NO=" & h_EntryId.Value & " order by INV_Id")
            End If
            CalculateTotalwithExempte()
            'CalculateVAT(LTrim(RTrim(txtTPT_Date.Text)), h_Total.Value, hTPT_Client_ID.Value)

            CalculateVAT(LTrim(RTrim(txtTPT_Date.Text)), h_Total.Value, hTPT_Client_ID.Value)
            If ViewState("MainMnu_code") = "T200135" Or ((ViewState("MainMnu_code") = "T200140") And (h_TrnStatus.Value = "P" Or h_TrnStatus.Value = "A" Or h_TrnStatus.Value = "I")) Then
                btnEdit.Visible = False
                btnPrint.Visible = False
            End If
            If ViewState("MainMnu_code") = "T200190" Then
                LblQty.Visible = True
                txtQty.Visible = True
            Else
                LblQty.Visible = False
                txtQty.Visible = False
            End If


            Dim iji_filename As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "select THI_FILENAME+';'+THI_CONTENTTYPE+';'+THI_NAME from TPTHiring_I where THI_DOCNO='" & txtTrn_No.Text & "' and THI_DELETE=0")
            If Not iji_filename Is Nothing Then
                hdnFileName.Value = iji_filename.Split(";")(0)
                hdnContentType.Value = iji_filename.Split(";")(1)
                btnDocumentLink.Text = iji_filename.Split(";")(2)
                btnDocumentLink.Visible = True
                btnDocumentDelete.Visible = True
                btnUpload.Visible = False
                UploadDocPhoto.Visible = False
            Else
                btnUpload.Visible = True
                UploadDocPhoto.Visible = True
            End If


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub CalculateVAT(ByVal InvDate As DateTime, ByVal Amount As Decimal, ByVal actCode As String)
        Dim dt1 As New DataTable
        Dim dtVATUserAccess As New DataTable
        dt1 = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','JOB_ORD','" & Format(CDate(InvDate), "dd/MMM/yyyy") & "'," & Amount & ",'" & actCode & "')", "OASIS_TRANSPORTConnectionString")
        If dt1.Rows.Count > 0 Then
            h_VATPerc.Value = dt1.Rows(0)("tax_perc_value")
            Dim lstDrp As New RadComboBoxItem
            lstDrp = radVat.Items.FindItemByValue(dt1.Rows(0)("tax_code"))
            If Not lstDrp Is Nothing Then
                radVat.SelectedValue = lstDrp.Value
            Else
                radVat.SelectedValue = "NA"
            End If
            txtBillingTotal.Text = Amount + h_EXMPTTOTAL.Value
            txtVATAmount.Text = Convert.ToDouble(Amount * h_VATPerc.Value / 100.0).ToString("####0.00")
            txtNetAmount.Text = Convert.ToDouble(Amount + txtVATAmount.Text + h_EXMPTTOTAL.Value).ToString("####0.00")
        End If
        dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")

        If dtVATUserAccess.Rows.Count > 0 Then
            If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                radVat.Enabled = True
                RadEmirate.Enabled = True
            Else
                radVat.Enabled = False
                RadEmirate.Enabled = False
            End If
        Else
            radVat.Enabled = False
            RadEmirate.Enabled = False
        End If

    End Sub

    Sub ClearDetails()
        txtTPT_Date.Text = Now.ToString("dd/MMM/yyyy")
        txtTrn_No.Text = "New"
        txtSTS_No.Text = ""
        txtLPO_No.Text = ""
        txtLocation.Text = ""
        hTPT_Client_ID.Value = "0"
        hTPT_Drv_ID.Value = "0"
        hTPT_Loc_ID.Value = "0"
        hTPT_Service_ID.Value = "0"
        hTPT_Veh_ID.Value = "0"
        txtVehicleNo.Text = ""
        txtDriversName.Text = ""
        txtVehicleName.Text = ""
        txtClientName.Text = ""
        txtCapacity.Text = "0"
        txtCapacityStr.Text = "0"
        txtService.Text = ""
        txtTPT_Description.Text = ""
        txtSupplier.Text = ""
        hdnSupplier.Value = ""
        txtDriver.Text = ""
        txtVehRegNo.Text = ""
        txtPickupPoint.Text = ""
        txtDropOffPoint.Text = ""
        txtTPT_STSSt_Time.Text = ""
        txtTPT_JobSt_Time.Text = ""
        txtTPT_STSSt_km.Text = "0"
        txtTPT_JOBCl_Time.Text = ""
        txtTPT_STSArr_Time.Text = ""
        txtTPT_JobSt_km.Text = "0"
        txtTPT_STSArr_km.Text = "0"
        txtTPT_JOBCl_km.Text = "0"
        rblJobType.Items(0).Selected = True
        txtTPT_Client_RefNo.Text = ""
        txtTPT_Remarks.Text = ""
        txtTPT_Add_Service.Text = ""
        txtTPT_Xtra_Hrs.Text = "0"
        txtTPT_Feedback.Text = ""
        txtBillingTotal.Text = "0"
        txtTRN_SalesMan.Text = ""
        txtTRN_Veh_CatId.Text = ""
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'ClearDetails()
        'setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdInvoice.DataSource = TPTHiringInvoice
        grdInvoice.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        grdInvoice.DataSource = TPTHiringInvoice
        grdInvoice.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'Dim Yearend As Date
            'Dim BsuFreezdt As Date
            'Yearend = Mainclass.getDataValue("select fyr_todt from oasis..financialyear_s where fyr_id='" & Session("F_YEAR") & "'", "OASIS_TRANSPORTConnectionString")
            'BsuFreezdt = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            'If BsuFreezdt <= Yearend Then
            'btnAdd.Visible = True
            'Else
            '   btnAdd.Visible = False
            'End If
            btnAdd.Visible = False


        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If ViewState("MainMnu_code") = "T200190" Then SaveOpenJob()
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        If txtTPT_Date.Text.Trim.Length = 0 Then strMandatory.Append("Date,")
        'If hTPT_Veh_ID.Value = "0" And txtVehRegNo.Text.Trim.Length = 0 Then strMandatory.Append("Vehicle No.,")
        If (rblData.Text = 0 Or rblData.Text = 1) And txtVehicleNo.Text.Trim.Length = 0 Then strMandatory.Append("Vehicle No.,")
        If (rblData.Text = 0 Or rblData.Text = 1) And txtDriversName.Text.Trim.Length = 0 Then strMandatory.Append("Driver,")
        If hTPT_Loc_ID.Value = "0" Then strMandatory.Append("Location,")
        If hTPT_Client_ID.Value = "0" Then strMandatory.Append("Client's Name,")

        If ViewState("MainMnu_code") <> "T200190" Then
            If txtBillingTotal.Text = "0" Then strMandatory.Append("Invoice Details,")
        End If
        If txtTPT_Description.Text.Trim.Length = 0 Then strMandatory.Append("Job Description,")
        If Not IsDate(txtTPT_Date.Text) Then strMandatory.Append("invalid date,")
        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If

        If grdInvoice.Rows.Count - 1 < 0 Then
            strError.Append("atleast add one entry in Billing Item")
            lblError.Text = "atleast add one entry in Billing Item"
            Exit Sub
        End If
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(46) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@TRN_No", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@TPT_Bsu_id", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@LPO_no", txtLPO_No.Text, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@STS_No", txtSTS_No.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@TPT_date", txtTPT_Date.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@TPT_Loc_id", hTPT_Loc_ID.Value, SqlDbType.Int)
        If hTPT_Veh_ID.Value = "" Then
            pParms(7) = Mainclass.CreateSqlParameter("@TPT_Veh_id", 0, SqlDbType.Int)
        Else
            pParms(7) = Mainclass.CreateSqlParameter("@TPT_Veh_id", hTPT_Veh_ID.Value, SqlDbType.Int)
        End If

        If hTPT_Drv_ID.Value = "" Then
            pParms(8) = Mainclass.CreateSqlParameter("@TPT_Drv_id", 0, SqlDbType.Int)
        Else
            pParms(8) = Mainclass.CreateSqlParameter("@TPT_Drv_id", hTPT_Drv_ID.Value, SqlDbType.Int)
        End If
        pParms(9) = Mainclass.CreateSqlParameter("@TPT_Client_id", hTPT_Client_ID.Value, SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@TPT_Service_id", hTPT_Service_ID.Value, SqlDbType.Int)
        pParms(11) = Mainclass.CreateSqlParameter("@TPT_Description", txtTPT_Description.Text, SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@TPT_PickupPoint", txtPickupPoint.Text, SqlDbType.VarChar)
        pParms(13) = Mainclass.CreateSqlParameter("@TPT_DropOffPoint", txtDropOffPoint.Text, SqlDbType.VarChar)
        pParms(14) = Mainclass.CreateSqlParameter("@TPT_STSSt_Time", txtTPT_STSSt_Time.Text, SqlDbType.VarChar)
        pParms(15) = Mainclass.CreateSqlParameter("@TPT_JobSt_Time", txtTPT_JobSt_Time.Text, SqlDbType.VarChar)
        pParms(16) = Mainclass.CreateSqlParameter("@TPT_STSSt_km", txtTPT_STSSt_km.Text, SqlDbType.Int)
        pParms(17) = Mainclass.CreateSqlParameter("@TPT_JOBCl_Time", txtTPT_JOBCl_Time.Text, SqlDbType.VarChar)
        pParms(18) = Mainclass.CreateSqlParameter("@TPT_STSArr_Time", txtTPT_STSArr_Time.Text, SqlDbType.VarChar)
        pParms(19) = Mainclass.CreateSqlParameter("@TPT_JobSt_km", txtTPT_JobSt_km.Text, SqlDbType.Int)
        pParms(20) = Mainclass.CreateSqlParameter("@TPT_STSArr_km", txtTPT_STSArr_km.Text, SqlDbType.Int)
        pParms(21) = Mainclass.CreateSqlParameter("@TPT_JOBCl_km", txtTPT_JOBCl_km.Text, SqlDbType.Int)
        pParms(22) = Mainclass.CreateSqlParameter("@TPT_Client_RefNo", txtTPT_Client_RefNo.Text, SqlDbType.VarChar)
        pParms(23) = Mainclass.CreateSqlParameter("@TPT_Remarks", txtTPT_Remarks.Text, SqlDbType.VarChar)
        pParms(24) = Mainclass.CreateSqlParameter("@TPT_Add_Service", txtTPT_Add_Service.Text, SqlDbType.VarChar)
        pParms(25) = Mainclass.CreateSqlParameter("@TPT_Xtra_Hrs", txtTPT_Xtra_Hrs.Text, SqlDbType.VarChar)
        pParms(26) = Mainclass.CreateSqlParameter("@TPT_Feedback", txtTPT_Feedback.Text, SqlDbType.VarChar)
        pParms(27) = Mainclass.CreateSqlParameter("@TPT_JobType", rblJobType.Text, SqlDbType.VarChar)
        pParms(28) = Mainclass.CreateSqlParameter("@TRN_Veh_CatId", hdnTRN_Veh_CatId.Value, SqlDbType.VarChar)

        If ViewState("MainMnu_code") <> "T200190" Then
            CheckDiscounted()
        End If

        If ViewState("MainMnu_code") = "T200190" Then
            pParms(29) = Mainclass.CreateSqlParameter("@TRN_Status", "", SqlDbType.VarChar)
        Else
            If Discounted = True Then
                pParms(29) = Mainclass.CreateSqlParameter("@TRN_Status", "A", SqlDbType.VarChar)
            Else
                pParms(29) = Mainclass.CreateSqlParameter("@TRN_Status", "S", SqlDbType.VarChar)
            End If
        End If
        pParms(30) = Mainclass.CreateSqlParameter("@TRN_Salesman", hdnTRN_SalesMan.Value, SqlDbType.VarChar)
        pParms(31) = Mainclass.CreateSqlParameter("@TRN_SEAS_ID", hSeason.Value, SqlDbType.Int)
        pParms(32) = Mainclass.CreateSqlParameter("@TPT_ENQUIRY", h_EnqMethod.Value, SqlDbType.VarChar)
        pParms(33) = Mainclass.CreateSqlParameter("@TRN_TRL_ID", hRateType.Value, SqlDbType.VarChar)
        pParms(34) = Mainclass.CreateSqlParameter("@TPT_SUPPLIER", hdnSupplier.Value, SqlDbType.VarChar)
        pParms(35) = Mainclass.CreateSqlParameter("@TPT_DRIVER", txtDriver.Text, SqlDbType.VarChar)
        pParms(36) = Mainclass.CreateSqlParameter("@TPT_VEHREGNO", txtVehRegNo.Text, SqlDbType.VarChar)
        pParms(37) = Mainclass.CreateSqlParameter("@TPT_DATA", rblData.Text, SqlDbType.VarChar)
        pParms(38) = Mainclass.CreateSqlParameter("@SC_VEHTYPE", txtVehType.Text, SqlDbType.VarChar)
        pParms(39) = Mainclass.CreateSqlParameter("@SC_VEHCAPACITY", txtVehCapacity.Text, SqlDbType.VarChar)
        pParms(40) = Mainclass.CreateSqlParameter("@TPT_STAFFNAME", txtStaff.Text, SqlDbType.VarChar)
        pParms(41) = Mainclass.CreateSqlParameter("@TPT_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(42) = Mainclass.CreateSqlParameter("@TPT_USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(43) = Mainclass.CreateSqlParameter("@TPT_TAX_AMOUNT", txtVATAmount.Text, SqlDbType.Float)
        pParms(44) = Mainclass.CreateSqlParameter("@TPT_TAX_CODE", radVat.SelectedValue, SqlDbType.VarChar)
        pParms(45) = Mainclass.CreateSqlParameter("@TPT_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
        pParms(46) = Mainclass.CreateSqlParameter("@TPT_EMR_CODE", RadEmirate.SelectedValue, SqlDbType.VarChar)



        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTHiringFleet", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            Dim RowCount As Integer
            For RowCount = 0 To TPTHiringInvoice.Rows.Count - 1
                If TPTHiringInvoice.Rows(0)(1) <> -1 Then
                    Dim iParms(6) As SqlClient.SqlParameter
                    Dim rowState As Integer = ViewState("EntryId")
                    If TPTHiringInvoice.Rows(RowCount).RowState = 8 Then rowState = -1
                    If ViewState("datamode") = "add" Then
                        iParms(1) = Mainclass.CreateSqlParameter("@INV_Id", 0, SqlDbType.Int)
                    Else
                        iParms(1) = Mainclass.CreateSqlParameter("@INV_Id", TPTHiringInvoice.Rows(RowCount)("INV_Id"), SqlDbType.Int)
                    End If

                    iParms(2) = Mainclass.CreateSqlParameter("@INV_CN_ID", TPTHiringInvoice.Rows(RowCount)("INV_CN_Id"), SqlDbType.Int)
                    iParms(3) = Mainclass.CreateSqlParameter("@INV_TRN_NO", rowState, SqlDbType.Int)
                    iParms(4) = Mainclass.CreateSqlParameter("@INV_QTY", TPTHiringInvoice.Rows(RowCount)("INV_QTY"), SqlDbType.VarChar)
                    iParms(5) = Mainclass.CreateSqlParameter("@INV_RATE", TPTHiringInvoice.Rows(RowCount)("INV_RATE"), SqlDbType.Decimal)
                    iParms(6) = Mainclass.CreateSqlParameter("@INV_PROD_ID", TPTHiringInvoice.Rows(RowCount)("INV_PROD_ID"), SqlDbType.VarChar)
                    Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTHiringInvoice", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                End If
            Next

            If hGridDelete.Value <> "0" Then
                Dim deleteId() As String = hGridDelete.Value.Split(";")
                Dim iParms(2) As SqlClient.SqlParameter
                For RowCount = 0 To deleteId.GetUpperBound(0)
                    iParms(1) = Mainclass.CreateSqlParameter("@Inv_Id", deleteId(RowCount), SqlDbType.Int)
                    iParms(2) = Mainclass.CreateSqlParameter("@Inv_Trn_No", ViewState("EntryId"), SqlDbType.Int)
                    Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from TPTHiringInvoice where Inv_id=@Inv_Id and Inv_Trn_No=@Inv_Trn_No", iParms)
                    If RetValFooter = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If

            Dim UDParms(1) As SqlClient.SqlParameter
            UDParms(1) = Mainclass.CreateSqlParameter("@INV_TRN_NO", ViewState("EntryId"), SqlDbType.Int)
            Dim RetValUpdateDiscount As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "UPDATEDISCOUNTAMOUNT", UDParms)
            If RetValUpdateDiscount = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            End If

            stTrans.Commit()

            writeWorkFlowTPTH(ViewState("EntryId"), IIf(h_EntryId.Value = 0, "Created", "Modified"), "Job Order-Fleet", "")
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Function writeWorkFlowTPTH(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal strStatus As String) As Integer
        Dim iParms(6) As SqlClient.SqlParameter
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TRN_NO", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveWorkFlowTPTH", iParms)
        If RetVal = "-1" Then
            lblError.Text = "Unexpected Error !!!"
            stTrans.Rollback()
            Exit Function
        Else
            stTrans.Commit()
        End If
    End Function
    Private Sub SaveOpenJob()
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim i As Integer
        If txtTPT_Date.Text.Trim.Length = 0 Then strMandatory.Append("Date,")
        If hTPT_Loc_ID.Value = "0" Then strMandatory.Append("Location,")
        If hTPT_Client_ID.Value = "0" Then strMandatory.Append("Client's Name,")
        If txtTPT_Description.Text.Trim.Length = 0 Then strMandatory.Append("Job Description,")
        If Not IsDate(txtTPT_Date.Text) Then strError.Append("invalid date,")
        Dim txtqty As Integer
        If ViewState("add") = True Then
            If Not IsNumeric(txtqty) = True Then strError.Append("invalid Quantity,")
        End If
        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        For i = 1 To Val(txtqty)
            Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
            Dim pParms(20) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TRN_No", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TPT_Bsu_id", Session("sBsuid"), SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@LPO_no", txtLPO_No.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@STS_No", txtSTS_No.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TPT_date", txtTPT_Date.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@TPT_Loc_id", hTPT_Loc_ID.Value, SqlDbType.Int)
            pParms(7) = Mainclass.CreateSqlParameter("@TPT_Client_id", hTPT_Client_ID.Value, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@TPT_Service_id", hTPT_Service_ID.Value, SqlDbType.Int)
            pParms(9) = Mainclass.CreateSqlParameter("@TPT_Description", txtTPT_Description.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@TPT_PickupPoint", txtPickupPoint.Text, SqlDbType.VarChar)
            pParms(11) = Mainclass.CreateSqlParameter("@TPT_DropOffPoint", txtDropOffPoint.Text, SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@TPT_JobType", rblJobType.Text, SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@TRN_Veh_CatId", hdnTRN_Veh_CatId.Value, SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@TRN_Status", "", SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@TRN_Salesman", hdnTRN_SalesMan.Value, SqlDbType.VarChar)
            pParms(16) = Mainclass.CreateSqlParameter("@TPT_Capacity", txtCapacity.Text, SqlDbType.VarChar)
            pParms(17) = Mainclass.CreateSqlParameter("@TRN_SEAS_ID", hSeason.Value, SqlDbType.Int)
            pParms(18) = Mainclass.CreateSqlParameter("@TPT_ENQUIRY", h_EnqMethod.Value, SqlDbType.VarChar)
            pParms(19) = Mainclass.CreateSqlParameter("@TRN_TRL_ID", hRateType.Value, SqlDbType.VarChar)
            pParms(20) = Mainclass.CreateSqlParameter("@TPT_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTJob", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
                stTrans.Commit()
            End If
        Next
        SetDataMode("view")
        setModifyvalues(ViewState("EntryId"))
        lblError.Text = "Data Saved Successfully !!!"

    End Sub
    Private Property TPTHiringInvoice() As DataTable
        Get
            Return ViewState("TPTHiringInvoice")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTHiringInvoice") = value
        End Set
    End Property
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTHiringInvoice = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTHiringInvoice)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTHiringInvoice = mtable
        fillGrdView.DataSource = TPTHiringInvoice
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub
    Private Sub showNoRecordsFound()
        If TPTHiringInvoice.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoice.Columns.Count - 2
            grdInvoice.Rows(0).Cells.Clear()
            grdInvoice.Rows(0).Cells.Add(New TableCell())
            grdInvoice.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoice.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub
    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property
    Protected Sub grdInvoice_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoice.RowCancelingEdit
        grdInvoice.ShowFooter = True
        grdInvoice.EditIndex = -1
        grdInvoice.DataSource = TPTHiringInvoice
        grdInvoice.DataBind()
    End Sub
    Protected Sub grdInvoice_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdInvoice.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtInv_Rate As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Qty")
            Dim txtInv_Product As TextBox = grdInvoice.FooterRow.FindControl("txtProduct")
            Dim txtProductid As HiddenField = grdInvoice.FooterRow.FindControl("h_prod_id")
            lblError.Text = ValidTPTHiringInvoice(txtInv_Rate.Text.Trim, txtInv_Qty.Text.Trim)
            If txtInv_Product.Text = "" Then lblError.Text &= " Select Product"
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            If TPTHiringInvoice.Rows(0)(1) = -1 Then TPTHiringInvoice.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = TPTHiringInvoice.NewRow
            mrow("Inv_id") = 0
            mrow("Inv_PROD_ID") = txtProductid.Value.Split(";")(0)
            mrow("Inv_CN_ID") = txtProductid.Value.Split(";")(1)
            mrow("Prod_Name") = txtInv_Product.Text
            mrow("Inv_Rate") = txtInv_Rate.Text
            mrow("Inv_Qty") = txtInv_Qty.Text
            mrow("Inv_Amt") = (CType(txtInv_Rate.Text, Double) * CType(txtInv_Qty.Text, Double)).ToString
            TPTHiringInvoice.Rows.Add(mrow)
            grdInvoice.EditIndex = -1
            grdInvoice.DataSource = TPTHiringInvoice
            grdInvoice.DataBind()
            showNoRecordsFound()
            txtBillingTotal.Text = TPTHiringInvoice.Compute("sum(inv_amt)", "")
            CalculateTotalwithExempte()
            CalculateVAT(LTrim(RTrim(txtTPT_Date.Text)), h_Total.Value, hTPT_Client_ID.Value)
        Else
            Dim txtInv_Rate As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Qty")
            Dim txtInv_Amt As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Amt")

            If Val(txtInv_Rate.Text) <> 0 Then
                txtInv_Rate.Enabled = False
                txtInv_Amt.Text = Val(txtInv_Qty.Text) * Val(txtInv_Rate.Text)
            Else
                txtInv_Rate.Enabled = True
                txtInv_Amt.Text = Val(txtInv_Qty.Text) * Val(txtInv_Rate.Text)
            End If

        End If
    End Sub

    Private Sub CalculateTotalwithExempte()
        Dim RowCount As Integer
        Dim EXEMPTEDDT As DataTable
        Dim EXCount As Integer
        h_EXMPTTOTAL.Value = 0.0
        'EXEMPTEDDT = Mainclass.getDataTable("SELECT 'PERMIT CHARGES' EXEMPTEDVATITEM UNION ALL SELECT  'PARKING CHARGES'", connectionString)
        EXEMPTEDDT = Mainclass.getDataTable("SELECT  EXEMPTEDVATITEM from  VATEXEMPTEDSERVICES", connectionString)

        For RowCount = 0 To TPTHiringInvoice.Rows.Count - 1
            If TPTHiringInvoice.Rows(0)(1) <> -1 Then
                If EXEMPTEDDT.Rows.Count > 0 Then
                    For EXCount = 0 To EXEMPTEDDT.Rows.Count - 1
                        If EXEMPTEDDT.Rows(EXCount)("EXEMPTEDVATITEM") = TPTHiringInvoice.Rows(RowCount)("PROD_NAME") Then
                            h_EXMPTTOTAL.Value = h_EXMPTTOTAL.Value + TPTHiringInvoice.Rows(RowCount)("INV_AMT")
                        End If
                    Next

                End If
            End If
        Next
        h_Total.Value = txtBillingTotal.Text - h_EXMPTTOTAL.Value
    End Sub
    Protected Sub grdInvoice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoice.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim txtInv_Rate As TextBox = e.Row.FindControl("txtInv_Rate")
            Dim txtInv_Qty As TextBox = e.Row.FindControl("txtInv_Qty")
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim txtInv_Rate_F As TextBox = e.Row.FindControl("txtInv_Rate")
                Dim txtInv_Qty_F As TextBox = e.Row.FindControl("txtInv_Qty")
                Dim imgSubLedger As ImageButton = e.Row.FindControl("imgSubLedger")
                Dim txtProduct_F As TextBox = e.Row.FindControl("txtProduct")
                Dim h_PRODUCT_ID_F As HiddenField = e.Row.FindControl("h_prod_id")
                imgSubLedger.Attributes.Add("OnClick", "javascript:getProduct('" & txtProduct_F.ClientID & "','" & txtInv_Rate_F.ClientID & "','" & h_PRODUCT_ID_F.ClientID & "','" & txtInv_Qty_F.ClientID & "'); return false;")
            End If

            ClientScript.RegisterArrayDeclaration("grdRate", String.Concat("'", txtInv_Rate.ClientID, "'"))
            ClientScript.RegisterArrayDeclaration("grdQty", String.Concat("'", txtInv_Qty.ClientID, "'"))
            If grdInvoice.ShowFooter Or grdInvoice.EditIndex > -1 Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
                Dim txtInv_Amt As TextBox = e.Row.FindControl("txtInv_Amt")

                txtInv_Rate.Attributes.Add("OnKeyUp", "javascript:return calculateTravel('" & txtInv_Rate.ClientID & "','" & txtInv_Qty.ClientID & "','" & txtInv_Amt.ClientID & "')")
                txtInv_Rate.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")
                txtInv_Qty.Attributes.Add("OnKeyUp", "javascript:return calculateTravel('" & txtInv_Rate.ClientID & "','" & txtInv_Qty.ClientID & "','" & txtInv_Amt.ClientID & "')")
                txtInv_Qty.Attributes.Add("onkeypress", "javascript:return Numeric_Only()")

                If IsNumeric(txtInv_Rate.Text) And IsNumeric(txtInv_Qty.Text) Then txtInv_Amt.Text = (CType(txtInv_Rate.Text, Double) * CType(txtInv_Qty.Text, Double)).ToString
                If IsNumeric(txtInv_Amt.Text) Then txtInv_Amt.Text = Convert.ToDouble(txtInv_Amt.Text).ToString("####0.00")

                Dim ddlProducts As DropDownList = e.Row.FindControl("ddlProducts")
                If Not ddlProducts Is Nothing Then
                    ddlProducts.Attributes.Add("OnChange", "javascript:return products('" & txtInv_Rate.ClientID & "','" & txtInv_Qty.ClientID & "','" & txtInv_Amt.ClientID & "',this)")
                    Dim hProducts As HiddenField = e.Row.FindControl("hProducts")
                    Dim sqlStr As String
                    If rblJobType.Items(0).Selected Then
                        sqlStr = "select '0;0' ddlId, ' Select Product' ddlDescr, 0 ddlRate " _
                        & " union " _
                        & "select cast(prod_id as varchar)+';'+cast(cnh_id as varchar), prod_name ddlDescr, TPTCONTRACT_D.CND_RATE from TPTCONTRACT_H INNER JOIN TPTCONTRACT_D ON CNH_ID=CND_CNH_ID INNER JOIN TPTHiringProduct ON PROD_ID=CND_PROD_ID WHERE CNH_ID IN (SELECT TOP 1 CNH_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & hTPT_Client_ID.Value & ",%'  and PROD_FLAG='G' ORDER BY CNH_SYSDATE DESC) " _
                        & "UNION ALL " _
                        & "select '0;0' ddlId, 'Select Product' ddlDescr, 0 " _
                        & " union " _
                        & " select cast(prod_id as varchar)+';'+cast(cnh_id as varchar), prod_name ddlDescr, TPTCONTRACT_D.CND_RATE from TPTCONTRACT_H  INNER JOIN TPTCONTRACT_D ON CNH_ID=CND_CNH_ID INNER JOIN TPTHiringProduct ON PROD_ID=CND_PROD_ID WHERE CNH_ID IN (SELECT TOP 1 CNH_ID FROM TPTCONTRACT_H where CNH_CLIENT_ID='00000000' ORDER BY CNH_SYSDATE DESC) AND PROD_ID NOT IN " _
                        & "(select prod_id from TPTCONTRACT_H INNER JOIN TPTCONTRACT_D ON CNH_ID=CND_CNH_ID INNER JOIN TPTHiringProduct ON PROD_ID=CND_PROD_ID WHERE CNH_ID IN (SELECT TOP 1 CNH_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & hTPT_Client_ID.Value & ",%'  and CNH_TRL_ID=" & hRateType.Value & "PROD_FLAG='G' ORDER BY CNH_SYSDATE DESC)) " _
                        & " union " _
                        & "SELECT cast(PROD_ID AS VARCHAR)+';0' ddlId,PROD_NAME ddlDescr,PROD_RATE FROM TPTHiringProduct WHERE PROD_fLAG='R'" _
                        & " order by ddlDescr "
                    Else
                        sqlStr = "select '0;0' ddlId, ' Select Product' ddlDescr, 0 ddlRate "
                        sqlStr &= "union SELECT cast(CNB_ID as varchar)+';1', 'Rate per hour', CNB_RATEHR FROM TPTCONTRACT_B where CNB_BSU_ID='" & hTPT_Client_ID.Value & "' and CNB_RATEHR>0 "
                        sqlStr &= "union SELECT cast(CNB_ID as varchar)+';2', 'Rate per km', CNB_RATEKM FROM TPTCONTRACT_B where CNB_BSU_ID='" & hTPT_Client_ID.Value & "' and CNB_RATEKM>0 "
                    End If
                    fillDropdown(ddlProducts, sqlStr, "ddlDescr", "ddlId", False)
                    Dim prdTable As DataTable = ddlProducts.DataSource
                    For rowNum As Int16 = 0 To prdTable.Rows.Count - 1
                        h_Products.Value &= "," & prdTable.Rows(rowNum)(2)
                    Next
                    ddlProducts.SelectedValue = "0;" & hProducts.Value
                End If
            End If
        End If
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub
    Protected Sub grdInvoice_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdInvoice.RowDeleting
        Dim mRow() As DataRow = TPTHiringInvoice.Select("ID=" & grdInvoice.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= ";" & mRow(0)("Inv_Id")
            TPTHiringInvoice.Select("ID=" & grdInvoice.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            TPTHiringInvoice.AcceptChanges()
        End If
        If TPTHiringInvoice.Rows.Count = 0 Then
            TPTHiringInvoice.Rows.Add(TPTHiringInvoice.NewRow())
            TPTHiringInvoice.Rows(0)(1) = -1
            txtBillingTotal.Text = "0"
        Else
            txtBillingTotal.Text = TPTHiringInvoice.Compute("sum(inv_amt)", "")
            txtBillingTotal_TextChanged(sender, Nothing)
        End If
        grdInvoice.DataSource = TPTHiringInvoice
        grdInvoice.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub grdInvoice_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdInvoice.RowEditing
        Try
            grdInvoice.ShowFooter = False
            grdInvoice.EditIndex = e.NewEditIndex
            grdInvoice.DataSource = TPTHiringInvoice
            grdInvoice.DataBind()
            Dim txtInv_Rate_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtInv_Rate")
            Dim txtInv_Qty_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtInv_Qty")
            Dim imgSubLedger As ImageButton = grdInvoice.Rows(e.NewEditIndex).FindControl("imgSubLedger_e")
            Dim txtProduct_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtProduct")
            Dim h_PRODUCT_ID_F As HiddenField = grdInvoice.Rows(e.NewEditIndex).FindControl("h_prod_id_e")
            Dim txtInv_Product As TextBox = grdInvoice.FooterRow.FindControl("txtProduct")
            imgSubLedger.Visible = True
            imgSubLedger.Attributes.Add("OnClick", "javascript:getProduct('" & txtProduct_F.ClientID & "','" & txtInv_Rate_F.ClientID & "','" & h_PRODUCT_ID_F.ClientID & "','" & txtInv_Qty_F.ClientID & "'); return false;")
            txtInv_Rate_F.ReadOnly = True
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub grdInvoice_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdInvoice.RowUpdating
        Dim s As String = grdInvoice.DataKeys(e.RowIndex).Value.ToString()
        Dim lblInv_id As Label = grdInvoice.Rows(e.RowIndex).FindControl("lblInv_ID")
        Dim txtInv_Rate As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInv_Rate")
        Dim txtInv_Qty As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInv_Qty")
        Dim txtInv_Amt As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInv_Amt")
        Dim txtInv_Product As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtProduct")
        Dim txtProductid As HiddenField = grdInvoice.Rows(e.RowIndex).FindControl("h_prod_id_e")
        Dim lblINVCNID As Label = grdInvoice.Rows(e.RowIndex).FindControl("lblInv_CN_ID")
        lblError.Text = ValidTPTHiringInvoice(txtInv_Rate.Text.Trim, txtInv_Qty.Text.Trim)
        If txtInv_Product.Text = "" Then lblError.Text &= " Select Product"
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If
        lblError.Text = ValidTPTHiringInvoice(txtInv_Rate.Text.Trim, txtInv_Qty.Text.Trim)
        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If
        Dim mrow As DataRow
        mrow = TPTHiringInvoice.Select("ID=" & s)(0)
        mrow("Inv_id") = lblInv_id.Text
        mrow("Inv_PROD_ID") = txtProductid.Value.Split(";")(0)
        mrow("Inv_CN_ID") = lblINVCNID.Text
        mrow("Prod_Name") = txtInv_Product.Text
        mrow("Inv_Rate") = txtInv_Rate.Text
        mrow("Inv_Qty") = txtInv_Qty.Text
        txtInv_Amt.Text = (CType(txtInv_Rate.Text, Double) * CType(txtInv_Qty.Text, Double)).ToString
        txtInv_Amt.Text = Convert.ToDouble(txtInv_Amt.Text).ToString("####0.00")
        mrow("Inv_Amt") = txtInv_Amt.Text
        grdInvoice.EditIndex = -1
        grdInvoice.ShowFooter = True
        grdInvoice.DataSource = TPTHiringInvoice
        grdInvoice.DataBind()
    End Sub
    Function ValidTPTHiringInvoice(ByVal txtTrv_Rate As String, ByVal txtTrv_Qty As String) As String
        ValidTPTHiringInvoice = ""
        If txtTrv_Rate.Length = 0 Then ValidTPTHiringInvoice &= "Rate"
        If txtTrv_Qty.Length = 0 Then ValidTPTHiringInvoice &= IIf(ValidTPTHiringInvoice.Length = 0, "", ",") & "Qty"
    End Function
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As Integer = CType(h_EntryId.Value, Integer)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTHiring"
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TRN_NO", TRN_NO, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            Dim ACY_DESCR As String = ""
            params("userName") = Session("sUsr_name")
            ACY_DESCR = Mainclass.getDataValue("select FYR_DESCR from OASIS..FINANCIALYEAR_S B,TPTHiring A where A.TPT_DATE BETWEEN B.FYR_FROMDT  AND B.FYR_TODT and a.TRN_no =" & TRN_NO, "OASIS_TRANSPORTConnectionString")
            'params("reportCaption") = TRN_NO.ToString("000000") & "/" & ACY_DESCR
            params("reportCaption") = Right(txtTrn_No.Text, 6) & "/" & ACY_DESCR
            params("@TRN_NO") = TRN_NO
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTHiring_Fleet.rpt"
            Dim subRep(0) As MyReportClass
            subRep(0) = New MyReportClass
            Dim subcmd1 As New SqlCommand
            subcmd1.CommandText = "rptTPTHiringInvoice"
            Dim sqlParam1(1) As SqlParameter
            sqlParam1(0) = Mainclass.CreateSqlParameter("@TRN_NO", TRN_NO, SqlDbType.Int)
            subcmd1.Parameters.Add(sqlParam1(0))
            subcmd1.CommandType = Data.CommandType.StoredProcedure
            subcmd1.Connection = New SqlConnection(str_conn)
            subRep(0).Command = subcmd1
            subRep(0).ResourceName = "wBTA_Sector"
            repSource.IncludeBSUImage = True
            repSource.SubReport = subRep
            'End If
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub FormatFigures()
        On Error Resume Next
        txtBillingTotal.Text = Convert.ToDouble(txtBillingTotal.Text).ToString("####0.00")
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetProducts(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim StrSQL As String
        If contextKey Is Nothing Then contextKey = ""
        StrSQL = "select top 10 ITM_ID, DESCRIPTION, COMMODITY, UNIT, PACKING, DETAILS, RATE from ("
        StrSQL &= "select ITM_ID, ITM_DESCR DESCRIPTION, isnull(UCO_DESCR,'') COMMODITY, ITM_UNIT UNIT, ITM_PACKING PACKING, ITM_DETAILS DETAILS, ITM_RATE RATE "
        StrSQL &= "FROM ITEM left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
        StrSQL &= ") a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("ITM_ID").ToString & "|" & ds.Tables(0).Rows(i2)("RATE").ToString & "|" & ds.Tables(0).Rows(i2)("DETAILS").ToString & "|" & ds.Tables(0).Rows(i2)("PACKING").ToString & "|" & ds.Tables(0).Rows(i2)("UNIT").ToString & "|" & ds.Tables(0).Rows(i2)("COMMODITY").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
            'items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function
    Private Sub CheckDiscounted()
        Dim RowCount As Integer
        For RowCount = 0 To TPTHiringInvoice.Rows.Count - 1
            If TPTHiringInvoice.Rows(RowCount)("INV_PROD_ID") = 130 Then
                Discounted = True
            Else
                Discounted = False
            End If
        Next
    End Sub
    Protected Sub btnFOC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFOC.Click
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        If txtFOCDescr.Text = "" Then
            lblError.Text = "Please enter the description in FOC details."
            Exit Sub
        End If
        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='F',TPT_Cancel_Remarks='" & txtFOCDescr.Text & "' where TRN_No=" & h_EntryId.Value)
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            Exit Sub
        Finally
            objConn.Close()
            Response.Redirect(ViewState("ReferrerUrl"))
        End Try
    End Sub
    Protected Sub rbCredit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCredit.CheckedChanged
        setRBEnqMethodCheckValues()
    End Sub
    Protected Sub rbEmail_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEmail.CheckedChanged
        setRBEnqMethodCheckValues()
    End Sub
    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged, rbCredit.CheckedChanged, rbEmail.CheckedChanged
        setRBEnqMethodCheckValues()
    End Sub
    Private Sub setRBEnqMethodCheckValues()
        If rbCash.Checked = True Then
            h_EnqMethod.Value = "CASH"
        ElseIf rbCredit.Checked = True Then
            h_EnqMethod.Value = "CARD"
        Else
            h_EnqMethod.Value = "EMAIL"
        End If
    End Sub

    Private Sub ReadRBEnqMethosValues()
        If h_EnqMethod.Value = "CASH" Then
            rbCash.Checked = True
        ElseIf h_EnqMethod.Value = "CARD" Then
            rbCredit.Checked = True
        Else
            rbEmail.Checked = True
        End If
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename '& "\temp\" & str_tempfilename

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                lblError.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Try
                    IO.File.Move(strFilepath, str_img & txtTrn_No.Text & "_" & Session("sBsuid") & FileExt)
                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "insert into TPTHiring_I (THI_CONTENTTYPE, THI_DOCNO, THI_BSU_ID, THI_NAME, THI_DATE, THI_USERNAME, THI_FILENAME, THI_DELETE) values ('" & ContentType & "','" & txtTrn_No.Text & "','" & Session("sBSUID") & "','" & UploadDocPhoto.FileName & "',getdate(),'" & Session("sUsr_name") & "','" & txtTrn_No.Text & "_" & Session("sBsuid") & FileExt & "',0)")
                    hdnFileName.Value = txtTrn_No.Text & "_" & Session("sBsuid") & FileExt
                    hdnContentType.Value = ContentType
                    btnDocumentLink.Text = UploadDocPhoto.FileName
                    btnDocumentLink.Visible = True
                    btnDocumentDelete.Visible = True
                    UploadDocPhoto.Visible = False
                    btnUpload.Visible = False
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select
    End Function
    Protected Sub btnDocumentDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocumentDelete.Click
        If Not btnSave.Visible Then
            lblError.Text = "Cannot delete file"
        Else
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "update TPTHiring_I set THI_DELETE=1 where THI_DOCNO='" & txtTrn_No.Text & "' and THI_bsu_id='" & Session("sBSUID") & "'")
            Dim fileToDelete As New FileInfo(WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Transport\" & hdnFileName.Value)
            Try

                fileToDelete.Delete()
                writeWorkFlowTPTH(h_EntryId.Value, "DELETE", "Fleet-Attachement Deleted File Name:" & hdnFileName.Value, "D")
                hdnFileName.Value = ""
                hdnContentType.Value = ""
                btnDocumentLink.Visible = False
                btnDocumentDelete.Visible = False
                btnUpload.Visible = True
                UploadDocPhoto.Visible = True

            Catch ex As Exception

            End Try



        End If
    End Sub
    Protected Sub btnPProforma_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPProforma.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As String = CType(h_EntryId.Value, String)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTInvoiceProforma"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@trn_No", TRN_NO, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")
            params("reportCaption") = "PROFORMA INVOICE"


            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            If Session("sBsuid") = "900501" Then
                repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTHiringProforma.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTHiringProformaBBT.rpt"
            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '   Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub radVat_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVat.SelectedIndexChanged

        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVat.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            txtVATAmount.Text = ((Val(txtBillingTotal.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtBillingTotal.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        Else
            radVat.SelectedValue = 0
            h_VATPerc.Value = radVat.SelectedValue
            txtVATAmount.Text = ((Val(txtBillingTotal.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtBillingTotal.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub txtClientName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtClientName.TextChanged
        CalculateVAT(LTrim(RTrim(txtTPT_Date.Text)), IIf((txtBillingTotal.Text = ""), 0, Val(LTrim(RTrim(txtBillingTotal.Text)))), hTPT_Client_ID.Value)
        SelectEmirate(hTPT_Client_ID.Value)
    End Sub
    Private Sub SelectEmirate(ByVal ACT_ID As String)
        Dim bsu_City As String = ""
        bsu_City = Mainclass.getDataValue("SELECT isnull(case when case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end end,'NA')   EMR_CODE   FROM OASISFIN..accounts_M left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE=ACT_EMR_CODE where act_id='" & ACT_ID & "'", "OASIS_TRANSPORTConnectionString")
        Dim lstDrp As New RadComboBoxItem
        lstDrp = RadEmirate.Items.FindItemByValue(bsu_City)

        If Not lstDrp Is Nothing Then
            RadEmirate.SelectedValue = lstDrp.Value
        Else
            RadEmirate.SelectedValue = "NA"
        End If
    End Sub
    Private Sub txtBillingTotal_TextChanged(sender As Object, e As EventArgs) Handles txtBillingTotal.TextChanged
        'CalculateVAT(LTrim(RTrim(txtTPT_Date.Text)), IIf((txtBillingTotal.Text = ""), 0, Val(LTrim(RTrim(txtBillingTotal.Text)))), hTPT_Client_ID.Value)
        CalculateTotalwithExempte()
        CalculateVAT(LTrim(RTrim(txtTPT_Date.Text)), h_Total.Value, hTPT_Client_ID.Value)
    End Sub
End Class



