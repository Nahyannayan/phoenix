﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime
Imports System.Text
Imports GridViewHelper
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Partial Class Transport_ExtraHiring_tptDriverCharges
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64
    Dim Posted As Boolean = False

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim dtVATUserAccess As New DataTable


                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200146") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtBasic.Attributes.Add("onblur", " return calc('O');")
                txtFirst.Attributes.Add("onblur", " return calc('O');")
                txtOther.Attributes.Add("onblur", " return calc('O');")
                txtMobile.Attributes.Add("onblur", " return calc('O');")
                txtUtility.Attributes.Add("onblur", " return calc('O');")
                txtMedical.Attributes.Add("onblur", " return calc('O');")
                txtAirafare.Attributes.Add("onblur", " return calc('O');")
                txtAccom.Attributes.Add("onblur", " return calc('O');")
                txtVisa.Attributes.Add("onblur", " return calc('O');")
                txtGratuity.Attributes.Add("onblur", " return calc('O');")
                txtUniform.Attributes.Add("onblur", " return calc('O');")
                txtDiscount.Attributes.Add("onblur", " return calc('O');")

                clearScreen()
                sqlStr = "select TDC_ID [ID], CONVERT(VARCHAR(20),TDC_TRDATE,106) DATE,BSU_NAME  UNIT,TDC_EMP_ID [EMPLOYEE ID],isnull(EMP_FNAME +' ' +EMP_MNAME +' ' +EMP_LNAME ,'') [EMPLOYEE NAME],EMPNO [OASIS ID], "
                sqlStr &= " TDC_DRIVERTYPE [DRIVER TYPE], TDC_BASIC BASIC,  TDC_FIRSTALLOW [FIRST ALLOW], TDC_SPLALLOW [SPL ALLOW], "
                sqlStr &= " TDC_MOBILE MOBILE,TDC_UTILITY UTILITY, TDC_MEDICAL MEDICAL, TDC_AIRFARE AIRFARE, TDC_ACCOMMODATION ACCOMMODATION, "
                sqlStr &= " TDC_VISA VISA, TDC_GRATUITY GRATUITY, TDC_UNIFORM UNIFORM, TDC_CTC CTC, TDC_OVERHEAD [OVER HEAD],TDC_DISCOUNT DISCOUNT,TDC_TOTALCHARGING [TOTAL CHARGING], "
                sqlStr &= " TDC_INVNO [INV NO],TDC_JHD_DOCNO [IJV NO],TDC_REMARKS REMARKS,"
                sqlStr &= " TDC_NOTES NOTES,TDC_USER [USER NAME],TDC_CTO_BSU_ID,TDC_EMP_ID,TDC_DRIVERTYPE,TDC_POSTING_DATE "
                'sqlStr &= ",TDC_BSU_ID, TDC_CTO_BSU_ID , TDC_FYEAR, TDC_DELETED,TDC_POSTED,TDC_DATE, "
                sqlStr &= ",isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(TDC_PRGM_DIM_CODE,'0') DAX_ID,ISNULL(TDC_TAX_AMOUNT,0)TDC_TAX_AMOUNT,ISNULL(TDC_TAX_NET_AMOUNT,0)TDC_TAX_NET_AMOUNT,ISNULL(TDC_TAX_CODE,'0')TDC_TAX_CODE,isnull(TDC_EMR_CODE,'NA')TDC_EMR_CODE,EMR_DESCR EMIRATE "
                sqlStr &= " FROM TPTDRIVERCHARGES "
                sqlStr &= " left outer join dbo.VV_DAX_CODES on ID=TDC_PRGM_DIM_CODE "
                sqlStr &= " INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=TDC_EMP_ID  "
                sqlStr &= " INNER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TDC_CTO_BSU_ID  "
                sqlStr &= " left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE= isnull(TDC_EMR_CODE,'NA') "

                BindDriver()
                BindBsu()
                BindDAXCodes()
                RadFilterBSU.SelectedValue = "0"
                refreshGrid()
                dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")
                If dtVATUserAccess.Rows.Count > 0 Then
                    If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                        radVAT.Enabled = True
                        RadEmirate.Enabled = True

                    Else
                        radVAT.Enabled = False
                        RadEmirate.Enabled = False
                    End If
                Else
                    radVAT.Enabled = False
                    RadEmirate.Enabled = False
                End If
                CalculateVAT(RadCBSU.SelectedValue)
                SelectEmirate(RadCBSU.SelectedValue)

                If Request.QueryString("VIEWID") <> "" Then
                    h_PRI_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_PRI_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        Dim TAXAMOUNT As Decimal = 0
        Dim NetAmount As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblTAXNetAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblTAXAmt"), Label)
            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                TAXAMOUNT += Val(lblVAT.Text)
                NetAmount += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVatAmount.Text = TAXAMOUNT
        txtSelNetAmount.Text = NetAmount

    End Sub

    Protected Sub Check_All(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim TAXAmount As Decimal = 0
        Dim NetAmount As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblTAXNetAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblTAXAmt"), Label)

            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                TAXAmount += Val(lblVAT.Text)
                NetAmount += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVatAmount.Text = TAXAmount
        txtSelNetAmount.Text = NetAmount
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub
    Private Sub refreshGrid()
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")

        If rblSelection.SelectedValue = 0 Then
            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " WHERE  MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"
            Else
                sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and TDC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"
            End If


        ElseIf rblSelection.SelectedValue = 1 Then
            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " WHERE  MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and isnull(TDC_POSTED,0)=1 ORDER BY UNIT"
            Else
                sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and TDC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"

            End If

        ElseIf rblSelection.SelectedValue = 2 Then
            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " WHERE  MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' ORDER BY UNIT"
            Else
                sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and TDC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "' ORDER BY UNIT"
            End If



        End If

        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTDriverCharges = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
            Session("myData") = TPTDriverCharges
            SetExportFileLink()
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub


    Protected Sub RadFilterBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadFilterBSU.SelectedIndexChanged

        refreshGrid()

        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")

        'If rblSelection.SelectedValue = 0 Then
        '    'sqlWhere = " WHERE  MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"

        '    If RadFilterBSU.SelectedValue = "0" Then
        '        sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"
        '    Else
        '        sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and TDC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"
        '    End If

        'ElseIf rblSelection.SelectedValue = 1 Then
        '    'sqlWhere = " WHERE  MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and isnull(TDC_POSTED,0)=1 ORDER BY UNIT"
        '    If RadFilterBSU.SelectedValue = "0" Then
        '        sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"
        '    Else
        '        sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and TDC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(TDC_POSTED,0)=0  ORDER BY UNIT"
        '    End If


        'ElseIf rblSelection.SelectedValue = 2 Then
        '    'sqlWhere = " WHERE  MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' ORDER BY UNIT"
        '    If RadFilterBSU.SelectedValue = "0" Then
        '        sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "'  ORDER BY UNIT"
        '    Else
        '        sqlWhere = " where  YEAR(TDC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TDC_TRDATE)= MONTH('" & txtDate.Text & "') AND TDC_DELETED=0 and TDC_BSU_ID='" & Session("sBsuid") & "' and TDC_FYEAR='" & Session("F_YEAR") & "' and TDC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "' ORDER BY UNIT"
        '    End If

        'End If


        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTDriverCharges = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)

        '    Session("myData") = TPTDriverCharges
        '    SetExportFileLink()


        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try
    End Sub
    Private Sub clearScreen()
        h_PRI_ID.Value = "0"
        txtDriver.Text = ""
        txtDriverName.Text = ""
        txtDriverType.Text = ""
        txtBasic.Text = "0"
        txtFirst.Text = "0"
        txtOther.Text = "0"
        txtMobile.Text = "0"
        txtUtility.Text = "0"
        txtMedical.Text = "0"
        txtAirafare.Text = "0"
        txtAccom.Text = "0"
        txtVisa.Text = "0"
        txtGratuity.Text = "0"
        txtUniform.Text = "0"
        txtCTC.Text = "0"
        txtOverHead.Text = "0"
        txtTotalCharges.Text = "0"
        txtAltDriver.Text = ""
        txtDiscount.Text = "0"
        TxtNotes.Text = ""
        RadDAXCodes.SelectedValue = "0"
        txtVATAmount.Text = "0.00"
        txtNetAmount.Text = "0.00"
        SelectEmirate(RadCBSU.SelectedValue)

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If h_TEI_ID.Value <> 0 Then lblError.Text &= " This month extra trips are posted already.You can not enter for this month, "
        If RadDAXCodes.SelectedValue = "0" Then lblError.Text &= " please select DAX Posting Code, "
        If RadEmirate.SelectedValue = "NA" Then lblError.Text &= " please select Emirate, "

        If PurposeExists = 0 Then lblError.Text &= "Purpose is "


        If RadEmirate.SelectedValue = "NA" Then
            'lblError.Text = "Please Select Emirate....!"
            usrMessageBar.ShowNotification("Please Select Emirate....!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid", UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Visible = True
            Return
        End If



        Try
            Dim pParms(32) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TDC_ID", h_PRI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TDC_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@TDC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@TDC_CTO_BSU_ID", RadCBSU.SelectedValue, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TDC_DRIVER", txtDriver.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@TDC_DRIVERTYPE", txtDriverType.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@TDC_BASIC", txtBasic.Text, SqlDbType.Float)
            pParms(8) = Mainclass.CreateSqlParameter("@TDC_FIRSTALLOW", txtFirst.Text, SqlDbType.Float)
            pParms(9) = Mainclass.CreateSqlParameter("@TDC_SPLALLOW", txtOther.Text, SqlDbType.Float)
            pParms(10) = Mainclass.CreateSqlParameter("@TDC_MOBILE", txtMobile.Text, SqlDbType.Float)
            pParms(11) = Mainclass.CreateSqlParameter("@TDC_UTILITY", txtUtility.Text, SqlDbType.Float)
            pParms(12) = Mainclass.CreateSqlParameter("@TDC_MEDICAL", txtMedical.Text, SqlDbType.Float)
            pParms(13) = Mainclass.CreateSqlParameter("@TDC_AIRFARE", txtAirafare.Text, SqlDbType.Float)
            pParms(14) = Mainclass.CreateSqlParameter("@TDC_ACCOMMODATION", txtAccom.Text, SqlDbType.Float)
            pParms(15) = Mainclass.CreateSqlParameter("@TDC_VISA", txtVisa.Text, SqlDbType.Float)
            pParms(16) = Mainclass.CreateSqlParameter("@TDC_GRATUITY", txtGratuity.Text, SqlDbType.Float)
            pParms(17) = Mainclass.CreateSqlParameter("@TDC_UNIFORM", txtUniform.Text, SqlDbType.Float)
            pParms(19) = Mainclass.CreateSqlParameter("@TDC_CTC", txtCTC.Text, SqlDbType.Float)
            pParms(20) = Mainclass.CreateSqlParameter("@TDC_OVERHEAD", txtOverHead.Text, SqlDbType.Float)
            pParms(21) = Mainclass.CreateSqlParameter("@TDC_TOTALCHARGING", txtTotalCharges.Text, SqlDbType.Float)
            pParms(22) = Mainclass.CreateSqlParameter("@TDC_REMARKS", txtAltDriver.Text, SqlDbType.VarChar)
            pParms(23) = Mainclass.CreateSqlParameter("@TDC_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(24) = Mainclass.CreateSqlParameter("@TDC_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(25) = Mainclass.CreateSqlParameter("@POSTING", 0, SqlDbType.Int)
            pParms(26) = Mainclass.CreateSqlParameter("@TDC_DISCOUNT", txtDiscount.Text, SqlDbType.Float)
            pParms(27) = Mainclass.CreateSqlParameter("@TDC_NOTES", TxtNotes.Text, SqlDbType.VarChar)
            pParms(28) = Mainclass.CreateSqlParameter("@TDC_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
            pParms(29) = Mainclass.CreateSqlParameter("@TDC_TAX_CODE", radVAT.SelectedValue, SqlDbType.VarChar)
            pParms(30) = Mainclass.CreateSqlParameter("@TDC_TAX_AMOUNT", txtVATAmount.Text, SqlDbType.Float)
            pParms(31) = Mainclass.CreateSqlParameter("@TDC_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
            pParms(32) = Mainclass.CreateSqlParameter("@TDC_EMR_CODE", RadEmirate.SelectedValue, SqlDbType.VarChar)



            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTDRIVERCHARGE", pParms)
                If RetVal = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ID, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                        clearScreen()
                        refreshGrid()
                        'lblError.Text = "Data Saved Successfully !!!"
                        usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If

                End If
                Exit Sub
            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub showEdit(ByVal showId As Integer)
        Dim rdrTrnsprt As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & " where TDC_ID='" & showId & "'")

        If rdrTrnsprt.HasRows Then
            rdrTrnsprt.Read()
            h_PRI_ID.Value = rdrTrnsprt("ID")
            txtDate.Text = rdrTrnsprt("DATE")
            txtDriver.Text = rdrTrnsprt("OASIS ID")
            txtDriverName.Text = rdrTrnsprt("EMPLOYEE NAME")
            txtDriverType.Text = rdrTrnsprt("DRIVER TYPE")
            txtBasic.Text = rdrTrnsprt("BASIC")
            txtFirst.Text = rdrTrnsprt("FIRST ALLOW")
            txtOther.Text = rdrTrnsprt("SPL ALLOW")
            txtMobile.Text = rdrTrnsprt("MOBILE")
            txtUtility.Text = rdrTrnsprt("UTILITY")
            txtMedical.Text = rdrTrnsprt("MEDICAL")
            txtAirafare.Text = rdrTrnsprt("AIRFARE")
            txtAccom.Text = rdrTrnsprt("ACCOMMODATION")
            txtVisa.Text = rdrTrnsprt("VISA")
            txtGratuity.Text = rdrTrnsprt("GRATUITY")
            txtUniform.Text = rdrTrnsprt("UNIFORM")
            txtCTC.Text = rdrTrnsprt("CTC")
            txtOverHead.Text = rdrTrnsprt("OVER HEAD")
            txtTotalCharges.Text = rdrTrnsprt("TOTAL CHARGING")
            RadDAXCodes.SelectedItem.Text = rdrTrnsprt("DAX_DESCR")
            RadDAXCodes.SelectedValue = rdrTrnsprt("DAX_ID")
            'txtInvNo.Text = rdrTrnsprt("TDC_INVNO")
            txtAltDriver.Text = rdrTrnsprt("REMARKS")
            RadCBSU.SelectedItem.Text = rdrTrnsprt("UNIT")
            'RadCBSU.SelectedValue = rdrTrnsprt("TDC_CTO_BSU_ID")
            RadCBSU.SelectedValue = Mainclass.getDataValue("select TDC_CTO_BSU_ID from tptDrivercharges where TDC_ID=" & showId, "OASIS_TRANSPORTConnectionString")
            txtDiscount.Text = rdrTrnsprt("DISCOUNT")
            TxtNotes.Text = rdrTrnsprt("NOTES")
            txtVATAmount.Text = rdrTrnsprt("TDC_TAX_AMOUNT")
            txtNetAmount.Text = rdrTrnsprt("TDC_TAX_NET_AMOUNT")
            radVAT.SelectedValue = rdrTrnsprt("TDC_TAX_CODE")
            Dim lstDrp As New RadComboBoxItem
            lstDrp = RadEmirate.Items.FindItemByValue(rdrTrnsprt("TDC_EMR_CODE"))
            If Not lstDrp Is Nothing Then
                RadEmirate.SelectedValue = lstDrp.Value
            Else
                RadEmirate.SelectedValue = "NA"
            End If
            BindDAXCodes()
            'CalculateVAT(radVAT.SelectedValue)
        End If
        rdrTrnsprt.Close()
        rdrTrnsprt = Nothing

    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub

    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(28).Text = "&nbsp;" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(33).Enabled = True
                gvr.Cells(34).Enabled = False
                gvr.Cells(35).Enabled = False

            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
                gvr.Cells(33).Enabled = False
                gvr.Cells(34).Enabled = True
                gvr.Cells(35).Enabled = True
            End If
        Next

    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTDCID"), Label)

        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTDRIVERCHARGES set TDC_DELETED=1,TDC_USER='" & Session("sUsr_name") & "',TDC_DATE=GETDATE() where TDC_ID=" & lblID.Text)
        refreshGrid()
    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTDC_INVNO"), Label)
        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTDriverCharge"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TDC_INVNO", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@TDC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            'If Format(CDate(lblPOstingDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(lblPOstingDate.Text) <= CDate("31/Dec/2017") Then
                repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTDriverCharge.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTDriverCharge.rpt"
            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '   Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub lnkEmail_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        Dim lblPOstingDate As New Label
        lblID = TryCast(sender.FindControl("lblDC_INVNO"), Label)
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Dim bEmailSuccess As Boolean
        SendEmail(CType(lblID.Text, String), CDate(lblPOstingDate.Text), bEmailSuccess)
        If bEmailSuccess = True Then
            'lblError.Text = "Email send  Sucessfully...!"
            usrMessageBar.ShowNotification("Email send  Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
        Else
            'lblError.Text = "Error while sending email.."
            usrMessageBar.ShowNotification("Error while sending email..", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Private Function SendEmail(ByVal RecNo As String, ByVal InvDate As DateTime, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""

        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        'Dim ClientId As String = ""
        'ClientId = Mainclass.getDataValue("select TLC_ACT_ID from TPTLEASECUSTOMER where TLC_ID in(select distinct tli_tlc_id from tptleasinginvoice where tli_invno='" & RecNo & "')", "OASIS_TRANSPORTConnectionString")
        param("@TDC_INVNO") = RecNo
        param("@TDC_BSU_ID") = Session("sBsuid")
        param.Add("userName", "SYSTEM")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))

        'If Format(CDate(InvDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
        If CDate(InvDate) <= CDate("31/Dec/2017") Then
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTDriverChargeEmail.rpt")
        Else
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTDriverChargeEmailTAX.rpt")
        End If

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = RecNo
        rptDownload.InvType = "DRIVER"
        rptDownload.FCO_ID = 0
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & RecNo & "','" & FCL_ID & "','" & SendEmail & "' ")
        End If
    End Function
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid()
    End Sub
    Private Sub BindDriver()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select '' EMPNO,'' EMP_NAME UNION ALL select DRIVER_EMPNO,DRIVER from [VW_DRIVER] A INNER JOIN OASIS..employee_m B ON B.EMP_ID=A.DRIVER_empid "
        sql_str &= " order by EMPNO "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim BSUParms(2) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            'RadCBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "gETUNITSFOREXTRATRIPS", BSUParms)
            'RadFilterBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "gETUNITSFOREXTRATRIPS", BSUParms)
            'RadCBSUPrint.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "gETUNITSFOREXTRATRIPS", BSUParms)
            RadCBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForDriverANDHirecharges", BSUParms)
            RadFilterBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForDriverANDHirecharges", BSUParms)
            RadCBSUPrint.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForDriverANDHirecharges", BSUParms)
            RadCBSU.DataTextField = "BSU_NAME"
            RadCBSU.DataValueField = "SVB_BSU_ID"
            RadFilterBSU.DataTextField = "BSU_NAME"
            RadFilterBSU.DataValueField = "SVB_BSU_ID"
            RadCBSUPrint.DataTextField = "BSU_NAME"
            RadCBSUPrint.DataValueField = "SVB_BSU_ID"
            RadCBSU.DataBind()
            RadFilterBSU.DataBind()
            RadCBSUPrint.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim strsql3 As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            strsql3 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()
            radVAT.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            radVAT.DataTextField = "DESCR"
            radVAT.DataValueField = "ID"
            radVAT.DataBind()

            RadEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql3)
            RadEmirate.DataTextField = "DESCR"
            RadEmirate.DataValueField = "ID"
            RadEmirate.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SelectEmirate(ByVal BSUID As String)
        Dim bsu_City As String = ""
        bsu_City = Mainclass.getDataValue("SELECT case when case when BSU_CITY='ALN' then 'AUH' else BSU_CITY end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when BSU_CITY='ALN' then 'AUH' else BSU_CITY end end   BSU_CITY   FROM OASIS..BUSINESSUNIT_M where BSU_id='" & BSUID & "' ", "OASIS_TRANSPORTConnectionString")
        Dim lstDrp As New RadComboBoxItem
        lstDrp = RadEmirate.Items.FindItemByValue(bsu_City)
        If Not lstDrp Is Nothing Then
            RadEmirate.SelectedValue = lstDrp.Value
        Else
            RadEmirate.SelectedValue = "NA"
        End If
    End Sub
    Protected Sub txtDriver_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim DriverName As String
        DriverName = Mainclass.getDataValue("select Driver from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtDriver.Text) & "'", "OASIS_TRANSPORTConnectionString")
        If DriverName <> "" Then
            txtDriverName.Text = DriverName
            txtDriverType.Focus()
        Else
            'lblError.Text = "Invalid Driver No...!:"
            usrMessageBar.ShowNotification("Invalid Driver No...!:", UserControls_usrMessageBar.WarningType.Danger)
            txtDriver.Text = ""
            txtDriver.Focus()
            Exit Sub
        End If
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click, btnPost1.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox

        Dim PartyCode As String = ""
        Dim i As Integer = 0
        Dim mrow As GridViewRow
        Dim SelectedIDs As String = ""
        Dim NotSameBsu As Boolean = False
        lblError.Text = ""


        If txtPostingDate.Text.Trim = "" Then lblError.Text &= "Please select Posting Date,"

        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " "
            'lblError.Visible = True
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " ", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If


        For Each mrow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(mrow.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(mrow.FindControl("lblTDCID"), Label)

            If ChkBxItem.Checked = True Then
                If i = 0 Then
                    PartyCode = mrow.Cells(3).Text
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                Else
                    If PartyCode = mrow.Cells(3).Text Then
                        IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    Else
                        NotSameBsu = True
                    End If
                End If
                i = i + 1
            End If
        Next
        If NotSameBsu = False Then
            PostDriverChargeInvoice(IDs)

            If Posted = True Then
                'lblError.Text = "Data Posted Successfully !!!"
                usrMessageBar.ShowNotification("Data Posted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            Else
                'lblError.Text = "Some error while posting..."
                usrMessageBar.ShowNotification("Some error while posting...", UserControls_usrMessageBar.WarningType.Danger)
            End If
            refreshGrid()
        Else
            'lblError.Text = "Selected business units are  not same!!!"
            usrMessageBar.ShowNotification("Selected business units are  not same!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

    End Sub
    Private Sub PostDriverChargeInvoice(ByVal id As String)

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(6) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", id, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@POSTINGDATE", txtPostingDate.Text, SqlDbType.DateTime)


        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostDriverCharges", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)

                stTrans.Rollback()
                Posted = False
                Exit Sub
            Else
                stTrans.Commit()
                Posted = True
                usrMessageBar.ShowNotification("Posted Sucessfully..!", UserControls_usrMessageBar.WarningType.Success)
            End If

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, id, "Posting", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Posted = False
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Property TPTDriverCharges() As DataTable
        Get
            Return ViewState("TPTDriverCharges")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTDriverCharges") = value
        End Set
    End Property

    Private Sub CopyDriverChargeDetails()

        Dim NewDate As Date
        Dim Newdate1 As Date

        NewDate = Format(CDate(txtDate.Text.ToString), "dd/MMM/yyyy")
        Newdate1 = NewDate.AddMonths(1)
        Newdate1 = New Date(NewDate.AddMonths(1).Year, NewDate.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1)
        Newdate1 = Format(CDate(Newdate1), "dd/MMM/yyyy")
        Dim CParms(4) As SqlClient.SqlParameter

        CParms(0) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
        CParms(0).Value = NewDate
        CParms(1) = New SqlClient.SqlParameter("@DATE1", SqlDbType.DateTime)
        CParms(1).Value = Newdate1

        CParms(2) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar, 2000)
        CParms(2).Value = "DRIVER"
        CParms(3) = New SqlClient.SqlParameter("@BSU", SqlDbType.VarChar, 6)
        CParms(3).Value = Session("sBsuid")

        CParms(4) = New SqlClient.SqlParameter("@Errormsg", SqlDbType.VarChar, 1000)
        CParms(4).Direction = ParameterDirection.Output

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "CopyInvoiceDetails", CParms)
            Dim ErrorMSG As String = CParms(4).Value.ToString()


            If ErrorMSG = "" Then
                stTrans.Commit()
                usrMessageBar.ShowNotification("Copied Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                txtDate.Text = Newdate1.ToString("dd/MMM/yyyy")
                refreshGrid()

            Else
                usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
                refreshGrid()
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
            refreshGrid()

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

        'lblError.Text = ""
        'Dim PurposeExists As Integer = 1

        'If Mainclass.getDataValue("select COUNT(*) from TPTDRIVERCHARGES where tdc_posted=0 and TDC_DELETED=0 and TDC_TRDATE<='" & txtDate.Text & "' and  Year(tdc_trdate)<=year('" & txtDate.Text & "') and TDC_BSU_ID='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString") > 0 Then
        '    lblError.Text &= " This month some driver charges are not posted.Post or Delete that entries. "
        'End If

        'If lblError.Text.Length > 0 Then
        '    lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
        '    lblError.Visible = True
        '    Return
        'End If

        'Dim objConn As New SqlConnection(connectionString)
        'objConn.Open()
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        'Dim NewDate As Date
        'Dim Newdate1 As Date


        'NewDate = Format(CDate(txtDate.Text.ToString), "dd/MMM/yyyy")
        'Newdate1 = NewDate.AddMonths(1)
        'Newdate1 = New Date(NewDate.AddMonths(1).Year, NewDate.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1)
        'Newdate1 = Format(CDate(Newdate1), "dd/MMM/yyyy")

        'Dim CParms(3) As SqlParameter
        'CParms(0) = Mainclass.CreateSqlParameter("@DATE", NewDate, SqlDbType.DateTime)
        'CParms(1) = Mainclass.CreateSqlParameter("@DATE1", Newdate1, SqlDbType.DateTime)
        'CParms(2) = Mainclass.CreateSqlParameter("@TYPE", "DRIVER", SqlDbType.VarChar)
        'CParms(3) = Mainclass.CreateSqlParameter("@BSU", Session("sBsuid"), SqlDbType.VarChar)

        'Try
        '    Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "CopyInvoiceDetails", CParms)
        '    If RetVal = "-1" Then
        '        lblError.Text = "Unexpected Error !!!"
        '        stTrans.Rollback()
        '        Exit Sub
        '    End If
        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        '    stTrans.Rollback()
        '    Exit Sub
        'End Try
        'stTrans.Commit()
        'txtDate.Text = Newdate1.ToString("dd/MMM/yyyy")
        'refreshGrid()
        'lblError.Text = "Copied Successfully !!!"
    End Sub
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click, btnCopy1.Click

        CopyDriverChargeDetails()


        'Dim NewDate As DateTime
        'Dim Newdate1 As DateTime
        'Dim RowCount As Integer

        'For RowCount = 0 To TPTDriverCharges.Rows.Count - 1
        '    Dim pParms(32) As SqlParameter
        '    pParms(1) = Mainclass.CreateSqlParameter("@TDC_ID", 0, SqlDbType.Int, True)
        '    NewDate = TPTDriverCharges.Rows(RowCount)("DATE")
        '    Newdate1 = NewDate.AddMonths(1)
        '    Newdate1 = New DateTime(NewDate.AddMonths(1).Year, NewDate.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1)
        '    pParms(2) = Mainclass.CreateSqlParameter("@TDC_TRDATE", Newdate1, SqlDbType.SmallDateTime)
        '    pParms(3) = Mainclass.CreateSqlParameter("@TDC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        '    pParms(4) = Mainclass.CreateSqlParameter("@TDC_CTO_BSU_ID", TPTDriverCharges.Rows(RowCount)("TDC_CTO_BSU_ID"), SqlDbType.VarChar)
        '    pParms(5) = Mainclass.CreateSqlParameter("@TDC_DRIVER", TPTDriverCharges.Rows(RowCount)("TDC_EMP_ID"), SqlDbType.VarChar)
        '    pParms(6) = Mainclass.CreateSqlParameter("@TDC_DRIVERTYPE", TPTDriverCharges.Rows(RowCount)("TDC_DRIVERTYPE"), SqlDbType.VarChar)
        '    pParms(7) = Mainclass.CreateSqlParameter("@TDC_BASIC", TPTDriverCharges.Rows(RowCount)("BASIC"), SqlDbType.Float)
        '    pParms(8) = Mainclass.CreateSqlParameter("@TDC_FIRSTALLOW", TPTDriverCharges.Rows(RowCount)("FIRST ALLOW"), SqlDbType.Float)
        '    pParms(9) = Mainclass.CreateSqlParameter("@TDC_SPLALLOW", TPTDriverCharges.Rows(RowCount)("SPL ALLOW"), SqlDbType.Float)
        '    pParms(10) = Mainclass.CreateSqlParameter("@TDC_MOBILE", TPTDriverCharges.Rows(RowCount)("MOBILE"), SqlDbType.Float)
        '    pParms(11) = Mainclass.CreateSqlParameter("@TDC_UTILITY", TPTDriverCharges.Rows(RowCount)("UTILITY"), SqlDbType.Float)
        '    pParms(12) = Mainclass.CreateSqlParameter("@TDC_MEDICAL", TPTDriverCharges.Rows(RowCount)("MEDICAL"), SqlDbType.Float)
        '    pParms(13) = Mainclass.CreateSqlParameter("@TDC_AIRFARE", TPTDriverCharges.Rows(RowCount)("AIRFARE"), SqlDbType.Float)
        '    pParms(14) = Mainclass.CreateSqlParameter("@TDC_ACCOMMODATION", TPTDriverCharges.Rows(RowCount)("ACCOMMODATION"), SqlDbType.Float)
        '    pParms(15) = Mainclass.CreateSqlParameter("@TDC_VISA", TPTDriverCharges.Rows(RowCount)("VISA"), SqlDbType.Float)
        '    pParms(16) = Mainclass.CreateSqlParameter("@TDC_GRATUITY", TPTDriverCharges.Rows(RowCount)("GRATUITY"), SqlDbType.Float)
        '    pParms(17) = Mainclass.CreateSqlParameter("@TDC_UNIFORM", TPTDriverCharges.Rows(RowCount)("UNIFORM"), SqlDbType.Float)
        '    pParms(19) = Mainclass.CreateSqlParameter("@TDC_CTC", TPTDriverCharges.Rows(RowCount)("CTC"), SqlDbType.Float)
        '    pParms(20) = Mainclass.CreateSqlParameter("@TDC_OVERHEAD", TPTDriverCharges.Rows(RowCount)("OVER HEAD"), SqlDbType.Float)
        '    pParms(21) = Mainclass.CreateSqlParameter("@TDC_TOTALCHARGING", TPTDriverCharges.Rows(RowCount)("TOTAL CHARGING") - TPTDriverCharges.Rows(RowCount)("DISCOUNT"), SqlDbType.Float)
        '    pParms(22) = Mainclass.CreateSqlParameter("@TDC_REMARKS", TPTDriverCharges.Rows(RowCount)("REMARKS"), SqlDbType.VarChar)
        '    pParms(23) = Mainclass.CreateSqlParameter("@TDC_USER", Session("sUsr_name"), SqlDbType.VarChar)
        '    pParms(24) = Mainclass.CreateSqlParameter("@TDC_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        '    pParms(25) = Mainclass.CreateSqlParameter("@POSTING", 1, SqlDbType.Int)
        '    pParms(26) = Mainclass.CreateSqlParameter("@TDC_DISCOUNT", 0, SqlDbType.Float)
        '    pParms(27) = Mainclass.CreateSqlParameter("@TDC_NOTES", TPTDriverCharges.Rows(RowCount)("NOTES"), SqlDbType.VarChar)
        '    pParms(28) = Mainclass.CreateSqlParameter("@TDC_PRGM_DIM_CODE", TPTDriverCharges.Rows(RowCount)("DAX_ID"), SqlDbType.VarChar)
        '    pParms(29) = Mainclass.CreateSqlParameter("@TDC_TAX_CODE", TPTDriverCharges.Rows(RowCount)("TDC_TAX_CODE"), SqlDbType.VarChar)
        '    pParms(30) = Mainclass.CreateSqlParameter("@TDC_TAX_AMOUNT", 0, SqlDbType.Float)
        '    pParms(31) = Mainclass.CreateSqlParameter("@TDC_TAX_NET_AMOUNT", TPTDriverCharges.Rows(RowCount)("TOTAL CHARGING") - TPTDriverCharges.Rows(RowCount)("DISCOUNT"), SqlDbType.Float)
        '    pParms(32) = Mainclass.CreateSqlParameter("@TDC_EMR_CODE", TPTDriverCharges.Rows(RowCount)("TDC_EMR_CODE"), SqlDbType.VarChar)

        '    Try
        '        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTDRIVERCHARGE", pParms)
        '        If RetVal = "-1" Then
        '            lblError.Text = "Unexpected Error !!!"
        '            stTrans.Rollback()
        '            Exit Sub
        '        End If
        '    Catch ex As Exception
        '        Errorlog(ex.Message)
        '        lblError.Text = ex.Message
        '    End Try
        'Next
        'stTrans.Commit()
        'SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTDRIVERCHARGES set TDC_POSTED=1,TDC_USER='" & Session("sUsr_name") & "',TDC_POSTING_DATE=GETDATE() where MONTH(TDC_TRDATE )=" & (Newdate1.Month - 1))
        'txtDate.Text = Newdate1.ToString("dd/MMM/yyyy")
        'refreshGrid()
        'lblError.Text = "Copied Successfully !!!"
    End Sub

    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Driver Charges")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim TAXAmount As Decimal = 0
        Dim NetAmount As Decimal = 0


        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)

            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                Dim lblVAT As Label = TryCast(gvr.FindControl("lblTAXAmt"), Label)
                Dim lblNet As Label = TryCast(gvr.FindControl("lblTAXNetAmt"), Label)

                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    Gross += Val(lblGross.Text)
                    TAXAmount += Val(lblVAT.Text)
                    NetAmount += Val(lblNet.Text)

                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
                TAXAmount = 0
                NetAmount = 0
            End If

        Next
        txtSelectedAmt.Text = Gross
        txtSelVatAmount.Text = TAXAmount
        txtSelNetAmount.Text = NetAmount
    End Sub
    Private Sub CalculateVAT(ByVal bsuid As String)
        Dim dt As New DataTable
        dt = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBSUid") & "','TRANTYPE','DRV_CHG','" & Format(CDate(txtDate.Text.ToString), "dd/MMM/yyyy") & "'," & txtTotalCharges.Text & ",'" & LTrim(RTrim(bsuid)) & "')", "OASIS_TRANSPORTConnectionString")

        If dt.Rows.Count > 0 Then

            Dim lstDrp As New RadComboBoxItem
            lstDrp = radVAT.Items.FindItemByValue(dt.Rows(0)("tax_code"))
            h_VATPerc.Value = dt.Rows(0)("tax_perc_value")
            'lstDrp = radVAT.Items.FindItemByValue("VAT5")
            'h_VATPerc.Value = 7
            If Not lstDrp Is Nothing Then
                radVAT.SelectedValue = lstDrp.Value
            Else
                radVAT.SelectedValue = 0
            End If

            txtVATAmount.Text = ((Val(txtTotalCharges.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = (Val(txtTotalCharges.Text) + Val(txtVATAmount.Text)).ToString("####0.000")

        End If
    End Sub
    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged
        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVAT.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            txtVATAmount.Text = ((Val(txtTotalCharges.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtTotalCharges.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        Else
            radVAT.SelectedValue = 0
            h_VATPerc.Value = radVAT.SelectedValue
            txtVATAmount.Text = ((Val(txtTotalCharges.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtTotalCharges.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub RadCBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadCBSU.SelectedIndexChanged
        CalculateVAT(RadCBSU.SelectedValue)
        SelectEmirate(RadCBSU.SelectedValue)
    End Sub

    Protected Sub rblSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSelection.SelectedIndexChanged
        refreshGrid()

    End Sub
End Class


