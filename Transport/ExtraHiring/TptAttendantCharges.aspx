﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TptAttendantCharges.aspx.vb" Inherits="Transport_ExtraHiring_TptAttendantCharges" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

    <style type="text/css">
        .autocomplete_highlightedListItem_S1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }

       

 .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


    </style>

    <script language="javascript" type="text/javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function parseTime(s) {
            var c = s.split(':');
            return parseInt(c[0]) * 60 + parseInt(c[1]);
        }

        function calc(Type, rate) {

            if
        (
        (document.getElementById("<%=txtBasic.ClientID %>").value != "") &&
        (document.getElementById("<%=txtFirst.ClientID %>").value != "") &&
        (document.getElementById("<%=txtOther.ClientID %>").value != "") &&
        (document.getElementById("<%=txtMobile.ClientID %>").value != "") &&
        (document.getElementById("<%=txtUtility.ClientID %>").value != "") &&
        (document.getElementById("<%=txtMedical.ClientID %>").value != "") &&
        (document.getElementById("<%=txtAirafare.ClientID %>").value != "") &&
        (document.getElementById("<%=txtAccom.ClientID %>").value != "") &&
        (document.getElementById("<%=txtVisa.ClientID %>").value != "") &&
        (document.getElementById("<%=txtGratuity.ClientID %>").value != "") &&
        (document.getElementById("<%=txtUniform.ClientID %>").value != "")) {


                var Grat = (eval(document.getElementById("<%=txtBasic.ClientID %>").value) / 30 * 21) / 12;
                document.getElementById("<%=txtGratuity.ClientID %>").value = Grat.toFixed(2);

                var total = eval(document.getElementById("<%=txtBasic.ClientID %>").value) +
               eval(document.getElementById("<%=txtFirst.ClientID %>").value) +
               eval(document.getElementById("<%=txtOther.ClientID %>").value) +
               eval(document.getElementById("<%=txtMobile.ClientID %>").value) +
               eval(document.getElementById("<%=txtUtility.ClientID %>").value) +
               eval(document.getElementById("<%=txtMedical.ClientID %>").value) +
               eval(document.getElementById("<%=txtAirafare.ClientID %>").value) +
               eval(document.getElementById("<%=txtAccom.ClientID %>").value) +
               eval(document.getElementById("<%=txtVisa.ClientID %>").value) +
               eval(document.getElementById("<%=txtGratuity.ClientID %>").value) +
               eval(document.getElementById("<%=txtUniform.ClientID %>").value)
                ;
                document.getElementById("<%=txtCTC.ClientID %>").value = total;
                document.getElementById("<%=txtOverHead.ClientID %>").value = (total / 100 * 10).toFixed(2);
                document.getElementById("<%=txtTotalCharges.ClientID %>").value = ((eval(document.getElementById("<%=txtCTC.ClientID %>").value) + eval(document.getElementById("<%=txtOverHead.ClientID %>").value)) - eval(document.getElementById("<%=txtDiscount.ClientID %>").value));

                var VATAmt = ((eval(document.getElementById("<%=txtTotalCharges.ClientID %>").value) * (document.getElementById("<%=h_VATPerc.ClientID %>").value))) / 100;
                document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
                document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txtTotalCharges.ClientID %>").value)).toFixed(2);


            }
        }


        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            var Total = 0;
            if (chk_state) {


            }
            else {

            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {

                        if (document.forms[0].elements[i].type == 'checkbox') {
                            if (document.forms[0].elements[i].disabled == false) {
                                document.forms[0].elements[i].checked = chk_state;
                                alert(document.forms[0].elements[15].value);
                            }
                        }
                    }
                }
            }
        }




        function getDriversName() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            pMode = "TPTDRIVERSCHOOL";
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up2");
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtDriver.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];
                __doPostBack('<%= txtDriver.ClientID%>', 'TextChanged');
            }
        }

        function getVehicleType() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTVEHICLETYPE";
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtDriver.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtDriver.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }





        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    </script>
    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Attendant Charges
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">
                            <table align="center" width="100%">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="25" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Attendant Charges</span></font>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="4%"><span class="field-label">Date</span> </td>
                                                <td width="7%">
                                                    <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                        TargetControlID="txtDate" PopupButtonID="lnkDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                                <td width="20%">
                                                    <asp:Button ID="btnFind" runat="server" CssClass="button m-0" Text="Load.." />
                                                </td>
                                                <td width="68%"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <table width="100%" class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th>Charging Unit
                                                </th>
                                                <th>Attendant
                                                </th>
                                                <th>Driver Type
                                                </th>

                                                <th>DAX Code
                                                </th>


                                                <th>Basic
                                                </th>
                                                <th>First Allow.
                                                </th>
                                                <th>Other Spl.Allow.
                                                </th>
                                                <th>Mobile Allow.
                                                </th>
                                                <th>Utility Allow.
                                                </th>
                                                <th>Medical
                                                </th>
                                                <th>AirFare
                                                </th>
                                                <th>Accom.
                                                </th>
                                                <th>Visa
                                                </th>
                                                <th>Gratuity
                                                </th>
                                                <th>Uniform
                                                </th>
                                                <th>CTC
                                                </th>
                                                <th>Overhead 10%
                                                </th>
                                                <th>Discount
                                                </th>
                                                <th>Total charging to units
                                                </th>
                                                <th>VAT Code
                                                </th>
                                                <th>Emirate
                                                </th>
                                                <th>VAT Amount
                                                </th>

                                                <th>Net Amount
                                                </th>

                                                <th align="left">Remarks
                                                </th>
                                                <th align="left">Notes
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtTSRSNo" runat="server" Width="100px" Visible="false"></asp:TextBox>
                                                    <telerik:RadComboBox ID="RadCustomer" Width="350px" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtDriver" runat="server" Width="100px" AutoPostBack="true" align="left"></asp:TextBox>

                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ImageDriver" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClientClick="getDriversName();return false;" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDriverName" runat="server" Width="200px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDriverType" runat="server" Width="200px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadDAXCodes" Width="225" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBasic" style="text-align:right" runat="server" Width="100px" align="left" TabIndex="1"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFirst" style="text-align:right" runat="server" Width="100px" align="left" TabIndex="2"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOther" style="text-align:right" runat="server" Width="100px" TabIndex="3"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMobile" style="text-align:right" runat="server" Width="100px" TabIndex="4"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUtility" style="text-align:right" runat="server" Width="100px" TabIndex="5"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMedical" style="text-align:right" runat="server" Width="100px" TabIndex="6"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAirafare" style="text-align:right" runat="server" Width="100px" TabIndex="7"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAccom" style="text-align:right" runat="server" Width="100px" TabIndex="8"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVisa" style="text-align:right" runat="server" Width="100px" TabIndex="9"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtGratuity" style="text-align:right" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUniform" style="text-align:right" runat="server" Width="100px" TabIndex="10"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCTC" style="text-align:right" runat="server" Width="100px" Enabled="false" TabIndex="1"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOverHead" style="text-align:right" runat="server" Width="100px" Enabled="false" TabIndex="6"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDiscount" style="text-align:right" runat="server" Width="100px" TabIndex="11"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalCharges" style="text-align:right" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="radVAT" Width="275" runat="server" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadEmirate" Width="225" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVATAmount" style="text-align:right" Enabled="false" Width="100px" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNetAmount" style="text-align:right" Enabled="false" Width="100px" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>


                                                <td>
                                                    <asp:TextBox ID="txtAltDriver" runat="server" Width="200px" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TxtNotes" runat="server" Width="200px" TabIndex="13"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="left">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button m-0" Text="Save" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button m-0"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                        <asp:Button ID="btnCopy1" runat="server" CssClass="button m-0" Text="Copy" />
                                        <asp:Button ID="btnPost1" runat="server" CssClass="button m-0" Text="Post" />
                                        <asp:HyperLink ID="btnExport" runat="server">Export to Excel</asp:HyperLink>
                                    </td>
                                    <td width="40%"></td>                                    
                                    <td width="40%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                      <td width="4%">
                                        <asp:Label ID="Postingdate" Text="Posting Date" CssClass="field-label" runat="server"></asp:Label>
                                    </td>
                                    <td width="7%">
                                        <asp:TextBox ID="txtPostingDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgPosting" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtPostingDate" PopupButtonID="ImgPosting">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="4%">
                                        <asp:Label ID="lblSelAmount" CssClass="field-label" Text="Selected Amount" runat="server"></asp:Label>
                                    </td>
                                    <td width="4%">
                                        <asp:TextBox ID="txtSelectedAmt" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="3%">
                                        <asp:Label ID="Label1" Text="VAT Amount" CssClass="field-label" runat="server"></asp:Label>
                                    </td>
                                    <td width="4%">
                                        <asp:TextBox ID="txtSelVAT" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="3%">
                                        <asp:Label ID="Label2" Text="Net Amount" CssClass="field-label" runat="server"></asp:Label>
                                    </td>
                                    <td width="4%">
                                        <asp:TextBox ID="txtSelNet" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="60%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <table width="100%">
                                <tr>
                                    <td width="10%">
                                        <asp:RadioButtonList ID="rblSelection" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="0"><span class="field-label"> Not Posted </span></asp:ListItem>
                                            <asp:ListItem Value="1"><span class="field-label">Posted </span></asp:ListItem>
                                            <asp:ListItem Value="2"><span class="field-label">All </span></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td width="10%">
                                        <telerik:RadComboBox ID="radFilterBSU" Width="300" AllowCustomText="true" Filter="Contains" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>
                                    </td>    
                                    <td width="80%"></td>                              
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row mb-0"
                                Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="ID">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAL" AutoPostBack="true" runat="server" OnCheckedChanged="chkAL_CheckedChanged"></asp:CheckBox>
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("[ID]") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DATE" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle Width="100px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CUSTOMER" HeaderText="Customer" />
                                    <asp:BoundField DataField="EMPLOYEE ID" HeaderText="Emp No" />
                                    <asp:BoundField DataField="EMPLOYEE NAME" HeaderText="Employee Name" />
                                    <asp:BoundField DataField="OASIS ID" HeaderText="Oasis ID" />
                                    <asp:BoundField DataField="DRIVER TYPE" HeaderText="Driver Type" />
                                    <asp:BoundField DataField="DAX_DESCR" HeaderText="DAX CODE" />
                                    <asp:BoundField DataField="BASIC" HeaderText="Basic" />
                                    <asp:BoundField DataField="FIRST ALLOW" HeaderText="First Allow." />
                                    <asp:BoundField DataField="SPL ALLOW" HeaderText="Other Spl.Allow." />
                                    <asp:BoundField DataField="MOBILE" HeaderText="Mobile Allow." />
                                    <asp:BoundField DataField="UTILITY" HeaderText="Utility Allow." />
                                    <asp:BoundField DataField="MEDICAL" HeaderText="Medical" />
                                    <asp:BoundField DataField="AIRFARE" HeaderText="AirFare" />
                                    <asp:BoundField DataField="ACCOMMODATION" HeaderText="Accom." />
                                    <asp:BoundField DataField="VISA" HeaderText="Visa" />
                                    <asp:BoundField DataField="GRATUITY" HeaderText="Gratuity" />
                                    <asp:BoundField DataField="UNIFORM" HeaderText="Uniform" />
                                    <asp:BoundField DataField="CTC" HeaderText="CTC" />
                                    <asp:BoundField DataField="OVER HEAD" HeaderText="OverHead" />
                                    <asp:BoundField DataField="DISCOUNT" HeaderText="Discount" />
                                    <%--<asp:BoundField DataField="TOTAL CHARGING" HeaderText="Total Charging to Unit" />--%>
                                    <asp:TemplateField HeaderText="Total Charging to Unit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Eval("TOTAL CHARGING") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TAC_TAX_CODE" HeaderText="VAT Code" />
                                    <asp:BoundField DataField="EMIRATE" HeaderText="Emirate" />
                                    <%--<asp:BoundField DataField="TAC_TAX_AMOUNT" HeaderText="VAT Amount" />
                        <asp:BoundField DataField="TAC_TAX_NET_AMOUNT" HeaderText="Net Amount" />--%>

                                    <asp:TemplateField HeaderText="VAT Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVatAmt" runat="server" Text='<%# Eval("TAC_TAX_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVatNetAmt" runat="server" Text='<%# Eval("TAC_TAX_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="INV NO" HeaderText="Invoice" />
                                    <asp:BoundField DataField="REMARKS" HeaderText="Remarks" />
                                    <asp:BoundField DataField="NOTES" HeaderText="Notes" />
                                    <asp:BoundField DataField="USER NAME" HeaderText="User Name" />
                                    <asp:BoundField DataField="IJV NO" HeaderText="Posted?" />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTDCID" runat="server" Text='<%# Eval("[ID]") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPOstingDate" runat="server" Text='<%# Eval("TAC_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" OnClick="lnkPrint_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTAC_invNo" runat="server" Text='<%# Eval("[INV NO]") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPDate" runat="server" Text='<%# Eval("TAC_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmail" runat="server" Text="Email" OnClick="lnkEmail_CLick"></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                     <td width="20%" align="left">
                                        <asp:Button ID="btnCopy" runat="server" CssClass="button m-0" Text="Copy" />
                                        <asp:Button ID="btnPost" runat="server" CssClass="button m-0" Text="Post" />
                                        <asp:HiddenField ID="hdngross" runat="server" />
                                    </td>
                                    <td width="40%"></td>
                                   
                                    <td width="40%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="3">
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_mimimum" runat="server" />
                            <asp:HiddenField ID="h_TEI_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_VehicleType" runat="server" />
                            <asp:HiddenField ID="h_VATPerc" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

