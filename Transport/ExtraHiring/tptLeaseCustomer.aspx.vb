﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class Transport_ExtraHiring_tptLeaseCustomer
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200118") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If


                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean

        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        txtAccountCode.Enabled = EditAllowed
        txtAddress.Enabled = EditAllowed
        txtCustomer.Enabled = EditAllowed
        txtEmail.Enabled = EditAllowed

        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable

        btnDelete.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("SELECT [TLC_ID],TLC_DESCR ,TLC_ACT_ID ,[TLC_ADDRESS],TLC_EMAIL FROM TPTLEASECUSTOMER WHERE TLC_ID=" & h_EntryId.Value)

                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then

                    txtCustomer.Text = dt.Rows(0)("TLC_DESCR")
                    txtAccountCode.Text = dt.Rows(0)("TLC_ACT_ID")
                    txtAddress.Text = dt.Rows(0)("TLC_ADDRESS")
                    txtEmail.Text = dt.Rows(0)("TLC_EMAIL")
                    h_EntryId.Value = dt.Rows(0)("TLC_ID")

                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub ClearDetails()
        txtAccountCode.Text = ""
        txtAddress.Text = ""
        txtCustomer.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update  TPTVEHICLEMAINTENANCE set TVM_DELETED=1 where TVM_ID=@TRNO"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@TRNO", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        If txtAccountCode.Text.Trim.Length = 0 Then strMandatory.Append("Enter Account Code,")
        If txtAddress.Text.Trim.Length = 0 Then strMandatory.Append("Enter Address,")
        If txtCustomer.Text.Trim.Length = 0 Then strMandatory.Append("Enter Customer Name,")

        If txtEmail.Text <> "" Then
            Dim s As String = txtEmail.Text
            Dim words As String() = s.Split(New Char() {","c})

            Dim word As String

            For Each word In words
                If word <> "" Then
                    If (Not word.Contains(".") Or Not word.Contains("@")) Then strMandatory.Append("Invalid Email Id " & word & ",")
                End If
            Next
        End If

        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                'lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
                'usrMessageBar.ShowNotification(strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory", UserControls_usrMessageBar.WarningType.Danger)
                usrMessageBar.ShowNotification(strMandatory.ToString, UserControls_usrMessageBar.WarningType.Danger)
            End If
            'lblError.Text &= strError.ToString


            Exit Sub
        End If
        



        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(7) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@TLC_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@TLC_DESCR", txtCustomer.Text, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@TLC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@TLC_ACT_ID", txtAccountCode.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@TLC_ADDRESS", txtAddress.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@TLC_USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@TLC_EMAIL", LTrim(RTrim(txtEmail.Text)), SqlDbType.VarChar)



        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTLEASECUSTOMER", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()

            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))


            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

            'lblError.Text = "Data Saved Successfully !!!"
            usrMessageBar.ShowNotification("Data saved Successfully..", UserControls_usrMessageBar.WarningType.Success)

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class




