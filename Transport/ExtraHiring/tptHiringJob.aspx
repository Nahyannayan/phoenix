<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptHiringJob.aspx.vb" Inherits="tptHiringBilling" Title="Extra Trip Jobs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
   
    </style>
    <script language="javascript" type="text/javascript">


        function confirm_delete() {

            if (confirm("You are about to cancel this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function getLocation() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTLOCATION"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            var location = document.getElementById("<%=hTPT_Loc_ID.ClientID %>").value;
            result = radopen(url, "pop_up");
        <%--if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById("<%=txtLocation.ClientID %>").value = NameandCode[1];
        document.getElementById("<%=hTPT_Loc_ID.ClientID %>").value = NameandCode[0];--%>
        }


        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtLocation.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hTPT_Loc_ID.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtLocation.ClientID %>', 'TextChanged');
            }
        }


        function getSeason() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTSEASON"
            //    url = "../../common/PopupSelect.aspx?id=" + pMode;
            var Client_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id;

            result = radopen(url, "pop_up2");
       <%-- if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById("<%=txtSeason.ClientID %>").value = NameandCode[1];
     document.getElementById("<%=hSeason.ClientID %>").value = NameandCode[0];--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtSeason.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hSeason.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtSeason.ClientID %>', 'TextChanged');
            }
        }

        function getVehicleCategory() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTVEHICLETYPE"
            var Client_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id;

            result = radopen(url, "pop_up3");
    <%-- if (result == '' || result == undefined) {
         return false;
     }
     NameandCode = result.split('___');
     document.getElementById("<%=txtTRN_Veh_CatId.ClientID %>").value = NameandCode[1];
    document.getElementById("<%=hdnTRN_Veh_CatId.ClientID %>").value = NameandCode[0];--%>
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtTRN_Veh_CatId.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hdnTRN_Veh_CatId.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtTRN_Veh_CatId.ClientID%>', 'TextChanged');
            }
        }

        function getSalesMan() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTSALESMANNEW"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up4");
   <%--  if (result == '' || result == undefined) {
         return false;
     }
     NameandCode = result.split('___');
     document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[1];
    document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = NameandCode[0];--%>
        }

        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtTRN_SalesMan.ClientID%>', 'TextChanged');
            }
        }
        function getClientName() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "TPTCLIENT"
            var RB1 = document.getElementById("<%=rblJobType.ClientID%>");
            var radio = RB1.getElementsByTagName("input");
            var label = RB1.getElementsByTagName("label");
            for (var x = 0; x < radio.length; x++) {
                if (radio[x].checked) {
                    var jobTypeSelected = label[x].innerHTML;
                }
            }
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&jobtype=" + jobTypeSelected;
            result = radopen(url, "pop_up5");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            CodeandCode = NameandCode[0].split(";");
            document.getElementById("<%=txtClientName.ClientID %>").value = NameandCode[2];
            document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = CodeandCode[0];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
            document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[3];
            document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = CodeandCode[1];--%>
        }

        function OnClientClose5(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                CodeandCode = NameandCode[0].split(";");
                document.getElementById("<%=txtClientName.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = CodeandCode[0];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = CodeandCode[1];
                __doPostBack('<%= txtClientName.ClientID%>', 'TextChanged');
            }
        }

        function getService() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTSERVICENEW"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up6");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtService.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=hTPT_Service_ID.ClientID %>").value = NameandCode[0];--%>
        }


        function OnClientClose6(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtService.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hTPT_Service_ID.ClientID %>").value = NameandCode[0];

                __doPostBack('<%= txtService.ClientID%>', 'TextChanged');
            }
        }

        function getRateType() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTRATETYPE"

            var Client_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
            //url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id;
            var Jdate = document.getElementById("<%=txtTPT_Date.ClientID%>").value
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id + "&JobDate=" + Jdate;

                    result = radopen(url, "pop_up7");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtRateType.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=hRateType.ClientID %>").value = NameandCode[0];--%>
        }


        function OnClientClose7(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtRateType.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hRateType.ClientID %>").value = NameandCode[0];

                __doPostBack('<%= txtRateType.ClientID%>', 'TextChanged');
            }
        }

        function showDocument() {

            contenttype = document.getElementById('<%=hdnContentType.ClientID %>').value;
            filename = document.getElementById('<%=hdnFileName.ClientID %>').value;
            result = radopen("../../Inventory/IFrameNew.aspx?id=0&path=TRANSPORT&filename=" + filename + "&contenttype=" + contenttype, "pop_up8")
            return false;
        }

        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }

        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up6" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose6"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up7" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose7"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up8" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Hiring and Billing Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle"><asp:Label runat="server" id="rptHeader" Text="Hiring and Billing Details"></asp:Label></td>
                    </tr>--%>
                                <asp:Panel runat="server" ID="pnlJobOrder">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Job Order No</span></td>

                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="txtTrn_No" Enabled="false" runat="server" MaxLength="100"></asp:TextBox></td>
                                        <td align="left" width="20%"><span class="field-label">Job Type</span></td>

                                        <td align="left" style="width: 30%">
                                            <asp:RadioButtonList ID="rblJobType" runat="server"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="1">Customer</asp:ListItem>
                                                <asp:ListItem Value="0">Business Unit</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Order/LPO No</span></td>

                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtLPO_No" runat="server" MaxLength="100"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Group</span></td>

                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtSTS_No" runat="server" MaxLength="15"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Job Date</span><span style="color: red">*</span></td>

                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtTPT_Date" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgTPT_Date" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <asp:TextBox ID="txtTrdate" runat="server" Enabled="false"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTPT_Date" PopupButtonID="imgTPT_Date">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Location</span><span style="color: red">*</span></td>

                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtLocation" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgLocation" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getLocation();return false;" />
                                        <asp:HiddenField ID="hTPT_Loc_ID" runat="server" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Client's Name</span><span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtClientName" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getClientName();return false;" />
                                        <asp:HiddenField ID="hTPT_Client_ID" runat="server" Value="0" />
                                    </td>
                                    <td align="left"><span class="field-label">Sales Man </span><span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTRN_SalesMan" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgTRN_SalesMan" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getSalesMan();return false;" />
                                        <asp:HiddenField ID="hdnTRN_SalesMan" runat="server" Value="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Vehicle Type </span><span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTRN_Veh_CatId" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgTRN_Veh_CatId" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleCategory();return false;" />
                                        <asp:HiddenField ID="hdnTRN_Veh_CatId" runat="server" Value="0" />
                                    </td>
                                    <td align="left"><span class="field-label">Service</span> <span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtService" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgService" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getService();return false;" />
                                        <asp:HiddenField ID="hTPT_Service_ID" runat="server" Value="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Season </span><span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtSeason" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgSeason" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getSeason();return false;" />
                                        <asp:HiddenField ID="hSeason" runat="server" Value="0" />
                                    </td>
                                    <td align="left"><span class="field-label">Rate Type</span> <span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRateType" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getRateType();return false;" />
                                        <asp:HiddenField ID="hRateType" runat="server" Value="0" />
                                    </td>


                                </tr>

                                <tr class="title-bg">
                                    <td valign="middle" align="left" colspan="4">Vehicle Details</td>
                                </tr>
                                <tr>
                                    <%--<td align="left"><span class="field-label">Capacity</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCapacity" runat="server"></asp:TextBox><br />
                                        <asp:Label ID="LblQty" runat="server" Text="Qty" CssClass="field-label"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtQty" runat="server" Width="55%"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Enquiry Method</span></td>
                                    <td align="left" style="width: 30%">
                                        <asp:RadioButton ID="rbCash" runat="server" Checked="True" GroupName="rbEnqMethod" Text="Cash"
                                            CssClass="radiobutton" AutoPostBack="True" />
                                        <asp:RadioButton ID="rbEmail" runat="server" GroupName="rbEnqMethod" Text="Email"
                                            CssClass="radiobutton" AutoPostBack="True" />
                                        <asp:RadioButton ID="rbCredit" runat="server" GroupName="rbEnqMethod" Text="Credit Card"
                                            CssClass="radiobutton" AutoPostBack="True" />
                                    </td>--%>
                                     <td style="width: 50%" colspan="2" >

                                        <table width="100%">
                                        <tr>
                                            <td align="left" style="width: 8%">
                                                <span class="field-label">Capacity</span>
                                            </td>
                                            <td align="left"  style="width: 5%">
                                                <asp:TextBox ID="txtCapacity" runat="server" style="width:75px"></asp:TextBox>
                                            </td>
                                        
                                            <td align="right"  style="width: 2%">
                                            
                                                <asp:Label ID="LblQty" runat="server" Text="Qty" CssClass="field-label"></asp:Label>
                                            </td>
                                        
                                        <td align="left" style="width: 5%">
                                            <asp:TextBox ID="txtQty" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        </table>
                                    </td>
                                    <td align="left" style="width: 20%"><span class="field-label">Enquiry Method</span></td>

                                    <td align="left" style="width: 30%">

                                        <asp:RadioButton ID="rbCash" runat="server" Checked="True" GroupName="rbEnqMethod" Text="Cash"
                                            CssClass="radiobutton" AutoPostBack="True" />

                                        <asp:RadioButton ID="rbEmail" runat="server" GroupName="rbEnqMethod" Text="Email"
                                            CssClass="radiobutton" AutoPostBack="True" />
                                        <asp:RadioButton ID="rbCredit" runat="server" GroupName="rbEnqMethod" Text="Credit Card"
                                            CssClass="radiobutton" AutoPostBack="True" />
                                    </td>

                                </tr>
                                <tr class="title-bg">
                                    <td valign="middle" align="left" colspan="4">Destination Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Pick up Point</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtPickupPoint" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Drop off Point</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDropOffPoint" runat="server"></asp:TextBox></td>
                                </tr>


                                <tr>
                                    <td align="left"><span class="field-label">Starting Time</span><span style="color: red">*</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTPT_JobSt_Time" runat="server"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meeJobStartTime" runat="server" AcceptAMPM="false"
                                            MaskType="Time" Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" ErrorTooltipEnabled="true" UserTimeFormat="None"
                                            TargetControlID="txtTPT_JobSt_Time" InputDirection="LeftToRight" AcceptNegative="Left">
                                        </ajaxToolkit:MaskedEditExtender>
                                        <ajaxToolkit:MaskedEditValidator ID="mevJobStartTime" runat="server" ControlExtender="meeJobStartTime"
                                            ControlToValidate="txtTPT_JobSt_Time" IsValidEmpty="False" EmptyValueMessage="Time is required "
                                            InvalidValueMessage="Time is invalid" Display="Dynamic" EmptyValueBlurredText="Time is required "
                                            InvalidValueBlurredMessage="Invalid Time" ValidationGroup="MKE" />
                                    </td>
                                    <td align="left"><span class="field-label">Closing Time</span><span style="color: red">*</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTPT_JOBCl_Time" runat="server"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meeClosingTime" runat="server" AcceptAMPM="false"
                                            MaskType="Time" Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" ErrorTooltipEnabled="true" UserTimeFormat="None"
                                            TargetControlID="txtTPT_JOBCl_Time" InputDirection="LeftToRight" AcceptNegative="Left">
                                        </ajaxToolkit:MaskedEditExtender>
                                        <ajaxToolkit:MaskedEditValidator ID="mevClosingTime" runat="server" ControlExtender="meeClosingTime"
                                            ControlToValidate="txtTPT_JOBCl_Time" IsValidEmpty="False" EmptyValueMessage="Time is required "
                                            InvalidValueMessage="Time is invalid" Display="Dynamic" EmptyValueBlurredText="Time is required "
                                            InvalidValueBlurredMessage="Invalid Time" ValidationGroup="MKE" />
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left" style="width: 20%"><span class="field-label">Supporting Docs</span>
                                    </td>

                                    <td align="left" style="width: 30%">
                                        <asp:LinkButton ID="btnDocumentLink" Text="view" Visible='false' runat="server" OnClientClick="showDocument();return false;"></asp:LinkButton>
                                        <asp:LinkButton ID="btnDocumentDelete" Text='x' Visible='false' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                        <asp:FileUpload ID="UploadDocPhoto" runat="server" Visible="false" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                        <asp:HiddenField ID="hdnFileName" runat="server" />
                                        <asp:HiddenField ID="hdnContentType" runat="server" />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Visible="false" />
                                    </td>
                                </tr>

                                <tr class="title-bg">
                                    <td valign="middle" align="left" colspan="4"><span class="field-label">Job Description</span><span style="color: red">*</span></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtTPT_Description" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                                <tr class="title-bg" id="trCancelRemarks" runat="server">
                                    <td valign="middle" align="left" colspan="4"><span class="field-label">Cancel Description</span><span style="color: red">*</span></td>
                                </tr>
                                <tr id="trtxtCancelDescr" runat="server">
                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtCancelDescr" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                                <tr class="title-bg" id="trDeclineRemarks" runat="server" visible="false">
                                    <td valign="middle" align="left" colspan="4"><span class="field-label">Decline Description</span><span style="color: red">*</span></td>
                                </tr>
                                <tr id="trDeclineDescr" runat="server" visible="false">
                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtDecline" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                            <asp:Button ID="btnDecline" runat="server" CausesValidation="False" CssClass="button" Text="Decline" />
                            <asp:Button ID="btnConfirm" runat="server" CausesValidation="False" CssClass="button" Text="Confirm" />

                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />

                            <asp:HiddenField ID="h_TPT_BllId" runat="server" Value="" />
                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="h_TRN_Status" Value="0" runat="server" />
                            <asp:HiddenField ID="h_Products" Value="0" runat="server" />
                            <asp:HiddenField ID="h_EnqMethod" runat="server" />
                            <asp:HiddenField ID="h_fileNamewithPath" runat="server" />
                            <asp:HiddenField ID="h_tempFilepath" runat="server" />
                            <asp:HiddenField ID="h_str_img" runat="server" />
                            <asp:HiddenField ID="h_FileName" runat="server" />



                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

