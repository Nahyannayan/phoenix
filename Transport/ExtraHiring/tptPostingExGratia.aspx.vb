﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports Telerik.Web.UI
Partial Class Transport_ExtraHiring_tptPostingExGratia
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200216") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            BindDAXCodes()
            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                If ViewState("datamode") = "add" Then
                    SetDataMode("add")
                Else
                    SetDataMode("view")
                    showNoRecordsFound()
                End If
                setModifyvalues(ViewState("EntryId"))
            End If

            showNoRecordsFound()
            grdInvoiceDetails.DataSource = TPTInvoiceDetails
            grdInvoiceDetails.DataBind()
            showNoRecordsFound()
        Else
            If ViewState("EntryId") = "0" Then
                calcTotals()
            End If

        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        grdInvoiceDetails.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnSAVE.Visible = False
        btnPrint.Visible = mDisable
        btnEmail.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim Amount(3) As String
            Dim bsu_City As String = ""
            Dim bsu_ID As String = ""

            h_EntryId.Value = p_Modifyid
            Dim dtVATUserAccess As New DataTable
            dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")

            If dtVATUserAccess.Rows.Count > 0 Then
                If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                    radVAT.Enabled = True
                    radEmirate.Enabled = True
                Else
                    radVAT.Enabled = False
                    radEmirate.Enabled = False
                End If
            Else
                radVAT.Enabled = False
                radEmirate.Enabled = False
            End If



            If p_Modifyid = "0" Then

            Else

                Dim str_conn As String = connectionString
                Dim dt, dt1 As New DataTable
                Dim NonGems As Boolean = False

                If ViewState("datamode") = "add" Then
                    If Right(p_Modifyid, 3) = "OOD" Then
                        hdnNetTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(EGC_NETAMOUNT),0) FROM TPTEXGRATIACHARGES INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_SHORTNAME=EGC_C_BSU_SHORTNAME WHERE LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND EGC_EGI_ID=0 and  egc_deleted=0 and egc_emp_id<>0 and  EGC_C_BSU_SHORTNAME=BSU_SHORTNAME  GROUP BY  LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")
                    Else
                        hdnNetTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(EGC_NETAMOUNT),0) FROM TPTEXGRATIACHARGES INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID WHERE LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND EGC_EGI_ID=0 and  egc_deleted=0 and egc_emp_id<>0 and  EGC_C_BSU_SHORTNAME=BSU_SHORTNAME  GROUP BY  LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")
                        If hdnNetTotal.Value = "" Then hdnNetTotal.Value = 0
                    End If
                    hdnSTSTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(EGC_NETAMOUNT),0) FROM TPTEXGRATIACHARGES INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID WHERE LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND EGC_EGI_ID=0 and  egc_deleted=0 and egc_emp_id<>0 and EGC_C_BSU_SHORTNAME='STS' GROUP BY  LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")
                    hdnBBTTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(EGC_NETAMOUNT),0) FROM TPTEXGRATIACHARGES INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID WHERE LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND EGC_EGI_ID=0 and  egc_deleted=0 and egc_emp_id<>0 and EGC_C_BSU_SHORTNAME='BBT' GROUP BY  LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")
                    lblbatchstr.Text = p_Modifyid
                    txtBatchNo.Text = "New"
                    bsu_City = Mainclass.getDataValue("SELECT case when case when max(BSU_CITY)='ALN' then 'AUH' else max(BSU_CITY) end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when max(BSU_CITY)='ALN' then 'AUH' else max(BSU_CITY) end end   BSU_CITY   FROM tptexgratiacharges INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID WHERE LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND EGC_EGI_ID=0  GROUP BY  LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")

                    Dim lstDrp1 As New RadComboBoxItem
                    lstDrp1 = radEmirate.Items.FindItemByValue(bsu_City)
                    If Not lstDrp1 Is Nothing Then
                        radEmirate.SelectedValue = lstDrp1.Value
                    Else
                        radEmirate.SelectedValue = "NA"
                    End If
                    txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
                    lblTrDate.Text = Now.ToString("dd/MMM/yyyy")
                    BindDAXCodes()
                    h_EntryId.Value = 0

                    If Right(p_Modifyid, 3) = "OOD" Then
                        fillGridView(grdInvoiceDetails, "select EGC_ID,EGC_EGI_ID,replace(convert(varchar(30), EGC_TRDATE, 106),' ','/') [DATE],EGC_TSTART,EGC_TEND,EGC_TTOTAL,EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE,EGC_DRIVERTYPE,case when EGC_DRIVERTYPE=1 then 'Bus' else case when EGC_DRIVERTYPE=2 then 'Car' else 'Van' end end  DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,EGC_C_BSU_SHORTNAME,BSU_SHORTNAME  FROM TPTEXGRATIACHARGES  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGC_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_SHORTNAME=EGC_C_BSU_SHORTNAME  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGC_EMP_ID where LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "'  and EGC_EGI_ID =0  and egc_deleted=0 and egc_emp_id<>0")
                    Else
                        fillGridView(grdInvoiceDetails, "select EGC_ID,EGC_EGI_ID,replace(convert(varchar(30), EGC_TRDATE, 106),' ','/') [DATE],EGC_TSTART,EGC_TEND,EGC_TTOTAL,EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE,EGC_DRIVERTYPE,case when EGC_DRIVERTYPE=1 then 'Bus' else case when EGC_DRIVERTYPE=2 then 'Car' else 'Van' end end  DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,EGC_C_BSU_SHORTNAME,BSU_SHORTNAME  FROM TPTEXGRATIACHARGES  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGC_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGC_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGC_EMP_ID where LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "'  and EGC_EGI_ID =0  and egc_deleted=0 and egc_emp_id<>0")
                    End If
                    NonGems = Mainclass.getDataValue("select count(*) from oasis..businessunit_m where BSU_SHORTNAME=substring('" & p_Modifyid & "', 8, Len('" & p_Modifyid & "') - 7) and bsu_contra_act_id is null", "OASIS_TRANSPORTConnectionString")

                    If NonGems = True Then
                        txtNetTotal.Enabled = True
                    Else
                        txtNetTotal.Enabled = False
                    End If

                    bsu_ID = Mainclass.getDataValue("select distinct EGC_BSU_ID   from TPTExgratiacharges   LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=EGC_BSU_ID where LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "'  and EGC_EGI_ID =0", "OASIS_TRANSPORTConnectionString")
                    dt1 = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','JOB_ORD','" & txtBatchDate.Text & "'," & txtNetTotal.Text & ",'" & bsu_ID & "')", "OASIS_TRANSPORTConnectionString")
                    If dt1.Rows.Count > 0 Then
                        Dim lstDrp As New RadComboBoxItem
                        lstDrp = radVAT.Items.FindItemByValue(dt1.Rows(0)("tax_code"))
                        h_VATPerc.Value = dt1.Rows(0)("tax_perc_value")
                        If Not lstDrp Is Nothing Then
                            radVAT.SelectedValue = lstDrp.Value
                        Else
                            radVAT.SelectedValue = 0
                        End If
                        txtVatAmount.Text = Convert.ToDouble(Val(txtNetTotal.Text) * h_VATPerc.Value / 100.0).ToString("####0.000")
                        txtNetAmount.Text = Convert.ToDouble(Val(txtNetTotal.Text) + txtVatAmount.Text).ToString("####0.000")

                    End If


                Else
                    dt = MainObj.getRecords("SELECT EGI_ID,isnull(EGI_TRNO,'')EGI_TRNO, replace(convert(varchar(30), EGI_DATE, 106),' ','/') [DATE],EGI_NET, EGI_USER_NAME, EGI_POSTED,EGI_REMARKS, EGI_DATE, EGI_POSTING_DATE,EGI_BSU_ID,EGI_STRNO,EGI_DATE,EGI_JHD_DOCNO,isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(EGI_PRGM_DIM_CODE,'0') DAX_ID,isnull(EGI_TAX_AMOUNT,0)EGI_TAX_AMOUNT,isnull(EGI_TAX_NET_AMOUNT,EGI_NET)EGI_TAX_NET_AMOUNT,isnull(EGI_TAX_CODE,'0')EGI_TAX_CODE,isnull(EGI_EMR_CODE,'NA')EGI_EMR_CODE FROM TPTEXGRATIA_INV left outer join dbo.VV_DAX_CODES on ID=EGI_PRGM_DIM_CODE  WHERE EGI_ID ='" & p_Modifyid & "' and EGI_BSU_ID='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                    If dt.Rows.Count > 0 Then
                        txtBatchDate.Text = Format(IIf(IsDBNull(dt.Rows(0)("EGI_POSTING_DATE")), Now.Date.ToString, dt.Rows(0)("EGI_POSTING_DATE")), "dd/MMM/yyyy")
                        txtBatchNo.Text = dt.Rows(0)("EGI_TRNO")
                        txtremarks.Text = dt.Rows(0)("EGI_REMARKS")
                        lblbatchstr.Text = dt.Rows(0)("EGI_STRNO")
                        lblTrDate.Text = dt.Rows(0)("EGI_DATE")
                        txtNetTotal.Text = dt.Rows(0)("EGI_NET")
                        lblJHDDocNo.Text = dt.Rows(0)("EGI_JHD_DOCNO")
                        txtremarks.Enabled = False
                        btnPost.Visible = False
                        lnkBatchDate.Enabled = False
                        txtBatchDate.Enabled = False
                        lblStatus.Text = "Posted"
                        h_EntryId.Value = dt.Rows(0)("EGI_ID")

                        txtVatAmount.Text = dt.Rows(0)("EGI_TAX_AMOUNT")
                        txtNetAmount.Text = dt.Rows(0)("EGI_TAX_NET_AMOUNT")
                        radVAT.SelectedValue = dt.Rows(0)("EGI_TAX_CODE")
                        radVAT.Enabled = False
                        radEmirate.Enabled = False


                        Dim lstDrp As New RadComboBoxItem
                        lstDrp = radEmirate.Items.FindItemByValue(dt.Rows(0)("EGI_EMR_CODE"))

                        If Not lstDrp Is Nothing Then
                            radEmirate.SelectedValue = lstDrp.Value
                        Else
                            radEmirate.SelectedValue = "NA"
                        End If
                        radEmirate.Enabled = False


                        fillGridView(grdInvoiceDetails, "select EGC_ID,EGC_EGI_ID,replace(convert(varchar(30), EGC_TRDATE, 106),' ','/') [DATE],EGC_TSTART,EGC_TEND,EGC_TTOTAL,EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE,EGC_DRIVERTYPE,case when EGC_DRIVERTYPE=1 then 'Bus' else case when EGC_DRIVERTYPE=2 then 'Car' else 'Van' end end  DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,EGC_C_BSU_SHORTNAME,BSU_SHORTNAME   from TPTEXGRATIACHARGES   LEFT OUTER JOIN VW_DRIVER on DRIVER_EMPID=EGC_EMP_ID LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=EGC_BSU_ID LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGC_EMP_ID where EGC_EGI_ID=" & p_Modifyid)
                        RadDAXCodes.SelectedItem.Text = dt.Rows(0)("DAX_DESCR")
                        RadDAXCodes.SelectedValue = dt.Rows(0)("DAX_ID")
                        RadDAXCodes.Enabled = False
                        showNoRecordsFound()
                    End If
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim strsql3 As String = ""

            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            strsql3 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

            radVAT.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            radVAT.DataTextField = "DESCR"
            radVAT.DataValueField = "ID"
            radVAT.DataBind()

            radEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql3)
            radEmirate.DataTextField = "DESCR"
            radEmirate.DataValueField = "ID"
            radEmirate.DataBind()



        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Property TPTInvoiceDetails() As DataTable
        Get
            Return ViewState("InvoiceDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("InvoiceDetails") = value
        End Set
    End Property

    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTInvoiceDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTInvoiceDetails = mtable
        fillGrdView.DataSource = TPTInvoiceDetails
        fillGrdView.DataBind()
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
        txtBatchNo.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property EGCFooter() As DataTable
        Get
            Return ViewState("EGCFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EGCFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Sub showNoRecordsFound()
        If Not EGCFooter Is Nothing AndAlso EGCFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoiceDetails.Columns.Count - 2
            grdInvoiceDetails.Rows(0).Cells.Clear()
            grdInvoiceDetails.Rows(0).Cells.Add(New TableCell())
            grdInvoiceDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoiceDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdInvoiceDetails.PageIndexChanging
        grdInvoiceDetails.PageIndex = e.NewPageIndex
        grdInvoiceDetails.DataSource = TPTInvoiceDetails
        grdInvoiceDetails.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoiceDetails.RowCancelingEdit
        grdInvoiceDetails.ShowFooter = True
        grdInvoiceDetails.EditIndex = -1
        grdInvoiceDetails.DataSource = EGCFooter
        grdInvoiceDetails.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub grdInvoiceDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoiceDetails.RowDataBound
        If ViewState("datamode") <> "add" Then
            grdInvoiceDetails.Columns(0).Visible = False
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox
        Dim total As Double = 0.0
        Dim gross As Double = 0.0
        Dim salik As Double = 0.0
        Dim net As Double = 0.0
        Dim NonGems As Boolean = False
        Dim ModifiedAmount As Double = 0.0

        NonGems = Mainclass.getDataValue("select count(*) from oasis..businessunit_m where BSU_SHORTNAME=substring('" & LTrim(RTrim(lblbatchstr.Text)) & "', 8, Len('" & LTrim(RTrim(lblbatchstr.Text)) & "') - 7) and bsu_contra_act_id is null", "OASIS_TRANSPORTConnectionString")
        If NonGems = True Then
            ModifiedAmount = txtNetTotal.Text
        End If

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(gvr.FindControl("lblEGCID"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            If ChkBxItem.Checked = True Then

                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                total += Val(lblNet.Text)
            End If
        Next
        'net = (total)
        If IDs = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        If RadDAXCodes.SelectedValue = "0" Then
            lblError.Text = "Please Select DAX Posting Code....!"
            Exit Sub
        End If

        If radEmirate.SelectedValue = "NA" Then
            lblError.Text = "Please Select Emirate....!"
            Exit Sub
        End If


        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(15) As SqlParameter
        Dim pParms1(1) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@EGI_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@EGI_STRNO", lblbatchstr.Text, SqlDbType.VarChar)
        If NonGems = True Then
            pParms(3) = Mainclass.CreateSqlParameter("@EGI_NET", ModifiedAmount, SqlDbType.Float)
        Else
            pParms(3) = Mainclass.CreateSqlParameter("@EGI_NET", Val(txtNetTotal.Text), SqlDbType.Float)
        End If

        pParms(4) = Mainclass.CreateSqlParameter("@EGI_USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@EGI_REMARKS", txtremarks.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@EGI_DATE", lblTrDate.Text, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@EGI_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@EGI_POSTING_DATE", txtBatchDate.Text, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@EGI_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@EGC_IDS", IDs, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@EGI_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@EGI_TAX_AMOUNT", txtVatAmount.Text, SqlDbType.Float)
        pParms(13) = Mainclass.CreateSqlParameter("@EGI_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
        pParms(14) = Mainclass.CreateSqlParameter("@EGI_TAX_CODE", radVAT.SelectedValue, SqlDbType.VarChar)
        pParms(15) = Mainclass.CreateSqlParameter("@EGI_EMR_CODE", radEmirate.SelectedValue, SqlDbType.VarChar)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostExGratiaCharges", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblbatchstr.Text, "Posting", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(lblbatchstr.Text)
            'lblError.Text = "Data Saved Successfully !!!"
            usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As String = CType(h_EntryId.Value, String)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTInvoiceExGratia"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EGI_ID", h_EntryId.Value, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@sBSUID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            Dim InvNo As String = ""
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "EXGRATIA CHARGES INVOICE"
            params("LPO") = ""
            params("InvNo") = txtBatchNo.Text
            params("InvDate") = txtBatchDate.Text
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            If Session("sBsuid") = "900501" Then
                'If Format(CDate(txtBatchDate.Text), "dd-MMM-yyyy") <= "31/Dec/2017" Then
                If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTInvoiceExGratia.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoiceExGratia.rpt"
                End If

            Else
                'If Format(CDate(txtBatchDate.Text), "dd-MMM-yyyy") <= "31/Dec/2017" Then
                If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTInvoiceExGratiaBBT.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoiceExGratiaBBT.rpt"
                End If

            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub UpdateAddress()
    End Sub
    Sub calcTotals()
        Dim NonGems As Boolean = False
        Dim ModifiedAmount As Double = 0.0
        Dim NetTotal As Decimal = 0, STSTotal As Decimal = 0, BBTTotal As Decimal = 0

        NonGems = Mainclass.getDataValue("select count(*) from oasis..businessunit_m where BSU_SHORTNAME=substring('" & LTrim(RTrim(lblbatchstr.Text)) & "', 8, Len('" & LTrim(RTrim(lblbatchstr.Text)) & "') - 7) and bsu_contra_act_id is null", "OASIS_TRANSPORTConnectionString")
        If txtNetTotal.Text = "" Then
            ModifiedAmount = 0.0
        Else
            ModifiedAmount = txtNetTotal.Text
        End If


        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            Dim lblBSUSHORTNAME As Label = TryCast(gvr.FindControl("lblBSUSHORTNAME"), Label)
            Dim lblCBSUSHORTNAME As Label = TryCast(gvr.FindControl("lblCBSUSHORTNAME"), Label)
            If ChkBxItem.Checked = True Then
                If lblBSUSHORTNAME.Text = lblCBSUSHORTNAME.Text Then
                    NetTotal += Val(lblNet.Text)
                ElseIf lblCBSUSHORTNAME.Text = "STS" Then
                    STSTotal += Val(lblNet.Text)
                Else
                    BBTTotal += Val(lblNet.Text)
                End If
            End If
        Next

        NonGems = Mainclass.getDataValue("select count(*) from oasis..businessunit_m where BSU_SHORTNAME=substring('" & LTrim(RTrim(lblbatchstr.Text)) & "', 8, Len('" & LTrim(RTrim(lblbatchstr.Text)) & "') - 7) and bsu_contra_act_id is null", "OASIS_TRANSPORTConnectionString")
        If NonGems = True Then
            txtNetTotal.Text = ModifiedAmount
            txtSTSAMOUNT.Text = STSTotal
            txtBBTAMOUNT.Text = BBTTotal

        Else
            txtNetTotal.Text = NetTotal
            txtSTSAMOUNT.Text = STSTotal
            txtBBTAMOUNT.Text = BBTTotal
        End If

        txtVatAmount.Text = (Val(txtNetTotal.Text) * h_VATPerc.Value) / 100.0
        txtNetAmount.Text = Val(txtNetTotal.Text) + Val(txtVatAmount.Text)
    End Sub
    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmail.Click
        Dim bEmailSuccess As Boolean
        SendEmail(CType(h_EntryId.Value, String), bEmailSuccess)
        If bEmailSuccess = True Then
            'lblError.Text = "Email send  Sucessfully...!"
            usrMessageBar.ShowNotification("Email send  Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
        Else
            'lblError.Text = "Error while sending email.."
            usrMessageBar.ShowNotification("Error while sending email..", UserControls_usrMessageBar.WarningType.Danger)
        End If

    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""
        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("userName", "SYSTEM")
        param("reportCaption") = "EXGRATIA CHARGES INVOICE"
        param("LPO") = ""
        param("InvNo") = txtBatchNo.Text
        param("InvDate") = txtBatchDate.Text
        param("@EGI_ID") = h_EntryId.Value
        param("@sBSUID") = HttpContext.Current.Session("sBsuid")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        'param.Add("@IMG_TYPE", "LOGO")
        If HttpContext.Current.Session("sBsuid") = "900501" Then
            'If Format(CDate(txtBatchDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExGratiaEmail.rpt")
            Else
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExGratiaEmailTAX.rpt")
            End If

        Else
            'If Format(CDate(txtBatchDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(txtBatchDate.Text) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExGratiaBBTEmail.rpt")
            Else
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceExGratiaBBTEmailTAX.rpt")
            End If

        End If
        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = LTrim(RTrim(txtBatchNo.Text))
        rptDownload.InvType = "EXGRATIA"
        rptDownload.FCO_ID = 0
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            'lblError.Text = "Email send  Sucessfully...!"
            usrMessageBar.ShowNotification("Email send  Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  'EXGRATIA Email Sent Sucessfully','EXGRATIA'" & ",'EXGRATIA Email Sent Sucessfully','" & Session("sUsr_name") & "','" & RecNo & "'")
        Else
            'lblError.Text = "Error while sending email.."
            usrMessageBar.ShowNotification("Error while sending email..", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Function
    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged

        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,isnull(tax_perc_value,0)tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVAT.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")
        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
        Else
            radVAT.SelectedValue = 0
            h_VATPerc.Value = 0
        End If
        calcTotals()
    End Sub
    Private Sub txtNetTotal_TextChanged(sender As Object, e As EventArgs) Handles txtNetTotal.TextChanged
        calcTotals()
    End Sub
End Class


