﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime




Partial Class Transport_ExtraHiring_NESExgratiaDetails
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim UserAccess As String = ""

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200218") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtTStart.Attributes.Add("onblur", " return calc('T');")
                txtTEnd.Attributes.Add("onblur", " return calc('T');")

                sqlStr = "SELECT EGN_ID, replace(convert(varchar(30), EGN_TRDATE, 106),' ','/') EGN_TRDATE,EGN_BSU_ID, EGN_EMP_ID, EGN_TSTART, EGN_TEND, EGN_TTOTAL,   "
                sqlStr &= " EGN_REMARKS, EGN_EGI_ID, EGN_FYEAR, EGN_USER, EGN_DELETED, EGN_DATE,EGN_DRIVERTYPE,case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA'  else 'IIT' end end DriverType"
                sqlStr &= ",DRIVER,DRIVER_EMPNO,BSU_NAME,isnull(EGN_PURPOSE,'') TMS_DESCR,EGN_HOLIDAY,EGN_OUTSTATION,EGN_OVERNIGHTSTAY  FROM TPTEXGRATIACHARGESNES  inner join VW_DRIVER  on driver_empid=EGN_EMP_ID "
                sqlStr &= "  inner join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID  "
                'UserAccess = Mainclass.getDataValue("select NEU_ACCESS from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "'", "OASIS_TRANSPORTConnectionString")

                CheckUserAccess()
                BindBsu()
                BindPurpose()
                clearScreen()
                If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                checkHolidaysRate()
                BindChargingBsu()


                refreshGrid()
                If Request.QueryString("VIEWID") <> "" Then
                    h_EGN_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_EGN_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            If ChkBxItem.Checked = True Then
                'Gross += Val(lblGross.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
    End Sub

    Protected Sub Check_All(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
    End Sub
    Private Sub refreshGrid()
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'sqlWhere = " where  MONTH(EGN_TRDATE)= MONTH('" & txtDate.Text & "') AND EGN_DELETED=0 and EGN_BSU_ID='" & Session("sbsuid") & "' and EGN_FYEAR='" & Session("F_YEAR") & "'"
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'BindChargingBsu()
        'sqlWhere &= " and case when EGN_DRIVERTYPE =1 then 'NES' else case when EGN_DRIVERTYPE =2 then 'VILLA' else 'IIT' end end  in(select NEU_ACCESS from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' union select 'NES' from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' and neu_access='' union select 'IIT' from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' and neu_access='' union select 'VILLA' from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' and neu_access='') "
        'sqlWhere &= " and month(EGN_TRDATE)=month('" & txtDate.Text & "') order by EGN_TRDATE,EGN_ID "
        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTEXGRATIACHARGESNEW = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTEXGRATIACHARGESNEW
        '    SetExportFileLink()

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try



        Dim ds As New DataSet
        Dim Posted As Integer = 0
        BindChargingBsu()
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 1, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGN_ID", 0, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", "01-01-1900", SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", "01-01-1900", SqlDbType.DateTime)
        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntriesNES", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTEXGRATIACHARGESNEW = ds.Tables(0)
            Session("myData") = TPTEXGRATIACHARGESNEW
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try



    End Sub

    Private Sub refreshGrid1()
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'sqlWhere = " where  MONTH(EGN_TRDATE)= MONTH('" & txtDate.Text & "') AND EGN_DELETED=0 and EGN_BSU_ID='" & Session("sbsuid") & "' and EGN_FYEAR='" & Session("F_YEAR") & "'"
        'sqlWhere &= " and EGN_TRDATE>='" & txtFromDate.Text & "' "
        ''sqlWhere &= "and case when EGN_DRIVERTYPE =1 then 'NES' else case when EGN_DRIVERTYPE =2 then 'VILLA' else 'IIT' end end  in(select NEU_ACCESS from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "')"
        'sqlWhere &= " and case when EGN_DRIVERTYPE =1 then 'NES' else case when EGN_DRIVERTYPE =2 then 'VILLA' else 'IIT' end end  in(select NEU_ACCESS from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' union select 'NES' from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' and neu_access='' union select 'IIT' from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' and neu_access='' union select 'VILLA' from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "' and neu_access='') "
        'sqlWhere &= " and EGN_TRDATE<='" & txtToDate.Text & "' order by EGN_TRDATE "

        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTEXGRATIACHARGESNEW = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTEXGRATIACHARGESNEW
        '    SetExportFileLink()

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try

        Dim ds As New DataSet
        Dim Posted As Integer = 0
        BindChargingBsu()
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 3, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGN_ID", 0, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", txtFromDate.Text, SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", txtToDate.Text, SqlDbType.DateTime)
        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntriesNES", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTEXGRATIACHARGESNEW = ds.Tables(0)
            Session("myData") = TPTEXGRATIACHARGESNEW
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub clearScreen()
        h_EGN_ID.Value = "0"
        h_Emp_id.Value = "0"
        txtOasis.Text = ""
        txtDriverName.Text = ""
        txtTTotal.Text = "0.0"
        txtTStart.Text = "00:00"
        txtTEnd.Text = "00:00"
        txtTTotal.Text = "0.0"
        'txtNetAmount.Text = "0.0"
        TxtRemarks.Text = ""
        chkHoliday.Checked = False
        txtDate.Text = Now.ToString("dd/MMM/yyyy")
        txtPurpose.Text = ""
        txtTStart.Enabled = True
        txtTEnd.Enabled = True
        chkHoliday.Checked = False
        chkOutStation.Checked = False
        chkNightStay.Checked = False
        CheckUserAccess()
    End Sub
    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If Left(txtTStart.Text, 2) > 24 Then lblError.Text &= "Invalid Time in,"
        If Left(txtTEnd.Text, 2) > 24 Then lblError.Text &= "Invalid Time Out,"
        If Right(txtTStart.Text, 2) > 60 Then lblError.Text &= "Invalid Time in,"
        If Right(txtTEnd.Text, 2) > 60 Then lblError.Text &= "Invalid Time Out,"
        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            lblError.Visible = True
            Return
        End If
        checkHolidaysRate()
        Try
            Dim pParms(16) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@EGN_ID", h_EGN_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@EGN_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@EGN_BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@EGN_EMP_ID", h_Emp_id.Value, SqlDbType.Int)
            If rbNES.Checked = True Then
                pParms(5) = Mainclass.CreateSqlParameter("@EGN_DRIVERTYPE", 1, SqlDbType.Int)
            ElseIf rbVILLA.Checked = True Then
                pParms(5) = Mainclass.CreateSqlParameter("@EGN_DRIVERTYPE", 2, SqlDbType.Int)
            Else
                pParms(5) = Mainclass.CreateSqlParameter("@EGN_DRIVERTYPE", 3, SqlDbType.Int)
            End If
            pParms(6) = Mainclass.CreateSqlParameter("@EGN_TSTART", txtTStart.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@EGN_TEND", txtTEnd.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@EGN_TTOTAL", txtTTotal.Text, SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@EGN_REMARKS", TxtRemarks.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@EGN_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(11) = Mainclass.CreateSqlParameter("@EGN_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@EGN_PURPOSE", LTrim(RTrim(txtPurpose.Text)), SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@EGN_HOLIDAY", chkHoliday.Checked, SqlDbType.Bit)
            pParms(14) = Mainclass.CreateSqlParameter("@EGN_OUTSTATION", chkOutStation.Checked, SqlDbType.Bit)
            pParms(15) = Mainclass.CreateSqlParameter("@EGN_OVERNIGHTSTAY", chkNightStay.Checked, SqlDbType.Bit)

            pParms(16) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
            pParms(16).Direction = ParameterDirection.Output

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                'Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTEXGRATIACHARGESNES", pParms)
                'If RetVal = "-1" Then
                '    lblError.Text = "Unexpected Error !!!"
                '    stTrans.Rollback()
                '    Exit Sub
                'Else
                '    stTrans.Commit()
                '    clearScreen()
                '    refreshGrid()
                '    lblError.Text = "Data Saved Successfully !!!"
                'End If
                'Exit Sub
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTEXGRATIACHARGESNES", pParms)
                Dim ErrorMSG As String = pParms(16).Value.ToString()

                If ErrorMSG = "" Then
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_id.Value, "SAVE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        usrMessageBar.ShowNotification("Could not process your request", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                    Else
                        stTrans.Commit()
                        clearScreen()
                        refreshGrid()
                        usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If
                Else
                    usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                End If
                Exit Sub
            Catch ex As Exception
                Errorlog(ex.Message)
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
        checkHolidaysRate()

    End Sub

    Private Sub showEdit(ByVal showId As Integer)


        Dim ds As New DataSet
        Dim Posted As Integer = 0
        BindChargingBsu()
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 2, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGN_ID", showId, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", "01-01-1900", SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", "01-01-1900", SqlDbType.DateTime)
        Try
            'ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntriesNES", pParms)
            Dim rdrExGratiaCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntriesNES", pParms)
            Dim d1 As Date
            Dim dayname1 As String = ""
            Dim HolidayCheck As Integer = 0
            If rdrExGratiaCharges.HasRows Then
                rdrExGratiaCharges.Read()
                h_EGN_ID.Value = rdrExGratiaCharges("EGN_ID")
                txtDate.Text = rdrExGratiaCharges("EGN_TRDATE")
                h_Emp_id.Value = rdrExGratiaCharges("EGN_EMP_ID")
                txtDriverName.Text = rdrExGratiaCharges("DRIVER")
                txtOasis.Text = rdrExGratiaCharges("DRIVER_EMPNO")
                If rdrExGratiaCharges("EGN_DRIVERTYPE") = 1 Then
                    rbNES.Checked = True
                ElseIf rdrExGratiaCharges("EGN_DRIVERTYPE") = 2 Then
                    rbVILLA.Checked = True
                Else
                    rbIIT.Checked = True

                End If

                txtTStart.Text = rdrExGratiaCharges("EGN_TSTART")
                txtTEnd.Text = rdrExGratiaCharges("EGN_TEND")
                txtTTotal.Text = rdrExGratiaCharges("EGN_TTOTAL")
                TxtRemarks.Text = rdrExGratiaCharges("EGN_REMARKS")
                txtPurpose.Text = rdrExGratiaCharges("TMS_DESCR")
                chkHoliday.Checked = rdrExGratiaCharges("EGN_HOLIDAY")
                chkOutStation.Checked = rdrExGratiaCharges("EGN_OUTSTATION")
                chkNightStay.Checked = rdrExGratiaCharges("EGN_OVERNIGHTSTAY")
                d1 = txtDate.Text
                dayname1 = UCase(Format(d1, "dddd"))
                HolidayCheck = Mainclass.getDataValue("select count(*) from TPTEXGRATIAHOLIDAY_M where '" & txtDate.Text & "' between EGH_SDATE and EGH_EDATE", "OASIS_TRANSPORTConnectionString")

                If (dayname1 = "FRIDAY") Or HolidayCheck <> 0 Then
                    h_Holiday.Value = 1
                    If rbNES.Checked = True Then
                        h_Rate.Value = 12

                    End If
                End If
            End If
            rdrExGratiaCharges.Close()
            rdrExGratiaCharges = Nothing

        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try







        'Dim rdrExGratiaCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & " where EGN_ID='" & showId & "'")
        'Dim d1 As Date
        'Dim dayname1 As String = ""
        'Dim HolidayCheck As Integer = 0
        'If rdrExGratiaCharges.HasRows Then
        '    rdrExGratiaCharges.Read()
        '    h_EGN_ID.Value = rdrExGratiaCharges("EGN_ID")
        '    txtDate.Text = rdrExGratiaCharges("EGN_TRDATE")
        '    h_Emp_id.Value = rdrExGratiaCharges("EGN_EMP_ID")
        '    txtDriverName.Text = rdrExGratiaCharges("DRIVER")
        '    txtOasis.Text = rdrExGratiaCharges("DRIVER_EMPNO")
        '    If rdrExGratiaCharges("EGN_DRIVERTYPE") = 1 Then
        '        rbNES.Checked = True
        '    ElseIf rdrExGratiaCharges("EGN_DRIVERTYPE") = 2 Then
        '        rbVILLA.Checked = True
        '    Else
        '        rbIIT.Checked = True

        '    End If
        '    'rblDriverType.SelectedValue = rdrExGratiaCharges("EGN_DRIVERTYPE")
        '    txtTStart.Text = rdrExGratiaCharges("EGN_TSTART")
        '    txtTEnd.Text = rdrExGratiaCharges("EGN_TEND")
        '    txtTTotal.Text = rdrExGratiaCharges("EGN_TTOTAL")
        '    TxtRemarks.Text = rdrExGratiaCharges("EGN_REMARKS")
        '    txtPurpose.Text = rdrExGratiaCharges("TMS_DESCR")
        '    chkHoliday.Checked = rdrExGratiaCharges("EGN_HOLIDAY")
        '    chkOutStation.Checked = rdrExGratiaCharges("EGN_OUTSTATION")
        '    chkNightStay.Checked = rdrExGratiaCharges("EGN_OVERNIGHTSTAY")
        '    d1 = txtDate.Text
        '    dayname1 = UCase(Format(d1, "dddd"))
        '    HolidayCheck = Mainclass.getDataValue("select count(*) from TPTEXGRATIAHOLIDAY_M where '" & txtDate.Text & "' between EGH_SDATE and EGH_EDATE", "OASIS_TRANSPORTConnectionString")

        '    If (dayname1 = "FRIDAY") Or HolidayCheck <> 0 Then
        '        h_Holiday.Value = 1
        '        If rbNES.Checked = True Then
        '            h_Rate.Value = 12

        '        End If
        '    End If
        'End If
        'rdrExGratiaCharges.Close()
        'rdrExGratiaCharges = Nothing
    End Sub

    Protected Sub GridViewShowDetails_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowCreated

    End Sub
    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(14).Text = "0" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(17).Enabled = True
                gvr.Cells(18).Enabled = False
            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
                gvr.Cells(17).Enabled = False
                gvr.Cells(18).Enabled = True
            End If
        Next
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblEGCID"), Label)

        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTEXGRATIACHARGESNES set EGN_DELETED=1,EGN_USER='" & Session("sUsr_name") & "',EGN_DATE=GETDATE() where EGN_ID=" & lblID.Text)
        refreshGrid()
    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTAC_INVNO"), Label)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIACHARGES"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EGN_INVNO", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@EGN_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTEXGRATIACHARGES.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid1()


    End Sub
    Private Sub CheckUserAccess()
        Dim dtAccess As New DataTable
        dtAccess = Mainclass.getDataTable("select NEU_ACCESS from NESEXGRATIAUSERS where neU_usr_name='" & Session("sUsr_name") & "'", ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim mRow As DataRow
        For Each mRow In dtAccess.Rows
            If mRow("NEU_ACCESS") = "" Then
                rbNES.Checked = True
                rbNES.Enabled = True
                rbVILLA.Enabled = True
                rbIIT.Enabled = True
                h_DriverType.Value = 1
            End If
            If mRow("NEU_ACCESS") = "NES" Then
                rbNES.Checked = True
                rbNES.Enabled = True
                h_DriverType.Value = 2
            End If
            If mRow("NEU_ACCESS") = "VILLA" Then
                If rbNES.Enabled <> True Then
                    rbVILLA.Checked = True
                    h_DriverType.Value = 3
                End If
                rbVILLA.Enabled = True
            End If

            If mRow("NEU_ACCESS") = "IIT" Then
                If rbVILLA.Enabled <> True Then
                    rbIIT.Checked = True
                    h_DriverType.Value = 3
                End If
                rbIIT.Enabled = True
            End If
        Next
    End Sub
    Private Sub BindBsu()
        Try
            'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            'Dim BSUParms(2) As SqlParameter
            'BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            'BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            'ddlBusinessUnit.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "gETUNITSFOREXTRATRIPS", BSUParms)
            'ddlBusinessUnit.DataTextField = "BSU_NAME"
            'ddlBusinessUnit.DataValueField = "SVB_BSU_ID"
            'ddlBusinessUnit.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindChargingBsu()
    End Sub
    Private Sub checkHolidaysRate()
        Try
            Dim d As Date
            Dim dayname As String = ""
            Dim Holiday As Integer = 0
            Dim Othr1 As Double = "0.0"
            Dim othr2 As Double = "0.0"
            Dim Rate1 As Integer = 0
            Dim time1 As TimeSpan
            Dim time2 As TimeSpan

            d = txtDate.Text
            dayname = UCase(Format(d, "dddd"))
            Holiday = Mainclass.getDataValue("select count(*) from TPTEXGRATIAHOLIDAY_M where '" & txtDate.Text & "' between EGH_SDATE and EGH_EDATE", "OASIS_TRANSPORTConnectionString")
            h_TotalHrs.Value = Mainclass.getDataValue("select isnull(sum(cast(EGN_TTOTAL as float)),0) from TPTEXGRATIACHARGESNES where EGN_DELETED=0 and EGN_TRDATE='" & txtDate.Text & "' and EGN_EMP_ID='" & h_Emp_id.Value & "'", "OASIS_TRANSPORTConnectionString")

            If (dayname = "FRIDAY") Then
                h_Holiday.Value = 1
                h_Rate.Value = 12
            End If

            'h_DriverType.Value = rblDriverType.SelectedValue


            If ((txtTStart.Text <> "00:00") And (txtTEnd.Text <> "00:00")) Then


                Dim HRS As TimeSpan
                Dim HRS1 As TimeSpan
                Dim hour As String = ""
                Dim hour1 As String = ""
                Dim STime As String = ""
                Dim ETime As String = ""
                Dim STimehrs As String = ""
                Dim ETimehrs As String = ""
                Dim Totalhrs As Double = 0.0
                Dim Totalhrs1 As Double = 0.0
                Dim Amount As Double = 0.0
                Dim OTAmount As Double = 0.0
                Dim St As TimeSpan = TimeSpan.Parse(txtTStart.Text)
                Dim Cl As TimeSpan = TimeSpan.Parse(txtTEnd.Text)
                STime = Left(txtTStart.Text, 5)
                ETime = Left(txtTEnd.Text, 5)
                STimehrs = Left(STime, 2)
                ETimehrs = Left(ETime, 2)
                HRS = (Cl - St)
                If STimehrs > ETimehrs Then
                    HRS = HRS + TimeSpan.FromHours(24)
                End If
                hour = Left(HRS.ToString(), 5)

                If Right(hour, 2) > 30 Then
                    Totalhrs = Val(Left(hour, 2)) + 1
                ElseIf Right(hour, 2) > 0 Then
                    Totalhrs = Val(Left(hour, 2)) + 0.5
                Else
                    Totalhrs = Val(Left(hour, 2))
                End If
                txtTTotal.Text = Totalhrs

                If rbNES.Checked = True Then
                    If h_Holiday.Value = 1 Then
                        'txtNetAmount.Text = Val(txtTTotal.Text) * 12
                    Else
                        If h_TotalHrs.Value >= 12 Then
                            'txtNetAmount.Text = Val(txtTTotal.Text) * 12
                        ElseIf (Val(h_TotalHrs.Value) + Val(txtTTotal.Text)) >= 12 Then
                            'txtNetAmount.Text = (Val(h_TotalHrs.Value) + Val(txtTTotal.Text) - 12) * 12
                        Else
                            'txtNetAmount.Text = 0.0
                        End If
                    End If
                ElseIf rbVILLA.Checked = True Then
                    'txtNetAmount.Text = 72.0
                Else
                    'txtNetAmount.Text = 150.0
                End If
            Else
                txtTTotal.Text = "0.0"

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
        checkHolidaysRate()
    End Sub
    Private Property TPTEXGRATIACHARGESNEW() As DataTable
        Get
            Return ViewState("TPTEXGRATIACHARGESNES")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTEXGRATIACHARGESNES") = value
        End Set
    End Property
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("ExGratia Charges")
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)
            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    'Gross += Val(lblGross.Text)
                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
            End If
        Next
        txtSelectedAmt.Text = Gross
    End Sub
    Protected Sub txtOasis_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOasis.TextChanged
        Dim DriverName As String
        DriverName = Mainclass.getDataValue("select Driver from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtOasis.Text) & "'", "OASIS_TRANSPORTConnectionString")
        h_Emp_id.Value = Mainclass.getDataValue("select driver_empid from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtOasis.Text) & "'", "OASIS_TRANSPORTConnectionString")
        If DriverName <> "" Then
            txtDriverName.Text = DriverName

        Else
            lblError.Text = "Invalid Driver No...!:"
            txtOasis.Text = ""
            txtOasis.Focus()
            Exit Sub
        End If
    End Sub
    Protected Sub rblDriverType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbNES.CheckedChanged, rbVILLA.CheckedChanged, rbIIT.CheckedChanged
        If rbNES.Checked = True Then
            h_DriverType.Value = 1
        ElseIf rbVILLA.Checked = True Then
            h_DriverType.Value = 2
        Else
            h_DriverType.Value = 3
        End If
        checkHolidaysRate()
        BindPurpose()
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIACHARGESNES"
            Dim sqlParam(4) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@FROMDT", txtFromDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@TODT", txtToDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(1))

            sqlParam(2) = Mainclass.CreateSqlParameter("@BSUID", Session("SBSUID"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))

            sqlParam(3) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))

            sqlParam(4) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("preparedby") = Session("sUsr_name")
            params("reportCaption") = "NES Exgratia Charges"
            params("DateRange") = txtFromDate.Text & " - " & txtToDate.Text
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/Reports/rptExGratiaChargesNES.rpt"

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '    Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindPurpose()
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        'Dim SelOption As String = ""
        'Dim BSUParms(2) As SqlParameter
        'If rblDriverType.SelectedValue = 1 Then
        '    SelOption = "1"
        'Else
        '    SelOption = "2"
        'End If
        'BSUParms(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        'BSUParms(2) = Mainclass.CreateSqlParameter("@Option", SelOption, SqlDbType.VarChar)
        'RadPurpose.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TPT_PURPOSE_EXGRATIA", BSUParms)
        'RadPurpose.DataTextField = "DESCR"
        'RadPurpose.DataValueField = "ID"
        'RadPurpose.DataBind()
    End Sub
    Protected Sub txtFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub chkHoliday_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHoliday.CheckedChanged
        Dim d As Date
        Dim dayname As String
        d = txtDate.Text
        dayname = UCase(Format(d, "dddd"))
        If chkHoliday.Checked = True Then
            If (dayname = "FRIDAY") Then
                h_Holiday.Value = 1
                checkHolidaysRate()
            Else
                lblError.Text = "Today is not FriDay"
                chkHoliday.Checked = False
                checkHolidaysRate()
                Exit Sub
            End If
        Else
            h_Holiday.Value = 0
            checkHolidaysRate()
        End If
    End Sub
    Protected Sub txtDriverName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDriverName.TextChanged
        checkHolidaysRate()
    End Sub


    'Protected Sub RadPurpose_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadPurpose.TextChanged
    '    Dim JobOrderNumber = RadPurpose.SelectedItem.Text
    '    Dim Sql As String = "select isnull(tpt_jobst_time,'00:00') tpt_jobst_time,isnull(tpt_jobCl_time,'00:00')tpt_jobCl_time,trn_jobnumberstr,* from tpthiring where trn_status='C' and tpt_bsu_id='" & Session("sbsuid") & "' and trn_jobnumberstr='" & RadPurpose.SelectedItem.Text & "'"
    '    Dim rdrData As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Sql)
    '    If rdrData.HasRows Then
    '        rdrData.Read()
    '        txtTStart.Text = rdrData("tpt_jobst_time")
    '        txtTEnd.Text = rdrData("tpt_jobCl_time")
    '    Else
    '        txtTStart.Text = "00:00"
    '        txtTEnd.Text = "00:00"
    '    End If
    '    checkHolidaysRate()

    '    If JobOrderNumber.Substring(2, 2) = "JB" Then
    '        txtTStart.Enabled = False
    '        txtTEnd.Enabled = False
    '    Else
    '        txtTStart.Enabled = True
    '        txtTEnd.Enabled = True

    '    End If
    'End Sub

    Protected Sub txtPurpose_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPurpose.TextChanged
        Dim Sql As String = "select isnull(tpt_jobst_time,'00:00') tpt_jobst_time,isnull(tpt_jobCl_time,'00:00')tpt_jobCl_time,trn_jobnumberstr,* from tpthiring where trn_status='C' and tpt_bsu_id='" & Session("sbsuid") & "' and trn_jobnumberstr='" & txtPurpose.Text & "'"
        Dim rdrData As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Sql)
        If rdrData.HasRows Then
            rdrData.Read()
            txtTStart.Text = rdrData("tpt_jobst_time")
            txtTEnd.Text = rdrData("tpt_jobCl_time")
        Else
            txtTStart.Text = "00:00"
            txtTEnd.Text = "00:00"
        End If
        checkHolidaysRate()
        If txtPurpose.Text.Length > 2 Then
            If txtPurpose.Text.Substring(2, 2) = "JB" Then
                txtTStart.Enabled = False
                txtTEnd.Enabled = False
            Else
                txtTStart.Enabled = True
                txtTEnd.Enabled = True
            End If
        Else
            txtTStart.Enabled = True
            txtTEnd.Enabled = True
        End If

    End Sub

    Private Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        RecallorApprove("A")
    End Sub

    Private Sub btnRecall_Click(sender As Object, e As EventArgs) Handles btnRecall.Click
        RecallorApprove("R")
    End Sub
    Sub RecallorApprove(ByVal Action As String)

        Dim ids As String = ""
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(gvr.FindControl("lblEGCID"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            If ChkBxItem.Checked = True Then

                ids &= IIf(ids <> "", "|", "") & lblID.Text
            End If
        Next

        If ids = "" Then

            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If


        Try
            Dim pParms(4) As SqlParameter
            pParms(0) = Mainclass.CreateSqlParameter("@IDS", ids, SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            If Action = "R" Then
                pParms(4) = Mainclass.CreateSqlParameter("@OPTION", 1, SqlDbType.Int)
            Else
                pParms(4) = Mainclass.CreateSqlParameter("@OPTION", 2, SqlDbType.Int)
            End If


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "RecallExgratiaChargesNES", pParms)
                If RetVal = "-1" Then
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    clearScreen()
                    refreshGrid()
                    If Action = "R" Then
                        usrMessageBar.ShowNotification("Records Recalled Successfully", UserControls_usrMessageBar.WarningType.Success)
                    Else
                        usrMessageBar.ShowNotification("Records sent for Approval Successfully", UserControls_usrMessageBar.WarningType.Success)
                    End If

                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

End Class






