﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptContractRateBSU.aspx.vb" Inherits="Transport_tptContractRate" Title="Extra Trips Rates and Contracts" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function getClientName() {
           
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
           
            
            pMode = "TPTCLIENT"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&jobtype=Business Unit";
            result = radopen(url, "pop_up");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtClientCode.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtClientName.ClientID %>").value = NameandCode[2];
            document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



 function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                document.getElementById("<%=txtClientCode.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtClientName.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                __doPostBack('<%= txtClientName.ClientID%>', 'TextChanged');
            }
        }


        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Extra Trips Rates and Contracts"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle"><asp:Label runat="server" id="rptHeader" Text="Extra Trips Rates and Contracts"></asp:Label></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Contract No</span></td>
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtCnb_Id" Enabled="false" runat="server"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Contract Date</span><span style="color: red">*</span></td>

                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtCnb_Date" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCnb_Date" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtCnb_Date" PopupButtonID="imgCnb_Date">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Client's Name</span><span style="color: red">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtClientCode" Enabled="false" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:TextBox ID="txtClientName" Enabled="false" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:LinkButton ID="lnlbtnAddClientID" runat="server" Enabled="False">Add</asp:LinkButton>
                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getClientName(); return false;" TabIndex="8" /></td>
                                    <td align="left" width="20%"><span class="field-label">Effective Date</span><span style="color: red">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCnb_Effective_Date" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCnb_Effective_Date" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtCnb_Effective_Date" PopupButtonID="imgCnb_Effective_Date">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Rate per Km</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCnb_RatePerKm" runat="server" Style="text-align: right" TabIndex="7" Text="0"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Rate per Hour</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCnb_RatePerHr" runat="server" Style="text-align: right" TabIndex="7" Text="0"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Bus Type</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBusType" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Min.Rate</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtMinimum" runat="server" Style="text-align: right" TabIndex="7" Text="0"></asp:TextBox></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Narration</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCnb_Print" TextMode="MultiLine" runat="server" TabIndex="23"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Button ID="btnCopy" runat="server" CausesValidation="False" CssClass="button"
                                Text="Copy" TabIndex="27" />
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />

                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="hGridDelete" Value="0" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
