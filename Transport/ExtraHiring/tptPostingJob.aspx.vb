﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports Telerik.Web.UI



Partial Class Transport_ExtraHiring_tptPostingJob
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()
    Dim Posted As Boolean = False


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200140" And ViewState("MainMnu_code") <> "T200140") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            BindBsu()
            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
            Else
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            End If

            showNoRecordsFound()
            grdInvoiceDetails.DataSource = TPTInvoiceDetails
            grdInvoiceDetails.DataBind()
            showNoRecordsFound()
        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        grdInvoiceDetails.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode
        btnPost.Visible = mDisable
        btnDelete.Visible = mDisable 'Or ViewState("MainMnu_code") = "U000086"
        btnPrint.Visible = mDisable
        btnEmail.Visible = mDisable
        RadDAXCodes.Enabled = mDisable
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim strsql3 As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            strsql3 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)

            'radVat.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            'radEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql3)

            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

            'radVat.DataTextField = "DESCR"
            'radVat.DataValueField = "ID"
            'radVat.DataBind()

            'radEmirate.DataTextField = "DESCR"
            'radEmirate.DataValueField = "ID"
            'radEmirate.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = "0" Then
                txtVatAmount.Enabled = False
                txtNetAmount.Enabled = False
                txtTotal.Enabled = False

            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                Dim dt1 As New DataTable
                Dim dtVATUserAccess As New DataTable
                dt = MainObj.getRecords("select max(a.TRN_BATCHSTR) BatchNo,max(a.TRN_BATCHDATE) BatchDate,max(B.ACT_NAME) Client,max(B.ACT_ID) ClientCode,max(ISNULL(a.TRN_STATUS,'')) STATUS,max(a.TRN_BATCH) as TRNNO,max(isnull(isnull(C.TPT_INV_ADDRESS,a.TPT_ADDRESS),'')) Address,max(isnull(TPT_JHD_DOCNO,'')) TPT_JHD_DOCNO,max(isnull(DESCR,'---SELECT ONE---')) DAX_DESCR,max(isnull(TPT_PRGM_DIM_CODE,'0')) DAX_ID ,sum(inv_amt) amount,(select isnull(sum(tpt_tax_amount),0) from tpthiring where TRN_BATCHSTR='" & p_Modifyid & "')  TAX_AMOUNT,max(isnull(TPT_TAX_CODE,'NA')) TAX_CODE,max(ACT_EMR_CODE)ACT_EMR_CODE,isnull(max(tpt_EMR_CODE),'NA')TPT_EMR_CODE,isnull(max(EMR_DESCR),'------SELECT ONE-------')LOC_DESCRIPTION,max(isnull(LOC_ID,'NA')) LOC_ID  from TPTHiring a LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M B on ACT_ID=TPT_Client_id LEFT OUTER JOIN TPT_INVOICEADDESS C on TPT_INV_ACT_ID=TPT_Client_id left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE=TPT_EMR_CODE left outer join dbo.VV_DAX_CODES on ID=TPT_PRGM_DIM_CODE LEFT OUTER JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID   left outer join tpthiringinvoice on TRN_no =INV_TRN_NO  WHERE A.TRN_BATCHSTR='" & p_Modifyid & "'and A.TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                txtVatAmount.Enabled = False
                txtNetAmount.Enabled = False
                txtTotal.Enabled = False

                If dt.Rows.Count > 0 Then
                    txtBatchNo.Text = dt.Rows(0)("BatchNo")
                    txtSupplier.Text = dt.Rows(0)("Client")
                    txtSupplierCode.Text = dt.Rows(0)("ClientCode")
                    txtAddress.Text = dt.Rows(0)("address")
                    lblLoc.Text = "Loc.: " + dt.Rows(0)("LOC_DESCRIPTION")

                    If dt.Rows(0)("Status") = "P" Then
                        btnPost.Visible = False
                        btnDelete.Visible = False
                        lnkBatchDate.Enabled = False
                        lblStatus.Text = dt.Rows(0)("TPT_JHD_DOCNO")
                        RadDAXCodes.SelectedItem.Text = dt.Rows(0)("DAX_DESCR")
                        RadDAXCodes.SelectedValue = dt.Rows(0)("DAX_ID")
                        RadDAXCodes.Enabled = False
                        txtTotal.Text = dt.Rows(0)("amount")
                        txtVatAmount.Text = dt.Rows(0)("TAX_AMOUNT")
                        txtNetAmount.Text = dt.Rows(0)("amount") + dt.Rows(0)("TAX_AMOUNT")
                        txtBatchDate.Text = Format(dt.Rows(0)("BatchDate"), "dd/MMM/yyyy")
                        lblTaxCode.Text = "TAX Code : " + dt.Rows(0)("TAX_CODE")
                    Else
                        btnPost.Visible = True
                        btnDelete.Visible = True
                        lnkBatchDate.Enabled = True
                        lblStatus.Text = "Not Posted"
                        txtBatchDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                        txtTotal.Text = dt.Rows(0)("Amount")
                        txtVatAmount.Text = dt.Rows(0)("TAX_Amount")
                        txtNetAmount.Text = dt.Rows(0)("Amount") + dt.Rows(0)("TAX_Amount")
                        h_taxCode.Value = dt.Rows(0)("TAX_CODE")
                        h_location.Value = dt.Rows(0)("TPT_EMR_CODE")
                        lblTaxCode.Text = "TAX Code : " + dt.Rows(0)("TAX_CODE")
                    End If
                    h_EntryId.Value = dt.Rows(0)("TRNNO")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
            fillGridView(grdInvoiceDetails, "select TRN_BATCHSTR BatchNo,TRN_BATCHDATE BatchDate,TRN_JobnumberStr JobNo,ACT_NAME Client,isnull(SUM(INV_Amt),0) Amount from TPTHiring LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id LEFT OUTER JOIN TPTHiringInvoice ON TRN_no =INV_TRN_NO where TRN_BATCHSTR='" & p_Modifyid & "' and TPT_Bsu_id='" & Session("sBsuid") & "' group by TRN_BATCHSTR,TRN_BATCHDATE,TRN_JobnumberStr,ACT_NAME")
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Property TPTInvoiceDetails() As DataTable
        Get
            Return ViewState("InvoiceDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("InvoiceDetails") = value
        End Set
    End Property

    Private Function writeWorkFlowTPTH(ByVal trnno As String, ByVal strAction As String, ByVal strDetails As String, ByVal strStatus As String) As Integer
        Dim iParms(6) As SqlClient.SqlParameter
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TRN_NO", trnno, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveWorkFlowTPTH", iParms)
        If RetVal = "-1" Then
            lblError.Text = "Unexpected Error !!!"
            stTrans.Rollback()
            Exit Function
        Else
            stTrans.Commit()
        End If
    End Function


    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTInvoiceDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTInvoiceDetails = mtable
        fillGrdView.DataSource = TPTInvoiceDetails
        fillGrdView.DataBind()
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
        txtSupplier.Text = ""
        txtBatchNo.Text = ""
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property QUDFooter() As DataTable
        Get
            Return ViewState("QUDFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("QUDFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property
    Private Sub showNoRecordsFound()
        If Not QUDFooter Is Nothing AndAlso QUDFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoiceDetails.Columns.Count - 2
            grdInvoiceDetails.Rows(0).Cells.Clear()
            grdInvoiceDetails.Rows(0).Cells.Add(New TableCell())
            grdInvoiceDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoiceDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdInvoiceDetails.PageIndexChanging
        grdInvoiceDetails.PageIndex = e.NewPageIndex
        grdInvoiceDetails.DataSource = tptinvoicedetails
        grdInvoiceDetails.DataBind()
    End Sub
    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoiceDetails.RowCancelingEdit
        grdInvoiceDetails.ShowFooter = True
        grdInvoiceDetails.EditIndex = -1
        grdInvoiceDetails.DataSource = QUDFooter
        grdInvoiceDetails.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCMID As New Label
        Dim TrnNo As Integer
        lblCMID = TryCast(sender.FindControl("lblJobNo"), Label)
        TrnNo = Mainclass.getDataValue("select trn_no from tpthiring where TRN_JobnumberStr='" & lblCMID.Text & "'", "OASIS_TRANSPORTConnectionString")
        Response.Redirect("../ExtraHiring/tptHiringBilling.aspx?MainMnu_code=" & Encr_decrData.Encrypt("T200140") & "&datamode=" & Encr_decrData.Encrypt("view") & "&viewid=" & Encr_decrData.Encrypt(TrnNo))
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblJobNo As New Label
        Dim lblBatchNo As New Label
        Dim DtD As New DataTable
        lblJobNo = TryCast(sender.FindControl("lblJobNo"), Label)
        lblBatchNo = TryCast(sender.FindControl("lblBatchNo"), Label)

        If lblStatus.Text <> "Not Posted" Then
            MsgBox("This Batch is posted You can not Delete....!", MsgBoxStyle.Information)
            Exit Sub

        Else
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TRN_BATCH=NULL,TRN_BATCHDATE=NULL,TRN_BATCHSTR=NULL,TRN_STATUS='S'  where TRN_JobnumberStr='" & lblJobNo.Text & "' and tpt_bsu_id='" & Session("sBsuid") & "'")
            writeWorkFlowTPTH(0, "Modified", "Job Orders Converted from  Invoice POsring to Execution before posting to Finance :" & lblBatchNo.Text, "D")
            showNoRecordsFound()
            fillGridView(grdInvoiceDetails, "select TRN_BATCHSTR BatchNo,TRN_BATCHDATE BatchDate,TRN_JobnumberStr JobNo,ACT_NAME Client,isnull(SUM(INV_Amt),0) Amount from TPTHiring LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id LEFT OUTER JOIN TPTHiringInvoice ON TRN_no =INV_TRN_NO where trn_batchstr ='" & lblBatchNo.Text & "' and tpt_bsu_id='" & Session("sBsuid") & "' group by TRN_BATCHSTR,TRN_BATCHDATE,TRN_JobnumberStr,ACT_NAME")
            DtD = MainObj.getRecords("select max(a.TRN_BATCHSTR) BatchNo,max(a.TRN_BATCHDATE) BatchDate,max(B.ACT_NAME) Client,max(B.ACT_ID) ClientCode,max(ISNULL(a.TRN_STATUS,'')) STATUS,max(a.TRN_BATCH) as TRNNO,max(isnull(isnull(C.TPT_INV_ADDRESS,a.TPT_ADDRESS),'')) Address,max(isnull(TPT_JHD_DOCNO,'')) TPT_JHD_DOCNO,max(isnull(DESCR,'---SELECT ONE---')) DAX_DESCR,max(isnull(TPT_PRGM_DIM_CODE,'0')) DAX_ID ,sum(inv_amt) amount,max(isnull(tpt_TAX_AMOUNT,0)) TAX_AMOUNT,max(isnull(TPT_TAX_CODE,'NA')) TAX_CODE,max(ACT_EMR_CODE)ACT_EMR_CODE,max(isnull(tpt_EMR_CODE,'NA'))TPT_EMR_CODE,max(isnull(LOC_DESCRIPTION,'NA'))LOC_DESCRIPTION,max(isnull(LOC_TAX_ID,'NA')) LOC_ID from TPTHiring a LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M B on ACT_ID=TPT_Client_id LEFT OUTER JOIN TPT_INVOICEADDESS C on TPT_INV_ACT_ID=TPT_Client_id left outer join dbo.VV_DAX_CODES on ID=TPT_PRGM_DIM_CODE LEFT OUTER JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID left outer join tpthiringinvoice on TRN_no =INV_TRN_NO WHERE A.TRN_BATCHSTR='" & lblBatchNo.Text & "'and A.TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            If DtD.Rows.Count > 0 Then
                txtTotal.Text = DtD.Rows(0)("Amount")
                txtVatAmount.Text = DtD.Rows(0)("TAX_Amount")
                txtNetAmount.Text = DtD.Rows(0)("Amount") + DtD.Rows(0)("TAX_Amount")
                h_taxCode.Value = DtD.Rows(0)("TAX_CODE")
                h_location.Value = DtD.Rows(0)("LOC_ID")
            End If
            If grdInvoiceDetails.Rows.Count - 1 = 0 Then
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub
    Protected Sub grdInvoiceDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoiceDetails.RowDataBound
        If lblStatus.Text <> "Not Posted" Then
            For Each gvr As GridViewRow In grdInvoiceDetails.Rows
                gvr.Cells(5).Enabled = False
            Next
        End If
    End Sub
    Protected Sub grdInvoiceDetails_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles grdInvoiceDetails.RowDeleted
    End Sub
    Protected Sub grdQUD_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdInvoiceDetails.RowDeleting
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        PostDeleteJob(txtBatchNo.Text, "D")
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        If RadDAXCodes.SelectedValue = "0" Then
            lblError.Text = "Please Select DAX Posting Code....!"
            Exit Sub
        Else
            UpdateAddress()
            PostDeleteJob(txtBatchNo.Text, "P")
            If Posted = True Then
                lblError.Text = "Invoice Posted Sucessfully...!"
            Else
                lblError.Text = "There is some error with this invoice!"
            End If


            Exit Sub
            'Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Private Sub PostDeleteJob(ByVal trnno As String, ByVal action As String)
        Dim pParms(10) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@TRN_no", trnno, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@action", action, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@sBSUID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@PostingDate", txtBatchDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@YEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@DAX_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@TPT_TAX_AMOUNT", txtVatAmount.Text, SqlDbType.Decimal)
        pParms(8) = Mainclass.CreateSqlParameter("@TPT_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Decimal)
        pParms(9) = Mainclass.CreateSqlParameter("@TPT_TAX_CODE", h_taxCode.Value, SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@TPT_EMR_CODE", h_location.Value, SqlDbType.VarChar)

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostJob", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Posted = False
                Exit Sub
            Else
                stTrans.Commit()
                Posted = True
                If action = "P" Then
                    writeWorkFlowTPTH(0, "Modified", "Job Order Posted to Finance :" & trnno, "P")
                Else
                    writeWorkFlowTPTH(0, "Modified", "Job Order Converted to Execution before posting to Finance :" & trnno, "S")
                End If
                setModifyvalues(txtBatchNo.Text)
                'Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            UpdateAddress()
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As String = CType(h_EntryId.Value, String)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTInvoice"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BatchNo", LTrim(RTrim(txtBatchNo.Text)), SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@sBSUID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            Dim InvNo As String = ""
            Dim invDate As Date
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "INVOICE"

            'InvNo = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHSTR FROM TPTHiring WHERE TRN_BATCH=" & TRN_NO, "OASIS_TRANSPORTConnectionString")
            'invDate = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHDATE FROM TPTHiring WHERE TRN_BATCH=" & TRN_NO, "OASIS_TRANSPORTConnectionString")
            'params("LPO") = Mainclass.getDataValue("SELECT  top 1 LPO_no FROM TPTHiring WHERE TRN_BATCH=" & TRN_NO, "OASIS_TRANSPORTConnectionString")
            'InvNo = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHSTR FROM TPTHiring WHERE TRN_BATCH=" & TRN_NO & " and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            'invDate = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHDATE FROM TPTHiring WHERE TRN_BATCH=" & TRN_NO & " and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            'params("LPO") = Mainclass.getDataValue("SELECT  top 1 LPO_no FROM TPTHiring WHERE TRN_BATCH=" & TRN_NO & " and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            invDate = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHDATE FROM TPTHiring WHERE TRN_BATCHSTR='" & LTrim(RTrim(txtBatchNo.Text)) & "' and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            params("LPO") = Mainclass.getDataValue("SELECT  top 1 LPO_no FROM TPTHiring WHERE TRN_BATCHSTR='" & LTrim(RTrim(txtBatchNo.Text)) & "'  and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            params("InvNo") = LTrim(RTrim(txtBatchNo.Text))
            params("InvDate") = invDate
            params("@BatchNo") = TRN_NO

            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            If Session("sBsuid") = "900501" Then
                'If Format(CDate(invDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
                If CDate(invDate) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTInvoice.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoice.rpt"
                End If

            Else
                'If Format(CDate(invDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
                If CDate(invDate) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTInvoiceBBT.rpt"
                Else
                    'repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoiceBBT.rpt"
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTInvoice.rpt"
                End If

            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub UpdateAddress()
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TPT_ADDRESS='" & txtAddress.Text & "' where TRN_BATCHSTR='" & txtBatchNo.Text & "'")
    End Sub
    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmail.Click
        'Try
        '    Dim RetMessage As String
        '    Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
        '    Dim hf_FCLID As HiddenField = sender.Parent.parent.findcontrol("hf_FCLID")
        '    RetMessage = DownloadInvoiceReceipt.DownloadOrEMailReceipt("EMAIL", CType(h_EntryId.Value, String), Session("sBsuId"), True, "HIRING")
        '    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & lblReceipt.Text & "','" & hf_FCLID.Value & "','" & RetMessage & "' ")
        '    lblError.Text = RetMessage
        'Catch ex As Exception
        '    lblError.Text = ex.Message
        'End Try
        Dim bEmailSuccess As Boolean
        SendEmail(CType(h_EntryId.Value, String), bEmailSuccess)
        If bEmailSuccess = True Then
            lblError.Text = "Email send  Sucessfully...!"
        Else
            lblError.Text = "Error while sending email.."
        End If
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""
        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("userName", "SYSTEM")
        param.Add("reportCaption", "INVOICE")
        Dim InvNo = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHSTR FROM TPTHiring WHERE TRN_BATCH=" & RecNo & " and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")

        'Dim invDate = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHDATE FROM TPTHiring WHERE TRN_BATCH=" & RecNo & " and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
        'param("LPO") = Mainclass.getDataValue("SELECT  top 1 LPO_no FROM TPTHiring WHERE TRN_BATCHSTR='" & LTrim(RTrim(txtBatchNo.Text)) & "' and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
        Dim invDate = Mainclass.getDataValue("SELECT DISTINCT TRN_BATCHDATE FROM TPTHiring WHERE TRN_BATCHSTR='" & LTrim(RTrim(txtBatchNo.Text)) & "' and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
        param("LPO") = Mainclass.getDataValue("SELECT  top 1 LPO_no FROM TPTHiring WHERE TRN_BATCHSTR='" & LTrim(RTrim(txtBatchNo.Text)) & "'  and TPT_Bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")

        param("InvNo") = LTrim(RTrim(txtBatchNo.Text))
        param("InvDate") = invDate
        param("@BatchNo") = LTrim(RTrim(txtBatchNo.Text))
        param("@sBSUID") = HttpContext.Current.Session("sBsuid")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        If HttpContext.Current.Session("sBsuid") = "900501" Then
            'If Format(CDate(invDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(invDate) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceEmail.rpt")
            Else
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceEmailTAX.rpt")
            End If

        Else
            'If Format(CDate(invDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(invDate) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceEmailBBT.rpt")
            Else
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTInvoiceEmailBBTTAX.rpt")
            End If

        End If
        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = LTrim(RTrim(txtBatchNo.Text))
        rptDownload.InvType = "JOB"
        rptDownload.FCO_ID = 0
        'UtilityObj.Errorlog("Calling Loadreports", "BBT_TEST")
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & RecNo & "','" & FCL_ID & "','" & SendEmail & "' ")
        End If
    End Function
    'Protected Sub radVat_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVat.SelectedIndexChanged
    'Dim dtVATPerc As New DataTable
    'dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVat.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")
    'If dtVATPerc.Rows.Count > 0 Then
    '    h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
    '    txtVatAmount.Text = ((Val(txtTotal.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
    '    txtNetAmount.Text = Convert.ToDouble(Val(txtTotal.Text) + Val(txtVatAmount.Text)).ToString("####0.000")
    'Else
    '    radVat.SelectedValue = 0
    '    h_VATPerc.Value = radVat.SelectedValue
    '    txtVatAmount.Text = ((Val(txtTotal.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
    '    txtNetAmount.Text = Convert.ToDouble(txtVatAmount.Text + txtVatAmount.Text).ToString("####0.000")
    'End If
    'End Sub
End Class

