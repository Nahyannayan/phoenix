<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptLeasingInvoice.aspx.vb" Inherits="Transport_ExtraHiring_tptLeasingInvoice" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    
    <style type="text/css">
        .autocomplete_highlightedListItem_S1 {
            /*font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;*/
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }

        .style1 {
            width: 100%;
        }

         /*.RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }*/
         .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function parseTime(s) {
            var c = s.split(':');
            return parseInt(c[0]) * 60 + parseInt(c[1]);
        }




        function ChangeAllCheckBoxStates(checkState) {

            var chk_state = document.getElementById("chkAL").checked;

            if (chk_state) {

                document.getElementById("<%=txtSelectedAmt.ClientID %>").value = document.getElementById("<%=hdngross.ClientID %>").value;
            }
            else {
                document.getElementById("<%=txtSelectedAmt.ClientID %>").value = '0.00';

            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }


        function getVehicleNo() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;


            var RB1 = document.getElementById("<%=rblData.ClientID%>");
            var radio = RB1.getElementsByTagName("input");
            var label = RB1.getElementsByTagName("label");
            for (var x = 0; x < radio.length; x++) {
                if (radio[x].checked) {
                    var jobTypeSelected = label[x].innerHTML;
                }
            }
            if (jobTypeSelected == 'School')
                pMode = "TPTVEHICLE";
            else
                pMode = "TPTVEHICLENEW";

            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtRegno.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = NameandCode[0];--%>
        }

        //}
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtRegno.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtRegno.ClientID%>', 'TextChanged');
            }
        }

        function calc(Type) {
            if (Type == "T") {
                document.getElementById("<%=txtAmount.ClientID %>").value = eval(document.getElementById("<%=txtqty.ClientID %>").value) * eval(document.getElementById("<%=txtRate.ClientID %>").value);
                document.getElementById("<%=txtTotAmt.ClientID %>").value = ((eval(document.getElementById("<%=txtAmount.ClientID %>").value) + eval(document.getElementById("<%=txtSalik.ClientID %>").value) + eval(document.getElementById("<%=txtFuel.ClientID %>").value) + eval(document.getElementById("<%=txtAddKMAmt.ClientID %>").value)) - ((eval(document.getElementById("<%=txtDiscount.ClientID %>").value) + (eval(document.getElementById("<%=txtAdvance.ClientID %>").value)))));

                var VATAmt = (((eval(document.getElementById("<%=txtTotAmt.ClientID %>").value) + eval(document.getElementById("<%=txtAdvance.ClientID %>").value) - eval(document.getElementById("<%=txtSalik.ClientID %>").value)) * (document.getElementById("<%=h_VATPerc.ClientID %>").value))) / 100;
                document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
                document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txtTotAmt.ClientID %>").value)).toFixed(2);

            }

        }


        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Leasing Invoice
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr id="ErrId" visible="false" >
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">
                            <table align="center" width="100%">
                                <%-- <tr class="subheader_img">
                                    <td align="left" colspan="25" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Leasing Invoice</span></font>
                                    </td>
                                </tr>--%>
                                <tr>
                                  <td>
                                      <table width="100%">
                                          <tr>
                                              <td align="left" width="3%"><span class="field-label">Date</span> </td>
                                              <td width="5%">
                                                  <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                  <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                      TargetControlID="txtDate" PopupButtonID="lnkDate">
                                                  </ajaxToolkit:CalendarExtender>
                                              </td>
                                              <td width="20%">
                                                  <asp:Button ID="btnFind" runat="server" CssClass="button m-0" Text="Load.." />
                                              </td>
                                              <td ></td>
                                          </tr>
                                      </table>
                                  </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" Class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th>Customer
                                                </th>
                                                <th>Lease Type
                                                </th>
                                                <th>DAX Code
                                                </th>
                                                <th>Reg.No
                                                </th>
                                                <th>Vehicle Type
                                                </th>
                                                <th>No of Buses
                                                </th>
                                                <th>Rate
                                                </th>
                                                <th>Amount
                                                </th>
                                                <th>Fuel
                                                </th>
                                                <th>Salik
                                                </th>
                                                <th>Add K.M Amt
                                                </th>
                                                <th>Discount 
                                                </th>
                                                <th>Advance
                                                </th>
                                                <th>Total Amount
                                                </th>
                                                <th>VAT Code
                                                </th>
                                                <th>Emirate
                                                </th>
                                                <th>VAT Amount
                                                </th>
                                                <th>Net Amount
                                                </th>
                                                <th>Remarks
                                                </th>
                                                <th>Notes
                                                </th>
                                                <th>LPO No
                                                </th>
                                                <th>Contract Strat Date
                                                </th>
                                                <th>Contract End Date
                                                </th>
                                                <th>Start Point
                                                </th>
                                                <th>Route
                                                </th>
                                                <th>Route Id
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtTSRSNo" runat="server" Width="100px" Visible="false"></asp:TextBox>
                                                    <telerik:RadComboBox ID="RadCustomer" Width="200" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rblData" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                        <asp:ListItem Selected="True" Value="0">NES</asp:ListItem>
                                                        <asp:ListItem Value="1">School</asp:ListItem>
                                                    </asp:RadioButtonList>

                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadDAXCodes" Width="225" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                   <table width="100%">
                                                       <tr style="background-color: transparent !important;">
                                                           <td style="border: 0px !important;">
                                                                <asp:TextBox ID="txtRegno" runat="server" Width="100px" AutoPostBack="true" Enabled="false" align="left"></asp:TextBox>
                                                  
                                                           </td>
                                                           <td style="border: 0px !important;">
                                                                 <asp:ImageButton ID="imgVehicle" runat="server" align="left" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getVehicleNo();return false;" />
                                                           </td>
                                                       </tr>
                                                   </table>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVehicleType" runat="server" TabIndex="1" Width="150px"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtqty" style="text-align:right" runat="server" Width="100px" TabIndex="2" align="left"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtRate" style="text-align:right" runat="server" Width="100px" TabIndex="3"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtAmount" style="text-align:right" Width="100px" runat="server" Enabled="false"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtFuel" style="text-align:right" Width="100px" runat="server" TabIndex="4"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtSalik"  style="text-align:right" Width="100px" runat="server" TabIndex="5"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtAddKMAmt"  style="text-align:right" Width="100px" runat="server" TabIndex="6"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtDiscount" style="text-align:right" Width="100px" runat="server" TabIndex="7"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtAdvance" style="text-align:right" Width="100px" runat="server" TabIndex="8"></asp:TextBox>
                                                </td>


                                                <td>
                                                    <asp:TextBox ID="txtTotAmt" style="text-align:right" Width="100px" runat="server" Enabled="false"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <telerik:RadComboBox ID="radVAT" Width="275" runat="server" TabIndex="9" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadEmirate" Width="225" runat="server" TabIndex="10" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVATAmount"  style="text-align:right" Width="100px" Enabled="false" runat="server" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNetAmount" style="text-align:right" Width="100px" Enabled="false" runat="server" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRemarks" runat="server" Width="200px" TabIndex="11"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtNotes" runat="server" Width="200px" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLPO" runat="server" Width="100px" TabIndex="13"></asp:TextBox>
                                                </td>


                                                <td>
                                                    <table width="100%">
                                                        <tr style="background-color: transparent !important;">
                                                            <td style="border: 0px !important;">
                                                                <asp:TextBox ID="txtContractSdate" runat="server" Width="150px" Enabled="false" TabIndex="14"></asp:TextBox>
                                                            </td>
                                                            <td style="border: 0px !important;">
                                                                <asp:ImageButton ID="imgSdate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server"
                                                                    TargetControlID="txtContractSdate" PopupButtonID="imgSdate">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                   
                                                  
                                                </td>
                                                <td>
                                                    <table width="100%">
                                                        <tr style="background-color: transparent !important;">
                                                            <td style="border: 0px !important;">
                                                                <asp:TextBox ID="txtContractEdate" runat="server" Width="150px" Enabled="false" TabIndex="15"></asp:TextBox></td>
                                                            <td style="border: 0px !important;">
                                                                <asp:ImageButton ID="imgEdate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" Format="dd/MMM/yyyy" runat="server"
                                                                    TargetControlID="txtContractEdate" PopupButtonID="imgEdate">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtStartPoint" runat="server" Enabled="true" TabIndex="16" Width="200px"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtRoute" runat="server" Enabled="true" TabIndex="17" Width="200px"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtRouteId" runat="server" Enabled="true" TabIndex="18" Width="200px"></asp:TextBox>
                                                </td>

                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                       <td colspan="3">
                           <table width="100%">
                               <tr>
                                   <td valign="bottom" width="20%" align="left">

                                       <asp:Button ID="btnCopy1" runat="server" CssClass="button m-0" Text="Copy" />
                                        <asp:Button ID="btnPost1" runat="server" CssClass="button m-0" Text="Post" />
                                        <asp:HiddenField ID="hdngross" runat="server" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button m-0" Text="Save" ValidationGroup="groupM1" />
                                       <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button m-0"
                                           Text="Cancel" UseSubmitBehavior="False" />
                                       <asp:HyperLink ID="btnExport" runat="server" Visible="true">Export to Excel</asp:HyperLink>
                                   </td>
                                   <td width="40%"></td>                                   
                                   <td width="40%"></td>
                               </tr>
                           </table>
                       </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <table width="100%">
                                <tr>
                                    <td width="4%">
                                        <asp:Label ID="Postingdate" Text="Posting Date" CssClass="field-label" runat="server"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:TextBox ID="txtPostingDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgPosting" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtPostingDate" PopupButtonID="ImgPosting">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="4%"> 
                                         <asp:Label ID="lblSelAmount" Text="Selected Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:TextBox ID="txtSelectedAmt" style="text-align:right" runat="server"></asp:TextBox>
                                    </td>

                                    <td width="4%">
                                        <asp:Label ID="lblVAT" Text="VAT Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:TextBox ID="txtSelVAT" style="text-align:right" runat="server"></asp:TextBox>
                                    </td>

                                    <td width="4%">
                                        <asp:Label ID="lblNet" Text="Net Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="3%">
                                        <asp:TextBox ID="txtSelNet" style="text-align:right" runat="server"></asp:TextBox>
                                    </td> 
                                    <td width="70%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="8%">

                                        <asp:RadioButtonList ID="rblSelection" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="0"> <span class="field-label"> Not Posted </span></asp:ListItem>
                                            <asp:ListItem Value="1"><span class="field-label">Posted </span></asp:ListItem>
                                            <asp:ListItem Value="2"><span class="field-label">All </span></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td width="8%">
                                        <telerik:RadComboBox ID="RadFilterBSU" Width="300" AllowCustomText="true" Filter="Contains" runat="server" Visible="true" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td width="84%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                  
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row mb-0"
                                Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="TLI_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <%--<input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All"   />                                --%>
                                            <asp:CheckBox ID="chkAL" AutoPostBack="true" runat="server" OnCheckedChanged="chkAL_CheckedChanged"></asp:CheckBox>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("TLI_ID") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TLI_TRDATE" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle Width="95px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Customer" HeaderText="Customer" />
                                    <asp:BoundField DataField="LeaseType" HeaderText="Lease Type" />
                                    <asp:BoundField DataField="DAX_DESCR" HeaderText="DAX Code" />
                                    <asp:BoundField DataField="REGNO" HeaderText="Reg.No" />
                                    <asp:BoundField DataField="TLI_CUS_LPONO" HeaderText="LPO No." />
                                    <asp:BoundField DataField="TLI_VEHTYPE" HeaderText="Vehicle Type" />
                                    <asp:BoundField DataField="TLI_QTY" HeaderText="No of Buses" />
                                    <asp:BoundField DataField="TLI_RATE" HeaderText="Rate" />
                                    <asp:BoundField DataField="TLI_AMOUNT" HeaderText="Amount" />
                                    <asp:BoundField DataField="TLI_FUEL" HeaderText="Fuel" />
                                    <asp:BoundField DataField="TLI_SALIK" HeaderText="Salik" />
                                    <asp:BoundField DataField="TLI_ADDKMAMT" HeaderText="Add. KM Amount" />
                                    <asp:BoundField DataField="TLI_DISCOUNT" HeaderText="Discount" />
                                    <asp:BoundField DataField="TLI_ADVANCE" HeaderText="Advance" />

                                    <asp:TemplateField HeaderText="Total  Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Eval("TLI_TOTAL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="TLI_TAX_CODE" HeaderText="VAT Code" />
                                    <asp:BoundField DataField="EMIRATE" HeaderText="Emirate" />
                                    <%--<asp:BoundField DataField="TLI_TAX_AMOUNT" HeaderText="VAT Amount" />--%>
                                    <%--<asp:BoundField DataField="TLI_TAX_NET_AMOUNT" HeaderText="Net Amount" />--%>

                                    <asp:TemplateField HeaderText="VAT Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVAT" runat="server" Text='<%# Eval("TLI_TAX_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetAmt" runat="server" Text='<%# Eval("TLI_TAX_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="TLI_REMARKS" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TLI_NOTES" HeaderText="Notes" />
                                    <asp:BoundField DataField="TLI_CONTRACT_SDATE" HeaderText="Contract Start Date" />
                                    <asp:BoundField DataField="TLI_CONTRACT_EDATE" HeaderText="Contract End Date" />
                                    <asp:BoundField DataField="TLI_START" HeaderText="Starting Point" />
                                    <asp:BoundField DataField="TLI_ROUTE" HeaderText="Route" />
                                    <asp:BoundField DataField="TLI_ROUTE_ID" HeaderText="Route ID" />
                                    <asp:BoundField DataField="TLI_INVNO" HeaderText="Invoice" />
                                    <asp:BoundField DataField="TLI_USER" HeaderText="Loged in User Name" />
                                    <asp:BoundField DataField="TLI_JHD_DOCNO" HeaderText="IJV NO" />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTHCID" runat="server" Text='<%# Eval("TLI_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" OnClick="lnkPrint_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTLI_invNo" runat="server" Text='<%# Eval("TLI_INVNO") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPOstingDate" runat="server" Text='<%# Eval("TLI_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmail" runat="server" Text="Email" OnClick="lnkEmail_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblLI_invNo" runat="server" Text='<%# Eval("TLI_INVNO") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPDate" runat="server" Text='<%# Eval("TLI_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr>
                        <td valign="bottom" colspan="3">
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_mimimum" runat="server" />
                            <asp:HiddenField ID="h_TEI_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_VehicleType" runat="server" />
                            <asp:HiddenField ID="hTPT_Veh_ID" runat="server" />
                            <asp:HiddenField ID="h_VATPerc" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="20%" align="left">
                                        <asp:Button ID="btnCopy" runat="server" CssClass="button m-0" Text="Copy" />
                                        <asp:Button ID="btnPost" runat="server" CssClass="button m-0" Text="Post" />
                                    </td>
                                    <td width="40%"></td>
                                    
                                    <td width="40%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
