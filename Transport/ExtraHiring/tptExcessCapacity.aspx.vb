﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime
Partial Class Transport_ExtraHiring_tptExcessCapacity
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim dtVATUserAccess As New DataTable
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200230") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtUsers.Attributes.Add("onBlur", " return calc('A');")
                txtChargingRate.Attributes.Add("onBlur", " return calc1('T');")
                clearScreen()
                sqlStr = "SELECT TEC_ID, replace(convert(varchar(30), TEC_TRDATE, 106),' ','/') TEC_TRDATE, TEC_BSU_ID, TEC_TLC_ID, TEC_EMP_ID, TEC_VEH_ID, TEC_ROUTE, TEC_CAPACITY80, TEC_NOOFUSERS, TEC_VACANTSEATS,  "
                sqlStr &= " TEC_RATE, TEC_AMOUNT, TEC_REMARKS, TEC_INVNO, TEC_FYEAR, TEC_USER, TEC_DELETED, TEC_DATE, TEC_POSTED, TEC_JHD_DOCNO, TEC_POSTING_DATE,tlc_descr customer,VEH_REGNO,LEFT(VEH_CAPACITY,2)VEH_CAPACITY,VEH_NAME,DRIVER,DRIVER_EMPID "
                sqlStr &= ",isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(TEC_PRGM_DIM_CODE,'0') DAX_ID,isnull(TEC_TAX_AMOUNT,0)TEC_TAX_AMOUNT,isnull(TEC_TAX_NET_AMOUNT,0)TEC_TAX_NET_AMOUNT,isnull(TEC_TAX_CODE,'NA')TEC_TAX_CODE "
                sqlStr &= ",isnull(TEC_EMR_CODE,'NA')TEC_EMR_CODE,EMR_DESCR EMIRATE"
                sqlStr &= " FROM TPTEXCESSCAPACITYCHARGES  inner join TPTLEASECUSTOMER  on TLC_ID=TEC_TLC_ID "
                sqlStr &= " left outer join dbo.VV_DAX_CODES on ID=TEC_PRGM_DIM_CODE "
                sqlStr &= " INNER JOIN VW_DRIVER E ON E.DRIVER_EMPID=TEC_EMP_ID  "
                sqlStr &= " INNER JOIN TPTVEHICLE_M on veh_id=tec_veh_id "
                sqlStr &= " left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE= isnull(TEC_EMR_CODE,'NA') "
                BindDriver()
                BindBsu()
                BindDAXCodes()
                RadFilterBSU.SelectedValue = 0
                refreshGrid()
                dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")
                If dtVATUserAccess.Rows.Count > 0 Then
                    If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                        radVAT.Enabled = True
                        RadEmirate.Enabled = True

                    Else
                        radVAT.Enabled = False
                        RadEmirate.Enabled = False
                    End If
                Else
                    radVAT.Enabled = False
                    RadEmirate.Enabled = False
                End If
                CalculateVAT(RadCustomer.SelectedValue)
                SelectEmirate(RadCustomer.SelectedValue)
                If Request.QueryString("VIEWID") <> "" Then
                    h_TEC_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_TEC_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim strsql3 As String = ""
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            strsql3 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

            Dim BSUParms(2) As SqlParameter

            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

            radVAT.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            radVAT.DataTextField = "DESCR"
            radVAT.DataValueField = "ID"
            radVAT.DataBind()


            RadEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql3)
            RadEmirate.DataTextField = "DESCR"
            RadEmirate.DataValueField = "ID"
            RadEmirate.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0

        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblVAtAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAfterVAT"), Label)


            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                VAT += Val(lblVAT.Text)
                Net += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVat.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Protected Sub Check_All(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)

            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblVAtAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAfterVAT"), Label)

            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                VAT += Val(lblVAT.Text)
                Net += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVat.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub
    Private Sub refreshGrid()
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")


        If rblSelection.SelectedValue = 0 Then
            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TEC_TRDATE)= MONTH('" & txtDate.Text & "') AND TEC_DELETED=0 and TEC_BSU_ID='" & Session("sBsuid") & "' and TEC_FYEAR='" & Session("F_YEAR") & "'  and isnull(TEC_POSTED,0)=0 "
            Else
                sqlWhere = " where  MONTH(TEC_TRDATE)= MONTH('" & txtDate.Text & "') AND TEC_DELETED=0 and TEC_BSU_ID='" & Session("sBsuid") & "' and TEC_FYEAR='" & Session("F_YEAR") & "' and TEC_TLC_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(TEC_POSTED,0)=0 "
            End If


        ElseIf rblSelection.SelectedValue = 1 Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TEC_TRDATE)= MONTH('" & txtDate.Text & "') AND TEC_DELETED=0 and TEC_BSU_ID='" & Session("sBsuid") & "' and TEC_FYEAR='" & Session("F_YEAR") & "' and isnull(TEC_POSTED,0)=1 "
            Else
                sqlWhere = " where  MONTH(TEC_TRDATE)= MONTH('" & txtDate.Text & "') AND TEC_DELETED=0 and TEC_BSU_ID='" & Session("sBsuid") & "' and TEC_FYEAR='" & Session("F_YEAR") & "' and TEC_TLC_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(TEC_POSTED,0)=1 "
            End If

        ElseIf rblSelection.SelectedValue = 2 Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TEC_TRDATE)= MONTH('" & txtDate.Text & "') AND TEC_DELETED=0 and TEC_BSU_ID='" & Session("sBsuid") & "' and TEC_FYEAR='" & Session("F_YEAR") & "'"
            Else
                sqlWhere = " where  MONTH(TEC_TRDATE)= MONTH('" & txtDate.Text & "') AND TEC_DELETED=0 and TEC_BSU_ID='" & Session("sBsuid") & "' and TEC_FYEAR='" & Session("F_YEAR") & "' and TEC_TLC_ID ='" & RadFilterBSU.SelectedValue & "' "
            End If

        End If




        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTEXCESSCAPACITYCHARGES = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
            Session("myData") = TPTEXCESSCAPACITYCHARGES
            SetExportFileLink()
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub clearScreen()
        h_TEC_ID.Value = "0"
        txtDriver.Text = ""
        txtDriverName.Text = ""
        txtRegno.Text = ""
        TxtRemarks.Text = ""
        txtAmount.Text = "0"
        txtAmount.Text = "0"
        txtRoute.Text = ""
        txtUsers.Text = "0"
        txtVehCapacity.Text = "0"
        txtVehCapacity80.Text = "0"
        txtVehType.Text = ""
        txtVacantSeats.Text = "0"
        txtChargingRate.Text = "0"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"


        If PurposeExists = 0 Then lblError.Text &= "Purpose is "

        If RadDAXCodes.SelectedValue = "0" Then lblError.Text &= "Please Select DAX Posting Code, "

        If RadEmirate.SelectedValue = "NA" Then lblError.Text &= "Please select correct Emirate Code, "

        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            'lblError.Visible = True
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Try
            Dim pParms(20) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TEC_ID", h_TEC_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TEC_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@TEC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@TEC_TLC_ID", RadCustomer.SelectedValue, SqlDbType.Int)
            pParms(5) = Mainclass.CreateSqlParameter("@TEC_EMP_ID", txtDriver.Text, SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@TEC_VEH_ID", hTPT_Veh_ID.Value, SqlDbType.Int)
            pParms(7) = Mainclass.CreateSqlParameter("@TEC_ROUTE", txtRoute.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@TEC_CAPACITY80", txtVehCapacity80.Text, SqlDbType.Int)
            pParms(9) = Mainclass.CreateSqlParameter("@TEC_NOOFUSERS", txtUsers.Text, SqlDbType.Int)
            pParms(10) = Mainclass.CreateSqlParameter("@TEC_VACANTSEATS", txtVacantSeats.Text, SqlDbType.Int)
            pParms(11) = Mainclass.CreateSqlParameter("@TEC_RATE", txtChargingRate.Text, SqlDbType.Float)
            pParms(12) = Mainclass.CreateSqlParameter("@TEC_AMOUNT", txtAmount.Text, SqlDbType.Float)
            pParms(13) = Mainclass.CreateSqlParameter("@TEC_REMARKS", TxtRemarks.Text, SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@TEC_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@TEC_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(16) = Mainclass.CreateSqlParameter("@TEC_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
            pParms(17) = Mainclass.CreateSqlParameter("@TEC_TAX_AMOUNT", txtVATAmount.Text, SqlDbType.Float)
            pParms(18) = Mainclass.CreateSqlParameter("@TEC_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
            pParms(19) = Mainclass.CreateSqlParameter("@TEC_TAX_CODE", radVAT.SelectedValue, SqlDbType.VarChar)
            pParms(20) = Mainclass.CreateSqlParameter("@TEC_EMR_CODE", RadEmirate.SelectedValue, SqlDbType.VarChar)


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTEXCESSCAPACITYCHARGES", pParms)
                If RetVal = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ID, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                        clearScreen()
                        refreshGrid()
                        'lblError.Text = "Data Saved Successfully !!!"
                        usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If
                    
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub showEdit(ByVal showId As Integer)
        Dim rdrTrnsprt As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & " where TEC_ID='" & showId & "'")
        If rdrTrnsprt.HasRows Then
            rdrTrnsprt.Read()
            h_TEC_ID.Value = rdrTrnsprt("TEC_ID")
            txtDate.Text = rdrTrnsprt("TEC_TRDATE")
            txtDriver.Text = rdrTrnsprt("TEC_EMP_ID")
            txtDriverName.Text = rdrTrnsprt("DRIVER")
            RadCustomer.SelectedItem.Text = rdrTrnsprt("CUSTOMER")
            RadCustomer.SelectedValue = rdrTrnsprt("TEC_TLC_ID")
            txtRegno.Text = rdrTrnsprt("VEH_REGNO")
            txtVehType.Text = rdrTrnsprt("VEH_NAME")
            txtVehCapacity.Text = rdrTrnsprt("VEH_CAPACITY")
            txtRoute.Text = rdrTrnsprt("TEC_ROUTE")
            txtVehCapacity80.Text = rdrTrnsprt("TEC_CAPACITY80")
            txtUsers.Text = rdrTrnsprt("TEC_NOOFUSERS")
            txtVacantSeats.Text = rdrTrnsprt("TEC_VACANTSEATS")
            txtChargingRate.Text = rdrTrnsprt("TEC_RATE")
            txtAmount.Text = rdrTrnsprt("TEC_AMOUNT")
            TxtRemarks.Text = rdrTrnsprt("TEC_REMARKS")
            hTPT_Veh_ID.Value = rdrTrnsprt("TEC_VEH_ID")
            RadDAXCodes.SelectedItem.Text = rdrTrnsprt("DAX_DESCR")
            RadDAXCodes.SelectedValue = rdrTrnsprt("DAX_ID")
            BindDAXCodes()
            txtVATAmount.Text = rdrTrnsprt("TEC_TAX_AMOUNT")
            txtNetAmount.Text = rdrTrnsprt("TEC_TAX_NET_AMOUNT")
            radVAT.SelectedValue = rdrTrnsprt("TEC_TAX_CODE")

            Dim lstDrp As New RadComboBoxItem
            lstDrp = RadEmirate.Items.FindItemByValue(rdrTrnsprt("TEC_EMR_CODE"))
            If Not lstDrp Is Nothing Then
                RadEmirate.SelectedValue = lstDrp.Value
            Else
                RadEmirate.SelectedValue = "NA"
            End If

            CalculateVAT(RadCustomer.SelectedValue)
            If RadEmirate.SelectedValue = "NA" Then
                SelectEmirate(RadCustomer.SelectedValue)
            End If


        End If
        rdrTrnsprt.Close()
        rdrTrnsprt = Nothing
    End Sub
    Protected Sub GridViewShowDetails_PageIndexChangingd(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub

    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(20).Text = "&nbsp;" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(24).Enabled = True
                gvr.Cells(25).Enabled = False
            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
                gvr.Cells(24).Enabled = False
                gvr.Cells(25).Enabled = True
            End If
        Next
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTECID"), Label)

        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTEXCESSCAPACITYCHARGES set TEC_DELETED=1,TEC_USER='" & Session("sUsr_name") & "',TEC_DATE=GETDATE() where TEC_ID=" & lblID.Text)
        'lblError.Text = "Record Deleted Successfully !!!"
        usrMessageBar.ShowNotification("Record Deleted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        refreshGrid()

    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTEC_INVNO"), Label)
        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTEXCESSCAPACITYCHARGES"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TEC_INVNO", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@TEC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params

            'If Format(CDate(lblPOstingDate.Text), "dd/MMM/yyyy") < "01/Jan/2018" Then

            If CDate(lblPOstingDate.Text) <= CDate("31/Dec/2017") Then
                repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTEXCESSCAPACITYCHARGES.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/Reports/rptTPTEXCESSCAPACITYCHARGES.rpt"
            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid()
    End Sub
    Private Sub BindDriver()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select '' EMPNO,'' EMP_NAME UNION ALL select DRIVER_EMPNO,DRIVER from [VW_DRIVER] A INNER JOIN OASIS..employee_m B ON B.EMP_ID=A.DRIVER_empid "
        sql_str &= " order by EMPNO "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            strsql = "select max(tlc_id) TLC_ID ,TLC_DESCR  from TPTLEASECUSTOMER where TLC_BSU_ID='" & Session("sBsuid") & "' group by tlc_descr union all select 0, '-----ALL-----' order by TLC_DESCR"
            RadCustomer.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadCustomer.DataTextField = "TLC_DESCR"
            RadCustomer.DataValueField = "TLC_ID"
            RadCustomer.DataBind()

            RadFilterBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadFilterBSU.DataTextField = "TLC_DESCR"
            RadFilterBSU.DataValueField = "TLC_ID"
            RadFilterBSU.DataBind()



        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub txtDriver_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDriver.TextChanged
        Dim DriverName As String
        DriverName = Mainclass.getDataValue("select Driver from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPID))='" & Trim(txtDriver.Text) & "'", "OASIS_TRANSPORTConnectionString")
        If DriverName <> "" Then
            txtDriverName.Text = DriverName
        Else
            'lblError.Text = "Invalid Driver No...!:"
            txtDriver.Text = ""
            usrMessageBar.ShowNotification("Invalid Driver No...!:", UserControls_usrMessageBar.WarningType.Danger)
            txtDriver.Focus()
            Exit Sub
        End If
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click, btnPost1.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox

        Dim PartyCode As String = ""
        Dim i As Integer = 0
        Dim mrow As GridViewRow
        Dim SelectedIDs As String = ""
        Dim NotSameBsu As Boolean = False
        lblError.Text = ""
        If txtPostingDate.Text.Trim = "" Then lblError.Text &= "Please select Posting Date,"

        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " "
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " ", UserControls_usrMessageBar.WarningType.Danger)
            lblError.Visible = True
            Return
        End If
        For Each mrow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(mrow.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(mrow.FindControl("lblTECID"), Label)

            If ChkBxItem.Checked = True Then

                If i = 0 Then
                    PartyCode = mrow.Cells(3).Text
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                Else
                    If PartyCode = mrow.Cells(3).Text Then
                        IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    Else
                        NotSameBsu = True
                    End If
                End If
                i = i + 1
            End If

        Next
        If NotSameBsu = False Then
            PostExcessCapacityChargeInvoice(IDs)
            'lblError.Text = "Data Posted Successfully !!!"
            usrMessageBar.ShowNotification("Data Posted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            refreshGrid()
        Else
            'lblError.Text = "Selected business units are  not same!!!"
            usrMessageBar.ShowNotification("Selected business units are  not same!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
    End Sub
    Private Sub PostExcessCapacityChargeInvoice(ByVal id As String)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(6) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", id, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@POSTINGDATE", txtPostingDate.Text, SqlDbType.DateTime)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostDriverCharges", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)

                stTrans.Rollback()
                Exit Sub
            Else
                stTrans.Commit()
                usrMessageBar.ShowNotification("Record Posted Sucessfully..!", UserControls_usrMessageBar.WarningType.Success)
            End If

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, id, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Success)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Property TPTEXCESSCAPACITYCHARGES() As DataTable
        Get
            Return ViewState("TPTEXCESSCAPACITYCHARGES")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTEXCESSCAPACITYCHARGES") = value
        End Set
    End Property
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click, btnCopy1.Click
        CopyDriverChargeDetails()
    End Sub
    Private Sub CopyDriverChargeDetails()

        Dim NewDate As Date
        Dim Newdate1 As Date

        NewDate = Format(CDate(txtDate.Text.ToString), "dd/MMM/yyyy")
        Newdate1 = NewDate.AddMonths(1)
        Newdate1 = New Date(NewDate.AddMonths(1).Year, NewDate.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1)
        Newdate1 = Format(CDate(Newdate1), "dd/MMM/yyyy")
        Dim CParms(4) As SqlClient.SqlParameter

        CParms(0) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
        CParms(0).Value = NewDate
        CParms(1) = New SqlClient.SqlParameter("@DATE1", SqlDbType.DateTime)
        CParms(1).Value = Newdate1

        CParms(2) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar, 10)
        CParms(2).Value = "EXCESS"
        CParms(3) = New SqlClient.SqlParameter("@BSU", SqlDbType.VarChar, 6)
        CParms(3).Value = Session("sBsuid")

        CParms(4) = New SqlClient.SqlParameter("@Errormsg", SqlDbType.VarChar, 1000)
        CParms(4).Direction = ParameterDirection.Output

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "CopyInvoiceDetails", CParms)
            Dim ErrorMSG As String = CParms(4).Value.ToString()


            If ErrorMSG = "" Then
                stTrans.Commit()
                usrMessageBar.ShowNotification("Copied Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                txtDate.Text = Newdate1.ToString("dd/MMM/yyyy")
                refreshGrid()

            Else
                usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
                refreshGrid()
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
            refreshGrid()

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub


    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Driver Charges")
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0

        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)

            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                Dim lblVAT As Label = TryCast(gvr.FindControl("lblVAtAmt"), Label)
                Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAfterVAT"), Label)

                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    Gross += Val(lblGross.Text)
                    VAT += Val(lblVAT.Text)
                    Net += Val(lblNet.Text)
                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
                VAT = 0
                Net = 0
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVat.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Protected Sub txtUsers_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUsers.TextChanged
        txtVacantSeats.Text = Val(txtVehCapacity80.Text) - Val(txtUsers.Text)
    End Sub
    Protected Sub txtVehCapacity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtVehCapacity.TextChanged
        txtVehCapacity80.Text = Math.Round((Val(txtVehCapacity.Text) * 80 / 100), 0)
    End Sub
    Private Sub CalculateVAT(ByVal Act_id As String)
        Dim dt As New DataTable
        dt = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','EX_CAP_CHG','" & txtDate.Text & "'," & txtAmount.Text & ",'" & Act_id & "')", "OASIS_TRANSPORTConnectionString")

        If dt.Rows.Count > 0 Then
            Dim lstDrp As New RadComboBoxItem
            lstDrp = radVAT.Items.FindItemByValue(dt.Rows(0)("tax_code"))
            h_VATPerc.Value = dt.Rows(0)("tax_perc_value")

            If Not lstDrp Is Nothing Then
                radVAT.SelectedValue = lstDrp.Value
            Else
                radVAT.SelectedValue = 0
            End If
            txtVATAmount.Text = ((Val(txtAmount.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = (Val(txtAmount.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub

    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged
        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVAT.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            txtVATAmount.Text = ((Val(txtAmount.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtAmount.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        Else
            radVAT.SelectedValue = 0
            h_VATPerc.Value = radVAT.SelectedValue
            txtVATAmount.Text = ((Val(txtAmount.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtAmount.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub RadCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadCustomer.SelectedIndexChanged
        CalculateVAT(RadCustomer.SelectedValue)
        SelectEmirate(RadCustomer.SelectedValue)
    End Sub
    Private Sub SelectEmirate(ByVal ACT_ID As String)
        Dim bsu_City As String = ""
        bsu_City = Mainclass.getDataValue("SELECT isnull(case when case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end end,'NA')   EMR_CODE   FROM OASISFIN..accounts_M left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE=ACT_EMR_CODE where act_id in(select distinct tlc_act_id from TPTLEASECUSTOMER where tlc_id='" & ACT_ID & "') ", "OASIS_TRANSPORTConnectionString")
        Dim lstDrp As New RadComboBoxItem
        lstDrp = RadEmirate.Items.FindItemByValue(bsu_City)

        If Not lstDrp Is Nothing Then
            RadEmirate.SelectedValue = lstDrp.Value
        Else
            RadEmirate.SelectedValue = "NA"
        End If
    End Sub

    Protected Sub rblSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSelection.SelectedIndexChanged
        refreshGrid()

    End Sub
    Protected Sub RadFilterBSU_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles RadFilterBSU.SelectedIndexChanged
        refreshGrid()
    End Sub
End Class


