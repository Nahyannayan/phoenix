﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime
Partial Class Transport_ExtraHiring_tptHireCharges
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64
    Dim Posted As Boolean = False


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim dtVATUserAccess As New DataTable


                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200148") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                txtDiscount.Attributes.Add("onblur", " return calc('T');")
                txtDiscountedRatePM.Attributes.Add("onblur", " return calc('T');")
                txtRentPM.Attributes.Add("onblur", " return calc('T');")
                txtTotalAmount.Attributes.Add("onblur", " return calc('T');")
                txtSalik.Attributes.Add("onblur", " return calc('T');")
                txtFuel.Attributes.Add("onblur", " return calc('T');")
                txtStartingKM.Attributes.Add("onblur", " return calc('K');")
                txtEndingKM.Attributes.Add("onblur", " return calc('K');")
                txtUsageLimit.Attributes.Add("onblur", " return calc('K');")
                txtExcessKMRate.Attributes.Add("onblur", " return calc('K');")
                txtAddKM.Attributes.Add("onblur", " return calc('K');")
                clearScreen()
                sqlStr = "select THC_ID ID, CONVERT(VARCHAR(20),THC_TRDATE,106)TRDATE,BSU_NAME  [Work Unit],THC_REGNO [Reg No.],THC_VEHICLETYPE [Type of Vehicle]"
                sqlStr &= ",THC_MODEL Model, THC_UNITREG [Unit Reg],THC_USERNAME [User],THC_RENTPM [Rent per month],THC_DISCOUNT Discount,THC_DISRENTPM [Discounted Rate per month]"
                sqlStr &= ",THC_SALIK [SALIK (@AED 4) p.m.],THC_FUEL [Fuel p.m.],THC_TOTAL [TOTAL AMOUNT IN AED.],THC_STARTKM [Starting KM],THC_ENDKM [Ending KM] "
                sqlStr &= ",THC_ENDKM-THC_STARTKM [Total KM Travelled], THC_USAGELIMIT [Usage Limit],THC_ADDKM [Additional KM Travelled] , THC_EXCESSKMRATE [Excess KM Rate]"
                sqlStr &= ",THC_EXCESSKMAMT [Excess KM Charges], THC_TOTBILLEDAMT [Total Amount to be billed],THC_INVOICE [Invoice No],THC_REMARKS Remarks"
                sqlStr &= ",THC_NOTES Notes,THC_USER [Login User],THC_JHD_DOCNO [IJV NO],THC_CTO_BSU_ID,THC_POSTING_DATE "
                'sqlStr &= ",THC_BSU_ID, THC_CTO_BSU_ID,  THC_FYEAR,THC_DATE, , THC_DELETED,THC_POSTED   "
                sqlStr &= ",isnull(DESCR,'') DAX_DESCR,isnull(THC_PRGM_DIM_CODE,'0') DAX_ID,ISNULL(THC_TAX_AMOUNT,0)THC_TAX_AMOUNT,ISNULL(THC_TAX_NET_AMOUNT,0)THC_TAX_NET_AMOUNT,ISNULL(THC_TAX_CODE,'0')THC_TAX_CODE,isnull(THC_EMR_CODE,'NA')THC_EMR_CODE,EMR_DESCR EMIRATE "
                sqlStr &= " FROM TPTHireCharges   INNER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=THC_CTO_BSU_ID  "
                sqlStr &= "  left outer join dbo.VV_DAX_CODES on ID=THC_PRGM_DIM_CODE  "
                sqlStr &= " left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE= isnull(THC_EMR_CODE,'NA') "

                BindBsu()
                BindDAXCodes()
                RadFilterBSU.SelectedValue = "0"
                refreshGrid()

                dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")
                If dtVATUserAccess.Rows.Count > 0 Then
                    If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                        radVAT.Enabled = True
                        RadEmirate.Enabled = True
                    Else
                        radVAT.Enabled = False
                        RadEmirate.Enabled = False
                    End If
                Else
                    radVAT.Enabled = False
                    RadEmirate.Enabled = False
                End If
                CalculateVAT(RadCBSU.SelectedValue)
                SelectEmirate(RadCBSU.SelectedValue)
                If Request.QueryString("VIEWID") <> "" Then
                    h_PRI_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_PRI_ID.Value)
                End If
                btnSave.Visible = True

            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblVATAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmt"), Label)
            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                VAT += Val(lblVAT.Text)
                Net += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAt.Text = VAT
        txtSelNet.Text = Net

    End Sub
    Private Sub refreshGrid()
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")

        If rblSelection.SelectedValue = 0 Then
            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(THC_TRDATE)= MONTH('" & txtDate.Text & "') AND THC_DELETED=0 and THC_BSU_ID='" & Session("sBsuid") & "' and THC_FYEAR='" & Session("F_YEAR") & "' and isnull(THC_POSTED,0)=0 ORDER BY [Work Unit]"
            Else
                sqlWhere = " where  MONTH(THC_TRDATE)= MONTH('" & txtDate.Text & "') AND THC_DELETED=0 and THC_BSU_ID='" & Session("sBsuid") & "' and THC_FYEAR='" & Session("F_YEAR") & "' and THC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(THC_POSTED,0)=0 ORDER BY [Work Unit]"
            End If


        ElseIf rblSelection.SelectedValue = 1 Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(THC_TRDATE)= MONTH('" & txtDate.Text & "') AND THC_DELETED=0 and THC_BSU_ID='" & Session("sBsuid") & "' and THC_FYEAR='" & Session("F_YEAR") & "' and isnull(THC_POSTED,0)=1 ORDER BY [Work Unit]"
            Else
                sqlWhere = " where  MONTH(THC_TRDATE)= MONTH('" & txtDate.Text & "') AND THC_DELETED=0 and THC_BSU_ID='" & Session("sBsuid") & "' and THC_FYEAR='" & Session("F_YEAR") & "' and THC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "'   and isnull(THC_POSTED,0)=1 ORDER BY [Work Unit]"
            End If

        ElseIf rblSelection.SelectedValue = 2 Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(THC_TRDATE)= MONTH('" & txtDate.Text & "') AND THC_DELETED=0 and THC_BSU_ID='" & Session("sBsuid") & "' and THC_FYEAR='" & Session("F_YEAR") & "' ORDER BY [Work Unit]"
            Else
                sqlWhere = " where  MONTH(THC_TRDATE)= MONTH('" & txtDate.Text & "') AND THC_DELETED=0 and THC_BSU_ID='" & Session("sBsuid") & "' and THC_FYEAR='" & Session("F_YEAR") & "' and THC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "' ORDER BY [Work Unit]"
            End If

        End If

        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTHireCharges = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
            Session("myData") = TPTHireCharges
            SetExportFileLink()
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub clearScreen()
        h_PRI_ID.Value = "0"
        txtRentPM.Text = 0
        txtDiscount.Text = "0"
        txtDiscountedRatePM.Text = "0"
        txtSalik.Text = "0"
        txtFuel.Text = "0"
        txtTotalAmount.Text = "0"
        txttotalBillAmount.Text = "0"
        txtTotalKM.Text = "0"
        txtStartingKM.Text = "0"
        txtEndingKM.Text = "0"
        txtRegno.Text = ""
        txtModel.Text = ""
        txtUser.Text = ""
        txtRemarks.Text = ""
        txtUsageLimit.Text = "0"
        txtAddKM.Text = "0"
        txtExcessKMCharge.Text = "0"
        txtExcessKMRate.Text = "0"
        txtVehicleType.Text = ""
        txtNotes.Text = ""
        txtUnitRegno.Text = ""
        RadDAXCodes.SelectedValue = "0"
        SelectEmirate(RadCBSU.SelectedValue)

    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim strsql3 As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            strsql3 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

            radVAT.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            radVAT.DataTextField = "DESCR"
            radVAT.DataValueField = "ID"
            radVAT.DataBind()

            RadEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql3)
            RadEmirate.DataTextField = "DESCR"
            RadEmirate.DataValueField = "ID"
            RadEmirate.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property
    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If h_TEI_ID.Value <> 0 Then lblError.Text &= " This month extra trips are posted already.You can not enter for this month, "

        If RadDAXCodes.SelectedValue = "0" Then lblError.Text &= " please select DAX Posting Code, "

        If PurposeExists = 0 Then lblError.Text &= "Purpose is "

        If RadEmirate.SelectedValue = "NA" Then lblError.Text &= "Please Select Emirate....!"

        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            lblError.Visible = True
            Return
        End If
        Try
            Dim pParms(32) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@THC_ID", h_PRI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@THC_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@THC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@THC_CTO_BSU_ID", RadCBSU.SelectedValue, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@THC_VEHICLETYPE", txtVehicleType.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@THC_REGNO", txtRegno.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@THC_MODEL", txtModel.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@THC_UNITREG", txtUnitRegno.Text, SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@THC_USERNAME", txtUser.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@THC_RENTPM", txtRentPM.Text, SqlDbType.VarChar)
            pParms(11) = Mainclass.CreateSqlParameter("@THC_DISCOUNT", txtDiscount.Text, SqlDbType.Float)
            pParms(12) = Mainclass.CreateSqlParameter("@THC_DISRENTPM", txtDiscountedRatePM.Text, SqlDbType.Float)
            pParms(13) = Mainclass.CreateSqlParameter("@THC_SALIK", txtSalik.Text, SqlDbType.Float)
            pParms(14) = Mainclass.CreateSqlParameter("@THC_FUEL", txtFuel.Text, SqlDbType.Float)
            pParms(15) = Mainclass.CreateSqlParameter("@THC_TOTAL", txtTotalAmount.Text, SqlDbType.Float)
            pParms(16) = Mainclass.CreateSqlParameter("@THC_STARTKM", txtStartingKM.Text, SqlDbType.Float)
            pParms(17) = Mainclass.CreateSqlParameter("@THC_ENDKM", txtEndingKM.Text, SqlDbType.Float)
            pParms(18) = Mainclass.CreateSqlParameter("@THC_TOTALKM", txtTotalKM.Text, SqlDbType.Float)
            pParms(19) = Mainclass.CreateSqlParameter("@THC_USAGELIMIT", txtUsageLimit.Text, SqlDbType.Float)
            pParms(20) = Mainclass.CreateSqlParameter("@THC_ADDKM", txtAddKM.Text, SqlDbType.Float)
            pParms(21) = Mainclass.CreateSqlParameter("@THC_EXCESSKMRATE", txtExcessKMRate.Text, SqlDbType.Float)
            pParms(22) = Mainclass.CreateSqlParameter("@THC_EXCESSKMAMT", txtExcessKMCharge.Text, SqlDbType.Float)
            pParms(23) = Mainclass.CreateSqlParameter("@THC_TOTBILLEDAMT", txttotalBillAmount.Text, SqlDbType.Float)
            pParms(24) = Mainclass.CreateSqlParameter("@THC_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
            pParms(25) = Mainclass.CreateSqlParameter("@THC_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(26) = Mainclass.CreateSqlParameter("@THC_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(27) = Mainclass.CreateSqlParameter("@THC_NOTES", txtNotes.Text, SqlDbType.VarChar)
            pParms(28) = Mainclass.CreateSqlParameter("@THC_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
            pParms(29) = Mainclass.CreateSqlParameter("@THC_TAX_CODE", radVAT.SelectedValue, SqlDbType.VarChar)
            pParms(30) = Mainclass.CreateSqlParameter("@THC_TAX_AMOUNT", txtVATAmount.Text, SqlDbType.Float)
            pParms(31) = Mainclass.CreateSqlParameter("@THC_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
            pParms(32) = Mainclass.CreateSqlParameter("@THC_EMR_CODE", RadEmirate.SelectedValue, SqlDbType.VarChar)


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTHIRECHARGE", pParms)
                If RetVal = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ID, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                        clearScreen()
                        refreshGrid()
                        'lblError.Text = "Data Saved Successfully !!!"
                        usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If


                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Success)
        End Try
    End Sub


    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub

    Private Sub showEdit(ByVal showId As Integer)
        Dim rdrHireCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & " where THC_ID='" & showId & "'")

        If rdrHireCharges.HasRows Then
            rdrHireCharges.Read()
            h_PRI_ID.Value = rdrHireCharges("ID")
            txtDate.Text = rdrHireCharges("TRDATE")
            RadCBSU.SelectedItem.Text = rdrHireCharges("Work Unit")
            RadCBSU.SelectedValue = Mainclass.getDataValue("select THC_CTO_BSU_ID from tpthirecharges where THC_ID=" & showId, "OASIS_TRANSPORTConnectionString")
            txtRegno.Text = rdrHireCharges("Reg No.")
            txtVehicleType.Text = rdrHireCharges("Type of Vehicle")
            txtModel.Text = rdrHireCharges("Model")
            txtUnitRegno.Text = rdrHireCharges("unit reg")
            txtUser.Text = rdrHireCharges("User")
            txtRentPM.Text = rdrHireCharges("Rent per month")
            txtDiscount.Text = rdrHireCharges("Discount")
            txtDiscountedRatePM.Text = rdrHireCharges("Discounted Rate per month")
            txtSalik.Text = rdrHireCharges("SALIK (@AED 4) p.m.")
            txtFuel.Text = rdrHireCharges("Fuel p.m.")
            txtTotalAmount.Text = rdrHireCharges("TOTAL AMOUNT IN AED.")
            txtStartingKM.Text = rdrHireCharges("Starting KM")
            txtEndingKM.Text = rdrHireCharges("Ending KM")
            txtUsageLimit.Text = rdrHireCharges("Usage Limit")
            txtAddKM.Text = rdrHireCharges("Additional KM Travelled")
            txtExcessKMRate.Text = rdrHireCharges("Excess KM Rate")
            txtExcessKMCharge.Text = rdrHireCharges("Excess KM Charges")
            txttotalBillAmount.Text = rdrHireCharges("Total Amount to be billed")
            txtTotalKM.Text = rdrHireCharges("Ending KM") - rdrHireCharges("Starting KM")
            txtVATAmount.Text = rdrHireCharges("THC_TAX_AMOUNT")
            txtNetAmount.Text = rdrHireCharges("THC_TAX_NET_AMOUNT")
            radVAT.SelectedValue = rdrHireCharges("THC_TAX_CODE")
            txtRemarks.Text = rdrHireCharges("Remarks")
            txtNotes.Text = rdrHireCharges("Notes")
            RadDAXCodes.SelectedItem.Text = rdrHireCharges("DAX_DESCR")
            RadDAXCodes.SelectedValue = rdrHireCharges("DAX_ID")

            Dim lstDrp As New RadComboBoxItem
            lstDrp = RadEmirate.Items.FindItemByValue(rdrHireCharges("THC_EMR_CODE"))
            If Not lstDrp Is Nothing Then
                RadEmirate.SelectedValue = lstDrp.Value
            Else
                RadEmirate.SelectedValue = "NA"
            End If
            BindDAXCodes()
            CalculateVAT(RadCBSU.SelectedValue)
        End If
        rdrHireCharges.Close()
        rdrHireCharges = Nothing

    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub
    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(26).Text = "&nbsp;" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(31).Enabled = True
                gvr.Cells(32).Enabled = False
                gvr.Cells(33).Enabled = False

            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
                gvr.Cells(31).Enabled = False
                gvr.Cells(32).Enabled = True
                gvr.Cells(33).Enabled = True
            End If
        Next


    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Vehicle Charges")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTHCID"), Label)

        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTHireCHARGES set THC_DELETED=1,THC_USER='" & Session("sUsr_name") & "',THC_DATE=GETDATE() where THC_ID=" & lblID.Text)
        refreshGrid()
    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTHC_invNo"), Label)
        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTHireCharges"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@THC_INVOICE", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@THC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            'If Format(CDate(lblPOstingDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(lblPOstingDate.Text) <= CDate("31/Dec/2017") Then
                repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTHireCharges.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTHireCharges.rpt"
            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/../Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/../Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub lnkEmail_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblHC_invNo"), Label)
        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Dim bEmailSuccess As Boolean
        SendEmail(CType(lblID.Text, String), CDate(lblPOstingDate.Text), bEmailSuccess)
        If bEmailSuccess = True Then
            'lblError.Text = "Email send  Sucessfully...!"
            usrMessageBar.ShowNotification("Email send  Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
        Else
            'lblError.Text = "Error while sending email.."
            usrMessageBar.ShowNotification("Error while sending email..", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByVal InvDate As DateTime, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""

        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass

        param("@THC_INVOICE") = RecNo
        param("@THC_BSU_ID") = Session("sBsuid")
        param.Add("userName", "SYSTEM")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))

        'If Format(CDate(InvDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
        If CDate(InvDate) <= CDate("31/Dec/2017") Then
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTHireChargesEmail.rpt")
        Else
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTHireChargesEmailTAX.rpt")
        End If

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = RecNo
        rptDownload.InvType = "HIRE"
        rptDownload.FCO_ID = 0
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & RecNo & "','" & FCL_ID & "','" & SendEmail & "' ")
        End If
    End Function


    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid()
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString

            Dim BSUParms(2) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            RadCBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForDriverANDHirecharges", BSUParms)
            RadFilterBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForDriverANDHirecharges", BSUParms)
            RadCBSU.DataTextField = "BSU_NAME"
            RadCBSU.DataValueField = "SVB_BSU_ID"

            RadFilterBSU.DataTextField = "BSU_NAME"
            RadFilterBSU.DataValueField = "SVB_BSU_ID"

            RadCBSU.DataBind()
            RadFilterBSU.DataBind()


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        clearScreen()
        refreshGrid()
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click, btnPost1.Click
        lblError.Text = ""
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox
        Dim PartyCode As String = ""
        Dim i As Integer = 0
        Dim mrow As GridViewRow
        Dim SelectedIDs As String = ""
        Dim NotSameBsu As Boolean = False
        Dim PurposeExists As Integer = 1

        If txtPostingDate.Text.Trim = "" Then lblError.Text &= "Please select Posting Date,"

        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " "
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " ", UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Visible = True
            Return
        End If



        For Each mrow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(mrow.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(mrow.FindControl("lblTHCID"), Label)

            If ChkBxItem.Checked = True Then

                If i = 0 Then
                    PartyCode = mrow.Cells(3).Text
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                Else
                    If PartyCode = mrow.Cells(3).Text Then
                        IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    Else
                        NotSameBsu = True
                    End If
                End If
                i = i + 1
            End If

        Next
        If NotSameBsu = False Then
            PostVehicleChargeInvoice(IDs)
            If Posted = True Then
                'lblError.Text = "Data Posted Successfully !!!"
                usrMessageBar.ShowNotification("Data Posted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            Else
                '                lblError.Text = "Some error while posting..."
                usrMessageBar.ShowNotification("Some error while posting...", UserControls_usrMessageBar.WarningType.Danger)
            End If

            refreshGrid()
        Else
            'lblError.Text = "Selected business units are  not same!!!"
            usrMessageBar.ShowNotification("Selected business units are  not same!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

    End Sub
    Private Sub PostVehicleChargeInvoice(ByVal id As String)

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(6) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", id, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@POSTINGDATE", txtPostingDate.Text, SqlDbType.DateTime)


        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostDriverCharges", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Posted = False
                Exit Sub
            Else
                stTrans.Commit()
                usrMessageBar.ShowNotification("Posted Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
                Posted = True
            End If

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, id, "Posting", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Posted = False
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub


    Private Property TPTHireCharges() As DataTable
        Get
            Return ViewState("TPTHireCharges")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTHireCharges") = value
        End Set
    End Property
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click, btnCopy1.Click

        CopyDriverChargeDetails()


    End Sub
    Private Sub CopyDriverChargeDetails()

        Dim NewDate As Date
        Dim Newdate1 As Date

        NewDate = Format(CDate(txtDate.Text.ToString), "dd/MMM/yyyy")
        Newdate1 = NewDate.AddMonths(1)
        Newdate1 = New Date(NewDate.AddMonths(1).Year, NewDate.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1)
        Newdate1 = Format(CDate(Newdate1), "dd/MMM/yyyy")
        Dim CParms(4) As SqlClient.SqlParameter

        CParms(0) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
        CParms(0).Value = NewDate
        CParms(1) = New SqlClient.SqlParameter("@DATE1", SqlDbType.DateTime)
        CParms(1).Value = Newdate1

        CParms(2) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar, 2000)
        CParms(2).Value = "VEHICLE"
        CParms(3) = New SqlClient.SqlParameter("@BSU", SqlDbType.VarChar, 6)
        CParms(3).Value = Session("sBsuid")

        CParms(4) = New SqlClient.SqlParameter("@Errormsg", SqlDbType.VarChar, 1000)
        CParms(4).Direction = ParameterDirection.Output

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "CopyInvoiceDetails", CParms)
            Dim ErrorMSG As String = CParms(4).Value.ToString()


            If ErrorMSG = "" Then
                stTrans.Commit()
                usrMessageBar.ShowNotification("Copied Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                txtDate.Text = Newdate1.ToString("dd/MMM/yyyy")
                refreshGrid()

            Else
                usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
                refreshGrid()
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
            refreshGrid()

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub RadFilterBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadFilterBSU.SelectedIndexChanged

        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'sqlWhere = " where  YEAR(THC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(THC_TRDATE)= MONTH('" & txtDate.Text & "') AND THC_DELETED=0 and THC_BSU_ID='" & Session("sBsuid") & "' and THC_FYEAR='" & Session("F_YEAR") & "' and THC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue & "' ORDER BY [Work Unit]"
        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTHireCharges = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTHireCharges
        '    SetExportFileLink()
        '    txtSelectedAmt.Text = "0.00"

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try
        refreshGrid()

    End Sub

    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim NEt As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)

            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                Dim lblVAT As Label = TryCast(gvr.FindControl("lblVATAmt"), Label)
                Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmt"), Label)
                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    Gross += Val(lblGross.Text)
                    VAT += Val(lblVAT.Text)
                    NEt += Val(lblNet.Text)
                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
                VAT = 0
                NEt = 0
            End If

        Next

        txtSelectedAmt.Text = Gross
        txtSelVAt.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Private Sub CalculateVAT(ByVal bsuid As String)
        Dim dt As New DataTable
        dt = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','VHC_HIRE_CHG','" & txtDate.Text & "'," & txttotalBillAmount.Text & ",'" & bsuid & "')", "OASIS_TRANSPORTConnectionString")
        If dt.Rows.Count > 0 Then
            Dim lstDrp As New RadComboBoxItem
            lstDrp = radVAT.Items.FindItemByValue(dt.Rows(0)("tax_code"))
            h_VATPerc.Value = dt.Rows(0)("tax_perc_value")
            'lstDrp = radVAT.Items.FindItemByValue("VAT5")
            'If bsuid <> "500953" Then
            '    h_VATPerc.Value = 5
            'Else
            '    h_VATPerc.Value = 7
            'End If
            If Not lstDrp Is Nothing Then
                radVAT.SelectedValue = lstDrp.Value
            Else
                radVAT.SelectedValue = 0
            End If
            txtVATAmount.Text = ((Val(txttotalBillAmount.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = (Val(txttotalBillAmount.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub

    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged
        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVAT.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            txtVATAmount.Text = ((Val(txttotalBillAmount.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txttotalBillAmount.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        Else
            radVAT.SelectedValue = 0
            h_VATPerc.Value = radVAT.SelectedValue
            txtVATAmount.Text = ((Val(txttotalBillAmount.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txttotalBillAmount.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub RadCBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadCBSU.SelectedIndexChanged
        CalculateVAT(RadCBSU.SelectedValue)
        SelectEmirate(RadCBSU.SelectedValue)
    End Sub
    Private Sub SelectEmirate(ByVal BSUID As String)
        Dim bsu_City As String = ""
        bsu_City = Mainclass.getDataValue("SELECT case when case when BSU_CITY='ALN' then 'AUH' else BSU_CITY end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when BSU_CITY='ALN' then 'AUH' else BSU_CITY end end   BSU_CITY   FROM OASIS..BUSINESSUNIT_M where BSU_id='" & BSUID & "' ", "OASIS_TRANSPORTConnectionString")
        Dim lstDrp As New RadComboBoxItem
        lstDrp = RadEmirate.Items.FindItemByValue(bsu_City)
        If Not lstDrp Is Nothing Then
            RadEmirate.SelectedValue = lstDrp.Value
        Else
            RadEmirate.SelectedValue = "NA"
        End If
    End Sub

    Protected Sub rblSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSelection.SelectedIndexChanged
        refreshGrid()

    End Sub
End Class



