<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptProforma.aspx.vb" Inherits="Transport_ExtraHiring_tptProforma" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        function calculate() {
        }

        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function ChangeAllCheckBoxStates(checkState) {

            var chk_state = document.getElementById("chkAL").checked;

            if (chk_state) {


            }
            else {




            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Invoice Posting
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <tr valign="bottom" width="100%">
                        <td align="left"  valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="100%">
                            <table align="left" cellpadding="5" cellspacing="0"
                                style="width: 100%">
                                <%--<tr class="subheader_img">
              <td align="center"  valign="middle" width="100%" colspan ="2">
                <div align="center">
                  Invoice Posting
                </div>
              </td>
            </tr>--%>
                                <tr>
                                    <td width="20%"><span class="field-label">Address</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAddress" runat="server"
                                            TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                    </td>
                                    <td width="20%"><span class="field-label">Remarks</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtRemarks" runat="server"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="100%" align="left" colspan="4">
                                        <asp:GridView ID="grdInvoiceDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            PageSize="100" Width="100%" ShowFooter="True" CaptionAlign="Top"
                                            DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField HeaderText=" ">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAL" AutoPostBack="true" runat="server" OnCheckedChanged="chkAL_CheckedChanged"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TRN_No" HeaderText="No" />
                                                <asp:BoundField DataField="Job No" HeaderText="Job No" />
                                                <asp:BoundField DataField="Job Date" HeaderText="Job Date" />
                                                <asp:BoundField DataField="Client" HeaderText="Client" />
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="TAX Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTAXAmount" runat="server" Text='<%# Eval("TAX Amount") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Net Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNetAmount" runat="server" Text='<%# Eval("Net Amount") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:BoundField DataField="Vehicle No" HeaderText="Vehicle No." />
                                                <asp:BoundField DataField="Group" HeaderText="Group" />
                                                <asp:BoundField DataField="Pickup Point" HeaderText="Pickup Point" />

                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltrn_No" runat="server" Text='<%# Eval("TRN_No") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="100%" colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td width="10%"><span class="field-label">Amount</span>
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblTotal" runat="server" CssClass="field-value"></asp:Label>
                                                </td>
                                                <td width="10%"><span class="field-label">TAX Amount </span></td>
                                                <td width="20%">
                                                    <asp:Label ID="lblTax" runat="server" CssClass="field-value"></asp:Label></td>

                                                <td width="10%"><span class="field-label">Net Amount</span></td>
                                                <td width="20%">
                                                    <asp:Label ID="lblNetAmount" runat="server" CssClass="field-value"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" width="100%" colspan="4">
                                        <asp:Button ID="btnProforma" runat="server" CausesValidation="False" CssClass="button" Text="Generate Proforma" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" width="100%" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="hdn_Total" runat="server" Value="0" />

                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_QUDGridDelete" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
