﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptIDCardCharges.aspx.vb" Inherits="Transport_ExtraHiring_tptIDCardCharges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

    <style type="text/css">
        .autocomplete_highlightedListItem_S1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }

        .style1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 107px;
        }

        

 .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


    </style>

    <script language="javascript" type="text/javascript">

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtCardType.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hCARDID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtPrice.ClientID %>").value = NameandCode[2];
                __doPostBack('<%= txtCardType.ClientID %>', 'TextChanged');
            }
        }


        function getCardsDetails() {
           
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
           

            var RB1 = document.getElementById("<%=rblType.ClientID%>");
            var radio = RB1.getElementsByTagName("input");
            var label = RB1.getElementsByTagName("label");
            for (var x = 0; x < radio.length; x++) {
                if (radio[x].checked) {
                    var TypeSelected = label[x].innerHTML;
                }
            }
            if (TypeSelected == 'Student')
                pMode = "S";
            else
                pMode = "E";

            url = "../../common/PopupSelect.aspx?id=TPTCARDCHARGES&Type=" + pMode;
           
            result =radopen(url, "pop_up");
           
        }


        function calcTotal() {
            document.getElementById('<%=txtAmount.ClientID %>').value = document.getElementById('<%=txtqty.ClientID %>').value * document.getElementById('<%=txtPrice.ClientID %>').value;
            document.getElementById('<%=txtTotal.ClientID %>').value = document.getElementById('<%=txtAmount.ClientID %>').value - document.getElementById('<%=txtDiscount.ClientID %>').value;

            var VATAmt = (eval(document.getElementById("<%=txtTotal.ClientID %>").value) * eval(document.getElementById("<%=h_VATPerc.ClientID %>").value)) / 100;
            document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
            document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txtTotal.ClientID %>").value)).toFixed(2);


        }

    </script>

    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            ID Card Charges
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">
                            <table align="center" width="100%">
                                
                                <tr>
                                   <td>
                                       <table width="100%">
                                           <tr>
                                               <td align="left" width="5%"><span class="field-label">Date </span></td>
                                               <td width="10%">
                                                   <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                   <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                   <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                       TargetControlID="txtDate" PopupButtonID="lnkDate">
                                                   </ajaxToolkit:CalendarExtender>
                                               </td>
                                               <td width="10%">
                                                   <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Load.." />
                                               </td>
                                               <td width="70%"></td>
                                           </tr>
                                       </table>
                                   </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th>Customer
                                                </th>
                                                <th>Employee Type
                                                </th>
                                                <th>DAX Code
                                                </th>
                                                <th>Card Type
                                                </th>
                                                <th>Price
                                                </th>
                                                <th>Qty
                                                </th>
                                                <th>Amount
                                                </th>
                                                <th>Discount
                                                </th>
                                                <th>Total Amount
                                                </th>
                                                <th>VAT Code
                                                </th>
                                                <th>Emirate
                                                </th>
                                                <th>VAT Amount
                                                </th>
                                                <th>Net Amount
                                                </th>
                                                <th>Remarks
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtTSRSNo" runat="server" Width="100px" Visible="false"></asp:TextBox>
                                                    <telerik:RadComboBox ID="RadCustomer" Width="350" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>

                                                <td>
                                                    <asp:RadioButtonList ID="rblType" runat="server" RepeatDirection="horizontal" AutoPostBack="true" Width="100">
                                                        <asp:ListItem Selected="True" Value="0">Student</asp:ListItem>
                                                        <asp:ListItem Value="1">Staff</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>

                                                <td>
                                                    <telerik:RadComboBox ID="RadDAXCodes" Width="225" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>

                                                <td>
                                                    <%--<telerik:RadComboBox ID="RadCardType" Width="200" runat="server" DropDownCssClass="multipleRowsColumns" >
                            </telerik:RadComboBox>--%>
                                                    <table>
                                                        <tr>
                                                            <td width="90%">
                                                                <asp:TextBox ID="txtCardType" runat="server" TabIndex="1" Width="300px"></asp:TextBox>
                                                            </td>
                                                            <td width="10%">
                                                                <asp:ImageButton ID="imgCardType" runat="server" align="left" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCardsDetails();return false;" />
                                                            </td>

                                                        </tr>
                                                    </table>


                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtPrice" style="text-align:right"  Width="100px" runat="server" TabIndex="1"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtqty" style="text-align:right"  Width="100px" runat="server" TabIndex="2" align="left"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtAmount" style="text-align:right"  Width="100px"  runat="server" Enabled="false"  TabIndex="3"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtDiscount" style="text-align:right"  Width="100px"  runat="server"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtTotal" style="text-align:right"  Width="100px"  runat="server" Enabled="false" TabIndex="4"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="radVAT" Width="275" runat="server" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadEmirate" Width="225" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtVATAmount" style="text-align:right"  Width="100px"  Enabled="false" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNetAmount" style="text-align:right"  Width="100px"  Enabled="false" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtRemarks" runat="server" Width="250px" TabIndex="4"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                       <td colspan="3">
                           <table width="100%">
                               <tr>
                                   <td width="40%"></td>
                                   <td valign="bottom" align="center">
                                       <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                       <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                           Text="Cancel" UseSubmitBehavior="False" />
                                       <asp:HyperLink ID="btnExport" runat="server" Visible="false">Export to Excel</asp:HyperLink>
                                   </td>
                                   <td width="40%"></td>
                               </tr>
                           </table>
                       </td>
                    </tr>


                    <tr>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="3%" >
                                        <asp:Button ID="btnPost1" runat="server" CssClass="button" Text="Post" />
                                        <asp:HiddenField ID="hdngross" runat="server" />
                                    </td>
                                    <td width="10%">
                                        <telerik:RadComboBox ID="RadFilterBSU" Width="350" runat="server" AllowCustomText="true" Filter="Contains" Visible="true" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>
                                    </td>
                                   
                                    <td width="15%">

                             <asp:RadioButtonList ID="rblSelection" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" >
                                            <asp:ListItem Selected="True" Value="0"><span class="field-label"> Not Posted </span></asp:ListItem>
                                            <asp:ListItem Value="1"><span class="field-label">Posted </span></asp:ListItem>
                                            <asp:ListItem Value="2"><span class="field-label">All </span></asp:ListItem>
                                        </asp:RadioButtonList>
                            </td>

                                    <td width="5%">
                                        <asp:Label ID="Postingdate" Text="Posting Date" CssClass="field-label" runat="server"></asp:Label>
                                    </td>
                                     
                                    <td width="8%">
                                        <asp:TextBox ID="txtPostingDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgPosting" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtPostingDate" PopupButtonID="ImgPosting">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>

                                    <td width="5%">
                                        <asp:Label ID="lblSelAmount" CssClass="field-label" Text="Selected Amount" runat="server"></asp:Label>
                                    </td>
                                    <td width="4%">
                                        <asp:TextBox ID="txtSelectedAmt" style="text-align:right" runat="server"></asp:TextBox>
                                    </td>

                                    <td width="5%">
                                        <asp:Label ID="Label1" CssClass="field-label" Text="VAT Amount" runat="server"></asp:Label>
                                    </td>
                                    <td width="4%">
                                        <asp:TextBox ID="txtSelVAT" style="text-align:right" runat="server"></asp:TextBox>
                                    </td>

                                    <td width="5%">
                                        <asp:Label ID="Label2" CssClass="field-label" Text="Net Amount" runat="server"></asp:Label>
                                    </td>
                                    <td width="4%">
                                        <asp:TextBox ID="txtSelNet" style="text-align:right" runat="server"></asp:TextBox>
                                    </td>
                                    
                                    <td width="31%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row mb-0"
                                Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="TIC_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <%--<input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All"   />                                --%>
                                            <asp:CheckBox ID="chkAL" AutoPostBack="true" runat="server" OnCheckedChanged="chkAL_CheckedChanged"></asp:CheckBox>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("TIC_ID") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TIC_TRDATE" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle Width="80px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Customer" HeaderText="Businessunit" />
                                    <asp:BoundField DataField="TCT_DESCR" HeaderText="Card Name" />
                                    <asp:BoundField DataField="DAX_DESCR" HeaderText="DAX CODE" />
                                    <asp:BoundField DataField="TIC_TYPE1" HeaderText="Type" />
                                    <asp:BoundField DataField="TIC_RATE" HeaderText="Rate" />
                                    <asp:BoundField DataField="TIC_QTY" HeaderText="Qty" />
                                    <asp:BoundField DataField="TIC_AMOUNT" HeaderText="Amount" />
                                    <asp:BoundField DataField="TIC_DISCOUNT" HeaderText="Discount" />
                                    <asp:TemplateField HeaderText="Total  Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Eval("TIC_TOTAL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TIC_TAX_CODE" HeaderText="VAT Code" />
                                    <asp:BoundField DataField="EMIRATE" HeaderText="Emirate" />
                                    <%--<asp:BoundField DataField="TIC_TAX_AMOUNT" HeaderText="VAT Amount" />
                                        <asp:BoundField DataField="TIC_VAT_NET_AMOUNT" HeaderText="Net Amount" />--%>

                                    <asp:TemplateField HeaderText="VAT Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVATAmt" runat="server" Text='<%# Eval("TIC_TAX_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetAmt" runat="server" Text='<%# Eval("TIC_TAX_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="TIC_remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TIC_INVNO" HeaderText="Invoice" />
                                    <asp:BoundField DataField="TIC_USER" HeaderText="User Name" />
                                    <asp:BoundField DataField="TIC_JHD_DOCNO" HeaderText="IJV No." />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTIC_ID" runat="server" Text='<%# Eval("TIC_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" OnClick="lnkPrint_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTIC_INVNO" runat="server" Text='<%# Eval("TIC_INVNO") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPOstingDate" runat="server" Text='<%# Eval("TIC_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmail" runat="server" Text="Email" OnClick="lnkEmail_CLick"></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                      <td colspan="3">
                          <table width="100%">
                              <tr>
                                  <td width="40%"></td>
                                  <td width="20%" align="center">
                                      <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" />
                                  </td>
                                  <td width="40%"></td>
                              </tr>
                          </table>
                      </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="3">
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_mimimum" runat="server" />
                            <asp:HiddenField ID="h_TEI_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_VehicleType" runat="server" />
                            <asp:HiddenField ID="hCARDID" runat="server" />
                            <asp:HiddenField ID="h_VATPerc" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
