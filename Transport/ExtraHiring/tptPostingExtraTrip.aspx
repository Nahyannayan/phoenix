﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="tptPostingExtraTrip.aspx.vb" Inherits="Transport_ExtraHiring_tptPostingExtraTrip" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
       
 .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function onChange() { alert('ok'); }
        function calculate() {
            var total = eval(document.getElementById("<%=txtgross.ClientID %>").value) - eval(document.getElementById("<%=txtDiscount.ClientID %>").value);

            document.getElementById("<%=txtNetTotal.ClientID %>").value = eval(document.getElementById("<%=txtsalik.ClientID %>").value) + eval(total);

            var VatAmount = total * document.getElementById("<%=h_VATPerc.ClientID %>").value / 100.00;
            document.getElementById("<%=txtVatAmount.ClientID %>").value = VatAmount;
            document.getElementById("<%=txtNewNetAmount.ClientID %>").value = eval(VatAmount) + eval(document.getElementById("<%=txtNetTotal.ClientID %>").value);



        }


        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            if (chk_state) {
                document.getElementById("<%=txtgross.ClientID %>").value = document.getElementById("<%=hdngross.ClientID %>").value;
                document.getElementById("<%=txtNet.ClientID %>").value = document.getElementById("<%=hdnNet.ClientID %>").value;
                document.getElementById("<%=txtsalik.ClientID %>").value = document.getElementById("<%=hdnsalik.ClientID %>").value;
                document.getElementById("<%=txtDiscount.ClientID %>").value = document.getElementById("<%=hdnDiscount.ClientID %>").value;
                document.getElementById("<%=txtNetTotal.ClientID %>").value = document.getElementById("<%=hdnNetTotal.ClientID %>").value;
                
                <%--var VatAmount = (document.getElementById("<%=txtNetTotal.ClientID %>").value - document.getElementById("<%=txtsalik.ClientID %>").value) * document.getElementById("<%=h_VATPerc.ClientID %>").value / 100.00;--%>

                var VatAmount = (document.getElementById("<%=txtgross.ClientID %>").value - document.getElementById("<%=txtDiscount.ClientID %>").value) * document.getElementById("<%=h_VATPerc.ClientID %>").value / 100.00;

                var NetTotal = document.getElementById("ctl00_cphMasterpage_txtNetTotal").value;
                document.getElementById("<%=txtVatAmount.ClientID %>").value = VatAmount;

                document.getElementById("<%=txtNewNetAmount.ClientID %>").value = eval(VatAmount) + eval(NetTotal);


            }
            else {
                document.getElementById("<%=txtgross.ClientID %>").value = '0.00';
                document.getElementById("<%=txtNetTotal.ClientID %>").value = '0.00';
                document.getElementById("<%=txtVatAmount.ClientID %>").value = '0.00';
                document.getElementById("<%=txtNewNetAmount.ClientID %>").value = '0.00';
                document.getElementById("<%=txtNet.ClientID %>").value = '0.00';
                document.getElementById("<%=txtsalik.ClientID %>").value = '0.00';


            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }



        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Extra Trip Invoice Posting
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%;">
                    <tr valign="bottom">
                        <td align="center" valign="bottom" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table align="left" style="width: 100%">
                                <%--  <tr class="subheader_img">
                            <td align="center" colspan="5" valign="middle" width="100%">
                                <div align="center">
                                    Extra Trip Invoice Posting</div>
                            </td>
                        </tr>--%>
                                <tr>
                                    <td width="20%">
                                        <asp:Label ID="lblQUONo" runat="server" Text="Batch Number" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="50%">
                                                    <asp:TextBox ID="txtBatchNo" runat="server" Enabled="false"  ></asp:TextBox>
                                                </td>
                                                <td align="left" width="50%">
                                                    <asp:Label ID="Label4" runat="server" Text="Batch:" Enabled="false" ></asp:Label>
                                        <asp:Label ID="lblbatchstr" runat="server" Enabled="false" ></asp:Label><br />
                                        <asp:Label ID="Label3" runat="server" Text="Tr.Date:"  Enabled="false"></asp:Label>
                                        <asp:Label ID="lblTrDate" runat="server"  Enabled="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>                                        
                                        
                                    </td>
                                    <td width="20%"><span class="field-label">Batch Date</span>
                                    </td>
                                    <td align="left"  width="30%">
                                        <asp:TextBox ID="txtBatchDate" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="lnkBatchDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtBatchDate" PopupButtonID="lnkBatchDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="title-bg">Amount
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">

                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblGross" runat="server" Enabled="false" Text="Amount" CssClass="field-label"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtgross" Text="0.00" runat="server" Enabled="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hdngross" runat="server" />
                                                </td>

                                            
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" Enabled="false" Text="Salik" CssClass="field-label"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtsalik" Text="0.00" runat="server" Enabled="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnsalik" runat="server" />
                                                </td>
                                           
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Enabled="false" Text="Gross Amount" CssClass="field-label"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNet" Text="0.00" runat="server" Enabled="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnNet" runat="server" />
                                                </td>
                                            
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Enabled="true" Text="Discount" CssClass="field-label"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDiscount" Text="0.00" runat="server" Enabled="true"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnDiscount" runat="server" />
                                                </td>
                                           
                                                <td>
                                                    <asp:Label ID="Label6" runat="server" Enabled="false" Text="Total Amount" CssClass="field-label"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNetTotal" Text="0.00" runat="server" Enabled="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnNetTotal" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span class="text-danger">
                                            <asp:Label ID="lblStatus" runat="server" Text="" Visible="false"></asp:Label></span>
                                      <span class="field-label">DAX Posting Code</span>
                                    
                                    </td>
                                    <td width="30%" align="left">
                                        <telerik:RadComboBox ID="RadDAXCodes" Width="350" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>

                                    </td>
                                    <td colspan="8">
                                       <table width="100%">
                                           <tr>
                                               <td width="20%">
                                                   <asp:Label ID="Label9" runat="server" Text="VAT Code" CssClass="field-label"></asp:Label>
                                               </td>
                                               <td >
                                                   <telerik:RadComboBox ID="radVat" Width="349" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                   </telerik:RadComboBox>

                                               </td>
                                               <td>
                                                   <asp:Label ID="Label10" runat="server" Text="Emirate" CssClass="field-label"></asp:Label>
                                               </td>
                                               <td>
                                                   <telerik:RadComboBox ID="radEmirate" Width="250" runat="server" RenderMode="Lightweight">
                                                   </telerik:RadComboBox>
                                               </td>
                                           </tr>
                                       </table>

                                   </td>
                                </tr>

                                <tr>
                                   
                                   
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" Text="VAT  Amount" CssClass="field-label" ></asp:Label>
                                                </td>
                                                <td  align="left">
                                                    <asp:TextBox ID="txtVatAmount" runat="server" ReadOnly="true" Style="text-align: right"></asp:TextBox></td>
                                           
                                                <td >
                                                    <asp:Label ID="Label8" runat="server" Text="Net Amount" CssClass="field-label"></asp:Label></td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtNewNetAmount" runat="server" ReadOnly="true" Style="text-align: right"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><span class="field-label">Document No.</span>
                                    </td>

                                    <td align="left">
                                        <span style="color: red">
                                            <asp:Label ID="lblJHDDocNo" runat="server" Text="" CssClass="text-danger"></asp:Label></span>
                                    </td>
                                </tr>
                                




                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtremarks" runat="server"  TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                    </td>
                                    
                                </tr>

                                <tr>
                                    <td colspan="4" align="left">


                                        <asp:GridView ID="grdInvoiceDetails" runat="server" AutoGenerateColumns="False" PageSize="100" CssClass="table table-bordered table-row"
                                            Width="100%" ShowFooter="True" CaptionAlign="Top" 
                                            DataKeyNames="TEX_ID">
                                            <Columns>


                                                <asp:TemplateField HeaderText=" ">
                                                    <HeaderTemplate>
                                                        <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                            value="Check All" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="date" HeaderText="Date" />
                                                <asp:BoundField DataField="VehicleType" HeaderText="Vehicle Type" />
                                                <asp:BoundField DataField="Driver" HeaderText="Driver" />
                                                <asp:BoundField DataField="PURPOSE" HeaderText="Purpose" />
                                                <asp:BoundField DataField="STARTKM" HeaderText="Starting (K.M)" />
                                                <asp:BoundField DataField="ENDKM" HeaderText="Ending (K.M)" />
                                                <asp:BoundField DataField="STARTTIME" HeaderText="Start Time" />
                                                <asp:BoundField DataField="ENDTIME" HeaderText="End Time" />
                                                <asp:BoundField DataField="CHARGINGUNIT" HeaderText="Charging Unit" />

                                                <asp:TemplateField HeaderText="Gross">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGross" runat="server" Text='<%# Eval("GROSS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Salik">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSalik" runat="server" Text='<%# Eval("SALIK") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Net">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNet" runat="server" Text='<%# Eval("NET") %>'></asp:Label>
                                                        <asp:Label ID="lblTEXID" runat="server" Text='<%# Eval("TEX_ID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTEXTEIID" runat="server" Text='<%# Eval("TEX_TEI_ID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSAVE" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Save" Visible="False" />
                                        <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Post" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Print" />
                                        <asp:Button ID="btnEmail" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Email" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_QUDGridDelete" runat="server" />
                                        <asp:HiddenField ID="h_VATPerc" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>
