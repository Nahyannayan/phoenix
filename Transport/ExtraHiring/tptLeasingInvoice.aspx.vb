﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime
Partial Class Transport_ExtraHiring_tptLeasingInvoice
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim dtVATUserAccess As New DataTable
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200149") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtqty.Attributes.Add("onblur", " return calc('T');")
                txtRate.Attributes.Add("onblur", " return calc('T');")
                txtAddKMAmt.Attributes.Add("onblur", " return calc('T');")
                txtSalik.Attributes.Add("onblur", " return calc('T');")
                txtFuel.Attributes.Add("onblur", " return calc('T');")
                txtDiscount.Attributes.Add("onblur", " return calc('T');")
                txtAdvance.Attributes.Add("onblur", " return calc('T');")
                clearScreen()
                sqlStr = "select TLI_ID, CONVERT(VARCHAR(20),TLI_TRDATE,106) TLI_TRDATE, TLI_BSU_ID, TLI_TLC_ID, TLI_VEH_ID, TLI_VEHTYPE, TLI_QTY, TLI_RATE, TLI_AMOUNT, TLI_FUEL,"
                sqlStr &= " TLI_SALIK, TLI_ADDKMAMT,TLI_DISCOUNT,TLI_ADVANCE, TLI_TOTAL, TLI_REMARKS, TLI_NOTES, CONVERT(VARCHAR(20),TLI_CONTRACT_SDATE,106) TLI_CONTRACT_SDATE, CONVERT(VARCHAR(20),TLI_CONTRACT_EDATE,106) TLI_CONTRACT_EDATE, TLI_INVNO,"
                sqlStr &= " TLI_FYEAR, TLI_USER, TLI_POSTED, CONVERT(VARCHAR(20),TLI_POSTING_DATE,106) TLI_POSTING_DATE, TLI_JHD_DOCNO, TLI_DELETED, TLI_DATE,TLC_DESCR  CUSTOMER,TLI_LEASETYPE,case when TLI_LEASETYPE=0 then 'NES' else 'SCHOOL' end LeaseType  "
                sqlStr &= " ,isnull(case when TLI_LEASETYPE=1 then VEHICLE_M.veh_regno else TPTVEHICLE_M.veh_regno end,'') REGNO,TLI_START,TLI_ROUTE,TLI_ROUTE_ID  "
                sqlStr &= ",isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(TLI_PRGM_DIM_CODE,'0') DAX_ID,TLI_TAX_NET_AMOUNT,TLI_TAX_CODE,TLI_TAX_AMOUNT,TLI_POSTING_DATE "
                sqlStr &= ",isnull(TLI_EMR_CODE,'NA')TLI_EMR_CODE,EMR_DESCR EMIRATE,isnull(TLI_CUS_LPONO,'')TLI_CUS_LPONO "
                sqlStr &= " FROM tPTLEASINGINVOICE LEFT OUTER JOIN TPTLEASECUSTOMER ON TLC_ID=TLI_TLC_ID"
                sqlStr &= " left outer join dbo.VV_DAX_CODES on ID=TLI_PRGM_DIM_CODE "
                sqlStr &= " LEFT OUTER JOIN TPTVEHICLE_M on VEH_ID=TLI_VEH_ID LEFT OUTER JOIN VEHICLE_M on VEHICLE_M.VEH_ID=TLI_VEH_ID  "
                sqlStr &= " left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE= isnull(TLI_EMR_CODE,'NA') "
                BindBsu()
                BindDAXCodes()
                RadFilterBSU.SelectedValue = 0
                refreshGrid()

                dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")
                If dtVATUserAccess.Rows.Count > 0 Then
                    If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                        radVAT.Enabled = True
                        RadEmirate.Enabled = True
                    Else
                        radVAT.Enabled = False
                        RadEmirate.Enabled = False
                    End If
                Else
                    radVAT.Enabled = False
                    RadEmirate.Enabled = False
                End If


                CalculateVAT(RadCustomer.SelectedValue)
                SelectEmirate(RadCustomer.SelectedValue)
                If Request.QueryString("VIEWID") <> "" Then
                    h_PRI_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_PRI_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsq2 As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            strsq2 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"
            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()


            RadEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsq2)
            RadEmirate.DataTextField = "DESCR"
            RadEmirate.DataValueField = "ID"
            RadEmirate.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblVAT"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmt"), Label)

            If ChkBxItem.Checked = True Then
                Gross += GetDoubleVal(lblGross.Text)
                VAT += GetDoubleVal(lblVAT.Text)
                Net += GetDoubleVal(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAT.Text = VAT
        txtSelNet.Text = Net
    End Sub

    Private Sub refreshGrid(Optional ByVal LoadByBSU As Boolean = False)
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")

        'If LoadByBSU = True Then
        '    sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "' and TLI_TLC_ID='" & RadCustomer.SelectedValue & "'"
        'Else
        '    sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "'"
        'End If


        If rblSelection.SelectedValue = 0 Then
            If RadFilterBSU.SelectedValue = "0" Then

                sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "'  and isnull(TLI_POSTED,0)=0 ORDER BY TLI_TRDATE DESC"


            Else
                sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "' and isnull(TLI_POSTED,0)=0 and TLI_TLC_ID='" & RadFilterBSU.SelectedValue & "'"

            End If


        ElseIf rblSelection.SelectedValue = 1 Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "'  and isnull(TLI_POSTED,0)=1 ORDER BY TLI_TRDATE DESC"
            Else
                sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "' and isnull(TLI_POSTED,0)=1 and TLI_TLC_ID='" & RadFilterBSU.SelectedValue & "'"
            End If

        ElseIf rblSelection.SelectedValue = 2 Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "' ORDER BY TLI_TRDATE DESC"
            Else
                sqlWhere = " where  MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "' and TLI_TLC_ID='" & RadFilterBSU.SelectedValue & "'"
            End If
        End If
        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTLeasingInvoice = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
            Session("myData") = TPTLeasingInvoice
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try


    End Sub
    Private Sub clearScreen()
        h_PRI_ID.Value = "0"
        txtRegno.Text = ""
        txtVehicleType.Text = ""
        txtqty.Text = "0"
        txtRate.Text = "0"
        txtAmount.Text = "0"
        txtFuel.Text = "0"
        txtSalik.Text = "0"
        txtAddKMAmt.Text = "0"
        txtTotAmt.Text = "0"
        txtRemarks.Text = ""
        txtNotes.Text = ""
        txtContractSdate.Text = ""
        txtContractEdate.Text = ""
        txtDiscount.Text = "0"
        txtAdvance.Text = "0"
        txtRoute.Text = ""
        txtRouteId.Text = ""
        txtStartPoint.Text = ""
        RadDAXCodes.SelectedValue = "0"
        CalculateVAT(RadCustomer.SelectedValue)
        SelectEmirate(RadCustomer.SelectedValue)
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property
    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If h_TEI_ID.Value <> 0 Then lblError.Text &= " This month extra trips are posted already.You can not enter for this month, "

        If PurposeExists = 0 Then lblError.Text &= "Purpose is "
        If RadDAXCodes.SelectedValue = "0" Then lblError.Text &= " please select DAX Posting Code, "
        If RadEmirate.SelectedValue = "NA" Then lblError.Text &= "Please Select Emirate....!"

        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            'lblError.Visible = True
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid", UserControls_usrMessageBar.WarningType.Danger)

            Return
        End If
        Try
            Dim pParms(32) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TLI_ID", h_PRI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TLI_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@TLI_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@TLI_TLC_ID", RadCustomer.SelectedValue, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TLI_VEH_ID", hTPT_Veh_ID.Value, SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@TLI_VEHTYPE", txtVehicleType.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@TLI_QTY", txtqty.Text, SqlDbType.Float)
            pParms(8) = Mainclass.CreateSqlParameter("@TLI_RATE", txtRate.Text, SqlDbType.Float)
            pParms(9) = Mainclass.CreateSqlParameter("@TLI_AMOUNT", txtAmount.Text, SqlDbType.Float)
            pParms(10) = Mainclass.CreateSqlParameter("@TLI_FUEL", txtFuel.Text, SqlDbType.Float)
            pParms(11) = Mainclass.CreateSqlParameter("@TLI_SALIK", txtSalik.Text, SqlDbType.Float)
            pParms(12) = Mainclass.CreateSqlParameter("@TLI_ADDKMAMT", txtAddKMAmt.Text, SqlDbType.Float)
            pParms(13) = Mainclass.CreateSqlParameter("@TLI_TOTAL", txtTotAmt.Text, SqlDbType.Float)
            pParms(14) = Mainclass.CreateSqlParameter("@TLI_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@TLI_NOTES", txtNotes.Text, SqlDbType.VarChar)
            pParms(16) = Mainclass.CreateSqlParameter("@TLI_CONTRACT_SDATE", txtContractSdate.Text, SqlDbType.SmallDateTime)
            pParms(17) = Mainclass.CreateSqlParameter("@TLI_CONTRACT_EDATE", txtContractEdate.Text, SqlDbType.SmallDateTime)
            pParms(18) = Mainclass.CreateSqlParameter("@TLI_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(19) = Mainclass.CreateSqlParameter("@TLI_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(20) = Mainclass.CreateSqlParameter("@TLI_LEASETYPE", rblData.Text, SqlDbType.VarChar)
            pParms(21) = Mainclass.CreateSqlParameter("@TLI_DISCOUNT", txtDiscount.Text, SqlDbType.Float)
            pParms(22) = Mainclass.CreateSqlParameter("@TLI_ADVANCE", txtAdvance.Text, SqlDbType.Float)
            pParms(23) = Mainclass.CreateSqlParameter("@TLI_START", txtStartPoint.Text, SqlDbType.VarChar)
            pParms(24) = Mainclass.CreateSqlParameter("@TLI_ROUTE", txtRoute.Text, SqlDbType.VarChar)
            pParms(25) = Mainclass.CreateSqlParameter("@TLI_ROUTE_ID", txtRouteId.Text, SqlDbType.VarChar)
            pParms(26) = Mainclass.CreateSqlParameter("@TLI_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
            pParms(27) = Mainclass.CreateSqlParameter("@TLI_TAX_CODE", radVAT.SelectedValue, SqlDbType.VarChar)
            pParms(28) = Mainclass.CreateSqlParameter("@TLI_TAX_AMOUNT", txtVATAmount.Text, SqlDbType.Float)
            pParms(29) = Mainclass.CreateSqlParameter("@TLI_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
            pParms(30) = Mainclass.CreateSqlParameter("@TLI_EMR_CODE", RadEmirate.SelectedValue, SqlDbType.VarChar)
            pParms(31) = Mainclass.CreateSqlParameter("@TLI_CUS_LPONO", txtLPO.Text, SqlDbType.VarChar)


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTLEASINGINVOICE", pParms)
                If RetVal = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ID, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                        clearScreen()
                        refreshGrid()
                        'lblError.Text = "Data Saved Successfully !!!"
                        usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub

    Private Sub showEdit(ByVal showId As Integer)
        Dim rdrHireCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & " where TLI_ID='" & showId & "'")

        If rdrHireCharges.HasRows Then
            rdrHireCharges.Read()
            h_PRI_ID.Value = rdrHireCharges("TLI_ID")
            hTPT_Veh_ID.Value = rdrHireCharges("TLI_VEH_ID")
            rblData.Text = rdrHireCharges("TLI_LEASETYPE")
            txtDate.Text = rdrHireCharges("TLI_TRDATE")
            RadCustomer.SelectedItem.Text = rdrHireCharges("CUSTOMER")
            RadCustomer.SelectedValue = rdrHireCharges("TLI_TLC_ID")
            txtRegno.Text = rdrHireCharges("REGNO")
            txtVehicleType.Text = rdrHireCharges("TLI_VEHTYPE")
            txtqty.Text = rdrHireCharges("TLI_QTY")
            txtRate.Text = rdrHireCharges("TLI_RATE")
            txtAmount.Text = rdrHireCharges("TLI_AMOUNT")
            txtAddKMAmt.Text = rdrHireCharges("TLI_ADDKMAMT")
            txtSalik.Text = rdrHireCharges("TLI_SALIK")
            txtFuel.Text = rdrHireCharges("TLI_FUEL")
            txtTotAmt.Text = rdrHireCharges("TLI_TOTAL")
            txtRemarks.Text = rdrHireCharges("TLI_REMARKS")
            txtNotes.Text = rdrHireCharges("TLI_NOTES")
            txtContractEdate.Text = rdrHireCharges("TLI_CONTRACT_EDATE")
            txtContractSdate.Text = rdrHireCharges("TLI_CONTRACT_SDATE")
            txtDiscount.Text = rdrHireCharges("TLI_DISCOUNT")
            txtAdvance.Text = rdrHireCharges("TLI_ADVANCE")
            RadDAXCodes.SelectedItem.Text = rdrHireCharges("DAX_DESCR")
            RadDAXCodes.SelectedValue = rdrHireCharges("DAX_ID")
            txtVATAmount.Text = rdrHireCharges("TLI_TAX_AMOUNT")
            txtNetAmount.Text = rdrHireCharges("TLI_TAX_NET_AMOUNT")
            radVAT.SelectedValue = rdrHireCharges("TLI_TAX_CODE")
            txtStartPoint.Text = rdrHireCharges("TLI_START")
            txtRoute.Text = rdrHireCharges("TLI_ROUTE")
            txtRouteId.Text = rdrHireCharges("TLI_ROUTE_ID")
            txtLPO.Text = rdrHireCharges("TLI_CUS_LPONO")

            Dim lstDrp As New RadComboBoxItem
            lstDrp = RadEmirate.Items.FindItemByValue(rdrHireCharges("TLI_EMR_CODE"))
            If Not lstDrp Is Nothing Then
                RadEmirate.SelectedValue = lstDrp.Value
            Else
                RadEmirate.SelectedValue = "NA"
            End If
            CalculateVAT(RadCustomer.SelectedValue)

        End If
        rdrHireCharges.Close()
        rdrHireCharges = Nothing
    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub
    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(29).Text = "&nbsp;" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(32).Enabled = True
                gvr.Cells(33).Enabled = False
                gvr.Cells(34).Enabled = False
            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
                gvr.Cells(32).Enabled = False
                gvr.Cells(33).Enabled = True
                gvr.Cells(34).Enabled = True
            End If
        Next
    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Leasing Invoice Details")
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTHCID"), Label)

        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  tPTLEASINGINVOICE set TLI_DELETED=1,TLI_USER='" & Session("sUsr_name") & "',TLI_DATE=GETDATE() where TLI_ID=" & lblID.Text)
        refreshGrid()
    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTLI_invNo"), Label)

        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            Dim ClientId As String = ""
            cmd.CommandText = "rptTPTLeaseInvoice"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TLI_INVOICE", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@TLI_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            ClientId = Mainclass.getDataValue("select TLC_ACT_ID from TPTLEASECUSTOMER where TLC_ID in(select distinct tli_tlc_id from tptleasinginvoice where tli_invno='" & lblID.Text & "' and tli_bsu_id='" & Session("sBsuid") & "')", "OASIS_TRANSPORTConnectionString")
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            If ClientId = "242H0003" Then ' For HSBC
                'If Format(CDate(lblPOstingDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
                If CDate(lblPOstingDate.Text) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTLeasingInvoiceHSBC.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/Reports/rptTPTLeasingInvoiceHSBC.rpt"
                End If
            Else
                'If Format(CDate(lblPOstingDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
                If CDate(lblPOstingDate.Text) <= CDate("31/Dec/2017") Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTLeasingInvoice.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTLeasingInvoice.rpt"
                End If
            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            ' Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/../Reports/ASPX Report/rptViewerNew.aspx');", True)

        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/../Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If

    End Sub
    Protected Sub lnkEmail_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblLI_invNo"), Label)
        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Dim bEmailSuccess As Boolean
        SendEmail(CType(lblID.Text, String), CDate(lblPOstingDate.Text), bEmailSuccess)
        If bEmailSuccess = True Then
            'lblError.Text = "Email send  Sucessfully...!"
            usrMessageBar.ShowNotification("Email send  Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
        Else
            'lblError.Text = "Error while sending email.."
            usrMessageBar.ShowNotification("Error while sending email..", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByVal InvDate As DateTime, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""
        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        Dim ClientId As String = ""
        'ClientId = Mainclass.getDataValue("select TLC_ACT_ID from TPTLEASECUSTOMER where TLC_ID in(select distinct tli_tlc_id from tptleasinginvoice where tli_invno='" & RecNo & "')", "OASIS_TRANSPORTConnectionString")
        'commented on 28-Jun-2020
        ClientId = Mainclass.getDataValue("select TLC_ACT_ID from TPTLEASECUSTOMER where TLC_ID in(select distinct tli_tlc_id from tptleasinginvoice where TLI_BSU_ID='" & Session("sBsuId") & "' and tli_invno='" & RecNo & "')", "OASIS_TRANSPORTConnectionString")
        param("@TLI_INVOICE") = RecNo
        param("@TLI_BSU_ID") = Session("sBsuid")
        param.Add("userName", "SYSTEM")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        'param.Add("@IMG_TYPE", "LOGO")
        If ClientId = "242H0003" Then ' For HSBC
            'If Format(CDate(InvDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(InvDate) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTLeasingInvoiceHSBCEmail.rpt")
            Else
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTLeasingInvoiceHSBCEmailTAX.rpt")
            End If
        Else
            'If Format(CDate(InvDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(InvDate) <= CDate("31/Dec/2017") Then
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTLeasingInvoiceEmail.rpt")
            Else
                RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTLeasingInvoiceEmailTAX.rpt")
            End If
        End If

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = RecNo
        rptDownload.InvType = "LEASING"
        rptDownload.FCO_ID = 0
        'UtilityObj.Errorlog("Calling Loadreports", "BBT_TEST")
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & RecNo & "','" & FCL_ID & "','" & SendEmail & "' ")
        End If
    End Function
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid(True)
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select max(tlc_id) TLC_ID ,TLC_DESCR  from TPTLEASECUSTOMER where TLC_BSU_ID='" & Session("sBsuid") & "' group by tlc_descr union all select 0,'-----ALL-----' order by TLC_DESCR"
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            RadCustomer.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            radVAT.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            RadCustomer.DataTextField = "TLC_DESCR"
            RadCustomer.DataValueField = "TLC_ID"
            RadCustomer.DataBind()

            RadFilterBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadFilterBSU.DataTextField = "TLC_DESCR"
            RadFilterBSU.DataValueField = "TLC_ID"
            RadFilterBSU.DataBind()

            radVAT.DataTextField = "DESCR"
            radVAT.DataValueField = "ID"
            radVAT.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click, btnPost1.Click
        lblError.Text = ""
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox
        Dim PartyCode As String = ""
        Dim i As Integer = 0
        Dim mrow As GridViewRow
        Dim SelectedIDs As String = ""
        Dim NotSameBsu As Boolean = False
        Dim PurposeExists As Integer = 1
        If txtPostingDate.Text.Trim = "" Then lblError.Text &= "Please select Posting Date,"
        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " "
            lblError.Visible = True
            Return
        End If
        For Each mrow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(mrow.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(mrow.FindControl("lblTHCID"), Label)

            If ChkBxItem.Checked = True Then

                If i = 0 Then
                    PartyCode = mrow.Cells(3).Text
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                Else
                    If PartyCode = mrow.Cells(3).Text Then
                        IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    Else
                        NotSameBsu = True
                    End If
                End If
                i = i + 1
            End If
        Next
        If NotSameBsu = False Then
            PostLeasingInvoice(IDs)
            'lblError.Text = "Data Posted Successfully !!!"
            usrMessageBar.ShowNotification("Data Posted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            refreshGrid()
        Else
            'lblError.Text = "Selected business units are  not same!!!"
            usrMessageBar.ShowNotification("Selected business units are  not same!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
    End Sub
    Private Sub PostLeasingInvoice(ByVal id As String)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(6) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", id, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@POSTINGDATE", txtPostingDate.Text, SqlDbType.DateTime)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostDriverCharges", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                stTrans.Commit()
            End If

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, id, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Property TPTLeasingInvoice() As DataTable
        Get
            Return ViewState("TPTLeasingInvoice")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTLeasingInvoice") = value
        End Set
    End Property
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click, btnCopy1.Click
        CopyDriverChargeDetails()
    End Sub

    Private Sub CopyDriverChargeDetails()

        Dim NewDate As Date
        Dim Newdate1 As Date

        NewDate = Format(CDate(txtDate.Text.ToString), "dd/MMM/yyyy")
        Newdate1 = NewDate.AddMonths(1)
        Newdate1 = New Date(NewDate.AddMonths(1).Year, NewDate.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1)
        Newdate1 = Format(CDate(Newdate1), "dd/MMM/yyyy")
        Dim CParms(4) As SqlClient.SqlParameter

        CParms(0) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
        CParms(0).Value = NewDate
        CParms(1) = New SqlClient.SqlParameter("@DATE1", SqlDbType.DateTime)
        CParms(1).Value = Newdate1

        CParms(2) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar, 2000)
        CParms(2).Value = "LEASE"
        CParms(3) = New SqlClient.SqlParameter("@BSU", SqlDbType.VarChar, 6)
        CParms(3).Value = Session("sBsuid")

        CParms(4) = New SqlClient.SqlParameter("@Errormsg", SqlDbType.VarChar, 1000)
        CParms(4).Direction = ParameterDirection.Output

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "CopyInvoiceDetails", CParms)
            Dim ErrorMSG As String = CParms(4).Value.ToString()


            If ErrorMSG = "" Then
                stTrans.Commit()
                usrMessageBar.ShowNotification("Copied Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                txtDate.Text = Newdate1.ToString("dd/MMM/yyyy")
                refreshGrid()

            Else
                usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
                refreshGrid()
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
            refreshGrid()

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub


    Protected Sub RadFilterBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadFilterBSU.SelectedIndexChanged
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'sqlWhere = " where  YEAR(TLI_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TLI_TRDATE)= MONTH('" & txtDate.Text & "') AND TLI_DELETED=0 and TLI_BSU_ID='" & Session("sBsuid") & "' and TLI_FYEAR='" & Session("F_YEAR") & "' and TLI_TLC_ID ='" & RadFilterBSU.SelectedValue & "' "
        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTLeasingInvoice = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTLeasingInvoice
        '    SetExportFileLink()
        '    txtSelectedAmt.Text = "0.00"

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try

        refreshGrid()

    End Sub


    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)

            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                Dim lblVAT As Label = TryCast(gvr.FindControl("lblVAT"), Label)
                Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmt"), Label)

                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    Gross += GetDoubleVal(lblGross.Text)
                    VAT += GetDoubleVal(lblVAT.Text)
                    Net += GetDoubleVal(lblNet.Text)
                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
                VAT = 0
                Net = 0
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAT.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Protected Sub rblData_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblData.TextChanged
        txtRegno.Text = ""
        hTPT_Veh_ID.Value = 0
    End Sub
    Protected Sub GridViewShowDetails_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridViewShowDetails.SelectedIndexChanged
    End Sub
    Private Sub CalculateVAT(ByVal Act_id As String)
        Dim dt As New DataTable
        Dim ACT_CODE As String = ""
        ACT_CODE = Mainclass.getDataValue("select distinct TLC_ACT_ID from TPTLEASECUSTOMER where tlc_ID='" & Act_id & "'", "OASIS_TRANSPORTConnectionString")
        dt = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','LEASE_INV','" & txtDate.Text & "'," & txtTotAmt.Text & ",'" & ACT_CODE & "')", "OASIS_TRANSPORTConnectionString")
        If dt.Rows.Count > 0 Then
            Dim lstDrp As New RadComboBoxItem
            lstDrp = radVAT.Items.FindItemByValue(dt.Rows(0)("tax_code"))
            h_VATPerc.Value = dt.Rows(0)("tax_perc_value")


            If Not lstDrp Is Nothing Then
                radVAT.SelectedValue = lstDrp.Value
            Else
                radVAT.SelectedValue = 0
            End If
            txtVATAmount.Text = (((GetDoubleVal(txtTotAmt.Text) - GetDoubleVal(txtSalik.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")

            txtNetAmount.Text = (GetDoubleVal(txtTotAmt.Text) + GetDoubleVal(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub

    Public Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function

    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged
        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVAT.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            txtVATAmount.Text = (((GetDoubleVal(txtTotAmt.Text) + GetDoubleVal(txtAdvance.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(GetDoubleVal(txtTotAmt.Text) + GetDoubleVal(txtVATAmount.Text)).ToString("####0.000")
        Else
            radVAT.SelectedValue = 0
            h_VATPerc.Value = radVAT.SelectedValue
            txtVATAmount.Text = (((GetDoubleVal(txtTotAmt.Text) + GetDoubleVal(txtAdvance.Text)) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(GetDoubleVal(txtTotAmt.Text) + GetDoubleVal(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub RadCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadCustomer.SelectedIndexChanged
        CalculateVAT(RadCustomer.SelectedValue)
        SelectEmirate(RadCustomer.SelectedValue)
    End Sub
    Private Sub SelectEmirate(ByVal ACT_ID As String)
        Dim bsu_City As String = ""
        bsu_City = Mainclass.getDataValue("SELECT isnull(case when case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end end,'NA')   EMR_CODE   FROM OASISFIN..accounts_M left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE=ACT_EMR_CODE where act_id in(select distinct tlc_act_id from TPTLEASECUSTOMER where tlc_id='" & ACT_ID & "') ", "OASIS_TRANSPORTConnectionString")
        Dim lstDrp As New RadComboBoxItem
        lstDrp = RadEmirate.Items.FindItemByValue(bsu_City)

        If Not lstDrp Is Nothing Then
            RadEmirate.SelectedValue = lstDrp.Value
        Else
            RadEmirate.SelectedValue = "NA"
        End If
    End Sub

    Protected Sub rblSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSelection.SelectedIndexChanged
        refreshGrid()

    End Sub
End Class



















