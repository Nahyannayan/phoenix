﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Transport_tptContractRate
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200115" And ViewState("MainMnu_code") <> "T200130" And ViewState("MainMnu_code") <> "T200140") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If
                hGridRefresh.Value = 0
                bindlist()
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    'UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        txtCnb_RatePerHr.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtCnb_RatePerKm.Attributes.Add("onkeypress", " return Numeric_Only();")

        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        txtCnb_Date.Enabled = EditAllowed
        txtCnb_Effective_Date.Enabled = EditAllowed
        txtMinimum.Enabled = EditAllowed
        txtCnb_Print.Enabled = EditAllowed
        ddlBusType.Enabled = EditAllowed
        imgClient.Enabled = EditAllowed
        imgCnb_Date.Enabled = EditAllowed
        imgCnb_Effective_Date.Enabled = EditAllowed
        txtCnb_RatePerKm.Enabled = EditAllowed
        txtMinimum.Enabled = EditAllowed
        txtCnb_RatePerKm.Enabled = EditAllowed
        txtCnb_RatePerHr.Enabled = EditAllowed



        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnCopy.Visible = mDisable
        btnDelete.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select TPTCONTRACT_B.*, replace(convert(varchar(30), Cnb_Date, 106),' ','/') CnbDATE, BSU_NAME ClientName, ")
                'strSql.Append("replace(convert(varchar(30), Cnb_Effective_Date, 106),' ','/') CnbEffectiveDATE,CAT_DESCRIPTION ")
                strSql.Append("replace(convert(varchar(30), Cnb_Effective_Date, 106),' ','/') CnbEffectiveDATE ")
                'strSql.Append("from TPTCONTRACT_B inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=CNB_BSU_ID inner JOIN CATEGORY_M on CAT_ID=CNB_CAT_ID  ")
                strSql.Append("from TPTCONTRACT_B inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=CNB_BSU_ID ")
                strSql.Append("where Cnb_ID=" & h_EntryId.Value)

                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtCnb_Id.Text = dt.Rows(0)("Cnb_numberstr")
                    txtCnb_Date.Text = dt.Rows(0)("CnbDATE")
                    txtCnb_Effective_Date.Text = dt.Rows(0)("CnbEffectiveDATE")
                    txtClientName.Text = dt.Rows(0)("ClientName")
                    txtCnb_RatePerHr.Text = dt.Rows(0)("Cnb_RateHr")
                    txtCnb_RatePerKm.Text = dt.Rows(0)("Cnb_RateKm")
                    txtClientCode.Text = dt.Rows(0)("Cnb_BSU_ID")
                    txtClientName.Text = dt.Rows(0)("ClientName")
                    'Mahesh
                    txtCnb_Print.Text = dt.Rows(0)("CNB_PRINT")
                    'ddlBusType.SelectedItem.Text = dt.Rows(0)("CAT_DESCRIPTION")
                    If dt.Rows(0)("CNB_CAT_ID") = "MB" Then
                        ddlBusType.SelectedItem.Text = "MEDIUM BUS"
                    ElseIf dt.Rows(0)("CNB_CAT_ID") = "BB" Then
                        ddlBusType.SelectedItem.Text = "BIG BUS"
                    Else
                        ddlBusType.SelectedItem.Text = "VAN"
                    End If
                    txtMinimum.Text = dt.Rows(0)("CNB_MINRATE")

                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ClearDetails()
        'txtCnb_Id.Visible = False
        txtCnb_Date.Text = Now.ToString("dd/MMM/yyyy")
        txtCnb_Effective_Date.Text = Now.ToString("dd/MMM/yyyy")
        txtCnb_Id.Text = ""
        txtClientName.Text = ""
        txtCnb_RatePerHr.Text = "0"
        txtCnb_RatePerKm.Text = "0"
        txtCnb_Print.Text = ""
        txtClientCode.Text = ""
        txtMinimum.Text = "0.00"
        'ddlBusType.SelectedItem.Text = "NONE"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT count(*) FROM TPTHiring inner JOIN TPTHiringInvoice on TRN_no=INV_TRN_NO inner JOIN TPTCONTRACT_B on CNB_ID=inv_cn_id WHERE CNB_ID=" & h_EntryId.Value & " and TPT_JobType=0") > 0 Then
                lblError.Text = "contract in use, unable to delete"
            Else
                Dim objConn As New SqlConnection(connectionString)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim sqlStr As String = "delete from TPTCONTRACT_B where Cnb_ID=@TRNO"
                    Dim pParms(1) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@TRNO", h_EntryId.Value, SqlDbType.Int, True)
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                    stTrans.Commit()
                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                    Exit Sub
                Finally
                    objConn.Close()
                End Try
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                'If flagAudit <> 0 Then
                '    Throw New ArgumentException("Could not process your request")
                'End If
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        If txtCnb_Date.Text.Trim.Length = 0 Then strMandatory.Append("Date,")
        'If hTPT_Client_ID.Value = "0" Then strMandatory.Append("Client's Name,")
        If txtCnb_Effective_Date.Text.Trim.Length = 0 Then strMandatory.Append("Effective Date,")
        If Not IsDate(txtCnb_Date.Text) Then strError.Append("invalid date,")
        If Not IsDate(txtCnb_Effective_Date.Text) Then strError.Append("invalid effective date,")
        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(9) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@Cnb_id", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@Cnb_date", txtCnb_Date.Text, SqlDbType.SmallDateTime)
        pParms(3) = Mainclass.CreateSqlParameter("@Cnb_Effective_Date", txtCnb_Effective_Date.Text, SqlDbType.SmallDateTime)
        pParms(4) = Mainclass.CreateSqlParameter("@Cnb_BSU_id", txtClientCode.Text, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@Cnb_Print", txtCnb_Print.Text, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@Cnb_RateHr", txtCnb_RatePerHr.Text, SqlDbType.Float)
        pParms(7) = Mainclass.CreateSqlParameter("@Cnb_RateKm", txtCnb_RatePerKm.Text, SqlDbType.Float)
        pParms(8) = Mainclass.CreateSqlParameter("@CNB_CAT_ID", ddlBusType.SelectedItem.Value, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@CNB_MINRATE", txtMinimum.Text, SqlDbType.Float)
        'If ddlBusType.SelectedItem.Value = "MB" Then
        '    pParms(8) = Mainclass.CreateSqlParameter("@CNB_CAT_ID", 1, SqlDbType.Float)
        'ElseIf ddlBusType.SelectedItem.Value = "BB" Then
        '    pParms(8) = Mainclass.CreateSqlParameter("@CNB_CAT_ID", 2, SqlDbType.Float)
        'Else
        '    pParms(8) = Mainclass.CreateSqlParameter("@CNB_CAT_ID", 0, SqlDbType.Float)
        'End If



        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTContract_b", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Function ValidTPTRates(ByVal txtCnd_Rate As String, ByVal txtCnd_Hrs As String, ByVal txtCnd_Km As String) As String
        ValidTPTRates = ""
        If txtCnd_Rate.Length = 0 Then ValidTPTRates &= "Rate"
        If txtCnd_Hrs.Length = 0 Then ValidTPTRates &= IIf(ValidTPTRates.Length = 0, "", ",") & "Qty"
        If txtCnd_Km.Length = 0 Then ValidTPTRates &= IIf(ValidTPTRates.Length = 0, "", ",") & "Km"
    End Function

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        h_EntryId.Value = 0
        txtCnb_Id.Text = ""
        txtClientName.Text = ""
        txtClientCode.Text = ""
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Private Sub bindlist()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            'Dim str_Sql As String = "select '0' cat_id,'NONE' CAT_DESCRIPTION union all select cat_id,CAT_DESCRIPTION FROM CATEGORY_M"
            Dim str_Sql As String = "SELECT 'BB' CAT_ID, 'BIG BUS' CAT_DESCRIPTION  UNION ALL SELECT 'MB' CAT_ID, 'MEDIUM BUS' CAT_DESCRIPTION UNION ALL SELECT 'VAN' CAT_ID, 'VAN' CAT_DESCRIPTION"
            ddlBusType.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlBusType.DataTextField = "CAT_DESCRIPTION"
            ddlBusType.DataValueField = "CAT_ID"
            ddlBusType.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
