﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime
Partial Class Transport_ExtraHiring_tptIDCardCharges
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim dtVATUserAccess As New DataTable

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200210") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtqty.Attributes.Add("onblur", " return calcTotal();")
                txtPrice.Attributes.Add("onblur", " return calcTotal();")
                txtDiscount.Attributes.Add("onblur", " return calcTotal();")

                clearScreen()
                sqlStr = "select TIC_ID,CONVERT(VARCHAR(20),TIC_TRDATE,106) TIC_TRDATE, TIC_BSU_ID, TIC_TCT_ID,TIC_TYPE, case when TIC_TYPE=0 then 'Student' else 'Staff'  end TIC_TYPE1, TIC_QTY, TIC_RATE, TIC_AMOUNT, TIC_DISCOUNT, TIC_TOTAL, TIC_REMARKS, TIC_INVNO, TIC_FYEAR, TIC_USER, TIC_POSTED, TIC_POSTING_DATE, TIC_JHD_DOCNO, TIC_DELETED, TIC_DATE, case when TIC_type=0 then 'Student' else 'Staff' end [TIC_TYPE],TCT_DESCR,TCT_REMARKS,BSU_NAME Customer,TIC_CTO_BSU_ID "
                sqlStr &= ",isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(TIC_PRGM_DIM_CODE,'0') DAX_ID,TIC_TAX_AMOUNT,TIC_TAX_NET_AMOUNT,TIC_TAX_CODE,TIC_POSTING_DATE,isnull(TIC_EMR_CODE,'NA')TIC_EMR_CODE,EMR_DESCR EMIRATE "
                sqlStr &= " FROM TPTIDCARDCHARGES LEFT OUTER JOIN OASIS..BUSINESSUNIT_M ON  BSU_ID=TIC_CTO_BSU_ID"
                sqlStr &= " left outer join dbo.VV_DAX_CODES on ID=TIC_PRGM_DIM_CODE "
                sqlStr &= " LEFT OUTER JOIN TPTCARDTYPE_M on TCT_ID=TIC_TCT_ID  "
                sqlStr &= " left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE= isnull(TIC_EMR_CODE,'NA') "

                BindBsu()
                BindDAXCodes()
                BindCardtype()
                RadFilterBSU.SelectedValue = 0
                refreshGrid()
                dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")
                If dtVATUserAccess.Rows.Count > 0 Then
                    If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                        radVAT.Enabled = True
                        RadEmirate.Enabled = True
                    Else
                        radVAT.Enabled = False
                        RadEmirate.Enabled = False
                    End If
                Else
                    radVAT.Enabled = False
                    RadEmirate.Enabled = False
                End If
                CalculateVAT(RadCustomer.SelectedValue)
                SelectEmirate(RadCustomer.SelectedValue)

                If Request.QueryString("VIEWID") <> "" Then
                    h_PRI_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_PRI_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)

        End Try
    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            Dim strsql3 As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "
            strsql3 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"

            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

            radVAT.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            radVAT.DataTextField = "DESCR"
            radVAT.DataValueField = "ID"
            radVAT.DataBind()

            RadEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql3)
            RadEmirate.DataTextField = "DESCR"
            RadEmirate.DataValueField = "ID"
            RadEmirate.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0

        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblVATAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmt"), Label)

            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                VAT += Val(lblVAT.Text)
                Net += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAT.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Private Sub refreshGrid()
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND YEAR(TIC_TRDATE)= YEAR('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "'"
        'sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH(convert(date,'" & txtDate.Text & "',105)) AND YEAR(TIC_TRDATE)= YEAR(convert(date,'" & txtDate.Text & "',105)) AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "'"

        If rblSelection.SelectedValue = 0 Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "' and isnull(TIC_POSTED,0)=0 "

            Else
                sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "' and isnull(TIC_POSTED,0)=0 and TIC_CTO_BSU_ID='" & RadFilterBSU.SelectedValue & "'"
            End If
        ElseIf rblSelection.SelectedValue = "1" Then

            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "' and isnull(TIC_POSTED,0)=1 "
            Else
                sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "' and isnull(TIC_POSTED,0)=1 and TIC_CTO_BSU_ID='" & RadFilterBSU.SelectedValue & "'"
            End If

        ElseIf rblSelection.SelectedValue = "2" Then
            If RadFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "' "
            Else
                sqlWhere = " where  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "' and TIC_CTO_BSU_ID='" & RadFilterBSU.SelectedValue & "'"
            End If

        End If




        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTIDCardCharges = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
            Session("myData") = TPTIDCardCharges
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub clearScreen()
        h_PRI_ID.Value = "0"
        txtqty.Text = "0"
        txtPrice.Text = "0"
        txtAmount.Text = "0"
        txtDiscount.Text = "0"
        txtTotal.Text = "0"
        txtRemarks.Text = ""
        txtCardType.Text = ""
        RadDAXCodes.SelectedValue = "0"
        txtVATAmount.Text = "0"
        txtNetAmount.Text = "0"

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property
    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        'If h_TEI_ID.Value <> 0 Then lblError.Text &= " This month extra trips are posted already.You can not enter for this month, "

        If RadDAXCodes.SelectedValue = "0" Then lblError.Text &= "Please Select DAX Posting Code, "
        If RadEmirate.SelectedValue = "NA" Then lblError.Text &= "Please Select Emirate....!"
        

        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            'lblError.Visible = True

            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Try
            Dim pParms(19) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TIC_ID", h_PRI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TIC_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@TIC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@TIC_TCT_ID", hCARDID.Value, SqlDbType.Int)
            pParms(5) = Mainclass.CreateSqlParameter("@TIC_TYPE", rblType.SelectedValue, SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@TIC_CTO_BSU_ID", RadCustomer.SelectedValue, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@TIC_QTY", txtqty.Text, SqlDbType.Float)
            pParms(8) = Mainclass.CreateSqlParameter("@TIC_RATE", txtPrice.Text, SqlDbType.Float)
            pParms(9) = Mainclass.CreateSqlParameter("@TIC_AMOUNT", txtAmount.Text, SqlDbType.Float)
            pParms(10) = Mainclass.CreateSqlParameter("@TIC_DISCOUNT", txtDiscount.Text, SqlDbType.Float)
            pParms(11) = Mainclass.CreateSqlParameter("@TIC_TOTAL", txtTotal.Text, SqlDbType.Float)
            pParms(12) = Mainclass.CreateSqlParameter("@TIC_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@TIC_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@TIC_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@TIC_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
            pParms(16) = Mainclass.CreateSqlParameter("@TIC_TAX_AMOUNT", txtVATAmount.Text, SqlDbType.Float)
            pParms(17) = Mainclass.CreateSqlParameter("@TIC_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
            pParms(18) = Mainclass.CreateSqlParameter("@TIC_TAX_CODE", radVAT.SelectedValue, SqlDbType.VarChar)
            pParms(19) = Mainclass.CreateSqlParameter("@TIC_EMR_CODE", RadEmirate.SelectedValue, SqlDbType.VarChar)



            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTIDCARDCHARGES", pParms)
                If RetVal = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    clearScreen()
                    refreshGrid()
                    'lblError.Text = "Data Saved Successfully !!!"
                    usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub

    Private Sub showEdit(ByVal showId As Integer)
        Dim rdrIDCARDCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & " where TIC_ID='" & showId & "'")

        If rdrIDCARDCharges.HasRows Then
            rdrIDCARDCharges.Read()
            h_PRI_ID.Value = rdrIDCARDCharges("TIC_ID")
            hCARDID.Value = rdrIDCARDCharges("TIC_TCT_ID")
            txtCardType.Text = rdrIDCARDCharges("TCT_DESCR")
            rblType.Text = rdrIDCARDCharges("TIC_TYPE")
            txtDate.Text = rdrIDCARDCharges("TIC_TRDATE")
            RadCustomer.SelectedItem.Text = rdrIDCARDCharges("CUSTOMER")
            RadCustomer.SelectedValue = rdrIDCARDCharges("TIC_CTO_BSU_ID")
            txtqty.Text = rdrIDCARDCharges("TIC_QTY")
            txtPrice.Text = rdrIDCARDCharges("TIC_RATE")
            txtAmount.Text = rdrIDCARDCharges("TIC_AMOUNT")
            txtDiscount.Text = rdrIDCARDCharges("TIC_DISCOUNT")
            txtTotal.Text = rdrIDCARDCharges("TIC_TOTAL")
            txtRemarks.Text = rdrIDCARDCharges("TIC_REMARKS")
            RadDAXCodes.SelectedItem.Text = rdrIDCARDCharges("DAX_DESCR")
            RadDAXCodes.SelectedValue = rdrIDCARDCharges("DAX_ID")
            BindDAXCodes()
            txtVATAmount.Text = rdrIDCARDCharges("TIC_TAX_AMOUNT")
            txtNetAmount.Text = rdrIDCARDCharges("TIC_TAX_NET_AMOUNT")
            radVAT.SelectedValue = rdrIDCARDCharges("TIC_TAX_CODE")
            Dim lstDrp As New RadComboBoxItem
            lstDrp = RadEmirate.Items.FindItemByValue(rdrIDCARDCharges("TIC_EMR_CODE"))
            If Not lstDrp Is Nothing Then
                RadEmirate.SelectedValue = lstDrp.Value
            Else
                RadEmirate.SelectedValue = "NA"
            End If

        End If
        rdrIDCARDCharges.Close()
        rdrIDCARDCharges = Nothing
    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub
    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(17).Text = "&nbsp;" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(20).Enabled = True
                gvr.Cells(21).Enabled = False
                gvr.Cells(22).Enabled = False
            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False

                gvr.Cells(20).Enabled = False
                gvr.Cells(21).Enabled = True
                gvr.Cells(22).Enabled = True
            End If
        Next
    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Vehicle Charges")
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTIC_ID"), Label)
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTIDCARDCHARGES set TIC_DELETED=1,TIC_USER='" & Session("sUsr_name") & "',TIC_DATE=GETDATE() where TIC_ID=" & lblID.Text)
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        refreshGrid()
    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTIC_INVNO"), Label)

        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            Dim ClientId As String = ""
            cmd.CommandText = "rptTPTIDCardInvoice"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TIC_INVOICE", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@TIC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim params As New Hashtable
            params("reportcaption") = "Id Card Charges Invoice"
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params

            'If Format(CDate(lblPOstingDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(lblPOstingDate.Text) <= CDate("31/Dec/2017") Then
                repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTIdCardInvoice.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTIdCardInvoice.rpt"
            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/../Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
            'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/../Reports/ASPX Report/rptViewerNew.aspx');", True)
        End If
    End Sub
    Protected Sub lnkEmail_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTIC_INVNO"), Label)

        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Dim bEmailSuccess As Boolean
        SendEmail(CType(lblID.Text, String), CDate(lblPOstingDate.Text), bEmailSuccess)
        If bEmailSuccess = True Then
            lblError.Text = "Email send  Sucessfully...!"
            usrMessageBar.ShowNotification("Email send  Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
        Else
            'lblError.Text = "Error while sending email.."
            usrMessageBar.ShowNotification("Error while sending email..", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByVal InvDate As DateTime, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""
        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        'Dim ClientId As String = ""
        'ClientId = Mainclass.getDataValue("select TLC_ACT_ID from TPTLEASECUSTOMER where TLC_ID in(select distinct tli_tlc_id from tptleasinginvoice where tli_invno='" & RecNo & "')", "OASIS_TRANSPORTConnectionString")
        param("@TIC_INVOICE") = RecNo
        param("@TIC_BSU_ID") = Session("sBsuid")
        param("reportcaption") = "Id Card Charges Invoice"
        param.Add("userName", "SYSTEM")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))

        'If Format(CDate(InvDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
        If CDate(InvDate) <= CDate("31/Dec/2017") Then
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTIdCardInvoiceEmail.rpt")
        Else
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTIdCardInvoiceEmailTAX.rpt")
        End If
        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = RecNo
        rptDownload.InvType = "IDCARD"
        rptDownload.FCO_ID = 0
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & RecNo & "','" & FCL_ID & "','" & SendEmail & "' ")
        End If
    End Function

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid()
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim BSUParms(2) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            RadCustomer.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForDriverANDHirecharges", BSUParms)
            RadFilterBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetUnitsForDriverANDHirecharges", BSUParms)
            RadCustomer.DataTextField = "BSU_NAME"
            RadCustomer.DataValueField = "SVB_BSU_ID"
            RadFilterBSU.DataTextField = "BSU_NAME"
            RadFilterBSU.DataValueField = "SVB_BSU_ID"
            RadCustomer.DataBind()
            RadFilterBSU.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindCardtype()
        Try
            txtCardType.Text = ""
            hCARDID.Value = "0.0"
            txtPrice.Text = "0.0"
            txtDiscount.Text = "0.0"
            txtNetAmount.Text = "0.0"
            CalculateVAT(RadCustomer.SelectedValue)
            SelectEmirate(RadCustomer.SelectedValue)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click, btnPost1.Click
        lblError.Text = ""
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox
        Dim PartyCode As String = ""
        Dim i As Integer = 0
        Dim mrow As GridViewRow
        Dim SelectedIDs As String = ""
        Dim SameStaffType As Boolean = False
        Dim PurposeExists As Integer = 1
        If txtPostingDate.Text.Trim = "" Then lblError.Text &= "Please select Posting Date,"
        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " "
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " ", UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Visible = True
            Return
        End If


        For Each mrow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(mrow.FindControl("chkControl"), CheckBox)


            Dim lblID As Label = TryCast(mrow.FindControl("lblTIC_ID"), Label)

            If ChkBxItem.Checked = True Then

                If i = 0 Then
                    PartyCode = mrow.Cells(5).Text
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                Else
                    If PartyCode = mrow.Cells(5).Text Then
                        IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    Else
                        SameStaffType = True
                    End If
                End If
                i = i + 1
            End If
        Next
        If SameStaffType = False Then
            PostLeasingInvoice(IDs)
            usrMessageBar.ShowNotification("Data Posted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            'lblError.Text = "Data Posted Successfully !!!"
            refreshGrid()
        Else
            'lblError.Text = "select either Student or Staff......!"
            usrMessageBar.ShowNotification("select either Student or Staff!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
    End Sub
    Private Sub PostLeasingInvoice(ByVal id As String)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(6) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", id, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@POSTINGDATE", txtPostingDate.Text, SqlDbType.DateTime)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostDriverCharges", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                stTrans.Commit()
                usrMessageBar.ShowNotification("Data Posted Sucessfully..", UserControls_usrMessageBar.WarningType.Success)
            End If

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, id, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Property TPTIDCardCharges() As DataTable
        Get
            Return ViewState("TPTIDCardCharges")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTLeasingInvoice") = value
        End Set
    End Property
    Protected Sub RadFilterBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadFilterBSU.SelectedIndexChanged
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'sqlWhere = " and  MONTH(TIC_TRDATE)= MONTH('" & txtDate.Text & "') AND TIC_DELETED=0 and TIC_BSU_ID='" & Session("sBsuid") & "' and TIC_FYEAR='" & Session("F_YEAR") & "' and TIC_CTO_BSU_ID ='" & RadFilterBSU.SelectedValue
        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTIDCardCharges = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTIDCardCharges
        '    SetExportFileLink()
        '    txtSelectedAmt.Text = "0.00"

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try
        refreshGrid()

    End Sub
    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0

        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)

            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                Dim lblVAT As Label = TryCast(gvr.FindControl("lblVATAmt"), Label)
                Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmt"), Label)
                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    Gross += Val(lblGross.Text)
                    VAT += Val(lblVAT.Text)
                    Net += Val(lblNet.Text)
                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
                VAT = 0
                NEt = 0
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAT.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Protected Sub rblType_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblType.TextChanged
        BindCardtype()
    End Sub
    Protected Sub txtPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrice.TextChanged
        txtqty.Focus()
    End Sub
    Private Sub CalculateTotal()
        txtAmount.Text = Val(txtPrice.Text) * Val(txtqty.Text)
        txtNetAmount.Text = Val(txtAmount.Text) - Val(txtDiscount.Text)
    End Sub
    Protected Sub txtqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtqty.TextChanged
        txtDiscount.Focus()
    End Sub
    Protected Sub txtDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount.TextChanged
        txtRemarks.Focus()
    End Sub
    Private Sub CalculateVAT(ByVal Act_id As String)
        Dim dt As New DataTable
        dt = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','ID_CRD_CHG','" & txtDate.Text & "'," & txtTotal.Text & ",'" & Act_id & "')", "OASIS_TRANSPORTConnectionString")

        If dt.Rows.Count > 0 Then
            Dim lstDrp As New RadComboBoxItem
            lstDrp = radVAT.Items.FindItemByValue(dt.Rows(0)("tax_code"))
            h_VATPerc.Value = dt.Rows(0)("tax_perc_value")
            'lstDrp = radVAT.Items.FindItemByValue("VAT5")
            'If bsuid <> "500953" Then
            '    h_VATPerc.Value = 5
            'Else
            '    h_VATPerc.Value = 7
            'End If

            If Not lstDrp Is Nothing Then
                radVAT.SelectedValue = lstDrp.Value
            Else
                radVAT.SelectedValue = 0
            End If
            txtVATAmount.Text = ((Val(txtTotal.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = (Val(txtTotal.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged
        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVAT.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            txtVATAmount.Text = ((Val(txtTotal.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtTotal.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        Else
            radVAT.SelectedValue = 0
            h_VATPerc.Value = radVAT.SelectedValue
            txtVATAmount.Text = ((Val(txtTotal.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtTotal.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub RadCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadCustomer.SelectedIndexChanged
        CalculateVAT(RadCustomer.SelectedValue)
        SelectEmirate(RadCustomer.SelectedValue)
    End Sub
    Private Sub SelectEmirate(ByVal BSUID As String)
        Dim bsu_City As String = ""
        bsu_City = Mainclass.getDataValue("SELECT case when case when BSU_CITY='ALN' then 'AUH' else BSU_CITY end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when BSU_CITY='ALN' then 'AUH' else BSU_CITY end end   BSU_CITY   FROM OASIS..BUSINESSUNIT_M where BSU_id='" & BSUID & "' ", "OASIS_TRANSPORTConnectionString")
        Dim lstDrp As New RadComboBoxItem
        lstDrp = RadEmirate.Items.FindItemByValue(bsu_City)

        If Not lstDrp Is Nothing Then
            RadEmirate.SelectedValue = lstDrp.Value
        Else
            RadEmirate.SelectedValue = "NA"
        End If
    End Sub
    
    Protected Sub rblSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSelection.SelectedIndexChanged
        refreshGrid()

    End Sub
End Class



