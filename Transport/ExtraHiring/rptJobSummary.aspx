<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptJobSummary.aspx.vb" Inherits="Transport_ExtraHiring_rptJobSummary" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function GetAccounts() {
            //var sFeatures;
            //var lstrVal;
            //var lintScrVal;
            //var pMode;
            //var NameandCode;
            //sFeatures = "dialogWidth: 760px; ";
            //sFeatures += "dialogHeight: 420px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            pMode = "TPTCLIENTREPORT"


            url = "../../common/PopupSelect.aspx?id=" + pMode + "&multiSelect=true";

            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_client");
            <%--document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;
            document.forms[0].submit();--%>

        }
        function OnClientClose_client(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode
                document.getElementById('<%=h_ACTIDs.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtClientName.ClientID%>').value = '';
                __doPostBack('<%= txtClientName.ClientID%>', 'TextChanged'); 
            }
        }

        function GetVehicleType() {
            //var sFeatures;
            //var lstrVal;
            //var lintScrVal;
            //var pMode;
            //var NameandCode;
            //sFeatures = "dialogWidth: 760px; ";
            //sFeatures += "dialogHeight: 420px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            pMode = "TPTVEHICLETYPEREPORT"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&multiSelect=true";
            var oWnd = radopen(url, "pop_vehicletype");
            //result = window.showModalDialog(url, "", sFeatures);
           <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_VehicleTypeId .ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function OnClientClose_vehicletype(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode
                document.getElementById('<%=h_VehicleTypeId.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtVTYPID.ClientID%>').value = '';
                __doPostBack('<%= txtVTYPID.ClientID%>', 'TextChanged');
            }
        }

        function GetVehicle() {
            //var sFeatures;
            //var lstrVal;
            //var lintScrVal;
            //var pMode;
            //var NameandCode;
            //sFeatures = "dialogWidth: 760px; ";
            //sFeatures += "dialogHeight: 420px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            pMode = "TPTVEHICLEREPORT"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&multiSelect=true";
            var oWnd = radopen(url, "pop_vehicle");
           <%-- result = window.showModalDialog(url, "", sFeatures);
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_Vehicle_id .ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function OnClientClose_vehicle(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode
                document.getElementById('<%=h_Vehicle_id.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtVehicleName.ClientID%>').value = '';
                __doPostBack('<%= txtVehicleName.ClientID%>', 'TextChanged');
            }
        }

        function GetSalesman() {
            //var sFeatures;
            //var lstrVal;
            //var lintScrVal;
            //var pMode;
            //var NameandCode;
            //sFeatures = "dialogWidth: 760px; ";
            //sFeatures += "dialogHeight: 420px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            pMode = "TPTSALESMANNEW"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&multiSelect=true";
            var oWnd = radopen(url, "pop_salesman");
           <%-- result = window.showModalDialog(url, "", sFeatures);
            if (result != '' && result != undefined) {

                document.getElementById('<%=h_SalesmanId .ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function OnClientClose_salesman(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode
                document.getElementById('<%=h_SalesmanId.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtSLSMID.ClientID%>').value = '';
                __doPostBack('<%= txtSLSMID.ClientID%>', 'TextChanged');
            }
        }

        function GetSeason() {
            //var sFeatures;
            //var lstrVal;
            //var lintScrVal;
            //var pMode;
            //var NameandCode;
            //sFeatures = "dialogWidth: 760px; ";
            //sFeatures += "dialogHeight: 420px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            pMode = "TPTSEASONREPORT"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&multiSelect=true";
            var oWnd = radopen(url, "pop_season");
            <%--result = window.showModalDialog(url, "", sFeatures);
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_Season.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function OnClientClose_season(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode
                document.getElementById('<%=h_Season.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtSEASID.ClientID%>').value = '';
                __doPostBack('<%= txtSEASID.ClientID%>', 'TextChanged');
            }
        }

        function GetDriver() {
            //var sFeatures;
            //var lstrVal;
            //var lintScrVal;
            //var pMode;
            //var NameandCode;
            //sFeatures = "dialogWidth: 760px; ";
            //sFeatures += "dialogHeight: 420px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            pMode = "TPTDRIVERREPORT"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&multiSelect=true";
            var oWnd = radopen(url, "pop_driver");
           <%-- result = window.showModalDialog(url, "", sFeatures);
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_driver_id.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function OnClientClose_driver(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode
                document.getElementById('<%=h_driver_id.ClientID%>').value = NameandCode;
                document.getElementById('<%=txtdriver.ClientID%>').value = '';
                __doPostBack('<%= txtdriver.ClientID%>', 'TextChanged');
            }
        }







    </script>

    &nbsp; &nbsp;

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_client" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_client"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        
            <telerik:RadWindow ID="pop_vehicletype" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_vehicletype"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

            <telerik:RadWindow ID="pop_vehicle" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_vehicle"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

              <telerik:RadWindow ID="pop_salesman" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_salesman"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        
         <telerik:RadWindow ID="pop_season" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_season"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

            <telerik:RadWindow ID="pop_driver" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose_driver"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Job Summary List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" style="width: 100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="center">
                            <table id="tblRate" runat="server" align="center" style="width: 100%">

                                <tr>
                                    <td width="20%" align="left"><span class="field-label">
                                        <asp:Label ID="lblfdate" runat="server" Text="From Date"></asp:Label></span></td>
                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtFDate" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="lnkFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFDate" PopupButtonID="lnkFDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="20%">
                                        <span class="field-label">
                                            <asp:Label ID="lbltdate" runat="server" Text="To Date"></asp:Label></span>
                                    </td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtTDate" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="lnkTDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTDate" PopupButtonID="lnkTDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>


                                <tr id="trJob" runat="server">
                                    <td align="left"><span class="field-label">Job Type</span> </td>

                                    <td align="left">
                                        <asp:RadioButton CssClass="field-label" ID="rbNES" Text="NES" runat="server" GroupName="jobtype" Checked="true" />
                                        <asp:RadioButton CssClass="field-label" ID="rbSchool" Text="School" runat="server" GroupName="jobtype" />
                                        <asp:RadioButton CssClass="field-label" ID="rbOthers" Text="SubCon" runat="server" GroupName="jobtype" />
                                        <asp:RadioButton CssClass="field-label" ID="rbAll" Text="All" runat="server" GroupName="jobtype" />
                                        <asp:CheckBox CssClass="field-label" ID="chkReimbursment" Text="Only Reimbursement" runat="server" Visible="true" />

                                    </td>
                                    <td></td>
                                </tr>

                                <tr id="trNewJob" runat="server" visible="false">
                                    <td align="left"><span class="field-label">Job Type </span></td>

                                    <td align="left">
                                        <asp:CheckBox CssClass="field-label" ID="NESChkBox" Text="NES" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="SchoolChkBox" Text="School" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="SubChkBox" Text="SubCon" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="AllChkBox" Text="All" runat="server" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr id="trClient" runat="server">
                                    <td align="left"><span class="field-label">Select Client </span></td>

                                    <td align="left">
                                        <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the Account ID you want to Add to the Search and click on Add)"></asp:Label><br />
                                        <asp:TextBox ID="txtClientName" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="lblAddID" runat="server">Add</asp:LinkButton>
                                        <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetAccounts(); return false;" />


                                        <asp:GridView ID="grdACTDetails" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ACT ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>

                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="trVehicleType" runat="server">
                                    <td align="left"><span class="field-label">Select Vehicle Type </span></td>

                                    <td align="left">

                                        <asp:Label ID="Label1" runat="server" Text="(Enter the Vehicle ID you want to Add to the Search and click on Add)"></asp:Label><br />
                                        <asp:TextBox ID="txtVTYPID" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="lblAddVTYPID" runat="server">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetVehicleType();return false;" />
                                        <asp:GridView CssClass="table table-bordered table-row" Width="100%" ID="grdVehTypeDetails" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Vehicle Type ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVTYPID" runat="server" Text='<%# Bind("VTYP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="VTYP_DESCRIPTION" HeaderText="Vehicle Type" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdVehTypeDelete" runat="server" OnClick="lnkbtngrdVehTypeDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>

                                    </td>

                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="trSalesman" runat="server">
                                    <td align="left"><span class="field-label">Select SalesMan </span></td>

                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" Text="(Enter the Salesman ID you want to Add to the Search and click on Add)"></asp:Label><br />
                                        <asp:TextBox ID="txtSLSMID" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="lblAddSLSMID" runat="server">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetSalesman(); return false;" />
                                        <asp:GridView CssClass="table table-bordered table-row" Width="100%" ID="grdSalesmanDetails" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Salesman ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSLSMID" runat="server" Text='<%# Bind("SLSM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SLSM_NAME" HeaderText="Salesman Name" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdSalesmanDelete" runat="server" OnClick="lnkbtngrdSalesmanDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>

                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="trSeason" runat="server">
                                    <td align="left"><span class="field-label">Select Season</span></td>

                                    <td align="left">

                                        <asp:Label ID="Label3" runat="server" Text="(Enter the Season ID you want to Add to the Search and click on Add)"></asp:Label><br />
                                        <asp:TextBox ID="txtSEASID" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="lblSEASID" runat="server">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetSeason();return false;" />
                                        <asp:GridView CssClass="table table-bordered table-row" Width="100%" ID="grdSeasonDetails" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Season ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSEASID" runat="server" Text='<%# Bind("SEAS_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SEAS_NAME" HeaderText="Season Name" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdSeasonDelete" runat="server" OnClick="lnkbtngrdSeasonDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>

                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="trDriver" runat="server">
                                    <td align="left"><span class="field-label">Select Driver</span></td>

                                    <td align="left">

                                        <asp:Label ID="Label4" runat="server" Text="(Enter the driver ID you want to Add to the Search and click on Add)"></asp:Label><br />
                                        <asp:TextBox ID="txtdriver" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="LinkButton1" runat="server">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetDriver();return false;" />
                                        <asp:GridView CssClass="table table-bordered table-row" Width="100%" ID="GrdDriverDetails" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Driver ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDriverID" runat="server" Text='<%# Bind("TPTD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TPTD_NAME" HeaderText="Driver Name" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdDriverDelete" runat="server" OnClick="lnkbtngrdSeasonDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>

                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="trVehicle" runat="server">
                                    <td align="left"><span class="field-label">Select Vehicle</span></td>

                                    <td align="left">

                                        <asp:Label ID="Label5" runat="server" Text="(Enter the Vehicle ID you want to Add to the Search and click on Add)"></asp:Label><br />
                                        <asp:TextBox ID="txtVehicleName" runat="server"></asp:TextBox>
                                        <asp:LinkButton ID="linkVehcile" runat="server">Add</asp:LinkButton>
                                        <asp:ImageButton ID="ImgVehicle" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetVehicle();return false;" />
                                        <asp:GridView CssClass="table table-bordered table-row" Width="100%" ID="GrdVehicle" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Vehicle ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVehicleID" runat="server" Text='<%# Bind("VEH_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="descr" HeaderText="VEH. REGNO./Type" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdVehicleDelete" runat="server" OnClick="lnkbtngrdVehicleDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>

                                    </td>
                                    <td></td>
                                    <td></td>

                                </tr>


                                <tr id="trJobStatus" runat="server">
                                    <td></td>
                                    <td align="left" colspan="3">
                                        <asp:RadioButton CssClass="field-label" ID="rbOpen" runat="server" Checked="True" GroupName="g" Text="Open" />
                                        <asp:RadioButton CssClass="field-label" ID="rbCancelled" runat="server" GroupName="g" Text="Cancelled" />
                                        <asp:RadioButton CssClass="field-label" ID="rbFOC" runat="server" GroupName="g" Text="FOC" />
                                        <asp:RadioButton CssClass="field-label" ID="rbExecuted" runat="server" GroupName="g" Text="Executed" />
                                        <asp:RadioButton CssClass="field-label" ID="rbApprov" runat="server" GroupName="g" Text="Waiting for Approval" />
                                        <asp:RadioButton CssClass="field-label" ID="rbInvoiced" runat="server" GroupName="g" Text="Invoiced" />
                                        <asp:RadioButton CssClass="field-label" ID="rbPosted" runat="server" GroupName="g" Text="Posted" />
                                        <asp:RadioButton CssClass="field-label" ID="rbJAll" runat="server" GroupName="g" Text="All" />
                                        <asp:CheckBox CssClass="field-label" ID="chkSummary" Text="Summary" runat="server" Visible="False" />
                                    </td>
                                </tr>
                                <tr id="TrStatus" runat="server" visible="false">
                                    <td></td>
                                    <td align="left" colspan="3">
                                        <asp:CheckBox CssClass="field-label" ID="ChkOpen" Text="Open" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkDeclined" Text="Decllined" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkConfirmed" Text="Confirmed" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkFOC" Text="FOC" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkCancel" Text="Cancel" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="CheckApproval" Text="Pending For Approve" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkExecuted" Text="Executed" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkInvoiced" Text="Invoiced" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkPosted" Text="Posted" runat="server" />
                                        <asp:CheckBox CssClass="field-label" ID="ChkAll" Text="All" runat="server" />

                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">

                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" ValidationGroup="groupM1"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                            <input id="lstValues" name="lstValues" runat="server" type="hidden" />

                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="h_ClientId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_Season" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_driver_id" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_SalesmanId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_VehicleTypeId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_Vehicle_id" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

