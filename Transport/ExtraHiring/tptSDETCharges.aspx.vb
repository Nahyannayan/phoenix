﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime

Partial Class Transport_ExtraHiring_tptSDETCharges
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim UserAccess As String = ""

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200245") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtTStart.Attributes.Add("onblur", " return calc('T');")
                txtTEnd.Attributes.Add("onblur", " return calc('T');")


                sqlStr = " SELECT EGS_ID, replace(convert(varchar(30), EGS_TRDATE, 106),' ','/') EGS_TRDATE, EGS_BSU_ID, EGS_EMP_ID, EGS_TSTART, EGS_TEND, EGS_TTOTAL, EGS_REMARKS, EGS_ESI_ID, EGS_SPID, EGS_OTHER_AMOUNT, EGS_FYEAR, EGS_USER, EGS_DELETED, EGS_DATE,CASE WHEN EGS_SPID=0 THEN '---None---' ELSE  SP_DESCR END PROJECT,driver_empno,DRIVER "
                sqlStr &= " FROM TPTEXGRATIACHARGESSDET  inner join VW_DRIVER_SDE  on driver_empid=EGS_EMP_ID LEFT OUTER join SDETPROJECTMASTER  on SP_ID=EGS_SPID "
                sqlStr &= "  inner join oasis..BUSINESSUNIT_M on BSU_ID=EGS_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGS_EMP_ID  "
                BindPurpose()
                clearScreen()
                If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                refreshGrid()
                If Request.QueryString("VIEWID") <> "" Then
                    h_EGN_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_EGN_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
    End Sub

    Protected Sub Check_All(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
    End Sub
    Private Sub refreshGrid()
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'sqlWhere = " where  MONTH(EGS_TRDATE)= MONTH('" & txtDate.Text & "') AND EGS_DELETED=0 and EGS_BSU_ID='" & Session("sbsuid") & "' and EGS_FYEAR='" & Session("F_YEAR") & "' order by EGS_TRDATE,EGS_ID "
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTEXGRATIACHARGESNEW = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTEXGRATIACHARGESNEW
        '    SetExportFileLink()

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try


        Dim ds As New DataSet
        Dim Posted As Integer = 0

        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 1, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGS_ID", 0, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", "01-01-1900", SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", "01-01-1900", SqlDbType.DateTime)
        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntriesSDET", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTEXGRATIACHARGESNEW = ds.Tables(0)
            Session("myData") = TPTEXGRATIACHARGESNEW
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try



    End Sub

    Private Sub refreshGrid1()
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'sqlWhere = " where  MONTH(EGS_TRDATE)= MONTH('" & txtDate.Text & "') AND EGS_DELETED=0 and EGS_BSU_ID='" & Session("sbsuid") & "' and EGS_FYEAR='" & Session("F_YEAR") & "'"
        'sqlWhere &= " and EGS_TRDATE>='" & txtFromDate.Text & "' "
        'sqlWhere &= " and EGS_TRDATE<='" & txtToDate.Text & "' order by EGS_TRDATE "

        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTEXGRATIACHARGESNEW = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTEXGRATIACHARGESNEW
        '    SetExportFileLink()

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        'BindChargingBsu()
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 3, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGS_ID", 0, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", txtFromDate.Text, SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", txtToDate.Text, SqlDbType.DateTime)
        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntriesSDET", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTEXGRATIACHARGESNEW = ds.Tables(0)
            Session("myData") = TPTEXGRATIACHARGESNEW
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub clearScreen()
        h_EGN_ID.Value = "0"
        h_Emp_id.Value = "0"
        txtOasis.Text = ""
        txtDriverName.Text = ""
        txtTTotal.Text = "0.0"
        txtTStart.Text = "00:00"
        txtTEnd.Text = "00:00"
        txtTTotal.Text = "0.0"
        txtAmount.Text = "0.0"
        TxtRemarks.Text = ""
        txtDate.Text = Now.ToString("dd/MMM/yyyy")
        txtTStart.Enabled = True
        txtTEnd.Enabled = True


    End Sub
    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        Dim PurposeExists As Integer = 1
        If txtAmount.Text = "" Then txtAmount.Text = "0.0"
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If Left(txtTStart.Text, 2) > 24 Then lblError.Text &= "Invalid Time in,"
        If Left(txtTEnd.Text, 2) > 24 Then lblError.Text &= "Invalid Time Out,"
        If Right(txtTStart.Text, 2) > 60 Then lblError.Text &= "Invalid Time in,"
        If Right(txtTEnd.Text, 2) > 60 Then lblError.Text &= "Invalid Time Out,"
        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            lblError.Visible = True
            Return
        End If
        Try
            Dim pParms(15) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@EGS_ID", h_EGN_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@EGS_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@EGS_BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@EGS_EMP_ID", h_Emp_id.Value, SqlDbType.Int)
            pParms(5) = Mainclass.CreateSqlParameter("@EGS_TSTART", txtTStart.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@EGS_TEND", txtTEnd.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@EGS_TTOTAL", txtTTotal.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@EGS_REMARKS", TxtRemarks.Text, SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@EGS_SPID", RadPurpose.SelectedValue, SqlDbType.Int)
            pParms(10) = Mainclass.CreateSqlParameter("@EGS_OTHER_AMOUNT", txtAmount.Text, SqlDbType.Decimal)
            pParms(11) = Mainclass.CreateSqlParameter("@EGS_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@EGS_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                'Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTEXGRATIACHARGESSDET", pParms)
                Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVETPTEXGRATIACHARGESSDET", pParms)


                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    clearScreen()
                    refreshGrid()
                    lblError.Text = "Data Saved Successfully !!!"
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub

    Private Sub showEdit(ByVal showId As Integer)

        Dim ds As New DataSet
        Dim Posted As Integer = 0

        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 2, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGS_ID", showId, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", "01-01-1900", SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", "01-01-1900", SqlDbType.DateTime)
        Try
            'Dim rdrExGratiaCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & " where EGS_ID='" & showId & "'")
            Dim rdrExGratiaCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntriesSDET", pParms)


            Dim d1 As Date
            Dim dayname1 As String = ""
            Dim HolidayCheck As Integer = 0
            If rdrExGratiaCharges.HasRows Then
                rdrExGratiaCharges.Read()
                h_EGN_ID.Value = rdrExGratiaCharges("EGS_ID")
                txtDate.Text = rdrExGratiaCharges("EGS_TRDATE")
                h_Emp_id.Value = rdrExGratiaCharges("EGS_EMP_ID")
                txtDriverName.Text = rdrExGratiaCharges("DRIVER")
                txtOasis.Text = rdrExGratiaCharges("DRIVER_EMPNO")
                txtTStart.Text = rdrExGratiaCharges("EGS_TSTART")
                txtTEnd.Text = rdrExGratiaCharges("EGS_TEND")
                txtTTotal.Text = rdrExGratiaCharges("EGS_TTOTAL")
                TxtRemarks.Text = rdrExGratiaCharges("EGS_REMARKS")
                RadPurpose.Text = rdrExGratiaCharges("PROJECT")
                RadPurpose.SelectedValue = rdrExGratiaCharges("EGS_SPID")
            End If
            rdrExGratiaCharges.Close()
            rdrExGratiaCharges = Nothing
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GridViewShowDetails_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowCreated

    End Sub
    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(11).Text = "0" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(14).Enabled = True
                gvr.Cells(15).Enabled = False
            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
                gvr.Cells(14).Enabled = False
                gvr.Cells(15).Enabled = True
            End If
        Next
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblEGSID"), Label)
        Try
            Dim pParms(2) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@EGS_ID", lblID.Text, SqlDbType.Int)
            pParms(2) = Mainclass.CreateSqlParameter("@EGS_USER", Session("sUsr_name"), SqlDbType.VarChar)

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "DELETESDETCHARGES", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    stTrans.Commit()
                    clearScreen()
                    refreshGrid()
                    lblError.Text = "Record DeletedSuccessfully !!!"

                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try


    End Sub

    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTAC_INVNO"), Label)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIACHARGES"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EGN_INVNO", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@EGN_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTEXGRATIACHARGES.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'Sub ReportLoadSelection()
    '    If Session("ReportSel") = "POP" Then
    '        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
    '    Else
    '        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
    '    End If
    'End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/../Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/../Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid1()
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
        CheckProjectandDriver()
    End Sub
    Private Property TPTEXGRATIACHARGESNEW() As DataTable
        Get
            Return ViewState("TPTEXGRATIACHARGESNES")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTEXGRATIACHARGESNES") = value
        End Set
    End Property
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("ExGratia Charges")
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)
            If Chkal.Checked = True Then
                'Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    'Gross += Val(lblGross.Text)
                    Gross = 0
                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
            End If
        Next
        txtSelectedAmt.Text = Gross
    End Sub
    Protected Sub txtOasis_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOasis.TextChanged
        CheckProjectandDriver()
    End Sub
    Private Sub CheckProjectandDriver()
        Dim DriverName As String
        Dim ProjectCount As Integer = 0
        If txtOasis.Text <> "" Then
            DriverName = Mainclass.getDataValue("select Driver from VW_DRIVER_SDE where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtOasis.Text) & "'", "OASIS_TRANSPORTConnectionString")
            If DriverName <> "" Then
                txtDriverName.Text = DriverName
            Else
                lblError.Text = "Invalid Driver No...!:"
                txtOasis.Text = ""
                txtOasis.Focus()
                Exit Sub
            End If

            h_Emp_id.Value = Mainclass.getDataValue("select driver_empid from VW_DRIVER_SDE where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtOasis.Text) & "'", "OASIS_TRANSPORTConnectionString")
            'ProjectCount = Mainclass.getDataValue("select count(*) from SDETPROJECTMASTER_STAFFLIST inner join SDETPROJECTMASTER on sps_SP_id=SP_ID where SPS_EMP_ID=" & h_Emp_id.Value & " and '" & LTrim(RTrim(txtDate.Text)) & "' between SP_FROMDATE and SP_TODATE and isnull(SP_STATUS,'O')='C' ", "OASIS_TRANSPORTConnectionString")
            ProjectCount = Mainclass.getDataValue("select count(*) from SDETPROJECTMASTER_STAFFLIST inner join SDETPROJECTMASTER on sps_SP_id=SP_ID where SPS_EMP_ID=" & h_Emp_id.Value & " and  isnull(SP_STATUS,'O')='C' ", "OASIS_TRANSPORTConnectionString")

            'Dim rdrSDETReader As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.text, "select SPS_AMOUNT,SPS_AMOUNT,SP_DESCR,SP_ID from SDETPROJECTMASTER_STAFFLIST inner join SDETPROJECTMASTER on sps_SP_id=SP_ID where SPS_EMP_ID=" & h_Emp_id.Value & " and '" & LTrim(RTrim(txtDate.Text)) & "' between SP_FROMDATE and SP_TODATE and isnull(SP_STATUS,'O')='O' ")



            Dim BSUParms(2) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@EMP_ID", h_Emp_id.Value, SqlDbType.Float)
            BSUParms(2) = Mainclass.CreateSqlParameter("@Date", LTrim(RTrim(txtDate.Text)), SqlDbType.DateTime)

            Dim rdrSDETReader As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "gETSDETProjectAmount", BSUParms)

            If ProjectCount = 1 Then
                If rdrSDETReader.HasRows Then
                    rdrSDETReader.Read()
                    txtAmount.Text = rdrSDETReader("SPS_AMOUNT")
                    RadPurpose.Text = rdrSDETReader("SP_DESCR")
                    RadPurpose.SelectedValue = rdrSDETReader("SP_ID")
                    txtTStart.Text = "00:00"
                    txtTEnd.Text = "00:00"
                    txtTTotal.Text = "0.0"
                    txtAmount.Enabled = False
                    txtTStart.Enabled = False
                    txtTEnd.Enabled = False
                    txtTTotal.Enabled = False
                    RadPurpose.Enabled = True
                End If

            ElseIf ProjectCount > 1 Then
                lblError.Text = "this employee is appearing in more than one Project"
                Exit Sub
            Else
                txtAmount.Text = 0
                txtAmount.Enabled = False
                txtTStart.Enabled = True
                txtTEnd.Enabled = True
                txtTTotal.Enabled = True
                RadPurpose.Text = "---None---"
                RadPurpose.SelectedValue = 0
            End If
        End If
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIASDET"
            Dim sqlParam(4) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@FROMDT", txtFromDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@TODT", txtToDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(1))

            sqlParam(2) = Mainclass.CreateSqlParameter("@BSUID", Session("SBSUID"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))

            sqlParam(3) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))

            sqlParam(4) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("preparedby") = Session("sUsr_name")
            params("reportCaption") = "SDET Charges"
            params("DateRange") = txtFromDate.Text & " - " & txtToDate.Text
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/Reports/rptSDETCharges.rpt"

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindPurpose()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select  SP_ID,SP_DESCR from VW_TPT_PROJECT order by SP_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
        RadPurpose.DataSource = ds
        RadPurpose.DataTextField = "SP_DESCR"
        RadPurpose.DataValueField = "SP_ID"
        RadPurpose.DataBind()
    End Sub
    Protected Sub txtFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromDate.TextChanged
        refreshGrid()
        CheckProjectandDriver()
    End Sub
    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToDate.TextChanged
        refreshGrid()
        CheckProjectandDriver()
    End Sub
    Protected Sub txtDriverName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDriverName.TextChanged
        CheckProjectandDriver()
    End Sub

    Private Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        RecallorApprove("A")
    End Sub
    Sub RecallorApprove(ByVal Action As String)

        Dim ids As String = ""
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(gvr.FindControl("lblEGSID"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            If ChkBxItem.Checked = True Then

                ids &= IIf(ids <> "", "|", "") & lblID.Text
            End If
        Next

        If ids = "" Then

            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If


        Try
            Dim pParms(4) As SqlParameter
            pParms(0) = Mainclass.CreateSqlParameter("@IDS", ids, SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            If Action = "R" Then
                pParms(4) = Mainclass.CreateSqlParameter("@OPTION", 1, SqlDbType.Int)
            Else
                pParms(4) = Mainclass.CreateSqlParameter("@OPTION", 2, SqlDbType.Int)
            End If


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "RecallExgratiaChargesSDET", pParms)
                If RetVal = "-1" Then
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    clearScreen()
                    refreshGrid()
                    If Action = "R" Then
                        usrMessageBar.ShowNotification("Records Recalled Successfully", UserControls_usrMessageBar.WarningType.Success)
                    Else
                        usrMessageBar.ShowNotification("Records sent for Approval Successfully", UserControls_usrMessageBar.WarningType.Success)
                    End If

                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnRecall_Click(sender As Object, e As EventArgs) Handles btnRecall.Click
        RecallorApprove("R")
    End Sub
End Class






