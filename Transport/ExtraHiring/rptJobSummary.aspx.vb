﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
Imports System.Collections
Partial Class Transport_ExtraHiring_rptJobSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                txtFDate.Text = Now.ToString("dd/MMM/yyyy")
                txtTDate.Text = Now.ToString("dd/MMM/yyyy")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200155" And ViewState("MainMnu_code") <> "T200160" And ViewState("MainMnu_code") <> "T200165" And ViewState("MainMnu_code") <> "T200160" And ViewState("MainMnu_code") <> "T200170" And ViewState("MainMnu_code") <> "T200175" And ViewState("MainMnu_code") <> "T200180" And ViewState("MainMnu_code") <> "T200185" And ViewState("MainMnu_code") <> "T200156") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                Select Case ViewState("MainMnu_code")
                    Case "T200155", "T200180"
                        trVehicle.Visible = False
                        lblCaption.Text = "Job Summary Report"
                    Case "T200160"
                        trJobStatus.Visible = False
                        trSeason.Visible = False
                        trSalesman.Visible = False
                        trJob.Visible = False
                        trClient.Visible = False
                        trDriver.Visible = False
                        trVehicleType.Visible = False
                        lblCaption.Text = "Vehicle Idle Report"
                    Case "T200165"
                        trJobStatus.Visible = False
                        trSeason.Visible = False
                        trSalesman.Visible = False
                        trJob.Visible = False
                        trVehicleType.Visible = False
                        trClient.Visible = False
                        trVehicle.Visible = False
                        lblCaption.Text = "Driver Idle Report"
                    Case "T200170"
                        trVehicle.Visible = False
                        lblCaption.Text = "Driver Usage Report"
                    Case "T200175"
                        trDriver.Visible = False
                        lblCaption.Text = "Vehicle Usage Report"
                    Case "T200185"
                        lblCaption.Text = "DAILY SALES REPORT"
                    Case "T200156"
                        trVehicle.Visible = False
                        trJobStatus.Visible = False
                        TrStatus.Visible = True
                        lblCaption.Text = "Job Summary Report"
                End Select

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Else
            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
            End If
            If h_VehicleTypeId.Value <> Nothing And h_VehicleTypeId.Value <> "" And h_VehicleTypeId.Value <> "undefined" Then
                FillVehicleTypes(h_VehicleTypeId.Value)
            End If
            If h_Season.Value <> Nothing And h_Season.Value <> "" And h_Season.Value <> "undefined" Then
                FillSEASON(h_Season.Value)
            End If
            If h_SalesmanId.Value <> Nothing And h_SalesmanId.Value <> "" And h_SalesmanId.Value <> "undefined" Then
                FillSalesman(h_SalesmanId.Value)
            End If
            If h_driver_id.Value <> Nothing And h_driver_id.Value <> "" And h_driver_id.Value <> "undefined" Then
                FillDriver(h_driver_id.Value)
            End If
            If h_Vehicle_id.Value <> Nothing And h_Vehicle_id.Value <> "" And h_Vehicle_id.Value <> "undefined" Then
                FillVehicle(h_Vehicle_id.Value)
            End If
        End If
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            callReport()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub callReport()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand

            Select Case ViewState("MainMnu_code")
                Case "T200155", "T200180", "T200185"
                    cmd.CommandText = "rptJOBHistory"
                Case "T200160"
                    cmd.CommandText = "rptVehicleIdle"
                Case "T200165"
                    cmd.CommandText = "rptDriverIdle"
                Case "T200170"
                    cmd.CommandText = "rptDriverUsage"
                Case "T200175"
                    cmd.CommandText = "rptVehicleUsage"
                Case "T200156"
                    cmd.CommandText = "rptJOBHistoryNew"

            End Select
            Dim sqlParam(11) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ClientId", h_ACTIDs.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@SalesmanId", h_SalesmanId.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@SeasonId", h_Season.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@VehicleTypeId", h_VehicleTypeId.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            If ViewState("MainMnu_code") = "T200156" Then
                Dim Status As String = ""
                If ChkOpen.Checked = True Then
                    Status = Status & "N|"
                End If
                If ChkCancel.Checked = True Then
                    Status = Status & "C|"
                End If
                If ChkConfirmed.Checked = True Then
                    Status = Status & "''|"
                End If
                If ChkDeclined.Checked = True Then
                    Status = Status & "R|"
                End If
                If ChkFOC.Checked = True Then
                    Status = Status & "F|"
                End If
                If ChkInvoiced.Checked = True Then
                    Status = Status & "I|"
                End If
                If ChkExecuted.Checked = True Then
                    Status = Status & "S|"
                End If
                If CheckApproval.Checked = True Then
                    Status = Status & "A|"
                End If
                If ChkPosted.Checked = True Then
                    Status = Status & "P|"
                End If

                If ChkAll.Checked = True Then
                    Status = Status & "N|S|R|D|C|I||F|''|A|P"
                End If
                sqlParam(4) = Mainclass.CreateSqlParameter("@Status", Status, SqlDbType.VarChar)



            Else
                If rbExecuted.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "S", SqlDbType.VarChar)
                ElseIf rbCancelled.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "C", SqlDbType.VarChar)
                ElseIf rbInvoiced.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "I", SqlDbType.VarChar)
                ElseIf rbFOC.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "F", SqlDbType.VarChar)
                ElseIf rbOpen.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "", SqlDbType.VarChar)
                ElseIf rbApprov.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "A", SqlDbType.VarChar)
                ElseIf rbPosted.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "P", SqlDbType.VarChar)
                ElseIf rbJAll.Checked = True Then
                    sqlParam(4) = Mainclass.CreateSqlParameter("@Status", "S|C|I||F|''|A|P", SqlDbType.VarChar)
                End If

            End If
            cmd.Parameters.Add(sqlParam(4))
            If rbNES.Checked = True Then
                sqlParam(5) = Mainclass.CreateSqlParameter("@JobType", "0", SqlDbType.VarChar)
            ElseIf rbSchool.Checked = True Then
                sqlParam(5) = Mainclass.CreateSqlParameter("@JobType", "1", SqlDbType.VarChar)
            ElseIf rbOthers.Checked = True Then
                sqlParam(5) = Mainclass.CreateSqlParameter("@JobType", "2", SqlDbType.VarChar)
            Else
                sqlParam(5) = Mainclass.CreateSqlParameter("@JobType", "", SqlDbType.VarChar)
            End If
            cmd.Parameters.Add(sqlParam(5))
            sqlParam(6) = Mainclass.CreateSqlParameter("@Fromdate", txtFDate.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(6))
            sqlParam(7) = Mainclass.CreateSqlParameter("@Todate", txtTDate.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(7))
            sqlParam(8) = Mainclass.CreateSqlParameter("@BSUId", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(8))
            sqlParam(9) = Mainclass.CreateSqlParameter("@MenuCode", ViewState("MainMnu_code"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(9))
            sqlParam(10) = Mainclass.CreateSqlParameter("@VehicleId", h_Vehicle_id.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(10))
            sqlParam(11) = Mainclass.CreateSqlParameter("@DriverId", h_driver_id.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(11))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            Select Case ViewState("MainMnu_code")
                Case "T200155"
                    If chkReimbursment.Checked = True Then
                        repSource.ResourceName = "../../Transport/ExtraHiring/rptJOBHistoryReimbursement.rpt"
                        params("reportCaption") = "Job Order Details with Reimbursement Details"
                    Else
                        repSource.ResourceName = "../../Transport/ExtraHiring/rptJOBHistory.rpt"
                        params("reportCaption") = "Job Order Details"
                    End If

                Case "T200160"
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptVehicleIdle.rpt"
                    params("reportCaption") = "Vehicle Idle Report"
                Case "T200165"
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptDriverIdle.rpt"
                    params("reportCaption") = "Driver Idle Report"
                Case "T200170"
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptDriverUsage.rpt"
                    params("reportCaption") = "Driver Usage Report"
                Case "T200175"
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptVehicleUsage.rpt"
                    params("reportCaption") = "Vehicle Usage Report"
                Case "T200180"
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptReservationPlanner.rpt"
                    params("reportCaption") = "RESERVATION PLANNER FOR THE PERIOD OF  " & txtFDate.Text & "  To  " & txtTDate.Text
                Case "T200185"
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptDailySalesReport.rpt"
                    params("reportCaption") = "DAILY SALES REPORT FOR THE PERIOD OF  " & txtFDate.Text & "  To  " & txtTDate.Text
                Case "T200156"
                    repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptJOBHistoryNew.rpt"
                    params("reportCaption") = "Job Order Details"
            End Select
            Dim subRep(0) As MyReportClass
            Dim sqlParam1(1) As SqlParameter
            repSource.IncludeBSUImage = True
            repSource.SubReport = subRep
            Session("ReportSource") = repSource
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub CallReport1()
        Dim param As New Hashtable
        param.Add("loginbsu", Session("sbsuid"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportParameters = param
            If ViewState("MainMnu_code") = "T200155" Then
            ElseIf ViewState("MainMnu_code") = "T200155" Then
                .reportPath = Server.MapPath("../Rpt/rptBuslistContacts.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                'i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub FillVehicleTypes(ByVal VtypeIDs As String)
        Try
            Dim IDs As String() = VtypeIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                'i += 1
            Next
            str_Sql = "select VTYP_DESCRIPTION,VTYP_ID from VEHTYPE_M WHERE VTYP_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdVehTypeDetails.DataSource = ds
            grdVehTypeDetails.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub FillSalesman(ByVal SalesmanID As String)
        Try
            Dim IDs As String() = SalesmanID.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                'i += 1
            Next
            str_Sql = "select SLSM_NAME,SLSM_ID from TPTSALESMAN WHERE SLSM_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdSalesmanDetails.DataSource = ds
            grdSalesmanDetails.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub FillDriver(ByVal DriverID As String)
        Try
            Dim IDs As String() = DriverID.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                'i += 1
            Next
            str_Sql = "SELECT  TPTD_ID,TPTD_NAME  FROM TPTDRIVER_M WHERE TPTD_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GrdDriverDetails.DataSource = ds
            GrdDriverDetails.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub FillVehicle(ByVal vehicleID As String)
        Try
            Dim IDs As String() = vehicleID.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                'i += 1
            Next
            str_Sql = "select VEH_ID ,VEH_REGNO+'-'+VEH_TYPE DESCR  from TPTVEHICLE_M where veh_id  IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GrdVehicle.DataSource = ds
            GrdVehicle.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub FillSEASON(ByVal SeasonID As String)
        Try
            Dim IDs As String() = SeasonID.Split("|")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                'i += 1
            Next
            str_Sql = "select SEAS_NAME,SEAS_ID from TPTSEASON WHERE SEAS_ID IN(" + condition + ")"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdSeasonDetails.DataSource = ds
            grdSeasonDetails.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If
    End Sub
    Protected Sub lnkbtngrdSeasonDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSeasonID As New Label
        lblSeasonID = TryCast(sender.FindControl("lblSEASID"), Label)
        If Not lblSeasonID Is Nothing Then
            h_Season.Value = h_Season.Value.Replace(lblSeasonID.Text, "").Replace("||||", "||")
            grdSeasonDetails.PageIndex = grdSeasonDetails.PageIndex
            FillSEASON(h_Season.Value)
        End If
    End Sub
    Protected Sub lnkbtngrdVehTypeDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblVehTypeID As New Label
        lblVehTypeID = TryCast(sender.FindControl("lblVTYPID"), Label)
        If Not lblVehTypeID Is Nothing Then
            h_VehicleTypeId.Value = h_VehicleTypeId.Value.Replace(lblVehTypeID.Text, "").Replace("||||", "||")
            grdVehTypeDetails.PageIndex = grdVehTypeDetails.PageIndex
            FillVehicleTypes(h_VehicleTypeId.Value)
        End If
    End Sub
    Protected Sub lnkbtngrdSalesmanDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSalesmanID As New Label
        lblSalesmanID = TryCast(sender.FindControl("lblSLSMID"), Label)
        If Not lblSalesmanID Is Nothing Then
            h_SalesmanId.Value = h_SalesmanId.Value.Replace(lblSalesmanID.Text, "").Replace("||||", "||")
            grdSalesmanDetails.PageIndex = grdSalesmanDetails.PageIndex
            FillSalesman(h_SalesmanId.Value)
        End If
    End Sub
    Protected Sub lnkbtngrdVehicleDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblVehicleID As New Label
        lblVehicleID = TryCast(sender.FindControl("lblVehicleID"), Label)
        If Not lblVehicleID Is Nothing Then
            h_Vehicle_id.Value = h_Vehicle_id.Value.Replace(lblVehicleID.Text, "").Replace("||||", "||")
            GrdVehicle.PageIndex = GrdVehicle.PageIndex
            FillVehicle(h_Vehicle_id.Value)
        End If
    End Sub
#End Region
    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub grdSalesmanDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSalesmanDetails.PageIndexChanging
        grdSalesmanDetails.PageIndex = e.NewPageIndex
        FillSalesman(h_SalesmanId.Value)
    End Sub

    Protected Sub grdSeasonDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSeasonDetails.PageIndexChanging
        grdSeasonDetails.PageIndex = e.NewPageIndex
        FillSEASON(h_Season.Value)
    End Sub

    Protected Sub grdVehTypeDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdVehTypeDetails.PageIndexChanging
        grdVehTypeDetails.PageIndex = e.NewPageIndex
        FillVehicleTypes(h_VehicleTypeId.Value)
    End Sub
    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddID.Click
        h_ACTIDs.Value += "||" + txtClientName.Text.Replace(",", "||")
        h_ACTIDs.Value = txtClientName.Text
        FillACTIDs(h_ACTIDs.Value)
    End Sub
    Protected Sub lblAddSLSMID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddSLSMID.Click
        h_SalesmanId.Value += "||" + txtSLSMID.Text.Replace(",", "||")
        h_SalesmanId.Value = txtSLSMID.Text
        FillSalesman(h_SalesmanId.Value)
    End Sub
    Protected Sub lblAddVTYPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddVTYPID.Click
        h_VehicleTypeId.Value += "||" + txtVTYPID.Text.Replace(",", "||")
        h_VehicleTypeId.Value = txtVTYPID.Text
        FillVehicleTypes(h_VehicleTypeId.Value)
    End Sub
    Protected Sub lblSEASID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblSEASID.Click
        h_Season.Value += "||" + txtSEASID.Text.Replace(",", "||")
        h_Season.Value = txtSEASID.Text
        FillSEASON(h_Season.Value)
    End Sub

  
End Class

