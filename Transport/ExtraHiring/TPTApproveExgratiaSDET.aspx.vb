﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic

Partial Class Transport_ExtraHiring_TPTApproveExgratiaSDET
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200415") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            BindDAXCodes()
            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                If ViewState("datamode") = "add" Then
                    SetDataMode("add")
                Else
                    SetDataMode("view")
                    showNoRecordsFound()
                End If
                setModifyvalues(ViewState("EntryId"))
            End If

            showNoRecordsFound()
            grdInvoiceDetails.DataSource = TPTNESExgratiaInvoiceDetails
            grdInvoiceDetails.DataBind()
            showNoRecordsFound()
        Else
            calcTotals()
        End If

    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "
            'RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            'RadDAXCodes.DataTextField = "DESCR"
            'RadDAXCodes.DataValueField = "ID"
            'RadDAXCodes.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        grdInvoiceDetails.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode

        btnPrint.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim Amount(3) As String
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = "0" Then
            Else
                Dim str_conn As String = connectionString
                Dim dt As New DataTable

                MyAprID.Value = Mainclass.getDataValue("SELECT EAL_EAM_ID from [EXGRATIA_APPROVER_USER_LIST] where EAL_TYPE='SDET' and eal_user='" & Session("sUsr_name") & "'", "OASIS_TRANSPORTConnectionString")

                If MyAprID.Value <> "" Then
                    MyNextAprID.Value = Mainclass.getDataValue("SELECT EAL_EAM_ID from [EXGRATIA_APPROVER_USER_LIST] where EAL_TYPE='SDET' and eal_eam_id > " & MyAprID.Value & " order by eal_eam_id ", "OASIS_TRANSPORTConnectionString")
                Else
                    usrMessageBar.ShowNotification("You are not there in the Approval List!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub

                End If




                If ViewState("datamode") = "add" Then

                    hdnNetTotal.Value = Mainclass.getDataValue("SELECT isnull(SUM(NetAmount),0) [NET AMOUNT] FROM  (select EGS_BSU_ID,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END + SUM(EGS_OTHER_AMOUNT) [NetAmount],EGS_EMP_ID,EGS_TRDATE from TPTEXGRATIACHARGESSDET left outer join oasis..businessunit_m  on bsu_id=EGS_BSU_ID WHERE EGS_ESI_ID = 0 and LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_deleted=0 and EGS_emp_id<>0 And isnull(EGS_APR_ID, 0)='" & MyAprID.Value & "'  group by EGS_EMP_ID,EGS_TRDATE,EGS_BSU_ID) Net", "OASIS_TRANSPORTConnectionString")
                    txtOTAMOUNT.Text = Mainclass.getDataValue("SELECT isnull(SUM(OTAmount),0) [NETAMOUNT] FROM  (select EGS_BSU_ID,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END [OTAmount],EGS_EMP_ID,EGS_TRDATE from TPTEXGRATIACHARGESSDET left outer join oasis..businessunit_m  on bsu_id=EGS_BSU_ID WHERE EGS_ESI_ID = 0 and LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_deleted=0 and EGS_emp_id<>0 And isnull(EGS_APR_ID, 0)='" & MyAprID.Value & "'  group by EGS_EMP_ID,EGS_TRDATE,EGS_BSU_ID) Net", "OASIS_TRANSPORTConnectionString")
                    TXTOTHERAMOUNT.Text = Mainclass.getDataValue("SELECT isnull(SUM(OTHERAMOUNT),0) [OTHERAMOUNT] FROM  (select EGS_BSU_ID,SUM(EGS_OTHER_AMOUNT)OTHERAMOUNT,EGS_EMP_ID,EGS_TRDATE from TPTEXGRATIACHARGESSDET left outer join oasis..businessunit_m  on bsu_id=EGS_BSU_ID WHERE EGS_ESI_ID = 0 and LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_deleted=0 and EGS_emp_id<>0 And isnull(EGS_APR_ID, 0)='" & MyAprID.Value & "'  group by EGS_EMP_ID,EGS_TRDATE,EGS_BSU_ID) OTHER", "OASIS_TRANSPORTConnectionString")

                    txtNetTotal.Text = Val(txtOTAMOUNT.Text) + Val(TXTOTHERAMOUNT.Text)

                    'lblbatchstr.Text = p_Modifyid
                    txtBatchNo.Text = p_Modifyid
                    'txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
                    'lblTrDate.Text = Now.ToString("dd/MMM/yyyy")
                    h_EntryId.Value = 0
                    'fillGridView(grdInvoiceDetails, "select replace(convert(varchar(30), EGS_TRDATE, 106),' ','/') [DATE],sum(cast(EGS_TTOTAL as numeric(12,2)))[Total_Hrs],max(DRIVER_EMPNO) DRIVER_EMPNO,max(DRIVER)DRIVER,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END EGS_AMOUNT, SUM(EGS_OTHER_AMOUNT)  OTHERAMOUNT,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END +SUM(EGS_OTHER_AMOUNT)  TotalAmount FROM TPTEXGRATIACHARGESSDET  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGS_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGS_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGS_EMP_ID where LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_ESI_ID =0  and EGS_deleted=0 and EGS_EMP_ID<>0 group by EGS_EMP_ID,EGS_TRDATE")
                    fillGridView(grdInvoiceDetails, "select EGS_EMP_ID,replace(convert(varchar(30), EGS_TRDATE, 106),' ','/') [DATE],sum(cast(EGS_TTOTAL as numeric(12,2)))[Total_Hrs],max(EMPNO) DRIVER_EMPNO,max(isnull(EMP_FNAME ,'')+ ' ' + isnull(emp_mname,'') + ' ' + isnull(EMP_LNAME,''))DRIVER,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END EGS_AMOUNT, SUM(EGS_OTHER_AMOUNT)  OTHERAMOUNT,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END +SUM(EGS_OTHER_AMOUNT)  TotalAmount FROM TPTEXGRATIACHARGESSDET  LEFT OUTER JOIN VW_DRIVER  (nolock) on driver_empid=EGS_EMP_ID left outer join oasis..BUSINESSUNIT_M (nolock) on BSU_ID=EGS_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E (nolock) ON E.EMP_ID=EGS_EMP_ID where LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_ESI_ID =0  and EGS_deleted=0 and EGS_EMP_ID<>0 And isnull(EGS_APR_ID, 0)='" & MyAprID.Value & "' group by EGS_EMP_ID,EGS_TRDATE")

                    If grdInvoiceDetails.Rows.Count >= 1 Then
                        btnReject.Visible = True
                        btnAPPROVE.Visible = True
                    Else
                        btnReject.Visible = False
                        btnAPPROVE.Visible = False

                    End If

                Else

                    If UCase(Request.QueryString("filter")) = "APPROVED" Then
                        btnPrint.Visible = True

                    Else
                        btnPrint.Visible = False

                    End If

                    'dt = MainObj.getRecords("SELECT ENI_ID,isnull(ENI_TRNO,'')ENI_TRNO, replace(convert(varchar(30), ENI_DATE, 106),' ','/') [DATE],ENI_NET, ENI_USER_NAME, ENI_POSTED,ENI_REMARKS, ENI_DATE, ENI_POSTING_DATE,ENI_BSU_ID,ENI_STRNO,ENI_DATE,ENI_JHD_DOCNO,isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(ENI_PRGM_DIM_CODE,'0') DAX_ID FROM TPTEXGRATIANES_INV left outer join dbo.VV_DAX_CODES on ID=ENI_PRGM_DIM_CODE  WHERE LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_BSU_ID='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                    'If dt.Rows.Count > 0 Then
                    'txtBatchDate.Text = Format(IIf(IsDBNull(dt.Rows(0)("ENI_POSTING_DATE")), Now.Date.ToString, dt.Rows(0)("ENI_POSTING_DATE")), "dd/MMM/yyyy")
                    txtBatchNo.Text = p_Modifyid

                    txtremarks.Text = ""
                    'lblbatchstr.Text = dt.Rows(0)("ENI_STRNO")
                    'lblTrDate.Text = dt.Rows(0)("ENI_DATE")
                    'txtNetTotal.Text = dt.Rows(0)("ENI_NET")
                    'lblJHDDocNo.Text = dt.Rows(0)("ENI_JHD_DOCNO")

                    'lnkBatchDate.Enabled = False
                    'txtBatchDate.Enabled = False
                    'lblStatus.Text = "Posted"
                    h_EntryId.Value = 0


                    If UCase(Request.QueryString("filter")) = "APPROVED" Then
                        'fillGridView(grdInvoiceDetails, "select replace(convert(varchar(30), EGN_TRDATE, 106),' ','/') [DATE],sum(cast(EGN_TTOTAL as numeric(12,2)))[Total_Hrs],max(case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA' else case when EGN_DRIVERTYPE=3 then 'IIT'  end end end)  DriverType,max(DRIVER)DRIVER,max(DRIVER_EMPNO) DRIVER_EMPNO,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN max(EGN_DRIVERTYPE) ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END EGN_TOTAL,0 EGN_AMOUNT,0 EGN_OUTSTATION,0 EGN_OVERNIGHTSTAY  FROM TPTEXGRATIACHARGESNES  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGN_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID where EGN_EGI_ID='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 And isnull(EGN_APR_ID, 0)=10 group by EGN_EMP_ID,EGN_TRDATE")
                        fillGridView(grdInvoiceDetails, "select EGS_EMP_ID,replace(convert(varchar(30), EGS_TRDATE, 106),' ','/') [DATE],sum(cast(EGS_TTOTAL as numeric(12,2)))[Total_Hrs],max(DRIVER_EMPNO) DRIVER_EMPNO,max(DRIVER)DRIVER,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END EGS_AMOUNT, SUM(EGS_OTHER_AMOUNT)  OTHERAMOUNT,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END +SUM(EGS_OTHER_AMOUNT)  TotalAmount FROM TPTEXGRATIACHARGESSDET  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGS_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGS_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGS_EMP_ID where LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_ESI_ID =0  and EGS_deleted=0 and EGS_EMP_ID<>0 And isnull(EGS_APR_ID, 0)='10' group by EGS_EMP_ID,EGS_TRDATE")
                    Else
                        'fillGridView(grdInvoiceDetails, "select replace(convert(varchar(30), EGN_TRDATE, 106),' ','/') [DATE],sum(cast(EGN_TTOTAL as numeric(12,2)))[Total_Hrs],max(case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA' else case when EGN_DRIVERTYPE=3 then 'IIT'  end end end)  DriverType,max(DRIVER)DRIVER,max(DRIVER_EMPNO) DRIVER_EMPNO,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN max(EGN_DRIVERTYPE) ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END EGN_TOTAL,0 EGN_AMOUNT,0 EGN_OUTSTATION,0 EGN_OVERNIGHTSTAY  FROM TPTEXGRATIACHARGESNES  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGN_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID where EGN_EGI_ID='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 And And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' group by EGN_EMP_ID,EGN_TRDATE")
                        fillGridView(grdInvoiceDetails, "select EGS_EMP_ID,replace(convert(varchar(30), EGS_TRDATE, 106),' ','/') [DATE],sum(cast(EGS_TTOTAL as numeric(12,2)))[Total_Hrs],max(DRIVER_EMPNO) DRIVER_EMPNO,max(DRIVER)DRIVER,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END EGS_AMOUNT, SUM(EGS_OTHER_AMOUNT)  OTHERAMOUNT,CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END +SUM(EGS_OTHER_AMOUNT)  TotalAmount FROM TPTEXGRATIACHARGESSDET  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGS_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGS_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGS_EMP_ID where LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGS_ESI_ID =0  and EGS_deleted=0 and EGS_EMP_ID<>0 And isnull(EGS_APR_ID, 0)='" & MyAprID.Value & "' group by EGS_EMP_ID,EGS_TRDATE")
                    End If


                    showNoRecordsFound()
                    'End If
                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Property TPTNESExgratiaInvoiceDetails() As DataTable
        Get
            Return ViewState("TPTNESExgratiaInvoiceDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTNESExgratiaInvoiceDetails") = value
        End Set
    End Property

    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTNESExgratiaInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTNESExgratiaInvoiceDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTNESExgratiaInvoiceDetails = mtable
        fillGrdView.DataSource = TPTNESExgratiaInvoiceDetails
        fillGrdView.DataBind()
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        'txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
        txtremarks.Text = ""

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property EGCFooter() As DataTable
        Get
            Return ViewState("EGCFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EGCFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Sub showNoRecordsFound()
        If Not EGCFooter Is Nothing AndAlso EGCFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoiceDetails.Columns.Count - 2
            grdInvoiceDetails.Rows(0).Cells.Clear()
            grdInvoiceDetails.Rows(0).Cells.Add(New TableCell())
            grdInvoiceDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoiceDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdInvoiceDetails.PageIndexChanging
        grdInvoiceDetails.PageIndex = e.NewPageIndex
        grdInvoiceDetails.DataSource = TPTNESExgratiaInvoiceDetails
        grdInvoiceDetails.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoiceDetails.RowCancelingEdit
        grdInvoiceDetails.ShowFooter = True
        grdInvoiceDetails.EditIndex = -1
        grdInvoiceDetails.DataSource = EGCFooter
        grdInvoiceDetails.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub grdInvoiceDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoiceDetails.RowDataBound
        'If ViewState("datamode") <> "add" Then
        '    grdInvoiceDetails.Columns(0).Visible = False
        'End If
    End Sub

    'Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
    '    Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
    '    Dim Empnos As String = ""
    '    Dim chkControl As New HtmlInputCheckBox
    '    Dim total As Double = 0.0
    '    Dim gross As Double = 0.0
    '    Dim salik As Double = 0.0
    '    Dim net As Double = 0.0

    '    For Each gvr As GridViewRow In grdInvoiceDetails.Rows
    '        Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
    '        Dim lblEmpnoID As Label = TryCast(gvr.FindControl("lblEmpNo"), Label)
    '        Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmount"), Label)
    '        If ChkBxItem.Checked = True Then

    '            Empnos &= IIf(Empnos <> "", "|", "") & lblEmpnoID.Text
    '            total += Val(lblNet.Text)
    '        End If
    '    Next
    '    'net = (total)
    '    If Empnos = "" Then
    '        lblError.Text = "No Item Selected !!!"
    '        Exit Sub
    '    End If
    '    If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
    '        lblError.Text = ""
    '        If strMandatory.ToString.Length > 0 Then
    '            lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
    '        End If
    '        lblError.Text &= strError.ToString
    '        Exit Sub
    '    End If
    '    addMode = (h_EntryId.Value = 0)
    '    Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
    '    Dim pParms(14) As SqlParameter
    '    Dim pParms1(1) As SqlParameter
    '    pParms(1) = Mainclass.CreateSqlParameter("@ENI_ID", h_EntryId.Value, SqlDbType.Int, True)
    '    pParms(2) = Mainclass.CreateSqlParameter("@ENI_STRNO", lblbatchstr.Text, SqlDbType.VarChar)
    '    'pParms(5) = Mainclass.CreateSqlParameter("@ENI_NET", Val(txtVillaAmount.Text), SqlDbType.Float)
    '    pParms(6) = Mainclass.CreateSqlParameter("@ENI_USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
    '    pParms(7) = Mainclass.CreateSqlParameter("@ENI_REMARKS", txtremarks.Text, SqlDbType.VarChar)
    '    pParms(8) = Mainclass.CreateSqlParameter("@ENI_DATE", lblTrDate.Text, SqlDbType.VarChar)
    '    pParms(9) = Mainclass.CreateSqlParameter("@ENI_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
    '    pParms(10) = Mainclass.CreateSqlParameter("@ENI_POSTING_DATE", txtBatchDate.Text, SqlDbType.VarChar)
    '    pParms(11) = Mainclass.CreateSqlParameter("@ENI_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
    '    pParms(13) = Mainclass.CreateSqlParameter("@EMPNOS", Empnos, SqlDbType.VarChar)
    '    pParms(14) = Mainclass.CreateSqlParameter("@ENI_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
    '    Dim objConn As New SqlConnection(connectionString)
    '    objConn.Open()
    '    Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
    '    Try
    '        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostExGratiaNESCharges", pParms)
    '        If RetVal = "-1" Then
    '            lblError.Text = "Unexpected Error !!!"
    '            stTrans.Rollback()
    '            Exit Sub
    '        Else
    '            ViewState("EntryId") = pParms(1).Value
    '        End If
    '        stTrans.Commit()
    '        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
    '        If flagAudit <> 0 Then
    '            Throw New ArgumentException("Could not process your request")
    '        End If
    '        SetDataMode("view")
    '        setModifyvalues(lblbatchstr.Text)
    '        lblError.Text = "Data Saved Successfully !!!"
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '        stTrans.Rollback()
    '        Exit Sub
    '    Finally
    '        If objConn.State = ConnectionState.Open Then
    '            objConn.Close()
    '        End If
    '    End Try
    '    Response.Redirect(ViewState("ReferrerUrl"))
    'End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIASDETNEW"
            Dim sqlParam(3) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@BATCH", txtBatchNo.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(0))


            sqlParam(1) = Mainclass.CreateSqlParameter("@BSUID", Session("SBSUID"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            sqlParam(2) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))

            sqlParam(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("preparedby") = Session("sUsr_name")
            params("reportCaption") = "SDET Charges"
            params("DateRange") = ""
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/Reports/rptSDETChargesNEW.rpt"

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '  Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub UpdateAddress()
    End Sub
    Sub calcTotals()
        Dim NeTTotal As Decimal = 0
        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNetAmount"), Label)
            If ChkBxItem.Checked = True Then
                NeTTotal += Val(lblNet.Text)
            End If
        Next
        txtNetTotal.Text = NeTTotal
    End Sub

    Private Sub btnAPPROVE_Click(sender As Object, e As EventArgs) Handles btnAPPROVE.Click
        ApproveORReject("A")
    End Sub

    Private Sub ApproveORReject(ByVal action As String)
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblEmpno As Label = TryCast(gvr.FindControl("lblEmpNo"), Label)

            If ChkBxItem.Checked = True Then
                IDs &= IIf(IDs <> "", "|", "") & lblEmpno.Text
            End If
        Next

        If IDs = "" Then
            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblEmpno As Label = TryCast(gvr.FindControl("lblEmpNo"), Label)
            Dim lblDate As Label = TryCast(gvr.FindControl("lblEGSDATE"), Label)

            If ChkBxItem.Checked = True Then
                Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
                Dim pParms(10) As SqlParameter
                'pParms(1) = Mainclass.CreateSqlParameter("@EGI_ID", h_EntryId.Value, SqlDbType.Int, True)
                pParms(1) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                pParms(4) = Mainclass.CreateSqlParameter("@EMPNO", lblEmpno.Text, SqlDbType.VarChar)
                pParms(5) = Mainclass.CreateSqlParameter("@DATE", lblDate.Text, SqlDbType.VarChar)
                If MyNextAprID.Value = "" Then
                    pParms(6) = Mainclass.CreateSqlParameter("@NEXTAPRID", 10, SqlDbType.Int)
                Else
                    pParms(6) = Mainclass.CreateSqlParameter("@NEXTAPRID", MyNextAprID.Value, SqlDbType.Int)
                End If
                pParms(7) = Mainclass.CreateSqlParameter("@BATCH", txtBatchNo.Text, SqlDbType.VarChar)
                pParms(8) = Mainclass.CreateSqlParameter("@ACTION", action, SqlDbType.VarChar)
                pParms(9) = Mainclass.CreateSqlParameter("@REMARKS", txtremarks.Text, SqlDbType.VarChar)
                pParms(10) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
                pParms(10).Direction = ParameterDirection.Output
                Dim objConn As New SqlConnection(connectionString)
                objConn.Open()

                Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
                Try
                    Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "ApproveExgratiaChargesSDET", pParms)
                    Dim ErrorMSG As String = pParms(10).Value.ToString()
                    If ErrorMSG = "" Then
                        ViewState("EntryId") = pParms(1).Value
                    Else

                        usrMessageBar.ShowNotification(pParms(10).Value, UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                    stTrans.Commit()
                    Dim flagAudit As Integer
                    If action = "A" Then
                        flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, "", "Approved", Page.User.Identity.Name.ToString, Me.Page)
                    Else
                        flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, "", "Rejected", Page.User.Identity.Name.ToString, Me.Page)
                    End If

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If


                    If action = "A" Then
                        usrMessageBar.ShowNotification("Data Approved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    Else
                        usrMessageBar.ShowNotification("Data Rejected Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If

                Catch ex As Exception
                    usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try

            End If
        Next

        SetDataMode("view")
        ClearDetails()
        setModifyvalues(txtBatchNo.Text)




    End Sub

    Private Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        ApproveORReject("R")
    End Sub

    Private Sub grdInvoiceDetails_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdInvoiceDetails.RowCommand

    End Sub
End Class




