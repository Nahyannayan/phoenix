<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptHiringBSU.aspx.vb" Inherits="tptHiringBSU" Title="Extra Hiring for BSU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    
  
    <style type="text/css">

        .autocomplete_highlightedListItem_S1 {                       
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }

        .autocomplete_completionListElement_S1 {
            background-color: #8dc24c;
        }

        .autocomplete_listItem_S1 {

             background-color: #8dc24c;
        }
         .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
       
    </style>
    <script language="javascript" type="text/javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
       1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        function parseTime(s) {
            var c = s.split(':');
            return parseInt(c[0]) * 60 + parseInt(c[1]);
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function getVehicleNo() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTVEHICLE";
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up");
            <%--<%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtVehNo.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=h_VehicleType.ClientID %>").value = NameandCode[3];--%>
        }


        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtVehNo.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=h_VehicleType.ClientID %>").value = NameandCode[3];
                __doPostBack('<%= txtVehNo.ClientID%>', 'TextChanged');

                  var txtControl = document.getElementById('<%= txtDriver.ClientID %>');
            txtControl.focus();       
            }
        }

        function getDriversName() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTDRIVERSCHOOL";
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up2");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtDriver.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtDriver.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];
                
            }
        }


        function getVehicleType() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTVEHICLETYPE";
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up3");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtDriver.ClientID %>").value = NameandCode[1];--%>
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtDriver.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtDriver.ClientID%>', 'TextChanged');
            }
        }


        function calc(Type, rate) {
            if (Type == "O") {
                if ((document.getElementById("<%=txtOStart.ClientID %>").value != "") && (document.getElementById("<%=txtOEnd.ClientID %>").value != ""))
                    var total = eval(document.getElementById("<%=txtOEnd.ClientID %>").value) - eval(document.getElementById("<%=txtOStart.ClientID %>").value);
                document.getElementById("<%=txtOTotal.ClientID %>").value = total;

                if (total * eval(document.getElementById("<%=h_Rate.ClientID %>").value) <= document.getElementById("<%=h_mimimum.ClientID %>").value) { document.getElementById("<%=txtGross.ClientID %>").value = document.getElementById("<%=h_mimimum.ClientID %>").value; } else { document.getElementById("<%=txtGross.ClientID %>").value = format("#0.00", total * eval(document.getElementById("<%=h_Rate.ClientID %>").value)); }

                if (total * eval(document.getElementById("<%=h_Rate.ClientID %>").value) <= document.getElementById("<%=h_mimimum.ClientID %>").value) {
                    total = (4 * (eval(document.getElementById("<%=txtSalik.ClientID %>").value))) + eval(document.getElementById("<%=h_mimimum.ClientID %>").value);

                } else { total = (total * eval(document.getElementById("<%=h_Rate.ClientID %>").value)) + 4 * (eval(document.getElementById("<%=txtSalik.ClientID %>").value)); }
                document.getElementById("<%=txtNet.ClientID %>").value = format("#0.00", total);
            }
            else {
                a = document.getElementById("<%=txtTStart.ClientID %>").value;
                b = document.getElementById("<%=txtTEnd.ClientID %>").value;
                p = "1/1/1970 ";
                difference = new Date(new Date(p + b) - new Date(p + a)).toUTCString().split(" ")[4];
                document.getElementById("<%=txtTTotal.ClientID %>").value = difference;
            }
        }
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Extra Trip Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">
                            <table width="100%">
                                <%-- <tr class="subheader_img">
               <td align="left" colspan="15" valign="middle">
                  <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                  <span style="font-family: Verdana">Extra Trip Details</span></font>
               </td>
            </tr>--%>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td width="5%"><span class="field-label">Date</span></td>
                                                <td align="left" width="10%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                                <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDate" PopupButtonID="lnkDate"></ajaxToolkit:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20%">
                                                    <asp:DropDownList ID="ddlBusinessUnit" runat="server" AutoPostBack="True" TabIndex="0"></asp:DropDownList>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th rowspan="2">Vehicle</th>
                                                <th rowspan="2">Type</th>
                                                <th rowspan="2">Driver</th>
                                                <th rowspan="2">Charging Unit</th>
                                                <th rowspan="2">Ref.No</th>
                                                <th colspan="2">Location/Area</th>
                                                <th rowspan="2">Purpose</th>
                                                <th rowspan="2">Description</th>
                                                <th colspan="3">ODO Meter</th>
                                                <th colspan="3">Timings</th>
                                                <th rowspan="2">Total</th>
                                                <th rowspan="2">Salik(No.of Crossings)</th>
                                                <th rowspan="2">Total</th>
                                            </tr>
                                            <tr>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Total</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Total</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtTSRSNo" runat="server" Visible="false" Width="200px" ></asp:TextBox>
                                                                <asp:TextBox ID="txtVehNo" runat="server" AutoPostBack="true" Width="200px" TabIndex="1"></asp:TextBox>                                                    
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgVehicle" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getVehicleNo();return false;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlVehType" Width="200px" runat="server"
                                                        AutoPostBack="True" Enabled="False" >
                                                    </asp:DropDownList>
                                                    <br />
                                                </td>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtDriver" runat="server" Width="200px" AutoPostBack="true" align="left" TabIndex="2"></asp:TextBox>                                                              
                                                            </td>
                                                            <td>
                                                                  <asp:ImageButton ID="ImageDriver" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClientClick="getDriversName();return false;" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDriverName" runat="server" Width="300px" TabIndex="3"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                     <telerik:RadComboBox ID="radCBSU" RenderMode="Lightweight" runat="server" TabIndex="4">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRef" runat="server" Width="100px" TabIndex="5"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLOCFROM" runat="server" Width="150px" TabIndex="6"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" BehaviorID="AutoCompleteEx3"
                                                        CompletionListCssClass="autocomplete_completionListElement_S1" UseContextKey="true"
                                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                                        CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                                        ServiceMethod="GetArea" ServicePath="~/Transport/ExtraHiring/tptHiringBSU.aspx" TargetControlID="txtLOCFROM">
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLOCTo" runat="server" Width="150px" TabIndex="7"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" BehaviorID="AutoCompleteEx4"
                                                        CompletionListCssClass="autocomplete_completionListElement_S1" UseContextKey="true"
                                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                                        CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                                        ServiceMethod="GetArea" ServicePath="~/Transport/ExtraHiring/tptHiringBSU.aspx" TargetControlID="txtLOCTo">
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadPurpose" RenderMode="Lightweight" runat="server" TabIndex="8">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDescr" runat="server" Width="250px" TabIndex="9"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOStart" runat="server" Width="100px" Style="text-align: right" TabIndex="10"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOEnd" runat="server" Width="100px" Style="text-align: right" TabIndex="11"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOTotal" runat="server" Width="100px" Style="text-align: right" Enabled="False" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTStart" runat="server" AutoPostBack="true" Width="100px" Style="text-align: right" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtTStart" Mask="99:99" MaskType="Time" CultureName="en-us" MessageValidatorTip="true" runat="server"></cc1:MaskedEditExtender>
                                                <td>
                                                    <asp:TextBox ID="txtTEnd" runat="server" AutoPostBack="true" Width="100px" Style="text-align: right" TabIndex="13"></asp:TextBox>
                                                </td>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtTEnd" Mask="99:99" MaskType="Time" CultureName="en-us" MessageValidatorTip="true" runat="server"></cc1:MaskedEditExtender>
                                                <td>
                                                    <asp:TextBox ID="txtTTotal" runat="server" Enabled="false" Width="100px" Style="text-align: right" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtGross" runat="server" Enabled="false" Width="100px" Style="text-align: right" ></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSalik" runat="server" Width="100px" Style="text-align: right" TabIndex="14"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNet" runat="server" Enabled="false" Width="100px" Style="text-align: right" ></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="40%"></td>
                                    <td valign="bottom" width="20%" >
                                        
                                    <td width="40%" align="right">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                    </td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">

                            <table width="100%">
                                <tr>
                                    <td width="10%">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Find.." Width="70px" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" Width="70px" />
                                    </td>

                                    <td width="3%"><span class="field-label">FromDate</span>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="90px"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFromDate" PopupButtonID="imgFromDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="3%"><span class="field-label">ToDate</span>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="90px"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtToDate" PopupButtonID="imgToDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="2%"><span class="field-label">Vehicle</span>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtVehicleId" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="2%"><span class="field-label">Driver</span>
                                    </td>
                                    <td width="10%">
                                        <asp:TextBox ID="txtDriverId" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="2%"><span class="field-label">From</span>
                                    </td>
                                    <td width="6%">
                                        <asp:TextBox ID="txtFromId" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="2%"><span class="field-label">To</span>
                                    </td>
                                    <td width="6%">
                                        <asp:TextBox ID="txtToId" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="2%"><span class="field-label">Duty</span>
                                    </td>
                                    <td width="6%">
                                        <asp:TextBox ID="txtDutyId" runat="server"></asp:TextBox>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="GridViewShowDetails" runat="server"
                                CssClass="table table-bordered table-row mb-0" Width="100%"
                                EmptyDataText="No Data Found" AutoGenerateColumns="False"
                                DataKeyNames="tex_no" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("tex_no") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Date" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle Width="80px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="VEH_REGNO" HeaderText="Vehicle" />
                                    <asp:BoundField DataField="VEH_TYPE" HeaderText="Type" />
                                    <asp:BoundField DataField="DRIVER" HeaderText="Driver" />
                                    <asp:BoundField DataField="TEX_REF" HeaderText="Ref.No" />
                                    <asp:BoundField DataField="CUNIT" HeaderText="Charging Unit" />
                                    <asp:BoundField DataField="LOC_FROM" HeaderText="From" />
                                    <asp:BoundField DataField="LOC_TO" HeaderText="To" />
                                    <asp:BoundField DataField="DUTY" HeaderText="Duty" />
                                    <asp:BoundField DataField="DESCR" HeaderText="Description" />
                                    <asp:BoundField DataField="TEX_OSTART" HeaderText="Start" />
                                    <asp:BoundField DataField="TEX_OEND" HeaderText="End" />
                                    <asp:BoundField DataField="TEX_OTOTAL" HeaderText="Total" />
                                    <asp:BoundField DataField="TEX_TSTART" HeaderText="Start" />
                                    <asp:BoundField DataField="TEX_TEND" HeaderText="End" />
                                    <asp:BoundField DataField="TEX_TTOTAL" HeaderText="Total" />
                                    <asp:BoundField DataField="TEX_GROSS" HeaderText="Total" />
                                    <asp:BoundField DataField="TEX_SALIK" HeaderText="Salik" />
                                    <asp:BoundField DataField="TEX_NET" HeaderText="Total" />
                                    <asp:BoundField DataField="TEX_USER" HeaderText="User" />
                                    <asp:BoundField DataField="TEX_TEI_ID" HeaderText="Posted Id" />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTEXID" runat="server" Text='<%# Eval("TEX_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <br />
                            <asp:Repeater runat="server" ID="rptTotals">
                                <HeaderTemplate>
                                    <table cellpadding='5' cellspacing='5'>
                                        <tr>
                                            <td>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCaption" runat="server"><%#DataBinder.Eval(Container.DataItem, "actName")%></asp:Label>
                                    <asp:Label ID="lblValue" Width="60px" runat="server"><%#DataBinder.Eval(Container.DataItem, "actValue")%></asp:Label>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_mimimum" runat="server" />
                            <asp:HiddenField ID="h_TEI_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_VehicleType" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
