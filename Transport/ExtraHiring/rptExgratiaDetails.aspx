﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptExgratiaDetails.aspx.vb" Inherits="Transport_ExtraHiring_rptExgratiaDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Exgratia Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" style="width:100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="tblRate" class="BlueTableView" runat="server" align="center" style="width: 100%">
                                <tr>
                                    <td width="20%" align="left">
                                        <span class="field-label">
                                            <asp:Label ID="lblfdate" runat="server" Text="From Date"></asp:Label></span></td>

                                    <td width="30%" align="left">
                                        <asp:TextBox ID="txtFDate" runat="server" Enabled="false" ></asp:TextBox>
                                        <asp:ImageButton ID="lnkFDate" runat="server" ImageUrl="~/Images/calendar.gif"  ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFDate" PopupButtonID="lnkFDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td width="20%">
                                        <span class="field-label">
                                            <asp:Label ID="lbltdate" runat="server" Text="To Date"></asp:Label></span> 
                                    </td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtTDate" runat="server" Enabled="false" ></asp:TextBox>
                                        <asp:ImageButton ID="lnkTDate" runat="server" ImageUrl="~/Images/calendar.gif"  ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTDate" PopupButtonID="lnkTDate"></ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>


                                <tr id="Bsuid" runat="server">
                                    <td  align="left"><span class="field-label">Select BusinessUnit</span>  </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr id="Purpose" runat="server">
                                    <td  align="left"><span class="field-label">Select Purpose </span></td>

                                    <td  >
                                        <asp:DropDownList ID="ddlPurpose" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr id="trBSUnit" runat="server">
                                    <td align="left"><span class="field-label">Business Unit</span> </td>

                                    <td>
                                        <asp:DropDownList ID="ddlBsu1" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>                                       
                                    </td>
                                    <td> <asp:CheckBox ID="chkSummary" AutoPostBack="true" runat="server" Text="Summary" Visible="false"></asp:CheckBox></td>
                                    <td></td>

                                </tr>


                                <tr>
                                    <td colspan="4" align="center">

                                        <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" ValidationGroup="groupM1"></asp:Button>
                                    </td>
                                </tr>



                            </table>
                            &nbsp;&nbsp;
                                <input id="lstValues" name="lstValues" runat="server" type="hidden" />
                            &nbsp;
           
                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="h_ClientId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_Season" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_driver_id" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_SalesmanId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_VehicleTypeId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_Vehicle_id" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_ACTIDs" runat="server" />


                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>

</asp:Content>
