﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Transport_ExtraHiring_tptCreditNote
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200235" And ViewState("MainMnu_code") <> "T200240") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            BindDropDown()
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then

            End If




            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                If ViewState("datamode") = "add" Then
                    SetDataMode("add")
                Else
                    SetDataMode("view")
                End If
                setModifyvalues(ViewState("EntryId"))
                showNoRecordsFound()
            End If
        Else
            showNoRecordsFound()

            If grdInvoice.Rows(0).Cells(0).Text <> "No Record Found" Then
                txtAmount.Text = CNInvoices.Compute("sum(NCD_AMOUNT)", "")
            End If
        End If
    End Sub
    Private Sub BindDropDown()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String = "select ID,DESCR  FROM VW_CNINVOICETYPES order by descr "
            ddlInvoices.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlInvoices.DataTextField = "DESCR"
            ddlInvoices.DataValueField = "ID"
            ddlInvoices.DataBind()
            h_InvType.Value = ddlInvoices.SelectedItem.Text
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        btnCancel.Visible = Not ItemEditMode
        btnSAVE.Visible = True
        btnPrint.Visible = False
        If ViewState("MainMnu_code") = "T200235" Then
            btnPost.Visible = False
            'lblPost.Visible = False
            'lblPosted.Visible = False
        Else
            btnPost.Visible = True
            'lblPost.Visible = True
            'lblPosted.Visible = True
        End If


    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim Amount(3) As String
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = "0" Then
                txtCreditNoteNo.Text = "New"
                TrDetailsGrid.Visible = False
                TRDataEntryGrid.Visible = True
            Else

                Dim str_conn As String = connectionString
                Dim dt As New DataTable
                Dim NonGems As Boolean = False

                If ViewState("datamode") = "add" Then
                    'lblCreditNoteNo.Text = p_Modifyid
                    txtCreditNoteNo.Text = "New"
                    txtCNDate.Text = Now.ToString("dd/MMM/yyyy")
                    lblDate.Text = Now.ToString("dd/MMM/yyyy")
                    h_EntryId.Value = 0
                    TrDetailsGrid.Visible = False
                    TRDataEntryGrid.Visible = True
                Else
                    dt = MainObj.getRecords("select  NCN_ID, NCN_NO, NCN_TRDATE,NCN_AMOUNT, NCN_TYPE,NCN_BSU_ID, NCN_REMARKS, NCN_FYEAR, NCN_USER, NCN_DELETED, NCN_DATE, NCN_POSTED, NCN_JHD_DOCNO, NCN_POSTING_DATE,NCN_ACT_ID,NCN_ACT_NAME,NCN_CN_TYPE,isnull(NCN_OPCN_ACT_ID,'')NCN_OPCN_ACT_ID,isnull(ACT_NAME,'')ACT_NAME,NCN_STATUS from NESCreditNote left outer join oasisfin..accounts_m on act_id=ncn_opcn_act_id  WHERE NCN_ID ='" & p_Modifyid & "' and NCN_BSU_ID='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                    If dt.Rows.Count > 0 Then
                        txtCNDate.Text = Format(IIf(IsDBNull(dt.Rows(0)("NCN_TRDATE")), Now.Date.ToString, dt.Rows(0)("NCN_TRDATE")), "dd/MMM/yyyy")
                        txtCreditNoteNo.Text = dt.Rows(0)("NCN_NO")
                        txtremarks.Text = dt.Rows(0)("NCN_REMARKS")
                        txtAmount.Text = dt.Rows(0)("NCN_AMOUNT")
                        txtCustomer.Text = dt.Rows(0)("NCN_ACT_ID")
                        h_Client_ID.Value = dt.Rows(0)("NCN_ACT_ID")
                        txtCustomerName.Text = dt.Rows(0)("NCN_ACT_NAME")
                        rblCustomerType.Text = dt.Rows(0)("NCN_TYPE")
                        rblCustomerType.Enabled = False
                        rbCNType.Text = dt.Rows(0)("NCN_CN_TYPE")

                        If rbCNType.Text = "0" Then
                            rbCNType.Enabled = True
                        Else
                            rbCNType.Enabled = False
                        End If
                        txtGLCode.Text = dt.Rows(0)("NCN_OPCN_ACT_ID")
                        txtGLName.Text = dt.Rows(0)("ACT_NAME")

                        If ViewState("MainMnu_code") = "T200235" Then
                            If dt.Rows(0)("NCN_STATUS") = "P" Then
                                TrDetailsGrid.Visible = False
                                TRDataEntryGrid.Visible = True
                            Else
                                btnSAVE.Visible = False
                                btnPrint.Visible = False
                                TrDetailsGrid.Visible = True
                                TRDataEntryGrid.Visible = False
                            End If
                        Else
                            TrDetailsGrid.Visible = True
                            TRDataEntryGrid.Visible = False
                        End If

                        If dt.Rows(0)("NCN_POSTED") = "0" Then
                            If ViewState("MainMnu_code") <> "T200235" Then
                                btnPost.Visible = True
                                btnPrint.Visible = False
                            End If
                            'lblPosted.Text = "Not Posted"
                        Else
                            If ViewState("MainMnu_code") <> "T200235" Then
                                btnPost.Visible = False
                                btnPrint.Visible = True

                            End If
                        End If

                        If ViewState("MainMnu_code") = "T200240" Then
                            btnSAVE.Visible = False
                        End If

                        ImageCustomer.Enabled = True
                    Else
                        ImageCustomer.Enabled = False
                    End If
                    h_EntryId.Value = dt.Rows(0)("NCN_ID")

                End If
            End If
            fillGridView(grdInvoice, "select NCD_ID,NCD_NCN_ID,NCD_INV_NO,NCD_INV_AMOUNT,NCD_AMOUNT,NCD_JHD_DOC_NO from NESCreditNote_Details where NCD_NCN_ID=" & h_EntryId.Value & " order by NCD_Id")
            fillGridView1(DisplayData, "select NCD_INV_NO,NCD_INV_AMOUNT,NCD_AMOUNT,isnull(NCD_CN_DOC_NO,'Not Posted')NCD_CN_DOC_NO from NESCreditNote_Details where NCD_NCN_ID=" & h_EntryId.Value & " order by NCD_Id")
            showNoRecordsFound()
            showNoRecordsFound1()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        CNInvoices = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(CNInvoices)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        CNInvoices = mtable
        fillGrdView.DataSource = CNInvoices
        fillGrdView.DataBind()
    End Sub
    Private Sub fillGridView1(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        CNInvoices1 = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(CNInvoices1)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        CNInvoices1 = mtable
        fillGrdView.DataSource = CNInvoices1
        fillGrdView.DataBind()
    End Sub
    Private Sub showNoRecordsFound()
        Try
            If CNInvoices.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdInvoice.Columns.Count - 1
                grdInvoice.Rows(0).Cells.Clear()
                grdInvoice.Rows(0).Cells.Add(New TableCell())
                grdInvoice.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdInvoice.Rows(0).Cells(0).Text = "No Record Found"
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub showNoRecordsFound1()
        Try
            If CNInvoices1.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = DisplayData.Columns.Count - 1
                DisplayData.Rows(0).Cells.Clear()
                DisplayData.Rows(0).Cells.Add(New TableCell())
                DisplayData.Rows(0).Cells(0).ColumnSpan = TotalColumns
                DisplayData.Rows(0).Cells(0).Text = "No Record Found"
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub ClearDetails()
        h_EntryId.Value = "0"
        txtCNDate.Text = Now.ToString("dd/MMM/yyyy")
        txtCreditNoteNo.Text = ""
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Private Property EGCFooter() As DataTable
        Get
            Return ViewState("EGCFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EGCFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property
    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox
        Dim total As Double = 0.0
        Dim gross As Double = 0.0
        Dim salik As Double = 0.0
        Dim net As Double = 0.0
        Dim NonGems As Boolean = False
        Dim ModifiedAmount As Double = 0.0

        If txtAmount.Text = 0 Then
            lblError.Text = "0 Amount Invoices are not Posted !!!"
            Exit Sub
        End If

        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If
        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(7) As SqlParameter

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Dim RetVal As String = ""

        Try
            If rblCustomerType.Text = 0 Then ' Customer Posting
                If rbCNType.Text = 0 Then 'Open Type
                    pParms(1) = Mainclass.CreateSqlParameter("@NCN_ID", h_EntryId.Value, SqlDbType.Int)
                    pParms(2) = Mainclass.CreateSqlParameter("@FLAG", "", SqlDbType.VarChar)
                    pParms(3) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                    pParms(4) = Mainclass.CreateSqlParameter("@CNTYPE", "OPEN", SqlDbType.VarChar)
                    pParms(5) = Mainclass.CreateSqlParameter("@POSTING_DATE", txtCNDate.Text, SqlDbType.VarChar)
                    pParms(6) = Mainclass.CreateSqlParameter("@YEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    pParms(7) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    RetVal = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostCreditNote", pParms)
                    If RetVal = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    Else
                        RetVal = "0"
                    End If

                Else
                    Dim PostCNs As DataTable
                    PostCNs = Mainclass.getDataTable("select distinct SUBSTRING(NCD_INV_NO,3,2) Trno from NESCreditNote_Details where NCD_NCN_ID=" & h_EntryId.Value, connectionString)
                    For rowNum As Int16 = 0 To PostCNs.Rows.Count - 1 'Invoice  Type
                        pParms(1) = Mainclass.CreateSqlParameter("@NCN_ID", h_EntryId.Value, SqlDbType.Int)
                        pParms(2) = Mainclass.CreateSqlParameter("@FLAG", PostCNs.Rows(rowNum)("Trno"), SqlDbType.VarChar)
                        pParms(3) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                        pParms(4) = Mainclass.CreateSqlParameter("@CNTYPE", "INVOICE", SqlDbType.VarChar)
                        pParms(5) = Mainclass.CreateSqlParameter("@POSTING_DATE", txtCNDate.Text, SqlDbType.VarChar)
                        pParms(6) = Mainclass.CreateSqlParameter("@YEAR", Session("F_YEAR"), SqlDbType.VarChar)
                        pParms(7) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        RetVal = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostCreditNote", pParms)
                        If RetVal = "-1" Then
                            lblError.Text = "Unexpected Error !!!"
                            stTrans.Rollback()
                            Exit Sub
                        Else
                            RetVal = "0"
                        End If
                    Next
                End If
            Else ' Businessunit  Posting
                Dim PostCNIJV As DataTable
                PostCNIJV = Mainclass.getDataTable("select distinct SUBSTRING(NCD_INV_NO,3,2) Trno from NESCreditNote_Details where NCD_NCN_ID=" & h_EntryId.Value, connectionString)
                For rowNum As Int16 = 0 To PostCNIJV.Rows.Count - 1 'Invoice  Type
                    pParms(1) = Mainclass.CreateSqlParameter("@NCN_ID", h_EntryId.Value, SqlDbType.Int)
                    pParms(2) = Mainclass.CreateSqlParameter("@FLAG", PostCNIJV.Rows(rowNum)("Trno"), SqlDbType.VarChar)
                    pParms(3) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                    pParms(4) = Mainclass.CreateSqlParameter("@CNTYPE", "INVOICE", SqlDbType.VarChar)
                    pParms(5) = Mainclass.CreateSqlParameter("@POSTING_DATE", txtCNDate.Text, SqlDbType.VarChar)
                    pParms(6) = Mainclass.CreateSqlParameter("@YEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    pParms(7) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    RetVal = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostCreditNoteIJV", pParms)
                    If RetVal = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    Else
                        RetVal = "0"
                    End If
                Next
            End If

            If RetVal = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                lblError.Text = "Data Saved Successfully !!!"
            Else
                stTrans.Rollback()
                Exit Sub
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim TRN_NO As String = CType(h_EntryId.Value, String)
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptCreditNoteInvoice"
            Dim sqlParam(2) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@NCN_ID", h_EntryId.Value, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)

            cmd.Parameters.Add(sqlParam(0))
            cmd.Parameters.Add(sqlParam(1))
            cmd.Parameters.Add(sqlParam(2))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            'V1.2 comments start------------
            Dim params As New Hashtable
            Dim InvNo As String = ""
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "CREDIT NOTE INVOICE"
            params("LPO") = ""
            params("InvNo") = txtCreditNoteNo.Text
            params("InvDate") = txtCNDate.Text
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            If Session("sBsuid") = "900501" Then
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptCreditNoteInvoice.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptCreditNoteInvoiceBBT.rpt"
            End If
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub UpdateAddress()
    End Sub

    Protected Sub btnSAVE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSAVE.Click
        saveData()
    End Sub
    Private Sub saveData()
        Dim Mclose As Date
        lblError.Text = ""

        Mclose = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
        If Mclose >= CDate(txtCNDate.Text) Then lblError.Text = "This Month is Closed,"

        If txtCustomer.Text = "" Then lblError.Text = "Select One Customer,"
        If txtremarks.Text = "" Then lblError.Text = "Enter Remarks,"

        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & ",So please Change"
            lblError.Visible = True
            Return
        End If
        Try
            Dim pParms(15) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@NCN_ID", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@NCN_TRDATE", txtCNDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@NCN_AMOUNT", txtAmount.Text, SqlDbType.Decimal)
            pParms(4) = Mainclass.CreateSqlParameter("@NCN_TYPE", rblCustomerType.Text, SqlDbType.Int)
            pParms(5) = Mainclass.CreateSqlParameter("@NCN_BSU_ID", Session("sBsuid"), SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@NCN_REMARKS", txtremarks.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@NCN_ACT_ID", txtCustomer.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@NCN_CN_TYPE", rbCNType.Text, SqlDbType.Int)
            pParms(9) = Mainclass.CreateSqlParameter("@NCN_ACT_NAME", txtCustomerName.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@NCN_OPCN_ACT_ID", txtGLCode.Text, SqlDbType.VarChar)
            pParms(11) = Mainclass.CreateSqlParameter("@NCN_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@NCN_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVENESCREDITNOTE", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    'stTrans.Commit()
                    If h_EntryId.Value = 0 Then
                        h_EntryId.Value = pParms(1).Value
                    End If

                    Dim RowCount As Integer
                    For RowCount = 0 To CNInvoices.Rows.Count - 1
                        If CNInvoices.Rows(0)(1) <> -1 Then
                            Dim iParms(6) As SqlClient.SqlParameter
                            Dim rowState As Integer = ViewState("EntryId")
                            If CNInvoices.Rows(RowCount).RowState = 8 Then rowState = -1

                            If ViewState("datamode") = "add" Then
                                iParms(1) = Mainclass.CreateSqlParameter("@NCD_Id", 0, SqlDbType.Int)
                            Else
                                iParms(1) = Mainclass.CreateSqlParameter("@NCD_ID", CNInvoices.Rows(RowCount)("NCD_Id"), SqlDbType.Int)
                            End If
                            iParms(2) = Mainclass.CreateSqlParameter("@NCD_NCN_ID", h_EntryId.Value, SqlDbType.Int)
                            iParms(3) = Mainclass.CreateSqlParameter("@NCD_INV_NO", CNInvoices.Rows(RowCount)("NCD_INV_NO"), SqlDbType.VarChar)
                            iParms(4) = Mainclass.CreateSqlParameter("@NCD_INV_AMOUNT", CNInvoices.Rows(RowCount)("NCD_INV_AMOUNT"), SqlDbType.VarChar)
                            iParms(5) = Mainclass.CreateSqlParameter("@NCD_AMOUNT", CNInvoices.Rows(RowCount)("NCD_AMOUNT"), SqlDbType.Decimal)
                            iParms(6) = Mainclass.CreateSqlParameter("@NCD_JHD_DOC_NO", CNInvoices.Rows(RowCount)("NCD_JHD_DOC_NO"), SqlDbType.VarChar)

                            Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVENESCREDITNOTEDETAILS", iParms)
                            If RetValFooter = "-1" Then
                                lblError.Text = "Unexpected Error !!!"
                                stTrans.Rollback()
                                Exit Sub
                            End If
                        End If
                    Next

                    If hGridDelete.Value <> "0" Then
                        Dim deleteId() As String = hGridDelete.Value.Split(";")
                        Dim iParms(2) As SqlClient.SqlParameter
                        For RowCount = 0 To deleteId.GetUpperBound(0)
                            If deleteId(RowCount) <> "" Then
                                iParms(1) = Mainclass.CreateSqlParameter("@Inv_Id", deleteId(RowCount), SqlDbType.Int)
                                iParms(2) = Mainclass.CreateSqlParameter("@Inv_Trn_No", ViewState("EntryId"), SqlDbType.Int)
                                Dim RetValFooter As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "delete from NESCreditNote_Details where NCD_ID=@Inv_Id and NCD_NCN_ID=@Inv_Trn_No", iParms)
                                If RetValFooter = "-1" Then
                                    lblError.Text = "Unexpected Error !!!"
                                    stTrans.Rollback()
                                    Exit Sub
                                End If
                            End If


                        Next
                    End If

                    stTrans.Commit()
                    lblError.Text = "Data Saved Successfully !!!"
                    ViewState("datamode") = "view"
                    setModifyvalues(h_EntryId.Value)
                    btnSAVE.Enabled = False
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Property CNInvoices() As DataTable
        Get
            Return ViewState("CNInvoices")
        End Get
        Set(ByVal value As DataTable)
            ViewState("CNInvoices") = value
        End Set
    End Property

    Private Property CNInvoices1() As DataTable
        Get
            Return ViewState("CNInvoices1")
        End Get
        Set(ByVal value As DataTable)
            ViewState("CNInvoices1") = value
        End Set
    End Property
    Protected Sub grdInvoice_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoice.RowCancelingEdit
        grdInvoice.ShowFooter = True
        grdInvoice.EditIndex = -1
        grdInvoice.DataSource = CNInvoices
        grdInvoice.DataBind()
    End Sub
    Protected Sub grdInvoice_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdInvoice.RowCommand
        Dim TotalAmount As Long = 0.0
        If e.CommandName = "AddNew" Then
            Dim txtInvNo As TextBox = grdInvoice.FooterRow.FindControl("txtInvoiceNo")
            Dim txtInvAmount As TextBox = grdInvoice.FooterRow.FindControl("txtInvoiceAmount")
            Dim txtAmount As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Amt")
            Dim h_InvNo As HiddenField = grdInvoice.FooterRow.FindControl("h_INV_NO")
            Dim h_InvAmount As HiddenField = grdInvoice.FooterRow.FindControl("h_INV_AMOUNT")
            Dim h_Doc_No_F As HiddenField = grdInvoice.FooterRow.FindControl("h_Doc_No")


            If h_InvNo.Value = txtInvNo.Text And Val(h_InvAmount.Value) <> Val(txtInvAmount.Text) Then lblError.Text &= "You Can not Edit the Only Invoice Amount"
            If Val(txtAmount.Text) > Val(txtInvAmount.Text) Then lblError.Text &= "Amount should be less than the Invoice Amount,"
            If Not IsNumeric(txtAmount.Text) Then lblError.Text &= "Enter Amount Only"
            If Not IsNumeric(txtInvAmount.Text) Then lblError.Text &= "Enter Amount Only"
            If txtInvNo.Text = "" Then lblError.Text &= "Select Invoice,"
            If txtAmount.Text = "" Then lblError.Text &= "Enter Amount,"
            If rbCNType.Text = "0" Then lblError.Text &= "You can not add Invoices to Open Credit Note,"
            If lblError.Text.Length > 0 Then
                showNoRecordsFound()
                Exit Sub
            End If
            If CNInvoices.Rows(0)(1) = -1 Then CNInvoices.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = CNInvoices.NewRow
            mrow("NCD_ID") = 0
            mrow("NCD_INV_NO") = txtInvNo.Text
            mrow("NCD_INV_AMOUNT") = txtInvAmount.Text
            mrow("NCD_AMOUNT") = txtAmount.Text
            mrow("NCD_JHD_DOC_NO") = h_Doc_No_F.Value

            CNInvoices.Rows.Add(mrow)
            grdInvoice.EditIndex = -1
            grdInvoice.DataSource = CNInvoices
            grdInvoice.DataBind()
            showNoRecordsFound()
            TotalAmount = CNInvoices.Compute("sum(NCD_AMOUNT)", "")
        Else
            Dim txtInvNo As TextBox = grdInvoice.FooterRow.FindControl("txtInvoiceNo")
            Dim txtInvAmount As TextBox = grdInvoice.FooterRow.FindControl("txtInvoiceAmount")
            Dim txtAmount As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Amt")
        End If
        txtAmount.Text = TotalAmount
    End Sub
    Protected Sub grdInvoice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoice.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Or e.Row.RowType = DataControlRowType.DataRow Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
            Dim txtInvNo As TextBox = e.Row.FindControl("txtInvoiceNo")
            Dim txtInvAmount As TextBox = e.Row.FindControl("txtInvoiceAmount")
            Dim txtAmount As TextBox = e.Row.FindControl("txtInv_Amt")
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim txtInv_Rate_F As TextBox = e.Row.FindControl("txtInvoiceNo")
                Dim txtInv_Qty_F As TextBox = e.Row.FindControl("txtInvoiceAmount")
                Dim imgInvoice As ImageButton = e.Row.FindControl("imgInvoice_F")
                Dim txtProduct_F As TextBox = e.Row.FindControl("txtInv_Amt")
                Dim h_inv_no_F As HiddenField = e.Row.FindControl("h_INV_NO")
                Dim h_inv_AMOUNT_F As HiddenField = e.Row.FindControl("h_INV_AMOUNT")
                Dim h_Doc_No_F As HiddenField = e.Row.FindControl("h_Doc_No")
                imgInvoice.Attributes.Add("OnClick", "javascript:return getProduct('" & txtInvNo.ClientID & "','" & txtInvAmount.ClientID & "','" & h_inv_no_F.ClientID & "','" & h_inv_AMOUNT_F.ClientID & "','" & h_Doc_No_F.ClientID & "')")
            End If
            If grdInvoice.ShowFooter Or grdInvoice.EditIndex > -1 Then '(e.Row.RowType = DataControlRowType.Footer And grdInvoice.ShowFooter) Or (grdInvoice.EditIndex = e.Row.RowIndex And grdInvoice.EditIndex > -1)
                Dim txtInv_No_E As TextBox = e.Row.FindControl("txtInvoiceNo")
                Dim txtInv_Amt_E As TextBox = e.Row.FindControl("txtInvoiceAmount")
                Dim imgInvoice_E As ImageButton = e.Row.FindControl("imgInvoice_e")
                Dim txtAmt_E As TextBox = e.Row.FindControl("txtInv_Amt")
                'imgInvoice_E.Attributes.Add("OnClick", "javascript:return getProduct('" & txtInv_No_E.ClientID & "','" & txtInv_Amt_E.ClientID & "')")
            End If
        End If
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
    End Sub
    Protected Sub grdInvoice_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdInvoice.RowDeleting
        Dim mRow() As DataRow = CNInvoices.Select("ID=" & grdInvoice.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            hGridDelete.Value &= ";" & mRow(0)("NCD_Id")
            CNInvoices.Select("ID=" & grdInvoice.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            CNInvoices.AcceptChanges()
        End If
        If CNInvoices.Rows.Count = 0 Then
            CNInvoices.Rows.Add(CNInvoices.NewRow())
            CNInvoices.Rows(0)(1) = -1
            txtAmount.Text = "0"
        Else
            txtAmount.Text = CNInvoices.Compute("sum(NCD_AMOUNT)", "")
        End If
        grdInvoice.DataSource = CNInvoices
        grdInvoice.DataBind()
        showNoRecordsFound()
    End Sub

    Protected Sub grdInvoice_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdInvoice.RowEditing
        Try
            grdInvoice.ShowFooter = False
            grdInvoice.EditIndex = e.NewEditIndex
            grdInvoice.DataSource = CNInvoices
            grdInvoice.DataBind()
            Dim txtInv_No_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtInvoiceNo")
            Dim txtInvAmount_F As TextBox = grdInvoice.Rows(e.NewEditIndex).FindControl("txtInvoiceAmount")
            Dim imgInvoice As ImageButton = grdInvoice.Rows(e.NewEditIndex).FindControl("imgInvoice_F")
            Dim txtInv_Product As TextBox = grdInvoice.FooterRow.FindControl("txtInv_Amt")
            imgInvoice.Visible = True
            'imgSubLedger.Attributes.Add("OnClick", "javascript:return getProduct('" & txtProduct_F.ClientID & "','" & txtInv_Rate_F.ClientID & "','" & h_PRODUCT_ID_F.ClientID & "','" & txtInv_Qty_F.ClientID & "')")
            imgInvoice.Attributes.Add("OnClick", "javascript:return getProduct('" & txtInv_No_F.ClientID & "','" & txtInvAmount_F.ClientID & "')")

        Catch ex As Exception
        End Try
    End Sub
    Protected Sub grdInvoice_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdInvoice.RowUpdating
        Dim s As String = grdInvoice.DataKeys(e.RowIndex).Value.ToString()

        Dim lblNCD_id As Label = grdInvoice.Rows(e.RowIndex).FindControl("lblNCD_ID")

        Dim txtInvoiceNo As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInvoiceNo")
        Dim txtInvoiceAmount As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInvoiceAmount")
        Dim txtInv_Amt As TextBox = grdInvoice.Rows(e.RowIndex).FindControl("txtInv_Amt")
        If Val(txtInv_Amt.Text) > Val(txtInvoiceAmount.Text) Then lblError.Text &= " Amount should be less than the Invoice Amount"
        If txtInvoiceNo.Text = "" Then lblError.Text &= " Select Invoice,"
        If txtInv_Amt.Text = "" Then lblError.Text &= " Enter Amount,"

        If lblError.Text.Length > 0 Then
            showNoRecordsFound()
            Exit Sub
        End If
        Dim mrow As DataRow
        mrow = CNInvoices.Select("ID=" & s)(0)
        mrow("NCD_ID") = lblNCD_id.Text
        'mrow("NCD_NCN_ID") = lblNCD_NCN_ID.Text
        mrow("NCD_INV_NO") = txtInvoiceNo.Text
        mrow("NCD_INV_AMOUNT") = txtInvoiceAmount.Text
        mrow("NCD_AMOUNT") = txtInv_Amt.Text

        grdInvoice.EditIndex = -1
        grdInvoice.ShowFooter = True
        grdInvoice.DataSource = CNInvoices
        grdInvoice.DataBind()
        txtAmount.Text = CNInvoices.Compute("sum(NCD_AMOUNT)", "")
    End Sub
    Protected Sub ddlInvoices_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInvoices.SelectedIndexChanged
        h_InvType.Value = ddlInvoices.SelectedItem.Text
    End Sub
    Protected Sub rblCustomerType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblCustomerType.SelectedIndexChanged
        If rblCustomerType.Text = "1" Then
            rbCNType.Text = 1
            rbCNType.Enabled = False
            txtGLCode.Text = ""
            txtGLName.Text = ""
            imgGLCode.Enabled = False
        Else
            rbCNType.Text = 0
            rbCNType.Enabled = True
            imgGLCode.Enabled = True
        End If

    End Sub
    Protected Sub rbCNType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCNType.SelectedIndexChanged
        If rbCNType.Text = "0" Then
            imgGLCode.Enabled = True
        Else
            txtGLCode.Text = ""
            txtGLName.Text = ""
            imgGLCode.Enabled = False
        End If
    End Sub
End Class



