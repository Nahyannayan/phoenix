﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime
Imports System.Text
Imports GridViewHelper
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Partial Class Transport_ExtraHiring_TptAttendantCharges

    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim dtVATUserAccess As New DataTable


                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200151") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtBasic.Attributes.Add("onblur", " return calc('O');")
                txtFirst.Attributes.Add("onblur", " return calc('O');")
                txtOther.Attributes.Add("onblur", " return calc('O');")
                txtMobile.Attributes.Add("onblur", " return calc('O');")
                txtUtility.Attributes.Add("onblur", " return calc('O');")
                txtMedical.Attributes.Add("onblur", " return calc('O');")
                txtAirafare.Attributes.Add("onblur", " return calc('O');")
                txtAccom.Attributes.Add("onblur", " return calc('O');")
                txtVisa.Attributes.Add("onblur", " return calc('O');")
                txtGratuity.Attributes.Add("onblur", " return calc('O');")
                txtUniform.Attributes.Add("onblur", " return calc('O');")
                txtDiscount.Attributes.Add("onblur", " return calc('O');")

                clearScreen()
                sqlStr = "select TAC_ID [ID], CONVERT(VARCHAR(20),TAC_TRDATE,106) DATE, TAC_EMP_ID [EMPLOYEE ID],isnull(EMP_FNAME +' ' +EMP_MNAME +' ' +EMP_LNAME ,'') [EMPLOYEE NAME],EMPNO [OASIS ID], "
                sqlStr &= " TAC_DRIVERTYPE [DRIVER TYPE], TAC_BASIC BASIC,  TAC_FIRSTALLOW [FIRST ALLOW], TAC_SPLALLOW [SPL ALLOW], "
                sqlStr &= " TAC_MOBILE MOBILE,TAC_UTILITY UTILITY, TAC_MEDICAL MEDICAL, TAC_AIRFARE AIRFARE, TAC_ACCOMMODATION ACCOMMODATION, "
                sqlStr &= " TAC_VISA VISA, TAC_GRATUITY GRATUITY, TAC_UNIFORM UNIFORM, TAC_CTC CTC, TAC_OVERHEAD [OVER HEAD],TAC_DISCOUNT DISCOUNT,TAC_TOTALCHARGING [TOTAL CHARGING], "
                sqlStr &= " TAC_INVNO [INV NO],TAC_JHD_DOCNO [IJV NO],TAC_REMARKS REMARKS,"
                sqlStr &= " TAC_NOTES NOTES,TAC_USER [USER NAME],TAC_TLC_ID,TAC_EMP_ID,TAC_DRIVERTYPE,TLC_DESCR CUSTOMER "
                sqlStr &= ",isnull(DESCR,'---SELECT ONE---') DAX_DESCR,isnull(TAC_PRGM_DIM_CODE,'0') DAX_ID,TAC_TAX_AMOUNT,TAC_TAX_NET_AMOUNT,TAC_TAX_CODE,TAC_POSTING_DATE "
                sqlStr &= ",isnull(TAC_EMR_CODE,'NA')TAC_EMR_CODE,EMR_DESCR EMIRATE"
                sqlStr &= " FROM TPTATTENDANTCHARGES  inner join TPTLEASECUSTOMER  on TLC_ID=TAC_TLC_ID "
                sqlStr &= " left outer join dbo.VV_DAX_CODES on ID=TAC_PRGM_DIM_CODE "
                sqlStr &= " INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=TAC_EMP_ID  "
                sqlStr &= " left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE= isnull(TAC_EMR_CODE,'NA') "


                BindDriver()
                BindBsu()
                BindDAXCodes()
                radFilterBSU.SelectedValue = 0
                refreshGrid()

                dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='TPT'", "OASIS_TRANSPORTConnectionString")
                If dtVATUserAccess.Rows.Count > 0 Then
                    If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                        radVAT.Enabled = True
                        RadEmirate.Enabled = True
                    Else
                        radVAT.Enabled = False
                        RadEmirate.Enabled = False
                    End If
                Else
                    radVAT.Enabled = False
                    RadEmirate.Enabled = False
                End If
                CalculateVAT(RadCustomer.SelectedValue)
                SelectEmirate(RadCustomer.SelectedValue)

                If Request.QueryString("VIEWID") <> "" Then
                    h_PRI_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_PRI_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
            'Testing for Connectivyt
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub BindDAXCodes()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsq2 As String = ""
            Dim BSUParms(2) As SqlParameter
            strsql = "select ID,DESCR from VV_DAX_CODES ORDER BY ID "

            strsq2 = "select EMR_CODE ID,EMR_DESCR DESCR from OASIS.TAX.VW_TAX_EMIRATES order by EMR_DESCR"
            RadDAXCodes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadDAXCodes.DataTextField = "DESCR"
            RadDAXCodes.DataValueField = "ID"
            RadDAXCodes.DataBind()

            RadEmirate.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsq2)
            RadEmirate.DataTextField = "DESCR"
            RadEmirate.DataValueField = "ID"
            RadEmirate.DataBind()


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub calcSelectedTotal()
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblVatAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblVatNetAmt"), Label)
            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                VAT += Val(lblVAT.Text)
                Net += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAT.Text = VAT
        txtSelNet.Text = Net
    End Sub

    Protected Sub Check_All(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            Dim lblVAT As Label = TryCast(gvr.FindControl("lblVatAmt"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblVatNetAmt"), Label)

            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
                VAT += Val(lblVAT.Text)
                Net += Val(lblNet.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAT.Text = VAT
        txtSelNet.Text = Net
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub
    Private Sub refreshGrid()
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")


        If rblSelection.SelectedValue = 0 Then

            If radFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TAC_TRDATE)= MONTH('" & txtDate.Text & "') AND TAC_DELETED=0 and TAC_BSU_ID='" & Session("sBsuid") & "' and TAC_FYEAR='" & Session("F_YEAR") & "' and isnull(TAC_POSTED,0)=0 "

            Else
                sqlWhere = " where  MONTH(TAC_TRDATE)= MONTH('" & txtDate.Text & "') AND TAC_DELETED=0 and TAC_BSU_ID='" & Session("sBsuid") & "' and TAC_FYEAR='" & Session("F_YEAR") & "' and isnull(TAC_POSTED,0)=0 and TAC_TLC_ID='" & radFilterBSU.SelectedValue & "'"
            End If
        ElseIf rblSelection.SelectedValue = "1" Then

            If radFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TAC_TRDATE)= MONTH('" & txtDate.Text & "') AND TAC_DELETED=0 and TAC_BSU_ID='" & Session("sBsuid") & "' and TAC_FYEAR='" & Session("F_YEAR") & "' and isnull(TAC_POSTED,0)=1 "
            Else
                sqlWhere = " where  MONTH(TAC_TRDATE)= MONTH('" & txtDate.Text & "') AND TAC_DELETED=0 and TAC_BSU_ID='" & Session("sBsuid") & "' and TAC_FYEAR='" & Session("F_YEAR") & "' and isnull(TAC_POSTED,0)=1 and TAC_TLC_ID='" & radFilterBSU.SelectedValue & "'"
            End If

        ElseIf rblSelection.SelectedValue = "2" Then
            If radFilterBSU.SelectedValue = "0" Then
                sqlWhere = " where  MONTH(TAC_TRDATE)= MONTH('" & txtDate.Text & "') AND TAC_DELETED=0 and TAC_BSU_ID='" & Session("sBsuid") & "' and TAC_FYEAR='" & Session("F_YEAR") & "' "
            Else
                sqlWhere = " where  MONTH(TAC_TRDATE)= MONTH('" & txtDate.Text & "') AND TAC_DELETED=0 and TAC_BSU_ID='" & Session("sBsuid") & "' and TAC_FYEAR='" & Session("F_YEAR") & "' and TAC_TLC_ID='" & radFilterBSU.SelectedValue & "'"
            End If

        End If



        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTATTENDANTCHARGES = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
            Session("myData") = TPTATTENDANTCHARGES
            SetExportFileLink()
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub clearScreen()
        h_PRI_ID.Value = "0"
        txtDriver.Text = ""
        txtDriverName.Text = ""
        txtDriverType.Text = ""
        txtBasic.Text = "0"
        txtFirst.Text = "0"
        txtOther.Text = "0"
        txtMobile.Text = "0"
        txtUtility.Text = "0"
        txtMedical.Text = "0"
        txtAirafare.Text = "0"
        txtAccom.Text = "0"
        txtVisa.Text = "0"
        txtGratuity.Text = "0"
        txtUniform.Text = "0"
        txtCTC.Text = "0"
        txtOverHead.Text = "0"
        txtTotalCharges.Text = "0"
        txtAltDriver.Text = ""
        txtDiscount.Text = "0"
        TxtNotes.Text = ""
        RadDAXCodes.SelectedValue = "0"
        txtVATAmount.Text = "0.0"
        txtNetAmount.Text = "0.0"
        CalculateVAT(RadCustomer.SelectedValue)
        SelectEmirate(RadCustomer.SelectedValue)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If h_TEI_ID.Value <> 0 Then lblError.Text &= " This month extra trips are posted already.You can not enter for this month, "

        If RadDAXCodes.SelectedValue = "0" Then lblError.Text &= " please select DAX Posting Code, "
        If PurposeExists = 0 Then lblError.Text &= "Purpose is "
        If RadEmirate.SelectedValue = "NA" Then lblError.Text &= "Please Select Emirate....!"

        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            'lblError.Visible = True

            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Try
            Dim pParms(32) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TAC_ID", h_PRI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TAC_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@TAC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@TAC_TLC_ID", RadCustomer.SelectedValue, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TAC_DRIVER", txtDriver.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@TAC_DRIVERTYPE", txtDriverType.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@TAC_BASIC", txtBasic.Text, SqlDbType.Float)
            pParms(8) = Mainclass.CreateSqlParameter("@TAC_FIRSTALLOW", txtFirst.Text, SqlDbType.Float)
            pParms(9) = Mainclass.CreateSqlParameter("@TAC_SPLALLOW", txtOther.Text, SqlDbType.Float)
            pParms(10) = Mainclass.CreateSqlParameter("@TAC_MOBILE", txtMobile.Text, SqlDbType.Float)
            pParms(11) = Mainclass.CreateSqlParameter("@TAC_UTILITY", txtUtility.Text, SqlDbType.Float)
            pParms(12) = Mainclass.CreateSqlParameter("@TAC_MEDICAL", txtMedical.Text, SqlDbType.Float)
            pParms(13) = Mainclass.CreateSqlParameter("@TAC_AIRFARE", txtAirafare.Text, SqlDbType.Float)
            pParms(14) = Mainclass.CreateSqlParameter("@TAC_ACCOMMODATION", txtAccom.Text, SqlDbType.Float)
            pParms(15) = Mainclass.CreateSqlParameter("@TAC_VISA", txtVisa.Text, SqlDbType.Float)
            pParms(16) = Mainclass.CreateSqlParameter("@TAC_GRATUITY", txtGratuity.Text, SqlDbType.Float)
            pParms(17) = Mainclass.CreateSqlParameter("@TAC_UNIFORM", txtUniform.Text, SqlDbType.Float)
            pParms(19) = Mainclass.CreateSqlParameter("@TAC_CTC", txtCTC.Text, SqlDbType.Float)
            pParms(20) = Mainclass.CreateSqlParameter("@TAC_OVERHEAD", txtOverHead.Text, SqlDbType.Float)
            pParms(21) = Mainclass.CreateSqlParameter("@TAC_TOTALCHARGING", txtTotalCharges.Text, SqlDbType.Float)
            pParms(22) = Mainclass.CreateSqlParameter("@TAC_REMARKS", txtAltDriver.Text, SqlDbType.VarChar)
            pParms(23) = Mainclass.CreateSqlParameter("@TAC_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(24) = Mainclass.CreateSqlParameter("@TAC_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(25) = Mainclass.CreateSqlParameter("@POSTING", 0, SqlDbType.Int)
            pParms(26) = Mainclass.CreateSqlParameter("@TAC_DISCOUNT", txtDiscount.Text, SqlDbType.Float)
            pParms(27) = Mainclass.CreateSqlParameter("@TAC_NOTES", TxtNotes.Text, SqlDbType.VarChar)
            pParms(28) = Mainclass.CreateSqlParameter("@TAC_PRGM_DIM_CODE", RadDAXCodes.SelectedValue, SqlDbType.VarChar)
            pParms(29) = Mainclass.CreateSqlParameter("@TAC_TAX_AMOUNT", txtVATAmount.Text, SqlDbType.Float)
            pParms(30) = Mainclass.CreateSqlParameter("@TAC_TAX_NET_AMOUNT", txtNetAmount.Text, SqlDbType.Float)
            pParms(31) = Mainclass.CreateSqlParameter("@TAC_TAX_CODE", radVAT.SelectedValue, SqlDbType.VarChar)
            pParms(32) = Mainclass.CreateSqlParameter("@TAC_EMR_CODE", RadEmirate.SelectedValue, SqlDbType.VarChar)


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTATTENDANTCHARGES", pParms)
                If RetVal = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ID, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)

                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    Else
                        stTrans.Commit()
                        clearScreen()
                        refreshGrid()
                        'lblError.Text = "Data Saved Successfully !!!"
                        usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub showEdit(ByVal showId As Integer)
        Dim rdrTrnsprt As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr & "  where TAC_ID='" & showId & "'")

        If rdrTrnsprt.HasRows Then
            rdrTrnsprt.Read()

            h_PRI_ID.Value = rdrTrnsprt("ID")
            txtDate.Text = rdrTrnsprt("DATE")
            txtDriver.Text = rdrTrnsprt("OASIS ID")
            txtDriverName.Text = rdrTrnsprt("EMPLOYEE NAME")
            txtDriverType.Text = rdrTrnsprt("DRIVER TYPE")
            txtBasic.Text = rdrTrnsprt("BASIC")
            txtFirst.Text = rdrTrnsprt("FIRST ALLOW")
            txtOther.Text = rdrTrnsprt("SPL ALLOW")
            txtMobile.Text = rdrTrnsprt("MOBILE")
            txtUtility.Text = rdrTrnsprt("UTILITY")
            txtMedical.Text = rdrTrnsprt("MEDICAL")
            txtAirafare.Text = rdrTrnsprt("AIRFARE")
            txtAccom.Text = rdrTrnsprt("ACCOMMODATION")
            txtVisa.Text = rdrTrnsprt("VISA")
            txtGratuity.Text = rdrTrnsprt("GRATUITY")
            txtUniform.Text = rdrTrnsprt("UNIFORM")
            txtCTC.Text = rdrTrnsprt("CTC")
            txtOverHead.Text = rdrTrnsprt("OVER HEAD")
            txtTotalCharges.Text = rdrTrnsprt("TOTAL CHARGING")
            txtAltDriver.Text = rdrTrnsprt("REMARKS")
            RadCustomer.SelectedItem.Text = rdrTrnsprt("CUSTOMER")
            RadCustomer.SelectedValue = rdrTrnsprt("TAC_TLC_ID")
            'RadCBSU.SelectedItem.Text = rdrTrnsprt("UNIT")
            'RadCBSU.SelectedValue = Mainclass.getDataValue("select TAC_CTO_BSU_ID from TPTATTENDANTCHARGES where TAC_ID=" & showId, "OASIS_TRANSPORTConnectionString")
            txtDiscount.Text = rdrTrnsprt("DISCOUNT")
            TxtNotes.Text = rdrTrnsprt("NOTES")
            RadDAXCodes.SelectedItem.Text = rdrTrnsprt("DAX_DESCR")
            RadDAXCodes.SelectedValue = rdrTrnsprt("DAX_ID")
            txtVATAmount.Text = rdrTrnsprt("TAC_TAX_AMOUNT")
            txtNetAmount.Text = rdrTrnsprt("TAC_TAX_NET_AMOUNT")
            radVAT.SelectedValue = rdrTrnsprt("TAC_TAX_CODE")
            TxtNotes.Text = rdrTrnsprt("NOTES")
            txtAltDriver.Text = rdrTrnsprt("REMARKS")
            Dim lstDrp As New RadComboBoxItem
            lstDrp = RadEmirate.Items.FindItemByValue(rdrTrnsprt("TAC_EMR_CODE"))
            If Not lstDrp Is Nothing Then
                RadEmirate.SelectedValue = lstDrp.Value
            Else
                RadEmirate.SelectedValue = "NA"
            End If
            BindDAXCodes()
            CalculateVAT(RadCustomer.SelectedValue)
            SelectEmirate(RadCustomer.SelectedValue)
        End If
        rdrTrnsprt.Close()
        rdrTrnsprt = Nothing

    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub

    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(28).Text = "&nbsp;" Then
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True
                gvr.Cells(33).Enabled = True
                gvr.Cells(34).Enabled = False
                gvr.Cells(35).Enabled = False
            Else
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
                gvr.Cells(33).Enabled = False
                gvr.Cells(34).Enabled = True
                gvr.Cells(35).Enabled = True
            End If
        Next
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTDCID"), Label)



        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTATTENDANTCHARGES set TAC_DELETED=1,TAC_USER='" & Session("sUsr_name") & "',TAC_DATE=GETDATE() where TAC_ID=" & lblID.Text)
        refreshGrid()
    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTAC_INVNO"), Label)
        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPOstingDate"), Label)

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptTPTATTENDANTCHARGES"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@TAC_INVNO", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@TAC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            'If Format(CDate(lblPOstingDate.Text), "dd/MMM/yyyy") <= "31/Dec/2017" Then
            If CDate(lblPOstingDate.Text) <= CDate("31/Dec/2017") Then
                repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTATTENDANTCHARGES.rpt"
            Else
                repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptTPTATTENDANTCHARGES.rpt"
            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '   Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub lnkEmail_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTAC_INVNO"), Label)

        Dim lblPOstingDate As New Label
        lblPOstingDate = TryCast(sender.FindControl("lblPDate"), Label)

        Dim bEmailSuccess As Boolean
        SendEmail(CType(lblID.Text, String), CDate(lblPOstingDate.Text), bEmailSuccess)
        If bEmailSuccess = True Then
            'lblError.Text = "Email send  Sucessfully...!"
            usrMessageBar.ShowNotification("Email send  Sucessfully...!", UserControls_usrMessageBar.WarningType.Success)
        Else
            'lblError.Text = "Error while sending email.."
            usrMessageBar.ShowNotification("Error while sending email..", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByVal InvDate As DateTime, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""

        Dim RptFile As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param("@TAC_INVNO") = RecNo
        param("@TAC_BSU_ID") = Session("sBsuid")
        param.Add("userName", "SYSTEM")
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        'If Format(CDate(InvDate), "dd/MMM/yyyy") <= "31/Dec/2017" Then
        If CDate(InvDate) <= CDate("31/Dec/2017") Then
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTATTENDANTCHARGESEmail.rpt")
        Else
            RptFile = Server.MapPath("~/Transport/ExtraHiring/reports/rptTPTATTENDANTCHARGESEmailTAX.rpt")
        End If

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailTPTInvoice
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.InvNo = RecNo
        rptDownload.InvType = "ACHARGE"
        rptDownload.FCO_ID = 0
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & RecNo & "','" & FCL_ID & "','" & SendEmail & "' ")
        End If
    End Function


    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid()
    End Sub
    Private Sub BindDriver()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select '' EMPNO,'' EMP_NAME UNION ALL select DRIVER_EMPNO,DRIVER from [VW_DRIVER] A INNER JOIN OASIS..employee_m B ON B.EMP_ID=A.DRIVER_empid "
        sql_str &= " order by EMPNO "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim strsql As String = ""
            Dim strsql2 As String = ""
            'strsql = "select TLC_ID,TLC_DESCR from TPTLEASECUSTOMER where TLC_BSU_ID='" & Session("sBsuid") & "'"
            strsql = "select max(tlc_id) TLC_ID ,TLC_DESCR  from TPTLEASECUSTOMER where TLC_BSU_ID='" & Session("sBsuid") & "' group by tlc_descr union all select 0,'----ALL----' order by TLC_DESCR"
            'strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
            strsql2 = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES_NES WHERE TAX_SOURCE='TPT' ORDER BY TAX_ID "

            RadCustomer.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            RadCustomer.DataTextField = "TLC_DESCR"
            RadCustomer.DataValueField = "TLC_ID"
            RadCustomer.DataBind()

            radFilterBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql)
            radFilterBSU.DataTextField = "TLC_DESCR"
            radFilterBSU.DataValueField = "TLC_ID"
            radFilterBSU.DataBind()



            radVAT.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strsql2)
            radVAT.DataTextField = "DESCR"
            radVAT.DataValueField = "ID"
            radVAT.DataBind()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub txtDriver_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDriver.TextChanged
        Dim DriverName As String
        DriverName = Mainclass.getDataValue("select Driver from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtDriver.Text) & "'", "OASIS_TRANSPORTConnectionString")
        If DriverName <> "" Then
            txtDriverName.Text = DriverName
            txtDriverType.Focus()
        Else
            'lblError.Text = "Invalid Driver No...!:"
            usrMessageBar.ShowNotification("Invalid Driver No...!:", UserControls_usrMessageBar.WarningType.Danger)
            txtDriver.Text = ""
            txtDriver.Focus()
            Exit Sub
        End If
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click, btnPost1.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox

        Dim PartyCode As String = ""
        Dim i As Integer = 0
        Dim mrow As GridViewRow
        Dim SelectedIDs As String = ""
        Dim NotSameBsu As Boolean = False
        lblError.Text = ""
        If txtPostingDate.Text.Trim = "" Then lblError.Text &= "Please select Posting Date,"

        If lblError.Text.Length > 0 Then
            'lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " "
            'lblError.Visible = True
            usrMessageBar.ShowNotification(lblError.Text.Substring(0, lblError.Text.Length - 1) & " ", UserControls_usrMessageBar.WarningType.Danger)

            Return
        End If
        For Each mrow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(mrow.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(mrow.FindControl("lblTDCID"), Label)

            If ChkBxItem.Checked = True Then

                If i = 0 Then
                    PartyCode = mrow.Cells(3).Text
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                Else
                    If PartyCode = mrow.Cells(3).Text Then
                        IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    Else
                        NotSameBsu = True
                    End If
                End If
                i = i + 1
            End If
        Next
        If NotSameBsu = False Then
            PostAttendantChargeInvoice(IDs)
            'lblError.Text = "Data Posted Successfully !!!"
            usrMessageBar.ShowNotification("Data Posted Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            refreshGrid()
        Else
            'lblError.Text = "Selected business units are  not same!!!"
            usrMessageBar.ShowNotification("Selected business units are  not same!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

    End Sub
    Private Sub PostAttendantChargeInvoice(ByVal id As String)

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(6) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@ID", id, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@MENU", ViewState("MainMnu_code"), SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@POSTINGDATE", txtPostingDate.Text, SqlDbType.DateTime)
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostDriverCharges", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                stTrans.Commit()
                usrMessageBar.ShowNotification("Data Posted Sucessfully..", UserControls_usrMessageBar.WarningType.Success)
            End If

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, id, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Property TPTATTENDANTCHARGES() As DataTable
        Get
            Return ViewState("TPTATTENDANTCHARGES")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTATTENDANTCHARGES") = value
        End Set
    End Property
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click, btnCopy1.Click
        CopyDriverChargeDetails()
    End Sub

    Private Sub CopyDriverChargeDetails()

        Dim NewDate As Date
        Dim Newdate1 As Date

        NewDate = Format(CDate(txtDate.Text.ToString), "dd/MMM/yyyy")
        Newdate1 = NewDate.AddMonths(1)
        Newdate1 = New Date(NewDate.AddMonths(1).Year, NewDate.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1)
        Newdate1 = Format(CDate(Newdate1), "dd/MMM/yyyy")
        Dim CParms(4) As SqlClient.SqlParameter

        CParms(0) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
        CParms(0).Value = NewDate
        CParms(1) = New SqlClient.SqlParameter("@DATE1", SqlDbType.DateTime)
        CParms(1).Value = Newdate1

        CParms(2) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar, 10)
        CParms(2).Value = "ATTEND"
        CParms(3) = New SqlClient.SqlParameter("@BSU", SqlDbType.VarChar, 6)
        CParms(3).Value = Session("sBsuid")

        CParms(4) = New SqlClient.SqlParameter("@Errormsg", SqlDbType.VarChar, 1000)
        CParms(4).Direction = ParameterDirection.Output

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "CopyInvoiceDetails", CParms)
            Dim ErrorMSG As String = CParms(4).Value.ToString()


            If ErrorMSG = "" Then
                stTrans.Commit()
                usrMessageBar.ShowNotification("Copied Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                txtDate.Text = Newdate1.ToString("dd/MMM/yyyy")
                refreshGrid()

            Else
                usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
                refreshGrid()
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            txtDate.Text = NewDate.ToString("dd/MMM/yyyy")
            refreshGrid()

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub


    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("Attendant Charges")
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim VAT As Decimal = 0
        Dim Net As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)

            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
                Dim lblVAT As Label = TryCast(gvr.FindControl("lblVatAmt"), Label)
                Dim lblNET As Label = TryCast(gvr.FindControl("lblVatNetAmt"), Label)
                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                    Gross += Val(lblGross.Text)
                    VAT += Val(lblVAT.Text)
                    Net += Val(lblNET.Text)
                End If
            Else
                ChkBxItem.Checked = False
                Gross = 0
                VAT = 0
                Net = 0

            End If
        Next
        txtSelectedAmt.Text = Gross
        txtSelVAT.Text = VAT
        txtSelNet.Text = Net
    End Sub

    Private Sub CalculateVAT(ByVal Act_id As String)
        Dim dt As New DataTable
        dt = MainObj.getRecords("SELECT * FROM  oasis.TAX.[GetTAXCodeAndAmount]('TPT','" & Session("sBsuid") & "','TRANTYPE','ATT_CHG','" & txtDate.Text & "'," & txtTotalCharges.Text & ",'" & Act_id & "')", "OASIS_TRANSPORTConnectionString")

        If dt.Rows.Count > 0 Then
            Dim lstDrp As New RadComboBoxItem
            lstDrp = radVAT.Items.FindItemByValue(dt.Rows(0)("tax_code"))
            h_VATPerc.Value = dt.Rows(0)("tax_perc_value")
            'lstDrp = radVAT.Items.FindItemByValue("VAT5")
            'If bsuid <> "500953" Then
            '    h_VATPerc.Value = 5
            'Else
            '    h_VATPerc.Value = 7
            'End If

            If Not lstDrp Is Nothing Then
                radVAT.SelectedValue = lstDrp.Value
            Else
                radVAT.SelectedValue = 0
            End If
            txtVATAmount.Text = ((Val(txtTotalCharges.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = (Val(txtTotalCharges.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub

    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged
        Dim dtVATPerc As New DataTable
        dtVATPerc = MainObj.getRecords("SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & radVAT.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        If dtVATPerc.Rows.Count > 0 Then
            h_VATPerc.Value = dtVATPerc.Rows(0)("tax_perc_value")
            txtVATAmount.Text = ((Val(txtTotalCharges.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtTotalCharges.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        Else
            radVAT.SelectedValue = 0
            h_VATPerc.Value = radVAT.SelectedValue
            txtVATAmount.Text = ((Val(txtTotalCharges.Text) * h_VATPerc.Value) / 100.0).ToString("####0.000")
            txtNetAmount.Text = Convert.ToDouble(Val(txtTotalCharges.Text) + Val(txtVATAmount.Text)).ToString("####0.000")
        End If
    End Sub
    Protected Sub RadCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles RadCustomer.SelectedIndexChanged
        CalculateVAT(RadCustomer.SelectedValue)
        SelectEmirate(RadCustomer.SelectedValue)
    End Sub
    Private Sub SelectEmirate(ByVal ACT_ID As String)
        Dim bsu_City As String = ""
        bsu_City = Mainclass.getDataValue("SELECT isnull(case when case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when EMR_CODE='ALN' then 'AUH' else EMR_CODE end end,'NA')   EMR_CODE   FROM OASISFIN..accounts_M left outer join oasis.TAX.VW_TAX_EMIRATES on EMR_CODE=ACT_EMR_CODE where act_id in(select distinct tlc_act_id from TPTLEASECUSTOMER where tlc_id='" & ACT_ID & "') ", "OASIS_TRANSPORTConnectionString")
        Dim lstDrp As New RadComboBoxItem

        lstDrp = RadEmirate.Items.FindItemByValue(bsu_City)

        If Not lstDrp Is Nothing Then
            RadEmirate.SelectedValue = lstDrp.Value
        Else
            RadEmirate.SelectedValue = "NA"
        End If
    End Sub
    Protected Sub radFilterBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radFilterBSU.SelectedIndexChanged
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'sqlWhere = " where  YEAR(TAC_TRDATE)= YEAR('" & txtDate.Text & "') and MONTH(TAC_TRDATE)= MONTH('" & txtDate.Text & "') AND TAC_DELETED=0 and TAC_BSU_ID='" & Session("sBsuid") & "' and TAC_FYEAR='" & Session("F_YEAR") & "' and TAC_TLC_ID ='" & radFilterBSU.SelectedValue & "'  ORDER BY TAC_TRDATE"
        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTATTENDANTCHARGES = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTATTENDANTCHARGES
        '    SetExportFileLink()
        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try
        refreshGrid()



    End Sub

    Protected Sub rblSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSelection.SelectedIndexChanged
        refreshGrid()

    End Sub
End Class
