﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptPostingExGratia.aspx.vb" Inherits="Transport_ExtraHiring_tptPostingExGratia" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        table td input[type=text]{
            min-width:20% !important;
            
        }
        .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:0px !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px !important;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox {
            width:100% !important;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function onChange() { alert('ok'); }
        function calculate() {
            document.getElementById("<%=txtNetTotal.ClientID %>").value = total;
        }




        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;



            if (chk_state) {

                document.getElementById("<%=txtNetTotal.ClientID %>").value = document.getElementById("<%=hdnNetTotal.ClientID %>").value;
                document.getElementById("<%=txtSTSAMOUNT.ClientID%>").value = document.getElementById("<%=hdnSTSTotal.ClientID%>").value;
                document.getElementById("<%=txtBBTAMOUNT.ClientID%>").value = document.getElementById("<%=hdnBBTTotal.ClientID%>").value;

                var VatAmount = document.getElementById("<%=txtNetTotal.ClientID %>").value * document.getElementById("<%=h_VATPerc.ClientID %>").value / 100.00;
                document.getElementById("<%=txtVatAmount.ClientID %>").value = VatAmount;
                document.getElementById("<%=txtNetAmount.ClientID %>").value = VatAmount + eval(document.getElementById("<%=txtNetTotal.ClientID %>").value);


            }
            else {
                document.getElementById("ctl00_cphMasterpage_txtNetTotal").value = '0.00';
                document.getElementById("ctl00_cphMasterpage_txtSTSAMOUNT").value = '0.00';
                document.getElementById("ctl00_cphMasterpage_txtBBTAMOUNT").value = '0.00';
                var VatAmount = document.getElementById("<%=txtNetTotal.ClientID %>").value * document.getElementById("<%=h_VATPerc.ClientID %>").value / 100.00;
                document.getElementById("<%=txtVatAmount.ClientID %>").value = VatAmount;
                document.getElementById("<%=txtNetAmount.ClientID %>").value = VatAmount + eval(document.getElementById("<%=txtNetTotal.ClientID %>").value);
            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }



        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
    </script>


    

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            ExGratia Charges Posting
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <tr valign="bottom" id="lblerrorid" visible="false"  >
                        <td align="center"  valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  valign="top">
                            <table align="left" cellpadding="5" cellspacing="0" style="width: 100%">
                                <%-- <tr class="subheader_img">
                            <td align="center" colspan="5" valign="middle">
                                <div align="center">
                                    ExGratia Charges Posting</div>
                            </td>
                        </tr>--%>
                                <tr>
                                    <td align="left" width="15%">
                                        <asp:Label ID="lblQUONo" runat="server" Text="Batch Number" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="40%" class="p-0">
                                        <asp:TextBox ID="txtBatchNo" runat="server" Enabled="false"></asp:TextBox>
                                                 </td>
                                                <td align="left" width="60%" class="p-0">
                                        <asp:Label ID="Label4" runat="server" Text="Batch:" Enabled="false"></asp:Label>
                                        <asp:Label ID="lblbatchstr" runat="server" Enabled="false"></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text="Tr.Date:" Enabled="false"></asp:Label>
                                        <asp:Label ID="lblTrDate" runat="server" Enabled="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table> 
                                    </td>
                                    <td width="20%"><span class="field-label">Batch Date</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBatchDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="lnkBatchDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtBatchDate" PopupButtonID="lnkBatchDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="left" width="15%">
                                        <asp:Label ID="Label2" runat="server" Text="Business Category" CssClass="field-label"> </asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <telerik:RadComboBox ID="RadDAXCodes" Width="325" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td width="20%">
                                        <asp:Label ID="Label1" runat="server" Text="Document Number" CssClass="field-label"> </asp:Label>
                                    </td>
                                    <td width="30%">
                                        <span class="text-danger">
                                            <asp:Label ID="lblJHDDocNo" runat="server" Text="" CssClass="field-label"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Text="" CssClass="field-label error" ></asp:Label>
                                        </span>
                                    </td>                           
                                    
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="15%"><span class="field-label">School Amount</span>
                                                </td>
                                                <td align="left" width="20%">
                                                    <asp:TextBox ID="txtNetTotal" Text="0.00" runat="server" Enabled="false" AutoPostBack="True"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnNetTotal" runat="server" />
                                                    <asp:HiddenField ID="hdnSTSTotal" runat="server" />
                                                    <asp:HiddenField ID="hdnBBTTotal" runat="server" />

                                                </td>
                                                <td align="left" width="13%">
                                                    <asp:Label ID="Label5" runat="server" Text="STS Amount" class="field-label"> </asp:Label>
                                                </td>
                                                <td align="left" width="20%">
                                                    <asp:TextBox ID="txtSTSAMOUNT" Text="0.00" runat="server" Enabled="false" Width="70%"></asp:TextBox>
                                                </td>
                                                <td align="left" width="13%">
                                                    <asp:Label ID="Label6" runat="server" Text="BBT Amount" class="field-label"> </asp:Label>
                                                </td>
                                                <td align="left" width="20%">
                                                    <asp:TextBox ID="txtBBTAMOUNT" Text="0.00" runat="server" Enabled="false" Width="70%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="6" class="title-bg">VAT Details</td>
                                </tr>
                                <tr>
                                    
                                                <td align="left" width="15%"><span class="field-label">VAT Code</span>
                                                </td>
                                                <td align="left" width="32%">
                                                    <telerik:RadComboBox ID="radVAT"  runat="server" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadComboBox>
                                                </td>
                                                <td align="left" width="20%">
                                                    <asp:Label ID="Label10" runat="server" Text="Emirate" CssClass="field-label" > </asp:Label>
                                                </td>
                                                <td align="left" width="33%">
                                                    <telerik:RadComboBox ID="radEmirate" runat="server" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                            
                                    </tr>
                                    <tr>
                                    
                                                <td align="left" width="15%"><span class="field-label">VAT Amount</span>
                                                </td>
                                                <td width="32%" align="left">
                                                    <%--<asp:Label ID="Label7" runat="server" Text="VAT Amount" CssClass="field-label" ></asp:Label>--%>
                                                    <asp:TextBox ID="txtVatAmount" runat="server" Enabled="false" Style="text-align: right"></asp:TextBox>
                                                </td>
                                                                                           
                                                <td width="20%" align="left">
                                                    <asp:Label ID="Label8" runat="server" Text="Net Amount" CssClass="field-label" ></asp:Label></td>
                                                <td width="33%" align="left">
                                                    <asp:TextBox ID="txtNetAmount" runat="server" Enabled="false" Style="text-align: right"></asp:TextBox>
                                                </td>
                                            
                                </tr>

                                <tr>
                                    <td width="15%"><span class="field-label">Remarks</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtremarks" runat="server"  TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                    

                                </tr>

                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:GridView ID="grdInvoiceDetails" runat="server" AutoGenerateColumns="False" PageSize="100"
                                            Width="100%" ShowFooter="True" CaptionAlign="Top" CssClass="table table-bordered table-row"
                                            DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField HeaderText=" ">
                                                    <HeaderTemplate>
                                                        <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                            value="Check All" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DATE" HeaderText="Date" />
                                                <asp:BoundField DataField="DRIVER_EMPNO" HeaderText="Driver OASIS No." />
                                                <asp:BoundField DataField="Driver" HeaderText="Driver" />
                                                <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit Name" />


                                                <asp:TemplateField HeaderText="Charging Unit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCBSUSHORTNAME" runat="server" Text='<%# Eval("EGC_C_BSU_SHORTNAME")%>'></asp:Label>
                                                        <asp:Label ID="lblBSUSHORTNAME" runat="server" Text='<%# Eval("BSU_SHORTNAME")%>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--<asp:BoundField DataField="EGC_C_BSU_SHORTNAME" HeaderText="" />--%>
                                                <asp:BoundField DataField="DriverType" HeaderText="Driver Type" />
                                                <asp:BoundField DataField="EGC_TSTART" HeaderText="Starting Time" />
                                                <asp:BoundField DataField="EGC_TEND" HeaderText="Ending Time" />
                                                <asp:BoundField DataField="EGC_TTOTAL" HeaderText="Total Time" />




                                                <asp:TemplateField HeaderText="Net Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNet" runat="server" Text='<%# Eval("EGC_NETAMOUNT") %>'></asp:Label>
                                                        <asp:Label ID="lblEGCID" runat="server" Text='<%# Eval("EGC_ID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="EGC_REMARKS" HeaderText="Remarks" />
                                                <asp:BoundField DataField="EGC_USER" HeaderText="User" />



                                                <%--<asp:TemplateField HeaderText="Gross">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGross" runat="server" Text='<%# Eval("GROSS") %>'></asp:Label>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Salik">
                                        <ItemTemplate>
                                        <asp:Label ID="lblSalik" runat="server" Text='<%# Eval("SALIK") %>'></asp:Label>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="" Visible="false" >
                                        <ItemTemplate>
                                        <asp:Label ID="lblTEXTEIID" runat="server" Text='<%# Eval("TEX_TEI_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        </asp:TemplateField>--%>
                                            </Columns>
                                        </asp:GridView>

                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSAVE" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Save" Visible="False" />
                                        <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Post" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Print" />
                                        <asp:Button ID="btnEmail" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Email" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_QUDGridDelete" runat="server" />
                                        <asp:HiddenField ID="h_VATPerc" Value="0" runat="server" />
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
