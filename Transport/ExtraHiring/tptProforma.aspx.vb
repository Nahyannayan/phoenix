﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Transport_ExtraHiring_tptProforma
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200200") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If
            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
            Else
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            End If
            showNoRecordsFound()
            grdInvoiceDetails.DataSource = TPTProformaInvoiceDetails
            grdInvoiceDetails.DataBind()
            showNoRecordsFound()
        Else
            calcTotals()
        End If
    End Sub
    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
        grdInvoiceDetails.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode
    End Sub
    Sub calcTotals()
        Dim dt As New DataTable

        Dim NetTotal As Decimal = 0
        Dim Total As Decimal = 0.0
        Dim NetTAX As Decimal = 0.0
        Dim NetAmount As Decimal = 0.0

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblAmount As Label = TryCast(gvr.FindControl("lblAmount"), Label)
            Dim lblTAXAmount As Label = TryCast(gvr.FindControl("lblTAXAmount"), Label)
            Dim lblNetAmount As Label = TryCast(gvr.FindControl("lblNetAmount"), Label)

            Total += Val(lblAmount.Text) + Val(lblTAXAmount.Text) + Val(lblNetAmount.Text)
            If ChkBxItem.Checked = True Then
                NetTotal += Val(lblAmount.Text)
                NetTAX += Val(lblTAXAmount.Text)
                NetAmount += Val(lblNetAmount.Text)
            End If
        Next
        lblTotal.Text = NetTotal
        lblTax.Text = NetTAX
        lblNetAmount.Text = NetAmount
        hdn_Total.Value = Total
    End Sub
    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim StrSQL As String = ""
            StrSQL = "SELECT TRN_No, [Job No], [Job Date],Client, isnull(Amount,0)Amount,[TAX Amount],isnull([Net Amount],0)[Net Amount], isnull([Vehicle No],'')[Vehicle No],[Group],[Pickup Point]  from ("
            StrSQL &= "select TRN_No,STS_No [Group],isnull([TRN_BATCH],0) [Batch No], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date],isnull(ACT_NAME,BSU_NAME) Client,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no) Amount,isnull(TPT_TAX_AMOUNT,0) [TAX Amount],isnull(TPT_TAX_NET_AMOUNT,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no)) [Net Amount], ISNULL(TPTVEHICLE_M.VEH_REGNO,vm2.VEH_REGNO) [Vehicle No],ISNULL(TPTD_NAME,Driver)[Driver], "
            StrSQL &= " LPO_no as [Ref No], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point], "
            StrSQL &= " '' Batch,'' [Batch date],'' [Total Amount],'' Status,'' [Debit Note No] "
            StrSQL &= "FROM TPTHiring with(nolock) "
            StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M with(nolock) on TPT_Loc_id=LOC_ID LEFT OUTER JOIN TPTDRIVER_M with(nolock) ON TPTD_ID=TPT_Drv_id  "
            StrSQL &= "LEFT OUTER JOIN TPTVEHICLE_M with(nolock) ON VEH_ID=TPT_Veh_id LEFT OUTER JOIN VEHICLE_M VM1 with(nolock) ON VM1.VEH_REGNO=TPTVEHICLE_M.VEH_REGNO LEFT OUTER JOIN VEHICLE_M VM2 with(nolock) ON VM2.VEH_ID=TPT_Veh_id LEFT OUTER JOIN VW_DRIVER with(nolock) ON DRIVER_EMPID=TPT_Drv_id  "
            StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
            StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M with(nolock) on BSU_ID=TPT_Client_id "
            StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M with(nolock) on TSM_ID=TPT_SERVICE_ID "
            StrSQL &= "LEFT OUTER JOIN TPTHIRING_I with(nolock) on THI_DOCNO=TRN_JobnumberStr "
            StrSQL &= "where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and TRN_STATUS='' and isnull(TPT_BllId,0)=0 and ISNULL(TRN_numberstr,'')='' and TPT_Bsu_id like '%" & Session("sBsuid") & "%' and TPT_Client_id in(select TPT_Client_id  from tpthiring where TRN_no=" & ViewState("EntryId") & ")) A where 1=1 "
            fillGridView(grdInvoiceDetails, StrSQL)
            calcTotals()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Property TPTProformaInvoiceDetails() As DataTable
        Get
            Return ViewState("ProformaInvoiceDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("ProformaInvoiceDetails") = value
        End Set
    End Property
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTProformaInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTProformaInvoiceDetails)
        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTProformaInvoiceDetails = mtable
        fillGrdView.DataSource = TPTProformaInvoiceDetails
        fillGrdView.DataBind()
    End Sub
    Sub ClearDetails()
        h_EntryId.Value = "0"
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Private Property QUDFooter() As DataTable
        Get
            Return ViewState("QUDFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("QUDFooter") = value
        End Set
    End Property
    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property
    Private Sub showNoRecordsFound()
        If Not QUDFooter Is Nothing AndAlso QUDFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoiceDetails.Columns.Count - 2
            grdInvoiceDetails.Rows(0).Cells.Clear()
            grdInvoiceDetails.Rows(0).Cells.Add(New TableCell())
            grdInvoiceDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoiceDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub
    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdInvoiceDetails.PageIndexChanging
        grdInvoiceDetails.PageIndex = e.NewPageIndex
        grdInvoiceDetails.DataSource = TPTProformaInvoiceDetails
        grdInvoiceDetails.DataBind()
    End Sub
    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoiceDetails.RowCancelingEdit
        grdInvoiceDetails.ShowFooter = True
        grdInvoiceDetails.EditIndex = -1
        grdInvoiceDetails.DataSource = QUDFooter
        grdInvoiceDetails.DataBind()
        showNoRecordsFound()
    End Sub
    Private Sub UpdateAddress()
        Dim Count As Integer = 0
        Dim Trnno As String = ""
        Dim chkControl As New CheckBox
        Dim lblID As Label
        Dim IDs As String = ""
        For Each grow As GridViewRow In grdInvoiceDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl IsNot Nothing Then
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("lbltrn_No"), Label)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                End If
            End If
        Next
        If IDs = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        Count = Mainclass.getDataValue("select COUNT(distinct TRN_numberstr  )+1 FROM tpthiring    where isnull(TRN_numberstr,'') <>''  and TPT_Bsu_id ='" & Session("sBsuid") & "' and Tpt_FYEAR='" & Session("F_YEAR") & "'", "OASIS_TRANSPORTConnectionString")
        Trnno = Right(Session("F_YEAR"), 2) + "PI-" + Format(Count, "0000000")
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update TPTHiring set TPT_Feedback='" & txtAddress.Text & "' ,TPT_Add_Service='" & txtRemarks.Text & "',TRN_numberstr='" & Trnno & "' where TRN_no in(select id from oasis..fnSplitMe ('" & IDs & "','|'))")
        writeWorkFlowTPTH(0, IIf(h_EntryId.Value = 0, "Created", "Modified"), "Job Order-Proforma" & Trnno, "")
    End Sub
    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        Dim NetTotal As Decimal = 0
        Dim NetTAX As Decimal = 0
        Dim NetAmount As Decimal = 0

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim Chkal As CheckBox = TryCast(grdInvoiceDetails.HeaderRow.FindControl("chkAL"), CheckBox)

            If Chkal.Checked = True Then
                Dim lblGross As Label = TryCast(gvr.FindControl("lblAmount"), Label)
                Dim lblTAX As Label = TryCast(gvr.FindControl("lblTAXAmount"), Label)
                Dim lblNetAmount As Label = TryCast(gvr.FindControl("lblNetAmount"), Label)

                If (gvr.Cells(0).Enabled = True) Then
                    ChkBxItem.Checked = True
                End If
                NetTotal += Val(lblGross.Text)
                NetTAX += Val(lblTAX.Text)
                NetAmount += Val(lblNetAmount.Text)
            Else
                ChkBxItem.Checked = False
                Gross = 0
            End If
        Next
        lblTotal.Text = NetTotal
        lblTax.Text = NetTAX
        lblNetAmount.Text = NetAmount
        hdn_Total.Value = NetTotal + NetTAX + NetAmount

    End Sub
    Private Function writeWorkFlowTPTH(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal strStatus As String) As Integer
        Dim iParms(6) As SqlClient.SqlParameter
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TRN_NO", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveWorkFlowTPTH", iParms)
        If RetVal = "-1" Then
            lblError.Text = "Unexpected Error !!!"
            stTrans.Rollback()
            Exit Function
        Else
            stTrans.Commit()
        End If
    End Function

    Protected Sub btnProforma_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProforma.Click
        If txtAddress.Text.Length = 0 Then
            lblError.Text = "plese enter the address"
            Exit Sub
        ElseIf txtRemarks.Text.Length = 0 Then
            lblError.Text = "plese Enter the Proforma Remarks"
            Exit Sub
        Else
            UpdateAddress()
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class

