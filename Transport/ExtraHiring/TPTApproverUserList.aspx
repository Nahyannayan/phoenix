﻿<%@ Page Language="VB"  MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TPTApproverUserList.aspx.vb" Inherits="Transport_ExtraHiring_TPTApproverUserList" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }


        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetEMPName() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "GETUSERFOREXGRATIA"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up4");
        }

        function OnClientClose4(oWnd, args) {

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtUser.ClientID%>").value = NameandCode[1];
                document.getElementById("<%=h_userName.ClientID%>").value = NameandCode[0];
                
            }
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Customer Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            
                        </td>
                    </tr>
                    <tr>
                        <td   valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="8" valign="middle"><asp:Label runat="server" id="rptHeader" Text="Customer Details"></asp:Label></td>
                    </tr>--%>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">User</span></td>
                                    
                                    <td align="left"  style="width: 30%" >
                                        <asp:TextBox ID="txtUser" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="imgGetUserList" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetEMPName();return false;" />
                                        <asp:HiddenField ID="h_userName" runat="server" Value="" />
                                        
                                    </td>


                                    <td align="left"  width="20%"><span class="field-label">User Type</span></td>
                                   
                                    <td align="left"  width="30%">
                                        
                                    <%--    <telerik:RadComboBox ID="ddlApproverType" Width="225" runat="server" TabIndex="10" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>--%>

                                        <asp:DropDownList ID="ddlApproverType" Width="300px" runat="server" CssClass="listbox"></asp:DropDownList>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Type</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlType" Width="300px" runat="server" CssClass="listbox"></asp:DropDownList>
                                    </td>
                                    <td align="left"  width="20%"><span class="field-label"></span></td>
                                    <td align="left"  width="30%"><span class="field-label"></span></td>


                                </tr>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">From Date</span></td>
                                   
                                    <td align="left"  style="width: 30%" >
                                        <asp:TextBox ID="txtFromDate" runat="server" Enabled="false"  CssClass="inputbox" ></asp:TextBox>
                                        <asp:ImageButton ID="ImgFDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                      TargetControlID="txtFromDate" PopupButtonID="ImgFDate">
                                                  </ajaxToolkit:CalendarExtender>
                                    </td>
                                     <td align="left"  width="20%"><span class="field-label">To Date</span></td>
                                    
                                    <td align="left"  width="30%" >
                                        <asp:TextBox ID="txtTODate" Enabled="false" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="ImgTDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                  <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                                      TargetControlID="txtTODate" PopupButtonID="ImgTDate">
                                                  </ajaxToolkit:CalendarExtender>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Applicable to Business Unit</span><br />
                                        <asp:CheckBox ID="chkBSUAll1" runat="server" Text='Select All' AutoPostBack="true" />
                                    </td>

                                    <td align="left" colspan="3">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plChkBUnit1" runat="server">
                                                <asp:Repeater ID="rptBSUNames1" runat="server">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="h_BSUid1" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bsu_id") %>' />
                                                        <%--<asp:CheckBox ID="chkBSU1" Checked='<%# DataBinder.Eval(Container.DataItem, "bsu_id")%>' runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "bsu_name") %>' /><br />--%>
                                                        <%--<asp:CheckBox ID="chkBSU1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "bsu_name") %>' /><br />--%>
                                                        <asp:CheckBox ID="chkBSU1" Checked='<%# DataBinder.Eval(Container.DataItem, "ChkVal")%>' runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "bsu_name") %>' /><br />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="left"  width="20%"><span class="field-label" colspan="4">
                                        
                                        <asp:CheckBox ID="chkStatus" runat="server" Text='Deleted' />
                                        
                                        </span></td>
                                   
                                    
                                    

                                </tr>
                               

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td   valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />                            
                            <asp:HiddenField ID="h_UtypeID" runat="server" Value="0" />                            
                            <asp:HiddenField ID="h_TYPE" runat="server" Value="0" />                            
                            
                            
                        </td>
                    </tr>
                </table>

            </div>
        </div>
<uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
