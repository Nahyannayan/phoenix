﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.Collections.Generic


Partial Class Transport_tptProduct
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200105") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If

                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    'UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    SetDataMode("add")
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        txtProdHrs.Attributes.Add("onkeypress", " return Numeric_Only();")
        txtProdKm.Attributes.Add("onkeypress", " return Numeric_Only();")
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        txtProdHrs.Enabled = EditAllowed
        txtProdKm.Enabled = EditAllowed
        txtproductName.Enabled = EditAllowed
        txtRate.Enabled = EditAllowed
        rbGen.Enabled = EditAllowed
        rbRate.Enabled = EditAllowed

        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As New StringBuilder
                strSql.Append("select PROD_ID,ISNULL(PROD_NAME,'') PROD_NAME,ISNULL(PROD_HRS,0) PROD_HRS,ISNULL(PROD_KM,0)PROD_KM,ISNULL(PROD_RATE,0) PROD_RATE,ISNULL(PROD_FLAG,'')PROD_FLAG   from TPTHiringProduct where prod_id=" & h_EntryId.Value)

                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
                If dt.Rows.Count > 0 Then
                    txtProdHrs.Text = dt.Rows(0)("prod_hrs")
                    txtProdKm.Text = dt.Rows(0)("prod_km")
                    txtproductName.Text = dt.Rows(0)("prod_name")
                    h_EntryId.Value = dt.Rows(0)("prod_id")
                    txtRate.Text = dt.Rows(0)("prod_rate")
                    If dt.Rows(0)("prod_FLAG") = "R" Then
                        rbRate.Checked = True
                    Else
                        rbGen.Checked = True
                    End If
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub ClearDetails()
        txtproductName.Text = ""
        txtProdHrs.Text = "0"
        txtProdKm.Text = "0"
        txtRate.Text = ""

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select * from TPTHiringProduct where PROD_ID=" & h_EntryId.Value & " and PROD_ID IN (select CND_PROD_ID from TPTCONTRACT_H inner JOIN TPTCONTRACT_D on CNH_ID=CND_CNH_ID)") > 0 Then
                lblError.Text = "product in use, unable to delete"
            Else
                Dim objConn As New SqlConnection(connectionString)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim sqlStr As String = "delete from tpthiringproduct where prod_ID=@TRNO"
                    Dim pParms(1) As SqlParameter
                    pParms(1) = Mainclass.CreateSqlParameter("@TRNO", h_EntryId.Value, SqlDbType.Int, True)
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                    stTrans.Commit()
                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                    Exit Sub
                Finally
                    objConn.Close()
                End Try
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                'If flagAudit <> 0 Then
                '    Throw New ArgumentException("Could not process your request")
                'End If
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim prod_flag As String
        If txtproductName.Text.Trim.Length = 0 Then strMandatory.Append("Product Name is mandatory")
        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = strMandatory.ToString
            Exit Sub
        End If

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim pParms(6) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@prod_id", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@prod_name", txtproductName.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@prod_hrs", txtProdHrs.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@prod_km", txtProdKm.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@prod_rate", txtRate.Text, SqlDbType.Float)
            If rbGen.Checked = True Then
                prod_flag = "G"
            Else
                prod_flag = "R"
            End If


            Dim sqlStr As String = "insert into tpthiringproduct (prod_name, prod_km, prod_hrs,prod_rate,prod_flag) values (@prod_name, @prod_km, @prod_hrs,@prod_rate,'" & prod_flag & "');select @prod_id=@@identity "
            If h_EntryId.Value > 0 Then sqlStr = "update tpthiringproduct set prod_name=@prod_name, prod_hrs=@prod_hrs, prod_km=@prod_km,prod_rate=@prod_rate,prod_flag=prod_flag where prod_id=@prod_id"
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
            stTrans.Commit()
            ViewState("EntryId") = pParms(1).Value

            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.menuname, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If

            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub


        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
