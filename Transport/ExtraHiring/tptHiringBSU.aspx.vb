Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime
Imports System.Web.Services
Partial Class tptHiringBSU
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200145") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                txtOStart.Attributes.Add("onblur", " return calc('O');")
                txtOEnd.Attributes.Add("onblur", " return calc('O');")
                txtTStart.Attributes.Add("onblur", " return calc('T');")
                txtTEnd.Attributes.Add("onblur", " return calc('T');")
                txtSalik.Attributes.Add("onblur", " return calc('O');")
                clearScreen()
                sqlStr = "select TEX_NO,TEX_ID, replace(convert(varchar(30), TEX_DATE, 106),' ','/') Date, VEH_REGNO, "
                sqlStr &= "case when TEX_VEH_TYPE=1 then 'MB' ELSE CASE WHEN TEX_VEH_TYPE=2 THEN  'BB' ELSE CASE WHEN TEX_VEH_TYPE=4 THEN 'SB' ELSE  'VAN' END END END VEH_TYPE, DRIVER, T1.TMS_DESCR LOC_FROM, "
                sqlStr &= "T2.TMS_DESCR LOC_TO, T3.TMS_DESCR DUTY, TEX_OSTART, TEX_OEND, TEX_OTOTAL, TEX_TSTART, TEX_TEND, TEX_TTOTAL, "
                sqlStr &= "TEX_GROSS, TEX_SALIK, TEX_NET,TEX_REF,E.EMPNO,isnull(TEX_DESCR,'') DESCR,isnull(TEX_USER,'')TEX_USER,TEX_TEI_ID,TEX_CBSU_ID,ISNULL(BSU_NAME,'') CUNIT "
                sqlStr &= "FROM TPTEXTRA inner JOIN VEHICLE_M on VEH_ID=TEX_VEH_ID INNER JOIN VW_DRIVER on driver_empid=TEX_DRIVER "
                sqlStr &= "INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=TEX_DRIVER "
                sqlStr &= "LEFT OUTER JOIN TPTMASTER T1 on TEX_FROM=T1.TMS_ID AND TEX_BSU_ID=T1.TMS_BSU_ID AND T1.TMS_TYPE='A' "
                sqlStr &= "LEFT OUTER JOIN TPTMASTER T2 on TEX_TO=T2.TMS_ID AND TEX_BSU_ID=T2.TMS_BSU_ID AND T2.TMS_TYPE='A' "
                sqlStr &= "LEFT OUTER JOIN TPTMASTER T3 on TEX_DUTY=T3.TMS_ID  AND T3.TMS_TYPE='D' "
                sqlStr &= "LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TEX_CBSU_ID"


                BindVehicle()
                BindVehicleType()
                BindDriver()
                BindLocationFrom()
                BindPurpose()
                BindBsu()
                BindChargingBsu()
                refreshGrid(True)
                If Request.QueryString("VIEWID") <> "" Then
                    h_PRI_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_PRI_ID.Value)
                End If
                btnSave.Visible = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Private Sub refreshGrid(Optional ByVal MonthTr As Boolean = False)
        Dim ds As New DataSet
        Dim pParms(7) As SqlParameter
        Try
            sqlWhere = " where TEX_BSU_ID='" & ddlBusinessUnit.SelectedValue & "' "
            If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
            If txtFromDate.Text = "" Then
                txtFromDate.Text = Now.ToString("dd/MMM/yyyy")
                txtToDate.Text = Now.ToString("dd/MMM/yyyy")
                sqlWhere &= " and TEX_DATE>='" & txtFromDate.Text & "' "
                sqlWhere &= " and TEX_DATE<='" & txtToDate.Text & "' "
            Else
                sqlWhere &= " and TEX_DATE>='" & txtFromDate.Text & "' "
                sqlWhere &= " and TEX_DATE<='" & txtToDate.Text & "' "
            End If
            If txtDriverId.Text <> "" Then sqlWhere &= " and DRIVER like '%" & txtDriverId.Text & "%'"
            If txtDutyId.Text <> "" Then sqlWhere &= " and T3.TMS_DESCR like '%" & txtDutyId.Text & "%'"
            If txtFromId.Text <> "" Then sqlWhere &= " and T1.TMS_DESCR like '%" & txtFromId.Text & "%'"
            If txtVehicleId.Text <> "" Then sqlWhere &= " and VEH_REGNO like '%" & txtVehicleId.Text & "%'"
            sqlWhere &= " order by tex_date"
            'ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
            'GridViewShowDetails.DataSource = ds
            'GridViewShowDetails.DataBind()



            If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")

            If txtFromDate.Text = "" Then
                txtFromDate.Text = Now.ToString("dd/MMM/yyyy")
                txtToDate.Text = Now.ToString("dd/MMM/yyyy")
            End If


            pParms(1) = Mainclass.CreateSqlParameter("@TEX_BSU_ID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)

            If MonthTr = True Then
                pParms(2) = Mainclass.CreateSqlParameter("@TEX_FROM_DATE", txtDate.Text, SqlDbType.Date)
                pParms(3) = Mainclass.CreateSqlParameter("@TEX_TO_DATE", "01/Jan/1900", SqlDbType.Date)

            Else
                pParms(2) = Mainclass.CreateSqlParameter("@TEX_FROM_DATE", txtFromDate.Text, SqlDbType.Date)
                pParms(3) = Mainclass.CreateSqlParameter("@TEX_TO_DATE", txtToDate.Text, SqlDbType.Date)
            End If


            pParms(4) = Mainclass.CreateSqlParameter("@DRIVER", txtDriverId.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@DUTY", txtTStart.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@FROM", txtFromId.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@VEHICLE", txtVehicleId.Text, SqlDbType.VarChar)

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GET_EXTRATRIP_DETAILS", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()





        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub clearScreen()
        h_PRI_ID.Value = "0"
        txtOStart.Text = "0"
        txtOEnd.Text = "0"
        txtOTotal.Text = "0"
        txtTStart.Text = "0:0"
        txtTEnd.Text = "0:0"
        txtTTotal.Text = "0:0"
        txtSalik.Text = "0"
        txtGross.Text = "0"
        txtNet.Text = "0"
        txtRef.Text = ""
        txtDriver.Text = ""
        txtDriverName.Text = ""
        txtVehNo.Text = ""
        ddlVehType.Text = ""
        txtDescr.Text = ""
        h_TEI_ID.Value = 0
        txtLOCFROM.Text = ""
        txtLOCTo.Text = ""
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property
    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If txtVehNo.Text = "" Then lblError.Text &= "Vehicle,"
        If ddlVehType.SelectedValue = "" Then lblError.Text &= "Vehicle Type,"
        If txtOStart.Text.Trim = "" Then lblError.Text &= "Odometer start,"
        If txtOEnd.Text.Trim = "" Then lblError.Text &= "Odometer end,"
        If txtOTotal.Text.Trim = "" Then lblError.Text &= "Odometer total,"
        If txtTStart.Text.Trim = "" Or txtTStart.Text.Trim = "00:00" Then lblError.Text &= "Timing Start,"
        If txtTEnd.Text.Trim = "" Or txtTEnd.Text.Trim = "00:00" Then lblError.Text &= "Timing end,"
        If txtTTotal.Text.Trim = "" Then lblError.Text &= "Timing total,"
        If txtRef.Text = "" Then lblError.Text &= " Ref.No "
        If h_TEI_ID.Value <> 0 Then lblError.Text &= " This month extra trips are posted already.You can not enter for this month, "
        PurposeExists = Mainclass.getDataValue("select case when COUNT(*)>0 then 1 else 0 end from TPTMASTER where tms_descr='" & RadPurpose.Text & "' and tms_type='D'", "OASIS_TRANSPORTConnectionString")
        If PurposeExists = 0 Then lblError.Text &= "Purpose is "
        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            lblError.Visible = True
            Return
        End If
        Try
            Dim pParms(25) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TEX_ID", h_PRI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TEX_NO", txtTSRSNo.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@TEX_DATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(4) = Mainclass.CreateSqlParameter("@TEX_BSU_ID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TEX_VEH_ID", txtVehNo.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@TEX_VEH_TYPE", ddlVehType.SelectedValue, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@TEX_DRIVER", Trim(txtDriver.Text), SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@TEX_FROM", txtLOCFROM.Text, SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@TEX_TO", txtLOCTo.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@TEX_DUTY", RadPurpose.Text, SqlDbType.VarChar)
            pParms(11) = Mainclass.CreateSqlParameter("@TEX_OSTART", txtOStart.Text, SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@TEX_OEND", txtOEnd.Text, SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@TEX_OTOTAL", txtOTotal.Text, SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@TEX_TSTART", txtTStart.Text, SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@TEX_TEND", txtTEnd.Text, SqlDbType.VarChar)
            pParms(16) = Mainclass.CreateSqlParameter("@TEX_TTOTAL", txtTTotal.Text, SqlDbType.VarChar)
            pParms(17) = Mainclass.CreateSqlParameter("@TEX_GROSS", txtGross.Text, SqlDbType.VarChar)
            pParms(19) = Mainclass.CreateSqlParameter("@TEX_SALIK", txtSalik.Text, SqlDbType.VarChar)
            pParms(20) = Mainclass.CreateSqlParameter("@TEX_NET", txtNet.Text, SqlDbType.VarChar)
            pParms(21) = Mainclass.CreateSqlParameter("@TEX_REF", txtRef.Text, SqlDbType.VarChar)
            pParms(22) = Mainclass.CreateSqlParameter("@TEX_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(23) = Mainclass.CreateSqlParameter("@TEX_DESCR", txtDescr.Text, SqlDbType.VarChar)
            pParms(24) = Mainclass.CreateSqlParameter("@TEX_CBSU_ID", radCBSU.SelectedValue, SqlDbType.VarChar)
            pParms(25) = Mainclass.CreateSqlParameter("@TEX_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTPTEXTRA", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    clearScreen()
                    h_BSUId.Value = ddlBusinessUnit.SelectedValue
                    refreshGrid(True)
                    lblError.Text = "Data Saved Successfully !!!"
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    <System.Web.Services.WebMethod()>
    Public Shared Function GetVehicle(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_Sql As String
        str_Sql = "SELECT VEH_REGNO FROM VEHICLE_M inner JOIN CATEGORY_M on VEH_CAT_ID=CAT_ID INNER JOIN TRANSPORT.VEHICLE_MAKE on VEH_VMK_ID=VMK_ID and VEH_BSU_ID Like '%" & contextKey & "%' and VEH_REGNO like '%" & prefixText & "%' order by veh_regno "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count

        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GetBusType(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_Sql As String
        str_Sql = "SELECT 'BB' VehType UNION ALL SELECT 'MB' VehType "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count

        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetArea(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_Sql As String
        str_Sql = " select DISTINCT TMS_DESCR TMS_DESCR from TPTMASTER where TMS_TYPE='A' and TMS_DESCR like '%" & prefixText & "%' ORDER by TMS_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)
        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer
        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GetDuty(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_Sql As String

        str_Sql = " select TMS_DESCR from TPTMASTER where TMS_TYPE='D' and TMS_DESCR like '%" & prefixText & "%' ORDER by TMS_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)
        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer
        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub
    Private Sub showEdit(ByVal showId As Integer)

        'Dim sqlParamView(2) As SqlParameter
        'Dim cmd As New SqlCommand

        'sqlParamView(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
        'cmd.Parameters.Add(sqlParamView(0))
        'sqlParamView(1) = Mainclass.CreateSqlParameter("@TEX_NO", showId, SqlDbType.VarChar)
        'cmd.Parameters.Add(sqlParamView(1))
        'sqlParamView(2) = Mainclass.CreateSqlParameter("@TEX_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        'cmd.Parameters.Add(sqlParamView(2))

        Dim GET_ExtraTripDetails As String = ""
        GET_ExtraTripDetails = "select TEX_NO,TEX_ID, replace(convert(varchar(30), TEX_DATE, 106),' ','/') Date, VEH_REGNO, case when TEX_VEH_TYPE=1 then 'MB' ELSE CASE WHEN TEX_VEH_TYPE=2 THEN  'BB' ELSE CASE WHEN TEX_VEH_TYPE=4 THEN 'SB' ELSE  'VAN' END END END VEH_TYPE, DRIVER, T1.TMS_DESCR LOC_FROM, T2.TMS_DESCR LOC_TO, T3.TMS_DESCR DUTY, TEX_OSTART, TEX_OEND, TEX_OTOTAL, TEX_TSTART, TEX_TEND, TEX_TTOTAL, TEX_GROSS, TEX_SALIK, TEX_NET,TEX_REF,E.EMPNO,isnull(TEX_DESCR,'') DESCR,isnull(TEX_USER,'')TEX_USER,TEX_TEI_ID,TEX_CBSU_ID,ISNULL(BSU_NAME,'') CUNIT FROM TPTEXTRA inner JOIN VEHICLE_M on VEH_ID=TEX_VEH_ID INNER JOIN VW_DRIVER on driver_empid=TEX_DRIVER INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=TEX_DRIVER LEFT OUTER JOIN TPTMASTER T1 on TEX_FROM=T1.TMS_ID AND TEX_BSU_ID=T1.TMS_BSU_ID AND T1.TMS_TYPE='A' LEFT OUTER JOIN TPTMASTER T2 on TEX_TO=T2.TMS_ID AND TEX_BSU_ID=T2.TMS_BSU_ID AND T2.TMS_TYPE='A' LEFT OUTER JOIN TPTMASTER T3 on TEX_DUTY=T3.TMS_ID  AND T3.TMS_TYPE='D' LEFT OUTER JOIN OASIS..BUSINESSUNIT_M on BSU_ID=TEX_CBSU_ID where TEX_NO ='" & showId & "' And TEX_BSU_ID='" & ddlBusinessUnit.SelectedValue & "'"



        Dim rdrTrnsprt As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, GET_ExtraTripDetails)

        If rdrTrnsprt.HasRows Then
            rdrTrnsprt.Read()
            If rdrTrnsprt("TEX_TEI_ID") <> 0 Then
                lblError.Text = "This record is already posted,you can not modify!"
                Exit Sub
            End If
            h_PRI_ID.Value = rdrTrnsprt("TEX_ID")
            txtDate.Text = rdrTrnsprt("DATE")
            txtGross.Text = rdrTrnsprt("TEX_GROSS")
            txtNet.Text = rdrTrnsprt("TEX_NET")
            txtOEnd.Text = rdrTrnsprt("TEX_OEND")
            txtOStart.Text = rdrTrnsprt("TEX_OSTART")
            txtOTotal.Text = rdrTrnsprt("TEX_OTOTAL")
            txtSalik.Text = rdrTrnsprt("TEX_SALIK")
            txtTEnd.Text = rdrTrnsprt("TEX_TEND")
            txtTSRSNo.Text = rdrTrnsprt("TEX_NO")
            txtTStart.Text = rdrTrnsprt("TEX_TSTART")
            txtTTotal.Text = rdrTrnsprt("TEX_TTOTAL")
            txtVehNo.Text = rdrTrnsprt("VEH_REGNO")
            ddlVehType.SelectedItem.Text = rdrTrnsprt("VEH_TYPE")
            ddlVehType.SelectedValue = rdrTrnsprt("VEH_TYPE")
            txtLOCFROM.Text = rdrTrnsprt("LOC_FROM")
            txtLOCTo.Text = rdrTrnsprt("LOC_TO")
            txtDriverName.Text = rdrTrnsprt("DRIVER")
            txtDriver.Text = rdrTrnsprt("EMPNO")
            RadPurpose.Text = rdrTrnsprt("DUTY")
            txtRef.Text = rdrTrnsprt("TEX_REF")
            txtDescr.Text = rdrTrnsprt("DESCR")
            h_TEI_ID.Value = rdrTrnsprt("TEX_TEI_ID")
            CalculateRate()
        End If
        rdrTrnsprt.Close()
        rdrTrnsprt = Nothing
    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub
    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            If gvr.Cells(20).Text <> "0" Then
                gvr.Cells(21).Enabled = False
            End If
        Next
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            gvr.Cells(20).Visible = False
        Next
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTEXID"), Label)
        Dim TDate As Date
        TDate = Mainclass.getDataValue("select getdate()", "OASIS_TRANSPORTConnectionString")

        'If TDate.Day >= 6 And TDate.Month - 1 >= CDate(txtDate.Text).Month Then
        If TDate.Day >= 6 And CDate(txtDate.Text) >= CDate(TDate.AddDays((TDate.Day - 1) * -1).AddMonths(-1)) And CDate(txtDate.Text) <= CDate(TDate.AddDays((TDate.Day - 1) * -1)).AddDays(-1) Then
            lblError.Text = "This month is already closed,You can not delete now...."
            Exit Sub
        End If


        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from TPTEXTRA where TEX_ID=" & lblID.Text)
        refreshGrid(True)
    End Sub
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid(False)
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "rptExtraTrips"
            Dim sqlParam(2) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@FROMDT", txtFromDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@TODT", txtToDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(1))

            sqlParam(2) = Mainclass.CreateSqlParameter("@BSUID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("preparedby") = Mainclass.getDataValue("SELECT TOP 1 TEX_USER FROM TPTEXTRA where TEX_BSU_ID='" & ddlBusinessUnit.SelectedValue & "' ORDER BY TEX_ID  DESC", "OASIS_TRANSPORTConnectionString")
            params("reportCaption") = "Transport Extra Trips"
            params("bsuName") = ddlBusinessUnit.SelectedItem.Text
            params("DateRange") = txtFromDate.Text & " - " & txtToDate.Text


            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptExtraTrips.rpt"

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub BindVehicle()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select '' VEH_REGNO union all SELECT VEH_REGNO FROM VEHICLE_M inner JOIN CATEGORY_M on VEH_CAT_ID=CAT_ID"
        sql_str &= "  INNER JOIN TRANSPORT.VEHICLE_MAKE on VEH_VMK_ID=VMK_ID "
        sql_str &= " GROUP BY VEH_REGNO ORDER BY VEH_REGNO "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
    End Sub
    Private Sub BindLocationFrom()
    End Sub
    Private Sub BindPurpose()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select  TMS_DESCR from VW_TPT_PURPOSE order by TMS_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
        RadPurpose.DataSource = ds
        RadPurpose.DataTextField = "TMS_DESCR"
        RadPurpose.DataValueField = "TMS_DESCR"
        RadPurpose.DataBind()






    End Sub


    Private Sub BindVehicleType()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = " select '' VehType UNION ALL SELECT 'BB' VehType UNION ALL SELECT 'MB' VehType UNION ALL SELECT 'VAN' VehType UNION ALL SELECT 'SB' VehType "
        sql_str &= " order by VehType "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
    End Sub
    Private Sub BindDriver()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select '' EMPNO,'' EMP_NAME UNION ALL select DRIVER_EMPNO,DRIVER from [VW_DRIVER] A INNER JOIN OASIS..employee_m B ON B.EMP_ID=A.DRIVER_empid "
        sql_str &= " order by EMPNO "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
    End Sub
    Protected Sub RadDriver_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)
        Dim sqlSelectCommand As String = "select DRIVER_EMPNO,DRIVER from [VW_DRIVER] A INNER JOIN OASIS..employee_m B ON B.EMP_ID=DRIVER_EMPID where EMPNO LIKE '%" & e.Text & "%'"
        Dim adapter As New SqlDataAdapter(sqlSelectCommand, ConnectionManger.GetOASISTRANSPORTConnectionString)
        adapter.SelectCommand.Parameters.AddWithValue("@text", e.Text)
        Dim dataTable As New DataTable()
        adapter.Fill(dataTable)
        For Each dataRow As DataRow In dataTable.Rows
            Dim item As New RadComboBoxItem()
            item.Text = DirectCast(dataRow("EMPNO"), String)
            item.Value = dataRow("EMPNO").ToString()

            Dim EmpName As String = DirectCast(dataRow("DRIVER"), String)
            item.Attributes.Add("DRIVER", EmpName.ToString())
            item.DataBind()
        Next
    End Sub
    Protected Sub RadVehicleNo_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)
        Dim sqlSelectCommand As String = "SELECT VEH_REGNO FROM VEHICLE_M inner JOIN CATEGORY_M on VEH_CAT_ID=CAT_ID  INNER JOIN TRANSPORT.VEHICLE_MAKE on VEH_VMK_ID=VMK_ID where VEH_REGNO LIKE '%" & e.Text & "%' GROUP BY VEH_REGNO ORDER BY VEH_REGNO "
        Dim adapter As New SqlDataAdapter(sqlSelectCommand, ConnectionManger.GetOASISTRANSPORTConnectionString)
        adapter.SelectCommand.Parameters.AddWithValue("@text", e.Text)
        Dim dataTable As New DataTable()
        adapter.Fill(dataTable)
        For Each dataRow As DataRow In dataTable.Rows
            Dim item As New RadComboBoxItem()
            item.Text = DirectCast(dataRow("VEH_REGNO"), String)
            item.Value = dataRow("VEH_REGNO").ToString()
            item.DataBind()
        Next
    End Sub
    Protected Sub RadPurpose_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)
        Dim sqlSelectCommand As String = "select TMS_DESCR from VW_TPT_PURPOSE  where  TMS_DESCR LIKE '%" & e.Text & "%'  ORDER BY TMS_DESCR "
        Dim adapter As New SqlDataAdapter(sqlSelectCommand, ConnectionManger.GetOASISTRANSPORTConnectionString)
        adapter.SelectCommand.Parameters.AddWithValue("@text", e.Text)
        Dim dataTable As New DataTable()
        adapter.Fill(dataTable)
        For Each dataRow As DataRow In dataTable.Rows
            Dim item As New RadComboBoxItem()
            item.Text = DirectCast(dataRow("TMS_DESCR"), String)
            item.Value = dataRow("TMS_DESCR").ToString()
            RadPurpose.Items.Add(item)
            item.DataBind()
        Next
    End Sub

    Private Sub BindChargingBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim BSUParms(3) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@BSUID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@EMPNO", "EXTRATRIP", SqlDbType.VarChar)
            BSUParms(3) = Mainclass.CreateSqlParameter("@LoginBsuID", Session("sBsuid"), SqlDbType.VarChar)
            radCBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "getUnitsForChargingBSU", BSUParms)
            radCBSU.DataTextField = "DESCR"
            radCBSU.DataValueField = "ID"
            radCBSU.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Private Sub BindBsu()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Try

            Dim str_Sql1 As String = "SELECT  '' as VehType union all select 'BB' VehType UNION ALL SELECT 'MB' VehType UNION ALL SELECT 'VAN' VehType  UNION ALL SELECT 'SB' VehType order by VehType "
            Dim BSUParms(2) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            ddlBusinessUnit.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "gETUNITSFOREXTRATRIPS", BSUParms)
            ddlVehType.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql1)
            ddlBusinessUnit.DataTextField = "BSU_NAME"
            ddlBusinessUnit.DataValueField = "SVB_BSU_ID"
            ddlVehType.DataTextField = "VehType"
            ddlVehType.DataValueField = "VehType"
            ddlBusinessUnit.DataBind()
            ddlVehType.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlBusinessUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessUnit.SelectedIndexChanged
        clearScreen()

        If ddlBusinessUnit.SelectedValue <> "" Then
            h_BSUId.Value = ddlBusinessUnit.SelectedValue
            h_Rate.Value = Mainclass.getDataValue("select CNB_RATEKM  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")
            h_mimimum.Value = Mainclass.getDataValue("select CNB_MINRATE  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")
            If h_Rate.Value = "" Then
                h_Rate.Value = 0
            End If
            If h_mimimum.Value = "" Then
                h_mimimum.Value = 0
            End If
        Else
            h_Rate.Value = 0
        End If
        txtGross.Text = Val(h_Rate.Value) * Val(txtOTotal.Text)
        If Val(txtGross.Text) <= h_mimimum.Value Then
            txtGross.Text = h_mimimum.Value
        End If
        txtNet.Text = Val(txtGross.Text) + 4 * (Val(txtSalik.Text))
        h_BSUId.Value = ddlBusinessUnit.SelectedValue
        refreshGrid()
        BindChargingBsu()

    End Sub
    Protected Sub txtVehNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtVehNo.TextChanged
        Dim COUNT As Integer = 0
        Dim rate As Long = 0.0
        Dim VehType As String = ""

        COUNT = Mainclass.getDataValue("SELECT COUNT(*)from VEHICLE_M where rtrim(ltrim(VEH_REGNO))='" & Trim(txtVehNo.Text) & "'", "OASIS_TRANSPORTConnectionString")
        VehType = Mainclass.getDataValue("select CAT_DESCRIPTION  from CATEGORY_M where CAT_ID in(select veh_cat_id from VEHICLE_M where rtrim(ltrim(VEH_REGNO))='" & Trim(txtVehNo.Text) & "')", "OASIS_TRANSPORTConnectionString")
        If COUNT > 0 Then
            If VehType = "MEDIUM BUS" Then
                h_VehicleType.Value = VehType
                ddlVehType.SelectedValue = "MB"
                ddlVehType.SelectedItem.Text = "MB"

            ElseIf VehType = "BIG BUS" Then
                h_VehicleType.Value = VehType
                ddlVehType.SelectedValue = "BB"
                ddlVehType.SelectedItem.Text = "BB"
            ElseIf VehType = "MINI BUS" Then
                h_VehicleType.Value = VehType
                ddlVehType.SelectedValue = "SB"
                ddlVehType.SelectedItem.Text = "SB"
            Else
                h_VehicleType.Value = VehType
                ddlVehType.SelectedValue = "VAN"
                ddlVehType.SelectedItem.Text = "VAN"
            End If
            If ddlVehType.SelectedValue <> "" Then
                h_Rate.Value = Mainclass.getDataValue("select CNB_RATEKM  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")
                h_mimimum.Value = Mainclass.getDataValue("select CNB_MINRATE  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")
                If h_Rate.Value = "" Then
                    h_Rate.Value = 0
                End If
                If h_mimimum.Value = "" Then
                    h_mimimum.Value = 0
                End If
            Else
                h_Rate.Value = 0
            End If
            txtGross.Text = Val(h_Rate.Value) * Val(txtOTotal.Text)
            If Val(txtGross.Text) <= h_mimimum.Value Then
                txtGross.Text = h_mimimum.Value
            End If
            txtNet.Text = Val(txtGross.Text) + 4 * (Val(txtSalik.Text))
            txtDriver.Focus()

        Else
            lblError.Text = "Invalid Vehicle Reg No...!:"
            txtVehNo.Text = ""
            txtVehNo.Focus()
            Exit Sub
        End If
    End Sub
    Protected Sub ddlVehType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVehType.SelectedIndexChanged
        Dim rate As Long = 0.0
        If ddlVehType.SelectedValue <> "" Then
            h_Rate.Value = Mainclass.getDataValue("select CNB_RATEKM  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")
            h_mimimum.Value = Mainclass.getDataValue("select CNB_MINRATE  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")

            If h_Rate.Value = "" Then
                h_Rate.Value = 0
            End If
            If h_mimimum.Value = "" Then
                h_mimimum.Value = 0
            End If
        Else
            h_Rate.Value = 0
        End If
        txtGross.Text = Val(h_Rate.Value) * Val(txtOTotal.Text)

        If Val(txtGross.Text) <= h_mimimum.Value Then
            txtGross.Text = h_mimimum.Value
        End If
        txtNet.Text = Val(txtGross.Text) + 4 * (Val(txtSalik.Text))

    End Sub
    Protected Sub txtDriver_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDriver.TextChanged
        Dim DriverName As String
        DriverName = Mainclass.getDataValue("select Driver from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtDriver.Text) & "'", "OASIS_TRANSPORTConnectionString")
        If DriverName <> "" Then
            txtDriverName.Text = DriverName
            txtRef.Focus()
        Else
            lblError.Text = "Invalid Driver No...!:"
            txtDriver.Text = ""
            txtDriver.Focus()
            Exit Sub
        End If
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        txtFromDate.Text = ""
        txtToDate.Text = ""
        refreshGrid(True)
    End Sub
    Private Sub CalculateRate()
        If ddlBusinessUnit.SelectedValue <> "" And ddlVehType.SelectedValue <> "" Then
            h_Rate.Value = Mainclass.getDataValue("select CNB_RATEKM  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")
            h_mimimum.Value = Mainclass.getDataValue("select CNB_MINRATE  from TPTCONTRACT_B where CNB_BSU_ID ='" & ddlBusinessUnit.SelectedValue & "' and CNB_CAT_ID='" & ddlVehType.SelectedValue & "'  ", "OASIS_TRANSPORTConnectionString")
            If h_Rate.Value = "" Then
                h_Rate.Value = 0
            End If
            If h_mimimum.Value = "" Then
                h_mimimum.Value = 0
            End If
        Else
            h_Rate.Value = 0
        End If
        txtGross.Text = Val(h_Rate.Value) * Val(txtOTotal.Text)
        If Val(txtGross.Text) <= h_mimimum.Value Then
            txtGross.Text = h_mimimum.Value
        End If
        txtNet.Text = Val(txtGross.Text) + 4 * (Val(txtSalik.Text))
    End Sub
    Protected Sub txtTStart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTStart.TextChanged
        txtTEnd.Focus()
    End Sub
    <WebMethod()>
    Public Shared Function LoadFromLocations(ByVal input As String) As List(Of String)
        Return GetCountries().FindAll(Function(item) item.ToLower().Contains(input.ToLower()))
    End Function
    Public Shared Function GetCountries() As List(Of String)
        Dim CountryInformation As New List(Of String)()
        Dim str_Sql As String
        str_Sql = " select DISTINCT TMS_DESCR TMS_DESCR from TPTMASTER where TMS_TYPE='A'  ORDER by TMS_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)
        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer
        For i2 = 0 To intRows - 1
            CountryInformation.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return CountryInformation
    End Function
End Class





