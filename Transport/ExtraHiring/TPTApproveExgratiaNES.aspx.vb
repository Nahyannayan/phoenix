﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports Telerik.Web.UI

Partial Class Transport_ExtraHiring_TPTApproveExgratiaNES
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200410") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If ViewState("EntryId") = "0" Then
                SetDataMode("add")
                ClearDetails()
                setModifyvalues(0)
            Else
                If ViewState("datamode") = "add" Then
                    SetDataMode("add")
                Else
                    SetDataMode("view")
                    showNoRecordsFound()
                End If
                setModifyvalues(ViewState("EntryId"))
            End If

            showNoRecordsFound()
            grdInvoiceDetails.DataSource = TPTInvoiceDetails
            grdInvoiceDetails.DataBind()
            showNoRecordsFound()
        Else
            If ViewState("EntryId") = "0" Then
                calcTotals()
            End If

        End If

    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable 'And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        grdInvoiceDetails.ShowFooter = Not mDisable
        btnCancel.Visible = Not ItemEditMode

    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            Dim Amount(3) As String
            Dim bsu_City As String = ""
            Dim bsu_ID As String = ""

            h_EntryId.Value = p_Modifyid




            If p_Modifyid = "0" Then

            Else

                Dim str_conn As String = connectionString
                Dim dt, dt1 As New DataTable
                Dim NonGems As Boolean = False


                MyAprID.Value = Mainclass.getDataValue("SELECT EAL_EAM_ID from [EXGRATIA_APPROVER_USER_LIST] where EAL_TYPE='NES' and eal_user='" & Session("sUsr_name") & "'", "OASIS_TRANSPORTConnectionString")

                If MyAprID.Value <> "" Then
                    MyNextAprID.Value = Mainclass.getDataValue("SELECT EAL_EAM_ID from [EXGRATIA_APPROVER_USER_LIST] where EAL_TYPE='NES' and eal_eam_id > " & MyAprID.Value & " order by eal_eam_id ", "OASIS_TRANSPORTConnectionString")
                Else
                    usrMessageBar.ShowNotification("You are not there in the Approval List!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub

                End If




                If ViewState("datamode") = "add" Then

                    txtBatchNo.Text = p_Modifyid
                    bsu_City = Mainclass.getDataValue("SELECT case when case when max(BSU_CITY)='ALN' then 'AUH' else max(BSU_CITY) end not in('AUH','DXB','RAK','SHJ','AJM','UAQ','FUJ') then 'NA' else case when max(BSU_CITY)='ALN' then 'AUH' else max(BSU_CITY) end end   BSU_CITY   FROM tptexgratiacharges INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID WHERE LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' AND EGC_EGI_ID=0  GROUP BY  LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME ", "OASIS_TRANSPORTConnectionString")

                    h_EntryId.Value = 0


                    txtNESAmount.Text = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1 THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END end [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0 and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and egN_emp_id<>0   and EGN_DELETED=0 and EGN_DRIVERTYPE=1 And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)NES  ", "OASIS_TRANSPORTConnectionString")
                    txtVillaAmount.Text = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 and EGN_DRIVERTYPE=2 And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")
                    txtIITAmountT.Text = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_DELETED=0 and EGN_DRIVERTYPE=3 And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)IIT  ", "OASIS_TRANSPORTConnectionString")
                    txtOutStation.Text = Mainclass.getDataValue("select isnull(sum(EGN_OUTSTATION),0) from (select MAX(EGN_OUTSTATION*25)  [EGN_OUTSTATION] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and  LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OUTSTATION ", "OASIS_TRANSPORTConnectionString")
                    txtONSAmount.Text = Mainclass.getDataValue(" select isnull(sum(EGN_OVERNIGHTSTAY),0) from (select case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end  [EGN_OVERNIGHTSTAY] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' and left(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OVERNIGHTSTAY ", "OASIS_TRANSPORTConnectionString")
                    txtOTAmount.Text = Mainclass.getDataValue("SELECT isnull(SUM(NetAmount),0) [NET AMOUNT] FROM  (select EGN_BSU_ID,EGN_DRIVERTYPE,CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1 THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 72 ELSE 0  END  ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 150 ELSE 0 END END END END [NetAmount],EGN_EMP_ID,EGN_TRDATE from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0 and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE) Net ", "OASIS_TRANSPORTConnectionString")
                    txtNetTotal.Text = Val(txtOutStation.Text) + Val(txtONSAmount.Text) + Val(txtOTAmount.Text)


                    h_EntryId.Value = 0
                    'fillGridView(grdInvoiceDetails, "select '" & p_Modifyid & "' as Batchstr,replace(convert(varchar(30), EGN_TRDATE, 106),' ','/') [DATE],sum(cast(EGN_TTOTAL as numeric(12,2)))[Total_Hrs],max(case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA' else case when EGN_DRIVERTYPE=3 then 'IIT'  end end end)  DriverType,max(DRIVER)DRIVER,max(DRIVER_EMPNO) DRIVER_EMPNO,max(BSU_NAME)BSU_NAME,max(BSU_SHORTNAME)BSU_SHORTNAME,CASE WHEN max(EGN_DRIVERTYPE) ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END EGN_AMOUNT, case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end  EGN_OVERNIGHTSTAY,MAX(EGN_OUTSTATION*25)  [EGN_OUTSTATION],MAX(EGN_OUTSTATION*25)+CASE WHEN max(EGN_DRIVERTYPE) ='1' THEN case when sum(cast(egn_ttotal as float))>=12 then (sum(cast(egn_ttotal as float))-12)*12 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='2' THEN case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end ELSE CASE WHEN max(EGN_DRIVERTYPE) ='3' THEN case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end END END END+case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end EGN_TOTAL    FROM TPTEXGRATIACHARGESNES  LEFT OUTER JOIN VW_DRIVER  on driver_empid=EGN_EMP_ID left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  LEFT OUTER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID where LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "'  and EGN_EGI_ID =0 and EGN_APR_ID='" & MyAprID.Value & "'  and EGN_deleted=0 and egN_emp_id<>0 group by EGN_EMP_ID,EGN_TRDATE,EGN_drivertype")
                    'Dim SqlStr As String = "Select EGN_ID, replace(convert(varchar(30), EGN_TRDATE, 106),' ','-') EGN_TRDATE,EGN_BSU_ID, EGN_EMP_ID, EGN_TSTART, EGN_TEND, EGN_TTOTAL, EGN_REMARKS, EGN_EGI_ID, EGN_FYEAR, EGN_USER, EGN_DELETED, EGN_DATE, EGN_DRIVERTYPE,case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA'  else 'IIT' end end DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,isnull(EGN_PURPOSE,'') TMS_DESCR,case when EGN_HOLIDAY=1 then 'YES' else 'No' END EGN_HOLIDAY,EGN_OUTSTATION*25 EGN_OUTSTATION,case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end EGN_OVERNIGHTSTAY    FROM TPTEXGRATIACHARGESNES  inner join VW_DRIVER  on driver_empid=EGN_EMP_ID  inner Join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID   where LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' And EGN_DELETED = 0  And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' order by EGN_TRDATE desc "
                    Dim SqlStr As String = "Select EGN_ID, replace(convert(varchar(30), EGN_TRDATE, 106),' ','-') EGN_TRDATE,EGN_BSU_ID, EGN_EMP_ID, EGN_TSTART, EGN_TEND, EGN_TTOTAL, EGN_REMARKS, EGN_EGI_ID, EGN_FYEAR, EGN_USER,EGN_REMARKS, EGN_DELETED, EGN_DATE, EGN_DRIVERTYPE,case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA'  else 'IIT' end end DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,isnull(EGN_PURPOSE,'') TMS_DESCR,case when EGN_HOLIDAY=1 then 'YES' else 'NO' END EGN_HOLIDAY,EGN_OUTSTATION*25 EGN_OUTSTATION,case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end EGN_OVERNIGHTSTAY,CASE WHEN egn_drivertype = 1 THEN CASE WHEN Cast(egn_holiday AS INTEGER) = 1 THEN Cast(egn_ttotal AS FLOAT) * 12 ELSE CASE WHEN Cast(egn_ttotal AS FLOAT) >= 12 THEN (Cast(egn_ttotal AS FLOAT) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 72 ELSE 0 END ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 150 ELSE 0 END END END END OTAMOUNT,EGN_HOLIDAY,EGN_OUTSTATION*25 +case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end +CASE WHEN egn_drivertype = 1 THEN CASE WHEN Cast(egn_holiday AS INTEGER) = 1 THEN Cast(egn_ttotal AS FLOAT) * 12 ELSE CASE WHEN Cast(egn_ttotal AS FLOAT) >= 12 THEN (Cast(egn_ttotal AS FLOAT) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 72 ELSE 0 END ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 150 ELSE 0 END END END END TOTALAMOUNT    FROM TPTEXGRATIACHARGESNES  inner join VW_DRIVER  on driver_empid=EGN_EMP_ID  inner Join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID   where LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' And EGN_DELETED = 0  And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' order by EGN_TRDATE desc "
                    fillGridView(grdInvoiceDetails, SqlStr)
                    If grdInvoiceDetails.Rows.Count >= 1 Then
                        btnReject.Visible = True
                        btnAPPROVE.Visible = True
                    Else
                        btnReject.Visible = False
                        btnAPPROVE.Visible = False

                    End If

                Else
                    If UCase(Request.QueryString("filter")) = "APPROVED" Then
                        btnPrint.Visible = True
                        VATDetails3.Visible = False
                    Else
                        btnPrint.Visible = False
                        VATDetails3.Visible = True
                    End If

                    txtNetTotal.Text = Mainclass.getDataValue("SELECT isnull(SUM(NetAmount),0) [NET AMOUNT] FROM  (select EGN_BSU_ID,EGN_DRIVERTYPE,CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1 THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 72 ELSE 0  END  ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 150 ELSE 0 END END END END [NetAmount],EGN_EMP_ID,EGN_TRDATE from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0 and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0   group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE) Net ", "OASIS_TRANSPORTConnectionString")
                    txtNESAmount.Text = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1 THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END end [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0 and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and egN_emp_id<>0   and EGN_DELETED=0 and EGN_DRIVERTYPE=1 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)NES  ", "OASIS_TRANSPORTConnectionString")
                    txtVillaAmount.Text = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 72 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 and EGN_DRIVERTYPE=2 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)VILLA ", "OASIS_TRANSPORTConnectionString")
                    txtIITAmountT.Text = Mainclass.getDataValue("select isnull(sum(netamount),0) from (select  case when sum(cast(egn_ttotal as float))>=0 then 150 else 0 end  [NetAmount] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m  on bsu_id=egn_bsu_id WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_DELETED=0 and EGN_DRIVERTYPE=3 group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)IIT  ", "OASIS_TRANSPORTConnectionString")
                    txtOutStation.Text = Mainclass.getDataValue("select isnull(sum(EGN_OUTSTATION),0) from (select MAX(EGN_OUTSTATION*25)  [EGN_OUTSTATION] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and  LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0  group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OUTSTATION ", "OASIS_TRANSPORTConnectionString")
                    txtONSAmount.Text = Mainclass.getDataValue(" select isnull(sum(EGN_OVERNIGHTSTAY),0) from (select case when MAX(cast(EGN_OVERNIGHTSTAY as integer))=1 then case when max(EGN_DRIVERTYPE)='1' then 100 else 50 end  else 0 end  [EGN_OVERNIGHTSTAY] from TPTEXGRATIACHARGESNES left outer join oasis..businessunit_m on bsu_id=EGN_BSU_ID WHERE EGn_EGI_ID = 0    and LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' and EGN_deleted=0 and egN_emp_id<>0 and left(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)OVERNIGHTSTAY ", "OASIS_TRANSPORTConnectionString")



                    txtBatchNo.Text = p_Modifyid

                    Dim Sqlstr1 As String = ""
                    If UCase(Request.QueryString("filter")) = "APPROVED" Then
                        'Sqlstr1 = "Select EGN_ID, replace(convert(varchar(30), EGN_TRDATE, 106),' ','-') EGN_TRDATE,EGN_BSU_ID, EGN_EMP_ID, EGN_TSTART, EGN_TEND, EGN_TTOTAL, EGN_REMARKS, EGN_EGI_ID, EGN_FYEAR, EGN_USER, EGN_DELETED, EGN_DATE, EGN_DRIVERTYPE,case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA'  else 'IIT' end end DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,isnull(EGN_PURPOSE,'') TMS_DESCR,case when EGN_HOLIDAY=1 then 'YES' else 'No' END EGN_HOLIDAY,EGN_OUTSTATION*25 EGN_OUTSTATION,case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end EGN_OVERNIGHTSTAY    FROM TPTEXGRATIACHARGESNES  inner join VW_DRIVER  on driver_empid=EGN_EMP_ID  inner Join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID   where LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' And EGN_DELETED = 0  And isnull(EGN_APR_ID, 0)=10 order by EGN_TRDATE desc "
                        Sqlstr1 = "Select EGN_ID, replace(convert(varchar(30), EGN_TRDATE, 106),' ','-') EGN_TRDATE,EGN_BSU_ID, EGN_EMP_ID,EGN_REMARKS, EGN_TSTART, EGN_TEND, EGN_TTOTAL, EGN_REMARKS, EGN_EGI_ID, EGN_FYEAR, EGN_USER, EGN_DELETED, EGN_DATE, EGN_DRIVERTYPE,case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA'  else 'IIT' end end DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,isnull(EGN_PURPOSE,'') TMS_DESCR,case when EGN_HOLIDAY=1 then 'YES' else 'No' END EGN_HOLIDAY,EGN_OUTSTATION*25 EGN_OUTSTATION,case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end EGN_OVERNIGHTSTAY,CASE WHEN egn_drivertype = 1 THEN CASE WHEN Cast(egn_holiday AS INTEGER) = 1 THEN Cast(egn_ttotal AS FLOAT) * 12 ELSE CASE WHEN Cast(egn_ttotal AS FLOAT) >= 12 THEN (Cast(egn_ttotal AS FLOAT) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 72 ELSE 0 END ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 150 ELSE 0 END END END END OTAMOUNT,EGN_HOLIDAY,EGN_OUTSTATION*25 +case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end +CASE WHEN egn_drivertype = 1 THEN CASE WHEN Cast(egn_holiday AS INTEGER) = 1 THEN Cast(egn_ttotal AS FLOAT) * 12 ELSE CASE WHEN Cast(egn_ttotal AS FLOAT) >= 12 THEN (Cast(egn_ttotal AS FLOAT) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 72 ELSE 0 END ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 150 ELSE 0 END END END END TOTALAMOUNT    FROM TPTEXGRATIACHARGESNES  inner join VW_DRIVER  on driver_empid=EGN_EMP_ID  inner Join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID   where LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' And EGN_DELETED = 0  And isnull(EGN_APR_ID, 0)=10 order by EGN_TRDATE desc "
                    Else
                        Sqlstr1 = "Select EGN_ID, replace(convert(varchar(30), EGN_TRDATE, 106),' ','-') EGN_TRDATE,EGN_BSU_ID, EGN_EMP_ID,EGN_REMARKS, EGN_TSTART, EGN_TEND, EGN_TTOTAL, EGN_REMARKS, EGN_EGI_ID, EGN_FYEAR, EGN_USER, EGN_DELETED, EGN_DATE, EGN_DRIVERTYPE,case when EGN_DRIVERTYPE=1 then 'NES' else case when EGN_DRIVERTYPE=2 then 'VILLA'  else 'IIT' end end DriverType,DRIVER,DRIVER_EMPNO,BSU_NAME,isnull(EGN_PURPOSE,'') TMS_DESCR,case when EGN_HOLIDAY=1 then 'YES' else 'No' END EGN_HOLIDAY,EGN_OUTSTATION*25 EGN_OUTSTATION,case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end EGN_OVERNIGHTSTAY ,CASE WHEN egn_drivertype = 1 THEN CASE WHEN Cast(egn_holiday AS INTEGER) = 1 THEN Cast(egn_ttotal AS FLOAT) * 12 ELSE CASE WHEN Cast(egn_ttotal AS FLOAT) >= 12 THEN (Cast(egn_ttotal AS FLOAT) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 72 ELSE 0 END ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 150 ELSE 0 END END END END OTAMOUNT,EGN_HOLIDAY,EGN_OUTSTATION*25 +case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end +CASE WHEN egn_drivertype = 1 THEN CASE WHEN Cast(egn_holiday AS INTEGER) = 1 THEN Cast(egn_ttotal AS FLOAT) * 12 ELSE CASE WHEN Cast(egn_ttotal AS FLOAT) >= 12 THEN (Cast(egn_ttotal AS FLOAT) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 72 ELSE 0 END ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Cast(egn_ttotal AS FLOAT) >= 1 THEN 150 ELSE 0 END END END END TOTALAMOUNT   FROM TPTEXGRATIACHARGESNES  inner join VW_DRIVER  on driver_empid=EGN_EMP_ID  inner Join oasis..BUSINESSUNIT_M on BSU_ID=EGN_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGN_EMP_ID   where LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME='" & p_Modifyid & "' And EGN_DELETED = 0  And isnull(EGN_APR_ID, 0)='" & MyAprID.Value & "' order by EGN_TRDATE desc "
                    End If
                    fillGridView(grdInvoiceDetails, Sqlstr1)










                End If
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Property TPTInvoiceDetails() As DataTable
        Get
            Return ViewState("InvoiceDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("InvoiceDetails") = value
        End Set
    End Property

    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        TPTInvoiceDetails = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(TPTInvoiceDetails)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        TPTInvoiceDetails = mtable
        fillGrdView.DataSource = TPTInvoiceDetails
        fillGrdView.DataBind()
    End Sub

    Sub ClearDetails()
        h_EntryId.Value = "0"
        'txtBatchDate.Text = Now.ToString("dd/MMM/yyyy")
        txtBatchNo.Text = ""
        txtNetAmount.Text = "0"
        txtNESAmount.Text = "0"
        txtIITAmountT.Text = "0"
        txtOutStation.Text = "0"
        txtONSAmount.Text = "0"
        txtVillaAmount.Text = "0"
        txtremarks.Text = ""
        txtComments.Text = ""

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Private Property EGCFooter() As DataTable
        Get
            Return ViewState("EGCFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EGCFooter") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Sub showNoRecordsFound()
        If Not EGCFooter Is Nothing AndAlso EGCFooter.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdInvoiceDetails.Columns.Count - 2
            grdInvoiceDetails.Rows(0).Cells.Clear()
            grdInvoiceDetails.Rows(0).Cells.Add(New TableCell())
            grdInvoiceDetails.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdInvoiceDetails.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub grdQUD_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdInvoiceDetails.PageIndexChanging
        grdInvoiceDetails.PageIndex = e.NewPageIndex
        grdInvoiceDetails.DataSource = TPTInvoiceDetails
        grdInvoiceDetails.DataBind()
    End Sub

    Protected Sub grdQUD_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdInvoiceDetails.RowCancelingEdit
        grdInvoiceDetails.ShowFooter = True
        grdInvoiceDetails.EditIndex = -1
        grdInvoiceDetails.DataSource = EGCFooter
        grdInvoiceDetails.DataBind()
        showNoRecordsFound()
    End Sub
    Protected Sub lnkView_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub grdInvoiceDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdInvoiceDetails.RowDataBound
        'If ViewState("datamode") <> "add" Then
        '    grdInvoiceDetails.Columns(0).Visible = False
        'End If
    End Sub

    Private Sub ApproveORReject(ByVal action As String)
        Dim strMandatory As New StringBuilder, strError As New StringBuilder
        Dim IDs As String = ""
        Dim chkControl As New HtmlInputCheckBox

        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(gvr.FindControl("lblEGNID"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            If ChkBxItem.Checked = True Then

                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
            End If
        Next

        If IDs = "" Then
            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(9) As SqlParameter
        'pParms(1) = Mainclass.CreateSqlParameter("@EGI_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(1) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@IDS", IDs, SqlDbType.VarChar)
        If MyNextAprID.Value = "" Then
            pParms(5) = Mainclass.CreateSqlParameter("@NEXTAPRID", 10, SqlDbType.Int)
        Else
            pParms(5) = Mainclass.CreateSqlParameter("@NEXTAPRID", MyNextAprID.Value, SqlDbType.Int)
        End If
        pParms(6) = Mainclass.CreateSqlParameter("@BATCH", txtBatchNo.Text, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@ACTION", action, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@REMARKS", txtComments.Text, SqlDbType.VarChar)
        pParms(9) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
        pParms(9).Direction = ParameterDirection.Output
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()

        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "ApproveExgratiaChargesNES", pParms)
            Dim ErrorMSG As String = pParms(9).Value.ToString()
            If ErrorMSG = "" Then
                ViewState("EntryId") = pParms(1).Value
            Else
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification(pParms(9).Value, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            End If
            stTrans.Commit()
            Dim flagAudit As Integer
            If action = "A" Then
                flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, "", "Approved", Page.User.Identity.Name.ToString, Me.Page)
            Else
                flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, "", "Rejected", Page.User.Identity.Name.ToString, Me.Page)
            End If

            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            SetDataMode("view")
            setModifyvalues(txtBatchNo.Text)

            If action = "A" Then
                usrMessageBar.ShowNotification("Data Approved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            Else
                usrMessageBar.ShowNotification("Data Rejected Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            End If
            ClearDetails()
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub
    Sub calcTotals()
        Dim NonGems As Boolean = False
        Dim ModifiedAmount As Double = 0.0
        Dim NetTotal As Decimal = 0, ONSAmount As Decimal = 0, OSAMT As Decimal = 0

        If txtNetTotal.Text = "" Then
            ModifiedAmount = 0.0
        Else
            ModifiedAmount = txtNetTotal.Text
        End If


        For Each gvr As GridViewRow In grdInvoiceDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblOSNet As Label = TryCast(gvr.FindControl("lblOSAMT"), Label)
            Dim lblONSNet As Label = TryCast(gvr.FindControl("lblONSAMT"), Label)
            Dim lblBSUSHORTNAME As Label = TryCast(gvr.FindControl("lblBSUSHORTNAME"), Label)
            Dim lblCBSUSHORTNAME As Label = TryCast(gvr.FindControl("lblCBSUSHORTNAME"), Label)
            If ChkBxItem.Checked = True Then
                If lblBSUSHORTNAME.Text = lblCBSUSHORTNAME.Text Then
                    ONSAmount += Val(lblONSNet.Text)
                ElseIf lblCBSUSHORTNAME.Text = "STS" Then
                    OSAMT += Val(lblOSNet.Text)
                Else
                    '                    Total += Val(lblNet.Text)
                End If
            End If
        Next
        txtONSAmount.Text = ONSAmount
        txtOutStation.Text = OSAMT


    End Sub

    Protected Sub radVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radVAT.SelectedIndexChanged

    End Sub
    Private Sub txtNetTotal_TextChanged(sender As Object, e As EventArgs) Handles txtNetTotal.TextChanged
        calcTotals()
    End Sub

    Protected Sub btnAPPROVE_Click(sender As Object, e As EventArgs) Handles btnAPPROVE.Click
        ApproveORReject("A")

    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        ApproveORReject("R")
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIACHARGESApproved"
            Dim sqlParam(2) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@STRNO", txtBatchNo.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            Dim strlen As Integer
            strlen = Len(txtBatchNo.Text)
            txtBatchNo.Text.Substring(7, strlen - 7)

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("preparedby") = Mainclass.getDataValue("SELECT TOP 1 EGC_USER FROM TPTEXGRATIACHARGES where EGC_BSU_ID='" & txtBatchNo.Text.Substring(7, strlen - 7) & "' ORDER BY EGC_ID  DESC", "OASIS_TRANSPORTConnectionString")
            params("reportCaption") = "Transport Extra Trips"

            params("bsuName") = Mainclass.getDataValue("SELECT BSU_NAME FROM oasis..businessunit_m wiht(nolock) where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
            params("DateRange") = ""
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptExGratiaChargesApproved.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            '   Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()

        Dim ReportPath As String = WebConfigurationManager.AppSettings.Item("CrystalPath")

        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/" & ReportPath & "/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/" & ReportPath & "/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class


