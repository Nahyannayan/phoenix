﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj


Partial Class Transport_ExtraHiring_TPTApproverUserList
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200265") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("viewid") Is Nothing Then
                    ViewState("EntryId") = "0"
                Else
                    ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                End If


                BindApprovers(IIf(ViewState("EntryId") = 0, 0, ViewState("EntryId")))

                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyvalues(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                Else
                    SetDataMode("add")
                    ClearDetails()
                    setModifyvalues(0)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean

        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        txtUser.Enabled = EditAllowed
        'txtFromDate.Enabled = EditAllowed
        'txtTODate.Enabled = EditAllowed
        ddlApproverType.Enabled = EditAllowed
        ddlType.Enabled = EditAllowed
        chkStatus.Enabled = EditAllowed
        ImgFDate.Enabled = EditAllowed
        ImgTDate.Enabled = EditAllowed
        plChkBUnit1.Enabled = EditAllowed
        chkStatus.Enabled = EditAllowed
        chkBSUAll1.Enabled = EditAllowed
        imgGetUserList.Enabled = EditAllowed
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable

    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim dt As New DataTable, strSql As String = ""
                'strSql.Append("SELECT [TLC_ID],TLC_DESCR ,TLC_ACT_ID ,[TLC_ADDRESS],TLC_EMAIL FROM TPTLEASECUSTOMER WHERE TLC_ID=" & h_EntryId.Value)
                Dim SPParam(1) As SqlParameter

                SPParam(0) = Mainclass.CreateSqlParameter("@ID", h_EntryId.Value, SqlDbType.Int)


                strSql = "SELECT_ExgratiaApproverUserList"
                dt = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, strSql, SPParam).Tables(0)
                If dt.Rows.Count > 0 Then

                    txtUser.Text = dt.Rows(0)("UserName")
                    h_userName.Value = dt.Rows(0)("EAL_USER")
                    txtFromDate.Text = dt.Rows(0)("EAL_FROMDT")
                    txtTODate.Text = dt.Rows(0)("EAL_TODT")


                    h_EntryId.Value = dt.Rows(0)("EAL_ID")

                    If dt.Rows(0)("EAL_EAM_ID") <> 0 Then
                        ddlApproverType.SelectedValue = dt.Rows(0)("EAL_EAM_ID")
                        h_UtypeID.Value = dt.Rows(0)("EAL_EAM_ID")
                    Else
                        ddlApproverType.SelectedValue = 0
                        h_UtypeID.Value = 0
                    End If

                    If dt.Rows(0)("EAL_TYPE") <> "" Then
                        ddlType.SelectedValue = dt.Rows(0)("EAL_TYPE")
                        h_TYPE.Value = dt.Rows(0)("EAL_TYPE")
                    Else
                        ddlType.SelectedValue = ""
                        h_TYPE.Value = ""
                    End If



                    chkStatus.Checked = dt.Rows(0)("eal_deleted")
                    'Dim bsu() As String = dt.Rows(0)("EAL_BSU_ID").ToString.Split("||")

                    'Dim part As String
                    'For Each part In bsu
                    '    Dim Item As RepeaterItem

                    '    For Each Item In rptBSUNames1.Items
                    '        Dim chkBSU1 As CheckBox = CType(Item.FindControl("chkBSU1"), CheckBox)
                    '        Dim hBSU As HiddenField = CType(Item.FindControl("h_BSUid1"), HiddenField)

                    '        If part = hBSU.Value Then
                    '            chkBSU1.Checked = True
                    '        Else
                    '            chkBSU1.Checked = False
                    '        End If

                    '    Next

                    'Next


                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub ClearDetails()

        txtUser.Text = ""
        txtFromDate.Text = ""
        txtTODate.Text = ""
    End Sub
    Private Sub BindApprovers(ByVal MOdId As Integer)

        'fillDropdown(ddlApproverType, "SELECT Iss_Id,Iss_Descr FROM DocIssuer union all SELECT 0,'----------None---------' order BY Iss_Descr", "Iss_Descr", "Iss_Id", True)

        ddlApproverType.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select EAM_ID,EAM_DESCR from EXGRATIA_APPROVER_MASTER union all select 0, '--------Select One--------'  order by EAM_DESCR")
        ddlApproverType.DataTextField = "EAM_DESCR"
        ddlApproverType.DataValueField = "EAM_ID"
        ddlApproverType.DataBind()

        ddlType.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select distinct EAL_TYPE ID,EAL_TYPE DESCR from EXGRATIA_APPROVER_USER_LIST     order by DESCR")
        ddlType.DataTextField = "DESCR"
        ddlType.DataValueField = "ID"
        ddlType.DataBind()

        Dim pParms(2) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@ID", MOdId, SqlDbType.Int)




        rptBSUNames1.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetUnitsForExGratia_APPROVER", pParms)
        rptBSUNames1.DataBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim sqlStr As String = "update  TPTVEHICLEMAINTENANCE set TVM_DELETED=1 where TVM_ID=@TRNO"
                Dim pParms(1) As SqlParameter
                pParms(1) = Mainclass.CreateSqlParameter("@TRNO", h_EntryId.Value, SqlDbType.Int, True)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, sqlStr, pParms)
                stTrans.Commit()
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strMandatory As New StringBuilder, strError As New StringBuilder, addMode As Boolean
        'If txtAddress.Text.Trim.Length = 0 Then strMandatory.Append("Enter Address,")
        'If txtCustomer.Text.Trim.Length = 0 Then strMandatory.Append("Enter Customer Name,")

        If strMandatory.ToString.Length > 0 Or strError.ToString.Length > 0 Then
            lblError.Text = ""
            If strMandatory.ToString.Length > 0 Then
                lblError.Text = strMandatory.ToString.Substring(0, strMandatory.ToString.Length - 1) & " Mandatory"
            End If
            lblError.Text &= strError.ToString
            Exit Sub
        End If

        addMode = (h_EntryId.Value = 0)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(10) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@EAL_ID", h_EntryId.Value, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@EAL_EAM_ID", ddlApproverType.SelectedValue, SqlDbType.Int)
        pParms(3) = Mainclass.CreateSqlParameter("@EAL_USER", h_userName.Value, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@EAL_TYPE", ddlType.SelectedValue, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@EAL_FROMDT", txtFromDate.Text, SqlDbType.DateTime)
        pParms(6) = Mainclass.CreateSqlParameter("@EAL_TODT", txtTODate.Text, SqlDbType.DateTime)

        Dim rowNum As Integer, bsuIds As String = ""
        Dim Item As RepeaterItem
        For Each Item In rptBSUNames1.Items
            Dim chkBSU1 As CheckBox = CType(Item.FindControl("chkBSU1"), CheckBox)
            Dim h_BSUid1 As HiddenField = CType(Item.FindControl("h_BSUid1"), HiddenField)
            If chkBSU1.Checked Then bsuIds = bsuIds & h_BSUid1.Value & "||"
            rowNum = rowNum + 1
        Next


        pParms(7) = Mainclass.CreateSqlParameter("@EAL_BSU_ID", bsuIds, SqlDbType.VarChar)


        pParms(8) = Mainclass.CreateSqlParameter("@EAL_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)

        If chkStatus.Checked = True Then
            pParms(9) = Mainclass.CreateSqlParameter("@EAL_DELETED", 1, SqlDbType.Int)
        Else
            pParms(9) = Mainclass.CreateSqlParameter("@EAL_DELETED", 0, SqlDbType.Int)
        End If

        pParms(10) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
        pParms(10).Direction = ParameterDirection.Output




        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVEEXGRATIA_APPROVER_USER_LIST", pParms)

            Dim ErrorMSG As String = pParms(10).Value.ToString()
            If ErrorMSG <> "" Then
                'If RetVal = "-1" Then
                usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If
            stTrans.Commit()

            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
                usrMessageBar.ShowNotification("Could not process your request", UserControls_usrMessageBar.WarningType.Danger)
            End If
            usrMessageBar.ShowNotification("Data Saved Successfully", UserControls_usrMessageBar.WarningType.Success)


        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Text = ex.Message
            stTrans.Rollback()
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub chkBSUAll1_CheckedChanged(sender As Object, e As EventArgs) Handles chkBSUAll1.CheckedChanged
        Dim Item As RepeaterItem
        For Each Item In rptBSUNames1.Items
            Dim chkBSU2 As CheckBox = CType(Item.FindControl("chkBSU1"), CheckBox)
            chkBSU2.Checked = chkBSUAll1.Checked
        Next
    End Sub
End Class





