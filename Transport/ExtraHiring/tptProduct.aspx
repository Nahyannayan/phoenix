﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptProduct.aspx.vb" Inherits="Transport_tptProduct" Title="Product Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="Label1" Text="Product Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0">
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle"><asp:Label runat="server" id="rptHeader" Text="Product Master"></asp:Label></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Product Name</span></td>

                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtproductName" runat="server" ></asp:TextBox></td>
                                     <td align="left" width="20%"><span class="field-label">Default Hours</span></td>
                                 
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtProdHrs" runat="server" ></asp:TextBox></td>
                                </tr>
                              
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Default Distance (Km)</span></td>
                                 
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtProdKm" runat="server" ></asp:TextBox></td>
                                     <td align="left" width="20%"><span class="field-label">Rate</span></td>
                                    
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtRate" runat="server"></asp:TextBox></td>
                                </tr>
                            
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Flag</span></td>
                                
                                    <td align="left" style="width: 30%">
                                        <asp:RadioButton ID="rbGen" runat="server" Checked="True" GroupName="rbFlag"
                                            Text="General" CssClass="field-label" AutoPostBack="True" />

                                        <asp:RadioButton ID="rbRate" runat="server" GroupName="rbFlag" Text="Rate"
                                            CssClass="field-label" AutoPostBack="True" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="hGridDelete" Value="0" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
