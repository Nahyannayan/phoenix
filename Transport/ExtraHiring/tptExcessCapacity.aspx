<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptExcessCapacity.aspx.vb" Inherits="Transport_ExtraHiring_tptExcessCapacity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ MasterType VirtualPath="~/mainMasterPage.master" %>
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
    <%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

    <style type="text/css">
         .autocomplete_highlightedListItem_S1 {
            /*font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;*/
            background-color: #CEE3FF;
            color: #1B80B6;
            padding: 1px;
        }

       .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function calc(Act) {

            if (Act = 'A') {
                a = document.getElementById("<%=txtVehCapacity80.ClientID %>").value;
                b = document.getElementById("<%=txtUsers.ClientID %>").value;
                var Seats = Math.floor(a - b);
                document.getElementById("<%=txtVacantSeats.ClientID %>").value = Seats;

            } else {

                a = document.getElementById("<%=txtVacantSeats.ClientID %>").value;
                b = document.getElementById("<%=txtChargingRate.ClientID %>").value;
                var Amount = Math.floor(a * b);
                document.getElementById("<%=txtAmount.ClientID %>").value = Amount;

                var VATAmt = (eval(document.getElementById("<%=txtAmount.ClientID %>").value) * eval(document.getElementById("<%=h_VATPerc.ClientID %>").value)) / 100;
                document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
                document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txtAmount.ClientID %>").value)).toFixed(2);

            }

        }

        function calc1(Act) {

            a = document.getElementById("<%=txtVacantSeats.ClientID %>").value;
            b = document.getElementById("<%=txtChargingRate.ClientID %>").value;
            var Amount = Math.floor(a * b);
            document.getElementById("<%=txtAmount.ClientID %>").value = Amount;

            var VATAmt = (eval(document.getElementById("<%=txtAmount.ClientID %>").value) * eval(document.getElementById("<%=h_VATPerc.ClientID %>").value)) / 100;
            document.getElementById("<%=txtVATAmount.ClientID %>").value = VATAmt.toFixed(2);
            document.getElementById("<%=txtNetAmount.ClientID %>").value = (VATAmt + eval(document.getElementById("<%=txtAmount.ClientID %>").value)).toFixed(2);
        }



        function getDriversName() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;


            pMode = "TPTDRIVERSCHOOL";

            url = "../../common/PopupSelect.aspx?id=" + pMode;

            result = radopen(url, "pop_up");
          <%--  if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtdriver.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];--%>
        }


        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtdriver.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtDriverName.ClientID %>").value = NameandCode[2];
                __doPostBack('<%= txtDriver.ClientID%>', 'TextChanged');
            }
        }


        function getVehicleNo() {

            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;

            pMode = "TPTVEHICLE_EXCESSCAPACITY";
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            result = radopen(url, "pop_up2");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtRegno.ClientID %>").value = NameandCode[1];
             document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = NameandCode[0];
             document.getElementById("<%=txtVehType.ClientID %>").value = NameandCode[3];
             document.getElementById("<%=txtVehCapacity.ClientID %>").value = NameandCode[4];--%>

        }





        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtRegno.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtVehType.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=txtVehCapacity.ClientID %>").value = NameandCode[4];
                __doPostBack('<%= txtRegno.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Excess Capacity Charges
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">
                            <table align="center" width="100%">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="20" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Excess Capacity Charges</span></font>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td width="5%"><span class="field-label">Date </span></td>
                                                <td align="left" width="7%">
                                                    <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                                    <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                                        TargetControlID="txtDate" PopupButtonID="lnkDate">
                                                    </ajaxToolkit:CalendarExtender>
                                                </td>
                                                <td width="20%">
                                                    <asp:Button ID="btnFind" runat="server" CssClass="button m-0" Text="Load.." />
                                                </td>
                                                <td width="72%"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th>Customer Name
                                                </th>
                                                <th width="100">DAX Code
                                                </th>
                                                <th>Driver Id
                                                </th>
                                                <th>Driver Name
                                                </th>
                                                <th>Vehicle Reg.
                                                </th>
                                                <th>Vehicle Type
                                                </th>
                                                <th>Vehicle Capacity
                                                </th>
                                                <th>Route No.
                                                </th>
                                                <th>Vehicle Capacity Seating (80%)
                                                </th>
                                                <th>No.of Users
                                                </th>
                                                <th>Vacant Seats
                                                </th>
                                                <th>Charging Rate
                                                </th>
                                                <th>Amount
                                                </th>
                                                <th>VAT Code
                                                </th>
                                                <th>Emirate
                                                </th>

                                                <th>VAT Amount
                                                </th>

                                                <th>Net Amount
                                                </th>
                                                <th align="left">Remarks
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtTSRSNo" runat="server" Width="100px" Visible="false"></asp:TextBox>
                                                    <telerik:RadComboBox ID="RadCustomer" Width="200" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadDAXCodes" Width="225" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtDriver" runat="server" Width="120px" AutoPostBack="true" align="left"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ImageDriver" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getDriversName();return false;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDriverName" runat="server" Width="300px" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtRegno" runat="server" Width="120px" align="left" TabIndex="1"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ImageVehicle" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getVehicleNo();return false;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVehType" runat="server" Width="200px" align="left" TabIndex="2"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVehCapacity" Width="100px" Style="text-align: right;" runat="server" AutoPostBack="true" TabIndex="3"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRoute" runat="server" Width="100px" TabIndex="4"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVehCapacity80" runat="server" Style="text-align: right;" Enabled="false" Width="100px" TabIndex="5"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUsers" runat="server" Style="text-align: right;" Width="100px" TabIndex="6"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVacantSeats" runat="server" Enabled="false" Width="100px" TabIndex="7"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtChargingRate" Style="text-align: right;" runat="server" Width="100px" TabIndex="8"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right;" Width="100px" Enabled="false" TabIndex="9"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <telerik:RadComboBox ID="radVAT" Width="255" runat="server" AutoPostBack="true" RenderMode="Lightweight"></telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <telerik:RadComboBox ID="RadEmirate" Width="125" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVATAmount" Width="100px" Style="text-align: right;" Enabled="false" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNetAmount" Width="100px" Style="text-align: right;" Enabled="false" runat="server" TabIndex="12"></asp:TextBox>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TxtRemarks" runat="server" Width="200px" TabIndex="13"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td colspan="3" align="left" >
                                        <asp:Button ID="btnCopy1" runat="server" CssClass="button m-0" Text="Copy" />
                                        <asp:Button ID="btnPost1" runat="server" CssClass="button m-0" Text="Post" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button m-0" Text="Save" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button m-0"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                        <asp:HyperLink ID="btnExport" runat="server">Export to Excel</asp:HyperLink>
                                        </td>
                                </tr>
                                
                                <tr>
                                    
                                    <td width="12%">
                                        
                                        <asp:RadioButtonList ID="rblSelection" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" >
                                            <asp:ListItem Selected="True" Value="0"><span class="field-label"> Not Posted </span></asp:ListItem>
                                            <asp:ListItem Value="1"><span class="field-label">Posted </span></asp:ListItem>
                                            <asp:ListItem Value="2"><span class="field-label">All </span></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td width="10%">
                                        <telerik:RadComboBox ID="RadFilterBSU" Width="600"  AllowCustomText="true" Filter="Contains" runat="server" AutoPostBack="true" RenderMode ="Lightweight">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td width="5%">
                                        <asp:Label ID="Postingdate" Text="Posting Date" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="7%">
                                        <asp:TextBox ID="txtPostingDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgPosting" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtPostingDate" PopupButtonID="ImgPosting">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>

                                    <td width="6%">
                                        <asp:Label ID="lblSelAmount" Text="Selected Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:TextBox ID="txtSelectedAmt" runat="server" Style="text-align: right;"></asp:TextBox>
                                    </td>

                                    <td width="5%">
                                        <asp:Label ID="Label1" Text="VAT Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:TextBox ID="txtSelVat" runat="server" Style="text-align: right;"></asp:TextBox>
                                    </td>
                                    <td width="5%">
                                        <asp:Label ID="Label2" Text="Net Amount" runat="server" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:TextBox ID="txtSelNet" runat="server" Style="text-align: right;"></asp:TextBox>
                                    </td>
                                    <td></td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%--<tr>
            <td  style="height: 19px" valign="bottom" colspan="19">                
            </td>
        </tr>--%>
                    <tr>
                        <td colspan="3">
                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row"
                                Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="TEC_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>

                                            <asp:CheckBox ID="chkAL" AutoPostBack="true" runat="server" OnCheckedChanged="chkAL_CheckedChanged"></asp:CheckBox>
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkControl" AutoPostBack="true" runat="server"></asp:CheckBox>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("[TEC_ID]") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TEC_TRDATE" ItemStyle-Width="80" HeaderText="Date">
                                        <ItemStyle Width="80px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="customer" HeaderText="Customer" />
                                    <asp:BoundField DataField="DAX_DESCR" HeaderText="DAX CODE" />
                                    <asp:BoundField DataField="DRIVER_EMPID" HeaderText="Driver" />
                                    <asp:BoundField DataField="DRIVER" HeaderText="Driver Name" />
                                    <asp:BoundField DataField="VEH_REGNO" HeaderText="Vehicle Reg." />
                                    <asp:BoundField DataField="VEH_NAME" HeaderText="Vehicle Type" />
                                    <asp:BoundField DataField="VEH_CAPACITY" HeaderText="Vehicle Capacity" />
                                    <asp:BoundField DataField="TEC_ROUTE" HeaderText="Route No." />
                                    <asp:BoundField DataField="TEC_CAPACITY80" HeaderText="Vehicle Capacity Seating (80%)" />
                                    <asp:BoundField DataField="TEC_NOOFUSERS" HeaderText="No.of Users" />
                                    <asp:BoundField DataField="TEC_VACANTSEATS" HeaderText="Vacant Seats" />
                                    <asp:BoundField DataField="TEC_RATE" HeaderText="Charging Rate" />
                                    <%--<asp:BoundField DataField="TEC_AMOUNT" HeaderText="Amount" />--%>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Eval("TEC_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="TEC_TAX_CODE" HeaderText="VAT Code" />
                                    <asp:BoundField DataField="EMIRATE" HeaderText="Emirate" />
                                    <%--<asp:BoundField DataField="TEC_TAX_AMOUNT" HeaderText="VAT Amount" />
                        <asp:BoundField DataField="THC_TAX_NET_AMOUNT" HeaderText="Net Amount" />--%>

                                    <asp:TemplateField HeaderText="VAT Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVATAmt" runat="server" Text='<%# Eval("TEC_TAX_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetAfterVAT" runat="server" Text='<%# Eval("TEC_TAX_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TEC_INVNO" HeaderText="Invoice" />
                                    <asp:BoundField DataField="TEC_REMARKS" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TEC_USER" HeaderText="User Name" />
                                    <asp:BoundField DataField="TEC_JHD_DOCNO" HeaderText="Posted?" />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTECID" runat="server" Text='<%# Eval("[TEC_ID]") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPOstingDate" runat="server" Text='<%# Eval("TEC_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPrint" runat="server" Text="Print" OnClick="lnkPrint_CLick"></asp:LinkButton>
                                            <asp:Label ID="lblTEC_invNo" runat="server" Text='<%# Eval("[TEC_INVNO]") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPDate" runat="server" Text='<%# Eval("TEC_POSTING_DATE") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                       <td colspan="3">
                           <table WIDTH="100%">
                               <tr>
                                    <td width="40%"></td>
                        <td align="center" width="20%">
                            <asp:Button ID="btnCopy" runat="server" CssClass="button m-0" Text="Copy" />
                            <asp:Button ID="btnPost" runat="server" CssClass="button m-0" Text="Post" />
                        </td>
                        <td width="40%"></td>
                               </tr>
                           </table>
                       </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="3">
                            <asp:HiddenField ID="h_BSUId" runat="server" />
                            <asp:HiddenField ID="h_bsuName" runat="server" />
                            <asp:HiddenField ID="h_Rate" runat="server" />
                            <asp:HiddenField ID="h_mimimum" runat="server" />
                            <asp:HiddenField ID="h_TEC_ID" runat="server" Value="0" />
                            <asp:HiddenField ID="h_VehicleType" runat="server" />
                            <asp:HiddenField ID="hTPT_Veh_ID" runat="server" />
                            <asp:HiddenField ID="h_VATPerc" runat="server" />
                            <asp:HiddenField ID="hdngross" runat="server" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

