﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptCreditNote.aspx.vb" Inherits="Transport_ExtraHiring_tptCreditNote" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" language="javascript">

        function onChange() { alert('ok'); }


        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function getProduct(InvNo, InvAmount, h_inv, h_invAmt, h_docno) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "TPTINVOICESFORCREDITNOTE"
            var InvTypeSelected = document.getElementById("<%=h_InvType.ClientID %>").value;
            var Client_id = document.getElementById("<%=h_Client_ID.ClientID %>").value
            var RB2 = document.getElementById("ctl00_cphMasterpage_rblCustomerType");
            var radio = RB2.getElementsByTagName("input");
            var label = RB2.getElementsByTagName("label");
            for (var x = 0; x < radio.length; x++) {
                if (radio[x].checked) {
                    var CNTypeSelected = label[x].innerHTML;
                }
            }
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&CNType=" + CNTypeSelected + "&InvType=" + InvTypeSelected + "&CLIENTID=" + Client_id;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById(InvNo).value = NameandCode[1];
            document.getElementById(InvAmount).value = NameandCode[2];

            document.getElementById(h_inv).value = NameandCode[1];
            document.getElementById(h_invAmt).value = NameandCode[2];
            document.getElementById(h_docno).value = NameandCode[3];


            document.getElementById(InvNo).disabled = true;
            document.getElementById(InvAmount).disabled = true;


        }

        function getGLList() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            pMode = "TPTGLLISTFORCREDITNOTE";

            url = "../../common/PopupSelect.aspx?id=" + pMode;

            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById("<%=txtGLCode.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtGLName.ClientID %>").value = NameandCode[2];
        }

        function getCustomers() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            pMode = "NESCUSTOMERFORCREDITNOTE";

            var RB1 = document.getElementById("<%=rblCustomerType.ClientID%>");
    var radio = RB1.getElementsByTagName("input");
    var label = RB1.getElementsByTagName("label");
    for (var x = 0; x < radio.length; x++) {
        if (radio[x].checked) {
            var CNTypeSelected = label[x].innerHTML;
        }
    }
    url = "../../common/PopupSelect.aspx?id=" + pMode + "&CNType=" + CNTypeSelected;
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('___');
    document.getElementById("<%=txtCustomer.ClientID %>").value = NameandCode[1];
    document.getElementById("<%=txtCustomerName.ClientID %>").value = NameandCode[2];
    document.getElementById("<%=h_Client_ID.ClientID %>").value = NameandCode[1];
}





    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Credit Note
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 90%; border-collapse: collapse;">
                    <tr valign="bottom">
                        <td align="center" style="height: 19px" valign="bottom" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 166px" valign="top">
                            <table align="left" cellpadding="5" cellspacing="0" style="width: 100%">
                                <%--<tr class="subheader_img">
                                    <td align="center" colspan="6" valign="middle">
                                        <div align="center">
                                            Credit Note
                                        </div>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblCreditNoteNo" runat="server" Text="Credit Note No" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCreditNoteNo" runat="server" Enabled="false" 
                                            ></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCNDate" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="lnkBatchDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtCNDate" PopupButtonID="lnkBatchDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label1" runat="server" Text="Type" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:RadioButtonList ID="rblCustomerType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="0">Customer</asp:ListItem>
                                            <asp:ListItem Value="1">Business Unit</asp:ListItem>
                                        </asp:RadioButtonList>

                                    </td>
                                </tr>
                                <tr>

                                    <td align="left" width="20%">
                                        <asp:Label ID="Label5" runat="server" Text="Customer" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCustomer" runat="server" Enabled="false" 
                                            ></asp:TextBox>
                                        <asp:TextBox ID="txtCustomerName" runat="server" Enabled="false"
                                            ></asp:TextBox>
                                        <asp:ImageButton ID="ImageCustomer" runat="server"
                                            ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getCustomers();return true;" />

                                        <asp:HiddenField ID="h_Client_ID" runat="server" />
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label2" runat="server" Text="C.N Type"> </asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:RadioButtonList ID="rbCNType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="0">Open</asp:ListItem>
                                            <asp:ListItem Value="1">Invoice</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>

                                <tr>

                                    <td style="width: 20%; "><span class="field-label">Remarks</span>
                                    </td>
                                    <td align="left" width=30%"  >
                                        <asp:TextBox ID="txtremarks" runat="server"  TextMode="MultiLine" ></asp:TextBox>
                                    </td>

                                    <td align="left" width="20%">
                                        <asp:Label ID="Label3" runat="server" Text="Amount" CssClass="field-label"> </asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAmount" Text="0.00" runat="server" 
                                            ></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>

                                    <td align="left" width="20%">
                                        <asp:Label ID="Label4" runat="server" Text="GL Code" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtGLCode" runat="server" Enabled="false" 
                                            ></asp:TextBox>
                                        <asp:TextBox ID="txtGLName" runat="server" Enabled="false" 
                                            ></asp:TextBox>
                                        <asp:ImageButton ID="imgGLCode" runat="server"
                                            ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getGLList();return true;" />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Label ID="Label6" runat="server" Text="Invoices" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td valign="middle" align="left" style="width: 30%">
                                        <asp:DropDownList ID="ddlInvoices" AutoPostBack="True" runat="server"></asp:DropDownList>
                                    </td>

                                </tr>
                                <tr class="title-bg">
                                    <td valign="middle" align="left" colspan="4">Invoice Details
                                    </td>
                                </tr>
                                <tr id="TRDataEntryGrid" runat="server">
                                    <td colspan="4">
                                        <asp:GridView ID="grdInvoice" runat="server" AutoGenerateColumns="False" Width="100%"
                                            ShowFooter="true" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="15"
                                            CssClass="table table-bordered table-row" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNCD_ID" runat="server" Text='<%# Bind("NCD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNCD_NCN_ID" runat="server" Text='<%# Bind("NCD_NCN_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblNCD_NCN_ID" runat="server" Text='<%# Bind("NCD_NCN_ID") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblNCD_NCN_ID" runat="server" Text='<%# Bind("NCD_NCN_ID") %>'></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Invoice No" ItemStyle-Width="44%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInvoiceNo" Width="300px" Enabled="false" runat="server" Text='<%# Bind("NCD_INV_NO") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgInvoice_I" Visible="false" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />


                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInvoiceNo" Width="300px" runat="server" Text='<%# Bind("NCD_INV_NO") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgInvoice_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />


                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInvoiceNo" Width="300px" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="imgInvoice_F" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />
                                                        <asp:HiddenField ID="h_INV_NO" runat="server" />
                                                        <asp:HiddenField ID="h_Doc_No" runat="server" />

                                                    </FooterTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Invoice Amount" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInvoiceAmount" Width="150px" Enabled="false" runat="server" Text='<%# Bind("NCD_INV_AMOUNT") %>'></asp:TextBox>


                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInvoiceAmount" Width="150px" runat="server" Text='<%# Bind("NCD_INV_AMOUNT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInvoiceAmount" Width="150px" runat="server" Text='<%# Bind("NCD_INV_AMOUNT") %>'></asp:TextBox>
                                                        <asp:HiddenField ID="h_INV_AMOUNT" runat="server" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>






                                                <asp:TemplateField HeaderText="Amount" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right"
                                                    FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" Width="100px" runat="server" Enabled="false" Style="text-align: right"
                                                            Text='<%# Eval("NCD_AMOUNT","{0:0.00}") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" Width="100px" runat="server" Style="text-align: right"
                                                            Text='<%# Eval("NCD_AMOUNT","{0:0.00}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" Width="100px" runat="server" Style="text-align: right"
                                                            Text='<%# Eval("NCD_AMOUNT","{0:0.00}") %>'></asp:TextBox>


                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-width="20%">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateBO" runat="server" CausesValidation="True" CommandName="Update"
                                                            Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelBO" runat="server" CausesValidation="False" CommandName="Cancel"
                                                            Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddBo" runat="server" CausesValidation="False" CommandName="AddNew"
                                                            Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditBO" runat="server" CausesValidation="False" CommandName="Edit"
                                                            Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-width="20%" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr id="TrDetailsGrid" runat="server">
                                    <td colspan="4">
                                        <asp:GridView ID="DisplayData" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                            ShowFooter="true" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="15"
                                            DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Invoice Number" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblINVNO" runat="server" Text='<%# Bind("NCD_INV_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Invoice Amount" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblINVAMOUNT" runat="server" Text='<%# Bind("NCD_INV_AMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Posted Amount" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount"  runat="server" Text='<%# Eval("NCD_AMOUNT","{0:0.00}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Document Number" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDOCNO"  runat="server" Text='<%# Bind("NCD_CN_DOC_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSAVE" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Save" Visible="False" />
                                        <asp:Button ID="btnPost" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Post" />
                                        <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Print" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                                        <asp:HiddenField ID="hGridDelete" Value="" runat="server" />
                                        <asp:HiddenField ID="h_InvType" Value="" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
