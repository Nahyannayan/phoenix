﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptSDETProjectMaster.aspx.vb" Inherits="Transport_ExtraHiring_tptSDETProjectMaster" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }

        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }


        function confirm_Close() {

            if (confirm("You are about to Close this Project.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }


        function getStaff(Staff, EmpId) {
           
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            
            pMode = "TPTDRIVERSCHOOLSDT"             

            url = "../../common/PopupSelect.aspx?id=" + pMode;
            document.getElementById('<%=hf_txtStaff.ClientID%>').value = Staff;
            document.getElementById('<%=hf_h_empid.ClientID%>').value = EmpId;
            result = radopen(url, "pop_up");
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //document.getElementById(Staff).value = NameandCode[2];
            //document.getElementById(EmpId).value = NameandCode[0];




        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



        function OnClientClose(oWnd, args) {
            var staff;
            staff = document.getElementById('<%=hf_txtStaff.ClientID%>').value;
            var h_empid;
            h_empid = document.getElementById('<%=hf_h_empid.ClientID%>').value;

            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                document.getElementById(staff).value = NameandCode[2];
                document.getElementById(h_empid).value = NameandCode[0];
                __doPostBack(staff, 'TextChanged');
            }
        }

    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Project Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center"  width="100%"  cellpadding="5" cellspacing="0">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle"><asp:Label runat="server" id="rptHeader" Text="Project Master"></asp:Label></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Project Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtproject" runat="server" ></asp:TextBox></td>

                                    <td align="left" width="20%"><span class="field-label">Project Amount</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAmount" runat="server" ></asp:TextBox></td>


                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Project Details</span></td>


                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtProjoectDetails"  TextMode="MultiLine" SkinID="MultiText"
                                            runat="server" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td align="left" width="20%">
                                        <asp:Label ID="lblCnd_ID" runat="server" Text="Fromdate" CssClass="field-label"></asp:Label>
                                    </td>


                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromdate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtFromdate" PopupButtonID="imgFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="Label2" runat="server" Text="Todate" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTodate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgTodate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" ></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtTodate" PopupButtonID="imgTodate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>


                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td valign="middle">
                            <table align="center"  width="100%"  cellpadding="5" cellspacing="0">
                                <tr class="title-bg">
                                    <td align="center" colspan="4" valign="middle">
                                        <asp:Label runat="server" ID="Label3" Text="Staff Details"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="grdStaff" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true" FooterStyle-HorizontalAlign="Left" CssClass="table table-bordered table-row"
                                CaptionAlign="Top" PageSize="15" SkinID="GridViewView"  DataKeyNames="ID">
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSPS_ID" runat="server" Text='<%# Bind("SPS_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSPS_SP_ID" runat="server" Text='<%# Bind("SPS_SP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>




                                    <asp:TemplateField HeaderText="Staff Name" ItemStyle-Width="44%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtStaff" Enabled="false" runat="server" Text='<%# Bind("STAFFNAME") %>'></asp:TextBox>
                                            <asp:ImageButton ID="imgSubLedger_e" Visible="false" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                TabIndex="14" />
                                            <asp:HiddenField ID="h_emp_id" runat="server" Value='<%# Bind("SPS_EMP_ID") %>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtStaff" runat="server" Text='<%# Bind("STAFFNAME") %>'></asp:TextBox>
                                            <asp:ImageButton ID="imgSubLedger_e" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                TabIndex="14" />
                                            <asp:HiddenField ID="h_emp_id" runat="server" Value='<%# Bind("SPS_EMP_ID") %>' />

                                        </EditItemTemplate>
                                        <FooterTemplate>

                                            <asp:TextBox ID="txtStaff" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="h_emp_id" runat="server" Value='<%# Bind("SPS_EMP_ID") %>' />
                                            <asp:ImageButton ID="imgSubLedger" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                TabIndex="14" />

                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Project Amount" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount"  Style="text-align: right" Enabled="false" runat="server" Text='<%# Eval("SPS_AMOUNT","{0:0.00}") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtAmount"  Style="text-align: right" runat="server" Text='<%# Eval("SPS_AMOUNT") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAmount"  Style="text-align: right" runat="server" Text='<%# Eval("SPS_AMOUNT") %>'></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdateRates" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancelRates" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:LinkButton ID="lnkAddRates" runat="server" CausesValidation="False" CommandName="AddNew" Text="Add New"></asp:LinkButton>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEditRates" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="27" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="28" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="29" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="30" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="31" />

                            <asp:Button ID="btnClose" runat="server" CausesValidation="False" Visible="false" CssClass="button"
                                Text="Close Project" OnClientClick="return confirm_Close();" TabIndex="31" />

                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="h_GridTotal" runat="server" Value="0" />
                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="hGridDelete" Value="0" runat="server" />
                            <asp:HiddenField ID="hf_txtStaff" runat="server" />
                            <asp:HiddenField ID="hf_h_empid" runat="server" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
