<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptFleet.aspx.vb" Inherits="tptFleet" Title="Fleet Management" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
         .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function calcKm() {
            document.getElementById('<%=lblTotalKm.ClientID %>').value = document.getElementById('<%=txtTPT_JobCl_km.ClientID %>').value - document.getElementById('<%=txtTPT_JobSt_km.ClientID %>').value;
        }

        function calcTime() {
        }

        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

        function confirm_FOC() {

            if (confirm("You are about to make this job as FOC.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function getProduct(Product, rateId, ProductID, qty) {
            
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            
            document.getElementById("<%=hf_product.ClientID%>").value = Product;
            document.getElementById("<%=hf_productid.ClientID%>").value = ProductID;
            document.getElementById("<%=hf_rateid.ClientID%>").value = rateId;
            document.getElementById("<%=hf_qty.ClientID%>").value = qty;         
            pMode = "TPTPRODUCT"
            var RB1 = document.getElementById("<%=rblJobType.ClientID%>");
            var radio = RB1.getElementsByTagName("input");
            var label = RB1.getElementsByTagName("label");
            var Client_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
            var VehTypeID = document.getElementById("<%=hdntrn_veh_catID.ClientID %>").value
            var Seasonid = document.getElementById("<%=hSeason.ClientID %>").value
            var RateTypeId = document.getElementById("<%=hRateType.ClientID %>").value
            for (var x = 0; x < radio.length; x++) {
                if (radio[x].checked) {
                    var jobTypeSelected = label[x].innerHTML;
                }
            }
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&jobtype=" + jobTypeSelected + "&CLIENTID=" + Client_id + "&VehTypeID=" + VehTypeID + "&Seasonid=" + Seasonid + "&ratetype=" + RateTypeId;

            result = radopen(url, "pop_up");
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //document.getElementById(Product).value = NameandCode[1];
            //document.getElementById(rateId).value = NameandCode[2];
            //document.getElementById(ProductID).value = NameandCode[0];
            //if (NameandCode[2] != "0.000")
            //    document.getElementById(rateId).disabled = true;
            //else
            //    document.getElementById(rateId).enabled = false;
            //document.getElementById(qty).value = 1;
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var Product = document.getElementById("<%=hf_product.ClientID%>").value;
            var ProductID = document.getElementById("<%=hf_productid.ClientID%>").value;
            var rateId = document.getElementById("<%=hf_rateid.ClientID%>").value;
            var qty = document.getElementById("<%=hf_qty.ClientID%>").value;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(Product).value = NameandCode[1];
                document.getElementById(rateId).value = NameandCode[2];
                document.getElementById(ProductID).value = NameandCode[0];
                if (NameandCode[2] != "0.000")
                    document.getElementById(rateId).disabled = true;
                else
                    document.getElementById(rateId).enabled = false;
                document.getElementById(qty).value = 1;
                __doPostBack(Product, 'TextChanged');
            }
        }

        function calculateTravel(rateId, qtyId, amtId) {
            if (document.getElementById(rateId).value == "") document.getElementById(rateId).value = 0;
            if (document.getElementById(qtyId).value == "") document.getElementById(qtyId).value = 0;
            var Rate = document.getElementById(rateId).value;
            var Qty = document.getElementById(qtyId).value;
            var total = eval(Rate) * eval(Qty);
            document.getElementById(amtId).value = total;

            var net = 0;
            for (cnt = 0; cnt < grdRate.length; cnt++)
                if (document.getElementById(grdRate[cnt])) {
                    var rate = document.getElementById(grdRate[cnt]).value;
                    var qty = document.getElementById(grdQty[cnt]).value;
                    var total = eval(rate) * eval(qty);
                    net = net + total;
                }
            document.getElementById('<%=txtBillingTotal.ClientID %>').value = net;

            return false;
        }


        function products(rateId, qtyId, amtId, e) {
            var products = document.getElementById("<%=h_Products.ClientID %>").value;
            var productValues = products.split(",");
            document.getElementById(rateId).value = productValues[e.selectedIndex + 1];
            calculateTravel(rateId, qtyId, amtId);
            return false;
        }

        function getLocation() {
           
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
           
            pMode = "TPTLOCATION"
            url = "../../common/PopupSelect.aspx?id=" + pMode;
            var location = document.getElementById("<%=hTPT_Loc_ID.ClientID %>").value;
           result = radopen(url, "pop_up2");
           <%--if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           document.getElementById("<%=txtLocation.ClientID %>").value = NameandCode[1];
           document.getElementById("<%=hTPT_Loc_ID.ClientID %>").value = NameandCode[0];--%>
       }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                document.getElementById("<%=txtLocation.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hTPT_Loc_ID.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtLocation.ClientID%>', 'TextChanged');
            }
        }

       function getVehicleCategory() {
           
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
          
           pMode = "TPTVEHICLETYPE"
           var Client_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
           url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id;
           result = radopen(url, "pop_up3");
           <%--if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           document.getElementById("<%=txtTRN_Veh_CatId.ClientID %>").value = NameandCode[1];
           document.getElementById("<%=hdnTRN_Veh_CatId.ClientID %>").value = NameandCode[0];--%>
       }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtTRN_Veh_CatId.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hdnTRN_Veh_CatId.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtTRN_Veh_CatId.ClientID%>', 'TextChanged');
            }
        }

       function getSalesMan() {
           
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
           
           pMode = "TPTSALESMANNEW"
           url = "../../common/PopupSelect.aspx?id=" + pMode;
           result = radopen(url, "pop_up4");
           <%--if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[1];
           document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = NameandCode[0];--%>
       }

        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtTRN_SalesMan.ClientID%>', 'TextChanged');
            }
        }

       function getVehicleNo() {
           
           var lstrVal;
           var lintScrVal;
           var pMode;
           var pDate;
           var S_Time;
           var E_Time;
           var NameandCode;
         

           var RB1 = document.getElementById("<%=rblData.ClientID%>");
           var radio = RB1.getElementsByTagName("input");
           var label = RB1.getElementsByTagName("label");
           for (var x = 0; x < radio.length; x++) {
               if (radio[x].checked) {
                   var jobTypeSelected = label[x].innerHTML;
               }
           }
           if (jobTypeSelected == 'School')
               pMode = "TPTVEHICLEFLEET";
           else
               pMode = "TPTVEHICLEFLEET";
           //               pMode = "TPTVEHICLEFLEETNEW";

           pDate = document.getElementById("<%=txtTPT_Date.ClientID %>").value
           S_Time = document.getElementById("<%=txtTPT_JobSt_Time.ClientID %>").value
           E_Time = document.getElementById("<%=txtTPT_JobCL_Time.ClientID %>").value

           url = "../../common/PopupSelect.aspx?id=" + pMode + "&jobDate=" + pDate + "&STime=" + S_Time + "&ETime=" + E_Time;
           result =radopen(url, "pop_up5");
           <%--if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           if (NameandCode[6] == 'Yes') {
               document.getElementById("<%=txtVehicleName.ClientID %>").value = '';
               document.getElementById("<%=txtCapacity.ClientID %>").value = 0;
               document.getElementById("<%=txtVehicleNo.ClientID %>").value = '';
               document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = 0;
               alert("This Vehicle is already allocated  to a job at this Time")
           }
           else {
               document.getElementById("<%=txtVehicleName.ClientID %>").value = NameandCode[3];
               document.getElementById("<%=txtCapacity.ClientID %>").value = NameandCode[4];
               document.getElementById("<%=txtVehicleNo.ClientID %>").value = NameandCode[1];
               document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = NameandCode[0];
           }--%>

       }

        function OnClientClose5(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (NameandCode[6] == 'Yes') {
                    document.getElementById("<%=txtVehicleName.ClientID %>").value = '';
                   document.getElementById("<%=txtCapacity.ClientID %>").value = 0;
                   document.getElementById("<%=txtVehicleNo.ClientID %>").value = '';
                   document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = 0;
                   alert("This Vehicle is already allocated  to a job at this Time")
               }
               else {
                   document.getElementById("<%=txtVehicleName.ClientID %>").value = NameandCode[3];
                   document.getElementById("<%=txtCapacity.ClientID %>").value = NameandCode[4];
                   document.getElementById("<%=txtVehicleNo.ClientID %>").value = NameandCode[1];
                   document.getElementById("<%=hTPT_Veh_ID.ClientID %>").value = NameandCode[0];
               }
                __doPostBack('<%= txtVehicleName.ClientID%>', 'TextChanged');
            }
        }

       function getDriversName() {
         
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
           

           var RB1 = document.getElementById("<%=rblData.ClientID%>");
           var radio = RB1.getElementsByTagName("input");
           var label = RB1.getElementsByTagName("label");
           for (var x = 0; x < radio.length; x++) {
               if (radio[x].checked) {
                   var jobTypeSelected = label[x].innerHTML;
               }
           }
           if (jobTypeSelected == 'School')
               pMode = "TPTFLEETDRIVERSCHOOL";
           else
               pMode = "TPTFLEETDRIVERNEW"

           pDate = document.getElementById("<%=txtTPT_Date.ClientID %>").value
           url = "../../common/PopupSelect.aspx?id=" + pMode + "&jobDate=" + pDate + "&menu=123456";

           result = radopen(url, "pop_up6");
          <%-- if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           document.getElementById("<%=txtDriversName.ClientID %>").value = NameandCode[2];
           document.getElementById("<%=hTPT_Drv_ID.ClientID %>").value = NameandCode[0];--%>
       }

        function OnClientClose6(oWnd, args) {                      
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtDriversName.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=hTPT_Drv_ID.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtDriversName.ClientID%>', 'TextChanged');
            }
        }
       function getClientName() {
           
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
         
           pMode = "TPTCLIENT"
           var RB1 = document.getElementById("<%=rblJobType.ClientID%>");
           var radio = RB1.getElementsByTagName("input");
           var label = RB1.getElementsByTagName("label");
           for (var x = 0; x < radio.length; x++) {
               if (radio[x].checked) {
                   var jobTypeSelected = label[x].innerHTML;
               }
           }
           url = "../../common/PopupSelect.aspx?id=" + pMode + "&jobtype=" + jobTypeSelected;
           result = radopen(url, "pop_up7");
           <%--if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           CodeandCode = NameandCode[0].split(";");
           document.getElementById("<%=txtClientName.ClientID %>").value = NameandCode[2];
           document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = CodeandCode[0];
           document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
           document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[3];
           document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = CodeandCode[1];--%>
       }

        function OnClientClose7(oWnd, args) {
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                CodeandCode = NameandCode[0].split(";");
                document.getElementById("<%=txtClientName.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=hTPT_Client_ID.ClientID %>").value = CodeandCode[0];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                document.getElementById("<%=txtTRN_SalesMan.ClientID %>").value = NameandCode[3];
                document.getElementById("<%=hdnTRN_SalesMan.ClientID %>").value = CodeandCode[1];
                __doPostBack('<%= txtClientName.ClientID%>', 'TextChanged');
            }
        }

       function getSupplierName() {
           
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
           
           pMode = "TPTSUPPLIER"
           url = "../../common/PopupSelect.aspx?id=" + pMode;
           result = radopen(url, "pop_up8");
          <%-- if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           CodeandCode = NameandCode[0].split(";");
           document.getElementById("<%=txtSupplier.ClientID %>").value = NameandCode[2];
           // /*document.getElementById("<%=hdnSupplier.ClientID %>").value = CodeandCode[1];*/
           document.getElementById("<%=hdnSupplier.ClientID %>").value = CodeandCode[0];

           document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;--%>
       }

        function OnClientClose8(oWnd, args) {                      
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                CodeandCode = NameandCode[0].split(";");
                document.getElementById("<%=txtSupplier.ClientID %>").value = NameandCode[2];
                // /*document.getElementById("<%=hdnSupplier.ClientID %>").value = CodeandCode[1];*/
                document.getElementById("<%=hdnSupplier.ClientID %>").value = CodeandCode[0];
                document.getElementById("<%=hGridRefresh.ClientID %>").value = 1;
                __doPostBack('<%= txtSupplier.ClientID%>', 'TextChanged');
            }
        }

       function getService() {
          
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
           
           pMode = "TPTSERVICENEW"
           url = "../../common/PopupSelect.aspx?id=" + pMode;
           result = radopen(url, "pop_up9");
          <%-- if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           document.getElementById("<%=txtService.ClientID %>").value = NameandCode[1];
           document.getElementById("<%=hTPT_Service_ID.ClientID %>").value = NameandCode[0];--%>
       }

        function OnClientClose9(oWnd, args) {
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtService.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hTPT_Service_ID.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtService.ClientID%>', 'TextChanged');
            }
        }
       function getRateType() {
          
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
           
           pMode = "TPTRATETYPE"

           var Client_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
           //url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id;

           var Jdate = document.getElementById("<%=txtTPT_Date.ClientID%>").value
           url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id + "&JobDate=" + Jdate;

           result = radopen(url, "pop_up10");
           <%--if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           document.getElementById("<%=txtRateType.ClientID %>").value = NameandCode[1];
           document.getElementById("<%=hRateType.ClientID %>").value = NameandCode[0];--%>
       }

        function OnClientClose10(oWnd, args) {
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtRateType.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hRateType.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtRateType.ClientID%>', 'TextChanged');
            }
        }

       function Numeric_Only() {
           if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
               if (event.keyCode == 13 || event.keyCode == 46)
               { return false; }
               else if (event.keyCode == 45)
               { return event.keyCode; }
               event.keyCode = 0
           }
       }

       function showDocument() {
           
           contenttype = document.getElementById('<%=hdnContentType.ClientID %>').value;
           filename = document.getElementById('<%=hdnFileName.ClientID %>').value;
           result = radopen("../../Inventory/IFrameNew.aspx?id=0&path=TRANSPORT&filename=" + filename + "&contenttype=" + contenttype, "pop_up11")
           return false;
       }


       function getSeason() {
           
           var lstrVal;
           var lintScrVal;
           var pMode;
           var NameandCode;
          
           pMode = "TPTSEASON"
           var Client_id = document.getElementById("<%=hTPT_Client_ID.ClientID %>").value
           url = "../../common/PopupSelect.aspx?id=" + pMode + "&ClinetID=" + Client_id;
           result = radopen(url, "pop_up12");
           <%--if (result == '' || result == undefined) {
               return false;
           }
           NameandCode = result.split('___');
           document.getElementById("<%=txtSeason.ClientID %>").value = NameandCode[1];
           document.getElementById("<%=hSeason.ClientID %>").value = NameandCode[0];--%>
       }

        function OnClientClose12(oWnd, args) {
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtSeason.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=hSeason.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtSeason.ClientID%>', 'TextChanged');
            }
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up6" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose6" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up7" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose7" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up8" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose8" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up9" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose9" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up10" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose10" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up11" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up12" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose12" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Hiring and Billing Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%">
                                <%-- <tr class="title-bg">
                  <td align="left" colspan="6" valign="middle">
                     <asp:Label runat="server" ID="rptHeader" Text="Hiring and Billing Details"></asp:Label>
                  </td>
               </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Job Order No</span>
                                    </td>
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtTrn_No" Enabled="false" runat="server" MaxLength="100"></asp:TextBox>
                                        <asp:Label ID="lblref" Enabled="false" runat="server" Text="" Visible="true"></asp:Label>
                                        <asp:RadioButtonList ID="rblData" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="0">NES</asp:ListItem>
                                            <asp:ListItem Value="1">School</asp:ListItem>
                                            <asp:ListItem Value="2">SubCon</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Job Type</span>
                                    </td>
                                    <td align="left" style="width: 30%">
                                        <asp:RadioButtonList ID="rblJobType" runat="server" Enabled="false" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="1">Customer</asp:ListItem>
                                            <asp:ListItem Value="0">Business Unit</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <asp:Panel runat="server" ID="pnlJobOrder" Visible="true">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Order/LPO No</span>
                                        </td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="txtLPO_No" runat="server" MaxLength="100"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Group</span>
                                        </td>
                                        <td align="left" style="width: 30%">
                                            <asp:TextBox ID="txtSTS_No" runat="server" MaxLength="15"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Enquiry Method</span>
                                        </td>

                                        <td align="left" style="width: 30%">
                                            <asp:RadioButton ID="rbCash" runat="server" Checked="True" GroupName="rbEnqMethod"
                                                Text="Cash" CssClass="radiobutton" AutoPostBack="True" />

                                            <asp:RadioButton ID="rbEmail" runat="server" GroupName="rbEnqMethod" Text="Email"
                                                CssClass="radiobutton" AutoPostBack="True" />

                                            <asp:RadioButton ID="rbCredit" runat="server" GroupName="rbEnqMethod" Text="Credit Card"
                                                CssClass="radiobutton" AutoPostBack="True" />
                                        </td>
                                        <td align="left" style="width: 20%"><span class="field-label">Supporting Docs</span>
                                        </td>

                                        <td align="left" style="width: 30%">
                                            <asp:LinkButton ID="btnDocumentLink" Text="view" Visible='false' runat="server" OnClientClick="showDocument();return true;"></asp:LinkButton>
                                            <asp:LinkButton ID="btnDocumentDelete" Text='x' Visible='false' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');">
                                            </asp:LinkButton>
                                            <asp:FileUpload ID="UploadDocPhoto" runat="server" Visible="false" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                            <asp:HiddenField ID="hdnFileName" runat="server" />
                                            <asp:HiddenField ID="hdnContentType" runat="server" />
                                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Visible="false" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Job Date</span><span style="color: red">*</span>
                                    </td>
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtTPT_Date" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgTPT_Date" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Style="cursor: hand" Width="16px"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                            TargetControlID="txtTPT_Date" PopupButtonID="imgTPT_Date">
                                        </ajaxToolkit:CalendarExtender>

                                        <asp:Label ID="lblTrDate" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Location</span><span style="color: red">*</span>
                                    </td>
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtLocation" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgLocation" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getLocation();return false;" />
                                        <asp:HiddenField ID="hTPT_Loc_ID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Client's Name</span><span style="color: red">*</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtClientName" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgClient" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getClientName();return false;" />
                                        <asp:HiddenField ID="hTPT_Client_ID" runat="server" />
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblSalesMan" runat="server" Text='Sales Man' CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTRN_SalesMan" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgTRN_SalesMan" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getSalesMan();return false;" />
                                        <asp:HiddenField ID="hdnTRN_SalesMan" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Vehicle Type</span><span style="color: red">*</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTRN_Veh_CatId" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgVehType" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleCategory();return false;" />
                                        <asp:HiddenField ID="hdnTRN_Veh_CatId" runat="server" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Service</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtService" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgService" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getService();return false;" />
                                        <asp:HiddenField ID="hTPT_Service_ID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Season</span><span style="color: red">*</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSeason" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImgSeason" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getSeason();return false;" />
                                        <asp:HiddenField ID="hSeason" runat="server" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Rate Type</span>
                                    </td>
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtRateType" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageRateType" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getRateType();return false;" />
                                        <asp:HiddenField ID="hRateType" runat="server" />
                                    </td>
                                </tr>
                                <tr class="title-bg">
                                    <td valign="middle" align="left" colspan="4">Job Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Job Description</span><span style="color: red">*</span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtTPT_Description" runat="server" TextMode="MultiLine"
                                            Width="92%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Pick up Point</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPickupPoint" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Drop off Point</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDropOffPoint" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" class="table table-bordered table-row mb-0">
                                            <tr>
                                                <th colspan="4" align="center">From S.T.S Depot
                                                </th>
                                            </tr>
                                            <tr>
                                                <td align="left" width="25%"><span style="font-weight: bold">Starting Time</span><span style="color: red">*</span><br />
                                                    <%--</td>

                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_STSSt_Time" runat="server"></asp:TextBox>
                                                    <ajaxToolkit:MaskedEditExtender ID="meeStartingTime" runat="server" AcceptAMPM="false"
                                                        MaskType="Time" Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                        OnInvalidCssClass="MaskedEditError" ErrorTooltipEnabled="true" UserTimeFormat="None"
                                                        TargetControlID="txtTPT_STSSt_Time" InputDirection="LeftToRight" AcceptNegative="Left">
                                                    </ajaxToolkit:MaskedEditExtender>
                                                    <ajaxToolkit:MaskedEditValidator ID="mevStartingTime" runat="server" ControlExtender="meeStartingTime"
                                                        ControlToValidate="txtTPT_STSSt_Time" IsValidEmpty="False" EmptyValueMessage="Time is required "
                                                        InvalidValueMessage="Time is invalid" Display="Dynamic" EmptyValueBlurredText="Time is required "
                                                        InvalidValueBlurredMessage="Invalid Time" ValidationGroup="MKE" />
                                                </td>
                                                <td align="left" width="25%"><span style="font-weight: bold">Arrival Time</span><span style="color: red">*</span><br />
                                                    <%--  </td>

                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_STSArr_Time" runat="server"></asp:TextBox>
                                                    <ajaxToolkit:MaskedEditExtender ID="meeArrivalTime" runat="server" AcceptAMPM="false"
                                                        MaskType="Time" Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                        OnInvalidCssClass="MaskedEditError" ErrorTooltipEnabled="true" UserTimeFormat="None"
                                                        TargetControlID="txtTPT_STSArr_Time" InputDirection="LeftToRight" AcceptNegative="Left">
                                                    </ajaxToolkit:MaskedEditExtender>
                                                    <ajaxToolkit:MaskedEditValidator ID="mevArrivalTime" runat="server" ControlExtender="meeArrivalTime"
                                                        ControlToValidate="txtTPT_STSArr_Time" IsValidEmpty="False" EmptyValueMessage="Time is required "
                                                        InvalidValueMessage="Time is invalid" Display="Dynamic" EmptyValueBlurredText="Time is required "
                                                        InvalidValueBlurredMessage="Invalid Time" ValidationGroup="MKE" />
                                                </td>
                                                <td align="left" width="25%"><span style="font-weight: bold">Starting Km</span><span style="color: red">*</span><br />
                                                    <%-- </td>

                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_STSSt_km" Style="text-align: right" runat="server"></asp:TextBox>
                                                </td>
                                                <td align="left" width="25%"><span style="font-weight: bold">Arrival Km</span><span style="color: red">*</span><br />
                                                    <%-- </td>

                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_STSArr_km" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table align="center" class="table table-bordered table-row mb-0"
                                            width="100%">

                                            <tr>
                                                <th colspan="4" align="center">On Job
                                                </th>
                                            </tr>
                                            <tr>
                                                <td align="left" width="25%"><span style="font-weight: bold">Starting Time</span><span style="color: red">*</span><br />
                                                    <%-- </td>
                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_JobSt_Time" runat="server"></asp:TextBox>
                                                    <ajaxToolkit:MaskedEditExtender ID="meeJobStartTime" runat="server" AcceptAMPM="false"
                                                        MaskType="Time" Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                        OnInvalidCssClass="MaskedEditError" ErrorTooltipEnabled="true" UserTimeFormat="None"
                                                        TargetControlID="txtTPT_JobSt_Time" InputDirection="LeftToRight" AcceptNegative="Left">
                                                    </ajaxToolkit:MaskedEditExtender>
                                                    <ajaxToolkit:MaskedEditValidator ID="mevJobStartTime" runat="server" ControlExtender="meeJobStartTime"
                                                        ControlToValidate="txtTPT_JobSt_Time" IsValidEmpty="False" EmptyValueMessage="Time is required "
                                                        InvalidValueMessage="Time is invalid" Display="Dynamic" EmptyValueBlurredText="Time is required "
                                                        InvalidValueBlurredMessage="Invalid Time" ValidationGroup="MKE" />
                                                </td>
                                                <td align="left" width="25%"><span style="font-weight: bold">Closing Time</span><span style="color: red">*</span><br />
                                                    <%--  </td>
                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_JOBCl_Time" runat="server"></asp:TextBox>
                                                    <ajaxToolkit:MaskedEditExtender ID="meeClosingTime" runat="server" AcceptAMPM="false"
                                                        MaskType="Time" Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                        OnInvalidCssClass="MaskedEditError" ErrorTooltipEnabled="true" UserTimeFormat="None"
                                                        TargetControlID="txtTPT_JOBCl_Time" InputDirection="LeftToRight" AcceptNegative="Left">
                                                    </ajaxToolkit:MaskedEditExtender>
                                                    <ajaxToolkit:MaskedEditValidator ID="mevClosingTime" runat="server" ControlExtender="meeClosingTime"
                                                        ControlToValidate="txtTPT_JOBCl_Time" IsValidEmpty="False" EmptyValueMessage="Time is required "
                                                        InvalidValueMessage="Time is invalid" Display="Dynamic" EmptyValueBlurredText="Time is required "
                                                        InvalidValueBlurredMessage="Invalid Time" ValidationGroup="MKE" />
                                                </td>
                                                <td align="left" width="25%"><span style="font-weight: bold">Starting Km</span><span style="color: red">*</span><br />
                                                    <%-- </td>
                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_JobSt_km" runat="server"></asp:TextBox>
                                                </td>
                                                <td align="left" width="25%"><span style="font-weight: bold">Closing Km</span><span style="color: red">*</span><br />
                                                    <%-- </td>
                                                <td align="left">--%>
                                                    <asp:TextBox ID="txtTPT_JOBCl_km" Style="text-align: right" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="title-bg">
                                    <td valign="middle" align="left" colspan="4">Vehicle Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Vehicle No</span><span style="color: red">*</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtVehicleNo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgVehicle" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getVehicleNo();return false;" />
                                        <asp:HiddenField ID="hTPT_Veh_ID" runat="server" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Driver's Name</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDriversName" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgDriver" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getDriversName();return false;" />
                                        <asp:HiddenField ID="hTPT_Drv_ID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Vehicle Name</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtVehicleName" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Capacity</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCapacity" runat="server" Enabled="false"></asp:TextBox>
                                        <table with="100%">
                                            <tr>
                                                <td><span style="font-weight: bold">Capacity Requested </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCapacityStr" Width="90%" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblQty" Style="font-weight: bold" runat="server" Text="Qty"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQty" Width="90%" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="title-bg">
                                    <td valign="middle" align="left" colspan="6">Invoice Details
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="grdInvoice" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                            ShowFooter="true" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="15"
                                            DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInv_ID" runat="server" Text='<%# Bind("Inv_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInv_TRN_No" runat="server" Text='<%# Bind("Inv_TRN_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInv_CN_ID" runat="server" Text='<%# Bind("Inv_CN_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Billing Item" ItemStyle-Width="44%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtProduct" Width="300px" Enabled="false" runat="server" Text='<%# Bind("Prod_Name") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgSubLedger_e" Visible="false" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />
                                                        <asp:HiddenField ID="h_prod_id_e" runat="server" Value='<%# Bind("Inv_Prod_id") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtProduct" Width="300px" runat="server" Text='<%# Bind("Prod_Name") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgSubLedger_e" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />
                                                        <asp:HiddenField ID="h_prod_id_e" runat="server" Value='<%# Bind("Inv_Prod_id") %>' />
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtProduct" Width="300px" runat="server"></asp:TextBox>
                                                        <asp:HiddenField ID="h_prod_id" runat="server" Value='<%# Bind("Inv_Prod_id") %>' />
                                                        <asp:ImageButton ID="imgSubLedger" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rate" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right"
                                                    FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInv_Rate" Width="100px" Enabled="false" Style="text-align: right"
                                                            runat="server" Text='<%# Eval("Inv_Rate","{0:0.00}") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInv_Rate" Width="100px" Enabled="false" runat="server" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Rate","{0:0.00}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInv_Rate" Width="100px" Enabled="true" runat="server" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Rate","{0:0.00}") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right"
                                                    FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInv_Qty" Width="70px" Enabled="false" runat="server" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Qty") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInv_Qty" Width="70px" runat="server" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Qty") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInv_Qty" Width="70px" runat="server" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Qty") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right"
                                                    FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" Width="100px" runat="server" Enabled="false" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Amt","{0:0.00}") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" Width="100px" runat="server" Enabled="false" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Amt","{0:0.00}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtInv_Amt" Width="100px" runat="server" Enabled="false" Style="text-align: right"
                                                            Text='<%# Eval("Inv_Amt","{0:0.00}") %>'></asp:TextBox>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateBO" runat="server" CausesValidation="True" CommandName="Update"
                                                            Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelBO" runat="server" CausesValidation="False" CommandName="Cancel"
                                                            Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddBo" runat="server" CausesValidation="False" CommandName="AddNew"
                                                            Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditBO" runat="server" CausesValidation="False" CommandName="Edit"
                                                            Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="OdoMeter" visible="false">                                    
                                    <td align="left" colspan="4">
                                          <span class="field-label">Odometer : &nbsp;&nbsp;
                                            <asp:Label runat="server" ID="lblTotalKm" Text="0.00" ></asp:Label> &nbsp;&nbsp; &nbsp;&nbsp;
                                        </span>
                                        <span class="field-label">Timing : &nbsp;&nbsp;
                                            <asp:Label runat="server" ID="lblTotalTime" Text="0.00" ></asp:Label>  &nbsp;&nbsp; &nbsp;&nbsp;</span>
                                        <span class="field-label">Total  : &nbsp;&nbsp;
                                            <asp:Label runat="server" ID="lblTotal" Text="0.00" ></asp:Label>  &nbsp;&nbsp; &nbsp;&nbsp;</span>
                                        <span class="field-label">Billing Total</span>
                                    </td>
                                </tr>
                                <tr>
                                   <td colspan="4">
                                       <table width="100%">
                                           <tr>
                                               <td><span class="field-label">Emirate</span>
                                                   <telerik:RadComboBox ID="RadEmirate" Width="245" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                   </telerik:RadComboBox>
                                               </td>

                                               <td><span class="field-label">VAT Code</span>
                                                   <telerik:RadComboBox ID="radVat" Width="345" runat="server" AutoPostBack="true" RenderMode="Lightweight">
                                                   </telerik:RadComboBox>
                                               </td>

                                               <td><span class="field-label">Billing Total</span>
                                                   <asp:TextBox ID="txtBillingTotal" Enabled="false" Style="text-align: right" Text="0.0"
                                                       runat="server"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <span class="field-label">VAT Amount</span></td>
                                               <td>
                                                   <asp:TextBox ID="txtVATAmount" Enabled="false" Style="text-align: right" Text="0.0"
                                                       runat="server"></asp:TextBox></td>
                                               <td><span class="field-label">Net Amount</span></td>
                                               <td>
                                                   <asp:TextBox ID="txtNetAmount" Enabled="false" Style="text-align: right" Text="0.0"
                                                       runat="server" Width="70px"></asp:TextBox>
                                               </td>
                                           </tr>
                                       </table>
                                   </td>
                                    
                                </tr>

                                <tr class="title-bg" id="trfoc" runat="server">
                                    <td valign="middle" align="left" colspan="6">FOC Details
                                    </td>
                                </tr>
                                <tr id="trfoc1" runat="server">
                                    <td align="left">Description<span style="color: red">*</span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtFOCDescr" runat="server" TextMode="MultiLine"
                                            Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" align="left" colspan="4">
                                        <asp:LinkButton ID="LnkSearch" runat="server" OnClientClick="javascript:return false;">Other Details</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Panel runat="server" ID="pnlInvoice">
                                            <table align="center" width="100%"
                                                cellpadding="5" cellspacing="0">
                                                <tr>
                                                    <td width="20%" align="left" class="title-bg" colspan="4">Sub Contractor Details
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="4">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="20%"><span class="field-label">Driver Name</span></td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtDriver" runat="server" MaxLength="100"></asp:TextBox></td>
                                                                <td width="20%"><span class="field-label">Vehicle Reg No</span></td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtVehRegNo" runat="server" MaxLength="100"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20%"><span class="field-label">Vendor Details</span></td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtSupplier" runat="server" MaxLength="100"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnSupplier" runat="server"></asp:HiddenField>
                                                                    <asp:ImageButton ID="imgSupplier" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClientClick="getSupplierName();return false;" />
                                                                </td>
                                                                <td width="20%"><span class="field-label">Vehicle Type</span>
                                                                </td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtVehType" runat="server" MaxLength="100"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20%"><span class="field-label">Vehicle Capacity</span>
                                                                </td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtVehCapacity" runat="server" MaxLength="100"></asp:TextBox>
                                                                </td>
                                                                <td width="20%"><span class="field-label">Staff Name</span>
                                                                </td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtStaff" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left" valign="middle" class="title-bg">Reimbursement Details
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="4">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="20%">
                                                                    <asp:Label ID="lblmeals" runat="server" Text="Meals" CssClass="field-label"></asp:Label>
                                                                </td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtTPT_Client_RefNo" runat="server" MaxLength="100" ></asp:TextBox></td>
                                                                <td width="20%">
                                                                    <asp:Label ID="lblPort" runat="server" Text="Port Passes" CssClass="field-label"></asp:Label>
                                                                </td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtTPT_Xtra_Hrs" runat="server" ></asp:TextBox>
                                                                </td>
                                                            </tr>                                                            
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Narration</span>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtTPT_Remarks"  TextMode="MultiLine" 
                                                            runat="server" ></asp:TextBox>
                                                    </td>
                                                     <td align="left" width="20%"><span class="field-label">Additional Service</span>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:TextBox ID="txtTPT_Add_Service"  TextMode="MultiLine" 
                                                            runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Client Feedback</span>
                                                    </td>
                                                    <td align="left" width="30%"> 
                                                        <asp:TextBox ID="txtTPT_Feedback"  TextMode="MultiLine" 
                                                            runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="20%"><span class="field-label">Extra Hours</span>
                                                    </td>
                                                    <td align="left" width="30%"></td>
                                                </tr>
                                                                                           
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                            CollapseControlID="LnkSearch" CollapsedSize="0" CollapsedText="Show Other Details"
                                            ExpandControlID="LnkSearch" ExpandedText="Hide Other Details" TargetControlID="pnlInvoice"
                                            TextLabelID="LnkSearch" Collapsed="True">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Visible="false" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                            <asp:Button ID="btnPProforma" runat="server" CausesValidation="False" CssClass="button" Text="Print Proforma" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnFOC" runat="server" CausesValidation="False" CssClass="button"
                                Text="Change Status to FOC" OnClientClick="return confirm_FOC();" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                            <asp:HiddenField ID="h_TPT_BllId" runat="server" Value="" />
                            <asp:HiddenField ID="hGridRefresh" Value="0" runat="server" />
                            <asp:HiddenField ID="hGridDelete" Value="0" runat="server" />
                            <asp:HiddenField ID="h_Products" Value="0" runat="server" />
                            <asp:HiddenField ID="h_EnqMethod" runat="server" />
                            <asp:HiddenField ID="h_TrnStatus" runat="server" />
                            <asp:HiddenField ID="h_VATPerc" Value="0" runat="server" />
                            <asp:HiddenField ID="h_Total" Value="0" runat="server" />
                            <asp:HiddenField ID="h_EXMPTTOTAL" Value="0" runat="server" />
                            <asp:HiddenField ID="hf_product" runat="server" />
                            <asp:HiddenField ID="hf_productid" runat="server" />
                            <asp:HiddenField ID="hf_rateid" runat="server" />
                            <asp:HiddenField ID="hf_qty" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
