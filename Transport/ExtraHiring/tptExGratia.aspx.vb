﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.DateTime


Partial Class Transport_tptExGratia
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")


                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "T200215") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                clearScreen()
                txtTStart.Attributes.Add("onblur", " return calc('T');")
                txtTEnd.Attributes.Add("onblur", " return calc('T');")
                txtOutStation.Attributes.Add("onblur", " return NetAmount();")
                txtoOTAmt.Attributes.Add("onblur", " return NetAmount();")

                sqlStr = "SELECT EGC_ID, replace(convert(varchar(30), EGC_TRDATE, 106),' ','/') EGC_TRDATE,EGC_BSU_ID, EGC_EMP_ID, EGC_TSTART, EGC_TEND, EGC_TTOTAL,   "
                sqlStr &= "  EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE,EGC_DRIVERTYPE,case when EGC_DRIVERTYPE=1 then 'Bus' else case when EGC_DRIVERTYPE=2 then 'Car' else case when EGC_DRIVERTYPE=3 then 'Van' else 'Conductor' end end end  DriverType"
                sqlStr &= ",DRIVER,DRIVER_EMPNO,BSU_NAME,isnull(TMS_DESCR,'') TMS_DESCR,EGC_OUTSTATION,EGC_C_BSU_SHORTNAME,isnull(EGC_OOTAMOUNT,0)EGC_OOTAMOUNT,isnull(EGC_TOTAL,0)EGC_TOTAL  FROM TPTEXGRATIACHARGES  inner join VW_DRIVER  on driver_empid=EGC_EMP_ID "
                sqlStr &= "  left outer join oasis..BUSINESSUNIT_M on BSU_ID=EGC_BSU_ID  INNER JOIN OASIS..EMPLOYEE_M E ON E.EMP_ID=EGC_EMP_ID  "
                sqlStr &= "Left outer join tptmaster  on tms_id=EGC_PURPOSE"
                BindDriver()
                BindBsu()
                If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                BindPurpose()
                checkHolidaysRate()
                BindChargingBsu()

                refreshGrid()
                If Request.QueryString("VIEWID") <> "" Then
                    h_EGC_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_EGC_ID.Value)
                End If
                btnSave.Visible = True
            Else
                calcSelectedTotal()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub calcSelectedTotal()
        'Dim Gross As Decimal = 0
        'For Each gvr As GridViewRow In GridViewShowDetails.Rows
        '    Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
        '    Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
        '    If ChkBxItem.Checked = True Then
        '        Gross += Val(lblGross.Text)
        '    End If
        'Next
        'txtSelectedAmt.Text = Gross
    End Sub

    Protected Sub Check_All(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Gross As Decimal = 0
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
            If ChkBxItem.Checked = True Then
                Gross += Val(lblGross.Text)
            End If
        Next
        txtSelectedAmt.Text = Gross
    End Sub
    Private Sub refreshGrid()
        Dim ds As New DataSet
        Dim Posted As Integer = 0
        BindChargingBsu()
        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        ddlBSU.SelectedValue = Mainclass.getDataValue("select isnull(bsu_shortname,'') from oasis..businessunit_M where bsu_id='" & ddlBusinessUnit.SelectedValue & "'", "OASIS_TRANSPORTConnectionString")

        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 1, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGC_ID", 0, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", "01-Jan-1900", SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", "01-Jan-1900", SqlDbType.DateTime)
        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntries", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTEXGRATIACHARGES = ds.Tables(0)
            Session("myData") = TPTEXGRATIACHARGES
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try




    End Sub

    Private Sub refreshGrid1()
        'Dim ds As New DataSet
        'Dim Posted As Integer = 0
        'sqlWhere = " where  MONTH(EGC_TRDATE)= MONTH('" & txtDate.Text & "') AND EGC_DELETED=0 and EGC_BSU_ID='" & ddlBusinessUnit.SelectedValue & "' and EGC_FYEAR='" & Session("F_YEAR") & "'"
        'sqlWhere &= " and EGC_TRDATE>='" & txtFromDate.Text & "' "
        'sqlWhere &= " and EGC_TRDATE<='" & txtToDate.Text & "' order by EGC_TRDATE "

        'Try
        '    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere)
        '    GridViewShowDetails.DataSource = ds
        '    GridViewShowDetails.DataBind()
        '    TPTEXGRATIACHARGES = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, sqlStr & sqlWhere).Tables(0)
        '    Session("myData") = TPTEXGRATIACHARGES
        '    SetExportFileLink()

        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try
        Dim ds As New DataSet
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 1, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGC_ID", 0, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", txtFromDate.Text, SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", txtToDate.Text, SqlDbType.DateTime)
        Try
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntries", pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
            TPTEXGRATIACHARGES = ds.Tables(0)
            Session("myData") = TPTEXGRATIACHARGES
            SetExportFileLink()

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try


    End Sub
    Private Sub clearScreen()
        h_EGC_ID.Value = "0"
        h_Emp_id.Value = "0"
        txtOasis.Text = ""
        txtDriverName.Text = ""
        txtTTotal.Text = "0.0"
        txtTStart.Text = "00:00"
        txtTEnd.Text = "00:00"
        txtTTotal.Text = "0.0"
        txtoOTAmt.Text = "0.0"
        txtTotalAmount.Text = "0.0"
        txtNetAmount.Text = "0.0"
        txtOutStation.Text = "0.0"
        TxtRemarks.Text = ""
        chkOutStation.Checked = False
        txtDate.Text = Now.ToString("dd/MMM/yyyy")
    End Sub
    Private Property sqlStr() As String
        Get
            Return ViewState("sqlStr")
        End Get
        Set(ByVal value As String)
            ViewState("sqlStr") = value
        End Set
    End Property

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub
    Private Sub saveData()
        lblError.Text = ""
        Dim E_BSU_ID_SHORT As String = ""

        Dim PurposeExists As Integer = 1
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If Left(txtTStart.Text, 2) > 24 Then lblError.Text &= "Invalid Time in,"
        If Left(txtTEnd.Text, 2) > 24 Then lblError.Text &= "Invalid Time Out,"
        If Right(txtTStart.Text, 2) > 60 Then lblError.Text &= "Invalid Time in,"
        If Right(txtTEnd.Text, 2) > 60 Then lblError.Text &= "Invalid Time Out,"

        If Val(txtoOTAmt.Text) > 0 And LTrim(RTrim(TxtRemarks.Text)) = "" Then lblError.Text &= "Enter the Remarks..,"


        'If E_BSU_ID_SHORT <> LTrim(Trim(txtCBSU.Text)) Then lblError.Text &= "Invalid Charging Business Unit,"

        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            lblError.Visible = True
            Return
        End If
        checkHolidaysRate()
        Try
            Dim pParms(18) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@EGC_ID", h_EGC_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@EGC_TRDATE", txtDate.Text, SqlDbType.SmallDateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@EGC_BSU_ID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@EGC_EMP_ID", h_Emp_id.Value, SqlDbType.Int)
            pParms(5) = Mainclass.CreateSqlParameter("@EGC_DRIVERTYPE", rblDriverType.SelectedValue, SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@EGC_TSTART", txtTStart.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@EGC_TEND", txtTEnd.Text, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@EGC_TTOTAL", txtTTotal.Text, SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@EGC_TOTAL", txtTotalAmount.Text, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@EGC_NETAMOUNT", txtNetAmount.Text, SqlDbType.Decimal)
            pParms(11) = Mainclass.CreateSqlParameter("@EGC_REMARKS", TxtRemarks.Text, SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@EGC_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@EGC_FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@EGC_PURPOSE", RadPurpose.Text, SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@EGC_OUTSTATION", chkOutStation.Checked, SqlDbType.Bit)
            'pParms(15) = Mainclass.CreateSqlParameter("@EGC_C_BSU_SHORTNAME", LTrim(RTrim(txtCBSU.Text)), SqlDbType.VarChar)
            pParms(16) = Mainclass.CreateSqlParameter("@EGC_C_BSU_SHORTNAME", ddlBSU.SelectedItem.Text, SqlDbType.VarChar)
            pParms(17) = Mainclass.CreateSqlParameter("@EGC_OOTAMOUNT", txtoOTAmt.Text, SqlDbType.VarChar)
            pParms(18) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
            pParms(18).Direction = ParameterDirection.Output

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                'Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTEXGRATIACHARGES", pParms)
                'If RetVal = "-1" Then
                '    lblError.Text = "Unexpected Error !!!"
                '    stTrans.Rollback()
                '    Exit Sub
                'Else
                '    stTrans.Commit()
                '    clearScreen()
                '    refreshGrid()
                '    lblError.Text = "Data Saved Successfully !!!"
                'End If
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVETPTEXGRATIACHARGES", pParms)
                Dim ErrorMSG As String = pParms(18).Value.ToString()

                If ErrorMSG = "" Then
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_Emp_id.Value, "SAVE", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        usrMessageBar.ShowNotification("Could not process your request", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                    Else
                        stTrans.Commit()
                        clearScreen()
                        refreshGrid()
                        usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                    End If
                Else
                    usrMessageBar.ShowNotification(ErrorMSG, UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                End If
                Exit Sub
            Catch ex As Exception
                Errorlog(ex.Message)
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub
    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
        checkHolidaysRate()
    End Sub

    Private Sub showEdit(ByVal showId As Integer)


        Dim ds As New DataSet
        Dim pParms(8) As SqlParameter
        pParms(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        pParms(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@SBSU", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@DATE", txtDate.Text, SqlDbType.DateTime)
        pParms(5) = Mainclass.CreateSqlParameter("@Option", 2, SqlDbType.Int)
        pParms(6) = Mainclass.CreateSqlParameter("@EGC_ID", showId, SqlDbType.Int)
        pParms(7) = Mainclass.CreateSqlParameter("@FDATE", txtFromDate.Text, SqlDbType.DateTime)
        pParms(8) = Mainclass.CreateSqlParameter("@TDATE", txtToDate.Text, SqlDbType.DateTime)
        Try
            'ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntries", pParms)

            Dim rdrExGratiaCharges As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "LoadingExgratiaEntries", pParms)

            Dim d1 As Date
            Dim dayname1 As String = ""
            Dim Rate1 As Integer = 0
            Dim HolidayCheck As Integer = 0
            BindChargingBsu()
            If rdrExGratiaCharges.HasRows Then
                rdrExGratiaCharges.Read()
                h_EGC_ID.Value = rdrExGratiaCharges("EGC_ID")
                ddlBusinessUnit.SelectedValue = rdrExGratiaCharges("EGC_BSU_ID")
                ddlBusinessUnit.SelectedItem.Text = rdrExGratiaCharges("BSU_NAME")
                txtDate.Text = rdrExGratiaCharges("EGC_TRDATE")
                h_Emp_id.Value = rdrExGratiaCharges("EGC_EMP_ID")
                txtDriverName.Text = rdrExGratiaCharges("DRIVER")
                txtOasis.Text = rdrExGratiaCharges("DRIVER_EMPNO")
                rblDriverType.SelectedValue = rdrExGratiaCharges("EGC_DRIVERTYPE")
                txtTStart.Text = rdrExGratiaCharges("EGC_TSTART")
                txtTEnd.Text = rdrExGratiaCharges("EGC_TEND")
                txtTTotal.Text = rdrExGratiaCharges("EGC_TTOTAL")
                txtTotalAmount.Text = rdrExGratiaCharges("EGC_TOTAL")
                TxtRemarks.Text = rdrExGratiaCharges("EGC_REMARKS")
                RadPurpose.Text = rdrExGratiaCharges("TMS_DESCR")
                chkOutStation.Checked = rdrExGratiaCharges("EGC_OUTSTATION")
                If chkOutStation.Checked = True Then
                    txtOutStation.Text = 25
                Else
                    txtOutStation.Text = 0
                End If
                ddlBSU.SelectedItem.Text = rdrExGratiaCharges("EGC_C_BSU_SHORTNAME")
                ddlBSU.SelectedValue = rdrExGratiaCharges("EGC_C_BSU_SHORTNAME")
                txtoOTAmt.Text = rdrExGratiaCharges("EGC_OOTAMOUNT")
                d1 = txtDate.Text
                dayname1 = UCase(Format(d1, "dddd"))
                HolidayCheck = Mainclass.getDataValue("select count(*) from TPTEXGRATIAHOLIDAY_M where '" & txtDate.Text & "' between EGH_SDATE and EGH_EDATE", "OASIS_TRANSPORTConnectionString")

                If (dayname1 = "FRIDAY" Or dayname1 = "SATURDAY") Or HolidayCheck <> 0 Then
                    h_Holiday.Value = 1
                    If rblDriverType.SelectedValue = 1 Then
                        h_Rate.Value = Mainclass.getDataValue("select tet_bus_OT1_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_bus_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    ElseIf rblDriverType.SelectedValue = 2 Then
                        h_Rate.Value = Mainclass.getDataValue("select tet_car_OT1_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_car_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    ElseIf rblDriverType.SelectedValue = 3 Then
                        h_Rate.Value = Mainclass.getDataValue("select tet_van_OT1_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_van_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    ElseIf rblDriverType.SelectedValue = 4 Then
                        h_Rate.Value = Mainclass.getDataValue("select tet_conss_OT1_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_conss_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    End If
                Else
                    h_Holiday.Value = 0
                    If rblDriverType.SelectedValue = 1 Then
                        h_Rate.Value = Mainclass.getDataValue("select TET_BUS_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_bus_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    ElseIf rblDriverType.SelectedValue = 2 Then
                        h_Rate.Value = Mainclass.getDataValue("select TET_CAR_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_car_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    ElseIf rblDriverType.SelectedValue = 3 Then
                        h_Rate.Value = Mainclass.getDataValue("select TET_VAN_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_van_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    ElseIf rblDriverType.SelectedValue = 4 Then
                        h_Rate.Value = Mainclass.getDataValue("select TET_CONSS_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        Rate1 = Mainclass.getDataValue("select tet_conss_OT2_rate from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                        h_rate1.Value = Rate1
                    End If
                End If
            End If
            rdrExGratiaCharges.Close()
            rdrExGratiaCharges = Nothing
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try




    End Sub

    Protected Sub GridViewShowDetails_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowCreated

    End Sub
    'Dim Count As Integer = 0
    Protected Sub GridViewShowDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewShowDetails.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(16).Text <> "0" Then
                'Count += 1
                'e.Row.Cells(0).Enabled = False
                e.Row.Cells(1).Enabled = False
                e.Row.Cells(19).Enabled = False
                e.Row.Cells(20).Enabled = True
            End If
        End If
        'If Count > 0 Then

        '    Dim CheckBox1 As HtmlInputCheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), HtmlInputCheckBox)
        '    If Not CheckBox1 Is Nothing Then
        '        CheckBox1.Disabled = True
        '    End If
        'End If

        'For Each gvr As GridViewRow In GridViewShowDetails.Rows
        '    If gvr.Cells(16).Text = "0" Then
        '        gvr.Cells(0).Enabled = True
        '        gvr.Cells(1).Enabled = True
        '        gvr.Cells(19).Enabled = True
        '        gvr.Cells(20).Enabled = False
        '    Else
        '        gvr.Cells(0).Enabled = False
        '        gvr.Cells(1).Enabled = False
        '        gvr.Cells(19).Enabled = False
        '        gvr.Cells(20).Enabled = True
        '    End If
        'Next

    End Sub
    Protected Sub lnkDelete_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblEGCID"), Label)
        Dim TDate As Date
        TDate = Mainclass.getDataValue("select getdate()", "OASIS_TRANSPORTConnectionString")

        If TDate.Day >= 6 And TDate.Month - 1 >= CDate(txtDate.Text).Month Then
            lblError.Text = "This month is already closed,You can not delete now...."
            Exit Sub
        End If



        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lblID.Text, "Delete", Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
        SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update  TPTEXGRATIACHARGES set EGC_DELETED=1,EGC_USER='" & Session("sUsr_name") & "',EGC_DATE=GETDATE() where EGC_ID=" & lblID.Text)
        refreshGrid()
    End Sub
    Protected Sub lnkPrint_CLick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblTAC_INVNO"), Label)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIACHARGES"
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EGC_INVNO", lblID.Text, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))

            sqlParam(1) = Mainclass.CreateSqlParameter("@EGC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTEXGRATIACHARGES.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            ' Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid1()
    End Sub
    Private Sub BindDriver()
    End Sub
    Private Sub BindBsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim BSUParms(2) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@usr_nAME", Session("sUsr_name"), SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@PROVIDER_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            ddlBusinessUnit.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "gETUNITSFOREXTRATRIPS", BSUParms)
            ddlBusinessUnit.DataTextField = "BSU_NAME"
            ddlBusinessUnit.DataValueField = "SVB_BSU_ID"
            ddlBusinessUnit.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindChargingBsu()
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        'Dim str_Sql3 As String = "select 'STS' ID,'STS' DESCR  union all select 'BBT' id,'BBT' DESCR union all select BSU_SHORTNAME ID,BSU_SHORTNAME DESCR  from oasis..BUSINESSUNIT_M  where bsu_id='" & ddlBusinessUnit.SelectedValue & "'"
        'Dim str_Sql3 As String = ""
        'If ddlBusinessUnit.SelectedValue = "125015" And txtOasis.Text = "10025666" Then
        '    str_Sql3 = "select 'OOD' ID,'OOD' DESCR  union all select 'STS' ID,'STS' DESCR  union all select 'AKG' ID,'AKG' DESCR  union all select 'SAR' ID,'SAR' DESCR  union all select 'SDE' ID,'SDE' DESCR  union all select 'BBT' id,'BBT' DESCR union all select BSU_SHORTNAME ID,BSU_SHORTNAME DESCR  from oasis..BUSINESSUNIT_M  where bsu_id='" & ddlBusinessUnit.SelectedValue & "' AND BSU_ID NOT IN('900501','900500')"
        'Else
        '    str_Sql3 = "select 'STS' ID,'STS' DESCR  union all select 'AKG' ID,'AKG' DESCR  union all select 'SAR' ID,'SAR' DESCR  union all select 'SDE' ID,'SDE' DESCR  union all select 'BBT' id,'BBT' DESCR union all select BSU_SHORTNAME ID,BSU_SHORTNAME DESCR  from oasis..BUSINESSUNIT_M  where bsu_id='" & ddlBusinessUnit.SelectedValue & "' AND BSU_ID NOT IN('900501','900500')"
        'End If
        'ddlBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql3)
        'ddlBSU.DataTextField = "DESCR"
        'ddlBSU.DataValueField = "ID"
        'ddlBSU.DataBind()


        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim BSUParms(3) As SqlParameter
            BSUParms(1) = Mainclass.CreateSqlParameter("@BSUID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
            BSUParms(2) = Mainclass.CreateSqlParameter("@EMPNO", txtOasis.Text, SqlDbType.VarChar)
            BSUParms(3) = Mainclass.CreateSqlParameter("@LoginBsuID", Session("sBsuid"), SqlDbType.VarChar)
            ddlBSU.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "getUnitsForChargingBSU", BSUParms)
            ddlBSU.DataTextField = "DESCR"
            ddlBSU.DataValueField = "ID"
            ddlBSU.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try


    End Sub
    Private Sub checkHolidaysRate()
        Try
            Dim d As Date
            Dim dayname As String = ""
            Dim Holiday As Integer = 0
            Dim Othr1 As Double = "0.0"
            Dim othr2 As Double = "0.0"
            Dim Rate1 As Integer = 0
            Dim time1 As TimeSpan
            Dim time2 As TimeSpan

            Dim Ot1Time As String = ""
            Dim Ot2Time As String = ""
            d = txtDate.Text
            dayname = UCase(Format(d, "dddd"))
            Holiday = Mainclass.getDataValue("select count(*) from TPTEXGRATIAHOLIDAY_M where '" & txtDate.Text & "' between EGH_SDATE and EGH_EDATE", "OASIS_TRANSPORTConnectionString")

            Ot1Time = Mainclass.getDataValue("select TET_OT1_S_HOURS from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
            Ot2Time = Mainclass.getDataValue("select TET_OT2_S_HOURS from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
            h_OT1_Hrs.Value = Mainclass.getDataValue("select TET_OT1_S_HOURS from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
            h_OT2_Hrs.Value = Mainclass.getDataValue("select TET_OT2_S_HOURS from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
            h_DriverType.Value = rblDriverType.SelectedValue
            If (dayname = "FRIDAY" Or dayname = "SATURDAY") Or Holiday <> 0 Then
                h_Holiday.Value = 1
                If rblDriverType.SelectedValue = 1 Then
                    'h_Rate.Value = 16
                    h_Rate.Value = Mainclass.getDataValue("select TET_BUS_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    Rate1 = Mainclass.getDataValue("select TET_BUS_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                ElseIf rblDriverType.SelectedValue = 2 Then
                    'h_Rate.Value = 12
                    h_Rate.Value = Mainclass.getDataValue("select TET_CAR_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    Rate1 = Mainclass.getDataValue("select TET_CAR_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                ElseIf rblDriverType.SelectedValue = 3 Then
                    'h_Rate.Value = 12
                    h_Rate.Value = Mainclass.getDataValue("select TET_VAN_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    Rate1 = Mainclass.getDataValue("select TET_VAN_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                ElseIf rblDriverType.SelectedValue = 4 Then
                    'h_Rate.Value = 12
                    h_Rate.Value = Mainclass.getDataValue("select TET_CONSS_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    Rate1 = Mainclass.getDataValue("select TET_CONSS_HOLIDAY_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                End If
            Else
                h_Holiday.Value = 0
                If rblDriverType.SelectedValue = 1 Then
                    'Ot1
                    h_Rate.Value = Mainclass.getDataValue("select TET_BUS_OT1_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    'Ot2
                    Rate1 = Mainclass.getDataValue("select TET_BUS_OT2_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                ElseIf rblDriverType.SelectedValue = 2 Then
                    'Ot1
                    h_Rate.Value = Mainclass.getDataValue("select TET_CAR_OT1_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    'Ot2
                    Rate1 = Mainclass.getDataValue("select TET_CAR_OT2_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                ElseIf rblDriverType.SelectedValue = 3 Then
                    'Ot1
                    h_Rate.Value = Mainclass.getDataValue("select TET_VAN_OT1_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    'Ot2
                    Rate1 = Mainclass.getDataValue("select TET_VAN_OT2_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                ElseIf rblDriverType.SelectedValue = 4 Then
                    'Ot1
                    h_Rate.Value = Mainclass.getDataValue("select TET_CONSS_OT1_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    'Ot2
                    Rate1 = Mainclass.getDataValue("select TET_CONSS_OT2_RATE from TPTEXGRATIATIMINGS ", "OASIS_TRANSPORTConnectionString")
                    h_rate1.Value = Rate1
                End If
            End If


            'h_DriverType.Value = rblDriverType.SelectedValue
            'If rblDriverType.SelectedValue = 1 Then
            '    Rate1 = 16
            'Else
            '    Rate1 = 12
            'End If
            If ((txtTStart.Text <> "00:00") And (txtTEnd.Text <> "00:00")) Then
                Dim HRS As TimeSpan
                Dim HRS1 As TimeSpan
                Dim hour As String = ""
                Dim hour1 As String = ""
                Dim STime As String = ""
                Dim ETime As String = ""
                Dim STimehrs As String = ""
                Dim ETimehrs As String = ""
                Dim Totalhrs As Double = 0.0
                Dim Totalhrs1 As Double = 0.0
                Dim Amount As Double = 0.0
                Dim OTAmount As Double = 0.0
                Dim St As TimeSpan = TimeSpan.Parse(txtTStart.Text)
                Dim Cl As TimeSpan = TimeSpan.Parse(txtTEnd.Text)
                STime = Left(txtTStart.Text, 5)
                ETime = Left(txtTEnd.Text, 5)
                STimehrs = Left(STime, 2)
                ETimehrs = Left(ETime, 2)
                If h_Holiday.Value = 1 Then
                    HRS = (Cl - St)
                    If STimehrs > ETimehrs Then
                        HRS = HRS + TimeSpan.FromHours(24)
                    End If
                    hour = Left(HRS.ToString(), 5)

                    If Right(hour, 2) > 30 Then
                        Totalhrs = Val(Left(hour, 2)) + 1
                    ElseIf Right(hour, 2) > 0 Then
                        Totalhrs = Val(Left(hour, 2)) + 0.5
                    Else
                        Totalhrs = Val(Left(hour, 2))
                    End If
                    txtTTotal.Text = Totalhrs
                    Amount = Totalhrs * h_Rate.Value
                    txtTotalAmount.Text = Amount
                Else
                    'If STimehrs < 18 Then
                    If STimehrs < CInt(Left(Ot1Time, Ot1Time.Length - 3)) Then
                        'St = TimeSpan.Parse("18:00")
                        St = TimeSpan.Parse(Ot1Time)
                    End If

                    'If ETimehrs < 18 And ETimehrs > 12 Then
                    If ETimehrs < CInt(Left(Ot1Time, Ot1Time.Length - 3)) And ETimehrs > 12 Then

                        'Cl = TimeSpan.Parse("18:00")
                        Cl = TimeSpan.Parse(Ot1Time)
                    End If

                    'If STimehrs < 21 And ETimehrs >= 21 Then
                    If STimehrs < CInt(Left(Ot2Time, Ot2Time.Length - 3)) And ETimehrs >= CInt(Left(Ot2Time, Ot2Time.Length - 3)) Then

                        time1 = Cl
                        'Cl = TimeSpan.Parse("21:00")
                        Cl = TimeSpan.Parse(Ot2Time)
                        HRS = (Cl - St)
                        HRS1 = (time1 - Cl)

                        hour = Left(HRS.ToString(), 5)
                        If Right(hour, 2) > 30 Then
                            Totalhrs = Val(Left(hour, 2)) + 1
                        ElseIf Right(hour, 2) > 0 Then
                            Totalhrs = Val(Left(hour, 2)) + 0.5
                        Else
                            Totalhrs = Val(Left(hour, 2))
                        End If

                        hour1 = Left(HRS1.ToString(), 5)
                        If Right(hour1, 2) > 30 Then
                            Totalhrs1 = Val(Left(hour1, 2)) + 1
                        ElseIf Right(hour1, 2) > 0 Then
                            Totalhrs1 = Val(Left(hour1, 2)) + 0.5
                        Else
                            Totalhrs1 = Val(Left(hour1, 2))
                        End If
                        txtTTotal.Text = Totalhrs + Totalhrs1
                        Amount = Totalhrs * h_Rate.Value
                        OTAmount = Totalhrs1 * Rate1
                        txtTotalAmount.Text = Amount + OTAmount
                        'ElseIf STimehrs < 21 And ETimehrs < 21 Then
                    ElseIf STimehrs < CInt(Left(Ot2Time, Ot2Time.Length - 3)) And ETimehrs < CInt(Left(Ot2Time, Ot1Time.Length - 3)) Then


                        If STimehrs <= ETimehrs Then
                            HRS = (Cl - St)
                            hour = Left(HRS.ToString(), 5)
                            If Right(hour, 2) > 30 Then
                                Totalhrs = Val(Left(hour, 2)) + 1
                            ElseIf Right(hour, 2) > 0 Then
                                Totalhrs = Val(Left(hour, 2)) + 0.5
                            Else
                                Totalhrs = Val(Left(hour, 2))
                            End If
                            txtTTotal.Text = Totalhrs
                            Amount = Totalhrs * h_Rate.Value
                            txtTotalAmount.Text = Amount
                        Else
                            time1 = Cl
                            'Cl = TimeSpan.Parse("21:00")
                            Cl = TimeSpan.Parse(Ot2Time)
                            HRS = (Cl - St)
                            HRS1 = (time1 - Cl)

                            hour = Left(HRS.ToString(), 5)
                            If Right(hour, 2) > 30 Then
                                Totalhrs = Val(Left(hour, 2)) + 1
                            ElseIf Right(hour, 2) > 0 Then
                                Totalhrs = Val(Left(hour, 2)) + 0.5
                            Else
                                Totalhrs = Val(Left(hour, 2))
                            End If

                            HRS1 = HRS1 + TimeSpan.FromHours(24)
                            hour1 = Left(HRS1.ToString(), 5)

                            If Right(hour1, 2) > 30 Then
                                Totalhrs1 = Val(Left(hour1, 2)) + 1
                            ElseIf Right(hour1, 2) > 0 Then
                                Totalhrs1 = Val(Left(hour1, 2)) + 0.5
                            Else
                                Totalhrs1 = Val(Left(hour1, 2))
                            End If
                            txtTTotal.Text = Totalhrs + Totalhrs1
                            Amount = Totalhrs * h_Rate.Value
                            OTAmount = Totalhrs1 * Rate1
                            txtTotalAmount.Text = Amount + OTAmount
                        End If

                        'ElseIf STimehrs >= 21 And ETimehrs >= 21 Then
                    ElseIf STimehrs >= CInt(Left(Ot2Time, Ot2Time.Length - 3)) And ETimehrs >= CInt(Left(Ot2Time, Ot2Time.Length - 3)) Then


                        HRS = (Cl - St)
                        hour = Left(HRS.ToString(), 5)
                        If Right(hour, 2) > 30 Then
                            Totalhrs = Val(Left(hour, 2)) + 1
                        ElseIf Right(hour, 2) > 0 Then
                            Totalhrs = Val(Left(hour, 2)) + 0.5
                        Else
                            Totalhrs = Val(Left(hour, 2))
                        End If
                        txtTTotal.Text = Totalhrs
                        Amount = Totalhrs * Rate1
                        txtTotalAmount.Text = Amount
                    Else
                        HRS = (Cl - St)
                        HRS = HRS + TimeSpan.FromHours(24)

                        hour = Left(HRS.ToString(), 5)
                        If Right(hour, 2) > 30 Then
                            Totalhrs = Val(Left(hour, 2)) + 1
                        ElseIf Right(hour, 2) > 0 Then
                            Totalhrs = Val(Left(hour, 2)) + 0.5
                        Else
                            Totalhrs = Val(Left(hour, 2))
                        End If
                        txtTTotal.Text = Totalhrs
                        Amount = Totalhrs * Rate1
                        txtTotalAmount.Text = Amount
                    End If
                End If
            End If
            txtNetAmount.Text = Val(txtTotalAmount.Text) + Val(txtOutStation.Text) + Val(txtoOTAmt.Text)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        refreshGrid()
        checkHolidaysRate()
    End Sub
    Private Property TPTEXGRATIACHARGES() As DataTable
        Get
            Return ViewState("TPTEXGRATIACHARGES")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TPTEXGRATIACHARGES") = value
        End Set
    End Property
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt("ExGratia Charges")
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub chkAL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim Gross As Decimal = 0
        'For Each gvr As GridViewRow In GridViewShowDetails.Rows
        '    Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
        '    Dim Chkal As CheckBox = TryCast(GridViewShowDetails.HeaderRow.FindControl("chkAL"), CheckBox)
        '    If Chkal.Checked = True Then
        '        Dim lblGross As Label = TryCast(gvr.FindControl("lblAmt"), Label)
        '        If (gvr.Cells(0).Enabled = True) Then
        '            ChkBxItem.Checked = True
        '            Gross += Val(lblGross.Text)
        '        End If
        '    Else
        '        ChkBxItem.Checked = False
        '        Gross = 0
        '    End If
        'Next
        'txtSelectedAmt.Text = Gross
    End Sub
    Protected Sub txtOasis_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOasis.TextChanged
        Dim DriverName As String
        DriverName = Mainclass.getDataValue("select Driver from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtOasis.Text) & "'", "OASIS_TRANSPORTConnectionString")
        h_Emp_id.Value = Mainclass.getDataValue("select driver_empid from VW_DRIVER where  rtrim(ltrim(DRIVER_EMPNO))='" & Trim(txtOasis.Text) & "'", "OASIS_TRANSPORTConnectionString")
        If DriverName <> "" Then
            txtDriverName.Text = DriverName
            BindChargingBsu()
        Else
            lblError.Text = "Invalid Driver No...!:"
            txtOasis.Text = ""
            txtOasis.Focus()
            BindChargingBsu()
            Exit Sub
        End If
    End Sub
    Private Sub TotalAmount()
        'txtNetAmount.Text = Val(txtNAmount.Text) + Val(txtHAmount.Text) + Val(txtCVAmount.Text)
    End Sub
    Protected Sub ddlBusinessUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessUnit.SelectedIndexChanged
        refreshGrid()
    End Sub
    Protected Sub rblDriverType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblDriverType.SelectedIndexChanged
        checkHolidaysRate()
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            cmd.CommandText = "RPTEXGRATIACHARGES"
            Dim sqlParam(2) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@FROMDT", txtFromDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@TODT", txtToDate.Text, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@BSUID", ddlBusinessUnit.SelectedValue, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("preparedby") = Mainclass.getDataValue("SELECT TOP 1 EGC_USER FROM TPTEXGRATIACHARGES where EGC_BSU_ID='" & ddlBusinessUnit.SelectedValue & "' ORDER BY EGC_ID  DESC", "OASIS_TRANSPORTConnectionString")
            params("reportCaption") = "Transport Extra Trips"
            params("bsuName") = ddlBusinessUnit.SelectedItem.Text
            params("DateRange") = txtFromDate.Text & " - " & txtToDate.Text
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Transport/ExtraHiring/reports/rptExGratiaCharges.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ReportLoadSelection()

        Dim ReportPath As String = WebConfigurationManager.AppSettings.Item("CrystalPath")

        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/" & ReportPath & "/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/" & ReportPath & "/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub BindPurpose()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sql_str As String = "select  TMS_DESCR from VW_TPT_PURPOSE order by TMS_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_str)
        RadPurpose.DataSource = ds
        RadPurpose.DataTextField = "TMS_DESCR"
        RadPurpose.DataValueField = "TMS_DESCR"
        RadPurpose.DataBind()
    End Sub
    Protected Sub txtFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToDate.TextChanged
        refreshGrid()
    End Sub
    Protected Sub chkOutStation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOutStation.CheckedChanged
        If chkOutStation.Checked = True Then
            txtOutStation.Text = 25
            txtNetAmount.Text = Val(txtTotalAmount.Text) + 25 + Val(txtoOTAmt.Text)
        Else
            txtOutStation.Text = 0.0
            txtNetAmount.Text = Val(txtTotalAmount.Text) + Val(txtoOTAmt.Text)
        End If
    End Sub
    Sub RecallorApprove(ByVal Action As String)

        Dim ids As String = ""
        For Each gvr As GridViewRow In GridViewShowDetails.Rows
            Dim ChkBxItem As CheckBox = TryCast(gvr.FindControl("chkControl"), CheckBox)
            Dim lblID As Label = TryCast(gvr.FindControl("lblEGCID"), Label)
            Dim lblNet As Label = TryCast(gvr.FindControl("lblNet"), Label)
            If ChkBxItem.Checked = True Then

                ids &= IIf(ids <> "", "|", "") & lblID.Text
            End If
        Next

        If ids = "" Then
            'UserMapPath() '.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If


        Try
            Dim pParms(4) As SqlParameter
            pParms(0) = Mainclass.CreateSqlParameter("@IDS", ids, SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
            If Action = "R" Then
                pParms(4) = Mainclass.CreateSqlParameter("@OPTION", 1, SqlDbType.Int)
            Else
                pParms(4) = Mainclass.CreateSqlParameter("@OPTION", 2, SqlDbType.Int)
            End If


            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "RecallExgratiaCharges", pParms)
                If RetVal = "-1" Then
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    clearScreen()
                    refreshGrid()
                    If Action = "R" Then
                        usrMessageBar.ShowNotification("Records Recalled Successfully", UserControls_usrMessageBar.WarningType.Success)
                    Else
                        usrMessageBar.ShowNotification("Records sent for Approval Successfully", UserControls_usrMessageBar.WarningType.Success)
                    End If

                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnRecall_Click(sender As Object, e As EventArgs) Handles btnRecall.Click
        RecallorApprove("R")
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        RecallorApprove("A")
    End Sub

    Private Sub btnPost_Click(sender As Object, e As EventArgs) Handles btnPost.Click

    End Sub
End Class






