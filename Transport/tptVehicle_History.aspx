<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tptVehicle_History.aspx.vb" Inherits="Transport_tptVehicle_History" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>

<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap header files ends here -->


<script language="javascript" type="text/javascript">


    function getVehicleDetail(mode) {
        var sFeatures;
        sFeatures = "dialogWidth: 460px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;

        url = 'tptShowDetail.aspx?id=' + mode;
        document.getElementById('<%=hdn_mode.ClientID%>').value = mode;

        if (mode == 'BO') {
            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_newitem");
          <%--  if (result == '' || result == undefined)
            { return false; }
            NameandCode = result.split('___');
            document.getElementById("<%=txtBSU_TO.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfBSU_TO.ClientID %>").value = NameandCode[1];--%>
        }

    }

    function getDate(val) {
        var sFeatures;
        sFeatures = "dialogWidth: 227px; ";
        sFeatures += "dialogHeight: 252px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: no; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        //alert("hello")
        result = window.showModalDialog("../Payroll/Calendar.aspx", "", sFeatures)
        if (result == '' || result == undefined) {

            return false;
        }

        if (val == 1) {
            document.getElementById('<%=txtFROMDT.ClientID %>').value = '';
            document.getElementById('<%=txtFROMDT.ClientID %>').value = result;
        }

    }

    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    function OnClientClose(oWnd, args) {
        var NameandCode;
        var arg = args.get_argument();

        if (arg) {
            var mode = document.getElementById('<%=hdn_mode.ClientID%>').value
            //alert(mode);
            if (mode == 'BO') {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtBSU_TO.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfBSU_TO.ClientID %>").value = NameandCode[1];
            }
        }
    }

    function closepopup() {
        //alert(0);
        parent.CloseFrame();
    }

</script>

<base target="_self" />

     <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_newitem" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
       
    </telerik:RadWindowManager>

<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <table width="100%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error"
                        ValidationGroup="CheckError" DisplayMode="List" EnableViewState="False" />
                </td>
            </tr>
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite" colspan="2">
                                <asp:Literal ID="ltLabel" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">From Business Unit</span><span class="text-danger">*</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtBSU_From" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">To Business Unit</span><span class="text-danger">*</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtBSU_TO" runat="server"></asp:TextBox><asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getVehicleDetail('BO');return false;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBSU_TO"
                                    CssClass="error" ErrorMessage="To Business Unit required" ForeColor="" ValidationGroup="CheckError">*</asp:RequiredFieldValidator>


                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">From Date</span><span class="text-danger">*</span></td>

                            <td align="left">
                                <asp:TextBox ID="txtFROMDT" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="imgFROMDT" runat="server" ImageUrl="~/Images/calendar.gif" />
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFROMDT"
                                    CssClass="error" ErrorMessage="From Date required" ForeColor="" ValidationGroup="CheckError">*</asp:RequiredFieldValidator>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnBsuSave" runat="server" CssClass="button" Text="Save" ValidationGroup="CheckError" /><asp:Button
                        ID="btnBsuClose" runat="server" CssClass="button" Text="Close" OnClientClick="closepopup()" CausesValidation="False" />
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">

                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    EmptyDataText="Currently no record available ." UseAccessibleHeader="False" Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Reg. No">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("REGNO") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("REGNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Unit">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBSU" runat="server" Text='<%# Bind("BSU") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Font-Bold="True" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblFROMDT" runat="server" Text='<%# Bind("FROMDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTODT" runat="server" Text='<%# Bind("TODT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VAL_ID" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <HeaderTemplate>

                                                <asp:Label ID="lblVAL_IDH" runat="server" Text="VAL_ID"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblVAL_ID" runat="server" Text='<%# Bind("VAL_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle />
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hfBSU_TO" runat="server" />
        <asp:HiddenField ID="hfBSU_From" runat="server" />
        <asp:HiddenField ID="hfVEH_ID" runat="server" />
        <asp:HiddenField ID="hfVAL_ID" runat="server" />
        &nbsp;<br />
        <asp:HiddenField ID="hdn_mode" runat="server" />
        <ajaxToolkit:CalendarExtender ID="CalendarextenderRegExpDT" runat="server" Format="dd/MMM/yyyy"
            PopupButtonID="imgFROMDT" TargetControlID="txtFROMDT" CssClass="MyCalendar">
        </ajaxToolkit:CalendarExtender>

    </form>


</body>
</html>
