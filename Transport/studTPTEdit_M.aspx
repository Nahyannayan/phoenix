<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTPTEdit_M.aspx.vb" Inherits="Students_studTPTEdit_M" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script type="text/javascript" language="javascript">

function ToolTipEnable(tip) { 
var theTip = document.getElementById("ToolTip");

theTip.style.top = window.event.clientY + 10;theTip.style.left = window.event.clientX; 
theTip.innerText = tip.options[tip.selectedIndex].text;

theTip.style.visibility = "visible"; 
}

function ToolTipDisable() {

document.getElementById("ToolTip").style.visibility = "hidden"; 
}


          function PAddtoTrip() 
            {        
            var sFeatures;
            sFeatures="dialogWidth: 400px; ";
            sFeatures+="dialogHeight: 400px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            var Pickup=document.getElementById("<%=ddlPPickup.ClientID%>");
            pnt_id=Pickup.options[Pickup.selectedIndex].value;
            url='tptAddPickuptoTrip.aspx?pntid='+pnt_id;
            window.showModalDialog(url,"", sFeatures);
                           
           }
           
                  function DAddtoTrip() 
            {        
            var sFeatures;
            sFeatures="dialogWidth: 400px; ";
            sFeatures+="dialogHeight: 400px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            var DropOff=document.getElementById("<%=ddlDPickup.ClientID%>");
            pnt_id=DropOff.options[DropOff.selectedIndex].value;
            url='tptAddPickuptoTrip.aspx?pntid='+pnt_id;
            window.showModalDialog(url,"", sFeatures);
                           
           }
           
           
           function AddtoTrip() 
            {        
            var sFeatures;
            sFeatures="dialogWidth: 500px; ";
            sFeatures+="dialogHeight: 400px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
             url='tptAddAreaPickuptoTrip.aspx?';
            //window.showModalDialog(url,"", sFeatures);
            //return true;

            var oWnd = radopen(url, "pop_addtotrip");

        }
           


           function autoSizeWithCalendar(oWindow) {
               var iframe = oWindow.get_contentFrame();
               var body = iframe.contentWindow.document.body;

               var height = body.scrollHeight;
               var width = body.scrollWidth;

               var iframeBounds = $telerik.getBounds(iframe);
               var heightDelta = height - iframeBounds.height;
               var widthDelta = width - iframeBounds.width;

               if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
               if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
               oWindow.center();
           }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_addtotrip" runat="server" Behaviors="Close,Move" 
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
     

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i>Edit Student Transportation
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">
       
        <tr valign="bottom">
            <td align="left" valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"></asp:Label></td>
        </tr>
    <tr valign="bottom">
        <td align="center">
            Fields Marked with (<span class="text-danger">*</span>) are mandatory
        </td>
    </tr>
        <tr>
            <td width="100%">
                <table align="center" width="100%">
                     <tr>
                        <td align="left" class="title-bg" colspan="4">
                            Student Details 
                                <asp:LinkButton ID="lnkPickup" runat="server" CausesValidation="False"
                                    OnClientClick="javascript:AddtoTrip(); return false;">Add Pickup Point to Trip</asp:LinkButton>
                        </td>
                    </tr>
                          <tr>
                        <td  align="left" width="20%"><span class="field-label">Student Id</span></td>
                        
                        <td  align="left" width="30%">  
                            <asp:Label id="lblSEN" runat="server" Text="Label" CssClass="field-value"></asp:Label></td>
                    
                      <td align="left" width="20%"><span class="field-label">Name</span></td>
                      
                        <td align="left" width="20%">
                            <asp:Label id="lblName" runat="server" Text="Label" CssClass="field-value"></asp:Label></td>
                    
                    
                    </tr>
                    
                       <tr>
                        <td  align="left" width="20%"><span class="field-label">Grade</span></td>
                        
                        <td  align="left" width="30%">  
                            <asp:Label id="lblGrade" runat="server" Text="Label" CssClass="field-value"></asp:Label></td>
                    
                      <td align="left" width="20%"><span class="field-label">Section</span></td>
                      
                        <td align="left" width="30%">
                            &nbsp;<asp:Label id="lblSection" runat="server" Text="Label" CssClass="field-value"></asp:Label></td>
                                      
                        </tr>
                    
                    
                    <tr >
                        <td align="left" width="20%">
                           <span class="field-label"> Bus/Pickup Change Date </span><span class="text-danger">*</span></td>
                        
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator
                                ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom" Display="Dynamic"
                                ErrorMessage="Enter the Trip Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                    ID="CustomValidator3" runat="server" ControlToValidate="txtFrom" CssClass="error"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date"
                                    ValidationGroup="groupM1">*</asp:CustomValidator><br />
                            <asp:RequiredFieldValidator ID="rfFrom" runat="server" ControlToValidate="txtFrom"
                                CssClass="error" Display="Dynamic" ErrorMessage="Please enter the bus change date" Font-Size="Medium"></asp:RequiredFieldValidator>
                        </td>
                        <td width="20%"></td>
                           <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg">                            
                              Pickup Point </td>
                    </tr>
                  
                    
                    <tr>
                    <td align="left" width="20%">
               <asp:Label ID="lblloc" runat="server" CssClass="field-label" Text="Location"></asp:Label></td>
              
                  
                 <td align="left" width="30%">
                    <asp:TextBox id="txtPLocation" runat="server" Enabled="False"></asp:TextBox></td>
                        
                   
                 <td align="left" width="20%">
               <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Area"></asp:Label></td>
              
                   
                 <td align="left" width="30%">
                    <asp:TextBox id="txtPSubLocation" runat="server" Enabled="False"></asp:TextBox></td>
                       
            </tr>
               
            <tr>
            
                 <td align="left" width="20%">
               <asp:Label ID="Label6" runat="server" Text="Trip" cssclass="field-label"></asp:Label></td>
              
                 
                 <td align="left"  width="30%">
                     <asp:DropDownList ID="ddlonward"   runat="server" AutoPostBack="True">
                     </asp:DropDownList>
                                       </td>
                   <td align="left" width="50%" colspan="2">
                   <asp:GridView id="gvOnward" runat="server" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                     <Columns>
                                         <asp:TemplateField HeaderText="Capacity">
                                             <ItemTemplate>
                                                    <asp:Label ID="lblCapacity" runat="server" text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Available">
                                             <ItemTemplate>
                                                     <asp:Label ID="lblAvailable" runat="server" text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                        </Columns>
                                     <HeaderStyle CssClass="gridheader_pop"></HeaderStyle>
                                     <RowStyle CssClass="griditem"  ></RowStyle>
                                     <%--<SelectedRowStyle CssClass="Green" ></SelectedRowStyle>
                                     <AlternatingRowStyle CssClass="griditem_alternative" ></AlternatingRowStyle>--%>
                                 </asp:GridView>
                   </td>
            </tr>
            
            
                 <tr>
                        <td align="left" width="20%">
               <asp:Label ID="Label2" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>
                        
                        <td align="left"  width="30%">
                     <asp:DropDownList ID="ddlPPickUp"  runat="server">
                     </asp:DropDownList>
                            </td>
                     <td width="20%"></td>
                           <td width="30%"></td>
                    </tr>
                    
                    
                  <tr>
                        <td align="left" colspan="4" class="title-bg">                            
                             Drop Off Point</td>
                    </tr>
                    
                     <tr>
           
                            <td align="left" width="20%">
               <asp:Label ID="Label3" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>
              
                   
                 <td align="left" width="30%">
                     &nbsp;<asp:TextBox id="txtDLocation" runat="server" Enabled="False"></asp:TextBox></td>
                        
                   
                            <td align="left" width="20%">
               <asp:Label ID="Label4" runat="server" Text="Area" CssClass="field-label"></asp:Label></td>
              
                    
                 <td align="left" width="30%">
                     &nbsp;<asp:TextBox id="txtDSubLocation" runat="server" Enabled="False"></asp:TextBox></td>
                       
            </tr>
                
              <tr>
                            <td align="left" width="20%">
               <asp:Label ID="Label7" runat="server" Text="Trip" CssClass="field-label"></asp:Label></td>
              
                 
                 <td align="left" width="30%">
                     <asp:DropDownList ID="ddlReturn"  runat="server" AutoPostBack="True">
                     </asp:DropDownList>
                   </td>
                    <td align="left" colspan="2" width="50%">
                   <asp:GridView id="gvReturn" runat="server" CssClass="table table-bordered table-row"  EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                     <Columns>
                                         <asp:TemplateField HeaderText="Capacity">
                                             <ItemTemplate>
                                                    <asp:Label ID="lblCapacity" runat="server" text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Available">
                                             <ItemTemplate>
                                                     <asp:Label ID="lblAvailable" runat="server" text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                        </Columns>
                                     <HeaderStyle CssClass="gridheader_pop" ></HeaderStyle>
                                     <RowStyle CssClass="griditem" ></RowStyle>
                                    <%-- <SelectedRowStyle CssClass="Green" ></SelectedRowStyle>
                                     <AlternatingRowStyle CssClass="griditem_alternative" ></AlternatingRowStyle>--%>
                                 </asp:GridView>
                   </td>
                                     
                  </tr>
                  
                  
                       <tr>
                        <td align="left" width="20%">
               <asp:Label ID="Label5" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>
                       
                        <td align="left"  width="30%">
                     <asp:DropDownList ID="ddlDPickUp" runat="server">
                     </asp:DropDownList>
                            </td>
                           <td width="20%"></td>
                           <td width="30%"></td>
                    </tr>
                    
                    
                                        </table>
               </td>
        </tr>
       
       
     
                    
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" CausesValidation="False" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  CausesValidation="False" />
                </td>
        </tr>
        <tr>
            <td align="left">
                <asp:HiddenField ID="hfSTU_ID" runat="server" />
                <asp:HiddenField ID="hfACD_ID" runat="server" /><asp:HiddenField ID="hfDbusNo" runat="server" />
                <asp:HiddenField ID="hfPbusNo" runat="server" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" PopupPosition="TopRight" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfSHF_ID" runat="server" EnableViewState="False" /><asp:HiddenField ID="hfPSBL_ID" runat="server" EnableViewState="False" />
                <asp:HiddenField ID="hfDSBL_ID" runat="server" EnableViewState="False" />
           </td>
        </tr>
    </table>

                </div>
            </div>
        </div>
</asp:Content>

