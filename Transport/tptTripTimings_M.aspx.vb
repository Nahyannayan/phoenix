Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptTripTimings_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000080" And ViewState("MainMnu_code") <> "T000081") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfTRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("trdid").Replace(" ", "+"))

                    GridBind()


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub


    'regular expression for time
    '(^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})([AM|PM|am|pm]{2,2})$)|(^([0-9]|[1][0-9]|[2][0-3])(\s{0,1})([AM|PM|am|pm]{2,2})$)

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TPP_ID,PNT_DESCRIPTION,TPP_TIME FROM  TRANSPORT.PICKUPPOINTS_M AS B INNER JOIN" _
                                  & " TRANSPORT.TRIPS_PICKUP_S AS A ON B.PNT_ID = A.TPP_PNT_ID WHERE TPP_TRD_ID=" + hfTRD_ID.Value.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptTime.DataSource = ds
        gvTptTime.DataBind()
    End Sub

    Private Sub SaveData()
        Dim i As Integer
        Dim strquery As String
        Dim lblTppId As Label
        Dim txtTime As TextBox
        Dim ddlTime As DropDownList

        Dim tppIds As String = ""
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                With gvTptTime

                    For i = 0 To .Rows.Count - 1
                        'lblTppId = New Label
                        'txtPickup = New TextBox
                        'txtDropoff = New TextBox

                        lblTppId = .Rows(i).FindControl("lblTppId")
                        txtTime = .Rows(i).FindControl("txtTime")
                        ddlTime = .Rows(i).FindControl("ddlTime")
                        'If txtTime.Text <> "" Then
                        UtilityObj.InsertAuditdetails(transaction, "edit", "TRANSPORT.TRIPS_PICKUP_S", "TPP_ID", "TPP_TRD_ID", "TPP_ID=" + lblTppId.Text.ToString)

                        strquery = "exec [TRANSPORT].[UpdatetptTripTiming] " + lblTppId.Text.ToString + "," _
                                    & "'" + txtTime.Text + " " + ddlTime.Text + " '"


                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, strquery)
                        If tppIds.Length <> 0 Then
                            tppIds += ","
                        End If
                        tppIds += lblTppId.Text
                        'End If
                    Next
                End With

                Dim flagAudit As Integer
                flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TPP_ID(" + tppIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                transaction.Commit()
                lblError.Text = "Record Saved SuccessFully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTptTime_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTptTime.PageIndexChanging
        Try
            gvTptTime.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTptTime_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTptTime.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtTime As TextBox
                Dim ddlTime As DropDownList
                txtTime = e.Row.FindControl("txtTime")
                ddlTime = e.Row.FindControl("ddlTime")
                If txtTime.Text <> "" And txtTime.Text.Trim <> "AM" And txtTime.Text.Trim <> "PM" Then
                    If txtTime.Text.Trim.Substring(txtTime.Text.Length - 3, 1) = "A" Or txtTime.Text.Trim.Substring(txtTime.Text.Length - 2, 1) = "A" Then
                        ddlTime.Text = "AM"
                        txtTime.Text = txtTime.Text.Replace("AM", "").Trim
                        txtTime.Text = txtTime.Text.Replace("am", "").Trim
                    Else
                        txtTime.Text = txtTime.Text.Replace("PM", "").Trim
                        txtTime.Text = txtTime.Text.Replace("pm", "").Trim
                        ddlTime.Text = "PM"
                    End If
                Else
                    ddlTime.Text = "AM"
                    txtTime.Text = ""
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
