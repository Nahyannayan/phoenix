<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studChangeAreaApprove_M.aspx.vb" Inherits="Transport_studChangeAreaApprove_M" title="Untitled Page" Debug="true" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function PShowSeats() 
 {        
            var sFeatures;
            sFeatures="dialogWidth: 700px; ";
            sFeatures+="dialogHeight: 400px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var sbl_id;
         
            sbl_id=document.getElementById("<%=hfPSBL_ID.ClientID%>").value
            var url = 'tptSeatCapacity_SelectArea_M.aspx?sbl_id=' + sbl_id;
            //window.showModalDialog(url,"", sFeatures);
           
            var oWnd = radopen(url, "pop_showseats");
           return false;
                      
           }
           
           function DShowSeats() 
            {        
            var sFeatures;
            sFeatures="dialogWidth: 700px; ";
            sFeatures+="dialogHeight: 400px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var sbl_id;
          sbl_id=document.getElementById("<%=hfDSBL_ID.ClientID%>").value
            url='tptSeatCapacity_SelectArea_M.aspx?sbl_id='+sbl_id;
            //window.showModalDialog(url,"", sFeatures);
            var oWnd = radopen(url, "pop_Dshowseats");
           return false;
                      
           }


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       
        <Windows>
            <telerik:RadWindow ID="pop_showseats" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_Dshowseats" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Fee Reminder...
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">

    <tr>
    <td align="left">
    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
    SkinID="error"></asp:Label>
    </td>
    </tr>
    <tr valign="bottom">
    <td align="center">
    Fields Marked with (<span class="text-danger">*</span>) are mandatory
    </td>
    </tr>

    <tr>
    <td width="100%">
    <table align="center" width="100%">
    
        <tr>
        <td align="left" wudth="20%">
            <span class="field-label">Student ID</span></td>
        
        <td align="left" width="30%" colspan="2">
            <asp:Label id="lblStuNo" runat="server" CssClass="field-value"></asp:Label></td>
        <td align="left" width="20%">
            <span class="field-label">Name</span></td>
        
        <td align="left"width="30%" colspan="2">
            <asp:Label id="lblName" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Grade</span></td>
           
            <td align="left" colspan="2">
                <asp:Label id="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
            <td align="left">
                <span class="field-label">Section</span></td>
            
            <td align="left" colspan="2">
                <asp:Label id="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Trip Type</span></td>
           
            <td align="left" colspan="5">
                <asp:Label id="lblTripType" runat="server" Text="Label" CssClass="field-label"></asp:Label>
                <asp:Label id="lblTripValue" runat="server" Text="Label" Visible="False" CssClass="field-label"></asp:Label></td>
        </tr>
        <tr runat="server" id="trOldTripType">
            <td align="left">
                <span class="field-label">Old Trip Type</span></td>
           
            <td align="left" colspan="5">
                <asp:Label id="lblOldTripType" runat="server" Text="Label" CssClass="field-label"></asp:Label>
                <asp:Label id="lblOldTripValue" runat="server" Text="Label" Visible="False" CssClass="field-label"></asp:Label></td>
        </tr>
        <tr >
        <td align="left">
       <span class="field-label"> Starting Date <span class="text-danger">*</span></span>
        </td>
        
        <td align="left" colspan="2">
            <asp:Label id="lblDate" runat="server" CssClass="field-value"></asp:Label></td>
        
            <td align="left">
                <span class="field-label">Approval Date</span></td>
            
            <td align="left" colspan="2">
                <asp:TextBox id="txtApprove" runat="server" AutoPostBack="True" Width="122px">
                </asp:TextBox><asp:ImageButton id="imgApprove" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:CustomValidator
                    id="CustomValidator1" runat="server" ControlToValidate="txtApprove" CssClass="error"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Approval Date entered is not a valid date"
                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                <asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ControlToValidate="txtApprove"
                    Display="Dynamic" ErrorMessage="Enter the Approval Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtApprove"
                    CssClass="error" Display="Dynamic" ErrorMessage="Please enter the approval date"
                   >
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        
        
        <tr>
        <td align="left" colspan="6" class="title-bg">
        
        Pickup Point  <asp:LinkButton id="lnkSeat"
                runat="server" CausesValidation="False" OnClientClick="PShowSeats();javascript:return false;"
                Text="Seat Capacity" ></asp:LinkButton></td>
        </tr>

         <tr>
            <td align="left">
                <asp:Label ID="Label6" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>
            
            <td align="left">
                <asp:Label ID="lblPlocation" runat="server"  CssClass="field-value"></asp:Label></td>
            <td align="left">
                <asp:Label ID="Label7" runat="server" Text="Area"  CssClass="field-label"></asp:Label></td>
            
            <td align="left">
                <asp:Label ID="lblPSublocation" runat="server" CssClass="field-value"></asp:Label></td>
            <td align="left">
                <asp:Label ID="Label8" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>
           
            <td align="left">
                <asp:Label ID="lblPPickup" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>
        </tr>
        
        <tr>

        <td align="left">
        <asp:Label ID="lblloc" runat="server" Text="Requested Location" CssClass="field-label"></asp:Label></td>

        <td align="left">
            &nbsp;<asp:TextBox id="txtPLocation" runat="server" Enabled="False" CssClass="field-value"></asp:TextBox></td>


        <td align="left">
        <asp:Label ID="Label1" runat="server" Text="Requested Area" CssClass="field-label"></asp:Label></td>

       
        <td align="left">
            <asp:TextBox id="txtPsublocation" runat="server" Enabled="False" CssClass="field-value"></asp:TextBox></td>


        <td align="left">
        <asp:Label ID="Label2" runat="server" Text="Requested  PickupPoint" CssClass="field-label"></asp:Label></td>

        <td align="left">
            &nbsp;<asp:TextBox id="txtPPickup" runat="server" Enabled="False" CssClass="field-value"></asp:TextBox></td>

        </tr>

        <tr>

        <td align="left">
        <asp:Label ID="Label61" runat="server" Text="Trip" CssClass="field-label"></asp:Label></td>

        <td align="left">
        <asp:DropDownList ID="ddlonward"   runat="server" AutoPostBack="True">
        </asp:DropDownList>
        </td>
        <td align="left" colspan="4" >
        <asp:GridView id="gvOnward" runat="server" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2" Width="100%">
        <Columns>
        <asp:TemplateField HeaderText="Capacity">
        <ItemTemplate>
        <asp:Label ID="lblCapacity" runat="server" text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Available">
        <ItemTemplate>
        <asp:Label ID="lblAvailable" runat="server" text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="gridheader_pop" ></HeaderStyle>
        <RowStyle CssClass="griditem"  ></RowStyle>
        <SelectedRowStyle CssClass="Green" ></SelectedRowStyle>
        <AlternatingRowStyle CssClass="griditem_alternative" ></AlternatingRowStyle>
        </asp:GridView>
        </td>
        </tr>

        <tr>
        <td align="left" colspan="6"  class="title-bg">
        
        Drop Off Point 
            <asp:LinkButton id="LinkButton1" runat="server" CausesValidation="False"  OnClientClick="javascript:return DShowSeats();" Text="Seat Capacity">
            </asp:LinkButton></td>
        </tr>

           <tr>
            <td align="left">
                <asp:Label ID="Label9" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>
           
            <td align="left">
                <asp:Label ID="lblDlocation" runat="server" CssClass="field-value"></asp:Label></td>
            <td align="left">
                <asp:Label ID="Label10" runat="server" Text="Area" CssClass="field-label"></asp:Label></td>
            
            <td align="left">
                <asp:Label ID="lblDsublocation" runat="server" CssClass="field-value"></asp:Label></td>
            <td align="left">
                <asp:Label ID="Label11" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>
           
            <td align="left">
                <asp:Label ID="lblDPickup" runat="server" CssClass="field-value"></asp:Label></td>
        </tr>


        <tr>

        <td align="left">
        <asp:Label ID="Label3" runat="server" Text="Requested  Location" CssClass="field-label"></asp:Label></td>

        <td align="left">
            &nbsp;<asp:TextBox id="txtDLocation" runat="server" Enabled="False" CssClass="field-value"></asp:TextBox></td>


        <td align="left">
        <asp:Label ID="Label4" runat="server" Text="Requested  Area" CssClass="field-label"></asp:Label></td>

        <td align="left">
            &nbsp;<asp:TextBox id="txtDSublocation" runat="server" Enabled="False" CssClass="field-value"></asp:TextBox></td>


        <td align="left">
        <asp:Label ID="Label5" runat="server" Text="Requested  PickupPoint" CssClass="field-label"></asp:Label></td>

        <td align="left">
            &nbsp;<asp:TextBox id="txtDPickup" runat="server" Enabled="False" CssClass="field-value"></asp:TextBox></td>

        </tr>
        
        <tr>
        <td align="left">
        <asp:Label ID="Label71" runat="server" Text="Trip"  CssClass="field-label"></asp:Label></td>

        <td align="left"   colspan="2" >
        <asp:DropDownList ID="ddlReturn"  runat="server" AutoPostBack="True">
        </asp:DropDownList>
        </td>
        <td align="left" colspan="3">
        <asp:GridView id="gvReturn" runat="server" Width="100%" CssClass="table table-bordered table-row"  EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
        <Columns>
        <asp:TemplateField HeaderText="Capacity">
        <ItemTemplate>
        <asp:Label ID="lblCapacity" runat="server" text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Available">
        <ItemTemplate>
        <asp:Label ID="lblAvailable" runat="server" text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="gridheader_pop" ></HeaderStyle>
        <RowStyle CssClass="griditem"  ></RowStyle>
        <SelectedRowStyle CssClass="Green" ></SelectedRowStyle>
        <AlternatingRowStyle CssClass="griditem_alternative" ></AlternatingRowStyle>
        </asp:GridView>
        </td>

        </tr>
       



        <tr>
            <td align="left">
                <span class="field-label">Request Remarks</span></td>
           
            <td align="left" colspan="5">
                <asp:TextBox id="txtReqRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText" ReadOnly="True"></asp:TextBox></td>
        </tr>
        
        
        <tr>
        <td align="left">
            <span class="field-label">Remarks</span></td>
       
        <td align="left" colspan="5">
        <asp:TextBox id="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText">
        </asp:TextBox></td>
        </tr>
        </table>
        </td>
        </tr>



<tr>
<td colspan="6" align="center">
<asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Accept" />
    <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
    &nbsp;
<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"  CausesValidation="False" />&nbsp;
</td>
</tr>
<tr>
<td valign="bottom">
&nbsp;
<asp:HiddenField ID="hfSTU_ID" runat="server" />
<asp:HiddenField ID="hfACD_ID" runat="server" /><asp:HiddenField ID="hfDbusNo" runat="server" /><asp:HiddenField ID="hfPSBL_ID" runat="server" />
    <asp:HiddenField ID="hfDSBL_ID" runat="server" /><asp:HiddenField ID="hfTCH_ID" runat="server" /><asp:HiddenField ID="hfPPNT_ID" runat="server" />
    <asp:HiddenField ID="hfDPNT_ID" runat="server" />
<asp:HiddenField ID="hfPbusNo" runat="server" />&nbsp;&nbsp;
    <asp:HiddenField ID="hfSHF_ID" runat="server" EnableViewState="False" />
    <asp:HiddenField ID="hfSTU_NO" runat="server" EnableViewState="False" />
    <asp:HiddenField ID="hfNAME" runat="server" EnableViewState="False" />
    <asp:HiddenField ID="hfGRADE" runat="server" EnableViewState="False" />
<asp:HiddenField ID="hfSECTION" runat="server" EnableViewState="False" /><ajaxToolkit:CalendarExtender
    ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
    PopupButtonID="imgApprove" TargetControlID="txtApprove">
</ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="txtApprove" PopupPosition="TopRight" TargetControlID="txtApprove">
    </ajaxToolkit:CalendarExtender>
</td>
</tr>
</table>

                </div>
            </div>
        </div>



</asp:Content>

