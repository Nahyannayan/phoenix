<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptExtraTrip_M.aspx.vb" Inherits="Transport_tptExtraTrip_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function MoveText() {
            document.getElementById('<%=txtTo.ClientID %>').value = document.getElementById('<%=txtFrom.ClientID %>').value;
            return false;
        }
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            EXTRA TRIP SETUP
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" style="width: 100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfTrip" runat="server" ControlToValidate="txtTrip"
                                Display="None" ErrorMessage="Please enter the field Trip" ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfFrom" runat="server" ControlToValidate="txtFrom"
                                Display="None" ErrorMessage="Please enter the starting date of the trip" ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfRemarks" runat="server" ControlToValidate="txtRemarks"
                                Display="None" ErrorMessage="Please enter data in the field Remarks" ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfdest" runat="server" ControlToValidate="txtDestination"
                                Display="None" ErrorMessage="Please enter data in the field destination" ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" Style="text-align: left" Width="342px" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory      
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" style="width: 100%">

                                <tr>

                                    <td align="left" width="20%"><span class="field-label">Trip</span><span class="text-danger">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTrip" runat="server" Width="104px" Enabled="False"></asp:TextBox></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Vehicle Category</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Vehicle Registration No.</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList AutoPostBack="true" ID="ddlVehicle" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblCapacity" runat="server" Style="text-align: right" Width="108px"></asp:Label></td>
                                </tr>
                                <tr>

                                    <td align="left"><span class="field-label">Driver</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlDriver" runat="server" Width="203px" Font-Size="X-Small">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Conductor</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlConductor" runat="server" Font-Size="X-Small">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Purpose</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPurpose" runat="server" Font-Size="X-Small">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Destination</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDestination" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From</span><span class="text-danger">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:ImageButton ID="ImgFrom" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                            runat="server" ControlToValidate="txtFrom" ErrorMessage="From Date required" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender5" CssClass="MyCalendar" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgFrom"
                                            TargetControlID="txtFrom">
                                        </ajaxToolkit:CalendarExtender>

                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtFrom" TargetControlID="txtFrom">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFrom"
                                            ErrorMessage="Enter From date" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator></td>
                                    <td align="left"><span class="field-label">To</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTo" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:ImageButton ID="ImgTo" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            runat="server" ControlToValidate="txtTo" ErrorMessage="From Date required" ValidationGroup="VALSALDET">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTo"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="VALEDITSAL">*</asp:RegularExpressionValidator>
                                        <ajaxToolkit:CalendarExtender ID="calFrom" CssClass="MyCalendar" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgTo"
                                            TargetControlID="txtTo">
                                        </ajaxToolkit:CalendarExtender>

                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="txtTo" TargetControlID="txtTo">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvFromDt" runat="server" ControlToValidate="txtTo"
                                            ErrorMessage="Enter To date" ValidationGroup="VALEDITSAL">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span><span class="text-danger">*</span></td>
                                    <td align="left" colspan="4">
                                        <asp:TextBox SkinID="MultiText" ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" TabIndex="5" OnClientClick="return UnCheckAll();" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" TabIndex="6" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" TabIndex="8" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" OnClientClick="return confirm_delete();" TabIndex="8" /></td>
                                </tr>
                            </table>
                            &nbsp;
               <asp:HiddenField ID="hfTRD_ID" runat="server" />
                            &nbsp; &nbsp;&nbsp;<asp:HiddenField ID="hfTRP_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                            </ajaxToolkit:CalendarExtender>

                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

</asp:Content>

