Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Transport_tptTripAlloc_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(tvLocations)

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000080") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfTRD_ID.Value = 0

                    BindCategory()
                    BindDriver()
                    BindConductor()
                    BindBusNo()
                    BindShift()

                    ViewState("action") = ""
                    If ViewState("datamode") = "view" Then
                        Dim editString As String = Encr_decrData.Decrypt(Request.QueryString("editString").Replace(" ", "+"))
                        BindData(editString)
                        EnableDisableControls(False)
                        hfDRV_ID.Value = ddlDriver.SelectedValue
                        hfCON_ID.Value = ddlConductor.SelectedValue
                        hfVEH_ID.Value = ddlVehicle.SelectedValue
                        rdOnward.Enabled = False
                        rdReturn.Enabled = False
                    Else
                        BindVehicle()
                        hfTRD_ID.Value = 0
                        hfTRP_ID.Value = 0
                        hfDRV_ID.Value = 0
                        hfCON_ID.Value = 0
                        hfVEH_ID.Value = 0
                    End If


                    ' BindTree()

                    BindTreeLocation()

                    If ddlVehicle.Items.Count <> 0 Then
                        GetCapaCity()
                    End If

                    SetVehicleUrl()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If

    End Sub


#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindCategory()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT CAT_ID,CAT_DESCRIPTION FROM TRANSPORT.VV_CATEGORY_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "CAT_DESCRIPTION"
        ddlCategory.DataValueField = "CAT_ID"
        ddlCategory.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = 0
        ddlCategory.Items.Insert(0, li)
    End Sub
    Private Sub BindData(ByVal editString As String)
        Dim editstrings As String() = editString.Split("|")
        lstTrip.Items.Clear()
        lstValues.Value = ""
        hfTRD_ID.Value = editstrings(0)

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TRD_TRP_ID,TRD_DRIVER_EMP_ID,TRD_CONDUCTOR_EMP_ID,TRD_VEH_ID,TRD_BNO_ID,isnull(TRD_REMARKS,''),CAT_ID " _
                                  & " FROM TRANSPORT.TRIPS_D AS A INNER JOIN TRANSPORT.VV_VEHICLE_M AS B ON A.TRD_VEH_ID=B.VEH_ID WHERE TRD_ID=" + editstrings(0)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        While reader.Read
            hfTRP_ID.Value = reader.GetValue(0)
            txtTrip.Text = editstrings(1)

            ddlDriver.ClearSelection()
            ddlDriver.Items.FindByValue(reader.GetValue(1)).Selected = True

            ddlConductor.ClearSelection()
            ddlConductor.Items.FindByValue(reader.GetValue(2)).Selected = True

            ddlCategory.ClearSelection()
            ddlCategory.Items.FindByValue(reader.GetValue(6)).Selected = True

            BindVehicle()

            ddlVehicle.ClearSelection()
            ddlVehicle.Items.FindByValue(reader.GetValue(3)).Selected = True

            ddlBusNo.ClearSelection()
            ddlBusNo.Items.FindByValue(reader.GetValue(4)).Selected = True

            txtRemarks.Text = reader.GetString(5)
            If editstrings(2) = "Onward" Then
                rdOnward.Checked = True
                rdReturn.Checked = False
            ElseIf editstrings(2) = "Return" Then
                rdReturn.Checked = True
                rdOnward.Checked = False
            End If

        End While
        reader.Close()

        str_query = "SELECT PNT_DESCRIPTION, TPP_PNT_ID FROM TRANSPORT.PICKUPPOINTS_M INNER JOIN" _
                    & " TRANSPORT.TRIPS_PICKUP_S ON TRANSPORT.PICKUPPOINTS_M.PNT_ID = TRANSPORT.TRIPS_PICKUP_S.TPP_PNT_ID" _
                    & " WHERE TPP_TRD_ID=" + hfTRD_ID.Value + " ORDER BY TPP_ORDER"
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            li = New ListItem
            li.Text = reader.GetString(0)
            li.Value = reader.GetValue(1).ToString
            lstTrip.Items.Add(li)
            lstValues.Value += "|" + reader.GetString(0) + "$" + reader.GetValue(1).ToString
        End While
        If lstValues.Value <> "" Then
            lstValues.Value = lstValues.Value.Remove(0, 1)
        End If


        lstTrip.ClearSelection()
        reader.Close()
    End Sub

    Sub SetVehicleUrl()

        If ddlVehicle.Items.Count <> 0 Then
            hfVehicleUrl.Value = "../Transport/tptVehicleAllotments_M.aspx?vehid=" + ddlVehicle.SelectedValue.ToString _
                                & "&journey=" + IIf(rdOnward.Checked = True, "Onward", "Return")
        End If

    End Sub

    Private Sub BindVehicle()
        ddlvehicle.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If ddlCategory.SelectedValue <> 0 Then
            str_query = "select veh_id,veh_regno from transport.vv_vehicle_m where VEH_ALTO_BSU_ID='" + Session("sbsuid") + "' " _
                                       & " and cat_id=" + ddlCategory.SelectedValue.ToString
        Else
            str_query = "select veh_id,veh_regno from transport.vv_vehicle_m where VEH_ALTO_BSU_ID='" + Session("sbsuid") + "' "
        End If

        str_query += " order by veh_regno"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlVehicle.DataSource = ds
        ddlVehicle.DataTextField = "veh_regno"
        ddlVehicle.DataValueField = "veh_id"
        ddlVehicle.DataBind()
    End Sub

    Sub GetCapaCity()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT VEH_CAPACITY FROM VEHICLE_M WHERE VEH_ID=" + ddlVehicle.SelectedValue.ToString
        lblCapacity.Text = "Capacity : " + SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
    End Sub

    Private Sub BindBusNo()
        ddlBusNo.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select bno_id,bno_descr from TRANSPORT.busnos_m where bno_bsu_id='" + Session("sbsuid") + "' order by bno_descr "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "bno_descr"
        ddlBusNo.DataValueField = "bno_id"
        ddlBusNo.DataBind()
    End Sub

    Private Sub BindShift()
        ddlShift.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SHF_ID,SHF_DESCR FROM VV_SHIFTS_M WHERE SHF_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
        'Dim li As New ListItem
        'li.Text = ""
        'li.Value = ""
        'ddlShift.Items.Add(LI)
    End Sub

    Sub BindDriver()
        ddlDriver.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
        '                         & "emp_id FROM employee_m WHERE emp_des_id=(select des_id from empdesignation_m where des_descr='Driver' and des_flag='SD') and emp_bsu_id='" + Session("sBsuid") + "'"
        '
        'Dim str_query As String = "SELECT  emp_name,emp_id FROM VV_DRIVER where bsu_alto_id='" + Session("sBsuid") + "'" _
        '                       & " ORDER BY EMP_NAME"


        Dim str_query As String = "SELECT V.emp_name,V.emp_id FROM VV_DRIVER  V  INNER JOIN TRANSPORT.DRIVERCON_ALLOC_S D  " & _
        " on V.emp_id=D.TRC_EMP_ID where D.TRC_BSU_ALTO_ID='" + Session("sBsuid") + "'" _
                                    & " ORDER BY V.EMP_NAME"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDriver.DataSource = ds
        ddlDriver.DataTextField = "emp_name"
        ddlDriver.DataValueField = "emp_id"
        ddlDriver.DataBind()
    End Sub

    Sub BindConductor()
        ddlConductor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
        '                         & "emp_id FROM employee_m WHERE emp_des_id=(select des_id from empdesignation_m where des_descr='Conductor' and des_flag='SD') and emp_bsu_id='" + Session("sBsuid") + "'"

        Dim str_query As String = "SELECT V.emp_name,V.emp_id FROM VV_CONDUCTOR  V    " & _
        "  where V.BSU_ALTO_ID='" + Session("sBsuid") + "'" _
                                    & " ORDER BY V.EMP_NAME"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlConductor.DataSource = ds
        ddlConductor.DataTextField = "emp_name"
        ddlConductor.DataValueField = "emp_id"
        ddlConductor.DataBind()
    End Sub

    Private Sub TransfertoList()
        lstTrip.Items.Clear()
        RecollectTrips()
        lstTrip.ClearSelection()
        Dim treeNode As TreeNode
        Dim subNode As TreeNode
        Dim childNode As TreeNode
        Dim li As New ListItem
        Dim mainNode As TreeNode
        '  mainNode = tvLocations.Nodes(0)
        For Each treeNode In tvLocations.Nodes
            For Each subNode In treeNode.ChildNodes
                For Each childNode In subNode.ChildNodes
                    If childNode.Checked = True Then
                        li = New ListItem
                        li.Text = childNode.Text
                        li.Value = childNode.Value
                        If lstTrip.Items.IndexOf(li) = -1 Then
                            lstTrip.Items.Add(li)
                            lstValues.Value += "|" + childNode.Text + "$" + childNode.Value
                        End If
                    End If
                Next
            Next
        Next
        If lstValues.Value.Length <> 0 Then
            If lstValues.Value(0) = "|" Then
                lstValues.Value = lstValues.Value.Remove(0, 1)
            End If
        End If
        lstTrip.ClearSelection()
    End Sub



    Private Sub BindTree()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        str_query = "SELECT LOC_ID,LOC_DESCRIPTION,SBL_ID,SBL_DESCRIPTION, " _
                    & " PNT_ID, PNT_DESCRIPTION FROM  TRANSPORT.LOCATION_M INNER JOIN " _
                    & "TRANSPORT.SUBLOCATION_M ON TRANSPORT.LOCATION_M.LOC_ID = TRANSPORT.SUBLOCATION_M.SBL_LOC_ID INNER JOIN" _
                    & " TRANSPORT.PICKUPPOINTS_M ON TRANSPORT.SUBLOCATION_M.SBL_ID = TRANSPORT.PICKUPPOINTS_M.PNT_SBL_ID " _
                    & " WHERE PNT_BSU_ID='" + Session("sbsuid") + "' order by LOC_ID,SBL_ID for xml auto"


        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String = ""
        While reader.Read
            str += reader.GetString(0)
        End While


        Dim xl As New SqlString

        str = str.Replace("LOC_ID", "ID")
        str = str.Replace("LOC_DESCRIPTION", "TEXT")
        str = str.Replace("SBL_ID", "ID")
        str = str.Replace("SBL_DESCRIPTION", "TEXT")
        str = str.Replace("PNT_ID", "ID")
        str = str.Replace("PNT_DESCRIPTION", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvLocations.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvLocations.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvLocations.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long

        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                tNode = inTreeNode.ChildNodes(i)
                If xNode.HasChildNodes Then
                    AddNode(xNode, tNode)
                End If
            Next
        Else
            inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
        End If
    End Sub

    Private Function SaveData() As Boolean
        Dim datamode As String = ""
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim reader As SqlDataReader
        Dim str_query As String
        Dim flag As Boolean = True
        Dim i As Integer

        RecollectTrips()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                'save trips_m
                Dim journey As String
                If rdOnward.Checked = True Then
                    journey = "Onward"
                ElseIf rdReturn.Checked = True Then
                    journey = "Return"
                Else
                    journey = "Extra"
                End If
                str_query = "exec [TRANSPORT].[SaveTRIPS_M] " _
                            & hfTRP_ID.Value + "," _
                            & "'" + Session("sbsuid") + "'," _
                             & "'" + IIf(hfTRP_ID.Value = 0, txtTrip.Text.Replace("'", "") + "-" + journey, txtTrip.Text.Replace("'", "")) + "'," _
                            & "'" + journey + "'," _
                            & ddlShift.SelectedValue.ToString + "," _
                            & "'" + ViewState("datamode") + "'"
                hfTRP_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

                If ViewState("datamode") = "edit" Then

                    str_query = "SELECT TRD_TRP_ID,TRD_VEH_ID,TRD_BNO_ID,TRD_DRIVER_EMP_ID,TRD_CONDUCTOR_EMP_ID " _
                              & " FROM TRANSPORT.TRIPS_D WHERE TRD_ID =" + hfTRD_ID.Value
                    reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
                    While reader.Read
                        With reader
                            If .GetValue(1) <> CType(ddlVehicle.SelectedValue, Integer) Then
                                flag = False
                            End If
                            If .GetValue(2) <> CType(ddlBusNo.SelectedValue, Integer) Then
                                flag = False
                            End If
                            If .GetValue(3) <> CType(ddlDriver.SelectedValue, Integer) Then
                                flag = False
                            End If
                            If .GetValue(4) <> CType(ddlConductor.SelectedValue, Integer) Then
                                flag = False
                            End If
                        End With
                    End While

                    reader.Close()

                    str_query = "SELECT TPP_PNT_ID FROM TRANSPORT.TRIPS_PICKUP_S WHERE TPP_TRD_ID=" + hfTRD_ID.Value + " ORDER BY TPP_ORDER"
                    reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

                    Dim str1 As String = ""
                    Dim str2 As String = ""

                    'WHEN a change is made to the list box at client side it is not reflected at the server side.
                    'data is again added through server side

                    While reader.Read
                        str1 += reader.GetValue(0).ToString + "|"
                    End While


                    For i = 0 To lstTrip.Items.Count - 1
                        str2 += lstTrip.Items(i).Value.ToString + "|"
                    Next
                    If str1 <> str2 Then

                        'check if any pickuppoint alloted to a student is removed
                        If CheckPickup(str1, str2) = False Then
                            lblError.Text = "Cannot remove the pickup points.Students are already allocated to these pickuppoints"
                            Return False
                        End If

                        flag = False
                    End If

                    If flag = False Then
                        datamode = "edit"
                    Else
                        datamode = "none"
                    End If
                ElseIf ViewState("datamode") = "add" Then
                    datamode = "add"
                ElseIf ViewState("datamode") = "delete" Then
                    datamode = "delete"
                End If

                Dim trd_previd As String

                If ViewState("datamode") = "edit" Then
                    trd_previd = hfTRD_ID.Value.ToString
                Else
                    trd_previd = 0
                End If

                If datamode <> "none" Then
                    If datamode = "edit" Or datamode = "delete" Then
                        UtilityObj.InsertAuditdetails(transaction, datamode, "TRANSPORT.TRIPS_D", "TRD_ID", "TRD_TRP_ID", "TRD_ID=" + hfTRD_ID.Value.ToString)
                    End If
                    str_query = "exec [TRANSPORT].[SaveTRIPS_D] '" _
                               & Session("sbsuid") + "'," _
                               & hfTRD_ID.Value + "," _
                               & hfTRP_ID.Value + "," _
                               & ddlVehicle.SelectedValue.ToString + "," _
                               & ddlBusNo.SelectedValue.ToString + "," _
                               & ddlDriver.SelectedValue.ToString + "," _
                               & ddlConductor.SelectedValue.ToString + ",'" _
                               & txtRemarks.Text.ToString + "'," _
                               & "'" + Format(Now.Date, "yyyy-MM-dd") + "'," _
                               & "'" + datamode + "'," _
                               & "'" + Session("sUsr_name") + "'"
                    hfTRD_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                End If

                If datamode <> "delete" And datamode <> "none" Then
                    For i = 0 To lstTrip.Items.Count - 1
                        str_query = "exec [TRANSPORT].[SaveTripPickUpPoints] '" _
                                   & Session("sbsuid") + "'," _
                                   & hfTRP_ID.Value.ToString + "," _
                                   & hfTRD_ID.Value.ToString + "," _
                                   & lstTrip.Items(i).Value.ToString + "," _
                                   & (i + 1).ToString + "," _
                                   & trd_previd
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    Next
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Dim flagAudit As Integer
                If datamode <> "edit" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TRD_ID(" + hfTRD_ID.Value.ToString + ")", datamode, Page.User.Identity.Name.ToString, Me.Page)
                Else
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TRD_ID(" + trd_previd.ToString + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TRD_ID(" + hfTRD_ID.Value.ToString + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                End If
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                ViewState("datamode") = ""
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"

            End Try
        End Using
        Return True
    End Function


    Sub BindTreeLocation()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String = "SELECT LOC_ID,LOC_DESCRIPTION FROM TRANSPORT.LOCATION_M AS A" _
        '                         & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.LOC_ID=B.PNT_LOC_ID WHERE PNT_BSU_ID='" + Session("SBSUID") + "'"

        Dim str_query As String = "SELECT LOC_ID,LOC_DESCRIPTION FROM TRANSPORT.LOCATION_M WHERE LOC_ID IN(SELECT DISTINCT PNT_LOC_ID FROM TRANSPORT.PICKUPPOINTS_M WHERE PNT_BSU_ID='" + Session("SBSUID") + "')" _
                                 & " ORDER BY LOC_DESCRIPTION"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim tn As New TreeNode(ds.Tables(0).Rows(i).Item(1).ToString, ds.Tables(0).Rows(i).Item(0).ToString, "", "javascript:void(0)", "_self")
            tn.ShowCheckBox = False
            tvLocations.Nodes.Add(tn)
            ' tn.PopulateOnDemand = True
            BindTreeSubLocation(ds.Tables(0).Rows(i).Item(0).ToString, tn)
        Next
    End Sub

    Sub BindTreeSubLocation(ByVal loc_id As String, ByVal inTreeNode As TreeNode)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A " _
                                  & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID WHERE SBL_LOC_ID='" + loc_id + "' AND PNT_BSU_ID='" + Session("SBSUID") + "'" _
                                  & " ORDER BY SBL_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim tn As New TreeNode(ds.Tables(0).Rows(i).Item(1).ToString, ds.Tables(0).Rows(i).Item(0).ToString, "", "javascript:void(0)", "_self")
            tn.PopulateOnDemand = True
            inTreeNode.ChildNodes.Add(tn)
        Next
    End Sub

    Sub BindTreepPickup(ByVal sbl_id As String, ByVal inTreeNode As TreeNode)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M WHERE PNT_BSU_ID='" + Session("SBSUID") + "' AND PNT_SBL_ID='" + sbl_id + "'" _
                                & " ORDER BY PNT_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim tn As New TreeNode(ds.Tables(0).Rows(i).Item(1).ToString, ds.Tables(0).Rows(i).Item(0).ToString, "", "javascript:void(0)", "_self")
            '  tn.PopulateOnDemand = True
            inTreeNode.ChildNodes.Add(tn)
        Next
    End Sub

    ''to retrive the trips added through client side after postback
    Private Sub RecollectTrips()
        Dim i As Integer
        Dim StrItem() As String
        Dim str() As String
        StrItem = lstValues.Value.Split("|")
        Dim li As New ListItem
        If lstValues.Value <> "" Then
            lstTrip.Items.Clear()
            For i = 0 To StrItem.Length - 1
                str = StrItem(i).Split("$")
                li = New ListItem
                li.Text = str(0)
                li.Value = str(1)
                If lstTrip.Items.IndexOf(li) = -1 Then
                    lstTrip.Items.Add(li)
                End If
            Next
        Else
            lstTrip.Items.Clear()
        End If
    End Sub


    Function CheckPickup(ByVal str1 As String, ByVal str2 As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        Dim pnt1s As String() = str1.Split("|")
        Dim pnt2s As String() = str2.Split("|")
        Dim pntids As String = ""
        Dim i As Integer

        For i = 0 To pnt1s.Length - 1
            If Array.IndexOf(pnt2s, pnt1s(i)) = -1 Then
                If pntids <> "" Then
                    pntids += ","
                End If
                pntids += pnt1s(i)
            End If
        Next
        If pntids <> "" Then

            If rdOnward.Checked = True Then
                str_query = "SELECT COUNT(STU_ID) FROM STUDENT_M WHERE" _
                       & " (STU_PICKUP_TRP_ID=" + hfTRP_ID.Value.ToString _
                       & " AND STU_PICKUP IN(" + pntids + ")) "
            Else
                str_query = "SELECT COUNT(STU_ID) FROM STUDENT_M WHERE" _
                             & " (STU_DROPOFF_TRP_ID=" + hfTRP_ID.Value.ToString _
                             & " AND STU_DROPOFF IN(" + pntids + "))  "
            End If
           

            Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            If count <> 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function


    Private Sub EnableDisableControls(ByVal value As Boolean)
        txtTrip.Enabled = value
        ddlVehicle.Enabled = value
        ddlDriver.Enabled = value
        ddlConductor.Enabled = value
        ddlBusNo.Enabled = value
        '  tvLocations.Enabled = value
        lstTrip.Enabled = value
        ddlShift.Enabled = value
        ddlCategory.Enabled = value
        txtTrip.Enabled = value
       
        txtRemarks.Enabled = value
        btnTransfer.Enabled = value
    End Sub

    Function isStudentAllocated() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If rdOnward.Checked = True Then
            str_query = "SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_CURRSTATUS NOT IN ('CN','TF') AND STU_LEAVEDATE IS NULL AND STU_PICKUP_TRP_ID= " + hfTRP_ID.Value
        ElseIf rdReturn.Checked = True Then
            str_query = "SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_CURRSTATUS NOT IN ('CN','TF') AND STU_LEAVEDATE IS NULL AND STU_DROPOFF_TRP_ID= " + hfTRP_ID.Value
        End If

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Function isVehicleAllocated() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(TRD_VEH_ID) FROM TRANSPORT.TRIPS_D AS A INNER JOIN " _
                               & " TRANSPORT.TRIPS_M AS B ON A.TRD_TRP_ID=B.TRP_ID WHERE TRP_SHF_ID=" + ddlShift.SelectedValue.ToString _
                               & " AND TRP_JOURNEY=" + IIf(rdOnward.Checked = True, "'ONWARD'", "'RETURN'") _
                               & " AND TRD_TODATE IS NULL AND TRD_VEH_ID=" + ddlVehicle.SelectedValue.ToString

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            lblText.Text = "The vehicle " + ddlVehicle.SelectedItem.Text + " for " + IIf(rdOnward.Checked = True, "Onward", "Return") _
                        & " Trip is already allocated in this shift.Do you wish to allocate this vehile again? "
            ViewState("action") = "vehicle"
            Return True
        End If
    End Function


    Function isDriverAllocated() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(TRD_DRIVER_EMP_ID) FROM TRANSPORT.TRIPS_D AS A INNER JOIN " _
                               & " TRANSPORT.TRIPS_M AS B ON A.TRD_TRP_ID=B.TRP_ID WHERE TRP_SHF_ID=" + ddlShift.SelectedValue.ToString _
                               & " AND TRP_JOURNEY=" + IIf(rdOnward.Checked = True, "'ONWARD'", "'RETURN'") _
                               & " AND TRD_TODATE IS NULL AND TRD_DRIVER_EMP_ID=" + ddlDriver.SelectedValue.ToString

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            lblText.Text = "The driver " + ddlDriver.SelectedItem.Text + " for " + IIf(rdOnward.Checked = True, "Onward", "Return") _
                        & " Trip is already allocated in this shift.Do you wish to allocate this driver again? "
            ViewState("action") = "driver"
            Return True
        End If
    End Function

    Function isConductorAllocated() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(TRD_CONDUCTOR_EMP_ID) FROM TRANSPORT.TRIPS_D AS A INNER JOIN " _
                               & " TRANSPORT.TRIPS_M AS B ON A.TRD_TRP_ID=B.TRP_ID WHERE TRP_SHF_ID=" + ddlShift.SelectedValue.ToString _
                               & " AND TRP_JOURNEY=" + IIf(rdOnward.Checked = True, "'ONWARD'", "'RETURN'") _
                               & " AND TRD_TODATE IS NULL AND TRD_CONDUCTOR_EMP_ID=" + ddlConductor.SelectedValue.ToString

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            lblText.Text = "The conductor " + ddlConductor.SelectedItem.Text + " for " + IIf(rdOnward.Checked = True, "Onward", "Return") _
                        & " Trip is already allocated in this shift.Do you wish to allocate this conductor again? "
            ViewState("action") = "conductor"
            Return True
        End If
    End Function



#End Region


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    

   
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If hfTRD_ID.Value = 0 Then
                lblError.Text = "No records to edit"
                Exit Sub
            End If
            ViewState("datamode") = "edit"
            lstTrip.ClearSelection()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            EnableDisableControls(True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            EnableDisableControls(True)
            lstValues.Value = ""
            lstTrip.Items.Clear()
            txtRemarks.Text = ""
            ' txtTrip.Text = ""
            ViewState("datamode") = "add"
            hfTRD_ID.Value = 0
            hfTRP_ID.Value = 0
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfTRD_ID.Value = 0
            lstTrip.ClearSelection()
            lstValues.Value = ""
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                lstTrip.Items.Clear()
                'ddlVehicle.Items(0).Selected = True
                'ddlTrip.Items(0).Selected = True
                'ddlBusNo.Items(0).Selected = True
                'ddlDriver.Items(0).Selected = True
                'ddlConductor.Items(0).Selected = True
                txtRemarks.Text = ""
                EnableDisableControls(False)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ViewState("datamode") = "delete"
            If hfTRD_ID.Value = 0 Then
                lblError.Text = "No records to delete"
                Exit Sub
            End If

            If isStudentAllocated() = True Then
                lblError.Text = "Cannot delete this trip as students are already alloted to this trip"
                Exit Sub
            End If

            SaveData()
            ViewState("datamode") = "none"
            lstTrip.Items.Clear()
            'ddlVehicle.Items(0).Selected = True
            'ddlTrip.Items(0).Selected = True
            'ddlBusNo.Items(0).Selected = True
            'ddlDriver.Items(0).Selected = True
            'ddlConductor.Items(0).Selected = True
            txtRemarks.Text = ""
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlVehicle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVehicle.SelectedIndexChanged
        Try
            GetCapaCity()
            SetVehicleUrl()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            BindVehicle()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Panel1.Visible = False
        EnableDisableControls(True)
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            If ViewState("action") = "vehicle" Then
                If isDriverAllocated() = True Then
                    Panel1.Visible = True
                    EnableDisableControls(False)
                    Exit Sub
                End If
                If isConductorAllocated() = True Then
                    Panel1.Visible = True
                    EnableDisableControls(False)
                    Exit Sub
                End If
            ElseIf ViewState("action") = "driver" Then
                If isConductorAllocated() = True Then
                    Panel1.Visible = True
                    EnableDisableControls(False)
                    Exit Sub
                End If
            End If
            If SaveData() = True Then
                EnableDisableControls(False)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Panel1.Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub rdOnward_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOnward.CheckedChanged
        Try
            If ddlVehicle.SelectedValue <> 0 Then
                SetVehicleUrl()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub rdReturn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdReturn.CheckedChanged
        Try
            If ddlVehicle.SelectedValue <> 0 Then
                SetVehicleUrl()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            btnSave.Enabled = False
            If ViewState("datamode") <> "none" And ViewState("datamode") <> "" Then
                If lstTrip.Items.Count = 0 Then
                    lblError.Text = "Please select the pick up points"
                End If
                If hfVEH_ID.Value <> ddlVehicle.SelectedValue Then
                    If isVehicleAllocated() = True Then
                        Panel1.Visible = True
                        EnableDisableControls(False)
                        btnSave.Enabled = True
                        Exit Sub
                    End If
                End If

                If hfDRV_ID.Value <> ddlDriver.SelectedValue Then
                    If isDriverAllocated() = True Then
                        Panel1.Visible = True
                        EnableDisableControls(False)
                        btnSave.Enabled = True
                        Exit Sub
                    End If
                End If

                If hfCON_ID.Value <> ddlConductor.SelectedValue Then
                    If isConductorAllocated() = True Then
                        Panel1.Visible = True
                        EnableDisableControls(False)
                        btnSave.Enabled = True
                        Exit Sub
                    End If
                End If

                If SaveData() = True Then
                    EnableDisableControls(False)
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            End If
            btnSave.Enabled = True
        Catch ex As Exception
            btnSave.Enabled = True
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

   

    Protected Sub tvLocations_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvLocations.TreeNodePopulate
        'If e.Node.Depth = 1 Then
        '    BindTreeSubLocation(e.Node.Value, e.Node)
        'ElseIf e.Node.Depth = 2 Then
        BindTreepPickup(e.Node.Value, e.Node)

        'End If
    End Sub

  
    Protected Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
        Try
            TransfertoList()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
