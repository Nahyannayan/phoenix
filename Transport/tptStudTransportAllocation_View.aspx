<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptStudTransportAllocation_View.aspx.vb" Inherits="Transport_tptStudTransportAllocation_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">




        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            Transport Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%-- <tr><td>
      <table width="100%" id="Table3" border="0" cellpadding="0" cellspacing="0" width="100%" > 
               <tr >             
                <td width="50%" align="left" class="title">TRANSPORT ALLOCATION 
              </td>
              </tr>
              </table>
       </td> </tr> --%>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>




                    <tr>

                        <td align="left">

                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">



                                <tr>
                                    <td align="center">

                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select Curriculum</span> </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select Academic Year </span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>

                                                </td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select Area</span> </td>

                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlSubLocation" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>

                                                <td align="left" width="20%"><span class="field-label">Select Pickup Point</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlPickUp" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:GridView ID="gvTptTripAlloc" runat="server"
                                                        AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                        OnRowDataBound="gvTptTripAlloc_RowDataBound" PageSize="20" AllowPaging="True" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                                                                        <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                                    </a>
                                                                </ItemTemplate>
                                                                <AlternatingItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
                                                                        <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                                    </a>
                                                                </AlternatingItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="trv_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrdId" runat="server" Text='<%# Bind("TRD_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Trips">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lbltp" runat="server" Text="Trip"></asp:Label><br />
                                                                    <asp:TextBox ID="txtTrip" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnTrip_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnTrip_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrip" runat="server" Text='<%# Bind("TRP_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Shift">
                                                                <HeaderTemplate>
                                                                    Shift<br />
                                                                    <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("SHF_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Journey">
                                                                <HeaderTemplate>
                                                                    Journey<br />
                                                                    <asp:DropDownList ID="ddlgvJourney" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvJourney_SelectedIndexChanged">
                                                                        <asp:ListItem>ALL</asp:ListItem>
                                                                        <asp:ListItem>Onward</asp:ListItem>
                                                                        <asp:ListItem>Return</asp:ListItem>
                                                                        <asp:ListItem>Extra Trip</asp:ListItem>

                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblJourney" runat="server" Text='<%# Bind("TRP_JOURNEY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Vehicle">
                                                                <HeaderTemplate>
                                                                    Vehicle<br />
                                                                    <asp:DropDownList ID="ddlgvVehicle" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvVehicle_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblVehicle" runat="server" Text='<%# Bind("VEH_REGNO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Bus No">
                                                                <HeaderTemplate>
                                                                    Bus No<br />
                                                                    <asp:DropDownList ID="ddlgvBus" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvBus_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBusId" runat="server" Width="70px" Text='<%# Bind("BNO_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Capacity">
                                                                <ItemStyle HorizontalAlign="center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCap" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Available Seats">
                                                                <ItemStyle HorizontalAlign="center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAvailable" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="-" Visible="false">
                                                                <ItemStyle HorizontalAlign="center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShfId" runat="server" Text='<%# Bind("SHF_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:ButtonField CommandName="edit" HeaderText="Allot Students" Text="Allot">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>

                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    </td></tr>
             <tr>
                 <td colspan="100%">
                     <div id="div<%# Eval("GUID") %>" style="display: none; position: relative; left: 20px;">
                         <asp:GridView ID="gvPickUp" runat="server" Width="100%"
                             AutoGenerateColumns="false" EmptyDataText="No pickup points for this.">
                             <Columns>
                                 <asp:BoundField DataField="SBL_DESCRIPTION" HeaderText="Area" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>
                                 <asp:BoundField DataField="PNT_DESCRIPTION" HeaderText="PickUp Points" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>
                                 <asp:BoundField DataField="TPP_TIME" HeaderText="Arrival Time" HtmlEncode="False">
                                     <ItemStyle HorizontalAlign="left" />
                                 </asp:BoundField>

                             </Columns>
                             <RowStyle CssClass="griditem" />
                             <HeaderStyle />
                         </asp:GridView>
                     </div>
                 </td>
             </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem" />
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <SelectedRowStyle />
                                                        <PagerStyle HorizontalAlign="Left" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /></td>
                    </tr>

                </table>



            </div>
        </div>
    </div>
</asp:Content>

