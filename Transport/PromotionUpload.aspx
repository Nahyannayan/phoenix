﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PromotionUpload.aspx.vb" Inherits="PromotionUpload" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
   
    </style>
    <script language="javascript" type="text/javascript">


        function confirm_delete() {

            if (confirm("You are about to cancel this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        

        function showDocument() {

            contenttype = document.getElementById('<%=hdnContentType.ClientID %>').value;
            filename = document.getElementById('<%=hdnFileName.ClientID %>').value;
            result = radopen("../../Inventory/IFrameNew.aspx?id=0&path=TRANSPORT&filename=" + filename + "&contenttype=" + contenttype, "pop_up8")
            return false;
        }

        function Numeric_Only() {
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }

        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label runat="server" ID="rptHeader" Text="Hiring and Billing Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0">
                                
                                <asp:Panel runat="server" ID="pnlJobOrder">
                                    
                                </asp:Panel>
                             
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Promotion Name</span><span style="color: red">*</span></td>

                                    <td align="left" style="width: 30%">
                                                                           
                                        <asp:TextBox ID="txtPromotion" runat="server"></asp:TextBox>

                                    </td>
                                    
                                </tr>

                              

                                

                                <tr>
                                    <td align="left" style="width: 20%"><span class="field-label">Upload Document</span>
                                    </td>

                                    <td align="left" style="width: 30%">
                                        <asp:LinkButton ID="btnDocumentLink" Text="view" Visible='false' runat="server" OnClientClick="showDocument();return false;"></asp:LinkButton>
                                        <asp:LinkButton ID="btnDocumentDelete" Text='[Delete]' Visible='false' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');"></asp:LinkButton>
                                        <asp:FileUpload ID="UploadDocPhoto" runat="server" Visible="false" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                        <asp:HiddenField ID="hdnFileName" runat="server" />
                                        <asp:HiddenField ID="hdnContentType" runat="server" />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Visible="false" />
                                    </td>
                                </tr>

                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" />
                            <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                                        <asp:HiddenField ID="h_fileNamewithPath" runat="server" />
                            <asp:HiddenField ID="h_tempFilepath" runat="server" />
                            <asp:HiddenField ID="h_str_img" runat="server" />
                            <asp:HiddenField ID="h_FileName" runat="server" />



                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>

