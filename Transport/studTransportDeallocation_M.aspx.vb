Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math

Partial Class Transport_studTransportDeallocation_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100200") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvAllot.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0

                    'hfJourney.Value = Encr_decrData.Decrypt(Request.QueryString("journey").Replace(" ", "+"))
                    'hfTRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("trdid").Replace(" ", "+"))
                    ' hfSHF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))
                    ' hfSeats.Value = Encr_decrData.Decrypt(Request.QueryString("seats").Replace(" ", "+"))
                    'hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
                    GridBind()
                    txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else


        End If
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnPTrip_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnDTrip_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAllot.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAllot.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

 
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = " AND " + "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = " AND " + "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvAllot.Rows.Count - 1
            Dim row As GridViewRow = gvAllot.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
      
        Dim str_query As String = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                             & " GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,D.TRP_DESCR AS TRP_PICKUP, " _
                             & " E.TRP_DESCR AS TRP_DROPOFF,SSV_FROMDATE" _
                             & " FROM STUDENT_M  AS A " _
                             & " INNER JOIN OASIS..STUDENT_SERVICES_D AS T ON A.STU_ID=T.SSV_STU_ID AND SSV_TODATE IS NULL" _
                             & " INNER JOIN vv_GRADE_BSU_M AS B ON A.STU_GRM_ID = B.GRM_ID" _
                             & " LEFT OUTER JOIN  vv_SECTION_M AS C ON" _
                             & " A.STU_SCT_ID = C.SCT_ID " _
                             & " LEFT OUTER JOIN TRANSPORT.TRIPS_M AS D ON A.STU_PICKUP_TRP_ID=D.TRP_ID" _
                             & " LEFT OUTER JOIN TRANSPORT.TRIPS_M AS E ON A.STU_DROPOFF_TRP_ID=E.TRP_ID" _
                             & " LEFT JOIN STUDENT_DISCONTINUE_S DIS ON STU_ID=STD_STU_ID AND STU_ACD_ID=STD_ACD_ID AND CONVERT(DATETIME,'" + Format(Now.Date, "yyyy-MM-dd") + "')<=CONVERT(DATETIME,ISNULL(STD_TODATE,'1900-01-01')) AND STD_bAPPROVE='TRUE' " _
                             & " WHERE  STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
                             & " AND (STU_SBL_ID_PICKUP IS NOT NULL OR STU_SBL_ID_DROPOFF IS NOT NULL)" _
                             & " AND STD_STU_ID IS NULL"
        '& " AND STU_ID NOT IN(SELECT STD_STU_ID FROM STUDENT_DISCONTINUE_S WHERE ( CONVERT(DATETIME,'" + Format(Now.Date, "yyyy-MM-dd") + "')<=CONVERT(DATETIME,ISNULL(STD_TODATE,'1900-01-01')) AND STD_bAPPROVE='TRUE') OR (STD_bAPPROVE='TRUE' AND STD_TYPE='P' ))"
                             
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim strPickUp As String = ""
        Dim strDropOff As String = ""
        Dim txtSearch As New TextBox

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""


        If gvAllot.Rows.Count > 0 Then


            txtSearch = gvAllot.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,'')", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtPTrip")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += GetSearchString("D.TRP_DESCR", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strPickUp = txtSearch.Text


            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtDTrip")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += GetSearchString("E.TRP_DESCR", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strDropOff = txtSearch.Text



            ddlgvGrade = gvAllot.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" And ddlgvGrade.Text <> "" Then
             
                strFilter += " AND grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

            ddlgvSection = gvAllot.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then
              
                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
            End If



            If strFilter <> "" Then
                str_query += strFilter
            End If



        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dv As DataView = ds.Tables(0).DefaultView


        Dim dt As DataTable
        gvAllot.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvAllot.DataBind()
            Dim columnCount As Integer = gvAllot.Rows(0).Cells.Count
            gvAllot.Rows(0).Cells.Clear()
            gvAllot.Rows(0).Cells.Add(New TableCell)
            gvAllot.Rows(0).Cells(0).ColumnSpan = columnCount
            gvAllot.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvAllot.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

        Else
            gvAllot.DataBind()

        End If


        dt = ds.Tables(0)
        If gvAllot.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = strName

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtFeeSearch")
            txtSearch.Text = strFee

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtPTrip")
            txtSearch.Text = strPickUp

            txtSearch = New TextBox
            txtSearch = gvAllot.HeaderRow.FindControl("txtDTrip")
            txtSearch.Text = strPickUp

            ddlgvGrade = gvAllot.HeaderRow.FindControl("ddlgvGrade")
            ddlgvSection = gvAllot.HeaderRow.FindControl("ddlgvSection")

            Dim dr As DataRow

            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If ddlgvGrade.Items.FindByText(.Item(3)) Is Nothing Then
                        ddlgvGrade.Items.Add(.Item(3))
                    End If
                    If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvSection.Items.Add(.Item(4))
                    End If

                End With

            Next

            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If


            If selectedSection <> "" Then
                ddlgvSection.Text = selectedSection
            End If



        End If
        set_Menu_Img()
    End Sub


    Function SaveData() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        Dim transaction As SqlTransaction
        Dim journey As String

        Dim stuNos As String = ""
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblPickId As Label
        Dim lblDropId As Label
        Dim lblFromDate As Label
        Dim lblStuNo As Label
        Dim chkFlag As Boolean = False
        Dim lblStuId As Label
        For i = 0 To gvAllot.Rows.Count - 1
            With gvAllot.Rows(i)
                chkSelect = .FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblStuId = .FindControl("lblStuId")
                    lblFromDate = .FindControl("lblFromDate")
                    lblStuNo = .FindControl("lblFeeId")
                    If Date.Parse(lblFromDate.Text) > Date.Parse(txtFrom.Text) Then
                        If stuNos <> "" Then
                            stuNos += ", "
                        End If
                        stuNos += lblStuNo.Text
                    Else
                        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                            Transaction = conn.BeginTransaction("SampleTransaction")
                            Try
                                str_query = "exec TRANSPORT.studDEALLOCATETRANSPORT " _
                                          & ddlAcademicYear.SelectedValue.ToString + "," _
                                          & lblStuId.Text + "," _
                                          & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'," _
                                          & "'" + Session("sUsr_name") + "'"

                                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                                Transaction.Commit()
                                lblError.Text = "Record saved successfully"
                            Catch myex As ArgumentException
                                Transaction.Rollback()
                                lblError.Text = myex.Message
                                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                            Catch ex As Exception
                                Transaction.Rollback()
                                lblError.Text = "Request could not be processed"
                                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                            End Try
                        End Using
                        chkFlag = True
                    End If
                End If
            End With
        Next

        If chkFlag = False And stuNos = "" Then
            lblError.Text = "No records selected"
            Return False
        End If
       
        If stuNos <> "" Then
            lblError.Text = "The following students " + stuNos + " could not be deallocated as the discontinuation date has to be higher than service date"
            'Else
            '    lblError.Text = "Records saved successfully"
        End If


        Return True
    End Function

   

#End Region



    Protected Sub gvAllot_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAllot.PageIndexChanging
        Try
            gvAllot.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btndeAllot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeAllot.Click
        Try
            If SaveData() = True Then
                GridBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        Try
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
            GridBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
