<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studChangeAreaRequest_M.aspx.vb" Inherits="Transport_studChangeAreaRequest_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Student Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>

                    <tr valign="bottom">
                        <td align="center">Fields Marked with (<span class="text-danger">*</span>) are mandatory
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                                    <td align="left" colspan="9" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
       Student Details</span></font></td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblStuNo" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Section</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Trip Type</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblTripType" runat="server" Text="Label" CssClass="field-value"></asp:Label>
                                        <asp:Label ID="lblTripValue" runat="server" Text="Label" Visible="False" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Trip Type</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTripType" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">RoundTrip</asp:ListItem>
                                            <asp:ListItem Value="1">Pick Up</asp:ListItem>
                                            <asp:ListItem Value="2">Drop Off</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Request Date</span></td>

                                    <td align="left">
                                        <asp:TextBox
                                            ID="txtRequest" runat="server" AutoPostBack="True" >
                                        </asp:TextBox><asp:ImageButton ID="imgRequest" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <br />
                                        <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRemarks" Display="Dynamic"
                                            ErrorMessage="Enter the RequestDate From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator1" runat="server" ControlToValidate="txtRequest" CssClass="error"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Request Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRequest"
                                                    CssClass="error" Display="Dynamic" ErrorMessage="Please enter the request date" ValidationGroup="v1"></asp:RequiredFieldValidator></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Starting Date</span> <span class="text-danger">*</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox
                                            ID="txtFrom" runat="server" AutoPostBack="True" ></asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <br />
                                        <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom" Display="Dynamic"
                                            ErrorMessage="Enter the start date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator3" runat="server" ControlToValidate="txtFrom" CssClass="error"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        
                                        <asp:RequiredFieldValidator ID="rfFrom" runat="server" ControlToValidate="txtFrom" ValidationGroup="v1"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter the transport facility starting date"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>


                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Pickup Point</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <table width="100%">
                                            <tr>                                                
                                                <td align="left">
                                                    <asp:Label ID="Label6" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblPlocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label7" runat="server" Text="Area" CssClass="field-label"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblPSublocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label8" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblPPickup" runat="server" CssClass="field-value"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblloc" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPLocation" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Text="Area" CssClass="field-label"></asp:Label></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPSubLocation" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>


                                    <td align="left">
                                        <asp:DropDownList ID="ddlPPickUp" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>


                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Drop Off Point</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="Label9" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblDlocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label10" runat="server" Text="Area" CssClass="field-label"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblDsublocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label11" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblDPickup" runat="server" CssClass="field-value"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="Label3" runat="server" Text="Location" CssClass="field-label"></asp:Label></td>                               
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlDLocation" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" >
                                        <asp:Label ID="Label4" runat="server" Text="Area" CssClass="field-label" ></asp:Label></td>                         
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlDSubLocation" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" >
                                        <asp:Label ID="Label5" runat="server" Text="PickupPoint" CssClass="field-label"></asp:Label></td>                         
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlDPickUp" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                     <td align="left"></td>
                                    <td align="left"></td>
                                </tr>

                                <tr>
                                    <td ><span class="field-label">Remarks</span></td>
                            
                                    <td align="left"colspan="3" >
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine">
                                        </asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td align="center">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="v1" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td >
                <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfDbusNo" runat="server" />
                            <asp:HiddenField ID="hfTCH_ID" runat="server" />
                            <asp:HiddenField ID="hfPSBL_ID" runat="server" EnableViewState="False" />
                            <asp:HiddenField ID="hfDSBL_ID" runat="server" EnableViewState="False" />
                            <asp:HiddenField ID="hfPbusNo" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" PopupPosition="TopRight" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="hfSHF_ID" runat="server" EnableViewState="False" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtRequest" TargetControlID="txtRequest">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgRequest" PopupPosition="TopRight" TargetControlID="txtRequest">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

