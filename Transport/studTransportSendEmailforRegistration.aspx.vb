﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Transport_studTransportSendEmailforRegistration
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000285") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    BindCurriculum()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue.ToString)
                    BindEmirate()
                    BindArea()

                    If Session("sbsuid") <> "900501" And ddlClm.Items.Count = 1 Then
                        trBsu.Visible = False
                    Else
                        If Session("sbsuid") <> "900501" Then
                            td1.Visible = False
                            'td2.Visible = False
                            td3.Visible = False
                            td6.ColSpan = 11
                        End If
                        If ddlClm.Items.Count = 1 Then
                            td4.Visible = False
                            'td5.Visible = False
                            td6.Visible = False
                            td3.ColSpan = 11
                        End If
                    End If
                    BindGrade()
                    BindSection()
                    trGrid.Visible = False
                    trSend.Visible = False
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

    Protected Sub lnkSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblId As Label
        lblId = TryCast(sender.findcontrol("lblId"), Label)
        SendEmail(lblId.Text)
        If rdEnquiry.Checked = True Then
            GridBindEnquiry()
        ElseIf rdStudent.Checked = True Then
            GridBindStudent()
        End If
    End Sub

    Protected Sub ddlgvStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBindEnquiry()
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M " _
                               & " INNER JOIN dbo.SERVICES_BSU_M ON BSU_ID=SVB_BSU_ID" _
                               & " INNER JOIN ACADEMICYEAR_D ON ACD_ID=SVB_ACD_ID AND ACD_CURRENT=1" _
                               & " WHERE SVB_SVC_ID=1 AND SVB_PROVIDER_BSU_ID IN('900500','900501')" _
                               & " AND BSU_bGEMSSCHOOL=1"

        If Session("sbsuid") <> "900501" Then
            str_query += " AND BSU_ID='" + Session("sbsuid") + "'"
        End If

        str_query += " ORDER BY BSU_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub
    Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT DISTINCT CLM_DESCR,CLM_ID FROM CURRICULUM_M INNER JOIN ACADEMICYEAR_D ON " _
                                & " ACD_CLM_ID=CLM_ID AND ACD_CURRENT=1 AND ACD_BSU_ID='" + ddlBsu.SelectedValue.ToString + "' ORDER BY CLM_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClm.DataSource = ds
        ddlClm.DataTextField = "CLM_DESCR"
        ddlClm.DataValueField = "CLM_ID"
        ddlClm.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                              & " WHERE GRM_ACD_ID= " + ddlAcademicYear.SelectedValue.ToString _
                              & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
    End Sub
    Sub BindSection()
        ddlSection.Items.Clear()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        If ddlGrade.SelectedValue = "0" Then
            ddlSection.Items.Add(li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                    & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub GridBindEnquiry()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        str_query = "SELECT ISNULL(EQM_APPLFIRSTNAME,'')+' '+ISNULL(EQM_APPLMIDNAME,'')+' '+ISNULL(EQM_APPLLASTNAME,'') AS APPNAME,EQS_APPLNO AS APPNO, " _
                  & " EQS_ID AS ID,CASE EQM_PRIMARYCONTACT WHEN 'F' THEN ISNULL(EQP_FMIDNAME,'')+' '+ISNULL(EQP_FMIDNAME,'')+' '+ISNULL(EQP_FLASTNAME,'') " _
                  & " WHEN 'M' THEN ISNULL(EQP_MMIDNAME,'')+' '+ISNULL(EQP_MMIDNAME,'')+' '+ISNULL(EQP_MLASTNAME,'') ELSE " _
                  & " ISNULL(EQP_GMIDNAME,'')+' '+ISNULL(EQP_GMIDNAME,'')+' '+ISNULL(EQP_GLASTNAME,'') END AS PNAME  ," _
                  & " CASE EQM_PRIMARYCONTACT WHEN 'F' THEN EQP_FMOBILE WHEN 'M' THEN EQP_MMOBILE ELSE EQP_GMOBILE END AS PMOBILE ," _
                  & " CASE EQM_PRIMARYCONTACT WHEN 'F' THEN EQP_FEMAIL WHEN 'M' THEN EQP_MEMAIL ELSE EQP_GEMAIL END AS PEMAIL ," _
                  & "'ENQUIRY' AS TYPE,ISNULL(EQS_DOJ,'01/01/1900') DOJ,CASE PRO_ID WHEN '3'" _
                  & " THEN 'REGISTERD' WHEN '6' THEN 'OFFERED' ELSE PRO_DESCRIPTION END AS STATUS, " _
                  & " CASE  EQM_PRIMARYCONTACT WHEN 'F' THEN (SELECT ISNULL(EMA_DESCR,'') FROM EMIRATE_AREA_M WHERE EMA_EMR_ID=B.EQP_FCOMAREA_ID) " _
                  & " WHEN 'M' THEN (SELECT ISNULL(EMA_DESCR,'') FROM EMIRATE_AREA_M WHERE EMA_EMR_ID=B.EQP_MCOMAREA_ID) " _
                  & " WHEN 'G' THEN (SELECT ISNULL(EMA_DESCR,'') FROM EMIRATE_AREA_M WHERE EMA_EMR_ID=B.EQP_MCOMAREA_ID) END AS AREA " _
                  & " FROM ENQUIRY_M INNER JOIN ENQUIRY_SCHOOLPRIO_S AS A " _
                  & " ON EQS_EQM_ENQID=EQM_ENQID INNER JOIN ENQUIRY_PARENT_M AS B ON EQP_EQM_ENQID=EQM_ENQID" _
                  & " INNER JOIN PROCESSFO_SYS_M ON PRO_ID=EQS_CURRSTATUS " _
                  & " WHERE EQS_CURRSTATUS<7 AND EQS_STATUS<>'DEL'" _
                  & " AND EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND EQS_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If

        If rdMailSend.Checked = True Then
            str_query += " AND (SELECT COUNT(EML_ID) FROM COM_EMAIL_SCHEDULE WHERE EML_PROFILE_ID=A.EQS_ID AND EML_TYPE='TPT_REQ_ENQ' AND EML_STATUS<>'FAILED')>0 "
        ElseIf rdMailNotSend.Checked = True Then
            str_query += " AND (SELECT COUNT(EML_ID) FROM COM_EMAIL_SCHEDULE WHERE EML_PROFILE_ID=A.EQS_ID AND EML_TYPE='TPT_REQ_ENQ' AND EML_STATUS<>'FAILED')=0 "
        End If

        If txtDojFrom.Text <> "" Then
            str_query += " AND EQS_DOJ>='" + txtDojFrom.Text + "'"
        End If

        If txtDojTo.Text <> "" Then
            str_query += " AND EQS_DOJ<='" + txtDojTo.Text + "'"
        End If

        If ddlEmirate.SelectedValue <> "0" Then
            str_query += " AND EQP_FCOMSTATE_ID=CASE WHEN EQM_PRIMARYCONTACT='F' THEN '" + ddlEmirate.SelectedValue.ToString + "' ELSE EQP_FCOMSTATE_ID END " _
                     & " AND EQP_MCOMSTATE_ID=CASE WHEN EQM_PRIMARYCONTACT='M' THEN '" + ddlEmirate.SelectedValue.ToString + "' ELSE EQP_MCOMSTATE_ID END " _
                     & " AND EQP_GCOMSTATE_ID=CASE WHEN EQM_PRIMARYCONTACT='G' THEN '" + ddlEmirate.SelectedValue.ToString + "' ELSE EQP_GCOMSTATE_ID END "

        End If


        If ddlArea.SelectedValue <> "0" Then
            str_query += " AND EQP_FCOMAREA_ID=CASE WHEN EQM_PRIMARYCONTACT='F' THEN " + ddlArea.SelectedValue.ToString + " ELSE EQP_FCOMAREA_ID END " _
                     & " AND EQP_MCOMAREA_ID=CASE WHEN EQM_PRIMARYCONTACT='M' THEN " + ddlArea.SelectedValue.ToString + " ELSE EQP_MCOMAREA_ID END " _
                     & " AND EQP_GCOMAREA_ID=CASE WHEN EQM_PRIMARYCONTACT='G' THEN " + ddlArea.SelectedValue.ToString + " ELSE EQG_FCOMAREA_ID END "

        End If



        Dim ddlgvStatus As DropDownList
        Dim strStatus As String
        If gvStud.Rows.Count > 0 Then
            ddlgvStatus = gvStud.HeaderRow.FindControl("ddlgvStatus")
            If ddlgvStatus.SelectedValue <> "0" Then
                str_query += " AND EQS_CURRSTATUS='" + ddlgvStatus.SelectedValue.ToString + "'"
            End If
            strStatus = ddlgvStatus.SelectedValue.ToString
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim lblHId As Label = gvStud.HeaderRow.FindControl("lblHId")
            Dim lblHName As Label = gvStud.HeaderRow.FindControl("lblHName")
            lblHId.Text = "Application Number"
            lblHName.Text = "Applicant Name"

            ddlgvStatus = gvStud.HeaderRow.FindControl("ddlgvStatus")

            If strStatus <> "0" Then
                If Not ddlgvStatus.Items.FindByValue(strStatus) Is Nothing Then
                    ddlgvStatus.Items.FindByValue(strStatus).Selected = True
                End If
            End If
        End If


    End Sub

    Sub GridBindStudent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        str_query = "SELECT ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS APPNAME,STU_NO AS APPNO, " _
                  & " STU_ID AS ID,CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                  & " WHEN 'M' THEN ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') ELSE " _
                  & " ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END AS PNAME  ," _
                  & " CASE STU_PRIMARYCONTACT WHEN 'F' THEN STS_FMOBILE WHEN 'M' THEN STS_MMOBILE ELSE STS_GMOBILE END AS PMOBILE ," _
                  & " CASE STU_PRIMARYCONTACT WHEN 'F' THEN STS_FEMAIL WHEN 'M' THEN STS_MEMAIL ELSE STS_GEMAIL END AS PEMAIL ," _
                  & "'STUDENT' AS TYPE,ISNULL(STU_DOJ,'01/01/1900') DOJ,'STUDENT' AS STATUS , " _
                  & " CASE  STU_PRIMARYCONTACT WHEN 'F' THEN (SELECT ISNULL(EMA_DESCR,'') FROM EMIRATE_AREA_M WHERE EMA_EMR_ID=B.STS_FCOMAREA_ID) " _
                  & " WHEN 'M' THEN (SELECT ISNULL(EMA_DESCR,'') FROM EMIRATE_AREA_M WHERE EMA_EMR_ID=B.STS_MCOMAREA_ID) " _
                  & " WHEN 'G' THEN (SELECT ISNULL(EMA_DESCR,'') FROM EMIRATE_AREA_M WHERE EMA_EMR_ID=B.STS_GCOMAREA_ID) END AS AREA " _
                  & " FROM STUDENT_M AS A INNER JOIN STUDENT_D AS B" _
                  & " ON STU_SIBLING_ID=STS_STU_ID " _
                  & " AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                  & " AND STU_CURRSTATUS='EN' AND STU_ID NOT IN(SELECT SSV_STU_ID FROM STUDENT_SERVICES_D WHERE SSV_STU_ID=A.STU_ID AND SSV_SVC_ID=1 AND SSV_TODATE IS NULL) "

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If

        If ddlSection.SelectedValue.ToString <> "0" Then
            str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        If rdMailSend.Checked = True Then
            str_query += " AND (SELECT COUNT(EML_ID) FROM COM_EMAIL_SCHEDULE WHERE EML_PROFILE_ID=A.STU_ID AND EML_TYPE='TPT_REQ_ENQ' AND EML_STATUS<>'FAILED')>0 "
        ElseIf rdMailNotSend.Checked = True Then
            str_query += " AND (SELECT COUNT(EML_ID) FROM COM_EMAIL_SCHEDULE WHERE EML_PROFILE_ID=A.STU_ID AND EML_TYPE='TPT_REQ_ENQ' AND EML_STATUS<>'FAILED')=0 "
        End If

        If txtDojFrom.Text <> "" Then
            str_query += " AND STU_DOJ>='" + txtDojFrom.Text + "'"
        End If

        If txtDojTo.Text <> "" Then
            str_query += " AND STU_DOJ<='" + txtDojFrom.Text + "'"
        End If

        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStudentID.Text + "%'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim lblHId As Label = gvStud.HeaderRow.FindControl("lblHId")
            Dim lblHName As Label = gvStud.HeaderRow.FindControl("lblHName")
            lblHId.Text = "Student ID"
            lblHName.Text = "Student Name"
        End If

    End Sub

    Sub BindEmirate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT EMR_CODE,EMR_DESCR FROM EMIRATE_M WHERE EMR_CTY_ID=172"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
        ddlEmirate.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlEmirate.Items.Insert(0, li)
    End Sub

    Sub BindArea()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlEmirate.SelectedValue.ToString <> "0" Then
            str_query = "SELECT EMA_ID,EMA_DESCR FROM  EMIRATE_AREA_M WHERE EMA_EMR_ID='" + ddlEmirate.SelectedValue.ToString + "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlArea.DataSource = ds
            ddlArea.DataTextField = "EMA_DESCR"
            ddlArea.DataValueField = "EMA_ID"
            ddlArea.DataBind()
        End If
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlArea.Items.Insert(0, li)
    End Sub

    Sub SendEmail(ByVal id As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim emailtype As String

        If rdEnquiry.Checked = True Then
            emailtype = "ENQ_ONLINEREQ"
        Else
            emailtype = "STU_ONLINEREQ"
        End If

        Dim str_query As String = "exec TRANSPORT.SENDEMAILFORREGISTRATION " _
                                & " @ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                & " @BSU_ID='" + ddlBsu.SelectedValue.ToString + "'," _
                                & " @PROFILEID=" + id + "," _
                                & " @EMAILTYPE='" + emailtype + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BulkSendMail()
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblId As Label
        For i = 0 To gvStud.Rows.Count - 1
            With gvStud.Rows(i)
                chkSelect = gvStud.Rows(i).FindControl("chkSelect")
                lblId = gvStud.Rows(i).FindControl("lblId")
                If chkSelect.Checked = True Then
                    SendEmail(lblId.Text)
                End If
            End With
        Next
    End Sub
#End Region

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        BindCurriculum()
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue.ToString)
        If Session("sbsuid") <> "900501" And ddlClm.Items.Count = 1 Then
            trBsu.Visible = False
        Else
            If Session("sbsuid") <> "900501" Then
                td1.Visible = False
                'td2.Visible = False
                td3.Visible = False
                td6.ColSpan = 11
            End If
            If ddlClm.Items.Count = 1 Then
                td4.Visible = False
                'td5.Visible = False
                td6.Visible = False
                td3.ColSpan = 11
            Else
                td4.Visible = True
                'td5.Visible = True
                td6.Visible = True
                td3.ColSpan = 1
            End If
        End If
        BindGrade()
        BindSection()
        trGrid.Visible = False
        trSend.Visible = False
    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue.ToString)
        BindGrade()
        BindSection()
        trGrid.Visible = False
        trSend.Visible = False
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
        trGrid.Visible = False
        trSend.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        trGrid.Visible = True
        trSend.Visible = True
        If rdEnquiry.Checked = True Then
            GridBindEnquiry()
        Else
            GridBindStudent()
        End If
    End Sub

    Protected Sub rdEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdEnquiry.CheckedChanged
        If rdEnquiry.Checked = True Then
            lblIdName.Text = "Applicant Name"
            lblIdText.Text = "Application Number"
            trGrid.Visible = False
            trSend.Visible = False
        End If
    End Sub

    Protected Sub rdStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdStudent.CheckedChanged
        If rdStudent.Checked = True Then
            lblIdName.Text = "Student Name"
            lblIdText.Text = "Student ID"
            trGrid.Visible = False
            trSend.Visible = False
        End If
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        BulkSendMail()
        If rdEnquiry.Checked = True Then
            GridBindEnquiry()
        Else
            GridBindStudent()
        End If
    End Sub

    Protected Sub ddlEmirate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmirate.SelectedIndexChanged
        BindArea()
    End Sub

   
    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        If rdEnquiry.Checked = True Then
            GridBindEnquiry()
        Else
            GridBindStudent()
        End If
    End Sub
End Class
