Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptFuel_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            'If isPageExpired() Then
            '    Response.Redirect("expired.htm")
            'Else
            '    Session("TimeStamp") = Now.ToString
            '    ViewState("TimeStamp") = Now.ToString
            'End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100260") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



                    Session("dateFilter") = " AND CONVERT(VARCHAR,TFL_FILLDATE,101)='" + Format(Now.Date, "MM/dd/yyyy") + "'"
                    txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
                    txtTo.Text = Format(Now.Date, "dd/MMM/yyyy")
                    BindVehicle()
                    tblFuel.Rows(2).Visible = False
                    tblFuel.Rows(3).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT TFL_ID,TFL_VEH_ID,TFL_PREVKM,TFL_CURRENTKM,TFL_GALLON,TFL_FILLDATE,TFL_AMOUNT" _
                                & " FROM TRANSPORT.TRIP_FUEL_S WHERE TFL_FILLDATE BETWEEN '" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'" _
                                & " AND '" + Format(Date.Parse(txtTo.Text), "yyyy-MM-dd") + "' AND TFL_VEH_ID=" + ddlVehicle.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvFuel.DataSource = ds
        gvFuel.DataBind()

    End Sub
    Private Sub BindVehicle()
        ddlvehicle.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "select veh_id,veh_regno from transport.vv_vehicle_m where VEH_ALTO_BSU_ID='" + Session("sbsuid") + "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlVehicle.DataSource = ds
        ddlVehicle.DataTextField = "veh_regno"
        ddlVehicle.DataValueField = "veh_id"
        ddlVehicle.DataBind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            GridBind()

            tblFuel.Rows(2).Visible = True
            tblFuel.Rows(3).Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Function SaveData() As Boolean


        Dim str_query As String

        Dim lbltflId As Label
        Dim txtpreV As TextBox
        Dim txtFill As TextBox
        Dim txtCurrent As TextBox
        Dim txtGallon As TextBox
        Dim txtAmount As TextBox

        Dim i As Integer

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                For i = 0 To gvFuel.Rows.Count - 1
                    With gvFuel.Rows(i)
                        lbltflId = .FindControl("lbltflId")
                        txtpreV = .FindControl("txtPrev")
                        txtFill = .FindControl("txtFill")
                        txtCurrent = .FindControl("txtCurrent")
                        txtGallon = .FindControl("txtGallon")
                        txtAmount = .FindControl("txtAmount")

                        str_query = "exec TRANSPORT.saveFUEL " _
                                        & lbltflId.Text + "," _
                                        & ddlVehicle.SelectedValue.ToString + "," _
                                        & Val(txtpreV.Text).ToString + "," _
                                        & txtCurrent.Text + "," _
                                        & txtGallon.Text + "," _
                                        & txtAmount.Text + "," _
                                        & "'" + Format(Date.Parse(txtFill.Text), "yyyy-MM-dd") + "'," _
                                        & "'edit'"
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End With
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                Return False
            End Try
        End Using
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
