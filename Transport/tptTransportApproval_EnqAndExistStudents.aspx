﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="tptTransportApproval_EnqAndExistStudents.aspx.vb" Inherits="Transport_tptTransportApproval_EnqAndExistStudents" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <style>
        .popfunc {
            z-index: 1  !important;
        }

        .popforefunc {
            z-index: 0 !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function PAddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 400px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            window.returnValue = true;
            pnt_id = document.getElementById("<%=hfPPNT_ID.ClientID%>").value
            bsu_id = document.getElementById("<%=hfBsuID.ClientID%>").value
            url = 'tptAddPickuptoTripNonGems.aspx?pntid=' + pnt_id + '&bsuid=' + bsu_id;
            //window.showModalDialog(url, "", sFeatures);
            //var oWnd = radopen(url, "pop_PAddtoTrip");
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
        }

        function DAddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 400px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            pnt_id = document.getElementById("<%=hfDPNT_ID.ClientID%>").value
            bsu_id = document.getElementById("<%=hfBsuID.ClientID%>").value
            url = 'tptAddPickuptoTripNonGems.aspx?pntid=' + pnt_id + '&bsuid=' + bsu_id;
            //window.showModalDialog(url, "", sFeatures);
            //var oWnd = radopen(url, "pop_DAddtoTrip");
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
        }

        //function autoSizeWithCalendar(oWindow) {
        //    var iframe = oWindow.get_contentFrame();
        //    var body = iframe.contentWindow.document.body;

        //    var height = body.scrollHeight;
        //    var width = body.scrollWidth;

        //    var iframeBounds = $telerik.getBounds(iframe);
        //    var heightDelta = height - iframeBounds.height;
        //    var widthDelta = width - iframeBounds.width;

        //    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        //    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        //    oWindow.center();
        //}
    </script>

    <%--    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_PAddtoTrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" CssClass="popfunc">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_DAddtoTrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>--%>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Transport Request Approval"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" id="Table3" align="center" border="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ValidationGroup="groupM1" Width="624px" />
                            <asp:RequiredFieldValidator ID="rfBSU" runat="server" ControlToValidate="ddlBsu"
                                Display="None" ErrorMessage="Please select a Business Unit" InitialValue="0"
                                SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfAcadYear" runat="server" ControlToValidate="ddlAcademicYear"
                                Display="None" ErrorMessage="Please select an Academic Year" InitialValue="0"
                                SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator>

                            <asp:Label ID="lblPopUp" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tbl_AddGroup" runat="server" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <table id="studTable" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                                    <td align="left" colspan="12" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                               
                            </span></font>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Business Unit</span> <span class="text-danger">*</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Select Curriculum</span> <span class="text-danger">*</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCurriculum" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblReqStatus" runat="server" Text="Request Status" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:RadioButtonList ID="rbtnList" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0">All</asp:ListItem>
                                            <asp:ListItem Value="1" Selected="True">Pending</asp:ListItem>
                                            <asp:ListItem Value="2">Approved</asp:ListItem>
                                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span> <span class="text-danger">*</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>

                                    <td align="left">
                                        <asp:Label ID="lblGrade" runat="server" Text="Select Grade" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblStage" runat="server" Text="Stage" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlStage" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <span class="field-label">
                                            <asp:RadioButtonList ID="rdReqType" runat="server" RepeatDirection="Horizontal"
                                                AutoPostBack="True">
                                                <asp:ListItem Text="Student" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Enquiry" Value="1"></asp:ListItem>
                                            </asp:RadioButtonList></span>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" class="matters" id="tdAppNo" runat="server">
                                        <asp:Label ID="lblApplicationNO" runat="server" Text="Student ID" CssClass="field-label"></asp:Label></td>

                                    <td align="left" class="matters" id="tdAppNoBox" runat="server">
                                        <asp:TextBox ID="txtApplicationNO" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" class="matters" id="tdAppName" runat="server">
                                        <asp:Label ID="lblApplicantName" runat="server" Text="Student Name" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left" class="matters" id="tdAppNameBox" runat="server">
                                        <asp:TextBox ID="txtApplicantName" runat="server"></asp:TextBox></td>
                                </tr>
                                <%--<td align="left" class="matters" id="tdStudId" runat="server" >
                            <asp:Label ID="Label1" runat="server" Text="Student ID"></asp:Label></td>
                            <td align="center" class="matters">
                            :
                        </td>
                            <td align="left" class="matters" id="tdStudidBox" runat="server"><asp:TextBox ID="txtStuID" runat="server" Width="100px" ></asp:TextBox></td>
                            
                            <td align="left" class="matters" id="tdStuName" runat="server"><asp:Label ID="Label2" runat="server" Text="Student Name"></asp:Label></td>
                            <td align="center" class="matters">
                            :
                        </td>
                            <td align="left" class="matters" id="tdStudNameBox" runat="server"><asp:TextBox ID="txtStuName" runat="server" Width="100px" ></asp:TextBox>
                        </td>--%>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Reference Number" CssClass="field-label"></asp:Label></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRef" runat="server"></asp:TextBox></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGet" runat="server" CssClass="button" Text="Get Transport Requests"
                                            CausesValidation="True" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                                <tr id="tr2">
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Requests with the specified parameters"
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Ref No">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblh1" runat="server" Text="Ref No."></asp:Label>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRefNo" runat="server" Text='<%# Bind("BBT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Application No">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblApplNo" runat="server" Text="Application Number"></asp:Label>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAppl" runat="server" Text='<%# Bind("EQS_APPLNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblStuID" runat="server" Text="Student ID"></asp:Label>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudNo" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <HeaderTemplate>
                                                        Name
                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">

                                                    <HeaderTemplate>
                                                        Grade
                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuGrade" Style="text-align: center" runat="server" Text='<%# Bind("GRADE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stage">
                                                    <HeaderTemplate>
                                                        Stage
                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStage" runat="server" Text='<%# Bind("STAGE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Request Status">
                                                    <HeaderTemplate>
                                                        Request Status
                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReqStatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Studentid">
                                                    <HeaderTemplate>
                                                        Student ID
                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudentid" runat="server" Text='<%# Bind("STUDENT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtnView" CommandName="Edit" runat="server" Text="Edit" OnClick="lnkbtnView_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtnViewBtn" CommandName="Edit" runat="server" Text="View" OnClick="lnkbtnViewBtn_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Auto Approval Status">
                                                    <HeaderTemplate>
                                                        Auto Approval Status
                                                               <br />
                                                        <asp:DropDownList ID="ddlgvChkStatus" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvChkStatus_SelectedIndexChanged">
                                                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Auto Approval" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Not Auto Approval" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgChk" runat="server" ImageUrl='<%# getChkImage(Eval("CHKAPRSTATUS")) %>'></asp:Image>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Save Status">
                                                    <HeaderTemplate>
                                                        Save Status
                                                               <br />
                                                        <asp:DropDownList ID="ddlgvSaveStatus" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvSaveStatus_SelectedIndexChanged">
                                                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Saved" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Not Saved" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgSave" runat="server" ImageUrl='<%# getSaveImage(Eval("SAVED")) %>'></asp:Image>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" style="height: 52px; width: 540px;" valign="bottom">
                            <asp:HiddenField ID="hfGRM_ID" runat="server" />
                            <asp:HiddenField ID="hfAcdID" runat="server" />
                            <asp:HiddenField ID="hfBsuID" runat="server" />
                            <asp:HiddenField ID="hfPSBL_ID" runat="server" />
                            <asp:HiddenField ID="hfDSBL_ID" runat="server" />
                            <asp:HiddenField ID="hfPPNT_ID" runat="server" />
                            <asp:HiddenField ID="hfDPNT_ID" runat="server" />
                            <asp:HiddenField ID="hfSave" runat="server" />
                            <asp:HiddenField ID="hfBBT_ID" runat="server" />
                            <asp:HiddenField ID="hfSHF_ID" runat="server" />
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfREQ_Type" runat="server" />
                            <asp:HiddenField ID="hfStudentID" runat="server" />
                            <asp:HiddenField ID="hfOnwardTripId" runat="server" />
                            <asp:HiddenField ID="hfReturnTripId" runat="server" />
                            <asp:HiddenField ID="hfViewBtn" runat="server" />
                            <asp:HiddenField ID="hfApprovePrintStatus" runat="server" />
                            <asp:HiddenField ID="hfEQS_Id" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlDetails" runat="server" BackColor="white" CssClass="panel-cover"
                                Style="width: 750px; height: 600px; vertical-align: middle; z-index: -1 !important;"
                                ScrollBars="Vertical">
                                <table style="width: 750; height: 600px;" border="0" cellpadding="0" cellspacing="0" id="tblpopUp" runat="server">
                                    <tr class="title-bg-lite">
                                        <td align="left" colspan="4">Transport Request Approval
                                        </td>
                                    </tr>
                                    <tr id="trSummativeError" runat="server">
                                        <td align="left" colspan="4">
                                            <asp:Label ID="lblFinalError" Text="" runat="server" CssClass="error"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr id="trPArea" runat="server">
                                        <td align="left" colspan="4">
                                            <asp:Label ID="pickupAreaError" Text="Please select a pickup area" runat="server" CssClass="error"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr id="trPPoint" runat="server">
                                        <td align="left" colspan="4">
                                            <asp:Label ID="pickupPointError" Text="Please select a pickup point" runat="server" CssClass="error"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr id="trDArea" runat="server">
                                        <td align="left" colspan="4">
                                            <asp:Label ID="dropoffAreaError" Text="Please select a dropoff area" runat="server" CssClass="error"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr id="trDPoint" runat="server">
                                        <td align="left" colspan="4">
                                            <asp:Label ID="dropoffPointError" Text="Please select a dropoff point" runat="server" CssClass="error"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <%--<tr id="trRemarks" runat="server">
            <td align="left" class="matters" colspan="4">
                 <asp:Label ID="lblRemarksError" Text="Please enter the remarks" runat="server" CssClass="error" ></asp:Label><br />
            </td>
            </tr>--%>

                                    <tr id="trSaveOk" runat="server">
                                        <td align="left" colspan="4">
                                            <asp:Label ID="lblSaveOk" Text="The data has been saved successfully" runat="server" CssClass="error"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Start Date</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtFrom" runat="server"> </asp:TextBox>
                                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                                                TabIndex="4" />

                                        </td>
                                        <td align="left" width="20%"></td>
                                        <td align="left" width="30%"></td>
                                    </tr>
                                    <tr id="trPopupName" runat="server">
                                        <td align="left" width="20%">
                                            <asp:Label ID="lblPopupId" runat="server" Text="" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:Label ID="lblAppNo" runat="server" Text="" CssClass="field-value"></asp:Label>
                                        </td>
                                        <td align="left" id="tdAppPhotoText" rowspan="4" runat="server" width="20%">
                                            <asp:Label ID="lblStuPhoto" runat="server" Text="" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" id="tdAppPhoto" rowspan="4" runat="server" width="30%">
                                            <asp:Image ID="imgEmpImage"
                                                Height="105px" runat="server" ImageUrl="~/Images/Photos/no_image.gif"
                                                Width="106px" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="left" width="20%">
                                            <asp:Label ID="lblPopupName" runat="server" Text="" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:Label ID="lblAppName" runat="server" Text="" CssClass="field-value"></asp:Label>
                                        </td>
                                    </tr>
                                    <%--<tr id="trPopupName1" runat="server">
                <td class="matters" align="left">
                   Student ID
                </td>
                <td align="left" class="matters">
                    <asp:Label ID="lblPopupStuId" runat="server" Text=""></asp:Label>
                </td>
                <td class="matters" align="left">
                    Student Name
                </td>
                <td align="left" class="matters">
                    <asp:Label ID="lblPopUpStuName" runat="server" Text=""></asp:Label>
                </td>
            </tr>--%>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Reference Number</span>
                                        </td>
                                        <td align="left" runat="server" width="30%">
                                            <asp:Label ID="lblReference" runat="server" Text="" CssClass="field-value"></asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label4" runat="server" Text="Area Description" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblAreaDescription" runat="server" CssClass="field-value"></asp:Label></td>

                                    </tr>



                                    <tr class="title-bg-lite">
                                        <td align="left" colspan="4">Pickup Details &nbsp; &nbsp; &nbsp; &nbsp;
                        <%--<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" ForeColor="White"
                            OnClientClick="javascript:return DShowSeats();" Text="Seat Capacity" Width="111px">
                        </asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Location</span>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblPlocation" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                        <td align="left"><span class="field-label">Trip Type</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlTripType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTripType_SelectedIndexChanged">
                                                <asp:ListItem Text="--" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Round Trip" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Pickup Only" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Dropoff Only" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Area</span><span class="text-danger">*</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlPickupArea" runat="server" AutoPostBack="True">
                                            </asp:DropDownList><br />
                                            <asp:Label ID="lblPickupArea" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="left"><span class="field-label">PickupPoint</span><span class="text-danger">*</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlPickupPoint" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                            <br />
                                            <asp:LinkButton ID="lnkPickup" runat="server" OnClientClick="javascript: PAddtoTrip(); return false;">Add to Trip</asp:LinkButton>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="field-label">Trip</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlOnward" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td id="tdOnward" align="left" runat="server">
                                            <asp:GridView ID="gvOnward" runat="server" EmptyDataText="No Records" AutoGenerateColumns="false" Width="100%" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Capacity">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Available">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle></HeaderStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                                <SelectedRowStyle></SelectedRowStyle>
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                            </asp:GridView>
                                        </td>
                                        <td id="tdEnqOnward" align="left" runat="server">
                                            <asp:GridView ID="gvEnqOnward" runat="server" Width="100%" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seats Alloted">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("SEAT_ALLOTED") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle></HeaderStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                                <SelectedRowStyle></SelectedRowStyle>
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="title-bg-lite">
                                        <td align="left" colspan="4">Drop Off Details &nbsp; &nbsp; &nbsp; &nbsp;
                        <%--<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" ForeColor="White"
                            OnClientClick="javascript:return DShowSeats();" Text="Seat Capacity" Width="111px">
                        </asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label9" runat="server" Text="Location" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblDlocation" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                        <td align="left" id="chkAutoDetails" runat="server"><span class="field-label">Auto Approval During Enrollment</span>
                                        </td>
                                        <td align="left">
                                            <asp:CheckBox ID="chkAutoApprove" runat="server" Checked="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Dropoff Area</span><span class="text-danger">*</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlDropoffArea" runat="server" AutoPostBack="True">
                                            </asp:DropDownList><br />
                                            <asp:Label ID="lblDropoffArea" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="left"><span class="field-label">DropoffPoint</span><span class="text-danger">*</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlDropoffPoint" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                            <br />
                                            <asp:LinkButton ID="lnkDropOff" runat="server" OnClientClick="javascript: DAddtoTrip(); return false;">Add to Trip</asp:LinkButton>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left"><span class="field-label">Trip</span>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlReturn" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td id="tdReturn" align="left" runat="server">
                                            <asp:GridView ID="gvReturn" runat="server" Width="100%" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Capacity">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Available">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle></HeaderStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                                <SelectedRowStyle></SelectedRowStyle>
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                            </asp:GridView>
                                        </td>
                                        <td id="tdEnqReturn" align="left" runat="server">
                                            <asp:GridView ID="gvEnqReturn" runat="server" Width="100%" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seats Allotted">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("SEAT_ALLOTED") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle></HeaderStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                                <SelectedRowStyle></SelectedRowStyle>
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                            </asp:GridView>
                                        </td>
                                    </tr>

                                    <tr class="title-bg-lite">
                                        <td align="left" colspan="4">Approval Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Remarks</span>
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" OnClick="btnEdit_Click" />
                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" OnClick="btnApprove_Click" />
                                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
                                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    cs
                </table>
                <ajaxToolkit:ModalPopupExtender ID="MPViewRequests" runat="server" BackgroundCssClass="popfunc" 
                    DropShadow="true" PopupControlID="pnlDetails" RepositionMode="RepositionOnWindowResizeAndScroll"
                    TargetControlID="lblPopUp">
                </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar" TargetControlID="txtFrom" PopupButtonID="imgFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>



    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>
