Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Partial Class Students_studTPTEdit_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100190") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    ViewState("datamode") = "edit"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfSHF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))
                    lblSEN.Text = Encr_decrData.Decrypt(Request.QueryString("sen").Replace(" ", "+"))
                    lblName.Text = Encr_decrData.Decrypt(Request.QueryString("name").Replace(" ", "+"))
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))

                    GetData()



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else

            AddTitle()
        End If
    End Sub
#Region "Private Methods"

    Sub GetData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  distinct  isnull(B.LOC_DESCRIPTION,''),isnull(D.SBL_DESCRIPTION,''),isnull(C.PNT_ID,0)," _
                                 & " isnull(E.LOC_DESCRIPTION,''),isnull(F.SBL_DESCRIPTION,''),isnull(G.PNT_ID,0)," _
                                 & " isnull(L.TRD_ID,0),isnull(M.TRD_ID,0),isnull(A.STU_ACD_ID,0),isnull(D.SBL_ID,0),isnull(F.SBL_ID,0)" _
                                 & " FROM  STUDENT_M AS A " _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON A.STU_SBL_ID_PICKUP = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = A.STU_SBL_ID_DROPOFF" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
                                 & " LEFT OUTER JOIN TRANSPORT.TRIPS_D AS L ON A.STU_PICKUP_TRP_ID=L.TRD_TRP_ID AND L.TRD_TODATE IS NULL" _
                                 & " LEFT OUTER JOIN TRANSPORT.TRIPS_D AS M ON A.STU_DROPOFF_TRP_ID=M.TRD_TRP_ID AND M.TRD_TODATE IS NULL" _
                                 & " WHERE STU_ID =" + hfSTU_ID.Value.ToString


       
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim lis(9) As Object
        While reader.Read

            lis(0) = reader.GetString(0)
            lis(1) = reader.GetString(1)
            lis(2) = reader.GetValue(2)
            lis(3) = reader.GetString(3)
            lis(4) = reader.GetString(4)
            lis(5) = reader.GetValue(5)
            lis(6) = reader.GetValue(6)
            lis(7) = reader.GetValue(7)
            hfACD_ID.Value = reader.GetValue(8)
            lis(8) = reader.GetValue(9)
            lis(9) = reader.GetValue(10)
        End While


        hfPSBL_ID.Value = lis(8)
        hfDSBL_ID.Value = lis(9)
        reader.Close()


              txtPLocation.Text = lis(0)
        txtPSubLocation.Text = lis(1)

        txtDLocation.Text = lis(3)
        txtDSubLocation.Text = lis(4)



        BindTrip(ddlonward, "Onward", lis(8))
        If lis(6) <> 0 And ddlonward.Items.Count <> 1 And Not ddlonward.Items.FindByValue(CType(lis(6), Integer)) Is Nothing Then
            ddlonward.Items.FindByValue(CType(lis(6), Integer)).Selected = True
        End If

        BindTrip(ddlReturn, "Return", lis(9))
        If lis(7) <> 0 And ddlReturn.Items.Count <> 1 And Not ddlReturn.Items.FindByValue(CType(lis(7), Integer)) Is Nothing Then
            ddlReturn.Items.FindByValue(CType(lis(7), Integer)).Selected = True
        End If


        ddlPPickUp = BindPickUp(lis(8), ddlPPickUp, lis(6))
        If Not ddlPPickUp.Items.FindByValue(CType(lis(2), Integer)) Is Nothing Then
            ddlPPickUp.Items.FindByValue(CType(lis(2), Integer)).Selected = True
        End If

        ddlDPickUp = BindPickUp(lis(9), ddlDPickUp, lis(7))
        If Not ddlDPickUp.Items.FindByValue(CType(lis(5), Integer)) Is Nothing Then
            ddlDPickUp.Items.FindByValue(CType(lis(5), Integer)).Selected = True
        End If


        If ddlonward.Items.Count <> 0 Then
            BindCapacity(gvOnward, ddlonward.SelectedValue.ToString, "Onward")
        End If
        If ddlReturn.Items.Count <> 0 Then
            BindCapacity(gvReturn, ddlReturn.SelectedValue.ToString, "Return")
        End If

        hfPbusNo.Value = ddlonward.SelectedItem.Text
        hfDbusNo.Value = ddlReturn.SelectedItem.Text

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function BindLocation(ByVal ddlLocation As DropDownList)
        ddlLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT DISTINCT loc_description,loc_id FROM TRANSPORT.location_m AS A " _
                                & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.LOC_ID=B.PNT_LOC_ID " _
                                & " WHERE PNT_bsu_id='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "loc_description"
        ddlLocation.DataValueField = "loc_id"
        ddlLocation.DataBind()
        Return ddlLocation
    End Function

    Function BindSubLocation(ByVal ddlLocation As DropDownList, ByVal ddlSubLocation As DropDownList)
        ddlSubLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT DISTINCT sbl_description,sbl_id FROM TRANSPORT.sublocation_m AS A " _
                                 & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                & " WHERE pnt_bsu_id='" + Session("sbsuid") + "' and sbl_loc_id=" + ddlLocation.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlSubLocation.DataSource = ds
        ddlSubLocation.DataTextField = "sbl_description"
        ddlSubLocation.DataValueField = "sbl_id"
        ddlSubLocation.DataBind()
        Return ddlSubLocation
    End Function

    Function BindPickUp(ByVal sblid As Object, ByVal ddlPickUp As DropDownList, ByVal trdid As String)
        ddlPickUp.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT pnt_description,pnt_id FROM TRANSPORT.pickuppoints_M AS A" _
                               & " INNER JOIN TRANSPORT.TRIPS_PICKUP_S AS B ON A.PNT_ID=B.TPP_PNT_ID " _
                               & " WHERE pnt_bsu_id='" + Session("sbsuid") + "' and pnt_sbl_id=" + sblid.ToString _
                               & " AND TPP_TRD_ID=" + trdid
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlPickUp.DataSource = ds
        ddlPickUp.DataTextField = "pnt_description"
        ddlPickUp.DataValueField = "pnt_id"
        ddlPickUp.DataBind()
        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"
        ddlPickUp.Items.Insert(0, li)

        li = New ListItem

        'For Each li In ddlPickUp.Items
        '    li.Attributes.Add("title", li.Text)
        'Next
        Return ddlPickUp
    End Function

    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String, ByVal sblId As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_SBL_ID=" + sblId _
                        & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_SBL_ID=" + sblId _
                 & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL"



        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"
        ddlTrip.Items.Insert(0, li)
        AddTitle()
    End Sub

  
    'Sub SaveData()

    '    'If ddlonward.SelectedItem.Text.Trim <> hfPbusNo.Value.Trim Then
    '    '    Dim lblPAvailable As Label = gvOnward.Rows(0).FindControl("lblAvailable")
    '    '    If Val(lblPAvailable.Text) = 0 Then
    '    '        lblError.Text = "There are no seats available in " + ddlonward.SelectedItem.Text
    '    '        Exit Sub
    '    '    End If
    '    'End If

    '    'If ddlReturn.SelectedItem.Text.Trim <> hfDbusNo.Value.Trim Then
    '    '    Dim lblDAvailable As Label = gvOnward.Rows(0).FindControl("lblAvailable")
    '    '    If Val(lblDAvailable.Text) = 0 Then
    '    '        lblError.Text = "There are no seats available in " + ddlReturn.SelectedItem.Text
    '    '        Exit Sub
    '    '    End If
    '    'End If

    '    Dim str_query As String
    '    '           str_query= "exec studTRANSPORTEDIT " + hfSTU_ID.Value.ToString + "," _
    '    '                                            & ddlonward.SelectedValue + "," _
    '    '                                            & "'" + ddlonward.SelectedItem.Text + "'," _
    '    '                                            & ddlPPickUp.SelectedValue + "," _
    '    '                                            & ddlDbusNo.SelectedValue + "," _
    '    '                                            & "'" + ddlDbusNo.SelectedItem.Text + "'," _
    '    '                                            & ddlDPickUp.SelectedValue + "," _
    '    '                                            & "'" + Session("sbsuid") + "'," _
    '    '                                            & hfACD_ID.Value.ToString + "," _
    '    '                                            & txtRate.Text
    '    Dim transaction As SqlTransaction
    '    Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
    '        transaction = conn.BeginTransaction("SampleTransaction")
    '        Try
    '            UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)

    '            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

    '            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + hfSTU_ID.Value.ToString + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
    '            If flagAudit <> 0 Then
    '                Throw New ArgumentException("Could not process your request")
    '            End If
    '            lblError.Text = "Record Saved Successfully"
    '            transaction.Commit()
    '        Catch myex As ArgumentException
    '            transaction.Rollback()
    '            lblError.Text = myex.Message
    '            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Catch ex As Exception
    '            transaction.Rollback()
    '            lblError.Text = "Record could not be Saved"
    '            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        End Try
    '    End Using
    'End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String

        Dim journey As String
        Dim trdPickup As String = ""
        Dim trdDropOff As String = ""

        If ddlonward.SelectedValue <> "0" And ddlReturn.SelectedValue <> "0" Then
            journey = "ALL"
            trdPickup = ddlonward.SelectedValue.ToString
            trdDropOff = ddlReturn.SelectedValue.ToString
        ElseIf ddlonward.SelectedValue <> "0" Then
            journey = "Onward"
            trdPickup = ddlonward.SelectedValue.ToString
            trdDropOff = "NULL"
        ElseIf ddlReturn.SelectedValue <> "0" Then
            journey = "Return"
            trdPickup = "NULL"
            trdDropOff = ddlReturn.SelectedValue.ToString
        Else
            journey = "ALL"
            trdPickup = "NULL"
            trdDropOff = "NULL"
        End If

        str_query = "exec TRANSPORT.saveSTUDTRANSPORT " _
                  & trdPickup + "," _
                  & trdDropOff + "," _
                  & "'" + journey + "'," _
                  & hfSTU_ID.Value + "," _
                  & ddlPPickUp.SelectedValue.ToString + "," _
                  & ddlDPickUp.SelectedValue.ToString + "," _
                  & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblError.Text = "Record saved successfully"
    End Sub

    Sub AddTitle()
        Dim li As New ListItem

        'For Each li In ddlPPickUp.Items
        '    li.Attributes.Add("title", li.Text)
        'Next

        'li = New ListItem
        'For Each li In ddlDPickUp.Items
        '    li.Attributes.Add("title", li.Text)
        'Next

        li = New ListItem
        For Each li In ddlonward.Items
            li.Attributes.Add("title", li.Text)
        Next

        li = New ListItem
        For Each li In ddlReturn.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub


    Sub BindCapacity(ByVal gvCap As GridView, ByVal trdId As String, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.getSEATCAPACITY " _
                                & "'" + Session("sbsuid") + "'," _
                                & trdId + "," _
                                & hfACD_ID.Value + "," _
                                & Session("Current_ACY_ID") + "," _
                                & "'" + journey + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvCap.DataSource = ds
        gvCap.DataBind()
    End Sub

    Sub CallReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptStudTransportInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
        ' LoadReports()
    End Sub

    'Sub LoadReports()
    '    Try


    '        Dim rs As New reportdocument
    '        Dim rptClass As New rptClass
    '        Dim iRpt As New DictionaryEntry
    '        Dim i As Integer
    '        Dim newWindow As String




    '        rptClass = Session("rptClass")


    '        Dim crParameterDiscreteValue As ParameterDiscreteValue
    '        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    '        Dim crParameterFieldLocation As ParameterFieldDefinition
    '        Dim crParameterValues As ParameterValues

    '        With rptClass




    '            'If Not Session("prevClass") Is Nothing And newWindow Is Nothing Then
    '            '    Session("rptClass") = Session("prevClass")
    '            '    rptClass = Session("prevClass")
    '            '    Session("prevClass") = Nothing
    '            '    Page_Load(sender, e)
    '            '    Exit Sub
    '            'End If

    '            rs.Load(.reportPath)
    '            rs.DataSourceConnections(0).SetConnection(.crInstanceName, .crDatabase, .crUser, .crPassword) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
    '            rs.SetDatabaseLogon(.crUser, .crPassword, .crInstanceName, .crDatabase, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '
    '            rs.VerifyDatabase()

    '            'Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
    '            'myConnectionInfo.ServerName = .crInstanceName
    '            'myConnectionInfo.DatabaseName = .crDatabase
    '            'myConnectionInfo.UserID = .crUser
    '            'myConnectionInfo.Password = .crPassword


    '            'SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
    '            'SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


    '            'If .subReportCount <> 0 Then
    '            '    For i = 0 To .subReportCount - 1
    '            '        rs.ReportDocument.Subreports(i).VerifyDatabase()
    '            '    Next
    '            'End If

    '            crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
    '            If .reportParameters.Count <> 0 Then
    '                For Each iRpt In .reportParameters
    '                    crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
    '                    crParameterValues = crParameterFieldLocation.CurrentValues
    '                    crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
    '                    crParameterDiscreteValue.Value = iRpt.Value
    '                    crParameterValues.Add(crParameterDiscreteValue)
    '                    crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
    '                Next
    '            End If



    '            If .selectionFormula <> "" Then
    '                rs.RecordSelectionFormula = .selectionFormula
    '            End If

    '            'Dim filepath As String = Server.MapPath("~\studTPTEdit_M.aspx")
    '            'filepath = filepath.Replace("studTPTEdit_M.aspx", lblSEN.Text + ".htm")
    '            'rs.ExportToDisk(ExportFormatType.HTML40, filepath)

    '        End With
    '    Catch ex As Exception


    '    End Try
    'End Sub

    Sub CallReqReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptTransportReqInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function


    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

#End Region

    Protected Sub ddlPPickUp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPPickUp.SelectedIndexChanged
        'Try
        '    BindTrip(ddlonward, "Onward")
        '    If ddlonward.Items.Count <> 0 Then
        '        BindCapacity(gvOnward, ddlonward.SelectedValue.ToString, "Onward")
        '    End If
        '    AddTitle()


        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    lblError.Text = "Request could not be processed"
        'End Try
    End Sub

    Protected Sub ddlDPickUp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDPickUp.SelectedIndexChanged
        'Try
        '    BindTrip(ddlReturn, "Return")
        '    If ddlReturn.Items.Count <> 0 Then
        '        BindCapacity(gvReturn, ddlReturn.SelectedValue.ToString, "Return")
        '    End If

        '    AddTitle()
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    lblError.Text = "Request could not be processed"
        'End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlPBusNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlonward.SelectedIndexChanged
        Try
            BindCapacity(gvOnward, ddlonward.SelectedValue.ToString, "Onward")
            BindPickUp(hfPSBL_ID.Value, ddlPPickUp, ddlonward.SelectedValue.ToString)
            AddTitle()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlDbusNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReturn.SelectedIndexChanged
        Try
            BindCapacity(gvReturn, ddlReturn.SelectedValue.ToString, "Return")
            BindPickUp(hfDSBL_ID.Value, ddlDPickUp, ddlReturn.SelectedValue.ToString)
            AddTitle()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If ddlonward.SelectedValue = "0" And ddlReturn.SelectedValue = "0" Then
            CallReqReport(hfSTU_ID.Value)
        Else
            CallReport(hfSTU_ID.Value)
        End If

    End Sub

  
    Protected Sub lnkPickup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPickup.Click
        BindPickUp(hfPSBL_ID.Value, ddlPPickUp, ddlonward.SelectedValue.ToString)
        BindPickUp(hfDSBL_ID.Value, ddlDPickUp, ddlReturn.SelectedValue.ToString)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
