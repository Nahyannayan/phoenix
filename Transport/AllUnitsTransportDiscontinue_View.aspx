﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AllUnitsTransportDiscontinue_View.aspx.vb" Inherits="Transport_AllUnitsTransportDiscontinue_View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<script runat="server">

    Protected Sub btnDetail_Click(sender As Object, e As EventArgs)

    End Sub
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script type="text/javascript" language="javascript">
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1 && document.forms[0].elements[i].disabled == false) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gdNEw.ClientID%>");
            if (color == '') {
                // color = getRowColor();
            }
            if (obj.checked) {
                // rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
        }
        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("ChkSelAll").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }


        function ViewDetail(url, text, w, h) {
            var sFeatures;
            sFeatures = "dialogWidth: 625px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen(url, "pop_up")
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //return false;
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
           
            <asp:Label ID="lblHead" runat="server" Text=" Approval For Discontinuation Of Transport- All Units"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server" Visible="False">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                     <tr>
                        <td align="left" width="20%"><span class="field-label">Select Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server"  DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True" SkinID="DropDownListNormal"
                                TabIndex="5">
                            </asp:DropDownList>
              
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                   <tr>
                      <td class="matters" align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server"   AutoPostBack="True">
                            </asp:DropDownList></td>
                   </tr>
                    <tr valign="top">

                        <td align="left" valign="middle" width="10%" display="none"><span class="field-label"> </span></td>
                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlTopFilter" runat="server" AutoPostBack="True" SkinID="DropDownListNormal" Visible="false"
                                OnSelectedIndexChanged="ddlCount_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Value="10">Last 10</asp:ListItem>
                                <asp:ListItem Value="25">Last 50</asp:ListItem>
                                <asp:ListItem Value="50">Last 100</asp:ListItem>
                                <asp:ListItem>All</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox id="txtFrom" runat="server" AutoCompleteType="Disabled" Width="112px" Visible="false"></asp:TextBox>
                        </td>
                        <td align="right" width="60%">
                            <asp:RadioButtonList ID="rblFilter" RepeatDirection="Horizontal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblFilter_SelectedIndexChanged" Visible="false">
                                <asp:ListItem Value="PWU">Pending @ Working Unit</asp:ListItem>
                                <asp:ListItem Selected="True" Value="PND">Pending</asp:ListItem>
                                <asp:ListItem Value="APD">Approved</asp:ListItem>
                                <asp:ListItem Value="RJD">Rejected</asp:ListItem>
                                <asp:ListItem Value="ALL">All</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>

                        
                    </tr>
                    <tr>
                    
                        <td align="center" valign="top" colspan="3">
                            <asp:GridView ID="gdNEw" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" PageSize="25" OnRowCommand="gdNEw_RowCommand" >
                                <Columns>
                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                       Select
                                                                    <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Select one" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                              
                                    <asp:TemplateField HeaderText="StudentID">
                                        <HeaderTemplate>
                                            Student#<br />
                                            <asp:TextBox ID="txtStuno" runat="server"  Width="75%" ></asp:TextBox>
                                            <asp:ImageButton ID="btnStunoSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student">
                                        <HeaderTemplate>
                                            Stud.Name
                                            <br />
                                            <asp:TextBox ID="txtStudName" runat="server"  Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnStunameSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuname" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade
                                            <br />
                                            <asp:TextBox ID="txtGrade" runat="server"  Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("STU_GRD_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stud.BSU">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstubsu" runat="server" Text='<%# Bind("BSU_SHORTNAME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU"  Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblparents" runat="server" Text='<%# Bind("BSU_SHORTNAME")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consn.From" Visible='false'>
                               
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consn.To" Visible='false'>
                                    
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gross" Visible='false'>
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblgross" runat="server" Text='<%# Bind("SCD_GROSS_AMT") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Concession" Visible='false'>
                                        <ItemTemplate>
                                            <asp:Label ID="lblconcession" runat="server" Text='<%# Bind("SCD_CONCESSION_AMT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField> 
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbDetail" OnClick="lbDetail_Click" runat="server" >Details</asp:LinkButton>
                                            <%--<asp:HiddenField ID="hf_EMPID" runat="server" Value='<%# Bind("SCR_REF_EMP_ID") %>' />--%>
                                            <asp:HiddenField ID="hf_STUID" runat="server" Value='<%# Bind("STU_ID")%>' />
                                            <asp:HiddenField ID="hf_STUNAME" runat="server" Value='<%# Bind("STU_NAME")%>'/>
                                            <asp:HiddenField ID="hf_STNO" runat="server"  Value='<%# Bind("STU_NO")%>'/>
                                            <asp:HiddenField ID="hf_Grade" runat="server" Value='<%# Bind("STU_GRD_ID")%>'/>
                                            <asp:HiddenField ID="hf_Section" runat="server"  Value='<%# Bind("SCT_DESCR")%>'/>
                                            
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>



                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class ="field-label">Comments  </span></td>
                        <td align="left">
                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" SkinID="MultiText"
                     runat="server"></asp:TextBox>
                            <asp:HiddenField ID="hfType" runat="server" />
                            <asp:Label ID="lblFrom" runat="server" visible="false"></asp:Label>
                        </td>
                        <td></td>
                        
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="3">
                            <asp:Label ID="lblAlert" runat="server" CssClass="text-danger"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="3">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve"  Visible="false"/>
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" Visible="false"/>
                            <asp:Button ID="btnApproveNew" runat="server" CssClass="button" Text="Approve" />
                            <asp:Button ID="btnRejectnew" runat="server" CssClass="button" Text="Reject" />
                        </td>
                    </tr>
                </table>


                <asp:HiddenField ID="hfSTD_ID" runat="server" EnableViewState="False" />
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />

            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

