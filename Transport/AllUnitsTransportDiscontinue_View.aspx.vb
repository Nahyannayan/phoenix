﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Transport_AllUnitsTransportDiscontinue_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Public Property bTRANSPORT() As Boolean
        Get
            Return ViewState("bTRANSPORT")
        End Get
        Set(value As Boolean)
            ViewState("bTRANSPORT") = value
        End Set
    End Property
    Public Property bMatchNames() As Boolean
        Get
            Return ViewState("bMatchNames")
        End Get
        Set(value As Boolean)
            ViewState("bMatchNames") = value
        End Set
    End Property



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            gdNEw.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim bsuid As String = "900500"
                Dim CurBsUnit As String = Session("sBsuid")
               ' Session("sBsuid") = bsuid.ToString  'BBT
                '  Session("sBsuid") = "900501"  'STS
                Dim USR_NAME As String = Session("sUsr_name")
                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000085" And ViewState("MainMnu_code") <> "F733034") Then
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200491") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "H000085"
                            lblHead.Text = "Approve Fee Concession Request"
                            bTRANSPORT = False
                            rblFilter.Items(0).Text = ""
                            rblFilter.Items(0).Attributes.CssStyle.Add("visibility", "hidden")
                        Case "F733034"
                            lblHead.Text = "Approve Staff Fee Concession Request"
                            bTRANSPORT = True
                    End Select
                    Me.btnApprove.Enabled = False
                    Me.btnReject.Enabled = False
                    bMatchNames = True
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), CStr(Session("sBsuid")))
                    ' ddlBusinessunit.DataBind()
                    'If Not ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")) Is Nothing Then
                    '    ddlBusinessunit.ClearSelection()
                    '    ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")).Selected = True
                    'End If
                    Call BindBusinessUnit()
                    Call FillACD()
                    'If Not ddlBusinessunit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                    '    ddlBusinessunit.ClearSelection()
                    '    ddlBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
                    'End If
                    gridbind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Sub BindBusinessUnit()
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString)

            Dim cmd As New SqlCommand("dbo.BusinessUnitsFeesSearch", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlBusinessunit.DataSource = dr
                    ddlBusinessunit.DataTextField = "BSU_NAME"
                    ddlBusinessunit.DataValueField = "BSU_ID"

                    ddlBusinessunit.DataBind()
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlBusinessunit.Items.Insert(0, li)
                    ddlBusinessunit.SelectedValue = CStr(Session("sBsuid"))

                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Private Sub SetNameMismatchAlert()
        'If gvJournal.Rows.Count <= 0 Then
        '    Me.lblAlert.Text = ""
        'ElseIf bMatchNames = False Then
        '    Me.lblAlert.Text = "Highlighted are Concession(s) having mismatch in Employee name and Parent name of Child, Please verify before approval"
        'Else
        '    Me.lblAlert.Text = ""
        'End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        'If gvJournal.Rows.Count > 0 Then
        '    Dim s As HtmlControls.HtmlImage
        '    Dim pControl As String

        '    pControl = pImg
        '    Try
        '        s = gvJournal.HeaderRow.FindControl(pControl)
        '        If p_imgsrc <> "" Then
        '            s.Src = p_imgsrc
        '        End If
        '        Return s.ClientID
        '    Catch ex As Exception
        '        Return ""
        '    End Try
        'End If
        Return ""
    End Function

    Protected Sub rblFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Sub gridbind()
        '  Try
        Dim str_Sql As String
        Dim ds As New DataSet
        Dim str_Filter As String = ""

        Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox

        lstrCondn1 = ""
        lstrCondn2 = ""
        lstrCondn3 = ""
        lstrCondn4 = ""
        lstrCondn5 = ""
        lstrCondn6 = ""
        lstrCondn7 = ""
        lstrCondn8 = ""
        str_Filter = ""
        Select Case ViewState("MainMnu_code").ToString
            Case "H000085"
                rblFilter.Items(0).Text = ""
                rblFilter.Items(0).Attributes.CssStyle.Add("visibility", "hidden")
        End Select

        Try
            'If gvJournal.Rows.Count > 0 Then
            ' Dim txtSchstudname As New TextBox
          
            
         
            Dim txtStudName As TextBox = New TextBox
            txtStudName.Text = ""
            Dim txtStuno As TextBox = New TextBox
            txtStuno.Text = ""
            Dim txtGrade As TextBox = New TextBox
            txtGrade.Text = ""

            If not gdNEw Is Nothing And gdNEw.Rows.Count > 0 Then
                txtStudName = DirectCast(gdNEw.HeaderRow.FindControl("txtStudName"), TextBox)                
                txtStuno = DirectCast(gdNEw.HeaderRow.FindControl("txtStuno"), TextBox)              
                txtGrade = DirectCast(gdNEw.HeaderRow.FindControl("txtGrade"), TextBox)
               

                If txtStudName.Text <> "" Then
                Else
                    txtStudName.Text = ""
                End If
                If txtStuno.Text <> "" Then
                Else
                    txtStuno.Text = ""
                End If
                If txtGrade.Text <> "" Then
                Else
                    txtGrade.Text = ""
                End If

            End If
           


            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString)
            Dim Param(4) As SqlParameter
            Dim bsuunit As String
            If ddlBusinessunit.SelectedItem.Value = "" Then '
                bsuunit = ""
            Else
                bsuunit = ddlBusinessunit.SelectedItem.Value
            End If
            Dim acdid As Integer
            If ddlAcademicYear.Text = Nothing Or ddlAcademicYear.Text = "" Then
                acdid = 0
            Else
                acdid = ddlAcademicYear.SelectedItem.Value.ToString
            End If


            ' Dim bsuid As Integer = ddlBusinessunit.SelectedItem.Value.ToString
            Param(0) = Mainclass.CreateSqlParameter("@STU_NO", txtStuno.Text, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@STU_NAME", txtStudName.Text, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@GRD_DISPLAY", txtGrade.Text, SqlDbType.VarChar) 'studno /empno 
            Param(3) = Mainclass.CreateSqlParameter("@SVB_PROVIDER_BSU_ID", bsuunit, SqlDbType.VarChar)
            Param(4) = Mainclass.CreateSqlParameter("@ACD_ID", acdid, SqlDbType.Int)
            Dim DS1 As New DataSet
            Dim dt1 As New DataTable
            DS1 = SqlHelper.ExecuteDataset(sqlCon, "[dbo].Allunitsdiscontinue_search", Param)
            dt1 = DS1.Tables(0)
            gdNEw.DataSource = dt1
            gdNEw.DataBind()


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try






    End Sub

    Protected Sub gdNEw_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdNEw.PageIndexChanging
        gdNEw.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim gvRow As GridViewRow = CType(CType(sender, Control).Parent.Parent,  _
                                           GridViewRow)
            Dim index As Integer = gvRow.RowIndex
            Dim hf_STUID As HiddenField = TryCast(gvRow.Cells(0).FindControl("hf_STUID"), HiddenField)
            Dim hf_STUNAME As HiddenField = TryCast(gvRow.Cells(0).FindControl("hf_STUNAME"), HiddenField)
            Dim hf_STNO As HiddenField = TryCast(gvRow.Cells(0).FindControl("hf_STNO"), HiddenField)
            Dim hf_Grade As HiddenField = TryCast(gvRow.Cells(0).FindControl("hf_Grade"), HiddenField)
            Dim hf_Section As HiddenField = TryCast(gvRow.Cells(0).FindControl("hf_Section"), HiddenField)
            Dim url1 As String
            '  Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            'Dim hf_STUID As HiddenField = sender.Parent.parent.findcontrol("hf_STUID")
            ''        Dim lblSCR_DATE As Label = DirectCast(e.Row.FindControl("lblSCR_DATE"), Label)
            'Dim hf_STUNAME As HiddenField = sender.Parent.parent.findcontrol("hf_STUNAME")

            'Dim hf_STNO As HiddenField = sender.Parent.parent.findcontrol("hf_STNO")
            'Dim hf_Grade As HiddenField = sender.Parent.parent.findcontrol("hf_Grade")
            'Dim hf_Section As HiddenField = sender.Parent.parent.findcontrol("hf_Section")
            If ViewState("MainMnu_code") = "T200491" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url1 = String.Format("~\Transport\AllUnitsTranspoDiscontinueDetails.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(hf_STUID.Value) + "&stuno=" + Encr_decrData.Encrypt(hf_STNO.Value) + "&stunam=" + Encr_decrData.Encrypt(hf_STUNAME.Value) + "&grade=" + Encr_decrData.Encrypt(hf_Grade.Value) + "&section=" + Encr_decrData.Encrypt(hf_Section.Value), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "T000210" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                '  url = String.Format("~\Transport\studTransportDiscontinueApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text) + "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text), ViewState("MainMnu_code"), ViewState("datamode"))

            End If
            Response.Redirect(url1)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTopFilter.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub details_Command(sender As Object, e As CommandEventArgs)
        'Try



        '    ' Dim id As Integer = Int32.Parse(e.CommandArgument.ToString())
        '    Dim url As String

        '    Dim ID As Integer = Convert.ToInt32(e.CommandArgument)
        '    Dim selectedRow As GridViewRow = DirectCast(gdNEw.Rows(index), GridViewRow)
        '    Dim hf_STUID As HiddenField
        '    Dim hf_STUNAME As HiddenField
        '    Dim hf_STNO As HiddenField
        '    Dim hf_Grade As HiddenField
        '    Dim hf_Section As HiddenField

        '    With selectedRow
        '        hf_STUID = .FindControl("hf_STUID")
        '        hf_STUNAME = .FindControl("hf_STUNAME")
        '        hf_STNO = .FindControl("hf_STNO")
        '        hf_Grade = .FindControl("hf_Grade")
        '        hf_Section = .FindControl("hf_Section")

        '    End With
        '    If ViewState("MainMnu_code") = "T200491" Then
        '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        '        url = String.Format("~\Transport\AllUnitsTranspoDiscontinueDetails.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(hf_STUID.Value) + "&stuno=" + Encr_decrData.Encrypt(hf_STNO.Value) + "&stunam=" + Encr_decrData.Encrypt(hf_STUNAME.Value) + "&grade=" + Encr_decrData.Encrypt(hf_Grade.Value) + "&section=" + Encr_decrData.Encrypt(hf_Section.Value), ViewState("MainMnu_code"), ViewState("datamode"))
        '    ElseIf ViewState("MainMnu_code") = "T000210" Then
        '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        '        '  url = String.Format("~\Transport\studTransportDiscontinueApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text) + "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text), ViewState("MainMnu_code"), ViewState("datamode"))

        '    End If
        '    Response.Redirect(url)
        'Catch any As Exception
        '    Console.WriteLine(any.ToString())
        'End Try
    End Sub

    Protected Sub gdNEw_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gdNEw.RowCommand
        If e.CommandName = "Detail" Then
            Dim rowIndex As Integer = Convert.ToInt32(e.CommandArgument)

            'Reference the GridView Row.
            Dim row As GridViewRow = gdNEw.Rows(rowIndex)
            'Dim gvr As GridViewRow = DirectCast(DirectCast(e.CommandSource, Control).NamingContainer, GridViewRow)
            'Dim rowIndex As Integer = gvr.RowIndex

            ' Dim Cat_name As String = TryCast(gdNEw.Rows(rowIndex).FindControl("TxtName"), TextBox).Text





            Dim url As String

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gdNEw.Rows(index), GridViewRow)
            Dim hf_STUID As HiddenField
            Dim hf_STUNAME As HiddenField
            Dim hf_STNO As HiddenField
            Dim hf_Grade As HiddenField
            Dim hf_Section As HiddenField

            With selectedRow
                hf_STUID = .FindControl("hf_STUID")
                hf_STUNAME = .FindControl("hf_STUNAME")
                hf_STNO = .FindControl("hf_STNO")
                hf_Grade = .FindControl("hf_Grade")
                hf_Section = .FindControl("hf_Section")

            End With
            If ViewState("MainMnu_code") = "T200491" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Transport\AllUnitsTranspoDiscontinueDetails.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(hf_STUID.Value) + "&stuno=" + Encr_decrData.Encrypt(hf_STNO.Value) + "&stunam=" + Encr_decrData.Encrypt(hf_STUNAME.Value) + "&grade=" + Encr_decrData.Encrypt(hf_Grade.Value) + "&section=" + Encr_decrData.Encrypt(hf_Section.Value), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "T000210" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                '  url = String.Format("~\Transport\studTransportDiscontinueApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text) + "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text), ViewState("MainMnu_code"), ViewState("datamode"))

            End If
            Response.Redirect(url)
        End If
    End Sub

    

    Protected Sub gdNEw_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdNEw.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
           
           
        End If
    End Sub

    Private Sub SetConcessionDates(ByRef txtF As TextBox, ByRef txtT As TextBox, ByVal EMPID As Long, ByVal SSVID As Integer, ByVal EntryDt As String)
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("[FEES].[SP_GET_PERIOD_FOR_STAFF_CONCESSION_AVAILING]", ConnectionManger.GetOASISTransportConnection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim pParms(5) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@ENTRYDT", SqlDbType.DateTime)
            pParms(0).Value = EntryDt
            cmd.Parameters.Add(pParms(0))

            pParms(1) = New SqlClient.SqlParameter("@REF_ID", SqlDbType.Int)
            pParms(1).Value = EMPID
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@SSV_ID", SqlDbType.Int)
            pParms(2).Value = SSVID
            cmd.Parameters.Add(pParms(2))

            pParms(3) = New SqlClient.SqlParameter("@DT_FROM", SqlDbType.DateTime)
            pParms(3).Direction = ParameterDirection.Output
            cmd.Parameters.Add(pParms(3))

            pParms(4) = New SqlClient.SqlParameter("@DT_TO", SqlDbType.DateTime)
            pParms(4).Direction = ParameterDirection.Output
            cmd.Parameters.Add(pParms(4))

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            txtF.Text = IIf(txtF.Text = "", pParms(3).Value, txtF.Text)
            txtT.Text = IIf(txtT.Text = "", pParms(4).Value, txtT.Text)
        Catch
            'txtF.Text = String.Format("dd/MMM/yyyy", DateTime.Now.ToShortDateString)
            'txtT.Text = ""
        Finally

        End Try
    End Sub

    Private Function CompareEmpParent(ByVal EMPID As Int64, ByVal STUID As Int64) As Boolean
        CompareEmpParent = True

        Try
            Dim bMatch As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT dbo.fnCompEmployeeAndParent(" & EMPID & "," & STUID & ")")
            CompareEmpParent = bMatch
        Catch ex As Exception
            CompareEmpParent = False
        End Try


    End Function

    Private Function GetSelectedRows() As String
        GetSelectedRows = ""
        For Each gvr As GridViewRow In gdNEw.Rows
            Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            '    Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
            Dim hf_STUID As HiddenField = DirectCast(gvr.FindControl("hf_STUID"), HiddenField)
            If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                GetSelectedRows &= "|" & hf_STUID.Value
            End If
        Next
    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If GetSelectedRows() <> "" Then
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim bRolledBack As Boolean = False
            Dim bCommitted As Boolean = False
            Dim Trans As SqlTransaction '= objConn.BeginTransaction("APRVE")
            Try
                Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
                For Each gvr As GridViewRow In gdNEw.Rows

                    Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                    Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
                    Dim hf_SCDID As HiddenField = DirectCast(gvr.FindControl("hf_SCDID"), HiddenField)
                    Dim txtCnFrom As TextBox = DirectCast(gvr.FindControl("txtCnFrom"), TextBox)
                    Dim txtCnTo As TextBox = DirectCast(gvr.FindControl("txtCnTo"), TextBox)
                    If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                        Dim retval As String = "1"
                        If objcls.VALIDATE_CONCESSION_REQUEST(hf_SCDID.Value, "") = True Then
                            Trans = objConn.BeginTransaction("APVE")
                            If bTRANSPORT AndAlso (Not IsDate(txtCnFrom.Text) Or Not IsDate(txtCnTo.Text)) Then
                                retval = "1"
                            Else
                                objcls.PROCESS = "A"
                                objcls.User = Session("sUsr_name")
                                objcls.SCR_SCD_IDs = hf_SCDID.Value 'GetSelectedRows()
                                objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
                                objcls.bTRANSPORT = bTRANSPORT
                                objcls.SCD_CONCESSION_FROMDT = txtCnFrom.Text
                                objcls.SCD_CONCESSION_TODT = txtCnTo.Text
                                retval = objcls.Approve_Employee_Child_ConcessionRequest(objConn, Trans)
                            End If
                            If retval <> "0" Then
                                bRolledBack = True
                                Trans.Rollback()
                            Else
                                Trans.Commit()
                                bCommitted = True
                            End If
                        Else
                            bRolledBack = True
                            gdNEw.Columns(20).Visible = True
                        End If
                    End If
                Next
            Catch ex As Exception
                'Me.lblError.Text = "Unable to approve Request(s)"
                Trans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
                If bRolledBack = True And bCommitted = True Then
                    'Me.lblError.Text = "Not all request(s) have been approved, check the 'Error' column for details"
                    usrMessageBar.ShowNotification("Not all request(s) have been approved, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
                ElseIf bCommitted Then
                    'Me.lblError.Text = "Selected Request(s) have been approved"
                    usrMessageBar.ShowNotification("Selected Request(s) have been approved", UserControls_usrMessageBar.WarningType.Success)
                ElseIf bRolledBack Then
                    'Me.lblError.Text = "Unable to approve Request(s)"
                    usrMessageBar.ShowNotification("Unable to approve Request(s)", UserControls_usrMessageBar.WarningType.Danger)
                End If
                ' gridbind()
            End Try
            'Else
            'Me.lblError.Text = "Please select one or more concession(s) to be approved"
        End If
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If GetSelectedRows() <> "" Then
            Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim Trans As SqlTransaction = objConn.BeginTransaction("REJECT")
            Try
                objcls.PROCESS = "R"
                objcls.User = Session("sUsr_name")
                objcls.SCR_SCD_IDs = GetSelectedRows()
                objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
                objcls.bTRANSPORT = bTRANSPORT
                Dim retval As String = objcls.Approve_Employee_Child_ConcessionRequest(objConn, Trans)
                If retval <> "0" Then
                    'Me.lblError.Text = UtilityObj.getErrorMessage(retval)
                    usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                    Trans.Rollback()
                Else
                    Trans.Commit()
                    'Me.lblError.Text = "Selected Request(s) have been rejected"
                    usrMessageBar.ShowNotification("Selected Request(s) have been rejected", UserControls_usrMessageBar.WarningType.Success)
                    ' gridbind()
                End If
            Catch ex As Exception
                'Me.lblError.Text = "Unable to Reject"
                usrMessageBar.ShowNotification("Unable to Reject", UserControls_usrMessageBar.WarningType.Danger)
                Trans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try


        Else
            'Me.lblError.Text = "Please select one or more concession(s) to be rejected"
            usrMessageBar.ShowNotification("Please select one or more concession(s) to be rejected", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    'Protected Sub gvJournal_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvJournal.DataBound
    '    SetNameMismatchAlert()
    'End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        If ddlBusinessunit.SelectedItem.Value <> "900500" And ddlBusinessunit.SelectedItem.Value <> "900501" Then
            Session("PROVIDER_BSU_ID") = ddlBusinessunit.SelectedItem.Value
            FillACD()
            gridbind()
        Else
            ddlAcademicYear.ClearSelection()
            gridbind()
        End If
       
    End Sub
    Function CheckStartDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND '" + Format(Date.Parse(lblFrom.Text), "yyyy-MM-dd") + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function

    Private Function APPROVE_TRANSPORT_DISCONTINUE_REQ(ByVal bApprove As Boolean) As Integer
        APPROVE_TRANSPORT_DISCONTINUE_REQ = 1
        lblError.Text = ""
        Dim conn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        conn.Open()
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction()
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("TRANSPORT.saveTRANSPORTDISCONTINUEAprrove", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@STD_STU_ID", SqlDbType.Int)
            sqlp1.Value = ViewState("hf_SCDID")
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@STD_ID", SqlDbType.Int)
            sqlp2.Value = ViewState("hfSTD_ID")
            cmd.Parameters.Add(sqlp2)
            'Dim ApprovDate As Date = Format(Now.Date, "dd/MMM/yyyy")
            'ApprovDate = Format(Date.Parse(ApprovDate), "yyyy-MM-dd")
            txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
            Dim sqlp3 As New SqlParameter("@STD_APPROVEDATE", SqlDbType.DateTime)
            sqlp3.Value = CDate(txtFrom.Text)
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@STD_bAPPROVE", SqlDbType.Bit)
            sqlp4.Value = bApprove
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@STD_bDiscontCharges", SqlDbType.Bit)
            sqlp5.Value = DBNull.Value
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@STD_APRREMARKS", SqlDbType.VarChar, 255)
            sqlp6.Value = txtRemarks.Text.Replace("'", "''")
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@STD_APRUSR", SqlDbType.VarChar, 100)
            sqlp7.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlp7)


            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            APPROVE_TRANSPORT_DISCONTINUE_REQ = retSValParam.Value
            If retSValParam.Value = 0 Then
                trans.Commit()
                lblError.Text = "Request " & IIf(bApprove = True, "Approved", "Rejected") & " successfully"
            Else
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(APPROVE_TRANSPORT_DISCONTINUE_REQ)
            End If
        Catch ex As Exception
            trans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function


    Sub GetData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT STD_FROMDATE,ISNULL(STD_TODATE,'1900-01-01'),STD_TYPE,STD_REQREMARKS,STD_ID,D.DISCONT_REASON  FROM STUDENT_DISCONTINUE_S" _
                                  & " left join  DISCONTINUEREASON_M D on d.DISCONTR_ID=STUDENT_DISCONTINUE_S.STD_REASON" _
                                & " WHERE STD_STU_ID=" + ViewState("hf_SCDID") + " AND STD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND STD_bAPPROVE IS NULL "
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim type As String
        While reader.Read
            lblFrom.Text = Format(reader.GetDateTime(0), "dd/MMM/yyyy")
            'lblTo.Text = Format(reader.GetDateTime(1), "dd/MMM/yyyy").Replace("01/Jan/1900", "")
            type = reader.GetString(2)
            hfType.Value = type
            If type = "T" Then
                'lblType.Text = "Temporary discontinuation"
            ElseIf type = "P" Then
                'lblType.Text = "Permanent discontinuation"
            ElseIf type = "S" Then
                'lblType.Text = "Tranport suspension"
            End If
            'txtReqRemarks.Text = reader.GetString(3)
            hfSTD_ID.Value = reader.GetValue(4)
            ViewState("hfSTD_ID") = hfSTD_ID.Value
            'lblreason.Text = reader.GetValue(5)
        End While
    End Sub
    Sub GetStudData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim str_query As String = "SELECT  distinct isnull(D.SBL_DESCRIPTION,''),isnull(C.PNT_DESCRIPTION,'')," _
                                 & " isnull(F.SBL_DESCRIPTION,''),isnull(G.PNT_DESCRIPTION,'')," _
                                 & " ISNULL(I.BNO_DESCR,'')+'-'+ISNULL(I.TRP_DESCR,'') AS PICKUPTRIP, " _
                                 & " ISNULL(J.BNO_DESCR,'')+'-'+ISNULL(J.TRP_DESCR,'') AS DROPOFFTRIP " _
                                 & " FROM  STUDENT_M AS A " _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.VV_TRIPS_D AS I ON A.STU_PICKUP_TRP_ID=I.TRP_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.VV_TRIPS_D AS J ON A.STU_DROPOFF_TRP_ID=J.TRP_ID" _
                                 & " WHERE STU_ID =" + ViewState("hf_SCDID")




        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                'lblPArea.Text = .GetString(0)
                'lblPickup.Text = .GetString(1)
                'lblDArea.Text = .GetString(2)
                'lblDropoff.Text = .GetString(3)
                'lblPtrip.Text = .GetString(4)
                'lblDTrip.Text = .GetString(5)
            End With
        End While
        reader.Close()

        str_query = "SELECT ISNULL(STD_FROMDATE,'01/Jan/1900') FROM STUDENT_DISCONTINUE_S WHERE STD_TYPE='P' " _
                   & " AND STD_STU_ID=" + ViewState("hf_SCDID")
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim disDate As DateTime
        While reader.Read
            disDate = reader.GetDateTime(0)
        End While
        reader.Close()


    End Sub
    Protected Sub btnApproveNew_Click(sender As Object, e As EventArgs) Handles btnApproveNew.Click
        If GetSelectedRows() <> "" Then
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim bRolledBack As Boolean = False
            Dim bCommitted As Boolean = False
            Dim Trans As SqlTransaction '= objConn.BeginTransaction("APRVE")
            Try
                ' Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
                For Each gvr As GridViewRow In gdNEw.Rows

                    Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                    Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
                    Dim hf_SCDID As HiddenField = DirectCast(gvr.FindControl("hf_SCDID"), HiddenField)
                    Dim txtCnFrom As TextBox = DirectCast(gvr.FindControl("txtCnFrom"), TextBox)
                    Dim txtCnTo As TextBox = DirectCast(gvr.FindControl("txtCnTo"), TextBox)

                    Dim hf_STUID As HiddenField = DirectCast(gvr.FindControl("hf_STUID"), HiddenField)
                    '        Dim lblSCR_DATE As Label = DirectCast(e.Row.FindControl("lblSCR_DATE"), Label)
                    Dim hf_STUNAME As HiddenField = DirectCast(gvr.FindControl("hf_STUNAME"), HiddenField)
                    Dim hf_STNO As HiddenField = DirectCast(gvr.FindControl("hf_STNO"), HiddenField)
                    Dim hf_Grade As HiddenField = DirectCast(gvr.FindControl("hf_Grade"), HiddenField)
                    Dim hf_Section As HiddenField = DirectCast(gvr.FindControl("hf_Section"), HiddenField)



                    If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                        ViewState("hf_SCDID") = hf_STUID.Value
                        Call GetData()
                        Call GetStudData()

                        'Dim retval As String = "1"
                        'If objcls.VALIDATE_CONCESSION_REQUEST(hf_SCDID.Value, "") = True Then
                        '    Trans = objConn.BeginTransaction("APVE")
                        '    If bTRANSPORT AndAlso (Not IsDate(txtCnFrom.Text) Or Not IsDate(txtCnTo.Text)) Then
                        '        retval = "1"
                        '    Else
                        '        objcls.PROCESS = "A"
                        '        objcls.User = Session("sUsr_name")
                        '        objcls.SCR_SCD_IDs = hf_SCDID.Value 'GetSelectedRows()
                        '        objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
                        '        objcls.bTRANSPORT = bTRANSPORT
                        '        objcls.SCD_CONCESSION_FROMDT = txtCnFrom.Text
                        '        objcls.SCD_CONCESSION_TODT = txtCnTo.Text
                        '        retval = objcls.Approve_Employee_Child_ConcessionRequest(objConn, Trans)
                        '    End If
                        '    If retval <> "0" Then
                        '        bRolledBack = True
                        '        Trans.Rollback()
                        '    Else
                        '        Trans.Commit()
                        '        bCommitted = True
                        '    End If
                        'Else
                        '    bRolledBack = True
                        '    gdNEw.Columns(20).Visible = True
                        'End If

                        If hfType.Value = "P" Or hfType.Value = "T" Then
                            If studClass.checkFeeClosingDate(Session("sbsuid"), ddlAcademicYear.SelectedValue.ToString, Date.Parse(lblFrom.Text)) = False Then
                                lblError.Text = "The discontinuation strarting date has to be higher than the fee closing date"
                                Exit Sub
                            End If

                            If CheckStartDate() = False Then
                                Exit Sub
                            End If
                        End If
                        'SaveData(True)
                        If APPROVE_TRANSPORT_DISCONTINUE_REQ(True) = 0 Then
                            btnApprove.Enabled = False
                            btnReject.Enabled = False
                        End If


                        gridbind()




                    End If
                Next
            Catch ex As Exception
                'Me.lblError.Text = "Unable to approve Request(s)"
                Trans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
                If bRolledBack = True And bCommitted = True Then
                    'Me.lblError.Text = "Not all request(s) have been approved, check the 'Error' column for details"
                    usrMessageBar.ShowNotification("Not all request(s) have been approved, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
                ElseIf bCommitted Then
                    'Me.lblError.Text = "Selected Request(s) have been approved"
                    usrMessageBar.ShowNotification("Selected Request(s) have been approved", UserControls_usrMessageBar.WarningType.Success)
                ElseIf bRolledBack Then
                    'Me.lblError.Text = "Unable to approve Request(s)"
                    usrMessageBar.ShowNotification("Unable to approve Request(s)", UserControls_usrMessageBar.WarningType.Danger)
                End If
                ' gridbind()
            End Try
            'Else
            'Me.lblError.Text = "Please select one or more concession(s) to be approved"
        End If
    End Sub

    Protected Sub btnRejectnew_Click(sender As Object, e As EventArgs) Handles btnRejectnew.Click
        'If GetSelectedRows() <> "" Then
        '    Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
        '    Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        '    objConn.Open()
        '    Dim Trans As SqlTransaction = objConn.BeginTransaction("REJECT")
        '    Try
        '        objcls.PROCESS = "R"
        '        objcls.User = Session("sUsr_name")
        '        objcls.SCR_SCD_IDs = GetSelectedRows()
        '        objcls.SCD_APPR_REMARKS = Me.txtRemarks.Text
        '        objcls.bTRANSPORT = bTRANSPORT
        '        Dim retval As String = objcls.Approve_Employee_Child_ConcessionRequest(objConn, Trans)
        '        If retval <> "0" Then
        '            'Me.lblError.Text = UtilityObj.getErrorMessage(retval)
        '            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
        '            Trans.Rollback()
        '        Else
        '            Trans.Commit()
        '            'Me.lblError.Text = "Selected Request(s) have been rejected"
        '            usrMessageBar.ShowNotification("Selected Request(s) have been rejected", UserControls_usrMessageBar.WarningType.Success)
        '            ' gridbind()
        '        End If
        '    Catch ex As Exception
        '        'Me.lblError.Text = "Unable to Reject"
        '        usrMessageBar.ShowNotification("Unable to Reject", UserControls_usrMessageBar.WarningType.Danger)
        '        Trans.Rollback()
        '    Finally
        '        If objConn.State = ConnectionState.Open Then
        '            objConn.Close()
        '        End If
        '    End Try


        'Else
        '    'Me.lblError.Text = "Please select one or more concession(s) to be rejected"
        '    usrMessageBar.ShowNotification("Please select one or more concession(s) to be rejected", UserControls_usrMessageBar.WarningType.Danger)
        'End If

        If GetSelectedRows() <> "" Then
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim bRolledBack As Boolean = False
            Dim bCommitted As Boolean = False
            Dim Trans As SqlTransaction '= objConn.BeginTransaction("APRVE")
            Try
                ' Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
                For Each gvr As GridViewRow In gdNEw.Rows

                    Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                    Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
                    Dim hf_SCDID As HiddenField = DirectCast(gvr.FindControl("hf_SCDID"), HiddenField)
                    Dim txtCnFrom As TextBox = DirectCast(gvr.FindControl("txtCnFrom"), TextBox)
                    Dim txtCnTo As TextBox = DirectCast(gvr.FindControl("txtCnTo"), TextBox)

                    Dim hf_STUID As HiddenField = DirectCast(gvr.FindControl("hf_STUID"), HiddenField)
                    '        Dim lblSCR_DATE As Label = DirectCast(e.Row.FindControl("lblSCR_DATE"), Label)
                    Dim hf_STUNAME As HiddenField = DirectCast(gvr.FindControl("hf_STUNAME"), HiddenField)
                    Dim hf_STNO As HiddenField = DirectCast(gvr.FindControl("hf_STNO"), HiddenField)
                    Dim hf_Grade As HiddenField = DirectCast(gvr.FindControl("hf_Grade"), HiddenField)
                    Dim hf_Section As HiddenField = DirectCast(gvr.FindControl("hf_Section"), HiddenField)



                    If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                        ViewState("hf_SCDID") = hf_STUID.Value
                        Call GetData()
                        Call GetStudData()

                     
                        If hfType.Value = "P" Or hfType.Value = "T" Then
                            If studClass.checkFeeClosingDate(Session("sbsuid"), ddlAcademicYear.SelectedValue.ToString, Date.Parse(lblFrom.Text)) = False Then
                                lblError.Text = "The discontinuation strarting date has to be higher than the fee closing date"
                                Exit Sub
                            End If

                            If CheckStartDate() = False Then
                                Exit Sub
                            End If
                        End If
                        'SaveData(True)
                        If APPROVE_TRANSPORT_DISCONTINUE_REQ(False) = 0 Then
                            btnApprove.Enabled = False
                            btnReject.Enabled = False
                        End If


                        gridbind()




                    End If
                Next
            Catch ex As Exception
                'Me.lblError.Text = "Unable to approve Request(s)"
                Trans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
                If bRolledBack = True And bCommitted = True Then
                    'Me.lblError.Text = "Not all request(s) have been approved, check the 'Error' column for details"
                    usrMessageBar.ShowNotification("Not all request(s) have been approved, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
                ElseIf bCommitted Then
                    'Me.lblError.Text = "Selected Request(s) have been approved"
                    usrMessageBar.ShowNotification("Selected Request(s) have been approved", UserControls_usrMessageBar.WarningType.Success)
                ElseIf bRolledBack Then
                    'Me.lblError.Text = "Unable to approve Request(s)"
                    usrMessageBar.ShowNotification("Unable to approve Request(s)", UserControls_usrMessageBar.WarningType.Danger)
                End If
                ' gridbind()
            End Try
            'Else
            'Me.lblError.Text = "Please select one or more concession(s) to be approved"
        End If






    End Sub
End Class
