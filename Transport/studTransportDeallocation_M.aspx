<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTransportDeallocation_M.aspx.vb" Inherits="Transport_studTransportDeallocation_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">



        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvAllot.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}








    </script>

    <script language="javascript" type="text/javascript">
        function confirm_remove() {

            if (confirm("Are you sure you want to De-Allocate the selected students?") == true)
                return true;
            else
                return false;

        }

    </script>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Transport De-Allocation
        </div>
        <div class="card-body">
            <div class="table-responsive">

                
                <table id="tbl_ShowScreen" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table align="center" id="tbPromote" runat="server" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Curriculum</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled" Width="112px">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton>
                                        <asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtFrom"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="From Date  entered is not a valid date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator><asp:RequiredFieldValidator
                                                ID="rfFrom" runat="server" ControlToValidate="txtFrom" Display="None" ErrorMessage="Please enter the field trip date from"
                                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="Enter the Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>


                                    <td align="center" colspan="11" valign="top">
                                        <asp:GridView ID="gvAllot" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                       Select <br />
                                                       <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ssv_fromdate" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFromDate" runat="server" Text='<%# Bind("SSV_FROMDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="SL.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student ID">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblFeeHeader" runat="server" Text="Stud. No"></asp:Label><br />
                                                        <asp:TextBox ID="txtFeeSearch" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnFeeId_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnFeeId_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label><br />
                                                        <asp:TextBox ID="txtStudName" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                      Grade<br />
                                                      <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged" >
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>
                                                        Section<br />
                                                        <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="PickUp Trip">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblph" runat="server" Text="PickUp Trip"></asp:Label><br />
                                                                                <asp:TextBox ID="txtPTrip" runat="server" Width="75%"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnPTrip_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnPTrip_Search_Click" />
                                                                        
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPTrip" runat="server" Text='<%# Bind("TRP_PICKUP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Drop Off Trip">
                                                    <HeaderTemplate>
                                                         <asp:Label ID="lbldh" runat="server" Text="Drop Off Trip"></asp:Label><br />
                                                         <asp:TextBox ID="txtDTrip" runat="server" Width="75%"></asp:TextBox>
                                                         <asp:ImageButton ID="btnDTrip_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnDTrip_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDrip" runat="server" Text='<%# Bind("TRP_DROPOFF") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="Green" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td colspan="4" align="center">
                                        <asp:Button ID="btndeAllot" OnClientClick="javascipt:return confirm_remove();" runat="server" Text="De-Allocate" CssClass="button" TabIndex="4" />
                                    </td>


                                </tr>
                            </table>
                            &nbsp;
                    <asp:HiddenField ID="hfSHF_ID" runat="server" />
                            <asp:HiddenField ID="hfTRD_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfSeats" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1"
                                runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="txtFrom"
                                TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="HiddenField" runat="server" />
                            <asp:HiddenField ID="hfJourney" runat="server" />
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>


                </table>

            </div>


        </div>

    </div>




</asp:Content>

