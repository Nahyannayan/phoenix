<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tptSeatCapacity_SelectArea_M.aspx.vb" Inherits="Transport_tptSeatCapacity_M" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
   <link href="../cssfiles/sb-admin.css" rel="stylesheet" type="text/css" />
   <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />  
          <script language="javascript" type="text/javascript">
      
       
function switchViews(obj,row)
        {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            
            if (div.style.display=="none")
                {
                    div.style.display = "inline";
                    if (row=='alt')
                       {
                           img.src="../Images/expand_button_white_alt_down.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_Button_white_Down.jpg" ;
                       }
                   img.alt = "Click to close";
               }
           else
               {
                   div.style.display = "none";
                   if (row=='alt')
                       {
                           img.src="../Images/Expand_button_white_alt.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_button_white.jpg" ;
                       }
                   img.alt = "Click to expand";
               }
       }

</script>

</head>
<body>
<form id="form1" runat="server">
<div>
 <table id="tbl_ShowScreen" runat="server" align="center" width="100%" class="m-1" >
         <tr><td>
      <table width="100%" id="Table3" >
                
               
               <tr>
              
                <td width="100%" align="left" class="title-bg">SEAT CAPACITY</td>
              </tr>
              </table>
       </td> </tr>           
            <tr >    <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                        </tr>
               
               
      
        
           <tr>
      
            <td align="center" colspan="4">
            
  <table id="Table1" runat="server" align="center" width="100%">
                    
        <tr><td  colspan="4" align="center" >
         
         <table id="Table2" runat="server" align="center" width="100%" >
             <tr>
               <td align="left" width="20%">
                            <span class="field-label">Select Academic Year</span></td>
                     <td align="left" width="30%"><asp:DropDownList ID="ddlAcademicYear" SKINID="smallcmb" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                     
                 </td>
                 <td width="20%"></td>
                 <td width="30%"></td>
            </tr>
          <tr><td align="left"><span class="field-label">Select Shift</span></td>
          <td align="left">
                <asp:DropDownList ID="ddlShift"  runat="server" AutoPostBack="True" AppendDataBoundItems="True">
               </asp:DropDownList></td>
            <td colspan="2">  <asp:RadioButton ID="rdOnward" AutoPostBack="true" runat="server" CssClass="field-label" Checked="True" GroupName="G1" Text="Onward" />
              <asp:RadioButton ID="rdReturn" AutoPostBack="true" runat="server" CssClass="field-label" GroupName="G1" Text="Return" /></td>
            </tr>
            
            <tr><td colspan="4" align="center">
           <asp:GridView ID="gvTPT" runat="server" CssClass="table table-bordered table-row" 
                    AutoGenerateColumns="False" PageSize="20"  AllowPaging="True" Width="100%" >
           <Columns>
         
                   <asp:TemplateField>
           <ItemTemplate>
              <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
               </a>
            </ItemTemplate>
            <AlternatingItemTemplate>
             <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
              <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
              </a>
            </AlternatingItemTemplate>
           </asp:TemplateField>
                       
                 <asp:TemplateField Visible="FALSE" >
                    <ItemTemplate>
                      <asp:Label ID="lblSblId" runat="server" text='<%# Bind("sbl_id") %>'></asp:Label>
                      </ItemTemplate>
                                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Area">
                <HeaderTemplate>
                <asp:Label ID="lblH1" runat="server" CssClass="gridheader_text" Text="Area"></asp:Label>
                <br />
                    <asp:TextBox ID="txtArea" runat="server" Width="75%"></asp:TextBox></td>
                    <asp:ImageButton ID="btnArea_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnArea_Search_Click" />
                               
                         </HeaderTemplate>

                      <ItemTemplate>
                      <asp:Label ID="lblArea" runat="server" text='<%# Bind("sbl_description") %>'></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                           
                     <asp:BoundField DataField="SEAT_TOT" ItemStyle-Width="70px" HeaderText="Total Seats"  HtmlEncode="False" >
                      <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                      
                    <asp:BoundField DataField="SEAT_CURRENT" ItemStyle-Width="70px" HeaderText="Currently Alloted"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    
                     <asp:BoundField DataField="SEAT_NEW" ItemStyle-Width="70px" HeaderText="New Admissions"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    
                     <asp:BoundField DataField="SEAT_TEMP" ItemStyle-Width="70px" HeaderText="Temporary Allotments"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                     
                     <asp:BoundField DataField="SEAT_WAIT" ItemStyle-Width="70px" HeaderText="Waiting List"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    
                     <asp:BoundField DataField="SEAT_AVAILABLE" ItemStyle-Width="70px" HeaderText="Available Seats"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                   
                            <asp:TemplateField >
                   <ItemTemplate>
                    </td></tr>
                     <tr>
                    <td colspan="100%">
                    <div id="div<%# Eval("GUID") %>" style="display:none;position:relative;left:20px;" >
                   <asp:GridView ID="gvPickUp" runat="server" Width="100%"
                   AutoGenerateColumns="false" EmptyDataText="No pickup points for this." >
                    <Columns>
                    
                            
                     <asp:BoundField DataField="PNT_DESCRIPTION" ItemStyle-Width="70px" HeaderText="Pickup Point"  HtmlEncode="False" >
                      <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                       
                           
                     <asp:BoundField DataField="SEAT_TOT_P" ItemStyle-Width="70px" HeaderText="Total Seats"  HtmlEncode="False" >
                      <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                      
                    <asp:BoundField DataField="SEAT_CURRENT_P" ItemStyle-Width="70px" HeaderText="Currently Alloted"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    
                     <asp:BoundField DataField="SEAT_NEW_P" ItemStyle-Width="70px" HeaderText="New Admissions"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    
                     <asp:BoundField DataField="SEAT_TEMP_P" ItemStyle-Width="70px" HeaderText="Temporary Allotments"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                     
                     <asp:BoundField DataField="SEAT_WAIT_P" ItemStyle-Width="70px" HeaderText="Waiting List"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    
                     <asp:BoundField DataField="SEAT_AVAILABLE_P" ItemStyle-Width="70px" HeaderText="Available Seats"  HtmlEncode="False" >
                    <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    
                  </Columns>
                  <RowStyle CssClass="griditem" /> 
                   <HeaderStyle CssClass="gridheader_pop" /> 
                 </asp:GridView>
                  </div>
                  </td>
                   </tr>
                  </ItemTemplate>
                 </asp:TemplateField>
                            
     
              </Columns>
                   <RowStyle CssClass="griditem"/>
                            <HeaderStyle CssClass="gridheader_pop"/>
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                            <SelectedRowStyle BackColor="Aqua" />
                            <PagerStyle HorizontalAlign="Left"  />                        
               </asp:GridView>   
        </td></tr>
        </table>
                   
           </td>
           </tr>
           </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /></td></tr>
          
        </table>


      
   
</div>
</form>
</body>
</html>
