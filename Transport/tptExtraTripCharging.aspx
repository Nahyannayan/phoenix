<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptExtraTripCharging.aspx.vb" Inherits="Transport_tptExtraTripCharging" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            EXTRA TRIP APPROVAL
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" style="width: 100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" SkinID="error" HeaderText="You must enter a value in the following fields:" EnableViewState="False"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%" id="Table4" runat="server">
                                <tbody>
                                    <tr>
                                        <td align="left" width="20%">
                                            <asp:Label ID="Label2" CssClass="field-label" runat="server" Text="Trip Date From"></asp:Label></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="groupM1" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$" ErrorMessage="Enter the Trip Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007" Display="Dynamic" ControlToValidate="txtFrom">*</asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="CustomValidator3" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date" Display="Dynamic" ControlToValidate="txtFrom">*</asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="rfFrom" runat="server" ValidationGroup="groupM1" ErrorMessage="Please enter the field trip date from" Display="None" ControlToValidate="txtFrom"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" width="20%">
                                            <asp:Label ID="Label3" CssClass="field-label" runat="server" Text="To"></asp:Label></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="groupM1" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$" ErrorMessage="Enter the Trip Date To in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007" Display="Dynamic" ControlToValidate="txtTo">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="Trip Date To entered is not a valid date" Display="Dynamic" ControlToValidate="txtTo">*</asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="rfTo" runat="server" ValidationGroup="groupM1" ErrorMessage="Please enter the field trip date to" Display="None" ControlToValidate="txtTo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" ValidationGroup="groupM1"></asp:Button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>

                        <td align="center">

                            <table id="Table1" runat="server" align="center" style="width: 100%">



                                <tr>
                                    <td align="center">

                                        <table id="Table2" runat="server" align="center" style="width: 100%">

                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvTrip" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None" AllowSorting="True">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="-" Visible="False">

                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrpId" runat="server" Text='<%# Bind("trp_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="-" Visible="False">

                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrdId" runat="server" Text='<%# Bind("trd_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Trip">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblTp" runat="server" CssClass="gridheader_text" Text="Trip"></asp:Label><br />
                                                                    <asp:TextBox ID="txtTrip" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnTrip_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnTrip_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTrip" runat="server" Text='<%# Bind("TRP_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Destination">

                                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("trp_destination") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Remarks">
                                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDestination" runat="server" Text='<%# Bind("trd_remarks") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Purpose">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblP" runat="server" CssClass="gridheader_text" Text="Purpose"></asp:Label><br />
                                                                    <asp:TextBox ID="txtPurpose" runat="server" Width="75%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnPurpose_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnPurpose_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPurpose" runat="server" Text='<%# Bind("PSE_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="From Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFrom" runat="server" Text='<%# Bind("TRD_FROMDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="To Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTo" runat="server" Text='<%# Bind("TRD_TODATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Payable/Non Payable
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:RadioButton ID="rdPay" Checked='<%# Bind("PAY") %>' GroupName='<%# Eval("trp_Id") %>' runat="server" />
                                                                    <asp:RadioButton ID="rdNoPay" Checked='<%# Bind("NOPAY") %>' GroupName='<%# Eval("trp_id") %>' runat="server" />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                                                            </asp:TemplateField>


                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                                        <RowStyle CssClass="griditem" Wrap="False" />
                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="7" Text="Save" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <input id="h_Selected_menu_1"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />&nbsp;
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>


                <script type="text/javascript">


                    cssdropdown.startchrome("Div1")
                    cssdropdown.startchrome("Div2")


                </script>
            </div>
        </div>
    </div>
</asp:Content>

