﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO

Partial Class Transport_tplSplTransportDet
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim st As Integer
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                h_GRD_IDs.Value = Request.QueryString("acd_id")
                st = Val(Request.QueryString("spl_id"))
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100500") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    FillGrade()
                    If ViewState("datamode") = "edit" Then
                        gridbind(st)
                        txtbind(st)
                        btn.Visible = False
                        Update.Visible = True
                    End If

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub
    'Sub FillStud()


    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

    '    Dim str_Sql As String = " SELECT stu_id,stu_no FROM student_m    WHERE stu_BSU_ID ='" + Session("SBSUID") + "'"

    '    Dim ds As DataSet
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

    '    ddlStudent.DataSource = ds
    '    ddlStudent.DataTextField = "stu_no"
    '    ddlStudent.DataValueField = "stu_no"
    '    ddlStudent.DataBind()
    '    If (Not ddlStudent.Items Is Nothing) AndAlso (ddlStudent.Items.Count > 1) Then
    '        ddlStudent.Items.Add(New ListItem("-", "-"))
    '        ddlStudent.Items.FindByText("-").Selected = True
    '    End If



    'End Sub
    'Sub FillStaff()


    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString

    '    Dim str_Sql As String = " SELECT emp_id,empno FROM employee_m    WHERE emp_BSU_ID ='" + Session("SBSUID") + "'"

    '    Dim ds As DataSet
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

    '    ddlStaff.DataSource = ds
    '    ddlStaff.DataTextField = "empno"
    '    ddlStaff.DataValueField = "empno"
    '    ddlStaff.DataBind()
    '    If (Not ddlStaff.Items Is Nothing) AndAlso (ddlStaff.Items.Count > 1) Then
    '        ddlStaff.Items.Add(New ListItem("-", "-"))
    '        ddlStaff.Items.FindByText("-").Selected = True
    '    End If



    'End Sub


    

    
    

 
    Private Sub CallReport(ByVal s As Integer)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@st_id", s)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_transport"
            .reportParameters = param
            .reportPath = Server.MapPath("../Transport/Reports/Rpt/rptSplTransArrangement.rpt")

        End With
        'If hfbDownload.Value = 1 Then
        'Dim rptDownload As New ReportDownload
        'rptDownload.LoadReports(rptClass, rs)
        'rptDownload = Nothing
        'Else
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        'End If
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn.Click
        Try


            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String


            str_query = "exec dbo.insert_spltransport  " & Session("sBsuid") & " ,'" & CDate(txtDate.Text) & "','" & txtPgm.Text & "','" & Trim(txtTfrm.Text) & "'," _
                        & "'" & Trim(txtToreach.Text) & " ','" & txtReturnfrm.Text & "','" & txtIncharge.Text & "','" & txtTime.Text & "','" & txtMobno.Text & "'," & Val(h_GRD_IDs.Value)


            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


            ' Dim url As String = String.Format("~\Transport\tplSplTransportDet.aspx?stuid=" & gettrid() & "")
            'Response.Redirect(url)
            Dim t As String = ""
            Dim s As String = ""

            For Each row As GridViewRow In grdStudent.Rows
                Dim lb As Label = row.FindControl("lbstu_no")
                s = lb.Text
                str_query = "exec dbo.insert_spltransport_D   " & gettrid() & " ,'" & t & "','" & s & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next

            s = ""

            For Each row As GridViewRow In grdStaff.Rows
                Dim lb As Label = row.FindControl("lbstf_no")
                t = lb.Text
                str_query = "exec dbo.insert_spltransport_D   " & gettrid() & " ,'" & t & "','" & s & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next





            st = gettrid()
            gridbind(st)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Function gettrid() As Integer
        Dim s As Integer


        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = " SELECT sta_id  FROM dbo.spltransport_m where sta_date='" + CDate(txtDate.Text) + "' AND sta_pgm='" + txtPgm.Text + "' and sta_acd_id=" & Val(h_GRD_IDs.Value)

        s = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Return s


    End Function
    Sub FillGrade()

        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER  FROM GRADE_BSU_M  INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID  WHERE " _
                                 & " GRM_BSU_ID ='" + Session("SBSUID") + "' AND GRM_ACD_ID= '" + h_GRD_IDs.Value + "'  ORDER BY GRD_DISPLAYORDER "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count >= 1 Then
            ddlGrade.SelectedIndex = 0
            Try
                FillSection()

            Catch ex As Exception

            End Try
        End If
    End Sub
    Sub FillSection()

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " select SCT_ID,SCT_DESCR  from  SECTION_M where " _
                                 & " sct_bsu_id ='" + Session("sBSUID") + "' and SCT_ACD_ID= '" + h_GRD_IDs.Value + "'  and " _
                                 & " SCT_GRD_ID = '" + ddlGrade.SelectedValue + "' and SCT_DESCR <>'TEMP'"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If

    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 'Handles imgStudent.Click
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub
    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub
 
  
    Public Function GetSelectedStudents(Optional ByVal vSTU_IDs As String = "")
        Dim str_sql As String = "SELECT DISTINCT STU_NO ID, STU_NAME DESCR " & _
         " FROM  vw_STUDENT_DETAILS "
        If vSTU_IDs <> "" Then
            str_sql += "WHERE STU_ID IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
    End Function
    Protected Sub grdStaff_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStaff.PageIndex = e.NewPageIndex
        GridBindStaff(h_EMP_IDs.Value)
    End Sub
    Public Function GetSelectedStaffs(Optional ByVal vSTU_IDs As String = "")
        Dim str_sql As String = "SELECT DISTINCT empno ID, empname DESCR " & _
         " FROM  Vw_EmployeeData  "
        If vSTU_IDs <> "" Then
            str_sql += "WHERE emp_id IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function
    Protected Sub imgStaff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStaff.Click
        If h_EMP_IDs.Value <> "" Then
            GridBindStaff(h_EMP_IDs.Value)
        End If
    End Sub
    Private Sub GridBindStaff(ByVal vSTU_IDs As String)
        grdStaff.DataSource = GetSelectedStaffs(vSTU_IDs)
        grdStaff.DataBind()
    End Sub
    Public Sub gridbind(ByVal t As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet


            str_Sql = "  SELECT  spl_id," _
 & "case when (SPL_EMPNO ='') then SPL_STUNO    else case when (SPL_STUNO='') then SPL_EMPNO   else '0' end end as stu_regno," _
 & "case when (SPL_EMPNO ='') then 'Student'    else case when (SPL_STUNO='') then 'Staff'   else '0' end end as stu_r," _
 & "case when (SPL_EMPNO ='') then (select stu_name from VW_STUDENT_M where STU_NO=spl_stuno)    else case when (SPL_STUNO='') then  (select ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME  ,'') from VW_EMPLOYEE_M where empno=SPL_EMPNO )  else '0' end end as stu_name , " _
 & "case when (SPL_EMPNO ='') then (select GRD_DISPLAY  from VW_STUDENT_M where STU_NO=spl_stuno)    else case when (SPL_STUNO='') then  ''  else '0' end end as stu_grd   " _
& " from dbo.SPLTRANSPORT_D  WHERE SPL_STA_ID=" & t & " order by stu_regno "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvGroup.DataSource = ds.Tables(0)
                gvGroup.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataSource = ds.Tables(0)
                Try
                    gvGroup.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Public Sub txtbind(ByVal t As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet


            str_Sql = "  SELECT  sta_date,sta_pgm,sta_tfrom,STA_TOREACH ,STA_RETURNFROM ,STA_NAMEINCHARGE,STA_TIME,STA_MOBNO  from dbo.SPLTRANSPORT_m " _
                            & "  where STA_ID=" & t & ""

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


            txtDate.Text = ds.Tables(0).Rows(0)("sta_date")
            txtPgm.Text = ds.Tables(0).Rows(0)("sta_pgm")
            txtTfrm.Text = ds.Tables(0).Rows(0)("sta_tfrom")
            txtToreach.Text = ds.Tables(0).Rows(0)("STA_TOREACH")
            txtReturnfrm.Text = ds.Tables(0).Rows(0)("STA_RETURNFROM")
            txtIncharge.Text = ds.Tables(0).Rows(0)("STA_NAMEINCHARGE")
            txtTime.Text = ds.Tables(0).Rows(0)("STA_TIME")
            txtMobno.Text = ds.Tables(0).Rows(0)("STA_MOBNO")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    'Protected Sub gvGroup_Rowcommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup.RowCommand


    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    '    Dim str_query As String
    '    Try


    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim selectedRow As GridViewRow = DirectCast(gvGroup.Rows(index), GridViewRow)
    '        Dim lblid As New Label

    '        lblid = selectedRow.FindControl("lblID")
    '        st = Val(lblid.Text)

    '        If e.CommandName = "Delete" Then
    '            'If vbYes = MsgBox("You are about to delete this record.Do you want to proceed?", MsgBoxStyle.YesNo, "Information") Then
    '            str_query = "exec dbo.delete_spltransport_D   " & st
    '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    '            gridbind(Val(Request.QueryString("spl_id")))
    '            'End If
    '        End If



    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    '    ' End If
    'End Sub

    Protected Sub gvGroup_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvGroup.RowDeleting

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        Try
            gvGroup.SelectedIndex = e.RowIndex
            'Dim ID As Integer = CInt(gvComments.DataKeys(e.RowIndex).Value)

            Dim row As GridViewRow = DirectCast(gvGroup.Rows(e.RowIndex), GridViewRow)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblID"), Label)
            st = Val(lblID.Text)
            If lblID.Text <> "" Then
                str_query = "exec dbo.delete_spltransport_D   " & st
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind(Val(Request.QueryString("spl_id")))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Update.Click
        Try


            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String
            Dim id As Integer = Val(Request.QueryString("spl_id"))

            str_query = "exec dbo.update_spltransport  " & id & " ,'" & CDate(txtDate.Text) & "','" & txtPgm.Text & "','" & txtTfrm.Text & "'," _
                        & "'" & txtToreach.Text & " ','" & txtReturnfrm.Text & "','" & txtIncharge.Text & "','" & txtTime.Text & "','" & txtMobno.Text & "'"


            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


            ' Dim url As String = String.Format("~\Transport\tplSplTransportDet.aspx?stuid=" & gettrid() & "")
            'Response.Redirect(url)
            Dim t As String = ""
            Dim s As String = ""

            For Each row As GridViewRow In grdStudent.Rows
                Dim lb As Label = row.FindControl("lbstu_no")
                s = lb.Text
                str_query = "exec dbo.insert_spltransport_D   " & id & " ,'" & t & "','" & s & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next

            s = ""

            For Each row As GridViewRow In grdStaff.Rows
                Dim lb As Label = row.FindControl("lbstf_no")
                t = lb.Text
                str_query = "exec dbo.insert_spltransport_D   " & id & " ,'" & t & "','" & s & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next

            gridbind(id)
            GridBindStaff(0)
            GridBindStudents(0)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub Print_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Print.Click
        st = gettrid()
        CallReport(st)
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        txtDate.Text = ""
        txtPgm.Text = ""
        txtTfrm.Text = ""
        txtToreach.Text = ""
        txtReturnfrm.Text = ""
        txtIncharge.Text = ""
        txtTime.Text = ""
        txtMobno.Text = ""
        gridbind(0)
        GridBindStaff(0)
        GridBindStudents(0)
        h_EMP_IDs.Value = ""
        h_STU_IDs.Value = ""
        Update.Visible = False
        btn.Visible = True
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        FillSection()
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        If h_STU_IDs.Value <> "" Then
            h_STU_IDs.Value = h_STU_IDs.Value.Replace("undefined", "___")
            txtStudIDs.Text = ""
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub txtEmpId_TextChanged(sender As Object, e As EventArgs)
        If h_EMP_IDs.Value <> "" Then
            h_EMP_IDs.Value = h_EMP_IDs.Value.Replace("undefined", "___")
            txtEmpId.Text = ""
            GridBindStaff(h_EMP_IDs.Value)
        End If
    End Sub
End Class
