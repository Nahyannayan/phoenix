Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studNewTransportApprove
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000240") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfCLM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("clm").Replace(" ", "+"))
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfSHF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))
                    lblName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                    BindPromotionDtls() 'Added by vikrnath on 30th Jan 2020
                    GetData()
                    txtApprove.Text = Format(Now.Date, "dd/MMM/yyyy")
                    btnService.Enabled = False
                    BindCapacity(gvOnward, ddlonward.SelectedValue.ToString, "Onward")
                    BindCapacity(gvReturn, ddlReturn.SelectedValue.ToString, "Return")

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub



#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindCapacity(ByVal gvCap As GridView, ByVal trdId As String, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.getSEATCAPACITY " _
                                & "'" + Session("sbsuid") + "'," _
                                & trdId + "," _
                                & hfACD_ID.Value + "," _
                                & Session("Current_ACY_ID") + "," _
                                & "'" + journey + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvCap.DataSource = ds
        gvCap.DataBind()
    End Sub

    Sub GetData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim trdPickup As Integer
        Dim trdDropoff As Integer
        Dim str_query As String = String.Empty
        'Dim str_query As String = "EXEC dbo.GET_TRANSPORT_SERVICE_REQUEST @STU_ID = " & hfSTU_ID.Value
        'Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        'While reader.Read
        '    With reader
        '        lblPlocation.Text = .GetString(0)
        '        lblPSublocation.Text = .GetString(1)
        '        lblPPickup.Text = .GetString(2)
        '        lblDlocation.Text = .GetString(3)
        '        lblDsublocation.Text = .GetString(4)
        '        lblDPickup.Text = .GetString(5)
        '        hfSNR_ID.Value = .GetValue(6)
        '        hfPSBL_ID.Value = .GetValue(7)
        '        hfDSBL_ID.Value = .GetValue(8)
        '        lblDate.Text = Format(.GetDateTime(9), "dd/MMM/yyyy")
        '        hfPPNT_ID.Value = .GetValue(10)
        '        hfDPNT_ID.Value = .GetValue(11)
        '        trdPickup = .GetValue(12)
        '        trdDropoff = .GetValue(13)
        '    End With
        'End While
        'reader.Close()

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = hfSTU_ID.Value
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = hfACD_ID.Value

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString,
          CommandType.StoredProcedure, "dbo.GET_TRANSPORT_SERVICE_REQUEST", pParms)
        If Not dsData Is Nothing And dsData.Tables(0).Rows.Count > 0 Then
            lblPlocation.Text = dsData.Tables(0).Rows(0)("PICKUP_LOC_DESCRIPTION")
            lblPSublocation.Text = dsData.Tables(0).Rows(0)("PICKUP_SBL_DESCRIPTION")
            lblPPickup.Text = dsData.Tables(0).Rows(0)("PICKUP_PNT_DESCRIPTION")
            lblDlocation.Text = dsData.Tables(0).Rows(0)("DROPOFF_LOC_DESCRIPTION")
            lblDsublocation.Text = dsData.Tables(0).Rows(0)("DROPOFF_SBL_DESCRIPTION")
            lblDPickup.Text = dsData.Tables(0).Rows(0)("DROPOFF_PNT_DESCRIPTION")
            hfSNR_ID.Value = dsData.Tables(0).Rows(0)("SNR_ID")
            hfPSBL_ID.Value = dsData.Tables(0).Rows(0)("PICKUP_SBL_ID")
            hfDSBL_ID.Value = dsData.Tables(0).Rows(0)("DROPOFF_SBL_ID")
            lblDate.Text = dsData.Tables(0).Rows(0)("SNR_FROMDATE")
            hfPPNT_ID.Value = dsData.Tables(0).Rows(0)("PICKUP_PNT_ID")
            hfDPNT_ID.Value = dsData.Tables(0).Rows(0)("DROPOFF_PNT_ID")
            trdPickup = dsData.Tables(0).Rows(0)("TRD_ID_PICKUP")
            trdDropoff = dsData.Tables(0).Rows(0)("TRD_ID_DROPOFF")
            lblAcdStartdate.Text = dsData.Tables(0).Rows(0)("ACD_START")
            lblTermdetails.Text = dsData.Tables(0).Rows(0)("TERM_DETAILS")
        End If


        str_query = "SELECT ISNULL(max(STD_FROMDATE),'01/Jan/1900') FROM STUDENT_DISCONTINUE_S WHERE STD_TYPE='P' " _
                   & " AND STD_STU_ID=" + hfSTU_ID.Value
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim disDate As DateTime
        While reader.Read
            disDate = reader.GetDateTime(0)
        End While
        reader.Close()

        If Format(disDate, "dd/MMM/yyyy") = "01/Jan/1900" Or Format(disDate, "dd/MMM/yyyy") = "01/Jan/0001" Then
            lblStatus.Text = "New"
        Else
            lblStatus.Text = "Transport discontinued on " + Format(disDate, "dd/MMM/yyyy")
            lblStatus.ForeColor = Drawing.Color.Red
        End If
        hfDisDate.Value = disDate

        BindTrip(ddlonward, "Onward")
        BindTrip(ddlReturn, "Return")
        If Not ddlonward.Items.FindByValue(trdPickup) Is Nothing Then
            ddlonward.Items.FindByValue(trdPickup).Selected = True
        End If
        If Not ddlReturn.Items.FindByValue(trdDropoff) Is Nothing Then
            ddlReturn.Items.FindByValue(trdDropoff).Selected = True
        End If
    End Sub


    Sub SaveData(ByVal bApprove As Boolean)
        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = " EXEC TRANSPORT.saveSTUDNEWTRANSPORTREQAPPROVAL " _
                                         & hfSTU_ID.Value + "," _
                                         & hfSNR_ID.Value + "," _
                                         & "'" + bApprove.ToString + "'," _
                                         & "'" + Format(Date.Parse(txtApprove.Text), "yyyy-MM-dd") + "'," _
                                         & IIf(ddlonward.SelectedValue = "0", "NULL", ddlonward.SelectedValue.ToString) + "," _
                                         & IIf(ddlReturn.SelectedValue = "0", "NULL", ddlReturn.SelectedValue.ToString) + "," _
                                         & "'" + Session("sUsr_name") + "'," _
                                         & hfCLM_ID.Value + "," _
                                         & IIf(ddlPromotions.SelectedValue = "0", "NULL", ddlPromotions.SelectedValue.ToString)
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                'transaction.Rollback()
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Request could not be processed (" & ex.Message & ")"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Function CheckStartDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND '" + Format(Date.Parse(lblDate.Text), "yyyy-MM-dd") + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function


    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_PNT_ID=" + hfPPNT_ID.Value _
                        & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_PNT_ID=" + hfDPNT_ID.Value _
                 & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL"



        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"

        ddlTrip.Items.Insert(0, li)

        'For Each li In ddlTrip.Items
        '    li.Attributes.Add("title", li.Text)
        'Next
    End Sub




    Sub CallReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptStudTransportInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub CallReqReport(ByVal STU_IDS As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_IDS", STU_IDS)
        param.Add("BSU", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptTransportReqInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function


    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

    Sub CallServiceReport()
        Dim param As New Hashtable

        param.Add("@STU_ACD_ID", hfACD_ID.Value)
        param.Add("@STU_GRD_ID", "ALL")
        param.Add("@STU_SCT_ID", "ALL")
        param.Add("@STU_SHF_ID", 0)
        param.Add("@STU_IDs", hfSTU_ID.Value)
        param.Add("@FLAGENQ_STUD", "true")
        param.Add("@bTANS_AVAIL", "true")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_transport"
            .reportParameters = param
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptStudTrans_Survey.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

#End Region

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim lintCheckRate
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Try
            If studClass.checkFeeClosingDate(Session("sbsuid"), CType(hfACD_ID.Value, Integer), Date.Parse(lblDate.Text)) = True Then
                Try
                    If hfDisDate.Value <> "" Then
                        If Date.Parse(hfDisDate.Value) >= Date.Parse(lblDate.Text) Then
                            lblError.Text = "Service date has to be a date higher than discontinuation date"
                            Exit Sub
                        End If
                    End If
                Catch ex As Exception
                End Try

                Dim acd_id As String = studClass.GetACD_ID(lblDate.Text, Session("sbsuid"), Session("clm")).ToString


                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))

                Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_CHECK_RATE", pParms)
                    While reader.Read
                        lintCheckRate = Convert.ToString(reader("BSU_bCheckRate"))
                    End While
                End Using


                If lintCheckRate = 1 Then
                    If studClass.CheckRateExists(Date.Parse(lblDate.Text), hfPSBL_ID.Value, acd_id) = False Then
                        lblError.Text = "Request could not be processed as rate is not set for " + lblPSublocation.Text
                        Exit Sub
                    End If
                End If


                'If CheckStartDate() = False Then
                '    Exit Sub
                'End If
                SaveData(True)

                btnApprove.Enabled = False
                btnReject.Enabled = False
                btnService.Enabled = True
                'ViewState("datamode") = Encr_decrData.Encrypt("view")
                'ViewState("MainMnu_code") = Encr_decrData.Encrypt("T100190")
                'Dim url As String = String.Format("~\Transport\studTPTEdit_M.aspx?MainMnu_code={0}&datamode={1}&stuid=" + Encr_decrData.Encrypt(hfSTU_ID.Value) + "&shfid=" + Encr_decrData.Encrypt(hfSHF_ID.Value) + "&sen=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&name=" + Encr_decrData.Encrypt(lblName.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                'Response.Redirect(url)
            Else
                lblError.Text = "The service starting date has to be higher than the fee closing date"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            SaveData(False)
            lblError.Text = "Record Saved Successfully"
            btnApprove.Enabled = False
            btnReject.Enabled = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub


    Protected Sub ddlonward_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlonward.SelectedIndexChanged
        Try
            BindCapacity(gvOnward, ddlonward.SelectedValue.ToString, "Onward")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlReturn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReturn.SelectedIndexChanged
        Try
            BindCapacity(gvReturn, ddlReturn.SelectedValue.ToString, "Return")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lnKPickup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnKPickup.Click
        BindTrip(ddlonward, "Onward")
        BindTrip(ddlReturn, "Return")
    End Sub

    Protected Sub lnkDropOff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDropOff.Click
        BindTrip(ddlonward, "Onward")
        BindTrip(ddlReturn, "Return")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If ddlonward.SelectedValue = "0" And ddlReturn.SelectedValue = "0" Then
            CallReqReport(hfSTU_ID.Value)
        Else
            CallReport(hfSTU_ID.Value)
        End If
    End Sub

    Protected Sub btnService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnService.Click
        CallServiceReport()
    End Sub
    'Added by vikranth on 30th Jan 2020
    Public Sub BindPromotionDtls()
        Try
            Dim ds As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@Bsu_Id", Session("sBsuid"), SqlDbType.VarChar)
            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                Dim sql_query As String = "[TRANSPORT].[GET_TRANSPORT_FEE_PROMOTION_DETAILS]"
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not ds Is Nothing Then
                ddlPromotions.DataSource = ds.Tables(0)
                ddlPromotions.DataTextField = "TPT_PROMO_NAME"
                ddlPromotions.DataValueField = "TPT_PROMO_ID"
                ddlPromotions.DataBind()
                ddlPromotions.Items.Insert(0, New ListItem("SELECT", 0))
                'ddl.Items.FindByValue(Session("BSU_COUNTRY_ID")).Selected = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlPromotions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPromotions.SelectedIndexChanged
        Try
            If ddlPromotions.SelectedValue <> 0 Then
                GetPromotionDetails(ddlPromotions.SelectedValue)
            Else tblPromoDetails.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub GetPromotionDetails(ByVal promo_Id As Integer)
        Try
            tblPromoDetails.Visible = False
            Dim conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ds As DataSet
            Dim Param(3) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@TPT_PROMO_ID", promo_Id, SqlDbType.Int)
            Param(1) = Mainclass.CreateSqlParameter("@TYPE", "DETAILS", SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GET_TRANSPORTPROMOTIONS", Param)
            If ds.Tables(0).Rows.Count > 0 Then
                tblPromoDetails.Visible = True
                lblPromoDescrVal.Text = ds.Tables(0).Rows(0)("TPT_PROMO_DESCR").ToString()
                lblPromoPeriodVal.Text = ds.Tables(0).Rows(0)("TPT_PROMO_PERIOD_VALUE").ToString()
                lblPromoAmtVal.Text = Format(FeeCollection.GetDoubleVal(ds.Tables(0).Rows(0)("AMOUNT")), "#,##0.00")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    'End by vikranth on 30th Jan 2020
End Class
