Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studChangeAreaApprove_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000190") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    lblName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                    hfSHF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))
                    GetOldData()
                    GetData()
                    hfTCH_ID.Value = "0"
                    BindTrip(ddlonward, "Onward")
                    BindTrip(ddlReturn, "Return")
                    txtApprove.Text = Format(Now.Date, "dd/MMM/yyyy")

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetOldData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  distinct  isnull(B.LOC_DESCRIPTION,''),isnull(D.SBL_DESCRIPTION,''),isnull(C.PNT_DESCRIPTION,'')," _
                                 & " isnull(E.LOC_DESCRIPTION,''),isnull(F.SBL_DESCRIPTION,''),isnull(G.PNT_DESCRIPTION,'')" _
                                 & " FROM  STUDENT_M AS A " _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
                                 & " WHERE STU_ID =" + hfSTU_ID.Value.ToString




        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim lis(7) As Integer
        While reader.Read
            With reader
                lblPlocation.Text = .GetString(0)
                lblPSublocation.Text = .GetString(1)
                lblPPickup.Text = .GetString(2)
                lblDlocation.Text = .GetString(3)
                lblDsublocation.Text = .GetString(4)
                lblDPickup.Text = .GetString(5)
              
            End With
        End While
        reader.Close()
    End Sub


    Sub GetData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  distinct  isnull(B.LOC_DESCRIPTION,''),isnull(D.SBL_DESCRIPTION,''),isnull(C.PNT_DESCRIPTION,'')," _
                                 & " isnull(E.LOC_DESCRIPTION,''),isnull(F.SBL_DESCRIPTION,''),isnull(G.PNT_DESCRIPTION,''),TCH_STARTDATE,TCH_REMARKS ," _
                                 & " ISNULL(D.SBL_ID,0),ISNULL(F.SBL_ID,0),ISNULL(C.PNT_ID,0),ISNULL(G.PNT_ID,0),ISNULL(TCH_OLDROUNDTRIP,0),ISNULL(TCH_ROUNDTRIP,0)" _
                                 & " FROM  STUDENT_CHANGEAREAREQ_S AS A " _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.TCH_PNT_ID_PICKUP" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.TCH_PNT_ID_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
                                 & " WHERE TCH_STU_ID =" + hfSTU_ID.Value.ToString + " AND TCH_bAPPROVE IS NULL"




        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim lis(7) As Integer
        While reader.Read
            With reader
                txtPLocation.Text = .GetString(0)
                txtPsublocation.Text = .GetString(1)
                txtPPickup.Text = .GetString(2)
                txtDLocation.Text = .GetString(3)
                txtDSublocation.Text = .GetString(4)
                txtDPickup.Text = .GetString(5)
                lblDate.Text = Format(.GetDateTime(6), "dd/MMM/yyyy")
                txtReqRemarks.Text = .GetString(7)
                hfPSBL_ID.Value = reader.GetValue(8)
                hfDSBL_ID.Value = reader.GetValue(9)
                hfPPNT_ID.Value = reader.GetValue(10)
                hfDPNT_ID.Value = reader.GetValue(11)
                lblOldTripValue.Text = reader.GetValue(12)
                lblTripValue.Text = reader.GetValue(13)
            End With
        End While
        reader.Close()
        If lblOldTripValue.Text = "0" Then
            lblOldTripType.Text = "RoundTrip"
        ElseIf lblOldTripValue.Text = "1" Then
            lblOldTripType.Text = "Pick Up"
        ElseIf lblOldTripValue.Text = "2" Then
            lblOldTripType.Text = "Drop Off"
        End If

        If lblTripValue.Text = "0" Then
            lblTripType.Text = "RoundTrip"
        ElseIf lblTripValue.Text = "1" Then
            lblTripType.Text = "Pick Up"
        ElseIf lblTripValue.Text = "2" Then
            lblTripType.Text = "Drop Off"
        End If

        If CInt(lblOldTripValue.Text) <> CInt(lblTripValue.Text) Then
            trOldTripType.Visible = True
        Else
            trOldTripType.Visible = False
        End If
    End Sub

    Function CheckFutureDatedDiscontinuation() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "select TOP 1 ISNULL(SSV_TODATE,'01/01/1900') from student_services_d where ssv_stu_id=" + hfSTU_ID.Value _
                                & " AND SSV_SVC_ID=1 ORDER BY SSV_FROMDATE DESC "
        Dim todate As DateTime = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return Format(todate, "dd/MMM/yyyy")
    End Function

    Sub SaveData(ByVal bApprove As Boolean)
        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If bApprove = True Then
                    UtilityObj.InsertAuditdetails(transaction, ViewState("datamode"), "OASIS.DBO.STUDENT_SERVICES_D", "SSV_ID", "SSV_STU_ID", " SSV_TODATE IS NULL AND SSV_SVC_ID=1 AND SSV_STU_ID=" + hfSTU_ID.Value)
                End If

                Dim todate As String = CheckFutureDatedDiscontinuation()

                If todate = "01/Jan/1900" Then
                    Dim str_query As String = " EXEC TRANSPORT.saveSTUDCHANGEAREAAPPROVAL " _
                                             & hfSTU_ID.Value + "," _
                                             & "'" + bApprove.ToString + "'," _
                                             & "'" + txtRemarks.Text + "'," _
                                             & IIf(ddlonward.SelectedValue = "0", "NULL", ddlonward.SelectedValue.ToString) + "," _
                                             & IIf(ddlReturn.SelectedValue = "0", "NULL", ddlReturn.SelectedValue.ToString) + "," _
                                             & "'" + txtApprove.Text.Replace("'", "''") + "'," _
                                             & "'" + Session("sUsr_name") + "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"
                Else
                    lblError.Text = "Request could not be processed as transport is discontinued from " + todate
                    transaction.Rollback()
                End If
                btnApprove.Enabled = False
                btnReject.Enabled = False
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                If ex.Message.ToLower = "can not update..charge for a future term exists. error while charging fee" Then
                    lblError.Text = "Request could not be processed..Charge for a future term exists."
                Else
                    lblError.Text = "Request could not be processed"
                End If
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
    Private Function APPROVE_TRANSPORT_CHANGEAREA_REQ(ByVal bApprove As Boolean) As Integer
        APPROVE_TRANSPORT_CHANGEAREA_REQ = 1
        lblError.Text = ""
        Dim conn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        conn.Open()
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction()
        
        Try
            Dim SSV_PICKUP_TRD_ID As Object, SSV_DROPOFF_TRD_ID As Object, TCH_APRDATE As Object
            SSV_PICKUP_TRD_ID = IIf(ddlonward.SelectedValue = "0", DBNull.Value, ddlonward.SelectedValue.ToString)
            SSV_DROPOFF_TRD_ID = IIf(ddlReturn.SelectedValue = "0", DBNull.Value, ddlReturn.SelectedValue.ToString)
            TCH_APRDATE = IIf(IsDate(txtApprove.Text), CDate(txtApprove.Text), DBNull.Value)

            Dim cmd As SqlCommand
            cmd = New SqlCommand("TRANSPORT.saveSTUDCHANGEAREAAPPROVAL", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@TCH_STU_ID", SqlDbType.Int)
            sqlp1.Value = hfSTU_ID.Value
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@TCH_bAPPROVE", SqlDbType.Bit)
            sqlp2.Value = bApprove
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@TCH_APRREMARKS", SqlDbType.VarChar, 255)
            sqlp3.Value = txtRemarks.Text.Replace("'", "''")
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@SSV_PICKUP_TRD_ID", SqlDbType.Int)
            sqlp4.Value = SSV_PICKUP_TRD_ID
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@SSV_DROPOFF_TRD_ID", SqlDbType.Int)
            sqlp5.Value = SSV_DROPOFF_TRD_ID
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@TCH_APRDATE", SqlDbType.DateTime)
            sqlp6.Value = TCH_APRDATE
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@TCH_APRUSR", SqlDbType.VarChar, 100)
            sqlp7.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlp7)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            APPROVE_TRANSPORT_CHANGEAREA_REQ = retSValParam.Value
            If retSValParam.Value = 0 Then
                trans.Commit()
                lblError.Text = "Request " & IIf(bApprove = True, "Approved", "Rejected") & " successfully"
            Else
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(APPROVE_TRANSPORT_CHANGEAREA_REQ)
            End If
        Catch ex As Exception
            trans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function
    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        'If journey = "Onward" Then
        '    str_query = " SELECT DISTINCT TRD_ID,TRP_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
        '                & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
        '                & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
        '                & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_PNT_ID=" + hfPPNT_ID.Value _
        '                & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL "
        'Else

        '    str_query = " SELECT DISTINCT TRD_ID,TRP_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
        '         & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
        '         & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
        '         & " WHERE A.TRP_JOURNEY='Return' AND TPP_PNT_ID=" + hfDPNT_ID.Value _
        '         & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL"


        'End If

        If journey = "Onward" Then
            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_PNT_ID=" + hfPPNT_ID.Value _
                        & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_PNT_ID=" + hfDPNT_ID.Value _
                 & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL"



        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"

        ddlTrip.Items.Insert(0, li)

        For Each li In ddlTrip.Items
            li.Attributes.Add("title", li.Text)
        Next
    End Sub

    Function CheckStartDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND '" + Format(Date.Parse(lblDate.Text), "yyyy-MM-dd") + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function
#End Region

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim lintCheckRate
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Try
            If studClass.checkFeeClosingDate(Session("sbsuid"), CType(hfACD_ID.Value, Integer), Date.Parse(lblDate.Text)) = True Then
                If CheckStartDate() = False Then
                    Exit Sub
                End If
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))

                Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_CHECK_RATE", pParms)
                    While reader.Read
                        lintCheckRate = Convert.ToString(reader("BSU_bCheckRate"))
                    End While
                End Using


                If lintCheckRate = 1 Then
                    If studClass.CheckRateExists(Date.Parse(lblDate.Text), hfPSBL_ID.Value, hfACD_ID.Value) = False Then
                        lblError.Text = "Request could not be processed as rate is not set for " + txtPsublocation.Text
                        Exit Sub
                    End If
                End If


                'SaveData(True)
                '  lblError.Text = "Record Saved Successfully"
                APPROVE_TRANSPORT_CHANGEAREA_REQ(True)
                'ViewState("datamode") = Encr_decrData.Encrypt("view")
                'ViewState("MainMnu_code") = Encr_decrData.Encrypt("T100190")
                'Dim url As String = String.Format("~\Transport\studTPTEdit_M.aspx?MainMnu_code={0}&datamode={1}&stuid=" + Encr_decrData.Encrypt(hfSTU_ID.Value) + "&shfid=" + Encr_decrData.Encrypt(hfSHF_ID.Value) + "&sen=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&name=" + Encr_decrData.Encrypt(lblName.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                'Response.Redirect(url)
            Else
                lblError.Text = "The service strarting date has to be higher than the fee closing date"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            'SaveData(False)
            'lblError.Text = "Record Saved Successfully"
            APPROVE_TRANSPORT_CHANGEAREA_REQ(False)
            btnApprove.Enabled = False
            btnReject.Enabled = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

  
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

   
End Class
