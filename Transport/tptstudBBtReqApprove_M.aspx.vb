Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.IO
Partial Class Transport_tptstudBBtReqApprove_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'UpLoadPhoto()
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                hfSave.Value = "0"
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                gvOnward.Visible = False
                gvReturn.Visible = False

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000260") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'FUUploadEmpPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfBSU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("bsuid").Replace(" ", "+"))
                    hfAcaID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    'BindArea(ddlPArea)
                    'BindArea(ddlDArea)
                    BindAcademicYear()
                    If Not ddlAcademicYear.Items.FindByText(hfAcaID.Value) Is Nothing Then
                        ddlAcademicYear.ClearSelection()
                        ddlAcademicYear.Items.FindByText(hfAcaID.Value).Selected = True
                    End If
                    BindGrade()
                    BindEmirate()
                    hfBBT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("bbtid").Replace(" ", "+"))

                    GetData(hfBBT_ID.Value)
                    GetData()
                    EnableDisableControls(False)
                    txtApprove.Text = Format(Now.Date, "dd/MMM/yyyy")
                    'btnPrint.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
                lblError.Visible = True
            End Try

        Else



        End If
    End Sub

    Sub ImgHeightnWidth(ByVal strImgPath As String)

        Dim actWidth, actHeight, imgWith, imgHeight As Integer
        Dim i As System.Drawing.Image = System.Drawing.Image.FromFile(strImgPath)
        imgWith = 202 'imgEmpImage.Width.Value
        imgHeight = 189 'imgEmpImage.Height.Value
        actWidth = i.Width
        actHeight = i.Height
        i.Dispose()
        Dim itempUnit As Integer
        If actWidth < imgWith And actHeight < imgHeight Then
            imgEmpImage.Height = actHeight
            imgEmpImage.Width = actWidth
            Exit Sub
        End If
        If actWidth < actHeight Then
            itempUnit = actWidth * imgHeight / actHeight
            If (imgWith > itempUnit) Then
                imgEmpImage.Width = itempUnit
            End If
        Else
            itempUnit = actHeight * imgWith / actWidth
            If (imgHeight > itempUnit) Then
                imgEmpImage.Height = itempUnit
            End If
        End If
    End Sub
    Private Function GetExtension(ByVal FileName As String) As String
        Dim Extension As String
        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)
        Return Extension
    End Function
    Function ContainsSpecialChars(s As String) As Boolean
        Return s.IndexOfAny("[~`!@#$%^&*()-+=|{}':;,<>/?]".ToCharArray) <> -1
    End Function
    Private Sub UpLoadPhoto()
        'ViewState("PhotoPath") = ""
        lblError.Text = ""
        If FUUploadEmpPhoto.FileName <> "" And ViewState("PhotoPath") <> FUUploadEmpPhoto.FileName Then
            Dim extention = GetExtension(FUUploadEmpPhoto.FileName)
            If extention <> "" Then
                'If extention.ToUpper <> "JPEG" Or extention.ToUpper <> "JPG" Or extention.ToUpper <> "PNG" Then
                If Not (extention.ToUpper = "JPEG" Or extention.ToUpper = "JPG" Or extention.ToUpper = "PNG") Then
                    lblError.Text = "Upload JPEG/JPG/PNG Files Only!!"
                    Exit Sub
                End If
            End If
            ' added by mahesh on 25-06-2018
            If extention.Length = 0 Then
                lblError.Text = "file with out extension not allowed...!!"
                Exit Sub
            End If
            Dim ContainsSplChar = ContainsSpecialChars(FUUploadEmpPhoto.FileName)
            If ContainsSplChar = True Then
                lblError.Text = "File Name with special characters are not allowed..!!"
                Exit Sub
            End If

            Dim FileNameLength = FUUploadEmpPhoto.FileName.Length
            If FileNameLength = 0 Or FileNameLength > 255 Then
                lblError.Text = "Error with file Name Length..!!"
                Exit Sub
            End If

            Dim s As String = FUUploadEmpPhoto.FileName
            Dim result() As String
            result = s.Split(".")

            If result.Length > 2 Then
                lblError.Text = "Invalid file Name..!!"
                Exit Sub
            End If
            If FUUploadEmpPhoto.PostedFile.ContentLength > 50000 Then
                'imgEmpImage.ImageUrl = "~/Images/Photos/no_image.gif"
                lblError.Text = "The Size of file is greater than 50 KB"
                lblError.Visible = True
                Exit Sub
            Else
                hfImageName.Value = FUUploadEmpPhoto.FileName.ToString
                'Label1.Text = FUUploadEmpPhoto.PostedFile.ContentLength.ToString
                'lblImageError.Visible = False
                Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString

                Dim fs As New FileInfo(FUUploadEmpPhoto.PostedFile.FileName)

                If Not Directory.Exists(str_img & "\STSBBT") Then
                    Directory.CreateDirectory(str_img & "\STSBBT")
                End If

                Dim str_tempfilename As String = AccountFunctions.GetRandomString() & FUUploadEmpPhoto.FileName

                'Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
                Dim strFilepath As String = str_img & "\STSBBT\" & str_tempfilename
                If FUUploadEmpPhoto.FileName <> "" Then
                    FUUploadEmpPhoto.PostedFile.SaveAs(strFilepath)
                    ImgHeightnWidth(strFilepath)
                    'hfEmpImagePath.Value = strFilepath
                    ViewState("PhotoPath") = strFilepath
                    Try
                        If Not FUUploadEmpPhoto.PostedFile.ContentType.Contains("image") Then
                            Throw New Exception
                        End If
                        imgEmpImage.ImageUrl = str_imgvirtual & "/STSBBT/" & str_tempfilename & "?" & DateTime.Now.Ticks.ToString()
                        ViewState("PhotoPath") = strFilepath
                    Catch ex As Exception
                        'File.Delete(strFilepath)
                        ViewState("PhotoPath") = ""
                        UtilityObj.Errorlog("No Image found")
                        'imgEmpImage.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                        imgEmpImage.AlternateText = "No Image found"
                    End Try
                End If
            End If
        End If
    End Sub

    Sub GetData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim trdPickup As Integer
        Dim trdDropoff As Integer

        Dim str_query As String = "SELECT  distinct  isnull(B.LOC_DESCRIPTION,''),isnull(D.SBL_DESCRIPTION,'OTHER'),isnull(C.PNT_DESCRIPTION,'OTHER')," _
                                 & " isnull(E.LOC_DESCRIPTION,''),isnull(F.SBL_DESCRIPTION,'OTHER'),isnull(G.PNT_DESCRIPTION,'OTHER'),BBT_ID," _
                                 & " isnull(D.SBL_ID,0),isnull(F.SBL_ID,0),BBT_STARTDATE,ISNULL(C.PNT_ID,0),ISNULL(G.PNT_ID,0), " _
                                 & " ISNULL(BBT_STU_FIRSTNAME,'')+' '+ISNULL(BBT_STU_MIDNAME,' ')+ISNULL(BBT_STU_LASTNAME,' ') AS BBT_NAME," _
                                 & " GRM_DISPLAY,GRM_SHF_ID,GRM_ACD_ID,ISNULL(BBT_EMAIL,'') AS BBT_EMAIL," _
                                 & " ISNULL(BBT_TITLE,'')+' '+ISNULL(BBT_PFIRSTNAME,'')+' '+ISNULL(BBT_PMIDNAME,'')+' '+ISNULL(BBT_PLASTNAME,'') AS PNAME,ISNULL(BBT_REGFROM,'bbt')," _
                                 & " ISNULL(BBT_STU_ID,0), " _
                                 & " ISNULL(P.GRM_BSU_ID,'')," _
                                 & " ISNULL(A.BBT_OTHERPICKUP,''), ISNULL(A.BBT_OTHERDROPOFF,''), ISNULL(A.BBT_IMAGE_LOC,''),GRM_ID" _
                                 & " FROM  STUDENT_BBTREQ_S AS A " _
                                 & " INNER  JOIN OASIS.DBO.GRADE_BSU_M AS P ON A.BBT_GRM_ID=P.GRM_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.BBT_PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.BBT_PNT_ID_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
                                 & " WHERE BBT_ID =" + hfBBT_ID.Value.ToString



        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                lblPlocation.Text = .GetString(0)
                Dim length As Integer = .GetString(23).Length
                If length <> 0 Then
                    Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                    Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                    Dim imagePath As String = .GetString(23).Replace(str_img, str_imgvirtual).Replace("\", "/")
                    imgEmpImage.ImageUrl = imagePath & "?" & DateTime.Now.Ticks.ToString() '.GetString(23)
                End If

                If .GetString(23) <> "" Then
                    ViewState("PhotoPath") = .GetString(23)
                Else
                    ViewState("PhotoPath") = ""
                End If
                'ViewState("PhotoPath") = hfEmpImagePath.Value
                lblDlocation.Text = .GetString(3)
                ddlTripType.SelectedIndex = 1
                hfGRM_ID.Value = .GetValue(24)

                If .GetString(1) = "OTHER" Then

                    If .GetString(21) <> "" Then
                        lblPickupArea.BackColor = Drawing.Color.Red
                        lblPickupArea.ForeColor = Drawing.Color.White
                        lblPickupArea.Text = .GetString(21)
                        lblPickupPoint.Text = ""
                        BindPickupAreaForAdmin(.GetString(1), .GetString(20))

                        ddlAreaForAdmin.Enabled = True
                        ddlonward.Enabled = True
                        ddlPickUpChoose.Enabled = True
                        lnKPickup.Enabled = True
                        rfAdminArea.Enabled = True
                        rfAdminPickUpPoint.Enabled = True
                    Else
                        ddlTripType.SelectedIndex = 3
                        lblPickupArea.Text = "DROPOFFONLY"
                        lblPickupPoint.Text = "DROPOFFONLY"
                        rfAdminArea.Enabled = False
                        rfAdminPickUpPoint.Enabled = False
                        ddlAreaForAdmin.Enabled = False
                        ddlPickUpChoose.Enabled = False
                        ddlonward.Enabled = False
                        lnKPickup.Enabled = False
                    End If

                Else
                    lblPickupArea.Text = .GetString(1)
                    lblPickupPoint.Text = .GetString(2)
                    BindPickupAreaForAdmin(.GetString(1), .GetString(20))
                End If

                If .GetString(4) = "OTHER" Then
                    If .GetString(22) <> "" Then
                        lblDropoffArea.BackColor = Drawing.Color.Red
                        lblDropoffArea.ForeColor = Drawing.Color.White
                        lblDropoffArea.Text = .GetString(22)
                        lblDropoffPoint.Text = ""
                        BindDropoffAreaForAdmin(.GetString(4), .GetString(20))
                        ddlDropAreaForAdmin.Enabled = True
                        ddlReturn.Enabled = True
                        ddlDropoffChoose.Enabled = True
                        lnkDropOff.Enabled = True
                        rfAdminDropArea.Enabled = True
                        rfAdminDropoffPoint.Enabled = True
                    Else
                        lblDropoffArea.Text = "PICKUPONLY"
                        lblDropoffPoint.Text = "PICKUPONLY"
                        ddlTripType.SelectedIndex = 2
                        ddlDropAreaForAdmin.Enabled = False
                        ddlReturn.Enabled = False
                        ddlDropoffChoose.Enabled = False
                        lnkDropOff.Enabled = False
                        rfAdminDropArea.Enabled = False
                        rfAdminDropoffPoint.Enabled = False
                    End If


                Else
                    lblDropoffArea.Text = .GetString(4)
                    lblDropoffPoint.Text = .GetString(5)
                    BindDropoffAreaForAdmin(.GetString(4), .GetString(20))
                End If

                hfPSBL_ID.Value = .GetValue(7)
                hfDSBL_ID.Value = .GetValue(8)
                hfPPNT_ID.Value = .GetValue(10)
                hfDPNT_ID.Value = .GetValue(11)
                hfSHF_ID.Value = .GetValue(14)
                hfACD_ID.Value = .GetValue(15)
                hfEmail.Value = .GetValue(16)
                hfParent.Value = .GetString(17)
                hfReg.Value = .GetString(18)

                'to avoid duplicate entry
                If reader.GetValue(19) <> 0 Then
                    'minu
                    'btnApprove.Enabled = False
                    'btnReject.Enabled = False
                End If
                'minu
                'txtStsRemarks.Text = reader.GetString(20)

            End With
        End While
        reader.Close()


        If Not ddlGrade.Items.FindByValue(hfGRM_ID.Value) Is Nothing Then
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByValue(hfGRM_ID.Value).Selected = True
        End If

        BindPickup(ddlPickUpChoose, hfPSBL_ID.Value)
        BindPickup(ddlDropoffChoose, hfDSBL_ID.Value)

        If Not ddlPickUpChoose.Items.FindByValue(hfPPNT_ID.Value) Is Nothing Then
            ddlPickUpChoose.Items.FindByValue(hfPPNT_ID.Value).Selected = True
        End If

        If Not ddlDropoffChoose.Items.FindByValue(hfDPNT_ID.Value) Is Nothing Then
            ddlDropoffChoose.Items.FindByValue(hfDPNT_ID.Value).Selected = True
        End If

        If ddlPickUpChoose.SelectedValue.ToString = "" Then
            ddlonward.Items.Clear()
            ddlonward.Items.Insert(0, New ListItem("--", "0"))
        Else
            BindTrip(ddlonward, "Onward")
        End If

        If ddlDropoffChoose.SelectedValue.ToString = "" Then
            ddlReturn.Items.Clear()
            ddlReturn.Items.Insert(0, New ListItem("--", "0"))
        Else
            BindTrip(ddlReturn, "Return")
        End If

        If Not ddlonward.Items.FindByValue(trdPickup) Is Nothing Then
            ddlonward.Items.FindByValue(trdPickup).Selected = True
        End If

        If Not ddlReturn.Items.FindByValue(trdDropoff) Is Nothing Then
            ddlReturn.Items.FindByValue(trdDropoff).Selected = True
        End If

    End Sub

    Public Shared Function GetAreaForAdmin(ByVal defaultArea As String) As SqlDataReader

        Dim sqlArea As String = "SELECT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M " _
                                   & " WHERE SBL_DESCRIPTION = '" & defaultArea & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection()
        Dim command As SqlCommand = New SqlCommand(sqlArea, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Sub BindDropoffAreaForAdmin(ByVal defaultArea As String, ByVal bsuId As String)
        Dim i As Integer
        Dim strBaseDescr As String
        Dim strBaseID As String
        Dim counter As Integer = 0
        ddlDropAreaForAdmin.Items.Clear()
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A INNER JOIN " _
                                & " TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                & " WHERE PNT_BSU_ID='" + bsuId + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStrTpt, CommandType.Text, str_query)
        ddlDropAreaForAdmin.DataSource = ds
        ddlDropAreaForAdmin.DataTextField = "SBL_DESCRIPTION"
        ddlDropAreaForAdmin.DataValueField = "SBL_ID"
        ddlDropAreaForAdmin.DataBind()
        ddlDropAreaForAdmin.Items.Insert(0, New ListItem("--", "0"))

        If defaultArea <> "" Then

            Using AreaReader As SqlDataReader = GetAreaForAdmin(defaultArea)
                While AreaReader.Read
                    strBaseDescr = Convert.ToString(AreaReader("SBL_DESCRIPTION"))
                    strBaseID = Convert.ToString(AreaReader("SBL_ID"))

                    ddlDropAreaForAdmin.Items.Insert(1, New ListItem(strBaseDescr, strBaseID))
                    ddlDropAreaForAdmin.SelectedIndex = 1
                End While
            End Using
            For i = 0 To ddlDropAreaForAdmin.Items.Count - 1
                If ddlDropAreaForAdmin.Items.Item(i).Value = strBaseID Then
                    counter = counter + 1
                    If counter = 2 Then
                        ddlDropAreaForAdmin.Items.RemoveAt(i)
                        Exit For
                    End If

                End If
            Next i
        End If
        Dim count As Integer
        count = ddlDropAreaForAdmin.Items.Count
        ddlDropAreaForAdmin.Items.Insert(count, New ListItem("Other", "1"))

    End Sub


    Sub BindPickupAreaForAdmin(ByVal defaultArea As String, ByVal bsuId As String)
        Dim i As Integer
        Dim strBaseDescr As String
        Dim strBaseID As String
        Dim counter As Integer = 0
        ddlAreaForAdmin.Items.Clear()
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A INNER JOIN " _
                                & " TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                & " WHERE PNT_BSU_ID='" + bsuId + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStrTpt, CommandType.Text, str_query)
        ddlAreaForAdmin.DataSource = ds
        ddlAreaForAdmin.DataTextField = "SBL_DESCRIPTION"
        ddlAreaForAdmin.DataValueField = "SBL_ID"
        ddlAreaForAdmin.DataBind()
        ddlAreaForAdmin.Items.Insert(0, New ListItem("--", "0"))
        If defaultArea <> "" Then

            Using AreaReader As SqlDataReader = GetAreaForAdmin(defaultArea)
                While AreaReader.Read
                    strBaseDescr = Convert.ToString(AreaReader("SBL_DESCRIPTION"))
                    strBaseID = Convert.ToString(AreaReader("SBL_ID"))

                    ddlAreaForAdmin.Items.Insert(1, New ListItem(strBaseDescr, strBaseID))
                    ddlAreaForAdmin.SelectedIndex = 1
                End While
            End Using

            For i = 0 To ddlAreaForAdmin.Items.Count - 1
                If ddlAreaForAdmin.Items.Item(i).Value = strBaseID Then
                    counter = counter + 1
                    If counter = 2 Then
                        ddlAreaForAdmin.Items.RemoveAt(i)
                        Exit For
                    End If

                End If
            Next i

        End If

        Dim count As Integer
        count = ddlAreaForAdmin.Items.Count
        ddlAreaForAdmin.Items.Insert(count, New ListItem("Other", "1"))

    End Sub

    Sub BindPickup(ByVal ddlPickup As DropDownList, ByVal area As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        ddlPickup.Items.Clear()
        Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M WHERE PNT_BSU_ID='" + hfBSU_ID.Value + "'" _
                        & " AND PNT_SBL_ID=" + area
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPickup.DataSource = ds
        ddlPickup.DataTextField = "PNT_DESCRIPTION"
        ddlPickup.DataValueField = "PNT_ID"
        ddlPickup.DataBind()
        ddlPickup.Items.Insert(0, New ListItem("--", "0"))
    End Sub

    Sub BindTrip(ByVal ddlTrip As DropDownList, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        If journey = "Onward" Then
            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                        & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                        & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                        & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                        & " WHERE  A.TRP_JOURNEY='Onward' AND TPP_PNT_ID=" + ddlPickUpChoose.SelectedValue.ToString _
                        & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL " _
                        & " ORDER BY BNO_DESCR,TRP_DESCR"
        Else

            str_query = " SELECT DISTINCT TRD_ID,BNO_DESCR+'-'+TRP_DESCR AS TRP_DESCR,BNO_DESCR FROM TRANSPORT.TRIPS_M AS A INNER JOIN " _
                 & " TRANSPORT.TRIPS_D AS B ON B.TRD_TRP_ID=A.TRP_ID INNER JOIN" _
                 & " TRANSPORT.TRIPS_PICKUP_S AS C ON B.TRD_ID=C.TPP_TRD_ID " _
                 & " INNER JOIN TRANSPORT.BUSNOS_M AS D ON B.TRD_BNO_ID=D.BNO_ID" _
                 & " WHERE A.TRP_JOURNEY='Return' AND TPP_PNT_ID=" + ddlDropoffChoose.SelectedValue.ToString _
                 & " AND A.TRP_SHF_ID=" + hfSHF_ID.Value + " AND TRD_TODATE IS NULL" _
                 & " ORDER BY BNO_DESCR,TRP_DESCR"


        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrip.DataSource = ds
        ddlTrip.DataTextField = "TRP_DESCR"
        ddlTrip.DataValueField = "TRD_ID"
        ddlTrip.DataBind()

        Dim li As New ListItem

        li.Text = "--"
        li.Value = "0"

        ddlTrip.Items.Insert(0, li)

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    ReadOnly Property conStrTpt() As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ToString
        End Get
    End Property

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + hfBSU_ID.Value + "'" _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + hfBSU_ID.Value + "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.ClearSelection()
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True

    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        ddlGrade.Items.Clear()
        Dim str_query As String = "SELECT GRM_ID,GRM_DISPLAY FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_ID"
        ddlGrade.DataBind()
    End Sub

    Sub GetData(ByVal bbtid As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT ISNULL(BBT_STU_FIRSTNAME,''),ISNULL(BBT_STU_MIDNAME,'')," _
                                & " ISNULL(BBT_STU_LASTNAME,''),BBT_STU_DOB,BBT_STU_GENDER,BBT_STARTDATE," _
                                & " BBT_SBL_ID,BBT_PNT_ID,BBT_SBL_ID_DROPOFF,BBT_PNT_ID_DROPOFF,BBT_PRIMARYCONTACT," _
                                & " BBT_TITLE,ISNULL(BBT_PFIRSTNAME,''),ISNULL(BBT_PMIDNAME,''),ISNULL(BBT_PLASTNAME,'')," _
                                & " BBT_POBOX,BBT_EMIRATE,BBT_ADDRESS,BBT_OFFPHONE,BBT_RESPHONE,BBT_MOBILEPHONE,BBT_EMAIL," _
                                & " BBT_ALTEMAIL,BBT_PICKUPLOCATION,ISNULL(BBT_REGFROM,'bbt'),GRM_ACD_ID, " _
                                & " ISNULL(BBT_MEDICAL_CONDITION,'NA'), ISNULL(BBT_MOTHER_MOBILE,''),ISNULL(BBT_EMERGENCY_MOBILE,''),ISNULL(BBT_HOUSE_NO,''), " _
                                & " GRM_ID FROM  STUDENT_BBTREQ_S AS A " _
                                 & " INNER  JOIN OASIS.DBO.GRADE_BSU_M AS P ON A.BBT_GRM_ID=P.GRM_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.BBT_PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.BBT_PNT_ID_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
                                 & " WHERE BBT_ID =" + bbtid

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            With reader
                txtFirstName.Text = .GetString(0)
                txtMidName.Text = .GetString(1)
                txtLastName.Text = .GetString(2)
                txtDob.Text = Format(.GetDateTime(3), "dd/MMM/yyyy")
                hfACD_ID.Value = .GetValue(25)
                txtMedCond.Text = .GetString(26)
                txtMotherMob.Text = .GetString(27)
                txtEmergencyMob.Text = .GetString(28)
                txtHouseNo.Text = .GetString(29)
                If .GetString(4) = "M" Then
                    rdMale.Checked = True
                Else
                    rdFemale.Checked = True
                End If
                txtFrom.Text = Format(.GetDateTime(5), "dd/MMM/yyyy")
                'If Not ddlPArea.Items.FindByValue(.GetValue(6)) Is Nothing Then
                '    ddlPArea.ClearSelection()
                '    ddlPArea.Items.FindByValue(.GetValue(6)).Selected = True
                'End If
                'BindPickup(ddlPArea, ddlPickup)

                'If Not ddlPickup.Items.FindByValue(.GetValue(7)) Is Nothing Then
                '    ddlPickup.ClearSelection()
                '    ddlPickup.Items.FindByValue(.GetValue(7)).Selected = True
                'End If

                'If Not ddlDArea.Items.FindByValue(.GetValue(8)) Is Nothing Then
                '    ddlDArea.ClearSelection()
                '    ddlDArea.Items.FindByValue(.GetValue(8)).Selected = True
                'End If

                'BindPickup(ddlDArea, ddlDropoff)

                'If Not ddlDropoff.Items.FindByValue(.GetValue(9)) Is Nothing Then
                '    ddlDropoff.ClearSelection()
                '    ddlDropoff.Items.FindByValue(.GetValue(9)).Selected = True
                'End If
                If .GetString(10) = "F" Then
                    rdFather.Checked = True
                ElseIf .GetString(10) = "M" Then
                    rdMother.Checked = True
                Else
                    rdGuardian.Checked = True
                End If

                If Not ddlTitle.Items.FindByValue(.GetString(11)) Is Nothing Then
                    ddlTitle.ClearSelection()
                    ddlTitle.Items.FindByValue(.GetString(11)).Selected = True
                End If
                txtPFirstName.Text = .GetString(12)
                txtPMidName.Text = .GetString(13)
                txtPLastName.Text = .GetString(14)

                txtPoBox.Text = .GetString(15)
                If Not ddlEmirate.Items.FindByValue(.GetString(16)) Is Nothing Then
                    ddlEmirate.Items.FindByValue(.GetString(16)).Selected = True
                End If
                txtAddress.Text = .GetString(17)
                txtOffPhone.Text = .GetString(18)
                txtResPhone.Text = .GetString(19)
                txtMobile.Text = .GetString(20)

                txtEmail.Text = .GetString(21)
                txtAltEmail.Text = .GetString(22)
                txtLocation.Text = .GetString(23)
                hfReg.Value = .GetString(24)
                Dim txtgrade As String = .GetValue(30)
                Dim xount As Integer = ddlGrade.Items.Count

            End With
        End While
    End Sub

    Sub BindArea(ByVal ddlArea As DropDownList)
        ddlArea.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT DISTINCT SBL_ID,SBL_DESCRIPTION FROM TRANSPORT.SUBLOCATION_M AS A INNER JOIN " _
                                & " TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID WHERE PNT_BSU_ID='" + hfBSU_ID.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlArea.DataSource = ds
        ddlArea.DataTextField = "SBL_DESCRIPTION"
        ddlArea.DataValueField = "SBL_ID"
        ddlArea.DataBind()

    End Sub
    Sub BindEmirate()
        Dim str_query As String = "SELECT EMR_CODE,EMR_DESCR FROM EMIRATE_M"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
        ddlEmirate.DataBind()
    End Sub
    Sub BindPickup(ByVal ddlArea As DropDownList, ByVal ddlPickup As DropDownList)
        ddlPickup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT PNT_ID,PNT_DESCRIPTION FROM TRANSPORT.PICKUPPOINTS_M WHERE PNT_BSU_ID='" + hfBSU_ID.Value + "'" _
                        & " AND PNT_SBL_ID=" + ddlArea.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPickup.DataSource = ds
        ddlPickup.DataTextField = "PNT_DESCRIPTION"
        ddlPickup.DataValueField = "PNT_ID"
        ddlPickup.DataBind()
        Dim li As New ListItem
        li.Text = "Other"
        li.Value = "-1"
        ddlPickup.Items.Add(li)
    End Sub

    Sub SaveEditData()
        UpLoadPhoto()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim primaryContact As String = ""
        Dim gender As String = ""

        If rdFather.Checked = True Then
            primaryContact = "F"
        ElseIf rdMother.Checked = True Then
            primaryContact = "M"
        ElseIf rdGuardian.Checked = True Then
            primaryContact = "G"
        End If

        If rdMale.Checked = True Then
            gender = "M"
        Else
            gender = "F"
        End If

        Dim pickupArea As String
        Dim pickupPoint As String
        Dim dropoffArea As String
        Dim dropoffPoint As String

        If ddlAreaForAdmin.SelectedValue.ToString = "" Then
            pickupArea = -1
            pickupPoint = 0
        Else
            pickupArea = ddlAreaForAdmin.SelectedValue
            pickupPoint = ddlPickUpChoose.SelectedValue

        End If

        If ddlDropAreaForAdmin.SelectedValue.ToString = "" Then
            dropoffArea = -1
            dropoffPoint = 0
        Else
            dropoffArea = ddlDropAreaForAdmin.SelectedValue
            dropoffPoint = ddlDropoffChoose.SelectedValue
        End If

        Dim ptripId As String
        Dim dtripId As String

        If ddlonward.SelectedIndex = 0 Then
            ptripId = ""
        Else
            ptripId = ddlonward.SelectedItem.Value
        End If

        If ddlReturn.SelectedIndex = 0 Then
            dtripId = ""
        Else
            dtripId = ddlReturn.SelectedItem.Value
        End If

        Dim str_query As String = "exec UPDATESTUDBBTREQ " _
                               & "'" + hfBBT_ID.Value + "'," _
                               & "'" + ddlAcademicYear.SelectedValue + "'," _
                               & "'" + ddlGrade.SelectedValue + "'," _
                               & "'" + txtFirstName.Text + "'," _
                               & "'" + txtMidName.Text + "'," _
                               & "'" + txtLastName.Text + "'," _
                               & "'" + Format(Date.Parse(txtDob.Text), "yyyy-MM-dd") + "'," _
                               & "'" + gender + "'," _
                               & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'," _
                               & "'" + pickupArea + "'," _
                               & "'" + pickupPoint + "'," _
                               & "'" + dropoffArea + "'," _
                               & "'" + dropoffPoint + "'," _
                               & "'" + primaryContact + "'," _
                               & "'" + ddlTitle.SelectedValue + "'," _
                               & "'" + txtPFirstName.Text + "'," _
                               & "'" + txtPMidName.Text + "'," _
                               & "'" + txtPLastName.Text + "'," _
                               & "'" + txtPoBox.Text + "'," _
                               & "'" + ddlEmirate.SelectedItem.ToString + "'," _
                               & "'" + txtAddress.Text.Replace("'", "''") + "'," _
                               & "'" + txtOffPhone.Text + "'," _
                               & "'" + txtResPhone.Text + "'," _
                               & "'" + txtMobile.Text + "'," _
                               & "'" + txtEmail.Text + "'," _
                               & "'" + txtAltEmail.Text + "'," _
                               & "'" + txtLocation.Text + "'," _
                               & "'" + txtHouseNo.Text.Replace("'", "''") + "'," _
                               & "'" + txtMotherMob.Text + "'," _
                               & "'" + txtEmergencyMob.Text + "'," _
                               & "'" + txtMedCond.Text + "'," _
                               & "'" + ViewState("PhotoPath").ToString + "'," _
                               & "'" + ptripId + "'," _
                               & "'" + dtripId + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        'If hfImageLocation.Value <> "" Then

        '    Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        '    Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
        '    Dim fs As New FileInfo(hfImageLocation.Value.ToString)
        '    ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()
        '    ViewState("EMPPHOTOFILEPATH") = str_img & "\" & hfBsuId.Value.ToString & ""
        '    Dim dirInfoBSUID As DirectoryInfo = Nothing
        '    Dim dirInfoEMPLOYEE As DirectoryInfo = Nothing

        '    If Directory.Exists(ViewState("EMPPHOTOFILEPATH")) Then
        '        If Directory.Exists(ViewState("EMPPHOTOFILEPATH") & "\" & hfStuId.Value.ToString) Then
        '            Dim strFilePath As String = ViewState("EMPPHOTOFILEPATH") & "\" & hfStuId.Value.ToString & "\STUPHOTO" & fs.Extension

        '            If Not Directory.Exists(str_img & "\STSBBT") Then
        '                Directory.CreateDirectory(str_img & "\stsbbt")
        '            End If

        '            Dim str_tempfilename As String = AccountFunctions.GetRandomString() & fs.Name
        '            Dim strStuTrPath As String = str_img & "\STSBBT\" & str_tempfilename
        '            File.Copy(strFilePath, strStuTrPath, True)
        '        End If
        '    End If

        '    'If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH")) Then
        '    '    dirInfoBSUID = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH"))
        '    'End If
        '    'If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH") & "\" & hfStuId.Value.ToString) Then
        '    '    dirInfoEMPLOYEE = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH") & "\" & hfStuId.Value.ToString)
        '    'End If

        'End If

    End Sub

    Sub EnableDisableControls(ByVal value As Boolean)
        txtAddress.Enabled = value
        txtAltEmail.Enabled = value
        txtHouseNo.Enabled = value
        txtMotherMob.Enabled = value
        txtEmergencyMob.Enabled = value
        txtMedCond.Enabled = value
        txtDob.Enabled = value
        txtEmail.Enabled = value
        txtFirstName.Enabled = value
        txtFrom.Enabled = value
        txtLastName.Enabled = value
        txtLocation.Enabled = value
        txtMidName.Enabled = value
        txtMobile.Enabled = value
        txtOffPhone.Enabled = value
        txtPFirstName.Enabled = value
        txtPLastName.Enabled = value
        txtPMidName.Enabled = value
        txtPoBox.Enabled = value
        txtResPhone.Enabled = value
        ddlAcademicYear.Enabled = value
        lblPickupArea.Enabled = value
        ddlAreaForAdmin.Enabled = value
        ddlPickUpChoose.Enabled = value
        ddlonward.Enabled = value
        ddlDropAreaForAdmin.Enabled = value
        ddlDropoffChoose.Enabled = value
        ddlReturn.Enabled = value
        lblPickupPoint.Enabled = value
        ddlEmirate.Enabled = value
        ddlGrade.Enabled = value
        lblDropoffArea.Enabled = value
        lblDropoffPoint.Enabled = value
        ddlTitle.Enabled = value
        ddlTripType.Enabled = value
        FUUploadEmpPhoto.Enabled = value
    End Sub

    'Sub SaveStsApprove(ByVal value As Boolean)
    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    '    Dim str_query As String = "update student_bbtreq_s set BBT_bSTSAPPROVE='" + value.ToString + "'," _
    '                             & " BBT_STSAPRDATE='" + Format(Date.Parse(txtApprove.Text), "yyyy-MM-dd") + "'," _
    '                             & " BBT_STSREMARKS='" + txtRemarks.Text.Replace("'", "''") + "'," _
    '                             & " BBT_STSAPRUSER='" + Session("sUsr_name") + "'" _
    '                             & " where bbt_id=" + hfBBT_ID.Value
    '    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    'End Sub

    Sub SaveAoApprove(ByVal value As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "update student_bbtreq_s set BBT_bAOAPPROVE='" + value.ToString + "' where bbt_id=" + hfBBT_ID.Value
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub SendRegretEmail()
        Dim filepath As String = Server.MapPath("~/Transport/BBTAORegretEmail.htm")
        Dim fr As System.IO.TextReader
        fr = File.OpenText(filepath)
        Dim sr As String = fr.ReadToEnd
        fr.Close()

        Dim parent As String = ddlTitle.SelectedValue + " " + txtPFirstName.Text + " " + txtPMidName.Text + " " + txtPLastName.Text
        sr = sr.Replace("%parent%", parent)
        sr = sr.Replace("%child%", txtFirstName.Text + " " + txtMidName.Text + " " + txtLastName.Text)
        sr = sr.Replace("%grade%", ddlGrade.SelectedItem.Text)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSC_HOST,BSC_USERNAME,BSC_PASSWORD,BSC_PORT,BSC_FROMEMAIL FROM BSU_COMMUNICATION_M WHERE BSC_TYPE='STSREG' AND BSC_BSU_ID='900501'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim host As String = ""
        Dim fromId As String = ""
        Dim password As String = ""
        Dim userName As String = ""
        Dim port As Integer
        While reader.Read
            host = reader.GetString(0)
            userName = reader.GetString(1)
            password = reader.GetString(2)
            port = reader.GetValue(3)
            fromId = reader.GetString(4)
        End While
        reader.Close()

        sr = SetEmailHeader(sr)


        Dim retVal As String = ""
        Dim AlternatreMailBody As AlternateView

        AlternatreMailBody = convertToEmbedResource(sr, "1")
        retVal = SendNewsLetters(fromId, txtEmail.Text, "Regret letter for transport", AlternatreMailBody, userName, password, host, port, "1", False)
        '  retVal = SendPlainTextEmails(fromId, "dhanyaajith1001@gmail.com", "Confirmation of transport", sr, fromId, password, host, port, "0", False)

        If retVal = "Successfully send" Then
            lblError.Text = "Email sent successfully"
        Else
            lblError.Text = "Email could not be sent"
        End If
    End Sub

    Function SetEmailHeader(ByVal sr As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim address As String
        If hfReg.Value = "bbt" Then
            str_query = "exec getBsuInFo '900500','logo'"
            sr = sr.Replace("%img%", "Schools/BBT/BRIGHT BUS LOGO.JPG")
            sr = sr.Replace("%regunit%", "Bright Bus Transport")
        Else
            str_query = "exec getBsuInfo '900501','logo'"
            sr = sr.Replace("%img%", "Schools/STS/stslogo.jpg")
            sr = sr.Replace("%regunit%", "School Transport Services")
        End If

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            sr = sr.Replace("%bsu%", reader("BSU_NAME"))
            sr = sr.Replace("%pobox%", reader("BSU_POBOX"))
            address = "Tel :" + reader("BSU_TEL") + " ,Fax : " + reader("BSU_FAX") + " ,Email : " + reader("BSU_EMAIL")
            sr = sr.Replace("%address%", address)
            sr = sr.Replace("%url%", reader("BSU_URL"))
        End While
        reader.Close()
        Return sr
    End Function

    Public Shared Function SendNewsLetters(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As AlternateView, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
        Dim RetutnValue = ""
        Try

            Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

            msg.Subject = Subject

            msg.AlternateViews.Add(MailBody)

            msg.Priority = Net.Mail.MailPriority.High

            msg.IsBodyHtml = True


            Dim client As New System.Net.Mail.SmtpClient(Host, Port)

            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            client.Send(msg)

            RetutnValue = "Successfully send"

        Catch ex As Exception
            RetutnValue = "Error : " & ex.Message
        End Try

        Return RetutnValue

    End Function


    Public Function convertToEmbedResource(ByVal emailHtml$, ByVal Templateid As String) As AlternateView

        'This is the website where the resources are located
        Dim ResourceUrl As String = WebConfigurationManager.AppSettings("WebsiteURLResource").ToString
        Dim webSiteUrl$ = ResourceUrl + Templateid + "/"

        ' The first regex finds all the url/src tags.
        Dim matchesCol As MatchCollection = Regex.Matches(emailHtml, "url\(['|\""]+.*['|\""]\)|src=[""|'][^""']+[""|']")

        Dim normalRes As Match


        Dim resCol As AlternateView = AlternateView.CreateAlternateViewFromString("", Nothing, "text/html")

        Dim resId% = 0

        ' Between the findings
        For Each normalRes In matchesCol

            Dim resPath$

            ' Replace it for the new content ID that will be embeded
            If Left(normalRes.Value, 3) = "url" Then
                emailHtml = emailHtml.Replace(normalRes.Value, "url(cid:EmbedRes_" & resId & ")")
            Else
                emailHtml = emailHtml.Replace(normalRes.Value, "src=""cid:EmbedRes_" & resId & """")
            End If

            ' Clean the path
            resPath = Regex.Replace(normalRes.Value, "url\(['|\""]", "")
            resPath = Regex.Replace(resPath, "src=['|\""]", "")
            resPath = Regex.Replace(resPath, "['|\""]\)", "").Replace(webSiteUrl, "").Replace("""", "")


            '' Map it on the server
            resPath = Server.MapPath(resPath)

            resPath = resPath.Replace("%20", " ")
            ' Embed the resource
            If resPath.LastIndexOf("http://") = -1 Then
                Dim theResource As LinkedResource = New LinkedResource(resPath)
                theResource.ContentId = "EmbedRes_" & resId
                resCol.LinkedResources.Add(theResource)
            End If

            ' Next resource ID
            resId = resId + 1

        Next


        ' Create our final object
        Dim finalEmail As AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString(emailHtml, Nothing, "text/html")
        Dim transferResource As LinkedResource

        ' And transfer all the added resources to the output object
        For Each transferResource In resCol.LinkedResources
            finalEmail.LinkedResources.Add(transferResource)
        Next

        Return finalEmail

    End Function

    Public Function GetBSUEmpFilePath() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_EMP_FILEPATH FROM BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & hfBSU_ID.Value.ToString & "'"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString
    End Function

    Public Function FolderExists(ByVal FolderPath As String) _
   As Boolean

        Dim f As New IO.DirectoryInfo(FolderPath)
        Return f.Exists

    End Function

    Function SaveData() As Boolean
        SaveData = False
        'Dim transaction As SqlTransaction
        'Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        ' transaction = conn.BeginTransaction("SampleTransaction")
        Try
            If ViewState("PhotoPath").ToString <> "" Then

                If hfSave.Value = "0" Then
                    SaveEditData()
                End If
                Dim fs As New FileInfo(ViewState("PhotoPath").ToString)
                Dim str_query As String = " EXEC saveSTUDBBTAOAPPROVE " _
                                         & hfBBT_ID.Value + "," _
                                         & "'" + txtApprove.Text + "'," _
                                         & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
                                         & ddlPickUpChoose.SelectedValue.ToString + "," _
                                         & ddlDropoffChoose.SelectedValue.ToString + "," _
                                         & IIf(ddlonward.SelectedValue = "0", "NULL", ddlonward.SelectedValue.ToString) + "," _
                                         & IIf(ddlReturn.SelectedValue = "0", "NULL", ddlReturn.SelectedValue.ToString) + "," _
                                         & "'" + Session("sUsr_name") + "'," _
                & "'" + ViewState("PhotoPath").ToString + "'" '_
                '& "'" + ddlTripType.SelectedItem.Value.ToString + "'"
                'Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, str_query)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                'While reader.Read
                With ds.Tables(0).Rows(0)
                    hfSTU_ID.Value = .Item(0).ToString  'reader.GetValue(0)
                    hfFEE_ID.Value = .Item(1).ToString  'reader.GetValue(1)
                    If .Item(2) = "F" Then
                        hfUser.Value = .Item(3)
                    ElseIf .Item(3) = "M" Then
                        hfUser.Value = .Item(4)
                    Else
                        hfUser.Value = .Item(5)
                    End If
                End With

                Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString

                'ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()/155011/20433740/STUPHOTO.jpg
                ViewState("EMPPHOTOFILEPATH") = str_img & "\" & hfBSU_ID.Value.ToString & ""
                Dim dirInfoBSUID As DirectoryInfo = Nothing
                Dim dirInfoEMPLOYEE As DirectoryInfo = Nothing
                If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH")) Then
                    dirInfoBSUID = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH"))
                End If
                If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH") & "\" & hfSTU_ID.Value.ToString) Then
                    dirInfoEMPLOYEE = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH") & "\" & hfSTU_ID.Value.ToString)
                End If
                Dim strFilePath As String = ViewState("EMPPHOTOFILEPATH") & "\" & hfSTU_ID.Value.ToString & "\STUPHOTO" & fs.Extension
                Dim strFilePath1 As String = ViewState("EMPPHOTOFILEPATH") & "\" & hfSTU_ID.Value.ToString
                Dim ar(100) As String
                'If (dirInfoEMPLOYEE.EnumerateFiles().Any()) Then

                'End If
                'ar = Directory.GetFiles(strFilePath1)
                If Directory.GetFiles(strFilePath1).Length <> 0 Then
                    File.Copy(ViewState("PhotoPath").ToString, strFilePath, True)
                Else
                    File.Move(ViewState("PhotoPath").ToString, strFilePath)
                End If
                SaveData = True
            Else
                Dim strImage As String = ""
                Dim str_query As String = " EXEC saveSTUDBBTAOAPPROVE " _
                                         & hfBBT_ID.Value + "," _
                                         & "'" + txtApprove.Text + "'," _
                                         & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
                                         & ddlPickUpChoose.SelectedValue.ToString + "," _
                                         & ddlDropoffChoose.SelectedValue.ToString + "," _
                                         & IIf(ddlonward.SelectedValue = "0", "NULL", ddlonward.SelectedValue.ToString) + "," _
                                         & IIf(ddlReturn.SelectedValue = "0", "NULL", ddlReturn.SelectedValue.ToString) + "," _
                                         & "'" + Session("sUsr_name") + "'," _
                & "'" + strImage + "'"
                '& "'" + ddlTripType.SelectedItem.Value.ToString + "'"
                'Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, str_query)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                'While reader.Read
                With ds.Tables(0).Rows(0)
                    hfSTU_ID.Value = .Item(0).ToString  'reader.GetValue(0)
                    hfFEE_ID.Value = .Item(1).ToString  'reader.GetValue(1)

                    If .Item(2) = "F" Then
                        hfUser.Value = .Item(3)
                    ElseIf .Item(3) = "M" Then
                        hfUser.Value = .Item(4)
                    Else
                        hfUser.Value = .Item(5)
                    End If
                End With
            End If
            SaveData = True
        Catch myex As ArgumentException
            'transaction.Rollback()
            lblError.Text = myex.Message
            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Catch ex As Exception
            'transaction.Rollback()
            lblError.Text = "Record could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'End Using
    End Function
#End Region

    'minu
    'Function SaveData_AO() As Boolean
    '    Dim transaction As SqlTransaction
    '    Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
    '        transaction = conn.BeginTransaction("SampleTransaction")
    '        Try
    '            Dim str_query As String = " EXEC saveSTUDBBTAOAPPROVE " _
    '                                     & hfBBT_ID.Value + "," _
    '                                     & "'" + txtApprove.Text + "'," _
    '                                     & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
    '                                     & ddlPickup.SelectedValue.ToString + "," _
    '                                     & ddlDropoff.SelectedValue.ToString + "," _
    '                                     & "NULL" + "," _
    '                                    & "NULL" + "," _
    '                                     & "'" + Session("sUsr_name") + "'"
    '            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(transaction, CommandType.Text, str_query)
    '            While reader.Read
    '                hfSTU_ID.Value = reader.GetValue(0)
    '                hfFEE_ID.Value = reader.GetValue(1)
    '                If reader.GetString(2) = "F" Then
    '                    hfUser.Value = reader.GetString(3)
    '                ElseIf reader.GetString(3) = "M" Then
    '                    hfUser.Value = reader.GetString(4)
    '                Else
    '                    hfUser.Value = reader.GetString(5)
    '                End If
    '            End While
    '            reader.Close()
    '            transaction.Commit()

    '            Return True
    '        Catch myex As ArgumentException
    '            transaction.Rollback()
    '            lblError.Text = myex.Message
    '            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '            Return False
    '        Catch ex As Exception
    '            transaction.Rollback()
    '            lblError.Text = "Record could not be Saved"
    '            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '            Return False
    '        End Try
    '    End Using
    'End Function
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        CallReport(hfSTU_ID.Value)

    End Sub
    Sub CallReport(ByVal STU_IDS As String)

        Dim param As New Hashtable

        param.Add("@BSU_ID", hfBSU_ID.Value)
        param.Add("@GEMSSTATUS", "0")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param.Add("@GRM_ID", hfGRM_ID.Value)
        param.Add("@SCT_ID", 0)
        param.Add("@STUTYPE", "")
        param.Add("@REQSTATUS", "0")
        param.Add("@DOJFROM", DBNull.Value)
        param.Add("@DOJTO", DBNull.Value)
        param.Add("reqType", "0")
        param.Add("@REF_NUM", hfBBT_ID.Value)
        param.Add("@STU_ID", STU_IDS)
        param.Add("@STU_NAME", "")


        'param.Add("@IMG_BSU_ID", Session("sbsuid"))
        'param.Add("@IMG_TYPE", "LOGO")
        'param.Add("UserName", Session("sUsr_name"))
        'param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        'param.Add("AO", GetEmpName("ADMIN OFFICER"))
        'param.Add("@STU_IDS", STU_IDS)
        'param.Add("BSU", GetBsuName)

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptPrintStudTransportInfo.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        'ReportLoadSelection()

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function


    Function GetEmpName(ByVal designation As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EnableDisableControls(True)

        btnCancel.Visible = False
        btnReject.Visible = False
        hfSave.Value = "0"
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveEditData()
        hfSave.Value = "1"
        EnableDisableControls(False)
        btnApprove.Visible = True
        btnReject.Visible = True
        ViewState("datamode") = "view"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    'minu
    'Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
    '    SaveStsApprove(False)
    '    btnAccept.Enabled = False
    '    btnReject.Enabled = False

    'End Sub

    Protected Sub ddlPickUpChoose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPickUpChoose.SelectedIndexChanged
        hfPPNT_ID.Value = ddlPickUpChoose.SelectedItem.Value.ToString
        BindTrip(ddlonward, "Onward")
    End Sub

    Protected Sub ddlDropoffChoose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDropoffChoose.SelectedIndexChanged
        hfDPNT_ID.Value = ddlDropoffChoose.SelectedItem.Value.ToString
        BindTrip(ddlReturn, "Return")
    End Sub


    Sub SendApprovalEmail()

        Dim stuName As String = txtFirstName.Text + " " + txtMidName.Text + " " + txtLastName.Text
        Dim refID As Integer = hfBBT_ID.Value
        Dim grade As String = ddlGrade.SelectedItem.Text
        Dim section As String = ""
        Dim onwAreaPoint As String = lblPickupArea.Text + " - " + lblPickupPoint.Text
        Dim retAreaPoint As String = lblDropoffArea.Text + " - " + lblDropoffPoint.Text
        Dim startDate As String = txtFrom.Text
        Dim reqDate As String = ""
        Dim bsuName As String = ""
        Dim bsuID As String = ""
        Dim reqType As String = "APPROVAL"
        Dim regNum As String = hfBBT_ID.Value.ToString
        Dim regValue As String = hfReg.Value.ToString
        Dim Parent As String = txtPFirstName.Text + " " + txtPMidName.Text + " " + txtPLastName.Text
        Dim toEmail As String = ""


        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BBT_REQDATE,BSU_NAME,BSU_ID,BBT_EMAIL FROM STUDENT_BBTREQ_S INNER JOIN OASIS..BUSINESSUNIT_M ON BBT_BSU_ID=BSU_ID  " _
                                & " WHERE BBT_ID = " + hfBBT_ID.Value

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                reqDate = Format(ds.Tables(0).Rows(0).Item(0), "dd/MMM/yyyy")
                bsuName = ds.Tables(0).Rows(0).Item(1).ToString
                bsuID = ds.Tables(0).Rows(0).Item(2).ToString
                toEmail = ds.Tables(0).Rows(0).Item(3).ToString
            End If
        End If

        str_query = "exec [TRANSPORT].[TRANSPORT_APPROVAL_EMAIL] " _
                               & "'" + bsuID + "'," _
                               & "'" + grade + "'," _
                               & "'" + section + "'," _
                               & "'" + stuName + "'," _
                               & "'" + reqType + "'," _
                               & "'" + regValue + "'," _
                               & "'" + refID.ToString + "'," _
                               & "'" + Parent + "'," _
                               & "'" + toEmail + "'," _
                               & "'" + onwAreaPoint + "'," _
                               & "'" + retAreaPoint + "'," _
                               & "'NONGEMS'," _
                               & "'" + bsuName + "'," _
                               & "'" + startDate + "'," _
                               & "'" + reqDate + "'"


        Dim retValue As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)



    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            'minu
            'If studClass.checkFeeClosingDate(Session("sbsuid"), CType(hfACD_ID.Value, Integer), Date.Parse(lblDate.Text)) = True Then
            If studClass.checkFeeClosingDate(Session("sbsuid"), CType(hfACD_ID.Value, Integer), Date.Parse(txtFrom.Text)) = True Then
                If SaveData() = True Then
                    SendApprovalEmail()
                    lblError.Text = "Record Saved Successfully"
                    btnPrint.Visible = True
                    btnPrint.Enabled = True
                    btnApprove.Enabled = False
                    btnReject.Enabled = False
                    btnEdit.Enabled = False
                    btnCancel.Enabled = True
                    btnSave.Enabled = False
                Else
                    'If hfEmpImagePath.Value = "" Then
                    'lblError.Text = "Please choose a file"
                    'Else
                    lblError.Text = "Request could not be processed"
                    'End If
                    Exit Sub
                End If
            Else
                lblError.Text = "The service strarting date has to be higher than the fee closing date"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lnKPickup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnKPickup.Click
        If ddlPickUpChoose.SelectedValue.ToString = "" Then
            ddlonward.Items.Clear()
            ddlonward.Items.Insert(0, New ListItem("--", "0"))
        Else
            BindTrip(ddlonward, "Onward")
        End If


    End Sub

    Protected Sub lnkDropOff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDropOff.Click

        If ddlDropoffChoose.SelectedValue.ToString = "" Then
            ddlReturn.Items.Clear()
            ddlReturn.Items.Insert(0, New ListItem("--", "0"))
        Else
            BindTrip(ddlReturn, "Return")
        End If
    End Sub

    Sub SaveAOReject()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "update student_bbtreq_s set BBT_bAOAPPROVE = 'FALSE', " _
                                 & " BBT_AOAPRDATE='" + Format(Date.Parse(txtApprove.Text), "yyyy-MM-dd") + "'," _
                                 & " BBT_AOREMARKS='" + txtRemarks.Text + "'," _
                                 & " BBT_AOAPRUSER='" + Session("sUsr_name") + "'" _
                                 & " where bbt_id=" + hfBBT_ID.Value
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            SaveAOReject()
            'SendRegretEmail()
            btnApprove.Enabled = False
            btnReject.Enabled = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub ddlDropAreaForAdmin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDropAreaForAdmin.SelectedIndexChanged

        BindPickup(ddlDropoffChoose, ddlDropAreaForAdmin.SelectedItem.Value)
    End Sub

    Protected Sub ddlAreaForAdmin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAreaForAdmin.SelectedIndexChanged
        BindPickup(ddlPickUpChoose, ddlAreaForAdmin.SelectedItem.Value)
    End Sub


    Protected Sub ddlTripType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTripType.SelectedIndexChanged
        If ddlTripType.SelectedIndex = 1 Then
            If ddlAreaForAdmin.Enabled = False Then
                ddlAreaForAdmin.Enabled = True
                ddlonward.Enabled = True
                ddlPickUpChoose.Enabled = True
                lnKPickup.Enabled = True
                rfAdminArea.Enabled = True
                rfAdminPickUpPoint.Enabled = True
                BindPickupAreaForAdmin("", hfBSU_ID.Value)
                BindPickup(ddlPickUpChoose, hfPSBL_ID.Value)
                lblPlocation.Enabled = True
            End If
            If ddlDropAreaForAdmin.Enabled = False Then
                ddlDropAreaForAdmin.Enabled = True
                ddlReturn.Enabled = True
                ddlDropoffChoose.Enabled = True
                lnkDropOff.Enabled = True
                rfAdminDropArea.Enabled = True
                rfAdminDropoffPoint.Enabled = True
                BindDropoffAreaForAdmin("", hfBSU_ID.Value)
                BindPickup(ddlDropoffChoose, hfPSBL_ID.Value)
                lblDlocation.Enabled = True
            End If
        ElseIf ddlTripType.SelectedIndex = 2 Then
            If ddlAreaForAdmin.Enabled = False Then
                ddlAreaForAdmin.Enabled = True
                ddlonward.Enabled = True
                ddlPickUpChoose.Enabled = True
                lnKPickup.Enabled = True
                rfAdminArea.Enabled = True
                rfAdminPickUpPoint.Enabled = True
                BindPickupAreaForAdmin("", hfBSU_ID.Value)
                BindPickup(ddlDropoffChoose, hfPSBL_ID.Value)
                lblPlocation.Enabled = True
            End If
            If ddlDropAreaForAdmin.Enabled = True Then
                ddlDropAreaForAdmin.Enabled = False
                ddlReturn.Enabled = False
                ddlDropoffChoose.Enabled = False
                lnkDropOff.Enabled = False
                rfAdminDropArea.Enabled = False
                rfAdminDropoffPoint.Enabled = False
                'ddlDropAreaForAdmin.Items.Insert(0, New ListItem("--", "0"))
                'ddlDropoffChoose.Items.Insert(0, New ListItem("--", "0"))
                'ddlReturn.Items.Insert(0, New ListItem("--", "0"))
                ddlDropAreaForAdmin.SelectedIndex = 0
                ddlDropoffChoose.SelectedIndex = 0
                ddlReturn.SelectedIndex = 0
                lblDlocation.Text = ""
            End If
        ElseIf ddlTripType.SelectedIndex = 3 Then
            If ddlAreaForAdmin.Enabled = True Then
                ddlAreaForAdmin.Enabled = False
                ddlonward.Enabled = False
                ddlPickUpChoose.Enabled = False
                lnKPickup.Enabled = False
                rfAdminArea.Enabled = False
                rfAdminPickUpPoint.Enabled = False
                'ddlAreaForAdmin.Items.Insert(0, New ListItem("--", "0"))
                'ddlPickUpChoose.Items.Insert(0, New ListItem("--", "0"))
                'ddlonward.Items.Insert(0, New ListItem("--", "0"))
                ddlAreaForAdmin.SelectedIndex = 0
                ddlPickUpChoose.SelectedIndex = 0
                ddlonward.SelectedIndex = 0
                lblPlocation.Enabled = False
                lblPlocation.Text = ""
            End If
            If ddlDropAreaForAdmin.Enabled = False Then
                ddlDropAreaForAdmin.Enabled = True
                ddlReturn.Enabled = True
                ddlDropoffChoose.Enabled = True
                lnkDropOff.Enabled = True
                rfAdminDropArea.Enabled = True
                rfAdminDropoffPoint.Enabled = True
                BindDropoffAreaForAdmin("", hfBSU_ID.Value)
                BindPickup(ddlPickUpChoose, hfPSBL_ID.Value)
                lblDlocation.Enabled = True
                lblDlocation.Text = ""
            End If
        Else
            lblError.Text = "Choose a trip type"
            Exit Sub
        End If
    End Sub

    Sub BindCapacity(ByVal gvCap As GridView, ByVal trdId As String, ByVal journey As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "exec TRANSPORT.getSEATCAPACITY " _
                                & "'" + Session("sbsuid") + "'," _
                                & trdId + "," _
                                & hfACD_ID.Value + "," _
                                & Session("Current_ACY_ID") + "," _
                                & "'" + journey + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvCap.DataSource = ds
        gvCap.DataBind()

    End Sub

    Protected Sub ddlReturn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReturn.SelectedIndexChanged
        BindCapacity(gvReturn, ddlReturn.SelectedItem.Value, "Return")
        gvReturn.Visible = True
    End Sub

    Protected Sub ddlonward_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlonward.SelectedIndexChanged
        BindCapacity(gvOnward, ddlonward.SelectedItem.Value, "Onward")
        gvOnward.Visible = True
        gvReturn.Enabled = True
    End Sub
End Class
