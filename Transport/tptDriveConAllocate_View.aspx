<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptDriveConAllocate_View.aspx.vb" Inherits="Transport_tptDriveConAllocate_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (row == 'alt') {
                    img.src = "../Images/expand_button_white_alt_down.jpg";
                }
                else {
                    img.src = "../Images/Expand_Button_white_Down.jpg";
                }
                img.alt = "Click to close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/Expand_button_white_alt.jpg";
                }
                else {
                    img.src = "../Images/Expand_button_white.jpg";
                }
                img.alt = "Click to expand";
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>Driver/Conductor Allocation     
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                   <%-- <tr>
                        <td align="left">
                            <table width="100%" id="Table3" border="0">
                                <tr>
                                    <td align="left" class="title-bg-lite">Driver/Conductor Allocation       
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="30%"><span class="field-label">Select Business Unit</span></td>
                                                <td align="left" width="70%">
                                                    <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left">&nbsp;</td>
                                                <td align="right">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td colspan="2" align="center">
                                                    <asp:GridView ID="gvDrvConAlloc" runat="server"
                                                        AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                        OnRowDataBound="gvDrvConAlloc_RowDataBound" PageSize="20" AllowPaging="True" Width="100%">
                                                        <Columns>

                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                                                                        <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                                                    </a>
                                                                </ItemTemplate>
                                                                <AlternatingItemTemplate>
                                                                    <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
                                                                        <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                                                    </a>
                                                                </AlternatingItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Driver/Conductor">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDrvCon" runat="server" Text='<%# Bind("DrvCon") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>


                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    </td></tr><tr>
                                                                        <td colspan="100%">
                                                                            <div id="div<%# Eval("GUID") %>" style="display: none; position: relative; left: 20px;">
                                                                                <asp:GridView ID="gvEmp" runat="server" Width="100%" CssClass="table table-bordered table-row"
                                                                                    AutoGenerateColumns="false" EmptyDataText="No records added">
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="EMP_NAME" HeaderText="Name" HtmlEncode="False">
                                                                                            <ItemStyle HorizontalAlign="left" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                    <RowStyle CssClass="griditem" />
                                                                                    <HeaderStyle />
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>

                                                        <RowStyle CssClass="griditem" />
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <SelectedRowStyle />
                                                        <PagerStyle HorizontalAlign="Left" />


                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3"
                                runat="server" type="hidden" value="=" />
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

</asp:Content>

