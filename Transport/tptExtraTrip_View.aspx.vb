Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptExtraTrip_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim studClass As New studClass


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100230") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    GridBind()

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Transport\tptExtraTrip_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvTrip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub ddlgvVehicle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnDriver_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnConductor_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnTrip_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvTptTripAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptTripAlloc.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvTptTripAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptTripAlloc.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvTptTripAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptTripAlloc.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvTptTripAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvTptTripAlloc.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind(Optional ByVal history As Boolean = False)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim str_query As String = "SELECT TRD_ID,TRP_DESCR,VEH_REGNO," _
                                  & " isnull(CON_NAME,'') AS CON_NAME,DRV_NAME,TRD_FROMDATE,TRD_TODATE,TRD_REMARKS," _
                                  & " CAT_DESCRIPTION,PSE_DESCR  " _
                                  & " FROM TRANSPORT.VV_EXTRATRIPS_D WHERE TRP_JOURNEY='EXTRA' AND TRD_BSU_ID='" + Session("SBSUID") + "'"

        If history = False Then
            str_query += " AND TRD_bACTIVE='TRUE' AND('" + Format(Now.Date, "yyyy-MM-dd") + "' BETWEEN " _
                       & " TRD_FROMDATE AND ISNULL(TRD_TODATE,'2099-01-01') OR '" + Format(Now.Date, "yyyy-MM-dd") + "' < TRD_FROMDATE)"
        Else
            str_query += " AND( TRD_bACTIVE='FALSE' OR ( NOT '" + Format(Now.Date, "yyyy-MM-dd") + "' BETWEEN " _
                       & " TRD_FROMDATE AND ISNULL(TRD_TODATE,'2099-01-01') AND NOT  '" + Format(Now.Date, "yyyy-MM-dd") + "' < TRD_FROMDATE))"
        End If




        Dim txtTrip As New TextBox
        Dim ddlgvTrip As New DropDownList
        Dim ddlgvVehicle As New DropDownList
        Dim ddlgvCategory As New DropDownList


        Dim selectedTrips As String = ""
        Dim selectedVehicle As String = ""
        Dim selectedShift As String = ""
        Dim selectedJourney As String = ""
        Dim selectedBus As String = ""
        Dim selectedCategory As String = ""

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim txtSearch As New TextBox
        Dim trpSearch As String = ""
        If gvTptTripAlloc.Rows.Count > 0 Then


            txtSearch = New TextBox
            txtSearch = gvTptTripAlloc.HeaderRow.FindControl("txtTrip")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("TRP_DESCR", txtSearch.Text, strSearch)
            trpSearch = txtSearch.Text


            txtSearch = New TextBox
            txtSearch = gvTptTripAlloc.HeaderRow.FindControl("txtDriver")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("DRV_NAME", txtSearch.Text, strSearch)


            txtSearch = New TextBox
            txtSearch = gvTptTripAlloc.HeaderRow.FindControl("txtConductor")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("CON_NAME", txtSearch.Text, strSearch)

            ddlgvVehicle = gvTptTripAlloc.HeaderRow.FindControl("ddlgvVehicle")
            If ddlgvVehicle.Text <> "ALL" Then
                strFilter += " and VEH_REGNO='" + ddlgvVehicle.Text + "'"
                selectedVehicle = ddlgvVehicle.Text
            End If

          
                 ddlgvCategory = gvTptTripAlloc.HeaderRow.FindControl("ddlgvCategory")
            If ddlgvCategory.Text <> "ALL" Then
                strFilter += " and CAT_DESCRIPTION='" + ddlgvCategory.Text + "'"
                selectedCategory = ddlgvCategory.Text
            End If


            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If

        str_query += " ORDER BY TRP_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTptTripAlloc.DataSource = ds


        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvTptTripAlloc.DataBind()
            Dim columnCount As Integer = gvTptTripAlloc.Rows(0).Cells.Count
            gvTptTripAlloc.Rows(0).Cells.Clear()
            gvTptTripAlloc.Rows(0).Cells.Add(New TableCell)
            gvTptTripAlloc.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTptTripAlloc.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTptTripAlloc.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvTptTripAlloc.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        If gvTptTripAlloc.Rows.Count > 0 Then
                 ddlgvVehicle = gvTptTripAlloc.HeaderRow.FindControl("ddlgvVehicle")
            ddlgvCategory = gvTptTripAlloc.HeaderRow.FindControl("ddlgvCategory")
        
            Dim dr As DataRow

      
            ddlgvVehicle.Items.Clear()
            ddlgvVehicle.Items.Add("ALL")

            ddlgvCategory.Items.Clear()
            ddlgvCategory.Items.Add("ALL")

      
            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr

                

                    If ddlgvVehicle.Items.FindByText(.Item(2)) Is Nothing Then
                        ddlgvVehicle.Items.Add(.Item(2))
                    End If

                    If ddlgvCategory.Items.FindByText(.Item(8)) Is Nothing Then
                        ddlgvCategory.Items.Add(.Item(8))
                    End If

                End With
            Next

            If selectedTrips <> "" Then
                ddlgvTrip.Text = selectedTrips
            End If

             If selectedVehicle <> "" Then
                ddlgvVehicle.Text = selectedVehicle
            End If

            If selectedCategory <> "" Then
                ddlgvCategory.Text = selectedCategory
            End If

            txtSearch = New TextBox
            txtSearch = gvTptTripAlloc.HeaderRow.FindControl("txtTrip")
            txtSearch.Text = trpSearch
        End If

     
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function


#End Region

    Protected Sub gvTptTripAlloc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTptTripAlloc.PageIndexChanging
        Try
            gvTptTripAlloc.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTptTripAlloc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTptTripAlloc.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvTptTripAlloc.Rows(index), GridViewRow)
            Dim lbltrdid As New Label
            Dim lblTrip As New Label
            Dim url As String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            With selectedRow
                lbltrdid = .FindControl("lblTrdId")
                lblTrip = .FindControl("lblTrip")
            End With
            If e.CommandName = "View" Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")

                Dim editString As String = lbltrdid.Text + "|" + lblTrip.Text
                url = String.Format("~\Transport\tptExtraTrip_M.aspx?MainMnu_code={0}&datamode={1}&editstring=" + Encr_decrData.Encrypt(editString), ViewState("MainMnu_code"), ViewState("datamode"))

                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lbHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbHistory.Click
        Try
            If lbHistory.Text.ToLower = "show history" Then
                lbHistory.Text = "Hide History"
                GridBind(True)
            Else
                lbHistory.Text = "Show History"
                GridBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
