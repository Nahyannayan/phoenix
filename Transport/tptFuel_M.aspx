<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptFuel_M.aspx.vb" Inherits="Transport_tptFuel_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> Fuel Entry Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center" width="100%" >
            
            <tr >
               <td align="left" >
               <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                      <asp:RequiredFieldValidator ID="rfFillDate" runat="server" ControlToValidate="txtFill"
                       Display="None" ErrorMessage="Please enter the field Fill Date" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
                       <asp:RequiredFieldValidator ID="rfCurrKM" runat="server" ControlToValidate="txtCurrent"
                       Display="None" ErrorMessage="Please enter data in the field Current KM reading" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfGallon" runat="server" ControlToValidate="txtGallon"
                       Display="None" ErrorMessage="Please enter data in the field Gallon" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfAmount" runat="server" ControlToValidate="txtAmt"
                       Display="None" ErrorMessage="Please enter data in the field Amount" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
                   <asp:CompareValidator ID="cvKM" runat="server" ControlToCompare="txtPrev" ControlToValidate="txtCurrent"
                       CssClass="error" Display="None" ErrorMessage="The Current KM reading should be higher than previous KM reading"
                       ForeColor="Transparent" Height="1px" Operator="GreaterThan" Type="Integer" ValidationGroup="groupM1"
                       Width="46px"></asp:CompareValidator></td>  
                        </tr>
                   
               <tr>
            <td >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" style="text-align: left" Width="342px" />
                &nbsp;&nbsp;
            </td>
        </tr>         
        <tr><td align="center">Fields Marked with (<span class="text-danger">*</span>)
                are mandatory      
        </td></tr>
         <tr><td align="center" width="100%">
         <table id="Table1" runat="server" align="center" width="100%">
             <tr>
                 <td align="left" width="20%">
                    <span class="field-label"> Vehicle Registration No.</span><span class="text-danger"></span></td>
                 
                 <td align="left" width="30%">
                     <asp:TextBox ID="txtVeh" runat="server" AutoCompleteType="Disabled"
                      Enabled="False"></asp:TextBox></td>
                             
                 <td align="left" width="20%">
                   <span class="field-label"> Fill Date</span><span class="text-danger">*</span></td>
               
                 <td align="left"  width="30%">
                     <asp:TextBox ID="txtFill" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                     <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                     <asp:RegularExpressionValidator id="revFillDate" runat="server" ControlToValidate="txtFill"
                         CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Fill Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                         ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                         ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                     <asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtFill"
                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Trip Date From entered is not a valid date"
                            ValidationGroup="groupM1">*</asp:CustomValidator>
                     <asp:RequiredFieldValidator ID="rfFrom" runat="server" ErrorMessage="Please enter the field trip date from" ControlToValidate="txtFill" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                 </td>
                     
             </tr>
             <tr>
             
              <td align="left"  >
                  <span class="field-label">Previous Km. Reading </span><span class="text-danger">*</span></td>
                    
                 <td align="left">
                     <asp:TextBox ID="txtPrev" runat="server" AutoCompleteType="Disabled" Width="112px" Enabled="False"></asp:TextBox></td>
                        
                    <td align="left">
                        <span class="field-label">Current Km. Reading </span><span class="text-danger">*</span>&nbsp;</td>
                   
                 <td align="left">
                     <asp:TextBox ID="txtCurrent" runat="server" AutoCompleteType="Disabled" Width="112px"></asp:TextBox></td>
                        
                   
             </tr>
             <tr>
                 <td align="left">
                     <span class="field-label">Gallons</span><span class="text-danger">*</span></td>
                
                 <td align="left">
                     <asp:TextBox ID="txtGallon" runat="server" AutoCompleteType="Disabled" Width="112px"></asp:TextBox></td>
                 <td align="left">
                    <span class="field-label"> Amount (AED)</span><span class="text-danger">*</span></td>
                 
                 <td align="left">
                     <asp:TextBox ID="txtAmt" runat="server" AutoCompleteType="Disabled" Width="112px"></asp:TextBox></td>
             </tr>
                  
                 
           
            <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel"  TabIndex="8" />
                </td>
        </tr>
                      </table>
               <asp:HiddenField ID="hfVEH_ID" runat="server" />
               &nbsp; &nbsp;&nbsp;<asp:HiddenField ID="hfTFL_ID" runat="server" />
               <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                   Format="dd/MMM/yyyy" PopupButtonID="txtFill" TargetControlID="txtFill">
               </ajaxToolkit:CalendarExtender>
               <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                   Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFill">
               </ajaxToolkit:CalendarExtender>
             
           
           </td></tr>
          
        </table>

    </div>
    </div>
    </div>

</asp:Content>

