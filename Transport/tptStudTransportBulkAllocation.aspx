<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptStudTransportBulkAllocation.aspx.vb" Inherits="Transport_tptStudTransportBulkAllocation" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
   
    
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvAllot.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }








    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>
            TRANSPORT ALLOCATION
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" style="width: 100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center"><span class="field-label">Fields Marked with (<span class="text-danger">*</span>) are mandatory</span>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table align="center" id="tblTpt" runat="server" style="width: 100%">
                                <tr>
                                    <td width="20%" align="left"><span class="field-label">Select Curriculum</span></td>
                                    <td width="30%" align="left">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td  width="20%" align="left"><span class="field-label">Select Academic Year</span></td>
                                    <td  width="30%" align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td  width="20%"><span class="field-label">Grade</span></td>
                                    <td  width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Shift</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Bus No</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlBusNo" runat="server">
                                        </asp:DropDownList></td>
                                        <td></td>
                                        <td></td>
                                </tr>




                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:RadioButton CssClass="field-label" ID="rdAll" runat="server" GroupName="g1" Text="All" Checked="True" />
                                        <asp:RadioButton CssClass="field-label" ID="rdSameShift" runat="server" GroupName="g1" Text="List students with trips in same shift" />
                                        <asp:RadioButton CssClass="field-label" ID="rdDifferentShift" runat="server" GroupName="g1" Text="List students with trips in different shift" />
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnList" runat="server" Text="List" CssClass="button" TabIndex="4" CausesValidation="False" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Select Onward Trip</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlOnward" SkinID="smallcmb" runat="server">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>


                                <tr>
                                    <td align="left"><span class="field-label">Select Return Trip</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlReturn" SkinID="smallcmb" runat="server">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAllot1" runat="server" Text="Allot" CssClass="button" TabIndex="4" /></td>
                                </tr>
                                <tr>


                                    <td align="center" colspan="4">

                                        <asp:GridView ID="gvAllot" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found" PageSize="20" Width="100%">

                                            <Columns>

                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle  />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Select<br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Stud No.">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblFeeHeader" runat="server" CssClass="gridheader_text" Text="Stud. No"></asp:Label><br />
                                                        <asp:TextBox ID="txtFeeSearch" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnFeeId_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnFeeId_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label><br />
                                                        <asp:TextBox ID="txtStudName" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Area">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblName1" runat="server" CssClass="gridheader_text" Text="Area"></asp:Label><br />
                                                        <asp:TextBox ID="txtArea" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnArea_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("SBL_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Ex Student">
                                                    <HeaderTemplate>
                                                        Bus No<br />
                                                        <asp:DropDownList ID="ddlgvBno" runat="server" CssClass="listbox" Width="75%" AutoPostBack="True" OnSelectedIndexChanged="ddlgvBno_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgBno" runat="server" HorizontalAlign="Center" ImageUrl='<%#  BIND("IMGBNO") %>' Visible='<%# BIND("bPICK") %>' />
                                                        <asp:Label ID="lblBno" Width="75%" runat="server" Text='<%#  BIND("BNO_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pick Up">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPickUp" runat="server" Text='<%# Bind("PICKUP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Drop Off">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDropOff" runat="server" Text='<%# Bind("DROPOFF") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Starting Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("SSV_FROMDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pick Up" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPickId" runat="server" Text='<%# Bind("STU_PICKUP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Drop Off" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDropId" runat="server" Text='<%# Bind("STU_DROPOFF") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <%--<RowStyle CssClass="griditem" Height="25px" />--%>
                                           <%-- <SelectedRowStyle CssClass="Green" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />--%>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAllot" runat="server" Text="Allot" CssClass="button" TabIndex="4"/>
                                    </td>


                                </tr>
                            </table>

                            <asp:HiddenField ID="hfSHF_ID" runat="server" />
                            <asp:Panel ID="Panel1" Visible="false" Style="z-index: 10; left: 497px; position: absolute; top: 843px;" runat="server" BackColor="AliceBlue" Height="99px" Width="274px" BorderStyle="Groove">
                                <asp:Label ID="lblText" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="8pt"
                                    ForeColor="#1B80B6" Style="left: -4px; position: relative; top: 22px" Height="27px" Width="263px" CssClass="matters"></asp:Label>
                                <asp:Button ID="btnYes" runat="server" Text="Yes" Style="left: 5px; position: relative; top: 36px" CssClass="button" Width="42px" Height="19px" />
                                <asp:Button ID="btnNo" runat="server" Text="No" Style="left: 0px; position: relative; top: 36px" CssClass="button" Width="46px" Height="19px" />
                            </asp:Panel>
                            <asp:HiddenField ID="hfTRD_ID_ONWARD" runat="server" />
                            &nbsp;&nbsp;
                    <asp:HiddenField ID="hfTRD_ID_RETURN" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                HorizontalSide="Center" TargetControlID="Panel1" VerticalSide="Middle">
                            </ajaxToolkit:AlwaysVisibleControlExtender>
                            <asp:HiddenField ID="hfSeats" runat="server" />
                            <asp:HiddenField ID="hfPickups" runat="server" />
                            <asp:HiddenField ID="hfJourney" runat="server" />
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>


                </table>



                <script type="text/javascript">
                    cssdropdown.startchrome("Div1")
                    cssdropdown.startchrome("Div2")
                    cssdropdown.startchrome("Div3")
                </script>
            </div>
        </div>
    </div>


</asp:Content>

