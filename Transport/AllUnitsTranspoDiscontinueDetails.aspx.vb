﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_AllUnitsTranspoDiscontinueDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass




    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T200491") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If

                ' Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ViewState("datamode") = "add"
                hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                lblName.Text = Encr_decrData.Decrypt(Request.QueryString("stunam").Replace(" ", "+"))
                lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                GetData()
                txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
                GetStudData()
                ' End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Sub GetData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        ''commented and removed acd id by nahyan on 29jan2020
        'Dim str_query As String = "SELECT STD_FROMDATE,ISNULL(STD_TODATE,'1900-01-01'),STD_TYPE,STD_REQREMARKS,STD_ID,D.DISCONT_REASON  FROM STUDENT_DISCONTINUE_S" _
        '                          & " left join  DISCONTINUEREASON_M D on d.DISCONTR_ID=STUDENT_DISCONTINUE_S.STD_REASON" _
        '                        & " WHERE STD_STU_ID=" + hfSTU_ID.Value + " AND STD_ACD_ID=" + hfACD_ID.Value + " AND STD_bAPPROVE IS NULL "

        Dim str_query As String = "SELECT STD_FROMDATE,ISNULL(STD_TODATE,'1900-01-01'),STD_TYPE,STD_REQREMARKS,STD_ID,isnull(D.DISCONT_REASON,'') DISCONT_REASON  FROM STUDENT_DISCONTINUE_S" _
                                  & " left join  DISCONTINUEREASON_M D on d.DISCONTR_ID=STUDENT_DISCONTINUE_S.STD_REASON" _
                                & " WHERE STD_STU_ID=" + hfSTU_ID.Value + " AND STD_bAPPROVE IS NULL "
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim type As String
        While reader.Read
            lblFrom.Text = Format(reader.GetDateTime(0), "dd/MMM/yyyy")
            lblTo.Text = Format(reader.GetDateTime(1), "dd/MMM/yyyy").Replace("01/Jan/1900", "")
            type = reader.GetString(2)
            hfType.Value = type
            If type = "T" Then
                lblType.Text = "Temporary discontinuation"
            ElseIf type = "P" Then
                lblType.Text = "Permanent discontinuation"
            ElseIf type = "S" Then
                lblType.Text = "Tranport suspension"
            End If
            txtReqRemarks.Text = reader.GetString(3)
            hfSTD_ID.Value = reader.GetValue(4)
            lblreason.Text = reader.GetValue(5)
        End While
    End Sub
    Sub GetStudData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim str_query As String = "SELECT  distinct isnull(D.SBL_DESCRIPTION,''),isnull(C.PNT_DESCRIPTION,'')," _
                                 & " isnull(F.SBL_DESCRIPTION,''),isnull(G.PNT_DESCRIPTION,'')," _
                                 & " ISNULL(I.BNO_DESCR,'')+'-'+ISNULL(I.TRP_DESCR,'') AS PICKUPTRIP, " _
                                 & " ISNULL(J.BNO_DESCR,'')+'-'+ISNULL(J.TRP_DESCR,'') AS DROPOFFTRIP " _
                                 & " FROM  STUDENT_M AS A " _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.VV_TRIPS_D AS I ON A.STU_PICKUP_TRP_ID=I.TRP_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.VV_TRIPS_D AS J ON A.STU_DROPOFF_TRP_ID=J.TRP_ID" _
                                 & " WHERE STU_ID =" + hfSTU_ID.Value.ToString




        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                lblPArea.Text = .GetString(0)
                lblPickup.Text = .GetString(1)
                lblDArea.Text = .GetString(2)
                lblDropoff.Text = .GetString(3)
                lblPtrip.Text = .GetString(4)
                lblDTrip.Text = .GetString(5)
            End With
        End While
        reader.Close()

        str_query = "SELECT ISNULL(STD_FROMDATE,'01/Jan/1900') FROM STUDENT_DISCONTINUE_S WHERE STD_TYPE='P' " _
                   & " AND STD_STU_ID=" + hfSTU_ID.Value
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim disDate As DateTime
        While reader.Read
            disDate = reader.GetDateTime(0)
        End While
        reader.Close()


    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function CheckStartDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND '" + Format(Date.Parse(lblFrom.Text), "yyyy-MM-dd") + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function

    Sub SaveData(ByVal bApprove As Boolean)
        Dim transaction As SqlTransaction
        Dim bDisCharges As Boolean

        If optDisChargesYes.Checked = True Then
            bDisCharges = True
        Else
            bDisCharges = False
        End If

        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = "exec TRANSPORT.saveTRANSPORTDISCONTINUEAprrove " _
                                    & hfSTD_ID.Value + "," _
                                    & hfSTU_ID.Value + "," _
                                    & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'," _
                                    & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
                                    & bApprove.ToString + "," _
                                    & "'" + Session("sUsr_name") + "'," _
                                    & bDisCharges.ToString + ""

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()

                lblError.Text = "Record saved successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
    Private Function APPROVE_TRANSPORT_DISCONTINUE_REQ(ByVal bApprove As Boolean) As Integer
        APPROVE_TRANSPORT_DISCONTINUE_REQ = 1
        lblError.Text = ""
        Dim conn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        conn.Open()
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction()
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("TRANSPORT.saveTRANSPORTDISCONTINUEAprrove", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@STD_STU_ID", SqlDbType.Int)
            sqlp1.Value = hfSTU_ID.Value
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@STD_ID", SqlDbType.Int)
            sqlp2.Value = hfSTD_ID.Value
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@STD_APPROVEDATE", SqlDbType.DateTime)
            sqlp3.Value = CDate(txtFrom.Text)
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@STD_bAPPROVE", SqlDbType.Bit)
            sqlp4.Value = bApprove
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@STD_bDiscontCharges", SqlDbType.Bit)
            sqlp5.Value = optDisChargesYes.Checked
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@STD_APRREMARKS", SqlDbType.VarChar, 255)
            sqlp6.Value = txtRemarks.Text.Replace("'", "''")
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@STD_APRUSR", SqlDbType.VarChar, 100)
            sqlp7.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlp7)


            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            APPROVE_TRANSPORT_DISCONTINUE_REQ = retSValParam.Value
            If retSValParam.Value = 0 Then
                trans.Commit()
                lblError.Text = "Request " & IIf(bApprove = True, "Approved", "Rejected") & " successfully"
            Else
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(APPROVE_TRANSPORT_DISCONTINUE_REQ)
            End If
        Catch ex As Exception
            trans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If hfType.Value = "P" Or hfType.Value = "T" Then
                If studClass.checkFeeClosingDate(Session("sbsuid"), hfACD_ID.Value, Date.Parse(lblFrom.Text)) = False Then
                    lblError.Text = "The discontinuation strarting date has to be higher than the fee closing date"
                    Exit Sub
                End If

                If CheckStartDate() = False Then
                    Exit Sub
                End If
            End If
            'SaveData(True)
            If APPROVE_TRANSPORT_DISCONTINUE_REQ(True) = 0 Then
                btnApprove.Enabled = False
                btnReject.Enabled = False
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            'SaveData(False)
            If APPROVE_TRANSPORT_DISCONTINUE_REQ(False) = 0 Then
                btnApprove.Enabled = False
                btnReject.Enabled = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

End Class
