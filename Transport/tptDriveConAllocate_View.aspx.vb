Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptDriveConAllocate_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000290") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    '  GridBind()

                    '
                    BindBsu()
                    BindDrvCon()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                                & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                                & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

    Sub BindDrvCon()
        Dim dt As New DataTable
        dt.Columns.Add("GUID", GetType(String))
        dt.Columns.Add("DrvCon", GetType(String))
        Dim dr As DataRow
        dr = dt.NewRow
        dr.Item(0) = "0"
        dr.Item(1) = "Driver"
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr.Item(0) = "1"
        dr.Item(1) = "Conductor"
        dt.Rows.Add(dr)
        gvDrvConAlloc.DataSource = dt
        gvDrvConAlloc.DataBind()
    End Sub

    Sub BindDriver(ByVal gv As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT a.guid as guid,isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
                                 & " emp_id FROM employee_m as a inner join oasis_transport.TRANSPORT.DRIVERCON_ALLOC_S as b on " _
                                 & " a.emp_id=b.trc_emp_id where TRC_BSU_ALTO_ID='" + ddlBsu.SelectedValue + "'" _
                                 & " and trc_emp_type='Driver'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gv.DataSource = ds
        gv.DataBind()
    End Sub

    Sub BindConductor(ByVal gv As GridView)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT a.guid as guid,isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
        '                          & " emp_id FROM employee_m as a inner join oasis_transport.TRANSPORT.DRIVERCON_ALLOC_S as b on " _
        '                          & " a.emp_id=b.trc_emp_id where TRC_BSU_ALTO_ID='" + ddlBsu.SelectedValue + "'" _
        '                          & " and trc_emp_type='Conductor'"
        Dim str_query As String = "SELECT a.guid as guid,isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'') as emp_name," _
                                  & " emp_id FROM employee_m as a " _
                                  & "where a.EMP_BSU_ID='" + ddlBsu.SelectedValue + "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gv.DataSource = ds
        gv.DataBind()
    End Sub


#End Region

    Protected Sub gvDrvConAlloc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDrvConAlloc.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvDrvConAlloc.Rows(index), GridViewRow)
            Dim lblDrvCon As Label
                   Dim url As String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            With selectedRow
                lblDrvCon = selectedRow.FindControl("lblDrvCon")
            End With
            If e.CommandName = "edit" Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")

                url = String.Format("~\Transport\tptDriveConAllocate_M.aspx?MainMnu_code={0}&datamode={1}&bsu=" + Encr_decrData.Encrypt(ddlBsu.SelectedValue) + "&emp=" + Encr_decrData.Encrypt(lblDrvCon.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

  

  

    Protected Sub gvDrvConAlloc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDrvConAlloc.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblDrvCon As Label
                lblDrvCon = e.Row.FindControl("lblDrvCon")

                Dim gvEmp As GridView
                gvEmp = e.Row.FindControl("gvEmp")
                If lblDrvCon.Text.ToLower = "driver" Then
                    BindDriver(gvEmp)
                Else
                    BindConductor(gvEmp)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        BindDrvCon()
    End Sub

    Protected Sub gvDrvConAlloc_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDrvConAlloc.RowDeleting

    End Sub

    Protected Sub gvDrvConAlloc_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDrvConAlloc.RowEditing

    End Sub
End Class
