Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studTransportDiscontinue_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000200" And ViewState("MainMnu_code") <> "T000210") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlClm = studClass.PopulateCurriculum(ddlClm, Session("sbsuid"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = "0"
                    ddlGrade.Items.Insert(0, li)


                    PopulateSection()

                    Select Case ViewState("MainMnu_code")
                        Case "T000200"
                            lblTitle.Text = "Request For Discontinuation Of Transport"
                        Case "T000210"
                            lblTitle.Text = "Approval For Discontinuation Of Transport"
                    End Select
                    If ddlClm.Items.Count = 1 Then
                        tblTPT.Rows(0).Visible = False
                    End If

                    tblTPT.Rows(5).Visible = False
                    tblTPT.Rows(6).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else



        End If
    End Sub

    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private Methods"

    Private Sub PopulateSection()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"


        ddlSection.Items.Clear()
        If ddlGrade.SelectedValue = "All" Then
            ddlSection.Items.Insert(0, li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " ORDER BY SCT_DESCR "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        'Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        'Dim str_query As String


        'TCH_bAPPROVE INITIALLY NULL
        'WHEN APPROVED THEN TRUE,WHEN REJECTED THEN FALSE

        'str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
        '               & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_SHF_ID,'' AS FROMPORTAL,GETDATE() AS STD_FROMDATE " _
        '               & " FROM STUDENT_M AS A WITH ( NOLOCK ) INNER JOIN VV_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
        '               & " INNER JOIN VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
        '               & " WHERE STU_ACD_ID = " + hfACD_ID.Value _
        '               & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
        '               & " AND (STU_SBL_ID_PICKUP IS NOT NULL OR STU_SBL_ID_DROPOFF IS NOT NULL)" _
        '               & " AND STU_CURRSTATUS<>'CN'"


        'If ViewState("MainMnu_code") = "T000200" Then
        '    str_query += " AND STU_ID NOT IN(SELECT STD_STU_ID FROM STUDENT_DISCONTINUE_S WHERE STD_bAPPROVE IS NULL OR ( CONVERT(DATETIME,'" + Format(Now.Date, "yyyy-MM-dd") + "')<=CONVERT(DATETIME,ISNULL(STD_TODATE,'1900-01-01')) AND STD_bAPPROVE='TRUE') OR (STD_bAPPROVE='TRUE' AND STD_TYPE='P' AND STD_FROMDATE>GETDATE()))"
        'Else

        '    ''  str_query += " AND STU_ID IN(SELECT STD_STU_ID FROM STUDENT_DISCONTINUE_S WHERE STD_bAPPROVE IS NULL)"

        '    str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
        '              & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_SHF_ID, " _
        '               & " CASE WHEN isnull(STD_PARENT_REQUEST,0) = '1' THEN 'YES' ELSE 'NO' END AS FROMPORTAL,STD_FROMDATE " _
        '              & " FROM STUDENT_M AS A WITH ( NOLOCK ) INNER JOIN VV_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
        '              & " INNER JOIN VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
        '              & " INNER JOIN STUDENT_DISCONTINUE_S ON STD_STU_ID=STU_ID " _
        '              & " WHERE STU_ACD_ID = " + hfACD_ID.Value _
        '              & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
        '              & " AND (STU_SBL_ID_PICKUP IS NOT NULL OR STU_SBL_ID_DROPOFF IS NOT NULL)" _
        '              & " AND STU_CURRSTATUS<>'CN' AND STD_bAPPROVE IS NULL "
        'End If
        

        'If ddlSection.SelectedValue <> "0" Then
        '    str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        'End If
        'If ddlGrade.SelectedValue <> "0" Then
        '    str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        'End If

        'If txtStuNo.Text <> "" Then
        '    str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        'End If
        'If txtName.Text <> "" Then
        '    str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        'End If
        

        Dim ds As New DataSet '= SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            'If strFilter <> "" Then
            '    str_query += strFilter
            'End If

        End If
        'If ViewState("MainMnu_code") <> "T000200" Then
        '    str_query &= " ORDER BY STD_REQDATE DESC"
        'Else
        '    str_query &= " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        'End If

        Try
            'Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC TRANSPORT.GET_STUDENTS_FOR_DISCONTINUATION @STU_BSU_ID , @STU_ACD_ID , @STU_SCT_ID , @MENU_CODE , @SCT_DESCR , @STU_GRD_ID , @GRM_DISPLAY , @STU_NO , @STU_NAME  ")

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuId")
            pParms(1) = New SqlClient.SqlParameter("@STU_ACD_ID", SqlDbType.Int)
            pParms(1).Value = ddlAcademicYear.SelectedValue
            pParms(2) = New SqlClient.SqlParameter("@STU_SCT_ID", SqlDbType.Int)
            pParms(2).Value = ddlSection.SelectedValue
            pParms(3) = New SqlClient.SqlParameter("@MENU_CODE", SqlDbType.VarChar, 10)
            pParms(3).Value = ViewState("MainMnu_code")
            pParms(4) = New SqlClient.SqlParameter("@SCT_DESCR", SqlDbType.VarChar, 50)
            pParms(4).Value = selectedSection.Trim
            pParms(5) = New SqlClient.SqlParameter("@STU_GRD_ID", SqlDbType.VarChar, 10)
            pParms(5).Value = ddlGrade.SelectedValue
            pParms(6) = New SqlClient.SqlParameter("@GRM_DISPLAY", SqlDbType.VarChar, 50)
            pParms(6).Value = selectedGrade.Trim
            pParms(7) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(7).Value = IIf(txtStuNo.Text.Trim = "", stunoSearch.Trim, txtStuNo.Text.Trim)
            pParms(8) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar, 310)
            pParms(8).Value = IIf(txtName.Text.Trim = "", stuNameSearch.Trim, txtName.Text.Trim)
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, Qry.ToString, pParms)

        Catch ex As Exception

        End Try


        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtSection")
        txtSearch.Text = selectedSection

        set_Menu_Img()


    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)


        PopulateSection()
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Or e.Row.RowType = DataControlRowType.Header Then
            Dim lblFrmPortal As Label = DirectCast(e.Row.FindControl("lblPortal"), Label)

            If ViewState("MainMnu_code") <> "T000200" Then
                e.Row.Cells(6).Visible = True
                ' gvStud.Columns(4).Visible = True
                'gvStud.HeaderRow.Cells(4).Visible = True


            Else
                e.Row.Cells(6).Visible = False
                '   gvStud.Columns(4).Visible = False
                ' gvStud.HeaderRow.Cells(4).Visible = True
            End If

        End If
    End Sub
    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        Try
            gvStud.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        If e.CommandName = "view" Then
            Dim url As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
            Dim lblStuId As Label
            Dim lblStuName As Label
            Dim lblStuNo As Label
            Dim lblGrade As Label
            Dim lblSection As Label
            Dim lblShfId As Label
            With selectedRow
                lblStuId = .FindControl("lblStuId")
                lblStuName = .FindControl("lblStuName")
                lblStuNo = .FindControl("lblStuNo")
                lblGrade = .FindControl("lblGrade")
                lblSection = .FindControl("lblSection")
                lblShfId = .FindControl("lblShfId")
            End With
            If ViewState("MainMnu_code") = "T000200" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Transport\studTransportDiscontinueRequest_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "T000210" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Transport\studTransportDiscontinueApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&section=" + Encr_decrData.Encrypt(lblSection.Text) + "&shfid=" + Encr_decrData.Encrypt(lblShfId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            tblTPT.Rows(5).Visible = True
            tblTPT.Rows(6).Visible = True
            hfACD_ID.Value = ddlAcademicYear.SelectedValue
            hfGRD_ID.Value = ddlGrade.SelectedValue
            hfSCT_ID.Value = ddlSection.SelectedValue
            hfSTUNO.Value = txtStuNo.Text
            hfNAME.Value = txtName.Text
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid").ToString)
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
        PopulateSection()
    End Sub
End Class
