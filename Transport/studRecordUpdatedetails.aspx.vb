Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_stuContactdetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        
            If Page.IsPostBack = False Then
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'hardcode the menu code
                If USR_NAME = "" Then
                    Response.Redirect("~\noAccess.aspx")
                End If
                Dim Cstu_id As String = Session("Cstu_id")
                If Not Request.QueryString("Stu_id") And Not Request.QueryString("Sib_id") Is Nothing Then
                    ''  ViewState("stu_id") = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
                    ViewState("stu_id") = Request.QueryString("Stu_id").ToString
                    ViewState("Sib_id") = Request.QueryString("Sib_id").ToString
                    'Session("sUsr_name") = 0
                Else
                    ViewState("stu_id") = ""
                    Response.Redirect("~\noAccess.aspx")
                End If '

                '  ViewState("viewid") = Session("Trans_Stu_ID")
                HF_SibId.Value = ViewState("Sib_id")
                HF_stuid.Value = ViewState("stu_id")
                bindEmirates(ddlFemirate)
                bindEmirates(ddlMemirate)
                bindEmirates(ddlGemirate)
                bindEmirates(ddlFemirate)
                GetNational(ddlFcountry, "C")
                GetNational(ddlMcountry, "C")
                GetNational(ddlGcountry, "C")
                GetNational(ddlFnationality, "N")
                GetNational(ddMnationality, "N")
                GetNational(ddGnationality, "N")

                GetNational(ddlFintercountry, "C")
                GetNational(ddlMintercountry, "C")
                GetNational(ddlGintercountry, "C")

                binddetails(HF_stuid.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub GetNational(ByVal ddlcountry As DropDownList, ByVal status As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sqlGetNational As String = ""

        sqlGetNational = "Select CTY_ID,case CTY_ID when '5' then 'Not Available' else isnull(CTY_NATIONALITY,'') end CTY_NATIONALITY ,CTY_DESCR from Country_m  order by CTY_NATIONALITY"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlGetNational)
        ddlcountry.Items.Clear()
        ddlcountry.DataSource = ds.Tables(0)
        If status = "C" Then
            ddlcountry.DataTextField = "CTY_DESCR"
        Else
            ddlcountry.DataTextField = "CTY_NATIONALITY"
        End If
        ddlcountry.DataValueField = "CTY_ID"
   
        ddlcountry.DataBind()
        Dim lst As New ListItem
        lst.Text = "--Select--"
        lst.Value = "0"
        ddlcountry.Items.Insert(0, lst)
    End Sub
    Sub bindEmirates(ByVal ddlEmirates As DropDownList)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "select EMR_CODE,EMR_DESCR from EMIRATE_M"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlEmirates.Items.Clear()
            ddlEmirates.DataSource = ds.Tables(0)
            ddlEmirates.DataTextField = "EMR_DESCR"
            ddlEmirates.DataValueField = "EMR_CODE"
            ddlEmirates.DataBind()
            Dim lst As New ListItem
            lst.Text = "--Select--"
            lst.Value = "0"
            ddlEmirates.Items.Insert(0, lst)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As New DataSet
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "TRANSPORT.STUCONTACTDETAILS", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        ltStudName.Text = Convert.ToString(readerStudent_Detail("STU_NAME"))
                        ltStudId.Text = Convert.ToString(readerStudent_Detail("STU_NO"))
                        'ltCLM.Text = Convert.ToString(readerStudent_Detail("clm_descr"))
                        ltGrd.Text = Convert.ToString(readerStudent_Detail("STU_GRD_ID"))
                        ltSct.Text = Convert.ToString(readerStudent_Detail("SCT_DESCR"))
                        'ltShf.Text = Convert.ToString(readerStudent_Detail("shf_descr"))
                        'ltStm.Text = Convert.ToString(readerStudent_Detail("STM_DESCR"))
                        'ltStatus.Text = Convert.ToString(readerStudent_Detail("STU_CURRSTATUS"))
                        'ltminlist.Text = Convert.ToString(readerStudent_Detail("STU_MINLIST"))
                        'ltmintype.Text = Convert.ToString(readerStudent_Detail("STU_MINLISTTYPE"))

                        ''FATHER
                        Ltl_Fname.Text = Convert.ToString(readerStudent_Detail("STS_FFIRSTNAME"))
                        txtFmiddlename.Text = Convert.ToString(readerStudent_Detail("STS_FMIDNAME"))
                        txtFLastname.Text = Convert.ToString(readerStudent_Detail("STS_FLASTNAME"))
                        ddlFnationality.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FNATIONALITY"))
                        txtFaddress1.Text = Convert.ToString(readerStudent_Detail("STS_FCOMADDR1"))
                        'txtFaddress2.Text = Convert.ToString(readerStudent_Detail("STS_FCOMADDR2"))
                        txtFstreet.Text = Convert.ToString(readerStudent_Detail("STS_FCOMSTREET"))
                        txtFarea.Text = Convert.ToString(readerStudent_Detail("STS_FCOMAREA"))
                        txtFBldg.Text = Convert.ToString(readerStudent_Detail("STS_FCOMBLDG"))
                        txtFAPPno.Text = Convert.ToString(readerStudent_Detail("STS_FCOMAPARTNO"))
                        Ltl_Fpob.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPOBOX"))
                        ddlFemirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        ddlFcountry.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FCOMCOUNTRY"))
                        txtFInteradd.Text = Convert.ToString(readerStudent_Detail("STS_FPRMADDR1"))
                        txtFIntercity.Text = Convert.ToString(readerStudent_Detail("STS_FPRMCITY"))
                        ddlFintercountry.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FPRMCOUNTRY"))
                        txtFinterPhone.Text = Convert.ToString(readerStudent_Detail("STS_FPRMPHONE"))

                        Ltl_Fpob.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPOBOX"))
                        ddlFemirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        'Ltl_FEmirate.Text = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        Ltl_FPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_FRESPHONE"))
                        Ltl_FOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_FOFFPHONE"))
                        Ltl_FMobile.Text = Convert.ToString(readerStudent_Detail("STS_FMOBILE"))
                        Ltl_FEmail.Text = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                        Ltl_FCompany.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPANY"))


                        ''MOTHER

                        Ltl_Mname.Text = Convert.ToString(readerStudent_Detail("STS_MFIRSTNAME"))
                        txtMmiddlename.Text = Convert.ToString(readerStudent_Detail("STS_MMIDNAME"))
                        txtMlastname.Text = Convert.ToString(readerStudent_Detail("STS_mLASTNAME"))
                        ddMnationality.SelectedValue = Convert.ToString(readerStudent_Detail("STS_MNATIONALITY"))
                        txtMaddress1.Text = Convert.ToString(readerStudent_Detail("STS_MCOMADDR1"))
                        'txtMaddress2.Text = Convert.ToString(readerStudent_Detail("STS_MCOMADDR2"))
                        txtMstreet.Text = Convert.ToString(readerStudent_Detail("STS_MCOMSTREET"))
                        txtmarea.Text = Convert.ToString(readerStudent_Detail("STS_MCOMAREA"))
                        txtMBldg.Text = Convert.ToString(readerStudent_Detail("STS_MCOMBLDG"))
                        txtMAPPno.Text = Convert.ToString(readerStudent_Detail("STS_MCOMAPARTNO"))
                        Ltl_Mpob.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPOBOX"))
                        ddlMemirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_MEMIR"))
                        ddlMcountry.SelectedValue = Convert.ToString(readerStudent_Detail("STS_MCOMCOUNTRY"))
                        txtMInteradd.Text = Convert.ToString(readerStudent_Detail("STS_MPRMADDR1"))
                        txtMIntercity.Text = Convert.ToString(readerStudent_Detail("STS_MPRMCITY"))
                        ddlMintercountry.SelectedValue = Convert.ToString(readerStudent_Detail("STS_MPRMCOUNTRY"))
                        txtMinterPhone.Text = Convert.ToString(readerStudent_Detail("STS_MPRMPHONE"))

                        'Ltl_MEmirate.Text = Convert.ToString(readerStudent_Detail("STS_MEMIR"))
                        Ltl_MPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_MRESPHONE"))
                        Ltl_MOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_MOFFPHONE"))
                        Ltl_MMobile.Text = Convert.ToString(readerStudent_Detail("STS_MMOBILE"))
                        Ltl_MEmail.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                        Ltl_MCompany.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPANY"))


                        ''GUARDIAN
                        Ltl_Gname.Text = Convert.ToString(readerStudent_Detail("STS_GFIRSTNAME"))
                        txtGmiddlename.Text = Convert.ToString(readerStudent_Detail("STS_GMIDNAME"))
                        txtGlastname.Text = Convert.ToString(readerStudent_Detail("STS_GLASTNAME"))
                        ddGnationality.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GNATIONALITY"))
                        txtGaddress1.Text = Convert.ToString(readerStudent_Detail("STS_GCOMADDR1"))
                        'txtGaddress2.Text = Convert.ToString(readerStudent_Detail("STS_GCOMADDR2"))

                        txtGstreet.Text = Convert.ToString(readerStudent_Detail("STS_GCOMSTREET"))
                        txtGarea.Text = Convert.ToString(readerStudent_Detail("STS_GCOMAREA"))
                        txtGBldg.Text = Convert.ToString(readerStudent_Detail("STS_GCOMBLDG"))
                        txtGAPPno.Text = Convert.ToString(readerStudent_Detail("STS_GCOMAPARTNO"))
                        Ltl_Gpob.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPOBOX"))
                        ddlGemirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        ddlGcountry.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GCOMCOUNTRY"))
                        txtGInteradd.Text = Convert.ToString(readerStudent_Detail("STS_GPRMADDR1"))
                        txtGIntercity.Text = Convert.ToString(readerStudent_Detail("STS_GPRMCITY"))
                        ddlGintercountry.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GPRMCOUNTRY"))
                        txtGinterPhone.Text = Convert.ToString(readerStudent_Detail("STS_GPRMPHONE"))

                        Ltl_Gpob.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPOBOX"))
                        ddlGemirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        'Ltl_GEmirate.Text = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        Ltl_GPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_GRESPHONE"))
                        Ltl_GOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_GOFFPHONE"))
                        Ltl_GMobile.Text = Convert.ToString(readerStudent_Detail("STS_GMOBILE"))
                        Ltl_GEmail.Text = Convert.ToString(readerStudent_Detail("STS_GEMAIL"))
                        Ltl_GCompany.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPANY"))

                        txtEmgcontact.Text = Convert.ToString(readerStudent_Detail("STU_EMGCONTACT"))
                        txtEmgcontactNAme.Text = Convert.ToString(readerStudent_Detail("STU_EMGCONTACT_NAME"))
                        txtComments.Text = Convert.ToString(readerStudent_Detail("STM_ENR_COMMENTS"))
                        Dim col As Integer
                        If Left(Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")), 1) = "F" Then
                            col = 1 ' was 2

                            Ltl_Fname.Enabled = False
                            txtFmiddlename.Enabled = False
                            txtFLastname.Enabled = False


                        ElseIf Left(Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")), 1) = "M" Then
                            col = 2 ' was 3
                            Ltl_Mname.Enabled = False
                            txtMmiddlename.Enabled = False
                            txtMlastname.Enabled = False
                        ElseIf Left(Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")), 1) = "G" Then
                            col = 3 ' was 4
                            Ltl_Gname.Enabled = False
                            txtGmiddlename.Enabled = False
                            txtGlastname.Enabled = False
                        Else
                            col = 1
                            Ltl_Fname.Enabled = False
                            txtFmiddlename.Enabled = False
                            txtFLastname.Enabled = False
                        End If

                        'table1.Rows(0).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(1).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(2).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(3).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(4).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(5).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(6).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(7).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(8).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(9).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(10).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(11).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(12).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(13).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(14).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(15).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(16).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(17).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(18).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(19).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(20).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(21).Cells(col).BgColor = "#FFFFC"

                    End While
                Else
                    lblerror.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim con As New SqlConnection(str_conn)
        Dim transaction As SqlTransaction
        con.Open()
        transaction = con.BeginTransaction("trans")
        Try
            If Page.IsValid = True Then
                Dim param(76) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@STU_ID", HF_SibId.Value)
                param(1) = New SqlClient.SqlParameter("@STS_FFIRSTNAME", Ltl_Fname.Text)
                param(2) = New SqlClient.SqlParameter("@STS_FLASTNAME", txtFLastname.Text)
                param(3) = New SqlClient.SqlParameter("@STS_FNATIONALITY", ddlFnationality.SelectedValue)
                param(4) = New SqlClient.SqlParameter("@STS_FCOMADDR1", txtFaddress1.Text)
                param(5) = New SqlClient.SqlParameter("@STS_FCOMADDR2", "")
                param(6) = New SqlClient.SqlParameter("@STS_FCOMSTREET", txtFstreet.Text)
                param(7) = New SqlClient.SqlParameter("@STS_FCOMAREA", txtFarea.Text)
                param(8) = New SqlClient.SqlParameter("@STS_FCOMBLDG", txtFBldg.Text)
                param(9) = New SqlClient.SqlParameter("@STS_FCOMAPARTNO", txtFAPPno.Text)
                param(10) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", Ltl_Fpob.Text)
                param(11) = New SqlClient.SqlParameter("@STS_FEMIR", ddlFemirate.SelectedValue)
                param(12) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", ddlFcountry.SelectedValue)
                param(13) = New SqlClient.SqlParameter("@STS_FRESPHONE", Ltl_FPhoneRes.Text)
                param(14) = New SqlClient.SqlParameter("@STS_FOFFPHONE", Ltl_FOfficePhone.Text)
                param(15) = New SqlClient.SqlParameter("@STS_FMOBILE", Ltl_FMobile.Text)
                param(16) = New SqlClient.SqlParameter("@STS_FPRMADDR1", txtFInteradd.Text)
                param(17) = New SqlClient.SqlParameter("@STS_FPRMCITY", txtFIntercity.Text)
                param(18) = New SqlClient.SqlParameter("@STS_FPRMCOUNTRY", ddlFintercountry.SelectedValue)
                param(19) = New SqlClient.SqlParameter("@STS_FPRMPHONE", txtFinterPhone.Text)
                param(20) = New SqlClient.SqlParameter("@STS_FCOMPANY", Ltl_FCompany.Text)
                param(21) = New SqlClient.SqlParameter("@STS_FEMAIL", Ltl_FEmail.Text)
                param(22) = New SqlClient.SqlParameter("@STS_MFIRSTNAME", Ltl_Mname.Text)
                param(23) = New SqlClient.SqlParameter("@STS_MLASTNAME", txtMlastname.Text)
                param(24) = New SqlClient.SqlParameter("@STS_MNATIONALITY", ddMnationality.SelectedValue)
                param(25) = New SqlClient.SqlParameter("@STS_MCOMADDR1", txtMaddress1.Text)
                param(26) = New SqlClient.SqlParameter("@STS_MCOMADDR2", "")
                param(27) = New SqlClient.SqlParameter("@STS_MCOMSTREET", txtMstreet.Text)
                param(28) = New SqlClient.SqlParameter("@STS_MCOMAREA", txtmarea.Text)
                param(29) = New SqlClient.SqlParameter("@STS_MCOMBLDG", txtMBldg.Text)
                param(30) = New SqlClient.SqlParameter("@STS_MCOMAPARTNO", txtMAPPno.Text)
                param(31) = New SqlClient.SqlParameter("@STS_MCOMPOBOX", Ltl_Mpob.Text)
                param(32) = New SqlClient.SqlParameter("@STS_MEMIR", ddlMemirate.SelectedValue)
                param(33) = New SqlClient.SqlParameter("@STS_MCOMCOUNTRY", ddlMcountry.SelectedValue)
                param(34) = New SqlClient.SqlParameter("@STS_MRESPHONE", Ltl_MPhoneRes.Text)
                param(35) = New SqlClient.SqlParameter("@STS_MOFFPHONE", Ltl_MOfficePhone.Text)
                param(36) = New SqlClient.SqlParameter("@STS_MMOBILE", Ltl_MMobile.Text)
                param(37) = New SqlClient.SqlParameter("@STS_MPRMADDR1", txtMInteradd.Text)
                param(38) = New SqlClient.SqlParameter("@STS_MPRMCITY", txtMIntercity.Text)
                param(39) = New SqlClient.SqlParameter("@STS_MPRMCOUNTRY", ddlMintercountry.SelectedValue)
                param(40) = New SqlClient.SqlParameter("@STS_MPRMPHONE", txtMinterPhone.Text)
                param(41) = New SqlClient.SqlParameter("@STS_MCOMPANY", Ltl_MCompany.Text)
                param(42) = New SqlClient.SqlParameter("@STS_MEMAIL", Ltl_MEmail.Text)
                param(43) = New SqlClient.SqlParameter("@STS_GFIRSTNAME", Ltl_Gname.Text)
                param(44) = New SqlClient.SqlParameter("@STS_GLASTNAME", txtGlastname.Text)
                param(45) = New SqlClient.SqlParameter("@STS_GNATIONALITY", ddGnationality.SelectedValue)
                param(46) = New SqlClient.SqlParameter("@STS_GCOMADDR1", txtGaddress1.Text)
                param(47) = New SqlClient.SqlParameter("@STS_GCOMADDR2", "")
                param(48) = New SqlClient.SqlParameter("@STS_GCOMSTREET", txtGstreet.Text)
                param(49) = New SqlClient.SqlParameter("@STS_GCOMAREA", txtGarea.Text)
                param(50) = New SqlClient.SqlParameter("@STS_GCOMBLDG", txtGBldg.Text)
                param(51) = New SqlClient.SqlParameter("@STS_GCOMAPARTNO", txtGAPPno.Text)
                param(52) = New SqlClient.SqlParameter("@STS_GCOMPOBOX", Ltl_Gpob.Text)
                param(53) = New SqlClient.SqlParameter("@STS_GEMIR", ddlGemirate.SelectedValue)
                param(54) = New SqlClient.SqlParameter("@STS_GCOMCOUNTRY", ddlGcountry.SelectedValue)
                param(55) = New SqlClient.SqlParameter("@STS_GRESPHONE", Ltl_GPhoneRes.Text)
                param(56) = New SqlClient.SqlParameter("@STS_GOFFPHONE", Ltl_GOfficePhone.Text)
                param(57) = New SqlClient.SqlParameter("@STS_GMOBILE", Ltl_GMobile.Text)
                param(58) = New SqlClient.SqlParameter("@STS_GPRMADDR1", txtGInteradd.Text)
                param(59) = New SqlClient.SqlParameter("@STS_GPRMCITY", txtGIntercity.Text)
                param(60) = New SqlClient.SqlParameter("@STS_GPRMCOUNTRY", ddlGintercountry.SelectedValue)
                param(61) = New SqlClient.SqlParameter("@STS_GPRMPHONE", txtGinterPhone.Text)
                param(62) = New SqlClient.SqlParameter("@STS_GCOMPANY", Ltl_GCompany.Text)
                param(63) = New SqlClient.SqlParameter("@STS_GEMAIL", Ltl_GEmail.Text)
                param(64) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))

                param(65) = New SqlClient.SqlParameter("@STS_FMIDNAME", txtFmiddlename.Text)
                param(66) = New SqlClient.SqlParameter("@STS_MMIDNAME", txtMmiddlename.Text)
                param(67) = New SqlClient.SqlParameter("@STS_GMIDNAME", txtGmiddlename.Text)
                param(68) = New SqlClient.SqlParameter("@STU_EMGCONTACT", txtEmgcontact.Text)
                param(69) = New SqlClient.SqlParameter("@STU_EMGCONTACT_NAME", txtEmgcontactNAme.Text)
                param(70) = New SqlClient.SqlParameter("@STU_ENR_COMMENTS", txtComments.Text)
                param(71) = New SqlClient.SqlParameter("@STU_ACT_ID", HF_stuid.Value)
                'param(64) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                'param(64).Direction = ParameterDirection.Output
                param(71) = New SqlClient.SqlParameter("@SESSION_USER", Session("sUsr_name"))
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "TRANSPORT.SAVE_STUCONTACTDETAILS ", param)

            End If
            transaction.Commit()
            lblerror.Text = "Records Saved sucessfully...!!!!"
        Catch ex As Exception
            transaction.Rollback()
            lblerror.Text = "Insertion Failed...!!!!"
        End Try
    End Sub
End Class
