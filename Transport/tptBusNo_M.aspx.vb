Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Transport_tptBusNo_M
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                 Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    If ViewState("datamode") = "add" Then
                        Panel2.Visible = False
                        hfBUS_ID.Value = 0
                        rfBusNo.Enabled = False
                    Else
                        Panel1.Visible = False
                        txtBusNo.ReadOnly = True
                        hfBUS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("busid").Replace(" ", "+"))
                        txtBusNo.Text = Encr_decrData.Decrypt(Request.QueryString("busno").Replace(" ", "+"))
                        rfPrefix.Enabled = False
                        rfFrm.Enabled = False
                        cvRange.Enabled = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private function SaveData(Optional ByVal check As Boolean = True) as Boolean 
        Dim busno As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
          Dim str_query As String = ""
        Dim count As Integer = 0
        Dim bnoId As Integer = 0
        Dim bnoIds As String = ""
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If ViewState("datamode") = "add" Then
                    If Val(txtTo.Text) <> 0 Then
                        Dim buses As New ArrayList

                        count = Val(txtTo.Text) - Val(txtFrom.Text) + 1
                        Dim i As Integer = 0
                        Dim checkstring As String = ""
                        While count > 0
                            busno = txtPrefix.Text.Trim + " " + (Val(txtFrom.Text.Trim) + i).ToString.Trim
                            buses.Add(busno.Trim)

                            'check if the any of the buses are already added
                            checkstring += "'" + busno + "',"
                            count -= 1
                            i += 1
                        End While
                        checkstring = checkstring.Remove(checkstring.Length - 1).Trim
                        If check = True Then
                            str_query = "SELECT count(bno_id) from TRANSPORT.BUSNOS_M where bno_descr IN (" + checkstring + ") and bno_bsu_id='" + Session("sbsuid") + "'"
                            count = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                            If count <> 0 Then
                                Panel1.Visible = False
                                Panel2.Visible = False
                                Panel3.Visible = True
                                Panel4.Visible = False
                                transaction.Dispose()
                                Return False
                            End If
                        End If

                        For i = 0 To buses.Count - 1
                            str_query = "exec[TRANSPORT].[SaveBUSNO] " + hfBUS_ID.Value + ",'" + Session("sbsuid") + "','" + buses(i) + " ','" + ViewState("datamode") + "'"
                            bnoId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                            If bnoIds.Length <> 0 Then
                                bnoIds += ","
                            End If
                            bnoIds += bnoId.ToString
                        Next
                    Else
                        busno = txtPrefix.Text.Trim + " " + txtFrom.Text.Trim
                        str_query = "SELECT count(bno_id) from TRANSPORT.BUSNOS_M where bno_descr='" + busno.Trim + "' and bno_bsu_id='" + Session("sbsuid") + "'"
                        count = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                        If count <> 0 Then
                            lblError.Text = "This record is already saved"
                            transaction.Dispose()
                            Return False
                        End If
                        str_query = "exec [TRANSPORT].[SaveBUSNO] " + hfBUS_ID.Value + ",'" + Session("sbsuid") + "','" + busno.Trim + " ','" + ViewState("datamode") + "'"
                        bnoId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                        bnoIds = bnoId.ToString
                    End If
                Else

                    If ViewState("datamode") = "edit" Then
                        str_query = "SELECT count(bno_id) from TRANSPORT.BUSNOS_M where bno_descr='" + txtBusNo.Text.Trim + "' and bno_bsu_id='" + Session("sbsuid") + "'"
                        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                        If count <> 0 Then
                            lblError.Text = "This record is already exits"
                            transaction.Dispose()
                            Return False
                        End If
                    End If
                    str_query = "exec [TRANSPORT].[SaveBUSNO] " + hfBUS_ID.Value + ",'" + Session("sbsuid") + "','" + txtBusNo.Text.Trim + "','" + ViewState("datamode") + "'"
                    bnoId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                    bnoIds = bnoId.ToString
                End If

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "BNO_ID(" + bnoIds + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using

    End Function
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ViewState("datamode") <> "none" Then
                If SaveData() = True Then
                    Panel1.Visible = True
                    Panel1.Enabled = False
                    Panel2.Visible = False
                    txtBusNo.ReadOnly = True
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            Panel3.Visible = False
            Panel1.Visible = True
            If SaveData(False) = True Then
                Panel1.Enabled = False
                Panel4.Visible = True
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                lblError.Text = "Error while saving data"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "add"
            txtPrefix.Text = "Bus No."
            txtFrom.Text = ""
            txtTo.Text = ""
            hfBUS_ID.Value = 0

            Panel1.Visible = True
            Panel2.Visible = False
            Panel3.Visible = False
            Panel1.Enabled = True

            rfPrefix.Enabled = True
            rfFrm.Enabled = True
            cvRange.Enabled = True
            rfBusNo.Enabled = False
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            Panel1.Visible = False
            Panel3.Visible = False
            Panel2.Visible = True
            txtBusNo.ReadOnly = False
            If hfBUS_ID.Value = 0 Then
                lblError.Text = "No records to edit"
                Exit Sub
            End If
            ViewState("datamode") = "edit"


            rfPrefix.Enabled = False
            rfFrm.Enabled = False
            cvRange.Enabled = False
            rfBusNo.Enabled = True

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            UtilityObj.beforeLoopingControls(Me.Page)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfBUS_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                Panel1.Visible = True
                Panel2.Visible = False
                txtPrefix.Text = ""
                txtFrom.Text = ""
                txtTo.Text = ""
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ViewState("datamode") = "delete"
            If hfBUS_ID.Value = 0 Then
                lblError.Text = "No records to delete"
                Exit Sub
            End If
            If SaveData() = True Then
                Panel1.Visible = True
                Panel2.Visible = False
                txtPrefix.Text = ""
                txtFrom.Text = ""
                txtTo.Text = ""
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                lblError.Text = "Error while saving data"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Try
            Panel1.Visible = True
            Panel2.Visible = False
            txtPrefix.Text = ""
            txtFrom.Text = ""
            txtTo.Text = ""
            'clear the textbox and set the default settings
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
