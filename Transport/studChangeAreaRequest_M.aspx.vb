Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Transport_studChangeAreaRequest_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T000180") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    lblName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                    GetData()
                    hfTCH_ID.Value = "0"
                    ' txtFrom.Text = Format(Now.Date, "dd/MMM/yyyy")
                    txtRequest.Text = Format(Now.Date, "dd/MMM/yyyy")
                    btnPrint.Enabled = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub GetData()

        Dim str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT  distinct  isnull(B.LOC_ID,0),isnull(D.SBL_ID,0),isnull(C.PNT_ID,0)," _
                                 & " isnull(E.LOC_ID,0),isnull(F.SBL_ID,0),isnull(G.PNT_ID,0)," _
                                 & " isnull(A.STU_ACD_ID,0),isnull(A.STU_ROUNDTRIP,0) " _
                                 & " FROM  STUDENT_M AS A " _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS C ON C.PNT_ID = A.STU_PICKUP" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS D ON C.PNT_SBL_ID = D.SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS B ON D.SBL_LOC_ID = B.LOC_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M AS G ON A.STU_DROPOFF = G.PNT_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.SUBLOCATION_M AS F  ON F.SBL_ID = G.PNT_SBL_ID" _
                                 & " LEFT OUTER JOIN TRANSPORT.LOCATION_M AS E   ON E.LOC_ID = F.SBL_LOC_ID " _
                                 & " WHERE STU_ID =" + hfSTU_ID.Value.ToString



        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim lis(8) As Integer
        While reader.Read

            lis(0) = reader.GetValue(0)
            lis(1) = reader.GetValue(1)
            lis(2) = reader.GetValue(2)
            lis(3) = reader.GetValue(3)
            lis(4) = reader.GetValue(4)
            lis(5) = reader.GetValue(5)
            hfACD_ID.Value = reader.GetValue(6)
            lis(8) = reader.GetValue(7)
        End While

        reader.Close()
        lblTripValue.Text = lis(8)
        If lis(8) = 0 Then
            lblTripType.Text = "RoundTrip"
        ElseIf lis(8) = 1 Then
            lblTripType.Text = "Pick Up"
        ElseIf lis(8) = 2 Then
            lblTripType.Text = "Drop Off"
        End If
        ddlTripType.Items.FindByValue(lis(8)).Selected = True
        ddlPLocation = BindLocation(ddlPLocation)
        If lis(0) <> 0 Then
            ddlPLocation.Items.FindByValue(lis(0)).Selected = True
            lblPlocation.Text = ddlPLocation.SelectedItem.Text
        End If

        hfPSBL_ID.Value = lis(1)
        ddlPSubLocation = BindSubLocation(ddlPLocation, ddlPSubLocation)
        If lis(1) <> 0 Then
            ddlPSubLocation.Items.FindByValue(lis(1)).Selected = True
            lblPSublocation.Text = ddlPSubLocation.SelectedItem.Text
        End If
        ddlPPickUp = BindPickUp(ddlPSubLocation, ddlPPickUp)
        If lis(2) <> 0 Then
            ddlPPickUp.Items.FindByValue(lis(2)).Selected = True
            lblPPickup.Text = ddlPPickUp.SelectedItem.Text
        End If
        ddlDLocation = BindLocation(ddlDLocation)
        If lis(3) <> 0 Then
            ddlDLocation.Items.FindByValue(lis(3)).Selected = True
            lblDlocation.Text = ddlDLocation.SelectedItem.Text
        End If

        hfDSBL_ID.Value = lis(4)
        ddlDSubLocation = BindSubLocation(ddlDLocation, ddlDSubLocation)
        If lis(4) <> 0 Then
            ddlDSubLocation.Items.FindByValue(lis(4)).Selected = True
            lblDsublocation.Text = ddlDSubLocation.SelectedItem.Text
        End If
        ddlDPickUp = BindPickUp(ddlDSubLocation, ddlDPickUp)
        If lis(5) <> 0 Then
            ddlDPickUp.Items.FindByValue(lis(5)).Selected = True
            lblDPickup.Text = ddlDPickUp.SelectedItem.Text
        End If

    End Sub


    Function BindLocation(ByVal ddlLocation As DropDownList)
        ddlLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT DISTINCT loc_description,loc_id FROM TRANSPORT.location_m AS A " _
                                & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.LOC_ID=B.PNT_LOC_ID " _
                                 & " WHERE PNT_bsu_id='" + Session("sbsuid") + "' AND LOC_COUNTRY_ID = (SELECT BSU_COUNTRY_ID FROM OASIS..BUSINESSUNIT_M WHERE BSU_id = '" + Session("sbsuid") + "')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "loc_description"
        ddlLocation.DataValueField = "loc_id"
        ddlLocation.DataBind()
        Return ddlLocation
    End Function

    Function BindSubLocation(ByVal ddlLocation As DropDownList, ByVal ddlSubLocation As DropDownList)
        Dim lintCheckRate

        ddlSubLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT DISTINCT sbl_description,sbl_id FROM TRANSPORT.sublocation_m AS A " _
                                 & " INNER JOIN TRANSPORT.PICKUPPOINTS_M AS B ON A.SBL_ID=B.PNT_SBL_ID " _
                                & " WHERE pnt_bsu_id='" + Session("sbsuid") + "' and sbl_loc_id=" + ddlLocation.SelectedValue.ToString

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_CHECK_RATE", pParms)
            While reader.Read
                lintCheckRate = Convert.ToString(reader("BSU_bCheckRate"))
            End While
        End Using

        If lintCheckRate = 1 Then
            If ddlTripType.SelectedValue = 0 Then
                str_Sql += " AND SBL_ID IN(SELECT SBL_ID FROM TRANSPORT.FN_GETAREARATE(" + hfACD_ID.Value + "))"
            Else
                str_Sql += " AND SBL_ID IN(SELECT SBL_ID FROM TRANSPORT.FN_GETAREARATE(" + hfACD_ID.Value + ") WHERE ISNULL(ONEWAY,0)<>0)"
            End If
        End If


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSubLocation.DataSource = ds
            ddlSubLocation.DataTextField = "sbl_description"
            ddlSubLocation.DataValueField = "sbl_id"
            ddlSubLocation.DataBind()
            Return ddlSubLocation
    End Function

    Function BindPickUp(ByVal ddlSubLocation As DropDownList, ByVal ddlPickUp As DropDownList)
        ddlPickUp.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String = "SELECT pnt_description,pnt_id FROM TRANSPORT.pickuppoints_m WHERE pnt_bsu_id='" + Session("sbsuid") + "' and pnt_sbl_id='" + ddlSubLocation.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlPickUp.DataSource = ds
        ddlPickUp.DataTextField = "pnt_description"
        ddlPickUp.DataValueField = "pnt_id"
        ddlPickUp.DataBind()
        Return ddlPickUp
    End Function

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim PLoc As Integer = 0
        Dim PSubLoc As Integer = 0
        Dim PPickUp As Integer = 0
        Dim DLoc As Integer = 0
        Dim DSubLoc As Integer = 0
        Dim DPickUp As Integer = 0

        If ddlTripType.SelectedValue = 0 Then
            PLoc = ddlPLocation.SelectedValue
            PSubLoc = ddlPSubLocation.SelectedValue
            PPickUp = ddlPPickUp.SelectedValue
            DLoc = ddlDLocation.SelectedValue
            DSubLoc = ddlDSubLocation.SelectedValue
            DPickUp = ddlDPickUp.SelectedValue
        ElseIf ddlTripType.SelectedValue = 1 Then
            PLoc = ddlPLocation.SelectedValue
            PSubLoc = ddlPSubLocation.SelectedValue
            PPickUp = ddlPPickUp.SelectedValue
            DLoc = ddlPLocation.SelectedValue
            DSubLoc = ddlPSubLocation.SelectedValue
            DPickUp = ddlPPickUp.SelectedValue
        ElseIf ddlTripType.SelectedValue = 2 Then
            PLoc = ddlDLocation.SelectedValue
            PSubLoc = ddlDSubLocation.SelectedValue
            PPickUp = ddlDPickUp.SelectedValue
            DLoc = ddlDLocation.SelectedValue
            DSubLoc = ddlDSubLocation.SelectedValue
            DPickUp = ddlDPickUp.SelectedValue
        End If

        'Dim str_query As String = " exec TRANSPORT.saveSTUDCHANGEAREAREQ " _
        '                         & "'" + hfTCH_ID.Value + "'," _
        '                         & hfSTU_ID.Value + "," _
        '                         & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'," _
        '                         & ddlPLocation.SelectedValue.ToString + "," _
        '                         & ddlPSubLocation.SelectedValue.ToString + "," _
        '                         & ddlPPickUp.SelectedValue.ToString + "," _
        '                         & ddlDLocation.SelectedValue.ToString + "," _
        '                         & ddlDSubLocation.SelectedValue.ToString + "," _
        '                         & ddlDPickUp.SelectedValue.ToString + "," _
        '                         & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
        '                         & "'add'," _
        '                         & "'" + txtRequest.Text.Replace("'", "''") + "'," _
        '                         & "'" + Session("sUsr_name") + "'"
        Dim str_query As String = " exec TRANSPORT.saveSTUDCHANGEAREA_REQUEST " _
                                 & "'" + hfTCH_ID.Value + "'," _
                                 & hfSTU_ID.Value + "," _
                                 & "'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "'," _
                                 & PLoc.ToString() + "," _
                                 & PSubLoc.ToString() + "," _
                                 & PPickUp.ToString() + "," _
                                 & DLoc.ToString() + "," _
                                 & DSubLoc.ToString() + "," _
                                 & DPickUp.ToString() + "," _
                                 & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
                                 & "'add'," _
                                 & "'" + txtRequest.Text.Replace("'", "''") + "'," _
                                 & "'" + Session("sUsr_name") + "'," _
                                 & "'" + ddlTripType.SelectedValue.ToString() + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
    Private Function SAVE_TRANSPORT_AREACHANGE_REQ() As Integer
        SAVE_TRANSPORT_AREACHANGE_REQ = 1
        lblError.Text = ""
        'Dim TODATE As New Object
        'If IsDate(txtTo.Text) Then
        '    TODATE = CDate(txtTo.Text)
        'Else
        '    TODATE = DBNull.Value
        'End If
        'If ddlType.SelectedValue = "P" Then
        '    TODATE = DBNull.Value
        'End If
        Dim PLoc As Integer = 0
        Dim PSubLoc As Integer = 0
        Dim PPickUp As Integer = 0
        Dim DLoc As Integer = 0
        Dim DSubLoc As Integer = 0
        Dim DPickUp As Integer = 0

        If ddlTripType.SelectedValue = 0 Then
            PLoc = ddlPLocation.SelectedValue
            PSubLoc = ddlPSubLocation.SelectedValue
            PPickUp = ddlPPickUp.SelectedValue
            DLoc = ddlDLocation.SelectedValue
            DSubLoc = ddlDSubLocation.SelectedValue
            DPickUp = ddlDPickUp.SelectedValue
        ElseIf ddlTripType.SelectedValue = 1 Then
            PLoc = ddlPLocation.SelectedValue
            PSubLoc = ddlPSubLocation.SelectedValue
            PPickUp = ddlPPickUp.SelectedValue
            DLoc = ddlPLocation.SelectedValue
            DSubLoc = ddlPSubLocation.SelectedValue
            DPickUp = ddlPPickUp.SelectedValue
        ElseIf ddlTripType.SelectedValue = 2 Then
            PLoc = ddlDLocation.SelectedValue
            PSubLoc = ddlDSubLocation.SelectedValue
            PPickUp = ddlDPickUp.SelectedValue
            DLoc = ddlDLocation.SelectedValue
            DSubLoc = ddlDSubLocation.SelectedValue
            DPickUp = ddlDPickUp.SelectedValue
        End If
        
        Dim conn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        conn.Open()
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction()
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("TRANSPORT.saveSTUDCHANGEAREA_REQUEST", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlp1 As New SqlParameter("@TCH_STU_ID", SqlDbType.Int)
            sqlp1.Value = hfSTU_ID.Value
            cmd.Parameters.Add(sqlp1)

            Dim sqlp2 As New SqlParameter("@TCH_ID", SqlDbType.Int)
            sqlp2.Value = hfTCH_ID.Value
            cmd.Parameters.Add(sqlp2)

            Dim sqlp3 As New SqlParameter("@TCH_STARTDATE", SqlDbType.DateTime)
            sqlp3.Value = Format(Date.Parse(txtFrom.Text), "dd/MMM/yyyy")
            cmd.Parameters.Add(sqlp3)

            Dim sqlp4 As New SqlParameter("@TCH_LOC_ID_PICKUP", SqlDbType.Int)
            sqlp4.Value = PLoc.ToString()
            cmd.Parameters.Add(sqlp4)

            Dim sqlp5 As New SqlParameter("@TCH_SBL_ID_PICKUP", SqlDbType.Int)
            sqlp5.Value = PSubLoc.ToString()
            cmd.Parameters.Add(sqlp5)

            Dim sqlp6 As New SqlParameter("@TCH_PNT_ID_PICKUP", SqlDbType.Int)
            sqlp6.Value = PPickUp.ToString()
            cmd.Parameters.Add(sqlp6)

            Dim sqlp7 As New SqlParameter("@TCH_LOC_ID_DROPOFF", SqlDbType.Int)
            sqlp7.Value = DLoc.ToString()
            cmd.Parameters.Add(sqlp7)

            Dim sqlp8 As New SqlParameter("@TCH_SBL_ID_DROPOFF", SqlDbType.Int)
            sqlp8.Value = DSubLoc.ToString
            cmd.Parameters.Add(sqlp8)

            Dim sqlp9 As New SqlParameter("@TCH_PNT_ID_DROPOFF", SqlDbType.Int)
            sqlp9.Value = DPickUp.ToString
            cmd.Parameters.Add(sqlp9)

            Dim sqlp10 As New SqlParameter("@TCH_REMARKS", SqlDbType.VarChar, 255)
            sqlp10.Value = txtRemarks.Text.Replace("'", "''")
            cmd.Parameters.Add(sqlp10)

            Dim sqlp11 As New SqlParameter("@DATAMODE", SqlDbType.VarChar, 10)
            sqlp11.Value = "add"
            cmd.Parameters.Add(sqlp11)

            Dim sqlp12 As New SqlParameter("@TCH_REQDATE", SqlDbType.DateTime)
            sqlp12.Value = CDate(txtRequest.Text.Replace("'", "''"))
            cmd.Parameters.Add(sqlp12)

            Dim sqlp13 As New SqlParameter("@TCH_REQUSR", SqlDbType.VarChar, 100)
            sqlp13.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlp13)

            Dim sqlp14 As New SqlParameter("@TCH_ROUNDTRIP", SqlDbType.Int)
            sqlp14.Value = ddlTripType.SelectedValue.ToString
            cmd.Parameters.Add(sqlp14)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            SAVE_TRANSPORT_AREACHANGE_REQ = retSValParam.Value
            If retSValParam.Value = 0 Then
                trans.Commit()
                lblError.Text = "Request saved successfully"
            Else
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(SAVE_TRANSPORT_AREACHANGE_REQ)
            End If
        Catch ex As Exception
            trans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function
    Function CheckServiceStartingDate() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(SSV_ID) FROM STUDENT_SERVICES_D WHERE SSV_STU_ID=" + hfSTU_ID.Value _
                                & " AND SSV_ACD_ID=" + hfACD_ID.Value + " AND SSV_SVC_ID=1 AND CONVERT(DATETIME,SSV_FROMDATE)>= " _
                                & " CONVERT(DATETIME,'" + Format(Date.Parse(txtFrom.Text), "yyyy-MM-dd") + "') AND SSV_TODATE IS NULL"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Dim sDate As DateTime
        If count > 0 Then
            str_query = "SELECT SSV_FROMDATE FROM STUDENT_SERVICES_D WHERE SSV_STU_ID=" + hfSTU_ID.Value _
                                & " AND SSV_ACD_ID=" + hfACD_ID.Value + " AND SSV_SVC_ID=1 AND SSV_TODATE IS NULL "
            sDate = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            Return sDate
        Else
            Return ""
        End If
    End Function

    Sub CallReport(ByVal STU_ID As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("AO", GetEmpName("ADMIN OFFICER"))
        param.Add("@STU_ID", STU_ID)
        param.Add("bsu", GetBsuName)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptChangeAreaReq.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Function GetBsuName() As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim bsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bsu
    End Function


    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not emp Is Nothing Then
            Return emp
        Else
            Return ""
        End If
    End Function

    Function CheckFutureDatedRequest() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(TCH_ID) FROM STUDENT_CHANGEAREAREQ_S WHERE " _
                                & " TCH_BAPPROVE='TRUE' AND TCH_STU_ID=" + hfSTU_ID.Value _
                                & " AND CONVERT(DATETIME,TCH_STARTDATE)>=CONVERT(DATETIME,GETDATE())"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If hfPSBL_ID.Value = ddlPSubLocation.SelectedValue And hfDSBL_ID.Value = ddlDSubLocation.SelectedValue And lblTripValue.Text = ddlTripType.SelectedValue Then
                lblError.Text = "Request could not be processed as there is no change in area"
                Exit Sub
            End If

            If CheckFutureDatedRequest() = True Then
                lblError.Text = "Request could not be processed a future dated request already exist"
                Exit Sub
            End If

            Dim sDate As String = CheckServiceStartingDate()
            If sDate = "" Then
                If studClass.checkFeeClosingDate(Session("sbsuid"), hfACD_ID.Value, Date.Parse(txtFrom.Text)) = True Then
                    'SaveData()
                    'lblError.Text = "Record saved successfully"
                    If SAVE_TRANSPORT_AREACHANGE_REQ() = 0 Then
                        btnSave.Enabled = False
                        btnPrint.Enabled = True
                    End If
                Else
                    lblError.Text = "The service strarting date has to be higher than the fee closing date"
                End If
            Else
                lblError.Text = "The service starting date has to be a date higer than " + Format(Date.Parse(sDate), "dd/MMM/yyyy")
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            If ex.Message.ToLower = "can not update..charge for a future term exists. error while charging fee" Then
                lblError.Text = "Can not update..Charge For a future term exists."
            Else
                lblError.Text = "Request could not be processed"
            End If
        End Try
    End Sub

    Protected Sub ddlDLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLocation.SelectedIndexChanged
        Try
            BindSubLocation(ddlDLocation, ddlDSubLocation)
            BindPickUp(ddlDSubLocation, ddlDPickUp)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlDSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDSubLocation.SelectedIndexChanged
        Try
            BindPickUp(ddlDSubLocation, ddlDPickUp)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlPLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPLocation.SelectedIndexChanged
        Try
            BindSubLocation(ddlPLocation, ddlPSubLocation)
            BindPickUp(ddlPSubLocation, ddlPPickUp)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



    Protected Sub ddlPSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPSubLocation.SelectedIndexChanged
        Try
            BindPickUp(ddlPSubLocation, ddlPPickUp)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
         Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CallReport(hfSTU_ID.Value)
    End Sub

    Protected Sub ddlTripType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTripType.SelectedIndexChanged
        ddlPSubLocation = BindSubLocation(ddlPLocation, ddlPSubLocation)
         ddlPPickUp = BindPickUp(ddlPSubLocation, ddlPPickUp)
          ddlDLocation = BindLocation(ddlDLocation)
        ddlDSubLocation = BindSubLocation(ddlDLocation, ddlDSubLocation)
       ddlDPickUp = BindPickUp(ddlDSubLocation, ddlDPickUp)
    End Sub
End Class
