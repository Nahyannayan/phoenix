<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptTripTimings_M.aspx.vb" Inherits="Transport_tptTripTimings_M"  Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Pickup Points Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">


<table id="tbl_ShowScreen" runat="server" align="center" width="100%">
            
            <tr>
                <td align="left"><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>  
                        </tr>
         
           <tr>
      
            <td align="center">
            
  <table id="Table1" runat="server" align="center"  width="100%">
                       
                                
                    
        <tr><td align="center">
         
         <table id="Table2" runat="server" align="center" width="100%">
          
          
      
            <tr><td align="center" width="100%">
                      <asp:GridView ID="gvTptTime" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    PageSize="20" Width="100%">
                     <Columns>
                    
                     <asp:TemplateField  Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblTppId" runat="server" Text='<%# Bind("tpp_id") %>'></asp:Label>
                       </ItemTemplate>
                     </asp:TemplateField>
              
                     <asp:TemplateField HeaderText="Pick Up Point" >
                       <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:Label ID="lblPickUpPoint" runat="server" text='<%# Bind("pnt_description") %>' ></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                   
                                         
                       
                       <asp:TemplateField HeaderText="Arrival Time (HH:MM)" >
                       <ItemStyle HorizontalAlign="Left" Width="30%" VerticalAlign="Middle"/>
                       <ItemTemplate>
                           <div style="width:30%;">
                        <asp:TextBox ID="txtTime" ValidationGroup='<%# Eval("tpp_id") %>' runat="server" CausesValidation="true" style="width:50% !important;" Text='<%# Bind("tpp_time") %>'></asp:TextBox>
                        <asp:DropDownList runat="server" ID="ddlTime">
                        <asp:ListItem>AM</asp:ListItem>                        
                        <asp:ListItem>PM</asp:ListItem>                        
                        </asp:DropDownList>
                        <asp:RegularExpressionValidator ID="regPickUp" style="width:30% !important;" runat="server" ControlToValidate="txtTime" ValidationGroup='<%# Eval("tpp_id") %>'
                       Display="static" ErrorMessage="Please enter a valid time" 
                       ValidationExpression="(^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])$)">
                       </asp:RegularExpressionValidator></div>
                         </ItemTemplate>
                       </asp:TemplateField>
                   
                   </Columns>  
                     <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                          <RowStyle CssClass="griditem" Wrap="False" />
                          <SelectedRowStyle CssClass="Green" Wrap="False" />
                          <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                          <EmptyDataRowStyle Wrap="False" />
                          <EditRowStyle Wrap="False" />
                     </asp:GridView>
                   </td></tr>
                   </table>
           </td>
           </tr>
            <tr>
            <td align="center">
               
                 <asp:Button ID="btnSave" CausesValidation="true" UseSubmitBehavior="true"  runat="server" CssClass="button" Text="Save" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
              
                   
                    </td>
        </tr>
        
           </table>
            
                    </td></tr>
          
        </table>
    <asp:HiddenField ID="hfTRD_ID" runat="server" />

                </div>
            </div>
        </div>
    
</asp:Content>

