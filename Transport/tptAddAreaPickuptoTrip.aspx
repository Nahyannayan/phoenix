<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tptAddAreaPickuptoTrip.aspx.vb" Inherits="Transport_tptAddPickuptoTrip" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Trips</title>
    <base target="_self" />
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap header files ends here -->

    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">


        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvTrip.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }


    </script>
</head>
<body onload="listen_window();">

    <form id="form1" runat="server">
        <div>

            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>

                    <td align="center">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Select Area</span></td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlSubLocation" runat="server" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Select Pickup</span></td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlPickup" runat="server" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Select Bus No</span> </td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlBusNo" runat="server" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Select Shift</span></td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:GridView ID="gvTrip" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                    Width="100%" CssClass="table table-bordered table-row">
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="sbg_id" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrpId" runat="server" Text='<%# Bind("TRP_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="sbg_id" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrdId" runat="server" Text='<%# Bind("TRD_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="Select">
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                            </EditItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <HeaderStyle Wrap="False" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                                Select
                                                                <br />
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                    ToolTip="Click here to select/deselect all rows" />
                                                            </HeaderTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Trip" ShowHeader="False">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblParent" runat="server" Text="Trip"></asp:Label><br />
                                                                <asp:TextBox ID="txtTrip" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnTrip" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClick="btnTrip_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblParent" runat="server" Text='<%# BIND("TRP_DESCR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Journey">
                                                            <HeaderTemplate>
                                                                Journey<br />
                                                                <asp:DropDownList ID="ddlgvJourney" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlgvJourney_SelectedIndexChanged">
                                                                    <asp:ListItem>ALL</asp:ListItem>
                                                                    <asp:ListItem>Onward</asp:ListItem>
                                                                    <asp:ListItem>Return</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblJourney" runat="server" Text='<%# BIND("TRP_JOURNEY") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    <RowStyle CssClass="griditem" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                        <input id="h_Selected_menu_2"
                            runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_1" runat="server"
                            type="hidden" value="=" />
                        &nbsp;
                    </td>
                </tr>
            </table>

        </div>




        <asp:HiddenField ID="hfPNT_ID" runat="server" />

    </form>
</body>
</html>
