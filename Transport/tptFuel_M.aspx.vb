Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Transport_tptFuel_M
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                       Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "T100250") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfVEH_ID.Value = Encr_decrData.Decrypt(Request.QueryString("vehicleid").Replace(" ", "+"))
                    txtVeh.Text = Encr_decrData.Decrypt(Request.QueryString("vehicle").Replace(" ", "+"))
                    txtFill.Text = Format(Now.Date, "dd/MMM/yyyy")
                    'If ViewState("datamode") = "edit" Then
                    ' hfTFL_ID.Value = Encr_decrData.Decrypt(Request.QueryString("tflid").Replace(" ", "+"))
                    'BindData()
                    ' Else
                    hfTFL_ID.Value = 0

                    ' End If

                    GetPreviousReading()



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If

    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    'Sub BindData()
    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    '    Dim str_query As String = "SELECT TFL_CURRENTKM,TFL_GALLON,TFL_AMOUNT,TFL_FILLDATE " _
    '                            & " FROM TRANSPORT.TRIP_FUEL_S WHERE TFL_ID=" + hfTFL_ID.Value
    '    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '    While reader.Read
    '        txtCurrent.Text = reader.GetDouble(0)
    '        txtGallon.Text = reader.GetDouble(1)
    '        txtAmt.Text = reader.GetDouble(2)
    '        txtFill.Text = Format(Date.Parse(reader.GetDateTime(3)), "dd/MMM/yyyy")
    '    End While
    '    reader.Close()

    'End Sub
    Function SaveData() As Boolean
        Dim str_query As String = "exec TRANSPORT.saveFUEL " _
                                & hfTFL_ID.Value + "," _
                                & hfVEH_ID.Value + "," _
                                & Val(txtPrev.Text).ToString + "," _
                                & txtCurrent.Text + "," _
                                & txtGallon.Text + "," _
                                & txtAmt.Text + "," _
                                & "'" + Format(Date.Parse(txtFill.Text), "yyyy-MM-dd") + "'," _
                                & "'" + ViewState("datamode") + "'"
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'If ViewState("datamode") = "edit" Then
                '    UtilityObj.InsertAuditdetails(transaction, "edit", "TRANSPORT.TRIP_FUEL_S", "TFL_ID", "TFL_VEH_ID", "TFL_ID=" + hfTFL_ID.Value.ToString)
                'End If
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()
                lblError.Text = "Record saved successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                Return False
            End Try
        End Using
    End Function
     
#End Region

   
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If SaveData() = True Then
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Sub GetPreviousReading()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT ISNULL(MAX(TFL_CURRENTKM),0) FROM TRANSPORT.TRIP_FUEL_S WHERE TFL_VEH_ID=" + hfVEH_ID.Value _
                                & " AND TFL_FILLDATE=(SELECT MAX(TFL_FILLDATE) FROM TRANSPORT.TRIP_FUEL_S WHERE TFL_VEH_ID=" + hfVEH_ID.Value _
                                & " AND TFL_ID<>" + hfTFL_ID.Value + " AND TFL_FILLDATE<='" + Format(Date.Parse(txtFill.Text), "yyyy-MM-dd") + "')"
        txtPrev.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfTFL_ID.Value = 0
          
            Response.Redirect(ViewState("ReferrerUrl"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

  
End Class
