<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTransportRequestChange.aspx.vb" Inherits="Transport_studTransportRequestChange" Title="Change Transport Request" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">



        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}
function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

function AddtoTrip() {
    var sFeatures;
    sFeatures = "dialogWidth: 500px; ";
    sFeatures += "dialogHeight: 400px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var url;
    url = 'tptAddAreaPickuptoTrip.aspx?';
    // window.showModalDialog(url,"", sFeatures);

    //return false;                           
    var oWnd = radopen(url, "pop_addtotrip");

}



function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_addtotrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-bus mr-3"></i> <asp:Label id="lblCaption" runat="server" Text="Update Transport Request Form Response"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

               
                <table id="tbl_ShowScreen" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" align="center">
                            <table align="center" width="100%">
                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Curriculum</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlClm_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"  width="20%"><span class="field-label">Academic Year</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <%--<tr>
                    <td align="left">
                        Academic Year&nbsp;</td>
                    <td style="width: 2px">
                        :</td>
                    <td align="left" colspan="9" style="text-align: left">
                        <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                        </asp:DropDownList></td>
                </tr>--%>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Section</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Shift</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Transport Avail Students</span></td>
                                    
                                    <td align="left" colspan="2" style="text-align: left">
                                        <asp:RadioButton ID="rbTransYes" runat="server" CssClass="field-label" AutoPostBack="True" GroupName="Trans" Text="Yes" Checked="True"></asp:RadioButton>
                                        <asp:RadioButton ID="rbTransNo" runat="server" CssClass="field-label" AutoPostBack="True" GroupName="Trans" Text="No"></asp:RadioButton>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="left"><span class="field-label">Bus No</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlBusNo" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <asp:RadioButton ID="rdOnward" runat="server" CssClass="field-label" Checked="True" GroupName="g1" Text="Onward"></asp:RadioButton>
                                        <asp:RadioButton ID="rdReturn" runat="server" CssClass="field-label" GroupName="g1" Text="Return"></asp:RadioButton></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search"  />
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Print" Visible="False" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" ></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                 PageSize="40" AllowSorting="True" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Available">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" Width="24px"></asp:CheckBox>

                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Select <br />
                                           <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server" Width="32px" ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                                   

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server" Width="25px" Height="17px"></asp:CheckBox>

                                        </ItemTemplate>

                                        <HeaderStyle Wrap="False"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblShfId" runat="server" Text='<%# Bind("STU_SHF_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stud No.">
                                        <HeaderTemplate>
                                           <asp:Label ID="lblFeeHeader" runat="server" Text="Student ID" ></asp:Label><br />
                                                                            <asp:TextBox ID="txtFeeSearch" runat="server" Width="75%"></asp:TextBox>
                                                                            <asp:ImageButton ID="btnFeeId_Search" OnClick="btnFeeId_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>

                                        </ItemTemplate>

                                        <HeaderStyle></HeaderStyle>

                                        <ItemStyle ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                           <asp:Label ID="lblName" runat="server" Text="Student Name" ></asp:Label><br />
                                            <asp:TextBox ID="txtStudName" runat="server" Width="75%"></asp:TextBox>
                                           <asp:ImageButton ID="btnStudName_Search" OnClick="btnStudName_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>

                                        </ItemTemplate>

                                        <HeaderStyle></HeaderStyle>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Area / Trip">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPickUp" runat="server" Text='<%# Bind("PICKUPOLD") %>' >
                                            </asp:Label>&nbsp;
                                        </ItemTemplate>

                                        <HeaderStyle></HeaderStyle>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Avail Trpt:">
                                        <ItemTemplate>
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server"  RepeatDirection="Horizontal">
                                                <asp:ListItem Value="N">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="Y">Yes</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblEdit" CommandName="Edit" runat="server" Text="Change Details" OnClick="lblEdit_Click" />


                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle  CssClass="gridheader_pop" />
                                <%--<AlternatingRowStyle CssClass="griditem_alternative" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">

                            <asp:Panel ID="pnlPopup" runat="server" CssClass="panel-cover" Width="100%">
                                <asp:UpdatePanel ID="uppnlPopup" runat="server">
                                    <ContentTemplate>
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td colspan="4" class="title-bg">Select Choice </td>
                                                </tr>
                                                <tr align="left">
                                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>
                                                   
                                                    <td align="left" width="30%">
                                                        <asp:Label ID="lblstudNo" runat="server" CssClass="field-value"></asp:Label>
                                                    </td>
                                               
                                                    <td align="left" width="20%"><span class="field-label">Student Name</span></td>
                                                   
                                                    <td align="left" width="30%">
                                                        <asp:Label ID="lblStudName" runat="server" CssClass="field-value"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Trip Type</span></td>
                                                   
                                                    <td align="left" width="30%">
                                                        <asp:Label ID="lblTripType" runat="server" CssClass="field-value"></asp:Label></td>
                                                
                                                    <td align="left" width="20%"><span class="field-label">Select Trip Type</span></td>
                                                    
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="ddlTripType" runat="server" AutoPostBack="true" Width="182px">
                                                            <asp:ListItem Value="0">Round Trip</asp:ListItem>
                                                            <asp:ListItem Value="1">Pick Up</asp:ListItem>
                                                            <asp:ListItem Value="2">Drop Off</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <table width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="title-bg" align="center">Area</td>
                                                                    <td class="title-bg" align="center">Trip</td>
                                                                    <td class="title-bg" align="center">Pickup</td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblPArea" runat="server" Visible="False" CssClass="field-value"></asp:Label>
                                                                    </td>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblPTrip" runat="server" Visible="False" CssClass="field-value"></asp:Label>
                                                                    </td>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblPickup" runat="server" Visible="False" CssClass="field-value"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr align="left">
                                                                    <td align="center">
                                                                        <asp:DropDownList ID="ddlPArea" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                                                    <td align="center">
                                                                        <asp:DropDownList ID="ddlPTrip" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                                                    <td align="center">
                                                                        <asp:DropDownList ID="ddlPickup" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr align="left">
                                                                    <td  align="left" colspan="4">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblDArea" runat="server" Visible="False" CssClass="field-value"></asp:Label></td>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblDTrip" runat="server" Visible="False" CssClass="field-value"></asp:Label></td>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblDropoff" runat="server" Visible="False" CssClass="field-value"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td  align="center">
                                                                        <asp:DropDownList ID="ddlDArea" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                                                    <td align="center">
                                                                        <asp:DropDownList ID="ddlDTrip" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                                                    <td align="center">
                                                                        <asp:DropDownList ID="ddlDropOff" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                                                </tr>
                                                                <tr align="center">
                                                                    <td colspan="4">
                                                                        <asp:Button ID="btnStudSave" runat="server" Text="Save" CssClass="button" ValidationGroup="Popup"></asp:Button>
                                                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" CausesValidation="false"></asp:Button>&nbsp; </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnStudSave" />
                                    </Triggers>

                                </asp:UpdatePanel>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="MPChangeTransport" runat="server" BackgroundCssClass="modalBackground"
                                DropShadow="true" PopupControlID="pnlPopup"
                                RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="lblError">
                            </ajaxToolkit:ModalPopupExtender>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" TabIndex="4" ValidationGroup="groupM1" Visible="False" />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="height: 15px">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="hfPSBL_ID" runat="server" />
    <asp:HiddenField ID="hfDSBL_ID" runat="server" />
    <br />
    <asp:HiddenField ID="hfPTRP_ID" runat="server" />
    <asp:HiddenField ID="hfDTRP_ID" runat="server" />
    <asp:HiddenField ID="hfPICKUP" runat="server" />
    <asp:HiddenField ID="hfDROPOFF" runat="server" />
    <asp:HiddenField ID="hfROUNDTRIP" runat="server" />


    <asp:HiddenField ID="hfSTU_ID" runat="server" />


</asp:Content>

