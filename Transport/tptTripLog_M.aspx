<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="tptTripLog_M.aspx.vb" Inherits="Transport_tptTripDetails_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i> Trips Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">


<table id="tbl_ShowScreen" runat="server" align="center"  width="100%">
            
            <tr>
                <td align="left" class="error">
                <asp:Literal ID="ltError" runat="server"></asp:Literal>
               </td>
               </tr>
               <tr>
                  <td align="left" ><asp:Label ID="lblError" runat="server" CssClass="error" ></asp:Label> 
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
              HeaderText="You must enter a value in the following fields:" ValidationGroup="groupM1" />
         <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTripDate"
               Display="None" ErrorMessage="Enter the Trip Date  in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
               ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
               ValidationGroup="groupM1"></asp:RegularExpressionValidator>&nbsp;
               <asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtTripDate"
                 Display="None" EnableViewState="False" ErrorMessage="Trip Date  entered is not a valid date"
                 ValidationGroup="groupM1"></asp:CustomValidator>
              <asp:RequiredFieldValidator ID="rfFrom" runat="server" ErrorMessage="Please enter the field trip date " ControlToValidate="txtTripDate" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                 </td> 
                        </tr>
                  
        
           <tr>
      
            <td align="center">
            
  <table id="Table1" runat="server" align="center" width="100%" >
         
         <tr>
         <td width="100%">
              <table id="Table3" runat="server" align="center" width="100%" >
                  <tr>
                      <td align="left" width="15%">
                          <span class="field-label">Vehicle Registratiion No</span></td>
                      
                      <td align="left" width="20%">
                          <asp:TextBox ID="txtVehNo" runat="server" Enabled="False">
                          </asp:TextBox></td>
                    
               <td align="left" width="15%">
                    <asp:Label ID="Label2" runat="server" CssClass="field-label" Text="Trip Date"></asp:Label></td>                   
                 <td align="left" width="20%">
                <asp:TextBox ID="txtTripDate" runat="server"></asp:TextBox>
               <asp:ImageButton ID="imgBtnTripDate" runat="server" ImageUrl="~/Images/calendar.gif" />
               
               </td>
                      
              <td align="center" width="30%">
             
                <asp:Button id="btnShow" runat="server" Text="List" CssClass="button" CausesValidation="false" ></asp:Button>
                </td>  
              
                                  </tr>
                    </table>
         
         </td>
         </tr>             
                    
           <tr>
          <td align="left" class="title-bg">          
        Details</td>
         </tr>                      
                    
        <tr><td align="center" width="100%">
         
         <table id="Table2" runat="server" align="center" width="100%">
          <tr>
          <td align="left" width="50%">
              <asp:LinkButton ID="lbAddNew" runat="server" Visible="False">Add New Trip</asp:LinkButton></td>
          <td align="right" width="50%">
              <asp:RadioButton ID="rdRegular" runat="server" CssClass="field-label" AutoPostBack="True" Checked="True"
                  GroupName="G1" Text="Regular Trip" />
              <asp:RadioButton ID="rdExtra" runat="server" CssClass="field-label" AutoPostBack="True" Text="Extra Trip" GroupName="G1" /></td>
          </tr>
                   <tr><td align="center" colspan="2">
                      <asp:GridView ID="gvTptTrip" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     PageSize="20" Width="100%">
                     <Columns>
                     <asp:TemplateField  Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblTrlId" runat="server" Text='<%# Bind("TRL_ID") %>'></asp:Label>
                       </ItemTemplate>
                     </asp:TemplateField>
              
                        <asp:TemplateField  Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblTrdId" runat="server" Text='<%# Bind("TRD_ID") %>'></asp:Label>
                       </ItemTemplate>
                     </asp:TemplateField>
                     
              
                     <asp:TemplateField  Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblTrpId" runat="server" Text='<%# Bind("TRP_ID") %>'></asp:Label>
                       </ItemTemplate>
                     </asp:TemplateField>
              
              
                      <asp:TemplateField HeaderText="Trip" >
                       <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:Label ID="lblTrip" runat="server" text='<%# Bind("TRP_DESCR") %>' ></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                   
                      <asp:TemplateField HeaderText="Remarks" >
                       <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:Label ID="lblRemarks" runat="server" text='<%# Bind("TRP_REMARKS") %>' ></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                                                                
                                         
                      <asp:TemplateField HeaderText="Start Time (HH:MM)" >
                      <ItemStyle HorizontalAlign="Left" Width="30%" VerticalAlign="Middle"/>
                          <HeaderStyle Width="25%" />
                      <ItemTemplate>
                          <div style="width:30%;">
                      <asp:TextBox ID="txtStart" runat="server" CausesValidation="true" style="width:100% !important;" Text='<%# Bind("TRL_STARTTIME") %>'></asp:TextBox>
                       <asp:DropDownList runat="server" ID="ddlStart">
                        <asp:ListItem>AM</asp:ListItem>                        
                        <asp:ListItem>PM</asp:ListItem>                        
                        </asp:DropDownList>
                              </div>
                      </ItemTemplate>
                      </asp:TemplateField>
                       
                   <asp:TemplateField HeaderText="End Time (HH:MM)"  >
                   <ItemStyle HorizontalAlign="Left" Width="30%" VerticalAlign="Middle" />
                   <ItemTemplate >
                       <div style="width:30%;">
                   <asp:TextBox ID="txtEnd" runat="server" CausesValidation="true" style="width:100% !important;" Text='<%# Bind("TRL_ENDTIME") %>'></asp:TextBox>
                    <asp:DropDownList runat="server" ID="ddlEnd" style="width:100% !important;">
                        <asp:ListItem>AM</asp:ListItem>                        
                        <asp:ListItem>PM</asp:ListItem>                        
                        </asp:DropDownList>
                            </div>
                   </ItemTemplate>
                   </asp:TemplateField>
                
                       
                      <asp:TemplateField HeaderText="Start Km." >
                       <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:TextBox ID="txtStartKm" runat="server" text='<%# Bind("TRL_STARTKM") %>' ></asp:TextBox>
                      </ItemTemplate>
                       </asp:TemplateField>
                     
                      <asp:TemplateField HeaderText="End Km." >
                       <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:TextBox ID="txtEndKm" runat="server" text='<%# Bind("TRL_ENDKM") %>' ></asp:TextBox>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="No. Of Passengers" >
                       <ItemStyle HorizontalAlign="Left"  />
                      <ItemTemplate>
                      <asp:TextBox ID="txtPassenger" runat="server" text='<%# Bind("TRL_PASSENGERS") %>' ></asp:TextBox>
                      </ItemTemplate>
                            <HeaderStyle Wrap="True" />
                       </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Max" Visible="False" >
                       <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:Label ID="lblMinKm" runat="server" text='<%# Bind("MINKM") %>' ></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                       
                       <asp:TemplateField HeaderText="Max" Visible="False" >
                       <ItemStyle HorizontalAlign="Left" />
                      <ItemTemplate>
                      <asp:Label ID="lblMaxKm" runat="server" text='<%# Bind("MAXKM") %>' ></asp:Label>
                      </ItemTemplate>
                       </asp:TemplateField>
                       
                       </Columns>  
                     <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                          <RowStyle CssClass="griditem" Wrap="False" />
                          <SelectedRowStyle CssClass="Green" Wrap="False" />
                          <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                          <EmptyDataRowStyle Wrap="False" />
                          <EditRowStyle Wrap="False" />
                     </asp:GridView>
                   </td></tr>
                   </table>
           </td>
           </tr>
            <tr>
            <td align="center" colspan="2">
                 <asp:Button ID="btnSave"  runat="server" CssClass="button" Text="Save" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
              
                   
                    </td>
        </tr>
        
           </table>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtTripDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgBtnTripDate" TargetControlID="txtTripDate">
                </ajaxToolkit:CalendarExtender>
            
                    </td></tr>
          
        </table>
    <asp:HiddenField ID="hfVEH_ID" runat="server" />
    <br />
    <asp:HiddenField ID="hfTRL_ID" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

