<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studNewTransportApprove.aspx.vb" Inherits="Transport_studNewTransportApprove" Title="Untitled Page" Debug="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function PShowSeats() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var sbl_id;

            sbl_id = document.getElementById("<%=hfPSBL_ID.ClientID%>").value
            url = 'tptSeatCapacity_SelectArea_M.aspx?sbl_id=' + sbl_id;
            //    window.showModalDialog(url, "", sFeatures);
            //    return false;
            //}
            var oWnd = radopen(url, "pop_PShowSeats");
        }

        function DShowSeats() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var sbl_id;
            sbl_id = document.getElementById("<%=hfDSBL_ID.ClientID%>").value
            url = 'tptSeatCapacity_SelectArea_M.aspx?sbl_id=' + sbl_id;
            //window.showModalDialog(url, "", sFeatures);
            //return false;
            var oWnd = radopen(url, "pop_DShowSeats");
        }

        function PAddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 400px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            pnt_id = document.getElementById("<%=hfPPNT_ID.ClientID%>").value
            url = 'tptAddPickuptoTrip.aspx?pntid=' + pnt_id;
            //window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_PAddtoTrip");
        }

        function DAddtoTrip() {
            var sFeatures;
            sFeatures = "dialogWidth: 400px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var pnt_id;
            pnt_id = document.getElementById("<%=hfDPNT_ID.ClientID%>").value
            url = 'tptAddPickuptoTrip.aspx?pntid=' + pnt_id;
            //window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_DAddtoTrip");
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_PShowSeats" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_DShowSeats" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_PAddtoTrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_DAddtoTrip" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-bus mr-3"></i>Pickup Points Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">

                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" width="100%">Fields Marked with (<span class="text-danger">*</span>) are mandatory
                        </td>
                    </tr>

                    <tr>
                        <td width="100%">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblStuNo" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"><span class="field-label">Section</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Academic Year Start</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblAcdStartdate" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left"><span class="field-label">Academic Year Term</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblTermdetails" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Transport Status</span></td>

                                    <td align="left">
                                        <asp:Label ID="lblStatus" runat="server" CssClass="field-value"></asp:Label></td>

                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Starting Date</span> <span class="text-danger">*</span>
                                    </td>

                                    <td align="left">
                                        <asp:Label ID="lblDate" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Approval Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtApprove" runat="server" AutoPostBack="True">
                                        </asp:TextBox><asp:ImageButton ID="imgApprove" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:CustomValidator
                                            ID="CustomValidator1" runat="server" ControlToValidate="txtApprove" CssClass="error"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Approval Date entered is not a valid date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtApprove"
                                            Display="Dynamic" ErrorMessage="Enter the Approval Date From in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtApprove"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter the approval date">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>


                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Pickup Point 
                                        <asp:LinkButton ID="lnkSeat"
                                            runat="server" CausesValidation="False" OnClientClick="javascript: PShowSeats(); return false;"
                                            Text="Seat Capacity"></asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">

                                        <table width="100%">
                                            <tr>


                                                <td align="left">
                                                    <asp:Label ID="Label6" runat="server" CssClass="field-label" Text="Location"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblPlocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label7" runat="server" CssClass="field-label" Text="Area"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblPSublocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label8" runat="server" CssClass="field-label" Text="PickupPoint"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblPPickup" runat="server" CssClass="field-value"></asp:Label>
                                                    <asp:LinkButton ID="lnKPickup" runat="server" OnClientClick="javascript: PAddtoTrip(); return false;">Add to Trip</asp:LinkButton></td>

                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr>

                                    <td align="left">
                                        <asp:Label ID="Label61" runat="server" Text="Trip" CssClass="field-label"></asp:Label></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlonward" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:GridView ID="gvOnward" runat="server" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Capacity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Available">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop"></HeaderStyle>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <%-- <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>--%>
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4" class="title-bg">Drop Off Point  
            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" OnClientClick="javascript: DShowSeats(); return false;" Text="Seat Capacity">
            </asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">

                                        <table width="100%">
                                            <tr>


                                                <td align="left">
                                                    <asp:Label ID="Label9" runat="server" CssClass="field-label" Text="Location"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblDlocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label10" runat="server" CssClass="field-label" Text="Area"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblDsublocation" runat="server" CssClass="field-value"></asp:Label></td>
                                                <td align="left">
                                                    <asp:Label ID="Label11" runat="server" CssClass="field-label" Text="PickupPoint"></asp:Label></td>

                                                <td align="left">
                                                    <asp:Label ID="lblDPickup" runat="server" CssClass="field-value"></asp:Label>
                                                    <asp:LinkButton ID="lnkDropOff" runat="server" OnClientClick="javascript: DAddtoTrip(); return false;">Add to Trip</asp:LinkButton></td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>


                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label71" runat="server" CssClass="field-label" Text="Trip" Width="74px"></asp:Label></td>


                                    <td align="left">
                                        <asp:DropDownList ID="ddlReturn" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" colspan="2">
                                        <asp:GridView ID="gvReturn" runat="server" CssClass="table table-bordered table-row" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Capacity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCapacity" runat="server" Text='<%# Bind("VEH_CAPACITY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Available">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAvailable" runat="server" Text='<%# Bind("VEH_AVAILABLE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle></HeaderStyle>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <%-- <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>--%>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <%--Added by vikranth on 30th Jan 2020--%>
                                <tr>

                                    <td align="left">
                                        <asp:Label ID="lblPromotions" runat="server" Text="Promotions" CssClass="field-label"></asp:Label></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPromotions" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" colspan="2">
                                        <table width="100%" runat="server" id="tblPromoDetails" visible="false">
                                            <tr>
                                                <td width="30%"> <asp:Label ID="lblPromoDescr" runat="server" Text="Promotion Description" CssClass="field-label"></asp:Label></td>
                                                <td><asp:Label runat="server" ID="lblPromoDescrVal" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td><asp:Label ID="lblPromoPeriod" runat="server" Text="Promotion Period" CssClass="field-label"></asp:Label></td>
                                                <td><asp:Label runat="server" ID="lblPromoPeriodVal" Text=""></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td><asp:Label ID="lblPromoAmt" runat="server" Text="Promotion Amount" CssClass="field-label"></asp:Label></td>
                                                <td><asp:Label runat="server" ID="lblPromoAmtVal" Text=""></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--End by vikrnath on 30th Jan 2020--%>
                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine">
                                        </asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td align="center">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Accept" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnService" runat="server" CssClass="button" Text="Print Service Form" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfSNR_ID" runat="server" />
                            <asp:HiddenField ID="hfSHF_ID" runat="server" />
                            <asp:HiddenField ID="hfPSBL_ID" runat="server" />
                            <asp:HiddenField ID="hfDSBL_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />

                            <asp:HiddenField ID="hfPPNT_ID" runat="server" />
                            <asp:HiddenField ID="hfDisDate" runat="server" EnableViewState="False" />
                            <asp:HiddenField ID="hfDPNT_ID" runat="server" />
                            <asp:HiddenField ID="hfCLM_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender
                                ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgApprove" TargetControlID="txtApprove">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtApprove" PopupPosition="TopRight" TargetControlID="txtApprove">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


</asp:Content>

