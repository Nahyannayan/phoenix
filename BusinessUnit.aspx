<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BusinessUnit.aspx.vb" Inherits="BusinessUnit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Unit Page</title>
    <link rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon">
    <link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
 
    </script>
<!-- new popup code added here-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js"></script>
    <style>
        .loading { background: url(/images/white_loading.gif) center no-repeat !important;}
    </style>
    
</head>
<body style="margin-top: -1px">
    <form id="form1" defaultfocus="imgBtnBsu" runat="server">
        <asp:ScriptManager runat="server" ID="sm1"></asp:ScriptManager>
        <table style="border-collapse: collapse;" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <img src="images/loginImage/head.jpg" alt="" /></td>
            </tr>
            <tr>
                <td style="height: 23px;" colspan="2"></td>
            </tr>
            <tr>
                <td style="background-color: #06c; color: white; font-weight: bold; font-size: 16px; font-family: Arial,Helvetica, sans-serif; background-image: url(images/loginImage/cellone_top.gif); background-repeat: no-repeat; height: 46px;"
                    valign="middle" align="center">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Select Business Unit &amp; Financial Year</td>
            </tr>

            <tr>
                <td style="height: 204px; background-image: url(images/loginImage/background1.jpg); background-repeat: repeat;" colspan="2" align="center">

                    <asp:Label ID="lblError" runat="server" CssClass="error"> </asp:Label><br />
                    <telerik:RadComboBox ID="ddlBUnit" runat="server" Filter="Contains" Width="400px" ZIndex="2000" ToolTip="Type in unit name or short code"></telerik:RadComboBox>
                    <%-- <asp:DropDownList ID="ddlBUnit" runat="server" DataTextField="BSU_NAME" DataValueField="BSU_ID">
                    </asp:DropDownList>--%>
                    <%-- <asp:DropDownList ID="ddFinancialYear" runat="server">
                    </asp:DropDownList>--%>
                    <telerik:RadComboBox ID="ddFinancialYear" runat="server" Filter="Contains" ZIndex="2000"></telerik:RadComboBox>
                    <asp:ImageButton ID="imgBtnBsu" runat="server" BorderColor="Purple" BorderStyle="Solid"
                        BorderWidth="2px" ImageAlign="Top" ImageUrl="~/Images/loginImage/select.jpg"
                        Style="background-repeat: no-repeat; text-align: right" /></td>

            </tr>


            <tr>
                <td style="background-image: url(images/loginImage/background1.jpg); background-repeat: repeat; height: 140px; text-align: center;" colspan="2">
                    <img src="images/loginImage/allschools4.jpg" width="757" align="middle" alt="" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px; background-image: url(images/loginImage/background1.jpg); background-repeat: repeat;" colspan="2" align="center">
                    <span style="letter-spacing: 3px; font-family: Arial Rounded MT Bold; font-size: 14px; color: #0066cc;">GEMS - 
            IT</span></td>
            </tr>
            <tr>
                <td colspan="2" style="background-image: url(images/loginImage/footerSep.gif); height: 10px"></td>
            </tr>
        </table>
        <asp:HiddenField ID="Hiddenrecoveryflag" Value="0" runat="server" />
    </form>

</body>
</html>
<script type="text/javascript">

    if (document.getElementById("Hiddenrecoveryflag").value == '1') {

        popup1();

    }


    //function openPopUp() {


    //    var sFeatures;
    //    sFeatures = "dialogWidth: 500px; ";
    //    sFeatures += "dialogHeight: 400px; ";
    //    sFeatures += "help: no; ";
    //    sFeatures += "resizable: no; ";
    //    sFeatures += "scroll: no; ";
    //    sFeatures += "status: no; ";
    //    sFeatures += "unadorned: no; ";
    //    window.showModalDialog("createpasswordqueansFirst.aspx", "", sFeatures)
    //}

    function popup1() {
        //$('a.ajax').click(function () {
       
        var url = "createpasswordqueansFirst.aspx";
            // show a spinner or something via css
            var dialog = $('<div style="display:none" class="loading"></div>').appendTo('body');
            // open the dialog
            dialog.dialog({
                maxWidth: 600,
                maxHeight: 500,
                width: 600,
                height: 500,
                // add a close listener to prevent adding multiple divs to the document
                close: function (event, ui) {
                    // remove div with all data and events
                    dialog.remove();
                },
                modal: true
            });
            // load remote content
            dialog.load(
                url,
                {}, // omit this param object to issue a GET request instead a POST request, otherwise you may provide post parameters within the object
                function (responseText, textStatus, XMLHttpRequest) {
                    // remove the loading class
                    dialog.removeClass('loading');
                }
            );
            //prevent the browser to follow the link
            return false;
            //});
    };

</script>
