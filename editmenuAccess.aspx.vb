Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class editmenuAccess
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Call callMenuload()
        End If
    End Sub


    Private Sub callMenuload()
        Try
            Dim ds As New DataSet
            Dim rol_id As String = Session("sroleid")

            Dim mnu_module As String = Session("sModule")
            Dim userSuper As Boolean = Session("sBusper")
            Dim bsu_id As String = Session("sBsuid")
            Dim usr_id As String = Session("sUsr_id")
            Dim sqlString As String = String.Empty

            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Using conn As SqlConnection = New SqlConnection(connStr)
                If userSuper = False Then
                    sqlString = "SELECT distinct MENUS_M.MNU_CODE as MNU_CODE,MENUS_M.MNU_TEXT as MNU_TEXT, MENUS_M.MNU_NAME as MNU_NAME," & _
                                 " MENUS_M.MNU_PARENTID as MNU_PARENTID, MENUS_M.MNU_MODULE +'_'+ case when str(MENURIGHTS_S.MNR_RIGHT) IS NULL " & _
                                 " then '0' else ltrim(str(MENURIGHTS_S.MNR_RIGHT)) end mnu_module  FROM   MENUS_M  JOIN MENURIGHTS_S ON MENUS_M.MNU_CODE = MENURIGHTS_S.MNR_MNU_ID " & _
                                 " and (MENURIGHTS_S.MNR_BSU_ID = '" & bsu_id & "' and MENURIGHTS_S.MNU_module='" & mnu_module & "') AND (MENURIGHTS_S.MNR_ROL_ID ='" & rol_id & "' and " & _
                                 " MENUS_M.MNU_CODE = MENURIGHTS_S.MNR_MNU_ID)or MENURIGHTS_S.MNR_MNU_ID is  null "
                Else
                    sqlString = " select MNU_CODE,MNU_TEXT,MNU_NAME,MNU_PARENTID,(MNU_MODULE+'_6') as mnu_module from MENUS_M " & _
                                " where mnu_module in (select distinct mnu_module from MENUS_M where mnu_module ='" & mnu_module & "') "
                End If

                Dim da As SqlDataAdapter = New SqlDataAdapter(sqlString, conn)
                da.Fill(ds)
                da.Dispose()
            End Using
            ds.DataSetName = "MENURIGHTS"
            ds.Tables(0).TableName = "MENURIGHT"
            Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("MENURIGHT").Columns("MNU_CODE"), ds.Tables("MENURIGHT").Columns("MNU_PARENTID"), True)
            relation.Nested = True
            ds.Relations.Add(relation)
            XmlDataSourceMenu.Data = ds.GetXml()
            tvmenu.DataBind()
            Session("toolStatus") = 1
            Session("dt") = CreateDataTable()
            Call callgridBind(mnu_module, rol_id, bsu_id, usr_id)
        Catch ex As Exception
            lblErrorMessage.Text = "Process could be completed"
        End Try
    End Sub


    Public Function gridbind() As String
        Try
            InfoGridView.DataSource = Session("dt")
            InfoGridView.DataBind()
            Return "1"
        Catch ex As Exception
            Return "1"
        End Try
    End Function


    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        If Not tvmenu Is Nothing Then
            If Session("toolStatus") = 0 Then
                ' setstyle(tvmenu.Nodes)
            ElseIf Session("toolStatus") = 1 Then
                'setstyle2(tvmenu.Nodes)
            End If
        End If
    End Sub


    Private Function CallTransactions() As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim StatusFlag As Integer
                Dim iIndex As Integer
                Dim arrModule As New ArrayList
                Dim menucode As String
                Dim Usr_ID As String
                Dim modules As String
                For iIndex = 0 To Session("dt").Rows.Count - 1
                    Dim RoleId As String = Session("dt").Rows(iIndex)("RoleId")
                    Dim BusUnit As String = Session("dt").Rows(iIndex)("BusUnit")
                    menucode = Session("dt").Rows(iIndex)("menucode")
                    Usr_ID = Session("dt").Rows(iIndex)("MNA_Usr_ID")
                    modules = Session("dt").Rows(iIndex)("modules")

                    StatusFlag = AccessRoleUser.InsertQuickMenu(RoleId, BusUnit, menucode, Usr_ID, modules, transaction)
                    ' StatusFlag = AccessRoleUser.InsertMenuRights(RoleId, BusUnit, menucode, Usr_ID, modules, transaction)

                    If StatusFlag <> 0 Then
                        Throw New ArgumentException("Error while inserting Menu")
                    End If
                Next
                transaction.Commit()
                Return "0"
                'Adding transaction info
            Catch myex As ArgumentException
                transaction.Rollback()
                Return "1"
            Catch ex As Exception
                transaction.Rollback()
                Return "1"
            End Try
        End Using
    End Function


    Private Function DupicateRecord(ByVal FormId As String) As Integer
        Try
            Dim iIndex As Integer = 0
            If (InfoGridView.Rows.Count) < 10 Then
                For iIndex = 0 To InfoGridView.Rows.Count - 1
                    If FormId = InfoGridView.Rows(iIndex).Cells(2).Text Then
                        InfoGridView.DeleteRow(iIndex)
                        InfoGridView.Rows(iIndex).Cells(5).Text = "2" 'ddlOprRight.SelectedValue()
                        InfoGridView.Rows(iIndex).Cells(6).Text = "1"  'ddlOprRight.SelectedItem.Text
                        InfoGridView.DataBind()
                        ' Return 1
                        Exit For
                    End If
                Next
                Return 0
            Else
                Return 1

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "EditMenuAccess")
        End Try

    End Function


    Protected Sub InfoGridView_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles InfoGridView.RowDeleting
        Try
            Dim row As GridViewRow = InfoGridView.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("idLabel"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("dt").Rows.Count - 1
                If iIndex = Session("dt").Rows(iRemove)(0) Then
                    Session("dt").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            gridbind()
        Catch ex As Exception
            lblErrorMessage.Text = "Record cannot be Deleted"
        End Try
    End Sub


    Private Function addMenuGrid() As Integer
        Try
            Session("dt").Rows.Clear()
            gridbind()
            Dim menu_text As String = String.Empty
            Dim tempAdd As Integer = 0
            Dim checkedNodes As TreeNodeCollection = tvmenu.CheckedNodes
            If tvmenu.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvmenu.CheckedNodes
                    Dim rDt As DataRow
                    rDt = Session("dt").NewRow
                    rDt("Id") = Session("idTrA")
                    rDt("RoleId") = Session("sroleid")
                    rDt("BusUnit") = Session("sBsuid")
                    rDt("menucode") = node.Value
                    Using readerUserDetail As SqlDataReader = AccessRoleUser.GetMenuText(node.Value)
                        If readerUserDetail.HasRows = True Then
                            While readerUserDetail.Read()
                                rDt("menuText") = Convert.ToString(readerUserDetail("mnu_text"))
                                menu_text = Convert.ToString(readerUserDetail("mnu_text"))
                                rDt("modules") = Convert.ToString(readerUserDetail("mnu_module"))
                            End While
                        End If
                    End Using
                    rDt("MNA_Usr_ID") = Session("sUsr_id")
                    rDt("Description") = "dsad"
                    If InfoGridView.Rows.Count = 0 Then
                        Session("dt").Rows.Add(rDt)
                        Session("idTrA") = Session("idTrA") + 1
                        gridbind()
                    Else
                        Dim recordsStatus As Integer = DupicateRecord(node.Value)
                        If recordsStatus = 0 Then
                            Session("dt").Rows.Add(rDt)
                            Session("idTrA") = Session("idTrA") + 1
                            gridbind()
                        Else
                            tempAdd = 10
                            'lblErrorMessage.Text = "Can not insert more than ten quick access menus"
                            Exit For
                        End If
                    End If
                Next
            Else
                tempAdd = 5
                'lblErrorMessage.Text = "Select the Menu for the quick access"
            End If
            Return tempAdd
        Catch ex As Exception
            Return -1
            ' lblErrorMessage.Text = "Record cannot be inserted"
        End Try
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Check if the InfoGridView is empty or not to do the transaction
            Dim addStatus As Integer = addMenuGrid()
            If addStatus = 5 Then
                lblErrorMessage.Text = "Select the Menu for the quick access"
            ElseIf addStatus = -1 Then
                lblErrorMessage.Text = "Record cannot be inserted"
            ElseIf addStatus = 0 Or addStatus = 10 Then
                If InfoGridView.Rows.Count > 0 Then
                    Dim str_err As String
                    Dim MNA_USR_ID As String = Session("sUsr_id")
                    Dim MNA_MODULE As String = Session("sModule")
                    'new added code
                    Dim MNA_BSU_ID As String = Session("sBsuid")
                    'new added code
                    Dim dupflag As Integer = AccessRoleUser.checkduplicateMenu_Access(MNA_USR_ID, MNA_MODULE, MNA_BSU_ID)
                    'code needs to be modified
                    If dupflag = 0 Then
                        Dim sqlstring As String
                        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                            sqlstring = "delete from MENUACCESS_S where  MNA_USR_ID='" & MNA_USR_ID & "' and MNA_MODULE='" & MNA_MODULE & "' and MNA_BSU_ID='" & MNA_BSU_ID & "'"
                            Dim command As SqlCommand = New SqlCommand(sqlstring, connection)
                            command.CommandType = Data.CommandType.Text
                            command.ExecuteNonQuery()
                        End Using
                    End If
                    str_err = CallTransactions()
                    If str_err = "0" Then
                        'Session("dt").Rows.Clear()
                        'Session("dt").clear()
                        Session("dt") = Nothing
                        gridbind()
                        btnSave.Visible = False
                        If addStatus = 10 Then
                            lblErrorMessage.Text = "Process completed successfully with first ten quick access menu only"
                        Else
                            lblErrorMessage.Text = "Process completed successfully"
                        End If
                    Else
                        lblErrorMessage.Text = "Process can not be completed"
                    End If
                Else
                    lblErrorMessage.Text = "Currently no menu is added"
                End If
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Process could not be completed"
        End Try
    End Sub


    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.Double"))
            Dim BusUnit As New DataColumn("BusUnit", System.Type.GetType("System.String"))
            Dim RoleId As New DataColumn("RoleId", System.Type.GetType("System.Double"))
            Dim menucode As New DataColumn("menucode", System.Type.GetType("System.String"))
            Dim menuText As New DataColumn("menuText", System.Type.GetType("System.String"))
            Dim MNA_Usr_ID As New DataColumn("MNA_Usr_ID", System.Type.GetType("System.String"))
            Dim Decription As New DataColumn("Description", System.Type.GetType("System.String"))
            Dim OperationId As New DataColumn("OperationId", System.Type.GetType("System.Double"))
            Dim modules As New DataColumn("Modules", System.Type.GetType("System.String"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(BusUnit)
            dtDt.Columns.Add(RoleId)
            dtDt.Columns.Add(menucode)
            dtDt.Columns.Add(menuText)
            dtDt.Columns.Add(MNA_Usr_ID)
            dtDt.Columns.Add(OperationId)
            dtDt.Columns.Add(Decription)
            dtDt.Columns.Add(modules)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "EDitmenuAccess")
            Return dtDt
        End Try
    End Function


    Public Sub callgridBind(ByVal mnu_module As String, ByVal rol_id As String, ByVal bsu_id As String, ByVal usr_id As String)
        Try
            Session("dt").Rows.Clear()
            Session("idTrA") = 0
            Using Userreader As SqlDataReader = AccessRoleUser.GetQuickAccess_menu(mnu_module, rol_id, bsu_id, usr_id)
                While Userreader.Read
                    Dim rDt As DataRow
                    rDt = Session("dt").NewRow
                    rDt("Id") = Session("idTrA")
                    rDt("RoleId") = Convert.ToString(Userreader("ROL_ID"))
                    rDt("BusUnit") = Convert.ToString(Userreader("BSU_ID"))
                    rDt("menucode") = Convert.ToString(Userreader("code"))
                    ' rDt("OperationId") = Convert.ToString(Userreader("MNR_RIGHT"))
                    rDt("menuText") = Convert.ToString(Userreader("MNU_TEXT"))
                    rDt("MNA_Usr_ID") = Convert.ToString(Userreader("usr_id"))
                    rDt("Description") = Convert.ToString(Userreader("MNU_Name"))
                    rDt("modules") = Convert.ToString(Userreader("M_Module"))
                    Session("dt").Rows.Add(rDt)
                    Session("idTrA") = Session("idTrA") + 1
                    Call checkme(tvmenu.Nodes, rDt("menucode"))
                    'tvmenu.SelectedNode.
                    gridbind()
                End While
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "EditmenuAccess")
        End Try
    End Sub


    Public Sub checkme(ByVal nodeList As TreeNodeCollection, ByVal menuCode As String)
        If Not nodeList Is Nothing Then
            For Each nodec As TreeNode In nodeList
                '  If Not nodec.Parent Is Nothing Then
                Dim strNodeValue As String = nodec.Value
                '   For Each node As TreeNode In nodec.Parent.ChildNodes
                If strNodeValue = menuCode Then
                    'tvmenu.SelectedNode.Checked
                    ' Label2.Text += nodec.Value & " "
                    nodec.Checked = True
                End If
                ' Next
                '  End If
                checkme(nodec.ChildNodes, menuCode)
            Next
        End If
    End Sub
    
    
End Class
