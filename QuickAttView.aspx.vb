﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class QuickAttView
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim Latest_refid As Integer = 0
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    'End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim smScriptManager As New ScriptManager
            smScriptManager = FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnDetailView)

            Dim ACD_ID As String = Session("Current_ACD_ID")
            Dim BSU_ID As String = Session("sBsuid")
            Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
            Dim var1 As String = String.Empty
            Try
                Dim ds As New DataSet
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim objConn As New SqlConnection(str_conn)
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@TODT", TODT)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[ATT].[ATT_PERC_QUICKVIEW]", pParms)
                If ds.Tables(0).Rows.Count > 0 Then
                    gvAttQuick.DataSource = ds.Tables(0)
                    gvAttQuick.DataBind()
                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    'start the count from 1 no matter gridcolumn is visible or not
                    ds.Tables(0).Rows(0)(6) = True
                    gvAttQuick.DataSource = ds.Tables(0)
                    Try
                        gvAttQuick.DataBind()
                    Catch ex As Exception
                    End Try
                    Dim columnCount As Integer = gvAttQuick.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                    gvAttQuick.Rows(0).Cells.Clear()
                    gvAttQuick.Rows(0).Cells.Add(New TableCell)
                    gvAttQuick.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvAttQuick.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvAttQuick.Rows(0).Cells(0).Text = "No Record Is Available"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Protected Sub gvAttQuick_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chk As CheckBox = DirectCast(e.Row.FindControl("chkSelect"), CheckBox)
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblGroup"), Label)
            If lbl.Text.Trim = "" Then

                chk.Visible = True
                lbl.Visible = False
            Else
                e.Row.Cells(0).ColumnSpan = 6
                e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left
                e.Row.Cells(0).BackColor = Drawing.Color.FromName("#e6e6e6")
                e.Row.Cells(6).BackColor = Drawing.Color.FromName("#e6e6e6")
                e.Row.Cells(1).Visible = False
                e.Row.Cells(2).Visible = False
                e.Row.Cells(3).Visible = False
                e.Row.Cells(4).Visible = False
                e.Row.Cells(5).Visible = False
                e.Row.Cells(6).Visible = False
                chk.Visible = False
                lbl.Visible = True

                If lbl.Text.Trim = "Overall School % for the classes marked" Then
                    e.Row.Cells(0).ForeColor = Drawing.Color.White
                    'e.Row.Cells(0).Font.Size = 8
                    e.Row.Cells(0).Font.Bold = True
                    e.Row.Cells(0).BackColor = Drawing.Color.FromName("#b2b0b0")
                    e.Row.Cells(7).ForeColor = Drawing.Color.White
                    'e.Row.Cells(7).Font.Size = 8
                    e.Row.Cells(7).Font.Bold = True
                    e.Row.Cells(7).BackColor = Drawing.Color.FromName("#b2b0b0")
                ElseIf lbl.Text.Contains("Single Group") = True Then
                    e.Row.Visible = False
                Else
                    'e.Row.Cells(0).Font.Size = 8
                    e.Row.Cells(0).Font.Bold = True
                    'e.Row.Cells(7).Font.Size = 8
                    e.Row.Cells(7).Font.Bold = True
                    e.Row.Cells(0).BackColor = Drawing.Color.FromName("#e6e6e6")
                    e.Row.Cells(7).BackColor = Drawing.Color.FromName("#e6e6e6")
                End If
            End If
        End If
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ACD_ID As String = Session("Current_ACD_ID")
        Dim BSU_ID As String = Session("sBsuid")
        Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@TODT", TODT)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("/Students/Reports/RPT/rptAtt_School_Grade_Perc.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
    End Sub
    Protected Sub btnDetailView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim GRD_ID As String = String.Empty
        Dim GRD_IDS As String = String.Empty
        Dim chk As CheckBox
        For Each rowItem As GridViewRow In gvAttQuick.Rows
            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                GRD_ID = DirectCast(rowItem.FindControl("lblGRD_ID"), Label).Text
                GRD_IDS = GRD_IDS + GRD_ID + "|"
            End If
        Next
        If GRD_IDS = "" Then
            lblAttGrd.Visible = True
            ' MPEATT.Show()
        Else
            lblAttGrd.Visible = False
            Call GetATT_DATA()

            Dim param As New Hashtable
            Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now) '"25/NOV/2008" '

            Dim ardate As String() = TODT.Split("/")

            param.Add("@IMG_BSU_ID", Session("sBsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@ACD_ID", Session("Current_ACD_ID"))
            param.Add("@BSU_ID", Session("sBsuid"))
            param.Add("@GRD_IDs", GRD_IDS)
            param.Add("@TODT", TODT)
            param.Add("UserName", Session("sUsr_name"))
            param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
            param.Add("STARTDATE", Format(Date.Parse(hfstartdt.Value), "dd/MMM/yyyy"))
            param.Add("ENDDATE", Format(Date.Parse(hfenddt.Value), "dd/MMM/yyyy"))
            param.Add("WEEKEND1", hfweek1.Value)
            param.Add("WEEKEND2", hfweek2.Value)
            param.Add("TDAY", ardate(0))
            param.Add("FORMONTH", ardate(1))
            param.Add("Acad_Year", hfYear.Value)

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param

                .reportPath = Server.MapPath("/Students/Reports/RPT/rptAtt_School_Perc.rpt")
            End With
            Session("rptClass") = rptClass
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
    Sub ReportLoadSelection()
        Session("ReportSel") = ""
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub GetATT_DATA()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@acd_id", Session("Current_ACD_ID"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetATT_DATA", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        hfweek1.Value = Convert.ToString(readerStudent_Detail("weekend1"))
                        hfweek2.Value = Convert.ToString(readerStudent_Detail("weekend2"))
                        hfstartdt.Value = Convert.ToString(readerStudent_Detail("startdt"))
                        hfenddt.Value = Convert.ToString(readerStudent_Detail("enddt"))
                        hfYear.Value = Convert.ToString(readerStudent_Detail("yrs_descr"))
                    End While
                Else
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvAttQuick_RowEditing(sender As Object, e As GridViewEditEventArgs)

    End Sub
End Class
