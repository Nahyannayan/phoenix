﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports System.IO
Imports UtilityObj
Imports ICSharpCode.SharpZipLib.Checksums
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.GZip
Imports Telerik.Web.UI
Imports System.Drawing
Imports Telerik.Web.UI.GridExcelBuilder

Partial Class DocTracker_UploadDocument
    Inherits System.Web.UI.Page
    Public selectedBSu As String = ""
    Public selectedSubDocIds As String = ""
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
    Dim ExpSubDoc As Boolean = False
    Dim encr As New SHA256EncrDecr

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ScriptManager1.RegisterPostBackControl(btnUpload)
        ScriptManager1.RegisterPostBackControl(btnFullVersion)
        Dim Strsql As String = ""

        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If


        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            h_status.Value = Request.QueryString("Status")
            If Request.QueryString("viewid") = 0 Then
                ViewState("EntryId") = "0"
                ViewState("datamode") = "add"
                ViewState("MainMnu_code") = Request.QueryString("Menucode")
            Else
                ViewState("EntryId") = Request.QueryString("viewid")
                ViewState("MainMnu_code") = Request.QueryString("Menucode")
                ViewState("datamode") = "view"
            End If

            'fillDropdown(ddlDocumentType, "SELECT Dot_Id,Dot_descr FROM DOCTYPE where ('||'+Dot_bsu_apply+'||' like '%||" & Session("sBsuid") & "||%' or '||'+Dot_bsu_share+'||' like '%||" & Session("sBsuid") & "||%') and  '||'+ Dot_Rol_share LIKE '%||'+(select top 1 cast(usr_rol_id as varchar(5)) from oasis..users_m  where usr_name='" & Session("sUsr_name") & "')+'||%'  order BY Dot_descr", "Dot_Descr", "Dot_Id", True)
            'fillDropdown(ddlDocumentType, "SELECT distinct DOT_ID,DOT_DESCR FROM dbo.DocType INNER JOIN OASIS_CLM..GET_EMLOYEES_FROM_OWNER ON '||' +  Dot_Rol_Share + '||' LIKE '%' + CAST(DOM_OWNER_ID AS VARCHAR)   + '%' INNER JOIN OASIS..USERS_M ON USR_EMP_iD=EMP_ID WHERE USR_NAME ='" & Session("sUsr_name") & "' order BY Dot_descr", "Dot_Descr", "Dot_Id", True)
            fillDropdown(ddlDocumentType, "SELECT 0 ID,'------Select One-----' DESCR,0 DOT_TRACKING_ID union all  SELECT * FROM [dbo].[GetAccessibleListForUser]( 'DOC','" & Session("sUsr_name") & "','" & Session("sBsuID") & "') order BY descr", "Descr", "Id", True)

            'Strsql = "SELECT * FROM [dbo].[GetAccessibleListForUser]( 'BSU','" & Session("sUsr_name") & "','" & Session("sBsuID") & "') ORDER BY DESCR"

            BindLandLordName()
            BindDocUploadtype()
            BindBSU()
            BindDepartment()
            BindSubGroup(0)
            BindDocIssueUserList()
            If ViewState("datamode") = "view" Then
                setModifyvalues(ViewState("EntryId"))
                SetDataMode("view")
                BindGrid(ViewState("EntryId"))
            Else
                txtIssuer.Text = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "SELECT top 1 iss_descr FROM DOCTYPE inner join docIssuer on iss_id=dot_iss_id order BY Dot_descr")
                'TransferFooter = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select Doi_Id, Doi_No, replace(convert(varchar(30), Doi_IssDate, 106),' ','/') Doi_issDate, replace(convert(varchar(30), Doi_ExpDate, 106),' ','/') Doi_expDate, Doi_Doc_id, Doi_Content_Type, replace(convert(varchar(30), Doi_SysDate, 106),' ','/') Doi_SysDate, Doi_File_name, '' Doi_File_nametemp from docimage where 1=0").Tables(0)
                TransferFooter = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select DOI_ID,DOI_DOC_ID,DOI_TYPE,DOI_FILE_name,doi_content_type,replace(convert(varchar(30), getdate(), 106),' ','/')doi_sysdate,DOI_LIBRARY,DOI_FOLDER,DOI_UPLOAD_USER, Doi_Doc_id, Doi_Content_Type, replace(convert(varchar(30), Doi_SysDate, 106),' ','/') Doi_SysDate, Doi_File_name, '' Doi_File_nametemp,''DOI_Sourcepath,''DOI_SOURCEFILE,''DOI_PATH from docimage where 1=0").Tables(0)
                TransferFooterFullversion = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select DOI_ID,DOI_DOC_ID,DOI_TYPE,DOI_FILE_name,doi_content_type,replace(convert(varchar(30), getdate(), 106),' ','/')doi_sysdate,DOI_LIBRARY,DOI_FOLDER,DOI_UPLOAD_USER, Doi_Doc_id, Doi_Content_Type, replace(convert(varchar(30), Doi_SysDate, 106),' ','/') Doi_SysDate, Doi_File_name, '' Doi_File_nametemp,''DOI_Sourcepath,''DOI_SOURCEFILE,''DOI_PATH from docimage where 1=0").Tables(0)
                setModifyvalues(0)
                SetDataMode("add")
            End If

            hdnIssuerId.Value = 1
            UploadDocPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
            UploadDocFullversion.Attributes.Add("onblur", "javascript:UploadFullVersion();")

            Dim strFilepath As String = WebConfigurationManager.AppSettings.Item("TempFileFolder")
            Dim FilesToDelete As New System.IO.DirectoryInfo(strFilepath)
            Try
                Dim Files As System.IO.FileInfo() = FilesToDelete.GetFiles(Session("sUsr_name") & "*")
                Dim FileToDelete As FileInfo
                For Each FileToDelete In Files
                    If Not FileToDelete.Name.Contains(Session.SessionID) Then
                        Try
                            FileToDelete.Delete()
                        Catch ex As Exception

                        End Try
                    End If
                Next
            Catch ex As Exception
            End Try
        End If
    End Sub


    Private Sub DisableButtons()

        Dim BlockAccess As Boolean = False
        Dim Editaccess As Boolean = False
        Dim AccessParams(4) As SqlParameter
        AccessParams(1) = Mainclass.CreateSqlParameter("@BSUID", ddlBSU.SelectedValue, SqlDbType.VarChar)
        AccessParams(2) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        AccessParams(3) = Mainclass.CreateSqlParameter("@DOC_ID", h_EntryId.Value, SqlDbType.Int)
        AccessParams(4) = Mainclass.CreateSqlParameter("@DOC_TYPE", ddlDocumentType.SelectedValue, SqlDbType.Int)
        Dim Sqlstr As String = "SP_GETDOC_ACCESS_DETAILS"
        Dim rdrDocDetails As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, Sqlstr, AccessParams)
        If rdrDocDetails.HasRows Then
            rdrDocDetails.Read()
            btnSave.Visible = False
            btnEdit.Visible = False
            If rdrDocDetails("Editaccess") = True Then
                If ViewState("datamode") = "add" Then
                    btnSave.Visible = rdrDocDetails("Editaccess")
                Else
                    btnEdit.Visible = rdrDocDetails("Editaccess")
                End If
                ' btnEdit.Visible = True
            End If
            If ViewState("datamode") = "add" Then
                If rdrDocDetails("FullVersionAccess") = True Then
                    TrFullVersion.Visible = True
                    TrFullVersion1.Visible = True
                    TrFullVersion2.Visible = True
                Else
                    TrFullVersion.Visible = False
                    TrFullVersion1.Visible = False
                    TrFullVersion2.Visible = False
                End If
            End If


            hfbtnEdit.Value = (rdrDocDetails("Editaccess"))
            hfbtnSave.Value = (rdrDocDetails("Editaccess"))
            hfbtnRenew.Value = False
            hfbtnRetire.Value = False
            hfbtnForward.Value = False
            hfbtnApprove.Value = False
            hfbtnBlock.Value = False
        End If
        rdrDocDetails.Close()
        rdrDocDetails = Nothing

        '   btnForward.Text = h_ForwardText.Value
        If h_DOC_Status.Value = "A" Then
            ShowUploadFullversionMode()
        End If
    End Sub
    Private Sub ShowUploadFullversionMode()
        btnEdit.Visible = False
        ddlDocumentType.Enabled = False
        ddlBSU.Enabled = False
        Dim EnableFullVersion As Boolean = False
        EnableFullVersion = Mainclass.getDataValue("SELECT isnull(FullVersionAccess,0) FROM [dbo].[Check_UserAccess_For_Documents]( '" & ddlBSU.SelectedValue & "','" & Session("sUsr_name") & "'," & h_EntryId.Value & "," & ddlDocumentType.SelectedValue & ")", "OASIS_CLMConnectionString")
        If EnableFullVersion = 1 Then
            fillDropdown(ddlDocType, "Select DESCR,ID,DEFAULT_DOT from (Select 'Others' DESCR,0 ID  ,0 DEFAULT_DOT union all  select  Dot_Descr,DOT_ID,1 from doctype where dot_id='" & ddlDocumentType.SelectedValue & "'  union all select 'Full Version' DESCR,999 ID  union all select Dot_Descr,DOT_ID,0 from doctype_sub inner join doctype on dot_id=DTS_SUB_DOT_ID where DTS_DOT_ID='" & ddlDocumentType.SelectedValue & "') A ORDER BY DEFAULT_DOT DESC,CASE WHEN ID=0 THEN '999999' ELSE ID END ", "Descr", "Id", True)
        Else
            fillDropdown(ddlDocType, "Select DESCR,ID,DEFAULT_DOT from (Select 'Others' DESCR,0 ID  ,0 DEFAULT_DOT union all  select  Dot_Descr,DOT_ID,1 from doctype where dot_id='" & ddlDocumentType.SelectedValue & "'  union all  select Dot_Descr,DOT_ID,0 from doctype_sub inner join doctype on dot_id=DTS_SUB_DOT_ID where DTS_DOT_ID='" & ddlDocumentType.SelectedValue & "') A ORDER BY DEFAULT_DOT DESC,CASE WHEN ID=0 THEN '999999' ELSE ID END ", "Descr", "Id", True)
        End If
    End Sub
    Private Sub BindDocUploadtype()
        Dim EnableFullVersion As Boolean = False
        EnableFullVersion = Mainclass.getDataValue("SELECT isnull(FullVersionAccess,0) FROM [dbo].[Check_UserAccess_For_Documents]( '" & h_BSUID.Value & "','" & Session("sUsr_name") & "'," & h_EntryId.Value & "," & ddlDocumentType.SelectedValue & ")", "OASIS_CLMConnectionString")
        If EnableFullVersion = 1 Then
            'fillDropdown(ddlDocType, "Select 0  ID, 'Support Document' as DESCr union all select 1,'Orginal Document' union all select 2,'Full Version Document'  order BY DESCr", "Descr", "Id", True)
            fillDropdown(ddlDocType, "Select DESCR,ID,DEFAULT_DOT from (Select 'Others' DESCR,0 ID  ,0 DEFAULT_DOT union all  select  Dot_Descr,DOT_ID,1 from doctype where dot_id='" & ddlDocumentType.SelectedValue & "'  union all select 'Full Version' DESCR,999 ID,0  union all select Dot_Descr,DOT_ID,0 from doctype_sub inner join doctype on dot_id=DTS_SUB_DOT_ID where DTS_DOT_ID='" & ddlDocumentType.SelectedValue & "') A ORDER BY DEFAULT_DOT DESC,CASE WHEN ID=0 THEN '999999' ELSE ID END ", "Descr", "Id", True)
        Else
            'fillDropdown(ddlDocType, "Select 0  ID, 'Support Document' as DESCr union all select 1,'Orginal Document'  order BY DESCr", "Descr", "Id", True)
            fillDropdown(ddlDocType, "Select DESCR,ID,DEFAULT_DOT from (Select 'Others' DESCR,0 ID  ,0 DEFAULT_DOT union all  select  Dot_Descr,DOT_ID,1 from doctype where dot_id='" & ddlDocumentType.SelectedValue & "'  union all  select Dot_Descr,DOT_ID,0 from doctype_sub inner join doctype on dot_id=DTS_SUB_DOT_ID where DTS_DOT_ID='" & ddlDocumentType.SelectedValue & "') A ORDER BY DEFAULT_DOT DESC,CASE WHEN ID=0 THEN '999999' ELSE ID END ", "Descr", "Id", True)
        End If
        TransferFooter = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select DOI_ID,DOI_DOC_ID,DOT_DESCR DOI_TYPE,DOI_FILE_name,doi_content_type,replace(convert(varchar(30), doi_sysdate, 106),' ','/')doi_sysdate,DOI_LIBRARY,DOI_FOLDER,DOI_UPLOAD_USER,DOI_PATH,'' Doi_File_nametemp,'' doi_Sourcepath,'' doi_SourceFile,'' DOI_PATH  from docimage left outer join DocUpload with(nolock) on doc_id=doi_doc_id	left outer join Doctype with(nolock) on dot_id=doc_dot_id where Doi_Doc_id=" & h_EntryId.Value & " order by  DOI_ID DESC").Tables(0)
        grdImage.DataSource = TransferFooter
        grdImage.DataBind()
    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            ddlBSU.Visible = False
            txtIssuedTo.Visible = True
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            ClearDetails()
            TransferFooter.Rows.Clear()
            grdImage.DataSource = TransferFooter
            grdImage.DataBind()
            txtIssuedTo.Visible = False
            ddlBSU.Visible = True
            imgBSU.Visible = False
            grdTracked.Enabled = True
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            ddlBSU.Visible = False
            txtIssuedTo.Visible = True
            grdTracked.Enabled = True
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable And Not (ViewState("MainMnu_code") = "U000086" Or ViewState("MainMnu_code") = "U000086")
        ddlDocumentType.Enabled = Not mDisable
        txtDocSpeName.Enabled = Not mDisable
        ddlLandlord.Enabled = Not mDisable
        txtRemarks.Enabled = Not mDisable
        ddlDepartment.Enabled = Not mDisable
        ddlSubGroup.Enabled = Not mDisable
        ddlDocIssUser.Enabled = Not mDisable
        UploadDocPhoto.Enabled = Not mDisable
        btnUpload.Visible = Not mDisable
        'UploadFullversionpnl.Enabled = Not mDisable
        'btnFullVersion.Visible = Not mDisable
        SetItemEditMode(Not mDisable)
        btnEdit.Visible = (mDisable And CBool(hfbtnEdit.Value))
        btnRenew.Visible = (mDisable And CBool(hfbtnRenew.Value))
        btnRetire.Visible = (mDisable And CBool(hfbtnRetire.Value))
        btnBlock.Visible = (mDisable And CBool(hfbtnBlock.Value))
        UploadImagepnl.Enabled = Not mDisable
        ddlDocumentType.Enabled = Not mDisable
        ddlDocType.Enabled = Not mDisable
        ddlTemplate.Enabled = Not mDisable
        ddlPaymentTerms.Enabled = Not mDisable
        txtContractValue.Enabled = Not mDisable

        grdImage.Columns(4).Visible = Not (ViewState("datamode") = "view")
        GRDFullVersion.Columns(4).Visible = Not (ViewState("datamode") = "view")
        'DisableButtons()
        'SetButtonVisibility()
    End Sub

    Private Sub SetItemEditMode(ByVal IsEditMode As Boolean)
        Dim EditAllowed As Boolean
        EditAllowed = ViewState("datamode") <> "view" And IsEditMode And ViewState("MainMnu_code") <> "FD00072"
        txtDocNo.Enabled = ViewState("datamode") <> "view" And IsEditMode
        lnkFromDate.Enabled = txtDocNo.Enabled
        lnkToDate.Enabled = txtDocNo.Enabled
        btnDocumentLink.Visible = ItemEditMode
        btnUpload.Enabled = ViewState("datamode") <> "view" And IsEditMode
    End Sub
    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            h_EntryId.Value = p_Modifyid
            h_UserId.Value = Session("sUsr_name")
            BindPaymentTerms()
            BindDocumentTemplate()
            If p_Modifyid = 0 Then
                If Not ddlBSU.SelectedItem Is Nothing Then
                    txtIssuedTo.Text = ddlBSU.SelectedItem.Text
                End If

                hdnIssuerId.Value = Mainclass.getDataValue("Select iss_id from DocIssuer where iss_id in(select DOT_ISS_ID from doctype where dot_id=" & ddlDocumentType.SelectedValue & ")", "OASIS_CLMConnectionString")
                If ddlDocType.SelectedValue = 0 Then
                    txtIssuer.Text = "Select a Document Type"
                Else
                    txtIssuer.Text = Mainclass.getDataValue("Select Iss_Descr from DocIssuer where iss_id In(Select DOT_ISS_ID from doctype where dot_id=" & ddlDocumentType.SelectedValue & ")", "OASIS_CLMConnectionString")
                End If
                h_BSUID.Value = ddlBSU.SelectedValue
                h_OLD_DOC_ID.Value = 0
                DisableButtons()
                EnableallChecks()









            Else
                Dim str_Sql As String
                'str_Sql = "Select iss_descr Doc_Issuer, BSU_NAME, [Doc_id], [Doc_Dot_id], [Doc_Narration], [Doc_LeadDays], [Doc_Olu_Name], [Doc_Bsu_Id], [Doc_Iss_Id], [Doc_SysDate], ISNULL([DOC_COMMENTS],'')[DOC_COMMENTS], isnull(DOC_STATUS,'A')DOC_STATUS, [DOC_OLD_DOC_ID], ISNULL([DOC_DELETED],0) DOC_DELETED,DOC_ISSDATE,DOC_EXPDATE,isnull(DOC_NO,'')DOC_NO from DocUpload INNER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=Doc_Bsu_Id inner join DocType on Dot_Id=Doc_Dot_id INNER JOIN DocIssuer on Iss_id=Dot_Iss_Id where doc_Id=" & p_Modifyid

                str_Sql = "DISPLAYDOCDETAILS"

                Dim rdDocDetails As SqlDataReader
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.Int)
                pParms(0).Value = p_Modifyid
                pParms(1) = New SqlClient.SqlParameter("@USER_NAME", SqlDbType.VarChar)
                pParms(1).Value = Session("sUsr_name")
                rdDocDetails = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, str_Sql, pParms)
                If rdDocDetails.HasRows Then
                    rdDocDetails.Read()
                    imgBSU.Visible = False
                    h_EntryId.Value = rdDocDetails("Doc_id")
                    h_OLD_DOC_ID.Value = rdDocDetails("Doc_id")
                    h_Doc_Olu_Name.Value = rdDocDetails("Doc_Olu_Name")
                    txtIssuedTo.Text = rdDocDetails("BSU_NAME")
                    txtIssuer.Text = rdDocDetails("Doc_Issuer")
                    hdnIssuerId.Value = rdDocDetails("Doc_Dot_Id")
                    'txtRemarks.Text = rdDocDetails("Doc_Narration")
                    txtRemarks.Text = rdDocDetails("Doc_Narration")
                    BindBSU()
                    BindSubGroup(rdDocDetails("Doc_Dot_Id"))

                    h_BSUID.Value = rdDocDetails("Doc_Bsu_id")
                    ddlBSU.SelectedValue = rdDocDetails("Doc_Bsu_id")
                    h_BSUNAME.Value = rdDocDetails("BSU_NAME")
                    'txtComments.Text = rdDocDetails("Doc_COMMENTS")
                    h_DOC_Status.Value = rdDocDetails("DOC_STATUS")
                    If h_DOC_Status.Value = "B" Then
                        btnBlock.Text = "UnBlock"
                    End If
                    h_DOC_Deleted.Value = rdDocDetails("DOC_DELETED")
                    txtIssueDate.Text = rdDocDetails("DOC_ISSDATE")
                    txtExpiryDate.Text = rdDocDetails("DOC_EXPDATE")
                    txtDocNo.Text = rdDocDetails("DOC_NO")
                    lblObservationNew.Text = rdDocDetails("DOC_OBSERVATION")

                    txtDocSpeName.Text = rdDocDetails("DOC_SPECIFICNAME")
                    ddlDocumentType.SelectedValue = hdnIssuerId.Value
                    ddlLandlord.SelectedValue = rdDocDetails("LLM_ID").ToString
                    ddlLandlord.Text = rdDocDetails("LLM_DESCR").ToString
                    ddlDocIssUser.SelectedValue = rdDocDetails("DOC_USR_ID")

                    ddlDepartment.SelectedValue = rdDocDetails("DOC_DPT_ID")
                    ddlSubGroup.SelectedValue = rdDocDetails("DOC_DSG_ID")


                    ddlPaymentTerms.SelectedValue = rdDocDetails("DOC_PTM_ID")
                    txtContractValue.Text = rdDocDetails("DOC_CONTRACT_VALUE")
                    ddlTemplate.SelectedValue = rdDocDetails("DOC_DTF_ID")


                    'Dim ProcDep1 As Integer
                    'ProcDep1 = Mainclass.getDataValue("select isnull(DOT_DEP_ID,0) from doctype with(nolock) where dot_id=" & ddlDocumentType.SelectedValue, "OASIS_CLMConnectionString")
                    'If ProcDep1 = 4 Then
                    '    Procure1.Visible = True
                    '    Procure2.Visible = True
                    'Else
                    '    Procure1.Visible = False
                    '    Procure2.Visible = False
                    '    ddlSubGroup.SelectedValue = 0
                    '    ddlDepartment.SelectedValue = 0
                    'End If

                    Dim ProcDep1 As Integer
                    ProcDep1 = Mainclass.getDataValue("select isnull(DOT_DEP_ID,0) from doctype with(nolock) where dot_id=" & ddlDocumentType.SelectedValue, "OASIS_CLMConnectionString")
                    If ProcDep1 = 4 Then
                        Procure1.Visible = True
                        Procure2.Visible = True
                        Procure3.Visible = True
                        Procure4.Visible = True
                        Procure5.Visible = True
                        Procure6.Visible = True
                    Else
                        Procure1.Visible = False
                        Procure2.Visible = False
                        Procure3.Visible = False
                        Procure4.Visible = False
                        Procure5.Visible = False
                        Procure6.Visible = False
                        ddlSubGroup.SelectedValue = 0
                        ddlDepartment.SelectedValue = 0
                        ddlTemplate.SelectedValue = 0
                        ddlPaymentTerms.SelectedValue = 0
                    End If

                    
                    h_ForwardText.Value = rdDocDetails("FORWARD_TEXT")
                    lnkSOP.OnClientClick = "return showDocument('" & encr.Encrypt_SHA256(rdDocDetails("Doc_id")) & "','" & rdDocDetails("DOT_SOPPATH") & "')"
                    TransferFooter = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select DOI_ID,DOI_DOC_ID,DOI_TYPE,DOI_FILE_name,doi_content_type,replace(convert(varchar(30), doi_sysdate, 106),' ','/')doi_sysdate,DOI_LIBRARY,DOI_FOLDER,DOI_UPLOAD_USER,DOI_PATH,'' Doi_File_nametemp,'' doi_Sourcepath,'' doi_SourceFile,'' DOI_PATH  from docimage where isnull(DOI_IS_FULLVERSION,0)=0 and Doi_Doc_id=" & h_EntryId.Value & " order by  DOI_ID DESC").Tables(0)
                    grdImage.DataSource = TransferFooter
                    grdImage.DataBind()
                    TransferFooterFullversion = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select DOI_ID,DOI_DOC_ID,DOI_TYPE,DOI_FILE_name,doi_content_type,replace(convert(varchar(30), doi_sysdate, 106),' ','/')doi_sysdate,DOI_LIBRARY,DOI_FOLDER,DOI_UPLOAD_USER,DOI_PATH,'' Doi_File_nametemp,'' doi_Sourcepath,'' doi_SourceFile,'' DOI_PATH  from docimage where isnull(DOI_IS_FULLVERSION,0)=1 and  Doi_Doc_id=" & h_EntryId.Value & " order by  DOI_ID DESC").Tables(0)
                    GRDFullVersion.DataSource = TransferFooterFullversion
                    GRDFullVersion.DataBind()

                    '------------------------------Setting button visiblity-----------------------------------------------------
                    btnSave.Visible = False
                    btnEdit.Visible = False

                    'DisableButtons()

                    hfbtnEdit.Value = (rdDocDetails("ShowEdit"))
                    hfbtnSave.Value = (rdDocDetails("ShowSave"))

                    btnUpload.Visible = (rdDocDetails("ShowSave"))

                    hfbtnRenew.Value = (rdDocDetails("ShowRenew"))
                    hfbtnRetire.Value = (rdDocDetails("ShowRetire"))
                    hfbtnForward.Value = (rdDocDetails("ShowForward"))
                    hfbtnApprove.Value = (rdDocDetails("ShowApprove"))
                    hfbtnBlock.Value = (rdDocDetails("ShowBlock"))


                    btnRenew.Visible = CBool(rdDocDetails("ShowRenew"))
                    btnRetire.Visible = CBool(rdDocDetails("ShowRetire"))
                    btnForward.Visible = CBool(rdDocDetails("ShowForward"))

                    btnForward.Text = rdDocDetails("FORWARD_TEXT").ToString

                    TermsCond.Visible = (rdDocDetails("ShowEdit"))
                    lnkTermsandConditions.Visible = (rdDocDetails("ShowEdit"))
                    btnUpload.Visible = Not (rdDocDetails("ShowEdit"))

                    btnApprove.Visible = CBool(rdDocDetails("ShowApprove"))

                    btnRevert.Visible = btnApprove.Visible
                    btnBlock.Visible = CBool(rdDocDetails("ShowBlock"))
                    If ViewState("datamode") = "add" Then
                        btnSave.Visible = rdDocDetails("ShowSave")
                        btnUpload.Visible = rdDocDetails("ShowSave")
                    Else
                        btnEdit.Visible = rdDocDetails("ShowEdit")
                        btnUpload.Visible = btnSave.Visible
                    End If
                    If CBool(rdDocDetails("ShowFullVersion")) = True Then
                        'f h_DOC_Status.Value = "A" Then
                        TrFullVersion1.Visible = True
                        TrFullVersion2.Visible = True
                        TrFullVersion.Visible = True
                        btnfvSave.Visible = True
                        If GRDFullVersion.Items.Count > 0 Then
                            btnfvSave.Visible = False
                            UploadDocFullversion.Enabled = False
                            btnFullVersion.Visible = False
                        Else
                            btnfvSave.Visible = True
                            UploadDocFullversion.Enabled = True
                            btnFullVersion.Visible = True
                        End If

                        'End If


                    Else
                        TrFullVersion1.Visible = False
                        TrFullVersion2.Visible = False
                        TrFullVersion.Visible = False
                        btnfvSave.Visible = False

                    End If


                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
                rdDocDetails.Close()
                rdDocDetails = Nothing
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
    End Sub
    Sub ClearDetails()
        h_EntryId.Value = "0"
        txtRemarks.Text = ""
        h_delete.Value = "0"
    End Sub

    Sub clearItem_All()
        txtDocNo.Text = ""
        txtIssueDate.Text = ""
        txtExpiryDate.Text = ""
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ItemEditMode = False
        SetDataMode("edit")
        grdImage.DataSource = TransferFooter
        grdImage.DataBind()
        UtilityObj.beforeLoopingControls(Me.Page)
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ShowUploadFullversionMode()
        btnSave.Visible = True
        btnApprove.Visible = False
        btnRevert.Visible = btnApprove.Visible
        TermsCond.Visible = False
        lnkTermsandConditions.Visible = False
        btnForward.Visible = False
        btnApprove.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            grdImage.DataSource = TransferFooter
            grdImage.DataBind()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        For i As Integer = 0 To grdTracked.Items.Count - 1
            Dim isChecked As Boolean = (CType(grdTracked.Items(i).FindControl("CheckSubdoc"), CheckBox)).Checked
            Dim SubDocId As HiddenField = CType(grdTracked.Items(i).FindControl("hdn_SubDocID"), HiddenField)
            If isChecked = True Then
                selectedSubDocIds = selectedSubDocIds & SubDocId.Value & ","
            End If
        Next

        If (TrFullVersion.Visible = True) Then

            Dim Errmsg As String = ""
            If TransferFooter.Rows.Count < 1 Then
                Errmsg = "Uploading Orginal,"
            End If

            If TransferFooterFullversion.Rows.Count < 1 Then
                Errmsg = Errmsg & "Upload Fullversion document,"
            End If

            If Errmsg <> "" Then
                Errmsg = Left(Errmsg, Len(Errmsg) - 1)
                ShowMessage(Errmsg)
                Exit Sub
            End If
        End If

        Dim BSUShort As String = Mainclass.getDataValue("select BSU_SHORTNAME from oasis..businessunit_m WITH(NOLOCK) where bsu_id='" & ddlBSU.SelectedValue & "'", "OASIS_CLMConnectionString")
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(24) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.Int)
        pParms(1).Value = h_EntryId.Value
        pParms(2) = New SqlClient.SqlParameter("@DOC_DOT_ID", SqlDbType.Int)
        pParms(2).Value = ddlDocumentType.SelectedValue
        pParms(3) = New SqlClient.SqlParameter("@DOC_ISS_ID", SqlDbType.Int)
        pParms(3).Value = hdnIssuerId.Value
        pParms(4) = New SqlClient.SqlParameter("@DOC_NARRATION", SqlDbType.VarChar)
        pParms(4).Value = txtRemarks.Text
        pParms(5) = New SqlClient.SqlParameter("@DOC_LEADDAYS", SqlDbType.Int)
        pParms(5).Value = 0
        pParms(6) = New SqlClient.SqlParameter("@DOC_OLU_NAME", SqlDbType.VarChar, 100)
        pParms(6).Value = Session("sUsr_name")
        pParms(7) = New SqlClient.SqlParameter("@DOC_BSU_ID", SqlDbType.VarChar, 20)
        pParms(7).Value = ddlBSU.SelectedValue
        pParms(8) = New SqlClient.SqlParameter("@DOC_COMMENTS", SqlDbType.VarChar, 2000)
        pParms(8).Value = "" 'txtComments.Text
        pParms(9) = New SqlClient.SqlParameter("@DOC_OLD_DOC_ID", SqlDbType.Int)
        pParms(9).Value = h_OLD_DOC_ID.Value
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        pParms(11) = New SqlClient.SqlParameter("@DOC_ISS_DATE", SqlDbType.DateTime)
        pParms(11).Value = txtIssueDate.Text
        pParms(12) = New SqlClient.SqlParameter("@DOC_EXP_DATE", SqlDbType.DateTime)
        pParms(12).Value = txtExpiryDate.Text
        pParms(13) = New SqlClient.SqlParameter("@DOC_NO", SqlDbType.VarChar, 100)
        pParms(13).Value = txtDocNo.Text

        'BSUShort = "Test"
        h_Library.Value = Mainclass.getDataValue("select BSU_SHORTNAME from oasis..businessunit_m where bsu_id='" & ddlBSU.SelectedValue & "'", "OASIS_CLMConnectionString")
        'h_Library.Value = "Dummy"
        h_Folder.Value = Mainclass.getDataValue("select ISNULL(DOT_SHAREPOINT_FOLDER,'') from DOCTYPE where DOT_ID='" & ddlDocumentType.SelectedValue & "'", "OASIS_CLMConnectionString")
        pParms(14) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(14).Direction = ParameterDirection.ReturnValue
        pParms(15) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
        pParms(15).Direction = ParameterDirection.Output
        pParms(16) = New SqlClient.SqlParameter("@DOC_SPECIFICNAME", SqlDbType.VarChar, 100)
        pParms(16).Value = ddlLandlord.SelectedItem.Text
        pParms(17) = New SqlClient.SqlParameter("@DOC_LLM_ID", SqlDbType.Int)
        pParms(17).Value = ddlLandlord.SelectedValue
        pParms(18) = New SqlClient.SqlParameter("@DOC_LINKED_SUB_DOC_IDS", SqlDbType.VarChar, 500)
        pParms(18).Value = selectedSubDocIds

        If Procure1.Visible = True Then
            pParms(19) = New SqlClient.SqlParameter("@DOC_DPT_ID", SqlDbType.Int)
            pParms(19).Value = ddlDepartment.SelectedValue

            pParms(20) = New SqlClient.SqlParameter("@DOC_DSG_ID", SqlDbType.Int)
            pParms(20).Value = ddlSubGroup.SelectedValue

            pParms(21) = New SqlClient.SqlParameter("@DOC_USR_ID", SqlDbType.Int)
            pParms(21).Value = ddlDocIssUser.SelectedValue


            pParms(22) = New SqlClient.SqlParameter("@DOC_PTM_ID", SqlDbType.Int)
            pParms(22).Value = ddlPaymentTerms.SelectedValue

            pParms(23) = New SqlClient.SqlParameter("@DOC_DTF_ID", SqlDbType.Int)
            pParms(23).Value = ddlTemplate.SelectedValue

            pParms(24) = New SqlClient.SqlParameter("@DOC_CONTRACT_VALUE", SqlDbType.Decimal)
            pParms(24).Value = txtContractValue.Text
        Else
            pParms(19) = New SqlClient.SqlParameter("@DOC_DPT_ID", SqlDbType.Int)
            pParms(19).Value = 0

            pParms(20) = New SqlClient.SqlParameter("@DOC_DSG_ID", SqlDbType.Int)
            pParms(20).Value = 0

            pParms(21) = New SqlClient.SqlParameter("@DOC_USR_ID", SqlDbType.Int)
            pParms(21).Value = 0


            pParms(22) = New SqlClient.SqlParameter("@DOC_PTM_ID", SqlDbType.Int)
            pParms(22).Value = 0

            pParms(23) = New SqlClient.SqlParameter("@DOC_DTF_ID", SqlDbType.Int)
            pParms(23).Value = 0

            pParms(24) = New SqlClient.SqlParameter("@DOC_CONTRACT_VALUE", SqlDbType.Decimal)
            pParms(24).Value = 0

        End If

       


        If (h_Library.Value = "" Or h_Folder.Value = "") Then
            'lblError.Text = "error with SharePoint Library or Folder"
            ShowMessage("error with SharePoint Library or Folder")
            Exit Sub
        End If

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveDocUpload", pParms)
            Dim ErrorMSG As String = pParms(15).Value.ToString()
            ViewState("EntryId") = pParms(10).Value
            If ErrorMSG = "" Then
                CheckSubDocumentStatus(ViewState("EntryId"), stTrans)
                If ExpSubDoc = False Then
                    Exit Sub
                End If
                If TransferFooter.Rows.Count >= 1 Then
                    Dim RowCount As Integer
                    For RowCount = 0 To TransferFooter.Rows.Count - 1
                        If TransferFooter.Rows(RowCount).RowState <> 8 Then
                            Dim Sharepointpath As String = ""

                            'Dim objSPC As New SharepointClass
                            'Dim cxt = objSPC.LoginToSharepoint(1)

                            'added by mahesh 29-Aug-2020
                            Dim DptID As Integer = 0
                            DptID = Mainclass.getDataValue("select dot_dep_id from doctype where dot_id='" & ddlDocumentType.SelectedValue & "'", "OASIS_CLMConnectionString")
                            Dim objSPC As New SharepointClass
                            Dim cxt = objSPC.LoginToSharepoint(DptID)


                            Dim FileError As Boolean = False
                            If cxt Is Nothing Then
                                Exit Sub
                            End If

                            If TransferFooter.Rows(RowCount)("DOI_Sourcepath") <> "" Then
                                'Dim AlreadyFilesUploaded As Integer = 0
                                'AlreadyFilesUploaded = Mainclass.getDataValue("select isnull(count(doi_id),0) count from docimage where doi_doc_id=" & IIf(h_EntryId.Value = 0, ViewState("EntryId"), h_EntryId.Value) & " and doi_type='" & TransferFooter.Rows(RowCount)("DOI_TYPE") & "'", "OASIS_CLMConnectionString")

                                'If AlreadyFilesUploaded > 0 Then
                                '    ShowMessage("This docuement is already attached")
                                '    stTrans.Rollback()
                                '    Exit Sub
                                'End If
                                Try
                                    'UtilityObj.Errorlog("Btnsaveclick" & IO.File.Exists(TransferFooter.Rows(RowCount)("DOI_Sourcepath")), "SharepointClass")
                                    If IO.File.Exists(TransferFooter.Rows(RowCount)("DOI_Sourcepath")) Then
                                        SharepointClass.UploadFile(cxt, h_Library.Value, h_Folder.Value, TransferFooter.Rows(RowCount)("DOI_Sourcepath"), Sharepointpath, BSUShort, ViewState("EntryId"))
                                    Else
                                        'lblError.Text = "there is no file upload to Sharepoint server"
                                        ShowMessage("No file found to upload")
                                        stTrans.Rollback()
                                        Exit Sub
                                    End If
                                Catch ex As Exception
                                    'lblError.Text = "Error while upload to Sharepoint folder. Error: " + ex.Message
                                    ShowMessage("Error while upload to Sharepoint folder. Error: " + ex.Message)
                                    stTrans.Rollback()
                                    Exit Sub
                                End Try
                                Try

                                    If Sharepointpath = "" Then
                                        'lblError.Text = "Error while upload to Sharepoint folder."
                                        ShowMessage("Error while upload to Sharepoint folder")
                                        stTrans.Rollback()
                                        Exit Sub
                                    Else
                                        If IO.File.Exists(TransferFooter.Rows(RowCount)("DOI_Sourcepath")) Then
                                            Dim fileToDelete As New FileInfo(TransferFooter.Rows(RowCount)("DOI_Sourcepath"))
                                            fileToDelete.Delete()
                                        End If
                                    End If
                                    Dim iParms(10) As SqlClient.SqlParameter
                                    iParms(1) = New SqlClient.SqlParameter("@DOI_ID", SqlDbType.Int)
                                    iParms(1).Value = TransferFooter.Rows(RowCount)("doi_Id")
                                    iParms(2) = New SqlClient.SqlParameter("@DOI_DOC_ID", SqlDbType.Int)
                                    iParms(2).Value = ViewState("EntryId")
                                    iParms(3) = New SqlClient.SqlParameter("@DOI_FILE_NAME", SqlDbType.VarChar, 100)
                                    iParms(3).Value = TransferFooter.Rows(RowCount)("doi_file_name")
                                    iParms(4) = New SqlClient.SqlParameter("@DOI_CONTENT_TYPE", SqlDbType.VarChar, 100)
                                    iParms(4).Value = TransferFooter.Rows(RowCount)("doi_content_type")
                                    iParms(5) = New SqlClient.SqlParameter("@DOI_LIBRARY", SqlDbType.VarChar, 100)
                                    iParms(5).Value = TransferFooter.Rows(RowCount)("doi_library")
                                    iParms(6) = New SqlClient.SqlParameter("@DOI_FOLDER", SqlDbType.VarChar, 100)
                                    iParms(6).Value = TransferFooter.Rows(RowCount)("doi_folder")
                                    iParms(7) = New SqlClient.SqlParameter("@DOI_UPLOAD_USER", SqlDbType.VarChar, 20)
                                    iParms(7).Value = Session("sUsr_name")
                                    iParms(8) = New SqlClient.SqlParameter("@DOI_TYPE", SqlDbType.VarChar, 1000)
                                    iParms(8).Value = TransferFooter.Rows(RowCount)("doi_Type")
                                    iParms(9) = New SqlClient.SqlParameter("@DOI_PATH", SqlDbType.VarChar, 1000)
                                    iParms(9).Value = encr.Encrypt_SHA256(Sharepointpath)

                                    iParms(10) = New SqlClient.SqlParameter("@DOI_IS_FULLVERSION", SqlDbType.Int)
                                    iParms(10).Value = 0
                                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveDocImage", iParms)
                                Catch ex As Exception
                                    UtilityObj.Errorlog(ex.Message, "UploadDocument")
                                End Try
                            End If
                        End If
                    Next
                End If

                If (ViewState("datamode") = "add" And TrFullVersion.Visible = True) Then
                    'SaveFullversion()
                    UPdateFullVersionDetails()
                End If

                If h_delete.Value.Length > 1 Then SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "delete from docImage where doi_id in (" & h_delete.Value & ")")
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("FD00072", h_EntryId.Value + "-" + h_delete.Value, "Delete", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                stTrans.Commit()
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))
                'lblError.Text = "Data Saved Successfully !!!"
                ShowMessage("Data Saved Successfully", False)
                HideButtons()
            Else
                'lblError.Text = ErrorMSG
                UtilityObj.Errorlog(ErrorMSG, "UploadDocument1")
                ShowMessage(ErrorMSG)
                stTrans.Rollback()
                Exit Sub
            End If
        Catch ex As Exception
            'lblError.Text = ex.Message
            UtilityObj.Errorlog(ex.Message, "UploadDocument2")
            ShowMessage(ex.Message)
            stTrans.Rollback()
            Exit Sub
        Finally
            objConn.Close()
        End Try
    End Sub
    Private Sub CheckSubDocumentStatus(ByVal DocId As Integer, Optional Tran As SqlTransaction = Nothing)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(10) As SqlParameter
            Dim ExpiredSubDocErrMsg As String = ""

            PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
            PARAM(1) = New SqlParameter("@FILTER", "ALL")
            PARAM(2) = New SqlParameter("@BSU_ID", ddlBSU.SelectedValue)
            PARAM(3) = New SqlParameter("@FROMDT", "1900-01-01")
            PARAM(4) = New SqlParameter("@TODT", "1900-01-01")
            PARAM(5) = New SqlParameter("@DTYPE", ddlDocumentType.SelectedValue)
            PARAM(6) = New SqlParameter("@SORT", 0)
            PARAM(7) = New SqlParameter("@DGROUP", 0)
            PARAM(8) = New SqlParameter("@LDAYS", 0)
            PARAM(9) = New SqlParameter("@MAIN_DOC_ID", DocId)
            Dim dsDetails As DataSet

            If Tran Is Nothing Then
                dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DOC_GETDOCLIST", PARAM)
            Else
                dsDetails = SqlHelper.ExecuteDataset(Tran, CommandType.StoredProcedure, "DOC_GETDOCLIST", PARAM)
            End If


            Dim Dt As DataTable
            Dt = dsDetails.Tables(1)

            For Each row In Dt.Rows
                If (row("CurrStatus") <> "A") Then
                    ExpiredSubDocErrMsg = ExpiredSubDocErrMsg + "Type:" + row.Item("Documenttype") + ",No:" + row.Item("DocumentNo") + ",Status:" + row.Item("CurrstatusDescr") + "<br>"
                End If

            Next
            If ExpiredSubDocErrMsg = "" Then
                ExpSubDoc = True
            Else
                'lblError.Text = "Error:<br> " + ExpiredSubDocErrMsg
                ShowMessage("Error:Document can not be renewed,there are some issues with below documents. <br>" + ExpiredSubDocErrMsg)
                ExpSubDoc = False
                Exit Sub
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            ShowMessage(ex.Message)
            ExpSubDoc = False
            Exit Sub
        End Try
    End Sub
    Private Sub HideButtons()
        If ViewState("datamode") = "view" Then
            'btnEdit.Visible = False
            'btnRenew.Visible = False
            'btnRetire.Visible = False
            'btnApprove.Visible = False
            'btnSave.Visible = False
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE DocUpload SET DOC_DELETED=1 where doc_id=" & h_EntryId.Value)
                If Not TransferFooter Is Nothing Then
                    Dim RowCount As Integer
                    For RowCount = 0 To TransferFooter.Rows.Count - 1
                        If TransferFooter.Rows(RowCount).RowState <> 8 Then
                            If TransferFooter.Rows(RowCount)("doi_id") > 0 Then
                                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE docImage SET DOI_DELETED=1 where Doi_id=" & TransferFooter.Rows(RowCount)("doi_id"))
                            End If
                        End If
                    Next
                End If
                stTrans.Commit()
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("FD00072", h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            ClearDetails()
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub



    Private Function getContentType(ByVal FilePath As String) As String
        Dim errString As String = "", contentType As String = ""
        Try
            Dim filename As String = Path.GetFileName(FilePath)
            Dim ext As String = Path.GetExtension(filename)
            Select Case ext.ToLower
                Case ".doc"
                    contentType = "application/vnd.ms-word"
                    Exit Select
                Case ".docx"
                    contentType = "application/vnd.ms-word"
                    Exit Select
                Case ".xls"
                    contentType = "application/vnd.ms-excel"
                    Exit Select
                Case ".xlsx"
                    contentType = "application/vnd.ms-excel"
                    Exit Select
                Case ".jpg"
                    contentType = "image/jpg"
                    Exit Select
                Case ".png"
                    contentType = "image/png"
                    Exit Select
                Case ".gif"
                    contentType = "image/gif"
                    Exit Select
                Case ".pdf"
                    contentType = "application/pdf"
                    Exit Select
            End Select
            If contentType <> String.Empty Then
                If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) from DocType where dot_id=" & ddlDocumentType.SelectedValue.Split(";")(0) & " AND '||'+Dot_Con_Type LIKE '%||' + (select distinct cast(Content_Type_No as varchar) FROM DocContentType where Content_Type_Name='" & contentType & "') + '||%'") = 0 Then
                    errString = "File format not allowed." '& " Upload Image/Word/PDF/Excel formats"
                End If
            Else
                errString = "File format not recognised." & " Upload Image/Word/PDF/Excel formats"
            End If
            If errString.Length > 0 Then
                'lblError.Text = errString
                ShowMessage(errString)

                contentType = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
        End Try
        Return contentType
    End Function

    Private Function ConvertDocumentToBytes(ByVal FilePath As String, Optional ByRef ContentType As String = "") As Byte()
        Try
            Dim filename As String = Path.GetFileName(FilePath)
            Dim ext As String = Path.GetExtension(filename)
            Dim fs As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim br As New BinaryReader(fs)
            Dim bytes As Byte() = br.ReadBytes(fs.Length)
            ConvertDocumentToBytes = bytes
            br.Close()
            fs.Close()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            ConvertDocumentToBytes = Nothing
        Finally
        End Try
    End Function

    'Protected Sub rptImages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles rptImages.ItemCommand
    '    If e.CommandSource.Text = "Delete" Or e.CommandSource.ToString = "Delete" Then
    '        Dim hdnDoi_Id As HiddenField = CType(e.Item.FindControl("hdnDoi_Id"), HiddenField)
    '        If CType(hdnDoi_Id.Value, Integer) > 0 Then h_delete.Value = h_delete.Value & "," & hdnDoi_Id.Value
    '        TransferFooter.Rows.Remove(TransferFooter.Rows(e.Item.ItemIndex))
    '        'rptImages.DataSource = TransferFooter
    '        'rptImages.DataBind()
    '    End If
    'End Sub

    'Protected Sub rptImages_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles rptImages.ItemDataBound
    '    Dim hdnDoi_Id As HiddenField = CType(e.Item.FindControl("hdnDoi_Id"), HiddenField)
    '    Dim hdnDoi_content_type As HiddenField = CType(e.Item.FindControl("hdnDoi_content_type"), HiddenField)
    '    Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete"), LinkButton)

    '    Dim hdnDoi_File_NameTemp As HiddenField = CType(e.Item.FindControl("hdnDoi_File_NameTemp"), HiddenField)
    '    Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
    '    btnDocumentLink.OnClientClick = "return showDocument('" & hdnDoi_Id.Value & "','" & hdnDoi_content_type.Value & "','" & hdnDoi_File_NameTemp.Value & "')"
    '    btnDelete.Visible = (h_Doc_Olu_Name.Value = Session("sUsr_name") Or hdnDoi_Id.Value = "0") 'And ViewState("datamode") = "edit"
    '    Exit Sub
    'End Sub

    Public Shared Sub Zip(ByVal SrcFile As String, ByVal DstFile As String)
        Dim fileStreamIn As FileStream = New FileStream(SrcFile, FileMode.Open, FileAccess.Read)
        Dim fileStreamOut As FileStream = New FileStream(DstFile, FileMode.Create, FileAccess.Write)
        Dim zipOutStream As ZipOutputStream = New ZipOutputStream(fileStreamOut)
        Dim buffer() As Byte = New Byte(4096) {}
        Dim enTry As ZipEntry = New ZipEntry(Path.GetFileName(SrcFile))
        zipOutStream.PutNextEntry(enTry)
        Dim size As Integer
        Do
            size = fileStreamIn.Read(buffer, 0, buffer.Length)
            zipOutStream.Write(buffer, 0, size)
        Loop While size > 0
        zipOutStream.Close()
        fileStreamOut.Close()
        fileStreamIn.Close()
    End Sub
    Private Sub AddFullVersionDocumentItem()
        Try
            ShowMessage("")
            If Not IsDate(txtIssueDate.Text) Then
                ShowMessage(preError.InnerHtml & " <br /> Invalid issue date")
            End If
            If Not IsDate(txtExpiryDate.Text) Then

                ShowMessage(preError.InnerHtml & " <br /> Invalid expiry date")
            End If
            If txtDocNo.Text.Trim.Length = 0 Then
                ShowMessage(preError.InnerHtml & " <br /> Document number is mandatory")
            End If
            If hdnDoi_File_Name_FV.Value.Length = 0 Then
                ShowMessage(preError.InnerHtml & " <br /> File upload is mandatory")
            End If
            If GRDFullVersion.Items.Count > 0 Then
                ShowMessage(preError.InnerHtml & " <br /> File already exists for the document")
            End If
            If preError.InnerHtml.Length > 0 Then Exit Sub

            Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile")

            'Dim path As String = Server.MapPath("~/UploadedImages/")
            Dim path As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile") + "CLM\FULLVERSION"
            Dim fileOK As Boolean = False
            Dim fileCopied As Boolean = False
            Try
                ' will delete the file if files existis
                If IO.File.Exists(path & "\" & h_sourceFileName_FullVersion.Value) Then
                    Dim ContentType As String = getContentType(path & "\" & h_sourceFileName_FullVersion.Value)
                    Dim fileToDelete As New FileInfo(path & "\" & h_sourceFileName_FullVersion.Value)
                    fileToDelete.Delete()
                    UploadDocFullversion.PostedFile.SaveAs(path & "\" & h_sourceFileName_FullVersion.Value)
                Else
                    UploadDocFullversion.PostedFile.SaveAs(path & "\" & h_sourceFileName_FullVersion.Value)
                    fileCopied = True
                End If
            Catch ex As Exception
                'lblError.Text = ex.Message
                ShowMessage(ex.Message)
                Exit Sub
            End Try

            Dim mrow As DataRow
            If ItemEditMode Then
                If TransferFooter.Select("Doi_ID=" & hdnDoi_Id.Value, "").Length > 0 Then
                    mrow = TransferFooterFullversion.Select("Doi_ID=" & hdnDoi_Id.Value, "")(0)
                Else
                    ShowMessage("Unexpected Error")
                    Exit Sub
                End If
            Else
                mrow = TransferFooterFullversion.NewRow
            End If
            If Not ItemEditMode Then hdnDoi_Id.Value = 0
            mrow("DOI_ID") = hdnDoi_Id.Value
            mrow("DOI_DOC_ID") = Val(h_EntryId.Value)
            mrow("DOI_TYPE") = "Full Version"
            mrow("DOI_LIBRARY") = h_Library.Value
            mrow("DOI_FOLDER") = h_Folder.Value
            mrow("doi_content_type") = hdnDoi_content_type_FV.Value
            mrow("doi_file_nametemp") = hdnDoi_File_NameTemp_FV.Value
            mrow("doi_file_name") = hdnDoi_File_Name_FV.Value
            mrow("doi_sysdate") = Now.ToString("dd/MMM/yyyy")
            mrow("doi_Sourcepath") = path + "\" + h_sourceFileName_FullVersion.Value
            mrow("doi_SourceFile") = h_sourceFileName_FullVersion.Value
            mrow("DOI_PATH") = h_DocumentPathFullVersion.Value
            If Not ItemEditMode Then
                TransferFooterFullversion.Rows.Add(mrow)
            End If
            ItemEditMode = False
            SetItemEditMode(True)
            GRDFullVersion.DataSource = TransferFooterFullversion
            GRDFullVersion.DataBind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub AddDocumentItem()
        Try
            'lblError.Text = ""


            ShowMessage("")
            If Not IsDate(txtIssueDate.Text) Then
                'lblError.Text = "Invalid Issue Date"
                ShowMessage(preError.InnerHtml & " <br /> Invalid issue date")
            End If
            If Not IsDate(txtExpiryDate.Text) Then
                'If lblError.Text.Length > 0 Then lblError.Text &= ", "
                'lblError.Text &= "Invalid Expiry Date"
                ShowMessage(preError.InnerHtml & " <br /> Invalid expiry date")
            End If
            If txtDocNo.Text.Trim.Length = 0 Then
                'If lblError.Text.Length > 0 Then lblError.Text &= ", "
                'lblError.Text &= "Document Number is mandatory"
                ShowMessage(preError.InnerHtml & " <br /> Document number is mandatory")
            End If
            If hdnDoi_File_Name.Value.Length = 0 Then
                'If lblError.Text.Length > 0 Then lblError.Text &= ", "
                'lblError.Text &= "File Upload is mandatory"
                ShowMessage(preError.InnerHtml & " <br /> File upload is mandatory")
            End If
            If grdImage.Items.Count > 0 Then
                ShowMessage(preError.InnerHtml & " <br /> File already exists for the document")
            End If
            If preError.InnerHtml.Length > 0 Then Exit Sub

            Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile")

            'Dim path As String = Server.MapPath("~/UploadedImages/")
            Dim path As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile") + "CLM"
            Dim fileOK As Boolean = False
            Dim fileCopied As Boolean = False
            Try
                ' will delete the file if files existis
                If IO.File.Exists(path & "\" & h_sourceFileName.Value) Then
                    Dim ContentType As String = getContentType(path & "\" & h_sourceFileName.Value)
                    Dim fileToDelete As New FileInfo(path & "\" & h_sourceFileName.Value)
                    fileToDelete.Delete()
                    UploadDocPhoto.PostedFile.SaveAs(path & "\" & h_sourceFileName.Value)
                Else
                    UploadDocPhoto.PostedFile.SaveAs(path & "\" & h_sourceFileName.Value)
                    fileCopied = True
                End If
            Catch ex As Exception
                'lblError.Text = ex.Message
                ShowMessage(ex.Message)
                Exit Sub
            End Try

            Dim mrow As DataRow
            If ItemEditMode Then
                If TransferFooter.Select("Doi_ID=" & hdnDoi_Id.Value, "").Length > 0 Then
                    mrow = TransferFooter.Select("Doi_ID=" & hdnDoi_Id.Value, "")(0)
                Else
                    'lblError.Text = "Unexpected Error!!"
                    ShowMessage("Unexpected Error")
                    Exit Sub
                End If
            Else
                mrow = TransferFooter.NewRow
            End If
            If Not ItemEditMode Then hdnDoi_Id.Value = 0
            mrow("DOI_ID") = hdnDoi_Id.Value
            mrow("DOI_DOC_ID") = Val(h_EntryId.Value)
            mrow("DOI_TYPE") = ddlDocType.SelectedItem.Text
            mrow("DOI_LIBRARY") = h_Library.Value
            mrow("DOI_FOLDER") = h_Folder.Value
            mrow("doi_content_type") = hdnDoi_content_type.Value
            mrow("doi_file_nametemp") = hdnDoi_File_NameTemp.Value
            mrow("doi_file_name") = hdnDoi_File_Name.Value
            mrow("doi_sysdate") = Now.ToString("dd/MMM/yyyy")
            mrow("doi_Sourcepath") = path + "\" + h_sourceFileName.Value
            mrow("doi_SourceFile") = h_sourceFileName.Value
            mrow("DOI_PATH") = h_DocumentPath.Value
            If Not ItemEditMode Then
                TransferFooter.Rows.Add(mrow)
            End If
            ItemEditMode = False
            SetItemEditMode(True)
            grdImage.DataSource = TransferFooter
            grdImage.DataBind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub grdImage_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdImage.DeleteCommand
        If e.CommandSource.Text = "Delete" Or e.CommandSource.ToString = "Delete" Then
            Dim hdnDoi_Id As HiddenField = CType(e.Item.FindControl("hdnDoi_Id"), HiddenField)
            If CType(hdnDoi_Id.Value, Integer) > 0 Then h_delete.Value = h_delete.Value & "," & hdnDoi_Id.Value
            TransferFooter.Rows.Remove(TransferFooter.Rows(e.Item.ItemIndex))
            grdImage.DataSource = TransferFooter
            grdImage.DataBind()
        End If
    End Sub

    Protected Sub grdImage_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdImage.ItemCommand

    End Sub

    Protected Sub grdImage_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdImage.ItemDataBound

        Try
            Dim hdnDoi_Id As HiddenField = CType(e.Item.FindControl("hdnDoi_Id"), HiddenField)
            Dim hdnDoi_content_type As HiddenField = CType(e.Item.FindControl("hdnDoi_content_type"), HiddenField)
            Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete"), LinkButton)
            Dim hdnDoi_File_Name As HiddenField = CType(e.Item.FindControl("hdnDoi_File_Name"), HiddenField)
            Dim hdnDoi_File_NameTemp As HiddenField = CType(e.Item.FindControl("hdnDoi_File_NameTemp"), HiddenField)
            Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink"), LinkButton)
            Dim hdnDoi_SharepointPath As HiddenField = CType(e.Item.FindControl("hdn_DOI_PATH"), HiddenField)
            If Not hdnDoi_SharepointPath Is Nothing Then
                btnDocumentLink.Visible = (hdnDoi_SharepointPath.Value.Length > 0)
            End If
            btnDocumentLink.OnClientClick = "return showDocument('" & encr.Encrypt_SHA256(hdnDoi_Id.Value) & "','" & hdnDoi_SharepointPath.Value & "')"

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlDocumentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocumentType.SelectedIndexChanged
        BindBSU()
        LoadAddnewRemarks()
        BindDocUploadtype()
        BindGrid(ddlDocumentType.SelectedValue)
        DisableButtons()
        BindSubGroup(ddlDocType.SelectedValue)

        Dim ProcDep1 As Integer
        ProcDep1 = Mainclass.getDataValue("select isnull(DOT_DEP_ID,0) from doctype with(nolock) where dot_id=" & ddlDocumentType.SelectedValue, "OASIS_CLMConnectionString")
        If ProcDep1 = 4 Then
            Procure1.Visible = True
            Procure2.Visible = True
            Procure3.Visible = True
            Procure4.Visible = True
            Procure5.Visible = True
            Procure6.Visible = True
        Else
            Procure1.Visible = False
            Procure2.Visible = False
            Procure3.Visible = False
            Procure4.Visible = False
            Procure5.Visible = False
            Procure6.Visible = False
            ddlSubGroup.SelectedValue = 0
            ddlDepartment.SelectedValue = 0
        End If
        BindPaymentTerms()
        BindDocumentTemplate()

    End Sub
    Private Sub BindPaymentTerms()
        Dim StrPymtTerms As String = ""
        StrPymtTerms = "select PTM_ID ID,PTM_DESCR DESCR from PAYMENTTERMS_M where PTM_bDELETED=0 union all select 0,'----Select One-----' order by PTM_ID"
        fillDropdown(ddlPaymentTerms, StrPymtTerms, "DESCR", "ID", True)
    End Sub
    Private Sub BindDocumentTemplate()
        Dim StrDocTemp As String = ""
        StrDocTemp = "select DTF_ID ID,DTF_DESCR DESCR from DOCUMENTTEMPLATEFORMAT_M where DTF_bDELETED=0 union all select 0,'----Select One-----' order by DTF_ID "
        fillDropdown(ddlTemplate, StrDocTemp, "DESCR", "ID", True)
    End Sub

    Protected Sub ddlBSU_123_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSU.SelectedIndexChanged
        'BindBSU()
        LoadAddnewRemarks()
        BindDocUploadtype()
        BindGrid(ddlDocumentType.SelectedValue)
        DisableButtons()
    End Sub
    Private Sub LoadAddnewRemarks()
        If ddlDocumentType.SelectedValue <> 0 Then
            Dim issuerDetails() As String = SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select iss_descr+';'+cast(iss_id as varchar) from DocType INNER join docIssuer on Iss_id=Dot_Iss_Id inner JOIN docCity on Iss_CityId=Cit_id where dot_id=" & ddlDocumentType.SelectedValue).split(";")
            txtIssuer.Text = issuerDetails(0)
            hdnIssuerId.Value = issuerDetails(1)

        End If


        Dim str_Sql As String


        str_Sql = "SELECT * FROM [dbo].[GetNotesAndStatusForDocument]('" & ddlBSU.SelectedValue & "','" & Session("sUsr_name") & "'," & ddlDocumentType.SelectedValue & ")"

        Dim rdDocDetails As SqlDataReader

        rdDocDetails = SqlHelper.ExecuteReader(connectionString, CommandType.Text, str_Sql)
        If rdDocDetails.HasRows Then
            rdDocDetails.Read()
            lblObservationNew.Text = rdDocDetails("Notes")

            If rdDocDetails("STATUS") <> "YES" Then
                btnSave.Visible = False
                btnUpload.Visible = False
            Else
                btnSave.Visible = True
                btnUpload.Visible = True
            End If

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
        rdDocDetails.Close()
        rdDocDetails = Nothing

    End Sub

    Private Property TransferFooter() As DataTable
        Get
            Return ViewState("TransferFooter")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TransferFooter") = value
        End Set
    End Property

    Private Property TransferFooterFullversion() As DataTable
        Get
            Return ViewState("TransferFooterFullversion")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TransferFooterFullversion") = value
        End Set
    End Property

    Private Property ItemEditMode() As Boolean
        Get
            Return ViewState("ItemEditMode")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ItemEditMode") = value
        End Set
    End Property

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then

        End If
    End Sub
    Private Sub ddlBSU_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBSU.SelectedIndexChanged
        h_BSUID.Value = ddlBSU.SelectedValue
        BindGrid(ddlDocumentType.SelectedValue)
    End Sub
    Private Sub btnRetire_Click(sender As Object, e As EventArgs) Handles btnRetire.Click
        lblError.Text = ""

        'If LTrim(RTrim(txtComments.Text)) = "" Then
        '    lblError.Text = "Comments must enter for Retiring Document."
        '    Exit Sub
        'End If
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE DocUpload SET DOC_STATUS='R',DOC_COMMENTS='" & LTrim(RTrim(txtComments.Text)) & "' where doc_id=" & h_EntryId.Value)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE DocUpload SET DOC_STATUS='C' where doc_id=" & h_EntryId.Value)
                'If Not TransferFooter Is Nothing Then
                '    Dim RowCount As Integer
                '    For RowCount = 0 To TransferFooter.Rows.Count - 1
                '        If TransferFooter.Rows(RowCount).RowState <> 8 Then
                '            If TransferFooter.Rows(RowCount)("doi_id") > 0 Then
                '                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE docImage SET DOI_DELETED=1 where Doi_id=" & TransferFooter.Rows(RowCount)("doi_id"))
                '            End If
                '        End If
                '    Next
                'End If
                stTrans.Commit()
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("DM00032", h_EntryId.Value, "RETIRE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            'ClearDetails()
            lblError.Text = "Retiring document done Successfully...!"
            'Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Private Sub btnRenew_Click(sender As Object, e As EventArgs) Handles btnRenew.Click

        CheckSubDocumentStatus(h_EntryId.Value)
        If ExpSubDoc = False Then
            Exit Sub
        End If


        lnkFromDate.Enabled = True
        lnkToDate.Enabled = True
        txtDocNo.Text = ""
        txtDocNo.Enabled = True
        btnDocumentLink.Visible = False
        txtRemarks.Enabled = True
        h_EntryId.Value = 0
        TransferFooter = Nothing
        TransferFooterFullversion = Nothing


        UploadDocPhoto.Enabled = True
        btnUpload.Enabled = True

        TransferFooter = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select DOI_ID,DOI_DOC_ID,'' DOI_TYPE,DOI_FILE_name,doi_content_type,replace(convert(varchar(30), getdate(), 106),' ','/')doi_sysdate,DOI_LIBRARY,DOI_FOLDER,DOI_UPLOAD_USER, Doi_Doc_id, Doi_Content_Type, replace(convert(varchar(30), Doi_SysDate, 106),' ','/') Doi_SysDate, Doi_File_name, '' Doi_File_nametemp,''DOI_Sourcepath,''DOI_SOURCEFILE,DOI_PATH from docimage where 1=0").Tables(0)
        TransferFooterFullversion = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select DOI_ID,DOI_DOC_ID,'' DOI_TYPE,DOI_FILE_name,doi_content_type,replace(convert(varchar(30), getdate(), 106),' ','/')doi_sysdate,DOI_LIBRARY,DOI_FOLDER,DOI_UPLOAD_USER, Doi_Doc_id, Doi_Content_Type, replace(convert(varchar(30), Doi_SysDate, 106),' ','/') Doi_SysDate, Doi_File_name, '' Doi_File_nametemp,''DOI_Sourcepath,''DOI_SOURCEFILE,DOI_PATH from docimage where 1=0").Tables(0)
        SetDataMode("add")
        BindBSU()

        ddlBSU.SelectedValue = h_BSUID.Value
        ddlBSU.SelectedItem.Text = h_BSUNAME.Value
        ddlDocumentType.Enabled = False
        ddlDepartment.Enabled = False
        ddlSubGroup.Enabled = False

        ddlBSU.Enabled = False

        grdImage.DataSource = TransferFooter
        grdImage.DataBind()

        GRDFullVersion.DataSource = TransferFooterFullversion
        GRDFullVersion.DataBind()
        EditDetails1.Visible = True
        EditDetails2.Visible = True
        EditDetails3.Visible = True
        EditDetails4.Visible = True
        txtDocNo.Enabled = True
        txtIssueDate.Enabled = True
        txtExpiryDate.Enabled = True
        txtRemarks.Enabled = True
        btnRenew.Visible = False
        btnRetire.Visible = False
        btnSave.Visible = True
        btnfvSave.Visible = False
        'DisableButtons()
        UploadDocFullversion.Enabled = True
        btnFullVersion.Visible = True



    End Sub
    Private Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        lblError.Text = ""
        If h_EntryId.Value > 0 Then
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                'SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, "UPDATE DocUpload SET DOC_STATUS='A' where doc_id=" & h_EntryId.Value)


                Dim pParms(6) As SqlClient.SqlParameter
                pParms(1) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.Int)
                pParms(1).Value = h_EntryId.Value
                pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
                pParms(2).Value = ddlBSU.SelectedValue
                pParms(3) = New SqlClient.SqlParameter("@USERNAME", SqlDbType.VarChar)
                pParms(3).Value = Session("sUsr_name")
                pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue
                pParms(5) = New SqlClient.SqlParameter("@ERROR_MSG", SqlDbType.VarChar, 1000)
                pParms(5).Direction = ParameterDirection.Output

                Try

                    Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SP_APPROVE_DOCUMENT", pParms)
                    Dim ErrorMSG As String = pParms(5).Value.ToString()
                    'ViewState("EntryId") = pParms(10).Value
                    If ErrorMSG = "" Then
                        stTrans.Commit()
                        'stTrans.Rollback()
                        ShowMessage("Document Approved Successfully", False)
                    Else
                        stTrans.Rollback()
                        Errorlog(ErrorMSG)
                        ShowMessage(ErrorMSG)
                        Exit Sub
                    End If

                Catch ex As Exception
                    stTrans.Rollback()
                    Errorlog(ex.Message)
                    ShowMessage(ex.Message)
                    Exit Sub
                End Try
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                ShowMessage(ex.Message)
                Exit Sub
            Finally
                objConn.Close()
            End Try
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("FD00034", h_EntryId.Value, "APPROVE", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            'ClearDetails()
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
        End If
    End Sub

    Private Sub ddlDocType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDocType.SelectedIndexChanged
        'BindDocUploadtype()


    End Sub
    Private Sub btnForward_Click(sender As Object, e As EventArgs) Handles btnForward.Click
        If TermsandConditions.Checked = False Then
            ShowMessage("Please accept terms & conditions for Forwarding Approval")
            Exit Sub
        End If
        UpdateDocDetails("F")
    End Sub
    Private Sub UpdateDocDetails(ByVal Action As String)
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.Int)
        pParms(1).Value = h_EntryId.Value
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms(2).Value = ddlBSU.SelectedValue
        pParms(3) = New SqlClient.SqlParameter("@ACTION", SqlDbType.VarChar, 100)
        pParms(3).Value = Action
        pParms(4) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 100)
        pParms(4).Value = Session("sUsr_name")
        pParms(5) = New SqlClient.SqlParameter("@BLOCKDATE", SqlDbType.DateTime)
        pParms(5).Value = "01-01-1900"
        pParms(6) = New SqlClient.SqlParameter("@COMMENTS", SqlDbType.VarChar)
        pParms(6).Value = ""
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@SUCCESS_MSG", SqlDbType.VarChar, 1000)
        pParms(9).Direction = ParameterDirection.Output
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SP_ChangeDOC_Status", pParms)
            Dim ErrorMSG As String = pParms(8).Value.ToString()
            If ErrorMSG = "" Then
                stTrans.Commit()
                'stTrans.Rollback()
                If Action = "B" Then
                    ShowMessage(pParms(9).Value.ToString(), False)
                Else
                    ShowMessage(pParms(9).Value.ToString(), False)
                End If
            Else
                ShowMessage(ErrorMSG)
                stTrans.Rollback()
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
            stTrans.Rollback()
            Exit Sub
        Finally
            objConn.Close()
        End Try
        SetDataMode("view")
        setModifyvalues(ViewState("EntryId"))
        'DisableButtons()
    End Sub
    Private Sub btnBlock_Click(sender As Object, e As EventArgs) Handles btnBlock.Click
        If btnBlock.Text = "UnBlock" Then
            UpdateDocDetails("U")
        Else
            UpdateDocDetails("B")
        End If

    End Sub
    Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
        If Message <> "" Then
            preError.Attributes.Remove("class")
            If bError Then
                preError.Attributes.Add("class", "alert alert-error")
            Else
                preError.Attributes.Add("class", "alert alert-success")
            End If
        Else
            preError.Attributes.Remove("class")
            preError.Attributes.Add("class", "invisible")
        End If
        preError.InnerHtml = Message
    End Sub
    Private Sub BindGrid(Optional ByVal MainDotId As Integer = 0)
        Try
            Dim dtNow As Date = Date.Now
            Dim lastDay As DateTime = New DateTime(Now.Year, Now.Month, 1)

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(10) As SqlParameter

            PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
            PARAM(1) = New SqlParameter("@FILTER", "ALL")
            PARAM(2) = New SqlParameter("@BSU_ID", ddlBSU.SelectedValue)
            PARAM(3) = New SqlParameter("@FROMDT", dtNow.AddDays(-dtNow.Day + 1).ToString("dd/MMM/yyyy"))
            PARAM(4) = New SqlParameter("@TODT", lastDay.AddMonths(1).AddDays(-1).ToString("dd/MMM/yyyy"))
            PARAM(5) = New SqlParameter("@DTYPE", 0)
            PARAM(6) = New SqlParameter("@SORT", 0)
            PARAM(7) = New SqlParameter("@DGROUP", 0)
            PARAM(8) = New SqlParameter("@LDAYS", 0)

            If ViewState("datamode") = "add" Then
                PARAM(9) = New SqlParameter("@MAIN_DOC_ID", 0)
                PARAM(10) = New SqlParameter("@MAIN_DOT_ID", MainDotId)
            Else
                PARAM(9) = New SqlParameter("@MAIN_DOC_ID", MainDotId)
                PARAM(10) = New SqlParameter("@MAIN_DOT_ID", 0)
            End If
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DOC_GETDOCLIST", PARAM)
            Dim dvNonTracking As DataView = New DataView(dsDetails.Tables(1))
            dvNonTracking.RowFilter = "TRACKING_DOC = 0"
            Dim dvTracking As DataView = New DataView(dsDetails.Tables(1))
            dvTracking.RowFilter = "TRACKING_DOC = 1"
            grdTracked.DataSource = dvTracking
            grdTracked.DataBind()

            grdNonTracked.DataSource = dvNonTracking
            grdNonTracked.DataBind()
            Dim resultCount As String, TotalRecords As String
            resultCount = "0"
            TotalRecords = "0"
            resultCount = dsDetails.Tables(0).Rows.Count.ToString()

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub Grid_Document_ItemCommand(sender As Object, e As GridCommandEventArgs)
        Try
            If e.CommandName = Telerik.Web.UI.RadGrid.ExportToExcelCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToWordCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToCsvCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToPdfCommandName Then
                'ConfigureExport()
            End If
        Catch
        End Try
    End Sub
    Sub BindLandLordName()
        Dim ds As New DataSet
        Dim str_sql As String = "SELECT LLM_ID, LLM_DESCR FROM dbo.LANDLORD_M WITH(NOLOCK) WHERE ISNULL(LLM_bDELETED,0) = 0 ORDER BY LLM_DESCR"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CLMConnectionString, CommandType.Text, str_sql)
        ddlLandlord.DataSource = ds.Tables(0)
        ddlLandlord.DataTextField = "LLM_DESCR"
        ddlLandlord.DataValueField = "LLM_ID"
        ddlLandlord.DataBind()
        ddlLandlord.SelectedIndex = -1
    End Sub
    Sub BindBSU()
        Dim Strsql As String = ""
        'Strsql = "SELECT * FROM [dbo].[GetAccessibleListForUser]( 'BSU','" & Session("sUsr_name") & "','" & Session("sBsuID") & "')  ORDER BY DESCR"
        If ViewState("datamode") = "view" Then
            Strsql = "SELECT * FROM [dbo].[GetAccessibleListForUser]( 'BSU','" & Session("sUsr_name") & "','" & Session("sBsuID") & "') where id in(select ID from dbo.fnSplitMe((select Dot_bsu_apply from doctype where dot_id=" & hdnIssuerId.Value & "),'|'))  ORDER BY DESCR"
        Else
            Strsql = "SELECT * FROM [dbo].[GetAccessibleListForUser]( 'BSU','" & Session("sUsr_name") & "','" & Session("sBsuID") & "') where id in(select ID from dbo.fnSplitMe((select Dot_bsu_apply from doctype where dot_id=" & ddlDocumentType.SelectedValue & "),'|'))  ORDER BY DESCR"
        End If


        fillDropdown(ddlBSU, Strsql, "descr", "ID", True)

    End Sub

    Sub BindDepartment()
        Dim StrDepartmentsql As String = ""
        StrDepartmentsql = "select DPT_ID ID ,DPT_DESCR DESCR from DEPARTMENT_M union all select 0,'----Select One-----' order by DPT_DESCR "
        fillDropdown(ddlDepartment, StrDepartmentsql, "DESCR", "ID", True)
    End Sub
    Sub BindDocIssueUserList()


        'Dim con1 As String = anger.connectionString
        Dim ds3 As New DataSet
        Dim param3(2) As SqlClient.SqlParameter
        param3(0) = New SqlClient.SqlParameter("@DOCTYPE", ddlDocumentType.SelectedValue)
        param3(1) = New SqlClient.SqlParameter("@SUBGROUP", ddlSubGroup.SelectedValue)
        param3(2) = New SqlClient.SqlParameter("@BSU_ID", ddlBSU.SelectedValue)
        ds3 = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GET_DOCISSUEUSERLIST", param3)
        Dim dv3 As New DataView(ds3.Tables(0))
        ddlDocIssUser.DataSource = dv3
        ddlDocIssUser.DataTextField = "DESCR"
        ddlDocIssUser.DataValueField = "ID"
        ddlDocIssUser.DataBind()
        ddlDocIssUser.SelectedValue = 0

        'fillDropdown(ddlDocIssUser, StrDepartmentsql, "DESCR", "ID", True)
    End Sub

    'Public Sub ConfigureExport()
    '    Grid_Document.ExportSettings.ExportOnlyData = True
    '    Grid_Document.ExportSettings.IgnorePaging = True
    'End Sub

    Protected Sub grdTracked_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles grdTracked.ItemDataBound
        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = e.Item
            If Not item.GetDataKeyValue("SM_COLOR") Is Nothing Then
                Dim grdItem As GridDataItem
                grdItem = e.Item
                e.Item.ForeColor = Color.FromName(item.GetDataKeyValue("SM_COLOR").ToString)
            End If
            Try
                Dim DOI_ID As Integer = IIf(Convert.ToString(item.GetDataKeyValue("DOI_ID")) = "", 0, item.GetDataKeyValue("DOI_ID"))
                Dim FILE_PATH As String = Convert.ToString(item.GetDataKeyValue("DOI_FILE_PATH"))
                Dim lbtnGrdtracking As LinkButton = CType(e.Item.FindControl("lbtnGrdtracking"), LinkButton)
                Dim MultiEnable As HiddenField = CType(e.Item.FindControl("MultipleEnable"), HiddenField)

                Dim CheckSubDoc As CheckBox = CType(e.Item.FindControl("CheckSubdoc"), CheckBox)
                Dim SubDocId As HiddenField = CType(e.Item.FindControl("hdn_SubDocID"), HiddenField)
                Dim LinkDocTypes As String = Mainclass.getDataValue("select isnull(doc_linked_sub_doc_ids,0) from docupload where doc_id  in('" & h_EntryId.Value & "')", "OASIS_CLMConnectionString")
                Dim deleteId() As String = LinkDocTypes.Split(",")

                For RowCount = 0 To deleteId.GetUpperBound(0)
                    If deleteId(RowCount) = SubDocId.Value Then
                        CheckSubDoc.Checked = True
                    End If

                Next
                If MultiEnable.Value = 1 Then
                    CheckSubDoc.Visible = True
                    CheckSubDoc.Enabled = True
                Else
                    CheckSubDoc.Visible = False
                End If
                lbtnGrdtracking.Visible = FILE_PATH <> ""
                lbtnGrdtracking.OnClientClick = "return showDocument('" & encr.Encrypt_SHA256(DOI_ID) & "','" & FILE_PATH & "')"
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub grdNonTracked_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles grdNonTracked.ItemDataBound
        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = e.Item
            If Not item.GetDataKeyValue("SM_COLOR") Is Nothing Then
                Dim grdItem As GridDataItem
                grdItem = e.Item
                e.Item.ForeColor = Color.FromName(item.GetDataKeyValue("SM_COLOR").ToString)
            End If
            Try
                Dim DOI_ID As Integer = IIf(Convert.ToString(item.GetDataKeyValue("DOI_ID")) = "", 0, item.GetDataKeyValue("DOI_ID"))
                Dim FILE_PATH As String = Convert.ToString(item.GetDataKeyValue("DOI_FILE_PATH"))
                Dim lbtnGrdnontracking As LinkButton = CType(e.Item.FindControl("lbtnGrdnontracking"), LinkButton)
                Dim btngrdFupload As Button = CType(e.Item.FindControl("btngrdFupload"), Button)
                lbtnGrdnontracking.Visible = FILE_PATH <> ""
                lbtnGrdnontracking.OnClientClick = "return showDocument('" & encr.Encrypt_SHA256(DOI_ID) & "','" & FILE_PATH & "')"
                ScriptManager1.RegisterPostBackControl(btngrdFupload)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Public Sub btngrdFupload_Click(sender As Object, e As EventArgs)
        ShowMessage("")
        Dim Parent As Object = sender.Parent.Parent
        If Not Parent Is Nothing AndAlso Parent.GetType() = GetType(GridDataItem) Then
            Dim item As GridDataItem = Parent
            Dim FUP As FileUpload = DirectCast(item.FindControl("fupGrdnontracking"), FileUpload)
            Dim btnViewFile As LinkButton = DirectCast(item.FindControl("lbtnGrdnontracking"), LinkButton)
            Dim hfGrdFilePath As HiddenField = DirectCast(item.FindControl("hfGrdFilePath"), HiddenField)
            Dim hfGrdFileName As HiddenField = DirectCast(item.FindControl("hfGrdFileName"), HiddenField)
            Dim hfGrdFileType As HiddenField = DirectCast(item.FindControl("hfGrdFileType"), HiddenField)
            Dim FilePath As String = FUP.PostedFile.FileName
            Dim Filename As String = FUP.FileName
            Dim FileContentType As String = getContentType(FilePath)
            Dim DOC_ID As Integer = item.GetDataKeyValue("Doc_ID")
            Dim DOI_ID As Integer = item.GetDataKeyValue("DOI_ID")
            Dim SavedSharepointpath As String = String.Empty
            Dim path As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile") & "CLM"
            Dim fileOK As Boolean = False
            Dim fileCopied As Boolean = False
            Try
                'will delete the file if exists
                If IO.File.Exists(path & "\" & Filename) Then
                    Dim fileToDelete As New FileInfo(path & "\" & Filename)
                    fileToDelete.Delete()
                    FUP.PostedFile.SaveAs(path & "\" & Filename)
                Else
                    FUP.PostedFile.SaveAs(path & "\" & Filename)
                    fileCopied = True
                End If
                FilePath = path & "\" & Filename
                '--------------------initializing sharepoint connection
                Dim objSPC As New SharepointClass
                'Dim cxt = objSPC.LoginToSharepoint(1)
                'added by Mahesh 29-Aug-2020
                Dim DptID As Integer = 0
                'DptID = Mainclass.getDataValue("select DOC_DPT_ID from DocImage with(nolock) inner join DocUpload with(nolock) on doc_id=Doi_Doc_id where doi_id=" & DOI_ID, "OASIS_CLMConnectionString")
                DptID = Mainclass.getDataValue("select dot_dep_id from DocImage with(nolock) inner join DocUpload with(nolock) on doc_id=Doi_Doc_id inner join DocType on dot_id=Doc_Dot_id where doi_id=" & DOI_ID, "OASIS_CLMConnectionString")
                Dim cxt = objSPC.LoginToSharepoint(DptID)





                Dim FileError As Boolean = False
                If cxt Is Nothing Then
                    Exit Sub
                End If
                '------------------------Uploading file to sharepoint-----------------------------
                Dim SharepointLibrary As String = Mainclass.getDataValue("select BSU_SHORTNAME from oasis..businessunit_m WITH(NOLOCK) where bsu_id='" & ddlBSU.SelectedValue & "'", "OASIS_CLMConnectionString")
                'SharepointLibrary = "Test"
                'commented on 04-Dec-2018
                Dim SharepointFolder As String = Mainclass.getDataValue("select ISNULL(DOT_SHAREPOINT_FOLDER,'') from DOCTYPE where DOT_ID='" & ddlDocumentType.SelectedValue & "'", "OASIS_CLMConnectionString")
                SharepointClass.UploadFile(cxt, SharepointLibrary, SharepointFolder, FilePath, SavedSharepointpath, SharepointLibrary, DOC_ID)

                If SavedSharepointpath <> "" Then
                    '---------------------------Deleting file from the temporary location on file server-------------------
                    If IO.File.Exists(FilePath) Then
                        Dim fileToDelete As New FileInfo(FilePath)
                        fileToDelete.Delete()
                    End If
                    '--------------------------updating file details to DB-------------------------
                    btnViewFile.Text = Filename
                    btnViewFile.Visible = True
                    Dim iParms(10) As SqlClient.SqlParameter
                    iParms(1) = New SqlClient.SqlParameter("@DOI_ID", SqlDbType.Int)
                    iParms(1).Value = DOI_ID
                    iParms(2) = New SqlClient.SqlParameter("@DOI_DOC_ID", SqlDbType.Int)
                    iParms(2).Value = DOC_ID
                    iParms(3) = New SqlClient.SqlParameter("@DOI_FILE_NAME", SqlDbType.VarChar, 100)
                    iParms(3).Value = Filename
                    iParms(4) = New SqlClient.SqlParameter("@DOI_CONTENT_TYPE", SqlDbType.VarChar, 100)
                    iParms(4).Value = FileContentType
                    iParms(5) = New SqlClient.SqlParameter("@DOI_LIBRARY", SqlDbType.VarChar, 100)
                    iParms(5).Value = SharepointLibrary
                    iParms(6) = New SqlClient.SqlParameter("@DOI_FOLDER", SqlDbType.VarChar, 100)
                    iParms(6).Value = SharepointFolder
                    iParms(7) = New SqlClient.SqlParameter("@DOI_UPLOAD_USER", SqlDbType.VarChar, 20)
                    iParms(7).Value = Session("sUsr_name")
                    iParms(8) = New SqlClient.SqlParameter("@DOI_TYPE", SqlDbType.VarChar, 20)
                    iParms(8).Value = ""
                    iParms(9) = New SqlClient.SqlParameter("@DOI_PATH", SqlDbType.VarChar, 1000)
                    iParms(9).Value = encr.Encrypt_SHA256(SavedSharepointpath)

                    iParms(10) = New SqlClient.SqlParameter("@DOI_IS_FULLVERSION", SqlDbType.Int)
                    iParms(10).Value = 0

                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_CLMConnectionString, CommandType.StoredProcedure, "saveDocImage", iParms)
                    BindGrid(ViewState("EntryId"))
                    ShowMessage("File uploaded successfully")
                Else
                    ShowMessage("File upload failed", True)
                End If
            Catch ex As Exception
                'lblError.Text = ex.Message
                ShowMessage("File upload failed with the error message " & ex.Message, True)
                Exit Sub
            End Try
        End If
    End Sub
    Protected Sub CausePostBack_Click(sender As Object, e As EventArgs) Handles CausePostBack.Click
        setModifyvalues(ViewState("EntryId"))
        SetDataMode("view")
        BindGrid(ViewState("EntryId"))
    End Sub
    Private Sub btnFullVersion_Click(sender As Object, e As EventArgs) Handles btnFullVersion.Click
        Dim FileError As Boolean = False
        If UploadDocFullversion.HasFile Then
            Dim UploadedPath As String = ""
            If UploadDocFullversion.FileName.Length > 0 Then
                h_SourceFolderPath_fullversion.Value = UploadDocFullversion.PostedFile.FileName
                h_sourceFileName_FullVersion.Value = UploadDocFullversion.FileName
                hdnDoi_File_Name_FV.Value = UploadDocFullversion.FileName
                hdnDoi_content_type_FV.Value = ContentType
                AddFullVersionDocumentItem()
            Else
                FileError = True
            End If

        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        Dim FileError As Boolean = False
        If UploadDocPhoto.HasFile Then
            Dim UploadedPath As String = ""
            Try
                If UploadDocPhoto.FileName.Length > 0 Then
                    h_SourceFolderPath.Value = UploadDocPhoto.PostedFile.FileName
                    h_sourceFileName.Value = UploadDocPhoto.FileName
                    hdnDoi_File_Name.Value = UploadDocPhoto.FileName
                    hdnDoi_content_type.Value = ContentType
                    AddDocumentItem()
                Else
                    FileError = True
                End If
            Catch ex As Exception
                UtilityObj.Errorlog("btnUpload_Click", "UploadDocument3")
            End Try

        End If
    End Sub
    Private Sub SaveFullversion(Optional ViewDet As Boolean = False)
        Try

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            If TransferFooterFullversion.Rows.Count >= 1 Then
                Dim RowCount As Integer
                For RowCount = 0 To TransferFooterFullversion.Rows.Count - 1
                    If TransferFooterFullversion.Rows(RowCount).RowState <> 8 Then
                        Dim Sharepointpath As String = ""
                        Dim objSPC As New SharepointClass
                        'Dim cxt = objSPC.LoginToSharepoint(1)
                        'Dim cxt = objSPC.LoginToFullVersionSharepoint(1)
                        'added by mahesh 29-Aug-2020

                        Dim DptID As Integer = 0
                        DptID = Mainclass.getDataValue("select dot_dep_id from doctype with(nolock) where dot_id='" & ddlDocumentType.SelectedValue & "'", "OASIS_CLMConnectionString")
                        Dim cxt = objSPC.LoginToFullVersionSharepoint(DptID)



                        Dim FileError As Boolean = False
                        If cxt Is Nothing Then
                            Exit Sub
                        End If
                        Dim BSUShort As String = Mainclass.getDataValue("select BSU_SHORTNAME from oasis..businessunit_m WITH(NOLOCK) where bsu_id='" & ddlBSU.SelectedValue & "'", "OASIS_CLMConnectionString")
                        Dim FolderName As String = Mainclass.getDataValue("select ISNULL(DOT_SHAREPOINT_FOLDER,'') from DOCTYPE where DOT_ID='" & ddlDocumentType.SelectedValue & "'", "OASIS_CLMConnectionString")
                        If (FolderName = "" Or BSUShort = "") Then
                            'lblError.Text = "error with SharePoint Library or Folder"
                            ShowMessage("error with SharePoint Library or Folder")
                            Exit Sub
                        End If

                        If TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath") <> "" Then
                            Try
                                UtilityObj.Errorlog("btnfvSave" & IO.File.Exists(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath")), "SharepointClass")
                                If IO.File.Exists(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath")) Then
                                    SharepointClass.UploadFile(cxt, BSUShort, FolderName, TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath"), Sharepointpath, BSUShort, ViewState("EntryId"))
                                Else
                                    ShowMessage("No file found to upload")
                                    stTrans.Rollback()
                                    Exit Sub
                                End If
                            Catch ex As Exception
                                ShowMessage("Error while upload to Sharepoint folder. Error: " + ex.Message)
                                stTrans.Rollback()
                                Exit Sub
                            End Try

                            If Sharepointpath = "" Then
                                ShowMessage("Error while upload to Sharepoint folder")
                                stTrans.Rollback()
                                Exit Sub
                            Else
                                If IO.File.Exists(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath")) Then
                                    Dim fileToDelete As New FileInfo(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath"))
                                    fileToDelete.Delete()
                                End If
                            End If
                            Dim iParms(10) As SqlClient.SqlParameter
                            iParms(1) = New SqlClient.SqlParameter("@DOI_ID", SqlDbType.Int)
                            iParms(1).Value = TransferFooterFullversion.Rows(RowCount)("doi_Id")
                            iParms(2) = New SqlClient.SqlParameter("@DOI_DOC_ID", SqlDbType.Int)
                            iParms(2).Value = ViewState("EntryId")
                            iParms(3) = New SqlClient.SqlParameter("@DOI_FILE_NAME", SqlDbType.VarChar, 100)
                            iParms(3).Value = TransferFooterFullversion.Rows(RowCount)("doi_file_name")
                            iParms(4) = New SqlClient.SqlParameter("@DOI_CONTENT_TYPE", SqlDbType.VarChar, 100)
                            iParms(4).Value = TransferFooterFullversion.Rows(RowCount)("doi_content_type")
                            iParms(5) = New SqlClient.SqlParameter("@DOI_LIBRARY", SqlDbType.VarChar, 100)
                            'iParms(5).Value = TransferFooterFullversion.Rows(RowCount)("doi_library")
                            iParms(5).Value = BSUShort
                            iParms(6) = New SqlClient.SqlParameter("@DOI_FOLDER", SqlDbType.VarChar, 100)
                            'iParms(6).Value = TransferFooterFullversion.Rows(RowCount)("doi_folder")
                            iParms(6).Value = FolderName
                            iParms(7) = New SqlClient.SqlParameter("@DOI_UPLOAD_USER", SqlDbType.VarChar, 20)
                            iParms(7).Value = Session("sUsr_name")
                            iParms(8) = New SqlClient.SqlParameter("@DOI_TYPE", SqlDbType.VarChar, 1000)
                            iParms(8).Value = TransferFooterFullversion.Rows(RowCount)("doi_Type")
                            iParms(9) = New SqlClient.SqlParameter("@DOI_PATH", SqlDbType.VarChar, 1000)
                            iParms(9).Value = encr.Encrypt_SHA256(Sharepointpath)

                            iParms(10) = New SqlClient.SqlParameter("@DOI_IS_FULLVERSION", SqlDbType.Int)
                            iParms(10).Value = 1

                            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveDocImage", iParms)


                            ShowMessage("File uploaded successfully", False)
                            stTrans.Commit()
                            SetDataMode("view")
                            setModifyvalues(ViewState("EntryId"))
                            HideButtons()
                        Else
                            ShowMessage("Error while uploaded ", False)
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub UPdateFullVersionDetails()
        Try

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction

            If TransferFooterFullversion.Rows.Count >= 1 Then
                Dim RowCount As Integer
                For RowCount = 0 To TransferFooterFullversion.Rows.Count - 1
                    If TransferFooterFullversion.Rows(RowCount).RowState <> 8 Then
                        Dim Sharepointpath As String = ""
                        Dim objSPC As New SharepointClass
                        'Dim cxt = objSPC.LoginToSharepoint(1)

                        'Dim cxt = objSPC.LoginToFullVersionSharepoint(1)
                        'added by Mahesh 29-Aug-2020
                        Dim DptID As Integer = 0
                        DptID = Mainclass.getDataValue("select dot_dep_id from doctype with(nolock) where dot_id='" & ddlDocumentType.SelectedValue & "'", "OASIS_CLMConnectionString")
                        Dim cxt = objSPC.LoginToFullVersionSharepoint(DptID)


                        Dim FileError As Boolean = False
                        If cxt Is Nothing Then
                            Exit Sub
                        End If
                        Dim BSUShort As String = Mainclass.getDataValue("select BSU_SHORTNAME from oasis..businessunit_m WITH(NOLOCK) where bsu_id='" & ddlBSU.SelectedValue & "'", "OASIS_CLMConnectionString")
                        Dim FolderName As String = Mainclass.getDataValue("select ISNULL(DOT_SHAREPOINT_FOLDER,'') from DOCTYPE where DOT_ID='" & ddlDocumentType.SelectedValue & "'", "OASIS_CLMConnectionString")
                        If (FolderName = "" Or BSUShort = "") Then
                            'lblError.Text = "error with SharePoint Library or Folder"
                            ShowMessage("error with SharePoint Library or Folder")
                            Exit Sub
                        End If

                        If TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath") <> "" Then
                            Try
                                UtilityObj.Errorlog("btnfvSave" & IO.File.Exists(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath")), "SharepointClass")
                                If IO.File.Exists(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath")) Then
                                    SharepointClass.UploadFile(cxt, BSUShort, FolderName, TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath"), Sharepointpath, BSUShort, ViewState("EntryId"))
                                Else
                                    ShowMessage("No file found to upload")
                                    stTrans.Rollback()
                                    Exit Sub
                                End If
                            Catch ex As Exception
                                ShowMessage("Error while upload to Sharepoint folder. Error: " + ex.Message)
                                stTrans.Rollback()
                                Exit Sub
                            End Try

                            If Sharepointpath = "" Then
                                ShowMessage("Error while upload to Sharepoint folder")
                                stTrans.Rollback()
                                Exit Sub
                            Else
                                If IO.File.Exists(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath")) Then
                                    Dim fileToDelete As New FileInfo(TransferFooterFullversion.Rows(RowCount)("DOI_Sourcepath"))
                                    fileToDelete.Delete()
                                End If
                            End If
                            Dim iParms(10) As SqlClient.SqlParameter
                            iParms(1) = New SqlClient.SqlParameter("@DOI_ID", SqlDbType.Int)
                            iParms(1).Value = TransferFooterFullversion.Rows(RowCount)("doi_Id")
                            iParms(2) = New SqlClient.SqlParameter("@DOI_DOC_ID", SqlDbType.Int)
                            iParms(2).Value = ViewState("EntryId")
                            iParms(3) = New SqlClient.SqlParameter("@DOI_FILE_NAME", SqlDbType.VarChar, 100)
                            iParms(3).Value = TransferFooterFullversion.Rows(RowCount)("doi_file_name")
                            iParms(4) = New SqlClient.SqlParameter("@DOI_CONTENT_TYPE", SqlDbType.VarChar, 100)
                            iParms(4).Value = TransferFooterFullversion.Rows(RowCount)("doi_content_type")
                            iParms(5) = New SqlClient.SqlParameter("@DOI_LIBRARY", SqlDbType.VarChar, 100)
                            'iParms(5).Value = TransferFooterFullversion.Rows(RowCount)("doi_library")
                            iParms(5).Value = BSUShort
                            iParms(6) = New SqlClient.SqlParameter("@DOI_FOLDER", SqlDbType.VarChar, 100)
                            'iParms(6).Value = TransferFooterFullversion.Rows(RowCount)("doi_folder")
                            iParms(6).Value = FolderName
                            iParms(7) = New SqlClient.SqlParameter("@DOI_UPLOAD_USER", SqlDbType.VarChar, 20)
                            iParms(7).Value = Session("sUsr_name")
                            iParms(8) = New SqlClient.SqlParameter("@DOI_TYPE", SqlDbType.VarChar, 1000)
                            iParms(8).Value = TransferFooterFullversion.Rows(RowCount)("doi_Type")
                            iParms(9) = New SqlClient.SqlParameter("@DOI_PATH", SqlDbType.VarChar, 1000)
                            iParms(9).Value = encr.Encrypt_SHA256(Sharepointpath)

                            iParms(10) = New SqlClient.SqlParameter("@DOI_IS_FULLVERSION", SqlDbType.Int)
                            iParms(10).Value = 1
                            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveDocImage", iParms)
                            'ShowMessage("File uploaded successfully", False)
                            stTrans.Commit()
                            'SetDataMode("view")
                            'setModifyvalues(ViewState("EntryId"))
                            'HideButtons()
                        Else
                            ShowMessage("Error while uploaded ", False)
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub btnfvSave_Click(sender As Object, e As EventArgs) Handles btnfvSave.Click

        SaveFullversion()
    End Sub
    Private Sub GRDFullVersion_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles GRDFullVersion.ItemDataBound
        Try
            Dim hdnDoi_Id As HiddenField = CType(e.Item.FindControl("hdnDoi_Id_FV"), HiddenField)
            Dim hdnDoi_content_type As HiddenField = CType(e.Item.FindControl("hdnDoi_content_type_FV"), HiddenField)
            Dim btnDelete As LinkButton = CType(e.Item.FindControl("btnDelete"), LinkButton)
            Dim hdnDoi_File_Name As HiddenField = CType(e.Item.FindControl("hdnDoi_File_Name_FV"), HiddenField)
            Dim hdnDoi_File_NameTemp As HiddenField = CType(e.Item.FindControl("hdnDoi_File_NameTemp_FV"), HiddenField)
            Dim btnDocumentLink As LinkButton = CType(e.Item.FindControl("btnDocumentLink_FV"), LinkButton)
            Dim hdnDoi_SharepointPath As HiddenField = CType(e.Item.FindControl("hdn_DOI_PATH_FV"), HiddenField)
            If Not hdnDoi_SharepointPath Is Nothing Then
                btnDocumentLink.Visible = (hdnDoi_SharepointPath.Value.Length > 0)
            End If
            btnDocumentLink.OnClientClick = "return showDocument('" & encr.Encrypt_SHA256(hdnDoi_Id.Value) & "','" & hdnDoi_SharepointPath.Value & "')"

        Catch ex As Exception

        End Try
    End Sub
    Private Sub EnableallChecks()
        For i As Integer = 0 To grdTracked.Items.Count - 1
            Dim CheckSubDoc As CheckBox = CType(grdTracked.Items(i).FindControl("CheckSubdoc"), CheckBox)
            'Dim isChecked As Boolean = (CType(grdTracked.Items(i).FindControl("CheckSubdoc"), CheckBox)).Checked
            CheckSubDoc.Checked = True
        Next
    End Sub
    Private Sub BindSubGroup(ByVal DOTID As Integer)
        Dim StrSubgrpSQL As String = ""
        'StrSubgrpSQL = "select DSG_ID ID,DSG_DESCR DESCR from DocSUBGroup where DSG_DDT_ID=" & DPTID & "  union all select 0,'----Select One-----' order by DSG_DESCR "
        StrSubgrpSQL = "select DSG_ID ID,DSG_DESCR DESCR from DocSUBGroup inner Join doctype On dot_dep_id=dsg_ddt_id where DOT_ID=" & DOTID & "  union all select 0,'----Select One-----' order by DSG_DESCR "

        fillDropdown(ddlSubGroup, StrSubgrpSQL, "DESCR", "ID", True)
    End Sub

End Class


