﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports System.IO
Imports UtilityObj
Imports ICSharpCode.SharpZipLib.Checksums
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.GZip
Partial Class DocTracker_ShowSubDocuments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString

    Private Sub DocTracker_ShowSubDocuments_Load(sender As Object, e As EventArgs) Handles Me.Load
        h_View_id.Value = Request.QueryString("viewid")


        BindGrid(h_View_id.Value)
        BindNontrackedDoclist(h_View_id.Value)


    End Sub
    Private Sub BindGrid(ByVal docid As Integer)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(10) As SqlParameter

            PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
            PARAM(1) = New SqlParameter("@FILTER", "ALL")
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(3) = New SqlParameter("@FROMDT", "1900-01-01")
            PARAM(4) = New SqlParameter("@TODT", "1900-01-01")
            PARAM(5) = New SqlParameter("@DTYPE", 0)
            PARAM(6) = New SqlParameter("@SORT", 0)
            PARAM(7) = New SqlParameter("@DGROUP", 0)
            PARAM(8) = New SqlParameter("@LDAYS", 0)
            PARAM(9) = New SqlParameter("@MAIN_DOC_ID", docid)
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DOC_GETDOCLIST", PARAM)
            grdSubDoc.DataSource = dsDetails.Tables(1)
            grdSubDoc.DataBind()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindNontrackedDoclist(ByVal docid As Integer)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
        Dim NonTrackingPARAM(1) As SqlParameter

        NonTrackingPARAM(0) = New SqlParameter("@DOC_ID", docid)
        Dim dsTrackingDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "select Dot_Descr NonTrackingDocument from doctype inner join doctype_sub on DTS_SUB_DOT_ID=dot_id inner join docupload on Doc_Dot_id=DTS_DOT_ID where doc_id=" & docid)
        GrdNonTracking.DataSource = dsTrackingDetails.Tables(0)
        GrdNonTracking.DataBind()

    End Sub
End Class
