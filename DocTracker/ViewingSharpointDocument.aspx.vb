﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class DocTracker_ViewingSharpointDocument
    Inherits System.Web.UI.Page
    Dim encr As New SHA256EncrDecr
    Private Sub DocTracker_ViewingSharpointDocument_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If

        Dim Path As String = Request.QueryString("path")
        Dim objSPC As New SharepointClass


        'Dim cxt = objSPC.LoginToSharepoint(1)
        'added on 29-Aug-2020
        Dim DptID As Integer = 0
        'DptID = Mainclass.getDataValue("select DOC_DPT_ID from DocImage with(nolock) inner join DocUpload with(nolock) on doc_id=Doi_Doc_id where doi_id=" & encr.Decrypt_SHA256(Request.QueryString("id")), "OASIS_CLMConnectionString")
        DptID = Mainclass.getDataValue("select dot_dep_id from DocImage with(nolock) inner join DocUpload with(nolock) on doc_id=Doi_Doc_id inner join DocType with(nolock) on dot_id=Doc_Dot_id where doi_id=" & encr.Decrypt_SHA256(Request.QueryString("id")), "OASIS_CLMConnectionString")
        Dim cxt = objSPC.LoginToSharepoint(DptID)
        Dim FileExists As Boolean = False

        If Path <> "" Then
            WritetoAudit()
            FileExists = SharepointClass.CheckFileExists(cxt, encr.Decrypt_SHA256(Path))
            If FileExists = True Then
                'Response.Redirect("~/Common/SharepointFilehandler.ashx?Path=" & Path)
                'added on 29-Aug-2020
                Response.Redirect("~/Common/SharepointFilehandler.ashx?Path=" & Path & "&DOIID=" & encr.Decrypt_SHA256(Request.QueryString("id")))

            End If
        End If
    End Sub
    Private Sub WritetoAudit()
        Using connection As SqlConnection = ConnectionManger.GetOASIS_CLMConnection
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@DAT_USER", HttpContext.Current.Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@DAT_DOC_ID", encr.Decrypt_SHA256(Request.QueryString("ID")))
            pParms(2) = New SqlClient.SqlParameter("@DAT_ACTION", "DOWNLOAD")
            pParms(3) = New SqlClient.SqlParameter("@DAT_REMARKS", encr.Decrypt_SHA256(Request.QueryString("path")))
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "dbo.SaveCLM_Audit", pParms)
        End Using

    End Sub
End Class
