﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowSubDocuments.aspx.vb" Inherits="DocTracker_ShowSubDocuments" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    <link href="css/enroll.css" rel="stylesheet" type="text/css" />
    <link href="css/gems.css" rel="stylesheet" />
    <link href="css/buttons.css" rel="stylesheet" />
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    

    </head>
 <body>
    
    <form id="bLOCKFORM" runat="server">   
        <%--<div>--%>


        <script type="text/javascript" language="javascript">
        
             function fancyClose() {
           
            parent.$.fancybox.close();

            }
            </script>

             <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
                    </ajaxToolkit:ToolkitScriptManager>

    

    

<tr>
                    <td  style="height: 166px" valign="top" align="left">
                        <table align="left" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" style="width: 100%">
                            <tr class="subheader_img">
                                <td align="center" colspan="6" style="height: 10px  ; background-color:#00a3e0; color:white"  width="100%" valign="middle">
                                    <div align="center">Dependancy Tracked Document list</div>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="8" align="center" class="matters">
                                    <asp:DataGrid ID="grdSubDoc" runat="server" AutoGenerateColumns="False" Width="99%"
                                        CaptionAlign="Top" PageSize="15" SkinID="GridViewView" CssClass="BlueTable">
                                        <HeaderStyle CssClass="gridheader"   ></HeaderStyle>
                                        <AlternatingItemStyle CssClass="griditem_alternative "></AlternatingItemStyle>
                                        <ItemStyle CssClass="griditem"></ItemStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Document ID" Visible ="false" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocid" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "DocumentId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="BSU"  >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBSU" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "BSU") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Business Unit" Visible ="false" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBusinessUnit" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "BusinessUnit") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Document Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentType" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "DocumentType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Document Issues">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocIssuer" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "DocumentIssuer") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            
                                            <asp:TemplateColumn HeaderText="Document No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocNo" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "DocumentNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn HeaderText="Issue Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIssueDate" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "IssueDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn HeaderText="Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExpDate" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "ExpiryDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            
                                            <asp:TemplateColumn HeaderText="Deatails">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDetails" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "Details") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                             <asp:TemplateColumn HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" Width="125px" Text='<%# DataBinder.Eval(Container.DataItem, "DSM_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                        </Columns>

                                    </asp:DataGrid>
                                </td>
                        </tr>




                            <tr class="subheader_img">
                                <td align="center" colspan="6" style="height: 10px  ; background-color:#00a3e0; color:white"  width="100%" valign="middle">
                                    <div align="center">Non Tracked Document list</div>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="8" align="center" class="matters">
                                    <asp:DataGrid ID="GrdNonTracking" runat="server" AutoGenerateColumns="False" Width="90%"
                                        CaptionAlign="Top" PageSize="15" SkinID="GridViewView" CssClass="BlueTable">
                                        <HeaderStyle CssClass="gridheader"   ></HeaderStyle>
                                        <AlternatingItemStyle CssClass="griditem_alternative "></AlternatingItemStyle>
                                        <ItemStyle CssClass="griditem"></ItemStyle>
                                        <Columns>
                                            

                                             <asp:TemplateColumn HeaderText="Document Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" Width="400px" Text='<%# DataBinder.Eval(Container.DataItem, "NonTrackingDocument") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                        </Columns>

                                    </asp:DataGrid>
                                </td>
                        </tr>






        <asp:HiddenField ID="h_View_id" runat="server" />


                          

                            
                          

                           
                            
                            </table>
                    </td>
                </tr>

    </form>

    

</body> </html>