<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ListReportView_DM.aspx.vb" Inherits="DOCTRACLER_ListReportView_DM" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        function ajaxToolkit_CalendarExtenderClientShowing(e) {
            if (!e.get_selectedDate() || !e.get_element().value)
                e._selectedDate = (new Date()).getDateOnly();
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }


        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
            }
            else if (pId == 4) {
                document.getElementById("<%=getid("mnu_4_img") %>").src = path;
            }
            else if (pId == 5) {
                document.getElementById("<%=getid("mnu_5_img") %>").src = path;
            }
            else if (pId == 6) {
                document.getElementById("<%=getid("mnu_6_img") %>").src = path;
            }
            else if (pId == 7) {
                document.getElementById("<%=getid("mnu_7_img") %>").src = path;
            }
            else if (pId == 8) {
                document.getElementById("<%=getid("mnu_8_img") %>").src = path;
            }
            else if (pId == 9) {
                document.getElementById("<%=getid("mnu_9_img") %>").src = path;
            }
            else if (pId == 10) {
                document.getElementById("<%=getid("mnu_10_img") %>").src = path;
            }
    document.getElementById(ctrl1).value = val + '__' + path;
}

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  <asp:Label ID="lblTopTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td align="left" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                   <%-- <tr valign="top">
                        <td align="left" colspan="2">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>

                        </td>
                    </tr>--%>
                    <tr valign="top">
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkSelection1" runat="server" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSelection2" runat="server" AutoPostBack="True" Height="16px" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad4" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad5" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad6" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td visible="false" runat="server" id="dateCol" align="left">
                            <asp:Label ID="lblDate" runat="server" Text="Expiry As on Date" CssClass="field-label"></asp:Label>
                            <asp:TextBox ID="txtDate" runat="server" CssClass="inputbox" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDate" PopupButtonID="lnkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                    <tr  valign="top">
                        <th align="left" valign="middle" width="70%">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink> <asp:Label ID="lblTitle" runat="server" Visible="false"></asp:Label>
                            <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="True" DataSourceID="namesXML" Visible="false"
                                DataTextField="FilterText" DataValueField="FilterValue" SkinID="DropDownListNormal">
                            </asp:DropDownList><asp:XmlDataSource ID="namesXML" runat="server" DataFile="~/XMLData/XMLTopFilter.xml"
                                XPath="FilterElements/FiletrList"></asp:XmlDataSource>
                        </th>
                        <th align="right" valign="middle" style="text-align: right" width="30%">
                            <asp:LinkButton ID="btnPrint" runat="server" Visible="False">Print</asp:LinkButton>
                            &nbsp;
                <asp:HyperLink ID="btnExport" runat="server">Export</asp:HyperLink>
                        </th>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="matters" width="100%" colspan="2">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol1" runat="server" EnableViewState="True" Text="Col1"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol1" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol2" runat="server" EnableViewState="True" Text="Col2"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol2" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol3" runat="server" EnableViewState="True" Text="Col3"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol3" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol4" runat="server" EnableViewState="True" Text="Col4"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol4" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol5" runat="server" EnableViewState="True" Text="Col5"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol5" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol6" runat="server" EnableViewState="True" Text="Col6"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol6" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col7" SortExpression="Col7">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol7" runat="server" Text='<%# Bind("Col7") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol7" runat="server" EnableViewState="True" Text="Col7"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol7" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch7" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col8" SortExpression="Col8">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol8" runat="server" Text='<%# Bind("Col8") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol8" runat="server" EnableViewState="True" Text="Col8"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol8" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch8" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col9" SortExpression="Col9">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol9" runat="server" Text='<%# Bind("Col9") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol9" runat="server" EnableViewState="True" Text="Col9"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol9" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch9" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col10" SortExpression="Col10">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol10" runat="server" Text='<%# Bind("Col10") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblHeaderCol10" runat="server" EnableViewState="True" Text="Col10"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol10" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch10" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                            Text="Select All" Visible="False" />
                                    </td>
                                    <td align="center" width="60%">
                                        <asp:Button ID="ListButton1" runat="server" CssClass="button" CausesValidation="False" OnClick="ListButton1_Click" Visible="False"></asp:Button>
                                        <asp:Button ID="ListButton2" runat="server" CssClass="button" CausesValidation="False" OnClick="ListButton2_Click" Visible="False"></asp:Button>
                                    </td>
                                    <td width="20%">&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>



                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_8" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_9" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_10" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="0" />


            </div>
        </div>
    </div>


</asp:Content>
