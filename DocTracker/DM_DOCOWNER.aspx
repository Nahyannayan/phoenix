﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_DOCOWNER.aspx.vb" Inherits="DOCTRACKER_DM_DOCOWNER" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/Popup.css" rel="stylesheet" />

    <style type="text/css">
        .gridheader_new {
            border-style: none;
            border-color: inherit;
            border-width: 0;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            background-image: url('../../Images/GRIDHEAD.gif');
            background-repeat: repeat-x;
            font-size: 11px;
            font-weight: bold;
            color: #1b80b6;
        }

        .inputbox {
            BACKGROUND: F5FED2;
            BORDER-BOTTOM: #1B80B6 1px solid;
            BORDER-LEFT: #1B80B6 1px solid;
            BORDER-RIGHT: #1B80B6 1px solid;
            BORDER-TOP: #1B80B6 1px solid;
            COLOR: #555555;
            CURSOR: text;
            FONT-FAMILY: verdana;
            FONT-SIZE: 11px;
            WIDTH: 199px;
            HEIGHT: 14px;
            TEXT-DECORATION: none;
        }

        .button {
        }
    </style>

    <script language="javascript" type="text/javascript">

        function getDOCEmpDesignations() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var SchoolorCorpt;
            SchoolorCorpt = document.getElementById("<%=h_BSUorSCHOOL.ClientID %>").value
            pMode = "DOCEMPDESIG";
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&SchoolORCorpt=" + SchoolorCorpt;
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
        }

        






       <%-- function getFullDocUploader(DFUType, DFUDESCR, DFUDOMID, OwnerDrop) {
            var sFeatures;

            var SelText;
            var pMode;
            var NameandCode;

            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "DOCOWNER_EMP_DES_LIST"

            document.getElementById('<%=hfDFUTypeid.ClientID%>').value = DFUType;
            document.getElementById('<%=hfDFUDESCRid.ClientID%>').value = DFUDESCR;
            document.getElementById('<%=hfDFUDOMID.ClientID%>').value = DFUDOMID;
            document.getElementById('<%=hfOwnerDropid.ClientID%>').value = OwnerDrop;
                        SelText = document.getElementById(OwnerDrop).value
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&OwnerType=" + SelText;
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
        }--%>


        function getFullDocUploader(DFUType, DFUDESCR, DFUDOMID, OwnerDrop) {
            var pMode;
            var NameandCode;
            var SelText;
            pMode = "DOCOWNER_EMP_DES_LIST"
            document.getElementById('<%=hf_1.ClientID%>').value = DFUType;
            document.getElementById('<%=hf_2.ClientID%>').value = DFUDESCR;
            document.getElementById('<%=hf_3.ClientID%>').value = DFUDOMID;
            document.getElementById('<%=hf_4.ClientID%>').value = OwnerDrop;



            SelText = document.getElementById(OwnerDrop).value
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&OwnerType=" + SelText;
            var oWnd = radopen(url, "pop_DEDL");
        }
        function OnClientCloseDEDL(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[3];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hf_3.ClientID%>").value).value = NameandCode[0];
                


                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
                



                //document.getElementById(DFUType).value = NameandCode[3];
                //document.getElementById(DFUDESCR).value = NameandCode[1];
                //document.getElementById(DFUDOMID).value = NameandCode[0];

                //document.getElementById(DFUType).disabled = true;
                //document.getElementById(DFUDESCR).disabled = true;

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }





        function setProduct(result) {
            //alert(1);
            NameandCode = result.split('___');
            str = document.getElementById('<%=hfDFUTypeid.ClientID%>').value
            str2 = document.getElementById('<%=hfDFUDESCRid.ClientID%>').value
            str3 = document.getElementById('<%=hfDFUDOMID.ClientID%>').value
            str4 = document.getElementById('<%=hfOwnerDropid.ClientID%>').value
            //document.getElementById(DOMDESCR).value = NameandCode[1];
            document.getElementById(document.getElementById('<%=hfDFUTypeid.ClientID%>').value).value = NameandCode[3];
            document.getElementById(document.getElementById('<%=hfDFUDESCRid.ClientID%>').value).value = NameandCode[1];
            document.getElementById(document.getElementById('<%=hfDFUDOMID.ClientID%>').value).value = NameandCode[0];

            document.getElementById(document.getElementById('<%=hfDFUTypeid.ClientID%>').value).disabled = true;
            document.getElementById(document.getElementById('<%=hfDFUDESCRid.ClientID%>').value).disabled = true;
            //alert(NameandCode[1]);
            //document.getElementById(DOMID).value = NameandCode[0];
            //document.getElementById(DOMDESCR).disabled = true;
            CloseFrame();
            //return false;
        }

        function setProduct3(result) {
            NameandCode = result.split('___');
            document.getElementById("<%=txtProfileID.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=txtProfileName.ClientID %>").value = NameandCode[2];
            document.getElementById("<%=h_Profile_ID.ClientID %>").value = NameandCode[0];
            CloseFrame();
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Document Roles Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Document Roles\Users Master
                        </td>
                    </tr>
                    <tr>
                        <td align="left">

                            <table align="left" width="100%">


                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="Tr2" runat="server">
                                    <td align="left"><span class="field-label">Description</span></td>

                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkViewOnly" runat="server" Text='ViewOnly' AutoPostBack="true" />
                                    </td>
                                </tr>

                                <tr id="selectOwner" runat="server" visible="false">
                                    <td align="left" width="20%"><span class="field-label">Owner Type</span></td>

                                    <td align="left" width="30%">
                                        <asp:RadioButtonList ID="rblOwnerType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="0">Designation</asp:ListItem>
                                            <asp:ListItem Value="1">Employee</asp:ListItem>

                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>





                                <tr id="OwnerSel" runat="server" visible="false">


                                    <td align="left">
                                        <asp:Label ID="lblCaption" Text="Owner Name" runat="server" CssClass="field-label"></asp:Label></td>


                                    <td align="left">
                                        <asp:TextBox ID="txtProfileID" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtProfileName" runat="server" AutoPostBack="true" align="left"></asp:TextBox>
                                        <asp:ImageButton ID="ImageDriver" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getDOCEmpDesignations();return true;" />

                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>

                                </tr>


                                <tr id="Tr1" runat="server">
                                    <td align="left"><span class="field-label">Type</span></td>

                                    <td align="left">
                                        <asp:RadioButtonList ID="rbType" runat="server" RepeatDirection="Horizontal"  AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="ALL">All Units</asp:ListItem>
                                            <asp:ListItem Value="OWN">Respective Unit</asp:ListItem>
                                            <asp:ListItem Value="ASSIGNED">Assigned Units</asp:ListItem>

                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAssignUserList" Visible="false" AutoPostBack="true" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                


                                <tr>



                                    <td colspan="4">
                                        <asp:Panel ID="PnlDocRoles" runat="server">

                                            <asp:GridView ID="grdDocOwner" runat="server" AutoGenerateColumns="False" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="10"
                                                CssClass="table table-bordered table-row" DataKeyNames="DOMF_ID" ShowFooter="True">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="EMP ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDOMF_ID" runat="server" Text='<%# Bind("DOMF_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Owner Type" ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPType" runat="server" Style="text-align: left" Width="120px"
                                                                Text='<%# Bind("STYPE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlOwnerType" runat="server" Width="120px"></asp:DropDownList>
                                                            <asp:HiddenField ID="hdnEMP_TYPE" Value='<%# Bind("STYPE") %>' runat="server" />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlOwnerType" runat="server" Width="120px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Owner Type" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDOMF_type" Width="100px" Enabled="false" runat="server" Text='<%# Bind("STYPE") %>'></asp:Label>
                                                        </ItemTemplate>

                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtDOMF_type" Width="100px" Enabled="false" runat="server" Text='<%# Bind("STYPE") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtDOMF_type" Width="100px" Enabled="false" runat="server"></asp:TextBox>
                                                        </FooterTemplate>

                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Owner Name" ItemStyle-Width="40%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDOMF_Descr" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="h_DOMF_DOM_ID" runat="server" Value='<%# Bind("DOMF_DOM_ID") %>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate>

                                                            <asp:TextBox ID="txtDOMF_Descr" Width="250px" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                            <asp:HiddenField ID="h_DOMF_DOM_ID" runat="server" Value='<%# Bind("DOMF_DOM_ID") %>' />
                                                            <asp:ImageButton ID="imgDocOwner_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />

                                                        </EditItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtDOMF_Descr" Width="250px" Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_DOMF_DOM_ID" runat="server" Value='<%# Bind("DOMF_DOM_ID") %>' />
                                                            <asp:ImageButton ID="imgDocOwner" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </FooterTemplate>

                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateDOL" runat="server" CausesValidation="True" CommandName="Update"
                                                                Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelDOL" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddDOL" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditDOL" runat="server" CausesValidation="False" CommandName="Edit"
                                                                Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />


                                                </Columns>

                                                <HeaderStyle />

                                            </asp:GridView>
                                        </asp:Panel>

                                    </td>


                                </tr>








                                <tr>

                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                                        <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />





                                    </td>
                                </tr>

                                <tr>

                                    <td align="left" colspan="4">

                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>


                <asp:HiddenField ID="Hiddensubtabids" runat="server" />
                <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
                <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
                <asp:HiddenField ID="HiddenType" runat="server" />
                <asp:HiddenField ID="h_Profile_ID" runat="server" />
                <asp:HiddenField ID="h_BSUorSCHOOL" runat="server" />
                <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                <asp:HiddenField ID="h_DOC_OWNER_DELETED" runat="server" />



            </div>
        </div>
    </div>
    <asp:HiddenField ID="h_BSUID" runat="server" />

    <asp:HiddenField ID="hfDFUTypeid" runat="server" Value="0" />
    <asp:HiddenField ID="hfDFUDESCRid" runat="server" Value="0" />
    <asp:HiddenField ID="hfDFUDOMID" runat="server" Value="0" />
    <asp:HiddenField ID="hfOwnerDropid" runat="server" Value="0" />

    <asp:HiddenField ID="hf_1" runat="server" />
    <asp:HiddenField ID="hf_2" runat="server" />
    <asp:HiddenField ID="hf_3" runat="server" />
    <asp:HiddenField ID="hf_4" runat="server" />

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>

