﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_DOCGROUP.aspx.vb" Inherits="DocTracker_DM_DOCGROUP" Title="Add/Edit Document Group" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Document Group Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="subheader_img">
                                    <td align="center" colspan="3" valign="middle">
                                        <div align="center">Document Group Master</div>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDocGroup" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Short Name</span></td>
                                    <td align="left" width="30%"><asp:TextBox ID="txtShortName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr id="city" runat ="server" visible ="false"  >
                                    <td align="left"><span class="field-label">City\Country</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlDocumentCity" runat="server"></asp:DropDownList>

                                    </td>
                                    <td align="left"></td>

                                    <td align="left">
                                        
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Details</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtDetails" runat="server"
                                            TabIndex="160" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
        </div>
    </div>

</asp:Content>

