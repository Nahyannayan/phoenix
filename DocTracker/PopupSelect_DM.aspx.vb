Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class DocTracker_PopupSelectDM
    Inherits BasePage
    Dim SearchMode As String
    'Version            Author          Date            Purpose
    ' 1.1               Shakeel          04/Sep/2011      Pop Up to select record
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property HideFirstColumn() As Boolean
        Get
            Return ViewState("HideFirstColumn")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideFirstColumn") = value
        End Set
    End Property
    Private Property MultiSelect() As Boolean
        Get
            Return ViewState("MultiSelect")
        End Get
        Set(ByVal value As Boolean)
            ViewState("MultiSelect") = value
        End Set
    End Property
    Private Property GridListData() As DataTable
        Get
            Return ViewState("GridListData")
        End Get
        Set(ByVal value As DataTable)
            ViewState("GridListData") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property IsDataTableAsList() As Boolean
        Get
            Return ViewState("IsDataTableAsList")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsDataTableAsList") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Page.DataBind()
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/like.gif"
                HideFirstColumn = True
                If Not Request.QueryString("MultiSelect") Is Nothing Then
                    If Request.QueryString("MultiSelect") = "1" Or Request.QueryString("MultiSelect") = "true" Then
                        MultiSelect = True
                    Else
                        MultiSelect = False
                    End If
                Else
                    MultiSelect = False
                End If
                IsStoredProcedure = False
                IsDataTableAsList = False
                gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure And Not IsDataTableAsList Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetSQLAndConnectionString(ByRef StrSQL As String, ByRef strConn As String, ByRef StrSortCol As String)
        Try
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            Select Case SearchMode


                Case "DOCEMPDESIG"
                    IsStoredProcedure = True
                    Dim EMPorDESIG As String
                    StrSQL = "[GetDOC_EMPDESIGNATION_LIST]"
                    EMPorDESIG = Request.QueryString("SchoolORCorpt").ToString
                    ReDim SPParam(1)
                    SPParam(0) = Mainclass.CreateSqlParameter("@EMPORDESIG", EMPorDESIG, SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASIS_CLMConnectionString

                    StrSortCol = "[DESCR] "
                    Page.Title = "Owner List"

                Case "APPROVERLIST"

                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim chkWhere As String = " 1=1"
                    StrSQL = "Select ID,DESCR from (Select  ID , DESCR from VW_DOC_DES_LIST) a where " & chkWhere
                    strConn = ConnectionManger.GetOASIS_CLMConnectionString
                    StrSortCol = "[ID]"
                    Page.Title = "Approvers List"
                Case "UPLOADFULLDOCLIST"

                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim chkWhere As String = " 1=1"
                    StrSQL = "Select ID,DESCR from (Select  ID , DESCR from VW_DOC_DES_LIST ) a where " & chkWhere
                    strConn = ConnectionManger.GetOASIS_CLMConnectionString
                    StrSortCol = "[ID]"
                    Page.Title = "Full Document Uploader List"

                Case "SubDoc"
                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim chkWhere As String = " 1=1"
                    Dim EnterDoctype As String = Request.QueryString("Doctype").ToString
                    StrSQL = "select ID,DESCR from (select dot_id ID,dot_descr DESCR,dot_id [Owner id]  from doctype where dot_descr not in('" & EnterDoctype & "')) a where " & chkWhere
                    strConn = ConnectionManger.GetOASIS_CLMConnectionString
                    StrSortCol = "[ID]"
                    Page.Title = "Sub Document List"
                Case "DOCOWNER_EMP_DES_LIST"

                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim chkWhere As String = " 1=1"
                    Dim OwnerType As String = Request.QueryString("OwnerType").ToString
                    If OwnerType = "EMP" Then
                        StrSQL = "select top 100 ID,DESCR,BSU,OWNTYPE,DESIGNATION from (select  ID,DESCR,BSU,OWNTYPE,DESIGNATION from VW_OWNER_EMP_LIST where owntype='EMPLOYEE') a where " & chkWhere
                    ElseIf OwnerType = "DESG" Then
                        StrSQL = "select top 100 ID,DESCR,BSU,OWNTYPE from (select  ID,DESCR,BSU,OWNTYPE from VW_OWNER_EMP_LIST where owntype='DESIGNATION') a where " & chkWhere
                    End If

                    strConn = ConnectionManger.GetOASIS_CLMConnectionString
                    StrSortCol = "[ID]"
                    Page.Title = "Sub Document List"

                Case "GetEmpBSUListNEW"


                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim chkWhere As String = " 1=1"
                    Dim SearchQry As String = Request.QueryString("EmpBsu").ToString
                    If SearchQry = "EMP" Then
                        StrSQL = "select top 100 ID,EMP_NO,DESCR,BSU from (select ID,EMP_NO,DESCR,BSU from VW_EMPLOYEE_ASSIGN) a where " & chkWhere

                    Else
                        StrSQL = "SELECT ID,BSU, DESCR FROM (select BSU_ID ID ,BSU_SHORTNAME BSU,BSU_NAME DESCR from oasis..BUSINESSUNIT_M with(nolock) )B where " & chkWhere
                    End If


                    If SearchQry = "EMP" Then
                        Page.Title = "Employee List"
                    Else
                        Page.Title = "Businessunit List"
                    End If

                    strConn = ConnectionManger.GetOASIS_CLMConnectionString
                    StrSortCol = "[ID]"
                    Page.Title = "[DESCR]"



                Case "GetEmpBSUList"
                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim chkWhere As String = " 1=1"
                    Dim SearchQry As String = Request.QueryString("EmpBsu").ToString
                    If SearchQry = "EMP" Then
                        StrSQL = "select top 100 ID,EMP_NO,DESCR,BSU from (select   EMP_ID ID,empno EMP_NO ,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') as DESCR,BSU_SHORTNAME BSU  from oasis..EMPLOYEE_M  with(nolock) inner join oasis..BUSINESSUNIT_M with (nolock) on bsu_id=emp_bsu_id where isnull(EMP_bACTIVE,0)=1 and BSU_COUNTRY_ID=172) a where " & chkWhere
                        'ElseIf SearchQry = "DESG" Then
                    Else
                        StrSQL = "SELECT ID,BSU, DESCR FROM (select BSU_ID ID ,BSU_SHORTNAME BSU,BSU_NAME DESCR from oasis..BUSINESSUNIT_M with(nolock) )B where " & chkWhere
                    End If
                    If SearchQry = "EMP" Then
                        Page.Title = "Employee List"
                    Else
                        Page.Title = "Businessunit List"
                    End If
                    strConn = ConnectionManger.GetOASIS_CLMConnectionString
                    StrSortCol = "[ID]"
                    Page.Title = "[DESCR]"
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function getLikeWhereQuerry(ByVal ColumnName As String, ByVal LikeStr As String) As String
        Dim whrQuery As String = ""
        Dim Arr() As String
        Arr = LikeStr.Split("|")
        Dim i As Int16
        For i = 0 To Arr.Length - 1
            whrQuery &= IIf(whrQuery <> "", " Or ", "") & "',' + cast(" & ColumnName & " as varchar) + ',' LIKE '%" & Arr(i) & "%'"
        Next
        If whrQuery = "" Then
            whrQuery = " (1 = 1) "
        Else
            whrQuery = " ( " & whrQuery & " ) "
        End If
        getLikeWhereQuerry = whrQuery
    End Function
    Private Sub gridbind()
        Try
            Dim GridData As New DataTable
            Dim str_conn As String = ""
            Dim str_Sql As String = ""
            Dim str_SortCol As String = ""
            SetSQLAndConnectionString(str_Sql, str_conn, str_SortCol)
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            If gvDetails.Rows.Count > 0 Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If str_SortCol <> "" Then
                OrderByStr = "order by " & str_SortCol
            Else
                OrderByStr = "order by  1"
            End If
            If Not IsStoredProcedure And Not IsDataTableAsList Then
                GridData = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_Filter & OrderByStr).Tables(0)
            ElseIf IsDataTableAsList Or IsStoredProcedure Then
                If IsStoredProcedure Then
                    GridData = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_Sql, CType(SPParam, SqlParameter())).Tables(0)
                    GridListData = GridData
                Else
                    GridData = GridListData
                End If
                str_Filter = " 1 = 1 " & str_Filter
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridListData.Clone
                Dim FilterCol As String
                If str_SortCol <> "" Then
                    FilterCol = str_SortCol
                Else
                    FilterCol = GridListData.Columns(0).ColumnName
                End If
                For Each mRow In GridListData.Select(str_Filter, FilterCol)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If


            Dim bindDT As New DataTable
            bindDT = GridData.Copy
            ViewState("GridTable") = GridData
            Dim HeaderLabel As New Label
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
            Next
            If bindDT.Columns.Count < 10 Then
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If

            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex

            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If
            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            gvDetails.Columns(1).Visible = Not HideFirstColumn
            gvDetails.Columns(0).Visible = MultiSelect
            btnSelect.Visible = MultiSelect
            chkSelAll.Visible = MultiSelect
            'txtSearch = gvDetails.HeaderRow.FindControl("txtName")
            'txtSearch.Text = str_txtName
            SetHeaderFilterString()
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub LinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If MultiSelect Then Exit Sub
        Dim lnk1 As New LinkButton
        Dim lnk2 As New LinkButton
        Dim lnk3 As New LinkButton
        Dim lnk4 As New LinkButton
        Dim lnk5 As New LinkButton
        Dim lnk6 As New LinkButton
        Dim lnk7 As New LinkButton
        Dim lnk8 As New LinkButton
        Dim lnk9 As New LinkButton
        Dim lnk10 As New LinkButton

        lnk1 = sender.Parent.FindControl("Lnk1")
        lnk2 = sender.Parent.FindControl("Lnk2")
        lnk3 = sender.Parent.FindControl("Lnk3")
        lnk4 = sender.Parent.FindControl("Lnk4")
        lnk5 = sender.Parent.FindControl("Lnk5")
        lnk6 = sender.Parent.FindControl("Lnk6")
        lnk7 = sender.Parent.FindControl("Lnk7")
        lnk8 = sender.Parent.FindControl("Lnk8")
        lnk9 = sender.Parent.FindControl("Lnk9")
        lnk10 = sender.Parent.FindControl("Lnk10")

        Dim l_Str_Msg As String = ""
        If Not lnk1 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk1.Text
        If Not lnk2 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk2.Text
        If Not lnk3 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk3.Text
        If Not lnk4 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk4.Text
        If Not lnk5 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk5.Text
        If Not lnk6 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk6.Text
        If Not lnk7 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk7.Text
        If Not lnk8 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk8.Text
        If Not lnk9 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk9.Text
        If Not lnk10 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk10.Text
        ' l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        'If (Not lblcode Is Nothing) Then
        l_Str_Msg = UtilityObj.CleanupStringForJavascript(l_Str_Msg)
        l_Str_Msg = l_Str_Msg.Replace(vbCrLf, "")
        l_Str_Msg = l_Str_Msg.Replace(vbCr, "")
        l_Str_Msg = l_Str_Msg.Replace(vbLf, "")

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & l_Str_Msg & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Dim javaSr As String

        javaSr = "function listen_window(){"
        javaSr += " var oArg = new Object();"
        javaSr += "oArg.NameandCode ='" & l_Str_Msg & "' ; "
        javaSr += "var oWnd = GetRadWindow('" & l_Str_Msg & "');"
        javaSr += "oWnd.close(oArg);"
        javaSr += "}"

        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "POPUP", javaSr, True)



        h_SelectedId.Value = "Close"
        'End If
    End Sub
    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As LinkButton
        Dim IDs As String = ""
        If chkSelAll.Checked Then
            IDs = hdnSelIDs.Value
        Else
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("Lnk1"), LinkButton)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                End If
            Next
        End If
        IDs = IDs.Replace("'", "\'")

        RememberOldValues()
        h_SelectedId.Value = ""
        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then h_SelectedId.Value &= IIf(h_SelectedId.Value <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If

        h_SelectedId.Value = h_SelectedId.Value.Replace("'", "\'")

        'If (Not lblcode Is Nothing) Then
        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & h_SelectedId.Value & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Dim javaSr As String

        javaSr = "function listen_window(){"
        javaSr += " var oArg = new Object();"
        javaSr += "oArg.NameandCode ='" & h_SelectedId.Value & "' ; "
        javaSr += "var oWnd = GetRadWindow('" & h_SelectedId.Value & "');"
        javaSr += "oWnd.close(oArg);"
        javaSr += "}"

        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "POPUP", javaSr, True)


        h_SelectedId.Value = "Close"
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        RememberOldValues()
        gvDetails.PageIndex = e.NewPageIndex
        gridbind()
        SetChk(Me.Page)
        RePopulateValues()
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim FloatColIndexArr(), i, ControlName As String
            FloatColIndexArr = FloatColIndex.Split("|")
            Dim lnk As LinkButton
            For Each i In FloatColIndexArr
                If IsNumeric(i) Then
                    ControlName = "lnk" & i.ToString
                    lnk = TryCast(e.Row.FindControl(ControlName), LinkButton)
                    If Not lnk Is Nothing Then
                        If IsNumeric(lnk.Text) Then
                            lnk.Text = Convert.ToDouble(lnk.Text).ToString("####0.00")
                        End If
                    End If
                End If
            Next
            Dim DateColIndexArr() As String
            DateColIndexArr = DateColIndex.Split("|")
            For Each i In DateColIndexArr
                If IsNumeric(i) Then
                    ControlName = "lnk" & i.ToString
                    lnk = TryCast(e.Row.FindControl(ControlName), LinkButton)
                    If Not lnk Is Nothing Then
                        If IsDate(lnk.Text) Then
                            lnk.Text = CDate(lnk.Text).ToString("dd/MMM/yyyy")
                        End If
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        Try
            hdnSelIDs.Value = ""
            If chkSelAll.Checked Then
                Dim mTTable As New DataTable
                mTTable = ViewState("GridTable")
                If mTTable Is Nothing Then Exit Sub
                Dim mRow As DataRow
                For Each mRow In mTTable.Rows
                    hdnSelIDs.Value &= IIf(hdnSelIDs.Value <> "", "|", "") & mRow(0)
                Next
            End If
            SetChk(Me.Page)
            RememberAllValues()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk IsNot Nothing Then
                    chk.Checked = chkSelAll.Checked
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Sub RememberOldValues()
        Dim itmIdList As New ArrayList()
        Dim bindDt As DataTable
        bindDt = gvDetails.DataSource
        For Each row As GridViewRow In gvDetails.Rows
            Dim index As String = DirectCast(row.FindControl("lnk1"), LinkButton).Text
            Dim result As Boolean = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox).Checked
            If ViewState("checked_items") IsNot Nothing Then
                itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
            End If
            If result Then
                If Not itmIdList.Contains(index) Then
                    itmIdList.Add(index)
                End If
            Else
                itmIdList.Remove(index)
            End If
        Next
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub
    Private Sub RememberAllValues()
        Dim itmIdList As New ArrayList()
        If ViewState("checked_items") IsNot Nothing Then
            itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
        End If
        itmIdList.Clear()
        If chkSelAll.Checked Then
            Dim mTTable As New DataTable
            mTTable = ViewState("GridTable")
            If mTTable Is Nothing Then Exit Sub
            Dim mRow As DataRow
            For Each mRow In mTTable.Rows
                If Not itmIdList.Contains(mRow(0)) Then
                    itmIdList.Add(mRow(0))
                End If
            Next
        End If
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub
    Private Sub RePopulateValues()
        Dim itmIdList As ArrayList = DirectCast(ViewState("checked_items"), ArrayList)
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            For Each row As GridViewRow In gvDetails.Rows
                Dim index As String = DirectCast(row.FindControl("lnk1"), LinkButton).Text
                If itmIdList.Contains(index) Then
                    Dim myCheckBox As HtmlInputCheckBox = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub

End Class
