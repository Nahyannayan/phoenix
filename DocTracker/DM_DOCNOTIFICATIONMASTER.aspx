﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_DOCNOTIFICATIONMASTER.aspx.vb" Inherits="DOCTRACKER_DOCNOTIFICATIONMASTER" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

        <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <style type="text/css">
        .gridheader_new {
            border-style: none;
            border-color: inherit;
            border-width: 0;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            background-image: url('../../Images/GRIDHEAD.gif');
            background-repeat: repeat-x;
            font-size: 11px;
            font-weight: bold;
            color: #1b80b6;
        }

        .inputbox {
            BACKGROUND: F5FED2;
            BORDER-BOTTOM: #1B80B6 1px solid;
            BORDER-LEFT: #1B80B6 1px solid;
            BORDER-RIGHT: #1B80B6 1px solid;
            BORDER-TOP: #1B80B6 1px solid;
            COLOR: #555555;
            CURSOR: text;
            FONT-FAMILY: verdana;
            FONT-SIZE: 11px;
            WIDTH: 199px;
            HEIGHT: 14px;
            TEXT-DECORATION: none;
        }

        .button {
        }
    </style>

    <script language="javascript" type="text/javascript">

        function GetEMPNAME() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var SchoolorCorpt;
            SchoolorCorpt = document.getElementById('<%=h_BSUorSCHOOL.ClientID %>').value
            return ShowWindowWithClose("../DocTracker/SelectDocEmpList_DM.aspx?ID=" + SchoolorCorpt + "", 'search', '55%', '85%')
            return false;
        }

        function setEmpValue(result) {
            NameandCode = result.split('||');
            document.getElementById('<%=h_EMPID.ClientID %>').value = result;
            CloseFrame();
            //return false;
        }

     
<%--function getProduct(DOMDESCR, DOMID) {
    var sFeatures;
    var lstrVal;
    var lintScrVal;
    var pMode;
    var NameandCode;
    sFeatures = "dialogWidth: 760px; ";
    sFeatures += "dialogHeight: 420px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    pMode = "APPROVERLIST"
    document.getElementById('<%=hfDOMDESCRid.ClientID%>').value = DOMDESCR;
    document.getElementById('<%=hfDOMIDid.ClientID%>').value = DOMID;
    url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode;
    return ShowWindowWithClose("../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "", 'search', '55%', '85%')
    return false;
}--%>

        function getProduct(DOMDESCR, DOMID) {
            var pMode;
            var NameandCode;
            pMode = "APPROVERLIST"
            document.getElementById('<%=hf_1.ClientID%>').value = DOMDESCR;
            document.getElementById('<%=hf_2.ClientID%>').value = DOMID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_ApprList");
        }

        function OnClientCloseApprover(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[0];
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
            }
        }

        
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }





function setProduct2(result) {
    //alert(1);
    NameandCode = result.split('___');
    str = document.getElementById('<%=hfDOMDESCRid.ClientID%>').value
    str2 = document.getElementById('<%=hfDOMIDid.ClientID%>').value
    //document.getElementById(DOMDESCR).value = NameandCode[1];
    document.getElementById(document.getElementById('<%=hfDOMDESCRid.ClientID%>').value).value = NameandCode[1];
    document.getElementById(document.getElementById('<%=hfDOMIDid.ClientID%>').value).value = NameandCode[0];
    //alert(NameandCode[1]);
    //document.getElementById(DOMID).value = NameandCode[0];
    //document.getElementById(DOMDESCR).disabled = true;
    CloseFrame();
    //return false;
}
function CloseFrame() {
    jQuery.fancybox.close();
}



function OnTreeClick(evt) {
    var src = window.event != window.undefined ? window.event.srcElement : evt.target;
    var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
    if (isChkBoxClick) {
        var parentTable = GetParentByTagName("table", src);
        var nxtSibling = parentTable.nextSibling;
        if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
        {
            if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
            {
                //check or uncheck children at all levels
                CheckUncheckChildren(parentTable.nextSibling, src.checked);
            }
        }
        //check or uncheck parents at all levels
        CheckUncheckParents(src, src.checked);
    }
}

function CheckUncheckChildren(childContainer, check) {
    var childChkBoxes = childContainer.getElementsByTagName("input");
    var childChkBoxCount = childChkBoxes.length;
    for (var i = 0; i < childChkBoxCount; i++) {
        childChkBoxes[i].checked = check;
    }
}

function CheckUncheckParents(srcChild, check) {
    var parentDiv = GetParentByTagName("div", srcChild);
    var parentNodeTable = parentDiv.previousSibling;

    if (parentNodeTable) {
        var checkUncheckSwitch;

        if (check) //checkbox checked
        {
            var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
            if (isAllSiblingsChecked)
                checkUncheckSwitch = true;
            else
                return; //do not need to check parent if any child is not checked
        }
        else //checkbox unchecked
        {
            checkUncheckSwitch = false;
        }

        var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
        if (inpElemsInParentTable.length > 0) {
            var parentNodeChkBox = inpElemsInParentTable[0];
            parentNodeChkBox.checked = checkUncheckSwitch;
            //do the same recursively
            CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
        }
    }
}

function AreAllSiblingsChecked(chkBox) {
    var parentDiv = GetParentByTagName("div", chkBox);
    var childCount = parentDiv.childNodes.length;
    for (var i = 0; i < childCount; i++) {
        if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
        {
            if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                //if any of sibling nodes are not checked, return false
                if (!prevChkBox.checked) {
                    return false;
                }
            }
        }
    }
    return true;
}

//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj) {
    var parent = childElementObj.parentNode;
    while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
        parent = parent.parentNode;
    }
    return parent;
}


function change_chk_stateg(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("Checkbsu") != -1) {

            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();
        }
    }
}


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_ApprList" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseApprover"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Email Notification Setup Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
                    <tr>
                        <td class="title-bg-lite">Email Notification Setup Master
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="100%">

                            <table width="100%">


                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Notification Description</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox>


                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr id="RowEmpName" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Document Group</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlDocGroup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Doc Type</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlDocType" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr id="trLevel1days" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Notification Days</span>
                                        <img alt="Escalation 1" src="../Images/Helpdesk/Escalation_Yellow.png" height="10" width="20" /></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtLevel1Days" runat="server" BackColor="Yellow" Wrap="False" EnableTheming="False">0</asp:TextBox>
                                        <br />
                                        Days before Expiry Date</td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Notification Text</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtNotification" runat="server" TextMode="MultiLine"> </asp:TextBox>
                                    </td>
                                    <%--  <td align="left"></td>
                                    <td align="left"></td>--%>
                                </tr>




                                <tr id="trBsu" runat="server" visible="false">
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>



                                <%-- <tr >
            <td>
            User Type</td>
        <td>
                        :</td>
        <td>
            <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" Visible ="false" >
</asp:DropDownList>
            <asp:RadioButtonList ID="rblUserType" runat="server" RepeatDirection="Horizontal" AutoPostBack ="true"   >
                <asp:ListItem Value="0" Selected="True">Designation</asp:ListItem>                
                <asp:ListItem  Value="1">Employee</asp:ListItem>
                                
                                
                            </asp:RadioButtonList>


        </td>
    </tr>--%>




                                <%-- <tr >
            <td>
            <asp:Label  ID="lblCaption" Text="" runat ="server" ></asp:Label></td>

            <td>
                        :</td>
        <td>
            <asp:TextBox ID="txtEMPNAME" runat="server" Width="250px" CssClass="inputbox" 
                    ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddEMPID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgGetLevel1Employees" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="return GetEMPNAME();return false;" /></td>
    </tr>--%>
                                <tr>
                                    <td class="title-bg-lite" colspan="4">User \Role Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False"
                                            Width="100%" PageSize="5" CssClass="table table-bordered table-row"
                                            EnableModelValidation="True" DataKeyNames="ID" ShowFooter="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDEF_ID" runat="server" Text='<%# Bind("DEF_ID") %>'></asp:Label>
                                                    </ItemTemplate>



                                                </asp:TemplateField>






                                                <asp:TemplateField HeaderText="DESCRIPTION" ItemStyle-Width="40%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtDescr" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDescr_e" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgRole_User_e" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            TabIndex="14" />


                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtDescr" Enabled="false" runat="server"></asp:TextBox>

                                                        <asp:ImageButton ID="imgRole_User" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />

                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cc" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="center">
                                                    <ItemTemplate>
                                                        <%--<asp:CheckBox ID="chkLevel1" runat="server" Checked='<%# Bind("DEFCC") %>' />--%>
                                                        <asp:CheckBox ID="chkLevel1" Enabled="false" runat="server" Checked='<%# IIf(Eval("DEFCC") Is DBNull.Value, "False", Eval("DEFCC")) %>' />



                                                    </ItemTemplate>
                                                    <ItemStyle BackColor="Yellow" HorizontalAlign="Center" />

                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkLevel1" runat="server" ItemStyle-HorizontalAlign="Center" Checked='<%# IIf(Eval("DEFCC") Is DBNull.Value, "False", Eval("DEFCC")) %>' />
                                                        <asp:HiddenField ID="h_DEF_DOM_ID" Value='<%# Bind("DEF_DOM_ID") %>' runat="server" />
                                                    </EditItemTemplate>

                                                    <FooterTemplate>
                                                        <%-- <table width="100%" align="center">
                                                            <tr>
                                                                <td align="center">--%>
                                                        <asp:CheckBox ID="chkLevel1" runat="server" Checked='<%# IIf(Eval("DEFCC") Is DBNull.Value, "False", Eval("DEFCC")) %>' />
                                                        <asp:HiddenField ID="h_DEF_DOM_ID" Value='<%# Bind("DEF_DOM_ID") %>' runat="server" />
                                                        <%--  </td>
                                                            </tr>
                                                        </table>--%>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField HeaderText="Alert for" ItemStyle-Width="20%">
                                             <ItemTemplate>
                                                <asp:Label ID="lblAlertFor" runat="server" Style="text-align: left" Width="120px"
                                                   Text='<%# Bind("ALERTFOR") %>'></asp:Label>
                                             </ItemTemplate>
                                             <EditItemTemplate>
                                                <asp:DropDownList ID="ddlAlertFor" runat="server" Width="120px"></asp:DropDownList>
                                                
                                             </EditItemTemplate>
                                             <FooterTemplate>
                                                <asp:DropDownList ID="ddlAlertFor" runat="server" Width="120px"></asp:DropDownList>
                                                 
                                             </FooterTemplate>
                                          </asp:TemplateField>
                   
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Remove</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkUpdateNSM" runat="server" CausesValidation="True" CommandName="Update"
                                                            Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkCancelBO" runat="server" CausesValidation="False" CommandName="Cancel"
                                                            Text="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:LinkButton ID="lnkAddNSM" runat="server" CausesValidation="False" CommandName="AddNew"
                                                            Text="Add New"></asp:LinkButton>
                                                    </FooterTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkEditNSM" runat="server" CausesValidation="False" CommandName="Edit"
                                                            Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />

                                            </Columns>
                                            <HeaderStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title-bg-lite">Applicable Business Units
                                    </td>

                                    <td colspan="3" width="100%">
                                        <%--<table width="100%">
                                            <tr>
                                                <td>--%>
                                        <asp:TextBox ID="lblOtherBsus" Enabled="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        <%-- </td>
                                            </tr>
                                        </table>--%>
                                    </td>



                                </tr>


                                <tr>
                                    <td colspan="4">

                                        <div class="checkbox-list">
                                            <asp:TreeView ID="TreeItemCategory" runat="server" onclick="OnTreeClick(event);"
                                                ImageSet="Msdn" ShowCheckBoxes="All" ShowLines="True">
                                                <ParentNodeStyle Font-Bold="False" />
                                                <HoverNodeStyle Font-Underline="True" />
                                                <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px"
                                                    VerticalPadding="0px" />
                                                <NodeStyle CssClass="matters" HorizontalPadding="5px"
                                                    NodeSpacing="0px" VerticalPadding="0px" />
                                            </asp:TreeView>
                                        </div>
                                    </td>


                                </tr>

                                <%--<tr id ="RowBusinessUnit" runat ="server" visible="false">
        <td valign="top">
            Business Unit (Access)
        </td>
        <td>
            &nbsp;
        </td>
        <td valign="top">
            <asp:GridView ID="GridRootingBsu" runat="server" AutoGenerateColumns="false" EnableTheming="false"
                Width="95%">
                <Columns>
                    <asp:TemplateField HeaderText="Owner">
                        <HeaderTemplate>
                            <center>
                        
                                <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_stateg(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:CheckBox ID="Checkbsu" runat="server"  />
                            </center>
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Business Unit">
                        <HeaderTemplate>
                            <span style="font-size: small">Business Unit</span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span style="font-size: small">
                                <%#Eval("BSU_NAME")%></span>
                            <asp:HiddenField ID="HiddenBsuid" runat="server" Value='<%#Eval("BSU_ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Font-Size="Small" Height="20px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>--%>







                                <tr>

                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                                        <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />




                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"></td>
                                </tr>
                                <tr>

                                    <td align="center" colspan="4">

                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="HiddenType" runat="server" />
                <asp:HiddenField ID="h_EMPID" runat="server" />
                <asp:HiddenField ID="h_BSUorSCHOOL" runat="server" />
                <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                <asp:HiddenField ID="h_Emp_Deleted" runat="server" Value="0" />
                <asp:HiddenField ID="h_OLD_EMPID" runat="server" Value="0" />
                <asp:HiddenField ID="h_Role_delete" runat="server" Value="0" />


                <asp:HiddenField ID="hfDOMDESCRid" runat="server" Value="0" />
                <asp:HiddenField ID="hfDOMIDid" runat="server" Value="0" />
                <asp:HiddenField ID="hf_1" runat="server" />
                <asp:HiddenField ID="hf_2" runat="server" />
            </div>
        </div>
    </div>

    <asp:HiddenField ID="h_BSUID" runat="server" />


    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>


</asp:Content>

