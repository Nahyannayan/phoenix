﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports Telerik.Web.UI
Imports Encryption64
Imports System.Drawing

Partial Class DocTracker_DocumentTracker
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
    Dim encr As New SHA256EncrDecr

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                h_menuCode.Value = ViewState("MainMnu_code")
                ViewState("EnqStatus") = "0"
                Try
                    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
                    Dim str_query As String = ""
                    Dim PARAM(10) As SqlParameter
                    PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
                    PARAM(1) = New SqlParameter("@BSU", Session("sBsuid"))
                    Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETBSULIST_CLM", PARAM)
                    ddlBSU.DataSource = dsDetails.Tables(0)
                    ddlBSU.DataTextField = "DESCR"
                    ddlBSU.DataValueField = "ID"
                    ddlBSU.DataBind()
                Catch ex As Exception
                    lblError.Text = ex.Message
                End Try

                fillDropdown(ddlGroup, "select DGR_ID,DGR_DESCR from DocGroup union all select  '0','------All------ ' order by dgr_descr", "DGR_DESCR", "DGR_ID", True)
                BindDropdowns()



                'txtFromDate.Text = Now.ToString("dd/MMM/yyyy")
                Dim dtNow As Date = Date.Now
                Dim lastDay As DateTime = New DateTime(Now.Year, Now.Month, 1)
                txtFromDate.SelectedDate = dtNow.AddDays(-dtNow.Day + 1).ToString("dd/MMM/yyyy")
                txtTodate.SelectedDate = lastDay.AddMonths(1).AddDays(-1).ToString("dd/MMM/yyyy")


                If Session("sUsr_name") = "" Or ((ViewState("MainMnu_code") <> "DM00032") And (ViewState("MainMnu_code") <> "FD00034")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Dim CurBsUnit As String = Session("sBsuid")
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), CurBsUnit, ViewState("MainMnu_code"))
                    'ViewState("datamode") = "View"
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                h_Status.Value = "Active"
                BindGrid("Active")
                ViewState("EnqStatus") = "Active"
                lblStatus.ForeColor = Color.Green
                lblStatus.Text = "Active "
                'BindGrid("ALL")
                FindJob.Style.Add("display", "none")


                rptStatus.Visible = True
            Catch ex As Exception

            End Try
        Else
            'BIND_status()
            UpdateLabelStatus()
            BindGrid(ViewState("EnqStatus"), 0)
            'ViewState("EnqStatus") = "Active"
            'lblStatus.Text = "Active Documents"
        End If
        DisplayUserDetails()

    End Sub

    Private Sub BindDropdowns()

        'If ddlGroup.SelectedValue = "0" Then
        '    'fillDropdown(ddlDocumentType, "select 0 Dot_Id,'-----All-----' Dot_descr union all SELECT Dot_Id,Dot_descr FROM DOCTYPE where ('||'+Dot_bsu_apply+'||' like '%||" & Session("sBsuid") & "||%' or '||'+Dot_bsu_share+'||' like '%||" & Session("sBsuid") & "||%') and  '||'+ Dot_Rol_share LIKE '%||'+(select top 1 cast(usr_rol_id as varchar(5)) from oasis..users_m  where usr_name='" & Session("sUsr_name") & "')+'||%'  order BY Dot_descr", "Dot_Descr", "Dot_Id", True)
        '    'fillDropdown(ddlDocumentType, "select 0 Dot_Id,'-----All-----' Dot_descr union all Select  distinct DOT_ID, DOT_DESCR FROM dbo.DocType  INNER JOIN OASIS_CLM..GET_EMLOYEES_FROM_OWNER On '||' +  Dot_Rol_Share + '||' LIKE '%' + CAST(DOM_OWNER_ID AS VARCHAR)   + '%'  INNER JOIN OASIS..USERS_M ON USR_EMP_iD=EMP_ID  inner join DocIssuer on iss_id=dot_iss_id inner join  DocGroup on DGR_ID=ISS_DGR_ID WHERE USR_NAME ='" & Session("sUsr_name") & "' order BY Dot_descr", "Dot_Descr", "Dot_Id", True)
        '    fillDropdown(ddlDocumentType, "select '0' ID,'-----All-----' DESCR union all Select ID,DESCR From [dbo].[GetAccessibleListForUser]( 'DOC','" & Session("sUsr_name") & "','" & Session("sBsuID") & "') where descr in(select Dot_Descr from doctype where Dot_Iss_Id in(select iss_id from DocIssuer)) order BY DESCR", "DESCR", "ID", True)

        'Else
        '    'fillDropdown(ddlDocumentType, " select 0 Dot_Id,'-----All-----' Dot_descr union all SELECT Dot_Id,Dot_descr FROM DOCTYPE inner join DocIssuer on iss_id=dot_iss_id inner join DocGroup on DGR_ID=ISS_DGR_ID where ('||'+Dot_bsu_apply+'||' like '%||" & Session("sBsuid") & "||%' or '||'+Dot_bsu_share+'||' like '%||" & Session("sBsuid") & "||%') and  '||'+ Dot_Rol_share LIKE '%||'+(select top 1 cast(usr_rol_id as varchar(5)) from oasis..users_m  where usr_name='" & Session("sUsr_name") & "')+'||%' and ISS_DGR_ID=" & ddlGroup.SelectedValue & " order BY Dot_descr", "Dot_Descr", "Dot_Id", True)
        '    'fillDropdown(ddlDocumentType, "Select  distinct DOT_ID, DOT_DESCR FROM dbo.DocType  INNER JOIN OASIS_CLM..GET_EMLOYEES_FROM_OWNER On '||' +  Dot_Rol_Share + '||' LIKE '%' + CAST(DOM_OWNER_ID AS VARCHAR)   + '%'  INNER JOIN OASIS..USERS_M ON USR_EMP_iD=EMP_ID  inner join DocIssuer on iss_id=dot_iss_id inner join  DocGroup on DGR_ID=ISS_DGR_ID   and ISS_DGR_ID=" & ddlGroup.SelectedValue & "   WHERE USR_NAME ='" & Session("sUsr_name") & "' order BY Dot_descr", "Dot_Descr", "Dot_Id", True)
        '    ''Filling based on Document Group
        '    'fillDropdown(ddlDocumentType, "select '0' ID,'-----All-----' DESCR union all Select ID,DESCR From [dbo].[GetAccessibleListForUser]( 'DOC','" & Session("sUsr_name") & "','" & Session("sBsuID") & "') where descr in(select Dot_Descr from doctype where Dot_Iss_Id in(select iss_id from DocIssuer where iss_dgr_id=" & ddlGroup.SelectedValue & ")) order BY DESCR", "DESCR", "ID", True)
        '    fillDropdown(ddlDocumentType, "select '0' ID,'-----All-----' DESCR union all Select ID,DESCR From [dbo].[GetAccessibleListForUser]( 'DOC','" & Session("sUsr_name") & "','" & Session("sBsuID") & "') where descr in(select Dot_Descr from doctype where Dot_DGR_ID=" & ddlGroup.SelectedValue & ") order BY DESCR", "DESCR", "ID", True)
        'End If


        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
        Dim str_query As String = ""
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@LUSERNAME", Session("sUsr_name"))
        PARAM(1) = New SqlParameter("@LBSU", Session("sBsuid"))
        PARAM(2) = New SqlParameter("@DDLGROUP", ddlGroup.SelectedValue)

        Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETDOCTYPE_CLM", PARAM)
        ddlDocumentType.DataSource = dsDetails.Tables(0)
        ddlDocumentType.DataTextField = "DESCR"
        ddlDocumentType.DataValueField = "ID"
        ddlDocumentType.DataBind()
        fillDropdown(ddlLandlord, "select  LLM_ID,LLM_DESCR from VW_DOC_LANDLORD union all select 0, '---All---'  order by LLM_DESCR", "LLM_DESCR", "LLM_ID", True)
        fillDropdown(ddlEmirate, "select cit_id, Cit_name from docCity union all select 0, '---All---'  order by Cit_name", "Cit_name", "cit_id", True)
        fillDropdown(ddlbDepartment, "select DPT_ID,DPT_DESCR from department_M union all select 0, '---All---'  order by DPT_DESCR", "DPT_DESCR", "DPT_ID", True)


        Dim str_conn1 = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
        Dim str_query1 As String = ""
        Dim PARAM1(2) As SqlParameter


        PARAM1(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
        PARAM1(1) = New SqlParameter("@BSUID", Session("sBsuid"))
        dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_LANDLORD_OWNER_LIST", PARAM1)
        


        Dim Dt As DataTable
        Dim EnableLord As Integer

        Dt = dsDetails.Tables(0)
        EnableLord = Dt.Rows(0).Item("LandlordEnable")

        





        If EnableLord = 0 Then
            Suplier1.Visible = False
            Suplier2.Visible = False
        Else
            Suplier1.Visible = True
            Suplier2.Visible = True
        End If



    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub
    Private Sub BindGrid(ByVal FILTERSTR As String, Optional ByVal MainDotId As Integer = 0)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(12) As SqlParameter

            PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
            PARAM(1) = New SqlParameter("@FILTER", FILTERSTR)
            PARAM(2) = New SqlParameter("@BSU_ID", ddlBSU.SelectedValue)
            PARAM(3) = New SqlParameter("@FROMDT", Convert.ToDateTime(txtFromDate.SelectedDate.ToString).ToString("dd/MMM/yyyy"))
            PARAM(4) = New SqlParameter("@TODT", Convert.ToDateTime(txtTodate.SelectedDate.ToString).ToString("dd/MMM/yyyy"))
            'PARAM(3) = New SqlParameter("@FROMDT", "11/Jun/2018")
            'PARAM(4) = New SqlParameter("@TODT", "30/Jun/2018")
            PARAM(5) = New SqlParameter("@DTYPE", ddlDocumentType.SelectedValue)
            PARAM(6) = New SqlParameter("@SORT", ddlBSU.SelectedValue)
            PARAM(7) = New SqlParameter("@DGROUP", ddlGroup.SelectedValue)
            PARAM(8) = New SqlParameter("@LDAYS", rbOption.SelectedIndex)
            PARAM(9) = New SqlParameter("@MAIN_DOC_ID", MainDotId)
            PARAM(10) = New SqlParameter("@EMIRATE", ddlEmirate.SelectedValue)
            PARAM(11) = New SqlParameter("@LANDLORD", ddlLandlord.SelectedValue)
            PARAM(12) = New SqlParameter("@DOC_DPT_ID", ddlbDepartment.SelectedValue)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DOC_GETDOCLIST", PARAM)
            Grid_Document.DataSource = dsDetails.Tables(1)
            Grid_Document.DataBind()
            Dim resultCount As String, TotalRecords As String
            resultCount = "0"
            TotalRecords = "0"
            resultCount = dsDetails.Tables(0).Rows.Count.ToString()
            rptStatus.DataSource = dsDetails.Tables(0)
            rptStatus.DataBind()

        Catch ex As Exception
            lblError.Text = ex.Message

        End Try

    End Sub
    'Private Function GetuserDetails() As DataSet
    Private Sub DisplayUserDetails()


        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(10) As SqlParameter

            PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DOC_GETUSERDETAILS", PARAM)
            'LoginUser.Text = "Welcome " + dsDetails.Tables(0).Rows(0)("USERDETAILS").ToString()

            'Dim OrgText As String = ""
            'For Each row In dsDetails.Tables(1).Rows




            'Next




        Catch ex As Exception
            lblError.Text = ex.Message

        End Try

    End Sub
    Private Function get_sel_ids() As String
        Dim chk As CheckBox
        Dim hdEnqID As HiddenField
        Dim str As String = ""
        For Each rowItem As GridDataItem In Grid_Document.MasterTableView.Items

            chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)

            hdEnqID = DirectCast(rowItem.FindControl("hdEnqID"), HiddenField)

            If chk.Checked = True Then
                If str = "" Then
                    str = hdEnqID.Value
                Else
                    str = str & "|" & hdEnqID.Value
                End If
            End If

        Next
        Return str
    End Function
    Protected Sub gv_Enquiry_NeedDataSource(source As Object, e As GridNeedDataSourceEventArgs)

        BindGrid(ViewState("EnqStatus"), 0)
    End Sub

    Protected Sub Grid_Document_ItemCommand(sender As Object, e As GridCommandEventArgs)
        Try

            If e.CommandName = Telerik.Web.UI.RadGrid.ExportToExcelCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToWordCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToCsvCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToPdfCommandName Then
                ConfigureExport()
            End If
        Catch
        End Try

    End Sub
    Public Sub ConfigureExport()
        Grid_Document.ExportSettings.ExportOnlyData = True
        Grid_Document.ExportSettings.IgnorePaging = True
    End Sub
    Protected Sub btnMsg_Click(sender As Object, e As EventArgs) Handles btnMsg.Click
        divNote.Visible = False
    End Sub

    Protected Sub rptStatus_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptStatus.ItemCommand
        Dim hdStatus As New HiddenField
        Dim EnqStatus As Integer = 0
        hdStatus = DirectCast(e.Item.FindControl("hdStatus"), HiddenField)
        ViewState("EnqStatus") = hdStatus.Value
        EnqStatus = hdStatus.Value
        'BindGrid(ViewState("EnqStatus"))
        divExpSoon.Visible = False
        If EnqStatus = 1 Then
            ViewState("EnqStatus") = "Active"
            h_Status.Value = "Active"
            BindGrid("ACTIVE", 0)
            lblStatus.Text = "Active "
            lblStatus.ForeColor = Color.Green
        ElseIf EnqStatus = 5 Then
            ViewState("EnqStatus") = "EXPIRINGSOON"
            h_Status.Value = "EXPIRINGSOON"
            BindGrid("EXPIRINGSOON", 0)
            lblStatus.Text = "Expiring soon"
            divExpSoon.Visible = True
            rbOption.SelectedIndex = 0
            lblStatus.ForeColor = Color.Yellow
        ElseIf EnqStatus = 4 Then
            ViewState("EnqStatus") = "PENDINGAPPROVAL"
            h_Status.Value = "PENDINGAPPROVAL"
            BindGrid("PENDINGAPPROVAL", 0)
            lblStatus.Text = "Waiting for Approval"
            lblStatus.ForeColor = Color.Navy
        ElseIf EnqStatus = 2 Then
            h_Status.Value = "EXPIRED"
            ViewState("EnqStatus") = "EXPIRED"
            BindGrid("EXPIRED", 0)
            lblStatus.Text = "Expired "
            lblStatus.ForeColor = Color.Purple
        ElseIf EnqStatus = 3 Then
            h_Status.Value = "RETIRE"
            ViewState("EnqStatus") = "RETIRE"
            BindGrid("RETIRE", 0)
            lblStatus.Text = "Retired "
            lblStatus.ForeColor = Color.Gray
        ElseIf EnqStatus = 8 Then
            h_Status.Value = "NOTUPLOADED"
            ViewState("EnqStatus") = "NOTUPLOADED"
            BindGrid("NOTUPLOADED", 0)
            lblStatus.Text = "Not Uploaded"
            lblStatus.ForeColor = Color.Blue
        ElseIf EnqStatus = 7 Then
            h_Status.Value = "ALL"
            ViewState("EnqStatus") = "ALL"
            BindGrid("ALL", 0)
            lblStatus.Text = "All "
            lblStatus.ForeColor = Color.Orange
        ElseIf EnqStatus = 6 Then
            h_Status.Value = "Blocked"
            ViewState("EnqStatus") = "Blocked"
            BindGrid("Blocked", 0)
            lblStatus.Text = "Blocked "
            lblStatus.ForeColor = Color.Red
        End If

    End Sub
    'Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click

    '    Response.Redirect("~/Homepage.aspx")
    'End Sub

    Private Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBSU.SelectedIndexChanged
        BindGrid(ViewState("EnqStatus"))
        FindJob.Style.Add("display", "none")
        'hrefEnq.Visible = True
        rptStatus.Visible = True

    End Sub
    Private Sub ddlDocumentType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDocumentType.SelectedIndexChanged
        'BIND_status()
        UpdateLabelStatus()
        BindGrid(ViewState("EnqStatus"))
    End Sub

    

    Private Sub UpdateLabelStatus()
        If ViewState("EnqStatus") = "0" Then
            'BindGrid("ALL")
            lblStatus.Text = "All Documents"
        ElseIf ViewState("EnqStatus") = "Active" Then
            'BindGrid("ACTIVE")
            lblStatus.Text = "Active "
        ElseIf ViewState("EnqStatus") = "EXPIRINGSOON" Then

            'BindGrid("EXPIRINGSOON")
            lblStatus.Text = "Expiring soon"
        ElseIf ViewState("EnqStatus") = "PENDINGAPPROVAL" Then

            'BindGrid("PENDINGAPPROVAL")
            lblStatus.Text = "Waiting for Approval"
        ElseIf ViewState("EnqStatus") = "EXPIRED" Then
            'BindGrid("EXPIRED")
            lblStatus.Text = "Expired "
        ElseIf ViewState("EnqStatus") = "RETIRE" Then

            'BindGrid("RETIRE")
            lblStatus.Text = "Retired "
        ElseIf ViewState("EnqStatus") = "NOTUPLOADED" Then
            'BindGrid("NOTUPLOADED")
            lblStatus.Text = "Not Uploaded"
        End If
    End Sub
    Private Sub txtFromDate_TextChanged(sender As Object, e As EventArgs) Handles txtFromDate.SelectedDateChanged
        BindGrid(ViewState("EnqStatus"))
        UpdateLabelStatus()
    End Sub

    Private Sub txtTodate_TextChanged(sender As Object, e As EventArgs) Handles txtTodate.SelectedDateChanged
        BindGrid(ViewState("EnqStatus"))
        UpdateLabelStatus()
    End Sub

    Private Sub gv_Enquiry_PreRender(sender As Object, e As EventArgs) Handles Grid_Document.PreRender
        If ViewState("EnqStatus") = "NOTUPLOADED" Then
            Grid_Document.MasterTableView.GetColumn("TemplateColumn").Display = False
        Else
            Grid_Document.MasterTableView.GetColumn("TemplateColumn").Display = True
        End If
    End Sub
    Private Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        BindDropdowns()
        UpdateLabelStatus()
        BindGrid(ViewState("EnqStatus"), 0)
    End Sub
    Private Sub chkShowFilters_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowFilters.CheckedChanged
        Try
            Grid_Document.AllowFilteringByColumn = chkShowFilters.Checked
            Grid_Document.Rebind()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub rbOption_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbOption.SelectedIndexChanged
        BindGrid(ViewState("EnqStatus"))
    End Sub
    Private Function BindGrid_det(ByVal id As String) As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
        Dim str_query As String = ""
        Dim PARAM(12) As SqlParameter

        PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
        PARAM(1) = New SqlParameter("@FILTER", ViewState("EnqStatus"))
        PARAM(2) = New SqlParameter("@BSU_ID", ddlBSU.SelectedValue)
        PARAM(3) = New SqlParameter("@FROMDT", Convert.ToDateTime(txtFromDate.SelectedDate.ToString).ToString("dd/MMM/yyyy"))
        PARAM(4) = New SqlParameter("@TODT", Convert.ToDateTime(txtTodate.SelectedDate.ToString).ToString("dd/MMM/yyyy"))
        'PARAM(3) = New SqlParameter("@FROMDT", "11/Jun/2018")
        'PARAM(4) = New SqlParameter("@TODT", "30/Jun/2018")
        PARAM(5) = New SqlParameter("@DTYPE", ddlDocumentType.SelectedValue)
        PARAM(6) = New SqlParameter("@SORT", ddlBSU.SelectedValue)
        PARAM(7) = New SqlParameter("@DGROUP", ddlGroup.SelectedValue)
        PARAM(8) = New SqlParameter("@LDAYS", rbOption.SelectedIndex)
        PARAM(9) = New SqlParameter("@MAIN_DOC_ID", id)
        PARAM(10) = New SqlParameter("@EMIRATE", ddlEmirate.SelectedValue)
        PARAM(11) = New SqlParameter("@LANDLORD", ddlLandlord.SelectedValue)
        PARAM(12) = New SqlParameter("@DOC_DPT_ID", ddlbDepartment.SelectedValue)


        Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DOC_GETDOCLIST", PARAM)
        Return dsDetails

    End Function


    Protected Sub Grid_Document_DetailTableDataBind(sender As Object, e As GridDetailTableDataBindEventArgs)
        Dim dataItem As GridDataItem = DirectCast(e.DetailTableView.ParentItem, GridDataItem)
        Select Case e.DetailTableView.Name
            Case "Doc_ID"
                If True Then
                    Dim DocumentId As String = dataItem.GetDataKeyValue("Doc_ID").ToString()
                    e.DetailTableView.DataSource = BindGrid_det(DocumentId).Tables(1)
                End If
        End Select
    End Sub

    Private Sub gv_Enquiry_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles Grid_Document.ItemDataBound

        If TypeOf e.Item Is GridHeaderItem Then

        ElseIf TypeOf e.Item Is GridGroupHeaderItem Then

        ElseIf TypeOf e.Item Is GridDataItem Then
            Dim HDN_SMCLASS As HiddenField

            Dim item As GridDataItem = e.Item
            HDN_SMCLASS = e.Item.FindControl("HDN_SMCLASS")
            If Not HDN_SMCLASS Is Nothing Then
                e.Item.CssClass = HDN_SMCLASS.Value
            End If

            If Not item.GetDataKeyValue("SM_COLOR") Is Nothing Then
                Dim grdItem As GridDataItem
                grdItem = e.Item
                e.Item.ForeColor = Color.FromName(item.GetDataKeyValue("SM_COLOR").ToString)
            End If

        End If
    End Sub

    Protected Sub ddlEmirate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlEmirate.SelectedIndexChanged
        UpdateLabelStatus()
        BindGrid(ViewState("EnqStatus"))
    End Sub

    Protected Sub ddlLandlord_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLandlord.SelectedIndexChanged

        UpdateLabelStatus()
        BindGrid(ViewState("EnqStatus"))
    End Sub
End Class

