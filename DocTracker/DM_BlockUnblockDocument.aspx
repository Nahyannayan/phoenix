﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DM_BlockUnblockDocument.aspx.vb" Inherits="DocTracker_DM_BlockUnblockDocument" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    <link href="css/enroll.css?1=2" rel="stylesheet" type="text/css" />
    <link href="css/gems.css?1=2" rel="stylesheet" />
    <link href="css/buttons.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />
   <%-- <script src="js/jquery-1.9.1.js?1=2" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-1.4.3.min.js?1=2"></script>--%>
     <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
    <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js?1=2"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js?1=2"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css?1=2" media="screen" />
</head>
<body>
    <form id="bLOCKFORM" runat="server">
        <script type="text/javascript" lang="javascript">
            function fancyClose() {
                parent.$.fancybox.close();
            }
        </script>
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <table align="left" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" style="width: 100%">
            <tr class="subheader_img">
                <td align="center" colspan="6" style="height: 10px; background-color: #00a3e0; color: white; width:100%" valign="middle" runat="server" id="tdTitle">
                    
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:Label ID="lblError" runat="server" ForeColor="blue"></asp:Label>
                    <pre id="preError" class="invisible" runat="server"></pre>
                </td>
            </tr>
            <tr>
                <td class="matters" width="10%">Document Type</td>
                <td class="matters" width="1%">:</td>
                <td class="matters" align="left" width="14%">
                    <asp:Label ID="lbldoctype" runat="server"></asp:Label></td>

                <td class="matters">Document Issued to</td>
                <td class="matters">:</td>
                <td class="matters" align="left">
                    <asp:Label ID="lblIssuedTo" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="matters" width="10%">Document Issuer</td>
                <td class="matters" width="1%">:</td>
                <td class="matters" width="14%" align="left">

                    <asp:Label ID="lblIssuer" runat="server"></asp:Label></td>
                <td class="matters" width="10%">Document No</td>
                <td class="matters" width="1%">:</td>
                <td class="matters" width="14%" align="left">

                    <asp:Label ID="lblDocno" runat="server"></asp:Label></td>
            </tr>

            <tr id="EditDetails3" runat="server">
                <td class="matters" width="10%">Issue Date</td>
                <td class="matters" width="1%">:</td>
                <td class="matters" width="14%">
                    <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
                </td>
                <td class="matters" width="10%">Expiry Date</td>
                <td class="matters" width="1%">:</td>
                <td class="matters" width="14%">
                    <asp:Label ID="lblExpDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="matters">Document Details</td>
                <td class="matters">:</td>
                <td class="matters" colspan="4" align="left">
                    <asp:Label ID="lblDocDetails" runat="server"></asp:Label></td>


            </tr>
            <tr id="TrUnblockdetails" runat="server">
                <td class="matters">Tentative Unblock Date</td>
                <td class="matters">:</td>
                <td class="matters" colspan="4" align="left">
                    <asp:TextBox ID="txtDate" CssClass="matters" runat="server" Width="90px"></asp:TextBox>
                    <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"
                        Width="16px"></asp:ImageButton>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                        TargetControlID="txtDate" PopupButtonID="lnkDate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                        TargetControlID="txtDate" PopupButtonID="txtDate">
                    </ajaxToolkit:CalendarExtender>
                </td>
            </tr>
            <tr id="Trcomments" runat="server">
                <td class="matters">Last Comments</td>
                <td class="matters">:</td>
                <td class="matters" colspan="4" align="left">
                    <asp:Label ID="lbllastComments" runat="server"></asp:Label></td>
            </tr>
            <tr id="Tr2" runat="server">
                <td class="matters">Comments</td>
                <td class="matters">:</td>
                <td class="matters" colspan="4" align="left">
                    <asp:TextBox ID="txtComments" CssClass="inputStyle" TextMode="MultiLine" Height="120px" SkinID="MultiText" runat="server" Width="99%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" class="matters" colspan="6">
                    <asp:Button ID="btnBlock" runat="server" Text="Block Document" class="btn btn-primary" Style="background: #fedb00; color: black; font-weight: normal;" />
                    <asp:Button ID="btnBack" runat="server" Text="Close window" class="btn btn-primary" Style="background: #fedb00; color: black; font-weight: normal;" />
                    <asp:HiddenField ID="h_View_id" runat="server" />
                    <asp:HiddenField ID="h_BSU_Id" runat="server" />
                    <asp:HiddenField ID="h_DOC_Status" runat="server" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
