﻿<%@ Page Language="VB"  MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_DOCSUBGROUP.aspx.vb" Inherits="DocTracker_DM_DOCSUBGROUP" Title="Document Sub Group" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Document Sub Type Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="subheader_img">
                                    <td align="center" colspan="3" valign="middle">
                                        <div align="center">Document Group Master</div>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDocSUBGroup" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><td align="left"><span class="field-label">Short Name</span></td></td>
                                    <td align="left" width="30%"> <asp:TextBox ID="txtShortName" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Department</span></td>
                                    

                                    <td align="left">
                                       <asp:DropDownList ID="ddlDep" runat="server"></asp:DropDownList>
                                             
                                    </td>
                                    

                                    <td align="left">
                                  
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Details</span></td>

                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtDetails" runat="server"
                                            TabIndex="160" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" class="matters" colspan="6">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                 <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </div>
        </div>
    </div>

</asp:Content>

