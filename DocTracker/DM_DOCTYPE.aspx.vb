Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj


Partial Class DOCTRACKER_DMDOCTYPE
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim encr As New SHA256EncrDecr
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "DM00012" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            BindCity()


            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                Dim encObj As New Encryption64
                setModifyvalues(encObj.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            Else
                SetDataMode("add")
                setModifyvalues(0)
            End If
        End If
    End Sub

    Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean

        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        txtDocType.Enabled = EditAllowed
        txtLife.Enabled = EditAllowed
        txtLeadDays.Enabled = EditAllowed
        ddlDocumentIssuer.Enabled = EditAllowed
        ddlDepartment.Enabled = EditAllowed
        ddlDocumentCity.Enabled = EditAllowed
        ddlDocumentGroup.Enabled = EditAllowed
        plChkBUnit1.Enabled = EditAllowed
        plChkBUnit2.Enabled = EditAllowed
        plChkRole.Enabled = EditAllowed
        plChkConType.Enabled = EditAllowed
        chkBSUAll1.Visible = EditAllowed
        chkBSUAll2.Visible = EditAllowed
        chkRolAll.Visible = EditAllowed
        chkConAll.Visible = EditAllowed
        pnlEmployee.Enabled = Not mDisable
        PnlSubDoc.Enabled = Not mDisable
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        rbTracking.Enabled = EditAllowed
        rbFullVersion.Enabled = EditAllowed
        txtSOPPath.Enabled = EditAllowed
        txtNotification.Enabled = EditAllowed
        AprPanel.Enabled = EditAllowed
        PnlUploadDocRoles.Enabled = EditAllowed
        pnlDocBlockUsers.Enabled = EditAllowed
        pnlEmployee.Enabled = EditAllowed
        chkEitherPreApprover.Enabled = EditAllowed
    End Sub

    Private Sub BindCity()

        fillDropdown(ddlDocumentIssuer, "SELECT Iss_Id,Iss_Descr FROM DocIssuer union all SELECT 0,'----------None---------' order BY Iss_Descr", "Iss_Descr", "Iss_Id", True)
        fillDropdown(ddlDepartment, "SELECT DDT_Id DEP_Id,DDT_DEPARTMENT_DESCR DESCR  FROM DOC_DEPARTMENT where isnull(DDT_DELETED,0)=0 union all SELECT 0,'----------None---------' order BY DESCR", "Descr", "DEP_Id", True)
        fillDropdown(ddlDocumentGroup, "select DGR_ID,DGR_DESCR from DocGroup union all select 0,'-----Select One-----' order by DGR_Descr", "DGR_Descr", "DGR_Id", True)

        'fillDropdown(ddlSubGroup, "select DSG_ID,DSG_Descr from DocSUBGroup inner join DocGroup on DGR_ID=DSG_DGR_ID union all select 0,'-----Select One-----' order by DSG_Descr", "DSG_Descr", "DSG_Id", True)
        'fillDropdown(ddlBDepartment, "SELECT DPT_ID ID ,DPT_DESCR DESCR  FROM DEPARTMENT_M where isnull(DPT_DELETED,0)=0 union all SELECT 0,'----------None---------' order BY DESCR", "DESCR", "ID", True)


        ddlDocumentCity.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select cit_id, Cit_name from docCity union all select 0, '--------Select One--------'  order by Cit_name")
        ddlDocumentCity.DataTextField = "cit_name"
        ddlDocumentCity.DataValueField = "cit_id"
        ddlDocumentCity.DataBind()
    End Sub
    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            If p_Modifyid > 0 Then



                Dim str_Sql As String
                'str_Sql = "select Dot_Id,Dot_Descr,Dot_Iss_Id,Dot_Bsu_Share,Dot_Rol_Share,Dot_Con_Type,Dot_WorkFlow,Dot_Expiry,Dot_LeadDays,Dot_Life,Dot_SysDate,Dot_bsu_apply,isnull(dot_dom_ids,'')dot_dom_ids,isnull(DOT_DEP_ID,0)DOT_DEP_ID,isnull(DOT_TRACKING_ID,0)DOT_TRACKING_ID,isnull(DOT_FULL_VERSION,0)DOT_FULL_VERSION,isnull(DOT_SOPPATH,'')DOT_SOPPATH,isnull(DOT_NOTIFICATION_TEXT,'')DOT_NOTIFICATION_TEXT,isnull(DOT_CHK_PREAPPROVER,0)DOT_CHK_PREAPPROVER,isnull(dot_Cityid,0) dot_Cityid,isnull(DOT_DGR_ID,0) DOT_DGR_ID FROM doctype where dot_id='" & p_Modifyid & "' "
                'str_Sql = "select Dot_Id,Dot_Descr,Dot_Iss_Id,Dot_Bsu_Share,Dot_Rol_Share,Dot_Con_Type,Dot_WorkFlow,Dot_Expiry,Dot_LeadDays,Dot_Life,Dot_SysDate,Dot_bsu_apply,isnull(dot_dom_ids,'')dot_dom_ids,isnull(DOT_DEP_ID,0)DOT_DEP_ID,isnull(DOT_TRACKING_ID,0)DOT_TRACKING_ID,isnull(DOT_FULL_VERSION,0)DOT_FULL_VERSION,isnull(DOT_SOPPATH,'')DOT_SOPPATH,isnull(DOT_NOTIFICATION_TEXT,'')DOT_NOTIFICATION_TEXT,isnull(DOT_CHK_PREAPPROVER,0)DOT_CHK_PREAPPROVER,isnull(dot_Cityid,0) dot_Cityid,isnull(DOT_DGR_ID,0) DOT_DGR_ID,isnull(DOT_DSG_ID,0)DOT_DSG_ID,isnull(DOT_DPT_ID,0)DOT_DPT_ID FROM doctype where dot_id='" & p_Modifyid & "' "
                str_Sql = "select Dot_Id,Dot_Descr,Dot_Iss_Id,Dot_Bsu_Share,Dot_Rol_Share,Dot_Con_Type,Dot_WorkFlow,Dot_Expiry,Dot_LeadDays,Dot_Life,Dot_SysDate,Dot_bsu_apply,isnull(dot_dom_ids,'')dot_dom_ids,isnull(DOT_DEP_ID,0)DOT_DEP_ID,isnull(DOT_TRACKING_ID,0)DOT_TRACKING_ID,isnull(DOT_FULL_VERSION,0)DOT_FULL_VERSION,isnull(DOT_SOPPATH,'')DOT_SOPPATH,isnull(DOT_NOTIFICATION_TEXT,'')DOT_NOTIFICATION_TEXT,isnull(DOT_CHK_PREAPPROVER,0)DOT_CHK_PREAPPROVER,isnull(dot_Cityid,0) dot_Cityid,isnull(DOT_DGR_ID,0) DOT_DGR_ID FROM doctype where dot_id='" & p_Modifyid & "' "


                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    h_EntryId.Value = ds.Tables(0).Rows(0)("Dot_Id")
                    Dim lstDrp As New ListItem
                    lstDrp = ddlDocumentIssuer.Items.FindByValue(ds.Tables(0).Rows(0)("DOT_ISS_ID"))
                    If Not lstDrp Is Nothing Then
                        ddlDocumentIssuer.SelectedValue = lstDrp.Value
                    End If

                    Dim lstDrp1 As New ListItem
                    lstDrp1 = ddlDepartment.Items.FindByValue(ds.Tables(0).Rows(0)("DOT_DEP_ID"))
                    If Not lstDrp1 Is Nothing Then
                        ddlDepartment.SelectedValue = lstDrp1.Value
                    End If


                    Dim lstDrpDocCity As New ListItem
                    lstDrpDocCity = ddlDocumentCity.Items.FindByValue(ds.Tables(0).Rows(0)("DOT_CITYID"))
                    If Not lstDrpDocCity Is Nothing Then
                        ddlDocumentCity.SelectedValue = lstDrpDocCity.Value
                    End If
                    'ddlDocumentCity.SelectedValue = ds.Tables(0).Rows(0)("dot_Cityid")

                    Dim lstDrpDocGroup As New ListItem
                    lstDrpDocGroup = ddlDocumentGroup.Items.FindByValue(ds.Tables(0).Rows(0)("DOT_DGR_ID"))
                    If Not lstDrpDocGroup Is Nothing Then
                        ddlDocumentGroup.SelectedValue = lstDrpDocGroup.Value
                    End If

                    'Dim lstDrpDocSubGroup As New ListItem
                    'lstDrpDocSubGroup = ddlSubGroup.Items.FindByValue(ds.Tables(0).Rows(0)("DOT_DSG_ID"))
                    'If Not lstDrpDocSubGroup Is Nothing Then
                    '    ddlSubGroup.SelectedValue = lstDrpDocSubGroup.Value
                    'End If

                    'BindSubGroup()

                    'Dim lstDrpDOCBDepartment As New ListItem
                    'lstDrpDOCBDepartment = ddlBDepartment.Items.FindByValue(ds.Tables(0).Rows(0)("DOT_DPT_ID"))
                    'If Not lstDrpDOCBDepartment Is Nothing Then
                    '    ddlBDepartment.SelectedValue = lstDrpDOCBDepartment.Value
                    'End If








                    txtDocType.Text = ds.Tables(0).Rows(0)("Dot_Descr")
                    txtLife.Text = ds.Tables(0).Rows(0)("Dot_Life")
                    txtLeadDays.Text = ds.Tables(0).Rows(0)("Dot_LeadDays")
                    rptBSUNames1.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select BSU_ID, CASE when (select count(*) FROM DocType where dot_id=" & ds.Tables(0).Rows(0)("Dot_id") & " and Dot_Bsu_apply LIKE '%'+bsu_id+'%')>0 THEN 1 ELSE 0 end bsu_check, bsu_name from VW_DOC_APPLICABLE_BSULIST order by BSU_CITY,isnull(BSU_Bschool,0),bsu_name")
                    rptBSUNames1.DataBind()
                    rptBSUNames2.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select BSU_ID, CASE when (select count(*) FROM DocType where dot_id=" & ds.Tables(0).Rows(0)("Dot_id") & " and Dot_Bsu_share LIKE '%'+bsu_id+'%')>0 THEN 1 ELSE 0 end bsu_check, bsu_name from oasis.dbo.BUSINESSUNIT_M order by BSU_CITY,isnull(BSU_Bschool,0),bsu_name")
                    rptBSUNames2.DataBind()

                    'rptRole.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select ROL_ID, CASE when (select count(*) FROM DocType where dot_id=" & ds.Tables(0).Rows(0)("Dot_id") & " and Dot_Rol_share LIKE '%'+cast(rol_id as varchar)+'%')>0 THEN 1 ELSE 0 end rol_check, rol_descr from oasis.dbo.ROLES_M order by rol_descr")
                    'rptRole.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select ID ROL_ID, Case When (Select count(*) FROM DocType where dot_id=" & ds.Tables(0).Rows(0)("Dot_id") & "  And Dot_Rol_share Like '%'+cast(ID as varchar)+'%')>0 THEN 1 ELSE 0 end rol_check, DESCR rol_descr from VW_DOC_DES_LIST order by DESCR")
                    rptRole.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select ID ROL_ID, Case When (Select count(*) FROM DocType where dot_id=" & ds.Tables(0).Rows(0)("Dot_id") & "  And '||' + Dot_Rol_share + '||' LIKE '%||' + CAST(ID AS VARCHAR) + '||%') > 0 THEN 1 ELSE 0 end rol_check, DESCR rol_descr from VW_DOC_DES_LIST order by DESCR")
                    rptRole.DataBind()
                    rptConType.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select distinct Content_Type_NO, CASE when (select count(*) FROM DocType where dot_id=" & ds.Tables(0).Rows(0)("Dot_id") & " And Dot_Con_type Like '%'+cast(content_type_no as varchar)+'%')>0 THEN 1 ELSE 0 end con_check, Content_Type_DESCR from DocContentType  WHERE ISNULL(Content_Deleted,0)=0 order by Content_Type_DESCR")
                    rptConType.DataBind()
                    h_EMPID.Value = ds.Tables(0).Rows(0)("dot_dom_ids")
                    rbTracking.SelectedValue = ds.Tables(0).Rows(0)("DOT_TRACKING_ID")
                    rbFullVersion.SelectedValue = ds.Tables(0).Rows(0)("DOT_FULL_VERSION")
                    If rbFullVersion.SelectedValue = 1 Then
                        PnlUploadDocRoles.Visible = True
                    Else
                        PnlUploadDocRoles.Visible = False
                    End If

                    If ds.Tables(0).Rows(0)("DOT_SOPPATH") <> "" Then
                        txtSOPPath.Text = encr.Decrypt_SHA256(ds.Tables(0).Rows(0)("DOT_SOPPATH"))
                    Else
                        txtSOPPath.Text = ""
                    End If

                    txtNotification.Text = ds.Tables(0).Rows(0)("DOT_NOTIFICATION_TEXT")
                    chkEitherPreApprover.Checked = ds.Tables(0).Rows(0)("DOT_CHK_PREAPPROVER")

                End If
            Else
                rptBSUNames1.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select BSU_ID, 0 bsu_check, bsu_name from VW_DOC_APPLICABLE_BSULIST order by BSU_CITY,isnull(BSU_Bschool,0),bsu_name")
                rptBSUNames1.DataBind()
                rptBSUNames2.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select BSU_ID, 0 bsu_check, bsu_name from oasis.dbo.BUSINESSUNIT_M order by BSU_CITY,isnull(BSU_Bschool,0),bsu_name")
                rptBSUNames2.DataBind()

                rptRole.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "Select ID ROL_ID, 0 rol_check, DESCR rol_descr from VW_DOC_DES_LIST order by DESCR  ")
                rptRole.DataBind()
                rptConType.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select distinct Content_Type_NO, 0 con_Check, Content_Type_DESCR from DocContentType WHERE ISNULL(Content_Deleted,0)=0 order by content_type_descr")
                rptConType.DataBind()
                PnlUploadDocRoles.Visible = False


            End If
            Bindgrid()
            'DisableContentType()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    'Private Sub DisableContentType()
    '    For Each Item In rptConType.Items
    '        Dim chkConAll As CheckBox = CType(Item.FindControl("chkCON"), CheckBox)
    '        If (chkConAll.Text = "Microsoft Word" Or chkConAll.Text = "Microsoft Excel") Then
    '            chkConAll.Enabled = False
    '        End If
    '    Next
    'End Sub

    Private Sub Bindgrid()

        fillGridView_SubDocument(GridSubDocument, "select DTS_ID,DTS_dot_id ID,dot_descr DESCR,DTS_ORDER_ID ORDNO,dts_SUB_dot_id SUB_DOT_ID from   doctype inner join doctype_sub on dot_id=dts_SUB_dot_id  where isnull(dts_log_deleted,0)=0 and  dtS_DOT_id = " & h_EntryId.Value & " order by DTS_Id")
        GridSubDocument.DataSource = DTSubDocList
        GridSubDocument.DataBind()
        showNoRecordsFoundSubDocument()



        'fillGridView_DOCDOCOwners(grdDocOwner, "Select DOL_ID ,DOL_DOM_ID,Case When DOM_OWNER_TYPE='DESG' then   'Designation' else 'Employee' end STYPE, DOM_OWNER_ID EMPNO,DESCR  As DESCR From DOC_OWNER_LIST inner Join VW_DOC_DES_LIST on ID=DOL_DOM_ID inner Join OASIS..EMPDESIGNATION_M WITh(NOLOCK)On DES_ID=DOM_OWNER_ID inner Join  OASIS..EMPLOYEE_m WITh(NOLOCK) On EMP_ID=DOM_OWNER_ID where isnull(DOL_DELETED, 0) = 0 And DOL_DOT_ID = " & h_EntryId.Value & " order by DOL_Id")
        fillGridView_DOCDOCOwners(grdDocOwner, "Select  DOL_ID ,DOL_DOM_ID,DESCR From DOC_OWNER_LIST  inner Join VW_DOC_DES_LIST On id=DOL_DOM_ID where isnull(DOL_DELETED, 0) = 0 And DOL_DOT_ID = " & h_EntryId.Value & " order by DOL_Id")

        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
        showNoRecordsFoundOWNERUSERS()


        fillGridView_DOCBlockUsers(grdBlockUserList, "Select DBL_ID , DBL_DOM_ID, DESCR FROM  DOCTYPE_BLOCKUSER_LIST inner join VW_DOC_DES_LIST On ID=DBL_DOM_ID  where isnull(DBL_LOG_DELETED, 0) = 0 And DBL_DOT_ID = " & h_EntryId.Value & " order by  DESCR")
        grdBlockUserList.DataSource = DTDOCBLOCUSERList
        grdBlockUserList.DataBind()
        showNoRecordsFoundBLOCKUSERS()

        fillGridView(grdAppPreAppList, "Select DAL_ID ,DAL_DOM_ID ,DAL_TYPE,DESCR ,case when isnull(DAL_TYPE,0)='0'  then '--Select one--' else case when isnull(DAL_TYPE,0)=1 then 'RENEWAL APPROVER ' else 'APPROVAL QUEUE' END END Approver_Type,DAL_LEVEL FROM DOCTYPE_APPROVER_LIST inner join VW_DOC_DES_LIST on ID=DAL_DOM_ID  where isnull(DAL_LOG_DELETED,0)=0 AND DAL_DOT_ID=" & h_EntryId.Value & " order by DAL_LEVEL")
        grdAppPreAppList.DataSource = DOCApproverList
        grdAppPreAppList.DataBind()
        showNoRecordsFound()

        fillDOCUploadGrid(GrdUploadFullDocRoles, "SELECT DFU_ID ,DFU_DOM_ID, DESCR FROM DOCTYPE_FULLVERSIONUPLOADER_LIST inner join VW_DOC_DES_LIST on ID=DFU_DOM_ID  where isnull(DFU_LOG_DELETED,0)=0 AND DFU_DOT_ID=" & h_EntryId.Value & " order by DFU_Id ")
        GrdUploadFullDocRoles.DataSource = DTDOCUPLOADFULLList
        GrdUploadFullDocRoles.DataBind()
        ShowNoDocUploadRecordsFound()
    End Sub
    Private Sub fillDOCUploadGrid(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DTDOCUPLOADFULLList = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTDOCUPLOADFULLList)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTDOCUPLOADFULLList = mtable
        fillGrdView.DataSource = DTDOCUPLOADFULLList
        fillGrdView.DataBind()
        ShowNoDocUploadRecordsFound()
    End Sub
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DOCApproverList = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DOCApproverList)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DOCApproverList = mtable
        fillGrdView.DataSource = DOCApproverList
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub


    Private Sub fillGridView_DOCBlockUsers(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DTDOCBLOCUSERList = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTDOCBLOCUSERList)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTDOCBLOCUSERList = mtable
        fillGrdView.DataSource = DTDOCBLOCUSERList
        fillGrdView.DataBind()
        showNoRecordsFoundBLOCKUSERS()
    End Sub

    Private Sub fillGridView_DOCDOCOwners(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DTDOCOWNERLIST = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTDOCOWNERLIST)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTDOCOWNERLIST = mtable
        fillGrdView.DataSource = DTDOCOWNERLIST
        fillGrdView.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub

    Private Sub fillGridView_SubDocument(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DTSubDocList = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTSubDocList)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTSubDocList = mtable
        fillGrdView.DataSource = DTSubDocList
        fillGrdView.DataBind()
        showNoRecordsFoundSubDocument()
    End Sub

    Private Sub showNoRecordsFoundOWNERUSERS()
        If DTDOCOWNERLIST.Rows.Count > 1 Then
            If DTDOCOWNERLIST.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdDocOwner.Columns.Count - 2
                grdDocOwner.Rows(0).Cells.Clear()
                grdDocOwner.Rows(0).Cells.Add(New TableCell())
                grdDocOwner.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdDocOwner.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub
    Private Sub showNoRecordsFoundSubDocument()
        If DTSubDocList.Rows.Count > 1 Then
            If DTSubDocList.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = GridSubDocument.Columns.Count - 2
                GridSubDocument.Rows(0).Cells.Clear()
                GridSubDocument.Rows(0).Cells.Add(New TableCell())
                GridSubDocument.Rows(0).Cells(0).ColumnSpan = TotalColumns
                GridSubDocument.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub
    Private Sub showNoRecordsFound()
        If DOCApproverList.Rows.Count > 1 Then
            If DOCApproverList.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdAppPreAppList.Columns.Count - 2
                grdAppPreAppList.Rows(0).Cells.Clear()
                grdAppPreAppList.Rows(0).Cells.Add(New TableCell())
                grdAppPreAppList.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdAppPreAppList.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub
    Private Sub ShowNoDocUploadRecordsFound()
        If DTDOCUPLOADFULLList.Rows.Count > 1 Then
            If DTDOCUPLOADFULLList.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = GrdUploadFullDocRoles.Columns.Count - 2
                GrdUploadFullDocRoles.Rows(0).Cells.Clear()
                GrdUploadFullDocRoles.Rows(0).Cells.Add(New TableCell())
                GrdUploadFullDocRoles.Rows(0).Cells(0).ColumnSpan = TotalColumns
                GrdUploadFullDocRoles.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub


    Private Sub showNoRecordsFoundBLOCKUSERS()
        If DTDOCBLOCUSERList.Rows.Count > 1 Then
            If DTDOCBLOCUSERList.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdBlockUserList.Columns.Count - 2
                grdBlockUserList.Rows(0).Cells.Clear()
                grdBlockUserList.Rows(0).Cells.Add(New TableCell())
                grdBlockUserList.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdBlockUserList.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        rbFullVersion.SelectedValue = 0



    End Sub

    Sub ClearDetails()
        txtDocType.Text = ""
        txtLife.Text = "0"
        txtLeadDays.Text = "30"
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtDocType.Text.Trim.Length = 0 Then
            'lblError.Text = "Document Type description should not be empty"
            usrMessageBar.ShowNotification("Document Type description should not be empty", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If ddlDocumentIssuer.SelectedValue = 0 Then
            'lblError.Text = "Please select the correct Document Issuing Authority!"
            usrMessageBar.ShowNotification("Please select the correct Document Issuing Authority!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If ddlDepartment.SelectedValue = 0 Then
            'lblError.Text = "Please select the correct Document Department..!"
            usrMessageBar.ShowNotification("Please select the correct Document Department..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If ddlDocumentGroup.SelectedValue = 0 Then
            'lblError.Text = "Please select the correct Document Department..!"
            usrMessageBar.ShowNotification("Please select the correct Document Group..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        'If ddlSubGroup.SelectedValue = 0 Then
        '    'lblError.Text = "Please select the correct Document Department..!"
        '    usrMessageBar.ShowNotification("Please select the correct Sub Document Group..!", UserControls_usrMessageBar.WarningType.Danger)
        '    Exit Sub
        'End If

        'If ddlBDepartment.SelectedValue = 0 Then
        '    'lblError.Text = "Please select the correct Document Department..!"
        '    usrMessageBar.ShowNotification("Please select the correct Document belongs to ..!", UserControls_usrMessageBar.WarningType.Danger)
        '    Exit Sub
        'End If

        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(23) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@DOT_ID", SqlDbType.Int)
        pParms(1).Value = h_EntryId.Value
        pParms(2) = New SqlClient.SqlParameter("@DOT_DESCR", SqlDbType.VarChar, 500)
        pParms(2).Value = txtDocType.Text
        pParms(3) = New SqlClient.SqlParameter("@DOT_WORKFLOW", SqlDbType.Int)
        pParms(3).Value = 1
        pParms(4) = New SqlClient.SqlParameter("@DOT_EXPIRY", SqlDbType.Int)
        pParms(4).Value = 1
        pParms(5) = New SqlClient.SqlParameter("@DOT_LEADDAYS", SqlDbType.Int)
        pParms(5).Value = txtLeadDays.Text
        pParms(6) = New SqlClient.SqlParameter("@DOT_LIFE", SqlDbType.Int)
        pParms(6).Value = txtLife.Text
        pParms(7) = New SqlClient.SqlParameter("@DOT_ISS_ID", SqlDbType.Int)
        pParms(7).Value = ddlDocumentIssuer.SelectedValue
        Dim rowNum As Integer, bsuIds As String = "", rolIds As String = "", conIds As String = ""
        Dim Item As RepeaterItem
        For Each Item In rptBSUNames1.Items
            Dim chkBSU1 As CheckBox = CType(Item.FindControl("chkBSU1"), CheckBox)
            Dim h_BSUid1 As HiddenField = CType(Item.FindControl("h_BSUid1"), HiddenField)
            If chkBSU1.Checked Then bsuIds = bsuIds & h_BSUid1.Value & "||"
            rowNum = rowNum + 1
        Next
        pParms(8) = New SqlClient.SqlParameter("@DOT_BSU_APPLY", SqlDbType.VarChar, 1000)
        pParms(8).Value = bsuIds
        bsuIds = ""
        For Each Item In rptBSUNames2.Items
            Dim chkBSU2 As CheckBox = CType(Item.FindControl("chkBSU2"), CheckBox)
            Dim h_BSUid2 As HiddenField = CType(Item.FindControl("h_BSUid2"), HiddenField)
            If chkBSU2.Checked Then bsuIds = bsuIds & h_BSUid2.Value & "||"
            rowNum = rowNum + 1
        Next
        pParms(9) = New SqlClient.SqlParameter("@DOT_BSU_SHARE", SqlDbType.VarChar, 1000)
        pParms(9).Value = bsuIds

        For Each Item In rptRole.Items
            Dim chkROL As CheckBox = CType(Item.FindControl("chkROL"), CheckBox)
            Dim h_ROLid As HiddenField = CType(Item.FindControl("h_ROLid"), HiddenField)
            If chkROL.Checked Then rolIds = rolIds & h_ROLid.Value & "||"
            rowNum = rowNum + 1
        Next
        pParms(10) = New SqlClient.SqlParameter("@DOT_ROL_SHARE", SqlDbType.VarChar, 500)
        pParms(10).Value = rolIds

        For Each Item In rptConType.Items
            Dim chkCon As CheckBox = CType(Item.FindControl("chkCON"), CheckBox)
            Dim h_CONid As HiddenField = CType(Item.FindControl("h_CONid"), HiddenField)
            If chkCon.Checked Then conIds = conIds & h_CONid.Value & "||"
            rowNum = rowNum + 1
        Next
        pParms(11) = New SqlClient.SqlParameter("@DOT_CON_TYPE", SqlDbType.VarChar, 100)
        pParms(11).Value = conIds

        pParms(12) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(12).Direction = ParameterDirection.ReturnValue

        pParms(13) = New SqlClient.SqlParameter("@dot_dom_ids", SqlDbType.VarChar)
        pParms(13).Value = h_EMPID.Value

        pParms(14) = New SqlClient.SqlParameter("@dot_DEP_ID", SqlDbType.Int)
        pParms(14).Value = ddlDepartment.SelectedValue

        pParms(15) = New SqlClient.SqlParameter("@DOT_TRACKING_ID", SqlDbType.Int)
        pParms(15).Value = rbTracking.SelectedValue

        pParms(16) = New SqlClient.SqlParameter("@DOT_FULL_VERSION", SqlDbType.Int)
        pParms(16).Value = rbFullVersion.SelectedValue

        pParms(17) = New SqlClient.SqlParameter("@DOT_SOPPATH", SqlDbType.VarChar)
        'pParms(17).Value = txtSOPPath.Text
        pParms(17).Value = encr.Encrypt_SHA256(txtSOPPath.Text)

        pParms(18) = New SqlClient.SqlParameter("@DOT_NOTIFICATION_TEXT", SqlDbType.VarChar)
        pParms(18).Value = txtNotification.Text

        pParms(19) = New SqlClient.SqlParameter("@DOT_CHK_PREAPPROVER", SqlDbType.Int)
        pParms(19).Value = IIf(chkEitherPreApprover.Checked = True, 1, 0)

        pParms(20) = New SqlClient.SqlParameter("@DOT_CITYID", SqlDbType.Int)
        pParms(20).Value = ddlDocumentCity.SelectedValue

        pParms(21) = New SqlClient.SqlParameter("@DOT_DGR_ID", SqlDbType.Int)
        pParms(21).Value = ddlDocumentGroup.SelectedValue

        'pParms(22) = New SqlClient.SqlParameter("@DOT_DPT_ID", SqlDbType.Int)
        'pParms(22).Value = ddlBDepartment.SelectedValue

        'pParms(22) = New SqlClient.SqlParameter("@DOT_DSG_ID", SqlDbType.Int)
        'pParms(22).Value = ddlSubGroup.SelectedValue








        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveDocType", pParms)
            ViewState("EntryId") = pParms(12).Value


            Dim DOCOWNEREowCount As Integer
            For DOCOWNEREowCount = 0 To DTDOCOWNERLIST.Rows.Count - 1
                Dim BlockUsersParms(4) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If DTDOCOWNERLIST.Rows(DOCOWNEREowCount).RowState = 8 Then rowState = -1
                BlockUsersParms(1) = Mainclass.CreateSqlParameter("@DOL_ID", DTDOCOWNERLIST.Rows(DOCOWNEREowCount)("DOL_ID"), SqlDbType.Int, True)
                BlockUsersParms(2) = Mainclass.CreateSqlParameter("@DOL_DOT_ID", IIf(h_EntryId.Value = 0, pParms(12).Value, h_EntryId.Value), SqlDbType.Int)
                BlockUsersParms(3) = Mainclass.CreateSqlParameter("@DOL_DOM_ID", DTDOCOWNERLIST.Rows(DOCOWNEREowCount)("DOL_DOM_ID"), SqlDbType.Int)
                BlockUsersParms(4) = Mainclass.CreateSqlParameter("@DOL_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOCOWNER_LIST", BlockUsersParms)
                If RetValFooter = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next

            If h_DOC_OWNER_DELETED.Value.Length > 0 Then
                Try
                    Dim DelDOcOwnerIDS As String = Mainclass.cleanString(h_DOC_OWNER_DELETED.Value)
                    Dim DeleteDocOwnerStr As String = " update DOC_OWNER_LIST set DOL_DELETED=1, DOL_LOG_USER ='" & Session("sUsr_name") & "', DOL_LOG_DATE = getdate() where DOL_ID in(select id from oasis.dbo.fnSplitMe ('" & DelDOcOwnerIDS & "','|'))"
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteDocOwnerStr)
                Catch ex As Exception
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End Try
            End If


            Dim SubDocRowCount As Integer
            For SubDocRowCount = 0 To DTSubDocList.Rows.Count - 1
                Dim SubDocParams(5) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If DTSubDocList.Rows(SubDocRowCount).RowState = 8 Then rowState = -1
                SubDocParams(1) = Mainclass.CreateSqlParameter("@DTS_ID", DTSubDocList.Rows(SubDocRowCount)("DTS_ID"), SqlDbType.Int, True)
                SubDocParams(2) = Mainclass.CreateSqlParameter("@DTS_DOT_ID", IIf(h_EntryId.Value = 0, pParms(12).Value, h_EntryId.Value), SqlDbType.Int)
                SubDocParams(3) = Mainclass.CreateSqlParameter("@DTS_SUB_DOT_ID", DTSubDocList.Rows(SubDocRowCount)("SUB_DOT_ID"), SqlDbType.Int)
                SubDocParams(4) = Mainclass.CreateSqlParameter("@DTS_ORDER_ID", DTSubDocList.Rows(SubDocRowCount)("ORDNO"), SqlDbType.Int)
                SubDocParams(5) = Mainclass.CreateSqlParameter("@DTS_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)

                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOCTYPE_SUB", SubDocParams)
                If RetValFooter = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next

            If h_Doc_Deleted.Value.Length > 0 Then
                Try
                    Dim DelIds As String = Mainclass.cleanString(h_Doc_Deleted.Value)
                    Dim DeleteDocStr As String = " update doctype_sub set dts_log_deleted=1 where dts_id in(select id from oasis.dbo.fnSplitMe ('" & DelIds & "','|'))"
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteDocStr)
                Catch ex As Exception
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End Try
            End If

            Dim BlockUserRowCount As Integer
            For BlockUserRowCount = 0 To DTDOCBLOCUSERList.Rows.Count - 1
                Dim BlockUsersParms(4) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If DTDOCBLOCUSERList.Rows(BlockUserRowCount).RowState = 8 Then rowState = -1
                BlockUsersParms(1) = Mainclass.CreateSqlParameter("@DBL_ID", DTDOCBLOCUSERList.Rows(BlockUserRowCount)("DBL_ID"), SqlDbType.Int, True)
                BlockUsersParms(2) = Mainclass.CreateSqlParameter("@DBL_DOT_ID", IIf(h_EntryId.Value = 0, pParms(12).Value, h_EntryId.Value), SqlDbType.Int)
                BlockUsersParms(3) = Mainclass.CreateSqlParameter("@DBL_DOM_ID", DTDOCBLOCUSERList.Rows(BlockUserRowCount)("DBL_DOM_ID"), SqlDbType.Int)
                BlockUsersParms(4) = Mainclass.CreateSqlParameter("@DBL_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOCBLOCKING_USER", BlockUsersParms)
                If RetValFooter = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next



            If h_DOC_Blocked_deleted.Value.Length > 0 Then
                Try
                    Dim DelDocBlockedDeletingIds As String = Mainclass.cleanString(h_DOC_Blocked_deleted.Value)
                    Dim DeleteBlcoedStr As String = " update DOCTYPE_BLOCKUSER_LIST Set DBL_LOG_DELETED=1, DBL_LOG_USER ='" & Session("sUsr_name") & "', DBL_LOG_DATE = getdate() where DBL_ID in(select ID from oasis.dbo.fnSplitMe ('" & DelDocBlockedDeletingIds & "','|'))"
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteBlcoedStr)

                Catch ex As Exception
                    'lblError.Text = ex.Message
                    usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End Try
            End If


            Dim RowCount As Integer
            For RowCount = 0 To DOCApproverList.Rows.Count - 1
                Dim ApproverParms(5) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If DOCApproverList.Rows(RowCount).RowState = 8 Then rowState = -1
                ApproverParms(1) = Mainclass.CreateSqlParameter("@DAL_ID", DOCApproverList.Rows(RowCount)("DAL_ID"), SqlDbType.Int, True)
                ApproverParms(2) = Mainclass.CreateSqlParameter("@DAL_DOT_ID", IIf(h_EntryId.Value = 0, pParms(12).Value, h_EntryId.Value), SqlDbType.Int)
                ApproverParms(3) = Mainclass.CreateSqlParameter("@DAL_DOM_ID", DOCApproverList.Rows(RowCount)("DAL_DOM_ID"), SqlDbType.Int)
                ApproverParms(4) = Mainclass.CreateSqlParameter("@DAL_TYPE", DOCApproverList.Rows(RowCount)("DAL_TYPE"), SqlDbType.VarChar)
                ApproverParms(5) = Mainclass.CreateSqlParameter("@DAL_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOCAPPROVER_LIST", ApproverParms)
                If RetValFooter = "-1" Then
                    'lblError.Text = "Unexpected Error !!!"
                    usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next

            If h_DELETED_APPROVER_ID.Value.Length > 0 Then
                Try
                    Dim DelDocApproverIds As String = Mainclass.cleanString(h_DELETED_APPROVER_ID.Value)
                    Dim DeleteApproverDocStr As String = " update DOCTYPE_APPROVER_LIST Set DAL_LOG_DELETED=1, DAL_LOG_USER ='" & Session("sUsr_name") & "', DAL_LOG_DATE = getdate() where dal_id in(select ID from oasis.dbo.fnSplitMe ('" & DelDocApproverIds & "','|'))"
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteApproverDocStr)

                Catch ex As Exception
                    'lblError.Text = ex.Message
                    usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End Try
            End If

            If rbFullVersion.SelectedValue = 1 Then
                Dim FullDocUploadCount As Integer
                For FullDocUploadCount = 0 To DTDOCUPLOADFULLList.Rows.Count - 1
                    Dim FULLDOCUPLOAD(4) As SqlClient.SqlParameter
                    Dim rowState As Integer = ViewState("EntryId")
                    If DTDOCUPLOADFULLList.Rows(FullDocUploadCount).RowState = 8 Then rowState = -1
                    FULLDOCUPLOAD(1) = Mainclass.CreateSqlParameter("@DFU_ID", DTDOCUPLOADFULLList.Rows(FullDocUploadCount)("DFU_ID"), SqlDbType.Int, True)
                    FULLDOCUPLOAD(2) = Mainclass.CreateSqlParameter("@DFU_DOT_ID", IIf(h_EntryId.Value = 0, pParms(12).Value, h_EntryId.Value), SqlDbType.Int)
                    FULLDOCUPLOAD(3) = Mainclass.CreateSqlParameter("@DFU_DOM_ID", DTDOCUPLOADFULLList.Rows(FullDocUploadCount)("DFU_DOM_ID"), SqlDbType.Int)
                    FULLDOCUPLOAD(4) = Mainclass.CreateSqlParameter("@DFU_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)
                    Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_FULLVERSIONUPLOADER_LIST", FULLDOCUPLOAD)
                    If RetValFooter = "-1" Then
                        'lblError.Text = "Unexpected Error !!!"
                        usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        Exit Sub
                    End If
                Next
            End If


            If h_FullDocUploadDeleted.Value.Length > 0 Then
                Try
                    Dim DelDOCFullUploadIds As String = Mainclass.cleanString(h_FullDocUploadDeleted.Value)
                    Dim DeleteFullUploadDocStr As String = " update DOCTYPE_FULLVERSIONUPLOADER_LIST Set DFU_LOG_DELETED=1, DFU_LOG_USER ='" & Session("sUsr_name") & "', DFU_LOG_DATE = getdate() where DFU_ID in(select ID from oasis.dbo.fnSplitMe ('" & DelDOCFullUploadIds & "','|'))"
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteFullUploadDocStr)

                Catch ex As Exception
                    'lblError.Text = ex.Message
                    usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End Try
            End If
            stTrans.Commit()
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            'lblError.Text = "Data Saved Successfully !!!"
            usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
        Catch ex As Exception
            'lblError.Text = getErrorMessage(str_success)
            'lblError.Text = "Unexpected Error !!!"
            usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            objConn.Close()
        End Try
        Dim encObj As New Encryption64
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtDocType.Text, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            Throw New ArgumentException("Could not process your request")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            lblError.Text = ""
            If Not Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")) Is Nothing Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "Delete from DocType where Dot_id=" & h_EntryId.Value)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtDocType.Text, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                Response.Redirect(ViewState("ReferrerUrl"))
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
            usrMessageBar.ShowNotification("The transaction ended in the trigger. The batch has been aborted!!!", UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Protected Sub chkBSUAll1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkBSUAll1.CheckedChanged
        Dim Item As RepeaterItem
        For Each Item In rptBSUNames1.Items
            Dim chkBSU1 As CheckBox = CType(Item.FindControl("chkBSU1"), CheckBox)
            chkBSU1.Checked = chkBSUAll1.Checked
        Next
    End Sub

    Protected Sub chkBSUAll2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkBSUAll2.CheckedChanged
        Dim Item As RepeaterItem
        For Each Item In rptBSUNames2.Items
            Dim chkBSU2 As CheckBox = CType(Item.FindControl("chkBSU2"), CheckBox)
            chkBSU2.Checked = chkBSUAll2.Checked
        Next
    End Sub

    Protected Sub chkRolAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRolAll.CheckedChanged
        Dim Item As RepeaterItem
        For Each Item In rptRole.Items
            Dim chkROL As CheckBox = CType(Item.FindControl("chkROL"), CheckBox)
            chkROL.Checked = chkRolAll.Checked
        Next
    End Sub

    Protected Sub chkConAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkConAll.CheckedChanged
        Dim Item As RepeaterItem
        For Each Item In rptConType.Items
            Dim chkCON As CheckBox = CType(Item.FindControl("chkCON"), CheckBox)
            chkCON.Checked = chkConAll.Checked
        Next
    End Sub
    Protected Sub EMPDELETE_CLICK(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        Dim dt As DataTable, rowtoremove As DataRow
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)

    End Sub
    Protected Sub lnkbtngrdDOCDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDTSID As New Label
        Dim dt As DataTable, rowtoremove As DataRow
        lblDTSID = TryCast(sender.FindControl("lblDTSNO"), Label)
        If Not lblDTSID Is Nothing Then
            If Not Session("dtSubDocuments") Is Nothing Then
                dt = Session("dtSubDocuments")
                If dt.Select("DTS_ID='" & lblDTSID.Text & "'").Length > 0 Then
                    rowtoremove = dt.Select("DTS_ID='" & lblDTSID.Text & "'")(0)
                    dt.Rows.Remove(rowtoremove)
                    dt.AcceptChanges()
                    Session("dtSubDocuments") = dt
                    h_DOCID.Value = ""

                    h_Doc_Deleted.Value = lblDTSID.Text & "|"
                    GridSubDocument.DataSource = dt
                    GridSubDocument.DataBind()
                    Bindsubdoc()
                End If
            End If
        End If
    End Sub
    Private Sub Bindsubdoc()
        For Each row As GridViewRow In GridSubDocument.Rows
            Dim ORDNO As Integer = GridSubDocument.DataKeys(row.RowIndex).Value
            Dim ddOrd As DropDownList = DirectCast(row.FindControl("ddlOrder"), DropDownList)
            ddOrd.SelectedValue = ORDNO
            ddOrd.SelectedItem.Text = ORDNO
        Next
    End Sub
    Private Sub GridSubDocument_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridSubDocument.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgApprover As ImageButton = e.Row.FindControl("imgSubDocumnet")
            Dim txtDescr As TextBox = e.Row.FindControl("txtDocDescr")
            Dim h_DTS_SUB_DOT_ID As HiddenField = e.Row.FindControl("h_DTS_SUB_DOT_ID")
            'imgApprover.Attributes.Add("OnClick", "javascript:return GetSubDocList('" & txtDescr.ClientID & "','" & h_DTS_SUB_DOT_ID.ClientID & "')")
            imgApprover.Attributes.Add("OnClick", "javascript:GetSubDocList('" & txtDescr.ClientID & "','" & h_DTS_SUB_DOT_ID.ClientID & "'); return false;")

        End If

        If grdAppPreAppList.ShowFooter Or grdAppPreAppList.EditIndex > -1 Then
            Dim ddlOrder As DropDownList = e.Row.FindControl("ddlOrder")
            If Not ddlOrder Is Nothing Then
                Dim sqlStr As String = ""

                sqlStr &= "SELECT 0 ORD_ID,'-----Select Order--------' ORD_DESCR UNION ALL SELECT 1 ORD_ID,'1' ORD_DESCR UNION ALL SELECT 2 ORD_ID,'2' ORD_DESCR UNION ALL SELECT 3 ORD_ID,'3' ORD_DESCR UNION ALL SELECT 4 ORD_ID,'4' ORD_DESCR UNION ALL SELECT 5 ORD_ID,'5' ORD_DESCR UNION ALL  "
                sqlStr &= "SELECT 6 ORD_ID,'6' ORD_DESCR UNION ALL SELECT 7 ORD_ID,'7' ORD_DESCR UNION ALL SELECT 8 ORD_ID,'8' ORD_DESCR UNION ALL SELECT 9 ORD_ID,'9' ORD_DESCR UNION ALL SELECT 10 ORD_ID,'10' ORD_DESCR order by ORD_ID "
                fillDropdown(ddlOrder, sqlStr, "ORD_DESCR", "ORD_ID", False)
            End If
        End If


    End Sub

    Private Sub FillApproverList(ByVal Aprid As String)

    End Sub

    Protected Sub lnkbtngrdBlockingDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDBLID As New Label
        Dim dt As DataTable, rowtoremove As DataRow
        lblDBLID = TryCast(sender.FindControl("lblDBL_ID"), Label)
        If Not lblDBLID Is Nothing Then
            If Not Session("dtBlockingUsers") Is Nothing Then
                dt = Session("dtBlockingUsers")
                If dt.Select("DBL_ID='" & lblDBLID.Text & "'").Length > 0 Then
                    rowtoremove = dt.Select("DTS_ID='" & lblDBLID.Text & "'")(0)
                    dt.Rows.Remove(rowtoremove)
                    dt.AcceptChanges()
                    Session("dtBlockingUsers") = dt
                    h_Doc_Blocking_Deleted.Value = lblDBLID.Text & "|"
                    GridSubDocument.DataSource = dt
                    GridSubDocument.DataBind()
                    Bindsubdoc()
                End If
            End If
        End If
    End Sub

    Private Sub grdAppPreAppList_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAppPreAppList.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgApprover As ImageButton = e.Row.FindControl("imgApprover")

            Dim txtDescr As TextBox = e.Row.FindControl("txtDescr")
            Dim h_dom_id As HiddenField = e.Row.FindControl("h_DOM_ID_e")
            'imgApprover.Attributes.Add("OnClick", "javascript:return getProduct('" & txtDescr.ClientID & "','" & h_dom_id.ClientID & "')")
            imgApprover.Attributes.Add("OnClick", "javascript:getProduct('" & txtDescr.ClientID & "','" & h_dom_id.ClientID & "'); return false;")

        End If
        If grdAppPreAppList.ShowFooter Or grdAppPreAppList.EditIndex > -1 Then
            Dim ddlApprover As DropDownList = e.Row.FindControl("ddlApproverType")
            If Not ddlApprover Is Nothing Then
                Dim hProducts As HiddenField = e.Row.FindControl("hProducts")
                Dim sqlStr As String = ""
                Dim sqlStr1 As String = ""
                sqlStr &= "SELECT 0 ID,'--Select Approver--' DESCR UNION ALL SELECT 1 ,'RENEWAL APPROVER' union all select 2 ,'APPROVAL QUEUE' "


                'sqlStr &= "SELECT 1 ,'Pre Approver' union all select 2 ,'Approver' "
                fillDropdown(ddlApprover, sqlStr, "DESCR", "ID", False)
            End If
        End If
    End Sub

    Private Sub grdAppPreAppList_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAppPreAppList.RowCommand
        If e.CommandName = "AddNew" Then

            Dim txtDescr As TextBox = grdAppPreAppList.FooterRow.FindControl("txtDescr")

            Dim hDOM_ID As HiddenField = grdAppPreAppList.FooterRow.FindControl("h_DOM_ID_e")
            Dim ddlAprType As DropDownList = grdAppPreAppList.FooterRow.FindControl("ddlApproverType")
            If DOCApproverList.Rows(0)(1) = -1 Then DOCApproverList.Rows.RemoveAt(0)


            If txtDescr.Text = "" Then
                'lblError.Text = "please select Approver First"
                usrMessageBar.ShowNotification("please select Approver First", UserControls_usrMessageBar.WarningType.Danger)

                Exit Sub
            End If

            If ddlAprType.SelectedValue = 0 Then
                'lblError.Text = "please select proper Approver type"
                usrMessageBar.ShowNotification("please select Approver type", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            Dim mrow As DataRow
            mrow = DOCApproverList.NewRow
            mrow("DAL_ID") = 0
            mrow("DAL_DOM_ID") = hDOM_ID.Value

            mrow("DESCR") = txtDescr.Text
            mrow("Approver_Type") = ddlAprType.SelectedItem.Text
            mrow("DAL_TYPE") = ddlAprType.SelectedValue

            DOCApproverList.Rows.Add(mrow)
            grdAppPreAppList.EditIndex = -1
            grdAppPreAppList.DataSource = DOCApproverList
            grdAppPreAppList.DataBind()
            showNoRecordsFound()

        Else
            Dim txtInv_Rate As TextBox = grdAppPreAppList.FooterRow.FindControl("txtType")
            Dim txtInv_Qty As TextBox = grdAppPreAppList.FooterRow.FindControl("txtDescr")



        End If
    End Sub

    Private Sub grdAppPreAppList_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdAppPreAppList.RowCancelingEdit
        grdAppPreAppList.ShowFooter = True
        grdAppPreAppList.EditIndex = -1
        grdAppPreAppList.DataSource = DOCApproverList
        grdAppPreAppList.DataBind()
    End Sub

    Private Sub grdAppPreAppList_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdAppPreAppList.RowDeleting
        Dim mRow() As DataRow = DOCApproverList.Select("ID=" & grdAppPreAppList.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_DELETED_APPROVER_ID.Value &= mRow(0)("DAL_ID") & "|"
            DOCApproverList.Select("ID=" & grdAppPreAppList.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            DOCApproverList.AcceptChanges()
        End If



        grdAppPreAppList.DataSource = DOCApproverList
        grdAppPreAppList.DataBind()
        showNoRecordsFound()
    End Sub

    Private Sub grdAppPreAppList_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdAppPreAppList.RowEditing
        Try
            grdAppPreAppList.ShowFooter = False
            grdAppPreAppList.EditIndex = e.NewEditIndex
            grdAppPreAppList.DataSource = DOCApproverList
            grdAppPreAppList.DataBind()

            Dim txtDescr As TextBox = grdAppPreAppList.Rows(e.NewEditIndex).FindControl("txtDescr")
            Dim imgApprover As ImageButton = grdAppPreAppList.Rows(e.NewEditIndex).FindControl("imgApprover_e")
            Dim h_dom_id As HiddenField = grdAppPreAppList.Rows(e.NewEditIndex).FindControl("h_DOM_ID_e")
            'imgApprover.Attributes.Add("OnClick", "javascript:return getProduct('" & txtDescr.ClientID & "','" & h_dom_id.ClientID & "')")
            imgApprover.Attributes.Add("OnClick", "javascript:getProduct('" & txtDescr.ClientID & "','" & h_dom_id.ClientID & "'); return false;")


        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdAppPreAppList_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdAppPreAppList.RowUpdating
        Dim s As String = grdAppPreAppList.DataKeys(e.RowIndex).Value.ToString()
        Dim lblDAL_ID As Label = grdAppPreAppList.Rows(e.RowIndex).FindControl("lblDAL_ID")
        Dim ddlAprType As DropDownList = grdAppPreAppList.Rows(e.RowIndex).FindControl("ddlApproverType")
        Dim txtDescr As TextBox = grdAppPreAppList.Rows(e.RowIndex).FindControl("txtDescr")

        Dim hDOM_ID As HiddenField = grdAppPreAppList.Rows(e.RowIndex).FindControl("h_DOM_ID_e")
        Dim mrow As DataRow
        mrow = DOCApproverList.Select("ID=" & s)(0)
        mrow("DAL_ID") = lblDAL_ID.Text
        mrow("DAL_DOM_ID") = hDOM_ID.Value
        mrow("DESCR") = txtDescr.Text
        mrow("Approver_Type") = ddlAprType.SelectedItem.Text
        mrow("DAL_TYPE") = ddlAprType.SelectedValue

        grdAppPreAppList.EditIndex = -1
        grdAppPreAppList.ShowFooter = True
        grdAppPreAppList.DataSource = DOCApproverList
        grdAppPreAppList.DataBind()
    End Sub

    Private Sub GrdUploadFullDocRoles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GrdUploadFullDocRoles.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgApprover As ImageButton = e.Row.FindControl("imgUploader")

            Dim txtDFUDescr As TextBox = e.Row.FindControl("txtDFU_Descr")
            Dim h_DFU_DOM_ID As HiddenField = e.Row.FindControl("h_DFU_DOM_ID")
            'imgApprover.Attributes.Add("OnClick", "javascript:return getFullDocUploader('" & txtDFUDescr.ClientID & "','" & h_DFU_DOM_ID.ClientID & "')")
            'imgApprover.Attributes.Add("OnClick", "javascript:getFullDocUploader('" & txtDFUDescr.ClientID & "','" & h_DFU_DOM_ID.ClientID & "');return false;")

            imgApprover.Attributes.Add("OnClick", "javascript:getProduct('" & txtDFUDescr.ClientID & "','" & h_DFU_DOM_ID.ClientID & "'); return false;")



        End If

    End Sub

    Private Sub GrdUploadFullDocRoles_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrdUploadFullDocRoles.RowCommand
        If e.CommandName = "AddNew" Then

            Dim txtDescr As TextBox = GrdUploadFullDocRoles.FooterRow.FindControl("txtDFU_Descr")
            Dim hDOM_ID As HiddenField = GrdUploadFullDocRoles.FooterRow.FindControl("h_DFU_DOM_ID")
            If DTDOCUPLOADFULLList.Rows(0)(1) = -1 Then DTDOCUPLOADFULLList.Rows.RemoveAt(0)
            If txtDescr.Text = "" Then
                'lblError.Text = "please select Role First"
                usrMessageBar.ShowNotification("please select Role first", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            Dim mrow As DataRow
            mrow = DTDOCUPLOADFULLList.NewRow
            mrow("DFU_ID") = 0
            mrow("DFU_DOM_ID") = hDOM_ID.Value
            mrow("DESCR") = txtDescr.Text
            DTDOCUPLOADFULLList.Rows.Add(mrow)
            GrdUploadFullDocRoles.EditIndex = -1
            GrdUploadFullDocRoles.DataSource = DTDOCUPLOADFULLList
            GrdUploadFullDocRoles.DataBind()
            ShowNoDocUploadRecordsFound()

        Else

            Dim txtInv_Qty As TextBox = GrdUploadFullDocRoles.FooterRow.FindControl("txtDescr")



        End If
    End Sub

    Private Sub rbFullVersion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbFullVersion.SelectedIndexChanged
        If rbFullVersion.SelectedValue = 1 Then
            PnlUploadDocRoles.Visible = True
        Else
            PnlUploadDocRoles.Visible = False
        End If

        fillDOCUploadGrid(GrdUploadFullDocRoles, "SELECT DFU_ID ,DFU_DOM_ID, DESCR FROM DOCTYPE_FULLVERSIONUPLOADER_LIST inner join VW_DOC_DES_LIST on ID=DFU_DOM_ID  where isnull(DFU_LOG_DELETED,0)=0 AND DFU_DOT_ID=0 order by DFU_Id")
        GrdUploadFullDocRoles.DataSource = DTDOCUPLOADFULLList
        GrdUploadFullDocRoles.DataBind()
        ShowNoDocUploadRecordsFound()
    End Sub

    Private Sub GrdUploadFullDocRoles_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles GrdUploadFullDocRoles.RowEditing
        Try
            GrdUploadFullDocRoles.ShowFooter = False
            GrdUploadFullDocRoles.EditIndex = e.NewEditIndex
            GrdUploadFullDocRoles.DataSource = DTDOCUPLOADFULLList
            GrdUploadFullDocRoles.DataBind()

            Dim txtDFU_Descr As TextBox = GrdUploadFullDocRoles.Rows(e.NewEditIndex).FindControl("txtDFU_Descr")
            Dim imgUploader As ImageButton = GrdUploadFullDocRoles.Rows(e.NewEditIndex).FindControl("imgUploader_e")
            Dim h_DFU_dom_id As HiddenField = GrdUploadFullDocRoles.Rows(e.NewEditIndex).FindControl("h_DFU_DOM_ID")
            'imgUploader.Attributes.Add("OnClick", "javascript:return getFullDocUploader('" & txtDFU_Descr.ClientID & "','" & h_DFU_dom_id.ClientID & "')")
            'imgUploader.Attributes.Add("OnClick", "javascript:getFullDocUploader('" & txtDFU_Descr.ClientID & "','" & h_DFU_dom_id.ClientID & "');return false;")
            imgUploader.Attributes.Add("OnClick", "javascript:getProduct('" & txtDFU_Descr.ClientID & "','" & h_DFU_dom_id.ClientID & "'); return false;")


        Catch ex As Exception

        End Try
    End Sub

    Private Sub GrdUploadFullDocRoles_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GrdUploadFullDocRoles.RowUpdating
        Dim s As String = GrdUploadFullDocRoles.DataKeys(e.RowIndex).Value.ToString()
        Dim lblDFU_ID As Label = GrdUploadFullDocRoles.Rows(e.RowIndex).FindControl("lblDFU_ID")
        Dim txtDescr As TextBox = GrdUploadFullDocRoles.Rows(e.RowIndex).FindControl("txtDFU_Descr")

        Dim hDOM_ID As HiddenField = GrdUploadFullDocRoles.Rows(e.RowIndex).FindControl("h_DFU_DOM_ID")
        Dim mrow As DataRow
        mrow = DTDOCUPLOADFULLList.Select("ID=" & s)(0)
        mrow("DFU_ID") = lblDFU_ID.Text
        mrow("DFU_DOM_ID") = hDOM_ID.Value

        mrow("DESCR") = txtDescr.Text

        GrdUploadFullDocRoles.EditIndex = -1
        GrdUploadFullDocRoles.ShowFooter = True
        GrdUploadFullDocRoles.DataSource = DTDOCUPLOADFULLList
        GrdUploadFullDocRoles.DataBind()
    End Sub

    Private Sub GrdUploadFullDocRoles_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GrdUploadFullDocRoles.RowDeleting
        Dim mRow() As DataRow = DTDOCUPLOADFULLList.Select("ID=" & GrdUploadFullDocRoles.DataKeys(e.RowIndex).Values(0), "")

        If mRow.Length > 0 Then
            h_FullDocUploadDeleted.Value &= mRow(0)("DFU_ID") & "|"
            DTDOCUPLOADFULLList.Select("ID=" & GrdUploadFullDocRoles.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            DTDOCUPLOADFULLList.AcceptChanges()
        End If
        GrdUploadFullDocRoles.DataSource = DTDOCUPLOADFULLList
        GrdUploadFullDocRoles.DataBind()
        ShowNoDocUploadRecordsFound()
    End Sub

    Private Sub grdBlockUserList_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdBlockUserList.RowCommand
        If e.CommandName = "AddNew" Then

            Dim txtDescr As TextBox = grdBlockUserList.FooterRow.FindControl("txtDBL_Descr")

            Dim hDOM_ID As HiddenField = grdBlockUserList.FooterRow.FindControl("h_DBL_DOM_ID")
            If DTDOCBLOCUSERList.Rows(0)(1) = -1 Then DTDOCBLOCUSERList.Rows.RemoveAt(0)

            If txtDescr.Text = "" Then
                'lblError.Text = "please select User First"
                usrMessageBar.ShowNotification("please select user first", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            Dim mrow As DataRow
            mrow = DTDOCBLOCUSERList.NewRow
            mrow("DBL_ID") = 0
            mrow("DBL_DOM_ID") = hDOM_ID.Value

            mrow("DESCR") = txtDescr.Text

            DTDOCBLOCUSERList.Rows.Add(mrow)
            grdBlockUserList.EditIndex = -1
            grdBlockUserList.DataSource = DTDOCBLOCUSERList
            grdBlockUserList.DataBind()
            showNoRecordsFoundBLOCKUSERS()

        Else

            Dim txtInv_Qty As TextBox = grdBlockUserList.FooterRow.FindControl("txtDBL_Descr")



        End If
    End Sub

    Private Sub grdBlockUserList_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdBlockUserList.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgApprover As ImageButton = e.Row.FindControl("imgBlocker")

            Dim txtDBLDescr As TextBox = e.Row.FindControl("txtDBL_Descr")
            Dim h_DBL_DOM_ID As HiddenField = e.Row.FindControl("h_DBL_DOM_ID")
            'imgApprover.Attributes.Add("OnClick", "javascript:return getProduct('" & txtDBLDescr.ClientID & "','" & h_DBL_DOM_ID.ClientID & "')")
            imgApprover.Attributes.Add("OnClick", "javascript:getProduct('" & txtDBLDescr.ClientID & "','" & h_DBL_DOM_ID.ClientID & "');return false;")



        End If
    End Sub

    Private Sub grdBlockUserList_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdBlockUserList.RowDeleting


        Dim mRow() As DataRow = DTDOCBLOCUSERList.Select("ID=" & grdBlockUserList.DataKeys(e.RowIndex).Values(0), "")

        If mRow.Length > 0 Then
            h_DOC_Blocked_deleted.Value &= mRow(0)("DBL_ID") & "|"
            DTDOCBLOCUSERList.Select("ID=" & grdBlockUserList.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            DTDOCBLOCUSERList.AcceptChanges()
        End If
        grdBlockUserList.DataSource = DTDOCBLOCUSERList
        grdBlockUserList.DataBind()
        showNoRecordsFoundBLOCKUSERS()
    End Sub

    Private Sub grdBlockUserList_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdBlockUserList.RowEditing
        Try
            grdBlockUserList.ShowFooter = False
            grdBlockUserList.EditIndex = e.NewEditIndex
            grdBlockUserList.DataSource = DTDOCBLOCUSERList
            grdBlockUserList.DataBind()

            Dim txtDBL_Descr As TextBox = grdBlockUserList.Rows(e.NewEditIndex).FindControl("txtDBL_Descr")
            Dim imgBlocker As ImageButton = grdBlockUserList.Rows(e.NewEditIndex).FindControl("imgBlocker_e")
            Dim h_DBL_dom_id As HiddenField = grdBlockUserList.Rows(e.NewEditIndex).FindControl("h_DBL_DOM_ID")
            'imgBlocker.Attributes.Add("OnClick", "javascript:return getProduct('" & txtDBL_Descr.ClientID & "','" & h_DBL_dom_id.ClientID & "')")
            imgBlocker.Attributes.Add("OnClick", "javascript:getProduct('" & txtDBL_Descr.ClientID & "','" & h_DBL_dom_id.ClientID & "');return false;")



        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdBlockUserList_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdBlockUserList.RowUpdating
        Dim s As String = grdBlockUserList.DataKeys(e.RowIndex).Value.ToString()
        Dim lblDFU_ID As Label = grdBlockUserList.Rows(e.RowIndex).FindControl("lblDBL_ID")
        Dim txtDescr As TextBox = grdBlockUserList.Rows(e.RowIndex).FindControl("txtDBL_Descr")

        Dim hDOM_ID As HiddenField = grdBlockUserList.Rows(e.RowIndex).FindControl("h_DBL_DOM_ID")
        Dim mrow As DataRow
        mrow = DTDOCBLOCUSERList.Select("ID=" & s)(0)
        mrow("DBL_ID") = lblDFU_ID.Text
        mrow("DBL_DOM_ID") = hDOM_ID.Value

        mrow("DESCR") = txtDescr.Text

        grdBlockUserList.EditIndex = -1
        grdBlockUserList.ShowFooter = True
        grdBlockUserList.DataSource = DTDOCBLOCUSERList
        grdBlockUserList.DataBind()
    End Sub

    Private Sub GrdUploadFullDocRoles_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles GrdUploadFullDocRoles.RowCancelingEdit
        GrdUploadFullDocRoles.ShowFooter = True
        GrdUploadFullDocRoles.EditIndex = -1
        GrdUploadFullDocRoles.DataSource = DTDOCUPLOADFULLList
        GrdUploadFullDocRoles.DataBind()

    End Sub

    Private Sub grdBlockUserList_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdBlockUserList.RowCancelingEdit
        grdBlockUserList.ShowFooter = True
        grdBlockUserList.EditIndex = -1
        grdBlockUserList.DataSource = DTDOCBLOCUSERList
        grdBlockUserList.DataBind()
    End Sub

    Private Sub grdDocOwner_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocOwner.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgDocOwner As ImageButton = e.Row.FindControl("imgDocOwner")
            'Dim txtDOLType As TextBox = e.Row.FindControl("txtDOL_Type")
            Dim txtDOLDescr As TextBox = e.Row.FindControl("txtDOL_Descr")
            Dim h_DOL_DOM_ID As HiddenField = e.Row.FindControl("h_DOL_DOM_ID")
            'imgDocOwner.Attributes.Add("OnClick", "javascript:return getFullDocUploader('" & txtDOLDescr.ClientID & "','" & h_DOL_DOM_ID.ClientID & "')")
            imgDocOwner.Attributes.Add("OnClick", "javascript:getProduct('" & txtDOLDescr.ClientID & "','" & h_DOL_DOM_ID.ClientID & "'); return false;")
        End If
    End Sub

    Private Sub grdDocOwner_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDocOwner.RowCommand
        If e.CommandName = "AddNew" Then

            Dim txtDescr As TextBox = grdDocOwner.FooterRow.FindControl("txtDOL_Descr")

            Dim hDOM_ID As HiddenField = grdDocOwner.FooterRow.FindControl("h_DOL_DOM_ID")


            If DTDOCOWNERLIST.Rows(0)(1) = -1 Then DTDOCOWNERLIST.Rows.RemoveAt(0)
            If txtDescr.Text = "" Then
                'lblError.Text = "please select Document owner First"
                usrMessageBar.ShowNotification("please select Document Owner first", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            Dim mrow As DataRow
            mrow = DTDOCOWNERLIST.NewRow
            mrow("DOL_ID") = 0
            mrow("DOL_DOM_ID") = hDOM_ID.Value

            mrow("DESCR") = txtDescr.Text

            DTDOCOWNERLIST.Rows.Add(mrow)
            grdDocOwner.EditIndex = -1
            grdDocOwner.DataSource = DTDOCOWNERLIST
            grdDocOwner.DataBind()
            showNoRecordsFoundOWNERUSERS()

        Else
            Dim txtInv_Rate As TextBox = grdDocOwner.FooterRow.FindControl("txtType")
            Dim txtInv_Qty As TextBox = grdDocOwner.FooterRow.FindControl("txtDescr")



        End If
    End Sub
    Private Sub grdDocOwner_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdDocOwner.RowEditing
        Try
            grdDocOwner.ShowFooter = False
            grdDocOwner.EditIndex = e.NewEditIndex
            grdDocOwner.DataSource = DTDOCOWNERLIST
            grdDocOwner.DataBind()

            Dim txtDOL_Descr As TextBox = grdDocOwner.Rows(e.NewEditIndex).FindControl("txtDOL_Descr")
            Dim imgDocOwner_e As ImageButton = grdDocOwner.Rows(e.NewEditIndex).FindControl("imgDocOwner_e")
            Dim h_DOL_dom_id As HiddenField = grdDocOwner.Rows(e.NewEditIndex).FindControl("h_DOL_DOM_ID")
            'imgDocOwner_e.Attributes.Add("OnClick", "javascript:return getFullDocUploader('" & txtDOL_Descr.ClientID & "','" & h_DOL_dom_id.ClientID & "')")
            imgDocOwner_e.Attributes.Add("OnClick", "javascript:getProduct('" & txtDOL_Descr.ClientID & "','" & h_DOL_dom_id.ClientID & "'); return false;")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub grdDocOwner_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdDocOwner.RowUpdating
        Dim s As String = grdDocOwner.DataKeys(e.RowIndex).Value.ToString()
        Dim lblDOL_ID As Label = grdDocOwner.Rows(e.RowIndex).FindControl("lblDOL_ID")
        Dim txtDescr As TextBox = grdDocOwner.Rows(e.RowIndex).FindControl("txtDOL_Descr")

        Dim hDOL_ID As HiddenField = grdDocOwner.Rows(e.RowIndex).FindControl("h_DOL_DOM_ID")
        Dim mrow As DataRow
        mrow = DTDOCOWNERLIST.Select("ID=" & s)(0)
        mrow("DOL_ID") = lblDOL_ID.Text
        mrow("DOL_DOM_ID") = hDOL_ID.Value
        mrow("DESCR") = txtDescr.Text

        grdDocOwner.EditIndex = -1
        grdDocOwner.ShowFooter = True
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
    End Sub

    Private Sub grdDocOwner_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdDocOwner.RowCancelingEdit
        grdDocOwner.ShowFooter = True
        grdDocOwner.EditIndex = -1
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
    End Sub

    Private Sub grdDocOwner_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdDocOwner.RowDeleting

        Dim mRow() As DataRow = DTDOCOWNERLIST.Select("ID=" & grdDocOwner.DataKeys(e.RowIndex).Values(0), "")

        If mRow.Length > 0 Then
            h_DOC_OWNER_DELETED.Value &= mRow(0)("DOL_ID") & "|"
            DTDOCOWNERLIST.Select("ID=" & grdDocOwner.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            DTDOCOWNERLIST.AcceptChanges()
        End If
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub

    Private Sub GridSubDocument_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridSubDocument.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtDescr As TextBox = GridSubDocument.FooterRow.FindControl("txtDocDescr")
            Dim h_DTS_SUB_DOT_ID As HiddenField = GridSubDocument.FooterRow.FindControl("h_DTS_SUB_DOT_ID")
            Dim ddlOrder As DropDownList = GridSubDocument.FooterRow.FindControl("ddlOrder")
            If DTSubDocList.Rows(0)(1) = -1 Then DTSubDocList.Rows.RemoveAt(0)

            If txtDescr.Text = "" Then
                'lblError.Text = "please select Subdocument First"
                usrMessageBar.ShowNotification("please select Subdocument First", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If


            If ddlOrder.SelectedValue = 0 Then
                'lblError.Text = "please select Correct Order"
                usrMessageBar.ShowNotification("please select Correct order", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If



            Dim mrow As DataRow
            mrow = DTSubDocList.NewRow
            mrow("DTS_ID") = 0
            mrow("SUB_DOT_ID") = h_DTS_SUB_DOT_ID.Value
            mrow("DESCR") = txtDescr.Text
            mrow("ORDNO") = ddlOrder.SelectedValue

            DTSubDocList.Rows.Add(mrow)
            GridSubDocument.EditIndex = -1
            GridSubDocument.DataSource = DTSubDocList
            GridSubDocument.DataBind()
            showNoRecordsFoundSubDocument()






        Else





        End If
    End Sub

    Private Sub GridSubDocument_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GridSubDocument.RowDeleting

        Dim mRow() As DataRow = DTSubDocList.Select("DTS_ID=" & GridSubDocument.DataKeys(e.RowIndex).Values(0), "")

        If mRow.Length > 0 Then
            h_Doc_Deleted.Value &= mRow(0)("DTS_ID") & "|"
            DTSubDocList.Select("DTS_ID=" & GridSubDocument.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            DTSubDocList.AcceptChanges()
        End If
        GridSubDocument.DataSource = DTSubDocList
        GridSubDocument.DataBind()
        showNoRecordsFoundSubDocument()
    End Sub

    Private Sub GridSubDocument_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GridSubDocument.RowUpdating
        Dim s As String = GridSubDocument.DataKeys(e.RowIndex).Value.ToString()
        Dim lblDTS_ID As Label = GridSubDocument.Rows(e.RowIndex).FindControl("lblDTSNO")
        Dim ddlOrder As DropDownList = GridSubDocument.Rows(e.RowIndex).FindControl("ddlOrder")
        Dim txtDescr As TextBox = GridSubDocument.Rows(e.RowIndex).FindControl("txtDocDescr")

        Dim hSUBDOT_ID As HiddenField = GridSubDocument.Rows(e.RowIndex).FindControl("h_DTS_SUB_DOT_ID")
        Dim mrow As DataRow
        mrow = DTSubDocList.Select("DTS_ID=" & s)(0)
        mrow("DTS_ID") = lblDTS_ID.Text
        mrow("SUB_DOT_ID") = hSUBDOT_ID.Value
        mrow("DESCR") = txtDescr.Text
        mrow("ORDNO") = ddlOrder.SelectedValue
        DTSubDocList.AcceptChanges()

        GridSubDocument.EditIndex = -1
        GridSubDocument.ShowFooter = True
        GridSubDocument.DataSource = DTSubDocList
        GridSubDocument.DataBind()





    End Sub

    Private Sub GridSubDocument_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles GridSubDocument.RowEditing
        Try
            GridSubDocument.ShowFooter = False
            GridSubDocument.EditIndex = e.NewEditIndex
            GridSubDocument.DataSource = DTSubDocList
            GridSubDocument.DataBind()

            Dim txtDescr_E As TextBox = GridSubDocument.Rows(e.NewEditIndex).FindControl("txtDocDescr")
            Dim imgSubDocE As ImageButton = GridSubDocument.Rows(e.NewEditIndex).FindControl("imgSubDocumnet_e")
            Dim h_SUBDOC_E As HiddenField = GridSubDocument.Rows(e.NewEditIndex).FindControl("h_DTS_SUB_DOT_ID")
            'imgSubDocE.Attributes.Add("OnClick", "javascript:return GetSubDocList('" & txtDescr_E.ClientID & "','" & h_SUBDOC_E.ClientID & "')")
            imgSubDocE.Attributes.Add("OnClick", "javascript:GetSubDocList('" & txtDescr_E.ClientID & "','" & h_SUBDOC_E.ClientID & "'); return false;")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GridSubDocument_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles GridSubDocument.RowCancelingEdit
        GridSubDocument.ShowFooter = True
        GridSubDocument.EditIndex = -1
        GridSubDocument.DataSource = DTSubDocList
        GridSubDocument.DataBind()
    End Sub


    Private Property DOCApproverList() As DataTable
        Get
            Return ViewState("DOCApproverList")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DOCApproverList") = value
        End Set
    End Property
    Private Property DTDOCBLOCUSERList() As DataTable
        Get
            Return ViewState("DTDOCBLOCUSERList")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTDOCBLOCUSERList") = value
        End Set
    End Property
    Private Property DTDOCUPLOADFULLList() As DataTable
        Get
            Return ViewState("DTDOCUPLOADFULLList")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTDOCUPLOADFULLList") = value
        End Set
    End Property

    Private Property DTDOCOWNERLIST() As DataTable
        Get
            Return ViewState("DTDOCOWNERLIST")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTDOCOWNERLIST") = value
        End Set
    End Property

    Private Property DTSubDocList() As DataTable
        Get
            Return ViewState("DTSubDocList")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTSubDocList") = value
        End Set
    End Property



End Class
