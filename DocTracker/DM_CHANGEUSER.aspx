﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_CHANGEUSER.aspx.vb" Inherits="DocTracker_DM_CHANGEUSER" Title="Change User" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>


    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />

     <script>
    function getEMPList(EMPNAME, EMPID) {
            var pMode;
        var NameandCode;
        //pMode = "GetEmpBSUList"
            pMode = "GetEmpBSUListNEW"
            document.getElementById('<%=hf_1.ClientID%>').value = EMPNAME;
             document.getElementById('<%=hf_2.ClientID%>').value = EMPID;

            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&EmpBsu=EMP";
            var oWnd = radopen(url, "pop_emp");
         }

         function getEMPListA(EMPNAME, EMPID) {
            var pMode;
            var NameandCode;
             //pMode = "GetEmpBSUList"
             pMode = "GetEmpBSUListNEW"
            document.getElementById('<%=hf_1.ClientID%>').value = EMPNAME;
             document.getElementById('<%=hf_2.ClientID%>').value = EMPID;

            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&EmpBsu=EMP";
            var oWnd = radopen(url, "pop_emp1");
         }



    function OnClientCloseS(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtOldEmployee.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=hOLD_EMP_ID.ClientID %>").value = NameandCode[0];
                //document.getElementById(document.getElementById("<%=txtOldEmployee.ClientID%>").value).disabled = true;
                //document.getElementById(document.getElementById("<%=hOLD_EMP_ID.ClientID%>").value).disabled = true;
                __doPostBack('<%= txtOldEmployee.ClientID%>', '');
            }
         }

         function OnClientCloseA(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txtNewEmployee.ClientID %>").value = NameandCode[2];
                document.getElementById("<%=hNEW_EMP_ID.ClientID %>").value = NameandCode[0];
                document.getElementById(document.getElementById("<%=txtNewEmployee.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hNEW_EMP_ID.ClientID%>").value).disabled = true;
            }
         }


         function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
</script>
    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseS"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_emp1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseA"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Change User master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Role</span></td>

                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtOldEmployee" runat="server" OnTextChanged="txtOldEmployee_TextChanged" AutoPostBack="true"  ></asp:TextBox>
                                        <asp:ImageButton ID="imgOldEmp" runat="server" OnClientClick="getEMPList();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hOLD_EMP_ID" runat="server" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Assign Role</span></td>

                                    <td align="left" width="30%"  >
                                        <asp:TextBox ID="txtNewEmployee" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgNewEmp" runat="server" OnClientClick="getEMPListA();return false;" ImageUrl="~/Images/forum_search.gif" TabIndex="8" />
                                        <asp:HiddenField ID="hNEW_EMP_ID" runat="server" />
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                    
                                </tr>
                                <tr>
                                    <td colspan ="4">

                                        <asp:GridView ID="grdDocOwner" runat="server" AutoGenerateColumns="False" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="10"
                                                CssClass="table table-bordered table-row" DataKeyNames="ID" ShowFooter="True">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="EMP NO" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDOMF_ID" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="NAME" ItemStyle-Width="30%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPType" runat="server" Style="text-align: left" Width="450px"
                                                                Text='<%# Bind("NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%" Visible="false"  >
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDOMF_type" Width="150px" Enabled="false" runat="server" Text='<%# Bind("TYPE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="DESCRIPTION" ItemStyle-Width="30%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDOMF_Descr" runat="server"  Width="450px" Text='<%# Bind("DESCRIPTION") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="BSU" ItemStyle-Width="30%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDOMF_Descr"  Width="450px" runat="server" Text='<%# Bind("BSU") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    


                                                </Columns>

                                                <HeaderStyle />

                                            </asp:GridView>

                                    </td>
                                </tr>
                                

                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                    </td>
                                </tr>
                                <asp:HiddenField ID="hf_EMPNAMEid" runat="server" />
                <asp:HiddenField ID="hf_EMPID" runat="server" />
                
                <asp:HiddenField ID="hf_1" runat="server" />
                <asp:HiddenField ID="hf_2" runat="server" />
                                
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <uc1:usrMessageBar runat="server" ID="usrMessageBar1" />
    </div>

</asp:Content>

