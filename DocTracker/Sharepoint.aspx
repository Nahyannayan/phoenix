﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Sharepoint.aspx.vb" Inherits="Fees_Sharepoint" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <table>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="matters">Select File&nbsp;
            </td>
            <td class="matters">:</td>
            <td class="matters" align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="matters">Library List</td>
            <td class="matters">:</td>
            <td class="matters" align="left">
                <asp:DropDownList ID="ddlLibrary" AutoPostBack="true" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="matters">Folder List</td>
            <td class="matters">:</td>
            <td class="matters" align="left">
                <asp:DropDownList ID="ddlFolders" AutoPostBack="true" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="matters">Select File</td>
            <td class="matters">:</td>
            <td class="matters" align="left">
                <asp:FileUpload ID="fup" runat="server" /><asp:Button ID="btnUpload" runat="server" Text="UPLOAD" />
            </td>
        </tr>
        <tr>
            <td class="matters">Files List</td>
            <td class="matters">:</td>
            <td class="matters" align="left">
                <asp:DropDownList ID="ddlFiles" AutoPostBack="true" runat="server">
                </asp:DropDownList>
            &nbsp;</td>
        </tr>
        <tr>
            <td class="matters">&nbsp;</td>
            <td class="matters">&nbsp;</td>
            <td class="matters" align="left">
                <asp:LinkButton ID="lbtnView" runat="server">View File</asp:LinkButton>
                <asp:Button ID="btnDelete" runat="server" Text="DELETE" />
            </td>
        </tr>
    </table>
</asp:Content>

