﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj


Partial Class DOCTRACKER_DM_DOCOWNER
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Session("sUsr_name") = "" Or (ViewState("MainMnu_code") <> "DM00022") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))

            Else
                SetDataMode("add")
                setModifyvalues(0)
            End If
        Else

        End If
        'rblOwnerType.Enabled = False
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        Dim EditAllowed As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            EditAllowed = False

            rblOwnerType.Enabled = False
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

            mDisable = False
            EditAllowed = True
            'ClearDetails()
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            EditAllowed = True
        End If

        txtProfileID.Enabled = EditAllowed
        txtProfileName.Enabled = EditAllowed
        ImageDriver.Enabled = EditAllowed
        rblOwnerType.Enabled = EditAllowed
        btnsave.Visible = Not mDisable

        rbType.Enabled = EditAllowed
        txtDescr.Enabled = EditAllowed
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        PnlDocRoles.Enabled = EditAllowed
        ddlAssignUserList.Enabled = EditAllowed
        chkViewOnly.Enabled = EditAllowed


    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        h_EntryId.Value = p_Modifyid
        If p_Modifyid = 0 Then

            If rblOwnerType.SelectedValue = 0 Then
                h_BSUorSCHOOL.Value = "DESG"
            Else
                h_BSUorSCHOOL.Value = "EMP"
            End If
        Else
            Dim dt As New DataTable, strSql As New StringBuilder
            strSql.Append("SELECT DOM_ID ID,DOM_ID [No],DOM_DESCR,DOM_TYPE,isnull(DOM_ASSIGN_TYPE,'')DOM_ASSIGN_TYPE,isnull(DOM_bOnlyViewAccess,0)DOM_bOnlyViewAccess from DOC_OWNER_MASTER   ")

            strSql.Append(" where isnull(DEN_LOG_DELETED,0)=0 and  DOM_ID=" & h_EntryId.Value)
            dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
            If dt.Rows.Count > 0 Then
                txtDescr.Text = dt.Rows(0)("DOM_DESCR")
                rbType.SelectedValue = dt.Rows(0)("DOM_TYPE")
                If rbType.SelectedValue = "ASSIGNED" Then
                    ddlAssignUserList.Visible = True
                    ddlAssignUserList.SelectedValue = dt.Rows(0)("DOM_ASSIGN_TYPE")
                End If
                If dt.Rows(0)("DOM_bOnlyViewAccess") = True Then
                    chkViewOnly.Checked = True
                Else
                    chkViewOnly.Checked = False
                End If
            End If

        End If
        Bindgrid()

    End Sub
    Private Sub Bindgrid()

        fillGridView_DOCDOCOwners(grdDocOwner, "Select DOMF_ID ,DOMF_DOM_ID,Case When DOMF_TYPE='DESIGNATION' then   'Designation' else 'Employee' end STYPE, DOMF_PROFILEID EMPNO,case when DOMF_TYPE='EMPLOYEE' then isnull(EMP_FNAME,'') +' ' + isnull(EMP_MNAME,'') +' ' + isnull(EMP_LNAME,'') else DES_DESCR end  As DESCR From DOC_OWNER_MASTER_F left outer Join OASIS..EMPDESIGNATION_M WITh(NOLOCK)On DES_ID=DOMF_PROFILEID left outer Join  OASIS..EMPLOYEE_m WITh(NOLOCK) On EMP_ID=DOMF_PROFILEID where isnull(DOMF_DELETED, 0) =0  And DOMF_DOM_ID= " & h_EntryId.Value & "   order by DOMF_Id")

        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
        showNoRecordsFoundOWNERUSERS()



        ddlAssignUserList.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select AUT_TYPE,AUT_DESCR from AssignedUser_Types union all select '0', '--------Select One--------'  order by AUT_DESCR")
        ddlAssignUserList.DataTextField = "AUT_DESCR"
        ddlAssignUserList.DataValueField = "AUT_TYPE"
        ddlAssignUserList.DataBind()

    End Sub
    Private Sub fillGridView_DOCDOCOwners(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DTDOCOWNERLIST = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTDOCOWNERLIST)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTDOCOWNERLIST = mtable
        fillGrdView.DataSource = DTDOCOWNERLIST
        fillGrdView.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Dim objConn As New SqlConnection(connectionString)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim pParms(6) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@DOM_ID", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@DOM_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@DOM_TYPE", rbType.SelectedValue, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@DOM_DESCR", txtDescr.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@DOM_ASSIGN_TYPE", ddlAssignUserList.SelectedValue, SqlDbType.VarChar)
            If chkViewOnly.Checked = True Then
                pParms(6) = Mainclass.CreateSqlParameter("@VIEWONLY", 1, SqlDbType.VarChar)
            Else
                pParms(6) = Mainclass.CreateSqlParameter("@VIEWONLY", 0, SqlDbType.VarChar)
            End If


            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOC_OWNERMASTER", pParms)

            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If


            Dim DOCOWNERRowCount As Integer
            For DOCOWNERRowCount = 0 To DTDOCOWNERLIST.Rows.Count - 1
                Dim OwnerParms(5) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If DTDOCOWNERLIST.Rows(DOCOWNERRowCount).RowState = 8 Then rowState = -1
                OwnerParms(1) = Mainclass.CreateSqlParameter("@DOMF_ID", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("DOMF_ID"), SqlDbType.Int, True)
                OwnerParms(2) = Mainclass.CreateSqlParameter("@DOMF_DOM_ID", IIf(h_EntryId.Value = 0, pParms(1).Value, h_EntryId.Value), SqlDbType.Int)
                OwnerParms(3) = Mainclass.CreateSqlParameter("@DOMF_PROFILEID", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("EMPNO"), SqlDbType.Int)
                OwnerParms(4) = Mainclass.CreateSqlParameter("@DOMF_TYPE", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("STYPE"), SqlDbType.VarChar)
                OwnerParms(5) = Mainclass.CreateSqlParameter("@DOMF_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOC_OWNER_MASTER_F", OwnerParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next


            Dim pParms1(1) As SqlParameter
            pParms1(1) = Mainclass.CreateSqlParameter("@DOM_ID", IIf(h_EntryId.Value = 0, pParms(1).Value, h_EntryId.Value), SqlDbType.Int)

            Dim RetVal1 As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOC_OWNER_MASTER_UPDATE", pParms1)

            If RetVal1 = "-1" Then
                lblError.Text = "Unexpected Error"
                stTrans.Rollback()
                Exit Sub
            End If


            If h_DOC_OWNER_DELETED.Value.Length > 0 Then
                Try
                    Dim DelDOcOwnerIDS As String = Mainclass.cleanString(h_DOC_OWNER_DELETED.Value)
                    Dim DeleteDocOwnerStr As String = " update DOC_OWNER_MASTER_F set DOMF_DELETED=1, DOMF_USER ='" & Session("sUsr_name") & "', DOMF_LOGDATE = getdate() where DOMF_ID in(select id from oasis.dbo.fnSplitMe ('" & DelDOcOwnerIDS & "','|'))"
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteDocOwnerStr)
                Catch ex As Exception
                    lblError.Text = "Unexpected Error"
                    stTrans.Rollback()
                    Exit Sub
                End Try
            End If





            stTrans.Commit()
            lblError.Text = "Data Saved Successfylly"
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))


        Catch ex As Exception
            setModifyvalues(ViewState("EntryId"))
            stTrans.Rollback()
            lblError.Text = ex.Message
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Private Sub rblOwnerType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblOwnerType.SelectedIndexChanged
        If rblOwnerType.SelectedValue = 0 Then
            h_BSUorSCHOOL.Value = "DESG"
            h_Profile_ID.Value = 0
            txtProfileID.Text = ""
            txtProfileName.Text = ""
        Else
            h_BSUorSCHOOL.Value = "EMP"
            h_Profile_ID.Value = 0
            txtProfileID.Text = ""
            txtProfileName.Text = ""
        End If

    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
    End Sub
    Private Sub ClearDetails()
        txtProfileID.Text = ""
        txtProfileName.Text = ""
        h_EntryId.Value = 0
        h_Profile_ID.Value = 0
        rblOwnerType.Enabled = True
        txtDescr.Text = ""

    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If Not Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")) Is Nothing Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "UPDATE DOC_OWNER_MASTER SET DEN_LOG_DELETED=1 where DOM_ID=" & h_EntryId.Value)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                Response.Redirect(ViewState("ReferrerUrl"))
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub grdDocOwner_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdDocOwner.RowCancelingEdit
        grdDocOwner.ShowFooter = True
        grdDocOwner.EditIndex = -1
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
    End Sub

    Private Sub grdDocOwner_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDocOwner.RowCommand


        If e.CommandName = "AddNew" Then

            Dim txtType As TextBox = grdDocOwner.FooterRow.FindControl("txtDOMF_type")
            Dim txtDescr As TextBox = grdDocOwner.FooterRow.FindControl("txtDOMF_Descr")
            Dim hDOM_ID As HiddenField = grdDocOwner.FooterRow.FindControl("h_DOMF_DOM_ID")

            Dim ddlOwnerType As DropDownList = grdDocOwner.FooterRow.FindControl("ddlOwnerType")

            If ddlOwnerType.SelectedValue = "0" Then
                lblError.Text = "please select Correct Owner Type"
                Exit Sub

            End If

            If DTDOCOWNERLIST.Rows(0)(1) = -1 Then DTDOCOWNERLIST.Rows.RemoveAt(0)
            If txtDescr.Text = "" Then
                lblError.Text = "please select Correct Owner"
                Exit Sub
            End If


            Dim mrow As DataRow
            mrow = DTDOCOWNERLIST.NewRow
            mrow("DOMF_ID") = 0
            mrow("EMPNO") = hDOM_ID.Value

            mrow("STYPE") = txtType.Text
            mrow("DESCR") = txtDescr.Text
            DTDOCOWNERLIST.Rows.Add(mrow)
            grdDocOwner.EditIndex = -1
            grdDocOwner.DataSource = DTDOCOWNERLIST
            grdDocOwner.DataBind()
            showNoRecordsFoundOWNERUSERS()
        Else
            Dim txtInv_Rate As TextBox = grdDocOwner.FooterRow.FindControl("txtType")
            Dim txtInv_Qty As TextBox = grdDocOwner.FooterRow.FindControl("txtDescr")
        End If
    End Sub
    Private Sub showNoRecordsFoundOWNERUSERS()
        If DTDOCOWNERLIST.Rows.Count > 0 Then
            If DTDOCOWNERLIST.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdDocOwner.Columns.Count - 2
                grdDocOwner.Rows(0).Cells.Clear()
                grdDocOwner.Rows(0).Cells.Add(New TableCell())
                grdDocOwner.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdDocOwner.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If

    End Sub

    Private Sub grdDocOwner_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocOwner.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgDocOwner As ImageButton = e.Row.FindControl("imgDocOwner")
            Dim txtDOLType As TextBox = e.Row.FindControl("txtDOMF_type")
            Dim txtDOLDescr As TextBox = e.Row.FindControl("txtDOMF_Descr")
            Dim h_DOL_DOM_ID As HiddenField = e.Row.FindControl("h_DOMF_DOM_ID")
            Dim ddlOwnerType As DropDownList = e.Row.FindControl("ddlOwnerType")
            'imgDocOwner.Attributes.Add("OnClick", "javascript:return getFullDocUploader('" & txtDOLType.ClientID & "','" & txtDOLDescr.ClientID & "','" & h_DOL_DOM_ID.ClientID & "','" & ddlOwnerType.ClientID & "')")
            imgDocOwner.Attributes.Add("OnClick", "javascript:getFullDocUploader('" & txtDOLType.ClientID & "','" & txtDOLDescr.ClientID & "','" & h_DOL_DOM_ID.ClientID & "','" & ddlOwnerType.ClientID & "');return false")
        End If


        If grdDocOwner.ShowFooter Or grdDocOwner.EditIndex > -1 Then
            Dim ddlOwnerType As DropDownList = e.Row.FindControl("ddlOwnerType")
            If Not ddlOwnerType Is Nothing Then
                Dim hProducts As HiddenField = e.Row.FindControl("hProducts")
                Dim sqlStr As String = ""
                Dim sqlStr1 As String = ""
                sqlStr &= "SELECT '0' ID,'--Select One--' DESCR UNION ALL SELECT 'EMP' ,'Employee' union all select 'DESG' ,'Designation' "
                'sqlStr &= "SELECT 1 ,'Pre Approver' union all select 2 ,'Approver' "
                fillDropdown(ddlOwnerType, sqlStr, "DESCR", "ID", False)
            End If
        End If

        If rbType.SelectedValue = "ASSIGNED" Then
            'For Each gvr As GridViewRow In grdDocOwner.Rows
            '    gvr.Cells(4).Visible = False
            '    gvr.Cells(5).Visible = False
            'Next
            grdDocOwner.Columns(4).Visible = False
            grdDocOwner.Columns(5).Visible = False
        Else
            'For Each gvr As GridViewRow In grdDocOwner.Rows
            '    gvr.Cells(4).Visible = True
            '    gvr.Cells(5).Visible = True
            'Next
            grdDocOwner.Columns(4).Visible = True
            grdDocOwner.Columns(5).Visible = True

        End If

    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()

    End Sub

    Private Sub grdDocOwner_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdDocOwner.RowDeleting
        Dim mRow() As DataRow = DTDOCOWNERLIST.Select("DOMF_ID=" & grdDocOwner.DataKeys(e.RowIndex).Values(0), "")

        If mRow.Length > 0 Then
            h_DOC_OWNER_DELETED.Value &= mRow(0)("DOMF_ID") & "|"
            DTDOCOWNERLIST.Select("DOMF_ID=" & grdDocOwner.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            DTDOCOWNERLIST.AcceptChanges()
        End If
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub

    Private Sub grdDocOwner_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdDocOwner.RowEditing
        Try
            grdDocOwner.ShowFooter = False
            grdDocOwner.EditIndex = e.NewEditIndex
            grdDocOwner.DataSource = DTDOCOWNERLIST
            grdDocOwner.DataBind()
            Dim txtDOL_Type As TextBox = grdDocOwner.Rows(e.NewEditIndex).FindControl("txtDOMF_type")
            Dim txtDOL_Descr As TextBox = grdDocOwner.Rows(e.NewEditIndex).FindControl("txtDOMF_Descr")
            Dim imgDocOwner_e As ImageButton = grdDocOwner.Rows(e.NewEditIndex).FindControl("imgDocOwner_e")
            Dim h_DOL_dom_id As HiddenField = grdDocOwner.Rows(e.NewEditIndex).FindControl("h_DOMF_DOM_ID")
            Dim ddlOwnerType As DropDownList = grdDocOwner.Rows(e.NewEditIndex).FindControl("ddlOwnerType")
            'imgDocOwner_e.Attributes.Add("OnClick", "javascript:return getFullDocUploader('" & txtDOL_Type.ClientID & "','" & txtDOL_Descr.ClientID & "','" & h_DOL_dom_id.ClientID & "','" & ddlOwnerType.ClientID & "')")
            imgDocOwner_e.Attributes.Add("OnClick", "javascript:getFullDocUploader('" & txtDOL_Type.ClientID & "','" & txtDOL_Descr.ClientID & "','" & h_DOL_dom_id.ClientID & "','" & ddlOwnerType.ClientID & "');return false")


        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdDocOwner_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdDocOwner.RowUpdating
        Dim s As String = grdDocOwner.DataKeys(e.RowIndex).Value.ToString()

        Dim lblDOMF_ID As Label = grdDocOwner.Rows(e.RowIndex).FindControl("lblDOMF_ID")
        Dim txtDescr As TextBox = grdDocOwner.Rows(e.RowIndex).FindControl("txtDOMF_Descr")
        Dim txtType As TextBox = grdDocOwner.Rows(e.RowIndex).FindControl("txtDOMF_type")
        Dim h_DOMF_DOM_ID As HiddenField = grdDocOwner.Rows(e.RowIndex).FindControl("h_DOMF_DOM_ID")

        Dim mrow As DataRow
        mrow = DTDOCOWNERLIST.Select("ID=" & s)(0)
        mrow("DOMF_ID") = lblDOMF_ID.Text
        mrow("EMPNO") = h_DOMF_DOM_ID.Value
        mrow("STYPE") = txtType.Text
        mrow("DESCR") = txtDescr.Text





        grdDocOwner.EditIndex = -1
        grdDocOwner.ShowFooter = True
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
    End Sub

    Private Property DTDOCOWNERLIST() As DataTable
        Get
            Return ViewState("DTDOCOWNERLIST")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTDOCOWNERLIST") = value
        End Set
    End Property

    Protected Sub rbType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbType.SelectedIndexChanged
        If rbType.SelectedValue = "ASSIGNED" Then
            ddlAssignUserList.Visible = True
            grdDocOwner.ShowFooter = False
            DisableEdit()
        Else
            ddlAssignUserList.Visible = False
            grdDocOwner.ShowFooter = True
        End If
        fillGridView_DOCDOCOwners(grdDocOwner, "Select 0 DOMF_ID ,0 DOMF_DOM_ID,'Employee' STYPE, BAU_EMP_ID EMPNO,isnull(EMP_FNAME,'') +' ' + isnull(EMP_MNAME,'') +' ' + isnull(EMP_LNAME,'') DESCR From BSU_AssignedFor_Users  left outer Join  OASIS..EMPLOYEE_m WITh(NOLOCK) On EMP_ID=BAU_EMP_ID where isnull(BAU_bDeleted,0) =0  And BAU_TYPE=''")
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub
    Private Sub DisableEdit()
        If rbType.SelectedValue = "ASSIGNED" Then
            'For Each gvr As GridViewRow In grdDocOwner.Rows
            '    gvr.Cells(4).Visible = False
            '    gvr.Cells(5).Visible = False
            'Next
            grdDocOwner.Columns(4).Visible = False
            grdDocOwner.Columns(5).Visible = False

        Else
            'For Each gvr As GridViewRow In grdDocOwner.Rows
            '    gvr.Cells(4).Visible = True
            '    gvr.Cells(5).Visible = True
            'Next
            grdDocOwner.Columns(4).Visible = True
            grdDocOwner.Columns(5).Visible = True
        End If



    End Sub

    Protected Sub ddlAssignUserList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssignUserList.SelectedIndexChanged
        fillGridView_DOCDOCOwners(grdDocOwner, "Select 0 DOMF_ID ,0 DOMF_DOM_ID,'Employee' STYPE, BAU_EMP_ID EMPNO,isnull(EMP_FNAME,'') +' ' + isnull(EMP_MNAME,'') +' ' + isnull(EMP_LNAME,'') DESCR From BSU_AssignedFor_Users  left outer Join  OASIS..EMPLOYEE_m WITh(NOLOCK) On EMP_ID=BAU_EMP_ID where isnull(BAU_bDeleted,0) =0  And BAU_TYPE='" & ddlAssignUserList.SelectedValue & "'")
        grdDocOwner.DataSource = DTDOCOWNERLIST
        grdDocOwner.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub
End Class
