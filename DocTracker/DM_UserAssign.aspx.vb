﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Partial Class DocTracker_DM_UserAssign
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Session("sUsr_name") = "" Or (ViewState("MainMnu_code") <> "DM00021") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))

            Else
                SetDataMode("add")
                setModifyvalues(0)
            End If
        Else

        End If
        'rblOwnerType.Enabled = False
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        Dim EditAllowed As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
            EditAllowed = False
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

            mDisable = False
            EditAllowed = True
            'ClearDetails()
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            EditAllowed = True
        End If
        btnsave.Visible = Not mDisable
        txtDescr.Enabled = EditAllowed
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        PnlDocRoles.Enabled = EditAllowed


    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        h_EntryId.Value = p_Modifyid
        If p_Modifyid = 0 Then

            
        Else
            Dim dt As New DataTable, strSql As New StringBuilder
            strSql.Append("SELECT AUT_ID,AUT_TYPE,AUT_DESCR from AssignedUser_Types   ")
            strSql.Append(" where AUT_ID=" & h_EntryId.Value)
            dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, strSql.ToString).Tables(0)
            If dt.Rows.Count > 0 Then
                txtDescr.Text = dt.Rows(0)("AUT_DESCR")
                h_type.Value = dt.Rows(0)("AUT_TYPE")
            End If
        End If
        Bindgrid(IIf(p_Modifyid = 0, "Nothing", h_type.Value))
    End Sub
    Private Sub Bindgrid(ByVal AUT_TYPE As String)

        fillGridView_DOCDOCOwners(grdAssignedUser, "Select BAU_ID ,BAU_EMP_ID,BAU_TYPE,BAU_EMP_ID EMPNO,isnull(EMP_FNAME,'') +' ' + isnull(EMP_MNAME,'') +' ' + isnull(EMP_LNAME,'') As DESCR,BSU_NAME BSU,BAU_FROMDT,BAU_TODT,BAU_BSU_ID From BSU_AssignedFor_Users  left outer Join  OASIS..EMPLOYEE_m WITh(NOLOCK) On EMP_ID=BAU_EMP_ID left outer Join  OASIS..businessunit_m WITh(NOLOCK) On BSU_ID=BAU_BSU_ID  where  BAU_TYPE ='" & AUT_TYPE & "' and isnull(BAU_bDeleted,0)=0 order by isnull(EMP_FNAME,'') +' ' + isnull(EMP_MNAME,'') +' ' + isnull(EMP_LNAME,'') ")
        grdAssignedUser.DataSource = DTDOCOWNERLIST
        grdAssignedUser.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub
    Private Sub fillGridView_DOCDOCOwners(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DTDOCOWNERLIST = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTDOCOWNERLIST)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTDOCOWNERLIST = mtable
        fillGrdView.DataSource = DTDOCOWNERLIST
        fillGrdView.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Dim objConn As New SqlConnection(connectionString)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim pParms(2) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@AUT_ID", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@AUT_DESCR", txtDescr.Text, SqlDbType.VarChar)

            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_AssignedUser_Types", pParms)

            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If



            Dim DOCOWNERRowCount As Integer
            For DOCOWNERRowCount = 0 To DTDOCOWNERLIST.Rows.Count - 1
                Dim OwnerParms(7) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If DTDOCOWNERLIST.Rows(DOCOWNERRowCount).RowState = 8 Then rowState = -1
                OwnerParms(1) = Mainclass.CreateSqlParameter("@BAU_ID", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("BAU_ID"), SqlDbType.Int, True)

                If h_EntryId.Value = 0 Then
                    OwnerParms(2) = Mainclass.CreateSqlParameter("@BAU_TYPE", "DOC_" + Replace(RTrim(LTrim(txtDescr.Text)), " ", "_"), SqlDbType.VarChar)
                Else
                    OwnerParms(2) = Mainclass.CreateSqlParameter("@BAU_TYPE", h_type.Value, SqlDbType.VarChar)
                End If
                OwnerParms(3) = Mainclass.CreateSqlParameter("@BAU_EMP_ID", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("BAU_EMP_ID"), SqlDbType.VarChar)
                OwnerParms(4) = Mainclass.CreateSqlParameter("@BAU_BSU_ID", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("BAU_BSU_ID"), SqlDbType.VarChar)
                OwnerParms(5) = Mainclass.CreateSqlParameter("@BAU_FROMDT", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("BAU_FROMDT"), SqlDbType.DateTime)
                OwnerParms(6) = Mainclass.CreateSqlParameter("@BAU_TODT", DTDOCOWNERLIST.Rows(DOCOWNERRowCount)("BAU_TODT"), SqlDbType.DateTime)
                OwnerParms(7) = Mainclass.CreateSqlParameter("@BAU_USER", Session("sUsr_name"), SqlDbType.VarChar)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_BSU_AssignedFor_Users", OwnerParms)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next

            Dim pParms2(1) As SqlParameter
            If h_EntryId.Value = 0 Then
                pParms2(1) = Mainclass.CreateSqlParameter("@AUT_TYPE", "DOC_" + Replace(RTrim(LTrim(txtDescr.Text)), " ", "_"), SqlDbType.VarChar)
            Else
                pParms2(1) = Mainclass.CreateSqlParameter("@AUT_TYPE", h_type.Value, SqlDbType.VarChar)
            End If


            Dim RetVal1 As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_AssignedUser_UPDATE", pParms2)

            If RetVal1 = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            End If




            'If h_DOC_OWNER_DELETED.Value.Length > 0 Then
            '    Try
            '        Dim DelDOcOwnerIDS As String = Mainclass.cleanString(h_DOC_OWNER_DELETED.Value)
            '        Dim DeleteDocOwnerStr As String = " update DOC_OWNER_MASTER_F set DOMF_DELETED=1, DOMF_USER ='" & Session("sUsr_name") & "', DOMF_LOGDATE = getdate() where DOMF_ID in(select id from oasis.dbo.fnSplitMe ('" & DelDOcOwnerIDS & "','|'))"
            '        SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteDocOwnerStr)
            '    Catch ex As Exception
            '        lblError.Text = "Unexpected Error !!!"
            '        stTrans.Rollback()
            '        Exit Sub
            '    End Try
            'End If





            stTrans.Commit()
            lblError.Text = "Data Saved Successfully...!"
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))


        Catch ex As Exception
            setModifyvalues(ViewState("EntryId"))
            stTrans.Rollback()
            lblError.Text = ex.Message
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
   
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
    End Sub
    Private Sub ClearDetails()
        
        h_EntryId.Value = 0
        h_Profile_ID.Value = 0
        txtDescr.Text = ""

    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If Not Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")) Is Nothing Then
                SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "UPDATE DOC_OWNER_MASTER SET DEN_LOG_DELETED=1 where DOM_ID=" & h_EntryId.Value)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_EntryId.Value, "DELETE", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                Response.Redirect(ViewState("ReferrerUrl"))
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub grdAssignedUser_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdAssignedUser.RowCancelingEdit
        grdAssignedUser.ShowFooter = True
        grdAssignedUser.EditIndex = -1
        grdAssignedUser.DataSource = DTDOCOWNERLIST
        grdAssignedUser.DataBind()
    End Sub

    Private Sub grdAssignedUser_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAssignedUser.RowCommand


        If e.CommandName = "AddNew" Then

            Dim txtEmpName As TextBox = grdAssignedUser.FooterRow.FindControl("txtEmpName")
            Dim txtBSU As TextBox = grdAssignedUser.FooterRow.FindControl("txtBSU")
            Dim h_EMP_ID As HiddenField = grdAssignedUser.FooterRow.FindControl("h_EMP_ID")
            Dim h_BSU_ID As HiddenField = grdAssignedUser.FooterRow.FindControl("h_BSU_ID")
            Dim txtFromdate As TextBox = grdAssignedUser.FooterRow.FindControl("txtFromDate")
            Dim txtToDate As TextBox = grdAssignedUser.FooterRow.FindControl("txtToDate")
            If DTDOCOWNERLIST.Rows(0)(1) = -1 Then DTDOCOWNERLIST.Rows.RemoveAt(0)
            Dim mrow As DataRow
            mrow = DTDOCOWNERLIST.NewRow
            mrow("BAU_ID") = 0
            mrow("BAU_EMP_ID") = h_EMP_ID.Value
            mrow("DESCR") = txtEmpName.Text
            mrow("BSU") = txtBSU.Text
            mrow("BAU_BSU_ID") = h_BSU_ID.Value
            mrow("BAU_FROMDT") = IIf(txtFromdate.Text = "", "01-Jan-1900", txtFromdate.Text)
            mrow("BAU_TODT") = IIf(txtToDate.Text = "", "01-Jan-1900", txtFromdate.Text)
            DTDOCOWNERLIST.Rows.Add(mrow)
            grdAssignedUser.EditIndex = -1
            grdAssignedUser.DataSource = DTDOCOWNERLIST
            grdAssignedUser.DataBind()
            showNoRecordsFoundOWNERUSERS()
        Else
            Dim txtInv_Rate As TextBox = grdAssignedUser.FooterRow.FindControl("txtEmpName")
            Dim txtInv_Qty As TextBox = grdAssignedUser.FooterRow.FindControl("txtBSU")
        End If
    End Sub
    Private Sub showNoRecordsFoundOWNERUSERS()
        If DTDOCOWNERLIST.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = grdAssignedUser.Columns.Count - 2
            grdAssignedUser.Rows(0).Cells.Clear()
            grdAssignedUser.Rows(0).Cells.Add(New TableCell())
            grdAssignedUser.Rows(0).Cells(0).ColumnSpan = TotalColumns
            grdAssignedUser.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Private Sub grdAssignedUser_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAssignedUser.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim imgDocOwner As ImageButton = e.Row.FindControl("imgEMPID")
            Dim imgBSU As ImageButton = e.Row.FindControl("imgBSU")
            Dim txtEmpName As TextBox = e.Row.FindControl("txtEmpName")
            Dim txtBSU As TextBox = e.Row.FindControl("txtBSU")
            Dim h_EMP_ID As HiddenField = e.Row.FindControl("h_EMP_ID")
            Dim h_BSU_ID As HiddenField = e.Row.FindControl("h_BSU_ID")
            imgDocOwner.Attributes.Add("OnClick", "javascript:getEMPList('" & txtEmpName.ClientID & "','" & h_EMP_ID.ClientID & "'); return false;")
            imgBSU.Attributes.Add("OnClick", "javascript:getBSUList('" & txtBSU.ClientID & "','" & h_BSU_ID.ClientID & "');return false;")
        End If


        'If grdAssignedUser.ShowFooter Or grdAssignedUser.EditIndex > -1 Then
        '    Dim ddlOwnerType As DropDownList = e.Row.FindControl("ddlOwnerType")
        '    If Not ddlOwnerType Is Nothing Then
        '        Dim hProducts As HiddenField = e.Row.FindControl("hProducts")
        '        Dim sqlStr As String = ""
        '        Dim sqlStr1 As String = ""
        '        sqlStr &= "SELECT '0' ID,'--Select One--' DESCR UNION ALL SELECT 'EMP' ,'Employee' union all select 'DESG' ,'Designation' "
        '        'sqlStr &= "SELECT 1 ,'Pre Approver' union all select 2 ,'Approver' "
        '        fillDropdown(ddlOwnerType, sqlStr, "DESCR", "ID", False)
        '    End If
        'End If
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()

    End Sub

    Private Sub grdAssignedUser_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdAssignedUser.RowDeleting
        'Dim mRow() As DataRow = DTDOCOWNERLIST.Select("DOMF_ID=" & grdAssignedUser.DataKeys(e.RowIndex).Values(0), "")

        'If mRow.Length > 0 Then
        '    h_DOC_OWNER_DELETED.Value &= mRow(0)("DOMF_ID") & "|"
        '    DTDOCOWNERLIST.Select("DOMF_ID=" & grdAssignedUser.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
        '    DTDOCOWNERLIST.AcceptChanges()
        'End If
        'grdAssignedUser.DataSource = DTDOCOWNERLIST
        'grdAssignedUser.DataBind()
        'showNoRecordsFoundOWNERUSERS()
    End Sub

    Private Sub grdAssignedUser_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdAssignedUser.RowEditing
        Try
            grdAssignedUser.ShowFooter = False
            grdAssignedUser.EditIndex = e.NewEditIndex
            grdAssignedUser.DataSource = DTDOCOWNERLIST
            grdAssignedUser.DataBind()

            Dim imgDocOwner_e As ImageButton = grdAssignedUser.Rows(e.NewEditIndex).FindControl("imgEMPID_e")
            Dim imgBSU_e As ImageButton = grdAssignedUser.Rows(e.NewEditIndex).FindControl("imgBSU_e")


            Dim txtEmpName As TextBox = grdAssignedUser.Rows(e.NewEditIndex).FindControl("txtEmpName")
            Dim txtBSU As TextBox = grdAssignedUser.Rows(e.NewEditIndex).FindControl("txtBSU")
            Dim h_EMP_ID As HiddenField = grdAssignedUser.Rows(e.NewEditIndex).FindControl("h_EMP_ID")
            Dim h_BSU_ID As HiddenField = grdAssignedUser.Rows(e.NewEditIndex).FindControl("h_BSU_ID")


            imgDocOwner_e.Attributes.Add("OnClick", "javascript:getEMPList('" & txtEmpName.ClientID & "','" & h_EMP_ID.ClientID & "');return false;")
            imgBSU_e.Attributes.Add("OnClick", "javascript:getBSUList('" & txtBSU.ClientID & "','" & h_BSU_ID.ClientID & "');return false;")





        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdAssignedUser_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdAssignedUser.RowUpdating
        Dim s As String = grdAssignedUser.DataKeys(e.RowIndex).Value.ToString()




        Dim lblBAU_ID As Label = grdAssignedUser.Rows(e.RowIndex).FindControl("lblBAU_ID")
        Dim txtEmpName As TextBox = grdAssignedUser.Rows(e.RowIndex).FindControl("txtEmpName")
        Dim txtBSU As TextBox = grdAssignedUser.Rows(e.RowIndex).FindControl("txtBSU")
        Dim h_EMP_ID As HiddenField = grdAssignedUser.Rows(e.RowIndex).FindControl("h_EMP_ID")
        Dim h_BSU_ID As HiddenField = grdAssignedUser.Rows(e.RowIndex).FindControl("h_BSU_ID")
        Dim txtFromDate As TextBox = grdAssignedUser.Rows(e.RowIndex).FindControl("txtFromDate")
        Dim txtToDate As TextBox = grdAssignedUser.Rows(e.RowIndex).FindControl("txtToDate")




        


        Dim mrow As DataRow
        mrow = DTDOCOWNERLIST.Select("BAU_ID=" & s)(0)
        mrow("BAU_ID") = lblBAU_ID.Text
        mrow("BAU_EMP_ID") = h_EMP_ID.Value
        mrow("DESCR") = txtEmpName.Text
        mrow("BSU") = txtBSU.Text
        mrow("BAU_BSU_ID") = h_BSU_ID.Value
        mrow("BAU_FROMDT") = txtFromDate.Text
        mrow("BAU_TODT") = txtToDate.Text






       

        grdAssignedUser.EditIndex = -1
        grdAssignedUser.ShowFooter = True
        grdAssignedUser.DataSource = DTDOCOWNERLIST
        grdAssignedUser.DataBind()
    End Sub

    Private Property DTDOCOWNERLIST() As DataTable
        Get
            Return ViewState("DTDOCOWNERLIST")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTDOCOWNERLIST") = value
        End Set
    End Property

End Class

