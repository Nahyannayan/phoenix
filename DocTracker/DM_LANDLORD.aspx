﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_LANDLORD.aspx.vb" Inherits="DocTracker_DM_LANDLORD" Title="Add/Edit Landlord / Supplier Master" %>



<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Landlord / Supplier Master Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDESCR" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Type</span></td>
                                    <td align="left" width="30%">
                                        <asp:RadioButton ID="radLanlord" runat="server" GroupName="LandlordSupplier" Text="Landlord" Checked="true"  />
                                        <asp:RadioButton ID="radSupplier" runat="server" GroupName="LandlordSupplier" Checked="false" Text="Supplier"  />


                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
        </div>
    </div>

</asp:Content>

