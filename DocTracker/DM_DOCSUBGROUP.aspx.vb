﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Partial Class DocTracker_DM_DOCSUBGROUP
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "DM00025" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                Dim encObj As New Encryption64
                SetDataMode("view")
                setModifyvalues(encObj.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            End If
            ddlDep.DataSource = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select DDT_ID,DDT_DEPARTMENT_DESCR from DOC_DEPARTMENT union all select 0,'----Select One----' order by DDT_DEPARTMENT_DESCR")
            ddlDep.DataTextField = "DDT_DEPARTMENT_DESCR"
            ddlDep.DataValueField = "DDT_ID"
            ddlDep.DataBind()
        End If
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        txtDocSUBGroup.Enabled = EditAllowed
        txtDetails.Enabled = EditAllowed
        txtShortName.Enabled = EditAllowed
        ddlDep.Enabled = EditAllowed
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim str_Sql As String
                str_Sql = "select * FROM DocSubGroup where DSG_ID='" & p_Modifyid & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    h_EntryId.Value = ds.Tables(0).Rows(0)("DSG_Id")
                    txtDetails.Text = ds.Tables(0).Rows(0)("DSG_Details")
                    txtShortName.Text = ds.Tables(0).Rows(0)("DSG_ShortName")

                    txtDocSUBGroup.Text = ds.Tables(0).Rows(0)("DSG_Descr")
                    ddlDep.SelectedValue = ds.Tables(0).Rows(0)("DSG_DDT_ID")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        setModifyvalues(0)
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub ClearDetails()
        txtDocSUBGroup.Text = ""
        txtDetails.Text = ""
        txtShortName.Text = ""
        ddlDep.SelectedIndex = 0
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtDocSUBGroup.Text.Trim.Length = 0 Then
            usrMessageBar.ShowNotification("Document Sub Group description should not be empty", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim pParms(10) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@DSG_ID", SqlDbType.Int)
        pParms(1).Value = h_EntryId.Value
        pParms(2) = New SqlClient.SqlParameter("@DSG_DESCR", SqlDbType.VarChar, 100)
        pParms(2).Value = txtDocSUBGroup.Text
        pParms(3) = New SqlClient.SqlParameter("@DSG_DDT_ID", SqlDbType.Int)
        pParms(3).Value = ddlDep.SelectedValue
        pParms(4) = New SqlClient.SqlParameter("@DSG_DETAILS", SqlDbType.VarChar, 500)
        pParms(4).Value = txtDetails.Text
        pParms(5) = New SqlClient.SqlParameter("@DSG_PHONE", SqlDbType.VarChar, 100)
        pParms(5).Value = ""
        pParms(6) = New SqlClient.SqlParameter("@DSG_FAX", SqlDbType.VarChar, 100)
        pParms(6).Value = ""
        pParms(7) = New SqlClient.SqlParameter("@DSG_EMAIL", SqlDbType.VarChar, 100)
        pParms(7).Value = ""
        pParms(8) = New SqlClient.SqlParameter("@DSG_WEBSITE", SqlDbType.VarChar, 100)
        pParms(8).Value = ""
        pParms(9) = New SqlClient.SqlParameter("@DSG_SHORTNAME", SqlDbType.VarChar, 8)
        pParms(9).Value = txtShortName.Text

        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "saveDocSubGroup", pParms)
            ViewState("EntryId") = pParms(10).Value
            stTrans.Commit()
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            usrMessageBar.ShowNotification("Data Saved Successfully.", UserControls_usrMessageBar.WarningType.Success)
        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            Exit Sub
        Finally
            objConn.Close()
        End Try
        Dim encObj As New Encryption64
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtDocSUBGroup.Text, ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
        If flagAudit <> 0 Then
            usrMessageBar.ShowNotification("Could not process your request", UserControls_usrMessageBar.WarningType.Danger)
            'Throw New ArgumentException("Could not process your request")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
       
    End Sub

End Class

