﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class DOCTRACKER_SelectDocEmpList_DM
    Inherits System.Web.UI.Page
    'Shared Session("liUserList") As List(Of String)
    Shared multiSel As Boolean
    Private Property FilterCondition() As String
        Get
            Return ViewState("FilterCondition")
        End Get
        Set(ByVal value As String)
            ViewState("FilterCondition") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If


        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        multiSel = IIf(Request.QueryString("multiSelect") = "true", True, False)

        Dim strMultiSel As String = Request.QueryString("multiSelect")
        If strMultiSel <> String.Empty And strMultiSel <> "" Then
            If String.Compare("False", strMultiSel, True) = 0 Then
                gvGroup.Columns(0).Visible = False
                gvGroup.Columns(2).Visible = False
                gvGroup.Columns(3).Visible = True
                'DropDownList1.Visible = False
                btnFinish.Visible = False
                chkSelAll.Visible = False
            Else
                gvGroup.Columns(0).Visible = True
                gvGroup.Columns(2).Visible = True
                gvGroup.Columns(3).Visible = False
                'DropDownList1.Visible = True
                btnFinish.Visible = True
            End If
        Else
            multiSel = True
            gvGroup.Columns(0).Visible = True
            gvGroup.Columns(2).Visible = True
            gvGroup.Columns(3).Visible = False
            'DropDownList1.Visible = True
            btnFinish.Visible = True
        End If
        If Page.IsPostBack = False Then

            h_Selected_menu_1.Value = "LI__../../../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../../../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../../../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../../../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../../../Images/operations/like.gif"

            Session("liUserList") = New List(Of String)

            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            GridBind()
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbindBSUnit()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql, str_filter_code, str_filter_name, str_mode, str_txtCode, str_txtName, str_BSUName As String
            Dim BUnitreaderSuper As SqlDataReader
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim tblbUsr_id As String = Session("sUsr_id")
            Dim tblbUSuper As Boolean = Session("sBusper")

            Dim bGetAll As Boolean = IIf(Request.QueryString("getall") = "true", True, False)
            If bGetAll Then
                str_query_header = "select BSU_ID as ID, '' EMP_NO, BSU_NAME as DESCR from BUSINESSUNIT_M where 1=1 " & "|" & "BSU ID|BSU Name"
            Else
                str_mode = ""

                str_txtCode = ""
                str_txtName = ""
                str_BSUName = ""
                If tblbUSuper = True Then
                    BUnitreaderSuper = AccessRoleUser.GetBusinessUnits()
                Else
                    BUnitreaderSuper = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                End If

                Dim strAccessibleBSUIDs As String = String.Empty

                If BUnitreaderSuper.HasRows = True Then
                    Dim bBUnitreaderSuper As Boolean = BUnitreaderSuper.Read()
                    While (bBUnitreaderSuper)
                        strAccessibleBSUIDs += "'" + BUnitreaderSuper(0) + "'"
                        bBUnitreaderSuper = BUnitreaderSuper.Read()
                        If bBUnitreaderSuper Then
                            strAccessibleBSUIDs += ","
                        End If

                    End While
                End If
                str_query_header = "select BSU_ID as ID,'' EMP_NO, BSU_NAME as DESCR from BUSINESSUNIT_M where BSU_ID in(" & strAccessibleBSUIDs & ")" & "|" & "BSU ID|BSU Name"
            End If
            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BSU_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BSU_NAME", str_Sid_search(0), str_txtName)

                ''column1
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindCAT()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "SELECT ECT_ID as ID, '' EMP_NO, ECT_DESCR as DESCR FROM EMPCATEGORY_M where 1=1 " & "|" & "CAT ID|CAT Name"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox


            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ECT_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ECT_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Sub GridBindEMP_ALLALLOC()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim dt As String = Format(DateSerial(Request.QueryString("year"), Request.QueryString("month"), 1), "dd/MMM/yyyy")
            str_query_header = "SELECT DISTINCT ID,EMP_NO, DESCR FROM (select EMP_ID ID, empno EMP_NO , ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') as DESCR " _
            & " from VW_OSO_EMPLOYEE_ALLOCATION where 1=1 " & Session("EMP_SEL_COND") _
              & ") a WHERE 1=1 "
            '& " AND dbo.fN_GetLastDayOFmonth('" & dt & "') BETWEEN dbo.fN_GetLastDayOFmonth(EAL_FROMDT) AND  dbo.fN_GetLastDayOFmonth(ISNULL(EAL_TODT, '" & dt & "')) " _

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMP_NO", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName
            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE NO"

            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub GridBindEMP_ALLOC()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim dt As String = Format(DateSerial(Request.QueryString("year"), Request.QueryString("month"), 1), "dd/MMM/yyyy")
            str_query_header = "SELECT distinct ID,EMP_NO, DESCR FROM (select EMP_ID ID, empno EMP_NO , ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') as DESCR " _
            & " from VW_OSO_EMPLOYEE_ALLOCATION where 1=1 " & Session("EMP_SEL_COND") _
            & " AND dbo.fN_GetLastDayOFmonth('" & dt & "') BETWEEN dbo.fN_GetLastDayOFmonth(EAL_FROMDT) AND  dbo.fN_GetLastDayOFmonth(ISNULL(EAL_TODT, '" & dt & "')) " _
            & ") a WHERE 1=1 "
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMP_NO", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName
            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE NO"

            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindEMP()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "SELECT ID,EMP_NO, DESCR FROM (select EMP_ID ID, empno EMP_NO , ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') as DESCR from EMPLOYEE_M where 1=1 " & Session("EMP_SEL_COND") & " ) a WHERE 1=1 "

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMP_NO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName
            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE NO"

            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"

            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindOwner()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "select ID,DESCR,[Owner Type],[OWNER ID] from (select DOM_ID ID,case when DOM_OWNER_TYPE='DESG' then DES_DESCR else ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '')  end DESCR,case when DOM_OWNER_TYPE='DESG' then 'Designation' else 'Employee'  end [Owner Type] "
            str_query_header = str_query_header & ",DOM_OWNER_ID [OWNER ID] From DOC_OWNER_MASTER left outer join oasis..EMPLOYEE_M On emp_id=DOM_OWNER_ID"
            str_query_header = str_query_header & " Left outer join oasis..EMPDESIGNATION_M on DES_ID =DOM_OWNER_ID) a where 1=1"


            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMP_NO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName
            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE NO"

            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"

            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Sub GridBindDocuments()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_Sql As String
            Dim docType As String = Request.QueryString("docname")
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "select ID,DESCR from (select dot_id ID,dot_descr DESCR,dot_id [Owner id]  from doctype where dot_descr not in('" & docType & "')) a where 1=1"


            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMP_NO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName
            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE NO"

            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"

            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindBlockUserList()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_Sql As String
            Dim docType As String = Request.QueryString("docname")
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim Chkwhere As String = " 1=1"
            str_query_header = "select ID,DESCR from (select ID , DESCR  from VW_DOC_DES_LIST ) a where" & Chkwhere


            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMP_NO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName
            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE NO"

            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"

            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindApproverList()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString
            Dim str_Sql As String
            Dim docType As String = Request.QueryString("docname")
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            Dim Chkwhere As String = " 1=1"
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""

            str_query_header = "select ID,DESCR from (select Case ID , DESCR  from VW_DOC_DES_LIST order by DESCR)) a where" & Chkwhere


            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMP_NO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName
            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "EMPLOYEE NO"

            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "EMPLOYEE NAME"

            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Sub GridbindDOCTYPE()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "SELECT ESD_ID as ID, '' EMP_NO, ESD_DESCR as DESCR FROM EMPSTATDOCUMENTS_M WHERE ESD_bCHKEXPIRY =1 " & "|" & "DOC ID|DOC Name"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ESD_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ESD_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            If Request.QueryString("ccsmode") <> "others" Then
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindDEPT()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "SELECT DPT_ID as ID, '' EMP_NO, DPT_DESCR as DESCR FROM DEPARTMENT_M where 1=1 " & "|" & "DEPT ID|DEPT Name"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("DPT_ID", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DPT_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            If Request.QueryString("ccsmode") <> "others" Then
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindDESG()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            'str_query_header = "SELECT DES_ID as ID, '' EMP_NO, DES_DESCR as DESCR FROM EMPDESIGNATION_M where DES_FLAG = 'ML' " & "|" & "DEPT ID|DEPT Name"
            str_query_header = " Select  distinct DES_ID As ID, DES_ID EMP_NO, DES_DESCR As DESCR FROM EMPDESIGNATION_M inner join EMPLOYEE_M On EMP_DES_ID=DES_ID where isnull(emp_bactive,0)=1  And emp_bsu_id='" & Session("sBSUID") & "'"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("DES_ID", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DES_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub GridBindSD_DESG()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "SELECT DES_ID as ID, '' EMP_NO, DES_DESCR as DESCR FROM EMPDESIGNATION_M where DES_FLAG = 'SD' " & "|" & "DESIGNATION ID|DESIGNATION NAME"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("DES_ID", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DES_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            FilterCondition = str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
        Select Case ViewState("ID").ToString
            Case "BSU"
                gridbindBSUnit()
            Case "DEPT"
                GridBindDEPT()
            Case "CAT"
                GridBindCAT()
            Case "DESG"
                GridBindDESG()
            Case "SD_DESG"
                GridBindSD_DESG()
            Case "EMP"
                GridBindEMP()
            Case "DOCTYPE"
                GridbindDOCTYPE()
            Case "EMP_ALLOC"
                GridBindEMP_ALLOC()
            Case "EMP_ALLALLOC"
                GridBindEMP_ALLALLOC()
            Case "DESGR"
                GridBindDESGR() 'V1.1
            Case "OWNER"
                GridBindOwner()
            Case "SubDoc"
                GridBindDocuments()
            Case "APPROVERLIST"
                GridBindApproverList()
            Case "BLOCKUSER"
                GridBindBlockUserList()







        End Select
    End Sub
    Sub GridBindDESGR() 'V1.1
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            str_query_header = "SELECT DES_ID as ID, '' EMP_NO, DES_DESCR as DESCR FROM EMPDESIGNATION_M where DES_FLAG = 'SD' " & "|" & "DESG ID|DESIGNATION "
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("DES_ID", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DES_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn1Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn2Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn3Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        Response.Write("window.close();")
        Response.Write("} </script>")

    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("Label1")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            Response.Write("window.close();")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        If chkSelAll.Checked Then
            Select Case ViewState("ID").ToString
                Case "BSU"
                    Dim tblbUSuper As Boolean = Session("sBusper")
                    Dim BUnitreaderSuper As SqlDataReader
                    Dim tblbUsr_id As String = Session("sUsr_id")

                    If tblbUSuper = True Then
                        BUnitreaderSuper = AccessRoleUser.GetBusinessUnits()
                    Else
                        BUnitreaderSuper = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                    End If

                    Dim strAccessibleBSUIDs As String = String.Empty

                    If BUnitreaderSuper.HasRows = True Then
                        While (BUnitreaderSuper.Read())
                            Session("liUserList").Remove(BUnitreaderSuper(0))
                            Session("liUserList").Add(BUnitreaderSuper(0))
                        End While
                    End If
                Case "DEPT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DPT_ID as ID FROM DEPARTMENT_M where 1=1" & FilterCondition
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DOCTYPE"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ESD_ID FROM EMPSTATDOCUMENTS_M WHERE ESD_bCHKEXPIRY =1 " & FilterCondition
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "CAT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ECT_ID as ID FROM EMPCATEGORY_M where 1=1 " & FilterCondition
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DESG"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DES_ID as ID FROM EMPDESIGNATION_M where 1=1 " & FilterCondition
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "SD_DESG"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DES_ID as ID FROM EMPDESIGNATION_M where DES_FLAG = 'SD' " & FilterCondition
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "EMP"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "select ID from (select EMP_ID as ID,EMPNO as EMP_NO, ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') DESCR from EMPLOYEE_M where 1=1 " & Session("EMP_SEL_COND") & " )A where 1 =1 " & FilterCondition
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "EMP_ALLOC"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim dt As String = Format(DateSerial(Request.QueryString("year"), Request.QueryString("month"), 1), "dd/MMM/yyyy")
                    Dim str_Sql As String = "SELECT ID,EMP_NO, DESCR FROM (select EMP_ID ID, empno EMP_NO , ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') as DESCR " _
                    & " from VW_OSO_EMPLOYEE_ALLOCATION where 1=1 " & Session("EMP_SEL_COND") & FilterCondition _
                    & " AND dbo.fN_GetLastDayOFmonth('" & dt & "') BETWEEN dbo.fN_GetLastDayOFmonth(EAL_FROMDT) AND  dbo.fN_GetLastDayOFmonth(ISNULL(EAL_TODT, '" & dt & "')) " _
                    & ") a WHERE 1=1 "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
            End Select
        Else
            Session("liUserList").Clear()
        End If
        GridBind()
    End Sub

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        If ViewState("ID") = "EMP" Or ViewState("ID") = "EMP_ALLOC" Then
            Dim lblEMPNO As Label = e.Row.FindControl("lblEMPNO")
            Dim lblID As Label = e.Row.FindControl("Label1")
            If (Not lblEMPNO Is Nothing) AndAlso (Not lblID Is Nothing) Then
                lblEMPNO.Visible = True
                lblID.Visible = False
            End If
        End If
    End Sub

End Class

