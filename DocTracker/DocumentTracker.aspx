<%@ Page Language="VB" AutoEventWireup="true" MasterPageFile="~/mainMasterPage.master" CodeFile="DocumentTracker.aspx.vb" Inherits="DocTracker_DocumentTracker" EnableEventValidation="false" %>

<%----%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <link   href='<%= ResolveUrl("~/DOCTRACKER/css/buttons.css")%>' rel="stylesheet" />


<%--<link href="/PHOENIXBETA/cssfiles/Popup.css" rel="stylesheet" />--%>
    <link   href='<%= ResolveUrl("~/DOCTRACKER/cssfiles/Popup.css")%>' rel="stylesheet" />


    <script type="text/javascript">
        function showHide() {
            $header = $(".header");
            $content = $(".FindJob");
            $ColExpId = $("#ColExpCss");
            $content.slideToggle(500, function () {
                if ($content.is(":visible") == true) {
                    $ColExpId.removeClass("colClass").addClass("ExpClass");
                }
                else {
                    $ColExpId.removeClass("ExpClass").addClass("colClass");
                }
            });
        }
        var tempObj = '';
        function popUpDetails(url) {
            try {
                if (tempObj && !tempObj.closed) {
                    tempObj.focus();
                }
                else {
                    window.open(url);
                }
            }
            catch (e) { }

            return false;
        }








        function fancyClose() {

            parent.$.fancybox.close();

        }


    </script>



    <style type="text/css">
        .RadGrid {
            border-radius: 0px !important;
            overflow: hidden;
        }

        .RadGrid_Default .rgCommandTable td {
            border: 0;
            /* padding: 2px 7px; */
        }

        div span.field-label {
            font-size: 18px;
            color: #000;
            font-weight: bold;
        }

        div span.field-value {
            border-radius: 6px;
            border: 1px solid #dee2da !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            padding: 10px;
            color: #999;
            background-color: rgba(0,0,0,0.01);
            display: block;
            width: 80%;
            font-size: 16px !important;
        }



        div select {
            border-radius: 6px;
            /* padding: 4px; */
            font-family: 'Nunito', sans-serif !important;
            height: inherit !important;
            min-height: 25px !important;
        }

        div select, div input[type=text], div input[type=date], div textarea {
            border-color: #dee2da !important;
            padding: 10px;
            min-width: 80% !important;
            width: 75%;
            font-size: 16px !important;
            color: #333;
        }



        #fancybox-content {
            /*border-color: #00a3e0 !important;
            background-color: #00a3e0 !important;*/
            width: 100% !important;
            overflow-x: hidden !important;
        }

        #fancybox-inner {
            overflow-x: hidden !important;
        }

        .RadGrid {
            border-radius: 10px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            /*background-color: #00a3e0;*/
        }

        .RadGrid_Office2010Blue th.rgSorted {
            /*background-color: #00a3e0;*/
        }

        .MyCalendar .ajax__calendar_container {
            border: 1px solid #646464;
            /*background-color: #00a3e0;*/
            color: white;
            border-radius: 10px;
        }
    </style>


    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                 args.get_eventTarget().indexOf("GECBtnExpandColumn") >= 0 ||
                 args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {

                args.set_enableAjax(false);
            }
        }

        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }

    </script>


    <%--</head>
<body class="bg-dark bg-white" >--%>
    <script type="text/javascript">



        function showFollowup(val, Sta) {


            var url = "UploadDocument.aspx?viewid= " + val + " &MenuCode= " + document.getElementById('<%= h_menuCode.ClientID%>').value + " &Status= " + Sta + ""
            //alert(url);
            $.fancybox({
                type: 'iframe',
                href: 'UploadDocument.aspx?viewid=' + val + '&MenuCode=' + document.getElementById('<%= h_menuCode.ClientID%>').value + '&Status=' + Sta,


                fitToView: false,
                width: '70%',
                height: '90%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                'onClosed': function () {
                    parent.$.fancybox.close();

                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }
            });
            //return ShowWindowWithClose(url, 'search', '55%', '85%');

            return false;
        }





    </script>
    <%--  <div class="col-md-12 col-lg-12 col-sm-12 mb-3">
            <img src="images/GEMS-banner.png" class="img-responsive" />
        </div>--%>

    <div class="card m-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Document Tracker</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <%--                <form id="form1" runat="server">--%>

                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
                    <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="Grid_Document">
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>

                




                <asp:LinkButton ID="CausePostBack" runat="server"></asp:LinkButton>

                
                <div id="divNote" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static">
                    <asp:Button ID="btnMsg" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                        ForeColor="White" Text="X" CausesValidation="false"></asp:Button>

                    <asp:Label ID="lblError" runat="server" EnableViewState="false"></asp:Label>

                </div>
                
                <div width="100%" class="row">
                    <div class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1 FindJob" runat="server" id="FindJob">
                        <div class="col-md-12 col-lg-12 col-sm-12 ">
                            <div class="col-md-3 col-lg-3 col-sm-3" style="padding-top: 10px; padding-bottom: 10px;">
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12 ">
                                <div class="col-md-3 col-lg-3 col-sm-3 " style="padding-top: 10px;">
                                    <span class="inputStyleLabel">As On Date:</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div align="left" width="100%" class="row">
                    <div class="col-md-14 col-lg-14 col-sm-14">
                        <asp:Label ID="lblMsg" runat="server" CssClass="error" Text=""></asp:Label>
                    </div>
                </div>

                <div width="100%" class="row">

                    <div class="col-md-1 col-lg-1 col-sm-1">
                        <asp:Label ID="Label7" runat="server" Text="Emirate" CssClass="field-label" ></asp:Label>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        <asp:DropDownList ID="ddlEmirate" runat="server" AutoPostBack="true" >
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-1 col-lg-1 col-sm-1">
                        <asp:Label ID="Label2" runat="server" Text="Doc.Group" CssClass="field-label"></asp:Label>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </div>

                    <div class="col-md-1 col-lg-1 col-sm-1">
                        <asp:Label ID="Label1" runat="server" Text="Doc.Type" CssClass="field-label"></asp:Label>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        <asp:DropDownList ID="ddlDocumentType" runat="server" AutoPostBack="true"  ></asp:DropDownList>
                    </div>                  
                                        
                    <div class="col-md-1 col-lg-1 col-sm-1">
                        <asp:Label ID="Label4" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        <asp:DropDownList ID="ddlBSU" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-1 col-lg-1 col-sm-1"  id="Suplier1" runat ="server">
                        <asp:Label ID="Label8" runat="server" Text="Supplier/Landlord" CssClass="field-label"></asp:Label>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2" id="Suplier2" runat ="server">
                        <asp:DropDownList ID="ddlLandlord" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>

                     <div class="col-md-1 col-lg-1 col-sm-1"  id="SubDoc1" runat ="server">
                        <asp:Label ID="Label9" runat="server" Text="Document Belongs to Department" CssClass="field-label"></asp:Label>
                    </div>
                     <div class="col-md-2 col-lg-2 col-sm-2" id="SubDoc2" runat ="server">
                        <asp:DropDownList ID="ddlbDepartment" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>



                </div>
                
                <div width="100%" class="row">
                    <br />
                    <div style="display: none;">
                        <a href="#" id="hrefTour" runat="server" visible="true" class="enrolnew">School Tour</a>
                        <a href="#" id="hrefchangestatus" runat="server" visible="true" class="enrolnew">Change Status</a>
                        <a href="#" id="hrefBulkemail" runat="server" visible="true" class="enrolnew">Bulk Email</a>
                        <a href="#" id="hrefOpenseat" runat="server" visible="true" class="enrolnew" onclick="showSeats();">Open Seats</a>
                    </div>
                </div>

                <div width="100%" id="buttonContainer" class="row">
                    <div class="col-md-11 col-lg-11 col-sm-11">
                        <asp:Repeater ID="rptStatus" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdStatus" runat="server" Value='<%# Eval("sm_id")%>' />
                                <asp:LinkButton class='<%# Eval("sm_class")%>' ID="btnREF" runat="server" CommandName='<%# Eval("sm_id")%>' CommandArgument='<%# Eval("sm_id")%>'
                                    title='<%# Eval("SM_DESCR")%>'><%# Eval("SM_DESCR")%>(<%# Eval("cnt")%>)</asp:LinkButton>
                                &nbsp;
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1 text-right"><a href="#" id="hrefEnq" runat="server" visible="true" class="btn btn-primary button" onclick="showFollowup(0);">Add New</a></div>
                </div>
                
                <div class="row mt-3" style="padding-top: 10px;" id="divExpSoon" runat="server" visible="false">
                    <div class="col-md-1 col-lg-1 col-sm-1 ">
                        <asp:Label ID="Label5" runat="server" Text="Expiring in " CssClass="field-label"></asp:Label>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 ">
                        <div id="buttonDateFilter">
                            <asp:RadioButtonList ID="rbOption" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                <asp:ListItem Text="30 Days" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="60 Days" Value="1"></asp:ListItem>
                                <asp:ListItem Text="90 Days" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Date Range" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>

                        </div>
                    </div>
                    <div class="col-md-5 col-lg-5 col-sm-5 ">
                        <telerik:RadDatePicker ID="txtFromDate" runat="server" DateInput-DateFormat="dd/MMM/yyyy" AutoPostBack="true"></telerik:RadDatePicker>

                        <asp:Label ID="Label3" runat="server" Text="To" CssClass="field-label"></asp:Label>
                        <telerik:RadDatePicker ID="txtTodate" runat="server" DateInput-DateFormat="dd/MMM/yyyy" AutoPostBack="true"></telerik:RadDatePicker>

                    </div>
                </div>
                <div class="row mt-3">
                    <div style="float: left; padding-top: 10px;" class="col-md-10 col-lg-10 col-sm-10 ">
                        <asp:Label ID="Label6" runat="server" Text="Trackable Documents :" CssClass="field-label"></asp:Label>
                        <asp:Label ID="lblStatus" runat="server" Text="" CssClass="field-label"></asp:Label>


                    </div>
                    <div style="float: right; padding-top: 10px;" class="col-md-2 col-lg-2 col-sm-2  text-right">
                        <asp:CheckBox ID="chkShowFilters" runat="server" AutoPostBack="True" Visible="true" Checked="false" Text="Show Filter" />
                    </div>
                </div>
                <div class="table-responsive" width="100%">
                    <br />


                    <telerik:radgrid id="Grid_Document" runat="server"
                        allowpaging="True" cssclass="table table-bordered  table-row"
                        cellspacing="0" gridlines="None" enabletheming="False" autogeneratecolumns="False"
                        onneeddatasource="gv_Enquiry_NeedDataSource" ondetailtabledatabind="Grid_Document_DetailTableDataBind"
                        onitemcommand="Grid_Document_ItemCommand" pagesize="15" xmlns:telerik="telerik.web.ui" allowsorting="True">
                            <clientsettings allowcolumnsreorder="true" allowdragtogroup="True" > </clientsettings>
                            <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true"  >
                         <Excel Format="ExcelML" />
                            </ExportSettings>
                             
                    <mastertableview commanditemdisplay="top" DataKeyNames="Doc_ID,SM_COLOR" HierarchyLoadMode ="ServerBind" > 
                        <%--<DetailTables >
                           
                            <telerik:GridTableView  DataKeyNames="Doc_ID,SM_COLOR" Name="Doc_ID" Width="100%" HierarchyLoadMode ="ServerBind" >
                              
                                <Columns >
                                     
                                    <telerik:GridBoundColumn SortExpression="Doc_ID" HeaderText="Document No" HeaderButtonType="TextButton"
                                        DataField="Doc_ID">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn SortExpression="Businessunit" HeaderText="Business Unit"  HeaderButtonType="TextButton"
                                        DataField="Businessunit">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn SortExpression="BSU" HeaderText="BSU"  HeaderButtonType="TextButton" DataField="BSU" >
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn SortExpression="DocumentType" HeaderText="Document Type"  HeaderButtonType="TextButton"
                                        DataField="DocumentType">
                                    </telerik:GridBoundColumn>
                                    
                                    <telerik:GridBoundColumn SortExpression="DocumentIssuer" HeaderText="Document Issuer" HeaderButtonType="TextButton" DataField="DocumentIssuer"></telerik:GridBoundColumn>

                                       <telerik:GridBoundColumn SortExpression="DocumentNo" HeaderText="Document No" HeaderButtonType="TextButton" DataField="DocumentNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn SortExpression="IssueDate" HeaderText="Issue Date"  HeaderButtonType="TextButton" DataField="IssueDate">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn SortExpression="Expirydate" HeaderText="Expiry Date" HeaderButtonType="TextButton" DataField="Expirydate">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn SortExpression="CurrStatusDescr" HeaderText="Status" HeaderButtonType="TextButton" DataField="CurrStatusDescr">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="View" UniqueName="TemplateColumn" Groupable="False" 
                                AllowFiltering="False" >
                                 <ItemTemplate > 
                                     <a id="framelnkFollowup_sub" style="color:#800000; "    onclick="showFollowup('<%# Eval("Doc_ID")%>','<%# Eval("CurrStatusDescr")%>');">View</a>
                               
                            </ItemTemplate> 
                            </telerik:GridTemplateColumn> 

                                    


                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>--%>
                    <CommandItemSettings ExportToPdfText="Export to PDF"  ShowAddNewRecordButton="false"  ShowExportToWordButton="true" ShowExportToExcelButton="true"  ></CommandItemSettings> 
                        <Columns>
                          
                            <telerik:GridBoundColumn  
                                HeaderText="Document Type" UniqueName="DocumentType" DataField="DocumentType"  >
                                <ColumnValidationSettings>
                                    <ModelErrorMessage Text="" />
                                </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn  
                                HeaderText="Document Number" UniqueName="DocumentNo" DataField="DocumentNo"  >
                               <ColumnValidationSettings>
                                   <ModelErrorMessage Text="" />
                               </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn  
                                HeaderText="BSU" UniqueName="bsu" DataField="BSU" AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains"  >
                                 <ColumnValidationSettings>
                                     <ModelErrorMessage Text="" />
                                 </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>


                             <telerik:GridBoundColumn  
                                HeaderText="Business Unit" UniqueName="BusinessUnit" DataField="BusinessUnit" AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains"  >
                                 <ColumnValidationSettings>
                                     <ModelErrorMessage Text="" />
                                 </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>
                            
                               <telerik:GridBoundColumn  
                                HeaderText="Issue Date" UniqueName="IssueDate" DataField="IssueDate"  DataFormatString="{0:dd/MMM/yyyy}">
                               <ColumnValidationSettings>
                                   <ModelErrorMessage Text="" />
                               </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>

                          <telerik:GridBoundColumn  
                                HeaderText="Expiry Date" UniqueName="ExpiryDate" DataField="ExpiryDate"  DataFormatString="{0:dd/MMM/yyyy}">
                               <ColumnValidationSettings>
                                   <ModelErrorMessage Text="" />
                               </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>
                          
                          <telerik:GridBoundColumn  
                                HeaderText="Days till Expiry" UniqueName="ExpDays" DataField="ExpDays"  >
                              <ColumnValidationSettings>
                                  <ModelErrorMessage Text="" />
                              </ColumnValidationSettings>
                              <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>
                         
                         

                         <telerik:GridBoundColumn  
                                HeaderText="Details" UniqueName="Details" DataField="Details"  >
                              <ColumnValidationSettings>
                                  <ModelErrorMessage Text="" />
                              </ColumnValidationSettings>
                              <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn  
                                HeaderText="Document Issued to User" UniqueName="DocumentIssuer" DataField="DocumentIssuedtoUser"  >
                                <ColumnValidationSettings>
                                    <ModelErrorMessage Text="" />
                                </ColumnValidationSettings>
                                <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>                           
	

                          
                            <telerik:GridBoundColumn  
                                HeaderText="Supplier/Landlord" UniqueName="Supplier_Contract" DataField="Supplier_Contract" AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains"  >
                                 <ColumnValidationSettings>
                                     <ModelErrorMessage Text="" />
                                 </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>              
                         

                             <%--<telerik:GridBoundColumn  
                                HeaderText="Document Issuer" UniqueName="DocumentIssuer" DataField="DocumentIssuer"  >
                                <ColumnValidationSettings>
                                    <ModelErrorMessage Text="" />
                                </ColumnValidationSettings>
                                <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn  
                                HeaderText="City" UniqueName="City" DataField="Emirate" AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains"  >
                                 <ColumnValidationSettings>
                                     <ModelErrorMessage Text="" />
                                 </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>
                                       
                         --%>
                         
                             <telerik:GridBoundColumn  
                                HeaderText="Status" UniqueName="Status" DataField="CurrStatusDescr" AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True" CurrentFilterFunction="Contains"  >
                                 <ColumnValidationSettings>
                                     <ModelErrorMessage Text="" />
                                 </ColumnValidationSettings>
                               <HeaderStyle /> <ItemStyle HorizontalAlign="left" />
                          </telerik:GridBoundColumn>  
                               




                        
                             <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="View" UniqueName="TemplateColumn" Groupable="False" 
                                AllowFiltering="False">
                                 <ItemTemplate > 
                                     <a id="framelnkFollowup" style="color:#800000; cursor: pointer;"    onclick="showFollowup('<%# Eval("DocumentId")%>','<%# Eval("STATUS")%>');">View</a>
                               
                            </ItemTemplate> 
                            <HeaderStyle /> <ItemStyle HorizontalAlign="Center" /> </telerik:GridTemplateColumn> 
                       </Columns>

                        
    </mastertableview>
                    <%--<pagerstyle mode="NextPrevNumericAndAdvanced" ></pagerstyle>--%>
                    </telerik:radgrid>


                </div>
                <br />
                <br />
                <br />
                <%--      </section>--%>



                <%--            </ContentTemplate>
        </asp:UpdatePanel>--%>

                <%--      <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="" runat="server">
                    <div id="processMessage" class="progMsg_Show">
                        <img alt="Loading..." src="Images/Loading.gif" /><br />

                    </div>
                </div>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="progressBackgroundFilter"
                    VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                    HorizontalOffset="10">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
                <asp:HiddenField ID="h_menuCode" runat="server" Value="0" />
                <asp:HiddenField ID="h_Status" runat="server" Value="" />


                <%--  </form>--%>
            </div>
        </div>
    </div>
    <%# Eval("SM_DESCR")%>


    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

    <%--</body>
</html>--%>


    <%--                <br />
                  <br />
                  <br /--%>
</asp:Content>
