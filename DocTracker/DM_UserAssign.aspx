﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_UserAssign.aspx.vb" Inherits="DocTracker_DM_UserAssign" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <style type="text/css">
        .gridheader_new {
            border-style: none;
            border-color: inherit;
            border-width: 0;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            background-image: url('../../Images/GRIDHEAD.gif');
            background-repeat: repeat-x;
            font-size: 11px;
            font-weight: bold;
            color: #1b80b6;
        }

        .inputbox {
            BACKGROUND: F5FED2;
            BORDER-BOTTOM: #1B80B6 1px solid;
            BORDER-LEFT: #1B80B6 1px solid;
            BORDER-RIGHT: #1B80B6 1px solid;
            BORDER-TOP: #1B80B6 1px solid;
            COLOR: #555555;
            CURSOR: text;
            FONT-FAMILY: verdana;
            FONT-SIZE: 11px;
            WIDTH: 199px;
            HEIGHT: 14px;
            TEXT-DECORATION: none;
        }

        .button {
        }
    </style>

    <script language="javascript" type="text/javascript">



     <%--   function getEMPList(EMPNAME, EMPID) {
            var sFeatures;

            var SelText;
            var pMode;
            var NameandCode;

            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "GetEmpBSUList"
            document.getElementById('<%=hf_EMPNAMEid.ClientID%>').value = EMPNAME;
            document.getElementById('<%=hf_EMPID.ClientID%>').value = EMPID;
                     
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&EmpBsu=EMP";
            //result = window.showModalDialog(url, "", sFeatures);
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;

            ////if (result == '' || result == undefined) {
            ////    return false;
            ////}
            ////NameandCode = result.split('___');
            ////document.getElementById(EMPNAME).value = NameandCode[2];
            ////document.getElementById(EMPID).value = NameandCode[0];


            ////document.getElementById(EMPNAME).disabled = true;
            ////document.getElementById(EMPID).disabled = true;

        }--%>


<%--        function getBSUList(BSU, BSUID) {
            var sFeatures;

            var SelText;
            var pMode;
            var NameandCode;

            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "GetEmpBSUList"

            document.getElementById('<%=hf_BSUDESCid.ClientID%>').value = BSU;
            document.getElementById('<%=hf_BSUIDid.ClientID%>').value = BSUID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&EmpBsu=BSU";
            ////result = window.showModalDialog(url, "", sFeatures);
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;

            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //document.getElementById(BSU).value = NameandCode[2];
            //document.getElementById(BSUID).value = NameandCode[0];


            //document.getElementById(BSU).disabled = true;
            //document.getElementById(BSUID).disabled = true;

        }--%>

        function setProduct(result) {
            //alert(1);
            NameandCode = result.split('___');
            str = document.getElementById('<%=hf_EMPNAMEid.ClientID%>').value
            str2 = document.getElementById('<%=hf_EMPID.ClientID%>').value

            //document.getElementById(DOMDESCR).value = NameandCode[1];
            document.getElementById(document.getElementById('<%=hf_EMPNAMEid.ClientID%>').value).value = NameandCode[2];
            document.getElementById(document.getElementById('<%=hf_EMPID.ClientID%>').value).value = NameandCode[0];


            document.getElementById(document.getElementById('<%=hf_EMPNAMEid.ClientID%>').value).disabled = true;
            document.getElementById(document.getElementById('<%=hf_EMPID.ClientID%>').value).disabled = true;

            //alert(NameandCode[1]);
            //document.getElementById(DOMID).value = NameandCode[0];
            //document.getElementById(DOMDESCR).disabled = true;
            CloseFrame();
            //return false;
        }

        function setProduct4(result) {
            //alert(1);
            NameandCode = result.split('___');
            str = document.getElementById('<%=hf_BSUDESCid.ClientID%>').value
            str2 = document.getElementById('<%=hf_BSUIDid.ClientID%>').value

             //document.getElementById(DOMDESCR).value = NameandCode[1];
             document.getElementById(document.getElementById('<%=hf_BSUDESCid.ClientID%>').value).value = NameandCode[2];
             document.getElementById(document.getElementById('<%=hf_BSUIDid.ClientID%>').value).value = NameandCode[0];


             document.getElementById(document.getElementById('<%=hf_BSUDESCid.ClientID%>').value).disabled = true;
             document.getElementById(document.getElementById('<%=hf_BSUIDid.ClientID%>').value).disabled = true;

             //alert(NameandCode[1]);
             //document.getElementById(DOMID).value = NameandCode[0];
             //document.getElementById(DOMDESCR).disabled = true;
             CloseFrame();
             //return false;
         }
        function CloseFrame() {
            jQuery.fancybox.close();
        }



    </script>



    <script>
        function getEMPList(EMPNAME, EMPID) {
            var pMode;
            var NameandCode;
            pMode = "GetEmpBSUList"
            document.getElementById('<%=hf_1.ClientID%>').value = EMPNAME;
             document.getElementById('<%=hf_2.ClientID%>').value = EMPID;

            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&EmpBsu=EMP";
            var oWnd = radopen(url, "pop_emp");
        }
        function OnClientCloseE(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[2];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[0];
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
            }
        }

        function getBSUList(BSU, BSUID) {
            var pMode;
            var NameandCode;
            pMode = "GetEmpBSUList"
            document.getElementById('<%=hf_1.ClientID%>').value = BSU;
            document.getElementById('<%=hf_2.ClientID%>').value = BSUID;

            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&EmpBsu=BSU";
            var oWnd = radopen(url, "pop_bsu");
        }

        function OnClientCloseB(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[2];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[0];
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
            }
        }

        
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseE"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseB"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>User Assign Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">User Assign Master
                        </td>
                    </tr>
                    <tr>
                        <td align="left">

                            <table width="100%">


                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="Tr2" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <asp:Panel ID="PnlDocRoles" runat="server">

                                            <asp:GridView ID="grdAssignedUser" runat="server" AutoGenerateColumns="False" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="10"
                                                CssClass="table table-bordered table-row" DataKeyNames="BAU_ID" ShowFooter="True" Width="100%">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="EMP ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBAU_ID" runat="server" Text='<%# Bind("BAU_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Staff Name" ItemStyle-Width="30%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtEmpName" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="h_EMP_ID" runat="server" Value='<%# Bind("BAU_EMP_ID")%>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEmpName" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                            <asp:HiddenField ID="h_EMP_ID" runat="server" Value='<%# Bind("BAU_EMP_ID")%>' />
                                                            <asp:ImageButton ID="imgEMPID_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </EditItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtEmpName" Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_EMP_ID" runat="server" Value='<%# Bind("BAU_EMP_ID")%>' />
                                                            <asp:ImageButton ID="imgEmpID" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Business unit" ItemStyle-Width="30%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtBSU" runat="server" Text='<%# Bind("BSU")%>'></asp:Label>
                                                            <asp:HiddenField ID="h_BSU_ID" runat="server" Value='<%# Bind("BAU_BSU_ID")%>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtBSU" Enabled="false" runat="server" Text='<%# Bind("BSU")%>'></asp:TextBox>
                                                            <asp:HiddenField ID="h_BSU_ID" runat="server" Value='<%# Bind("BAU_BSU_ID")%>' />
                                                            <asp:ImageButton ID="imgBSU_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </EditItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtBSU" Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_BSU_ID" runat="server" Value='<%# Bind("BAU_BSU_ID")%>' />
                                                            <asp:ImageButton ID="imgBSU" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="From Date" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFromDate" runat="server" Style="text-align: right"
                                                                Text='<%# Eval("BAU_FROMDT", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                            <%--<asp:ImageButton ID="imgChqDate" runat="server" ImageUrl="~/Images/forum_search.gif" />--%>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtFromDate" Enabled="false" runat="server" Style="text-align: right"
                                                                Text='<%# Eval("BAU_FROMDT", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <asp:ImageButton ID="imgEFromDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                                TargetControlID="txtFromDate" PopupButtonID="imgEFromDate">
                                                            </ajaxToolkit:CalendarExtender>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtFromDate" runat="server" Enabled="false" Style="text-align: right"
                                                                Text='<%# Eval("BAU_FROMDT", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <asp:ImageButton ID="imgFFromDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                                TargetControlID="txtFromDate" PopupButtonID="imgFFromDate">
                                                            </ajaxToolkit:CalendarExtender>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="To Date" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblToDate" runat="server" Style="text-align: right" 
                                                                Text='<%# Eval("BAU_TODT", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                            <%--<asp:ImageButton ID="imgTodate" runat="server" ImageUrl="~/Images/forum_search.gif" />--%>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtToDate" Enabled="false" runat="server" Style="text-align: right"
                                                                Text='<%# Eval("BAU_TODT", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <asp:ImageButton ID="imgEToDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd-MMM-yyyy" runat="server"
                                                                TargetControlID="txtToDate" PopupButtonID="imgEToDate">
                                                            </ajaxToolkit:CalendarExtender>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtToDate" runat="server" Enabled="false" Style="text-align: right"
                                                                Text='<%# Eval("BAU_TODT", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <asp:ImageButton ID="imgFToDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd-MMM-yyyy" runat="server"
                                                                TargetControlID="txtToDate" PopupButtonID="imgFToDate">
                                                            </ajaxToolkit:CalendarExtender>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="5%">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateDOL" runat="server" CausesValidation="True" CommandName="Update"
                                                                Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelDOL" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddDOL" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditDOL" runat="server" CausesValidation="False" CommandName="Edit"
                                                                Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" Visible="false" ShowHeader="True" ItemStyle-Width="5%" />


                                                </Columns>

                                                <HeaderStyle />

                                            </asp:GridView>
                                        </asp:Panel>

                                    </td>


                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                                        <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>


                <%--<asp:HiddenField ID="Hiddensubtabids" runat="server" />
    <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
    <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />--%>
                <asp:HiddenField ID="h_Profile_ID" runat="server" />
                <asp:HiddenField ID="h_BSUorSCHOOL" runat="server" />
                <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                <asp:HiddenField ID="h_type" runat="server" Value="0" />
                <asp:HiddenField ID="h_DOC_OWNER_DELETED" runat="server" />




                <asp:HiddenField ID="h_BSUID" runat="server" />

                <asp:HiddenField ID="hf_EMPNAMEid" runat="server" />
                <asp:HiddenField ID="hf_EMPID" runat="server" />
                <asp:HiddenField ID="hf_BSUDESCid" runat="server" />
                <asp:HiddenField ID="hf_BSUIDid" runat="server" />
                <asp:HiddenField ID="hf_1" runat="server" />
                <asp:HiddenField ID="hf_2" runat="server" />

            </div>
        </div>
    </div>

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>

