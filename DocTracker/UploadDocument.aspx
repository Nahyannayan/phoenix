﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UploadDocument.aspx.vb" Inherits="DocTracker_UploadDocument" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    
     <!-- Bootstrap core JavaScript-->
    <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
    <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


    <!-- Bootstrap core CSS-->
 
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">

    <link href="/cssfiles/BSUstyles.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <%--<link href="/vendor/font-awesome/css/font-awesome-all.min.css" rel="stylesheet" type="text/css">--%>
    <!-- Page level plugin CSS-->
   
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/cssfiles/Accordian.css" rel="stylesheet" />


    <link type="text/css" href="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet">
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <link href="/PHOENIXBETA/cssfiles/Popup.css?1=2" rel="stylesheet" />
    <style type="text/css">
        /*table td select:disabled, table td input[type=text]:disabled, table td textarea:disabled {
visibility: hidden;
}*/

.RadComboBox .rcbInputCell {
    width: 100%;
    height: 31px !important;
background-color: transparent !important; 
border-radius: 6px !important;
background-image: none !important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
}

.RadComboBox .rcbArrowCellRight a {
    background-position: -18px -176px;
    border: solid black;
    border-width: 0 1px 1px 0;
    transform: rotate(360deg);
    -webkit-transform: rotate(45deg) !important;
    color: black;
    width: 7px !important;
    height: 7px !important;
    overflow: hidden;
        margin-top: -1px;
    margin-left: -15px;
}
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 31px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}

        .RadComboBox_Default {
    /* color: #333; */
    font: inherit;
    width: 80% !important;
}
        .RadComboBox_Default .rcbFocused .rcbInput, .RadComboBox .rcbInput {
    color: black;
    padding: 20px !important;
}

           .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
              .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }

              .RadComboBox table td.rcbInputCell {
    padding: 0px !important;
    border-width: 0;
}
  /*.RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
      
     */



        /*.panel-cover {
            padding: 10px;
            border: 1px solid #808080;
            background-color: #e9ecef;
            width: 80%;
        }

        td.panel-cover {
            border: 1px solid inherit;
        }

        .RadGrid {
            border-radius: 10px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            background-color: #00a3e0;
        }

        .RadGrid_Office2010Blue th.rgSorted {
            background-color: #00a3e0;
        }*/
    </style>

    <script type="text/javascript" lang="javascript">
        function showDocument(id, path) {
            var sFeatures;
            var path1;
            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";
            window.open("ViewingSharpointDocument.aspx?id=" + id + "&path=" + path)
            return false;
        }
        function getBSU() {
        }
        var isShift = false;
        function isNumeric(keyCode) {
            if (keyCode == 16) isShift = true;
            return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 39 || keyCode == 46 || keyCode == 37 ||
            (keyCode >= 96 && keyCode <= 105)) && isShift == false);
        }
        function keyUP(keyCode) {
            if (keyCode == 16)
                isShift = false;
        } function ajaxToolkit_CalendarExtenderClientShowing(e) {
        } function checkDate(sender, args) {
        }
    </script>
</head>
<body>
    
    <form id="form1" runat="server">
           <link href='<%= ResolveUrl("~/vendor/bootstrap/css/bootstrap.css") %>' rel="stylesheet">
         <link href='<%= ResolveUrl("~/vendor/datatables/dataTables.bootstrap4.css") %>' rel="stylesheet">
        <script type="text/javascript" lang="javascript">
            function UploadPhoto() {
                var filepath = document.getElementById('<%=UploadDocPhoto.ClientID %>').value;
                if ((filepath != '') && (document.getElementById('<%=h_DocumentPath.ClientID %>').value != filepath)) {
                    document.getElementById('<%=h_DocumentPath.ClientID %>').value = filepath;
                }
            }


            function UploadFullVersion() {
                var fullversionfilepath = document.getElementById('<%=UploadDocFullversion.ClientID %>').value;
                if ((fullversionfilepath != '') && (document.getElementById('<%=h_DocumentPathFullVersion.ClientID %>').value != fullversionfilepath)) {
                    document.getElementById('<%=h_DocumentPathFullVersion.ClientID %>').value = fullversionfilepath;
                }
            }

            function fancyClose() {
                parent.$.fancybox.close();
            }
            function showBlock(ACTION) {
                var EntryId = document.getElementById('<%=h_EntryId.ClientID %>').value;
                $.fancybox({
                    type: 'iframe',
                    href: 'DM_BlockUnblockDocument.aspx?viewid=' + EntryId + '&TYPE=' + ACTION,
                    fitToView: false,
                    width: '90%',
                    height: '70%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    'onClosed': function () {
                        $.fancybox.close();
                        document.getElementById('<%= CausePostBack.ClientID%>').click();
                    }
                })
                ; return false;
            }
            function showSubDoc() {
                var EntryId = document.getElementById('<%=h_EntryId.ClientID %>').value;
                $.fancybox({
                    type: 'iframe',
                    href: 'ShowSubDocuments.aspx?viewid=' + EntryId,
                    fitToView: false,
                    width: '100%',
                    height: '70%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    'onClosed': function () {
                        $.fancybox.close();
                    }
                })
                ; return false;
            }
            function showFollowup(val, Sta) {
                $.fancybox({
                    type: 'iframe',
                    href: 'UploadDocument.aspx?viewid=' + val + '&MenuCode=DM00032&Status=' + Sta,


                    fitToView: false,
                    width: '70%',
                    height: '90%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    'onClosed': function () {
                        parent.$.fancybox.close();
                    }
                });
            }
        </script>



        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="Upnl1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:LinkButton ID="CausePostBack" runat="server"></asp:LinkButton>
                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%; ">
                    <tr valign="bottom">
                        <td align="left"  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label>

                            <pre id="preError" class="invisible" runat="server"></pre>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4" >
                                        Document Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="25%"> <span class="field-label">Document Type</span><span class="text-danger">*</span></td>
             
                                    <td  align="left" colspan="3">
                                        <asp:DropDownList ID="ddlDocumentType" AutoPostBack="true" runat="server" ></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Document Issuer</span></td>
                                
                                    <td align="left" colspan="3">
                                        <asp:Label ID="txtIssuer" runat="server" CssClass="field-value" text="Select a Document Type"></asp:Label>
                                        <asp:HiddenField ID="hdnIssuerId" Value="0" runat="server" />
                                    </td>
                                </tr>
                                <tr>

                                     <td align="left" width="25%"> <span class="field-label">Document Issued To</span></td>
                    
                                    <td  align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBSU"  AutoPostBack="true" runat="server" ></asp:DropDownList>

                                        <asp:TextBox ID="txtIssuedTo" Enabled="false" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgBSU" runat="server" OnClientClick="getBSU();return true;" ImageUrl="~/Images/forum_search.gif" />
                                    </td>
                                </tr>


                                <tr>
                                     <td align="left"><span class="field-label">Supplier (contracts only) /Landlord (leases only)</span></td>
                        
                                    <td align="left" colspan="3">
                                        <telerik:RadComboBox ID="ddlLandlord" runat="server" Filter="Contains" ZIndex="2000" RenderMode="Lightweight" ToolTip="Type in landlord name or choose from the list"></telerik:RadComboBox>
                                        <br />
                                        <asp:TextBox ID="txtDocSpeName" runat="server"  EnableTheming="False"  Visible="false"></asp:TextBox></td>
                                </tr>

                                <tr id="Procure2" runat="server" visible ="false" >

                                    <td align="left"><span class="field-label">Document Sub Type </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlSubGroup" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    </tr>
                                
                                <tr id="Procure1" runat="server" visible ="false" >

                                    <td align="left"><span class="field-label">Department Document Belongs to </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlDepartment" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    </tr>

                                 

                                <tr id="EditDetails2" runat="server">
                                    <td align="left"><span class="field-label">Document Registration No</span><span class="text-danger">*</span></td>
                       
                                    <td  align="left" colspan="2">
                                        <asp:TextBox ID="txtDocNo" runat="server" ></asp:TextBox>
                                        <asp:HiddenField ID="hdnDoi_Id" runat="server" />
                                        <asp:HiddenField ID="hdnDoi_File_Name" runat="server" />
                                        <asp:HiddenField ID="hdnDoi_File_NameTemp" runat="server" />
                                        <asp:HiddenField ID="hdnDoi_content_type" runat="server" />
                                        
                                    </td>
                                   <td  align="left"><asp:LinkButton ID="lnkSOP" runat="server" OnClientClick="return showDocument(0,'')" Text="Renewal Process SOP-Download" CssClass="button small"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkSubDoc" runat="server" Text="View Dependancy Documents" OnClientClick="return showSubDoc()" CssClass="button small" Visible="false"></asp:LinkButton></td>
                                   
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Document Details</span><span class="text-danger">*</span></td>
                               
                                    <td  colspan="3" align="left">
                                        <asp:TextBox ID="txtRemarks" runat="server"  EnableTheming="False" ></asp:TextBox></td>
                                </tr>

                                

                                <tr id="Procure3" runat="server" visible ="false"  >

                                    <td align="left"><span class="field-label">Document issued to user </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlDocIssUser" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    </tr>

                                 <tr id="Procure4" runat="server" visible ="false"  >

                                    <td align="left"><span class="field-label">Supplier / Landlord Payment Terms </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlPaymentTerms" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    </tr>
                                
                                <tr id="Procure5" runat="server" visible ="false"  >

                                    <td align="left"><span class="field-label">Document Template Format </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlTemplate" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    </tr>

                                 <tr  id="Procure6" runat="server" visible ="false"  >
                                    <td align="left"><span class="field-label">Contract Value (AED)</span></td>
                               
                                    <td  colspan="3" align="left">
                                        <asp:TextBox ID="txtContractValue" runat="server"  EnableTheming="False" ></asp:TextBox></td>
                                </tr>


                                <tr id="EditDetails3" runat="server">
                                    <td align="left"><span class="field-label">Issue Date</span><span class="text-danger">*</span></td>
                      
                                    <td align="left">
                                        <asp:TextBox ID="txtIssueDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="lnkFromDate" runat="server" CssClass="ajax_calendar" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtIssueDate" PopupButtonID="lnkFromDate" OnClientDateSelectionChanged="checkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left"><span class="field-label">Expiry Date</span><span class="text-danger">*</span></td>
                              
                                    <td align="left">
                                        <asp:TextBox ID="txtExpiryDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="lnkToDate" CssClass="ajax_calendar" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtExpiryDate" PopupButtonID="lnkToDate" OnClientDateSelectionChanged="checkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing"></ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <%--<tr id="Trcomments" runat="server" visible="false">
                            <td class="matters">Comments</td>
                            <td class="matters">:</td>
                            <td class="matters" colspan="4" align="left">
                                <asp:TextBox ID="txtComments" CssClass="inputStyle" TextMode="MultiLine" runat="server" Width="80%" EnableTheming="False"></asp:TextBox></td>
                        </tr>--%>
                                <tr class="title-bg-lite" id="EditDetails1" runat="server">
                                    <td colspan="4" >Documents Required for renewal
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <table width="100%">
                                            <tr >
                                                <td align="left"><span class="field-label">Tracked Documents</span>
                                                </td>
                                            </tr>
                                            <tr class="table table-bordered table-row">
                                                <td align="left">
                                                    <div class="table-responsive divDashboardContainer">
                                                        <telerik:radgrid id="grdTracked" runat="server"
                                                            allowpaging="True" cssclass="table table-bordered  table-row"
                                                            cellspacing="0" gridlines="None" enabletheming="False"  autogeneratecolumns="False" 
                                                            onitemcommand="Grid_Document_ItemCommand" pagesize="15" xmlns:telerik="telerik.web.ui">
                                                    <MasterTableView DataKeyNames="Doc_ID,SM_COLOR,DOI_ID,DOI_FILE_PATH,Dot_Id" NoMasterRecordsText="No documents to display"> 
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn  headertext="Select">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox  ID="CheckSubdoc" checked="false"  runat="server"></asp:CheckBox>
                                                                            <asp:HiddenField ID="hdn_SubDocID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "DocumentId") %>' />
                                                                            <asp:HiddenField ID="MultipleEnable" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "DOT_MUTLIPLE_ENABLE") %>' />
                                                                        </ItemTemplate>

                                                                    </telerik:GridTemplateColumn>
                                                                        
                                                                    
                                                                    <telerik:GridBoundColumn SortExpression="DocumentType" HeaderText="Document Type"  HeaderButtonType="TextButton"
                                                                        DataField="DocumentType">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="DocumentIssuer" HeaderText="Document Issuer" HeaderButtonType="TextButton" DataField="DocumentIssuer"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="DocumentNo" HeaderText="Document No" HeaderButtonType="TextButton" DataField="DocumentNo">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="IssueDate" HeaderText="Issue Date"  HeaderButtonType="TextButton" DataField="IssueDate">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="Expirydate" HeaderText="Expiry Date" HeaderButtonType="TextButton" DataField="Expirydate">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="CurrStatusDescr" HeaderText="Status" HeaderButtonType="TextButton" DataField="CurrStatusDescr">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Download" UniqueName="TemplateColumn2" Groupable="False" 
                                                                     AllowFiltering="False">
                                                                         <ItemTemplate> 
                                                                             <asp:LinkButton ID="lbtnGrdtracking" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_FILE_name") %>' runat="server"></asp:LinkButton>
                                                                         </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn> 
                                                           </Columns>
                                                     </MasterTableView>
                                                </telerik:radgrid>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr  >
                                                <td align="left"><span class="field-label">Non Tracked Documents</span>
                                                </td>
                                            </tr>
                                            <tr class="table table-bordered table-row">
                                                <td align="left">
                                                    <div class="table-responsive divDashboardContainer">
                                                        <telerik:radgrid id="grdNonTracked" runat="server"
                                                            allowpaging="True" cssclass="table table-bordered  table-row"
                                                            cellspacing="0" gridlines="None" enabletheming="False" autogeneratecolumns="False" 
                                                            onitemcommand="Grid_Document_ItemCommand" pagesize="15" xmlns:telerik="telerik.web.ui">
                                                    <MasterTableView DataKeyNames="Doc_ID,SM_COLOR,DOI_ID,DOI_FILE_PATH,Dot_Id" NoMasterRecordsText="No documents to display" > 
                                                                <Columns>
                                                                    <%--<telerik:GridBoundColumn SortExpression="Doc_ID" HeaderText="Document No" HeaderButtonType="TextButton"
                                                                        DataField="Doc_ID">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="Businessunit" HeaderText="Business Unit"  HeaderButtonType="TextButton"
                                                                        DataField="Businessunit">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="BSU" HeaderText="BSU"  HeaderButtonType="TextButton" DataField="BSU" >
                                                                    </telerik:GridBoundColumn>--%>
                                                                    <telerik:GridBoundColumn SortExpression="DocumentType" HeaderText="Document Type"  HeaderButtonType="TextButton"
                                                                        DataField="DocumentType">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="DocumentIssuer" HeaderText="Document Issuer" HeaderButtonType="TextButton" DataField="DocumentIssuer"></telerik:GridBoundColumn>
                                                                   <%-- <telerik:GridBoundColumn SortExpression="DocumentNo" HeaderText="Document No" HeaderButtonType="TextButton" DataField="DocumentNo">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="IssueDate" HeaderText="Issue Date"  HeaderButtonType="TextButton" DataField="IssueDate">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn SortExpression="Expirydate" HeaderText="Expiry Date" HeaderButtonType="TextButton" DataField="Expirydate">
                                                                    </telerik:GridBoundColumn>--%>
                                                                    <telerik:GridBoundColumn SortExpression="CurrStatusDescr" HeaderText="Status" HeaderButtonType="TextButton" DataField="CurrStatusDescr">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Upload" UniqueName="TemplateColumn1" Groupable="False" 
                                                                     AllowFiltering="False">
                                                                         <ItemTemplate> 
                                                                             <asp:FileUpload ID="fupGrdnontracking" runat="server" Width="200px" ToolTip='Click "Browse" to select the file.' />
                                                                             <asp:Button ID="btngrdFupload" runat="server" Text="Upload" OnClick="btngrdFupload_Click"/>
                                                                             <asp:HiddenField ID="hfGrdFilePath" runat="server" Value='' />
                                                                            <asp:HiddenField ID="hfGrdFileName" runat="server" />
                                                                            <asp:HiddenField ID="hfGrdFileType" runat="server" />
                                                                         </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn> 
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Download" UniqueName="TemplateColumn2" Groupable="False" 
                                                                     AllowFiltering="False">
                                                                         <ItemTemplate> 
                                                                             <asp:LinkButton ID="lbtnGrdnontracking" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_FILE_name") %>' runat="server"></asp:LinkButton>
                                                                         </ItemTemplate> 
                                                                    </telerik:GridTemplateColumn> 
                                                           </Columns>
                                                     </MasterTableView>
                                                </telerik:radgrid>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                              
                                

                                  <tr style="display: none;">
                                    <td align="left"><span class="field-label">Document Type</span><span class="text-danger">*</span></td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlDocType" AutoPostBack="true" runat="server" ></asp:DropDownList>
                                    </td>
                                      <td align="left" ></td>
                                     <td align="left" ></td>
                                </tr>
                                <tr id="EditDetails4" runat="server">
                                    <td align="left"><span class="field-label">Upload Original Document</span><span class="text-danger">*</span></td>
                                
                                    <td colspan="3" align="left">
                                        <asp:Panel ID="UploadImagepnl" runat="server">
                                            <asp:FileUpload ID="UploadDocPhoto" runat="server" ToolTip='Click "Browse" to select the file.' />
                                            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button"  />
                                            <asp:LinkButton ID="btnDocumentLink" Visible="false" OnClientClick="return showDocument()" runat="server" CssClass="btn btn-primary button" >
                                                <asp:Image ID="imgFolder" runat="server" ImageUrl="~/Images/acrobat_icon.jpg" Height="50px" Width="50px" /><br />
                                            </asp:LinkButton><br />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" >
                                        <asp:DataGrid ID="grdImage" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CaptionAlign="Top" PageSize="15" CssClass="table table-bordered table-row">
                                            <HeaderStyle ></HeaderStyle>
                                            <AlternatingItemStyle CssClass="griditem_alternative "></AlternatingItemStyle>
                                            <ItemStyle ></ItemStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Document Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtDocNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_TYPE") %>'></asp:Label>
                                                        <asp:HiddenField ID="hdnDoi_Id" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "doi_id") %>' />
                                                        <asp:HiddenField ID="hdnDoi_File_Name" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "doi_id") %>' />
                                                        <asp:HiddenField ID="hdnDoi_File_NameTemp" runat="server" />
                                                        <asp:HiddenField ID="hdnDoi_content_type" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Library" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtIssueDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_LIBRARY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Folder" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtExpiryDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_FOLDER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="File Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnDocumentLink" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_FILE_name") %>' runat="server"></asp:LinkButton>
                                                        <asp:HiddenField ID="hdn_DOI_PATH" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "DOI_PATH") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--<asp:ButtonColumn Text="Edit" CommandName="Edit" Visible="false"  ></asp:ButtonColumn>--%>
                                                <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                                <tr class="title-bg-lite" id="TrFullVersion2" runat="server" visible ="false" >
                                    <td colspan="4" >Full version Upload
                                    </td>
                                </tr>
                                <tr id="TrFullVersion" runat="server"  visible ="false" >
                                    <td align="left"><span class="field-label">Upload Full Version Document</span><span class="text-danger">*</span></td>
                                
                                    
                                    <td colspan="3" align="left">
                                        <asp:Panel ID="UploadFullversionpnl" runat="server">
                                            <asp:FileUpload ID="UploadDocFullversion" runat="server" ToolTip='Click "Browse" to select the file.' />
                                            <asp:Button ID="btnFullVersion" runat="server" Text="Upload FullVersion" CssClass="button"  />
                                            <asp:LinkButton ID="lnkFullVersion" Visible="false" OnClientClick="return showDocument()" runat="server" CssClass="btn btn-primary button" >
                                                <asp:Image ID="imgFullversion" runat="server" ImageUrl="~/Images/acrobat_icon.jpg" Height="50px" Width="50px" /><br />
                                            </asp:LinkButton><br />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                 <tr id="TrFullVersion1" runat="server" visible ="false"  >
                                    <td colspan="4" align="center" >
                                        <asp:DataGrid ID="GRDFullVersion" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CaptionAlign="Top" PageSize="15" CssClass="table table-bordered table-row">
                                            <HeaderStyle ></HeaderStyle>
                                            <AlternatingItemStyle CssClass="griditem_alternative "></AlternatingItemStyle>
                                            <ItemStyle ></ItemStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Document Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtDocNo_FV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_TYPE") %>'></asp:Label>
                                                        <asp:HiddenField ID="hdnDoi_Id_FV" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "doi_id") %>' />
                                                        <asp:HiddenField ID="hdnDoi_File_Name_FV" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "doi_id") %>' />
                                                        <asp:HiddenField ID="hdnDoi_File_NameTemp_FV" runat="server" />
                                                        <asp:HiddenField ID="hdnDoi_content_type_FV" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Library" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtLibFV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_LIBRARY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Folder" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtFolderFv" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_FOLDER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="File Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnDocumentLink_FV" Text='<%# DataBinder.Eval(Container.DataItem, "DOI_FILE_name") %>' runat="server"></asp:LinkButton>
                                                        <asp:HiddenField ID="hdn_DOI_PATH_FV" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "DOI_PATH") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                
                                                <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td  colspan="4">
                                        <asp:Label ID="lblObservationNew" runat="server"></asp:Label>
                                        
                                    </td>
                                </tr>
                                <tr id="TermsCond" runat="server" visible="false">
                                    <td colspan="4">
                                        <asp:CheckBox ID="TermsandConditions" runat="server"></asp:CheckBox>
                                        <asp:LinkButton ID="lnkTermsandConditions" runat="server" Text="I have read and accept these Terms and Conditions" CssClass="btn-anchor" Font-Bold="True" Font-Italic="True" Font-Underline="True"></asp:LinkButton>
                                        <ajaxToolkit:ModalPopupExtender ID="mpe1" runat="server" TargetControlID="lnkTermsandConditions" BackgroundCssClass="modalbackground" PopupControlID="TermsDiv" CancelControlID="lbPnlJobClose"></ajaxToolkit:ModalPopupExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Add" />

                                        <asp:Button ID="btnEdit" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" Visible="false" Text="Save" CssClass="btn btn-primary button"  />
                                        <asp:Button ID="btnfvSave" runat="server" Visible="false" Text="SaveFullVersion" CssClass="btn btn-primary button"  />
                                        <asp:Button ID="btnDelete" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                                        <asp:Button ID="btnRenew" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Renew" />
                                        <asp:Button ID="btnRetire" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Retire" />
                                        <asp:Button ID="btnForward" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Forward" />
                                        <asp:Button ID="btnApprove" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Approve" />
                                        <asp:Button ID="btnRevert" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Revert" OnClientClick=" return showBlock('REVERT');" />
                                        <asp:Button ID="btnBlock" runat="server" Visible="false" CausesValidation="False" CssClass="btn btn-primary button"  Text="Block" OnClientClick=" return showBlock('BLOCK');" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"  Text="Close window" />
                                        <asp:HiddenField ID="h_ACTTYPE" runat="server" />
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_Doc_Olu_Name" runat="server" />
                                        <asp:HiddenField ID="h_OLD_DOC_ID" runat="server" />
                                        <asp:HiddenField ID="h_delete" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_DocumentPath" runat="server" />
                                        <asp:HiddenField ID="h_DocumentPathFullVersion" runat="server" />
                                        <asp:HiddenField ID="h_Library" runat="server" />
                                        <asp:HiddenField ID="h_Folder" runat="server" />
                                        <asp:HiddenField ID="h_BSUID" runat="server" />
					                    <asp:HiddenField ID="h_BSUNAME" runat="server" />
                                        <asp:HiddenField ID="h_UserId" runat="server" />
                                        <asp:HiddenField ID="h_DOC_Status" runat="server" />
                                        <asp:HiddenField ID="h_DOC_Deleted" runat="server" />
                                        <asp:HiddenField ID="h_status" runat="server" />
                                        <asp:HiddenField ID="h_SourceFolderPath" runat="server" />
                                        <asp:HiddenField ID="h_sourceFileName" runat="server" />

                                        <asp:HiddenField ID="h_SourceFolderPath_fullversion" runat="server" />
                                        <asp:HiddenField ID="h_sourceFileName_FullVersion" runat="server" />

                                        <asp:HiddenField ID="h_SharepointPath" runat="server" />
                                        <asp:HiddenField ID="h_ForwardText" runat="server" />
                                        <asp:HiddenField ID="hfbtnEdit" runat="server" />
                                        <asp:HiddenField ID="hfbtnSave" runat="server" />
                                        <asp:HiddenField ID="hfbtnRenew" runat="server" />
                                        <asp:HiddenField ID="hfbtnRetire" runat="server" />
                                        <asp:HiddenField ID="hfbtnForward" runat="server" />
                                        <asp:HiddenField ID="hfbtnApprove" runat="server" />
                                        <asp:HiddenField ID="hfbtnBlock" runat="server" />

                                        <asp:HiddenField ID="h_FullVersion_SourceFolderPath" runat="server" />
                                        <asp:HiddenField ID="h_FullVersion_sourceFileName" runat="server" />
                                        <asp:HiddenField ID="h_FullVersion_SharepointPath" runat="server" />


                                        <asp:HiddenField ID="hdnDoi_File_Name_FV" runat="server" />
                                        <asp:HiddenField ID="hdnDoi_content_type_FV" runat="server" />
                                        <asp:HiddenField ID="hdnDoi_File_NameTemp_FV" runat="server" />
                                        


                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div id="TermsDiv" runat="server" style="display: none; width: 80%;" class="panel-cover">
                    <table style="width: 100%">
                        <tr>
                            <td align="right">
                                <asp:LinkButton ID="lbPnlJobClose" CssClass="closebtnFee" runat="server" OnClientClick="return false;"><span style="padding: 1px 4px;background-color: #ccc6c6;border:1px solid #999;color:  #333;border-radius:  4px;box-shadow: 1px 2px 5px rgba(0,0,0,0.1);text-shadow: 2px 0px 2px rgba(0,0,0,0.4);">X</span></asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Terms" SkinID="MultiText" Width="100%" Height="100%" runat="server" Text=""></asp:Label>
                                © 2018 GEMS MENASA IPCO (Cayman) Limited. ALL RIGHTS RESERVED. No part of this database may be reproduced or transmitted in any form or by any means,
                    electronic or mechanical including photocopying, recording, downloading, digital transmission, imaging, or any other information storage and retrieval 
                    system without the written permission of GEMS MENASA IPCO (Cayman) Limited.This licensing and leasing database is a proprietary database of GEMS MENASA
                    (Cayman) Limited (together with its affiliates, “GEMS Education”). By clicking on the [SUBMIT] button below you acknowledge and accept the Conditions of 
                    Access and further acknowledge and accept that this database contains personal information about GEMS Education employees and other individuals not affiliated 
                    with GEMS Education as well as commercially sensitive information about GEMS Education, none of which may be disclosed, copied, reproduced, furnished, 
                    or distributed to any person without the consent of GEMS Education. By accessing this database, you acknowledge that your activities including downloading 
                    of any documents will be recorded and monitored for compliance purposes.I am being granted access to certain information contained on this database in order 
                    to review certain information of GEMS Education as well as to download documents in relation to renewal of licenses and leases, as applicable.
                        <br />
                                I understand that my access to this database is subject to the following conditions:<br />
                                1.	All of the information contained on this database is considered
                    confidential, and is subject to the confidentiality undertakings in my employment contract.<br />
                                2.	I will not attempt to download, scan, copy, print or
                    otherwise capture any of the information contained on the database, except that I may view, print, or download information for which the view, print,
                    or download capability has been enabled as indicated by the database index.
                        <br />
                                3.	I will not attempt to circumvent any of the security features 
                    of the database, and will not enable or allow others to access the database using my authorization to the database. 
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1" AssociatedUpdatePanelID="Upnl1">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="" runat="server">
                    <div id="processMessage" class="progMsg_Show">
                        <img alt="Loading..." src="~/Images/loader.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>

