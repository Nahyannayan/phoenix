﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net
Imports System.IO

Partial Class DocTracker_DM_BlockUnblockDocument
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = ConnectionManger.GetOASIS_CLMConnectionString
    Private Sub DocTracker_DM_BlockUnblockDocument_Load(sender As Object, e As EventArgs) Handles Me.Load
        h_View_id.Value = Request.QueryString("viewid").Replace(" ", "+")
        ViewState("TYPE") = String.Empty
        If Not Request.QueryString("TYPE") Is Nothing Then
            ViewState("TYPE") = Request.QueryString("TYPE").Replace(" ", "+")
        End If
        showDetails(h_View_id.Value)
        If ViewState("TYPE") = "REVERT" Then
            btnBlock.Text = "Revert"
            TrUnblockdetails.Visible = False
            tdTitle.InnerText = "Revert Document"
        Else
            tdTitle.InnerText = "Blocking\Unblocking Document"
        End If
    End Sub
    Private Sub showDetails(ByVal EntryId As Integer)
        Dim Sqlstr As String = "Select iss_descr Doc_Issuer, BSU_NAME,DOC_BSU_ID, [Doc_id], [Doc_Dot_id],isnull(DOC_COMMENTS,'')DOC_COMMENTS, DOT_DESCR,Doc_Narration, [Doc_LeadDays],isnull(DOC_BLOCKED_LAST_COMMENTS,'')DOC_BLOCKED_LAST_COMMENTS,CONVERT(VARCHAR(20),DOC_BLOCKED_TENTATIVE_DATE,106)DOC_BLOCKED_TENTATIVE_DATE, [Doc_Olu_Name], [Doc_Bsu_Id], [Doc_Iss_Id], [Doc_SysDate], ISNULL([DOC_COMMENTS],'')[DOC_COMMENTS], isnull(DOC_STATUS,'A')DOC_STATUS, [DOC_OLD_DOC_ID], ISNULL([DOC_DELETED],0) DOC_DELETED,CONVERT(VARCHAR(20),DOC_ISSDATE,106) DOC_ISSDATE,CONVERT(VARCHAR(20),DOC_EXPDATE,106)DOC_EXPDATE,isnull(DOC_NO,'')DOC_NO from DocUpload INNER JOIN OASIS.dbo.BUSINESSUNIT_M WITH(NOLOCK) on BSU_ID=Doc_Bsu_Id inner join DocType WITH(NOLOCK) on Dot_Id=Doc_Dot_id INNER JOIN DocIssuer WITH(NOLOCK) on Iss_id=Dot_Iss_Id where doc_Id=" & EntryId
        Dim rdrDocDetails As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, Sqlstr)
        If rdrDocDetails.HasRows Then
            rdrDocDetails.Read()
            lbldoctype.Text = rdrDocDetails("DOT_DESCR")
            lblIssuedTo.Text = rdrDocDetails("BSU_NAME")
            lblIssuer.Text = rdrDocDetails("Doc_Issuer")
            lblDocno.Text = rdrDocDetails("DOC_NO")
            lblIssueDate.Text = rdrDocDetails("DOC_ISSDATE")
            lblExpDate.Text = rdrDocDetails("DOC_EXPDATE")
            lblDocDetails.Text = rdrDocDetails("Doc_Narration")
            lbllastComments.Text = rdrDocDetails("DOC_BLOCKED_LAST_COMMENTS")
            h_DOC_Status.Value = rdrDocDetails("DOC_STATUS")
            h_BSU_Id.Value = rdrDocDetails("DOC_BSU_ID")
            If lbllastComments.Text = "" Then
                Trcomments.Visible = False
            Else
                Trcomments.Visible = True
            End If
            If rdrDocDetails("DOC_STATUS") = "B" Then
                btnBlock.Text = "Unblock Document"
                txtDate.Text = rdrDocDetails("DOC_BLOCKED_TENTATIVE_DATE")
                lnkDate.Enabled = False
            End If
        End If
        rdrDocDetails.Close()
        rdrDocDetails = Nothing
    End Sub
    Private Sub btnBlock_Click(sender As Object, e As EventArgs) Handles btnBlock.Click
        If txtComments.Text = "" Then
            'lblError.Text = "Enter Commentes..."
            ShowMessage("Error: Comments cannot be empty.")
            Exit Sub
        End If
        If h_DOC_Status.Value = "A" And txtDate.Text = "" And ViewState("TYPE") <> "REVERT" Then
            'lblError.Text = "Date should not be empty..."
            ShowMessage("Error: Please enter the date and continue..")
            Exit Sub
        End If
        Dim myProvider As IFormatProvider = New System.Globalization.CultureInfo("en-CA", True)
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOC_ID", SqlDbType.Int)
        pParms(0).Value = h_View_id.Value
        pParms(1) = New SqlClient.SqlParameter("@ACTION", SqlDbType.VarChar, 100)
        If ViewState("TYPE") = "REVERT" Then
            pParms(1).Value = "RV"
        Else
            If UCase(btnBlock.Text) = "BLOCK DOCUMENT" Then
                pParms(1).Value = "B"
            Else
                pParms(1).Value = "U"
            End If
        End If
        pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 100)
        pParms(2).Value = Session("sUsr_name")
        pParms(3) = New SqlClient.SqlParameter("@BLOCKDATE", SqlDbType.DateTime)
        If UCase(btnBlock.Text) = "BLOCK DOCUMENT" Then
            pParms(3).Value = txtDate.Text
        Else
            pParms(3).Value = "01-01-1900"
        End If

        pParms(4) = New SqlClient.SqlParameter("@COMMENTS", SqlDbType.VarChar)
        pParms(4).Value = txtComments.Text

        pParms(5) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(5).Value = h_BSU_Id.Value

        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue

        pParms(7) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 1000)
        pParms(7).Direction = ParameterDirection.Output

        pParms(8) = New SqlClient.SqlParameter("@SUCCESS_MSG", SqlDbType.VarChar, 1000)
        pParms(8).Direction = ParameterDirection.Output

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SP_ChangeDOC_Status", pParms)
            Dim ErrorMSG As String = pParms(7).Value.ToString()
            If ErrorMSG = "" Then
                stTrans.Commit()
                ShowMessage(pParms(8).Value.ToString(), False)
                btnBlock.Enabled = False
                btnBlock.Visible = False
            Else
                stTrans.Rollback()
                ShowMessage(ErrorMSG)
            End If
        Catch ex As Exception
            ShowMessage("Error: " & ex.Message)
            'lblError.Text = ex.Message + "!!!"
            stTrans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then objConn.Close()
        End Try
    End Sub
    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
    End Sub
    Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
        If Message <> "" Then
            preError.Attributes.Remove("class")
            If bError Then
                preError.Attributes.Add("class", "alert alert-error")
            Else
                preError.Attributes.Add("class", "alert alert-success")
            End If
        Else
            preError.Attributes.Remove("class")
            preError.Attributes.Add("class", "invisible")
        End If
        preError.InnerHtml = Message
    End Sub
End Class
