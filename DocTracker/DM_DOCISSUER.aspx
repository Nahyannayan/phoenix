﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_DOCISSUER.aspx.vb" Inherits="DOCTRACKER_DOCISSUER" Title="Add/Edit Document Issuer" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Document Issuer
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4" valign="middle">
                                        <div align="left">Document Issuing Authority</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDocIssuer" runat="server" MaxLength="500"></asp:TextBox></td>
                                    <td align="left" width="20%">
                                        <%--<span class="field-label">Document Group</span>--%>

                                    </td>

                                    <td align="left" width="30%">
                                        <%--<asp:DropDownList ID="ddlDocumentGroup" runat="server"></asp:DropDownList>--%>

                                    </td>
                                </tr>



                                <tr>
                                    <td align="left"><span class="field-label">City\Country</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlDocumentCity" runat="server"></asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Short Name</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtShortName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4" valign="middle">
                                        <div align="left">Issuing Authority Contact Details</div>
                                    </td>
                                </tr>



                                <tr>
                                    <td align="left"><span class="field-label">Contact Details</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtDetails" runat="server" TabIndex="160" TextMode="MultiLine"></asp:TextBox>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Phone </span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="500"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Fax</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFax" runat="server" MaxLength="100"></asp:TextBox></td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Email</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="100"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Website</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtWebSite" runat="server" MaxLength="100"></asp:TextBox></td>

                                </tr>

                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4" valign="middle">
                                        <div align="left">Contact Person to obtain Information</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Contact Person</span></td>


                                    <td align="left">
                                        <asp:TextBox ID="txtOrgContactPerson" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>


                                <tr>
                                    <td align="left"><span class="field-label">Phone </span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtOrgContactPhone" runat="server" MaxLength="50"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Email</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtOrgContactEmail" runat="server" MaxLength="50"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Organization Contact Details</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtOrgDetails" runat="server" TabIndex="160" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                    </td>
                                </tr>




                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
        </div>
    </div>

</asp:Content>

