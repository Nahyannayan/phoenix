﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObjTreeItemCategory
Partial Class DOCTRACKER_DOCNOTIFICATIONMASTER
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindEmployeeBsu()
            BindEmpName()

            BindMainTabs()

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Session("sUsr_name") = "" Or (ViewState("MainMnu_code") <> "DM00013") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                SetDataMode("view")
                setModifyvalues(ViewState("EntryId"))

            Else
                SetDataMode("add")
                setModifyvalues(0)
            End If

        Else


        End If

    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"

        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"

        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"

        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable

        gvEMPName.Enabled = EditAllowed
        ddlDocGroup.Enabled = EditAllowed
        ddbsu.Enabled = EditAllowed
        txtDescr.Enabled = EditAllowed
        txtLevel1Days.Enabled = EditAllowed
        ddlDocType.Enabled = EditAllowed
        btnsave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnCancel.Visible = Not mDisable
        btnDelete.Visible = Not mDisable
        txtNotification.Enabled = EditAllowed


    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        h_EntryId.Value = p_Modifyid
        If p_Modifyid = 0 Then
            lblOtherBsus.Visible = False
            CheckBSUs(ddlDocType.SelectedValue, False)

        Else

            Dim str_Sql As String
            str_Sql = "select DEN_ID,isnull(DEN_NOTIFICATION_TEXT,'')DEN_NOTIFICATION_TEXT,den_Descr,den_type,den_grp_id,den_level_1_days,ISNULL(DGR_DESCR,'--------Select One--------')DGR_DESCR,ISNULL(DEN_GRP_ID,0)DEN_GRP_ID,ISNULL(DOT_DESCR,'--------ALL----------')DOT_DESCR,ISNULL(DEN_DOT_ID,0)DEN_DOT_ID,DEN_BSU_ID,BSU_NAME,isnull(stuff((select BSU_SHORTNAME +',' FROM DOC_EMAIL_NOTIFICATION_D inner join oasis..businessunit_m on BSU_ID=DED_BSU_ID and ded_den_id=" & p_Modifyid & "  for xml path(''), type ).value('.','varchar(max)'), 1, 0, ''),'') BSU_List   from DOC_EMAIL_NOTIFICATION_M LEFT OUTER join docgroup ON dgr_id=DEN_GRP_ID LEFT OUTER  join DocType ON dOT_id=DEN_DOT_ID INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=DEN_BSU_ID WHERE  DEN_ID='" & p_Modifyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                h_EntryId.Value = ds.Tables(0).Rows(0)("DEN_ID")
                txtDescr.Text = ds.Tables(0).Rows(0)("den_Descr")
                ddlDocGroup.SelectedValue = ds.Tables(0).Rows(0)("DEN_GRP_ID")
                ddlDocGroup.SelectedItem.Text = ds.Tables(0).Rows(0)("DGR_DESCR")
                ddbsu.SelectedItem.Text = ds.Tables(0).Rows(0)("BSU_NAME")
                ddbsu.SelectedItem.Value = ds.Tables(0).Rows(0)("DEN_BSU_ID")
                txtNotification.Text = ds.Tables(0).Rows(0)("DEN_NOTIFICATION_TEXT")
                txtLevel1Days.Text = ds.Tables(0).Rows(0)("den_level_1_days")
                lblOtherBsus.Text = ds.Tables(0).Rows(0)("BSU_List")
                lblOtherBsus.Visible = True
                BindMainTabs()
                CheckBSUs(p_Modifyid)

                ddlDocType.SelectedValue = ds.Tables(0).Rows(0)("DEN_DOT_ID")
                ddlDocType.SelectedItem.Text = ds.Tables(0).Rows(0)("DOT_DESCR")
            Else

            End If


        End If
        Bindgrid()

    End Sub
    Private Sub Bindgrid()
        fillGridView(gvEMPName, "Select DEF_ID ,DEF_DOM_ID, DESCR, isnull(DEF_CC,0) DEFCC FROM OASIS_CLM..DOC_EMAIL_NOTIFICATION_f inner join OASIS_CLM..VW_DOC_DES_LIST on id=DEF_DOM_ID  WHERE isnull(DEF_DELETED,0)=0 and DEF_DEN_ID = " & h_EntryId.Value & " order by DESCR")
        gvEMPName.DataSource = DTNotificationList
        gvEMPName.DataBind()
        showNoRecordsFound()
    End Sub
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        DTNotificationList = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, fillSQL).Tables(0)
        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTNotificationList)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTNotificationList = mtable
        fillGrdView.DataSource = DTNotificationList
        fillGrdView.DataBind()
        showNoRecordsFound()
    End Sub
    Private Sub showNoRecordsFound()
        If DTNotificationList.Rows(0)(1) = -1 Then
            Dim TotalColumns As Integer = gvEMPName.Columns.Count - 2
            gvEMPName.Rows(0).Cells.Clear()
            gvEMPName.Rows(0).Cells.Add(New TableCell())
            gvEMPName.Rows(0).Cells(0).ColumnSpan = TotalColumns
            gvEMPName.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub


    Private Sub Bindsubdoc()
        For Each row As GridViewRow In gvEMPName.Rows
            '     Dim AlerFor As String = gvEMPName.DataKeys(row.RowIndex).Value
            Dim ddlAlert As DropDownList = DirectCast(row.FindControl("ddlalertfor"), DropDownList)
            Dim Alertfor As Label = DirectCast(row.FindControl("lblAlertFor"), Label)

            ddlAlert.SelectedValue = Alertfor.Text
            ddlAlert.SelectedItem.Text = Alertfor.Text

        Next
    End Sub

    Public Sub BindEmployeeBsu()
        Dim ds As New DataSet

        Dim query = " select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 " &
                    " union select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M WHERE BSU_CRM_NON_GEMS='True' order by BSU_NAME  "

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)
        ddbsu.DataSource = ds
        ddbsu.DataTextField = "BSU_NAME"
        ddbsu.DataValueField = "BSU_ID"
        ddbsu.DataBind()
        ddbsu.SelectedValue = Session("sbsuid")
    End Sub


    Public Sub BindMainTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString()
        Dim str_conn1 = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString()

        Dim Sql_Query = "Select 'DESIGNATION' USERTYPE union all select 'EMPLOYEE'"
        'Dim Sql_Query1 = "select DOt_ID,DOT_DESCr from doctype union all  select 0,'--------All--------' order by dot_descr"
        'Dim Sql_Query1 = "Select  DOt_ID,DOT_DESCr from doctype inner Join DocIssuer On iss_id=Dot_Iss_Id inner Join DocGroup On dgr_id=ISS_DGR_ID where dgr_id =" & ddlDocGroup.SelectedValue & " and isnull(DOT_TRACKING_ID,0)=1 union all  Select 0,'--------Select One--------' order by dot_descr"

        Dim docTypeParams(1) As SqlParameter
        docTypeParams(1) = Mainclass.CreateSqlParameter("@DEN_GRP_ID", ddlDocGroup.SelectedValue, SqlDbType.Int)

        'Dim Sql_Query1 = "Select  DOt_ID,DOT_DESCr from doctype inner Join DocIssuer On iss_id=Dot_Iss_Id inner Join DocGroup On dgr_id=ISS_DGR_ID where dgr_id =" & ddlDocGroup.SelectedValue & " and isnull(DOT_TRACKING_ID,0)=1 union all  Select 0,'--------Select One--------' order by dot_descr"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        'Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, Sql_Query1)
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn1, CommandType.StoredProcedure, "DOC_LOADDOCTYPES", docTypeParams)
        ddlDocType.DataSource = ds1
        ddlDocType.DataTextField = "DOT_DESCr"
        ddlDocType.DataValueField = "DOt_ID"
        ddlDocType.DataBind()
        BindSubTabs()
    End Sub

    Public Sub BindSubTabs()
        BindJobCategory()
    End Sub
    Public Sub GetAccessSettings_Tree(ByVal BSUIDs As String, Optional ByVal Loading As Boolean = False)


        Dim tr As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnection.ConnectionString
        Dim con As New SqlConnection(str_conn)
        Dim ds As DataSet
        Try
            Dim tmpBsu As String = ""

            con.Open()


            ds = SqlHelper.ExecuteDataset(con, CommandType.Text, "select BSU_ID,BSU_NAME from oasis..businessunit_m where bsu_id in ('" & BSUIDs & "')")

            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then

                For Each row As DataRow In ds.Tables(0).Rows
                    tmpBsu = row.Item("bsu_id")
                    'Dim tv As TreeView = TreeItemCategory.FindControl("BSU_ID")
                    'If tmpBsu = tv Then

                    'End If


                    For Each TN As TreeNode In TreeItemCategory.Nodes
                        If TN.Text = tmpBsu Then
                            TN.Selected = True
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            'lblError.Text = "Error occured getting allowed business unit(s) for the selected source and fee type"
            UtilityObj.Errorlog(ex.Message)
        Finally
            If Not con.State = ConnectionState.Closed Then con.Close()
        End Try
    End Sub

    Private Sub CheckBSUs(ByVal DEN_ID As Integer, Optional ByVal Preload As Boolean = True)
        Dim tr As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_CLMConnectionString
        Dim con As New SqlConnection(str_conn)
        Dim ds As DataSet
        Try
            Dim tmpBsu As String = ""

            con.Open()

            If Preload = True Then
                ds = SqlHelper.ExecuteDataset(con, CommandType.Text, "select BSU_ID,BSU_NAME from oasis..businessunit_m where bsu_id in (SELECT DED_BSU_ID FROM DOC_EMAIL_NOTIFICATION_D where DED_DEN_ID=" & DEN_ID & ")")
            Else
                ds = SqlHelper.ExecuteDataset(con, CommandType.Text, "select BSU_ID,BSU_NAME from oasis..businessunit_m where bsu_id in (SELECT ID FROM  OASIS.dbo.fnSplitMe((select Dot_bsu_apply from doctype where dot_id=" & DEN_ID & "),'|'))")
            End If
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then

                For Each row As DataRow In ds.Tables(0).Rows
                    tmpBsu = row.Item("bsu_id")
                    CheckSelectedNode(row.Item("bsu_id"), TreeItemCategory.Nodes)
                Next
            Else

            End If
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
        Finally
            If Not con.State = ConnectionState.Closed Then con.Close()
        End Try

    End Sub


    Private Function GetNavigateDepartmentEdit(ByVal pId As String) As String

        'Return String.Format("javascript:var popup = window.showModalDialog('YearlyDepartmentsEdit.aspx?dep_id={0}', '','dialogHeight:500px;dialogWidth:600px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

    End Function

    Private Sub CheckSelectedNode(ByVal vBSU_ID As String, ByVal vNodes As TreeNodeCollection)
        Dim ienum As IEnumerator = vNodes.GetEnumerator()
        Dim trNode As TreeNode
        While (ienum.MoveNext())
            trNode = ienum.Current
            If trNode.ChildNodes.Count > 0 Then
                CheckSelectedNode(vBSU_ID, trNode.ChildNodes)
            ElseIf trNode.Value = vBSU_ID Then
                trNode.Checked = True
            End If
        End While
    End Sub




    Public Sub BindJobCategory()

        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString()

        'Dim Sql_Query = "select distinct case when  bsu_city='AUH' then 'Aub Dhabi Schools' else case when bsu_city='DXB'  then 'Dubai Schools' else case when bsu_city='RAK'  then 'RAK Schools' else case when bsu_city='SHJ'  then 'Sharjah Schools'else case when bsu_city='ALN' then 'Al Ain Schools' else case when bsu_city='FUJ' then 'Fujaiarha Schools' else bsu_city end end  end end end end 'Schools'  from oasis..businessunit_m where bsu_country_id=172 and isnull(bsu_bschool,0)=1 union all select distinct 'Other Units(UAE)'  from oasis..businessunit_m where bsu_country_id=172 and   isnull(bsu_bschool,0)=1 union all select distinct 'Other Units(OUTSIDE UAE)'  from oasis..businessunit_m where bsu_country_id<>172 and   isnull(bsu_bschool,0)=0"
        Dim Sql_Query = "GetSchoolsForDocuments"
        Dim Parms(2) As SqlClient.SqlParameter
        Parms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        Parms(1) = New SqlClient.SqlParameter("@NODE", "M")
        Parms(2) = New SqlClient.SqlParameter("@SelectedSchool", "")

        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_DELETE_TASK_ROOTING_MASTER", pParms)




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, Sql_Query, Parms)
        Dim view As New DataView(ds.Tables(0))
        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())


        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("Schools").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("Schools").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next
        TreeItemCategory.CollapseAll()

    End Sub

    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        'Dim Sql_Query = "select School,bsu_name,bsu_id from (select distinct case when  bsu_city='AUH' then 'Aub Dhabi Schools' else case when bsu_city='DXB'  then 'Dubai Schools' else case when bsu_city='RAK'  then 'RAK Schools' else case when bsu_city='SHJ'  then 'Sharjah Schools'else case when bsu_city='ALN' then 'Al Ain Schools' else case when bsu_city='FUJ' then 'Fujaiarha Schools' else bsu_city end end  end end end end SCHOOL ,bsu_name,BSU_ID from oasis..businessunit_m where bsu_country_id=172 and isnull(bsu_bschool,0)=1 union all select distinct 'Other Units(UAE)',bsu_name,BSU_ID  from oasis..businessunit_m where bsu_country_id=172 and   isnull(bsu_bschool,0)=1 union all select distinct 'Other Units(OUTSIDE UAE)',bsu_name,BSU_ID  from oasis..businessunit_m where bsu_country_id<>172 and   isnull(bsu_bschool,0)=0) a where school='" & childnodeValue & "'"
        Dim Sql_Query = "GetSchoolsForDocuments"
        Dim CNodeParms(2) As SqlClient.SqlParameter
        CNodeParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        CNodeParms(1) = New SqlClient.SqlParameter("@NODE", "C")
        CNodeParms(2) = New SqlClient.SqlParameter("@SelectedSchool", childnodeValue)


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, Sql_Query, CNodeParms)

        Dim view As New DataView(ds.Tables(0))



        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("BSU_NAME").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("BSU_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If

        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next
    End Sub
    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged

        GetAccessSettings_Tree(ddbsu.SelectedValue)


    End Sub

    Public Sub BindEmpName()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString()
        Dim Sql_Query = " select DGR_ID,DGR_Descr from DocGroup union all select 0,'--------Select One--------' order by DGR_DESCR "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddlDocGroup.DataSource = ds
        ddlDocGroup.DataTextField = "DGR_DESCR"
        ddlDocGroup.DataValueField = "DGR_ID"
        ddlDocGroup.DataBind()


    End Sub


    Private Function ValidatePageData() As Boolean
        ValidatePageData = False
        'Business Unit
        If Me.ddbsu.SelectedIndex < 0 Then
            Me.lblmessage.Text = "Please select Business Unit"
            Exit Function
        End If

        If Not IsNumeric(Me.txtLevel1Days.Text) Or Val(Me.txtLevel1Days.Text) <= 0 Then
            Me.lblmessage.Text = "Please enter valid no for Level 1 Days. It should be numeric value and also not a decimal"
            Exit Function
        End If

        'Employee Grid
        If Me.gvEMPName.Rows.Count <= 0 Then
            Me.lblmessage.Text = "Please select atleast one employee who should be notified in the event of escalation of the task"
            Exit Function
        End If

        If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
            If CType(Session("dtTaskEscalationLevelsMasterEmployee"), DataTable).Rows.Count <= 0 Then
                Me.lblmessage.Text = "Please select atleast one escalation level for the selected employees"
                Exit Function
            End If
            For Each row As DataRow In CType(Session("dtTaskEscalationLevelsMasterEmployee"), DataTable).Rows
                If row.Item("Level1") = 0 And row.Item("Level2") = 0 And row.Item("Level3") = 0 Then
                    Me.lblmessage.Text = "Please select atleast one escalation level for the selected employees"
                    Exit Function
                End If
            Next
        End If

        Me.lblmessage.Text = ""
        Return True

    End Function



    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        SaveEmailNotificationData()



    End Sub
    Private Sub SaveEmailNotificationData()

        Dim objConn As New SqlConnection(connectionString)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim pParms(10) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@DEN_ID", h_EntryId.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@DEN_GRP_ID", ddlDocGroup.SelectedValue, SqlDbType.Int)
            pParms(3) = Mainclass.CreateSqlParameter("@DEN_LEVEL_1_DAYS", txtLevel1Days.Text, SqlDbType.Int)
            pParms(4) = Mainclass.CreateSqlParameter("@DEN_LOG_USER", Session("sUsr_name"), SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@DEN_DESCR", txtDescr.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@DEN_BSU_ID", ddbsu.SelectedValue, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@DEN_TYPE", "", SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@DEN_DOT_ID", ddlDocType.SelectedValue, SqlDbType.Int)
            pParms(9) = Mainclass.CreateSqlParameter("@DEN_NOTIFICATION_TEXT", txtNotification.Text, SqlDbType.VarChar)


            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOC_EMAIL_NOTIFICATION_M", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                ViewState("EntryId") = pParms(1).Value
            End If

            Dim strBSUnits As New StringBuilder
            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Length > 2 Then
                    strBSUnits.Append(node.Value)
                    strBSUnits.Append("||")
                End If
            Next

            Dim dt As DataTable
            Dim ds As DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "select bsu_id from oasis..BUSINESSUNIT_M where bsu_id in(select ID from OASIS.dbo.fnSplitMe('" & strBSUnits.ToString & "','|'))")
            dt = ds.Tables(0)
            For Each row In ds.Tables(0).Rows
                Try
                    Dim pParms1(3) As SqlParameter

                    pParms1(1) = Mainclass.CreateSqlParameter("@DED_ID", h_EntryId.Value, SqlDbType.Int, True)
                    pParms1(2) = Mainclass.CreateSqlParameter("@DED_DEN_ID", ViewState("EntryId"), SqlDbType.Int)
                    pParms1(3) = Mainclass.CreateSqlParameter("@DED_BSU_ID", row.Item("BSU_ID"), SqlDbType.Int)
                    Dim RetVal1 As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOC_EMAIL_NOTIFICATION_D", pParms1)
                    If RetVal1 = "-1" Then
                        lblError.Text = "Unexpected Error !!!"
                        stTrans.Rollback()
                        Exit Sub
                    Else
                        'ViewState("EntryId") = pParms(1).Value
                    End If
                Catch ex As Exception
                    stTrans.Rollback()
                    lblError.Text = ex.Message
                End Try

            Next



            Dim RowCount As Integer
            For RowCount = 0 To DTNotificationList.Rows.Count - 1
                Dim AddRolesParam(5) As SqlClient.SqlParameter
                Dim rowState As Integer = ViewState("EntryId")
                If DTNotificationList.Rows(RowCount).RowState = 8 Then rowState = -1
                AddRolesParam(1) = Mainclass.CreateSqlParameter("@DEF_ID", DTNotificationList.Rows(RowCount)("DEF_ID"), SqlDbType.Int, True)
                AddRolesParam(2) = Mainclass.CreateSqlParameter("@DEF_DEN_ID", ViewState("EntryId"), SqlDbType.Int)
                AddRolesParam(3) = Mainclass.CreateSqlParameter("@DEF_CC", DTNotificationList.Rows(RowCount)("DEFCC"), SqlDbType.Int)
                AddRolesParam(4) = Mainclass.CreateSqlParameter("@DEF_DOM_ID", DTNotificationList.Rows(RowCount)("DEF_DOM_ID"), SqlDbType.Int)
                'AddRolesParam(5) = Mainclass.CreateSqlParameter("@DEF_ALERTFOR", DTNotificationList.Rows(RowCount)("DEF_ALERTFOR"), SqlDbType.Int)
                AddRolesParam(5) = Mainclass.CreateSqlParameter("@DEF_ALERTFOR", 0, SqlDbType.Int)
                Dim RetValFooter As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "SAVE_DOC_EMAIL_NOTIFICATION_F", AddRolesParam)
                If RetValFooter = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                End If
            Next

            If h_Emp_Deleted.Value.Length > 0 Then
                Try
                    Dim DelIds As String = Mainclass.cleanString(h_Emp_Deleted.Value)
                    'Dim DeleteDocStr As String = " UPDATE DOC_EMAIL_NOTIFICATION_F set DEF_DELETED=1    WHERE DEF_ID in(select ID from oasis.dbo.fnSplitMe ('" & DelIds & "','|'))"
                    Dim DeleteDocStr As String = " Delete from DOC_EMAIL_NOTIFICATION_F   WHERE DEF_ID in(select ID from oasis.dbo.fnSplitMe ('" & DelIds & "','|'))"
                    Dim AuditStr As String = "INSERT  INTO DOCAUDITTRAIL ( DAT_DOC_ID, DAT_ACTION, DAT_REMARKS, DAT_USER, DAT_DATE )VALUES( '" & DelIds & "', 'D','DocumentOwner Deletion','" & Session("sUsr_name") & "', GETDATE())"


                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, DeleteDocStr)
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, AuditStr)

                Catch ex As Exception
                    lblError.Text = ex.Message
                    stTrans.Rollback()
                    Exit Sub
                End Try
            End If
            stTrans.Commit()
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            h_Emp_Deleted.Value = ""
            lblError.Text = "Data Saved Successfully !!!"
        Catch ex As Exception
            setModifyvalues(ViewState("EntryId"))
            stTrans.Rollback()
            lblError.Text = ex.Message
            Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub







    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
            Session.Remove("dtTaskEscalationLevelsMasterEmployee")
        End If

    End Sub

    Private Sub ddlDocGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDocGroup.SelectedIndexChanged
        BindMainTabs()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            SetDataMode("view")
            setModifyvalues(ViewState("EntryId"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then

        End If
    End Sub
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Private Sub gvEMPName_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvEMPName.RowCommand
        If e.CommandName = "AddNew" Then
            Dim txtDescr As TextBox = gvEMPName.FooterRow.FindControl("txtDescr")

            Dim txtEmpNo As TextBox = gvEMPName.FooterRow.FindControl("txtEmpNo")
            Dim ChkBox As CheckBox = gvEMPName.FooterRow.FindControl("chkLevel1")
            'Dim ddlAlertFor As DropDownList = gvEMPName.FooterRow.FindControl("ddlAlertFor")
            Dim DEF_DOM_ID As HiddenField = gvEMPName.FooterRow.FindControl("h_DEF_DOM_ID")
            If DTNotificationList.Rows(0)(1) = -1 Then DTNotificationList.Rows.RemoveAt(0)

            If txtDescr.Text = "" Then
                lblError.Text = "please select Approver First"
                Exit Sub
            End If

            Dim mrow As DataRow
            mrow = DTNotificationList.NewRow
            mrow("DEF_ID") = 0
            mrow("DESCR") = txtDescr.Text
            mrow("DEFCC") = ChkBox.Checked
            mrow("DEF_DOM_ID") = DEF_DOM_ID.Value

            DTNotificationList.Rows.Add(mrow)
            gvEMPName.EditIndex = -1
            gvEMPName.DataSource = DTNotificationList
            gvEMPName.DataBind()
            showNoRecordsFound()
        Else
            Dim txtInv_Rate As TextBox = gvEMPName.FooterRow.FindControl("txtType")
            Dim txtInv_Qty As TextBox = gvEMPName.FooterRow.FindControl("txtDescr")
        End If
    End Sub
    Private Sub gvEMPName_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvEMPName.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim imgRole_User As ImageButton = e.Row.FindControl("imgRole_User")

                Dim txtDescr As TextBox = e.Row.FindControl("txtDescr")

                Dim h_DEF_DOM_ID As HiddenField = e.Row.FindControl("h_DEF_DOM_ID")
                'imgRole_User.Attributes.Add("OnClick", "javascript:return getProduct('" & txtDescr.ClientID & "','" & h_DEF_DOM_ID.ClientID & "')")
                imgRole_User.Attributes.Add("OnClick", "javascript:getProduct('" & txtDescr.ClientID & "','" & h_DEF_DOM_ID.ClientID & "'); return false;")



            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub gvEMPName_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvEMPName.RowEditing
        Try
            gvEMPName.ShowFooter = False
            gvEMPName.EditIndex = e.NewEditIndex
            gvEMPName.DataSource = DTNotificationList
            gvEMPName.DataBind()

            Dim txtDescr As TextBox = gvEMPName.Rows(e.NewEditIndex).FindControl("txtDescr_e")
            Dim imgRoleUserE As ImageButton = gvEMPName.Rows(e.NewEditIndex).FindControl("imgRole_User_e")
            Dim h_DEF_DOM_ID As HiddenField = gvEMPName.Rows(e.NewEditIndex).FindControl("h_DEF_DOM_ID")
            'imgRoleUserE.Attributes.Add("OnClick", "javascript:return getProduct('" & txtDescr.ClientID & "','" & h_DEF_DOM_ID.ClientID & "')")
            imgRoleUserE.Attributes.Add("OnClick", "javascript:getProduct('" & txtDescr.ClientID & "','" & h_DEF_DOM_ID.ClientID & "'); return false;")

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvEMPName_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvEMPName.RowUpdating
        'Me.gvEMPName.Rows(e.RowIndex).Cells(1).Text = True

        Dim s As String = gvEMPName.DataKeys(e.RowIndex).Value.ToString()
        Dim lblDEF_ID As Label = gvEMPName.Rows(e.RowIndex).FindControl("lblDEF_ID")
        'Dim ddlAprType As DropDownList = gvEMPName.Rows(e.RowIndex).FindControl("ddlAlertFor")
        Dim txtDescr As TextBox = gvEMPName.Rows(e.RowIndex).FindControl("txtDescr_e")





        Dim hDEF_DOM_ID As HiddenField = gvEMPName.Rows(e.RowIndex).FindControl("h_DEF_DOM_ID")
        Dim ChkBox As CheckBox = gvEMPName.Rows(e.RowIndex).FindControl("chkLevel1")
        Dim mrow As DataRow
        mrow = DTNotificationList.Select("ID=" & s)(0)
        mrow("DEF_ID") = lblDEF_ID.Text
        mrow("DEF_DOM_ID") = hDEF_DOM_ID.Value

        mrow("DESCR") = txtDescr.Text
        'mrow("alertfor") = ddlAprType.SelectedItem.Text
        'mrow("DEF_ALERTFOR") = ddlAprType.SelectedValue
        mrow("DEFCC") = ChkBox.Checked
        gvEMPName.EditIndex = -1
        gvEMPName.ShowFooter = True
        gvEMPName.DataSource = DTNotificationList
        gvEMPName.DataBind()
    End Sub



    Private Sub gvEMPName_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvEMPName.RowCancelingEdit
        gvEMPName.ShowFooter = True
        gvEMPName.EditIndex = -1
        gvEMPName.DataSource = DTNotificationList
        gvEMPName.DataBind()
    End Sub

    Private Sub gvEMPName_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvEMPName.RowDeleting
        Dim mRow() As DataRow = DTNotificationList.Select("ID=" & gvEMPName.DataKeys(e.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            h_Emp_Deleted.Value &= mRow(0)("DEF_ID") & "|"
            DTNotificationList.Select("ID=" & gvEMPName.DataKeys(e.RowIndex).Values(0), "")(0).Delete()
            DTNotificationList.AcceptChanges()
        End If

        gvEMPName.DataSource = DTNotificationList
        gvEMPName.DataBind()
        showNoRecordsFound()
    End Sub

    Private Sub ddlDocType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDocType.SelectedIndexChanged
        CheckBSUs(ddlDocType.SelectedValue, False)
    End Sub



    Private Property DTNotificationList() As DataTable
        Get
            Return ViewState("DTNotificationList")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTNotificationList") = value
        End Set
    End Property
End Class

