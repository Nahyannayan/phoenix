﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class DocTracker_DM_CHANGEUSER
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_CLMConnectionString").ConnectionString

    Private Sub DocTracker_DM_CHANGEUSER_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "DM00014" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") Is Nothing Then
                ViewState("EntryId") = "0"
            Else
                ViewState("EntryId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                Dim encObj As New Encryption64
                SetDataMode("view")
                setModifyvalues(encObj.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            End If

        End If
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        Dim EditAllowed As Boolean
        EditAllowed = Not mDisable
    End Sub

    Private Sub setModifyvalues(ByVal p_Modifyid As String)
        Try
            h_EntryId.Value = p_Modifyid
            If p_Modifyid = 0 Then

            Else
                Dim str_Sql As String
                str_Sql = "select * FROM DocGroup where DGR_ID='" & p_Modifyid & "' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, str_Sql)
                If ds.Tables(0).Rows.Count > 0 Then
                    h_EntryId.Value = ds.Tables(0).Rows(0)("DGR_Id")
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub txtOldEmployee_TextChanged(sender As Object, e As EventArgs) Handles txtOldEmployee.TextChanged
        fillGridView_DOCDOCOwners(grdDocOwner, "GET_OLDROLE_EMP_DETAILS")
        grdDocOwner.DataSource = DTOLDEMPLIST
        grdDocOwner.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub
    Private Sub fillGridView_DOCDOCOwners(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        Dim PARAM(0) As SqlClient.SqlParameter
        PARAM(0) = New SqlParameter("@EMPID", hOLD_EMP_ID.Value)
        Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, fillSQL, PARAM)
        DTOLDEMPLIST = dsDetails.Tables(0)

        Dim mtable As New DataTable
        Dim dcID As New DataColumn("ID", GetType(Integer))
        dcID.AutoIncrement = True
        dcID.AutoIncrementSeed = 1
        dcID.AutoIncrementStep = 1
        mtable.Columns.Add(dcID)
        mtable.Merge(DTOLDEMPLIST)

        If mtable.Rows.Count = 0 Then
            mtable.Rows.Add(mtable.NewRow())
            mtable.Rows(0)(1) = -1
        End If
        DTOLDEMPLIST = mtable
        fillGrdView.DataSource = DTOLDEMPLIST
        fillGrdView.DataBind()
        showNoRecordsFoundOWNERUSERS()
    End Sub
    Private Sub showNoRecordsFoundOWNERUSERS()
        If DTOLDEMPLIST.Rows.Count > 0 Then
            If DTOLDEMPLIST.Rows(0)(1) = -1 Then
                Dim TotalColumns As Integer = grdDocOwner.Columns.Count - 2
                grdDocOwner.Rows(0).Cells.Clear()
                grdDocOwner.Rows(0).Cells.Add(New TableCell())
                grdDocOwner.Rows(0).Cells(0).ColumnSpan = TotalColumns
                grdDocOwner.Rows(0).Cells(0).Text = "No Record Found"
            End If
        End If

    End Sub
    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        If hNEW_EMP_ID.Value = "" Or txtNewEmployee.Text = "" Then
            'lblError.Text = "Assign Role should not be empty"
            usrMessageBar.ShowNotification("Assign Role should not be empty", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If hNEW_EMP_ID.Value = hOLD_EMP_ID.Value Or txtNewEmployee.Text = txtOldEmployee.Text Then
            'lblError.Text = "selected and Assign Role should not be same...."
            usrMessageBar.ShowNotification("selected and Assign Role should not be same....", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If



        Dim CParms(3) As SqlClient.SqlParameter

        CParms(0) = New SqlClient.SqlParameter("@OLDEMPID", SqlDbType.Int)
        CParms(0).Value = hOLD_EMP_ID.Value

        CParms(1) = New SqlClient.SqlParameter("@NEWEMPID", SqlDbType.Int)
        CParms(1).Value = hNEW_EMP_ID.Value

        CParms(2) = New SqlClient.SqlParameter("@User", SqlDbType.VarChar, 200)
        CParms(2).Value = Session("sUsr_name")

        CParms(3) = New SqlClient.SqlParameter("@Errormsg", SqlDbType.VarChar, 1000)
        CParms(3).Direction = ParameterDirection.Output

        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim RetVal As String = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "UpdateUserDetails", CParms)
            Dim ErrorMSG As String = CParms(3).Value.ToString()


            If ErrorMSG = "" Then
                stTrans.Commit()
                'lblError.Text = " User Changed Successfully !!!"
                usrMessageBar.ShowNotification("User Changed Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                'btnUpdate.Enabled = False
                btnUpdate.Visible = False

            Else
                lblError.Text = ErrorMSG
                stTrans.Rollback()
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Property DTOLDEMPLIST() As DataTable
        Get
            Return ViewState("DTOLDEMPLIST")
        End Get
        Set(ByVal value As DataTable)
            ViewState("DTOLDEMPLIST") = value
        End Set
    End Property
End Class
