<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupSelect_DM.aspx.vb" Inherits="DocTracker_PopupSelectDM"
    Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>

    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    <!-- Custom fonts for this template-->
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Page level plugin CSS-->
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet"/>
    <link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet"/>
    <link href="/PHOENIXBETA/cssfiles/jquery-ui.structure.min.css" rel="stylesheet"/>

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">
        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }
        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
            }
            else if (pId == 4) {
                document.getElementById("<%=getid("mnu_4_img") %>").src = path;
                }
                else if (pId == 5) {
                    document.getElementById("<%=getid("mnu_5_img") %>").src = path;
               }
               else if (pId == 6) {
                   document.getElementById("<%=getid("mnu_6_img") %>").src = path;
               }
               else if (pId == 7) {
                   document.getElementById("<%=getid("mnu_7_img") %>").src = path;
             }
             else if (pId == 8) {
                 document.getElementById("<%=getid("mnu_8_img") %>").src = path;
               }
               else if (pId == 9) {
                   document.getElementById("<%=getid("mnu_9_img") %>").src = path;
               }
               else if (pId == 10) {
                   document.getElementById("<%=getid("mnu_10_img") %>").src = path;
               }
    document.getElementById(ctrl1).value = val + '__' + path;
        }


        function SetValuetoParent(id) {
            //alert (id);
            parent.setProduct(id);
            return false;
        }

        function SetValuetoParent2(id) {
            //alert (id);
            parent.setProduct2(id);
            return false;
        }

        function SetValuetoParent3(id) {
            //alert (id);
            parent.setProduct3(id);
            return false;
        }
        function SetValuetoParent4(id) {
            //alert (id);
            parent.setProduct4(id);
            return false;
        }


    </script>
    <script>
        function listen_window() {

        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

    </script>
</head>
<body onload="listen_window();" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server">

        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="0px">
            <tr>
                <td>
                    <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                        AllowPaging="True" CssClass="table table-bordered table-row">
                        <RowStyle />
                        <Columns>
                            <asp:TemplateField HeaderText=" ">
                                <HeaderTemplate>

                                    <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                        value="Check All" />

                                </HeaderTemplate>
                                <ItemTemplate>

                                    <input id="chkControl" runat="server" type="checkbox" />

                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <HeaderTemplate>

                                    <asp:Label ID="lblHeaderCol1" runat="server" Text="Col1"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCol1" runat="server"></asp:TextBox>

                                    <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                        OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk1" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col1") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <HeaderTemplate>

                                    <asp:Label ID="lblHeaderCol2" runat="server" EnableViewState="True" Text="Col2"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCol2" runat="server"></asp:TextBox>

                                    <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                        OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk2" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col2") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <HeaderTemplate>

                                    <asp:Label ID="lblHeaderCol3" runat="server" EnableViewState="True" Text="Col3"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCol3" runat="server"></asp:TextBox>

                                    <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                        OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk3" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col3") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <HeaderTemplate>

                                    <asp:Label ID="lblHeaderCol4" runat="server" EnableViewState="True" Text="Col4"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCol4" runat="server"></asp:TextBox>

                                    <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                        OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk4" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col4") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk5" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col5") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                    
                                                <asp:Label ID="lblHeaderCol5" runat="server" EnableViewState="True" Text="Col5"></asp:Label>
                                            <br />
                                                            <asp:TextBox ID="txtCol5" runat="server"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk6" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col6") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                    
                                                <asp:Label ID="lblHeaderCol6" runat="server" EnableViewState="True" Text="Col6"></asp:Label>
                                           <br />
                                                            <asp:TextBox ID="txtCol6" runat="server" ></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col7" SortExpression="Col7">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk7" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col7") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                    
                                                <asp:Label ID="lblHeaderCol7" runat="server" EnableViewState="True" Text="Col7"></asp:Label>
                                            <br />
                                                            <asp:TextBox ID="txtCol7" runat="server"></asp:TextBox>
                                                        
                                                            <asp:ImageButton ID="btnSearch7" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col8" SortExpression="Col8">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk8" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col8") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                    
                                                <asp:Label ID="lblHeaderCol8" runat="server" EnableViewState="True" Text="Col8"></asp:Label>
                                           <br />
                                                            <asp:TextBox ID="txtCol8" runat="server"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearch8" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col9" SortExpression="Col9">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk9" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col9") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                    
                                                <asp:Label ID="lblHeaderCol9" runat="server" EnableViewState="True" Text="Col9"></asp:Label>
                                           <br />
                                                            <asp:TextBox ID="txtCol9" runat="server"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearch9" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Col10" SortExpression="Col10">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <HeaderTemplate>
                                   
                                                <asp:Label ID="lblHeaderCol10" runat="server" EnableViewState="True" Text="Col10"></asp:Label>
                                           <br />
                                                            <asp:TextBox ID="txtCol10" runat="server"></asp:TextBox>
                                                      
                                                            <asp:ImageButton ID="btnSearch10" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                OnClick="btnSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk10" runat="server" OnClick="LinkButton_Click" Text='<%# Bind("Col10") %>'></asp:LinkButton>&nbsp;
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="100%">
                        <tr>
                            <td width="50%" align="left">
                                <asp:CheckBox ID="chkSelAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                    Text="Select All" Width="76px" />
                            </td>
                            <td width="50%" align="left">
                                <asp:Button ID="btnSelect" runat="server" CssClass="button" CausesValidation="False"
                                    OnClick="btnSelect_Click" Visible="False" Text="Select"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

          <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_8" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_9" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_10" runat="server" type="hidden" value="=" />
        <input id="h_SelectedId" runat="server" type="hidden" value="0" />
        <asp:HiddenField ID="hdnSelIDs" runat="server" />
    </form>
</body>
</html>
