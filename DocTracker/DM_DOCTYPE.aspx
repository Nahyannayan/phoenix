<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DM_DOCTYPE.aspx.vb" Inherits="DOCTRACKER_DMDOCTYPE" Title="Add/Edit Document Type" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">    
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

   <script language="javascript" type="text/javascript">
       var isShift = false;
       function isNumeric(keyCode) {
           if (keyCode == 16) isShift = true;

           return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 39 || keyCode == 46 || keyCode == 37 ||
               (keyCode >= 96 && keyCode <= 105)) && isShift == false);
       }

       function keyUP(keyCode) {
           if (keyCode == 16)
               isShift = false;
       }


       function autoSizeWithCalendar(oWindow) {
           var iframe = oWindow.get_contentFrame();
           var body = iframe.contentWindow.document.body;

           var height = body.scrollHeight;
           var width = body.scrollWidth;

           var iframeBounds = $telerik.getBounds(iframe);
           var heightDelta = height - iframeBounds.height;
           var widthDelta = width - iframeBounds.width;

           if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
           if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
           oWindow.center();
       }


       function GetEMPNAME() {
           var sFeatures;
           sFeatures = "dialogWidth: 729px; ";
           sFeatures += "dialogHeight: 445px; ";
           sFeatures += "help: no; ";
           sFeatures += "resizable: no; ";
           sFeatures += "scroll: yes; ";
           sFeatures += "status: no; ";
           sFeatures += "unadorned: no; ";
           var NameandCode;
           var result;
           var SchoolorCorpt;
           SchoolorCorpt = 'OWNER'
           result = window.showModalDialog("../DocTracker/SelectDocEmpList_DM.aspx?ID=" + SchoolorCorpt + "", sFeatures)
           if (result != '' && result != undefined) {
               document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
            }
            else {
                return false;
            }

        }



        function GetSubBlockUserList() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var doctype;
            var docname;
            doctype = 'BLOCKUSER'


            if (docname != '') {
                result = window.showModalDialog("../DocTracker/SelectDocEmpList_DM.aspx?ID=" + doctype, sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=h_BLOCKID.ClientID %>').value = result; //NameandCode[0];
                }
                else {
                    return false;
                }
            }
        }

        function GetApproverList() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var doctype;
            var docname;
            doctype = 'APPROVERLIST'
            if (docname != '') {
                result = window.showModalDialog("../DocTracker/SelectDocEmpList_DM.aspx?ID=" + doctype, sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=h_APPROVERID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }
        }

        function GetApproverList() {
            var pMode;
            var NameandCode;
            pMode = "APPROVERLIST"
            document.getElementById('<%=hf_1.ClientID%>').value = DOMDESCR;
            document.getElementById('<%=hf_2.ClientID%>').value = DOMID;
            result = window.showModalDialog("../DocTracker/SelectDocEmpList_DM.aspx?ID=" + pMode, sFeatures)
            var oWnd = radopen(url, "pop_ApprList");
        }

        function OnClientCloseApprover(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[0];
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
            }
        }




       <%-- function getProduct(DOMDESCR, DOMID) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "APPROVERLIST"
            document.getElementById('<%=hf_DOMDESCRid.ClientID%>').value = DOMDESCR;
            document.getElementById('<%=hf_DOMID.ClientID%>').value = DOMID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode;
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
        }--%>



       function getProduct(DOMDESCR, DOMID) {
           var pMode;
           var NameandCode;
           pMode = "APPROVERLIST"
           document.getElementById('<%=hf_1.ClientID%>').value = DOMDESCR;
            document.getElementById('<%=hf_2.ClientID%>').value = DOMID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_product");
        }

        function OnClientCloseProduct(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[0];
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
            }
        }






        <%--function getFullDocUploader(DFUDESCR, DFUDOMID) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "UPLOADFULLDOCLIST"

            document.getElementById('<%=hf_DFUDESCRid.ClientID%>').value = DFUDESCR;
            document.getElementById('<%=hf_DFUDOMID.ClientID%>').value = DFUDOMID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode;

            
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
             //document.getElementById(DFUDESCR).value = NameandCode[1];
            //document.getElementById(DFUDOMID).value = NameandCode[0];
                        //document.getElementById(DFUDESCR).disabled = true;

        }--%>



       function getFullDocUploader(DFUDESCR, DFUDOMID) {
           var pMode;
           var NameandCode;
           pMode = "UPLOADFULLDOCLIST"
           document.getElementById('<%=hf_1.ClientID%>').value = DOMDESCR;
            document.getElementById('<%=hf_2.ClientID%>').value = DOMID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_fullversion");
        }

        function OnClientClosefullversion(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[0];
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
            }
        }



        <%--function GetSubDocList(DFUDESCR, DFUDOMID) {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var doctype;
            var NameandCode;
            sFeatures = "dialogWidth: 760px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            pMode = "SubDoc"
            doctype = document.getElementById("<%=txtDocType.ClientID %>").value

            document.getElementById('<%=hf_DFUDESCRid.ClientID%>').value = DFUDESCR;
            document.getElementById('<%=hf_DFUDOMID.ClientID%>').value = DFUDOMID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&Doctype=" + doctype;

            
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;

            
            //NameandCode = result.split('___');
            //document.getElementById(DFUDESCR).value = NameandCode[1];
            //document.getElementById(DFUDOMID).value = NameandCode[0];
            //document.getElementById(DFUDESCR).disabled = true;

        }--%>


       function GetSubDocList(DFUDESCR, DFUDOMID) {
           var pMode;
           var NameandCode;
           var doctype;
           pMode = "SubDoc"
           doctype = document.getElementById("<%=txtDocType.ClientID %>").value

            document.getElementById('<%=hf_1.ClientID%>').value = DFUDESCR;
            document.getElementById('<%=hf_2.ClientID%>').value = DFUDOMID;
            url = "../DocTracker/PopupSelect_DM.aspx?id=" + pMode + "&Doctype=" + doctype;
            var oWnd = radopen(url, "pop_subdoc");
        }

        function OnClientClosesubdoc(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).value = NameandCode[1];
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).value = NameandCode[0];
                document.getElementById(document.getElementById("<%=hf_1.ClientID%>").value).disabled = true;
                document.getElementById(document.getElementById("<%=hf_2.ClientID%>").value).disabled = true;
            }
        }




        function setProduct(result) {
            //alert(1);
            NameandCode = result.split('___');
            str = document.getElementById('<%=hf_DFUDESCRid.ClientID%>').value
            str2 = document.getElementById('<%=hf_DFUDOMID.ClientID%>').value
           <%-- str3 = document.getElementById('<%=hfDFUDOMID.ClientID%>').value
            str4 = document.getElementById('<%=hfOwnerDropid.ClientID%>').value--%>
            //document.getElementById(DOMDESCR).value = NameandCode[1];
            document.getElementById(document.getElementById('<%=hf_DFUDESCRid.ClientID%>').value).value = NameandCode[1];
            document.getElementById(document.getElementById('<%=hf_DFUDOMID.ClientID%>').value).value = NameandCode[0];


            document.getElementById(document.getElementById('<%=hf_DFUDESCRid.ClientID%>').value).disabled = true;

            //alert(NameandCode[1]);
            //document.getElementById(DOMID).value = NameandCode[0];
            //document.getElementById(DOMDESCR).disabled = true;
            CloseFrame();
            //return false;
        }

        function setProduct2(result) {
            //alert(1);
            NameandCode = result.split('___');
            str = document.getElementById('<%=hf_DOMDESCRid.ClientID%>').value
            str2 = document.getElementById('<%=hf_DOMID.ClientID%>').value
            //document.getElementById(DOMDESCR).value = NameandCode[1];
            document.getElementById(document.getElementById('<%=hf_DOMDESCRid.ClientID%>').value).value = NameandCode[1];
            document.getElementById(document.getElementById('<%=hf_DOMID.ClientID%>').value).value = NameandCode[0];
            document.getElementById(document.getElementById('<%=hf_DOMDESCRid.ClientID%>').value).disabled = true;
            //alert(NameandCode[1]);
            //document.getElementById(DOMID).value = NameandCode[0];
            //document.getElementById(DOMDESCR).disabled = true;
            CloseFrame();
            //return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_product" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseProduct"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

         <%--<Windows>
            <telerik:RadWindow ID="pop_fullversion" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClosefullversion"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_approver" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseApprover"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>--%>
        

        <Windows>
            <telerik:RadWindow ID="pop_subdoc" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClosesubdoc"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>


        
         
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Document Type Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom" id="lblErrorId" runat ="server" visible ="false" > 
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="title-bg-lite">
                                    <td align="left" colspan="4">Document Type Master</td>
                                </tr>--%>
                                <tr>
                                    <td align="left"><span class="field-label">Description</span></td>

                                    <td colspan="4" align="left">
                                        <asp:TextBox ID="txtDocType" runat="server" MaxLength="500" Width="800px"></asp:TextBox></td>
                                    
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">City \ Country</span></td>

                                    <td align="left" width="30%">
                                    <asp:DropDownList ID="ddlDocumentCity" runat="server"></asp:DropDownList>    
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Document Issuing Authority</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlDocumentIssuer" runat="server"></asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Document Group</span></td>

                                    <td align="left" width="30%">
                                    <asp:DropDownList ID="ddlDocumentGroup" runat="server" AutoPostBack="true"></asp:DropDownList>    
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Document Department</span></td>
                                    
                                    <td align="left" width="30%">
                                        
                                        <asp:DropDownList ID="ddlDepartment" Width="300px" runat="server" CssClass="listbox"></asp:DropDownList></td>
                                </tr>
                                <tr>

                                
                                <td  align="left" width="20%"><span class="field-label">Tracking / Non Tracking</span>

                                </td>
                                <td align="left" colspan="4">
                                    <asp:RadioButtonList ID="rbTracking" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="1">Tracking</asp:ListItem>
                                            <asp:ListItem Value="0">Non Tracking</asp:ListItem>
                                        </asp:RadioButtonList>
                                </td>
                                    <%--<td align="left"><span class="field-label"></span></td>

                                    <td align="left">
                                        </td>--%>
                                    
                                
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Document Life(Months)</span></td>

                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtLife" runat="server" Text="0" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);" onpaste="return false;" /></td>
                                   <%-- <td  align="left" width="20%"><span class="field-label">Document belongs to</span>

                                </td>--%>
                                   <%-- <td  align="left" width="20%">&nbsp;</td>
                                </tr>--%>
                               

                                <tr>
                                    <td align="left"><span class="field-label">SOP Path</span></td>

                                    <td colspan="3" align="left">
                                        <asp:TextBox ID="txtSOPPath" runat="server" TextMode="MultiLine">
                                
                                
                                        </asp:TextBox></td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Observation</span></td>

                                    <td colspan="3" align="left">
                                        <asp:TextBox ID="txtNotification" runat="server" TextMode="MultiLine"></asp:TextBox></td>


                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Applicable to Business Unit</span><br />
                                        <asp:CheckBox ID="chkBSUAll1" runat="server" Text='Select All' AutoPostBack="true" />
                                    </td>

                                    <td align="left" colspan="3">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plChkBUnit1" runat="server">
                                                <asp:Repeater ID="rptBSUNames1" runat="server">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="h_BSUid1" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bsu_id") %>' />
                                                        <asp:CheckBox ID="chkBSU1" Checked='<%# DataBinder.Eval(Container.DataItem, "bsu_check") %>' runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "bsu_name") %>' /><br />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="ShareBsuUnits" runat="server" visible="false">
                                    <td align="left"><span class="field-label">Share With Busness Unit</span><br />
                                        <asp:CheckBox ID="chkBSUAll2" runat="server" Text='Select All' AutoPostBack="true" />
                                    </td>

                                    <td align="left" colspan="3">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plChkBUnit2" runat="server">
                                                <asp:Repeater ID="rptBSUNames2" runat="server">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="h_BSUid2" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bsu_id") %>' />
                                                        <asp:CheckBox ID="chkBSU2" Checked='<%# DataBinder.Eval(Container.DataItem, "bsu_check") %>' runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "bsu_name") %>' /><br />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Edit/ Access Permits</span><br />
                                        <asp:CheckBox ID="chkRolAll" runat="server" Text='Select All' AutoPostBack="true" />
                                    </td>

                                    <td align="left" colspan="3">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plChkRole" runat="server">
                                                <asp:Repeater ID="rptRole" runat="server">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="h_ROLid" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "rol_id") %>' />
                                                        <asp:CheckBox ID="chkROL" Checked='<%# DataBinder.Eval(Container.DataItem, "rol_check") %>' runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "rol_descr") %>' /><br />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Content Type Allowed</span><br />
                                        <asp:CheckBox ID="chkConAll" runat="server" Text='Select All' AutoPostBack="true" />
                                    </td>

                                    <td align="left" colspan="3">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plChkConType" runat="server">
                                                <asp:Repeater ID="rptConType" runat="server">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="h_CONid" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "content_type_no") %>' />
                                                        <asp:CheckBox ID="chkCON" Checked='<%# DataBinder.Eval(Container.DataItem, "con_check") %>' runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "content_type_descr") %>' /><br />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>




                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Document Owner
                                    </td>
                                </tr>


                                <tr>

                                    <td class="matters" colspan="4">
                                        <asp:Panel ID="pnlEmployee" runat="server">


                                            <asp:GridView ID="grdDocOwner" runat="server" AutoGenerateColumns="False" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="10"
                                                CssClass="table table-bordered table-row" DataKeyNames="ID" ShowFooter="True">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="EMP ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDOL_ID" runat="server" Text='<%# Bind("DOL_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>




                                                    <asp:TemplateField HeaderText="Roles" ItemStyle-Width="80%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDOL_Descr" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="h_DOL_DOM_ID" runat="server" Value='<%# Bind("DOL_DOM_ID") %>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate>

                                                            <asp:TextBox ID="txtDOL_Descr" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                            <asp:HiddenField ID="h_DOL_DOM_ID" runat="server" Value='<%# Bind("DOL_DOM_ID") %>' />
                                                            <asp:ImageButton ID="imgDocOwner_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />

                                                        </EditItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtDOL_Descr"  Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_DOL_DOM_ID" runat="server" Value='<%# Bind("DOL_DOM_ID") %>' />
                                                            <asp:ImageButton ID="imgDocOwner" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </FooterTemplate>

                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateDOL" runat="server" CausesValidation="True" CommandName="Update"
                                                                Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelDOL" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddDOL" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditDOL" runat="server" CausesValidation="False" CommandName="Edit"
                                                                Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />


                                                </Columns>
                                                <HeaderStyle />
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                            </asp:GridView>
                                        </asp:Panel>

                                    </td>
                                </tr>

                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Dependency Document List
                                    </td>
                                </tr>





                                <tr>

                                    <td align="left" colspan="4">

                                        <asp:Panel ID="PnlSubDoc" runat="server">
                                            <asp:GridView ID="GridSubDocument" runat="server" AutoGenerateColumns="False"
                                                Width="100%" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="5"
                                                CssClass="table table-bordered table-row" DataKeyNames="DTS_ID" ShowFooter="True">


                                                <Columns>

                                                    <asp:TemplateField HeaderText="SUB Document Id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDTSNO" runat="server" Text='<%# Bind("DTS_ID") %>'></asp:Label>
                                                            <asp:Label ID="lblSubDOTID" runat="server" Text='<%# Bind("SUB_DOT_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Document Id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocNO" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Document Description" ItemStyle-Width="60%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocDescr" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtDocDescr" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                            <asp:HiddenField ID="h_DTS_SUB_DOT_ID" runat="server" Value='<%# Bind("SUB_DOT_ID") %>' />
                                                            <asp:ImageButton ID="imgSubDocumnet_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtDocDescr" Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_DTS_SUB_DOT_ID" runat="server" Value='<%# Bind("SUB_DOT_ID") %>' />
                                                            <asp:ImageButton ID="imgSubDocumnet" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />

                                                        </FooterTemplate>


                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Order">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPType" runat="server" Style="text-align: left" Text='<%# Bind("ORDNO") %>'></asp:Label>

                                                        </ItemTemplate>

                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlOrder" runat="server" ></asp:DropDownList>
                                                            <asp:HiddenField ID="hdnDisplayOrder" Value='<%# Bind("ORDNO") %>' runat="server" />
                                                        </EditItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlOrder" runat="server" ></asp:DropDownList>
                                                        </FooterTemplate>

                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateSD" runat="server" CausesValidation="True" CommandName="Update"
                                                                Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelSD" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddSD" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditSD" runat="server" CausesValidation="False" CommandName="Edit"
                                                                Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                                </Columns>
                                                <HeaderStyle />
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <%--Blocking Details--%>

                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4" valign="middle">Blocking User List
                                    </td>
                                </tr>



                                <tr>

                                    <td align="left" colspan="4">

                                        <asp:Panel ID="pnlDocBlockUsers" runat="server">
                                            <asp:GridView ID="grdBlockUserList" runat="server" AutoGenerateColumns="False"
                                                Width="100%" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="10"
                                                CssClass="table table-bordered table-row" DataKeyNames="ID" ShowFooter="True">





                                                <Columns>

                                                    <asp:TemplateField HeaderText="SUB Document Id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDBL_ID" runat="server" Text='<%# Bind("DBL_ID") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>




                                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="80%">

                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDFU_Descr" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                            <asp:HiddenField ID="h_DBL_DOM_ID" runat="server" Value='<%# Bind("DBL_DOM_ID") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtDBL_Descr" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                            <asp:HiddenField ID="h_DBL_DOM_ID" runat="server" Value='<%# Bind("DBL_DOM_ID") %>' />
                                                            <asp:ImageButton ID="imgBlocker_e" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtDBL_Descr" Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_DBL_DOM_ID" runat="server" Value='<%# Bind("DBL_DOM_ID") %>' />
                                                            <asp:ImageButton ID="imgBlocker" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />
                                                        </FooterTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateDBL" runat="server" CausesValidation="True" CommandName="Update"
                                                                Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelDBL" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddDBL" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditDBL" runat="server" CausesValidation="False" CommandName="Edit"
                                                                Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                                </Columns>
                                                <HeaderStyle />
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>





                                <%--Approver Details--%>


                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Approver\Pre Approver List &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkEitherPreApprover" runat="server" Text='Either of selected Pre Approver' />
                                    </td>
                                </tr>




                                <tr>

                                    <td align="left" colspan="4">


                                        <asp:Panel ID="AprPanel" runat="server">

                                            <asp:GridView ID="grdAppPreAppList" runat="server" AutoGenerateColumns="False" Width="100%"
                                                ShowFooter="true" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="15"
                                                CssClass="table table-bordered table-row" DataKeyNames="ID">
                                                <Columns>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDAL_ID" runat="server" Text='<%# Bind("DAL_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="60%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDescr" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                            <asp:ImageButton ID="imgApprover_e" Visible="false" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                TabIndex="14" />
                                                            <asp:HiddenField ID="h_DOM_ID_e" runat="server" Value='<%# Bind("DAL_DOM_ID") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtDescr" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                            <asp:ImageButton ID="imgApprover_e" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                TabIndex="14" />
                                                            <asp:HiddenField ID="h_DOM_ID_e" runat="server" Value='<%# Bind("DAL_DOM_ID") %>' />


                                                        </EditItemTemplate>
                                                        <FooterTemplate>

                                                            <asp:TextBox ID="txtDescr" Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_DOM_ID_e" runat="server" Value='<%# Bind("DAL_DOM_ID") %>' />
                                                            <asp:ImageButton ID="imgApprover" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />

                                                        </FooterTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Type" >
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPType" runat="server" Style="text-align: left" 
                                                                Text='<%# Bind("Approver_Type") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlApproverType" runat="server" ></asp:DropDownList>
                                                            <asp:HiddenField ID="hdnITM_TYPE" Value='<%# Bind("Approver_Type") %>' runat="server" />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlApproverType" runat="server" ></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Approver Order" >
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblALevel" runat="server" Style="text-align: left" Text='<%# Bind("DAL_LEVEL") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lblALevel" runat="server" Style="text-align: left" Text='<%# Bind("DAL_LEVEL") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblALevel" runat="server" Style="text-align: left" Text='<%# Bind("DAL_LEVEL") %>'></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateBO" runat="server" CausesValidation="True" CommandName="Update"
                                                                Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelBO" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddBo" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditBO" runat="server" CausesValidation="False" CommandName="Edit"
                                                                Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                            </asp:GridView>
                                        </asp:Panel>




                                    </td>
                                </tr>









                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Designations to Upload Full Version Document
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Full Version Upload?</span></td>

                                    <td align="left">
                                        <asp:RadioButtonList ID="rbFullVersion" runat="server" a RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="0">No</asp:ListItem>

                                        </asp:RadioButtonList></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>


                                    <td align="left" colspan="4">

                                        <asp:Panel ID="PnlUploadDocRoles" runat="server">


                                            <asp:GridView ID="GrdUploadFullDocRoles" runat="server" AutoGenerateColumns="False" Width="100%"
                                                ShowFooter="true" FooterStyle-HorizontalAlign="Left" CaptionAlign="Top" PageSize="15"
                                                CssClass="table table-bordered table-row" DataKeyNames="ID">
                                                <Columns>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDFU_ID" runat="server" Text='<%# Bind("DFU_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>




                                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="80%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtDFU_Descr" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>

                                                            <asp:HiddenField ID="h_DFU_DOM_ID" runat="server" Value='<%# Bind("DFU_DOM_ID") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtDFU_Descr" Enabled="false" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                            <asp:ImageButton ID="imgUploader_e" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                TabIndex="14" />
                                                            <asp:HiddenField ID="h_DFU_DOM_ID" runat="server" Value='<%# Bind("DFU_DOM_ID") %>' />


                                                        </EditItemTemplate>
                                                        <FooterTemplate>

                                                            <asp:TextBox ID="txtDFU_Descr" Enabled="false" runat="server"></asp:TextBox>
                                                            <asp:HiddenField ID="h_DFU_DOM_ID" runat="server" Value='<%# Bind("DFU_DOM_ID") %>' />



                                                            <asp:ImageButton ID="imgUploader" runat="server" ImageUrl="~/Images/forum_search.gif" TabIndex="14" />

                                                        </FooterTemplate>
                                                    </asp:TemplateField>





                                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False" ItemStyle-Width="10%">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkUpdateDFU" runat="server" CausesValidation="True" CommandName="Update"
                                                                Text="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancelDFU" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                Text="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkAddDFU" runat="server" CausesValidation="False" CommandName="AddNew"
                                                                Text="Add New"></asp:LinkButton>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditDFU" runat="server" CausesValidation="False" CommandName="Edit"
                                                                Text="Edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ShowHeader="True" ItemStyle-Width="10%" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                <RowStyle CssClass="griditem"></RowStyle>
                                            </asp:GridView>

                                        </asp:Panel>


                                    </td>
                                </tr>















                                <tr id="EmailLeaddays" runat="server" visible="false">
                                    <td align="left"><span class="field-label">Lead Days</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtLeadDays" runat="server" Text="0" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);" onpaste="return false;" /></td>
                                    <td align="left"><span class="field-label">Email Days</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtEmailDays" runat="server" Text="0" onkeyup="keyUP(event.keyCode)" onkeydown="return isNumeric(event.keyCode);" onpaste="return false;" /></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:HiddenField ID="h_ACTTYPE" runat="server" />
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_EMPID" runat="server" />
                                        <asp:HiddenField ID="h_DOCID" runat="server" />
                                        <asp:HiddenField ID="h_OLD_DOCID" runat="server" />
                                        <asp:HiddenField ID="h_BLOCKID" runat="server" />
                                        <asp:HiddenField ID="h_DOC_Blocked_deleted" runat="server" />
                                        <asp:HiddenField ID="h_Old_Block_id" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_APPROVERID" runat="server" />
                                        <asp:HiddenField ID="h_Doc_Deleted" runat="server" />
                                        <asp:HiddenField ID="h_Doc_Blocking_Deleted" runat="server" />
                                        <asp:HiddenField ID="h_FullDocUploadDeleted" runat="server" />
                                        <asp:HiddenField ID="h_Owner_Deleted" runat="server" />
                                        <asp:HiddenField ID="h_DELETED_APPROVER_ID" runat="server" />
                                        <asp:HiddenField ID="h_DOC_OWNER_DELETED" runat="server" />
                                        <asp:HiddenField ID="h_SubDocDeleted" runat="server" />

                                        <asp:HiddenField ID="hf_DOMDESCRid" runat="server" />
                                        <asp:HiddenField ID="hf_DOMID" runat="server" />
                                        <asp:HiddenField ID="hf_DFUDESCRid" runat="server" />
                                        <asp:HiddenField ID="hf_DFUDOMID" runat="server" />

                                        <asp:HiddenField ID="hf_1" runat="server" />
                                        <asp:HiddenField ID="hf_2" runat="server" />

                                        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>

