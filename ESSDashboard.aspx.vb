﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Partial Class ESSDashboard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Session("modPage") = "0"
            Dim userMod As String = Session("sModule")
            Dim bunit As String = Session("sBsuid")
            Dim userRol As String = Session("sroleid")
            Dim empid As String = Session("EmployeeId")
            Dim userSuper As Boolean
            userSuper = Session("sBusper")
            BIND_ESS_MENUS1(bunit, userRol, userMod, userSuper, empid)
            BIND_ESS_MENUS2(bunit, userRol, userMod, userSuper, empid)
            BIND_ESS_MENUS3(bunit, userRol, userMod, userSuper, empid)
            BIND_ESS_MENUS4(bunit, userRol, userMod, userSuper, empid)
            ViewState("EDD_EMP_ID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            'BIND_EMPDETAILS(ViewState("EDD_EMP_ID"))
        End If
    End Sub
    Private Sub BIND_ESS_MENUS1(ByVal bunit As String, ByVal userRol As String, ByVal userMod As String, ByVal userSuper As Integer, ByVal empid As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@bunit", bunit)
        param(1) = New SqlParameter("@userMod", userMod)
        param(2) = New SqlParameter("@userRol", userRol)
        param(3) = New SqlParameter("@userSuper", userSuper)
        param(4) = New SqlParameter("@order", 1)
        param(5) = New SqlParameter("@emp_id", empid)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "GET_ESS_MENUS", param)
            rptESS1.DataSource = datareader
            rptESS1.DataBind()
        End Using
    End Sub
    Private Sub BIND_ESS_MENUS2(ByVal bunit As String, ByVal userRol As String, ByVal userMod As String, ByVal userSuper As Integer, ByVal empid As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@bunit", bunit)
        param(1) = New SqlParameter("@userMod", userMod)
        param(2) = New SqlParameter("@userRol", userRol)
        param(3) = New SqlParameter("@userSuper", userSuper)
        param(4) = New SqlParameter("@order", 2)
        param(5) = New SqlParameter("@emp_id", empid)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "GET_ESS_MENUS", param)
            rptESS2.DataSource = datareader
            rptESS2.DataBind()
        End Using
    End Sub
    Private Sub BIND_ESS_MENUS3(ByVal bunit As String, ByVal userRol As String, ByVal userMod As String, ByVal userSuper As Integer, ByVal empid As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@bunit", bunit)
        param(1) = New SqlParameter("@userMod", userMod)
        param(2) = New SqlParameter("@userRol", userRol)
        param(3) = New SqlParameter("@userSuper", userSuper)
        param(4) = New SqlParameter("@order", 3)
        param(5) = New SqlParameter("@emp_id", empid)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "GET_ESS_MENUS", param)
            rptESS3.DataSource = datareader
            rptESS3.DataBind()
        End Using
    End Sub
    Private Sub BIND_ESS_MENUS4(ByVal bunit As String, ByVal userRol As String, ByVal userMod As String, ByVal userSuper As Integer, ByVal empid As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@bunit", bunit)
        param(1) = New SqlParameter("@userMod", userMod)
        param(2) = New SqlParameter("@userRol", userRol)
        param(3) = New SqlParameter("@userSuper", userSuper)
        param(4) = New SqlParameter("@order", 4)
        param(5) = New SqlParameter("@emp_id", empid)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "GET_ESS_MENUS", param)
            rptESS4.DataSource = datareader
            rptESS4.DataBind()
        End Using
    End Sub
    'Private Sub BIND_EMPDETAILS(ByVal empid As String)
    '    Dim conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim param(1) As SqlParameter
    '    param(0) = New SqlParameter("@EMPID", empid)

    '    Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "get_emp_details_ESS", param)
    '        While datareader.Read
    '            lblNo.Text = Convert.ToString(datareader("empno"))
    '            lblDOJ.Text = Convert.ToString(datareader("EMP_JOINDT"))
    '            lblDOJ.Text = Format(CDate(lblDOJ.Text), OASISConstants.DateFormat)
    '            lblBSu.Text = Convert.ToString(datareader("WORK_BSU_NAME"))
    '            lblDept.Text = Convert.ToString(datareader("CATEGORY_DESC"))
    '            lblDesig.Text = Convert.ToString(datareader("empvisa_descr"))
    '            lblmanager.Text = Convert.ToString(datareader("EMP_REP_TO_Name"))
    '        End While
    '    End Using
    'End Sub
    Protected Sub lbText1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblmenu As New HiddenField
            Dim lblcode As New HiddenField
            Dim lblMenuText As New LinkButton

            Dim url As String = String.Empty
            lblmenu = TryCast(sender.FindControl("hdnMnu"), HiddenField)

            lblcode = TryCast(sender.FindControl("hdnCode"), HiddenField)
            Dim mCode As String = encrData.Encrypt(lblcode.Value)
            Dim datamode As String = encrData.Encrypt("none")
            Session("temp") = lblMenuText.Text
            url = String.Format("{0}?MainMnu_code={1}&datamode={2}", lblmenu.Value, mCode, datamode)

            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lbText2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblmenu As New HiddenField
            Dim lblcode As New HiddenField
            Dim lblMenuText As New LinkButton

            Dim url As String = String.Empty
            lblmenu = TryCast(sender.FindControl("hdnMnu"), HiddenField)

            lblcode = TryCast(sender.FindControl("hdnCode"), HiddenField)
            Dim mCode As String = encrData.Encrypt(lblcode.Value)
            Dim datamode As String = encrData.Encrypt("none")
            Session("temp") = lblMenuText.Text
            url = String.Format("{0}?MainMnu_code={1}&datamode={2}", lblmenu.Value, mCode, datamode)

            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lbText3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblmenu As New HiddenField
            Dim lblcode As New HiddenField
            Dim lblMenuText As New LinkButton

            Dim url As String = String.Empty
            lblmenu = TryCast(sender.FindControl("hdnMnu"), HiddenField)

            lblcode = TryCast(sender.FindControl("hdnCode"), HiddenField)
            Dim mCode As String = encrData.Encrypt(lblcode.Value)
            Dim datamode As String = encrData.Encrypt("none")
            Session("temp") = lblMenuText.Text
            url = String.Format("{0}?MainMnu_code={1}&datamode={2}", lblmenu.Value, mCode, datamode)

            If lblcode.Value = "U000053" Then
                Dim sb As New System.Text.StringBuilder("")
                sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
                ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
            Else
                Response.Redirect(url)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lbText4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim encrData As New Encryption64
            Dim lblmenu As New HiddenField
            Dim lblcode As New HiddenField
            Dim lblMenuText As New LinkButton

            Dim url As String = String.Empty
            lblmenu = TryCast(sender.FindControl("hdnMnu"), HiddenField)

            lblcode = TryCast(sender.FindControl("hdnCode"), HiddenField)
            Dim mCode As String = encrData.Encrypt(lblcode.Value)
            Dim datamode As String = encrData.Encrypt("none")
            Session("temp") = lblMenuText.Text
            url = String.Format("{0}?MainMnu_code={1}&datamode={2}", lblmenu.Value, mCode, datamode)

            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub
End Class
