﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class Re_enroll_Reset
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'collect the url of the file to be redirected in view state
                If (ViewState("MainMnu_code") <> "S400018") Then


                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                Else

                    rbReenroll.Checked = True

                    btnAppr.Enabled = False
                    btnOverRide.Enabled = False
                    btnAppr.Visible = True
                    btnOverRide.Visible = False
                    callYEAR_DESCRBind()

                    getreenroll_count()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub

    Private Sub getreenroll_count()
        Dim param(5) As SqlParameter
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        param(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        param(1) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
        param(2) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)

        Using dr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "opl.GET_RE_ENROL_COUNT", param)
            If dr.HasRows = True Then
                While dr.Read
                    lblReenroll_Count.Text = "<div style='padding-top:10px'>Fee Pending <font color='black'>" & Convert.ToString(dr("FEE_PEN")) & _
                    "</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   GEMS Staff Reenrolled / Fee Override  <font color='black'>" & _
                    Convert.ToString(dr("GEMS_STAFF")) & "/" & Convert.ToString(dr("OVER_RIDE")) & "</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Not Reenrolling  <font color='black'>" & _
                     Convert.ToString(dr("NOT_ENROLL")) & "</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Pending <font color='black'>" & Convert.ToString(dr("PEND")) & "</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Reenrolled  <font color='black'>" & _
                    Convert.ToString(Convert.ToInt64(dr("EN")) + Convert.ToInt64(dr("GEMS_STAFF")) + Convert.ToInt64(dr("OVER_RIDE"))) & "</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  TC Students  <font color='black'>" & _
                    Convert.ToString(Convert.ToInt64(dr("TC"))) & "</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Reenrolled TC Students  <font color='black'>" & _
                                        Convert.ToString(Convert.ToInt64(dr("TC_ENROLL"))) & "</font></div>"

                End While
            Else
            End If

        End Using
    End Sub


    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Session("hashCheck") = Nothing
        callGrade_ACDBind()

    End Sub
    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Session("hashCheck") = Nothing
        callGrade_Section()
        getreenroll_count()
        gridbind()
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        Session("hashCheck") = Nothing
        gridbind()
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If

            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub gridbind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet
            Dim txtSearch As New TextBox
            Dim str_query As String = String.Empty
            Dim STU_NO As String = String.Empty
            Dim str_STU_NO As String = String.Empty

            Dim SNAME As String = String.Empty
            Dim str_SNAME As String = String.Empty

            Dim CONTACTNAME As String = String.Empty
            Dim str_CONTACTNAME As String = String.Empty


            Dim CONTACTMOBILE As String = String.Empty
            Dim str_CONTACTMOBILE As String = String.Empty

            Dim CONTACTMAIL As String = String.Empty
            Dim str_CONTACTMAIL As String = String.Empty

            Dim FLAG_STATUS As String = String.Empty
            Dim str_FLAG_STATUS As String = String.Empty
            Dim GRD_SCT As String = String.Empty
            Dim str_GRD_SCT As String = String.Empty
            Dim FILTER_COND As String = String.Empty
            Dim FILTER_ENROLL As String = String.Empty
            Dim PARAM(6) As SqlParameter


            If gvReEnrol.Rows.Count > 0 Then


                txtSearch = gvReEnrol.HeaderRow.FindControl("txtSTU_NO")

                If txtSearch.Text.Trim <> "" Then
                    STU_NO = " AND replace(STU_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NO = txtSearch.Text.Trim
                End If


                txtSearch = gvReEnrol.HeaderRow.FindControl("txtSNAME")

                If txtSearch.Text.Trim <> "" Then
                    SNAME = " AND SNAME like '%" & txtSearch.Text.Trim & "%'"
                    str_SNAME = txtSearch.Text.Trim
                End If


                txtSearch = gvReEnrol.HeaderRow.FindControl("txtCONTACTNAME")

                If txtSearch.Text.Trim <> "" Then
                    CONTACTNAME = " AND CONTACTNAME like '%" & txtSearch.Text.Trim & "%'"
                    str_CONTACTNAME = txtSearch.Text.Trim
                End If



                txtSearch = gvReEnrol.HeaderRow.FindControl("txtCONTACTMOBILE")

                If txtSearch.Text.Trim <> "" Then
                    CONTACTMOBILE = " AND CONTACTMOBILE like '%" & txtSearch.Text.Trim & "%'"
                    str_CONTACTMOBILE = txtSearch.Text.Trim
                End If


                txtSearch = gvReEnrol.HeaderRow.FindControl("txtFLAG_STATUS")

                If txtSearch.Text.Trim <> "" Then
                    FLAG_STATUS = " AND FLAG_STATUS like '%" & txtSearch.Text.Trim & "%'"
                    str_FLAG_STATUS = txtSearch.Text.Trim
                End If

                txtSearch = gvReEnrol.HeaderRow.FindControl("txtGRD_SCT")

                If txtSearch.Text.Trim <> "" Then
                    GRD_SCT = " AND GRD_SCT like '%" & txtSearch.Text.Trim & "%'"
                    str_GRD_SCT = txtSearch.Text.Trim
                End If


            End If
            If rbFullPaidNotReenrolled.Checked = True Then
                PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                PARAM(2) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
                PARAM(3) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)
                PARAM(4) = New SqlParameter("@STU_NO", str_STU_NO)
                PARAM(5) = New SqlParameter("@STU_NAME", str_SNAME)

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[GET_NextAcademicFullPaymentDoneStudents]", PARAM)
            ElseIf rbTCStudent.Checked = True Then
                FILTER_COND = STU_NO + SNAME + CONTACTNAME + CONTACTMOBILE + CONTACTMAIL + FLAG_STATUS + GRD_SCT
                PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                PARAM(1) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
                PARAM(2) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)
                PARAM(3) = New SqlParameter("@FILTER_COND", FILTER_COND)
                PARAM(4) = New SqlParameter("@INFO_TYPE", "TC")
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GET_RE_ENROL_RESET", PARAM)
            ElseIf rbRefund.Checked = True Then
                FILTER_COND = STU_NO + SNAME + CONTACTNAME + CONTACTMOBILE + CONTACTMAIL + FLAG_STATUS + GRD_SCT
                PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                PARAM(1) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
                PARAM(2) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)
                PARAM(3) = New SqlParameter("@FILTER_COND", FILTER_COND)
                PARAM(4) = New SqlParameter("@INFO_TYPE", "REF")
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GET_RE_ENROL_RESET", PARAM)
            ElseIf rbBlocked.Checked Then
                FILTER_COND = STU_NO + SNAME + CONTACTNAME + CONTACTMOBILE + CONTACTMAIL + FLAG_STATUS + GRD_SCT
                PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                PARAM(1) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
                PARAM(2) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)
                PARAM(3) = New SqlParameter("@FILTER_COND", FILTER_COND)
                PARAM(4) = New SqlParameter("@INFO_TYPE", "BLOCK")
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GET_RE_ENROL_RESET", PARAM)
            Else
                If rbReenroll.Checked = True Then
                    FILTER_ENROLL = " AND FILTER_ENROLL IN('EN','GEMS_STAFF','OVER_RIDE')"
                ElseIf rbFeePend.Checked = True Then

                    FILTER_ENROLL = " AND FILTER_ENROLL IN('FEE_PEN')"
                ElseIf rbGEMSStaff.Checked = True Then
                    FILTER_ENROLL = " AND FILTER_ENROLL IN('GEMS_STAFF','OVER_RIDE')"
                ElseIf rbNotReenroll.Checked = True Then
                    FILTER_ENROLL = " AND FILTER_ENROLL IN('NOT_ENROLL')"
                ElseIf rbPending.Checked = True Then
                    FILTER_ENROLL = " AND FILTER_ENROLL IN('PEND')"
                End If

                FILTER_COND = STU_NO + SNAME + CONTACTNAME + CONTACTMOBILE + CONTACTMAIL + FLAG_STATUS + GRD_SCT + FILTER_ENROLL

                PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                PARAM(1) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
                PARAM(2) = New SqlParameter("@SCT_ID", ddlSection.SelectedValue)
                PARAM(3) = New SqlParameter("@FILTER_COND", FILTER_COND)
                If rbNotReenroll.Checked = True Then
                    PARAM(4) = New SqlParameter("@INFO_TYPE", "NOT")
                Else
                    PARAM(4) = New SqlParameter("@INFO_TYPE", "OTH")
                End If

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GET_RE_ENROL_RESET", PARAM)
            End If



            If ds.Tables(0).Rows.Count > 0 Then
                gvReEnrol.DataSource = ds.Tables(0)
                gvReEnrol.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(6) = True
                ds.Tables(0).Rows(0)("bTC") = False
                gvReEnrol.DataSource = ds.Tables(0)
                Try
                    gvReEnrol.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvReEnrol.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvReEnrol.Rows(0).Cells.Clear()
                gvReEnrol.Rows(0).Cells.Add(New TableCell)
                gvReEnrol.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReEnrol.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReEnrol.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If



            txtSearch = gvReEnrol.HeaderRow.FindControl("txtSTU_NO")
            txtSearch.Text = str_STU_NO

            txtSearch = gvReEnrol.HeaderRow.FindControl("txtSNAME")
            txtSearch.Text = str_SNAME
            txtSearch = gvReEnrol.HeaderRow.FindControl("txtCONTACTNAME")
            txtSearch.Text = str_CONTACTNAME
            txtSearch = gvReEnrol.HeaderRow.FindControl("txtCONTACTMOBILE")
            txtSearch.Text = str_CONTACTMOBILE
            txtSearch = gvReEnrol.HeaderRow.FindControl("txtCONTACTMAIL")
            txtSearch.Text = str_CONTACTMAIL

            txtSearch = gvReEnrol.HeaderRow.FindControl("txtFLAG_STATUS")
            txtSearch.Text = str_FLAG_STATUS
            txtSearch = gvReEnrol.HeaderRow.FindControl("txtGRD_SCT")
            txtSearch.Text = str_GRD_SCT
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            gvReEnrol.Rows(0).Cells.Clear()
            gvReEnrol.Rows(0).Cells.Add(New TableCell)
            gvReEnrol.Rows(0).Cells(0).ColumnSpan = 10
            gvReEnrol.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvReEnrol.Rows(0).Cells(0).Text = "Sorry! Error Ocuured."
            gvReEnrol.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        End Try
    End Sub

    Protected Sub gvRef_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReEnrol.PageIndexChanged
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If

        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
            ' txtRemark = DirectCast((rowItem.Cells(0).FindControl("txtRemark")), TextBox)
            STU_ID = gvReEnrol.DataKeys(rowItem.RowIndex)("STU_ID").ToString()
            If hash.Contains(STU_ID) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next
    End Sub

    Protected Sub gvRef_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReEnrol.PageIndexChanging
        gvReEnrol.PageIndex = e.NewPageIndex
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If

        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
            txtRemark = DirectCast((rowItem.Cells(0).FindControl("txtRemark")), TextBox)

            STU_ID = gvReEnrol.DataKeys(rowItem.RowIndex)("STU_ID").ToString()
            If chk.Checked = True Then
                If hash.Contains(STU_ID) = False Then
                    hash.Add(STU_ID, txtRemark.Text)
                End If

            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If
        Next
        Session("hashCheck") = hash
        gridbind()
    End Sub




    Protected Sub btnSearchFLAG_STATUS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchCONTACTMOBILE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing

        gridbind()
    End Sub

    Protected Sub btnSearchCONTACTMAIL_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchCONTACTNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchGRD_SCT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("hashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub rbFeePend_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbFeePend.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()

        btnAppr.Enabled = True
        btnOverRide.Enabled = True
        btnAppr.Visible = True
        btnOverRide.Visible = True
        btnRegReEnroll.Visible = False


        btnTCNotEnroll_Rfd.Visible = False
        btnStaffChild_Rfd.Visible = False
        btnReenrolled_Rfd.Visible = False
        btnPending_Rfd.Visible = False
        btnRegister.Visible = False
        btnNotreenroll.Visible = False
    End Sub

    Protected Sub rbGEMSStaff_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbGEMSStaff.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()
        btnAppr.Enabled = True
        btnOverRide.Enabled = False
        btnAppr.Visible = True
        btnOverRide.Visible = True
        btnRegReEnroll.Visible = False

        btnTCNotEnroll_Rfd.Visible = False
        btnStaffChild_Rfd.Visible = False
        btnReenrolled_Rfd.Visible = False
        btnPending_Rfd.Visible = False
        btnRegister.Visible = False
        btnNotreenroll.Visible = False
    End Sub

    Protected Sub rbReenroll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbReenroll.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()
        btnAppr.Enabled = True
        btnOverRide.Enabled = False
        btnAppr.Visible = True
        btnOverRide.Visible = False
        btnRegReEnroll.Visible = False

        btnTCNotEnroll_Rfd.Visible = False
        btnStaffChild_Rfd.Visible = False
        btnReenrolled_Rfd.Visible = False
        btnPending_Rfd.Visible = False
        btnRegister.Visible = False
        btnNotreenroll.Visible = False
    End Sub

    Protected Sub rbNotReenroll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbNotReenroll.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()
        btnAppr.Enabled = True
        btnOverRide.Enabled = False
        btnAppr.Visible = True
        btnOverRide.Visible = True
        btnRegReEnroll.Visible = False

        btnTCNotEnroll_Rfd.Visible = False
        btnStaffChild_Rfd.Visible = False
        btnReenrolled_Rfd.Visible = False
        btnPending_Rfd.Visible = False
        btnRegister.Visible = False
        btnNotreenroll.Visible = False
    End Sub


    Protected Sub rbPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPending.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()
        btnAppr.Enabled = False
        btnOverRide.Enabled = False
        btnAppr.Visible = True
        btnOverRide.Visible = True
        btnRegReEnroll.Visible = False

        btnTCNotEnroll_Rfd.Visible = False
        btnStaffChild_Rfd.Visible = False
        btnReenrolled_Rfd.Visible = False
        btnPending_Rfd.Visible = False
        btnRegister.Visible = True
        btnNotreenroll.Visible = True
    End Sub
    Protected Sub rbFullPaidNotReenrolled_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbFullPaidNotReenrolled.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()
        btnAppr.Enabled = True
        btnOverRide.Enabled = True
        btnAppr.Visible = False
        btnOverRide.Visible = False
        btnRegReEnroll.Visible = True

        btnTCNotEnroll_Rfd.Visible = False
        btnStaffChild_Rfd.Visible = False
        btnReenrolled_Rfd.Visible = False
        btnPending_Rfd.Visible = False
        btnRegister.Visible = False
        btnNotreenroll.Visible = False
    End Sub
    Protected Sub rbTCStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTCStudent.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()
        btnAppr.Enabled = False
        btnOverRide.Enabled = False
        btnAppr.Visible = False
        btnOverRide.Visible = False
        btnRegReEnroll.Visible = False

        btnTCNotEnroll_Rfd.Visible = True
        btnStaffChild_Rfd.Visible = False
        btnReenrolled_Rfd.Visible = False
        btnPending_Rfd.Visible = False
        btnRegister.Visible = False
        btnNotreenroll.Visible = False
    End Sub
    Protected Sub rbRefund_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRefund.CheckedChanged
        Session("hashCheck") = Nothing
        gridbind()
        btnAppr.Enabled = False
        btnOverRide.Enabled = False
        btnAppr.Visible = False
        btnOverRide.Visible = False
        btnRegReEnroll.Visible = False
        btnTCNotEnroll_Rfd.Visible = True
        btnStaffChild_Rfd.Visible = True
        btnReenrolled_Rfd.Visible = True
        btnPending_Rfd.Visible = True
        btnRegister.Visible = False
        btnNotreenroll.Visible = False
    End Sub
    Protected Sub btnRegReEnroll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegReEnroll.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Register & Re-Enroll"
            Exit Sub
        End If


        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        str_err = calltransaction(errorMessage, "ADVANCE")
        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub

    Protected Sub btnAppr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppr.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If


        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

      
        str_err = calltransaction(errorMessage, "RESET")





        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub
    Protected Sub btnOverRide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOverRide.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If


        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        str_err = calltransaction(errorMessage, "OVERRIDE")
        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub
    Protected Sub btnTCNotEnroll_Rfd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTCNotEnroll_Rfd.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        If rbRefund.Checked = True Then
            str_err = calltransaction(errorMessage, "REFUND", "TC")
        End If
        If rbTCStudent.Checked = True Then
            str_err = calltransaction(errorMessage, "TC", "NOT REENROLL")
        End If
        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If

    End Sub

    Protected Sub btnPending_Rfd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPending_Rfd.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        If rbRefund.Checked = True Then
            str_err = calltransaction(errorMessage, "REFUND", "PEND")
        End If
      
        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub

    Protected Sub btnReenrolled_Rfd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReenrolled_Rfd.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        If rbRefund.Checked = True Then
            str_err = calltransaction(errorMessage, "REFUND", "RE-ENROLL")
        End If
      
        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub

    Protected Sub btnStaffChild_Rfd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStaffChild_Rfd.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        If rbRefund.Checked = True Then
            str_err = calltransaction(errorMessage, "REFUND", "STAFF")
        End If
      
        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")

            gridbind()
        Else

            lblError.Text = errorMessage
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String, ByVal FLAG As String, Optional ByVal ACTION As String = "NOTHING") As Integer
        Dim ACD_ID As String = ddlAcademicYear.SelectedValue
        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty
        Dim STU_IDS As New StringBuilder
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            STU_ID = DirectCast(rowItem.FindControl("lblSTU_ID"), Label).Text

            If chk.Checked = True Then
                'If hash.Contains(RF_ID) = False Then
                '    hash.Add(RF_ID, DirectCast(rowItem.FindControl("lblRF_ID"), Label).Text)
                'End If
                txtRemark = DirectCast((rowItem.Cells(0).FindControl("txtRemark")), TextBox)

                hash(STU_ID) = txtRemark.Text
            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If

        Next
        Session("hashCheck") = hash
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry
            For Each hashloop In hash
                STU_IDS.Append(hashloop.Key.ToString + "^" + hashloop.Value.ToString)
                STU_IDS.Append("|")

            Next
        End If

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_id"))
                pParms(2) = New SqlClient.SqlParameter("@STU_IDS", STU_IDS.ToString)
                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                pParms(4) = New SqlClient.SqlParameter("@FLAG", FLAG)
                pParms(5) = New SqlClient.SqlParameter("@ACTION", ACTION)
                pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(6).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.SAVERE_ENROL_RESET", pParms)
                status = pParms(6).Value

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If

                Session("hashCheck") = Nothing
                calltransaction = "0"
                gridbind()
                getreenroll_count()
            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

    Protected Sub gvReEnrol_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReEnrol.RowCommand
        Try
            '    Dim url As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvReEnrol.Rows(index), GridViewRow)
            Dim lblstuid As Label = selectedRow.FindControl("lblSTU_ID")
            ViewState("stu_id") = lblstuid.Text
            If e.CommandName = "block" Then
                bindReason()
                Panel_MSG.Visible = True


            End If
            If e.CommandName = "unblock" Then
                display_items(lblstuid.Text)
                'update_block()

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub update_block()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = ""

        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(str_conn)
        Try
            objConn.Open()
            cmd = New SqlCommand("OPL.SAVEREENROLL_BLOCK", objConn)


            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RBM_RBR_ID", 0)
            cmd.Parameters.AddWithValue("@RBM_STU_ID", ViewState("stu_id"))
            cmd.Parameters.AddWithValue("@RBM_ACD_ID", ddlAcademicYear.SelectedValue)
            cmd.Parameters.AddWithValue("@RBM_UNBLOCKUSR_ID", Session("EmployeeId"))


            cmd.ExecuteNonQuery()


            lblUerror.Text = "Updated Successfully"

            gridbind()

        Catch ex As Exception
            lblUerror.Text = "Request could not be processed"
        Finally
            cmd.Dispose()
            objConn.Close()
        End Try
    End Sub

    Sub bindReason()
        ddlReason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT RBR_ID,RBR_REASON FROM OPL.REENROLL_BLOCK_REASON_M "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReason.DataSource = ds
        ddlReason.DataTextField = "RBR_REASON"
        ddlReason.DataValueField = "RBR_ID"
        ddlReason.DataBind()
    End Sub

    Protected Sub btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn.Click
        Panel_MSG.Visible = False
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Panel_MSG.Visible = False
    End Sub

    Protected Sub btnReason_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReason.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim grade As String()
        Dim str_query As String = ""

        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(str_conn)
        Try
            objConn.Open()
            cmd = New SqlCommand("OPL.SAVEREENROLL_BLOCK", objConn)


            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RBM_RBR_ID", ddlReason.SelectedValue)
            cmd.Parameters.AddWithValue("@RBM_STU_ID", ViewState("stu_id"))
            cmd.Parameters.AddWithValue("@RBM_ACD_ID", ddlAcademicYear.SelectedValue)
            cmd.Parameters.AddWithValue("@RBM_BLOCKUSR_ID", Session("EmployeeId"))


            cmd.ExecuteNonQuery()


            lblError1.Text = "Record Saved Successfully"

            gridbind()
           
        Catch ex As Exception
            lblError1.Text = "Request could not be processed"
        Finally
            cmd.Dispose()
            objConn.Close()
        End Try
    End Sub

    Protected Sub rbBlocked_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBlocked.CheckedChanged
        btnRegister.Visible = False
        gridbind()
    End Sub

    Protected Sub gvReEnrol_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReEnrol.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If rbBlocked.Checked Then
                Dim ib As LinkButton = e.Row.Cells(8).Controls(0)
                ib.Text = "UnBlock"
                ib.CommandName = "unblock"
            End If
        End If
    End Sub
    Sub display_items(ByVal stuid As Integer)
        divUnblok.Visible = True
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT RBR_REASON,REPLACE(CONVERT(VARCHAR(11),RBM_BLOCKDATE,113),' ','/')RBM_BLOCKDATE," _
                                  & " (ISNULL(EMP_FNAME,'')+ISNULL(EMP_MNAME,'')+ISNULL(EMP_LNAME,''))EMP_NAME  FROM OPL.REENROLL_BLOCK_M " _
                                  & " INNER JOIN OPL.REENROLL_BLOCK_REASON_M  ON RBM_RBR_ID=RBR_ID " _
                                  & " INNER JOIN EMPLOYEE_M ON RBM_BLOCKUSR_ID=EMP_ID " _
                                  & " WHERE RBM_STU_ID =" & stuid & " And RBM_ACD_ID =" & ddlAcademicYear.SelectedValue

        If str_query <> "" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count >= 1 Then
                lblReasons.Text = ds.Tables(0).Rows(0).Item("RBR_REASON")
                lblBdate.Text = ds.Tables(0).Rows(0).Item("RBM_BLOCKDATE")
                lblBUser.Text = ds.Tables(0).Rows(0).Item("EMP_NAME")
            End If
        End If


    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divUnblok.Visible = False
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divUnblok.Visible = False
    End Sub

    Protected Sub btnUnBlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnBlock.Click
        update_block()
    End Sub

    Sub update_reenrol()

        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty
      
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            STU_ID = DirectCast(rowItem.FindControl("lblSTU_ID"), Label).Text
            txtRemark = DirectCast((rowItem.Cells(0).FindControl("txtRemark")), TextBox)
            If chk.Checked = True Then


                Dim Status As Integer
                Dim param(14) As SqlClient.SqlParameter
                Dim transaction As SqlTransaction
                Dim BSU_ID As String = IIf(Session("sBsuid") Is Nothing, "", Session("sBsuid"))

                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        param(0) = New SqlParameter("@STU_ID", STU_ID)
                        param(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                        param(2) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                        param(3) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_name"))
                        param(4) = New SqlClient.SqlParameter("@REMARKS", txtRemark.Text)
                        param(5) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                        param(5).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.UPDATEREENROLL_REG_BYREGISTRAR", param)
                        Status = param(5).Value





                    Catch ex As Exception

                        lblError.Text = "Record could not be updated"
                    Finally
                        If Status <> 0 Then
                            UtilityObj.Errorlog(lblError.Text)
                            transaction.Rollback()
                        Else
                            lblError.Text = ""
                            transaction.Commit()
                        End If
                    End Try

                End Using
            End If
        Next
    End Sub

    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Reset"
            Exit Sub
        End If

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        If rbPending.Checked = True Then
            update_reenrol()
            gridbind()
        End If

        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")


        Else

            lblError.Text = errorMessage
        End If
    End Sub

    Protected Sub btnNotreenroll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNotreenroll.Click
        Dim chk As CheckBox
        Dim bCHKED As Boolean
        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            If chk.Checked = True Then
                bCHKED = True
            End If
        Next
        If bCHKED = False Then

            lblError.Text = "No record selected for Not Re enroll"
            Exit Sub
        End If

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        If rbPending.Checked = True Then
            update_notreenrol()
            gridbind()
        End If

        If str_err = "0" Then
            lblError.Text = "Record Updated Successfully"
            Session.Remove("hashCheck")


        Else

            lblError.Text = errorMessage
        End If
    End Sub
    Sub update_notreenrol()

        Dim chk As CheckBox
        Dim txtRemark As TextBox
        Dim STU_ID As String = String.Empty

        For Each rowItem As GridViewRow In gvReEnrol.Rows
            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)
            STU_ID = DirectCast(rowItem.FindControl("lblSTU_ID"), Label).Text
            txtRemark = DirectCast((rowItem.Cells(0).FindControl("txtRemark")), TextBox)
            If chk.Checked = True Then


                Dim Status As Integer
                Dim param(14) As SqlClient.SqlParameter
                Dim transaction As SqlTransaction
                Dim BSU_ID As String = IIf(Session("sBsuid") Is Nothing, "", Session("sBsuid"))

                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        param(0) = New SqlParameter("@STU_ID", STU_ID)
                        param(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
                        param(2) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                        param(3) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_name"))
                        param(4) = New SqlClient.SqlParameter("@REMARKS", txtRemark.Text)
                        param(5) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                        param(5).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.UPDATEREENROLL_NOT", param)
                        Status = param(5).Value





                    Catch ex As Exception

                        lblError.Text = "Record could not be updated"
                    Finally
                        If Status <> 0 Then
                            UtilityObj.Errorlog(lblError.Text)
                            transaction.Rollback()
                        Else
                            lblError.Text = ""
                            transaction.Commit()
                        End If
                    End Try

                End Using
            End If
        Next
    End Sub
End Class


