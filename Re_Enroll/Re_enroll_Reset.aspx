﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Re_enroll_Reset.aspx.vb" Inherits="Re_enroll_Reset" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        input {
            vertical-align :middle !important;
        }
    </style>

     <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>


    <script language="javascript" type="text/javascript">

        function confirm(var1) {
            var msg;

            if (var1 == 1) {
                msg = "You are about to move the selected student(s) to the TC not re-enrolling list.Do you want to proceed?"
            }
            else if (var1 == 2) {
                msg = "You are about to move the selected student(s) to the re-enrolling list.Do you want to proceed?"

            }
            else if (var1 == 3) {
                msg = "You are about to move the selected student(s) to the re-enrolling list.Do you want to proceed?"

            }
            else if (var1 == 4) {
                msg = "You are about to move the selected student(s) to the pending list.Do you want to proceed?"

            }

            if (confirm(msg) == true)
                return true;
            else
                return false;

        }

        function confirm_Reset() {

            if (confirm("You are about to reset the reenrollment of  the selected student(s).Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function confirm_OverRide() {

            if (confirm("Do you want to proceed with overriding the fee payment pending for the reenrollment?") == true)
                return true;
            else
                return false;

        }
        function confirm_Register() {

            if (confirm("You are about to Register & Re-Enroll the selected student(s).Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function confirm_notreenrol() {

            if (confirm("You are about to Not Re-Enroll the selected student(s).Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkList") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }
    </script>
    <style>
        table td span.field-value{
            width:100% !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Re-Enrollment Details
</div>
 <div class="card-body">
            <div class="table-responsive m-auto">

   <%-- <table id="Table1" border="0" width="100%">
        <tr >
            <td align="left" class="title" >
                
            </td>
        </tr>
    </table>--%>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        width="100%" cellspacing="0">
        <tr>
            <td align="left" >
                <div align="left">
                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                        ></asp:Label></div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table align="center" width="100%" cellpadding="2" cellspacing="0"
                    >
                    <tr class="title-bg">
                        <td align="center" colspan="4" >
                            <div align="left">
                             <span class="field-label">Search Filter</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"  >
                           <span class="field-label" >Academic year</span>
                        </td>
                       
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label" >Grade</span>
                        </td>
                        
                        <td align="left"  style="text-align: left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" >
                           <span class="field-label" >Section</span>
                        </td>
                       
                        <td align="left"  style="text-align: left" width="30%">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" >
                          <span class="field-label" >Reenroll Status</span>
                        </td>
                       
                        <td align="left"  style="text-align: left" colspan="3" >
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="rbFeePend" runat="server" Text="Fee Pending" AutoPostBack="true"
                                GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                    <td>
                                         <asp:RadioButton ID="rbGEMSStaff" runat="server" Text="GEMS Staff Reenrolled / Fee Override"
                                AutoPostBack="true" GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                    <td>
                                          <asp:RadioButton ID="rbNotReenroll" runat="server" Text="Not Reenrolling" AutoPostBack="true"
                                GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rbPending" runat="server" Text="Pending" AutoPostBack="true"
                                GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                </tr>
                               
                              
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="rbReenroll" runat="server" Text="Reenrolled" AutoPostBack="true"
                                GroupName="reenroll" CssClass="field-label"  />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rbFullPaidNotReenrolled" runat="server" Text="Full Fee Paid (Not Reenrolled)"
                                AutoPostBack="true" GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rbTCStudent" runat="server" Text="TC Students" AutoPostBack="true"
                                GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                     <td>
                                         <asp:RadioButton ID="rbRefund" runat="server" Text="Refund" AutoPostBack="true" GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                         <asp:RadioButton ID="rbBlocked" runat="server" Text="Blocked" AutoPostBack="true" GroupName="reenroll" CssClass="field-label" />
                                    </td>
                                </tr>
                            </table>                                                                                                                                                                                                                                                                                
                        </td>                      
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblReenroll_Count" runat="server" CssClass="field-value"></asp:Label>
            </td>
        </tr>
      
        <tr>
            <td  align="center">
                <asp:GridView ID="gvReEnrol" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    PageSize="20" DataKeyNames="STU_ID" AllowPaging="True">
                    <RowStyle CssClass="griditem"  />
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server">
                                </asp:CheckBox>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkList" runat="server"></asp:CheckBox>
                                <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student No">
                            <HeaderTemplate>
                                 Student No<br />
                                                <asp:TextBox ID="txtSTU_NO" runat="server" ></asp:TextBox>
                                                <asp:ImageButton ID="btnSearchSTU_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                    ImageAlign="Top" OnClick="btnSearchSTU_NO_Click"></asp:ImageButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                <asp:Label
                                    ID="lblTC" runat="server" Text="*" Font-Bold="true" Font-Size="12px" ForeColor="Red"
                                    Visible='<%# bind("bTC") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <HeaderTemplate>
                                Student Name<br />
                                                <asp:TextBox ID="txtSNAME" runat="server" ></asp:TextBox>
                                                 <asp:ImageButton ID="btnSearchSNAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                    ImageAlign="Top" OnClick="btnSearchSNAME_Click"></asp:ImageButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GRD_SCT">
                            <HeaderTemplate>
                                Grade & Section<br />
                                <asp:TextBox ID="txtGRD_SCT" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="btnSearchGRD_SCT" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    ImageAlign="Top" OnClick="btnSearchGRD_SCT_Click"></asp:ImageButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGRD_SCT" runat="server" Text='<%# Bind("GRD_SCT") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Primary Contact">
                            <HeaderTemplate>
                                Primary Contact<br />
                                <asp:TextBox ID="txtCONTACTNAME" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="btnSearchCONTACTNAME" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearchCONTACTNAME_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCONTACTNAME" runat="server" Text='<%# Bind("CONTACTNAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <HeaderTemplate>
                                Email<br />
                                <asp:TextBox ID="txtCONTACTMAIL" runat="server" ></asp:TextBox>
                                <asp:ImageButton ID="btnSearchCONTACTMAIL" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearchCONTACTMAIL_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCONTACTMAIL" runat="server" Text='<%# bind("CONTACTMAIL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mobile No.">
                            <HeaderTemplate>
                                Mobile No<br />
                                <asp:TextBox ID="txtCONTACTMOBILE" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="btnSearchCONTACTMOBILE" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearchCONTACTMOBILE_Click" />                               
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCONTACTMOBILE" runat="server" Text='<%# bind("CONTACTMOBILE") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle  />
                            <ItemStyle  HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <HeaderTemplate>
                                Status<br />
                                <asp:TextBox ID="txtFLAG_STATUS" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="btnSearchFLAG_STATUS" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="btnSearchFLAG_STATUS_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFLAG_STATUS" runat="server" Text='<%# bind("FLAG_STATUS") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle  />
                            <ItemStyle  HorizontalAlign="LEFT" />
                        </asp:TemplateField>
                        <asp:ButtonField CommandName="block" Text="Block" HeaderText="Blocked">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" >
                                        </ItemStyle>
                                    </asp:ButtonField>                                                  
                        <asp:TemplateField HeaderText="Remark">
                            <ItemTemplate>
                                <asp:TextBox ID="txtRemark" runat="server" Rows="2" CssClass="Textboxmulti" Text='<%# Bind("REMARKS") %>'
                                    TextMode="MultiLine"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbeRemark" runat="server" TargetControlID="txtRemark"
                                    FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars=". ">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle  />
                    <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
        
        <tr>
            <td  valign="middle" align="center">
                <asp:Button ID="btnAppr" runat="server" CssClass="button" Text="Reset" OnClientClick="return confirm_Reset();"
                     />
                <asp:Button ID="btnOverRide" runat="server" CssClass="button" Text="Fee Override"
                    OnClientClick="return confirm_OverRide();"  />
                <asp:Button ID="btnRegReEnroll" runat="server" CssClass="button" Text="Register & Re-Enroll"
                    OnClientClick="return confirm_Register();"  Visible="False" /><asp:Button
                        ID="btnTCNotEnroll_Rfd" runat="server" CssClass="button" Text="TC Not Re-Enrolling"
                        OnClientClick="return confirm(1);"  Visible="false" />
                <asp:Button ID="btnStaffChild_Rfd" runat="server" CssClass="button" Text="Staff Child Re-Enrolled"
                    OnClientClick="return confirm(2);" Visible="false"  />
                <asp:Button ID="btnReenrolled_Rfd" runat="server" CssClass="button" Text="Re-Enrolled"
                    OnClientClick="return confirm(3);" Visible="false"  /><asp:Button ID="btnPending_Rfd"
                        runat="server" CssClass="button" Text="Pending" OnClientClick="return confirm(4);"
                        Visible="false"  />
                         <asp:Button ID="btnRegister" runat="server" CssClass="button" Text="Register" OnClientClick="return confirm_Register();"
                    Visible="false"  />
                     <asp:Button ID="btnNotreenroll" runat="server" CssClass="button" Text="Not Re-Enrolling" OnClientClick="return confirm_notreenrol();"
                    Visible="false"  />
                        
            </td>
        </tr>
     
    </table>
                <div id="Panel_MSG" runat="server" class="darkPanlAlumini" visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <asp:Button ID="btn" type="button" runat="server"
                            Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X"></asp:Button>
                        <div class="holderInner">
                            <div align="CENTER">
                                <asp:Label ID="lblError1" runat="server" CssClass="error" EnableViewState="False"
                                    ></asp:Label>
                            </div>
                           
                            <table align="center" cellpadding="2" cellspacing="0"
                                style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td align="left"><span class="field-label">Reason</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="true" ></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnReason" Text="Save" runat="server" CssClass="button" />
                                        <asp:Button ID="btnClose" Text="Close" runat="server" CssClass="button" />
                                    </td>
                                </tr>

                            </table>
                        </div>

                    </div>
                </div>
                
       <div id="divUnblok" runat="server" class="darkPanlAlumini" visible="false">
           <div class="panel-cover inner_darkPanlAlumini">
               <asp:Button ID="btClose" type="button" runat="server"
                   Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                   ForeColor="White" Text="X"></asp:Button>
               <div class="holderInner">
                   <div align="CENTER">
                       <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"
                           ></asp:Label>
                   </div>
                  
                   <table align="center" cellpadding="2" cellspacing="0"
                       style="width: 100%; border-collapse: collapse;">
                       <tr>
                           <td align="left"><span class="field-label">Reason</span>
                           </td>

                           <td align="left">
                               <asp:Label ID="lblReasons" runat="server" ></asp:Label>
                           </td>
                       </tr>
                       <tr>
                           <td align="left"><span class="field-label">Blocked Date</span>
                           </td>

                           <td align="left">
                               <asp:Label ID="lblBdate" runat="server" ></asp:Label>
                           </td>
                       </tr>
                       <tr>
                           <td align="left"><span class="field-label">Blocked User</span>
                           </td>

                           <td align="left">
                               <asp:Label ID="lblBUser" runat="server" ></asp:Label>
                           </td>
                       </tr>
                       <tr>
                           <td align="center" colspan="2">
                               <asp:Button ID="btnUnBlock" Text="UnBlock" runat="server" CssClass="button" />
                               <asp:Button ID="btnUClose" Text="Close" runat="server" CssClass="button" />
                           </td>
                       </tr>

                   </table>
               </div>

           </div>
                </div>          
                
             </div></div>   
</asp:Content>
