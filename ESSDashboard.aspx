﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ESSDashboard.aspx.vb" Inherits="ESSDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

   <style type="text/css">
       /* Module class css goes here */

/* GRID */

.grid { 
    max-width: 1600px; 
    width: 90%; 
    margin: 0 auto; 
}

.four { 
	width: 32.26%; 
}

/* COLUMNS */

.col {
  display: block;
  float:left;
  margin: 1% 0 1% 1.6%;
}

.col:first-of-type { margin-left: 0; }

/* CLEARFIX */

.cf:before,
.cf:after {
    content: " ";
    display: table; 
}

.cf:after { clear: both; }
.cf { *zoom: 1; }

/* GENERAL STYLES FOR BOX AND OVERLAY */

.box {
  display: block;
  width: 100%;
  height: 150px;
  border-radius:10px;
  overflow: hidden;
  background-color: /*#b7cce8 rgba(238, 109, 139, 0.3)*/ #fff;
  text-align: center;
  position: relative;
  line-height: 20px;
  padding:32px 4px 0 4px;
  font-weight:600;
  /*box-shadow: 4px 4px 6px #999;*/
}
.overlay a {
        color: #ffffff !important;
    }
.overlay{
  width: 100%;
  height:100%;
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
}

/* SLIDE IN */

.ess-over .overlay{
  background-color: #8dc24c /*rgba(113,41,108, 0.47)rgba(61,203,232,0.47) #3dcbe8*/ /*rgba(10,35,62, 0.47) rgba(106,221,170,0.47) #6addaa*/;
  /*line-height: 170px;*/
  color: #fff;
  transform: translateX(-100%);
  -webkit-transition: transform 0.01s ease-out;
  -o-transition: transform 0.01s ease-out;
  transition: transform 0.01s ease-out;
  padding:32px 4px 0 4px;
}

.ess-over .box:hover .overlay{
  transform: translateX(0);
}



/* CORNER */

.corner-bottom .overlay{
  background-color: #9c27b0;
  line-height: 200px;
  color: #fff;
  transform: translate(100%, 100%);
  -webkit-transition: transform 0.4s ease-in-out;
  -o-transition: transform 0.4s ease-in-out;
  transition: transform 0.4s ease-in-out;
}

.corner-bottom .box:hover .overlay{
   transform: translate(0, 0);
}

/* CORNER */

.corner-top .overlay{
  background-color: #ff5722;
  line-height: 200px;
  color: #fff;
  transform: translate(-100%, -100%);
  -webkit-transition: transform 0.4s ease-in-out;
  -o-transition: transform 0.4s ease-in-out;
  transition: transform 0.4s ease-in-out;
}

.corner-top .box:hover .overlay{
   transform: translate(0, 0);
}



h3.edu-title {
    writing-mode: vertical-rl;
    -ms-writing-mode:bt-rl;
    text-orientation: mixed;
    transform: rotate(180deg);
    margin: 0;
    color: #fff;
    border-bottom-right-radius: 6px;
    padding: 10px 6px;
    border-top-right-radius: 6px;
    background-color: #ff2f92;
}
h3.fin-title {
    writing-mode: vertical-rl;
    -ms-writing-mode:bt-rl;
    text-orientation: mixed;
    transform: rotate(180deg);
    margin: 0;
    color: #fff;
    border-bottom-right-radius: 6px;
    padding: 10px 6px;
    border-top-right-radius: 6px;
    background-color:#00b0f0;
}
h3.adm-title {
    writing-mode: vertical-rl;
    -ms-writing-mode:bt-rl;
    text-orientation: mixed;
    transform: rotate(180deg);
    margin: 0;
    color: #fff;
    border-bottom-right-radius: 6px;
    padding: 10px 6px;
    border-top-right-radius: 6px;
    background-color: #203864;
}
h3.mod-title{
    /*font-weight: 600;
    margin: 10px 0 20px 0;*/
    font-size:20px;
    max-width: 40px;
}
.brd-edu {
    border: 2px solid #ff2f92;
    border-radius: 10px;
    /*padding: 5px;*/
}
.brd-fin {
    border: 2px solid #00b0f0;
    border-radius: 10px;
    /*padding: 5px;*/
}
.brd-adm {
    border: 2px solid #203864;
    border-radius:  10px;
    /*padding: 5px;*/
}
h3.ess-title {
    font-size: 22px;
    padding-bottom: 6px;
    /*border-bottom: 2px solid rgba(141, 194, 76,0.4);*/
    
}
span.border-ess-h3 {
    display:block;
    border:none;
    margin-top: 6px;
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    height:2px;
    background:-webkit-gradient(linear, 0 0, 90% 0, from(#8dc24ce3), to(white), color-stop(50%, #8dc24ce3));
    background:-moz-linear-gradient(0 0, 100% 0, from(#8dc24ce3), to(white), color-stop(40%, #8dc24ce3));
}

   </style>
    <script type="text/javascript">
     var tempObj = '';
        function popUpDetails(url) {
            try {
                if (tempObj && !tempObj.closed) {
                    tempObj.focus();
                }
                else {
                    window.open(url);
                }
            }
            catch (e) { }

            return false;
        }

   </script>
   <div class="container">
      <%-- <h3 class="ess-title">Leave Transactions<span class="border-ess-h3"></span></h3>--%>
        <div class="row cf mb-lg-4">
 
        <asp:Repeater ID="rptESS1" runat="server">
            <ItemTemplate>
         
                
          <div class="ess-over four col-lg-4 col-md-6 col-sm-12 col-sm-auto">
				<div class="box">
					<span class="original">
						<i class='<%# Eval("ESM_ICON")%>' style="color:#8dc24c;" aria-hidden="true"></i>
						<h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</span>
                     <a id="btnESS" runat="server" onserverclick="lbText1_Click">
					<div class="overlay">
						<i class='<%# Eval("ESM_ICON")%>' aria-hidden="true"></i>
                        <h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</div>
                    </a>
				</div>
			</div>      
                
                
                         
   
        
                <asp:HiddenField ID="hdnMnu" value='<%# Eval("MNU_NAME")%>' runat="server"/>
                <asp:HiddenField ID="hdnCode" value='<%# Eval("MNU_CODE")%>' runat="server"/>
                </ItemTemplate>
           
       </asp:Repeater>
    </div>
    

    <%--<h3 class="ess-title">Personal Details<span class="border-ess-h3"></span></h3>--%>
    <div class="row cf mb-lg-4">
 
        <asp:Repeater ID="rptESS2" runat="server">
            <ItemTemplate>
                
              <div class="ess-over four col-lg-4 col-md-6 col-sm-12 col-xs-12">
				<div class="box">
					<span class="original">
						<i class='<%# Eval("ESM_ICON")%>' style="color:#8dc24c;" aria-hidden="true"></i>
						<h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</span>
                     <a id="btnESS" runat="server" onserverclick="lbText2_Click">
					<div class="overlay">
						<i class='<%# Eval("ESM_ICON")%>' aria-hidden="true"></i>
                        <h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</div>
                    </a>
				</div>
			</div>   
                
                  
       
        
                <asp:HiddenField ID="hdnMnu" value='<%# Eval("MNU_NAME")%>' runat="server"/>
                <asp:HiddenField ID="hdnCode" value='<%# Eval("MNU_CODE")%>' runat="server"/>
                </ItemTemplate>
           
       </asp:Repeater>
    </div>
   
  <%-- <h3 class="ess-title">Personal Development<span class="border-ess-h3"></span></h3>--%>
    <div class="row cf mb-lg-4">
 
        <asp:Repeater ID="rptESS3" runat="server">
            <ItemTemplate>

 <div class="ess-over four col-lg-4 col-md-6 col-sm-12 col-xs-12">
				<div class="box">
					<span class="original">
						<i class='<%# Eval("ESM_ICON")%>' style="color:#8dc24c;" aria-hidden="true"></i>
						<h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</span>
                    <a id="btnESS" runat="server" onserverclick="lbText3_Click">
					<div class="overlay">
						<i class='<%# Eval("ESM_ICON")%>' aria-hidden="true"></i>
                        <h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</div>
                    </a>
				</div>
			</div>
                  
        
        
                <asp:HiddenField ID="hdnMnu" value='<%# Eval("MNU_NAME")%>' runat="server"/>
                <asp:HiddenField ID="hdnCode" value='<%# Eval("MNU_CODE")%>' runat="server"/>
                </ItemTemplate>
           
       </asp:Repeater>
    </div>

<div class="row cf mb-lg-4">
 
        <asp:Repeater ID="rptESS4" runat="server">
            <ItemTemplate>

 <div class="ess-over four col-lg-4 col-md-6 col-sm-12 col-xs-12">
				<div class="box">
					<span class="original">
						<i class='<%# Eval("ESM_ICON")%>' style="color:#8dc24c;" aria-hidden="true"></i>
						<h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</span>
                    <a id="btnESS" runat="server" onserverclick="lbText4_Click">
					<div class="overlay">
						<i class='<%# Eval("ESM_ICON")%>' aria-hidden="true"></i>
                        <h5><%# Eval("ESM_MNU_NAME")%></h5>
                        <p class="card-text"><%# Eval("ESM_DESCR")%></p>
					</div>
                    </a>
				</div>
			</div>
                  
        
        
                <asp:HiddenField ID="hdnMnu" value='<%# Eval("MNU_NAME")%>' runat="server"/>
                <asp:HiddenField ID="hdnCode" value='<%# Eval("MNU_CODE")%>' runat="server"/>
                </ItemTemplate>
           
       </asp:Repeater>
    </div>

    </div>

   



   
</asp:Content>

