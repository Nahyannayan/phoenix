﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Telerik.Web.UI

Partial Class pdpSchool_V2
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim TAB_MANDATORY As New Hashtable
    Dim TAB_TITLE As New Hashtable

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            lbtnStep1.Attributes.Add("class", "cssStepBtnActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep2.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep3.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            ' lbtnStep4.Attributes.Add("class", "cssStepBtnInActiveDisable col-md-2 col-lg-2 col-sm-2")
            lbtnStep5.Attributes.Add("class", "cssStepBtnInActiveDisable col-md-2 col-lg-2 col-sm-2")
            tb_ObjectivesCurrent.Visible = True     'TAB-1
            tb_CareerAspirations.Visible = False    'TAB-3
            tb_competencies.Visible = False

            tb_summary.Visible = False              'TAB-5
            tb_IntermReview.Visible = False         'TAB-6
            ViewState("EMP_ID") = ""
            ViewState("EPR_ID") = ""
            ViewState("RVW_EMP_ID") = ""
            ViewState("PBS_ID") = ""
            ViewState("CYCL_ID") = ""
            ViewState("REVIEW_LEVEL") = ""
            ViewState("PDP_BSU_ID") = ""

            If Not (Request.QueryString("U") Is Nothing) Then
                ViewState("EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("U").Replace(" ", "+"))
            End If
            'EMPLOYEE REVIEW MASTER ID
            If Not (Request.QueryString("ER") Is Nothing) Then
                ViewState("EPR_ID") = Encr_decrData.Decrypt(Request.QueryString("ER").Replace(" ", "+"))
            End If
            'EMPLOYEE REVIEWER
            If Not (Request.QueryString("MR") Is Nothing) Then
                ViewState("RVW_EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("MR").Replace(" ", "+"))
            End If
            'CURRENT REVIEW STAGE FOR BSU 
            If Not (Request.QueryString("BR") Is Nothing) Then
                ViewState("PBS_ID") = Encr_decrData.Decrypt(Request.QueryString("BR").Replace(" ", "+"))
            End If
            'REVIEWING YEAR
            If Not (Request.QueryString("RY") Is Nothing) Then
                ViewState("CYCL_ID") = Encr_decrData.Decrypt(Request.QueryString("RY").Replace(" ", "+"))
            End If
            '@OPTION='LVL0' -- 0 -> Level 0 1 -> Reviewer LEVEL
            If Not (Request.QueryString("RL") Is Nothing) Then
                ViewState("REVIEW_LEVEL") = Encr_decrData.Decrypt(Request.QueryString("RL").Replace(" ", "+"))
            End If
            'REVIEW AGAINST WHICH BSU 
            If Not (Request.QueryString("B") Is Nothing) Then
                ViewState("PDP_BSU_ID") = Encr_decrData.Decrypt(Request.QueryString("B").Replace(" ", "+"))
            End If

            'ViewState("EMP_ID") = "5639"
            'ViewState("EPR_ID") = "7"
            'ViewState("RVW_EMP_ID") = "9219"
            'ViewState("PBS_ID") = "1"
            'ViewState("CYCL_ID") = "1"
            'ViewState("REVIEW_LEVEL") = "LVL1"
            'ViewState("PDP_BSU_ID") = "151001"

            ViewState("REVIEW_LEVEL") = "LVL" + GetEmployeeLevel()
            Bind_Competencies()
            If ViewState("EMP_ID") = "" Or ViewState("EPR_ID") = "" Or _
                ViewState("RVW_EMP_ID") = "" Or ViewState("PBS_ID") = "" _
                Or ViewState("CYCL_ID") = "" Or ViewState("REVIEW_LEVEL") = "" Or ViewState("PDP_BSU_ID") = "" Then
                'INVALID LOGIN
            Else
                Session("MIN_KPI_REQUIRED") = Nothing

                Intital_Data_Bind()
                BIND_PERF_INDICATOR()
                BIND_CAREER_DEVEL()
                Session("MIN_KPI_REQUIRED") = TAB_MANDATORY
                Session("KPI_TITLE") = TAB_TITLE
                Dim MSG As String = String.Empty
                Dim txtReadonly As String = String.Empty
                txtReadonly = CHECK_GET_OBJ(MSG)

                lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
                lbtnStep3.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
            End If
        End If
    End Sub

    Private Sub Intital_Data_Bind()
        ViewState("DISABLE_TEXTBOX") = "NO"
        BIND_EMP_COMPETENCIES()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PRI.INIT_DATA", pParms)
            While reader.Read()
                empName.InnerText = Convert.ToString(reader("EMP_NAME"))
                txt_EmpName.Value = Convert.ToString(reader("EMP_NAME"))
                txt_bsu.Value = Convert.ToString(reader("BSU_NAME"))
                txt_ManagerName.Value = Convert.ToString(reader("EMP_MANAGER_NAME"))
                txtEmpSigned.Value = Convert.ToString(reader("EMP_NAME"))
                txtMgrSigned1.Value = Convert.ToString(reader("EMP_MANAGER_NAME"))
                txt_role.Value = Convert.ToString(reader("EMP_DES"))
                empDesig.InnerText = Convert.ToString(reader("EMP_DES"))
                txt_FromDate.Value = Convert.ToString(reader("CYCLE_STARTDATE"))
                txt_ToDate.Value = Convert.ToString(reader("CYCLE_ENDDATE"))
                ViewState("CAREER_REQUIRED") = Convert.ToString(reader("CAREER_REQUIRED"))
                ViewState("DEVELOPMENT_REQUIRED") = Convert.ToString(reader("DEVELOPMENT_REQUIRED"))
                ViewState("COMPLETED_LEVEL0") = Convert.ToString(reader("LVL0_COMPL"))
                ViewState("COMPLETED_LEVEL1") = Convert.ToString(reader("LVL1_COMPL"))
                ViewState("DISABLE_FINAL_SAVE") = Convert.ToString(reader("DISABLE_FINAL_SAVE"))

                txtEmpDate.Value = Convert.ToString(reader("LVL1_FINISH_DT"))
                txtMgrDate1.Value = Convert.ToString(reader("LVL2_FINISH_DT"))

                If ViewState("REVIEW_LEVEL") = "LVL0" Then 'With  principal(level 0) login-and if level 0 is completed
                    btnRevert.Visible = False
                    cbeConfirmPDP.Enabled = True
                    divAlert.Visible = True
                    status_readonly()
                    If ViewState("COMPLETED_LEVEL0") = 1 Then
                        btn_Next.Visible = False
                        btn_Previous.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SaveFinish.Visible = False
                        ViewState("DISABLE_TEXTBOX") = "YES"
                        divAlert.Visible = False
                        MAKE_READONLY()
                    End If
                ElseIf ViewState("REVIEW_LEVEL") = "LVL1" Then  'With  reviewer (level 1) login-and if level 1 is completed
                    cbeConfirmPDP.Enabled = False
                    divAlert.Visible = False
                    MAKE_READONLY()
                    If ViewState("COMPLETED_LEVEL1") = 1 Then
                        btn_Next.Visible = False
                        btn_Previous.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SaveFinish.Visible = False
                        ViewState("DISABLE_TEXTBOX") = "YES"
                        btnRevert.Visible = True
                        MAKE_READONLY()
                    ElseIf ViewState("COMPLETED_LEVEL0") = 0 Then
                        btn_Next.Visible = False
                        btn_Previous.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SaveFinish.Visible = False
                        ViewState("DISABLE_TEXTBOX") = "YES"
                        btnRevert.Visible = False
                        MAKE_READONLY()
                    Else
                        divAlert.Visible = True
                        btn_Next.Text = "Next"
                        btn_Next.Visible = True
                        ' btn_Previous.Visible = True
                        btn_SaveDraft.Visible = True
                        btn_SaveFinish.Text = "Approve"
                        ' btn_SaveFinish.Visible = True
                        ViewState("DISABLE_TEXTBOX") = "YES"
                        btnRevert.Visible = True
                        status_readonly()
                    End If
                Else 'With  logined in 100 -and disable all
                    divAlert.Visible = False
                    btnRevert.Visible = False
                    btn_Next.Visible = True
                    ' btn_Previous.Visible = True
                    btn_Next.Text = "Next"
                    btn_SaveDraft.Visible = False
                    btn_SaveFinish.Visible = False
                    ViewState("DISABLE_TEXTBOX") = "YES"
                    MAKE_READONLY()
                End If

            End While
        End Using

        If ViewState("DISABLE_TEXTBOX") = "YES" Then
            txt_CarrierInput.Attributes.Add("readonly", "readonly")
            txt_ProfDevelNeeds.Attributes.Add("readonly", "readonly")
        End If
    End Sub
    Private Sub BIND_EMP_COMPETENCIES()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PRI.BIND_EMP_COMPETENCIES", pParms)
            While datareader.Read
                '-----------------------------1---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "1" Then
                    If Not ddlCOM_1.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_1.ClearSelection()
                        ddlCOM_1.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM1.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR1.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------2---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "2" Then
                    If Not ddlCOM_2.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_2.ClearSelection()
                        ddlCOM_2.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM2.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR2.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------3---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "3" Then
                    If Not ddlCOM_3.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_3.ClearSelection()
                        ddlCOM_3.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM3.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR3.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If


                '-----------------------------4---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "4" Then
                    If Not ddlCOM_4.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_4.ClearSelection()
                        ddlCOM_4.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM4.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR4.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------5---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "5" Then
                    If Not ddlCOM_5.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_5.ClearSelection()
                        ddlCOM_5.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM5.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR5.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------6---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "6" Then
                    If Not ddlCOM_6.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_6.ClearSelection()
                        ddlCOM_6.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM6.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR6.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If


                '-----------------------------7---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "7" Then
                    If Not ddlCOM_7.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_7.ClearSelection()
                        ddlCOM_7.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM7.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR7.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If
                '-----------------------------8---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "8" Then
                    If Not ddlCOM_8.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_8.ClearSelection()
                        ddlCOM_8.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM8.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR8.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------9---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "9" Then
                    If Not ddlCOM_9.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_9.ClearSelection()
                        ddlCOM_9.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM9.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR9.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------10---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "10" Then
                    If Not ddlCOM_10.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_10.ClearSelection()
                        ddlCOM_10.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM10.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR10.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If


            End While
        End Using

    End Sub
    Private Sub BIND_PERF_INDICATOR()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        pParms(7) = New SqlClient.SqlParameter("@INFO_TYPE", "CAT")
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[PRI].[INIT_OBJECTIVES]", pParms)
            rptKRA_M.DataSource = datareader
            rptKRA_M.DataBind()
        End Using

    End Sub
    Protected Sub rptKRA_M_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptKRA_M.ItemDataBound
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        pParms(7) = New SqlClient.SqlParameter("@INFO_TYPE", "DETAIL")

        Dim hfKRA_ID As New HiddenField
        Dim hfKRA_MIN_REQD As New HiddenField
        Dim hfKRA_DESCR As New HiddenField
        Dim hfTab_id As New HiddenField

        Dim rptPerf_Indicator As New Repeater
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            hfKRA_ID = DirectCast(e.Item.FindControl("hfKRA_ID"), HiddenField)
            hfKRA_MIN_REQD = DirectCast(e.Item.FindControl("hfKRA_MIN_REQD"), HiddenField)
            hfKRA_DESCR = DirectCast(e.Item.FindControl("hfKRA_DESCR"), HiddenField)
            hfTab_id = DirectCast(e.Item.FindControl("hfTab_id"), HiddenField)
            If hfCurrTabid.Value = "" Then
                hfCurrTabid.Value = hfTab_id.Value
            End If

            TAB_MANDATORY.Add(hfKRA_ID.Value, hfKRA_MIN_REQD.Value)
            TAB_TITLE.Add(hfKRA_ID.Value, hfKRA_DESCR.Value)

            rptPerf_Indicator = DirectCast(e.Item.FindControl("rptPerf_Indicator"), Repeater)
            pParms(8) = New SqlParameter("@KRA_ID", hfKRA_ID.Value)
            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[PRI].[INIT_OBJECTIVES]", pParms)
                rptPerf_Indicator.DataSource = datareader
                rptPerf_Indicator.DataBind()
            End Using

        End If

    End Sub
    Private Sub BIND_CAREER_DEVEL()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PRI.INIT_CAREER_DEVEL", pParms)
            While datareader.Read
                txt_CarrierInput.Value = Convert.ToString(datareader("EPR_CAREER_ASPR"))
                txt_ProfDevelNeeds.Value = Convert.ToString(datareader("EPR_PD_NEEDS"))

            End While
        End Using

    End Sub
    Protected Sub lbtnStep1_Click(sender As Object, e As EventArgs) Handles lbtnStep1.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""

        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep1.Attributes.Add("class", "cssStepBtnActive")

        tb_ObjectivesCurrent.Visible = True    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-3
        tb_competencies.Visible = False

        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = False

        'TAB-6
        btn_Previous.Visible = False
        btn_Next.Visible = True
        btn_SaveFinish.Visible = False
        VALIDATE_BUTTTON_RIGHTS()
    End Sub


    Protected Sub lbtnStep2_Click(sender As Object, e As EventArgs) Handles lbtnStep2.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = True    'TAB-3
        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = False         'TAB-6
        tb_competencies.Visible = False

        Dim StatusObjective As String = String.Empty
        Dim XML_OBJ As String = CHECK_GET_OBJ(StatusObjective)
        lbtnStep1.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", "cssStepBtnActive")
        lbtnStep3.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))

        btn_Next.Visible = False
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = True

        VALIDATE_BUTTTON_RIGHTS()
    End Sub
    Private Sub VALIDATE_BUTTTON_RIGHTS()
        If ViewState("REVIEW_LEVEL") = "LVL0" Then 'With  principal(level 0) login-and if level 0 is completed
            btnRevert.Visible = False
            cbeConfirmPDP.Enabled = True
            divAlert.Visible = True
            status_readonly()
            If ViewState("COMPLETED_LEVEL0") = 1 Then
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                ViewState("DISABLE_TEXTBOX") = "YES"
                divAlert.Visible = False
                MAKE_READONLY()
            End If
        ElseIf ViewState("REVIEW_LEVEL") = "LVL1" Then  'With  reviewer (level 1) login-and if level 1 is completed
            cbeConfirmPDP.Enabled = False
            divAlert.Visible = False
            MAKE_READONLY()
            If ViewState("COMPLETED_LEVEL1") = 1 Then
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                ViewState("DISABLE_TEXTBOX") = "YES"
                btnRevert.Visible = True
                MAKE_READONLY()
            ElseIf ViewState("COMPLETED_LEVEL0") = 0 Then
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                ViewState("DISABLE_TEXTBOX") = "YES"
                btnRevert.Visible = False
                MAKE_READONLY()
            Else
                'btn_Next.Visible = True
                btn_Next.Text = "Next"
                'btn_Previous.Visible = True
                btn_SaveDraft.Visible = True
                btn_SaveFinish.Text = "Approve"
                ' btn_SaveFinish.Visible = True
                ViewState("DISABLE_TEXTBOX") = "YES"
                btnRevert.Visible = True
                status_readonly()
            End If
        Else 'With  logined in 100 -and disable all
            divAlert.Visible = False
            btnRevert.Visible = False
            ' btn_Next.Visible = True
            '  btn_Previous.Visible = True
            btn_SaveDraft.Visible = False
            btn_SaveFinish.Visible = False
            btn_Next.Text = "Next"
            ViewState("DISABLE_TEXTBOX") = "YES"
            MAKE_READONLY()
        End If

    End Sub
    Protected Sub lbtnStep3_Click(sender As Object, e As EventArgs) Handles lbtnStep3.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-3
        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = False         'TAB-6
        tb_competencies.Visible = True

        Dim StatusObjective As String = String.Empty
        Dim XML_OBJ As String = CHECK_GET_OBJ(StatusObjective)
        lbtnStep1.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", "cssStepBtnActive")
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))

        btn_Next.Visible = True
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = False

        VALIDATE_BUTTTON_RIGHTS()
    End Sub

    Protected Sub lbtnStep4_Click(sender As Object, e As EventArgs) Handles lbtnStep4.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
    End Sub

    Protected Sub lbtnStep5_Click(sender As Object, e As EventArgs) Handles lbtnStep5.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
    End Sub

    Private Sub SAVE_OBJECTIVE(ByRef errorMessage As String, ByVal XML_OBJ As String, transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Dim PARAM(12) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        PARAM(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        PARAM(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        PARAM(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        PARAM(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        PARAM(5) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        PARAM(6) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        PARAM(7) = New SqlClient.SqlParameter("@STR_XML", "<KPI_M>" + XML_OBJ + "</KPI_M>")
        PARAM(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[PRI].[SAVEINIT_OBJECTIVES_XML]", PARAM)
        ReturnFlag = PARAM(8).Value
        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If


    End Sub
    Private Sub SAVE_CAREER_DEVELOPMENT(ByRef errorMessage As String, transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Dim PARAM(14) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        PARAM(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        PARAM(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        PARAM(3) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        PARAM(4) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        PARAM(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        PARAM(6) = New SqlClient.SqlParameter("@EPR_CAREER_ASPR", txt_CarrierInput.InnerText.Trim())
        PARAM(7) = New SqlClient.SqlParameter("@EPR_PD_NEEDS", txt_ProfDevelNeeds.InnerText.Trim())
        PARAM(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[PRI].[SAVEINIT_CAREER_DEVELOPMENT]", PARAM)
        ReturnFlag = PARAM(8).Value

        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If

    End Sub

    Private Function CHECK_GET_OBJ(ByRef errormsg As String) As String

        Dim STR_XML As New StringBuilder

        Dim KPI_ID As String = String.Empty
        Dim KRA_ID As String = String.Empty
        Dim KPI_ID_COMPLETED As Integer
        Dim KPI_OBJ_TXT As String = String.Empty
        Dim TAB_CHECK As New Hashtable
        Dim MANDATORY_FIELDS_EXISTS As String = "NO"
        Dim TOTAL_OBJ_ENTRY_DONE As Integer
        Dim str_app As String = String.Empty
        Dim rptPerf_Indicator As Repeater
        Dim objTxtBox As HtmlTextArea
        Dim hfKPI_ID As HiddenField
        Dim hfKRA_ID As HiddenField
        Dim TAB_MANDATORY_comp As New Hashtable
        TAB_MANDATORY_comp = Session("MIN_KPI_REQUIRED")
        Dim TAB_TITLE_msg As New Hashtable
        TAB_TITLE_msg = Session("KPI_TITLE")

        For Each objItem As RepeaterItem In rptKRA_M.Items
            rptPerf_Indicator = DirectCast(objItem.FindControl("rptPerf_Indicator"), Repeater)
            KPI_ID_COMPLETED = 0
            hfKRA_ID = DirectCast(objItem.FindControl("hfKRA_ID"), HiddenField)
            For Each objChildItem As RepeaterItem In rptPerf_Indicator.Items
                objTxtBox = DirectCast(objChildItem.FindControl("txt_KPIObjective"), HtmlTextArea)
                hfKPI_ID = DirectCast(objChildItem.FindControl("hfKPI_ID"), HiddenField)
                KPI_OBJ_TXT = objTxtBox.Value.ToString.Trim.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")

                If ViewState("DISABLE_TEXTBOX") = "YES" Then
                    objTxtBox.Attributes.Add("readonly", "readonly")
                End If

                STR_XML.Append(String.Format("<KPI KRA_ID='{0}' KPI_ID='{1}' KPI_OBJ_TXT ='{2}'/>", hfKRA_ID.Value, _
              hfKPI_ID.Value, KPI_OBJ_TXT))
                If KPI_OBJ_TXT <> "" Then
                    KPI_ID_COMPLETED += 1
                    TOTAL_OBJ_ENTRY_DONE += 1
                End If
            Next
            If TAB_MANDATORY_comp(hfKRA_ID.Value) <> 0 Then
                MANDATORY_FIELDS_EXISTS = "YES"
                If KPI_ID_COMPLETED < TAB_MANDATORY_comp(hfKRA_ID.Value) Then
                    errormsg += "<div>Minimum Objectives Required For " + TAB_TITLE_msg(hfKRA_ID.Value) + " : " + TAB_MANDATORY_comp(hfKRA_ID.Value) + "</div>"
                End If
            End If
        Next
        If (TOTAL_OBJ_ENTRY_DONE < 1) And (MANDATORY_FIELDS_EXISTS = "NO") Then
            errormsg = "NOENTRY"
        End If

        Return STR_XML.ToString()
    End Function
    Private Function CHECK_CAREER_DEVELOPMENT_COMPLETED() As String
        Dim errormsg As String = String.Empty
        If Convert.ToInt16(ViewState("CAREER_REQUIRED")) <> 0 Then

            If (txt_CarrierInput.InnerText.Trim() = "") Then
                errormsg += "<div>Enter Your Career Aspirations</div>"
            End If
        End If
        If Convert.ToInt16(ViewState("DEVELOPMENT_REQUIRED")) <> 0 Then
            If (txt_ProfDevelNeeds.InnerText.Trim() = "") Then
                errormsg += "<div>Enter your Professional Development Needs</div>"
            End If
        End If


        Return errormsg
    End Function
    Private Function CHECK_Competencies() As String
        Dim errormsg As String = String.Empty

        If ((ddlCOM_1.SelectedValue <> "0" And txt_COM1.InnerText.Trim() = "") Or (ddlCOM_1.SelectedValue = "0" And txt_COM1.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_2.SelectedValue <> "0" And txt_COM2.InnerText.Trim() = "") Or (ddlCOM_2.SelectedValue = "0" And txt_COM2.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_3.SelectedValue <> "0" And txt_COM3.InnerText.Trim() = "") Or (ddlCOM_3.SelectedValue = "0" And txt_COM3.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_4.SelectedValue <> "0" And txt_COM4.InnerText.Trim() = "") Or (ddlCOM_4.SelectedValue = "0" And txt_COM4.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_5.SelectedValue <> "0" And txt_COM5.InnerText.Trim() = "") Or (ddlCOM_5.SelectedValue = "0" And txt_COM5.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_6.SelectedValue <> "0" And txt_COM6.InnerText.Trim() = "") Or (ddlCOM_6.SelectedValue = "0" And txt_COM6.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_7.SelectedValue <> "0" And txt_COM7.InnerText.Trim() = "") Or (ddlCOM_7.SelectedValue = "0" And txt_COM7.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_8.SelectedValue <> "0" And txt_COM8.InnerText.Trim() = "") Or (ddlCOM_8.SelectedValue = "0" And txt_COM8.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_9.SelectedValue <> "0" And txt_COM9.InnerText.Trim() = "") Or (ddlCOM_9.SelectedValue = "0" And txt_COM9.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If
        If ((ddlCOM_10.SelectedValue <> "0" And txt_COM10.InnerText.Trim() = "") Or (ddlCOM_10.SelectedValue = "0" And txt_COM10.InnerText.Trim() <> "")) Then
            errormsg = "Competencies or Employee comments field is empty on Competencies Page , please check."

        End If


        Return errormsg
    End Function
    Function save_comp() As Integer
        Dim stat As Integer
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
               
                If ERRORMSG <> "-1" Then
                    Save_COMPETENCIES(ERRORMSG, transaction)
                End If
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                    stat = 1
                  
                Else
                    transaction.Commit()
                    stat = 0
                    
                End If
            End Try
        End Using
        Return stat
    End Function
    Protected Sub btn_SaveDraft_Click(sender As Object, e As EventArgs) Handles btn_SaveDraft.Click

        Dim XML_OBJ As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                XML_OBJ = CHECK_GET_OBJ(ERRORMSG)

                SAVE_OBJECTIVE(ERRORMSG, XML_OBJ, transaction)
                If ERRORMSG <> "-1" Then
                    SAVE_CAREER_DEVELOPMENT(ERRORMSG, transaction)
                End If
                If ERRORMSG <> "-1" Then
                    Save_COMPETENCIES(ERRORMSG, transaction)
                End If
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()

                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    lblError.Text = "<div>Error occured while saving </div>"
                Else
                    transaction.Commit()

                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                    lblError.Text = "<div>You have successfully saved your Performance Development Plan </div>"
                End If
            End Try
        End Using






    End Sub
    Protected Sub btn_Previous_Click(sender As Object, e As EventArgs) Handles btn_Previous.Click
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                If tb_CareerAspirations.Visible = True Then 'TAB-2
                    lbtnStep3_Click(lbtnStep3, Nothing)
                    SAVE_CAREER_DEVELOPMENT(ERRORMSG, transaction)
                ElseIf tb_competencies.Visible = True Then 'TAB-2
                    lbtnStep1_Click(lbtnStep1, Nothing)
                    Save_COMPETENCIES(ERRORMSG, transaction)

                End If

            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using


    End Sub
    Protected Sub btn_Next_Click(sender As Object, e As EventArgs) Handles btn_Next.Click

        Dim XML_OBJ As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                If tb_ObjectivesCurrent.Visible = True Then 'TAB-1
                    lbtnStep3_Click(lbtnStep3, Nothing)
                    XML_OBJ = CHECK_GET_OBJ(ERRORMSG)
                    SAVE_OBJECTIVE(ERRORMSG, XML_OBJ, transaction)

                ElseIf tb_competencies.Visible = True Then 'TAB-2
                    lbtnStep2_Click(lbtnStep2, Nothing)
                    Save_COMPETENCIES(ERRORMSG, transaction)

                End If

            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using



    End Sub
    Protected Sub btn_SaveFinish_Click(sender As Object, e As EventArgs) Handles btn_SaveFinish.Click

        Dim errormsg As String = String.Empty
        Dim Status As Integer = 0
        Dim XML_OBJ As String = CHECK_GET_OBJ(errormsg)
        Dim CAREER_DEVEL_MSG As String = CHECK_CAREER_DEVELOPMENT_COMPLETED()
        Dim competencies_MSG As String = CHECK_Competencies()
        If errormsg = "NOENTRY" Then
            errormsg = "<div>Minimum One Objectives Required Against Any Key Result Area </div>"
        End If

        If CAREER_DEVEL_MSG <> "" Then
            errormsg += "<div>" & CAREER_DEVEL_MSG & " </div>"
        End If

        If competencies_MSG <> "" Then
            errormsg += "<div>" & competencies_MSG & " </div>"
        End If

        If errormsg <> "" Then
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
            lblError.Text = errormsg
        Else
            Status = save_comp()

            If Status <> 0 Then
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoError"
                lblError.Text = "Request could not be processed"
            Else
                Status = SAVE_FINAL_KPI(errormsg)

                If Status <> 0 Then
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    lblError.Text = errormsg
                Else

                    Intital_Data_Bind()
                    Dim MSG As String = String.Empty
                    Dim txtReadonly As String = String.Empty
                    txtReadonly = CHECK_GET_OBJ(MSG)

                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"

                    If ViewState("REVIEW_LEVEL") = "LVL0" Then
                        lblError.Text = "<div>You have successfully completed the Performance Development Plan. </div>"
                    ElseIf ViewState("REVIEW_LEVEL") = "LVL1" Then
                        lblError.Text = "<div>You have successfully approved the performance objectives for  " & empName.InnerText & " </div>"
                    End If
                End If
            End If

        End If

    End Sub
    Private Sub Save_COMPETENCIES(ByVal errorMessage As String, ByVal sqltran As SqlTransaction)
        Dim ReturnFlag As Integer


        Dim pParms(33) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

        pParms(1) = New SqlClient.SqlParameter("@COMP_COM_ID1", ddlCOM_1.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@COMP_EMP_CMNT1", txt_COM1.InnerText.Trim())
        pParms(3) = New SqlClient.SqlParameter("@COMP_MGR_CMNT1", txt_COM_MGR1.InnerText.Trim())


        pParms(4) = New SqlClient.SqlParameter("@COMP_COM_ID2", ddlCOM_2.SelectedValue)
        pParms(5) = New SqlClient.SqlParameter("@COMP_EMP_CMNT2", txt_COM2.InnerText.Trim())
        pParms(6) = New SqlClient.SqlParameter("@COMP_MGR_CMNT2", txt_COM_MGR2.InnerText.Trim())


        pParms(7) = New SqlClient.SqlParameter("@COMP_COM_ID3", ddlCOM_3.SelectedValue)
        pParms(8) = New SqlClient.SqlParameter("@COMP_EMP_CMNT3", txt_COM3.InnerText.Trim())
        pParms(9) = New SqlClient.SqlParameter("@COMP_MGR_CMNT3", txt_COM_MGR3.InnerText.Trim())


        pParms(10) = New SqlClient.SqlParameter("@COMP_COM_ID4", ddlCOM_4.SelectedValue)
        pParms(11) = New SqlClient.SqlParameter("@COMP_EMP_CMNT4", txt_COM4.InnerText.Trim())
        pParms(12) = New SqlClient.SqlParameter("@COMP_MGR_CMNT4", txt_COM_MGR4.InnerText.Trim())

        pParms(13) = New SqlClient.SqlParameter("@COMP_COM_ID5", ddlCOM_5.SelectedValue)
        pParms(14) = New SqlClient.SqlParameter("@COMP_EMP_CMNT5", txt_COM5.InnerText.Trim())
        pParms(15) = New SqlClient.SqlParameter("@COMP_MGR_CMNT5", txt_COM_MGR5.InnerText.Trim())


        pParms(16) = New SqlClient.SqlParameter("@COMP_COM_ID6", ddlCOM_6.SelectedValue)
        pParms(17) = New SqlClient.SqlParameter("@COMP_EMP_CMNT6", txt_COM6.InnerText.Trim())
        pParms(18) = New SqlClient.SqlParameter("@COMP_MGR_CMNT6", txt_COM_MGR6.InnerText.Trim())


        pParms(19) = New SqlClient.SqlParameter("@COMP_COM_ID7", ddlCOM_7.SelectedValue)
        pParms(20) = New SqlClient.SqlParameter("@COMP_EMP_CMNT7", txt_COM7.InnerText.Trim())
        pParms(21) = New SqlClient.SqlParameter("@COMP_MGR_CMNT7", txt_COM_MGR7.InnerText.Trim())

        pParms(22) = New SqlClient.SqlParameter("@COMP_COM_ID8", ddlCOM_8.SelectedValue)
        pParms(23) = New SqlClient.SqlParameter("@COMP_EMP_CMNT8", txt_COM8.InnerText.Trim())
        pParms(24) = New SqlClient.SqlParameter("@COMP_MGR_CMNT8", txt_COM_MGR8.InnerText.Trim())

        pParms(25) = New SqlClient.SqlParameter("@COMP_COM_ID9", ddlCOM_9.SelectedValue)
        pParms(26) = New SqlClient.SqlParameter("@COMP_EMP_CMNT9", txt_COM9.InnerText.Trim())
        pParms(27) = New SqlClient.SqlParameter("@COMP_MGR_CMNT9", txt_COM_MGR9.InnerText.Trim())


        pParms(28) = New SqlClient.SqlParameter("@COMP_COM_ID10", ddlCOM_10.SelectedValue)
        pParms(29) = New SqlClient.SqlParameter("@COMP_EMP_CMNT10", txt_COM10.InnerText.Trim())
        pParms(30) = New SqlClient.SqlParameter("@COMP_MGR_CMNT10", txt_COM_MGR10.InnerText.Trim())

        pParms(31) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(31).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PRI.SAVE_EMP_COMPETENCIES", pParms)
        ReturnFlag = pParms(31).Value

        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If

    End Sub
    Private Function SAVE_FINAL_KPI(ByRef errorMessage As String) As Integer

        Dim ReturnFlag As Integer
        Dim Transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            Transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim PARAM(14) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
                PARAM(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
                PARAM(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                PARAM(3) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
                PARAM(4) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
                PARAM(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
                PARAM(6) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))

                PARAM(7) = New SqlClient.SqlParameter("@ASPIRE", txt_CarrierInput.InnerText.Trim())
                PARAM(8) = New SqlClient.SqlParameter("@PDNEEDS", txt_ProfDevelNeeds.InnerText.Trim())
                PARAM(9) = New SqlClient.SqlParameter("@FINAL", "1")
                PARAM(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(10).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "[PRI].[FINAL_SAVE]", PARAM)
                ReturnFlag = PARAM(10).Value

                If ReturnFlag <> 0 Then
                    ReturnFlag = 1
                End If

            Catch ex As Exception
                ReturnFlag = 1
                errorMessage = "Error Occured While Saving."
            Finally
                If ReturnFlag = 1 Then

                    errorMessage = "Error Occured While Saving."
                    Transaction.Rollback()
                Else
                    errorMessage = ""
                    Transaction.Commit()
                End If
            End Try

        End Using
        Return ReturnFlag

    End Function
    Public Function GetEmployeeLevel() As String
        Dim EmpLevel As String = String.Empty

        Try
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            EmpLevel = SqlHelper.ExecuteScalar(connStr, _
            CommandType.StoredProcedure, "PRI.GET_EMP_LEVEL", pParms)
            Return EmpLevel
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmployeeLevel")
        End Try
    End Function
    Protected Sub btnRevert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRevert.Click
        Try

            Panel_RevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_PRINCIPAL_ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", ViewState("RVW_EMP_ID"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_RIEVEWERS_TOREVERT", pParms)
                rbtnList_Rievewer.DataSource = ds
                rbtnList_Rievewer.DataTextField = "EMP_NAME"
                rbtnList_Rievewer.DataValueField = "EMP_ID"
                rbtnList_Rievewer.DataBind()
                rbtnList_Rievewer.Items(0).Selected = True
                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_RevertBack.Visible = False

        End Try


    End Sub
    Protected Sub btn_Revert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Revert.Click
        ViewState("FINAL") = "0"
        Dim XML_OBJ As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                cbeRevert.Enabled = True
                ' Panel_RevertBack.Visible = True
                FINAL_REVERT(ERRORMSG, transaction)




            Catch ex As Exception
                ERRORMSG = "-1"
                Panel_RevertBack.Visible = False
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using
        Intital_Data_Bind()

    End Sub
    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancel.Click
        Panel_RevertBack.Visible = False
        rbtnList_Rievewer.ClearSelection()
        txt_Remarks.InnerText = ""
        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
    End Sub
    Private Sub FINAL_REVERT(ByRef errorMessage As String, ByVal transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Panel_RevertBack.Visible = False

        Dim PARAM(12) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        PARAM(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", ViewState("EMP_ID"))
        PARAM(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        PARAM(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
        PARAM(4) = New SqlClient.SqlParameter("@URL", "")


        PARAM(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(5).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[PRI].[REVERT_BACKTO_REVIEWER]", PARAM)
        ReturnFlag = PARAM(5).Value
        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        Else
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"


            lblError.Text = "<div>You have succesully reverted the PDP for  " & empName.InnerText & " </div>"

        End If


    End Sub
    Protected Sub A1_ServerClick(sender As Object, e As EventArgs) Handles A1.ServerClick
        Span2.InnerHtml = "Page <font color='#58B0E7'>1</font> of 2"

        A1.Attributes.Add("class", "current")
        A2.Attributes.Add("class", " ")

        ddlCOM_1.Visible = True
        ddlCOM_2.Visible = True
        ddlCOM_3.Visible = True
        ddlCOM_4.Visible = True
        ddlCOM_5.Visible = True

        ddlCOM_6.Visible = False
        ddlCOM_7.Visible = False
        ddlCOM_8.Visible = False
        ddlCOM_9.Visible = False
        ddlCOM_10.Visible = False


        txt_COM1.Visible = True
        txt_COM2.Visible = True
        txt_COM3.Visible = True
        txt_COM4.Visible = True
        txt_COM5.Visible = True

        txt_COM6.Visible = False
        txt_COM7.Visible = False
        txt_COM8.Visible = False
        txt_COM9.Visible = False
        txt_COM10.Visible = False

        txt_COM_MGR1.Visible = True
        txt_COM_MGR2.Visible = True
        txt_COM_MGR3.Visible = True
        txt_COM_MGR4.Visible = True
        txt_COM_MGR5.Visible = True

        txt_COM_MGR6.Visible = False
        txt_COM_MGR7.Visible = False
        txt_COM_MGR8.Visible = False
        txt_COM_MGR9.Visible = False
        txt_COM_MGR10.Visible = False


        divNote.Visible = False
    End Sub

    Protected Sub A2_ServerClick(sender As Object, e As EventArgs) Handles A2.ServerClick
        Span2.InnerHtml = "Page <font color='#58B0E7'>2</font> of 2"
        A2.Attributes.Add("class", "current")
        A1.Attributes.Add("class", " ")

        ddlCOM_1.Visible = False
        ddlCOM_2.Visible = False
        ddlCOM_3.Visible = False
        ddlCOM_4.Visible = False
        ddlCOM_5.Visible = False

        ddlCOM_6.Visible = True
        ddlCOM_7.Visible = True
        ddlCOM_8.Visible = True
        ddlCOM_9.Visible = True
        ddlCOM_10.Visible = True


        txt_COM1.Visible = False
        txt_COM2.Visible = False
        txt_COM3.Visible = False
        txt_COM4.Visible = False
        txt_COM5.Visible = False

        txt_COM6.Visible = True
        txt_COM7.Visible = True
        txt_COM8.Visible = True
        txt_COM9.Visible = True
        txt_COM10.Visible = True

        txt_COM_MGR1.Visible = False
        txt_COM_MGR2.Visible = False
        txt_COM_MGR3.Visible = False
        txt_COM_MGR4.Visible = False
        txt_COM_MGR5.Visible = False

        txt_COM_MGR6.Visible = True
        txt_COM_MGR7.Visible = True
        txt_COM_MGR8.Visible = True
        txt_COM_MGR9.Visible = True
        txt_COM_MGR10.Visible = True

        divNote.Visible = False
    End Sub
    Private Sub Bind_Competencies()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@INFO_TYPE", "COMP")
        pParms(1) = New SqlClient.SqlParameter("@PDPY_ID", Session("PDP_YEAR"))

        ddlCOM_1.Items.Clear()
        ddlCOM_2.Items.Clear()
        ddlCOM_3.Items.Clear()
        ddlCOM_4.Items.Clear()
        ddlCOM_5.Items.Clear()
        ddlCOM_6.Items.Clear()
        ddlCOM_7.Items.Clear()
        ddlCOM_8.Items.Clear()
        ddlCOM_9.Items.Clear()
        ddlCOM_10.Items.Clear()
        ddlCOM_1.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_2.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_3.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_4.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_5.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_6.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_7.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_8.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_9.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_10.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PRI.BIND_MASTER_DETAILS", pParms)
            While datareader.Read

                ddlCOM_1.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_2.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_3.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_4.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_5.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_6.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_7.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_8.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_9.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_10.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))


            End While
        End Using

    End Sub
    Private Sub status_readonly()
        If ViewState("REVIEW_LEVEL") = "LVL1" Then
            If ddlCOM_1.SelectedItem.Value <> 0 Then
                txt_COM_MGR1.Attributes.Remove("readonly")
            Else
                txt_COM_MGR1.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_2.SelectedItem.Value <> 0 Then
                txt_COM_MGR2.Attributes.Remove("readonly")
            Else
                txt_COM_MGR2.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_3.SelectedItem.Value <> 0 Then
                txt_COM_MGR3.Attributes.Remove("readonly")
            Else
                txt_COM_MGR3.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_4.SelectedItem.Value <> 0 Then
                txt_COM_MGR4.Attributes.Remove("readonly")
            Else
                txt_COM_MGR4.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_5.SelectedItem.Value <> 0 Then
                txt_COM_MGR5.Attributes.Remove("readonly")
            Else
                txt_COM_MGR5.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_6.SelectedItem.Value <> 0 Then
                txt_COM_MGR6.Attributes.Remove("readonly")
            Else
                txt_COM_MGR6.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_7.SelectedItem.Value <> 0 Then
                txt_COM_MGR7.Attributes.Remove("readonly")
            Else
                txt_COM_MGR7.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_8.SelectedItem.Value <> 0 Then
                txt_COM_MGR8.Attributes.Remove("readonly")
            Else
                txt_COM_MGR8.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_9.SelectedItem.Value <> 0 Then
                txt_COM_MGR9.Attributes.Remove("readonly")
            Else
                txt_COM_MGR9.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_10.SelectedItem.Value <> 0 Then
                txt_COM_MGR10.Attributes.Remove("readonly")
            Else
                txt_COM_MGR10.Attributes.Add("readonly", "readonly")
            End If

        Else
            txt_COM_MGR1.Attributes.Add("readonly", "readonly")
            txt_COM_MGR2.Attributes.Add("readonly", "readonly")
            txt_COM_MGR3.Attributes.Add("readonly", "readonly")
            txt_COM_MGR4.Attributes.Add("readonly", "readonly")
            txt_COM_MGR5.Attributes.Add("readonly", "readonly")
            txt_COM_MGR6.Attributes.Add("readonly", "readonly")
            txt_COM_MGR7.Attributes.Add("readonly", "readonly")
            txt_COM_MGR8.Attributes.Add("readonly", "readonly")
            txt_COM_MGR9.Attributes.Add("readonly", "readonly")
            txt_COM_MGR10.Attributes.Add("readonly", "readonly")
        End If
    End Sub
    Private Sub MAKE_READONLY()
        
        ddlCOM_1.Enabled = False
        ddlCOM_2.Enabled = False
        ddlCOM_3.Enabled = False
        ddlCOM_4.Enabled = False
        ddlCOM_5.Enabled = False
        ddlCOM_6.Enabled = False
        ddlCOM_7.Enabled = False
        ddlCOM_8.Enabled = False
        ddlCOM_9.Enabled = False
        ddlCOM_10.Enabled = False

        txt_COM1.Attributes.Add("readonly", "readonly")
        txt_COM2.Attributes.Add("readonly", "readonly")
        txt_COM3.Attributes.Add("readonly", "readonly")
        txt_COM4.Attributes.Add("readonly", "readonly")
        txt_COM5.Attributes.Add("readonly", "readonly")
        txt_COM6.Attributes.Add("readonly", "readonly")
        txt_COM7.Attributes.Add("readonly", "readonly")
        txt_COM8.Attributes.Add("readonly", "readonly")
        txt_COM9.Attributes.Add("readonly", "readonly")
        txt_COM10.Attributes.Add("readonly", "readonly")

        txt_COM_MGR1.Attributes.Add("readonly", "readonly")
        txt_COM_MGR2.Attributes.Add("readonly", "readonly")
        txt_COM_MGR3.Attributes.Add("readonly", "readonly")
        txt_COM_MGR4.Attributes.Add("readonly", "readonly")
        txt_COM_MGR5.Attributes.Add("readonly", "readonly")
        txt_COM_MGR6.Attributes.Add("readonly", "readonly")
        txt_COM_MGR7.Attributes.Add("readonly", "readonly")
        txt_COM_MGR8.Attributes.Add("readonly", "readonly")
        txt_COM_MGR9.Attributes.Add("readonly", "readonly")
        txt_COM_MGR10.Attributes.Add("readonly", "readonly")

    End Sub
End Class
