﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="pdpSchool_interim_v2.aspx.vb" Inherits="PDP_pdpSchool_interim_v2" MaintainScrollPositionOnPostback ="true"%>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>GEMS Performance Development Plan</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    <link href="../PDP/Styles/pdpSchool.css?id=ver2.4" rel="stylesheet" />
       <link href="../PDP/Styles/bootstrap.css?id=ver2.4" rel="stylesheet" />

       <script type="text/javascript">

           function ShowTabs(tab) {
               if (tab == 1) {
                   $("#tb_ObjectivesCurrent").show();
                   $("#tb_SmartObjective").hide();
               }
               else if (tab == 2) {
                   $("#tb_ObjectivesCurrent").hide();
                   $("#tb_SmartObjective").show();

               }
               return false;
           }

           function showHide() {

               $header = $(".header");
               $content = $(".contentPanel");
               $ColExpId = $("#ColExpCss");

               $content.slideToggle(500, function () {

                   if ($content.is(":visible") == true) {

                       $ColExpId.removeClass("colClass").addClass("ExpClass");
                   }
                   else {

                       $ColExpId.removeClass("ExpClass").addClass("colClass");
                   }

               });

           }




           function isNumberKey(evt) {

               var charCode = (evt.which) ? evt.which : event.keyCode
               if (charCode > 31 && (charCode < 48 || charCode > 57))
                   return false;

               return true;
           }

           function getrowid(v1) {
               document.getElementById("hf_Interim").value = v1;
           }

           function getStatus(ddl) {
               // var txt = document.getElementById(id).innerText
               // document.getElementById('<%= divNote.ClientID %>').visible = false;
            if ((ddl.options(ddl.selectedIndex).value) == 2) {
                document.getElementById('<%= divNote.ClientID %>').visible = true;

                document.getElementById('<%= divNote.ClientID %>').addClass("msgInfoBox msgInfoError")

            }
        }



</script>

</head>
<body>
    <form id="form1" runat="server" >
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <img src="Images/GEMS-pdp-banner_2014_15.png" class="img-responsive" />
        </div>

        <section class="col-sm-12 col-md-12 col-lg-12">

            <div class="header" onclick="showHide();">

                <span id="ColExpCss" class="ExpClass"></span><span id="empName" runat="server" enableviewstate="true"></span>
                <span id="empDesig" runat="server" enableviewstate="true"></span>
                <span id="spRev" runat="server" class="PlanningPhaseCss">Reviewing Phase</span>

            </div>
            <div class="contentPanel panelHeader panel-body">
                  <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Name:</span>
   <input type="text" id="txt_EmpName" runat="server" class="form-control" readonly="true"/>
                      
                    </div>
                </div>
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Designation:</span>
                          <input type="text" id="txt_role" runat="server" class="form-control" readonly="true"/>
                       </div>
                </div>
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">School:</span>
                              <input type="text" id="txt_bsu" runat="server" class="form-control" readonly="true" style="font-size:11px;"/>
                                          </div>
                </div>
                 <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                     <span class="input-group-addon">Reviewing Manager:</span>
                            <input type="text" id="txt_ManagerName" runat="server" class="form-control" readonly="true" />
                      
                    </div>
                </div>
                   <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                            <span class="input-group-addon">Review Period From:</span>
                             <input type="text" id="txt_FromDate" runat="server" class="form-control" readonly="true"/>
                                                </div>
                    </div>
                    <div  class="form-group col-md-4 col-lg-4">
                        <div class="input-group">
                            <span class="input-group-addon">Review Period To:</span>
                            
                              <input type="text" id="txt_ToDate" runat="server" class="form-control" readonly="true"/>
                        </div>
                    </div>
                </div>

                      
                    </section>
        <section class="col-sm-12 col-md-12 col-lg-12">
<div id="divAlert" runat="server" class="divinfoInner"><font color="red"><marquee behavior="alternate">Please click on ‘Save As Draft’ often to save your inputs</marquee></font></div>
</section>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <section class="col-md-11 col-lg-11 col-sm-11 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                    <div class="row">
                        <asp:LinkButton ID="lbtnStep1" runat="server" CssClass="">
                 <div class="cssStepNo" >1</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP ONE</div>   <div class="cssStepInfo" id="">Objectives & Interim Review</div>     
                 </div>
                    
                
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep2" runat="server" CssClass="">
                 <div class="cssStepNo" >2</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP TWO</div>   <div class="cssStepInfo">Career Aspirations & Development</div>     
                 </div>
                    
                
                        </asp:LinkButton>
                        
                        <asp:LinkButton ID="lbtnStep5" runat="server" CssClass="" Enabled="false">
                 <div class="cssStepNo" >3</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP THREE</div>   <div class="cssStepInfo">Final Review</div>     
                 </div>
                    
                
                        </asp:LinkButton>
                       
                    </div>
                </section>

 <section class="col-md-12 col-lg-12 col-sm-12 colContainerBox">

       <div id="divNote" runat="server"  title="Click on the message box to drag it up and down" visible="false" ClientIDMode="Static"><span class="msgInfoclose"></span>
                                       
                        <asp:Label ID="lblError" runat="server" EnableViewState="false"></asp:Label>
                   
                </div>
     <div runat="server" id="tb_IntermReview" class="table-responsive" style="width:100%;" ClientIDMode="Static" >
                   
                  <asp:Repeater ID="rptInterim" runat="server">
                <ItemTemplate>
                    <asp:HiddenField ID="hfKRA_IDInterim" runat="server" Value='<%# Eval("KRA_ID")%>'></asp:HiddenField>
<asp:HiddenField ID="hfKRA_MIN_REQDInterim" runat="server" Value='<%# Eval("KRA_MIN_REQD")%>'></asp:HiddenField>
                    <asp:HiddenField ID="hfKRA_DESCRInterim"  runat="server" Value='<%# Eval("KRA_HDR_DESCR")%>'></asp:HiddenField>
                    <asp:HiddenField ID="hfTab_idInterim"  runat="server" Value='<%# Eval("TabId")%>'></asp:HiddenField>
                  <h2 class="acc_Header" id='<%# Eval("TabId")%>'><span class="glyphicon glyphicon-plus"></span> <%# Eval("KRA_HDR_DESCR")%> <span class="tabMandatorycss"><%# Eval("REQUIRED_FIELD")%></span></h2>

                <div class="acc_container">

                        <asp:Repeater ID="rptPerf_IndicatorInterim" runat="server">
                            <HeaderTemplate>
                                <table class="table">

                                   <thead> <tr><th style="text-align:center;">
                                   <span style="border-bottom:1px solid #003a62;">Key Performance Indicator</span></th>
                                   <th  style="text-align:center;"><span style="border-bottom:1px solid #003a62;">Performance Objectives 2015/16</span></th>
                                   <th style="text-align:center;"><span style="border-bottom:1px solid #003a62;">Review status</span></th><th style="text-align:center;"><span style="border-bottom:1px solid #003a62;">Enter Interim review comments</span></th></tr></thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                              <tr id="tr1" runat="server"><td style="width: 25%;padding:8px 8px 8px 4px;color:#2fa4e7;font-size:14px;"><asp:label ID="lblDescr" runat="server" ><%# Eval("KPI_DESCR")%></asp:label></td>
                                    <td style="width: 35%;padding:8px 8px 8px 4px;"> <textarea class="form-control" rows="3" id="txt_KPIObjectiveInterim" runat="server" 
                                     draggable="false"     
                                        enableviewstate="true" readonly="true"><%# Eval("EPD_OBJECTIVES")%></textarea>
                                        <asp:HiddenField ID="hfKPI_IDInterim" runat="server" Value='<%# Eval("KPI_ID")%>' />
                      

                                    </td>
                                    <td style="width: 10%;padding:8px 8px 8px 4px;">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="input-sm dropdown dropDownCss"   
                                     ClientIDMode="Static" 
                                     text='<%#Eval("EPD_INTERIM_STATUS")%>'>
                        <asp:ListItem Value="0" Text="Select Status"></asp:ListItem>
                         <asp:ListItem Value="1" Text="On Target"></asp:ListItem>
                         <asp:ListItem Value="2" Text="Goal Change"></asp:ListItem>
                         <asp:ListItem Value="3" Text="At Risk"></asp:ListItem>
                                                             </asp:DropDownList>
                                    </td>
                                    <td style="width: 30%;padding:8px 8px 8px 4px;">
                                     <textarea class="form-control" rows="3" id="txt_InterimComment" runat="server" 
                                     draggable="false"     
                                        enableviewstate="true"><%#Eval("EPD_INTERIM_COMMENT")%></textarea></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv_txtInterimComment" 
           ControlToValidate="txt_InterimComment"   Display="Dynamic"
                                        ValidationGroup="Valids" runat="server" CssClass="errValidator" ForeColor="red"  visible="false"></asp:RequiredFieldValidator>
                                    </td>
                               </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                </div>
                </ItemTemplate>
            </asp:Repeater> 
                    
</div>

                <div runat="server" id="tb_ObjectivesCurrent" class="table-responsive" style="width:100%;" ClientIDMode="Static" visible="false" >
 
          <asp:Repeater ID="rptKRA_M" runat="server">
                <ItemTemplate>
                    <asp:HiddenField ID="hfKRA_ID" runat="server" Value='<%# Eval("KRA_ID")%>'></asp:HiddenField>
<asp:HiddenField ID="hfKRA_MIN_REQD" runat="server" Value='<%# Eval("KRA_MIN_REQD")%>'></asp:HiddenField>
                    <asp:HiddenField ID="hfKRA_DESCR"  runat="server" Value='<%# Eval("KRA_HDR_DESCR")%>'></asp:HiddenField>
                    <asp:HiddenField ID="hfTab_id"  runat="server" Value='<%# Eval("TabId")%>'></asp:HiddenField>
                  <h2 class="acc_Header" id='<%# Eval("TabId")%>'><span class="glyphicon glyphicon-plus"></span> <%# Eval("KRA_HDR_DESCR")%> <span class="tabMandatorycss"><%# Eval("REQUIRED_FIELD")%></span></h2>

                <div class="acc_container">

                        <asp:Repeater ID="rptPerf_Indicator" runat="server">
                            <HeaderTemplate>
                                <table class="table">

                                   <thead> <tr><th style="width: 40%;text-align:center;"><span style="border-bottom:1px solid #003a62;">Key Performance Indicator</span></th><th  style="width: 50%;text-align:center;"><span style="border-bottom:1px solid #003a62;">Enter Performance Objectives 2014/15</span></th></tr></thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                              <tr><td style="width: 40%;padding:8px 8px 8px 4px;color:#2fa4e7;font-size:16px;"><%# Eval("KPI_DESCR")%></td>
                                    <td style="width: 50%;padding:8px 8px 8px 4px;"> <textarea class="form-control" rows="3" id="txt_KPIObjective" runat="server" 
                                     draggable="false"   readonly="readonly"  
                                        enableviewstate="true"><%# Eval("EPD_OBJECTIVES")%></textarea>
                                        <asp:HiddenField ID="hfKPI_ID" runat="server" Value='<%# Eval("KPI_ID")%>' />
                      

                                    </td>
                               </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                </div>
                </ItemTemplate>
            </asp:Repeater>
                   
     
                </div>
                <div runat="server" id="tb_CareerAspirations">
                    <div class="wellCustom  col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                        &nbsp;
                    </div>

                    <section class="col-md-6 col-lg-6 col-sm-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                              Career Aspirations
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="8" id="txt_CarrierInput"  runat="server" placeholder="Enter your Career Aspirations" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"  ></textarea>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="col-md-6 col-lg-6 col-sm-6">
                        <div class="panel  panel-info">
                            <div class="panel-heading">
                                Professional Development Needs
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="8" id="txt_ProfDevelNeeds"  runat="server" placeholder="Enter your Professional Development Needs" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>

                    <div class="row" visible="false">
                            <div class="form-group col-md-5 col-lg-5 col-sm-5" visible="false">

                                <div class="input-group" visible="false">
                                    <span class="input-group-addon label-primary">Employee Signed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                     <input type="text" id="txtEmpSigned" runat="server" class="form-control" readonly="true"/>
                                  
                                </div>
                            </div>
                            <div class="form-group col-md-2 col-lg-2 col-sm-2">
                                <div class="input-group">
                                    <span class="input-group-addon label-primary">Date</span>
                                   
                                     <input type="text" id="txtEmpDate" runat="server" class="form-control" readonly="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="row" visible="false">
                            <div class="form-group  col-md-5 col-lg-5 col-sm-5">

                                <div class="input-group">
                                    <span class="input-group-addon label-primary">Manager Signed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
  <input type="text" id="txtMgrSigned1" runat="server" class="form-control" readonly="true"/>
                                

                                </div>


                            </div>
                            <div class="form-group col-md-2 col-lg-2 col-sm-2">
                                <div class="input-group">
                                    <span class="input-group-addon label-primary">Date</span>
                                 
                                     <input type="text" id="txtMgrDate1" runat="server" class="form-control" readonly="true"/>
                                </div>
                            </div>
                        </div>
                 
                


                  
                </div>


               

                <div runat="server" id="tb_summary">
                    <div class="wellCustom  col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                        &nbsp;
                    </div>

                                      
                   

                </div>

                   

     </section>
                <div class="form-group col-md-12 col-lg-12 col-sm-12">
                    <span id="divMandatory" runat="server" class="MandatoryCss" enableviewstate="false"></span>
                    <div class="pull-right">
                      <asp:LinkButton ID="btn_Previous" runat="server" CssClass="buttonNext"  Width="133px" Height="27px" Visible="false" > Previous
                                                    </asp:LinkButton>
                        <asp:LinkButton ID="btn_SaveDraft" runat="server" CssClass="buttonDraft"  Width="133px" Height="27px" > Save As Draft
                                                    </asp:LinkButton>
                        <asp:LinkButton ID="btn_Next" runat="server" CssClass="buttonNext"  Width="133px" Height="27px" > Save & Next
                                                    </asp:LinkButton>
                        <asp:LinkButton ID="btn_SaveFinish" runat="server" CssClass="buttonFinish"  Width="133px" Height="27px"   Visible="false"> Submit
                                                    </asp:LinkButton>
             
                        <ajaxToolkit:ConfirmButtonExtender ID="cbeConfirmPDP" runat="server"
                            TargetControlID="btn_SaveFinish"
                            ConfirmText="Are you sure you want to submit the GEMS Interim Review form to the next reviewer?" />

                       <ajaxToolkit:ConfirmButtonExtender ID="cbeRevert" runat="server"
                            TargetControlID="btn_next"
                            ConfirmText="Are you sure you want to Revert Back?" enabled="false" />

                    </div>

                </div>
                <section class="col-md-12 col-lg-12 col-sm-12">
                    <div class="footerStyle">
                        <center> <span  class="input-group"><img src="favicon.ico" style="height:15px;width:15px; vertical-align:top;" ></img> &nbsp; Powered by GEMS OASIS &nbsp; <img src="favicon.ico" style="height:15px;width:15px; vertical-align:top;"></img></span></center>
                    </div>
                </section>
                  <div id="Panel_RevertBack" runat="server" class="darkPanelM" visible="false">
                    <div class="darkPanelMTop">
                        <div class="holderInner" style="overflow:scroll" >

                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td valign="top" style="padding-left:20px;" >
                                        <asp:Label ID="lblerrormsg" runat="server" EnableViewState="false" Text="Select  Employee">
                                        </asp:Label>
                                        
                                    </td>
                                    </tr>
                                <tr>
                                    <td valign="top" style="padding-left:20px;"> 
                                        <asp:RadioButtonList ID="rbtnList_Rievewer" runat="server">
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="rfv_Rievewer" runat="server" ErrorMessage="Select the Rievewer" ControlToValidate="rbtnList_Rievewer" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <td width="80%"> <textarea class="form-control" rows="3" id="txt_Remarks" runat="server" placeholder="Remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"></textarea>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Remarks Required" ControlToValidate="txt_Remarks" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btn_Revert" runat="server" Text="Revert" CssClass="buttonSave" Height="26px" Width="80px" ValidationGroup="SaveRevertBack"  CausesValidation="true" />
                                         <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server"
                            TargetControlID="btn_Revert" 
                            ConfirmText="Are you sure you want to revert the PDP form to the selected employee?" />
                                          <asp:Button ID="btn_Cancel" runat="server" Text="Cancel"  Height="26px" Width="80px" ToolTip="Click here to cancel and close"  CssClass="buttonCancel" />
                                   <asp:Repeater ID="rptPDP_Reveiwer" runat="server">
                            <HeaderTemplate>
                                <table style="width: 100%;" border="0" class="table">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="tbldbImgCss">
                                        <asp:HiddenField ID="hfID" runat="server" Value='<%# Eval("EMP_ID")%>' />
                                        <img src='<%# Eval("EMD_PHOTO")%>' class="img-thumbnail img-responsive" alt='<%# Eval("EMP_NAME")%>' style="min-height: 120px; height: 120px; width: 110px; min-width: 110px;" />                                                                     
                                    </td>
                                    <td class="tbldbContentcss">
                                        <div class="divDbHeadercss">
                                            <span class="spDbEnamecss"><%# Eval("EMP_NAME")%></span>
                                            <span class="spDbDesign"><%# Eval("DES_DESCR")%></span><span class="spDbDept"><%# Eval("DPT_DESCR")%></span>
                                           
                                        </div>                                                                     
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater> 
<asp:HiddenField ID="hdnEpr" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="darkPanelFooter">
                            <span class="TitlePl">Revert Back</span>
                        </div>
                    </div>
                </div>

                <asp:HiddenField ID="hfCurrTabid"  runat="Server"    />     
  <asp:HiddenField ID="hf_Interim"  runat="Server"    />    

                       <div id="myModal" class="modal fade" data-backdrop="static" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="HideModal()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    
                    <section class="col-md-12 col-lg-12 col-sm-12">    



                    </section>
</div>
                
                    <div class="modal-footer"> 
                     <asp:Button ID="btnSaveChanges" runat="server"
                         CssClass="buttonFinish" Text="Save changes" Width="136px" Height="27px"  />
<div id="divlogNote" runat="server" class="divinfoInner">
    <div id="divErrorLog" runat="server" ></div>
   
                                </div>
                </div>

        
    </div>
            </div></div>




            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progBgFilter_Show" runat="server"></div>
                <div id="processMessage" class="progMsg_Show">
                    <img alt="Loading..." src="Images/Loading.gif" /><br />
                    <br />
                    Loading Please Wait...
                </div>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="progressBackgroundFilter"
                    VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                    HorizontalOffset="10">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </form>

       <script type="text/javascript"  src="../PDP/Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../PDP/Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript"  src="../PDP/Scripts/bootstrap.min.js"></script>
    <script>



        $(document).ready(function () {


            $(".msgInfoBox").draggable({ axis: "y" });

            $(".msgInfoclose").click(function (e) {
                $(this).parent().fadeTo(300, 0, function () {
                    $(this).remove();
                });
                e.preventDefault();
            });


            if ($('#hfCurrTabid').length > 0) {

                var CurrTabId = $('#hfCurrTabid').val();
                $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                $('.acc_container').hide();
                $("#" + CurrTabId).addClass('active').next().show();

                $('.glyphicon', "#" + CurrTabId).removeClass("glyphicon-plus").addClass("glyphicon-minus");
            }


            ////Hide/close all containers
            // $('.acc_Header:first').addClass('active').next().show();




            $('.acc_Header').click(function (e) {
                var maxHeight = 0;
                var cpHeight = 0;
                var minusHeight = 30;
                var ids = '';
                $content = $(".contentPanel");
                tab_id = e.target.id;

                if ($(this).next().is(':hidden')) { //If immediate next container is closed...
                    $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                    $('.acc_Header').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
                    $(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
                    $('.glyphicon', this).removeClass("glyphicon-plus").addClass("glyphicon-minus");
                    $('#hfCurrTabid').val(tab_id);

                    if ($content.is(":visible") == true) {

                        cpHeight = $content.outerHeight();
                        minusHeight = 115;
                    }


                    $(".acc_Header").each(function () {
                        ids = this.id;
                        maxHeight += $(this).outerHeight();

                        if (ids == tab_id) {
                            return false;
                        }

                    });


                    //                $('html, body').animate({
                    //                    scrollTop: ($('#tb_ObjectivesCurrent').offset().top + maxHeight + cpHeight - minusHeight)
                    //                }, 1000);

                    $('html, body').animate({
                        scrollTop: ($('#tb_IntermReview').offset().top + maxHeight + cpHeight - minusHeight)
                    }, 1000);

                }
                e.preventDefault(); //Prevent the browser jump to the link anchor
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {

            $(".msgInfoBox").draggable({ axis: "y" });

            $(".msgInfoclose").click(function (e) {
                $(this).parent().fadeTo(300, 0, function () {

                    $(this).remove();
                });
                e.preventDefault();
            });

            if ($('#hfCurrTabid').length > 0) {
                var CurrTabId = $('#hfCurrTabid').val();
                $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                $('.acc_container').hide();
                $("#" + CurrTabId).addClass('active').next().show();

                $('.glyphicon', "#" + CurrTabId).removeClass("glyphicon-plus").addClass("glyphicon-minus");

            }


            //$('.acc_container').hide(); //Hide/close all containers
            //$('.acc_Header:first').addClass('active').next().show();

            $('.acc_Header').click(function (e) {
                var maxHeight = 0;
                var cpHeight = 0;
                var minusHeight = 30;
                var ids = '';
                $content = $(".contentPanel");
                tab_id = e.target.id;

                if ($(this).next().is(':hidden')) { //If immediate next container is closed...
                    $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                    $('.acc_Header').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
                    $(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
                    $('.glyphicon', this).removeClass("glyphicon-plus").addClass("glyphicon-minus");
                    $('#hfCurrTabid').val(tab_id);

                    if ($content.is(":visible") == true) {

                        cpHeight = $content.outerHeight();
                        minusHeight = 115;
                    }



                    $(".acc_Header").each(function () {
                        ids = this.id;
                        maxHeight += $(this).outerHeight();
                        if (ids == tab_id) {
                            return false;
                        }

                    });
                    //                          $('html, body').animate({
                    //                              scrollTop: ($('#tb_ObjectivesCurrent').offset().top + maxHeight + cpHeight - minusHeight)
                    //                        }, 1000);

                    $('html, body').animate({
                        scrollTop: ($('#tb_IntermReview').offset().top + maxHeight + cpHeight - minusHeight)
                    }, 1000);
                }
                e.preventDefault(); //Prevent the browser jump to the link anchor

            });
        });



    </script>
</body>
</html>
