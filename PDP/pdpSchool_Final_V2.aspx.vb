﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Telerik.Web.UI
Partial Class PDP_pdpSchool_Final_V2
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim TAB_MANDATORY As New Hashtable
    Dim TAB_TITLE As New Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            lbtnStep1.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep2.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep3.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep4.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep5.Attributes.Add("class", "cssStepBtnActive col-md-2 col-lg-2 col-sm-2")
            tb_ObjectivesCurrent.Visible = False     'TAB-1
            tb_CareerAspirations.Visible = False    'TAB-3
            tb_summary.Visible = False              'TAB-5
            tb_IntermReview.Visible = False         'TAB-6
            tb_FinalReview.Visible = True
            tb_summary_Final.Visible = False
            ViewState("EMP_ID") = ""
            ViewState("EPR_ID") = ""
            ViewState("RVW_EMP_ID") = ""
            ViewState("PBS_ID") = ""
            ViewState("CYCL_ID") = ""
            ViewState("REVIEW_LEVEL") = ""
            ViewState("PDP_BSU_ID") = ""
            ViewState("SAVE_DRAFT") = "NO"
            If Not (Request.QueryString("U") Is Nothing) Then
                ViewState("EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("U").Replace(" ", "+"))
            End If
            'EMPLOYEE REVIEW MASTER ID
            If Not (Request.QueryString("ER") Is Nothing) Then
                ViewState("EPR_ID") = Encr_decrData.Decrypt(Request.QueryString("ER").Replace(" ", "+"))
            End If
            'EMPLOYEE REVIEWER
            If Not (Request.QueryString("MR") Is Nothing) Then
                ViewState("RVW_EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("MR").Replace(" ", "+"))
            End If
            'CURRENT REVIEW STAGE FOR BSU 
            If Not (Request.QueryString("BR") Is Nothing) Then
                ViewState("PBS_ID") = Encr_decrData.Decrypt(Request.QueryString("BR").Replace(" ", "+"))
            End If
            'REVIEWING YEAR
            If Not (Request.QueryString("RY") Is Nothing) Then
                ViewState("CYCL_ID") = Encr_decrData.Decrypt(Request.QueryString("RY").Replace(" ", "+"))
            End If
            '@OPTION='LVL0' -- 0 -> Level 0 1 -> Reviewer LEVEL
            If Not (Request.QueryString("RL") Is Nothing) Then
                ViewState("REVIEW_LEVEL") = Encr_decrData.Decrypt(Request.QueryString("RL").Replace(" ", "+"))
            End If
            'REVIEW AGAINST WHICH BSU 
            If Not (Request.QueryString("B") Is Nothing) Then
                ViewState("PDP_BSU_ID") = Encr_decrData.Decrypt(Request.QueryString("B").Replace(" ", "+"))
            End If
            'Session("sUsr_name") = "prem.sunder"
            'ViewState("EMP_ID") = "5639"
            'ViewState("EPR_ID") = "1"
            'ViewState("RVW_EMP_ID") = "9219"
            'ViewState("PBS_ID") = "1"
            'ViewState("CYCL_ID") = "1"
            'ViewState("REVIEW_LEVEL") = "LVL1"
            'ViewState("PDP_BSU_ID") = "151001"

            ViewState("REVIEW_LEVEL") = "LVL" + GetEmployeeLevel()
            If ViewState("EMP_ID") = "" Or ViewState("EPR_ID") = "" Or _
                ViewState("RVW_EMP_ID") = "" Or ViewState("PBS_ID") = "" _
                Or ViewState("CYCL_ID") = "" Or ViewState("REVIEW_LEVEL") = "" Or ViewState("PDP_BSU_ID") = "" Then
                'INVALID LOGIN
            Else
                Session("MIN_KPI_REQUIRED") = Nothing

                Intital_Data_Bind()
                BIND_PERF_INDICATOR()
                BIND_CAREER_DEVEL()
                readonly_textbox()
                Session("MIN_KPI_REQUIRED") = TAB_MANDATORY
                Session("KPI_TITLE") = TAB_TITLE
                Dim MSG As String = String.Empty
                Dim txtReadonly As String = String.Empty
                Dim txtReadonlyIntrim As String = String.Empty
                ' txtReadonly = CHECK_GET_OBJ(MSG)
                txtReadonlyIntrim = CHECK_GET_INTRIM_FINAL(MSG)
                Dim StatusObjective As String = ""
                ' Dim XML_OBJ As String = CHECK_GET_INTRIM(StatusObjective)

                lbtnStep1.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))


                lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))

                lbtnStep3.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
                hfCurrTabid.Value = "tab_1"
                check_published()
            End If
        End If
    End Sub

    Private Sub Intital_Data_Bind()
        ViewState("DISABLE_TEXTBOX") = "NO"
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PRI.INIT_INTERIM_DATA", pParms)
            While reader.Read()
                empName.InnerText = Convert.ToString(reader("EMP_NAME"))
                txt_EmpName.Value = Convert.ToString(reader("EMP_NAME"))
                txt_bsu.Value = Convert.ToString(reader("BSU_NAME"))
                txt_ManagerName.Value = Convert.ToString(reader("EMP_MANAGER_NAME"))
                ' txtEmpSigned.Value = Convert.ToString(reader("EMP_NAME"))
                'txtMgrSigned1.Value = Convert.ToString(reader("EMP_MANAGER_NAME"))
                txt_role.Value = Convert.ToString(reader("EMP_DES"))
                empDesig.InnerText = Convert.ToString(reader("EMP_DES"))
                txt_FromDate.Value = Convert.ToString(reader("CYCLE_STARTDATE"))
                txt_ToDate.Value = Convert.ToString(reader("CYCLE_ENDDATE"))
                ViewState("CAREER_REQUIRED") = Convert.ToString(reader("CAREER_REQUIRED"))
                ViewState("DEVELOPMENT_REQUIRED") = Convert.ToString(reader("DEVELOPMENT_REQUIRED"))
                ViewState("COMPLETED_LEVEL0") = Convert.ToString(reader("LVL0_COMPL"))
                ViewState("COMPLETED_LEVEL1") = Convert.ToString(reader("LVL1_COMPL"))

                ViewState("DISABLE_FINAL_SAVE") = Convert.ToString(reader("DISABLE_FINAL_SAVE"))
                ViewState("EPR_bPUBLISHED") = Convert.ToString(reader("EPR_bPUBLISHED"))
                'txtEmpDate.Value = Convert.ToString(reader("LVL1_FINISH_DT"))
                ' txtMgrDate1.Value = Convert.ToString(reader("LVL2_FINISH_DT"))

                Bind_GetPDP_FinalCompletedDate()
                Bind_Summary()

                If ViewState("REVIEW_LEVEL") = "LVL0" Then 'With  principal(level 0) login-and if level 0 is completed
                    btn_Next.Visible = False
                    btn_Previous.Visible = False
                    btn_SaveDraft.Visible = True
                    btn_SubmitDraft.Visible = False
                    btn_SaveFinish.Visible = True
                    cbeConfirmPDP.Enabled = True
                    btnRevert.Visible = False
                    divAlert.Visible = True
                    txt_mgrOverallSummary.Attributes.Add("readonly", "readonly")
                    hmeOverall.Enabled = False
                    divOverallRating.Attributes.Add("class", "divRatingDisable")

                    If ViewState("COMPLETED_LEVEL0") = 1 Then
                        btn_Next.Visible = False
                        btn_Previous.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SaveFinish.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SubmitDraft.Visible = False
                        divAlert.Visible = False
                        ViewState("DISABLE_TEXTBOX") = "YES"
                    End If
                ElseIf ViewState("REVIEW_LEVEL") = "LVL1" Then  'With  reviewer (level 1) login-and if level 1 is completed
                    cbeConfirmPDP.Enabled = False
                    btn_SubmitDraft.Visible = False
                    btn_Next.Visible = True
                    btn_Previous.Visible = True
                    divAlert.Visible = True
                    If ViewState("COMPLETED_LEVEL0") = 0 Then
                        btn_Next.Visible = False
                        btn_Previous.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SaveFinish.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SubmitDraft.Visible = False
                        btnRevert.Visible = False
                        divAlert.Visible = False
                        ViewState("DISABLE_TEXTBOX") = "YES"

                    ElseIf ViewState("COMPLETED_LEVEL1") = 1 Then
                        btn_Next.Visible = False
                        btn_Previous.Visible = False
                        btn_SaveDraft.Visible = False
                        btn_SaveFinish.Visible = False
                        divAlert.Visible = False
                        ViewState("DISABLE_TEXTBOX") = "YES"
                        '  btnRevert.Visible = False
                        'ElseIf ViewState("COMPLETED_LEVEL0") <> 1 Then
                        '    btn_Next.Visible = True
                        '    btn_Previous.Visible = True
                        '    btn_SaveDraft.Visible = False
                        '    btn_SaveFinish.Visible = False
                        '    ViewState("DISABLE_TEXTBOX") = "YES"
                    Else

                        ' btn_Next.Visible = True
                        btn_Next.Text = "Next"
                        btn_Previous.Visible = True
                        btn_SaveDraft.Visible = True
                        divAlert.Visible = True
                        btn_SaveFinish.Text = "Approved"
                        btn_SaveFinish.Visible = False
                        btnRevert.Visible = True
                        ViewState("DISABLE_TEXTBOX") = "YES"
                    End If
                Else 'With  logined in 100 -and disable all
                    btnRevert.Visible = False
                    btn_SubmitDraft.Visible = False
                    btn_Final.Visible = False
                    btn_Next.Visible = False
                    btn_Previous.Visible = False
                    btn_SaveDraft.Visible = False
                    btn_SaveFinish.Visible = False
                    divAlert.Visible = False
                    ViewState("DISABLE_TEXTBOX") = "YES"
                End If

            End While
        End Using

        If ViewState("DISABLE_TEXTBOX") = "YES" Then
            txt_CarrierInput.Attributes.Add("readonly", "readonly")
            txt_ProfDevelNeeds.Attributes.Add("readonly", "readonly")

        End If


        txt_mgrOverallSummary.Attributes.Add("readonly", "readonly")
        hmeOverall.Enabled = False
        divOverallRating.Attributes.Add("class", "divRatingDisable")

       

        If ViewState("REVIEW_LEVEL") = "LVL1" Then
            If ViewState("COMPLETED_LEVEL0") = 0 Then
                txt_mgrOverallSummary.Attributes.Add("readonly", "readonly")
                hmeOverall.Enabled = False
                divOverallRating.Attributes.Add("class", "divRatingDisable")

            ElseIf ViewState("COMPLETED_LEVEL1") = 0 Then

                txt_mgrOverallSummary.Attributes.Remove("readonly")
                hmeOverall.Enabled = True
                divOverallRating.Attributes.Remove("class")


            End If

        End If

    End Sub

    Private Sub BIND_PERF_INDICATOR()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        pParms(7) = New SqlClient.SqlParameter("@INFO_TYPE", "CAT")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[PRI].[INIT_OBJECTIVES]", pParms)
        rptKRA_M.DataSource = ds
        rptKRA_M.DataBind()

        rptInterim.DataSource = ds
        rptInterim.DataBind()


        rptFinalReview.DataSource = ds
        rptFinalReview.DataBind()

    End Sub

    Protected Sub rptKRA_M_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptKRA_M.ItemDataBound
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        pParms(7) = New SqlClient.SqlParameter("@INFO_TYPE", "DETAIL")

        Dim hfKRA_ID As New HiddenField
        Dim hfKRA_MIN_REQD As New HiddenField
        Dim hfKRA_DESCR As New HiddenField
        Dim hfTab_id As New HiddenField

        Dim rptPerf_Indicator As New Repeater
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            hfKRA_ID = DirectCast(e.Item.FindControl("hfKRA_ID"), HiddenField)
            hfKRA_MIN_REQD = DirectCast(e.Item.FindControl("hfKRA_MIN_REQD"), HiddenField)
            hfKRA_DESCR = DirectCast(e.Item.FindControl("hfKRA_DESCR"), HiddenField)
            hfTab_id = DirectCast(e.Item.FindControl("hfTab_id"), HiddenField)
            If hfCurrTabid.Value = "" Then
                hfCurrTabid.Value = hfTab_id.Value
            End If

            'TAB_MANDATORY.Add(hfKRA_ID.Value, hfKRA_MIN_REQD.Value)
            'TAB_TITLE.Add(hfKRA_ID.Value, hfKRA_DESCR.Value)

            rptPerf_Indicator = DirectCast(e.Item.FindControl("rptPerf_Indicator"), Repeater)
            pParms(8) = New SqlParameter("@KRA_ID", hfKRA_ID.Value)
            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[PRI].[INIT_OBJECTIVES]", pParms)
                rptPerf_Indicator.DataSource = datareader
                rptPerf_Indicator.DataBind()
            End Using

        End If

    End Sub
    Private Sub BIND_CAREER_DEVEL()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PRI.INIT_CAREER_DEVEL", pParms)
            While datareader.Read
                txt_CarrierInput.Value = Convert.ToString(datareader("EPR_CAREER_ASPR"))
                txt_ProfDevelNeeds.Value = Convert.ToString(datareader("EPR_PD_NEEDS"))

            End While
        End Using

    End Sub
    Protected Sub lbtnStep1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnStep1.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        lbtnStep3.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep5.Attributes.Add("class", IIf(CHECK_CAREER_DEVELOPMENT_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep1.Attributes.Add("class", "cssStepBtnActive")

        tb_ObjectivesCurrent.Visible = True    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-3
        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = False
        tb_FinalReview.Visible = False
        tb_summary_Final.Visible = False
        divStatus.Visible = False
        'TAB-6
        btn_Next.Text = "Next"
        btn_Previous.Visible = False
        btn_Next.Visible = True
        btn_SaveDraft.Visible = False
        btn_SaveFinish.Visible = False
        btn_SubmitDraft.Visible = False
        btnRevert.Visible = False
        ' VALIDATE_BUTTTON_RIGHTS()
    End Sub


    Protected Sub lbtnStep2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnStep2.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = True    'TAB-3
        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = False         'TAB-6
        tb_FinalReview.Visible = False
        tb_summary_Final.Visible = False
        divStatus.Visible = False

        Dim StatusObjective As String = String.Empty
        Dim StatusIntrim As String = String.Empty
        Dim XML_OBJ As String = CHECK_GET_OBJ(StatusObjective)
        Dim XML_INTRIM As String = CHECK_GET_INTRIM_FINAL(StatusIntrim)
        lbtnStep1.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", "cssStepBtnActive")
        lbtnStep3.Attributes.Add("class", IIf(StatusIntrim = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep5.Attributes.Add("class", IIf(StatusIntrim = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))

        btn_Next.Text = "Next"
        btn_Next.Visible = False
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = False
        btn_Next.Visible = True
        btn_SaveDraft.Visible = False
        btn_SubmitDraft.Visible = False
        btnRevert.Visible = False
        'VALIDATE_BUTTTON_RIGHTS()
    End Sub
    Private Sub VALIDATE_BUTTTON_RIGHTS()
        'If ViewState("REVIEW_LEVEL") = "LVL0" Then 'With  principal(level 0) login-and if level 0 is completed

        '    cbeConfirmPDP.Enabled = True
        '    If ViewState("COMPLETED_LEVEL0") = 1 Then
        '        btn_Next.Visible = False
        '        btn_Previous.Visible = False
        '        btn_SaveDraft.Visible = False
        '        btn_SaveFinish.Visible = False
        '        ViewState("DISABLE_TEXTBOX") = "YES"


        '    End If
        'ElseIf ViewState("REVIEW_LEVEL") = "LVL1" Then  'With  reviewer (level 1) login-and if level 1 is completed
        '    cbeConfirmPDP.Enabled = False

        '    If ViewState("COMPLETED_LEVEL1") = 1 Then
        '        btn_Next.Visible = False
        '        btn_Previous.Visible = False
        '        btn_SaveDraft.Visible = False
        '        btn_SaveFinish.Visible = False
        '        ViewState("DISABLE_TEXTBOX") = "YES"
        '    Else
        '        btn_Next.Visible = True
        '        btn_Next.Text = "Revert Back"
        '        btn_Previous.Visible = False
        '        btn_SaveDraft.Visible = False
        '        btn_SaveFinish.Text = "Approved"
        '        btn_SaveFinish.Visible = True
        '        ViewState("DISABLE_TEXTBOX") = "YES"
        '    End If
        'Else 'With  logined in 100 -and disable all

        '    btn_Next.Visible = False
        '    btn_Previous.Visible = False
        '    btn_SaveDraft.Visible = False
        '    btn_SaveFinish.Visible = False
        '    ViewState("DISABLE_TEXTBOX") = "YES"
        'End If


        btn_Final.Visible = False


        If ViewState("REVIEW_LEVEL") = "LVL0" Then 'With  principal(level 0) login-and if level 0 is completed
            btn_Next.Visible = False
            btn_Previous.Visible = False
            btn_SaveDraft.Visible = True
            btn_SubmitDraft.Visible = False
            btn_SaveFinish.Visible = True
            cbeConfirmPDP.Enabled = True



            If ViewState("COMPLETED_LEVEL0") = 1 Then
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                btn_SaveDraft.Visible = False
                btn_SubmitDraft.Visible = False
                ViewState("DISABLE_TEXTBOX") = "YES"
            End If
        ElseIf ViewState("REVIEW_LEVEL") = "LVL1" Then  'With  reviewer (level 1) login-and if level 1 is completed
            cbeConfirmPDP.Enabled = False
            btn_SubmitDraft.Visible = False
            btn_Next.Visible = True
            btn_Previous.Visible = True
            If ViewState("COMPLETED_LEVEL0") = 0 Then
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                btn_SaveDraft.Visible = False
                btn_SubmitDraft.Visible = False
                btnRevert.Visible = False
                ViewState("DISABLE_TEXTBOX") = "YES"
            ElseIf ViewState("COMPLETED_LEVEL1") = 1 Then
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                ViewState("DISABLE_TEXTBOX") = "YES"
                btn_Final.Visible = False
                'ElseIf ViewState("COMPLETED_LEVEL0") <> 1 Then
                '    btn_Next.Visible = True
                '    btn_Previous.Visible = True
                '    btn_SaveDraft.Visible = False
                '    btn_SaveFinish.Visible = False
                '    ViewState("DISABLE_TEXTBOX") = "YES"
            Else
                btnRevert.Visible = True
                ' btn_Next.Visible = True
                btn_Next.Text = "Next"
                btn_Previous.Visible = True
                btn_SaveDraft.Visible = True
                btn_SaveFinish.Text = "Approved"
                btn_SaveFinish.Visible = False
                btn_Final.Visible = True
                ViewState("DISABLE_TEXTBOX") = "YES"
            End If
        Else 'With  logined in 100 -and disable all
            btnRevert.Visible = False
            btn_SubmitDraft.Visible = False
            btn_Final.Visible = False
            btn_Next.Visible = False
            btn_Previous.Visible = False
            btn_SaveDraft.Visible = False
            btn_SaveFinish.Visible = False
            ViewState("DISABLE_TEXTBOX") = "YES"
        End If












    End Sub
    Protected Sub lbtnStep3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnStep3.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-3
        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = True         'TAB-6
        tb_FinalReview.Visible = False
        tb_summary_Final.Visible = False
        divStatus.Visible = False

        Dim StatusObjective As String = String.Empty

        Dim XML_OBJ As String = CHECK_GET_OBJ(StatusObjective)

        lbtnStep1.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))

        lbtnStep2.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep5.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", "cssStepBtnActive")

        btn_Next.Text = "Next"
        btn_Next.Visible = True
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = False
        btn_SaveDraft.Visible = False
        btn_SubmitDraft.Visible = False
        btnRevert.Visible = False
        ' VALIDATE_BUTTTON_RIGHTS()
       
    End Sub



    Protected Sub lbtnStep5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnStep5.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-3
        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = False         'TAB-6
        tb_FinalReview.Visible = True
        tb_summary_Final.Visible = False
        divStatus.Visible = True
        Dim StatusObjective As String = String.Empty

        '  Dim XML_OBJ As String = CHECK_GET_OBJ(StatusObjective)

        lbtnStep1.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))

        lbtnStep2.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep5.Attributes.Add("class", "cssStepBtnActive")

        btn_Next.Text = "Next"
        btn_Next.Visible = True
        btn_Previous.Visible = False
        btn_SaveFinish.Visible = True
        btn_SaveDraft.Visible = True
        btn_SubmitDraft.Visible = False
        VALIDATE_BUTTTON_RIGHTS()
        If ViewState("REVIEW_LEVEL") = "LVL1" Then
            btn_SaveFinish.Visible = False
            btn_Final.Visible = False
        End If

    End Sub
    Protected Sub lbtnStep4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnStep4.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-3
        tb_summary.Visible = False              'TAB-5
        tb_IntermReview.Visible = False         'TAB-6
        tb_FinalReview.Visible = False
        tb_summary_Final.Visible = True
        divStatus.Visible = False
        Dim StatusObjective As String = String.Empty

        '  Dim XML_OBJ As String = CHECK_GET_OBJ(StatusObjective)

        lbtnStep1.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))

        lbtnStep2.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep5.Attributes.Add("class", IIf(StatusObjective = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", "cssStepBtnActive")


        'If ViewState("REVIEW_LEVEL") = "LVL1" Then
        '    btn_Next.Visible = False
        '    btn_Previous.Visible = True
        '    btn_SaveFinish.Visible = False
        '    btn_SaveDraft.Visible = False
        '    btn_SubmitDraft.Visible = False
        '    If ViewState("COMPLETED_LEVEL0") = 1 Then
        '        btn_Final.Visible = True
        '    Else
        '        btn_Final.Visible = False
        '    End If
        '    btnRevert.Visible = False
        'Else
        '    btnRevert.Visible = False
        '    btn_Next.Visible = False
        '    btn_Previous.Visible = False
        '    btn_SaveFinish.Visible = False
        '    btn_SaveDraft.Visible = False
        '    btn_SubmitDraft.Visible = False
        '    btn_Final.Visible = False
        'End If
        VALIDATE_BUTTTON_RIGHTS()
        btn_Next.Visible = False
    End Sub


    Private Sub FINAL_REVERT(ByRef errorMessage As String, ByVal transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Panel_RevertBack.Visible = False

        Dim PARAM(12) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        PARAM(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", ViewState("EMP_ID"))
        PARAM(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        PARAM(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
        PARAM(4) = New SqlClient.SqlParameter("@URL", "")


        PARAM(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(5).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[PRI].[REVERT_BACKTO_REVIEWER_INTERIM_FINAL]", PARAM)
        ReturnFlag = PARAM(5).Value
        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        Else
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"


            lblError.Text = "<div>You have succesully reverted the Final review for  " & empName.InnerText & " </div>"

        End If


    End Sub


    Private Sub SAVE_FINAL_INTRIM(ByRef errorMessage As String, ByVal XML_OBJ As String, ByVal status As String, ByVal transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Dim PARAM(12) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        PARAM(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        PARAM(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        PARAM(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        PARAM(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        PARAM(5) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        PARAM(6) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        PARAM(7) = New SqlClient.SqlParameter("@STR_XML", "<KPI_M>" + XML_OBJ + "</KPI_M>")
        PARAM(8) = New SqlClient.SqlParameter("@STATUS", status)
        PARAM(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(9).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[PRI].[UPDATE_FINAL_XML]", PARAM)
        ReturnFlag = PARAM(9).Value
        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If


    End Sub
    Private Function SAVE_FINAL_DRAFT(ByRef errorMessage As String, ByVal XML_OBJ As String) As Integer
        Dim ReturnFlag As Integer
        Dim Transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            Transaction = conn.BeginTransaction("SampleTransaction")
            Try


                Dim PARAM(12) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
                PARAM(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
                PARAM(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
                PARAM(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                PARAM(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
                PARAM(5) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
                PARAM(6) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
                PARAM(7) = New SqlClient.SqlParameter("@STR_XML", "<KPI_M>" + XML_OBJ + "</KPI_M>")
                PARAM(8) = New SqlClient.SqlParameter("@STATUS", "COMPLETED")
                PARAM(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(9).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "[PRI].[UPDATE_FINAL_XML_DRAFT]", PARAM)
                ReturnFlag = PARAM(9).Value
                If ReturnFlag <> 0 Then
                    errorMessage = "-1"
                End If

            Catch ex As Exception
                ReturnFlag = 1
                errorMessage = "Error Occured While Saving."
            Finally
                If ReturnFlag = 1 Then

                    errorMessage = "Error Occured While Saving."
                    Transaction.Rollback()
                Else
                    errorMessage = ""
                    Transaction.Commit()
                End If
            End Try

        End Using
        Return ReturnFlag





    End Function
    Private Function SAVE_FINAL(ByRef errorMessage As String, ByVal XML_OBJ As String) As Integer
        Dim ReturnFlag As Integer
        Dim Transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            Transaction = conn.BeginTransaction("SampleTransaction")
            Try


                Dim PARAM(12) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
                PARAM(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
                PARAM(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
                PARAM(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                PARAM(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
                PARAM(5) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
                PARAM(6) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
                PARAM(7) = New SqlClient.SqlParameter("@STR_XML", "<KPI_M>" + XML_OBJ + "</KPI_M>")
                PARAM(8) = New SqlClient.SqlParameter("@STATUS", "COMPLETED")
                PARAM(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(9).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "[PRI].[UPDATE_FINAL_XML]", PARAM)
                ReturnFlag = PARAM(9).Value
                If ReturnFlag <> 0 Then
                    errorMessage = "-1"
                End If

            Catch ex As Exception
                ReturnFlag = 1
                errorMessage = "Error Occured While Saving."
            Finally
                If ReturnFlag = 1 Then

                    errorMessage = "Error Occured While Saving."
                    Transaction.Rollback()
                Else
                    errorMessage = ""
                    Transaction.Commit()
                End If
            End Try

        End Using
        Return ReturnFlag





    End Function


    Private Sub SAVE_OBJECTIVE(ByRef errorMessage As String, ByVal XML_OBJ As String, ByVal transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Dim PARAM(12) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        PARAM(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        PARAM(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        PARAM(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        PARAM(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        PARAM(5) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        PARAM(6) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        PARAM(7) = New SqlClient.SqlParameter("@STR_XML", "<KPI_M>" + XML_OBJ + "</KPI_M>")
        PARAM(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[PRI].[SAVEINIT_OBJECTIVES_XML]", PARAM)
        ReturnFlag = PARAM(8).Value
        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If


    End Sub
    Private Sub SAVE_CAREER_DEVELOPMENT(ByRef errorMessage As String, ByVal transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Dim PARAM(14) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        PARAM(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        PARAM(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        PARAM(3) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        PARAM(4) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        PARAM(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        PARAM(6) = New SqlClient.SqlParameter("@EPR_CAREER_ASPR", txt_CarrierInput.InnerText.Trim())
        PARAM(7) = New SqlClient.SqlParameter("@EPR_PD_NEEDS", txt_ProfDevelNeeds.InnerText.Trim())
        PARAM(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[PRI].[SAVEINIT_CAREER_DEVELOPMENT]", PARAM)
        ReturnFlag = PARAM(8).Value

        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If

    End Sub
    Sub readonly_textbox()
        Dim STR_XML As New StringBuilder

       
        Dim KPI_OBJ_TXT As String = String.Empty
        Dim KPI_CMNT_TXT As String = String.Empty
        Dim MGR_CMNT_TXT As String = String.Empty
        Dim KPI_SUBMIT As String = String.Empty

   
        Dim str_app As String = String.Empty
        Dim rptPerf_Indicator As Repeater
        Dim objTxtBox As HtmlTextArea
        Dim cmtTxtBox As HtmlTextArea
        Dim mgrTxtBox As HtmlTextArea
        Dim ddlStatus As DropDownList

    

        For Each objItem As RepeaterItem In rptFinalReview.Items
            rptPerf_Indicator = DirectCast(objItem.FindControl("rptPerf_IndicatorFinal"), Repeater)

           


            For Each objChildItem As RepeaterItem In rptPerf_Indicator.Items
        
                objTxtBox = DirectCast(objChildItem.FindControl("txt_KPIObjectiveFinal"), HtmlTextArea)
                cmtTxtBox = DirectCast(objChildItem.FindControl("txt_FinalReviewComment"), HtmlTextArea)
                mgrTxtBox = DirectCast(objChildItem.FindControl("txt_LineManagerComment"), HtmlTextArea)
                ddlStatus = DirectCast(objChildItem.FindControl("ddlFinalStatus"), DropDownList)
               
                ddlStatus.Enabled = False
                mgrTxtBox.Attributes.Add("readonly", "readonly")
                cmtTxtBox.Attributes.Add("readonly", "readonly")

                If ViewState("REVIEW_LEVEL") = "LVL0" Then
                    mgrTxtBox.Attributes.Remove("readonly")
                    cmtTxtBox.Attributes.Remove("readonly")
                    If objTxtBox.InnerText = "" Then
                        ddlStatus.Enabled = False
                    Else
                        ddlStatus.Enabled = True
                    End If

                    If ViewState("COMPLETED_LEVEL0") = 1 Then

                        ddlStatus.Enabled = False
                        cmtTxtBox.Attributes.Add("readonly", "readonly")


                    End If
                    mgrTxtBox.Attributes.Add("readonly", "readonly")

                End If

                If ViewState("REVIEW_LEVEL") = "LVL1" Then
                    mgrTxtBox.Attributes.Remove("readonly")
                    cmtTxtBox.Attributes.Remove("readonly")
                    If objTxtBox.InnerText = "" Then
                        ddlStatus.Enabled = False
                    Else
                        ddlStatus.Enabled = True
                    End If
                    If ViewState("COMPLETED_LEVEL1") = 1 Then

                        ddlStatus.Enabled = False
                        mgrTxtBox.Attributes.Add("readonly", "readonly")


                    End If
                    cmtTxtBox.Attributes.Add("readonly", "readonly")
                    If ViewState("COMPLETED_LEVEL0") = 0 Then
                        ddlStatus.Enabled = False
                        mgrTxtBox.Attributes.Add("readonly", "readonly")
                        cmtTxtBox.Attributes.Add("readonly", "readonly")
                    End If
                End If


            Next

        Next


    End Sub
    Private Function CHECK_GET_INTRIM_FINAL(ByRef errormsg As String) As String

        Dim STR_XML As New StringBuilder

        Dim KPI_ID As String = String.Empty
        Dim KRA_ID As String = String.Empty
        Dim KPI_ID_COMPLETED As Integer
        Dim KPI_OBJ_TXT As String = String.Empty
        Dim KPI_CMNT_TXT As String = String.Empty
        Dim MGR_CMNT_TXT As String = String.Empty
        Dim KPI_SUBMIT As String = String.Empty

        Dim TAB_CHECK As New Hashtable
        Dim MANDATORY_FIELDS_EXISTS As String = "NO"
        Dim TOTAL_OBJ_ENTRY_DONE As Integer
        Dim str_app As String = String.Empty
        Dim rptPerf_Indicator As Repeater
        Dim objTxtBox As HtmlTextArea
        Dim cmtTxtBox As HtmlTextArea
        Dim mgrTxtBox As HtmlTextArea
        Dim ddlStatus As DropDownList
        Dim hfKPI_ID As HiddenField
        Dim hfKRA_ID As HiddenField
        Dim hfTab_id As HiddenField
        Dim hfKRA_DESCR As HiddenField
        Dim hdn_Submit As HiddenField
        Dim tr As Control
        Dim lblDescr As Label
        Dim TAB_MANDATORY_comp As New Hashtable
        TAB_MANDATORY_comp = Session("MIN_KPI_REQUIRED")
        Dim TAB_TITLE_msg As New Hashtable
        TAB_TITLE_msg = Session("KPI_TITLE")
        KPI_ID_COMPLETED = 0

        For Each objItem As RepeaterItem In rptFinalReview.Items
            rptPerf_Indicator = DirectCast(objItem.FindControl("rptPerf_IndicatorFinal"), Repeater)

            hfKRA_ID = DirectCast(objItem.FindControl("hfKRA_IDFinal"), HiddenField)
            hfTab_id = DirectCast(objItem.FindControl("hfTab_idFinal"), HiddenField)
            hfKRA_DESCR = DirectCast(objItem.FindControl("hfKRA_DESCRFinal"), HiddenField)
            hfCurrTabid.Value = hfTab_id.Value


            For Each objChildItem As RepeaterItem In rptPerf_Indicator.Items
                lblDescr = DirectCast(objChildItem.FindControl("lblDescr"), Label)
                tr = DirectCast(objChildItem.FindControl("tr1"), Control)
                objTxtBox = DirectCast(objChildItem.FindControl("txt_KPIObjectiveFinal"), HtmlTextArea)
                cmtTxtBox = DirectCast(objChildItem.FindControl("txt_FinalReviewComment"), HtmlTextArea)
                mgrTxtBox = DirectCast(objChildItem.FindControl("txt_LineManagerComment"), HtmlTextArea)
                ddlStatus = DirectCast(objChildItem.FindControl("ddlFinalStatus"), DropDownList)
                hfKPI_ID = DirectCast(objChildItem.FindControl("hfKPI_IDFinal"), HiddenField)
                hdn_Submit = DirectCast(objChildItem.FindControl("hdn_Submit"), HiddenField)
                KPI_OBJ_TXT = objTxtBox.Value.ToString.Trim.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                KPI_CMNT_TXT = cmtTxtBox.Value.ToString.Trim.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                MGR_CMNT_TXT = mgrTxtBox.Value.ToString.Trim.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")
                KPI_SUBMIT = hdn_Submit.Value
                'If KPI_OBJ_TXT = "" Then
                '    ' tr.Visible = False
                '    'lblDescr.Visible = False
                '    ' ddlStatus.Visible = False
                '    cmtTxtBox.Visible = False
                '    objTxtBox.Visible = False

                'End If
                Dim chk As Integer = 1


                'If (((ddlStatus.SelectedValue = "4")) And (ViewState("FINAL") = "1")) Then
                '    errormsg = "Review cannot be save  when status is'' " + ddlStatus.SelectedItem.Text + "''"
                '    errormsg += " in  " + hfKRA_DESCR.Value + " tab"
                '    Return ""
                '    Exit Function
                'End If

                'If (((ddlStatus.SelectedValue = "5")) And (ViewState("REVIEW_LEVEL") = "LVL1") And (ViewState("FINAL") = "1")) Then
                '    errormsg = "Review cannot be save  when status is'' " + ddlStatus.SelectedItem.Text + "''"
                '    errormsg += " in  " + hfKRA_DESCR.Value + " tab"
                '    Return ""
                '    Exit Function
                'End If

                If ViewState("REVIEW_LEVEL") = "LVL0" Then
                    If ViewState("COMPLETED_LEVEL0") = 1 Then
                        'If ((KPI_SUBMIT = 1) And ((ddlStatus.SelectedValue = "0") Or (ddlStatus.SelectedValue = "1") Or (ddlStatus.SelectedValue = "2") Or (ddlStatus.SelectedValue = "3") Or (ddlStatus.SelectedValue = "4"))) Then

                        ddlStatus.Enabled = False
                        cmtTxtBox.Attributes.Add("readonly", "readonly")
                        chk = 0

                    End If
                    mgrTxtBox.Attributes.Add("readonly", "readonly")
                    'ddlStatus.Enabled = False
                End If

                If ViewState("REVIEW_LEVEL") = "LVL1" Then
                    If ViewState("COMPLETED_LEVEL1") = 1 Then
                        ' If (KPI_SUBMIT = 0) Then
                        ddlStatus.Enabled = False
                        mgrTxtBox.Attributes.Add("readonly", "readonly")


                    End If
                    cmtTxtBox.Attributes.Add("readonly", "readonly")
                End If

                If ((ddlStatus.SelectedValue = "3") And (KPI_CMNT_TXT = "") And (ViewState("REVIEW_LEVEL") = "LVL0") And (ViewState("SAVE_DRAFT") <> "YES")) Then
                    errormsg = "Comment cannot be left empty when status is ''" + ddlStatus.SelectedItem.Text + "''"
                    errormsg += " in  " + hfKRA_DESCR.Value + " tab"
                    Return ""
                    Exit Function
                End If

                If ((ViewState("SUBMIT") = "YES") And ((ddlStatus.SelectedValue = "0") Or (ddlStatus.SelectedValue = "1") Or (ddlStatus.SelectedValue = "2") Or (ddlStatus.SelectedValue = "3") Or (ddlStatus.SelectedValue = "4"))) Then
                    KPI_SUBMIT = 1
                ElseIf ((ViewState("SUBMIT") = "NO") And (chk = 0)) Then
                    KPI_SUBMIT = 1

                End If







                STR_XML.Append(String.Format("<KPI KRA_ID='{0}' KPI_ID='{1}' KPI_OBJ_TXT ='{2}' KPI_STATUS='{3}' KPI_CMNT_TXT='{4}' KPI_MGR_TXT='{5}' KPI_SUBMIT='{6}'/>", hfKRA_ID.Value, _
              hfKPI_ID.Value, KPI_OBJ_TXT, ddlStatus.SelectedValue, KPI_CMNT_TXT, MGR_CMNT_TXT, KPI_SUBMIT))
                If ddlStatus.SelectedValue <> "0" Then
                    KPI_ID_COMPLETED += 1

                End If
                If KPI_OBJ_TXT <> "" Then
                    TOTAL_OBJ_ENTRY_DONE += 1
                End If
            Next
            'If TAB_MANDATORY_comp(hfKRA_ID.Value) <> 0 Then
            '    MANDATORY_FIELDS_EXISTS = "YES"
            '    If KPI_ID_COMPLETED < TAB_MANDATORY_comp(hfKRA_ID.Value) Then
            '        errormsg += "<div>Minimum Objectives Required For " + TAB_TITLE_msg(hfKRA_ID.Value) + " : " + TAB_MANDATORY_comp(hfKRA_ID.Value) + "</div>"
            '    End If
            'End If
        Next

        If ((TOTAL_OBJ_ENTRY_DONE > KPI_ID_COMPLETED) And (ViewState("SAVE_DRAFT") = "NO")) Then
            errormsg = "NOT COMPLETE"
        End If
        'If (TOTAL_OBJ_ENTRY_DONE < 1) And (MANDATORY_FIELDS_EXISTS = "NO") Then
        '    errormsg = "NOENTRY"
        'End If

        Return STR_XML.ToString()
    End Function




    Private Function CHECK_GET_OBJ(ByRef errormsg As String) As String

        Dim STR_XML As New StringBuilder

        Dim KPI_ID As String = String.Empty
        Dim KRA_ID As String = String.Empty
        Dim KPI_ID_COMPLETED As Integer
        Dim KPI_OBJ_TXT As String = String.Empty
        Dim TAB_CHECK As New Hashtable
        Dim MANDATORY_FIELDS_EXISTS As String = "NO"
        Dim TOTAL_OBJ_ENTRY_DONE As Integer
        Dim str_app As String = String.Empty
        Dim rptPerf_Indicator As Repeater
        Dim objTxtBox As HtmlTextArea
        Dim hfKPI_ID As HiddenField
        Dim hfKRA_ID As HiddenField
        Dim TAB_MANDATORY_comp As New Hashtable
        TAB_MANDATORY_comp = Session("MIN_KPI_REQUIRED")
        Dim TAB_TITLE_msg As New Hashtable
        TAB_TITLE_msg = Session("KPI_TITLE")

        For Each objItem As RepeaterItem In rptKRA_M.Items
            rptPerf_Indicator = DirectCast(objItem.FindControl("rptPerf_Indicator"), Repeater)
            KPI_ID_COMPLETED = 0
            hfKRA_ID = DirectCast(objItem.FindControl("hfKRA_ID"), HiddenField)
            For Each objChildItem As RepeaterItem In rptPerf_Indicator.Items
                objTxtBox = DirectCast(objChildItem.FindControl("txt_KPIObjective"), HtmlTextArea)
                hfKPI_ID = DirectCast(objChildItem.FindControl("hfKPI_ID"), HiddenField)
                KPI_OBJ_TXT = objTxtBox.Value.ToString.Trim.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")

                If ViewState("DISABLE_TEXTBOX") = "YES" Then
                    objTxtBox.Attributes.Add("readonly", "readonly")
                End If

                STR_XML.Append(String.Format("<KPI KRA_ID='{0}' KPI_ID='{1}' KPI_OBJ_TXT ='{2}'/>", hfKRA_ID.Value, _
              hfKPI_ID.Value, KPI_OBJ_TXT))
                If KPI_OBJ_TXT <> "" Then
                    KPI_ID_COMPLETED += 1
                    TOTAL_OBJ_ENTRY_DONE += 1
                End If
            Next
            'If TAB_MANDATORY_comp(hfKRA_ID.Value) <> 0 Then
            '    MANDATORY_FIELDS_EXISTS = "YES"
            '    If KPI_ID_COMPLETED < TAB_MANDATORY_comp(hfKRA_ID.Value) Then
            '        errormsg += "<div>Minimum Objectives Required For " + TAB_TITLE_msg(hfKRA_ID.Value) + " : " + TAB_MANDATORY_comp(hfKRA_ID.Value) + "</div>"
            '    End If
            'End If
        Next
        If (TOTAL_OBJ_ENTRY_DONE < 1) And (MANDATORY_FIELDS_EXISTS = "NO") Then
            errormsg = "NOENTRY"
        End If

        Return STR_XML.ToString()
    End Function
    Private Function CHECK_CAREER_DEVELOPMENT_COMPLETED() As String
        Dim errormsg As String = String.Empty
        If Convert.ToInt16(ViewState("CAREER_REQUIRED")) <> 0 Then

            If (txt_CarrierInput.InnerText.Trim() = "") Then
                errormsg += "<div>Enter Your Career Aspirations</div>"
            End If
        End If
        If Convert.ToInt16(ViewState("DEVELOPMENT_REQUIRED")) <> 0 Then
            If (txt_ProfDevelNeeds.InnerText.Trim() = "") Then
                errormsg += "<div>Enter your Professional Development Needs</div>"
            End If
        End If


        Return errormsg
    End Function

    Protected Sub btn_SaveDraft_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_SaveDraft.Click
        ViewState("FINAL") = "0"


        If Page.IsValid Then

            Dim XML_INTRIM As String = String.Empty
            Dim transaction As SqlTransaction
            Dim ERRORMSG As String = String.Empty
            Dim Status As Integer = 0
            Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
                transaction = conn.BeginTransaction("SampleTransaction")

                Try
                    ViewState("SAVE_DRAFT") = "YES"
                    ViewState("SUBMIT") = "NO"
                    XML_INTRIM = CHECK_GET_INTRIM_FINAL(ERRORMSG)
                    If ERRORMSG <> "" Then
                        divNote.Visible = True
                        divNote.Attributes("class") = "msgInfoBox msgInfoError"
                        lblError.Text = ERRORMSG

                    Else
                        Status = SAVE_FINAL_DRAFT(ERRORMSG, XML_INTRIM)
                        If ViewState("REVIEW_LEVEL") = "LVL1" Then
                            Dim overallrating As String = HF_OverallRating.Value
                            Dim ovarallsumm As String = txt_mgrOverallSummary.InnerText.Trim


                            HF_OverallRating.Value = overallrating
                            txt_mgrOverallSummary.InnerText = ovarallsumm
                            Save_OverallSummary(transaction)
                        End If
                        If ERRORMSG <> "-1" Then
                            transaction.Commit()
                        End If
                        If Status <> 0 Then
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoError"
                            lblError.Text = ERRORMSG
                        Else

                            Intital_Data_Bind()
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                            lblError.Text = "<div>You have successfully saved the Final Review </div>"
                            'If ERRORMSG <> "-1" Then
                            '    SAVE_CAREER_DEVELOPMENT(ERRORMSG, transaction)
                        End If
                    End If
                Catch ex As Exception
                    ERRORMSG = "-1"
                    transaction.Rollback()

                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    lblError.Text = "<div>Error occured while saving </div>"



                End Try
            End Using
        Else
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Error occured while saving </div>"
        End If




    End Sub
    Protected Sub btn_Previous_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Previous.Click
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                If tb_CareerAspirations.Visible = True Then 'TAB-2
                    lbtnStep1_Click(lbtnStep1, Nothing)
                    'SAVE_CAREER_DEVELOPMENT(ERRORMSG, transaction)
                    txt_EmpName.Focus()
                End If
                If tb_IntermReview.Visible = True Then
                    lbtnStep2_Click(lbtnStep2, Nothing)
                    txt_EmpName.Focus()
                End If
                If tb_FinalReview.Visible = True Then
                    lbtnStep3_Click(lbtnStep3, Nothing)
                    txt_EmpName.Focus()
                End If
                If tb_summary_Final.Visible = True Then

                    lbtnStep5_Click(lbtnStep5, Nothing)
                    Save_OverallSummary(transaction)
                    Bind_Summary()
                    txt_EmpName.Focus()
                End If



            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using


    End Sub
    Protected Sub btn_Next_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Next.Click
        ViewState("FINAL") = "0"
        Dim XML_OBJ As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try
                If btn_Next.Text = "Revert Back" Then
                    'cbeRevert.Enabled = True
                    'INTRIM_REVERT(ERRORMSG, transaction)


                ElseIf tb_ObjectivesCurrent.Visible = True Then 'TAB-1
                    cbeRevert.Enabled = False
                    lbtnStep2_Click(lbtnStep2, Nothing)
                    txt_EmpName.Focus()
                    'XML_OBJ = CHECK_GET_OBJ(ERRORMSG)
                    'SAVE_OBJECTIVE(ERRORMSG, XML_OBJ, transaction)

                ElseIf tb_CareerAspirations.Visible = True Then
                    cbeRevert.Enabled = False
                    lbtnStep3_Click(lbtnStep3, Nothing)
                    txt_EmpName.Focus()
                ElseIf tb_IntermReview.Visible = True Then
                    lbtnStep5_Click(lbtnStep5, Nothing)
                    txt_EmpName.Focus()
                ElseIf tb_FinalReview.Visible = True Then
                    lbtnStep4_Click(lbtnStep4, Nothing)
                    txt_EmpName.Focus()
                End If
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using



    End Sub

    Protected Sub btn_SaveFinish_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_SaveFinish.Click
        ViewState("SAVE_DRAFT") = "NO"
        ViewState("FINAL") = "1"
        ViewState("SUBMIT") = "YES"
        Dim errormsg As String = String.Empty
        Dim Status As Integer = 0
        'Dim XML_OBJ As String = CHECK_GET_OBJ(errormsg)
        'Dim CAREER_DEVEL_MSG As String = CHECK_CAREER_DEVELOPMENT_COMPLETED()
        Dim XML_INTRIM As String = CHECK_GET_INTRIM_FINAL(errormsg)
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            If errormsg = "NOENTRY" Then
                errormsg = "<div>Minimum One Objectives Required Against Any Key Result Area </div>"
            End If



            If errormsg = "NO COMPLETE" Then
                errormsg = "<div>Should select the Final Review Status against all filled objectives </div>"
            End If
            If errormsg = "NOT COMPLETE" Then
                errormsg = "<div> Final Review status not updated for 1 or more KPI. Please check </div>"
            End If
            If errormsg <> "" Then
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                lblError.Text = errormsg
            Else


                Status = SAVE_FINAL(errormsg, XML_INTRIM)

                If Status <> 0 Then
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    lblError.Text = errormsg
                Else
                    If ViewState("FINAL_S") = "1" Then
                        Exit Sub
                    End If
                    Intital_Data_Bind()
                    Dim MSG As String = String.Empty
                    Dim txtReadonly As String = String.Empty
                    txtReadonly = CHECK_GET_INTRIM_FINAL(MSG)

                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"

                    If ViewState("REVIEW_LEVEL") = "LVL0" Then
                        lblError.Text = "<div>You have successfully completed the Final review. </div>"
                    ElseIf ViewState("REVIEW_LEVEL") = "LVL1" Then
                        lblError.Text = "<div>You have succesully reviewed the Final review for  " & empName.InnerText & " </div>"
                    End If

                End If

            End If
        End Using
    End Sub
    Private Function SAVE_FINAL_KPI(ByRef errorMessage As String) As Integer

        Dim ReturnFlag As Integer
        Dim Transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            Transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim PARAM(14) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
                PARAM(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
                PARAM(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                PARAM(3) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
                PARAM(4) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
                PARAM(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
                PARAM(6) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))

                PARAM(7) = New SqlClient.SqlParameter("@ASPIRE", txt_CarrierInput.InnerText.Trim())
                PARAM(8) = New SqlClient.SqlParameter("@PDNEEDS", txt_ProfDevelNeeds.InnerText.Trim())
                PARAM(9) = New SqlClient.SqlParameter("@FINAL", "1")
                PARAM(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(10).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "[PRI].[FINAL_SAVE]", PARAM)
                ReturnFlag = PARAM(10).Value

                If ReturnFlag <> 0 Then
                    ReturnFlag = 1
                End If

            Catch ex As Exception
                ReturnFlag = 1
                errorMessage = "Error Occured While Saving."
            Finally
                If ReturnFlag = 1 Then

                    errorMessage = "Error Occured While Saving."
                    Transaction.Rollback()
                Else
                    errorMessage = ""
                    Transaction.Commit()
                End If
            End Try

        End Using
        Return ReturnFlag

    End Function

    Private Sub Save_OverallSummary(ByVal sqltran As SqlTransaction)
        Dim pParms(8) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(1) = New SqlClient.SqlParameter("@EPR_FINAL_RTM_ID", HF_OverallRating.Value)
        pParms(2) = New SqlClient.SqlParameter("@EPR_FINAL_SUMMARY", txt_mgrOverallSummary.InnerText.Trim())

        pParms(3) = New SqlClient.SqlParameter("@EMP_ID", ViewState("RVW_EMP_ID"))
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PRI.SAVE_SUMMARY_FINAL", pParms)

    End Sub


    Protected Sub rptInterim_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInterim.ItemDataBound
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        pParms(7) = New SqlClient.SqlParameter("@INFO_TYPE", "DETAIL")

        Dim hfKRA_IDInterim As New HiddenField
        Dim hfKRA_MIN_REQDInterim As New HiddenField
        Dim hfKRA_DESCRInterim As New HiddenField
        Dim hfTab_idInterim As New HiddenField

        Dim rptPerf_IndicatorInterim As New Repeater
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            hfKRA_IDInterim = DirectCast(e.Item.FindControl("hfKRA_IDInterim"), HiddenField)
            hfKRA_MIN_REQDInterim = DirectCast(e.Item.FindControl("hfKRA_MIN_REQDInterim"), HiddenField)
            hfKRA_DESCRInterim = DirectCast(e.Item.FindControl("hfKRA_DESCRInterim"), HiddenField)
            hfTab_idInterim = DirectCast(e.Item.FindControl("hfTab_idInterim"), HiddenField)
            If hfCurrTabid.Value = "" Then
                hfCurrTabid.Value = hfTab_idInterim.Value
            End If

            'TAB_MANDATORY.Add(hfKRA_IDInterim.Value, hfKRA_MIN_REQDInterim.Value)
            'TAB_TITLE.Add(hfKRA_IDInterim.Value, hfKRA_DESCRInterim.Value)

            rptPerf_IndicatorInterim = DirectCast(e.Item.FindControl("rptPerf_IndicatorInterim"), Repeater)
            pParms(8) = New SqlParameter("@KRA_ID", hfKRA_IDInterim.Value)
            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[PRI].[INIT_OBJECTIVES]", pParms)
                rptPerf_IndicatorInterim.DataSource = datareader
                rptPerf_IndicatorInterim.DataBind()
            End Using

        End If
    End Sub
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim XML_INTRIM As String = String.Empty
        Dim ERRORMSG As String = String.Empty

        divNote.Visible = False
        Try
            Dim ddl As DropDownList = DirectCast(sender, DropDownList)
            Dim gr As Repeater = DirectCast(ddl.Parent.Parent, Repeater)
            Dim i As Integer = Convert.ToInt32(hf_Interim.Value) - 1

            'Dim it As RepeaterItem = DirectCast(gr.Items(i), RepeaterItem)

            'Dim txt As RequiredFieldValidator
            'txt = it.FindControl("rfv_txtInterimComment")


            If ddl.SelectedValue = "2" Then

                XML_INTRIM = CHECK_GET_INTRIM_FINAL(ERRORMSG)
                If ERRORMSG <> "" Then
                    Dim s As String = hfCurrTabid.Value
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    lblError.Text = ERRORMSG + s
                End If
                'txt.Visible = True
                'txt.Validate()
                'divNote.Visible = True
                'divNote.Attributes("class") = "msgInfoBox msgInfoError"
                'lblError.Text = "Comment cannot be left empty when status At Risk"
            Else
                divNote.Visible = False
                'txt.Visible = False

            End If

        Catch ex As Exception


        End Try


    End Sub

    Protected Sub rptFinalReview_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFinalReview.ItemDataBound
        Dim conn As String = ConnectionManger.GetOASIS_PDP_PRINCIPALConnectionString

        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ViewState("PDP_BSU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@CYCL_ID", ViewState("CYCL_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(3) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(4) = New SqlClient.SqlParameter("@REVWR_EMP_ID", ViewState("RVW_EMP_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PBS_ID", ViewState("PBS_ID"))
        pParms(6) = New SqlClient.SqlParameter("@OPTION", ViewState("REVIEW_LEVEL"))
        pParms(7) = New SqlClient.SqlParameter("@INFO_TYPE", "DETAIL")

        Dim hfKRA_IDFinal As New HiddenField
        Dim hfKRA_MIN_REQDFinal As New HiddenField
        Dim hfKRA_DESCRFinal As New HiddenField
        Dim hfTab_idFinal As New HiddenField

        Dim rptPerf_IndicatorFinal As New Repeater
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            hfKRA_IDFinal = DirectCast(e.Item.FindControl("hfKRA_IDFinal"), HiddenField)
            hfKRA_MIN_REQDFinal = DirectCast(e.Item.FindControl("hfKRA_MIN_REQDFinal"), HiddenField)
            hfKRA_DESCRFinal = DirectCast(e.Item.FindControl("hfKRA_DESCRFinal"), HiddenField)
            hfTab_idFinal = DirectCast(e.Item.FindControl("hfTab_idFinal"), HiddenField)
            If hfCurrTabid.Value = "" Then
                hfCurrTabid.Value = hfTab_idFinal.Value
            End If

            'TAB_MANDATORY.Add(hfKRA_IDInterim.Value, hfKRA_MIN_REQDInterim.Value)
            'TAB_TITLE.Add(hfKRA_IDInterim.Value, hfKRA_DESCRInterim.Value)

            rptPerf_IndicatorFinal = DirectCast(e.Item.FindControl("rptPerf_IndicatorFinal"), Repeater)
            pParms(8) = New SqlParameter("@KRA_ID", hfKRA_IDFinal.Value)
            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[PRI].[INIT_OBJECTIVES]", pParms)
                rptPerf_IndicatorFinal.DataSource = datareader
                rptPerf_IndicatorFinal.DataBind()
            End Using

        End If
    End Sub

    Protected Sub btn_SubmitDraft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SubmitDraft.Click
        ViewState("FINAL") = "0"
        If Page.IsValid Then

            Dim XML_INTRIM As String = String.Empty
            Dim transaction As SqlTransaction
            Dim ERRORMSG As String = String.Empty
            Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
                transaction = conn.BeginTransaction("SampleTransaction")

                Try
                    ViewState("SAVE_DRAFT") = "NO"
                    ViewState("SUBMIT") = "YES"
                    XML_INTRIM = CHECK_GET_INTRIM_FINAL(ERRORMSG)

                    If ERRORMSG = "NO COMPLETE" Then
                        ERRORMSG = "<div>Should select the Final Review Status against all filled objectives </div>"
                    End If

                    If ERRORMSG <> "" Then
                        divNote.Visible = True
                        divNote.Attributes("class") = "msgInfoBox msgInfoError"
                        lblError.Text = ERRORMSG

                    Else
                        SAVE_FINAL_INTRIM(ERRORMSG, XML_INTRIM, "SUBMIT", transaction)
                        If ERRORMSG <> "-1" Then
                            transaction.Commit()

                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                            lblError.Text = "<div>You have successfully saved the Final Review </div>"
                            'If ERRORMSG <> "-1" Then
                            '    SAVE_CAREER_DEVELOPMENT(ERRORMSG, transaction)
                        End If
                    End If
                Catch ex As Exception
                    ERRORMSG = "-1"
                    transaction.Rollback()

                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    lblError.Text = "<div>Error occured while saving </div>"



                End Try
            End Using
        Else
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Error occured while saving </div>"
        End If
    End Sub

    Protected Sub btn_Final_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Final.Click

        If Page.IsValid Then

            Dim XML_INTRIM As String = String.Empty
            Dim transaction As SqlTransaction
            Dim ERRORMSG As String = String.Empty
            Dim Status As Integer = 0
            Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
                transaction = conn.BeginTransaction("SampleTransaction")

                Try
                    ViewState("FINAL") = "1"
                    ViewState("FINAL_S") = "1"
                    XML_INTRIM = CHECK_GET_INTRIM_FINAL(ERRORMSG)

                    Dim overallrating As String = HF_OverallRating.Value
                    Dim ovarallsumm As String = txt_mgrOverallSummary.InnerText.Trim
                    '' Dim fff As String = txt_mgrOverallSummary.InnerHtml.Trim


                    HF_OverallRating.Value = overallrating
                    txt_mgrOverallSummary.InnerText = ovarallsumm


                    If (txt_mgrOverallSummary.InnerText.Trim() = "" Or HF_OverallRating.Value = "") Then

                        ERRORMSG = "<div> Overall Summary and Rating required.</div>"

                    End If

                    If ERRORMSG <> "" Then
                        divNote.Visible = True
                        divNote.Attributes("class") = "msgInfoBox msgInfoError"
                        lblError.Text = ERRORMSG

                    Else




                        btn_SaveFinish_Click(btn_SaveFinish, Nothing)

                        Save_OverallSummary(transaction)



                        If ERRORMSG <> "-1" Then
                            transaction.Commit()
                            Intital_Data_Bind()
                            btn_Final.Visible = False
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                            lblError.Text = "<div>You have successfully approved the Final Review</div>"
                            'If ERRORMSG <> "-1" Then
                            '    SAVE_CAREER_DEVELOPMENT(ERRORMSG, transaction)
                        End If
                    End If
                Catch ex As Exception
                    ERRORMSG = "-1"
                    transaction.Rollback()

                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    lblError.Text = "<div>Error occured while saving </div>"



                End Try
            End Using
        Else
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Error occured while saving </div>"
        End If
    End Sub

    Protected Sub btnRevert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRevert.Click
        Try
            divNote.Visible = False
            Panel_RevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_PRINCIPAL_ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", ViewState("RVW_EMP_ID"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_RIEVEWERS_TOREVERT", pParms)
                rbtnList_Rievewer.DataSource = ds
                rbtnList_Rievewer.DataTextField = "EMP_NAME"
                rbtnList_Rievewer.DataValueField = "EMP_ID"
                rbtnList_Rievewer.DataBind()
                rbtnList_Rievewer.Items(0).Selected = True
                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_RevertBack.Visible = False

        End Try








    End Sub
    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancel.Click
        Panel_RevertBack.Visible = False
        rbtnList_Rievewer.ClearSelection()
        txt_Remarks.InnerText = ""
        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
    End Sub
    Private Sub Bind_GetPDP_FinalCompletedDate()
        Try
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_PRINCIPAL_ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

                Using reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "PRI.GET_FINAL_COMPLETED_DATE", pParms)
                    While reader.Read()
                        If (Convert.ToString(reader("EPL_LVL_ORDER")) = "0") Then
                            txtEmpSigned.Value = Convert.ToString(reader("EMP_NAME"))
                            txtEmpDate.Value = Convert.ToString(reader("EPL_LVL_FINAL_FINISH_DATE"))
                        ElseIf (Convert.ToString(reader("EPL_LVL_ORDER")) = "1") Then
                            txtMgrSigned1.Value = Convert.ToString(reader("EMP_NAME"))
                            txtMgrDate1.Value = Convert.ToString(reader("EPL_LVL_FINAL_FINISH_DATE"))

                        End If
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Bind_Summary()
        Dim msg As String = String.Empty
        Try

            Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_PRINCIPAL_ConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))



            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "PRI.GET_EMP_FINAL_SUMMARY", pParms)
                While DATAREADER.Read
                    txt_mgrOverallSummary.InnerText = Convert.ToString(DATAREADER("EPR_FINAL_SUMMARY"))


                    If (Convert.ToString(DATAREADER("EPR_FINAL_RTM_ID")) <> "") Then
                        SpOverallRatingWatermark.InnerText = ""
                    End If
                    HF_OverallRating.Value = Convert.ToString(DATAREADER("EPR_FINAL_RTM_ID"))
                    SpOverallRatingNo.InnerText = Convert.ToString(DATAREADER("EPR_FINAL_RTM_ID"))

                End While
            End Using


        Catch ex As Exception
            msg = ex.Message
        End Try

    End Sub

    Protected Sub btn_Revert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Revert.Click
        ViewState("FINAL") = "0"

        Dim XML_OBJ As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                cbeRevert.Enabled = True
                ' Panel_RevertBack.Visible = True
                FINAL_REVERT(ERRORMSG, transaction)

                Intital_Data_Bind()


            Catch ex As Exception
                ERRORMSG = "-1"
                Panel_RevertBack.Visible = False
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using


    End Sub
    Public Function GetEmployeeLevel() As String
        Dim EmpLevel As String = String.Empty

        Try
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            EmpLevel = SqlHelper.ExecuteScalar(connStr, _
            CommandType.StoredProcedure, "PRI.GET_EMP_LEVEL", pParms)
            Return EmpLevel
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmployeeLevel")
        End Try
    End Function
    Sub check_published()
        If ViewState("REVIEW_LEVEL") = "LVL0" And ViewState("EPR_bPUBLISHED") = "False" Then
            txt_mgrOverallSummary.InnerText = ""
            SpOverallRatingNo.InnerText = ""
        End If
    End Sub
End Class
