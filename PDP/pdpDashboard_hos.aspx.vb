﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports Telerik.Web.UI
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO
Partial Class pdpDashboard_hos
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        '  ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnPDF_D)
        'Session("sUsr_name") = "manoj"
        'Session("sUsr_name") = "prem.sunder"
        'Session("sUsr_name") = "ROBBIE"
        ''Session("sUsr_name") = "margaret.atack"
        ' Session("sUsr_name") = "SRIVALSAN.M_OOF"
        '  Session("sUsr_name") = "F.COTTAM_jcd"
        'Session("sUsr_name") = "ANINDA.C_OOS"
        'Session("EmployeeId") = "9219"
        ' GETCURRENT_YEAR()
        btnPublish.Visible = False
        GETCURRENT_YEAR_nEW()
        If Session("EmployeeId") <> "" Then
            If Page.IsPostBack = False Then
                'Session("sUsr_name") = "prem.sunder"
                'Bind_Department()
                bindBusinessUnits()
                Bind_Levels()
                Bind_Level1()
                Bind_Level2()
                Bind_Level3()
                Bind_Level4()
                Bind_Level5()
                GETCURRENT_YEAR()
                'gridbind()

            End If
        Else
            Response.Redirect("../login.aspx")
        End If
        Dim ToolkitScriptManager1 As AjaxControlToolkit.ToolkitScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me.Page)
        ToolkitScriptManager1.RegisterPostBackControl(btnExport)




    End Sub
    Private Sub Bind_Department()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDPConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        '   pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP.GET_DEPARTEMENTS", pParms)

        ddlDept.DataSource = ds
        ddlDept.DataValueField = "DPT_ID"
        ddlDept.DataTextField = "DPT_DESCR"
        ddlDept.DataBind()
        ddlDept.Items.Insert(0, New ListItem("-- ALL --", "0"))
    End Sub
    Private Sub Bind_Levels()
        ddlPending.Items.Insert(0, New ListItem("-- Select --", ""))
        ddlPending.Items.Insert(1, New ListItem("Employee", "0"))
        ddlPending.Items.Insert(2, New ListItem("Reviewer 1", "1"))
        ddlPending.Items.Insert(3, New ListItem("Reviewer 2", "2"))
        ddlPending.Items.Insert(4, New ListItem("Reviewer 3", "3"))
    End Sub

    Private Sub Bind_Level1()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        '   pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_EMPLOYEE_CORP", pParms)

        ddl_Level1.DataSource = ds
        ddl_Level1.DataValueField = "EMP_ID"
        ddl_Level1.DataTextField = "EMP_NAME"
        ddl_Level1.DataBind()
        ddl_Level1.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    End Sub

    Private Sub Bind_Level2()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        '   pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_EMPLOYEE_CORP", pParms)

        ddl_Level2.DataSource = ds
        ddl_Level2.DataValueField = "EMP_ID"
        ddl_Level2.DataTextField = "EMP_NAME"
        ddl_Level2.DataBind()
        ddl_Level2.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    End Sub
    Private Sub Bind_Level3()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        '   pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_EMPLOYEE_CORP", pParms)

        ddl_Level3.DataSource = ds
        ddl_Level3.DataValueField = "EMP_ID"
        ddl_Level3.DataTextField = "EMP_NAME"
        ddl_Level3.DataBind()
        ddl_Level3.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    End Sub
    Private Sub Bind_Level4()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        '   pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_EMPLOYEE_CORP", pParms)

        ddl_Level4.DataSource = ds
        ddl_Level4.DataValueField = "EMP_ID"
        ddl_Level4.DataTextField = "EMP_NAME"
        ddl_Level4.DataBind()
        ddl_Level4.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    End Sub
    Private Sub Bind_Level5()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        '   pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_EMPLOYEE_CORP", pParms)

        ddl_Level5.DataSource = ds
        ddl_Level5.DataValueField = "EMP_ID"
        ddl_Level5.DataTextField = "EMP_NAME"
        ddl_Level5.DataBind()
        ddl_Level5.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    End Sub

    Private Sub gridbind()
        cbePublish.ConfirmText = "Are you sure you want to Publish the Rating to  " & ddlBusinessunit.SelectedItem.Text & " employees ?"
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Try
            'param(1) = New SqlParameter("@emp_id", 16450)
            Dim DS As New DataSet
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
            pParms(1) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_EmpName.Text.Trim() = "", DBNull.Value, txt_EmpName.Text.Trim()))
            pParms(2) = New SqlClient.SqlParameter("@EMP_NO", IIf(txt_EmpNo.Text.Trim() = "", DBNull.Value, txt_EmpNo.Text.Trim()))
            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", IIf(ddlBusinessunit.SelectedValue = "0", DBNull.Value, ddlBusinessunit.SelectedItem.Value))
            pParms(4) = New SqlClient.SqlParameter("@PENDINGUNDER_LVL_ID", IIf(ddlPending.SelectedValue = "", "-1", ddlPending.SelectedValue))
            pParms(5) = New SqlClient.SqlParameter("@PDPY_ID", ViewState("Year"))
            pParms(6) = New SqlClient.SqlParameter("@TYPE_ID", IIf(ddlType.SelectedValue = "0", DBNull.Value, ddlType.SelectedValue))
            DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[PRI].[GET_EMP_PDP_COMPLETIONDETAILS]", pParms)

            rptPdp.DataSource = DS.Tables(0)
            rptPdp.DataBind()
            DS.Clear()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "gridbind")
        End Try
        'Dim param(2) As SqlParameter
        'param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))

    End Sub

    Public Function GetEmployeeLevel(ByVal eprid As Integer) As String
        Dim EmpLevel As String = String.Empty

        Try
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", eprid)
            EmpLevel = SqlHelper.ExecuteScalar(connStr, _
            CommandType.StoredProcedure, "PRI.GET_EMP_LEVEL", pParms)
            Return EmpLevel
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmployeeLevel")
        End Try
    End Function



    Public Function GetCurrentCycle() As String
        Dim currentCycleEncr As String = String.Empty

        Try
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString


            currentCycleEncr = SqlHelper.ExecuteScalar(connStr, _
            CommandType.StoredProcedure, "PRI.GETCURRENT_CYCLE")
            Return currentCycleEncr
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCurrentCycle")
        End Try
    End Function
    Private Sub Bind_TYPE()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        pParms(1) = New SqlClient.SqlParameter("@pdpy_id", ViewState("Year"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_emp_types", pParms)

        ddlType.DataSource = ds
        ddlType.DataValueField = "DESIG"
        ddlType.DataTextField = "DESIG"
        ddlType.DataBind()
        ddlType.Items.Insert(0, New ListItem("-- ALL --", "0"))
    End Sub
    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()



        Dim ds As New DataSet
        Try
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))

            ds = SqlHelper.ExecuteDataset(connStr, _
            CommandType.StoredProcedure, "PRI.GET_Businessunits", pParms)
            ddlBusinessunit.DataSource = ds.Tables(0)
            ddlBusinessunit.DataTextField = "BSU_NAME"
            ddlBusinessunit.DataValueField = "BSU_ID"
            ddlBusinessunit.DataBind()
            ddlBusinessunit.SelectedIndex = -1
            ddlBusinessunit.Items.Insert(0, New ListItem("All", "0"))
            'ddlBusinessunit_SelectedIndexChanged(ddlBusinessunit, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind bsu")
        End Try


        If Not ddlBusinessunit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '  ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "ExportJobs.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "ExportPDPfil.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click

        gridbind()
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click

        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim DS As New DataSet
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
        pParms(1) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_EmpName.Text.Trim() = "", DBNull.Value, txt_EmpName.Text.Trim()))
        pParms(2) = New SqlClient.SqlParameter("@EMP_NO", IIf(txt_EmpNo.Text.Trim() = "", DBNull.Value, txt_EmpNo.Text.Trim()))
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", IIf(ddlBusinessunit.SelectedValue = "0", DBNull.Value, ddlBusinessunit.SelectedValue))
        pParms(4) = New SqlClient.SqlParameter("@PENDINGUNDER_LVL_ID", IIf(ddlPending.SelectedValue = "", "-1", ddlPending.SelectedValue))
        pParms(5) = New SqlClient.SqlParameter("@PDPY_ID", ViewState("Year"))
        pParms(6) = New SqlClient.SqlParameter("@TYPE_ID", IIf(ddlType.SelectedValue = "0", DBNull.Value, ddlType.SelectedValue))
        DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[PRI].EXPORT_EMP_PDP_COMPLETIONDETAILS", pParms)

        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(DS.Tables(0), New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        'ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "ExportPDP.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "ExportPDP.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub
    Protected Sub lbtnRev_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
        Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
        Dim HF_USR_EMP_ID As HiddenField = sender.parent.FindControl("HF_REV_EMPID")
        Dim HF_PBS_ID As HiddenField = sender.parent.FindControl("HF_PBS_ID")
        Dim HF_BSU_ID As HiddenField = sender.parent.FindControl("HF_BSU_ID")
        Dim usrLevel As String = "LVL" + GetEmployeeLevel(HF_EPR_ID.Value)
        Dim curentCycle As String = GetCurrentCycle()
        Dim redirectUrl As String
        '= "PerformanceDevelopmentPlan.aspx?P=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&R=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
        If ViewState("Year") = "4" Then
            redirectUrl = "pdpSchool_v2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)
        Else
            redirectUrl = "pdpSchool.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)

        End If
       
        '+ "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + curentCycleEncr + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard_hos.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Dim sb As New System.Text.StringBuilder("")
        sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
        '  GETCURRENT_YEAR()
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "reportshow", sb.ToString())


    End Sub
    Protected Sub lbtnRevert_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            Dim HF_MAIN_LVL As HiddenField = sender.parent.FindControl("HF_MAIN_LVL")
            Dim HF_INTERIM_EDIT As HiddenField = sender.parent.FindControl("HF_INTERIM_EDIT")

            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EPR_ID") = HF_EPR_ID.Value
            ViewState("HF_EMP_ID") = ""
            ViewState("HF_EMP_ID") = HF_EMP_ID.Value
            ViewState("HF_MAIN_LVL") = ""
            ViewState("HF_MAIN_LVL") = HF_MAIN_LVL.Value
            ViewState("HF_INTERIM_EDIT") = ""
            ViewState("HF_INTERIM_EDIT") = HF_INTERIM_EDIT.Value

            Panel_RevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_RIEVEWERS_TOREVERT", pParms)
                rbtnList_Rievewer.DataSource = ds
                rbtnList_Rievewer.DataTextField = "EMP_NAME"
                rbtnList_Rievewer.DataValueField = "EMP_ID"
                rbtnList_Rievewer.DataBind()
                rbtnList_Rievewer.Items(0).Selected = True

                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_RevertBack.Visible = False
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EMP_ID") = ""
            ViewState("HF_MAIN_LVL") = ""
        End Try

    End Sub
    Protected Sub lbtnInterim_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            Dim HF_USR_EMP_ID As HiddenField = sender.parent.FindControl("HF_REV_EMPID")
            Dim HF_PBS_ID As HiddenField = sender.parent.FindControl("HF_PBS_ID")
            Dim HF_BSU_ID As HiddenField = sender.parent.FindControl("HF_BSU_ID")
            Dim HF_INTERIM_EDIT As HiddenField = sender.parent.FindControl("HF_INTERIM_EDIT")

            Dim usrLevel As String = "LVL" + GetEmployeeLevel(HF_EPR_ID.Value)
            Dim curentCycle As String = GetCurrentCycle()
            Dim redirectUrl As String
            If ViewState("Year") = 2 Then
                redirectUrl = "pdpSchool_Interim_v2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)
            ElseIf ViewState("Year") = 3 Then
                If HF_INTERIM_EDIT.Value = "1" Then
                    redirectUrl = "pdpSchool_Interim_v3_Edit.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)

                Else
                    redirectUrl = "pdpSchool_Interim_v3.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)

                End If
            ElseIf ViewState("Year") = 4 Then
                If HF_INTERIM_EDIT.Value = "1" Then
                    redirectUrl = "pdpSchool_Interim_v4_Edit.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)

                Else
                    redirectUrl = "pdpSchool_Interim_v4.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)

                End If

            Else
                redirectUrl = "pdpSchool_Interim.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)

            End If
            Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
            If url.Contains("?") Then
                url = url.Remove(url.IndexOf("?"))
            End If
            url = LCase(url)
            url = url.Replace("pdpdashboard_hos.aspx", redirectUrl)
            url = url.Replace("http:", "https:")
            Dim sb As New System.Text.StringBuilder("")
            sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
            ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
            ' GETCURRENT_YEAR()
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "reportshow", sb.ToString())
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub lbtnInterim_Edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
        Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")


        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")



        Try
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)



            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PRI.GET_INTERIM_EDIT", pParms)

            sqltran.Commit()
            gridbind()

        Catch ex As Exception
            sqltran.Rollback()
        End Try



    End Sub

    Protected Sub lbtnSetLevels_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EPR_ID") = HF_EPR_ID.Value
            ViewState("HF_EMP_ID") = ""
            ViewState("HF_EMP_ID") = HF_EMP_ID.Value
            Panel_SetLevel.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)
                Dim ds As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "PRI.GET_RIEVEWERS_LEVEL", pParms)
                While ds.Read
                    lbl_EMPNAME.Text = Convert.ToString(ds("EMPNAME"))
                    ddl_Level1.ClearSelection()
                    ddl_Level2.ClearSelection()
                    ddl_Level3.ClearSelection()
                    ddl_Level4.ClearSelection()
                    ddl_Level5.ClearSelection()

                    If Not ddl_Level1.Items.FindByValue(ds("LVL1")) Is Nothing Then
                        ddl_Level1.Items.FindByValue(ds("LVL1")).Selected = True
                    Else
                        ddl_Level1.Items.FindByValue("0").Selected = True
                    End If

                    If Not ddl_Level2.Items.FindByValue(ds("VIEW1")) Is Nothing Then
                        ddl_Level2.Items.FindByValue(ds("VIEW1")).Selected = True
                    Else
                        ddl_Level2.Items.FindByValue("0").Selected = True
                    End If

                    If Not ddl_Level3.Items.FindByValue(ds("VIEW2")) Is Nothing Then
                        ddl_Level3.Items.FindByValue(ds("VIEW2")).Selected = True
                    Else

                        ddl_Level3.Items.FindByValue("0").Selected = True
                    End If

                    If Not ddl_Level4.Items.FindByValue(ds("VIEW3")) Is Nothing Then
                        ddl_Level4.Items.FindByValue(ds("VIEW3")).Selected = True
                    Else

                        ddl_Level4.Items.FindByValue("0").Selected = True
                    End If
                    If Not ddl_Level5.Items.FindByValue(ds("VIEW4")) Is Nothing Then
                        ddl_Level5.Items.FindByValue(ds("VIEW4")).Selected = True
                    Else

                        ddl_Level5.Items.FindByValue("0").Selected = True
                    End If

                End While

            End Using
            '   GETCURRENT_YEAR()
        Catch ex As Exception
            Panel_RevertBack.Visible = False
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EMP_ID") = ""
        End Try

    End Sub

    Protected Sub btn_SaveLevel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_SaveLevel.Click
        '  Panel_RevertBack.Visible = False
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")


        Try
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
            pParms(2) = New SqlClient.SqlParameter("@REV1", ddl_Level1.SelectedItem.Value)
            pParms(3) = New SqlClient.SqlParameter("@REV2", ddl_Level2.SelectedItem.Value)
            pParms(4) = New SqlClient.SqlParameter("@REV3", ddl_Level3.SelectedItem.Value)
            pParms(5) = New SqlClient.SqlParameter("@REV4", ddl_Level4.SelectedItem.Value)
            pParms(6) = New SqlClient.SqlParameter("@REV5", ddl_Level5.SelectedItem.Value)
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PRI.SAVE_SET_LEVELS", pParms)
            sqltran.Commit()

            ' gridbind()
            '  GETCURRENT_YEAR()
            'lblerrormsg.Text = "You have successfully saved the Performance Development Plan"
            Panel_SetLevel.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub

    Protected Sub btn_SaveLevel_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_SaveLevel_Cancel.Click
        Panel_SetLevel.Visible = False
        ' GETCURRENT_YEAR()
        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
    End Sub


    Protected Sub lbtnFinal_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            Dim HF_USR_EMP_ID As HiddenField = sender.parent.FindControl("HF_REV_EMPID")
            Dim HF_PBS_ID As HiddenField = sender.parent.FindControl("HF_PBS_ID")
            Dim HF_BSU_ID As HiddenField = sender.parent.FindControl("HF_BSU_ID")
            Dim usrLevel As String = "LVL" + GetEmployeeLevel(HF_EPR_ID.Value)
            Dim curentCycle As String = GetCurrentCycle()
            Dim redirectUrl As String
            If ViewState("Year") = 4 Then
                redirectUrl = "pdpSchool_Final_V3.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)
            ElseIf ViewState("Year") = 3 Then
                redirectUrl = "pdpSchool_Final_V2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)
            ElseIf ViewState("Year") = 2 Then
                redirectUrl = "pdpSchool_Final_V2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)
            Else
                redirectUrl = "pdpSchool_Final.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value) + "&BR=" + Encr_decrData.Encrypt(HF_PBS_ID.Value) + "&RL=" + Encr_decrData.Encrypt(usrLevel) + "&RY=" + Encr_decrData.Encrypt(curentCycle) + "&B=" + Encr_decrData.Encrypt(HF_BSU_ID.Value)
            End If
            Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
            If url.Contains("?") Then
                url = url.Remove(url.IndexOf("?"))
            End If
            url = LCase(url)
            url = url.Replace("pdpdashboard_hos.aspx", redirectUrl)
            url = url.Replace("http:", "https:")
            Dim sb As New System.Text.StringBuilder("")
            sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
            ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
            '  GETCURRENT_YEAR()
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "reportshow", sb.ToString())
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btn_Revert_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Revert.Click
        '  Panel_RevertBack.Visible = False
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")

        Dim redirectUrl As String = "PerformanceDevelopmentPlan.aspx?P=" + Encr_decrData.Encrypt(ViewState("HF_EPR_ID")) + "&U=" + Encr_decrData.Encrypt(ViewState("HF_EMP_ID")) + "&R=" + Encr_decrData.Encrypt(rbtnList_Rievewer.SelectedValue)
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard_hos.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            If ViewState("HF_MAIN_LVL") = "3" Then
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", ViewState("HF_EMP_ID"))
                pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
                pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
                pParms(4) = New SqlClient.SqlParameter("@URL", "")

                SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[PRI].[REVERT_BACKTO_REVIEWER_INTERIM_FINAL]", pParms)
            ElseIf ViewState("HF_MAIN_LVL") = "2" Then

                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", ViewState("HF_EMP_ID"))
                pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
                pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
                pParms(4) = New SqlClient.SqlParameter("@URL", "")
                If ViewState("HF_INTERIM_EDIT") = "1" Then
                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[PRI].[REVERT_BACKTO_REVIEWER_INTERIM_EDIT]", pParms)
                Else
                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[PRI].[REVERT_BACKTO_REVIEWER_INTERIM]", pParms)
                End If

            ElseIf ViewState("HF_MAIN_LVL") = "1" Then
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", ViewState("HF_EMP_ID"))
                pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
                pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
                pParms(4) = New SqlClient.SqlParameter("@URL", "")
                SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PRI.REVERT_BACKTO_REVIEWER", pParms)
            End If
            sqltran.Commit()
            ' GETCURRENT_YEAR()
            'lblerrormsg.Text = "You have successfully saved the Performance Development Plan"
            Panel_RevertBack.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
        gridbind()
    End Sub
    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancel.Click
        Panel_RevertBack.Visible = False
        rbtnList_Rievewer.ClearSelection()
        txt_Remarks.InnerText = ""
        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
        ViewState("HF_MAIN_LVL") = ""
    End Sub

    Protected Sub btnPDF_D_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPDF_D.Click
        If hdnEpr.Value <> "" Then
            Try

                Dim EPR_ID As String = hdnEpr.Value
                Dim param As New Hashtable
                ' param.Add("@epr_id", EPR_ID)
                param.Add("@emp_id", ViewState("HF_EMP_ID"))
                param.Add("@pdpy_id", ViewState("Year"))
                param.Add("@usr_emp_id", Session("EmployeeId"))

                Dim rptClass As New rptClass
                With rptClass
                    .crDatabase = "OASIS_PDP_PRINCIPALS"
                    .reportParameters = param
                    .reportPath = Server.MapPath("Reports/rptPDPDetailReport_pri.rpt")
                End With

                Dim rptDownload As New ReportDownload
                rptDownload.LoadReports(rptClass, rs1)
                rptDownload = Nothing
                'GETCURRENT_YEAR()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub btnDownloadPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            ViewState("HF_EMP_ID") = HF_EMP_ID.Value
            hdnEpr.Value = HF_EPR_ID.Value
            ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", "buttonDownClick();", True)
            'GETCURRENT_YEAR()
        Catch ex As Exception

        End Try
    End Sub




    Protected Sub rptPdp_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptPdp.ItemDataBound
        ' Execute the following logic for Items and Alternating Items.


        Try
            If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then


                Dim HF_EPR_ID As HiddenField = e.Item.FindControl("HF_EPR_ID")
                Dim lbtnPdf As LinkButton = e.Item.FindControl("btnDownloadPdf")
                Dim HF_MAIN_LVL As HiddenField = e.Item.FindControl("HF_MAIN_LVL")

                Dim lbtnRev As LinkButton = e.Item.FindControl("lbtnRev")

                Dim EmpStatus_CSS As HtmlGenericControl = e.Item.FindControl("EmpStatus_CSS")
                Dim EmpStatus_Value As HtmlGenericControl = e.Item.FindControl("EmpStatus_Value")
                Dim LEVEL1_COLOR As HtmlGenericControl = e.Item.FindControl("LEVEL1_COLOR")
                Dim LEVEL1_CHECKBOX As HtmlGenericControl = e.Item.FindControl("LEVEL1_CHECKBOX")
                Dim spSummary As HtmlGenericControl = e.Item.FindControl("spSummary")
                Dim spRating As HtmlGenericControl = e.Item.FindControl("spRating")
                Dim hfSuper As HiddenField = e.Item.FindControl("hfSuper")
                ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbtnRev)

                ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbtnPdf)
                Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@YEAR", ViewState("Year"))
                pParms(1) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
                pParms(2) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)
                ' Dim LVL_FINISH As String = "0"
                Dim NXT_USR_LVL_FINISH As String = "0"
                Dim USR_LVL_FINISH As String = "0"
                Dim PRV_USR_LVL_FINISH As String = "0"
                Dim INTERIM_COMP As String = "0"
                Dim bENABLESETLEVELS As Boolean
                Dim bENABLE_REVERTBACK_BUTTON As Boolean
                Dim MAIN_LVL As String = "0"
                Dim Interim_Css As String = ""
                Dim EPR_bPUBLISHED As String = "0"
                Dim bSHOW_MGR As String = "0"
                Dim REVERT_RATING_SHOW As String = "0"
                Dim PUBLISH_RATING_SHOW As String = "0"
                Dim PUBLISH_BUTTON_SHOW As String = "0"
                Dim INTERIM_EDIT_BUTTON_SHOW As String = "0"

                Using reader As SqlDataReader = SqlHelper.ExecuteReader(connStr, CommandType.StoredProcedure, "PRI.GET_PDP_LEVEL", pParms)
                    While reader.Read()
                        'PRV_USR_LVL_FINISH = Convert.ToString(reader("PRV_USR_LVL_FINISH"))
                        'NXT_USR_LVL_FINISH = Convert.ToString(reader("NXT_USR_LVL_FINISH"))
                        'USR_LVL_FINISH = Convert.ToString(reader("USR_LVL_FINISH"))
                        'INTERIM_COMP = Convert.ToString(reader("INTERIM_COMP"))
                        'PRV_USR_LVL_FINISH = Convert.ToString(reader("PRV_USR_LVL_FINISH"))
                        MAIN_LVL = Convert.ToString(reader("LVL"))
                        HF_MAIN_LVL.Value = MAIN_LVL
                        EmpStatus_CSS.Attributes.Add("class", Convert.ToString(reader("EmpStatus_CSS")))
                        EmpStatus_Value.InnerHtml = Convert.ToString(reader("EmpStatus_Value"))


                        LEVEL1_COLOR.Attributes.Add("class", Convert.ToString(reader("LEVEL1_COLOR")))
                        LEVEL1_CHECKBOX.Attributes.Add("class", Convert.ToString(reader("LEVEL1_CHECKBOX")))

                        bENABLESETLEVELS = Convert.ToBoolean(reader("bENABLESETLEVELS"))
                        bENABLE_REVERTBACK_BUTTON = Convert.ToBoolean(reader("bENABLE_REVERTBACK_BUTTON"))
                        Interim_Css = Convert.ToString(reader("Interim_Css"))
                        EPR_bPUBLISHED = Convert.ToString(reader("EPR_bPUBLISHED"))
                        bSHOW_MGR = Convert.ToString(reader("bSHOW_MGR"))
                        REVERT_RATING_SHOW = Convert.ToString(reader("REVERT_RATING_SHOW"))
                        PUBLISH_RATING_SHOW = Convert.ToString(reader("PUBLISH_RATING_SHOW"))
                        PUBLISH_BUTTON_SHOW = Convert.ToString(reader("PUBLISH_BUTTON_SHOW"))
                        INTERIM_EDIT_BUTTON_SHOW = Convert.ToString(reader("INTERIM_EDIT_BUTTON"))
                    End While
                End Using

                If PUBLISH_BUTTON_SHOW = "1" Then
                    btnPublish.Visible = True
                End If


                If EPR_bPUBLISHED = "0" And bSHOW_MGR = "0" Then
                    spSummary.InnerHtml = ""
                    spRating.InnerHtml = ""
                End If


                If (bENABLESETLEVELS = "1") Then
                    CType(e.Item.FindControl("lbtnSetLevels"), LinkButton).Visible = True
                Else
                    CType(e.Item.FindControl("lbtnSetLevels"), LinkButton).Visible = False
                End If

                'If (PRV_USR_LVL_FINISH = "1") Then
                '    CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = True
                'Else
                '    CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = False
                'End If


                'If (USR_LVL_FINISH = "0" And PRV_USR_LVL_FINISH = "1") Then
                '    CType(e.Item.FindControl("lbtnRevert"), LinkButton).Visible = True

                'Else
                '    CType(e.Item.FindControl("lbtnRevert"), LinkButton).Visible = False

                'End If

                'CType(e.Item.FindControl("lbtnRevert"), LinkButton).Visible = False

                If bENABLE_REVERTBACK_BUTTON = "1" Then

                    CType(e.Item.FindControl("lbtnRevert"), LinkButton).Visible = True
                Else
                    CType(e.Item.FindControl("lbtnRevert"), LinkButton).Visible = False
                End If


                If (MAIN_LVL = "1") Then
                    CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = True
                    CType(e.Item.FindControl("lbtnInterim"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnFinal"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = False
                End If

                If (MAIN_LVL = "2") Then
                    CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnInterim"), LinkButton).Visible = True
                    CType(e.Item.FindControl("lbtnFinal"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnInterim"), LinkButton).CssClass = Interim_Css
                    CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = False
                End If
                If (MAIN_LVL = "3") Then
                    CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnInterim"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnFinal"), LinkButton).Visible = True
                    CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = False
                End If


                If PUBLISH_RATING_SHOW = "1" Then
                    CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = True
                End If
                If REVERT_RATING_SHOW = "1" Then
                    CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = True
                End If

                If INTERIM_EDIT_BUTTON_SHOW = "1" Then

                    CType(e.Item.FindControl("lbtnInterim_Edit"), LinkButton).Visible = True
                Else
                    CType(e.Item.FindControl("lbtnInterim_Edit"), LinkButton).Visible = False
                End If


                Dim HF_EMP_ID As HiddenField = e.Item.FindControl("HF_EMP_ID")
                Dim HF_USR_EMP_ID As HiddenField = e.Item.FindControl("HF_USR_EMP_ID")

                'If (HF_EMP_ID.Value = HF_USR_EMP_ID.Value) Then
                '    CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = True
                'End If

            End If
        Catch ex As Exception
            Dim ex1 As String = ex.Message
        End Try

    End Sub
    Protected Sub lnk_home_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnk_home.Click
        Response.Redirect("/PHOENIXBETA/ESSDashboard.aspx")
    End Sub

    Protected Sub lbtn_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim hf As HiddenField = sender.parent.FindControl("h_ID")
            Dim lnk As LinkButton = sender.parent.FindControl("lbtn")

            ViewState("Year") = hf.Value
            Session("PDP_YEAR") = ViewState("Year")
            Session("PDP_YEAR_ord") = hf.Value
            BIND_YEAR(lnk.CommandArgument)
            Bind_TYPE()
            gridbind()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GETCURRENT_YEAR()
        Try




            Dim conn As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
            Dim DS As New DataSet
            Dim param(1) As SqlParameter

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "PRI.GET_CURRENT_YEAR", param)
            If DS.Tables(0).Rows.Count >= 1 Then
                ViewState("Year") = DS.Tables(0).Rows(0).Item("id")
                Session("PDP_YEAR") = ViewState("Year")
                Session("PDP_YEAR_ord") = DS.Tables(0).Rows(0).Item("ord")
                BIND_YEAR(DS.Tables(0).Rows(0).Item("ord"))
                Bind_TYPE()
                gridbind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub BIND_YEAR(ByVal i As Integer)
        Try




            Dim conn As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
            Dim DS As New DataSet
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@id", i)
            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "PRI.BIND_YEAR", param)
            rptYear.DataSource = DS.Tables(0)
            rptYear.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btn_Revert_Publish_Click(sender As Object, e As EventArgs) Handles btn_Revert_Publish.Click
        Dim lnkRev As LinkButton = sender.parent.FindControl("lbtnRevNew")


        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim redirectUrl As String

        redirectUrl = "pdpCStaff.aspx?ER=" + Encr_decrData.Encrypt(ViewState("HF_EPR_ID")) + "&U=" + Encr_decrData.Encrypt(ViewState("HF_EMP_ID")) + "&MR=" + Encr_decrData.Encrypt(rbtnList_Rievewer.SelectedValue)
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", rbtnList_Rievewer_Publish.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
            pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks_Publish.InnerText.Trim())
            pParms(4) = New SqlClient.SqlParameter("@URL", url)


            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PRI.REVERT_RATING", pParms)

            sqltran.Commit()
            gridbind()
            'lblerrormsg.Text = "You have successfully saved the Performance Development Plan"
            Panel_PublishRevertBack.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub

    Protected Sub btn_Cancel_Publish_Click(sender As Object, e As EventArgs) Handles btn_Cancel_Publish.Click
        Panel_PublishRevertBack.Visible = False
        rbtnList_Rievewer_Publish.ClearSelection()
        txt_Remarks_Publish.InnerText = ""
        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
    End Sub
    Private Sub PUBLISH_RATING(ByRef errorMessage As String, ByVal XML_OBJ As String, ByVal status As String, ByVal transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Dim PARAM(4) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))

        PARAM(1) = New SqlClient.SqlParameter("@STR_XML", "<RT_M>" + XML_OBJ + "</RT_M>")

        PARAM(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(2).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "PRI.PUBLISH_RATING", PARAM)
        ReturnFlag = PARAM(2).Value
        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If


    End Sub

    Protected Sub lbtnPublish_Click(sender As Object, e As EventArgs)
        Dim XML_INTRIM As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                Dim STR_XML As New StringBuilder


                Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")


                STR_XML.Append(String.Format("<RT EPR_ID='{0}' STATUS='{1}' />", HF_EPR_ID.Value, 1))

                XML_INTRIM = STR_XML.ToString()

                PUBLISH_RATING(ERRORMSG, XML_INTRIM, "", transaction)
                If ERRORMSG <> "-1" Then
                    transaction.Commit()
                    gridbind()

                End If

            Catch ex As Exception
                ERRORMSG = "-1"
                transaction.Rollback()





            End Try
        End Using

    End Sub
    Private Function CHECK_GET_RATING(ByRef errormsg As String) As String
        Dim STR_XML As New StringBuilder
        Dim hfEPR_ID As HiddenField
        Dim lbtnPublish As LinkButton

        For Each objItem As RepeaterItem In rptPdp.Items
            hfEPR_ID = DirectCast(objItem.FindControl("HF_EPR_ID"), HiddenField)
            lbtnPublish = DirectCast(objItem.FindControl("lbtnPublish"), LinkButton)


            If lbtnPublish.Visible = "True" Then
                STR_XML.Append(String.Format("<RT EPR_ID='{0}' STATUS='{1}' />", hfEPR_ID.Value, 1))
            End If
        Next


        Return STR_XML.ToString()
    End Function

    Protected Sub btnPublish_Click(sender As Object, e As EventArgs) Handles btnPublish.Click


        Dim XML_INTRIM As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_PRINCIPALConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try



                XML_INTRIM = CHECK_GET_RATING(ERRORMSG)

                PUBLISH_RATING(ERRORMSG, XML_INTRIM, "", transaction)
                If ERRORMSG <> "-1" Then
                    transaction.Commit()
                    gridbind()

                End If

            Catch ex As Exception
                ERRORMSG = "-1"
                transaction.Rollback()





            End Try
        End Using
    End Sub
    Protected Sub lbtnRevertPublish_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EPR_ID") = HF_EPR_ID.Value
            ViewState("HF_EMP_ID") = ""
            ViewState("HF_EMP_ID") = HF_EMP_ID.Value
            Panel_PublishRevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_Principal_ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PRI.GET_RIEVEWERS_TOREVERT_PUBLISHED", pParms)
                rbtnList_Rievewer_Publish.DataSource = ds
                rbtnList_Rievewer_Publish.DataTextField = "EMP_NAME"
                rbtnList_Rievewer_Publish.DataValueField = "EMP_ID"
                rbtnList_Rievewer_Publish.DataBind()
                rbtnList_Rievewer_Publish.Items(0).Selected = True
                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_PublishRevertBack.Visible = False
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EMP_ID") = ""
        End Try


    End Sub
    Private Sub GETCURRENT_YEAR_nEW()
        Try





            BIND_YEAR(Session("PDP_YEAR_ord"))
            ' gridbind()
            ' lbtn_Click(Nothing, Nothing)

        Catch ex As Exception

        End Try
    End Sub
End Class

