<%@ Control Language="VB" AutoEventWireup="false" CodeFile="createpasswordquestions.ascx.vb" Inherits="createpasswordquestions" %>
<div class="matters" style="background-image: url(images/loginImage/background1.jpg); background-repeat: repeat; height: 204px; width: 398px;">
<table>
    <tr>
        <td>
          <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="300px" >
                        <tr>
                            <td class="subheader_img">
                               Password Recovery Question</td>
                        </tr>
                        <tr>
                            <td >
            <asp:Panel ID="Panel1" runat="server" >
<table >
    <tr align="left">
        <td >
            Email ID</td>
        <td>
            :</td>
        <td>
            <asp:TextBox ID="txtemailid" Width="300px" runat="server"></asp:TextBox></td>
    </tr>
    <tr align="left">
        <td >
            Questions</td>
        <td>
            :</td>
        <td>
            <asp:DropDownList ID="ddquestions" runat="server">
            </asp:DropDownList></td>
    </tr>
    <tr align="left">
        <td>
            Answer</td>
        <td>
            :</td>
        <td>
            <asp:TextBox ID="txtanswer" runat="server"></asp:TextBox></td>
    </tr>
</table>
            </asp:Panel>
           
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Button ID="btnsave" CssClass="button" runat="server" Text="Save" /><asp:Button ID="btnupdate"
                runat="server" Text="Update" CssClass="button" Visible="False" /><br />
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtemailid"
                Display="None" ErrorMessage="Please enter email address" SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemailid"
                Display="None" ErrorMessage="Please enter valid email address" SetFocusOnError="true"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtanswer"
                Display="None" ErrorMessage="Please answer the requested question." SetFocusOnError="True"></asp:RequiredFieldValidator>&nbsp;
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
        </td>
    </tr>
</table>
  </td>
                    </tr>
               </table>
<asp:HiddenField ID="Hiddenusername" runat="server" /><asp:HiddenField ID="Hiddencloseflag" runat="server" />

</div>

