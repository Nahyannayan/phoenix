<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" encoding="utf-8"/>
	<!-- Find the root node called Menus 
       and call MenuListing for its children -->
	<xsl:template match="/TABRIGHTS">
		<MenuItems>
			<xsl:call-template name="MenuListing" />
		</MenuItems>
	</xsl:template>

	<!-- Allow for recusive child node processing -->
	<xsl:template name="MenuListing">
		<xsl:apply-templates select="TABRIGHT" />
	</xsl:template>

	<xsl:template match="TABRIGHT">
		<MenuItem>
			<!-- Convert Menu child elements to MenuItem attributes -->

			<xsl:attribute name="valuefield">
				<xsl:value-of select="TAB_CODE"/>
			</xsl:attribute>
			<xsl:attribute name="Text">
				<xsl:value-of select="TAB_TEXT"/>
			</xsl:attribute>

			<xsl:attribute name="modules">
				<xsl:value-of select="TAB_MODULE"/>
			</xsl:attribute>
			<xsl:attribute name="ImageUrl">
				<xsl:value-of select="MNU_IMAGE"/>
			</xsl:attribute>
			<xsl:attribute name="NavigateUrl">
				<xsl:text></xsl:text>
				<xsl:value-of select="TAB_NAME"/>
			</xsl:attribute>
			<!-- Call MenuListing if there are child Menu nodes -->
			<xsl:if test="count(TABRIGHT) > 0">
				<xsl:call-template name="MenuListing" />
			</xsl:if>
		</MenuItem>
	</xsl:template>
</xsl:stylesheet>

