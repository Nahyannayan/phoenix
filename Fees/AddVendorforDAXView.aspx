﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AddVendorforDAXView.aspx.vb" Inherits="Fees_AddVendorforDAXView" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}
.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Add Vendor
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table  width="100%">
                    <tr valign="top">
                        <td align="left"  >
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" EnableViewState="false" SkinID="LabelError"></asp:Label>
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        </td>
                        <td align="right"> </td>
                    </tr>
                </table>
                <table align="center"  style="width: 100% !important;"  >
                    <tr>
                        <td class="matters" align="left" width="20%"><span class="field-label"> Business Unit</span></td>
                        <td width="80%">
                <telerik:RadComboBox ID="ddlBsu" runat="server" AutoPostBack="True" RenderMode="Lightweight" Filter="Contains" Width="100%" ZIndex="2000"></telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="2">
                            <asp:GridView ID="gvVendor" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                Width="100%" AllowPaging="True" PageSize="30" DataKeyNames="VDR_ID,EMP_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText="BSU">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSU" runat="server" Text='<%# Bind("BSU_SHORTNAME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp.Id">
                                        <HeaderTemplate>
                                            Emp.Id<br />
                                                                            <asp:TextBox ID="txtEmpNo" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnEmpnoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("VDR_REF_NO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp.Name">
                                        <HeaderTemplate>
                                            Emp.Name<br />
                                                                            <asp:TextBox ID="txtEmpName" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnEmpnameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EMP_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DES_DESCR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Code">
                                        <HeaderTemplate>
                                            Vendor Code<br />
                                                                            <asp:TextBox ID="txtVendorcode" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnVendorcodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVcode" runat="server" Text='<%# Bind("VDR_CODE")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnView" OnClick="lblPrintReciept_Click" runat="server">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

