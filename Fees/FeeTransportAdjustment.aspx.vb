Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj

Partial Class FEES_FeeTransportAdjustment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_TRANSPORT Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ddBusinessunit.DataBind()
            FillACD()
            h_BSUID.Value = Session("sbsuid")
            gvLedger.Attributes.Add("bordercolor", "#1b80b6")
            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvStudentLocation.Attributes.Add("bordercolor", "#1b80b6")
            txtHeaderRemarks.Attributes.Add("OnBlur", "FillDetailRemarks()")
            Session("sFEE_ADJUSTMENT") = Nothing
            txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
            trStudDetails.Visible = False
            trserviceDetails.Visible = False
            'btnPrint.Visible = True
            If ViewState("datamode") = "view" Then
                Try
                    Dim FAH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FAH_ID").Replace(" ", "+"))
                    Dim vFEE_ADJ As FEEADJUSTMENTTransport = FEEADJUSTMENTTransport.GetFeeAdjustments(FAH_ID)
                    txtDate.Text = Format(vFEE_ADJ.FAH_DATE, OASISConstants.DateFormat)
                    txtHeaderRemarks.Text = vFEE_ADJ.FAH_REMARKS
                    txtStudName.Text = vFEE_ADJ.FAH_STU_NAME
                    h_STUD_ID.Value = vFEE_ADJ.FAH_STU_ID
                    'BindArea()
                    ddBusinessunit.ClearSelection()
                    ddBusinessunit.SelectedValue = vFEE_ADJ.FAH_STU_BSU_ID
                    FillACD()
                    ddlAcademicYear.ClearSelection()
                    ddlAcademicYear.SelectedValue = vFEE_ADJ.FAH_ACD_ID
                    PopulateFeeType(vFEE_ADJ.FAH_GRD_ID)
                    If vFEE_ADJ.FAH_STU_TYPE = "S" Then
                        radStud.Checked = True
                    Else
                        radEnq.Checked = True
                    End If
                    Session("sFEE_ADJUSTMENT") = vFEE_ADJ
                    GridBindAdjustments()
                    GetStudentNo(vFEE_ADJ.FAH_STU_ID, vFEE_ADJ.FAH_ACD_ID, vFEE_ADJ.FAH_STU_BSU_ID)
                    trStudDetails.Visible = True
                    trserviceDetails.Visible = True
                    GetStudentDetails(vFEE_ADJ.FAH_STU_ID)
                    FillLocationDetails(vFEE_ADJ.FAH_STU_ID, Nothing)
                    FillLedgerDetails(vFEE_ADJ.FAH_STU_ID)
                    DissableAllControls(True)
                    btnPrint.Visible = True
                Catch ex As Exception
                    Errorlog(ex.Message)
                    'lblError.Text = "Could not load the data"
                    usrMessageBar2.ShowNotification("Could not load the data", UserControls_usrMessageBar.WarningType.Danger)
                End Try
            End If
        End If
    End Sub

    Private Sub PopulateFeeType(ByVal vGRD_ID As String)
        ddlFeeType.DataSource = FEEADJUSTMENTTransport.PopulateFeeMaster(h_BSUID.Value, ddlAcademicYear.SelectedValue)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub

    Private Sub DissableAllControls(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtHeaderRemarks.ReadOnly = dissble
        txtDetRemarks.ReadOnly = dissble
        txtDetAmount.ReadOnly = dissble
        ddlFeeType.Enabled = Not dissble
        btnDetAdd.Enabled = Not dissble
        imgDate.Enabled = Not dissble
        radEnq.Enabled = Not dissble
        radStud.Enabled = Not dissble
        'ddlGrade.Enabled = Not dissble
        imgProcess.Enabled = Not dissble
        ddlAcademicYear.Enabled = Not dissble
        ddBusinessunit.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        gvFeeDetails.Columns(5).Visible = Not dissble
    End Sub

    Private Sub GridBindAdjustments()
        Dim vFEE_ADJ As FEEADJUSTMENTTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            gvFeeDetails.DataSource = FEEADJUSTMENTTransport.GetSubDetailsAsDataTable(vFEE_ADJ.FEE_ADJ_DET)
            gvFeeDetails.DataBind()
        Else
            gvFeeDetails.DataSource = Nothing
            gvFeeDetails.DataBind()
        End If
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New FEEADJUSTMENTTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            vFEE_ADJ.FEE_ADJ_DET.Remove(CInt(lblFEE_ID.Text))
        End If
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        GridBindAdjustments()
    End Sub

    Private Function GetTotalAmountRemarks(ByVal vFEE_ADJ As FEEADJUSTMENTTransport, ByVal bEditMode As Boolean) As String
        Dim totAmount As Double
        Dim vEnum As IDictionaryEnumerator = vFEE_ADJ.FEE_ADJ_DET.GetEnumerator()
        While vEnum.MoveNext()
            Dim vFEE_ADJ_DET As FEEADJUSTMENT_S = vEnum.Value
            If Not vFEE_ADJ_DET.bDelete Then
                totAmount += vFEE_ADJ_DET.FAD_AMOUNT
            End If
        End While
        If bEditMode Then
            Return "Total Amount :- " & vFEE_ADJ.TotalAdjustmentAmount & "changed to :- " & totAmount
        Else
            Return "New Transport Adjustment inserted with Total Amount :- " & totAmount
        End If
    End Function

    Private Sub ClearDetails()
        h_BSUID.Value = Session("sBSUID")
        h_STUD_ID.Value = ""
        txtStudName.Text = ""
        txtStudNo.Text = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtHeaderRemarks.Text = ""
        lblGrade.Text = ""
        lblDivision.Text = ""
        lblBusNo.Text = ""
        trStudDetails.Visible = False
        trserviceDetails.Visible = False
        ddlFeeType.ClearSelection()
        Session("sFEE_ADJUSTMENT") = Nothing
        ClearSubDetails()
        GridBindAdjustments()
        gvStudentLocation.DataSource = Nothing
        gvStudentLocation.DataBind()
        gvLedger.DataSource = Nothing
        gvLedger.DataBind()
    End Sub

    Private Sub ClearSubDetails()
        txtDetAmount.Text = ""
        txtDetRemarks.Text = ""
        CBoneway.Checked = False
        ddlFeeType.SelectedIndex = -1
        btnDetAdd.Text = "Add"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        DissableAllControls(False)
        gvFeeDetails.Columns(3).Visible = True
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        imgProcess.Enabled = True
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        Try
            Dim vFEE_ADJ As New FEEADJUSTMENTTransport
            If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
                vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            End If
            vFEE_ADJ.FEE_ADJ_DET = AddDetails(vFEE_ADJ.FEE_ADJ_DET)
            Session("sFEE_ADJUSTMENT") = vFEE_ADJ
            GridBindAdjustments()
        Catch ex As Exception

        End Try
    End Sub

    Private Function AddDetails(ByVal htFEE_DET As Hashtable) As Hashtable
        Dim vFEE_DET As New FEEADJUSTMENT_S
        Dim eId As Integer = -1
        If btnDetAdd.Text = "Save" Then
            vFEE_DET = htFEE_DET(ViewState("Eid"))
            htFEE_DET.Remove(ViewState("Eid"))
            eId = ViewState("Eid")
        End If
        If Not htFEE_DET(CInt(ddlFeeType.SelectedValue)) Is Nothing Then
            'lblError.Text = "The Fee type is repeating..."
            usrMessageBar2.ShowNotification("The Fee type is repeating...", UserControls_usrMessageBar.WarningType.Danger)
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
        Else
            'lblError.Text = ""
            If radPositive.Checked Then
                vFEE_DET.AdjustmentType = 1
            ElseIf radNegtive.Checked Then
                vFEE_DET.AdjustmentType = -1
            End If
            vFEE_DET.FAD_AMOUNT = CDbl(txtDetAmount.Text) * vFEE_DET.AdjustmentType
            vFEE_DET.FEE_TYPE = ddlFeeType.SelectedItem.Text
            vFEE_DET.FAD_FEE_ID = ddlFeeType.SelectedValue
            vFEE_DET.FAD_REMARKS = txtDetRemarks.Text
            vFEE_DET.oneway = IIf(CBoneway.Checked = True, "TRUE", "FALSE")
            vFEE_DET.SSV_ID = HFssv_id.Value
            htFEE_DET(vFEE_DET.FAD_FEE_ID) = vFEE_DET
            ClearSubDetails()
        End If
        Return htFEE_DET
    End Function

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubCancel.Click
        ClearSubDetails()
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New FEEADJUSTMENTTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            Dim vFEE_DET As FEEADJUSTMENT_S = vFEE_ADJ.FEE_ADJ_DET(CInt(lblFEE_ID.Text))
            If Not vFEE_DET Is Nothing Then
                ddlFeeType.SelectedIndex = -1
                ddlFeeType.SelectedValue = vFEE_DET.FAD_FEE_ID
                txtDetAmount.Text = vFEE_DET.FAD_AMOUNT
                txtDetRemarks.Text = vFEE_DET.FAD_REMARKS
                If vFEE_DET.oneway = "TRUE" Then
                    CBoneway.Checked = True
                Else
                    CBoneway.Checked = False
                End If
                If vFEE_DET.AdjustmentType = 1 Then
                    radPositive.Checked = True
                ElseIf vFEE_DET.AdjustmentType = -1 Then
                    radNegtive.Checked = True
                End If
                ViewState("Eid") = CInt(lblFEE_ID.Text)
                btnDetAdd.Text = "Save"
            End If
        End If
    End Sub

    Private Sub FillFeeType()
        Session("strEditRemarks") += +"FEE TYPE :- " & ddlFeeType.SelectedItem.Text & _
        ";AMOUNT:- " & txtDetAmount.Text & ";REMARKS :- " & txtDetRemarks.Text

    End Sub

    Protected Sub imgProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgProcess.Click
        If h_STUD_ID.Value <> "" Then
            'ddlGrade.Enabled = True
            Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
            FillGradeDetails(STUD_ID)
            ClearEnteredDatas()
            'h_GRD_ID.Value = ddlGrade.SelectedValue
            PopulateFeeType(h_GRD_ID.Value)
            'BindArea()
        End If
    End Sub

    Private Sub BindArea()
        If txtDate.Text = "" Then Return
        GetAreaForStudent(h_STUD_ID.Value, ddlAcademicYear.SelectedValue, ddBusinessunit.SelectedValue, CDate(txtDate.Text))
    End Sub

    Private Sub GetAreaForStudent(ByVal vSTU_ID As String, ByVal vACD_ID As Integer, ByVal vBSU_ID As String, ByVal vDate As Date)
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Dim sqlStr As String = " SELECT oasis..STUDENT_SERVICES_D.SSV_ID, oasis..STUDENT_SERVICES_D.SSV_SBL_ID, " & _
            " TRANSPORT.SUBLOCATION_M.SBL_DESCRIPTION FROM oasis..STUDENT_SERVICES_D INNER JOIN " & _
            " TRANSPORT.SUBLOCATION_M ON oasis..STUDENT_SERVICES_D.SSV_SBL_ID = TRANSPORT.SUBLOCATION_M.SBL_ID " & _
            "WHERE oasis..STUDENT_SERVICES_D.SSV_STU_ID = '" & vSTU_ID & "' AND " & _
            " oasis..STUDENT_SERVICES_D.SSV_ACD_ID = " & vACD_ID & " AND oasis..STUDENT_SERVICES_D.SSV_BSU_ID = '" & vBSU_ID & _
            "' AND '" & txtDate.Text & "' BETWEEN SSV_FROMDATE AND isnull(SSV_TODATE,'1/1/9999') "
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sqlStr)
            'lblArea.Text = ""
            While (drReader.Read())
                'lblArea.Text = drReader("SBL_DESCRIPTION")
                ViewState("SSV_ID") = drReader("SSV_ID")
                ViewState("SSV_SBL_ID") = drReader("SSV_SBL_ID")
            End While
        End Using
    End Sub

    Private Sub FillGradeDetails(ByVal STU_ID As Integer)
        Dim STU_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT
        If radEnq.Checked Then
            STU_TYP = STUDENTTYPE.ENQUIRY
        ElseIf radStud.Checked Then
            STU_TYP = STUDENTTYPE.STUDENT
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ClearDetails()
    End Sub

    Private Sub MakeEditable()
        If Session("sFEE_ADJUSTMENT") Is Nothing Then Return
        Dim vFEE_ADJ As New FEEADJUSTMENTTransport
        vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        vFEE_ADJ.bEdit = True
        'For Each vFEE_DET As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
        '    vFEE_DET.bEdit = True
        'Next
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        ClearEnteredDatas()
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        ClearDetails()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        ClearDetails()
    End Sub

    Private Sub ClearEnteredDatas()
        ClearSubDetails()
        Session("sFEE_ADJUSTMENT") = Nothing
        GridBindAdjustments()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillACD()
        ClearDetails()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim vFEE_ADJ As FEEADJUSTMENTTransport = Session("sFEE_ADJUSTMENT")
        If Not vFEE_ADJ Is Nothing Then
            Session("ReportSource") = FEEADJUSTMENTTransport.PrintAdjustmentVoucher(vFEE_ADJ.FAH_ID, vFEE_ADJ.FAH_STU_BSU_ID, _
            Session("sBsuid"), Session("sUsr_name"))
            h_print.Value = "print"
        Else
            GetFeeAdjustmentID()
        End If
    End Sub

    Protected Sub txtStudNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim str_data, str_sql As String
            txtStudNo.Text = txtStudNo.Text.Trim
            Dim iStdnolength As Integer = txtStudNo.Text.Length
            If radStud.Checked Then
                If iStdnolength < 9 Then
                    If txtStudNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStudNo.Text = "0" & txtStudNo.Text
                        Next
                    End If
                    'txtStudNo.Text = Session("sBsuid") & txtStudNo.Text
                    txtStudNo.Text = ddBusinessunit.SelectedValue & txtStudNo.Text
                End If
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM VW_OSO_STUDENT_M" _
                 & " WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & ddBusinessunit.SelectedValue & "') AND  STU_NO='" & txtStudNo.Text & "'"
                'str_data = GetDataFromSQL(str_sql, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
                str_data = GetDataFromSQL(str_sql, ConnectionManger.GetOASISTRANSPORTConnectionString)
            Else
                'str_data = GetDataFromSQL("SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
                '& " WHERE     (STU_BSU_ID = '" & ddBusinessunit.SelectedValue & "') AND  STU_NO='" & txtStudNo.Text & "'", _
                'WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
                str_data = GetDataFromSQL("SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
                & " WHERE     (STU_BSU_ID = '" & ddBusinessunit.SelectedValue & "') AND  STU_NO='" & txtStudNo.Text & "'", _
                ConnectionManger.GetOASISTRANSPORTConnectionString)
            End If
            If str_data <> "--" Then
                h_STUD_ID.Value = str_data.Split("|")(0)
                txtStudNo.Text = str_data.Split("|")(1)
                txtStudName.Text = str_data.Split("|")(2)
                trStudDetails.Visible = True
                trserviceDetails.Visible = True
                ddlFeeType.DataSource = FEEADJUSTMENTTransport.PopulateFeeMaster(h_BSUID.Value, ddlAcademicYear.SelectedValue)
                ddlFeeType.DataTextField = "FEE_DESCR"
                ddlFeeType.DataValueField = "FEE_ID"
                ddlFeeType.DataBind()

                'Gridbind_Feedetails()
                lblGrade.Text = ""
                lblDivision.Text = ""
                lblBusNo.Text = ""
                txtssv_id.Text = ""
                HFssv_id.Value = ""

                GetStudentDetails(h_STUD_ID.Value)
                FillLocationDetails(h_STUD_ID.Value, Nothing)
                FillLedgerDetails(h_STUD_ID.Value)
            Else
                h_STUD_ID.Value = ""
                txtStudName.Text = ""
                txtssv_id.Text = ""
                HFssv_id.Value = ""
                'lblNoStudent.Text = "Student # Entered is not valid  !!!"
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GetStudentDetails(ByVal Stud_ID As String)
        Dim strSql As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim ds As New DataSet
        strSql = "SELECT     STUDENT_M.STU_NO, STUDENT_M.STU_BSU_ID, STUDENT_M.STU_ACD_ID, VW_OSO_GRADE_M.GRD_DISPLAY, VV_SECTION_M.SCT_DESCR, " _
                 & " TRANSPORT.VV_BUSES.BNO_DESCR, STUDENT_M.STU_ID " _
                 & " FROM STUDENT_M INNER JOIN " _
                 & " VW_OSO_GRADE_M ON STUDENT_M.STU_GRD_ID = VW_OSO_GRADE_M.GRD_ID LEFT OUTER JOIN " _
                 & " VV_SECTION_M ON STUDENT_M.STU_SCT_ID = VV_SECTION_M.SCT_ID LEFT OUTER JOIN " _
                 & " TRANSPORT.VV_BUSES ON STUDENT_M.STU_PICKUP_TRP_ID = TRANSPORT.VV_BUSES.TRP_ID WHERE STUDENT_M.STU_ID='" & Stud_ID & "' AND STUDENT_M.STU_BSU_ID='" & ddBusinessunit.SelectedValue & "' AND STUDENT_M.STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            lblGrade.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("GRD_DISPLAY")) = False, ds.Tables(0).Rows(0)("GRD_DISPLAY"), "")
            lblDivision.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("SCT_DESCR")) = False, ds.Tables(0).Rows(0)("SCT_DESCR"), "")
            lblBusNo.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("BNO_DESCR")) = False, ds.Tables(0).Rows(0)("BNO_DESCR"), "")
        End If
    End Sub

    Private Sub FillLocationDetails(ByVal Stud_ID As String, ByVal SSV_ID As String)
        Dim ds As DataSet
        Dim str_Sql As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        str_Sql = "exec dbo.GetPickupDropoffPoints_Student '" & Stud_ID & "','" & ddBusinessunit.SelectedValue & "','" & ddlAcademicYear.SelectedValue & "','" & SSV_ID & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            gvStudentLocation.DataSource = ds
            gvStudentLocation.DataBind()
        Else
            gvStudentLocation.DataSource = Nothing
            gvStudentLocation.DataBind()
        End If
    End Sub

    Private Sub FillLedgerDetails(ByVal Stud_ID As String)
        Dim ds As DataSet
        Dim str_Sql As String
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        str_Sql = " TRANSPORT.GetLedgerForAdjustment '" & Session("sbsuid") & "','" & ddBusinessunit.SelectedValue & "' ,'" & Stud_ID & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            gvLedger.DataSource = ds
            gvLedger.DataBind()
        Else
            gvLedger.DataSource = Nothing
            gvLedger.DataBind()
        End If
    End Sub

    Private Sub GetFeeAdjustmentID()
        Dim strSql As String
        Dim ds As DataSet
        Dim FAHID As Integer
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        strSql = "SELECT MAX(FAH_ID) as FAH_ID FROM FEES.FEEADJUSTMENT_H WHERE FAH_BSU_ID='" & Session("sbsuid") & "' AND FAH_STU_BSU_ID='" & ddBusinessunit.SelectedValue & "' AND FAH_ACD_ID='" & ddlAcademicYear.SelectedValue & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            FAHID = IIf(IsDBNull(ds.Tables(0).Rows(0)("FAH_ID")) = False, ds.Tables(0).Rows(0)("FAH_ID"), 0)
        End If
        Session("ReportSource") = FEEADJUSTMENTTransport.PrintAdjustmentVoucher(FAHID, ddBusinessunit.SelectedValue, _
        Session("sBsuid"), Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    Private Sub GetStudentNo(ByVal Stud_ID As String, ByVal ACDID As String, ByVal Stu_Bsu_id As String)
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        strSql = "SELECT STUDENT_M.STU_NO FROM STUDENT_M WHERE STUDENT_M.STU_ID='" & Stud_ID & "' AND STUDENT_M.STU_BSU_ID='" & Stu_Bsu_id & "' AND STUDENT_M.STU_ACD_ID='" & ACDID & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtStudNo.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("STU_NO")) = False, ds.Tables(0).Rows(0)("STU_NO"), 0)
        End If
    End Sub

    Protected Sub imgservice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        '' FillLocationDetails(h_STUD_ID.Value, HFssv_id.Value)
    End Sub

    Protected Sub txtssv_id_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If HFssv_id.Value <> "" Then
                FillLocationDetails(h_STUD_ID.Value, HFssv_id.Value)
            Else
                'lblError.Text = "Select Service Area"
                usrMessageBar2.ShowNotification("Select Service Area", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class
