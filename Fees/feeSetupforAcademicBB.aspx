<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeSetupforAcademicBB.aspx.vb" Inherits="Fees_feeSetupforAcademicBB" title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" EnableViewState="false" >
 <script type="text/javascript" language="javascript">
    function getLOCATION() 
   {  
        var sFeatures,url;
        sFeatures="dialogWidth: 600px; ";
        sFeatures+="dialogHeight: 500px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result; 
          <%--url= "../common/PopupFormIDhidden.aspx?iD=LOCATIONFEE&bsu=<%= Session("sBsuid") %>&MULTISELECT=FALSE";
          result = window.showModalDialog(url,"", sFeatures); 
           if (result=='' || result==undefined)
            {
            return false;
            }             
            NameandCode = result.split('___');
           document.getElementById('<%= H_Location.ClientID %>').value=NameandCode[0]; 
           document.getElementById('<%= txtLocation.ClientID %>' ).value=NameandCode[1] ; 
           return false;
    }   --%>

         var url = "../common/PopupFormIDhidden.aspx?iD=LOCATIONFEE&bsu=<%= Session("sBsuid") %>&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_location");
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%= H_Location.ClientID %>').value=NameandCode[0]; 
                document.getElementById('<%= txtLocation.ClientID %>' ).value=NameandCode[1] ;
                __doPostBack('<%=txtLocation.ClientID%>', 'TextChanged');
            }
        }
  
   function GetLinkToStage() 
   {      
        var sFeatures,url;
        sFeatures="dialogWidth:350px; ";
        sFeatures+="dialogHeight: 400px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;
       <%--result = window.showModalDialog("../common/PopupForm.aspx?id=LINKTOSTAGE&multiSelect=false","", sFeatures); 
        if (result=='' || result==undefined)
        {    return false;      } 
         NameandCode=result.split('___');     
         document.getElementById('<%=txtLinkToStage.ClientID %>').value=NameandCode[1];
         document.getElementById('<%=hf_PRO_ID.ClientID %>').value=NameandCode[0];
        return false;
    }   --%>

        var url = "../common/PopupForm.aspx?id=LINKTOSTAGE&multiSelect=false";
            var oWnd = radopen(url, "pop_linktostage");
                        
        } 

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtLinkToStage.ClientID %>').value=NameandCode[1];
                document.getElementById('<%=hf_PRO_ID.ClientID %>').value=NameandCode[0];
                __doPostBack('<%=txtLinkToStage.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_linktostage" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_location" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Set Transport Fees (For Outsourced Units Only)
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table align="center" width="100%">
     <tr>
         <td align="left">
         <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
             <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
         </td>
     </tr>
 </table>
 <table  align="center" width="100%">            
            
     <tr>
         <td align="left" width="20%">
             <span class="field-label">Business Unit</span></td> 
         <td align="left" width="30%">
             <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True"
                 DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                 TabIndex="5">
             </asp:DropDownList></td>          
    
         <td align="left" width="20%">
             <span class="field-label">Academic Year</span></td>
         <td align="left" width="30%">
             <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" TabIndex="10">
             </asp:DropDownList></td>
         </tr>
        <tr>
             <td align="left" width="20%">
                 <span class="field-label">From Date</span></td>
             <td align="left" width="30%"> 
             <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="None"></asp:TextBox>
             <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />&nbsp;
         </td>
     
         <td align="left">
             <span class="field-label">Collection Schedule</span></td>
         <td align="left">
             <asp:DropDownList ID="ddlCollectionSchedule" runat="server" DataSourceID="odsGetSCHEDULE_M"
                 DataTextField="SCH_DESCR" DataValueField="SCH_ID" AutoPostBack="True" SkinID="DropDownListNormal">
             </asp:DropDownList></td>
         </tr>
         <tr>
          <td align="left">
             <span class="field-label">Fee Type</span></td>
             <td align="left">
             <asp:DropDownList ID="ddlFeetype" runat="server" DataSourceID="odsGetFEETYPE_M" DataTextField="FEE_DESCR" DataValueField="FEE_ID" AutoPostBack="True" SkinID="DropDownListNormal" >
             </asp:DropDownList></td>
             <td></td>
             <td></td>
     </tr>    
     <tr style="display:none">
         <td align="left">
             <span class="field-label">Stream</span></td>
         <td align="left">
             <asp:CheckBox ID="chkActive" runat="server" Checked="True" Text="Active" Visible="False" /> </td>
         <td align="left">
              <span class="field-label">Grade</span></td>
         <td align="left"> &nbsp;</td>
     </tr>
     <tr>
         <td align="left">
             <span class="field-label">Pro Rata (Join)</span></td>
         <td align="left">
             <asp:DropDownList ID="ddlJoinprorata" runat="server" DataSourceID="SqlProrata" DataTextField="FPM_DESCR"
                 DataValueField="FPM_ID" SkinID="DropDownListNormal">
             </asp:DropDownList></td>
            <td align="left">
                <span class="field-label">Pro Rata (Discontinue)</span></td>
         <td align="left">
             <asp:DropDownList ID="ddlDiscontinueprorata" runat="server" DataSourceID="SqlProrata" DataTextField="FPM_DESCR"
                 DataValueField="FPM_ID" SkinID="DropDownListNormal">
             </asp:DropDownList></td>  
     </tr>     
     <tr>
         <td align="left">
             <span class="field-label">Link To Stage</span></td>
         <td align="left">
             <asp:TextBox id="txtLinkToStage" runat="server">
             </asp:TextBox>
             <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/cal.gif"
                 OnClientClick="GetLinkToStage();return false;"></asp:ImageButton></td>
         <td></td>
         <td></td>
     </tr>
     <tr id="tr_Transport" runat="server" > 
         <td align="left">
             <span class="field-label">Area</span></td>
         <td align="left">
             <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;
             <asp:ImageButton ID="imgLocation" runat="server" ImageAlign="AbsMiddle"
                 ImageUrl="~/Images/Misc/Route.png" OnClientClick="getLOCATION();return false;" TabIndex="30" /></td>
         <td></td>
         <td></td>
     </tr>
     <tr>
         <td align="left">
             <span class="field-label">Amount</span></td>
         <td align="left" valign="middle">
             <asp:TextBox ID="txtHAmount" style="text-align:right;" runat="server" AutoCompleteType="Disabled">0.00</asp:TextBox>
             </td>
         <td align="left">
             <span class="field-label">OneWay Amount</span>
             </td>
         <td>
             <telerik:RadNumericTextBox ID="txtOneWayAmt" Runat="server" 
                 Culture="English (United States)" style="text-align:right;" Value="0">
             </telerik:RadNumericTextBox>
             <asp:CheckBox id="chkRefundable" runat="server" Text="Refundable">
             </asp:CheckBox>
             <asp:Label ID="lblChargeSchedule" runat="server" SkinID="LabelError" Visible="False"></asp:Label></td>
     </tr>
      <tr class="title-bg" ><td colspan="4" align="left" >
          Late Fee Charges</td></tr>
     <tr>
         <td align="left">
             <span class="field-label">Charge Type</span></td>
         <td align="left" colspan="3">
             <asp:RadioButton ID="rbPercentage" runat="server" CssClass="field-label" GroupName="type" Text="Percentage" />
             <asp:RadioButton ID="rbAmount" runat="server" CssClass="field-label" GroupName="type" Text="Amount"  Checked="true"  /></td>
     </tr>     
     <tr>
         <td align="left"  >
             <span class="field-label">Late Fee Amount/Percentage</span></td>
         <td align="left"  >
             <asp:TextBox ID="txtLateAmount" runat="server"  AutoCompleteType="Disabled">0</asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLateAmount"
                 CssClass="error" ErrorMessage="Please enter late fee Amount/Percentage" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
         <td align="left"  >
             <span class="field-label">Fee Late Days</span></td>
         <td align="left">
             <asp:TextBox ID="txtLatefeedays" runat="server" Width="50px" AutoCompleteType="Disabled">0</asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtLatefeedays"
                 CssClass="error" ErrorMessage="Please Enter late fee days" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
     </tr>
     <tr id="tr_DetailViewDetailHeader" runat="server" class="title-bg"><td colspan="4" align="left">
         <span class="field-label">Fee Details</span></td></tr>
     <tr id="tr_DetailViewDetails" runat="server">
         <td align="left" colspan="4">
             <asp:GridView ID="gvAttendance" runat="server" CssClass="table table-bordered table-row" Width="100%" AutoGenerateColumns="False" EmptyDataText="No Details Added">
                        <Columns>
                            <asp:BoundField DataField="Fdate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                                HtmlEncode="False" ReadOnly="True" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="tdate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="To Date"
                                HtmlEncode="False" ReadOnly="True" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField> 
                            <asp:BoundField DataField="Acd_year" HeaderText="Academic Year" ReadOnly="True" >
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:0.00}" ReadOnly="True">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>  
                            <asp:BoundField DataField="Fee_descr" HeaderText="Fee" ReadOnly="True" >
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:BoundField> 
                            <asp:BoundField DataField="Grd_id" HeaderText="Grade" ApplyFormatInEditMode="True" ReadOnly="True" >
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:CommandField ShowDeleteButton="True" Visible="False" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:CommandField>
                            <asp:TemplateField ShowHeader="False" HeaderText="Edit">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                        Text="Update"></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                        Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                        Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="id" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns> 
                    </asp:GridView> 
         </td>
           </tr>
     <tr><td colspan="4">&nbsp;</td></tr>
         </table>
          <table align="center" width="100%"> 
                    <tr > 
                    <td colspan="4" align="center" >  
                <asp:GridView ID="gvMonthly" runat="server" AutoGenerateColumns="False" CellPadding="4" SkinID="GridViewNormal">
                 <Columns>
                     <asp:BoundField DataField="DESCR" HeaderText="Term/Month" InsertVisible="False" ReadOnly="True" />
                     <asp:TemplateField HeaderText="Amount">
                         <ItemTemplate>
                             <asp:TextBox ID="txtAmount" runat="server" style="text-align:right;" Text='<%# Bind("FDD_AMOUNT") %>' AutoCompleteType="Disabled" Width="102px"></asp:TextBox>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="OneWay">
                         <ItemTemplate>
                             <asp:TextBox ID="txt1Way" style="text-align:right;" runat="server" AutoCompleteType="Disabled" 
                                 Text='<%# Bind("ONEWAYAMT") %>' ></asp:TextBox>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Charge Dt.">
                         <EditItemTemplate> 
                         </EditItemTemplate>
                         <ItemTemplate> 
                             <asp:TextBox id="txtChargeDate" runat="server" Text='<%# Bind("FDD_DATE" ) %>' Width="75%"></asp:TextBox>
                             <asp:ImageButton id="imgChargeDate" tabIndex=4 runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>

                             <ajaxToolkit:CalendarExtender ID="CalChargeDate" runat="server" PopupPosition="TopLeft"
                                PopupButtonID="imgChargeDate" TargetControlID="txtChargeDate" CssClass="MyCalendar"  Format="dd/MMM/yyyy">
                             </ajaxToolkit:CalendarExtender>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="First Memo Dt.">
                         <ItemTemplate>
                             <asp:TextBox ID="txtFirstMemoDate" runat="server" Text='<%# Bind("FDD_FIRSTMEMODT" ) %>' Width="75%"></asp:TextBox>
                             <asp:ImageButton ID="imgFirstMemoDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />

                             <ajaxToolkit:CalendarExtender ID="CalFirstMemoDate" runat="server" PopupPosition="TopLeft"
                                PopupButtonID="imgFirstMemoDate" TargetControlID="txtFirstMemoDate" CssClass="MyCalendar"  Format="dd/MMM/yyyy">
                             </ajaxToolkit:CalendarExtender>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Second  Memo Dt.">
                         <ItemTemplate>
                             <asp:TextBox id="txtSecondMemoDate" runat="server" Text='<%# Bind("FDD_SECONDMEMODT" ) %>' Width="75%"></asp:TextBox>
                             <asp:ImageButton id="imgSecondMemoDate" tabIndex=4 runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>

                             <ajaxToolkit:CalendarExtender ID="CalSecondMemoDate" runat="server" PopupPosition="TopLeft"
                                 PopupButtonID="imgSecondMemoDate" TargetControlID="txtSecondMemoDate" CssClass="MyCalendar"  Format="dd/MMM/yyyy">
                             </ajaxToolkit:CalendarExtender>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Third  Memo Dt.">
                         <ItemTemplate>
                             <asp:TextBox id="txtThirdMemoDate" runat="server" Text='<%# Bind("FDD_THIRDMEMODT" ) %>' Width="75%"></asp:TextBox>
                             <asp:ImageButton id="imgThirdMemoDate" tabIndex=4 runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                             <ajaxToolkit:CalendarExtender ID="calThirdMemoDate" runat="server" PopupPosition="TopLeft"
                                  PopupButtonID="imgThirdMemoDate" TargetControlID="txtThirdMemoDate" CssClass="MyCalendar"  Format="dd/MMM/yyyy">
                             </ajaxToolkit:CalendarExtender>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ID" Visible="False">
                         <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>
           </asp:GridView> 
                </td>
            </tr>     
            <tr>
                <td colspan="4" align="center">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /><asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" /><asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                
            </tr>
        </table>
             <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                 PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
             </ajaxToolkit:CalendarExtender>
         <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                 PopupButtonID="txtFrom" TargetControlID="txtFrom" CssClass="MyCalendar"  Format="dd/MMM/yyyy">
             </ajaxToolkit:CalendarExtender>
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
             <asp:ObjectdataSource ID="odsGetSCHEDULE_M" runat="server" SelectMethod="GetSCHEDULE_M"
                 TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                 <selectparameters>
<asp:Parameter DefaultValue="false" Name="bisCollection" Type="Boolean"></asp:Parameter>
</selectparameters>
             </asp:ObjectdataSource>
             <asp:ObjectdataSource ID="odsGetFEETYPE_M" runat="server" SelectMethod="GetFEES_M_TRANSPORT"
                 TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}"></asp:ObjectdataSource>
             <asp:ObjectdataSource ID="odsGetGRADE_M" runat="server" SelectMethod="GetGRADE_M"
                 TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                 <SelectParameters>
                     <asp:ControlParameter ControlID="ddBusinessunit" Name="BSU_ID" PropertyName="SelectedValue"
                         Type="String" />
                     <asp:ControlParameter ControlID="ddlAcademicYear" Name="ACD_ID" PropertyName="SelectedValue"
                         Type="String" />
                 </SelectParameters>
             </asp:ObjectdataSource>
    <asp:ObjectdataSource ID="odsStream_m" runat="server" SelectMethod="GetSTREAM_M"
        TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddBusinessunit" DefaultValue="" Name="BSU_ID" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="ddlAcademicYear" DefaultValue="" Name="ACD_ID" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectdataSource>
             <asp:SqlDataSource ID="SqlProrata" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
                 SelectCommand="SELECT       FPM_ID, FPM_DESCR  &#13;&#10;FROM         FEES.FEES_PRORATA_M where FPM_Bactive=1&#13;&#10;">
             </asp:SqlDataSource>
    <asp:HiddenField ID="H_Location" runat="server" /><asp:HiddenField ID="hf_PRO_ID" runat="server" />

                </div>
            </div>
        </div>

</asp:Content>

