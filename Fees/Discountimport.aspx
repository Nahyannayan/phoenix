<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Discountimport.aspx.vb" Inherits="Fees_Discountimport"  %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   <script type="text/javascript" language ="javascript" >
    Sys.Application.add_load(  
    function CheckForPrint()  
     {     
       if (document.getElementById('<%= h_print.ClientID %>' ).value!='')
       { 
          document.getElementById('<%= h_print.ClientID %>' ).value=''; 
           //showModelessDialog ('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
           var url = "../Reports/ASPX Report/RptViewerModal.aspx";
           var oWnd = radopen(url, "pop_clickforprint");

       }
     } 
    );
    function getFile()
        {
            var filepath = document.getElementById('<%=uploadFile.ClientID %>').value;
            document.getElementById('<%=HidUpload.ClientID %>').value = filepath;
        }
   
           
 
    
function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       
        <Windows>
            <telerik:RadWindow ID="pop_clickforprint" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Discount Scheme
        </div>
        <div class="card-body">
            <div class="table-responsive">

     <table align="center" width="100%">
        <tr valign="top">
            <td align="left" >
            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
	        <input id="h_NextLine" runat="server" type="hidden" /></td>
        </tr> 
        <tr valign="top">
            <td>
    <table align="center" width="100%">
            
        <tr>
            <td align="left" width="20%">
                <span class="field-label">File Name</span></td>
           
            <td align="left" width="30%">
                <asp:FileUpload id="uploadFile" runat="server"  EnableTheming="True" style="border-right: rgba(0,0,0,0.2) 1px solid; border-top: rgba(0,0,0,0.2) 1px solid; border-left: rgba(0,0,0,0.2) 1px solid; border-bottom: rgba(0,0,0,0.2) 1px solid">
                </asp:FileUpload>&nbsp;</td>
       
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span></td>
            
            <td align="left" width="30%">
                <asp:DropDownList id="ddlBSUnit" runat="server" >
                </asp:DropDownList></td>
            
        </tr>
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Acadamic Year</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlAcademicYear" runat="server"   SkinID="DropDownListNormal">
                </asp:DropDownList>
                </td>
            <td align="left" colspan="2"><asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload File" CausesValidation="False" 
                TabIndex="30"  /></td>
            
        </tr>

    </table>
    <br />
       <table align="center" width="100%">                         
        <tr> 
            <td  colspan ="4" class="title-bg" align ="left" ><asp:Label ID="labdetailHead" runat="server" EnableViewState="False">Excel Details</asp:Label></td>
                    </tr>
         <tr>
                <td >
                   <asp:GridView id="gvExcel" runat="server" Width="100%" EmptyDataText="No Accounts are Listing" 
                   CssClass="table table-bordered table-row" OnRowDataBound="gvExcel_RowDataBound" >
                        <RowStyle CssClass="griditem"  ></RowStyle>
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" ></EmptyDataRowStyle>
                       <footerstyle  />                   
                        <%--<SelectedRowStyle BackColor="Aqua" ></SelectedRowStyle>--%>
                        <HeaderStyle CssClass="gridheader_new"  ></HeaderStyle>                        
                        <%--<AlternatingRowStyle CssClass="griditem_alternative" ></AlternatingRowStyle>--%>
                    </asp:GridView>
                  
                   </td> </tr> </table> 
                    <br />
                    
                    <table align="center" width="100%" ><tr><td align="center"><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="26"  /></td></tr></table>
                   
    
            </td>
        </tr>
    </table>
    
                                <asp:HiddenField ID="h_Editid" runat="server" Value="0" />
                                <asp:HiddenField ID="HidUpload" runat="server" />
                                <asp:HiddenField ID="h_print" runat="server" />


                </div>
            </div>
        </div>
</asp:Content>

