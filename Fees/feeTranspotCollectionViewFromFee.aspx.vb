﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeTranspotCollectionViewFromFee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "feeTranspotCollectionNew.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300265" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "F300265"
                            lblHead.Text = "Transport Fee Collection"
                    End Select 
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim lstrOpr, str_Filter, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_RECNO", lstrCondn1)
                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_DATE", lstrCondn2)
                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)
                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn4)
                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)
                '   -- 6  txtAmount
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_AMOUNT", lstrCondn6)
                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_NARRATION", lstrCondn7)
            End If
            
            str_Sql = "SELECT * FROM FEES.VW_OSO_FEES_RECEIPT where " _
            & " isnull(FCL_bDELETED,0)=0 AND FCL_STU_BSU_ID='" & Session("sBSUID") & "'" & str_Filter _
            & " ORDER BY FCL_DATE DESC ,FCL_RECNO DESC,STU_NAME"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1
            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2
            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3
            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4
            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5
            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrCondn6
            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            Dim lblBSU_ID As Label = sender.Parent.parent.findcontrol("lblBSU_ID")
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
            "&bsu_id=" & Encr_decrData.Encrypt(lblBSU_ID.Text) & _
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
            "&stu_bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
            "&isexport=0"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub  

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub lbExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            Dim lblBSU_ID As Label = sender.Parent.parent.findcontrol("lblBSU_ID")
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
            "&bsu_id=" & Encr_decrData.Encrypt(lblBSU_ID.Text) & _
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
            "&stu_bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
            "&isexport=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

End Class
