<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Baddebit.aspx.vb" Inherits="Fees_Baddebit" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <style type="text/css">
        .hide {
            display: none;
        }
    </style>
    <script type="text/javascript" language="javascript">


        function AllowNumberOnly() {
            //called when key is pressed in textbox
            $('[custom-amount="true"]').keypress(function (evt) {
                var self = $(this);
                //if the letter is not digit then display error and don't type anything
                if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) {
                    //display error message
                    return false;
                }
            });
        }


        function uploadfile() {
            document.getElementById('<%=btnuploadfile.ClientID %>').click();

        }
        //function Numeric_Only() {
        //    //alert(event.keyCode)
        //    if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
        //        if (event.keyCode == 13 || event.keyCode == 45) { return false; }
        //        event.keyCode = 0
        //    }

        //}


        function UpdateSum() {
            var sum = 0.0;
            //iterate through each textboxes and add the values
            $('[custom-amount="true"]').each(function () {
                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                }
            });
            //.toFixed() method will roundoff the final sum to 2 decimal places
            $('[custom-value="true"]').text(sum.toFixed(2));

        }

        function valueCopy(Amnt) {
            $(Amnt).val($(Amnt).parent().prev().text());
        }

        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(2);
            return true;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Set Bad Debts Provision
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Ageing Days</span></td>
                        <td align="left" colspan="1" width="30%">
                            <asp:TextBox ID="txtAigingDays" runat="server"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmountToPay" runat="server" FilterType="Numbers"
                                TargetControlID="txtAigingDays" />
                        </td>
                        <td align="left" colspan="1" class="matters" width="20%"><span class="field-label">Date</span> </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Business Unit</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBSUnit_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" class="matters"><span class="field-label">InActive Students</span></td>
                        <td class="matters" style="text-align: left">
                            <asp:CheckBox ID="chkInActive" runat="server" Text="" /></td>
                    </tr>
                    <tr>
                        <td class=""><span class="field-label">File Upload</span></td>
                        <td class="" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel53" runat="server">
                                <ContentTemplate>
                                    <asp:FileUpload ID="FileUpload1" onchange="uploadfile()" runat="server"></asp:FileUpload>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnuploadfile" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:Button ID="btnuploadfile" Style="display: none" runat="server" CssClass="button" Text="Upload File" CausesValidation="False"
                                TabIndex="30" OnClick="btnuploadfile_Click" />
                        </td>

                        <td>
                            <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Find" CausesValidation="False" OnClick="btnFind_Click" />
                            <asp:Button ID="btnClear" runat="server" CssClass="button" Text="Clear" CausesValidation="False" OnClick="btnClear_Click" />
                        </td>
                    </tr>
                </table>


                <asp:HyperLink ID="lnkXcelFormat" runat="server">Click here to get the formatted Excel file</asp:HyperLink>
                <br />
                <br />
                <div class="container" runat="server" id="divDetails" visible="false">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Total Number of Students</th>
                                <th>Total Provision(AED)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Label ID="lbltotalstudents" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbltotaldue" custom-value="true" runat="server"></asp:Label></td>
                            </tr>
                        </tbody>
                    </table>

                </div>


                <table align="center" width="100%">
                    <tr class="title-bg">
                        <td colspan="4" align="left">Student Details</td>
                    </tr>
                    <tr>
                        <td class="matters" align="center" colspan="4">
                            <asp:GridView ID="gvStudentDetails" runat="server" Width="100%" EmptyDataText="No Details Added"
                                CssClass="table table-row table-bordered" EnableViewState="true" AutoGenerateColumns="False" ShowFooter="True">
                                <Columns>
                                    <%--<asp:BoundField DataField="STU_ID"   HeaderText="StuID"></asp:BoundField>--%>
                                    <asp:BoundField DataField="SLNO" HeaderText="S.No">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="STU_NO" HeaderText="Student ID">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="STU_NAME" HeaderText="Student Name">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GRM_DISPLAY" HeaderText="Grade">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="STU_LEAVEDATE" HeaderText="Leave Date">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AGEDUE" HeaderText="Due (AED)">
                                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Provision (AED)">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDAmount" custom-amount="true" runat="server" onfocusout="return UpdateSum()" ondblclick="valueCopy(this)" Style="direction: rtl" Text='<%# Bind("BDP_AMOUNT") %>'></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmountToPay" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtDAmount" />

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtPrvAmount" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                </table>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
    <asp:HiddenField ID="hdnfield" runat="server" Value="False" />
</asp:Content>

