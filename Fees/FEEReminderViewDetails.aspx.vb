﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CrystalDecisions.Shared
Partial Class Fees_FEEReminder
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Shared reportHeader As String

    Private Property iChecked() As Int16
        Get
            Return ViewState("iChecked")
        End Get
        Set(ByVal value As Int16)
            ViewState("iChecked") = value
        End Set
    End Property
    Private Property ACD_ID() As Integer
        Get
            Return ViewState("ACD_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("ACD_ID") = value
        End Set
    End Property
    Private Property FRH_ID() As Integer
        Get
            Return ViewState("FRH_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("FRH_ID") = value
        End Set
    End Property
    Private Property IsAutoReminder() As Boolean
        Get
            Return ViewState("bAutoReminder")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bAutoReminder") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEReminder.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
               
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REMINDER And ViewState("MainMnu_code") <> "F300400") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                    
                End If
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtAsOnDate.Attributes.Add("ReadOnly", "ReadOnly")
                txtDate.Attributes.Add("ReadOnly", "ReadOnly")
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                iChecked = 0 : FRH_ID = 0 : IsAutoReminder = False
                If ViewState("datamode") = "view" Then
                    FRH_ID = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
                    Dim vFEE_REM As FEEReminder = FEEReminder.GetHeaderDetails(FRH_ID)
                    If vFEE_REM Is Nothing Then
                        Exit Sub
                    End If
                    txtAsOnDate.Text = Format(vFEE_REM.FRH_ASONDATE, OASISConstants.DateFormat)
                    txtDate.Text = Format(vFEE_REM.FRH_DT, OASISConstants.DateFormat)
                    lblReminderType.Text = vFEE_REM.ReminderLevel 'GerReminderType(vFEE_REM.FRH_Level)
                    lblReminderTitle.Text = vFEE_REM.FRH_REMARKS
                    lblAcademicYear.Text = vFEE_REM.FRH_ACY_DESCR
                    ACD_ID = vFEE_REM.FRH_ACD_ID
                    IsAutoReminder = vFEE_REM.IsAutoReminder
                    GridBind()

                    Dim bArabic As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(RMD_bARABIC, 0) FROM dbo.REMINDERTYPE_M WITH(NOLOCK) INNER JOIN FEES.FEE_REMINDER_H WITH(NOLOCK) ON FRH_Level =RMD_ID   WHERE FRH_ID=" & FRH_ID.ToString & "")
                    If IsNothing(bArabic) Then
                        bArabic = False
                    End If
                    ' bArabic = True
                    If bArabic Then
                        Dim smScriptManager As New ScriptManager
                        smScriptManager = Master.FindControl("ScriptManager1")
                        smScriptManager.RegisterPostBackControl(btnPrint)
                    End If
                    h_IsArabic.Value = bArabic

                End If

                If ViewState("MainMnu_code") = "F300400" Then
                    btnPrint.Visible = False
                    btnSave.Visible = False

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If

    End Sub

    Public Function GerReminderType(ByVal typ As Int16) As String
        Dim str_Sql As String = "SELECT ISNULL(RMD_DESCR,'') FROM REMINDERTYPE_M WITH(NOLOCK) WHERE RMD_ID =" & typ
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
    End Function

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEReminder.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEReminder.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub ClearSubGrid()
        gvSTU_RemDetails.DataSource = Nothing
        gvSTU_RemDetails.DataBind()
    End Sub

    Sub GridBind()
        Try
            ClearSubGrid()
            gvFEEReminder.SelectedIndex = -1
            Dim ds As New DataSet
            Dim str_Filter, lstrOpr, str_Sql, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            str_Filter = ""
            If gvFEEReminder.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)

                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)

            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            Dim str_cond As String = String.Empty
            Dim asOnDate As Date = CDate(txtAsOnDate.Text)

            If radSent.Checked Then
                str_cond = str_cond & " AND isnull(FRD_Email,0) =2 "
            End If
            If radNotSent.Checked Then
                str_cond = str_cond & " AND isnull(FRD_Email,0) =0 "
            End If
            If radEmailProcessed.Checked Then
                str_cond = str_cond & " AND isnull(FRD_Email,0) =1 AND isnull(FRD_Fetched,0) =0 "
            End If
            If radEmailFailed.Checked Then
                str_cond = str_cond & " AND isnull(FRD_Email,0) =1 AND isnull(FRD_Fetched,0) =1 "
                gvFEEReminder.Columns(7).Visible = True
            Else
                gvFEEReminder.Columns(7).Visible = False
            End If
            'str_cond += " AND FEES.FEESCHEDULE.FSH_BSU_ID='" & Session("sBsuid") & "' " & str_Filter
            str_Sql = "SELECT CAST(" & iChecked & " AS BIT) AS CHKD,FEES.VW_OSO_STUDENT_DETAILS.STU_NO, FEES.VW_OSO_STUDENT_DETAILS.STU_NAME, " & _
           "FEES.FEE_REMINDER_D.FRD_FRH_ID, A.ACY_DESCR,  " & _
           "FEES.FEE_REMINDER_D.FRD_STU_ID, SUM(FEES.FEE_REMINDER_D.FRD_DUE_AMT) AS 'DUE_AMT',FRD_EmailStatus,  " & _
           " SUM(FEES.FEE_REMINDER_D.FRD_ADV_AMT) AS 'ADV_AMT',  " & _
           "FEES.FEE_REMINDER_D.FRD_REMARKS, B.SCT_DESCR, C.GRM_DISPLAY " & _
           "FROM FEES.FEE_REMINDER_D WITH ( NOLOCK ) INNER JOIN FEES.VW_OSO_STUDENT_DETAILS ON  " & _
           "FEES.FEE_REMINDER_D.FRD_STU_ID = FEES.VW_OSO_STUDENT_DETAILS.STU_ID  " & _
           "INNER JOIN OASIS..ACADEMICYEAR_D WITH ( NOLOCK ) INNER JOIN OASIS..ACADEMICYEAR_M A WITH ( NOLOCK ) ON  " & _
           " ACD_ACY_ID =  ACY_ID ON FEES.FEE_REMINDER_D.FRD_ACD_ID = ACD_ID  " & _
           "INNER JOIN OASIS..SECTION_M B WITH ( NOLOCK ) ON B.SCT_ID = FEES.FEE_REMINDER_D.FRD_SCT_ID " & _
           "INNER JOIN OASIS..GRADE_BSU_M C WITH ( NOLOCK ) ON C.GRM_ID = SCT_GRM_ID " & _
           " WHERE FRD_ACD_ID = " & ACD_ID & " AND FRD_FRH_ID = " & FRH_ID & str_Filter & str_cond & _
           " GROUP BY FEES.VW_OSO_STUDENT_DETAILS.STU_NO, FEES.VW_OSO_STUDENT_DETAILS.STU_NAME,  " & _
           "FEES.FEE_REMINDER_D.FRD_FRH_ID, FEES.FEE_REMINDER_D.FRD_STU_ID, " & _
           " A.ACY_DESCR, FEES.FEE_REMINDER_D.FRD_REMARKS, B.SCT_DESCR, C.GRM_DISPLAY,FRD_EmailStatus" & _
           " ORDER BY C.GRM_DISPLAY, B.SCT_DESCR, FEES.VW_OSO_STUDENT_DETAILS.STU_NAME"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvFEEReminder.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEReminder.DataBind()
                Dim columnCount As Integer = gvFEEReminder.Rows(0).Cells.Count
                gvFEEReminder.Rows(0).Cells.Clear()
                gvFEEReminder.Rows(0).Cells.Add(New TableCell)
                gvFEEReminder.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEReminder.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEReminder.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEReminder.DataBind()
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEReminder.PageIndexChanging
        gvFEEReminder.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click
        iChecked = 1
        GridBind()
    End Sub
    Protected Sub btnDeselectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeselectAll.Click
        iChecked = 0
        GridBind()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_redirect As String = "FeeReminderWithAging.aspx" & "?MainMnu_code=" & _
         Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
        Response.Redirect(str_redirect)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub lnkViewSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSTU_ID As New Label
        Dim i As Integer = sender.parent.parent.RowIndex
        gvFEEReminder.SelectedIndex = i
        lblSTU_ID = TryCast(sender.FindControl("lblSTU_ID"), Label)
        If lblSTU_ID IsNot Nothing Then
            GridBindDetails(lblSTU_ID.Text)
        End If
    End Sub

    Sub GridBindDetails(ByVal STU_ID As Integer)
        Try
            Dim ds As New DataSet
            Dim str_Sql As String = String.Empty
            str_Sql = " SELECT FEES.FEE_REMINDER_D.FRD_FRH_ID, ACY_DESCR," & _
            " FEES.FEE_REMINDER_D.FRD_DUE_AMT FRD_AMOUNT, FEES.FEE_REMINDER_D.FRD_REMARKS, " & _
            " FEES.FEE_REMINDER_D.FRD_STU_ID, FEES.FEES_M.FEE_DESCR, FEES.FEE_REMINDER_D.FRD_bExclude" & _
            " FROM FEES.FEE_REMINDER_D WITH(NOLOCK) INNER JOIN" & _
            " FEES.FEES_M WITH(NOLOCK) ON FEES.FEE_REMINDER_D.FRD_FEE_ID = FEES.FEES_M.FEE_ID" & _
            " INNER JOIN OASIS..ACADEMICYEAR_D WITH(NOLOCK) ON  ACD_ID = FEES.FEE_REMINDER_D.FRD_ACD_ID" & _
            " INNER JOIN OASIS..ACADEMICYEAR_M WITH(NOLOCK)  ON  ACY_ID = ACD_ACY_ID  " & _
            " WHERE FRD_FRH_ID = " & FRH_ID & " AND FEES.FEE_REMINDER_D.FRD_STU_ID = " & STU_ID
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvSTU_RemDetails.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEReminder.DataBind()
                Dim columnCount As Integer = gvFEEReminder.Rows(0).Cells.Count
                gvSTU_RemDetails.Rows(0).Cells.Clear()
                gvSTU_RemDetails.Rows(0).Cells.Add(New TableCell)
                gvSTU_RemDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSTU_RemDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSTU_RemDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvSTU_RemDetails.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim chkControl As New CheckBox
        Dim STU_IDs As String = ""
        For Each grow As GridViewRow In gvFEEReminder.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl.Checked Then
                STU_IDs &= IIf(STU_IDs <> "", "|", "") & DirectCast(grow.FindControl("lblSTU_ID"), Label).Text.ToString
            End If
        Next
        If STU_IDs = "" Then
            STU_IDs = "0"
        End If
        'ltrlHtml.Mode = LiteralMode.Encode
        If IsAutoReminder Then
            h_print.Value = "print_auto"
            hfStuIds.Value = Encr_decrData.Encrypt(STU_IDs)
            hfFrhId.Value = Encr_decrData.Encrypt(FRH_ID)
        Else
            If h_IsArabic.Value.ToString.ToLower = "false" Then
                Session("ReportSource") = FEEReminder.PrintReminder(FRH_ID, Session("sUsr_name"), True, STU_IDs)
                h_print.Value = "print"
            Else
                'Session("ReportSource") = FEEReminder.PrintReminderArabic(FRH_ID, Session("sUsr_name"), True, STU_IDs)
                'h_print.Value = "print"
                ExportArabicReminder(FRH_ID, Session("sUsr_name"), True, STU_IDs)
            End If
        End If


    End Sub
    Public Function ExportArabicReminder(ByVal vFRH_ID As Integer, ByVal Usr_name As String, Optional ByVal Fees As Boolean = True, Optional ByVal vSTU_IDs As String = "") As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        If Fees = False Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        End If

        Dim cmd As New SqlCommand("[FEES].[F_GETReminderLetter]", New SqlConnection(str_conn))
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRH_ID As New SqlParameter("@FRH_ID", SqlDbType.Int)
        sqlpFRH_ID.Value = vFRH_ID
        cmd.Parameters.Add(sqlpFRH_ID)

        Dim sqlpSTU_ID As New SqlParameter("@FRD_STU_ID", SqlDbType.VarChar)
        sqlpSTU_ID.Value = vSTU_IDs
        cmd.Parameters.Add(sqlpSTU_ID)


        Dim ds As New DataSet
        Dim _adapter As New SqlDataAdapter(cmd)
        _adapter.Fill(ds)

        'Dim repSourceSubRep(1) As MyReportClass
        'repSourceSubRep(0) = New MyReportClass

        'Dim cmdSubColln As New SqlCommand

        ' ''SUBREPORT1
        'cmdSubColln.CommandText = "EXEC [FEES].F_GETReminderLetterDetail " & vFRH_ID & ",'" & vSTU_IDs & "'"
        'cmdSubColln.Connection = New SqlConnection(str_conn)
        'cmdSubColln.CommandType = CommandType.Text
        'repSourceSubRep(0).Command = cmdSubColln

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        'repSource.SubReport = repSourceSubRep
        params("UserName") = Usr_name
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If Fees = True Then
            repSource.ReportUniqueName = "ArabicReminder"
            Dim BSUID As String
            BSUID = Session("sBsuid")
            If h_IsArabic.Value.ToLower = "true" Then
                repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT/rptFeeReminder_Arabic.rpt")
                If Not repSource Is Nothing Then
                    Dim repClassVal As New RepClass
                    repClassVal = New RepClass
                    repClassVal.ResourceName = repSource.ResourceName
                    repClassVal.SetDataSource(ds.Tables(0))
                    If repSource.IncludeBSUImage Then
                        If repSource.HeaderBSUID Is Nothing Then
                            IncludeBSUmage(repClassVal, BSUID)
                        Else
                            IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                        End If

                    End If
                    SubReport(repSource, repSource, repClassVal)
                    Dim pdfFilePath As String
                    Dim pdfFileName As String
                    pdfFileName = "ArabicReminder.pdf"

                    pdfFilePath = WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
                    '  pdfFilePath = "C:\0Junk\"
                    pdfFilePath += pdfFileName

                    repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                    Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                    While ienum.MoveNext()
                        repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                    End While
                    Try
                        If System.IO.File.Exists(pdfFilePath) Then
                            System.IO.File.Delete(pdfFilePath)
                        End If
                    Catch ex As Exception

                    End Try

                    repClassVal.ExportToDisk(ExportFormatType.PortableDocFormat, pdfFilePath)

                    Dim BytesData As Byte()
                    BytesData = ConvertFiletoBytes(pdfFilePath)
                    Dim ContentType, Extension As String
                    ContentType = "application/pdf"
                    Extension = GetFileExtension(ContentType)
                    Dim Title As String = "Receipt"
                    If repSource.ReportUniqueName.ToString.Split("_").Length > 1 Then
                        Title = repSource.ReportUniqueName.ToString.Split("_")(1)
                    End If
                    DownloadFile(System.Web.HttpContext.Current, BytesData, ContentType, Extension, Title, pdfFileName)
                    repClassVal.Close()
                    repClassVal.Dispose()
                    repSource = Nothing
                    repClassVal = Nothing
                End If
            End If
        End If

        Return repSource
    End Function
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String, ByVal FileName As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.End()
    End Sub
    Private Shared Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Shared Sub SubReport(ByVal repSource As MyReportClass, ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(repSource, subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            reportHeader = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Shared Sub IncludeBSUmage(ByRef repClassVal As RepClass, ByVal BSUID As String)
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        'Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        Dim cmd As New SqlCommand("ReportHeader_Subreport", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = BSUID
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_Sub_Landscape.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_Sub_Landscape.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub
    Protected Sub btnEMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEMail.Click
        Dim chkControl As New CheckBox
        For Each grow As GridViewRow In gvFEEReminder.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl.Checked Then
                If Not doEMail(DirectCast(grow.FindControl("lblSTU_ID"), Label).Text) Then
                    Exit Sub
                End If
            End If
        Next
        'lblError.Text = "Email Processed for Reminder"
        usrMessageBar.ShowNotification("Email Processed for Reminder", UserControls_usrMessageBar.WarningType.Success)
        GridBind()
    End Sub
    Private Function doEMail(ByVal STU_ID As String) As Boolean
        Dim sqlParam(7) As SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        sqlParam(0) = Mainclass.CreateSqlParameter("@FRD_STU_ID", STU_ID, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@FRH_ID", FRH_ID, SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@AUD_WINUSER", Page.User.Identity.Name.ToString(), SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@Aud_form", Master.MenuName.ToString(), SqlDbType.VarChar)
        sqlParam(5) = Mainclass.CreateSqlParameter("@Aud_user", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(6) = Mainclass.CreateSqlParameter("@Aud_module", Session("sModule"), SqlDbType.VarChar)
        sqlParam(7) = Mainclass.CreateSqlParameter("@v_ReturnMsg", Session("sModule"), SqlDbType.VarChar, True)
        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "[dbo].[EMailFeeReminders]", sqlParam)
        If str_success <> 0 Then
            'lblError.Text = sqlParam(7).Value
            usrMessageBar.ShowNotification(sqlParam(7).Value, UserControls_usrMessageBar.WarningType.Danger)
            doEMail = False
        Else
            doEMail = True
        End If
    End Function
    Protected Sub chkEMail_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSent.CheckedChanged, radEmailProcessed.CheckedChanged, radAll.CheckedChanged, radNotSent.CheckedChanged, radEmailFailed.CheckedChanged
        GridBind()
    End Sub
End Class
