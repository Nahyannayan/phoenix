Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports GemBox.Spreadsheet
Partial Class Fees_ImportFeeExcel
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
     
    'Private Property VSgvExcelImport() As DataTable
    '    Get
    '        Return ViewState("gvExcelImport")
    '    End Get
    '    Set(ByVal value As DataTable)
    '        ViewState("gvExcelImport") = value
    '    End Set
    'End Property
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New AjaxControlToolkit.ToolkitScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImport)
        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            'lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

        End If
    End Sub 
    Protected Sub gvExcelImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExcelImport.PageIndexChanging
        Me.gvExcelImport.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    'Protected Sub gvImportSmry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvImportSmry.PageIndexChanging
    '    Me.gvImportSmry.PageIndex = e.NewPageIndex
    '    ImportAdjustments(False)
    'End Sub

    Protected Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        Dim conn As New OleDbConnection
        'Me.lblError.Text = ""
        Dim errString As String = ""
        Try
            Dim str_conn As String = ""
            Dim cmd As New OleDbCommand
            Dim da As New OleDbDataAdapter
            Dim ds As DataSet
            Dim dtXcel As DataTable
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                    str_conn = IIf(strFileType = ".xls", "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties=""Excel 8.0;HDR=YES;""" _
                                    , "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2""")
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    errString = "Only Excel files are allowed" 
                    Exit Try
                End If

                Dim Query = "SELECT * FROM [ENTEBE]"
                conn = New OleDbConnection(str_conn)
                'If conn.State = ConnectionState.Closed Then conn.Open()
                'cmd = New OleDbCommand(Query, conn)
                'da = New OleDbDataAdapter(cmd)
                'ds = New DataSet()
                'da.Fill(ds)
                'If conn.State = ConnectionState.Open Then
                '    conn.Close()
                'End If
                'dtXcel = ds.Tables(0)
                dtXcel = FetchFromExcelIntoDataTable_Fee(FileName, 1, 1, 12)
                If dtXcel.Rows.Count > 0 Then
                    'VSgvExcelImport = dtXcel 
                    ValidateAndSave(dtXcel)
                Else
                    'Me.lblError.Text += "Empty Document!!"
                    errString = errString + "Empty Document!!" 
                End If

            Else
                'Me.lblError.Text += "File not Found!!"
                errString = errString + "File not Found!!" 
                Me.FileUpload1.Focus()
            End If

            File.Delete(FileName) 'delete the file after copying the contents
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            'Me.lblError.Text += Err.Description
            errString = errString + Err.Description
            usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Public Shared Function CheckIfArrayRowBlank(ByVal xlsRow As ExcelRow, Optional ByVal MaxCol As Int16 = -1) As Boolean
        Dim i, colCount As Int16
        If MaxCol = -1 Then MaxCol = xlsRow.AllocatedCells.Count
        colCount = 0
        For i = 0 To MaxCol - 1
            If xlsRow.Cells(i).Value Is Nothing Then colCount += 1
        Next
        If colCount = MaxCol Then
            CheckIfArrayRowBlank = True
        Else
            CheckIfArrayRowBlank = False
        End If
    End Function
    Public Shared Function FetchFromExcelIntoDataTable_Fee(ByVal FileName As String, ByVal SheetNo As Int16, ByVal StartingRowNo As Int16, Optional ByVal MaxCol As Int16 = 100) As DataTable
        Dim iRowRead As Boolean
        iRowRead = True
        Dim iRow, iCol, i As Int32
        iRow = StartingRowNo - 1
        Dim mTable As New DataTable
        Dim mDataRow As DataRow
        Dim mObj As ExcelRowCollection
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        ef = ExcelFile.Load(FileName)
        Try
            'ef.LoadXls(FileName)
            ' Dim ef = ExcelFile.Load(FileName)
            Dim mRowObj As ExcelRow
            mObj = ef.Worksheets(SheetNo - 1).Rows
            iCol = 0
            Dim NewColName As String
            While iRowRead
                mRowObj = mObj(iRow)
                If Not CheckIfArrayRowBlank(mRowObj, MaxCol) Then
                    If StartingRowNo = iRow + 1 Then
                        While iCol <= MaxCol    ' Create the columns
                            If mRowObj.Cells(iCol).Value <> "" And Not mRowObj.Cells(iCol).Value Is Nothing Then
                                NewColName = mRowObj.Cells(iCol).Value
                                NewColName = Replace(NewColName, " ", "")
                                NewColName = Replace(NewColName, ",", "")
                                NewColName = Replace(NewColName, ".", "")
                                mTable.Columns.Add(NewColName, System.Type.GetType("System.String"))
                            End If
                            iCol += 1
                        End While
                        iRow += 1
                    Else
                        mDataRow = mTable.NewRow
                        For i = 0 To MaxCol - 1
                            If Not mRowObj.Cells(i).Value Is Nothing Then mDataRow(i) = mRowObj.Cells(i).Value
                        Next
                        mTable.Rows.Add(mDataRow)
                        iRow += 1
                    End If
                Else
                    iRowRead = False
                End If
            End While
            mTable.AcceptChanges()
            FetchFromExcelIntoDataTable_Fee = mTable
        Catch ex As Exception

        Finally
            ef = Nothing
        End Try
    End Function
    

    Private Sub ValidateAndSave(ByRef DT As DataTable)
        Dim pParms(11) As SqlClient.SqlParameter
        Dim ErrorLog As String = ""
         
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)

        Dim qry As String = "[FEES].[GET_IMPORT_RECIPT_EXCEL_BATCH_M]"
        Dim FAI_BATCHNO As Integer = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, qry)
        ViewState("FAI_BATCHNO") = FAI_BATCHNO
         
        DT.Columns.Add(New DataColumn() With {.ColumnName = "batchno",
                                              .DataType = GetType(Integer),
                                              .DefaultValue = ViewState("FAI_BATCHNO")})
        DT.Columns.Add(New DataColumn() With {.ColumnName = "BSU_ID",
                                             .DataType = GetType(String),
                                             .DefaultValue = Session("sBsuid")})
        DT.AcceptChanges()

        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
        objConn.Open()
        Dim Success As Boolean = True
        ' Dim stTrans As SqlTransaction = objConn.BeginTransaction

       
        Dim bulk As SqlBulkCopy = New SqlBulkCopy(objConn)
        bulk.ColumnMappings.Add("SchoolName", "IRE_SCHOOL_NAME")
        bulk.ColumnMappings.Add("StudentID", "IRE_STUDENT_ID")
        bulk.ColumnMappings.Add("NationalID", "IRE_NATIONAL_ID")

        bulk.ColumnMappings.Add("STUDENTNAME", "IRE_STUDENT_NAME")
        bulk.ColumnMappings.Add("Date", "IRE_FEE_DATE")
        bulk.ColumnMappings.Add("FEETYPE", "IRE_FEE_TYPE")

        bulk.ColumnMappings.Add("FeeAmount", "IRE_FEE_AMOUNT")
        bulk.ColumnMappings.Add("Modeofpayment", "IRE_MODE_OF_PAYMENT")
        bulk.ColumnMappings.Add("Transactionrefno#", "IRE_TRANSACTION_REF_NO")

        bulk.ColumnMappings.Add("Narration", "IRE_NARRATION")
        bulk.ColumnMappings.Add("Bank", "IRE_BANK")
        bulk.ColumnMappings.Add("Receiptno", "IRE_RECEIPT_NO")
        bulk.ColumnMappings.Add("batchno", "IRE_BATCHNO")
        bulk.ColumnMappings.Add("BSU_ID", "IRE_BSU_ID")


        bulk.DestinationTableName = "[FEES].[IMPORT_RECEIPT_EXCEL]"
        bulk.WriteToServer(DT)

        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
         
        FillGridView()

    End Sub
    Protected Sub FillGridView()
        Try
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            Dim pParams(0) As SqlParameter
            pParams(0) = New SqlParameter("@BatchNo", ViewState("FAI_BATCHNO"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(objConn, "[FEES].[GET_FEES_IMPORT_BY_BATCH_NO]", pParams)
            If Not ds Is Nothing And ds.Tables.Count > 0 Then
                Dim dt As DataTable = ds.Tables(0)
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.trGvImport.Visible = True

                    Me.gvExcelImport.DataSource = dt
                    Me.gvExcelImport.DataBind()
                    If dt.Rows(0)("Has_Error") = "1" Then
                        Me.lblMessage.Text = "Kindly verify the error"
                        Me.btnProceedImpt.Visible = False
                        Me.btnImport.Visible = True
                        trgvImportSmry.Visible = False
                    Else
                        Me.lblMessage.Text = "Click Proceed to Import the data"
                        Me.btnProceedImpt.Visible = True
                        Me.btnImport.Visible = False
                        Me.btnCommitImprt.Visible = False
                        trgvImportSmry.Visible = False
                    End If 
                    'Me.lblMessage.Text = "Click Proceed to Import the data"
                Else
                    Me.lblMessage.Text = ""
                    Me.trGvImport.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnProceedImpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceedImpt.Click
        ImportAdjustments(False)
    End Sub

    Protected Sub btnCommitImprt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitImprt.Click
        ImportAdjustments(True)
    End Sub


    Private Sub ImportAdjustments_old(ByVal Commit As Boolean)
        Try
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            Dim pParams(1) As SqlParameter
            pParams(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            pParams(1) = New SqlParameter("@BatchNo", ViewState("FAI_BATCHNO"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(objConn, "[FEES].[Validate_import_summary]", pParams)
            If Not ds Is Nothing And ds.Tables.Count > 0 Then
                Dim dt As DataTable = ds.Tables(7)
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.btnCommitImprt.Visible = True
                    Me.trGvImport.Visible = False
                    Me.gvImportSmry.DataSource = dt
                    Me.gvImportSmry.DataBind()
                Else
                    Me.btnCommitImprt.Visible = False
                    Me.trGvImport.Visible = True
                End If
            End If
        Catch ex As Exception
            Me.lblMessage.Text = ex.Message
        End Try
        Me.btnProceedImpt.Visible = False
        Me.btnImport.Visible = True
    End Sub

    Private Sub ImportAdjustments(ByVal Commit As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(4) As SqlParameter
        Dim sqlParam2(1) As SqlParameter

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand
            cmd.Dispose()
            cmd = New SqlCommand("FEES.VALIDATE_IMPORT_SUMMARY_FIRST", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            Dim retval As String = ""
            sqlParam(0) = New SqlParameter("@ReturnValue", retval)
            sqlParam(0).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = New SqlParameter("@BATCHNO", ViewState("FAI_BATCHNO"))
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            cmd.Parameters.Add(sqlParam(2))


            'Dim pParams1(4) As SqlParameter
            'pParams1(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            'pParams1(1) = New SqlParameter("@BatchNo", ViewState("FAI_BATCHNO"))
            'pParams1(2) = New SqlParameter("@ReturnValue", retval)
            'pParams1(2).Direction = ParameterDirection.Output
            'SqlHelper.ExecuteScalar(stTrans, CommandType.StoredProcedure, "FEES.VALIDATE_IMPORT_SUMMARY_FIRST", pParams1)

            'Mainclass.ExecuteParamQRY(objConn, stTrans, "IMPORT_FEE_ADJUSTMENTS", sqlParam)
            cmd.ExecuteNonQuery()
            retval = sqlParam(0).Value
            'Me.lblError.Text += retval

            If retval Is Nothing Or retval = "" Or retval = "0" Then
                Dim pParams(1) As SqlParameter
                pParams(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                pParams(1) = New SqlParameter("@BatchNo", ViewState("FAI_BATCHNO"))
                dsSummary = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "FEES.GET_VALIDATE_IMPORT_SUMMARY_FIRST", pParams)
                If dsSummary.Tables(0).Rows.Count > 0 Then
                    Me.btnCommitImprt.Visible = True
                    Me.trGvImport.Visible = False
                    Me.gvImportSmry.DataSource = dsSummary.Tables(0)
                    Me.gvImportSmry.DataBind()
                    trgvImportSmry.Visible = True
                Else
                    Me.btnCommitImprt.Visible = False
                    Me.trGvImport.Visible = True
                    trgvImportSmry.Visible = False
                End If
            End If

            If Commit = True Then
                Me.lblMessage.Text = ""
                stTrans.Commit()
                'ClearAllFields()
                Me.lblMessage.Text = "Data Saved successfully"
                btnCommitImprt.Visible = False
                'usrMessageBar2.ShowNotification("Data Saved successfully", UserControls_usrMessageBar.WarningType.Success)
            Else
                Me.lblMessage.Text = "Click the Commit button to commit the transaction"
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblError.Text = ex.Message
            'usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Me.lblMessage.Text = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Me.btnProceedImpt.Visible = False
        Me.btnImport.Visible = True
    End Sub
End Class
