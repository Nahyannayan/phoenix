Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GemBox.Spreadsheet
Imports Telerik.Web
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Threading.Tasks

Partial Class Fees_FeeAdjustmentBulk
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass
    Dim AegAmount As Double
    Dim SetAmount As Double
    Dim SerialNo As Int32
    Public Shared Has_Error As Boolean
    Private Property VSgvExcelImport() As DataTable
        Get
            Return ViewState("gvExcelImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvExcelImport") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImport)
        If (rblAdjFor.SelectedValue = "T") Then
            hdrblAdjFor.Value = "TPT_ADJ"
        ElseIf (rblAdjFor.SelectedValue = "F") Then
            hdrblAdjFor.Value = "FEES_ADJ"
        ElseIf (rblAdjFor.SelectedValue = "S") Then
            hdrblAdjFor.Value = "SRV_ADJ"
        End If

        lnkXcelFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("ADJUSTMENT") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/" & If(rblAdjFor.SelectedValue = "T", "TransportAdjustmentsFormatFile", "FeeAdjustmentsFormatFile") & ".xls")
        'smScriptManager.RegisterPostBackControl(chkSelectAllBsu)

        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> "F100176" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            txtAdjDT.Text = Date.Now.ToString("dd/MMM/yyyy")
            BindBusinessUnitForAdjustment()
            FillAdjReason()

        End If

        'BindFeeTypeAccessBsu()
    End Sub
    Sub FillAdjReason()
        Dim dtReason As DataTable = FEEADJUSTMENTREQ.GET_ADJ_REASON(Session("sBsuId"))
        If Not dtReason Is Nothing Then
            ddlAdjType.DataSource = dtReason
            ddlAdjType.DataTextField = "ARM_DESCR"
            ddlAdjType.DataValueField = "ARM_ID"
            ddlAdjType.DataBind()
        End If
    End Sub
    Protected Sub btnPostback_Click(sender As Object, e As EventArgs)
        If Me.h_STATUS.Value = "0" Then
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            '    ddlAcademicYear.SelectedItem.Value & " - " & txtFrom.Text & " - " & ddBusinessunit.SelectedItem.Value, _
            '    "Insert", Page.User.Identity.Name.ToString, Me.Page)
            Dim errormsg As String = h_errormessage.Value
            Dim IsSuccess As String = h_IsSuccess.Value

            'Clear_All()
            Me.lblMessage.Text = ""
            Me.btnCommitImprt.Visible = False
            Me.btnProceedImpt.Visible = False
            Me.btnImport.Visible = True
            Me.btnAddAdj.Visible = True

            'Me.lblError.Text = errormsg
            usrMessageBar.ShowNotification(errormsg, If(IsSuccess = "1", UserControls_usrMessageBar.WarningType.Success, UserControls_usrMessageBar.WarningType.Danger))
            h_Processeing.Value = ""
            h_IsSuccess.Value = 0
        End If
    End Sub
    Protected Sub gvExcelImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExcelImport.PageIndexChanging
        Me.gvExcelImport.PageIndex = e.NewPageIndex
        BindExcel()
    End Sub
    Sub BindExcel()
        Me.gvExcelImport.DataSource = VSgvExcelImport
        Me.gvExcelImport.DataBind()
    End Sub
    Sub BindBusinessUnitForAdjustment()
        Dim ds As New DataSet
        Dim str_sql As String = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW, 0) = 1 ORDER BY BSU_NAME"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataTextField = "BSU_NAMEwithShort"
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataBind()
        ddlBUnit.SelectedIndex = -1
        If Not ddlBUnit.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub rblAdjFor_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindBusinessUnitForAdjustment()
        If (rblAdjFor.SelectedValue = "T") Then
            hdrblAdjFor.Value = "TPT_ADJ"
            btnCancelAdj_Click(Nothing, Nothing)
            BSUDDLTD.Visible = False
            BSUTD.Visible = False
            TdHideForBSU1.Visible = True
            TdHideForBSU2.Visible = True
        Else
            If (rblAdjFor.SelectedValue = "F") Then
                hdrblAdjFor.Value = "FEES_ADJ"
            Else
                hdrblAdjFor.Value = "SRV_ADJ"
            End If
            BSUDDLTD.Visible = True
            BSUTD.Visible = True
            TdHideForBSU1.Visible = False
            TdHideForBSU2.Visible = False

        End If
    End Sub
    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Me.gvImportSmry.DataSource = Nothing
        Me.gvImportSmry.DataBind()
        VSgvExcelImport = Nothing
        BindExcel()
        ImportAdjustmentSheet()
        'UpLoadDBF()
        'Dim conn As New OleDbConnection
        'Me.lblError.Text = ""
        'Try
        '    Dim str_conn As String = ""
        '    Dim cmd As New OleDbCommand
        '    Dim da As New OleDbDataAdapter
        '    'Dim ds As DataSet
        '    Dim dtXcel As DataTable
        '    Dim FileName As String = ""
        '    If (FileUpload1.HasFile) Then
        '        Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
        '        FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
        '        If strFileType = ".xls" Or strFileType = ".xlsx" Then
        '            If File.Exists(FileName) Then
        '                File.Delete(FileName)
        '            End If
        '            Me.FileUpload1.SaveAs(FileName)
        '            str_conn = IIf(strFileType = ".xls", "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties=""Excel 8.0;HDR=YES;""" _
        '                            , "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2""")
        '        Else
        '            Me.lblError.Text = "Only Excel files are allowed"
        '            Exit Try
        '        End If

        '        Dim Query = "SELECT * FROM [Sheet1$]"
        '        conn = New OleDbConnection(str_conn)
        '        'If conn.State = ConnectionState.Closed Then conn.Open()
        '        'cmd = New OleDbCommand(Query, conn)
        '        'da = New OleDbDataAdapter(cmd)
        '        'ds = New DataSet()
        '        'da.Fill(ds)
        '        'If conn.State = ConnectionState.Open Then
        '        '    conn.Close()
        '        'End If
        '        'dtXcel = ds.Tables(0)
        '        dtXcel = Mainclass.FetchFromExcelIntoDataTable(FileName, 1, 1, 9)
        '        If dtXcel.Rows.Count > 0 Then
        '            VSgvExcelImport = dtXcel
        '            BindExcel()
        '            ValidateAndSave(dtXcel)
        '        Else
        '            Me.lblError.Text += "Empty Document!!"
        '        End If

        '    Else
        '        Me.lblError.Text += "File not Found!!"
        '        Me.FileUpload1.Focus()
        '    End If

        '    File.Delete(FileName) 'delete the file after copying the contents
        'Catch
        '    If conn.State = ConnectionState.Open Then
        '        conn.Close()
        '    End If
        '    Me.lblError.Text += Err.Description
        'End Try
    End Sub

    Protected Sub btnProceedImpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceedImpt.Click
        Try
            Dim AdjDt As Date = CDate(txtAdjDT.Text)
        Catch ex As Exception
            'lblError.Text = "Invalid adjustment date"
            usrMessageBar.ShowNotification("Invalid adjustment date", UserControls_usrMessageBar.WarningType.Danger)
            txtAdjDT.Focus()
            Exit Sub
        End Try
        Dim str_conn As String = IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, If(rblAdjFor.SelectedValue = "T", ConnectionManger.GetOASISTRANSPORTConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString))
        Using con As New SqlConnection(str_conn)
            con.Open()
            Dim stTrans As SqlTransaction = con.BeginTransaction
            Try
                Using cmd As New SqlCommand("DataImport.IMPORT_BULK_ADJUSTMENT", con, stTrans)
                    cmd.CommandTimeout = 0
                    cmd.CommandType = CommandType.StoredProcedure
                    Dim sqlParam(9) As SqlParameter
                    Dim dtParam As DataTable = VSgvExcelImport
                    If dtParam.Columns.Contains("bVALID") Then
                        dtParam.Columns.Remove("bVALID")
                    End If
                    If dtParam.Columns.Contains("ERROR_MSG") Then
                        dtParam.Columns.Remove("ERROR_MSG")
                    End If
                    Dim drlist As New List(Of DataRow)()
                    If (dtParam.Columns.Contains("SLNO")) Then
                        dtParam.Columns.Remove("SLNO")
                    End If

                    dtParam.AcceptChanges()
                    sqlParam(0) = New SqlParameter("@DT_XL", dtParam)
                    cmd.Parameters.Add(sqlParam(0))
                    sqlParam(1) = New SqlParameter("@BSU_ID", If(rblAdjFor.SelectedValue = "T", Session("sBsuId"), ddlBUnit.SelectedValue))
                    cmd.Parameters.Add(sqlParam(1))
                    sqlParam(2) = New SqlParameter("@ARM_ID", ddlAdjType.SelectedValue)
                    cmd.Parameters.Add(sqlParam(2))
                    sqlParam(3) = New SqlParameter("@ADJ_DATE", txtAdjDT.Text)
                    cmd.Parameters.Add(sqlParam(3))
                    sqlParam(4) = New SqlParameter("@FAI_USER", Session("sUsr_name"))
                    cmd.Parameters.Add(sqlParam(4))
                    sqlParam(5) = New SqlParameter("@ADJ_REASON_ID", 0)
                    cmd.Parameters.Add(sqlParam(5))
                    sqlParam(6) = New SqlParameter("@RET_VAL", 0)
                    sqlParam(6).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(6))
                    sqlParam(7) = New SqlParameter("@BATCH_NO", 0)
                    sqlParam(7).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(7))
                    sqlParam(8) = New SqlParameter("@bSET_DEFAULT_TAX_CODE", chkDefaultTaxcode.Checked)
                    cmd.Parameters.Add(sqlParam(8))

                    Using dr As SqlDataReader = cmd.ExecuteReader()
                        Dim dttb As New DataTable
                        dttb.Load(dr)
                        If sqlParam(6).Value <> 0 Then
                            stTrans.Rollback()
                            'stTrans.Commit()
                            Me.btnImport.Visible = True
                            Me.btnCommitImprt.Visible = False
                            btnProceedImpt.Visible = False
                        Else
                            stTrans.Commit()
                            btnProceedImpt.Visible = False
                            Me.btnCommitImprt.Visible = True
                            ViewState("FAI_BATCHNO") = sqlParam(7).Value
                            hdBATCHNO.Value = sqlParam(7).Value
                            ShowAdjustmentSummary()
                            DisableControls(False)
                        End If
                        If Not dttb Is Nothing AndAlso dttb.Rows.Count > 0 Then
                            gvExcelImport.Columns(9).Visible = True 'shoes the error message column
                            VSgvExcelImport = dttb
                            Dim Counter As Int32 = 0
                            If (Not dttb.Columns.Contains("SLNO")) Then
                                dttb.Columns.Add("SLNO")
                                Dim dtDataTable As DataTable = ViewState("ExcelData")
                                For i As Integer = 0 To dttb.Rows.Count - 1
                                    dttb.Rows(i)(dttb.Columns.Count - 1) = DirectCast(ViewState("ExcelData"), DataTable).Rows(i)("SLNO")
                                Next
                            End If
                            BindExcel()
                        End If
                    End Using
                    con.Close()
                End Using
            Catch ex As Exception
                'Me.lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
            End Try
        End Using
    End Sub
    Sub DisableControls(ByVal bEnabled As Boolean)
        ddlBUnit.Enabled = bEnabled
        txtAdjDT.Enabled = bEnabled
        rblAdjFor.Enabled = bEnabled
        ddlAdjType.Enabled = bEnabled
        rbEnquiry.Enabled = bEnabled
        rbEnrollment.Enabled = bEnabled
        FileUpload1.Enabled = bEnabled
        txtNarration.Enabled = bEnabled
    End Sub
    Private Sub ShowAdjustmentSummary()
        Dim str_conn As String = IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, If(rblAdjFor.SelectedValue = "T", ConnectionManger.GetOASISTRANSPORTConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString))
        Dim dsSummary = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "dbo.GET_FEEADJUSTMENT_BULK_IMPORT_SUMMARY '" & ViewState("FAI_BATCHNO") & "'")
        If dsSummary.Tables(0).Rows.Count > 0 Then
            Me.gvImportSmry.DataSource = dsSummary.Tables(0)
            Me.gvImportSmry.DataBind()
        End If
    End Sub
    Private Sub ImportAdjustmentSheet()
        Try
            lblError.Text = ""
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If
            End If
            If File.Exists(FileName) Then
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                'Dim ef As New GemBox.Spreadsheet.ExcelFile
                Dim workbook = ExcelFile.Load(FileName)
                ' Select active worksheet.
                Dim worksheet = workbook.Worksheets.ActiveWorksheet
                Dim dtXL As New DataTable
                If worksheet.Rows.Count > 1 Then
                    dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With
                            {
                             .ColumnHeaders = True,
                             .StartRow = 0,
                             .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count,
                             .NumberOfRows = worksheet.Rows.Count,
                             .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                            })
                End If
                ' File delete first 
                File.Delete(FileName)
                If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then
                    ViewState("ExcelData") = dtXL
                    If (rblAdjFor.SelectedValue <> "T") Then
                        If Not dtXL.Columns.Contains("BSU") Then
                            Dim _Data As New DataColumn
                            _Data.ColumnName = "BSU"

                            _Data.DefaultValue = GETSHORTBSUNAME(ddlBUnit.SelectedItem.Text)
                            dtXL.Columns.Add(_Data)
                            dtXL.Columns("BSU").SetOrdinal(0)
                        End If
                        'dtXL.Columns.Remove("BSU")
                    End If
                    Dim dvxl As DataView = New DataView(dtXL)
                    VSgvExcelImport = dvxl.ToTable()
                    Dim Columns As DataColumnCollection = VSgvExcelImport.Columns
                    If Not Columns.Contains("TAX_CODE") Then
                        VSgvExcelImport.Columns.Add("TAX_CODE", GetType(String), "")
                    End If
                    If Not Columns.Contains("bVALID") Then
                        VSgvExcelImport.Columns.Add("bVALID", GetType(Boolean), True)
                    End If
                    If Not Columns.Contains("ERROR_MSG") Then
                        VSgvExcelImport.Columns.Add("ERROR_MSG", GetType(String), "")
                    End If
                    BindExcel()
                    Me.trGvImport.Visible = True
                    Me.lblMessage.Text = "Click Proceed to Import the data"
                    gvExcelImport.Columns(9).Visible = False 'hides the error message column initially
                    Me.btnProceedImpt.Visible = True
                    Me.btnImport.Visible = False
                End If
                'SAVE_SFTP_LOG("IMPORTDATA", LocalFilePath, "SUCCESS")
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            'SAVE_SFTP_LOG("IMPORTDATA", LocalFilePath, "FAILED", ex.InnerException.Message)
            FeeBulkAdjustment.Errorlog("Unable to load datatable from file - ImportAdjustmentSheet()")
        End Try
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBUnit.SelectedIndexChanged
        btnCancelAdj_Click(Nothing, Nothing)
    End Sub
    Protected Sub btnCancelAdj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAdj.Click
        txtNarration.Text = ""
        lblError.Text = ""
        DisableControls(True)
        Me.gvImportSmry.DataSource = Nothing
        Me.gvImportSmry.DataBind()
        VSgvExcelImport = Nothing
        BindExcel()
        Me.trGvImport.Visible = False
        Me.lblMessage.Text = ""
        gvExcelImport.Columns(9).Visible = False 'hides the error message column initially
        Me.btnProceedImpt.Visible = False
        Me.btnCommitImprt.Visible = False
        Me.btnImport.Visible = True
        ViewState("FAI_BATCHNO") = 0
        hdBATCHNO.Value = 0
        'If Not ddlBUnit.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
        '    ddlBUnit.Items.FindItemByValue(Session("sBsuid")).Selected = True
        'End If
    End Sub
    Protected Sub btnCommitImprt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitImprt.Click
        h_Processeing.Value = "1"

        Session("API_ERROR-FEE_ADJ") = False
        ' Kick off the actual, private long-running process in a new Task
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, If(rblAdjFor.SelectedValue = "T", ConnectionManger.GetOASISTRANSPORTConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString)))
        Dim task As Task
        task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                Dim flag As Boolean = True
                                                                objConn.Open()
                                                                transaction = objConn.BeginTransaction("SampleTransaction")
                                                                Try
                                                                    Dim sqlParam(2) As SqlParameter
                                                                    Dim cmd As New SqlCommand
                                                                    cmd.Dispose()
                                                                    cmd = New SqlCommand("IMPORT_FEE_ADJUSTMENTS_NEW", objConn, transaction)
                                                                    cmd.CommandType = CommandType.StoredProcedure
                                                                    cmd.CommandTimeout = 0
                                                                    Dim retmsg As String = ""
                                                                    sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retmsg, SqlDbType.VarChar, True, 200)
                                                                    cmd.Parameters.Add(sqlParam(0))
                                                                    sqlParam(1) = Mainclass.CreateSqlParameter("@FAI_BATCHNO", ViewState("FAI_BATCHNO"), SqlDbType.VarChar)
                                                                    cmd.Parameters.Add(sqlParam(1))
                                                                    cmd.ExecuteNonQuery()
                                                                    retmsg = sqlParam(0).Value
                                                                    If retmsg = "" Then
                                                                        transaction.Commit()
                                                                        System.Threading.Thread.Sleep(1000)
                                                                        h_Processeing.Value = ""
                                                                    Else
                                                                        transaction.Rollback()
                                                                        FeeBulkAdjustment.Errorlog("Rollback - " & retmsg, "PHOENIX")
                                                                        h_Processeing.Value = ""
                                                                        Session("API_ERROR-FEE_ADJ") = True
                                                                        Exit Sub
                                                                    End If
                                                                Catch ex As Exception
                                                                    transaction.Rollback()
                                                                    System.Threading.Thread.Sleep(1500)
                                                                    h_Processeing.Value = ""
                                                                    Session("API_ERROR-FEE_ADJ") = True
                                                                    FeeBulkAdjustment.Errorlog("IMPORT_ADJUSTMENT - Exception: " & ex.Message, "PHOENIX")
                                                                Finally
                                                                    If objConn.State = ConnectionState.Open Then
                                                                        objConn.Close()
                                                                    End If
                                                                End Try
                                                            End Sub)
    End Sub
    Private Function GETSHORTBSUNAME(ByVal BSU_NAME As String) As String
        Dim ReturnStr As String

        Dim REMOVE_AT As String = "("
        Dim x As Integer = InStr(BSU_NAME, REMOVE_AT)
        ReturnStr = BSU_NAME.Substring(x + REMOVE_AT.Length - 1)
        ReturnStr = ReturnStr.Replace(")", "")
        Return ReturnStr
    End Function



End Class


