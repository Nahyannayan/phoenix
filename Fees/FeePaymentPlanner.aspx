﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" EnableViewState="true" AutoEventWireup="false" CodeFile="FeePaymentPlanner.aspx.vb" Inherits="Fees_FeePaymentPlanner" %>

<%@ Register Src="~/UserControls/uscStudentPicker.ascx" TagPrefix="uc1" TagName="uscStudentPicker2" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>

    <%--  <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>--%>
    <%--  <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>--%>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        function CheckForPrint() {
            if ($("#<%= h_print.ClientID%>").val != '') {
                $("#<%= h_print.ClientID%>").val('');
                //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                var url = "../Reports/ASPX Report/RptViewerModal.aspx"
                var oWnd = radopen(url, "pop_checkforprint");
                //openShadowBox("../Reports/ASPX Report/RptViewerModal.aspx", 700, 800, "")
            }
        }

        //function openShadowBox(url, height, width, title) {
        //    width = parseInt(width) + 60;
        //    var horizontalPadding = 30;
        //    var verticalPadding = 30;
        //    $('<iframe id="cdt_shadowbox" src="' + url + '" frameBorder="0"/>').dialog({
        //        title: (title) ? title : 'CDT Shadowbox',
        //        autoOpen: true,
        //        width: width,
        //        height: height,
        //        modal: true,
        //        resizable: true,
        //        autoResize: false,
        //        closeOnEscape: true,
        //        //position: 'top',
        //        overlay: {
        //            opacity: 0.5,
        //            background: "black"
        //        }
        //    }).width(width - horizontalPadding).height(height - verticalPadding);
        //    $('html, body').scrollTop(0);
        //}




        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_checkforprint" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Payment Planner
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <asp:HiddenField ID="h_print" runat="server" />
                <table border="0" cellpadding="0" onclick="return true;" id="tblErrortop" cellspacing="0"
                    align="center" width="90%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>

                <table width="100%">
                    <asp:Panel runat="server" ID="PnlOuter">
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" TabIndex="1"></asp:DropDownList>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Date</span></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtDT" runat="server" TabIndex="2" onfocus="this.blur();"></asp:TextBox>
                                <asp:ImageButton
                                    ID="imgDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="2" />
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                    PopupButtonID="imgDT" TargetControlID="txtDT" Format="dd/MMM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Select Student</span></td>
                            <td align="left" colspan="3">
                                <uc1:uscStudentPicker2 runat="server" ID="uscStudentPicker" />
                                <uc1:usrSelStudent ID="UsrSelStudent" runat="server" Visible="false" />
                                <%--GRADE--%>
                                <asp:Label ID="lblGrade" runat="server" Text="" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblParentHeader" runat="server" class="field-label" Text="Parent Details" />
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblParentDetails" runat="server" Text="-" /></td>

                        </tr>
                        <tr id="id_PayPlanReq" runat="server" visible="false">
                            <td align="left"><span class="field-label">Payment plan request by parent </span></td>
                            <td colspan="3" align="left">
                                <asp:RadioButtonList ID="rblParentPaymodesReq" Enabled="false" CssClass="field-label" DataValueField="CLT_ID" DataTextField="CLT_DESCR" runat="server" RepeatDirection="Horizontal" TabIndex="5">
                                </asp:RadioButtonList></td>
                        </tr>

                        <tr>
                            <td align="left"><span class="field-label">FeeType</span></td>
                            <td align="left" colspan="2">
                                <asp:CheckBoxList ID="cblFeeType" CssClass="field-label" RepeatDirection="Horizontal" RepeatColumns="5" runat="server" TabIndex="3"></asp:CheckBoxList>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnLoad" runat="server" CssClass="button" Visible="false" Text="Load" /></td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td align="left"><span class="field-label">Outstanding</span></td>
                        <td align="left" colspan="3">
                            <asp:GridView ID="gvStudentDetail" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" DataKeyNames="STU_ID,FEE_ID" EnableModelValidation="True" SkinID="GridViewNormal" ShowFooter="True" Width="90%" TabIndex="4">
                                <Columns>
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BKT1" HeaderText="(0-30)" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BKT2" HeaderText="(31-60)" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BKT3" HeaderText="(61-90)" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BKT4" HeaderText="(90-180)" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BKT5" HeaderText="(180+)" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Total">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalDue" runat="server" Text="0.00"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("TOTALDUE", "{0:0.00}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Fee for upcoming Term/Month</span></td>
                        <td align="left" colspan="3">
                            <asp:GridView ID="gvFeeDetail" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="True" EnableModelValidation="True" SkinID="GridViewNormal" ShowFooter="true" Width="90%" TabIndex="4">
                                <%--<Columns>
                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD01" HeaderText="Period1" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD02" HeaderText="Period2" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD03" HeaderText="Period3" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD06" HeaderText="Period4" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD07" HeaderText="Period5" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD08" HeaderText="Period6" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD09" HeaderText="Period7" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD10" HeaderText="Period8" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD11" HeaderText="Period9" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRD12" HeaderText="Period10" DataFormatString="{0:0.00}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                      
                                    </Columns>--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="Total" Visible="false">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalUPCOMING" runat="server" Text="0.00"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelUPCOMING" runat="server" Text='<%# Bind("TOTAL", "{0:0.00}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right"></td>
                        <td align="left">
                            <asp:Label ID="lblGrandTotal" CssClass="field-label" runat="server" Text=""></asp:Label></td>
                    </tr>

                    <asp:Panel runat="server" ID="PnlOuter2">
                        <tr>
                            <td colspan="4" class="title-bg" align="left">Payment Details</td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Payment mode </span></td>
                            <td colspan="2" align="left">
                                <asp:RadioButtonList ID="rblPaymodes" CssClass="field-label" DataValueField="CLT_ID" DataTextField="CLT_DESCR" runat="server" RepeatDirection="Horizontal" TabIndex="5" OnSelectedIndexChanged="OnrblPaymodes_Changed" AutoPostBack="true">
                                </asp:RadioButtonList></td>
                            <td align="left">
                                <asp:Label ID="lblbalance" CssClass="field-label" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Amount</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtPayAmt" runat="server" Text="0.00" Style="text-align: right" TabIndex="6"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, Custom"
                                                    ValidChars="." TargetControlID="txtPayAmt" />
                                            </td>
                                            <td align="left" width="20%"><span class="field-label">Date </span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtPayDT" runat="server" TabIndex="2" onfocus="this.blur();" Enabled="false"></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="imgPayDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="7" Enabled="false" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="MyCalendar" runat="server"
                                                    PopupButtonID="imgPayDT" TargetControlID="txtPayDT" Format="dd/MMM/yyyy">
                                                </ajaxToolkit:CalendarExtender>

                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="trChq" runat="server" style="display: none">
                            <td colspan="4">
                                <asp:Panel ID="pnlChq" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Cheque No</span>
                                            </td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtChqNo" runat="server" TabIndex="8"></asp:TextBox>
                                            </td>
                                            <td align="left" width="20%"><span class="field-label">Bank</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtBank" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="imgBank1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                                    OnClientClick="getBankOrEmirate(1,1); return false;" TabIndex="9" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Cheque Date</span></td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtChqDT" runat="server" TabIndex="2" onfocus="this.blur();"></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="imgCHQDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="10" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" CssClass="MyCalendar" runat="server"
                                                    PopupButtonID="imgCHQDT" TargetControlID="txtChqDT" Format="dd/MMM/yyyy">
                                                </ajaxToolkit:CalendarExtender>
                                            </td>
                                            <td align="left" width="20%"></td>
                                            <td align="left" width="30%"></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="trCard" runat="server" style="display: none">
                            <td colspan="4">
                                <asp:Panel ID="pnlCard" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left"><span class="field-label">Card No</span>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCardNo" runat="server" TabIndex="11"></asp:TextBox>
                                            </td>
                                            <td align="left"><span class="field-label">Card Type</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard" DataTextField="CRI_DESCR"
                                                    DataValueField="CRR_ID" SkinID="DropDownListNormal" TabIndex="12">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="id_cmnts" runat="server" visible="false">
                            <td align="left" width="20%"><span class="field-label">Comments</span></td>
                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" TabIndex="13"></asp:TextBox></td>
                            <td align="left"></td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"></td>
                            <td align="left" colspan="2"></td>
                            <td align="left">
                                <asp:Button ID="btnAddPP" runat="server" CssClass="button" Text="Add" TabIndex="14" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td colspan="4" class="title-bg" align="left">Payment Plan
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvPayPlanDetail" runat="server" CssClass="table table-bordered table-row" DataKeyNames="CLT_ID,BANK_ID,CRR_ID" AutoGenerateColumns="False" EnableModelValidation="True" SkinID="GridViewView" ShowFooter="True" TabIndex="15">
                                <Columns>

                                    <asp:TemplateField HeaderText="Id" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PaymentMode">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PAYMODE") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            BALANCE
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("PAYMODE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblBalanceDue" runat="server" Text="0.00"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPayAmt" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PAY_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="Date" />
                                    <asp:BoundField DataField="CHQNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="Cheque/Card No" />
                                    <asp:BoundField DataField="CHQDT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Cheque date" />
                                    <asp:BoundField DataField="BANK" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="Bank" />
                                    <asp:BoundField DataField="COMMENTS" HeaderText="Comments" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit" OnClick="lbEdit_Click"> </asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>

                            </asp:GridView>
                            <asp:HiddenField ID="hfFPD_ID" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr id="id_finalComments" runat="server" visible="false">
                        <td align="left" colspan="4">
                            <asp:Label ID="lblFinalComments" runat="server" class="field-label"></asp:Label>
                        </td>
                    </tr>
                    <tr id="id_fin_cmnts" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Comments</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txt_fin_comments" runat="server" TextMode="MultiLine" TabIndex="13"></asp:TextBox></td>
                    </tr>
                    <tr id="id_princ_cmnts" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Comments</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txt_princ_comments" runat="server" TextMode="MultiLine" TabIndex="13"></asp:TextBox></td>
                    </tr>
                    <tr id="id_cashier" runat="server" visible="true">
                        <td align="left" width="20%"><span class="field-label">Comments</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txt_cashier_cmnts" runat="server" TextMode="MultiLine" TabIndex="13"></asp:TextBox></td>
                    </tr>
                </table>

                 <table width="100%">
                     <tr id="id_fecoll" runat="server" visible="false">
                         <td align="left" width="25%">
                               <asp:CheckBox ID="ChkCreateCollection" runat="server" Text="Create Collection Receipt" class="field-label"></asp:CheckBox>
                         </td>
                         <td align="left" width="25%">                              
                         </td>
                         <td align="left" width="25%">
                         </td>
                         <td align="left" width="25%">
                         </td>
                     </tr>
                 </table>
                <table width="100%">
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" TabIndex="16" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="16" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" TabIndex="17" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" TabIndex="18" />
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" TabIndex="19" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" TabIndex="20" />
                            <asp:Button ID="btnOnhold" runat="server" CssClass="button" Text="On Hold" TabIndex="20" />
                            <asp:Button ID="btnRevert" runat="server" CssClass="button" Text="Revert" TabIndex="20" />
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" TabIndex="20" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" TabIndex="21" />
                            <%--<asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" TabIndex="22" Visible="false" />--%>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="exec GetCreditCardListForCollection 'FEES'"></asp:SqlDataSource>
                <asp:HiddenField ID="hfBank" runat="server" Value="" />
                <asp:HiddenField ID="hfBankCrd" runat="server" Value="" />
                <script type="text/javascript">
                    Sys.Application.add_load(
                    function PayMode() {
                        var rbvalue = $("input[name='<%=rblPaymodes.UniqueID%>']:radio:checked").val();

                        if (rbvalue == '1') {
                            $("#<%= trChq.ClientID%>").hide();
                            $("#<%=trCard.ClientID%>").hide();
                        }
                        else if (rbvalue == '2') {
                            $("#<%= trChq.ClientID%>").show();
                            $("#<%=trCard.ClientID%>").hide();
                        }
                        else if (rbvalue == '3') {
                            $("#<%= trCard.ClientID%>").show();
                            $("#<%= trChq.ClientID%>").hide();
                        }
                        else if (rbvalue == '9') {
                            $("#<%= trChq.ClientID%>").show();
                            $("#<%= trCard.ClientID%>").hide();
                        } else {
                            $("#<%= trChq.ClientID%>").hide();
                            $("#<%=trCard.ClientID%>").hide();
                        }
                    }
            );
        $(document).load(function () { PayMode(); });
        $(document).ready(function () { PayMode(); });
        $("[id$=rblPaymodes]").live("click", function () {
            PayMode();
        });
        function PayMode() {
            var rbvalue = $("input[name='<%=rblPaymodes.UniqueID%>']:radio:checked").val();

                if (rbvalue == '1') {
                    $("#<%= trChq.ClientID%>").hide();
                    $("#<%=trCard.ClientID%>").hide();
                }
                else if (rbvalue == '2') {
                    $("#<%= trChq.ClientID%>").show();
                    $("#<%=trCard.ClientID%>").hide();
                }
                else if (rbvalue == '3') {
                    $("#<%= trCard.ClientID%>").show();
                    $("#<%= trChq.ClientID%>").hide();
                } else if (rbvalue == '9') {
                    $("#<%= trChq.ClientID%>").show();
                    $("#<%= trCard.ClientID%>").hide();
                } else {
                    $("#<%= trChq.ClientID%>").hide();
                    $("#<%=trCard.ClientID%>").hide();
                }
}
                </script>
                <script type="text/javascript" language="javascript">
                    function getBankOrEmirate(mode, ctrl) {
                        var sFeatures, url;
                        var NameandCode;
                        var result;
                        url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
                        var oWnd = radopen(url, "pop_getBankOrEmirate");
                    }
                    function getBankOrEmirate2(mode, ctrl) {
                        var sFeatures, url;
                        var NameandCode;
                        var result;
                        url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
                        var oWnd = radopen(url, "pop_getBankOrEmirate2");
                    }

                    function OnClientClose1(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {
                            NameandCode = arg.NameCode.split('||');
                            document.getElementById('<%= hfBank.ClientID%>').value = NameandCode[0];
                            document.getElementById('<%= txtBank.ClientID%>').value = NameandCode[1];
                            __doPostBack('<%=txtBank.ClientID%>', 'TextChanged');
                        }
                    }

                    function OnClientClose2(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {

                            NameandCode = arg.NameCode.split('||');
                            document.getElementById('<%= hfBank.ClientID%>').value = NameandCode[0];
                            document.getElementById('<%= txt_bankid.ClientID%>').value = NameandCode[1];
                            //__doPostBack('<%=txtBank.ClientID%>', 'TextChanged');

                        }
                    }
                </script>
                <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="CENTER">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>
                                <table align="center" width="100%" cellpadding="2" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Cheque Date</span>
                                        </td>
                                        <td align="left">
                                            <asp:HiddenField ID="rowid" runat="server" />
                                            <asp:TextBox ID="txt_chqedat" runat="server" TabIndex="8"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Amount</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txt_amounte" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Cheque No</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txt_cheqno" runat="server" TabIndex="8"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Bank</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txt_bankid" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                                OnClientClick="getBankOrEmirate2(1,1); return false;" TabIndex="9" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" />
                                            <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>

