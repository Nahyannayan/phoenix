Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Partial Class Fees_FeeApproveProformaPayment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property FilterString() As String
        Get
            Return ViewState("FilterString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property SQLSelect() As String
        Get
            Return ViewState("SQLSelect")
        End Get
        Set(ByVal value As String)
            ViewState("SQLSelect") = value
        End Set
    End Property
    Private Property SortColString() As String
        Get
            Return ViewState("SortColString")
        End Get
        Set(ByVal value As String)
            ViewState("SortColString") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property EditPagePath() As String
        Get
            Return ViewState("EditPagePath")
        End Get
        Set(ByVal value As String)
            ViewState("EditPagePath") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property
    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property
    Private Property HeaderTitle() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            Page.Title = value
            lblTitle.Text = value
        End Set
    End Property
    Private Property ShowGridCheckBox() As Boolean
        Get
            Return ViewState("ShowGridCheckBox")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowGridCheckBox") = value
        End Set
    End Property
    Private Property NoAddingAllowed() As Boolean
        Get
            Return ViewState("NoAddingAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoAddingAllowed") = value
        End Set
    End Property
    Private Property NoViewAllowed() As Boolean
        Get
            Return ViewState("NoViewAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAllowed") = value
        End Set
    End Property
    Private Property HideExportButton() As Boolean
        Get
            Return ViewState("HideExportButton")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideExportButton") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Private Property FilterText1() As String
        Get
            Return chkSelection1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection1.Text = value
                chkSelection1.Visible = True
            Else
                chkSelection1.Visible = False
                chkSelection1.Text = ""
            End If
        End Set
    End Property
    Private Property FilterText2() As String
        Get
            Return chkSelection2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection2.Text = value
                chkSelection2.Visible = True
            Else
                chkSelection2.Visible = False
                chkSelection2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox1Text() As String
        Get
            Return rad1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad1.Text = value
                rad1.Visible = True
            Else
                rad1.Visible = False
                rad1.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox2Text() As String
        Get
            Return rad2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad2.Text = value
                rad2.Visible = True
            Else
                rad2.Visible = False
                rad2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox3Text() As String
        Get
            Return rad3.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad3.Text = value
                rad3.Visible = True
            Else
                rad3.Visible = False
                rad3.Text = ""
            End If
        End Set
    End Property

    Private Property ButtonText1() As String
        Get
            Return ListButton1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton1.Text = value
                ListButton1.Visible = True
            Else
                ListButton1.Visible = False
                ListButton1.Text = ""
            End If
        End Set
    End Property
    Private Property ButtonText2() As String
        Get
            Return ListButton2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton2.Text = value
                ListButton2.Visible = True
            Else
                ListButton2.Visible = False
                ListButton2.Text = ""
            End If
        End Set
    End Property

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Session("sModule") = "SS" Then
                Me.MasterPageFile = "../mainMasterPageSS.master"
            Else
                Me.MasterPageFile = "../mainMasterPage.master"
            End If
        Catch ex As Exception

        End Try
    End Sub
    ' Dim SPParam(10) As SqlParameter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/like.gif"

                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    'lblError.Text = msgText
                    usrMessageBar.ShowNotification(msgText, UserControls_usrMessageBar.WarningType.Danger)
                End If
                ShowGridCheckBox = False
                NoAddingAllowed = True
                NoViewAllowed = True
                HideExportButton = True
                IsStoredProcedure = False
                ButtonText1 = "Approve"
                ButtonText2 = ""
                rad1.Checked = True
                CheckBox1Text = ""
                CheckBox2Text = ""
                CheckBox3Text = ""

                FilterText1 = ""
                FilterText2 = ""
                BuildListView()
                gridbind(False)
                btnExport.Visible = Not HideExportButton
                If NoAddingAllowed Then
                    hlAddNew.Visible = False
                Else
                    hlAddNew.Visible = True
                    Dim url As String
                    url = EditPagePath & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    hlAddNew.NavigateUrl = url
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(ByVal NoReset As Boolean)
        Try
            Dim GridData As New DataTable
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            FilterTextboxString = ""
            If gvDetails.Rows.Count > 0 Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If SortColString <> "" Then
                OrderByStr = "order by " & SortColString
            Else
                OrderByStr = "order by  1"
            End If
            If Not IsStoredProcedure Then
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQLSelect & str_Filter & OrderByStr).Tables(0)
            Else
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SQLSelect, CType(SPParam, SqlParameter())).Tables(0)
                str_Filter = " 1 = 1 " & str_Filter
                GridData.Select(str_Filter)
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridData.Copy
                mySelectDT.Rows.Clear()
                For Each mRow In GridData.Select(str_Filter)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If

            Dim bindDT, myData As New DataTable
            bindDT = GridData.Copy
            myData = GridData.Copy
            If myData.Columns.Count > 0 Then
                myData.Columns.RemoveAt(0)
            End If
            ViewState("GridTable") = GridData
            Session("myData") = myData
            SetExportFileLink()
            GridData.Rows.Clear()
            Dim HeaderLabel As New Label
            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex
            If bindDT.Columns.Count < 10 Then
                For i = 0 To bindDT.Columns.Count
                    gvDetails.Columns(i).Visible = True
                Next
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If
            gvDetails.Columns(0).Visible = ShowGridCheckBox
            gvDetails.Columns(11).Visible = Not NoAddingAllowed
            gvDetails.Columns(1).Visible = False
            gvDetails.Columns(11).Visible = Not NoViewAllowed
            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If

            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            SetHeaderFilterString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label
            lblItemColID = TryCast(e.Row.FindControl("lblItemCol1"), Label)
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If e.Row.RowType = DataControlRowType.Header Then
                If rblFilter.SelectedValue <> "NA" Then
                    Me.gvDetails.Columns(0).Visible = False
                Else
                    Me.gvDetails.Columns(0).Visible = True
                End If
            End If
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            Try
                Dim FloatColIndexArr(), i, ControlName As String
                FloatColIndexArr = FloatColIndex.Split("|")
                Dim lblItemCol As Label
                For Each i In FloatColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsNumeric(lblItemCol.Text) Then
                                lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("####0.00")
                            End If
                        End If
                    End If
                Next
                Dim DateColIndexArr() As String
                DateColIndexArr = DateColIndex.Split("|")
                For Each i In DateColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsDate(lblItemCol.Text) Then
                                lblItemCol.Text = CDate(lblItemCol.Text).ToString("dd/MMM/yyyy")
                            End If
                        End If
                    End If
                Next

            Catch ex As Exception

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind(True)
    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt(lblTitle.Text)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkSelection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelection1.CheckedChanged, chkSelection2.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Dim chkControl As New HtmlInputCheckBox
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                chkControl.Checked = chkSelectAll.Checked
            Next
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Protected Sub ListButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListButton1.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl IsNot Nothing Then
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                End If
            End If
        Next
        If IDs = "" Then
            'lblError.Text = "No Item Selected !!!"
            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "F351087"
                ApproveGrpProformaPaymnts(IDs)
            Case "F351088"
                ApproveProformaPaymnts(IDs)
        End Select
    End Sub


    Protected Sub ListButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListButton2.Click
       
    End Sub

    Private Function ApproveGrpProformaPaymnts(ByVal InvoiceNos As String) As Boolean
        Try
            If InvoiceNos <> "" Then
                If Not Master.IsSessionMatchesForSave() Then
                    'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
                    usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
                    Exit Function
                End If

                Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim cmd As New SqlCommand("FEES.SaveCollectionFromProforma")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Transaction = stTrans
                cmd.Connection = stTrans.Connection
                cmd.CommandTimeout = 0
                Try
                    Dim retval As String = "11"
                    Dim STR_TYPE As Char = "S"
                    Dim ExchRate As Decimal = 1

                    Dim pParms(11) As SqlClient.SqlParameter

                    pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.VarChar, 5)
                    pParms(1).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(pParms(1))
                    pParms(0) = New SqlClient.SqlParameter("@InvoiceNos", SqlDbType.VarChar)
                    pParms(0).Value = InvoiceNos
                    cmd.Parameters.Add(pParms(0))
                    pParms(2) = New SqlClient.SqlParameter("@IsGroupProforma", SqlDbType.Bit)
                    pParms(2).Value = True
                    cmd.Parameters.Add(pParms(2))
                    pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
                    pParms(3).Value = Session("sBsuid")
                    cmd.Parameters.Add(pParms(3))
                    pParms(4) = New SqlClient.SqlParameter("@User_Name", SqlDbType.VarChar)
                    pParms(4).Value = Session("sUsr_name")
                    cmd.Parameters.Add(pParms(4))
                    pParms(5) = New SqlClient.SqlParameter("@BSU_Currency", SqlDbType.VarChar)
                    pParms(5).Value = Session("BSU_CURRENCY")
                    cmd.Parameters.Add(pParms(5))
                    pParms(6) = New SqlClient.SqlParameter("@BSU_EXRate", SqlDbType.Decimal)
                    pParms(6).Value = 1.0
                    cmd.Parameters.Add(pParms(6))
                    pParms(7) = New SqlClient.SqlParameter("@Aud_module", SqlDbType.VarChar)
                    pParms(7).Value = Session("sModule")
                    cmd.Parameters.Add(pParms(7))
                    pParms(8) = New SqlClient.SqlParameter("@Aud_form", SqlDbType.VarChar)
                    pParms(8).Value = Master.MenuName
                    cmd.Parameters.Add(pParms(8))
                    pParms(9) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar)
                    pParms(9).Value = HttpContext.Current.Request.UserHostAddress.ToString()
                    cmd.Parameters.Add(pParms(9))
                    pParms(10) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar)
                    pParms(10).Value = Page.User.Identity.Name.ToString
                    cmd.Parameters.Add(pParms(10))

                    retval = cmd.ExecuteNonQuery()
                    retval = pParms(1).Value

                    If retval = "0" Then
                        ViewState("FCL_IDS") = Nothing
                        stTrans.Commit()
                        BuildListView()
                        gridbind(False)
                        'Session("FeeCollection").rows.clear()
                        ' h_print.Value = "print"
                        ' h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(str_NEW_FCL_RECNO) & _
                        '"&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
                        '"&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
                        '"&isenq=" & Encr_decrData.Encrypt(False) & _
                        '"&iscolln=" & Encr_decrData.Encrypt(True) & "&isexport=0"
                        'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FCL_RECNO, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                        '   Session("ReportSource") = FeeCollection.PrintReceipt(str_NEW_FCL_RECNO, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), True)
                        'lblError.Text = getErrorMessage("0")
                        usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
                    Else
                        stTrans.Rollback()
                        'lblError.Text = getErrorMessage(retval)
                        usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                    End If
                     
                      
                Catch ex As Exception
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(1000)
                    usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
                    Errorlog(ex.Message)
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try



            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function ApproveProformaPaymnts(ByVal InvoiceNos As String) As Boolean
        Try
            If InvoiceNos <> "" Then
                If Not Master.IsSessionMatchesForSave() Then
                    'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
                    usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
                    Exit Function
                End If

                Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim cmd As New SqlCommand("FEES.SaveCollectionFromProforma")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Transaction = stTrans
                cmd.Connection = stTrans.Connection
                cmd.CommandTimeout = 0
                Try
                    Dim retval As String = "11"
                    Dim STR_TYPE As Char = "S"
                    Dim ExchRate As Decimal = 1

                    Dim pParms(11) As SqlClient.SqlParameter

                    pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.VarChar, 5)
                    pParms(1).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(pParms(1))
                    pParms(0) = New SqlClient.SqlParameter("@InvoiceNos", SqlDbType.VarChar)
                    pParms(0).Value = InvoiceNos
                    cmd.Parameters.Add(pParms(0))
                    pParms(2) = New SqlClient.SqlParameter("@IsGroupProforma", SqlDbType.Bit)
                    pParms(2).Value = False
                    cmd.Parameters.Add(pParms(2))
                    pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
                    pParms(3).Value = Session("sBsuid")
                    cmd.Parameters.Add(pParms(3))
                    pParms(4) = New SqlClient.SqlParameter("@User_Name", SqlDbType.VarChar)
                    pParms(4).Value = Session("sUsr_name")
                    cmd.Parameters.Add(pParms(4))
                    pParms(5) = New SqlClient.SqlParameter("@BSU_Currency", SqlDbType.VarChar)
                    pParms(5).Value = Session("BSU_CURRENCY")
                    cmd.Parameters.Add(pParms(5))
                    pParms(6) = New SqlClient.SqlParameter("@BSU_EXRate", SqlDbType.Decimal)
                    pParms(6).Value = 1.0
                    cmd.Parameters.Add(pParms(6))
                    pParms(7) = New SqlClient.SqlParameter("@Aud_module", SqlDbType.VarChar)
                    pParms(7).Value = Session("sModule")
                    cmd.Parameters.Add(pParms(7))
                    pParms(8) = New SqlClient.SqlParameter("@Aud_form", SqlDbType.VarChar)
                    pParms(8).Value = Master.MenuName
                    cmd.Parameters.Add(pParms(8))
                    pParms(9) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar)
                    pParms(9).Value = HttpContext.Current.Request.UserHostAddress.ToString()
                    cmd.Parameters.Add(pParms(9))
                    pParms(10) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar)
                    pParms(10).Value = Page.User.Identity.Name.ToString
                    cmd.Parameters.Add(pParms(10))

                    retval = cmd.ExecuteNonQuery()
                    retval = pParms(1).Value

                    If retval = "0" Then
                        ViewState("FCL_IDS") = Nothing
                        stTrans.Commit()
                        BuildListView()
                        gridbind(False)
                        'Session("FeeCollection").rows.clear()
                        ' h_print.Value = "print"
                        ' h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(str_NEW_FCL_RECNO) & _
                        '"&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
                        '"&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
                        '"&isenq=" & Encr_decrData.Encrypt(False) & _
                        '"&iscolln=" & Encr_decrData.Encrypt(True) & "&isexport=0"
                        'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FCL_RECNO, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                        '   Session("ReportSource") = FeeCollection.PrintReceipt(str_NEW_FCL_RECNO, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), True)
                        'lblError.Text = getErrorMessage("0")
                        usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
                    Else
                        stTrans.Rollback()
                        'lblError.Text = getErrorMessage(retval)
                        usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                    End If


                Catch ex As Exception
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(1000)
                    usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
                    Errorlog(ex.Message)
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try



            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function PrintPasswords(ByVal EMP_IDs As String) As Boolean
        If EMP_IDs <> "" Then
            Try
                Dim sqlParam(0) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@EMP_Ids", EMP_IDs, SqlDbType.VarChar)
                Dim dtPasswords As New DataTable
                dtPasswords = Mainclass.getDataTable("RPT_PrintEmployeePasswords", sqlParam, ConnectionManger.GetOASISConnectionString)
                Dim mRow As DataRow
                Dim passwordEncr As New Encryption64
                For Each mRow In dtPasswords.Rows
                    mRow("EMP_PASSWORD") = passwordEncr.Decrypt(Convert.ToString(mRow("EMP_PASSWORD")).Replace(" ", "+"))
                Next
                Dim RepClass As New ReportClass
                Dim strResourseName As String = String.Empty
                Dim params As New Hashtable




                strResourseName = "../Payroll/Reports/Rpt/rptPrintEmpPwd.rpt"
                RepClass.ResourceName = strResourseName
                RepClass.ReportData = dtPasswords
                Session("ReportClassSource") = RepClass
                Response.Redirect("PrintViewer.aspx", True)
            Catch ex As Exception
                'Response.Redirect(ViewState("ReferrerUrl"))
            End Try
        End If
    End Function
    Private Sub BuildListView()
        Try
            Dim strConn, StrSQL, StrSortCol As String
            strConn = "" : StrSQL = "" : StrSortCol = ""
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            Select Case MainMenuCode
                Case "F351087" 'Approve Group Proforma Payment
                    IsStoredProcedure = True
                    ShowGridCheckBox = True
                    StrSQL = "FEES.GroupProformaInvoiceList"
                    ReDim SPParam(3)
                    SPParam(0) = Mainclass.CreateSqlParameter("@CompanyID", 0, SqlDbType.Int)
                    SPParam(1) = Mainclass.CreateSqlParameter("@Filter", Me.rblFilter.SelectedValue, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    EditPagePath = "#"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    HeaderTitle = "Approve Group Proforma Invoices Payment"
                    StrSortCol = ""
                Case "F351088" 'Fee Collection - ProForma Invoice
                    IsStoredProcedure = True
                    ShowGridCheckBox = True
                    StrSQL = "FEES.GetUnApprovedProFormaInvoicePayments"
                    ReDim SPParam(1)
                    SPParam(0) = Mainclass.CreateSqlParameter("@Bsu_ID", Session("sBsuid"), SqlDbType.Int)
                    SPParam(1) = Mainclass.CreateSqlParameter("@Filter", Me.rblFilter.SelectedValue, SqlDbType.VarChar)
                    EditPagePath = "#"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    HeaderTitle = "Approve Fee Collection - Proforma Invoices"
                    StrSortCol = ""
            End Select
            SQLSelect = StrSQL
            ConnectionString = strConn
            SortColString = StrSortCol
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

 
    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblFilter.SelectedIndexChanged
        If rblFilter.SelectedValue <> "NA" Then
            Me.ListButton1.Visible = False
        Else
            Me.ListButton1.Visible = True
        End If
        BuildListView()
        gridbind(False)
    End Sub
End Class

