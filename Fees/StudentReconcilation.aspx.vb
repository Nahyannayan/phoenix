Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_StudentReconcilation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            doClear()
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            'ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBSuid")
            Dim USR_NAME As String = Session("sUsr_name")

            InitialiseCompnents()
            drpBusinessunit.SelectedValue = CurBsUnit

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300100") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
        End If
    End Sub

    Sub InitialiseCompnents()
        gvAreaChange.Attributes.Add("bordercolor", "#1b80b6")
        gvDiscontinue.Attributes.Add("bordercolor", "#1b80b6")
        gvSchedule.Attributes.Add("bordercolor", "#1b80b6")
        gvService.Attributes.Add("bordercolor", "#1b80b6")
        gvTrnRequest.Attributes.Add("bordercolor", "#1b80b6")
        'gvRates.Attributes.Add("bordercolor", "#1b80b6")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtTo.Text = Format(Date.Now, "dd/MMM/yyyy")
        FillBsu()
    End Sub

    Private Sub bindData(ByVal Mode As String)
        doClear()
        Try
            Dim _parameter As String(,) = New String(4, 1) {}
            _parameter(0, 0) = "@STU_ID"
            _parameter(0, 1) = h_STUD_ID.Value.ToString()
            _parameter(1, 0) = "@FROMDATE"
            _parameter(1, 1) = txtFrom.Text.ToString()
            _parameter(2, 0) = "@TODATE"
            _parameter(2, 1) = txtTo.Text.ToString()
            _parameter(3, 0) = "@BSU_ID"
            _parameter(3, 1) = drpBusinessunit.SelectedValue.ToString()
            _parameter(4, 0) = "@MODE"
            _parameter(4, 1) = Mode

            Dim _table As DataTable = MainObj.getRecords("Student_Reconcilation", _parameter, "OASIS_TRANSPORTConnectionString")

            If _table.Rows.Count > 0 Then
                btnNext.Visible = True
                btnPrevious.Visible = True 
                h_STUD_ID.Value = _table.Rows(0)("STU_ID")
                txtStudName.Text = _table.Rows(0)("STU_NAME")
                txtGrade.Text = _table.Rows(0)("GRD_DISPLAY")
                txtStuNo.Text = _table.Rows(0)("STU_NO")

                gvBind(gvAreaChange, _table, "MODE='AREACHANGE'", TbChange)
                gvBind(gvDiscontinue, _table, "MODE='DISCONTINUE'", TbDiscontinue)
                gvBind(gvSchedule, _table, "MODE='SCHEDULE'", TbSchedule)
                gvBind(gvService, _table, "MODE='SERVICE'", TbService)
                gvBind(gvTrnRequest, _table, "MODE='TRANSPORTREQ'", TbRequest)
            Else
                'lblError.Text = "Details Not Avilable.! "
                usrMessageBar.ShowNotification("Details Not Avilable.! ", UserControls_usrMessageBar.WarningType.Danger)
                txtStudName.Text = ""
                txtStuNo.Text = ""
                txtGrade.Text = ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub hideYN(ByVal dtView As DataView, ByVal TbName As Control)
        If dtView.Count > 0 Then
            TbName.Visible = True
        End If
    End Sub

    Sub gvBind(ByVal gvName As GridView, ByVal _table As DataTable, ByVal viewFillter As String, ByVal TbName As Control)
        Dim dtView As New DataView(_table)
        dtView.RowFilter = viewFillter
        'dtView.Sort = "ONE"
        If dtView.Count > 0 Then
            TbName.Visible = True
            gvName.DataSource = dtView
            gvName.DataBind()
        End If
    End Sub

    Sub doClear()
        gvAreaChange.DataSource = Nothing
        gvAreaChange.DataBind()

        gvDiscontinue.DataSource = Nothing
        gvDiscontinue.DataBind()

        gvSchedule.DataSource = Nothing
        gvSchedule.DataBind()

        gvService.DataSource = Nothing
        gvService.DataBind()

        gvTrnRequest.DataSource = Nothing
        gvTrnRequest.DataBind()

        TbChange.Visible = False
        TbDiscontinue.Visible = False
        TbRequest.Visible = False
        TbSchedule.Visible = False
        TbService.Visible = False
        'btnNext.Visible = False
        'btnPrevious.Visible = False
    End Sub

    Sub FillBsu()
        drpBusinessunit.DataSource = FeeCommon.SERVICES_BSU_M(Session("sUsr_id"), Session("sBSuid"))
        'MainObj.getRecords("SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_bSHOW = 1 ORDER BY BSU_NAME", "OASISConnectionString")
        drpBusinessunit.DataTextField = "BSU_NAME"
        drpBusinessunit.DataValueField = "SVB_BSU_ID"
        drpBusinessunit.DataBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub txtStudName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (h_STUD_ID.Value <> "") Then
            bindData("NORMAL")
        End If
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindData("NEXT")
    End Sub

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindData("PREVIOUS")
    End Sub
    
    Protected Sub drpBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        doClear()
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
      Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()
        Try
            If contextKey.Split("|").Length > 0 Then
                Dim AcdId As String = contextKey.Split("|")(0)
                Dim Area As String = contextKey.Split("|")(1)
                Dim drReader As SqlDataReader
                Dim SqlQry = "SELECT AREA,TERM,REPLACE(CONVERT(VARCHAR,STARTDATE,106),' ','/')STARTDATE,REPLACE(CONVERT(VARCHAR,ENDDATE,106),' ','/')ENDDATE,AMOUNT,SBL_ID FROM TRANSPORT.FN_getAREARATE (" & AcdId & ") WHERE SBL_ID = '" & Area & "' "
                drReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, SqlQry)
                If Not drReader.HasRows Then
                    Return ""
                End If
                sTemp.Append("<table class ='popdetails'>")
                sTemp.Append("<tr>")
                sTemp.Append("<td colspan=5><b>Area</b></td>")
                sTemp.Append("</tr>")
                sTemp.Append("<tr>")
                sTemp.Append("<td><b>Area</b></td>")
                sTemp.Append("<td><b>Term</b></td>")
                sTemp.Append("<td><b>Start Date</b></td>")
                sTemp.Append("<td><b>End Date</b></td>")
                sTemp.Append("<td><b>Amount</b></td>")
                sTemp.Append("</tr>")

                While (drReader.Read())
                    sTemp.Append("<tr>")
                    If drReader("AREA").ToString() = "" Then
                        Return ""
                    End If
                    sTemp.Append("<td>" & drReader("AREA").ToString & "</td>")
                    sTemp.Append("<td>" & drReader("TERM").ToString & "</td>")
                    sTemp.Append("<td>" & drReader("STARTDATE").ToString & "</td>")
                    sTemp.Append("<td>" & drReader("ENDDATE").ToString & "</td>")
                    sTemp.Append("<td>" & AccountFunctions.Round(drReader("AMOUNT")) & "</td>")
                    sTemp.Append("</tr>")
                End While
                drReader.Close()
                sTemp.Append("</table>")
            End If
        Catch ex As Exception
        End Try
        Return sTemp.ToString()
    End Function

    Protected Sub txtTo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (h_STUD_ID.Value <> "") Then
            bindData("NORMAL")
        End If
    End Sub

    Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (h_STUD_ID.Value <> "") Then
            bindData("NORMAL")
        End If
    End Sub

End Class

