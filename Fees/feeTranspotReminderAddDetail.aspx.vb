Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Data.SqlTypes
Imports UtilityObj
Partial Class Fees_feeTranspotReminderAddDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ClientScript.RegisterStartupScript(Me.GetType(), _
                "script", "<script language='javascript'>  CheckOnPostback(); </script>")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_REMINDER Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                ddlBusinessunit.DataBind()
                'BindAcademicYear(ddlBusinessunit.SelectedValue)
                FillACD()
                Session("liUserList") = New ArrayList
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "view" Then
                    Dim FRH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
                    Dim vFEE_REM As FEEReminderTransport = FEEReminderTransport.GetHeaderDetails(FRH_ID)
                    If vFEE_REM Is Nothing Then
                        Exit Sub
                    End If
                    txtAsOnDate.Text = Format(vFEE_REM.FRH_ASONDATE, OASISConstants.DateFormat)
                    txtDate.Text = Format(vFEE_REM.FRH_DT, OASISConstants.DateFormat)
                    Select Case vFEE_REM.FRH_Level
                        Case ReminderType.FIRST
                            radFirstRM.Checked = True
                        Case ReminderType.SECOND
                            radSecRM.Checked = True
                        Case ReminderType.THIRD
                            radThirdRM.Checked = True
                    End Select
                End If
                If radGrade.Checked = True Then
                    BindGrade()
                Else
                    BindBus()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        'If gvFEEReminder.Rows.Count > 0 Then
        '    Dim s As HtmlControls.HtmlImage
        '    Dim pControl As String

        '    pControl = pImg
        '    Try
        '        s = gvFEEReminder.HeaderRow.FindControl(pControl)
        '        If p_imgsrc <> "" Then
        '            s.Src = p_imgsrc
        '        End If
        '        Return s.ClientID
        '    Catch ex As Exception
        '        Return ""
        '    End Try
        'End If
        Return ""
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If (Session("liUserList") Is Nothing) OrElse (Session("liUserList").Count <= 0) Then
            'lblError.Text = "Please Select atleast 1 Student..."
            'Exit Sub
        End If
        Dim conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim trans As SqlTransaction
        Dim bGrade As Boolean
        Dim xml_ids As String
        'Dim grade_ids As Hashtable = GetGradeList()
        'Dim bus_ids As Hashtable = GetBusList()
        If radGrade.Checked = True Then
            xml_ids = GetSelectedList()
        Else
            xml_ids = GetSelectedBus()
        End If
        trans = conn.BeginTransaction("FEE_REMINDER")
        Dim dtAsOndate As DateTime = CDate(txtAsOnDate.Text)
        Dim dtDate As DateTime = CDate(txtDate.Text)
        Dim vFEE_REM As New FEEReminderTransport
        vFEE_REM.FRH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_REM.FRH_ASONDATE = dtAsOndate
        vFEE_REM.FRH_DT = dtDate
        If radFirstRM.Checked Then
            vFEE_REM.FRH_Level = ReminderType.FIRST
        ElseIf radSecRM.Checked Then
            vFEE_REM.FRH_Level = ReminderType.SECOND
        ElseIf radThirdRM.Checked Then
            vFEE_REM.FRH_Level = ReminderType.THIRD
        End If
        If radGrade.Checked = True Then
            bGrade = True
        Else
            bGrade = False
        End If
        vFEE_REM.FRH_BSU_ID = Session("sBSUID")
        vFEE_REM.FRH_STU_BSU_ID = ddlBusinessunit.SelectedValue
        Try
            Dim vFRH_ID As Integer
            Dim retVal As Integer = FEEReminderTransport.SaveFEEReminderDetails(xml_ids, vFEE_REM, vFRH_ID, bGrade, conn, trans)
            If retVal > 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", vFRH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                If ChkPrint.Checked Then
                    Session("ReportSource") = FEEReminder.PrintReminder(vFRH_ID, Session("sUsr_name"))
                    h_print.Value = "print"
                End If
                Response.Redirect("feeTranspotReminderView.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add"))
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        Finally
            conn.Close()
        End Try

    End Sub

    Private Function GetSelectedList() As String
        Dim strSelect As New StringBuilder
        For Each node As TreeNode In tvGrade.CheckedNodes
            'If ((Not node.ChildNodes Is Nothing) OrElse node.ChildNodes.Count <= 0) AndAlso (node.Value.Length) > 2 Then
            If ((Not node.ChildNodes Is Nothing) AndAlso node.ChildNodes.Count <= 0) Then
                strSelect.Append(node.Value)
                strSelect.Append("||")
            End If
        Next
        Return strSelect.ToString()
    End Function

    Private Sub ClearDetails()
        txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        radThirdRM.Checked = True
        'lblError.Text = ""
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'GridBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'BindAcademicYear(ddlBusinessunit.SelectedValue)
        FillACD()
        If radBus.Checked = True Then
            BindBus()
        Else
            BindGrade()
        End If
    End Sub

    Function GetGradeQuery() As String
        Dim strFilter As String = ""
        Dim str_Bsu As String = ""
        Dim str_Bsu1 As String = ""
        Dim str_query As String = "SELECT GRM_GRD_ID,GRM_DESCR,SCT_ID,SCT_DESCR FROM VW_OSO_GRADE_BSU_M " _
                                        & " INNER JOIN VV_SECTION_M ON GRM_ID=VV_SECTION_M.SCT_GRM_ID " _
                                        & "WHERE GRM_BSU_ID ='" + ddlBusinessunit.SelectedValue + "' AND GRM_ACD_ID = " & ddlAcademicYear.SelectedValue
        Dim str_Veh As String = ""
        Dim str_Area As String = ""
        Return str_query + " ORDER BY GRM_GRD_ID FOR XML AUTO"
    End Function

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        str_query = GetGradeQuery()
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String = ""
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("GRM_GRD_ID", "ID")
        str = str.Replace("GRM_DESCR", "TEXT")
        str = str.Replace("SCT_ID", "ID")
        str = str.Replace("SCT_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvGrade.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvGrade.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvGrade.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)
        'If Session("sbsuid") <> "900501" And Session("sbsuid") <> "900500" Then
        tvGrade.ExpandAll()
        lblTitle.Text = "Grade"
        'End If
    End Sub

    Sub BindBus()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String
        str_query = GetBusListQuery()
       
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String = ""
        'Dim ntr As Integer
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString

        str = str.Replace("BNO_ID", "ID")

        str = str.Replace("BNO_DESCR", "TEXT")
        'str = str.Replace("VEH_REGNO", "ID")
        'str = str.Replace("VEH_ID", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvGrade.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvGrade.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvGrade.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)
        'If Session("sbsuid") <> "900501" And Session("sbsuid") <> "900500" Then
        tvGrade.ExpandAll()
        lblTitle.Text = "Bus List"
        'tvCategory.ShowExpandCollapse = True
        'End If
    End Sub

    Function GetBusListQuery() As String
        Dim str_Bsu1 As String = ""
        Dim str_Bsu As String = ""

        Dim str_query As String = "SELECT BNO_ID,BNO_DESCR from TRANSPORT.TRIPS_M A " _
                                    & " INNER JOIN TRANSPORT.TRIPS_D C ON A.TRP_ID=C.TRD_TRP_ID" _
                                    & " INNER JOIN TRANSPORT.BUSNOS_M D ON C.TRD_BNO_ID=BNO_ID " _
                                    & " WHERE BNO_BSU_ID=" + ddlBusinessunit.SelectedValue + " " _
                                    & " FOR XML AUTO"
        Return str_query
    End Function

    Protected Sub radBus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindBus()
    End Sub

    Protected Sub radGrade_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrade()
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub ChkPrint_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Function GetSelectedBus() As String
        Dim strSelect As New StringBuilder
        For Each node As TreeNode In tvGrade.CheckedNodes
            If node.Value.Length > 0 Then
                strSelect.Append(node.Value)
                strSelect.Append("||")
            End If
        Next
        Return strSelect.ToString()

    End Function

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

End Class


