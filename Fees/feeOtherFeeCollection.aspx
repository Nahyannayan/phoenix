<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeOtherFeeCollection.aspx.vb" Inherits="Fees_feeOtherFeeCollection"
    Title="Untitled Page" %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">

    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
function CheckForPrint() {
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        Popup('../Reports/ASPX Report/RptViewerModal.aspx ');
    }
}
    );

<%--function getAccount() {
    var sFeatures;
    sFeatures = "dialogWidth: 709px; ";
    sFeatures += "dialogHeight: 434px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;

    result = window.showModalDialog("..\/accounts\/ShowAccount.aspx?ShowType=NOTCC&codeorname=" + document.getElementById('<%=txtDAccountCode.ClientID %>').value, "", sFeatures);

    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('||');
    document.getElementById('<%=txtDAccountCode.ClientID %>').value = NameandCode[0];
    document.getElementById('<%=txtDAccountName.ClientID %>').value = NameandCode[1];
    return false;
}--%>
//function PayTransportFee(url) {
//    var sFeatures;
//    sFeatures = "dialogWidth: 755px; ";
//    sFeatures += "dialogHeight: 670px; ";
//    sFeatures += "help: no; ";
//    sFeatures += "resizable: no; ";
//    sFeatures += "scroll: yes; ";
//    sFeatures += "status: no; ";
//    sFeatures += "unadorned: no; ";
//    var NameandCode;
//    var result;
//    result = window.showModalDialog(url, "", sFeatures);
//    if (result == '' || result == undefined) {
//        return false;
//    }
//    return false;
//}
<%--function getBankOrEmirate(mode, ctrl) {
    var sFeatures, url;
    sFeatures = "dialogWidth: 600px; ";
    sFeatures += "dialogHeight: 500px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    if (mode == 1)
        url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
    else
        url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('___');
    if (mode == 1) {
        if (ctrl == 1) {
            document.getElementById('<%= h_Bank.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
        }
    }
    else {
    }
    return false;
}--%>

        function Showdata(mode) {
            var sFeatures, url;
            sFeatures = "dialogWidth:750px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var STU_ID = document.getElementById('').value;
            var result;
            if (mode == 1)
                url = "../common/PopupShowData.aspx?id=RECEIPTHISTORY&stuid=" + STU_ID;
            else if (mode == 2)
                url = "../common/PopupShowData.aspx?id=CHARGEHISTORY&stuid=" + STU_ID;
            else if (mode == 3)
                url = "../common/PopupShowData.aspx?id=SIBBLINGS&stuid=" + STU_ID;
            else if (mode == 4)
                url = "../common/PopupShowData.aspx?id=CONCESSION&stuid=" + STU_ID;
            else if (mode == 5)
                url = "../common/PopupShowData.aspx?id=ADJUSTMENT&stuid=" + STU_ID;
            else if (mode == 6)
                url = "../common/PopupShowData.aspx?id=CHQBOUNCE&stuid=" + STU_ID;
            else if (mode == 7)
                url = "../common/PopupShowData.aspx?id=ADVPAY&stuid=" + STU_ID;
            else if (mode == 8)
                url = "../common/PopupShowData.aspx?id=AGING&stuid=" + STU_ID;
            else if (mode == 9)
                url = "FeeCalculateFee.aspx";
            result = window.showModalDialog(url, "", sFeatures);
            return false;
        }

        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
    var amt;
    amt = parseFloat(roundOff)
    if (isNaN(amt))
        amt = 2;
    return amt;
}
function CheckAmount(e) {
    var amt;
    amt = parseFloat(e.value)
    if (isNaN(amt))
        amt = 0;
    e.value = amt.toFixed(getRoundOff());
    return true;
}
function UpdateSum() {

    var txtCCTotal, txtChequeTotal, txtCashTotal,
            txtTotal, txtReturn, txtReceivedTotal, txtBalance, txtBankTotal;
    try {
        txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
    }
    catch (e) { }
    if (isNaN(txtCCTotal))
        txtCCTotal = 0;
    try {
        txtChequeTotal = parseFloat(document.getElementById('<%=txtChequeTotal.ClientID %>').value);
    }
    catch (e) { }

    if (isNaN(txtChequeTotal))
        txtChequeTotal = 0;
    try {
        txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
            }
            catch (e) { }

            if (isNaN(txtCashTotal))
                txtCashTotal = 0;

            if (isNaN(txtTotal))
                txtTotal = 0;
            txtReceivedTotal = txtCCTotal + txtChequeTotal + txtCashTotal;
            txtBankTotal = txtChequeTotal;
            txtBalance = 0;
            if (txtCashTotal > 0)
                if (txtReceivedTotal > txtTotal) {
                    txtBalance = txtReceivedTotal - txtTotal;
                    if (txtBalance > txtCashTotal)
                        txtBalance = 0;
                }
    //document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtReceivedTotal.toFixed(getRoundOff());
    try {
        document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());
    } catch (e) { }
    try {
        document.getElementById('<%=txtChequeTotal.ClientID %>').value = txtBankTotal.toFixed(getRoundOff());
            } catch (e) { }
            try {
                document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
            } catch (e) { }


            if (txtCCTotal >= 0) {
                CalculateRate(txtCCTotal);
            }
        }
        function CalculateRate(CardAmount) {
            var temp = new Array();
            temp = document.getElementById('<%=hfCobrand.ClientID %>').value.split("|");
            var ddlist = document.getElementById('<%=ddCreditcard.ClientID %>');

            if (temp.length > 0 && ddlist != null) {
                for (var i = 0; i < temp.length; i++) {
                    if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                        document.getElementById('<%=txtCrCharge.ClientID %>').value = Math.ceil(CardAmount * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff());
                        //document.getElementById('<%=txtReceivedTotal.ClientID %>').value = CardAmount + Math.ceil(CardAmount * (temp[i].split("=")[1] / 100.00));
                        document.getElementById('<%=txtChargeTotal.ClientID %>').value = CardAmount + Math.ceil(CardAmount * (temp[i].split("=")[1] / 100.00));
                    }
                }
            }
        }
        function allownumber() {
            var e = window.event;
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);

            }
            if (key == 13) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }
    </script>
    <script>
    

        function getAccount() {
             url = "..\/accounts\/ShowAccount.aspx?ShowType=NOTCC&codeorname=" + document.getElementById('<%=txtDAccountCode.ClientID %>').value;

             var oWnd = radopen(url, "pop_getAccount");
         }
         function OnClientClose1(oWnd, args) {

             //get the transferred arguments
             var arg = args.get_argument();
             if (arg) {

                 NameandCode = arg.NameandCode.split('||');

                 document.getElementById('<%=txtDAccountCode.ClientID %>').value = NameandCode[0];
                 document.getElementById('<%=txtDAccountName.ClientID %>').value = NameandCode[1];
            }
        }

        function getBankOrEmirate(mode, ctrl) {

            var url;
            document.getElementById('<%= hf_mode.ClientID%>').value = mode
            document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
            if (mode == 1)
                url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_getBankOrEmirate");

        }
        function OnClientClose3(oWnd, args) {
                
            var mode = document.getElementById('<%= hf_mode.ClientID%>').value
                var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {

                    NameandCode = arg.NameCode.split('||');
                    if (mode == 1) {
                        if (ctrl == 1) {
                            document.getElementById('<%= h_Bank.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
                    }
                }
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
                  <Windows>
            <telerik:RadWindow ID="pop_getAccount" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
                                <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Other Fee Collection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="1%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Receipt No</span>
                        </td>
                        <td align="left" colspan="1" class="matters" width="30%">
                            <asp:TextBox ID="txtRefno" runat="server" Width="90px" TabIndex="80" AutoCompleteType="Disabled" Enabled="false">
                            </asp:TextBox>
                        </td>
                        <td width="20%"><span class="field-label">Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" Width="100px" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Collection</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddCollection" runat="server" DataSourceID="sdsCollection" DataTextField="COL_DESCR"
                                DataValueField="COL_ID" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" class="matters"><span class="field-label">Payment Mode</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlPaymentMode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPaymentMode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr id="trCurrency" runat="server">
                        <td colspan="2"></td>
                        <td align="right" class="matters" colspan="2">
                            <asp:Panel ID="pnlCurrency" runat="server">
                                <table style="width: 100% !important;">
                                    <tr>
                                        <td class="matters" width="40%"><span class="field-label">Currency</span>
                                        </td>
                                        <td class="matters" width="60%">
                                            <asp:DropDownList ID="ddCurrency" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="matters"><span class="field-label">Exchange Rate</span>
                                        </td>
                                        <td class="matters">
                                            <asp:Label ID="lblExgRate" runat="server" CssClass="field-value"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Account Code</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtDAccountCode" runat="server" AutoPostBack="True" CssClass="inputbox"
                                Width="20%" OnTextChanged="txtDAccountCode_TextChanged">
                            </asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txtDAccountCode" ErrorMessage="Account Code Cannot Be Empty"
                                ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton ID="btnAccount"
                                    runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;"
                                    TabIndex="14"></asp:ImageButton></td>
                        <td align="left" class="matters"><span class="field-label">Account Name</span></td>
                        <td>
                            <asp:TextBox ID="txtDAccountName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Narration</span>
                        </td>
                        <td align="left" class="matters" colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" Height="32px" TextMode="MultiLine" SkinID="MultiText"
                                Width="424px" TabIndex="160"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr class="title-bg">
                        <td align="left" colspan="6">Payment Details
                        </td>
                    </tr>
                    <tr id="tr_Cash" runat="server">
                        <td align="left" class="matters" width="20%"><span class="field-label">Cash</span>
                        </td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtCashTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.select();"
                                onBlur="UpdateSum();" Style="text-align: right" TabIndex="45"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtCashTotal"
                                FilterType="Numbers,Custom" ValidChars="." />


                        </td>
                        <td align="left" rowspan="1" class="matters" colspan="4" width="60%">
                            <asp:Button ID="btnAddCash" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="46" />
                        </td>
                    </tr>
                    <tr id="tr_CreditCard" runat="server">
                        <td colspan="6">
                            <center>
                    <table  >
                        <tr class="title-bg">
                            <td align="left" class="matters_Colln">Card Type
                            </td>
                            <td align="left" class="matters_Colln">Auth Code
                            </td>
                            <td align="left" class="matters_Colln">Amount
                            </td>
                            <td align="left" class="matters_Colln">Charge
                            </td>
                            <td align="left" class="matters_Colln">Total
                            </td>
                            <td align="left" class="matters_Colln"></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard" DataTextField="CRI_DESCR"
                                    DataValueField="CRR_ID" SkinID="DropDownListNormal" TabIndex="55" onChange="UpdateSum();">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="56"
                                    ></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCCTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.select();"
                                    onBlur="UpdateSum();" Style="text-align: right" TabIndex="57"  ></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                    TargetControlID="txtCCTotal" FilterType="Numbers,Custom" ValidChars="." />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCrCharge" runat="server" AutoCompleteType="Disabled" Text="0.00"
                                    Style="text-align: right" TabIndex="58"  Enabled="false"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChargeTotal" runat="server" AutoCompleteType="Disabled" Text="0.00"
                                    Style="text-align: right" TabIndex="59"  Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnAddCreditcard" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                    TabIndex="60" />
                            </td>
                        </tr>
                    </table>
                </center>
                        </td>
                    </tr>
                    <tr id="tr_Cheque1" runat="server">
                        <td align="left" class="matters" width="15%"><span class="field-label">Cheque Amount</span>
                        </td>
                        <td align="left" width="15%">
                            <asp:TextBox ID="txtChequeTotal" onBlur="UpdateSum();" onFocus="this.select();" runat="server"
                                Style="text-align: right" TabIndex="85" AutoCompleteType="Disabled"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                TargetControlID="txtChequeTotal" FilterType="Numbers,Custom" ValidChars="." />
                        </td>
                       
         <td align="left" class="matters"><span class="field-label">Bank</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBank" runat="server" AutoPostBack="True" TabIndex="86"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton5" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getBankOrEmirate(1,1); return false;" TabIndex="87" />
                                        <asp:HiddenField ID="hf_BNK_TYPE" runat="server" />
                                    </td>

                        <td align="left" class="matters" width="15%"><span class="field-label">Emirate</span>
                        </td>
                        <td align="left" width="15%">
                            <asp:DropDownList ID="ddlEmirate" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="89">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr_Cheque2" runat="server">
                        <td align="left" class="matters"><span class="field-label">Chq. No.</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqno" runat="server" TabIndex="90" AutoCompleteType="Disabled">
                            </asp:TextBox>
                        </td>
                        <td align="left" class="matters"  ><span class="field-label">Date</span> </td>
                        <td>
                            <asp:TextBox ID="txtChqDate" runat="server" TabIndex="91"></asp:TextBox>
                            <asp:ImageButton ID="imgChequedate" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="92" />
                        </td>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnAddCheque" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="93" OnClick="btnAddCheque_Click" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr class="title-bg">
                        <td colspan="6" align="left">Collection Details
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                CssClass="table table-row table-bordered" DataKeyNames="REF_ID,PayMode">
                                <Columns>
                                    <asp:BoundField DataField="REF_DESCR" HeaderText="Descr" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="REF_NO" HeaderText="Ref No." ReadOnly="True"></asp:BoundField>
                                    <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CrAmount" DataFormatString="{0:0.00}" HeaderText="Amount">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CrCharge" DataFormatString="{0:0.00}" HeaderText="Charge">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CrTotal" DataFormatString="{0:0.00}" HeaderText="Total">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FCAmount" DataFormatString="{0:0.00}" HeaderText="Paid(FC)"
                                        ReadOnly="True" Visible="false">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AMOUNT" DataFormatString="{0:0.00}" HeaderText="Paid"
                                        ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="middle"><span class="field-label">Total</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtReceivedTotal" Style="text-align: right;width: 180px;min-width: 10% !important" runat="server"></asp:TextBox>
                            <asp:HiddenField ID="hfCobrand" runat="server" />
                            <asp:HiddenField ID="hf_mode" runat="server" />
                            <asp:HiddenField ID="hf_ctrl" runat="server" />
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button
                                ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" /><asp:Button
                                    ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                    SelectCommand="SELECT [EMR_CODE], [EMR_DESCR] FROM [EMIRATE_M]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="GetCreditCardListForCollection" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="CollectFrom" DefaultValue="FEES" Type="String" />
                        <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sdsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="SELECT [COL_ID], [COL_DESCR], [COL_ACT_ID] FROM [COLLECTION_M] WHERE ([COL_ID] <> 1)"></asp:SqlDataSource>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_Bank" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calCheque" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgChequedate" TargetControlID="txtChqDate" Format="dd/MMM/yyyy"
                    Enabled="True" PopupPosition="TopLeft">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
