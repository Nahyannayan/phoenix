Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Collections.Generic
Imports System.Linq


Partial Class Fees_ImportTransportRate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Private Property VSgvExcelImport() As DataTable
    '    Get
    '        Return ViewState("gvExcelImport")
    '    End Get
    '    Set(ByVal value As DataTable)
    '        ViewState("gvExcelImport") = value
    '    End Set
    'End Property
    Private Property VSgvTrRateImport() As DataTable
        Get
            Return ViewState("gvTrRateImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvTrRateImport") = value
        End Set
    End Property
    Private Property VSGridRowCount() As Integer
        Get
            Return ViewState("GridRowCount")
        End Get
        Set(ByVal value As Integer)
            ViewState("GridRowCount") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImpTrRate)
        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            'lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ClearAllFields()
            FillBSU()
            ddlTBSU_SelectedIndexChanged(sender, e)
            LoadCollectionSchedule()
            LoadProrata()
            chkDefault.Checked = True
            chkDefault_CheckedChanged(Nothing, Nothing)
            lnkTrRateFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("TRANSPORT RATE") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/TransportRateFormat.xls")
        End If
    End Sub

    Sub FillBSU()
        Dim dt As New DataTable
        'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        'CommandType.Text, "select BSU_ID as SVB_BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') order by BSU_NAME")
        dt = FeeCommon.SERVICES_BSU_M(Session("sUsr_name"), Session("sBsuId"))
        ddlTBSU.DataSource = dt
        ddlTBSU.DataBind()
    End Sub

    Private Sub ClearAllFields()
        Me.lblError3.Text = ""
        'Me.lblError.Text = ""
        Me.lblXLOneway.Text = ""
        Me.lblXLNormal.Text = ""
        If Not VSgvTrRateImport Is Nothing Then
            VSgvTrRateImport.Clear()
        End If
        BindTrRateExcel()
        Me.gvTrRateImportSummary.DataSource = ""
        Me.gvTrRateImportSummary.DataBind()
        Me.btntrProceed.Visible = False
        Me.btnTrCommit.Visible = False
        Me.btnImpTrRate.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call ClearAllFields()
    End Sub

    '--------------Transport Rate Import-----------------------

    Protected Sub ddlTBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearAllFields()
        fillTranAcdY()
    End Sub
    Sub fillTranAcdY()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Me.ddlTBSU.SelectedValue)
        ddlACDYear.DataSource = dtACD
        ddlACDYear.DataTextField = "ACY_DESCR"
        ddlACDYear.DataValueField = "ACD_ID"
        ddlACDYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlACDYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnImpTrRate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim conn As New OleDbConnection
        'Me.lblError.Text = ""
        Try
            Dim str_conn As String = ""
            Dim cmd As New OleDbCommand
            Dim da As New OleDbDataAdapter
            Dim dtXcel As DataTable
            Dim FileName As String = ""
            If (FileUpload2.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload2.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload2.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload2.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar2.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If

                dtXcel = Mainclass.FetchFromExcelIntoDataTable(FileName, 1, 1, 10)
                If Not dtXcel Is Nothing AndAlso dtXcel.Rows.Count > 0 Then
                    If Not dtXcel.Columns.Contains("NEW_TOTAL") Then
                        dtXcel.Columns.Add(New DataColumn("NEW_TOTAL", GetType(System.Double), 0))
                    End If
                    If Not dtXcel.Columns.Contains("EXISTING_TOTAL") Then
                        dtXcel.Columns.Add(New DataColumn("EXISTING_TOTAL", GetType(System.Double), 0))
                    End If
                    If Not dtXcel.Columns.Contains("DIFF_TOTAL") Then
                        dtXcel.Columns.Add(New DataColumn("DIFF_TOTAL", GetType(System.Double), 0))
                    End If
                    Dim columns As List(Of DataControlField) = gvTrRateImport.Columns.Cast(Of DataControlField)().ToList()
                    columns.Find(Function(col) col.HeaderText = "New Rate").Visible = False
                    columns.Find(Function(col) col.HeaderText = "Existing Rate").Visible = False
                    columns.Find(Function(col) col.HeaderText = "Difference").Visible = False

                    dtXcel.AcceptChanges()
                    VSgvTrRateImport = dtXcel.Copy
                    'BindTrRateExcel()
                    ValidateRate(dtXcel)
                    'ValidateAndSaveTrRates(dtXcel)
                Else
                    'Me.lblError.Text += "Empty Document!!"
                    usrMessageBar2.ShowNotification("Empty Document!!", UserControls_usrMessageBar.WarningType.Danger)
                End If

            Else
                'Me.lblError.Text += "File not Found!!"
                usrMessageBar2.ShowNotification("File not Found!!", UserControls_usrMessageBar.WarningType.Danger)
            End If

            File.Delete(FileName) 'delete the file after copying the contents
        Catch
            'If conn.State = ConnectionState.Open Then
            '    conn.Close()
            'End If
            'Me.lblError.Text += Err.Description
            usrMessageBar2.ShowNotification(Err.Description, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Sub BindTrRateExcel()
        If Not VSgvTrRateImport Is Nothing Then
            Me.gvTrRateImport.DataSource = VSgvTrRateImport
            Me.gvTrRateImport.DataBind()
        Else
            'Me.trGvImport.Visible = False
        End If

    End Sub

    Protected Sub gvTrRateImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Me.gvTrRateImport.PageIndex = e.NewPageIndex
        BindTrRateExcel()
    End Sub

    Protected Sub gvTrRateImport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Footer AndAlso Not VSgvTrRateImport Is Nothing Then
            Dim N1, N2, N3, Way1, Way2, Way3, NewRate, CurrentRate, RateDiff As New Double

            N1 = VSgvTrRateImport.Compute("Sum(T1NORMAL_RATE)", "[T1NORMAL_RATE] IS NOT NULL").ToString
            N2 = VSgvTrRateImport.Compute("Sum(T2NORMAL_RATE)", "[T2NORMAL_RATE] IS NOT NULL").ToString
            N3 = VSgvTrRateImport.Compute("Sum(T3NORMAL_RATE)", "[T3NORMAL_RATE] IS NOT NULL").ToString
            Way1 = FeeCollection.GetDoubleVal(VSgvTrRateImport.Compute("Sum(T1ONEWAY_RATE)", "[T1ONEWAY_RATE] IS NOT NULL").ToString)
            Way2 = FeeCollection.GetDoubleVal(VSgvTrRateImport.Compute("Sum(T2ONEWAY_RATE)", "[T2ONEWAY_RATE] IS NOT NULL").ToString)
            Way3 = FeeCollection.GetDoubleVal(VSgvTrRateImport.Compute("Sum(T3ONEWAY_RATE)", "[T3ONEWAY_RATE] IS NOT NULL").ToString)
            NewRate = FeeCollection.GetDoubleVal(VSgvTrRateImport.Compute("Sum(NEW_TOTAL)", "[NEW_TOTAL] IS NOT NULL").ToString)
            CurrentRate = FeeCollection.GetDoubleVal(VSgvTrRateImport.Compute("Sum(EXISTING_TOTAL)", "[EXISTING_TOTAL] IS NOT NULL").ToString)
            RateDiff = FeeCollection.GetDoubleVal(VSgvTrRateImport.Compute("Sum(DIFF_TOTAL)", "[DIFF_TOTAL] IS NOT NULL").ToString)
            e.Row.Cells(2).Text = "TOTAL :"

            e.Row.Cells(3).Text = N1.ToString("#,##0.00")
            e.Row.Cells(4).Text = Way1.ToString("#,##0.00")
            e.Row.Cells(5).Text = N2.ToString("#,##0.00")
            e.Row.Cells(6).Text = Way2.ToString("#,##0.00")
            e.Row.Cells(7).Text = N3.ToString("#,##0.00")
            e.Row.Cells(8).Text = Way3.ToString("#,##0.00")
            e.Row.Cells(9).Text = NewRate.ToString("#,##0.00")
            e.Row.Cells(10).Text = CurrentRate.ToString("#,##0.00")
            e.Row.Cells(11).Text = RateDiff.ToString("#,##0.00")
        End If
    End Sub

    'Private Sub ValidateAndSaveTrRates(ByRef DT As DataTable)
    '    Dim pParms(12) As SqlClient.SqlParameter
    '    Dim ErrorLog As String = ""
    '    Me.lblError3.Text = ""
    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

    '    Dim qry As String = "SELECT ISNULL(MAX(FTI_BATCHNO),0)+1 FROM [DataImport].[FEE_TRANSPORTRATE_IMPORT]"
    '    Dim FTI_BATCHNO As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, qry)
    '    ViewState("FTI_BATCHNO") = FTI_BATCHNO
    '    Dim i = 1
    '    Dim cmd As New SqlCommand

    '    Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
    '    objConn.Open()
    '    Dim Success As Boolean = True

    '    Dim stTrans As SqlTransaction = objConn.BeginTransaction
    '    For Each Dr As DataRow In DT.Rows
    '        cmd.Dispose()
    '        cmd = New SqlCommand("FIX.ValidateAndImportTrRateExceldata", objConn, stTrans)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.CommandTimeout = 0
    '        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
    '        pParms(0).Value = Me.ddlTBSU.SelectedValue
    '        cmd.Parameters.Add(pParms(0))
    '        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
    '        pParms(1).Direction = ParameterDirection.ReturnValue
    '        cmd.Parameters.Add(pParms(1))

    '        pParms(2) = New SqlClient.SqlParameter("@FLOC", SqlDbType.VarChar)
    '        pParms(2).Value = Dr("Loc").ToString
    '        cmd.Parameters.Add(pParms(2))
    '        pParms(3) = New SqlClient.SqlParameter("@TLOC", SqlDbType.VarChar)
    '        pParms(3).Value = Dr("Area").ToString
    '        cmd.Parameters.Add(pParms(3))
    '        pParms(4) = New SqlClient.SqlParameter("@FTI_1NORMAL", SqlDbType.Decimal)
    '        pParms(4).Value = Val(Dr("1Normal").ToString)
    '        cmd.Parameters.Add(pParms(4))
    '        pParms(5) = New SqlClient.SqlParameter("@FTI_1ONEWAY", SqlDbType.Decimal)
    '        pParms(5).Value = Val(Dr("1OneWay").ToString)
    '        cmd.Parameters.Add(pParms(5))
    '        pParms(6) = New SqlClient.SqlParameter("@FTI_2NORMAL", SqlDbType.Decimal)
    '        pParms(6).Value = Val(Dr("2Normal").ToString)
    '        cmd.Parameters.Add(pParms(6))
    '        pParms(7) = New SqlClient.SqlParameter("@FTI_2ONEWAY", SqlDbType.Decimal)
    '        pParms(7).Value = Val(Dr("2OneWay").ToString)
    '        cmd.Parameters.Add(pParms(7))
    '        pParms(8) = New SqlClient.SqlParameter("@FTI_3NORMAL", SqlDbType.Decimal)
    '        pParms(8).Value = Val(Dr("3Normal").ToString)
    '        cmd.Parameters.Add(pParms(8))
    '        pParms(9) = New SqlClient.SqlParameter("@FTI_3ONEWAY", SqlDbType.Decimal)
    '        pParms(9).Value = Val(Dr("3OneWay").ToString)
    '        cmd.Parameters.Add(pParms(9))
    '        pParms(10) = New SqlClient.SqlParameter("@FTI_USER", SqlDbType.VarChar)
    '        pParms(10).Value = Session("sUsr_name")
    '        cmd.Parameters.Add(pParms(10))
    '        pParms(11) = New SqlClient.SqlParameter("@FTI_BATCHNO", SqlDbType.BigInt)
    '        pParms(11).Value = FTI_BATCHNO
    '        cmd.Parameters.Add(pParms(11))
    '        pParms(12) = New SqlClient.SqlParameter("@FTI_ACD_ID", SqlDbType.Int)
    '        pParms(12).Value = Me.ddlACDYear.SelectedValue
    '        cmd.Parameters.Add(pParms(12))

    '        Dim retval As String
    '        cmd.ExecuteNonQuery()
    '        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "FIX.ValidateAndImportTrRateExceldata", pParms)
    '        retval = pParms(1).Value
    '        If retval <> 0 Then
    '            If Me.gvTrRateImport.Rows.Count > i Then
    '                Me.gvTrRateImport.Rows(i - 1).BackColor = Drawing.Color.Red
    '            End If
    '            ErrorLog = ErrorLog & "@" & retval & "#" & Convert.ToString(Dr("SlNo")) ' keeping errono and line in a string variable for further use
    '        End If
    '        i += 1
    '    Next

    '    If ErrorLog <> "" Then
    '        Me.btntrProceed.Visible = False
    '        Me.btnTrCommit.Visible = False
    '        Me.btnImpTrRate.Visible = True
    '        Me.lblError3.Text = ""
    '        ShowErrorLogTr(ErrorLog)
    '        stTrans.Rollback()
    '        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "delete from [DataImport].[FEE_TRANSPORTRATE_IMPORT] where FTI_BATCHNO='" & FTI_BATCHNO & "'")
    '    Else
    '        stTrans.Commit()
    '        Me.btntrProceed.Visible = True
    '        Me.btnImpTrRate.Visible = False
    '    End If
    '    If objConn.State = ConnectionState.Open Then
    '        objConn.Close()
    '    End If
    'End Sub
    Private Sub ShowErrorLogTr(ByVal Err As String)
        Dim ReturnVal() As String = Err.Split("@")
        For i As Integer = 0 To ReturnVal.Length - 1
            If ReturnVal(i).Trim <> "" Then
                Dim aa() As String = ReturnVal(i).Split("#")
                Me.lblError3.Text = Me.lblError3.Text & "Serial No(" & aa(1) & ") : "
                Me.lblError3.Text = Me.lblError3.Text & IIf(aa(0) = "100", "Business Unit not found,", IIf(aa(0) = "101", "From Location not found,", IIf(aa(0) = "102", "To Location not found,", IIf(aa(0) = "103", "incorrect 1Normal amount,", ""))))
                Me.lblError3.Text = Me.lblError3.Text & IIf(aa(0) = "104", "incorrect 1ONEWAY amount,", IIf(aa(0) = "105", "incorrect 2Normal amount,", IIf(aa(0) = "106", "incorrect 2ONEWAY amount,", IIf(aa(0) = "107", "incorrect 3Normal amount,", ""))))
                Me.lblError3.Text = Me.lblError3.Text & IIf(aa(0) = "108", "incorrect 3ONEWAY amount,", IIf(aa(0) = "109", "Unable to Save,", IIf(aa(0) = "110", "Sublocation not found,", "")))
                Me.lblError3.Text = Me.lblError3.Text & IIf(aa(0) = "2562", "Area already exists for the location on this batch,", "")
            End If
        Next
    End Sub

    Protected Sub btntrProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ImportTrRates(False)
    End Sub
    Private Sub ImportTrRates(ByVal Commit As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString) '
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(7) As SqlParameter
        Dim ErrMsg As String = String.Empty

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = ""
            'sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retval, SqlDbType.VarChar, True, 100)
            sqlParam(0) = New SqlClient.SqlParameter("@ReturnValue", SqlDbType.Int)
            sqlParam(0).Direction = ParameterDirection.ReturnValue
            'sqlParam(1) = Mainclass.CreateSqlParameter("@FTI_BATCHNO", ViewState("FTI_BATCHNO"), SqlDbType.BigInt)
            sqlParam(1) = New SqlClient.SqlParameter("@FTI_BATCHNO", SqlDbType.BigInt)
            sqlParam(1).Value = ViewState("FTI_BATCHNO")
            'sqlParam(2) = Mainclass.CreateSqlParameter("@FTI_ACD_ID", Me.ddlACDYear.SelectedValue, SqlDbType.Int)
            sqlParam(2) = New SqlClient.SqlParameter("@FTI_ACD_ID", SqlDbType.Int)
            sqlParam(2).Value = Me.ddlACDYear.SelectedValue
            'sqlParam(3) = Mainclass.CreateSqlParameter("@OutputXML", "", SqlDbType.Xml, True)
            sqlParam(3) = New SqlClient.SqlParameter("@OutputXML", SqlDbType.Xml)
            sqlParam(3).Direction = ParameterDirection.Output
            'sqlParam(4) = Mainclass.CreateSqlParameter("@JOIN_PRO", ddlJoinPR.SelectedValue, SqlDbType.Int)
            sqlParam(4) = New SqlClient.SqlParameter("@JOIN_PRO", SqlDbType.Int)
            sqlParam(4).Value = ddlJoinPR.SelectedValue
            'sqlParam(5) = Mainclass.CreateSqlParameter("@DISCONTINUE_PRO", ddlDiscontinuePR.SelectedValue, SqlDbType.Int)
            sqlParam(5) = New SqlClient.SqlParameter("@DISCONTINUE_PRO", SqlDbType.Int)
            sqlParam(5).Value = ddlDiscontinuePR.SelectedValue
            'sqlParam(6) = Mainclass.CreateSqlParameter("@COLLECTION_SCHEDULE", Me.ddlCollSchedule.SelectedValue, SqlDbType.Int)
            sqlParam(6) = New SqlClient.SqlParameter("@COLLECTION_SCHEDULE", SqlDbType.Int)
            sqlParam(6).Value = ddlCollSchedule.SelectedValue
            'sqlParam(7) = Mainclass.CreateSqlParameter("@ReturnMsg", ErrMsg, SqlDbType.VarChar, True, 500)
            sqlParam(7) = New SqlClient.SqlParameter("@ReturnMsg", SqlDbType.VarChar, 500)
            sqlParam(7).Direction = ParameterDirection.Output

            Mainclass.ExecuteParamQRY(objConn, stTrans, "FIX.IMPORT_TRANSPORT_RATES", sqlParam)
            retval = sqlParam(0).Value
            ErrMsg = sqlParam(7).Value
            'Me.lblError.Text += IIf(retval <> "0", retval, "")
            'If (retval <> "0") Then
            '    usrMessageBar2.ShowNotification(ErrMsg, UserControls_usrMessageBar.WarningType.Danger)
            'End If

            If Not retval Is Nothing And retval = "0" Then
                If Not sqlParam(3).Value Is DBNull.Value Then
                    Dim readXML As StringReader = New StringReader(HttpUtility.HtmlDecode(sqlParam(3).Value))
                    dsSummary.ReadXml(readXML)
                End If
                If dsSummary.Tables(0).Rows.Count > 0 Then ' AndAlso (Val(dsSummary.Tables(0).Rows(0)(0)) = Val(dsSummary.Tables(0).Rows(0)(2)) And Val(dsSummary.Tables(0).Rows(0)(1)) = Val(dsSummary.Tables(0).Rows(0)(3))) Then
                    Me.btnTrCommit.Visible = True
                    Me.btntrProceed.Visible = False
                    Me.btnImpTrRate.Visible = False
                    Me.gvTrRateImportSummary.DataSource = dsSummary.Tables(0)
                    Me.gvTrRateImportSummary.DataBind()
                Else
                    Me.btntrProceed.Visible = True
                    Me.btnImpTrRate.Visible = True
                    Me.btnTrCommit.Visible = False
                End If
            ElseIf retval = "222" Then
                Commit = False
                Me.lblError3.Text = "Totals not tallying, unable to save"
            Else
                Commit = False
                ErrMsg = IIf(ErrMsg <> "", ErrMsg, "Unable to process the request")
                Me.lblError3.Text = " "
                usrMessageBar2.ShowNotification(ErrMsg, UserControls_usrMessageBar.WarningType.Danger)
            End If

            If Commit = True Then
                stTrans.Commit()
                ClearAllFields()
                'Me.lblError.Text = "Data Saved successfully"
                usrMessageBar2.ShowNotification("Data Saved successfully", UserControls_usrMessageBar.WarningType.Success)
            Else
                Me.lblError3.Text = IIf(Me.lblError3.Text = "", "Click the Commit button to commit the transaction", Me.lblError3.Text)
                stTrans.Rollback()
                'Me.btntrProceed.Visible = False
                'Me.btnImpTrRate.Visible = False
                'Me.btnTrCommit.Visible = True
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "delete from [DataImport].[FEE_TRANSPORTRATE_IMPORT] where FTI_BATCHNO='" & ViewState("FTI_BATCHNO") & "'")
            ViewState("FTI_BATCHNO") = ""
            VSgvTrRateImport.Clear()
            BindTrRateExcel()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub btnTrCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ImportTrRates(True)
    End Sub

    Protected Sub btnCancelTrImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTrImport.Click
        Me.lblError3.Text = ""
        'Me.lblError.Text = ""
        Me.lblXLOneway.Text = ""
        Me.lblXLNormal.Text = ""
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "delete from [DataImport].[FEE_TRANSPORTRATE_IMPORT] where FTI_BATCHNO='" & ViewState("FTI_BATCHNO") & "'")
        ViewState("FTI_BATCHNO") = ""
        VSgvTrRateImport.Clear()
        BindTrRateExcel()
        Me.btntrProceed.Visible = False
        Me.btnTrCommit.Visible = False
        Me.btnImpTrRate.Visible = True
    End Sub
    'Protected Sub lnkTrRateFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTrRateFormat.Click
    '    Try
    '        Dim filePath As String = "~\Template\TransportRateFormat.xls"lnkTrRateFormat
    '        Response.Redirect(filePath)
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.Message
    '    End Try

    'End Sub
    Sub LoadCollectionSchedule()
        Dim dtCollSch As DataTable = FeeCommon.GetSCHEDULE_M(False)
        If Not dtCollSch Is Nothing AndAlso dtCollSch.Rows.Count > 0 Then
            ddlCollSchedule.DataSource = dtCollSch
            ddlCollSchedule.DataValueField = "SCH_ID"
            ddlCollSchedule.DataTextField = "SCH_DESCR"
            ddlCollSchedule.DataBind()
        End If
    End Sub
    Sub LoadProrata()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, "SELECT FPM_ID, FPM_DESCR FROM FEES.FEES_PRORATA_M WITH(NOLOCK) WHERE ISNULL(FPM_Bactive, 0) = 1")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlJoinPR.DataSource = ds.Tables(0)
            ddlJoinPR.DataValueField = "FPM_ID"
            ddlJoinPR.DataTextField = "FPM_DESCR"
            ddlJoinPR.DataBind()

            ddlDiscontinuePR.DataSource = ds.Tables(0)
            ddlDiscontinuePR.DataValueField = "FPM_ID"
            ddlDiscontinuePR.DataTextField = "FPM_DESCR"
            ddlDiscontinuePR.DataBind()
        End If
    End Sub

    Private Function CreateRatesSchema() As DataTable
        Dim dtRates As New DataTable
        Try
            Dim dcloc As New DataColumn("LOC", System.Type.GetType("System.String"))
            Dim dcarea As New DataColumn("AREA", System.Type.GetType("System.String"))
            Dim dct1nr As New DataColumn("T1NORMAL_RATE", System.Type.GetType("System.Double"))
            Dim dct1oneway As New DataColumn("T1ONEWAY_RATE", System.Type.GetType("System.Double"))
            Dim dct2nr As New DataColumn("T2NORMAL_RATE", System.Type.GetType("System.Double"))
            Dim dct2oneway As New DataColumn("T2ONEWAY_RATE", System.Type.GetType("System.Double"))
            Dim dct3nr As New DataColumn("T3NORMAL_RATE", System.Type.GetType("System.Double"))
            Dim dct3oneway As New DataColumn("T3ONEWAY_RATE", System.Type.GetType("System.Double"))

            dtRates.Columns.Add(dcloc)
            dtRates.Columns.Add(dcarea)
            dtRates.Columns.Add(dct1nr)
            dtRates.Columns.Add(dct1oneway)
            dtRates.Columns.Add(dct2nr)
            dtRates.Columns.Add(dct2oneway)
            dtRates.Columns.Add(dct3nr)
            dtRates.Columns.Add(dct3oneway)
        Catch ex As Exception
            dtRates = Nothing
        End Try
        Return dtRates
    End Function
    Private Sub ValidateRate(ByVal DT As DataTable)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim Success As Boolean = True
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        ' Dim cmd As New SqlCommand
        Dim dsOldRates As New DataSet
        ViewState("FTI_BATCHNO") = 0
        Try
            If DT.Columns.Contains("SlNo") Then
                DT.Columns.Remove("SlNo")
                DT.AcceptChanges()
            End If
            Dim pParms(7) As SqlClient.SqlParameter
            ' cmd = New SqlCommand("[DataImport].[VALIDATE_AND_IMPORT_TPT_RATE]", objConn, stTrans)
            'cmd.CommandType = CommandType.StoredProcedure
            'cmd.CommandTimeout = 0
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Me.ddlTBSU.SelectedValue
            ' cmd.Parameters.Add(pParms(0))
            pParms(1) = New SqlClient.SqlParameter("@FTI_ACD_ID", SqlDbType.Int)
            pParms(1).Value = Me.ddlACDYear.SelectedValue
            ' cmd.Parameters.Add(pParms(1))
            pParms(2) = New SqlClient.SqlParameter("@FTI_USER", SqlDbType.VarChar, 50)
            pParms(2).Value = Session("sUsr_name")
            'cmd.Parameters.Add(pParms(2))
            pParms(3) = New SqlClient.SqlParameter("@FTI_BATCHNO", SqlDbType.BigInt)
            pParms(3).Direction = ParameterDirection.Output
            'cmd.Parameters.Add(pParms(3))
            pParms(4) = New SqlClient.SqlParameter("@DT_RATES", SqlDbType.Structured)
            pParms(4).Value = DT
            'cmd.Parameters.Add(pParms(4))
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            'cmd.Parameters.Add(pParms(5))

            Dim retval As String
            'cmd.ExecuteNonQuery()
            dsOldRates = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "[DataImport].[VALIDATE_AND_IMPORT_TPT_RATE]", pParms)
            retval = pParms(5).Value
            If retval <> 0 Then
                Me.btntrProceed.Visible = False
                Me.btnTrCommit.Visible = False
                Me.btnImpTrRate.Visible = True
                stTrans.Rollback()
                'usrMessageBar2.ShowNotification(pParms(6).Value, UserControls_usrMessageBar.WarningType.Danger)
            Else
                stTrans.Commit()
                ViewState("FTI_BATCHNO") = pParms(3).Value
                Me.btntrProceed.Visible = True
                Me.btnImpTrRate.Visible = False
                ShowRateComparison(dsOldRates)
            End If
        Catch ex As Exception
            Me.btntrProceed.Visible = False
            Me.btnTrCommit.Visible = False
            Me.btnImpTrRate.Visible = True
            stTrans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub chkDefault_CheckedChanged(sender As Object, e As EventArgs) Handles chkDefault.CheckedChanged
        If chkDefault.Checked Then
            ddlCollSchedule.Enabled = False
            ddlJoinPR.Enabled = False
            ddlDiscontinuePR.Enabled = False
            Dim QRY As String = "SELECT ISNULL(BUS_TPT_JOIN_FPM_ID, 1) AS BUS_TPT_JOIN_FPM_ID,ISNULL(BUS_TPT_DISCONTINUE_FPM_ID, 1) AS BUS_TPT_DISCONTINUE_FPM_ID,ISNULL(BUS_TPT_COLLECTION_SCH_ID, 2) AS BUS_TPT_COLLECTION_SCH_ID FROM OASIS..BUSINESSUNIT_SUB WITH(NOLOCK) WHERE BUS_BSU_ID = '" & Session("sBsuId") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
            CommandType.Text, QRY)
            Dim BUS_TPT_JOIN_FPM_ID As Integer = 0, BUS_TPT_DISCONTINUE_FPM_ID As Integer = 0, BUS_TPT_COLLECTION_SCH_ID As Integer = 0
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                BUS_TPT_COLLECTION_SCH_ID = ds.Tables(0).Rows(0)("BUS_TPT_COLLECTION_SCH_ID")
                BUS_TPT_DISCONTINUE_FPM_ID = ds.Tables(0).Rows(0)("BUS_TPT_DISCONTINUE_FPM_ID")
                BUS_TPT_JOIN_FPM_ID = ds.Tables(0).Rows(0)("BUS_TPT_JOIN_FPM_ID")
                ddlJoinPR.SelectedValue = BUS_TPT_JOIN_FPM_ID
                ddlDiscontinuePR.SelectedValue = BUS_TPT_DISCONTINUE_FPM_ID
                ddlCollSchedule.SelectedValue = BUS_TPT_COLLECTION_SCH_ID
            End If
        Else
            ddlCollSchedule.Enabled = True
            ddlJoinPR.Enabled = True
            ddlDiscontinuePR.Enabled = True
        End If
    End Sub

    Sub ShowRateComparison(ByVal ds As DataSet)
        If ds.Tables(0).Rows.Count > 0 Then
            VSgvTrRateImport = ds.Tables(0).Copy()
            'Dim n1 = VSgvTrRateImport.Columns("T1NORMAL_RATE").DataType

            Try
                Dim columns As List(Of DataControlField) = gvTrRateImport.Columns.Cast(Of DataControlField)().ToList()
                columns.Find(Function(col) col.HeaderText = "New Rate").Visible = True
                columns.Find(Function(col) col.HeaderText = "Existing Rate").Visible = True
                columns.Find(Function(col) col.HeaderText = "Difference").Visible = True
            Catch ex As Exception

            End Try
        End If
        BindTrRateExcel()
    End Sub
End Class
