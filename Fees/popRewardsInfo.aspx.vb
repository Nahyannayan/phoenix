﻿Imports System.Net
Imports System.IO
Imports Newtonsoft.Json.Linq
Imports Microsoft.ApplicationBlocks.Data
Imports RestSharp

Partial Class Fees_popRewardsInfo
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Property AVAILABLE_AMOUNT As Double
    Public Property AVAILABLE_POINTS As Integer
    Public Property REDEEMABLE_POINTS As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("sUsr_name") = "" Or Session("sBSuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
            If clsRewards.IS_UAE_SCHOOL(Session("sBSuid")) Then
                If Not Request.QueryString("ID") Is Nothing Then
                    Dim STU_ID As Long = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                    ClearPage()
                    If Not Request.QueryString("MOD") Is Nothing Then
                        Dim MODE As String = Encr_decrData.Decrypt(Request.QueryString("MOD").Replace(" ", "+"))
                        h_Module.Value = MODE
                    Else
                        h_Module.Value = "SCHOOL"
                    End If
                    GET_POINTS_SUMMARY_CALL(STU_ID)
                End If
            Else
                ShowMessage(UtilityObj.getErrorMessage("362"))
            End If
        End If
    End Sub
    Private Sub ClearPage()
        ShowMessage("")
        ShowInfoMessage("Please enter the amount for redemption and Apply.")
        h_Success.Value = ""
        h_Module.Value = "SCHOOL"
        lblEmailId.Text = "-"
        lblAvailablePoints.Text = 0
        lblRedeemableAmt.Text = "0.00"
        lblRedeemablePoints.Text = 0
        txtAmounttoRedeem.Text = "0.00"
        ViewState("AVAILABLE_AMOUNT") = 0
        ViewState("AVAILABLE_POINTS") = 0
        ViewState("REDEEMABLE_POINTS") = 0
    End Sub
    Private Function VALIDATE_PAGE() As Boolean
        VALIDATE_PAGE = False
        ShowMessage("")
        If FeeCollection.GetDoubleVal(ViewState("AVAILABLE_AMOUNT")) < 10 Then
            ShowMessage(UtilityObj.getErrorMessage(351))
            Exit Function
        End If
        If FeeCollection.GetDoubleVal(txtAmounttoRedeem.Text) < 1 Then
            ShowMessage(UtilityObj.getErrorMessage(344).Replace("$$$", Session("BSU_CURRENCY").ToString))
            Exit Function
        End If
        'If (FeeCollection.GetDoubleVal(txtAmounttoRedeem.Text) Mod 10) <> 0 Then
        '    ShowMessage(UtilityObj.getErrorMessage(345))
        '    Exit Function
        'End If
        If FeeCollection.GetDoubleVal(txtAmounttoRedeem.Text) > ViewState("AVAILABLE_AMOUNT") Then
            ShowMessage("Amount exceeded the eligible redemption amount")
            Exit Function
        End If
        VALIDATE_PAGE = True
    End Function
    Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
        If Message <> "" Then
            preError.Attributes.Remove("class")
            If bError Then
                preError.Attributes.Add("class", "alert alert-danger")
            Else
                preError.Attributes.Add("class", "alert alert-success")
            End If
        Else
            preError.Attributes.Remove("class")
            preError.Attributes.Add("class", "invisible")
        End If
        preError.InnerHtml = Message
    End Sub
    Sub ShowInfoMessage(ByVal Message As String)
        If Message <> "" Then
            preError.Attributes.Remove("class")
            preError.Attributes.Add("class", "alert alert-info")
        Else
            preError.Attributes.Remove("class")
        End If
        preError.InnerHtml = Message
    End Sub
    Private Sub GET_POINTS_SUMMARY_CALL(ByVal STU_ID As Long)
        Dim flag As Boolean = False
        If STU_ID > 0 Then
            Dim objcls As New clsRewards
            objcls.FETCH_REWARDS_INFO(STU_ID)

            If objcls.CUSTOMER_ID.Trim <> "" And objcls.CUSTOMER_ID.Trim <> "0" Then
                Dim ACL_ID As Long = 0, GENERATED_TOKEN As String = "", responseData As String = ""
                ViewState("CUSTOMER_ID") = objcls.CUSTOMER_ID
                Try
                    If objcls.GENERATE_ACCESS_TOKEN(objcls, GENERATED_TOKEN) Then
                        objcls.GET_API_CALL_PARAMETERS("GETPOINTSBALANCE") 'function gets the web api call parameters
                        Dim str = clsRewards.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, Session("sBsuid"), DateTime.Now, "GETPOINTSBALANCE", objcls.API_URI, objcls.API_METHOD, objcls.CUSTOMER_ID, Session("username"))
                        Const contentType As String = "application/x-www-form-urlencoded"

                        Dim client = New RestClient(New Uri(objcls.API_URI).AbsoluteUri)
                        Dim APIRequest = New RestRequest(clsRewards.GetAPIMethod(objcls.API_METHOD))
                        APIRequest.AddHeader("Content-Type", contentType)
                        APIRequest.AddHeader("CC_TOKEN", GENERATED_TOKEN)
                        APIRequest.AddHeader("TP_APPLICATION_KEY", objcls.API_TOKEN)
                        APIRequest.AddParameter("customer_id", objcls.CUSTOMER_ID)
                        APIRequest.AddParameter("type", "parent")
                        Dim Response As IRestResponse = client.Execute(APIRequest)
                        responseData = Response.Content
                        Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
                    End If
                    Dim jsonPointSummary = JObject.Parse(responseData)
                    If Not jsonPointSummary("status") Is Nothing AndAlso jsonPointSummary("status").ToObject(Of String)().ToLower = "true" Then 'if web api return with success=true message
                        Dim jsonValues = JObject.Parse(jsonPointSummary("values").ToString())
                        Dim ParentName As String = ""
                        ViewState("AVAILABLE_POINTS") = FeeCollection.ValidateValue(jsonValues.Property("point_balance"))
                        ViewState("REDEEMABLE_POINTS") = FeeCollection.ValidateValue(jsonValues.Property("tentative_points"))
                        ViewState("AVAILABLE_AMOUNT") = FeeCollection.GetDoubleVal(ViewState("AVAILABLE_POINTS")) / 10
                        ParentName = FeeCollection.ValidateValue(jsonValues.Property("first_name")) & " " & FeeCollection.ValidateValue(jsonValues.Property("last_name"))
                        lblEmailId.Text = ParentName & ", " & objcls.PARENT_EMAIL
                        lblAvailablePoints.Text = ViewState("AVAILABLE_POINTS")
                        lblRedeemablePoints.Text = ViewState("REDEEMABLE_POINTS")
                        lblRedeemableAmt.Text = ViewState("AVAILABLE_AMOUNT")
                    ElseIf Not jsonPointSummary("status") Is Nothing AndAlso jsonPointSummary("status").ToObject(Of String)().ToLower = "false" Then
                        ShowMessage(jsonPointSummary("message").ToObject(Of String)())
                    End If
                Catch ex As WebException
                    UtilityObj.Errorlog("popRewardsInfo-GET_POINTS_SUMMARY_CALL, Error:" & ex.Message, "PHOENIX")
                End Try
            Else
                ShowMessage(UtilityObj.getErrorMessage("343"))
                flag = True
                lblEmailId.Text = ""
            End If
        Else
            ShowMessage("Your session expired! Unable to fetch the GEMS Rewards data.")
            flag = True

        End If
        If (flag) Then
            trapply.Visible = False
            chkAgree.Visible = False
        End If
    End Sub
    Protected Sub btnApply_Click(sender As Object, e As EventArgs) Handles btnApply.Click
        If VALIDATE_PAGE() Then
            h_Success.Value = "1"
        End If
    End Sub

    Protected Sub chkAgree_CheckedChanged(sender As Object, e As EventArgs)
        If chkAgree.Checked Then
            trapply.Visible = True
        Else
            trapply.Visible = False
        End If
    End Sub

    
End Class
