﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Fees_feeChangePaymentPlan
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            btnSave.Attributes.Add("onclick", ("javascript:" & btnSave.ClientID & ".disabled=true;") + ClientScript.GetPostBackEventReference(btnSave, "").ToString())
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            InitialiseCompnents()

            If Session("sUsr_name") = "" Or Session("sBSuid") = "" Or ViewState("MainMnu_code") <> "F300245" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBSuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub

    Sub InitialiseCompnents()
        gvHistory.Attributes.Add("bordercolor", "#1b80b6")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        FillACD()
        UsrSelStudent1.IsStudent = True
        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Sub clear_All()
        txtRemarks.Text = ""
        gvHistory.DataBind()

        labGrade.Text = ""
        ClearStudentData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
     
        Dim str_error As String = check_errors_details()
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If UsrSelStudent1.STUDENT_NO = "" Then
            str_error = str_error & "Please select student <br />"
        End If
       
        If str_error <> "" Then
            'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim NEW_STP_ID As String = ""
            retval = SaveSTUDENT_PAYMENTPLAN(UsrSelStudent1.STUDENT_ID, txtFrom.Text, _
            ddlCollectionSchedule.SelectedItem.Value, txtRemarks.Text, _
            Session("sBsuid"), NEW_STP_ID, ddlAcademicYear.SelectedItem.Value, stTrans)
            If retval = "0" Then
                If retval = "0" Then
                    stTrans.Commit()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, NEW_STP_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                    'lblError.Text = getErrorMessage("0")
                    usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                    clear_All()
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub 

    Function check_errors_details() As String
        Dim str_error As String = ""

      
        Return str_error
    End Function
 

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
        ClearStudentData()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
        ClearStudentData()
    End Sub

    Sub ClearStudentData()
        UsrSelStudent1.IsStudent = rbEnrollment.Checked
        UsrSelStudent1.ClearDetails()
        lbLDAdate.Text = ""
        lblStuStatus.Text = ""
        lbStuDOJ.Text = ""
        lbTCdate.Text = ""
        labGrade.Text = ""
    End Sub

    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
        Try
            labGrade.Text = "Grade : " & UsrSelStudent1.GRM_DISPLAY
            lblStuStatus.Visible = False
            lbLDAdate.Text = ""
            lbTCdate.Text = ""
            lbStuDOJ.Text = ""
            If UsrSelStudent1.STUDOJ <> "" Then
                lbStuDOJ.Text = "Join Date : " & UsrSelStudent1.STUDOJ
            End If
            If UsrSelStudent1.LDADATE <> "" Then
                lbLDAdate.Text = "LDA Date : " & UsrSelStudent1.LDADATE
            End If
            If UsrSelStudent1.TCDATE <> "" Then
                lbTCdate.Text = "Cancel Date : " & UsrSelStudent1.TCDATE
            End If

            If UsrSelStudent1.STU_STATUS <> "EN" Then
                lblStuStatus.Visible = True
                lblStuStatus.Text = "Status : " & UsrSelStudent1.STU_STATUS
                UsrSelStudent1.ChangeColor(True)
            Else
                UsrSelStudent1.ChangeColor(False)
            End If
            setStudentHistory()

            If Not ddlAcademicYear.Items.FindByValue(UsrSelStudent1.ACD_ID) Is Nothing Then
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(UsrSelStudent1.ACD_ID).Selected = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
    End Sub

    Sub setStudentHistory()
        If UsrSelStudent1.STUDENT_ID <> "" Then
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "exec FEES.GETPAYMENTPLANHISTORY '" & UsrSelStudent1.STUDENT_ID & "'")
            gvHistory.DataSource = ds.Tables(0)
            gvHistory.DataBind()
        End If
    End Sub

    Public Shared Function SaveSTUDENT_PAYMENTPLAN(ByVal STP_STU_ID As Long, ByVal STP_FROMDT As String, _
        ByVal STP_PAY_SCH_ID As String, ByVal STP_REMARKS As String, ByVal STP_BSU_ID As String, _
        ByRef NEW_STP_ID As String, ByVal STP_ACD_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STP_STU_ID", SqlDbType.BigInt)
        pParms(0).Value = STP_STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STP_FROMDT", SqlDbType.DateTime)
        pParms(1).Value = STP_FROMDT
        pParms(2) = New SqlClient.SqlParameter("@STP_PAY_SCH_ID", SqlDbType.Int)
        pParms(2).Value = STP_PAY_SCH_ID
        pParms(3) = New SqlClient.SqlParameter("@STP_REMARKS", SqlDbType.VarChar)
        pParms(3).Value = STP_REMARKS
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@STP_BSU_ID", SqlDbType.VarChar)
        pParms(5).Value = STP_BSU_ID
        pParms(6) = New SqlClient.SqlParameter("@NEW_STP_ID", SqlDbType.BigInt)
        pParms(6).Direction = ParameterDirection.Output
        pParms(7) = New SqlClient.SqlParameter("@STP_ACD_ID", SqlDbType.VarChar)
        pParms(7).Value = STP_ACD_ID
        If pParms(4).Value = 0 Then
            NEW_STP_ID = pParms(6).Value
        End If
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.SaveSTUDENT_PAYMENTPLAN", pParms)
        Return pParms(4).Value
    End Function

End Class
