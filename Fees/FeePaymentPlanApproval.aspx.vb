﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Web.UI
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI
Imports System.Linq
Partial Class Fees_FeePaymentPlanApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'ViewState("FPP_EMP_ID") = Request.QueryString("UID") 'Encr_decrData.Decrypt(Request.QueryString("UID").Replace(" ", "+"))
            ViewState("FPP_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
                LoadSavedData(ViewState("FPP_ID"))
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub LoadSavedData(ByVal FPPID As String)
        Dim objFPP As New FeePaymentPlanner
        Try
            objFPP.FPP_ID = FPPID
        Catch Ea As Exception
            objFPP.FPP_ID = 0
        End Try
        If objFPP.FPP_ID <> 0 Then


            objFPP.GetSavedDetails()
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "Images/Home/no_image.gif"
            Dim STU_PATH As String = ""
            Dim dt2 As DataTable = GetStudentDetails(objFPP.BSU_ID, objFPP.STU_ID)
            For Each row2 As DataRow In dt2.Rows
                LabelPhotoPath.Value = connPath & row2.Item("STU_PHOTOPATH")
                'STU_PHOTOPATH,STU_ID,STU_NO,GRM_DISPLAY,SCT_DESCR,STU_DOJ,ACY_DESCR , StudName
                LabelACDYear.Text = row2.Item("ACY_DESCR")
                StudentNo.Text = row2.Item("STU_NO")
                Name.Text = row2.Item("StudName")
                Grade.Text = row2.Item("GRM_DISPLAY") & " - " & row2.Item("SCT_DESCR")
                DOJ.Text = row2.Item("STU_DOJ")
                objFPP.FPP_GRD_ID = row2.Item("STU_GRD_ID")

                lbl_SchoolName.Text = row2.Item("SCHOOL_NAME")
                header_image.Src = row2.Item("BSU_LOGO_PATH")
            Next

            Dim LEVEL_STRING As String = ""
            'ViewState("LEVEL") = "3"
            'Dim dt12 As DataTable = GetEmployeeDetails(Convert.ToString(ViewState("FPP_EMP_ID")))
            'For Each row2 As DataRow In dt12.Rows
            'If ViewState("LEVEL") = "1" Then
            '    LEVEL_STRING = "Welcome " & row2.Item("EMP_NAME") & " !!! - " & "Registrar Approval Required"
            'ElseIf ViewState("LEVEL") = "2" Then
            '    LEVEL_STRING = "Welcome " & row2.Item("EMP_NAME") & " !!! - " & "Finance Approval Required"
            'ElseIf ViewState("LEVEL") = "3" Then
            If (objFPP.bPLAN_APPROVED Or objFPP.bPLAN_REJECTED) Then
                'LEVEL_STRING = "Welcome " & row2.Item("EMP_NAME") & " !!! - " & "Principal Approval/Reject Completed"
                If objFPP.PrincipalStatus = "PA" Then
                    status_class.Attributes("class") = "status approved"
                    LEVEL_STRING = "" & "Approved"
                End If
                If objFPP.PrincipalStatus = "PR" Then
                    status_class.Attributes("class") = "status reject"
                    LEVEL_STRING = "" & "Rejected"
                End If

            Else
                'LEVEL_STRING = "Welcome " & row2.Item("EMP_NAME") & " !!! - " & "Principal Approval Required"
                If objFPP.PrincipalStatus = "PB" Then
                    status_class.Attributes("class") = "status reject"
                    LEVEL_STRING = "" & "Reverted"
                Else
                    status_class.Attributes("class") = "status pending"
                    LEVEL_STRING = "" & "Approval Pending"
                End If

            End If

            'End If
            'Next

            lbl_Status.Text = LEVEL_STRING

            showSelectedFees(objFPP.FEE_IDs)

            If objFPP.IS_CASHIER_EDITABLE(FPPID) Then ' FB PB PA           
                'If objFPP.IsPrincipalApprove Then 'PA
                '    gvPayPlanDetail.Columns(7).Visible = False
                '    gvPayPlanDetail.Columns(8).Visible = True
                'Else
                '    gvPayPlanDetail.Columns(7).Visible = False
                '    gvPayPlanDetail.Columns(8).Visible = False
                'End If
            End If

            If objFPP.IsAmountEditable = 0 Then 'STATUS 3
                'gvPayPlanDetail.Columns(7).Visible = False
                'gvPayPlanDetail.Columns(8).Visible = True
            End If

            'objFPP.STU_ID = 20459694
            objFPP.GetContactDetails()
            ParentDetails.Text = "" + objFPP.ContactName.ToString + "" + "</br>" + objFPP.Email.ToString + " " + "</br>" + objFPP.MobileNo.ToString + " " + "</br>" + objFPP.ResidenceNo.ToString

            Me.lblDate.Text = Format(Convert.ToDateTime(objFPP.DOCDATE), OASISConstants.DataBaseDateFormat)
            ViewState("gvStudentDetail") = XMLtoDataTable(objFPP.Aging_XML)
            Dim dt As New DataTable
            objFPP.ASONDATE = objFPP.DOCDATE
            'ASONDATE = DOCDATE
            'STU_ID = STU_ID
            objFPP.Bkt1 = "30"
            objFPP.Bkt2 = "60"
            objFPP.Bkt3 = "90"
            objFPP.Bkt4 = "180"
            'FEE_IDs = FEE_IDs
            dt = objFPP.GetStudentFeeAging
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Dim dblOutStanding As Double
                dblOutStanding = dt.Rows(0)("TOTALDUE")
                LabelOutstanding.Text = dblOutStanding.ToString("#,##0.00")
            Else
                LabelOutstanding.Text = "0.00"
            End If


            'Fee for upcoming Terms/Months
            Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACD_ID", objFPP.ACD_ID)
            param(1) = New SqlClient.SqlParameter("@BSU_IDS", objFPP.BSU_ID)
            param(2) = New SqlClient.SqlParameter("@GRD_IDS", objFPP.FPP_GRD_ID)
            param(3) = New SqlClient.SqlParameter("@FEE_IDS", objFPP.FEE_IDs)
            param(4) = New SqlClient.SqlParameter("@STU_ID", objFPP.STU_ID.ToString)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[dbo].[FEE_FUTURE_PAYMENTS]", param)
            If Not ds.Tables(0) Is Nothing Then
                ViewState("gvFeeDetail") = ds.Tables(0)
                Dim dblUpcomingTotal As Double
                dblUpcomingTotal = ds.Tables(0).Rows(0)("TOTAL")
                LabelUPCOMINGTOTAL.Text = dblUpcomingTotal.ToString("#,##0.00")
            End If

            Bindgrid()
            ViewState("gvPayPlanDetail") = objFPP.gvPayPlanDetail
            BindGridPP()
            If (objFPP.bPLAN_APPROVED Or objFPP.bPLAN_REJECTED) Then 'And ViewState("MainMnu_code") = "F300271"
                btnApprove.Visible = False
                btnReject.Visible = False
                btnRevert.Visible = False
                'btnOnhold.Visible = False
                id_princ_cmnts.Visible = False
                id_finalComments.Visible = True
                lblFinalComments.Text = "<b>Cashier Comments:</b> " & objFPP.Comments & "</br> <b>Finance Comments:</b>  " & objFPP.FinanceComments & "</br> </br>  <b>Principal Comments:</b> " & objFPP.PrincipalComments & ""
                id_fin_cmnts.Visible = False
                id_princ_cmnts.Visible = False
            ElseIf objFPP.PrincipalStatus = "PB" Then
                btnApprove.Visible = False
                btnReject.Visible = False
                btnRevert.Visible = False
                'btnOnhold.Visible = False
                id_princ_cmnts.Visible = False
                id_finalComments.Visible = True
                lblFinalComments.Text = "<b>Cashier Comments:</b> " & objFPP.Comments & "</br> <b>Finance Comments:</b>  " & objFPP.FinanceComments & "</br> </br>  <b>Principal Comments:</b> " & objFPP.PrincipalComments & ""
                id_fin_cmnts.Visible = False
                id_princ_cmnts.Visible = False

            ElseIf objFPP.bPLAN_APPROVED = False AndAlso objFPP.bPLAN_REJECTED = False AndAlso objFPP.Status = "2" AndAlso objFPP.PrincipalStatus = "" Then 'And ViewState("MainMnu_code") = "F300271"
                btnApprove.Visible = True
                btnReject.Visible = True
                btnRevert.Visible = True
                btnRevert.Visible = False 'added line to comment revert button
                'btnOnhold.Visible = True
                id_finalComments.Visible = True
                lblFinalComments.Text = "<b>Cashier Comments:</b> " & objFPP.Comments & "</br> <b>Finance Comments:</b> " & objFPP.FinanceComments & ""
                id_fin_cmnts.Visible = False
                id_princ_cmnts.Visible = True
            Else
                btnApprove.Visible = False
                btnReject.Visible = False
                btnRevert.Visible = False
            End If
        Else
            btnApprove.Visible = False
            btnReject.Visible = False
            btnRevert.Visible = False
        End If
    End Sub
    Public Shared Function GetEmployeeDetails(ByVal EMP_ID As String) As DataTable

        Dim str_Sql As String = "SELECT CONCAT(EMP_SALUTE,EMP_FNAME,' ',EMP_MNAME,' ',EMP_LNAME) AS EMP_NAME FROM [OASIS].[DBO].EMPLOYEE_M WHERE  EMP_ID ='" & EMP_ID & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function
    Public Shared Function GetStudentDetails(ByVal sBsuid As String, ByVal stu_id As Integer) As DataTable

        Dim str_Sql As String = "SELECT STU_PHOTOPATH,STU_ID,STU_NO,StudName,STU_GRD_ID,GRM_DISPLAY,SCT_DESCR,STU_DOJ,ACY_DESCR,SCHOOL_NAME,BSU_LOGO_PATH from OASIS.[dbo].[vw_PARENT_LIST] WHERE STU_ID='" & stu_id & "' AND STU_BSU_ID='" & sBsuid & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function
    Sub Bindgrid()
        'Me.gvStudentDetail.DataSource = ViewState("gvStudentDetail")
        'Me.gvStudentDetail.DataBind()

        Me.gvFeeDetail.DataSource = ViewState("gvFeeDetail")
        Me.gvFeeDetail.DataBind()

    End Sub
    Private Sub BindGridPP()
        Try
            Me.gvPayPlanDetail.DataSource = ViewState("gvPayPlanDetail")
            Me.gvPayPlanDetail.DataBind()
        Catch Ex As Exception
        End Try
    End Sub
    Sub BindFeeType(ByVal flag As Boolean)
        Dim objFPP As New FeePaymentPlanner
        objFPP.BSU_ID = objFPP.BSU_ID 'ViewState("FPP_BSU_ID")
        objFPP.ACD_ID = Convert.ToInt32(objFPP.ACD_ID)
        Dim dtFeeType As DataTable = objFPP.getFeeTypes()

        cblFeeType.DataSource = dtFeeType
        cblFeeType.DataTextField = "FEE_DESCR"
        cblFeeType.DataValueField = "FEE_ID"
        cblFeeType.DataBind()
        For Each li As ListItem In cblFeeType.Items
            li.Selected = flag
        Next
    End Sub

    Sub showSelectedFees(ByVal feeIDs As String)
        BindFeeType(False)
        Dim FEE_ID As String() = feeIDs.Split("|")
        For i As Int16 = 0 To FEE_ID.Length - 1 Step 1
            For Each li As ListItem In cblFeeType.Items
                If li.Value = FEE_ID(i) Then
                    li.Selected = True
                    'Else
                    '    li.Selected = False
                End If
            Next
        Next
    End Sub
    Public Function XMLtoDataTable(ByVal XMLData As String) As DataTable
        Dim theReader As New StringReader(XMLData)
        Dim theDataSet As New DataSet()
        theDataSet.ReadXml(theReader)
        Dim dtCloned = theDataSet.Tables(0).Clone
        dtCloned.Columns("TOTALDUE").DataType = Type.GetType("System.Double")
        For Each row As DataRow In theDataSet.Tables(0).Rows
            dtCloned.ImportRow(row)
        Next
        Return dtCloned
    End Function
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Dim objFPP As New FeePaymentPlanner
        objFPP.FPP_ID = ViewState("FPP_ID")
        If objFPP.FPP_ID <> 0 Then
            objFPP.FPP_ID = ViewState("FPP_ID")
            objFPP.User = Session("sUsr_name")
            objFPP.BSU_ID = objFPP.BSU_ID 'ViewState("FPP_BSU_ID")
            If objFPP.User = Nothing OrElse objFPP.User.ToString = "" Then
                objFPP.User = "PRINCIPAL"
            End If
            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                'If ViewState("MainMnu_code") = "F300271" Then
                objFPP.Comments = txt_princ_comments.InnerText
                'RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_LEVEL2(sqlConn, sqlTrans)
                RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(1, sqlConn, sqlTrans)
                'End If


                If RetFlag Then
                    sqlTrans.Commit()
                    'clearAll()
                    ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                    'usrMessageBar.ShowMessage("Fee Payment plan has been approved successfully", False)
                    ShowMessage("&#10004; Fee Payment plan has been approved successfully", 0)
                    divNote.Visible = True
                    'divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                    divNote.Attributes("class") = "status approved"
                    lblError.Text = "<div>&#10004; Fee Payment plan has been approved successfully </div>"
                    'lblError.Attributes("class") = "status approved"
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    btnRevert.Visible = False

                    Dim LEVEL_STRING As String = ""
                    objFPP.PrincipalStatus = "PA"
                    If objFPP.PrincipalStatus = "PA" Then
                        status_class.Attributes("class") = "status approved"
                        LEVEL_STRING = "" & "Approved"
                        lbl_Status.Text = LEVEL_STRING
                    End If
                    If objFPP.PrincipalStatus = "PR" Then
                        status_class.Attributes("class") = "status reject"
                        LEVEL_STRING = "" & "Rejected"
                        lbl_Status.Text = LEVEL_STRING
                    End If

                Else
                    sqlTrans.Rollback()
                    'Me.lblError.Text = "unable to approve fee payment plan"
                    'usrMessageBar.ShowMessage("unable to approve fee payment plan", True)
                    ShowMessage("&#10060; unable to approve fee payment plan", 0)
                    divNote.Visible = True
                    'divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    divNote.Attributes("class") = "status reject"
                    lblError.Text = "<div>&#10060; unable to approve fee payment plan </div>"
                    'lblError.Attributes("class") = "status reject"
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    btnRevert.Visible = False
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                'usrMessageBar.ShowMessage("unable to approve fee payment plan", True)
                ShowMessage("&#10060; unable to approve fee payment plan", 0)
                divNote.Visible = True
                'divNote.Attributes("class") = "msgInfoBox msgInfoError"
                divNote.Attributes("class") = "status reject"
                lblError.Text = "<div>&#10060; unable to approve fee payment plan </div>"
                'lblError.Attributes("class") = "status reject"
                btnApprove.Visible = False
                btnReject.Visible = False
                btnRevert.Visible = False
            End Try
        End If
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Dim objFPP As New FeePaymentPlanner
        objFPP.FPP_ID = ViewState("FPP_ID")
        If objFPP.FPP_ID <> 0 Then
            objFPP.FPP_ID = ViewState("FPP_ID")
            objFPP.User = Session("sUsr_name")
            objFPP.BSU_ID = objFPP.BSU_ID 'ViewState("FPP_BSU_ID")

            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                'If ViewState("MainMnu_code") = "F300171" Then
                '    objFPP.Comments = txt_fin_comments.Text
                '    RetFlag = objFPP.REJECT_FEE_PAYMENT_PLAN(sqlConn, sqlTrans)
                'ElseIf ViewState("MainMnu_code") = "F300271" Then
                objFPP.Comments = txt_princ_comments.InnerText
                'RetFlag = objFPP.REJECT_FEE_PAYMENT_PLAN_LEVEL2(sqlConn, sqlTrans)
                RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(2, sqlConn, sqlTrans)
                'End If

                If RetFlag Then
                    sqlTrans.Commit()
                    'clearAll()
                    'Me.lblError.Text = "Fee Payment plan has been rejected"
                    'usrMessageBar.ShowMessage("Fee Payment plan has been rejected", False)
                    ShowMessage("&#10060; Fee Payment plan has been rejected", 1)
                    divNote.Visible = True
                    'divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                    divNote.Attributes("class") = "status reject"
                    lblError.Text = "<div>&#10060; Fee Payment plan has been rejected </div>"
                    'lblError.Attributes("class") = "status reject"
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    btnRevert.Visible = False
                    Dim LEVEL_STRING As String = ""
                    objFPP.PrincipalStatus = "PR"
                    If objFPP.PrincipalStatus = "PA" Then
                        status_class.Attributes("class") = "status approved"
                        LEVEL_STRING = "" & "Approved"
                        lbl_Status.Text = LEVEL_STRING
                    End If
                    If objFPP.PrincipalStatus = "PR" Then
                        status_class.Attributes("class") = "status reject"
                        LEVEL_STRING = "" & "Rejected"
                        lbl_Status.Text = LEVEL_STRING
                    End If

                Else
                    sqlTrans.Rollback()
                    ' Me.lblError.Text = "unable to reject fee payment plan"
                    'usrMessageBar.ShowMessage("unable to reject fee payment plan", True)
                    ShowMessage("&#10060; unable to reject fee payment plan", 1)
                    divNote.Visible = True
                    'divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                    divNote.Attributes("class") = "status reject"
                    lblError.Text = "<div>&#10060; unable to reject fee payment plan </div>"
                    'lblError.Attributes("class") = "status reject"
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    btnRevert.Visible = False


                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to reject fee payment plan"
                'usrMessageBar.ShowMessage("unable to reject fee payment plan", True)
                ShowMessage("&#10060; unable to reject fee payment plan", 1)
                divNote.Visible = True
                'divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                divNote.Attributes("class") = "status reject"
                lblError.Text = "<div>&#10060; unable to reject fee payment plan </div>"
                lblError.Attributes("class") = "status reject"
                btnApprove.Visible = False
                btnReject.Visible = False
                btnRevert.Visible = False
            End Try
        End If
    End Sub
    Protected Sub btnRevert_Click(sender As Object, e As EventArgs) Handles btnRevert.Click
        Dim objFPP As New FeePaymentPlanner
        objFPP.FPP_ID = ViewState("FPP_ID")
        If objFPP.FPP_ID <> 0 Then
            objFPP.FPP_ID = ViewState("FPP_ID")
            objFPP.User = Session("sUsr_name")

            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                'If ViewState("MainMnu_code") = "F300171" Then
                '    objFPP.Comments = txt_fin_comments.Text
                '    RetFlag = objFPP.REVERT_FEE_PAYMENT_PLAN(sqlConn, sqlTrans)
                'ElseIf ViewState("MainMnu_code") = "F300271" Then
                objFPP.Comments = txt_princ_comments.InnerText
                'RetFlag = objFPP.REVERT_FEE_PAYMENT_PLAN_LEVEL2(sqlConn, sqlTrans)
                RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(3, sqlConn, sqlTrans)
                'End If


                If RetFlag Then
                    sqlTrans.Commit()
                    'clearAll()
                    ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                    'usrMessageBar.ShowMessage("Fee Payment plan has been revert successfully", False)
                    ShowMessage("&#10004; Fee Payment plan has been reverted successfully", 0)
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                    lblError.Text = "<div>&#10004; Fee Payment plan has been reverted successfully </div>"
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    btnRevert.Visible = False
                    
                Else
                    sqlTrans.Rollback()
                    'Me.lblError.Text = "unable to approve fee payment plan"
                    'usrMessageBar.ShowMessage("unable to revert fee payment plan", True)
                    ShowMessage("&#10060; unable to revert fee payment plan", 1)
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                    lblError.Text = "<div>&#10060; unable to revert fee payment plan </div>"
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    btnRevert.Visible = False
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                'usrMessageBar.ShowMessage("unable to revert fee payment plan", True)
                ShowMessage("&#10060; unable to revert fee payment plan", 1)
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                lblError.Text = "<div>&#10060; unable to revert fee payment plan </div>"
                btnApprove.Visible = False
                btnReject.Visible = False
                btnRevert.Visible = False
            End Try
        End If
    End Sub
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                'lblMessage.CssClass = "diverrorPopUp"
                lblMessage.Attributes("class") = "status reject"
            Else
                'lblMessage.CssClass = "divvalidPopUp"
                lblMessage.Attributes("class") = "status approved"
            End If
        Else
            lblMessage.CssClass = ""
        End If
        lblMessage.Text = Message
    End Sub
    Protected Sub btn_Click(sender As Object, e As EventArgs) Handles btn.Click
        divNote.Visible = False
        lblError.Text = ""
    End Sub
    Public Shared Function GetDateFormat(ByVal date_f As DateTime) As String
        Dim format As String = "dd/MMM/yyyy"
        Return date_f.ToString(format)
    End Function
    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

    End Sub
End Class
