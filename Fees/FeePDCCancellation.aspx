﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeePDCCancellation.aspx.vb" Inherits="Fees_FeePDCCancellation" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

 
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <link href="../cssfiles/title.css" rel="stylesheet" />
    <link href="../cssfiles/popup.css" rel="stylesheet" />--%>
    <style type="text/css">
        .selectedRow {
            border-bottom: #bbd9ee 1px solid;
            text-align: left;
            padding-bottom: 6px;
            padding-left: 4px;
            padding-right: 4px;
            font-size: 0.99em;
            border-top: #bbd9ee 1px solid;
            padding-top: 6px;
            background-color: #dce88e;
        }
    </style>
    <script type="text/javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Fee Collection PDC Cancellation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <div style="width: 100%;">
                                <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                            </div>

                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlPage" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                            <td align="left" class="matters" width="30%">
                                <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" DataSourceID="odsGetBSUAcademicYear" DataTextField="ACY_DESCR" DataValueField="ACD_ID">
                                </asp:DropDownList>

                            </td>
                            <td align="left" class="matters" width="20%"><span class="field-label">Date</span></td>
                            <td align="left" class="matters" width="30%">
                                <asp:TextBox ID="txtDocDate" runat="server"  ></asp:TextBox>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton2" TargetControlID="txtDocDate">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator
                                    ID="rfvtxtOPendte" ControlToValidate="txtDocDate" Display="Dynamic"
                                    Text="The date is required!" runat="server" ValidationGroup="DOCDATE" CssClass="validate_error" />
                                <asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtDocDate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="DOCDATE">*</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters"><span class="field-label">Student Type</span></td>
                            <td align="left" class="matters" colspan="3">
                                <asp:RadioButton ID="rbtnEnrollment" runat="server" Checked="True" GroupName="STYPE" Text="Enrollment" CssClass="field-label" AutoPostBack="true" />
                                <asp:RadioButton ID="rbtnEnquiry" runat="server" GroupName="STYPE" Text="Enquiry" AutoPostBack="true" CssClass="field-label" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters"><span class="field-label">Student</span></td>
                            <td align="left" class="matters" colspan="3">
                                <uc1:usrSelStudent ID="usrSelStudent1" runat="server"  />

                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters"><span class="field-label">Select Receipt</span>
                            </td>
                            <td align="left" class="matters" colspan="3">
                                <asp:GridView ID="gvReceipts" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered" Style="width: 100%;" AllowPaging="True" PageSize="5">
                                    <RowStyle CssClass="griditem" Height="25px" />
                                    <SelectedRowStyle CssClass="selectedRow" />
                                    <HeaderStyle CssClass="gridheader_pop" Height="30px" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFCLID" runat="server" Text='<%# Bind("FCL_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Receipt Date" DataField="FCL_DATE" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Receipt No" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnRecNo" Text='<%# Bind("FCL_RECNO") %>' runat="server"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="Amount" DataField="FCL_AMOUNT" DataFormatString="{0:N}" ItemStyle-HorizontalAlign="Right" />--%>
                                        <asp:CommandField ShowSelectButton="True" ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters"><span class="field-label">PDC</span>
                            </td>
                            <td align="left" class="matters" colspan="3">
                                <asp:GridView ID="gvPDC" runat="server" AutoGenerateColumns="False" ShowFooter="True" DataKeyNames="bDISABLE" Style="width: 100%;" CssClass="table table-row table-bordered">
                                    <RowStyle CssClass="griditem_dark" Height="25px" />
                                    <SelectedRowStyle CssClass="selectedRow" />
                                    <HeaderStyle CssClass="gridheader_pop" Height="30px" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <Columns>
                                        <asp:TemplateField Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFCQID" runat="server" Text='<%# Eval("FCD_UNIQCHQ_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="javascript:expandcollapse('div<%# Eval("FCD_UNIQCHQ_ID")%>', 'one');">
                                                    <img id="imgdiv<%# Eval("FCD_UNIQCHQ_ID")%>" alt="Click to show/hide details of cheque no <%# Eval("FCQ_CHQNO")%>" width="14px" height="20px" border="0" src="../Images/expand.jpg" />
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelectPDC" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Chq.No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("FCQ_CHQNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField HeaderText="Chq.Date" DataField="FCQ_DATE" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Amount" DataField="FCQ_AMOUNT" DataFormatString="{0:N}" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Bank" DataField="BNK_DESCRIPTION" />
                                        <asp:BoundField HeaderText="Emirate" DataField="EMR_DESCR" />
                                        <asp:BoundField HeaderText="Receipt Date" DataField="FCL_DATE" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <EditItemTemplate>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td colspan="100%"  >
                                                        <div id="div<%# Eval("FCD_UNIQCHQ_ID") %>" style="display: none; position: relative; left: 5px; overflow: auto; width: 100%">
                                                            <asp:GridView ID="gvPDCSplit" runat="server" Style="width: 100%;" AutoGenerateColumns="false" RowStyle-Font-Size="Small" CssClass="table table-row table-bordered">
                                                                <Columns>
                                                                    <asp:BoundField HeaderText="Student Id" DataField="STU_NO" />
                                                                    <asp:BoundField HeaderText="Name" DataField="STU_NAME" />
                                                                    <asp:BoundField HeaderText="Receipt.No" DataField="FCL_RECNO" />
                                                                    <asp:BoundField HeaderText="Fee Type" DataField="FEE_DESCR" />
                                                                    <asp:BoundField HeaderText="Amount" DataField="FCA_AMOUNT" DataFormatString="{0:N}" ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters"><span class="field-label">Comments</span>
                            </td>
                            <td align="left" class="matters" colspan="3">
                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" SkinID="MultiText" MaxLength="500"  ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtTitle" ControlToValidate="txtComments"
                                    Display="Dynamic" Text="* Narration is required!" runat="server" ValidationGroup="DOCDATE" CssClass="validate_error" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table   width="100%">
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="DOCDATE" />
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" Visible="false" CssClass="button" />
                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button" />
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                        </td>
                    </tr>
                </table>
                <div id="alertpopup" class="darkPanelM" runat="server" style="display: none; z-index: 3000 !important;">
                    <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 25%; margin-left: 10%; margin-top: 15%;">
                        <div class="holderInner" style="height: 90%; width: 98%;">
                            <center>
                    <table cellpadding="5" cellspacing="2" border="3" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                        class="tableNoborder">
                        <tr class="subheader_img">
                            <td style="border: 0 !important;">
                                <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
                                INFO
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="border: 0 !important;">
                                <span class="ui-icon ui-icon-info" style="float: left; margin: 0 7px 7px 0;"></span>
                                <asp:Label ID="lblMsg" CssClass="matters" runat="server"></asp:Label><br />
                                <br />
                                <br />

                                <asp:Label ID="lblMsg2" runat="server" BackColor="Beige" Font-Names="Verdana" ForeColor="Navy"
                                   ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border: 0;">
                                <asp:Button ID="btnOkay" runat="server" CausesValidation="False" CssClass="button"
                                    Text="OK" />
                                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="button"
                                    Text="CANCEL" />
                            </td>
                        </tr>
                    </table>
                </center>
                        </div>
                    </div>
                </div>

                <asp:ObjectDataSource ID="odsGetBSUAcademicYear" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetBSUAcademicYear" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <script type="text/javascript">
                    function expandcollapse(obj, row) {
                        var div = document.getElementById(obj);
                        var img = document.getElementById('img' + obj);

                        if (div.style.display == "none") {
                            div.style.display = "block";
                            if (row == 'alt') {
                                img.src = "../Images/collapse.jpg";
                            }
                            else {
                                img.src = "../Images/collapse.jpg";
                            }
                            img.alt = "Close to view cheque(s)";
                        }
                        else {
                            div.style.display = "none";
                            if (row == 'alt') {
                                img.src = "../Images/expand.jpg";
                            }
                            else {
                                img.src = "../Images/expand.jpg";
                            }
                            img.alt = "Expand to show receipts";
                        }
                    }
                    function unselect(objcheckbox,bDisable)
                    {
                        //alert(bDisable);
                        if (bDisable == 'true')
                        {
                            $("#" + objcheckbox).prop('checked', false);
                            alert('Cannot cancel the PDC, date already past the current date.');
                        }
                    }
                </script>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

