﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Transport_TransportFeeRefund
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtChqNo.Attributes.Add("readonly", "readonly")
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            InitialiseCompnents()
            ddlBusinessunit.DataBind()
            FillACD()
            'Session("FeeCollection") = FeeCollection.CreateFeeCollection
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_TRANSPORT_REFUND_FEE_APPROVAL _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_TRANSPORT_REFUND_REQUEST And ViewState("MainMnu_code") <> "F300161" And ViewState("MainMnu_code") <> "F351067") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_TRANSPORT_REFUND_REQUEST, "F300161"
                        lblHead.Text = "Transport Refund Request"
                        tbl_Approval.Visible = False
                        If Request.QueryString("view_id") <> "" Then
                            ViewState("datamode") = "view"
                            'SET_VIEWDATA(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            LoadSavedData(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            'tbl_Allocation.Visible = False
                            'tbl_Approval.Visible = True
                        End If
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Case OASISConstants.MNU_TRANSPORT_REFUND_FEE_APPROVAL, "F351067"
                        lblHead.Text = "Transport Refund Request Approval"
                        btnApprove.Visible = True
                        btnReject.Visible = True
                        rbEnrollment.Enabled = False
                        rbEnquiry.Enabled = False
                        txtBankCharge.Attributes.Add("readonly", "readonly")
                        txtFrom.Attributes.Add("readonly", "readonly")
                        'imgFrom.Enabled = False
                        tbl_Allocation.Visible = False
                        tbl_Approval.Visible = True
                        If Request.QueryString("view_id") <> "" Then
                            'SET_VIEWDATA(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            LoadSavedData(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            SetTCReference()
                            tbl_Allocation.Visible = False
                            tbl_Approval.Visible = True
                        End If
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        btnSave.Visible = False
                End Select
            End If
        End If
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clear_All()
        Set_Previous_Acd()
        'PopulateMonths()
        'Gridbind_Feedetails()
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        Set_Previous_Acd()

    End Sub
    Sub Set_Previous_Acd()
        h_PREV_ACD.Value = TransportRefund.GetPreviousACD(ddlAcademicYear.SelectedItem.Value, ddlBusinessunit.SelectedItem.Value, _
        ConnectionManger.GetOASISTRANSPORTConnectionString)
    End Sub
    Sub set_bankaccount()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_PAYMENTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_PAYMENTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", ConnectionManger.GetOASISFINConnectionString)
        If str_bankact_name <> "--" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            txtBankDescr.Text = str_bankact_name.Split("|")(1)
        End If
    End Sub
    Sub LoadSavedData(ByVal FRH_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim STR_SQL As String = "SELECT FRH.FRH_BSU_ID, FRH.FRH_ACD_ID, FRH.FRH_STU_TYPE, FRH.FRH_STU_ID, " _
        & " FRH.FRH_DATE, FRH.FRH_NARRATION, FRH.FRH_ACT_ID, FRH.FRH_PAIDTO, FRH.FRH_STU_BSU_ID, " _
        & " FRH.FRH_TOTAL, FRH.FRH_FAR_ID, ACT.ACT_NAME, FRH.FRH_CHQDT, " _
        & " FRH.FRH_SCT_ID, FRH.FRH_BANK_CASH, FAR.FAR_REMARKS, FRH.FRH_VHH_DOCNO, " _
        & " isnull(FRH.FRH_bPosted,0) FRH_bPosted,ISNULL(FRH_BANKCHARGE,0) FRH_BANKCHARGE FROM FEES.FEE_REFUND_H AS FRH " _
        & " INNER JOIN OASIS_FEES.dbo.vw_OSF_ACCOUNTS_M AS ACT ON FRH.FRH_ACT_ID = ACT.ACT_ID " _
        & " LEFT OUTER JOIN FEES.FEEADJREQUEST_H AS FAR ON FRH.FRH_FAR_ID = FAR.FAR_ID WHERE FRH.FRH_ID='" & FRH_ID & "'"
        Dim dsFRH As DataSet
        dsFRH = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STR_SQL)
        rbBank.Enabled = False
        rbCash.Enabled = False
        ViewState("FRH_VHH_DOCNO") = dsFRH.Tables(0).Rows(0)("FRH_VHH_DOCNO")
        ViewState("FRH_BANK_CASH") = dsFRH.Tables(0).Rows(0)("FRH_BANK_CASH")
        txtBankCharge.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_BANKCHARGE"), "0.00")
        ddlBusinessunit.SelectedValue = dsFRH.Tables(0).Rows(0)("FRH_STU_BSU_ID").ToString
        If Not ViewState("FRH_VHH_DOCNO") Is Nothing And dsFRH.Tables(0).Rows(0)("FRH_bPosted") = True Then
            btnPrint1.Visible = True
            chkPrintChq.Visible = True
            gvSelectedStudents.Columns(4).Visible = False
            gvSelectedStudents.Columns(5).Visible = False
        End If
        If dsFRH.Tables(0).Rows(0)("FRH_BANK_CASH") = "C" Then
            chkPrintChq.Visible = False
            rbBank.Checked = False
            rbCash.Checked = True
            txtCashAcc.Text = dsFRH.Tables(0).Rows(0)("FRH_ACT_ID")
            txtCashDescr.Text = dsFRH.Tables(0).Rows(0)("ACT_NAME")
        Else
            rbCash.Checked = False
            rbBank.Checked = True
            txtBankCode.Text = dsFRH.Tables(0).Rows(0)("FRH_ACT_ID")
            txtBankDescr.Text = dsFRH.Tables(0).Rows(0)("ACT_NAME")
        End If
        Set_CashorBank()
        set_chequeno_controls()
        txPaidto.Text = dsFRH.Tables(0).Rows(0)("FRH_PAIDTO")
        txtAmount.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_TOTAL"), "0.00")
        txtFrom.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_DATE"), "dd/MMM/yyyy")
        txtChqdt.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_CHQDT"), "dd/MMM/yyyy")
        txtRemarks.Text = dsFRH.Tables(0).Rows(0)("FRH_NARRATION")
        Dim strSTU_TYP As String = dsFRH.Tables(0).Rows(0)("FRH_STU_TYPE")
        If strSTU_TYP = "E" Then
            rbEnquiry.Checked = True
        Else
            rbEnrollment.Checked = True
        End If
        txtAdjustment.Text = dsFRH.Tables(0).Rows(0)("FAR_REMARKS").ToString
        h_Adjustment.Value = dsFRH.Tables(0).Rows(0)("FRH_FAR_ID").ToString

        Dim SQL_FRD As String = "SELECT  FRD_ID ,FRD_FRH_ID ,FRD_FEE_ID ,FRD_AMOUNT ,FRD_STU_ID , " & _
        "FRD_NARRATION FROM FEES.FEE_REFUND_D WHERE FRD_FRH_ID=" & FRH_ID & ""

        Dim dtFRD As DataTable = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, SQL_FRD).Tables(0)
        If dtFRD.Rows.Count > 0 Then
            For Each dr As DataRow In dtFRD.Rows
                txtStudent.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT STU_NO FROM dbo.STUDENT_M WHERE STU_ID='" & dr("FRD_STU_ID") & "'")
                AddStudent()
            Next
            Dim dtREfundSummary As DataTable = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "EXEC FEES.GET_REFUND_SUMMARY @FRH_ID=" & FRH_ID & "").Tables(0)
            ViewState("gvRefundSummary") = dtREfundSummary
            BindRefundSummary(dtREfundSummary)
        End If
    End Sub

    Sub InitialiseCompnents()
        h_bIsonDAX.Value = Session("BSU_IsOnDAX")
        H_STU_TYPE.Value = "S"
        txtBankCharge.Text = "0"
        gvRefund.Attributes.Add("bordercolor", "#1b80b6")
        gvNetRefund.Attributes.Add("bordercolor", "#1b80b6")
        Set_CashorBank()
        set_chequeno_controls()
        txtAmount.Attributes.Add("readonly", "readonly")
        txtCashAcc.Attributes.Add("readonly", "readonly")
        txtCashDescr.Attributes.Add("readonly", "readonly")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqdt.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtBankCode.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtChqNo.Attributes.Add("readonly", "readonly")
        'txtAdjustment.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtProvCode.Attributes.Add("readonly", "readonly")
        txtProvDescr.Attributes.Add("readonly", "readonly")
        'txPaidto.Attributes.Add("readonly", "readonly")
        set_bankaccount()
        DisableChequeDetails()
    End Sub
    Sub DisableChequeDetails()
        If Session("BSU_IsOnDAX") = "1" Then
            ChkBearer.Enabled = False
            txtChqBook.Enabled = False
            txtChqNo.Enabled = False
        Else
            ChkBearer.Enabled = True
            txtChqBook.Enabled = True
            txtChqNo.Enabled = True
        End If
    End Sub
    Sub clear_All()
        txtBankCharge.Text = "0"
        h_Adjustment.Value = ""
        h_STUD_ID.Value = ""
        h_Student_no.Value = ""
        H_STU_TYPE.Value = ""
        txtCashAcc.Text = ""
        txtCashDescr.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqdt.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtChqBook.Text = ""
        txtChqNo.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtProvCode.Text = ""
        txtProvDescr.Text = ""
        ClearStudentData()
        set_bankaccount()
        txtRemarks.Text = ""
        txtAdjustment.Text = ""
        lblAdjType.Text = ""
        txPaidto.Text = ""
        chkFee.Checked = False
        chkLab.Checked = False
        chkLibrary.Checked = False
        chkRegistrar.Checked = False
        setStudntData()
        gvRefund.DataBind()
        gvNetRefund.DataBind()
        Set_GridTotal()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            '  lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_error As String = ""
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If h_Student_no.Value = "" Then
            str_error = str_error & "Please select student <br />"
        End If
        If h_Adjustment.Value = "" Then
            h_Adjustment.Value = 0
        End If
        If txtBankCode.Text = "" Then
            str_error = str_error & "Please select bank <br />"
        End If

        Dim lblAmt As New Label
        Dim dRefundAmount As Decimal = 0
        If gvRefundSummary.Rows.Count > 0 And gvRefundSummary.Rows.Count > 0 Then
            'dRefundAmount = Convert.ToDecimal(gvRefund.Rows(gvRefund.Rows.Count - 1).Cells(1).Text)
            lblAmt = CType(gvRefundSummary.FooterRow.FindControl("lblAmtTotal"), Label)
            dRefundAmount = Convert.ToDecimal(lblAmt.Text)
            If dRefundAmount <= 0 Then
                str_error = str_error & "Invalid selection <br />"
            End If
        Else
            str_error = str_error & "Invalid selection <br />"
        End If

        If Not IsDate(txtChqdt.Text) Then
            str_error = str_error & "Invalid cheque date<br />"
        End If

        If Not IsNumeric(txtBankCharge.Text) Then
            txtBankCharge.Text = "0"
        End If

        If txtRemarks.Text = "" Then
            str_error = str_error & "Please enter remarks<br />"
        End If
        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        If rbBank.Checked Then
            STR_BankOrCash = "B"
            If txtBankCode.Text = "" Then
                str_error = str_error & "Invalid bank<br />"
            Else
                STR_ACCOUNT = txtBankCode.Text
            End If
        Else
            STR_BankOrCash = "C"
            If txtCashAcc.Text = "" Then
                str_error = str_error & "Invalid cash account<br />"
            Else
                STR_ACCOUNT = txtCashAcc.Text
            End If
            'If AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtFrom.Text.Trim) < dRefundAmount Then
            '    str_error = str_error & "There is not Enough Balance in the Account"
            'End If
        End If

        If str_error <> "" Then
            '    lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)


        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()

        Dim str_NEW_FRH_ID As String = ""
        Dim dDiffAmount As Decimal = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000" 'current acd of student selected in sp so here scd is pasased as 0
            Dim FRH_STU_ID As String = Me.gvRefundSummary.DataKeys(gvRefundSummary.Rows.Count - 1)("STU_ID")
            retval = TransportRefund.F_SaveTRANSPORT_REFUND_H(0, Session("sBsuid"), ddlBusinessunit.SelectedValue, 0, H_STU_TYPE.Value, _
                FRH_STU_ID, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, str_NEW_FRH_ID, False, _
             txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, "", dRefundAmount, txtBankCharge.Text, stTrans)
            If retval = "0" Then
                For Each gvr As GridViewRow In gvRefundSummary.Rows
                    Dim STU_ID As String = Me.gvRefundSummary.DataKeys(gvr.RowIndex)("STU_ID")
                    Dim FEEIDs As String = DirectCast(gvr.FindControl("lblFeeIDs"), Label).Text
                    Dim dAmount As Decimal = Convert.ToDouble(DirectCast(gvr.FindControl("lblAmt"), Label).Text)

                    Dim FEE_ID_AMOUNT As String()
                    If FEEIDs.Trim <> "" Then
                        FEE_ID_AMOUNT = FEEIDs.Split("|")
                    Else
                        FEE_ID_AMOUNT = FEEIDs.Split("")
                    End If
                    If dAmount > 0 And FEE_ID_AMOUNT.Length > 0 Then
                        For i As Int16 = 0 To FEE_ID_AMOUNT.Length - 1
                            If retval = 0 And FEE_ID_AMOUNT(i).Trim <> "" Then
                                Dim FRD_FEE_ID As String = FEE_ID_AMOUNT(i).Split("#")(0)
                                Dim FRD_NARRATION As String = SqlHelper.ExecuteScalar(objConn.ConnectionString, CommandType.Text, "SELECT MAX(FEE_DESCR) FROM FEES.FEES_M WHERE FEE_ID=" & IIf(FRD_FEE_ID.Trim <> "", FRD_FEE_ID, 0) & "")
                                Dim FRD_AMOUNT As Double = Convert.ToDouble(FEE_ID_AMOUNT(i).Split("#")(1))
                                retval = TransportRefund.F_SaveTRANSPORT_REFUND_D(0, str_NEW_FRH_ID, FRD_AMOUNT, _
                                FRD_FEE_ID, FRD_NARRATION, STU_ID, stTrans)

                                If retval <> 0 Then
                                    Exit For
                                End If
                                dDiffAmount = dDiffAmount + dAmount - FRD_AMOUNT
                            End If
                        Next
                    Else
                        Exit For
                    End If

                Next
            End If
            If dDiffAmount < CDec(txtBankCharge.Text) Or CDec(txtBankCharge.Text) < 0 Then
                retval = 941 'Please check bank charge
            End If
            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FRH_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                '  lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
                clear_All()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            ' lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub SettleFee()

        If IsDate(txtFrom.Text) And h_Student_no.Value <> "" Then
            Dim retval As Integer
            Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                retval = FeeCollectionTransport.F_SettleFee_Transport(Session("sBsuid"), txtFrom.Text, _
                h_Student_no.Value, ddlBusinessunit.SelectedValue, stTrans)
                If retval = "0" Then
                    stTrans.Commit()
                Else
                    stTrans.Rollback()
                    '    lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                '  lblError.Text = getErrorMessage(1000)
                usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        End If
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        rbBank.Enabled = True
        rbCash.Enabled = True
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub SetTCReference()
        If h_Student_no.Value <> "" Then
            Dim STR_SQL As String = "SELECT TCM.TCM_bLabAppr, TCM.TCM_bFeeAppr, TCM.TCM_bLibAppr,TCM.TCM_ID, " & _
            " TCM_bRegAppr,'Refund'AS EVENT, TCM.TCM_TCSO, TCM.TCM_LASTATTDATE, TCM.TCM_REASON " & _
            " FROM VW_OSO_STUDENT_M INNER JOIN TCM_M TCM ON TCM_BSU_ID =STU_BSU_ID " & _
            " AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID  " & _
            " WHERE STU_bActive=1 AND isnull(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' " & _
            " AND TCM_bCANCELLED = 0 AND TCM_bRegAppr = 1 AND ISNULL(TCM_bCANCELLED,0) = 0 " & _
            " AND TCM_BSU_ID = '" & Session("sBsuid") & "' AND STU_ID = " & h_Student_no.Value
            Dim dr As SqlDataReader
            Dim bDataExists As Boolean = False
            dr = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, STR_SQL)
            While (dr.Read())
                bDataExists = True
                chkFee.Checked = dr("TCM_bFeeAppr")
                chkLab.Checked = dr("TCM_bLabAppr")
                chkLibrary.Checked = dr("TCM_bLibAppr")
                chkRegistrar.Checked = dr("TCM_bRegAppr")
                lblAdjType.Text = "Last Att. Date : " & Format(dr("TCM_LASTATTDATE"), OASISConstants.DateFormat)
                txtAdjustment.Text = dr("TCM_REASON")
                h_Adjustment.Value = dr("TCM_ID")
                Exit While
            End While
            If Not bDataExists Then
                chkFee.Checked = False
                chkLab.Checked = False
                chkLibrary.Checked = False
                chkRegistrar.Checked = False
                lblAdjType.Text = ""
                txtAdjustment.Text = ""
                h_Adjustment.Value = ""
            End If
        End If
    End Sub

    Sub setStudntData()
        If rbEnquiry.Checked Then
            H_STU_TYPE.Value = "E"
        Else
            H_STU_TYPE.Value = "S"
        End If
        SettleFee()
    End Sub

    Sub StudentDetails(ByRef gvStuDetail As GridView, ByVal STU_ID As String)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                       CommandType.Text, " FEES.GetStudentDetailsForConcession '" & STU_ID & "'")
        gvStuDetail.DataSource = ds.Tables(0)
        gvStuDetail.DataBind()
    End Sub

    Sub Set_GridTotal()
        Dim dRefundAmount As Decimal = 0
        Dim dNetRefundAmount As Decimal
        Dim dtTotalApprvAmt As Double = 0
        Dim txtAmt As TextBox
        For i As Integer = 0 To gvRefund.Rows.Count - 2
            Dim gvRefundRow As GridViewRow = gvRefund.Rows(i)
            txtAmt = CType(gvRefundRow.FindControl("txtApprAmt"), TextBox)
            If txtAmt Is Nothing OrElse txtAmt.Text = "" Then Continue For
            dtTotalApprvAmt += CDbl(txtAmt.Text)
        Next
        If gvRefund.Rows.Count > 1 And gvNetRefund.Rows.Count > 1 Then
            'dRefundAmount = Convert.ToDecimal(gvRefund.Rows(gvRefund.Rows.Count - 1).Cells(1).Text)
            dNetRefundAmount = Convert.ToDecimal(gvNetRefund.Rows(gvNetRefund.Rows.Count - 1).Cells(1).Text)
            txtAmt = CType(gvRefund.Rows(gvRefund.Rows.Count - 1).FindControl("txtApprAmt"), TextBox)
            If Not txtAmt Is Nothing Then
                txtAmt.Text = dtTotalApprvAmt
                txtAmt.Attributes.Add("readonly", "readonly")
            End If
            If dtTotalApprvAmt <= dNetRefundAmount * -1 AndAlso dtTotalApprvAmt > 0 Then
                btnSave.Enabled = True
                btnAddUpdate.Enabled = True
            Else
                btnSave.Enabled = False
                btnAddUpdate.Enabled = False
            End If
        Else
            btnAddUpdate.Enabled = False
        End If
        If gvRefundSummary.Rows.Count < 1 Then
            btnSave.Enabled = False
        Else
            btnSave.Enabled = True
        End If
    End Sub

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
        ClearStudentData()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
        ClearStudentData()
    End Sub

    Sub ClearStudentData()
        txtStudent.Text = ""
        ViewState("gvSelectedStudents") = Nothing
        ViewState("gvRefund") = Nothing
        ViewState("gvNetRefund") = Nothing
        ViewState("gvRefundSummary") = Nothing
        setStudntData()
        gvSelectedStudents.DataBind()
        gvRefundSummary.DataBind()
        gvRefund.DataBind()
        gvNetRefund.DataBind()
        Set_GridTotal()
    End Sub

    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged
        Set_CashorBank()
    End Sub

    Sub Set_CashorBank()
        If rbCash.Checked Then
            tr_Bank.Visible = False
            tr_Cheque.Visible = False
            tr_Cash.Visible = True
            tr_Provision.Visible = False
            tr_ChqType.Visible = False
        Else
            tr_Bank.Visible = True
            If btnApprove.Visible Then
                tr_Cheque.Visible = True
                tr_Provision.Visible = True
                tr_ChqType.Visible = True
            End If
            FillLotNo()
            tr_Cash.Visible = False
        End If
    End Sub

    Private Sub FillLotNo()
        If txtBankCode.Text <> "" Then
            Dim str_sql As String = " SELECT TOP 1 ISNULL(CHQBOOK_M.CHB_LOTNO,'') AS LOT_NO, " & _
            " CHB_ID, ISNULL(MIN(ISNULL(CHD_NO,'')),'') AS CHD_NO FROM CHQBOOK_M INNER JOIN " & _
            " CHQBOOK_D ON CHQBOOK_M.CHB_ID = CHQBOOK_D.CHD_CHB_ID WHERE " & _
            " (CHQBOOK_D.CHD_ALLOTED = 0  ) AND (CHQBOOK_M.CHB_ACT_ID = '" & txtBankCode.Text & "')" & _
            " AND CHQBOOK_M.CHB_BSU_ID = '" & Session("sBSUID") & "'"

            str_sql += "GROUP BY CHB_LOTNO, CHB_ID "
            hCheqBook.Value = "0"
            txtChqNo.Text = "0"
            txtChqBook.Text = ""
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
            While (dr.Read() And Session("BSU_IsOnDAX") = "0")
                hCheqBook.Value = dr("CHB_ID")
                txtChqNo.Text = dr("CHD_NO")
                If dr("LOT_NO") <> 0 Then
                    txtChqBook.Text = dr("LOT_NO")
                Else
                    txtChqBook.Text = ""
                End If
            End While
        End If
    End Sub

    Protected Sub rbBank_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBank.CheckedChanged
        Set_CashorBank()
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim str_error As String = ""
        If rbBank.Checked And Session("BSU_IsOnDAX") = "0" Then
            If (txtChqNo.Text = "" Or hCheqBook.Value = "") And rbCheque.Checked Then
                str_error = str_error & "Please Select Cheque"
            End If
        Else
            txtChqNo.Text = "0"
            hCheqBook.Value = "0"
        End If

        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        Dim STR_DOCTYPE As String = String.Empty
        Dim STR_DOCNO As String = String.Empty
        If rbBank.Checked Then
            STR_DOCTYPE = "BP"
            STR_BankOrCash = "B"
            If txtBankCode.Text = "" Then
                str_error = str_error & "Invalid bank<br />"
            Else
                STR_ACCOUNT = txtBankCode.Text
            End If
            If rbOthers.Checked And txtrefChequeno.Text.Trim = "" Then
                str_error = str_error & "Invalid Ref. No. <br />"
            End If
        Else
            STR_DOCTYPE = "CP"
            STR_BankOrCash = "C"
            If txtCashAcc.Text = "" Then
                str_error = str_error & "Invalid cash account<br />"
            Else
                STR_ACCOUNT = txtCashAcc.Text
            End If
            'If AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtFrom.Text.Trim) < CDbl(txtAmount.Text) Then
            '    lblError.Text = "There is not Enough Balance in the Account"
            '    Exit Sub
            'End If
        End If
        Dim lblAmt As New Label
        Dim dRefundAmount As Decimal = 0
        If gvRefundSummary.Rows.Count > 0 And gvRefundSummary.Rows.Count > 0 Then
            'dRefundAmount = Convert.ToDecimal(gvRefund.Rows(gvRefund.Rows.Count - 1).Cells(1).Text)
            lblAmt = CType(gvRefundSummary.FooterRow.FindControl("lblAmtTotal"), Label)
            dRefundAmount = Convert.ToDecimal(lblAmt.Text)
            If dRefundAmount <= 0 Then
                str_error = str_error & "Invalid selection <br />"
            End If
        Else
            str_error = str_error & "Invalid selection <br />"
        End If
        If str_error <> "" Then
            ' lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            If h_Adjustment.Value = "" Then
                h_Adjustment.Value = 0
            End If
            Dim retval As String = "1000"
            AccountFunctions.GetNextDocId(STR_DOCTYPE, Session("sBsuid"), CType(txtFrom.Text, Date).Month, CType(txtFrom.Text, Date).Year)
            ViewState("doctype") = STR_DOCTYPE
            STR_DOCNO = "" 'current acd of student selected in sp so here scd is pasased as 0
            Dim FRH_STU_ID As String = Me.gvRefundSummary.DataKeys(gvRefundSummary.Rows.Count - 1)("STU_ID")
            retval = TransportRefund.F_SaveTRANSPORT_REFUND_H(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), Session("sBsuid"), ddlBusinessunit.SelectedValue, 0, H_STU_TYPE.Value, _
            FRH_STU_ID, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, 0, False, _
            txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, txPaidto.Text, dRefundAmount, txtBankCharge.Text, stTrans)

            Dim dTotalAmount As Decimal = 0
            Dim dDiffAmount As Decimal = 0
            Dim STU_NOs As String = ""
            If retval = "0" Then
                For Each gvr As GridViewRow In gvRefundSummary.Rows
                    Dim STU_ID As String = Me.gvRefundSummary.DataKeys(gvr.RowIndex)("STU_ID")
                    Dim FEEIDs As String = DirectCast(gvr.FindControl("lblFeeIDs"), Label).Text
                    Dim dAmount As Decimal = Convert.ToDouble(DirectCast(gvr.FindControl("lblAmt"), Label).Text)
                    STU_NOs = STU_NOs + IIf(STU_NOs = "", "", ",") + gvr.Cells(0).Text

                    Dim FEE_ID_AMOUNT As String()
                    If FEEIDs.Trim <> "" Then
                        FEE_ID_AMOUNT = FEEIDs.Split("|")
                    Else
                        FEE_ID_AMOUNT = FEEIDs.Split("")
                    End If
                    If dAmount > 0 And FEE_ID_AMOUNT.Length > 0 Then
                        For i As Int16 = 0 To FEE_ID_AMOUNT.Length - 1
                            If retval = 0 And FEE_ID_AMOUNT(i).Trim <> "" Then
                                Dim FRD_FEE_ID As String = FEE_ID_AMOUNT(i).Split("#")(0)
                                Dim FRD_NARRATION As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT MAX(FEE_DESCR) FROM FEES.FEES_M WITH(NOLOCK) WHERE FEE_ID=" & IIf(FRD_FEE_ID.Trim <> "", FRD_FEE_ID, 0) & "")
                                Dim FRD_AMOUNT As Double = Convert.ToDouble(FEE_ID_AMOUNT(i).Split("#")(1))
                                Dim FRD_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT FRD_ID FROM FEES.FEE_REFUND_D WITH(NOLOCK) WHERE FRD_FRH_ID = " & Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")) & " AND FRD_FEE_ID=" & FRD_FEE_ID & " AND FRD_STU_ID=" & STU_ID & "")
                                retval = TransportRefund.F_SaveTRANSPORT_REFUND_D(FRD_ID, Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), FRD_AMOUNT, _
                                    FRD_FEE_ID, FRD_NARRATION, STU_ID, stTrans)

                                If retval <> 0 Then
                                    Exit For
                                End If
                                dDiffAmount = dDiffAmount + dAmount - FRD_AMOUNT
                            End If
                        Next
                    Else
                        Exit For
                    End If
                Next
            End If
            If retval = 0 Then
                If dTotalAmount <> txtAmount.Text Then
                    txtAmount.Text = dTotalAmount
                End If
                retval = F_GenerateRefundVoucher(objConn, txtFrom.Text, "Transport Fee Refund for : " & STU_NOs & " " & txtRemarks.Text, dRefundAmount, STR_DOCTYPE, _
                       STR_DOCNO, FRH_STU_ID, stTrans)
            End If

            If ViewState("datamode") <> "edit" And retval = 0 Then
                retval = TransportRefund.PostTransportFeeRefund(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), _
                STR_DOCNO, txtFrom.Text, stTrans)
            End If
            If retval = "0" Then
                stTrans.Commit()
                ViewState("FRH_VHH_DOCNO") = STR_DOCNO
                ViewState("FRH_BANK_CASH") = STR_BankOrCash
                If STR_BankOrCash = "B" Then
                    chkPrintChq.Visible = True
                End If
                btnPrint1.Visible = True
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, STR_DOCNO, _
            "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                ' lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
                clear_All()
            Else
                stTrans.Rollback()
                '   lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            ' lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub txtApprAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub

    Protected Sub txtApprAmt_Approve_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal_Approve()
    End Sub

    Sub Set_GridTotal_Approve()
        Dim dRefundAmount As Decimal = 0
        Dim txtAmt As TextBox
        For i As Integer = 0 To gvApproval.Rows.Count - 1
            Dim gvApprovalRow As GridViewRow = gvApproval.Rows(i)
            txtAmt = CType(gvApprovalRow.FindControl("txtApprAmt"), TextBox)
            If txtAmt Is Nothing OrElse txtAmt.Text = "" Then Continue For
            dRefundAmount += CDbl(txtAmt.Text)
        Next
        txtAmount.Text = Format(dRefundAmount, "0.00")
    End Sub

    Protected Sub btnPrint1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint1.Click
        If Not ViewState("FRH_VHH_DOCNO") Is Nothing Then
            If ViewState("FRH_BANK_CASH") = "C" Then
                Session("ReportSource") = VoucherReports.CashPaymentVoucher(Session("sBSUID"), Session("F_YEAR"), Session("SUB_ID"), "CP", ViewState("FRH_VHH_DOCNO"), True)
                Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
            Else
                If chkPrintChq.Checked Then
                    Session("ReportSource") = AccountsReports.ChquePrint(ViewState("FRH_VHH_DOCNO"), "", Session("sBSUID"), Session("F_YEAR"), "BP")
                    If Session("ReportSource").Equals(Nothing) Then
                        '   lblError.Text = "Cheque Format not supported for printing"
                        usrMessageBar.ShowNotification("Cheque Format not supported for printing", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    Response.Redirect("../Accounts/accChqPrint.aspx?ChequePrint=BP", True)
                Else
                    Session("ReportSource") = VoucherReports.BankPaymentVoucher(Session("sBSUID"), Session("F_YEAR"), Session("SUB_ID"), "BP", ViewState("FRH_VHH_DOCNO"), False, True)
                    Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
                End If
            End If
        End If

    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_error As String = ""
        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        Dim STR_DOCNO As String = String.Empty
        If rbBank.Checked Then
            STR_BankOrCash = "B"
        Else
            STR_BankOrCash = "C"
        End If
        If h_Adjustment.Value = "" Then
            h_Adjustment.Value = 0
        End If

        If txtBankCode.Text = "" Then
            str_error = str_error & "Invalid bank<br />"
        Else
            STR_ACCOUNT = txtBankCode.Text
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim FRH_STU_ID As String = Me.gvRefundSummary.DataKeys(gvRefundSummary.Rows.Count - 1)("STU_ID")
            retval = TransportRefund.F_SaveTRANSPORT_REFUND_H(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), Session("sBsuid"), ddlBusinessunit.SelectedValue, 0, H_STU_TYPE.Value, _
               FRH_STU_ID, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, 0, True, _
            txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, txPaidto.Text, txtAmount.Text, txtBankCharge.Text, stTrans)

            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, STR_DOCNO, _
                "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                '  lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
                clear_All()
            Else
                stTrans.Rollback()
                ' lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Public Function F_GenerateRefundVoucher(ByVal objConn As SqlConnection, ByVal p_Date As Date, ByVal p_Narration As String, _
    ByVal p_Amount As String, ByVal p_Doctype As String, ByRef STR_DOCNO As String, ByVal STU_ID As String, ByVal stTrans As SqlTransaction) As String
        Dim SqlCmd As New SqlCommand("[F_GenerateTransportRefundVoucher]", objConn, stTrans)
        SqlCmd.CommandType = CommandType.StoredProcedure

        SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
        SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
        SqlCmd.Parameters.AddWithValue("@VHH_STU_BSU_ID", Me.ddlBusinessunit.SelectedValue)
        SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
        SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", p_Doctype)

        If rbCash.Checked Then
            SqlCmd.Parameters.AddWithValue("@VHD_CHQID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", "1/jan/1900")
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtCashAcc.Text)
            SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", "")
        Else
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
            If rbCheque.Checked = True Then
                If Session("BSU_IsOnDAX") = "1" Then
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQID", 0)
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtFrom.Text))
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", "0")
                Else
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQID", Convert.ToInt32(hCheqBook.Value))
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtChqdt.Text))
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", txtChqNo.Text)
                End If
            Else
                SqlCmd.Parameters.AddWithValue("@VHD_CHQID", 0)
                SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", txtFrom.Text)
                If Session("BSU_IsOnDAX") = "1" Then
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", "0")
                Else
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", txtrefChequeno.Text)
                End If
            End If
        End If

        SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
        SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", p_Narration)
        SqlCmd.Parameters.AddWithValue("@AMOUNT", p_Amount)
        SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txPaidto.Text)

        SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", txtProvCode.Text)
        SqlCmd.Parameters.AddWithValue("@STU_ID", STU_ID)
        SqlCmd.Parameters.AddWithValue("@STU_NAME", "Multiple Students")
        SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", p_Date)
        SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
        SqlCmd.Parameters.AddWithValue("@VHH_BBearer", ChkBearer.Checked)
        SqlCmd.Parameters.Add("@VHH_DOCNO", SqlDbType.VarChar, 20)
        SqlCmd.Parameters("@VHH_DOCNO").Direction = ParameterDirection.Output
        SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
        SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue


        SqlCmd.ExecuteNonQuery()
        If SqlCmd.Parameters("@ReturnValue").Value = 0 Then
            STR_DOCNO = CStr(SqlCmd.Parameters("@VHH_DOCNO").Value)
        End If
        Return SqlCmd.Parameters("@ReturnValue").Value
    End Function

    Protected Sub rbCheque_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCheque.CheckedChanged
        set_chequeno_controls()
    End Sub

    Protected Sub rbOthers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOthers.CheckedChanged
        set_chequeno_controls()
    End Sub

    Sub set_chequeno_controls()
        If rbOthers.Checked = True Then
            txtrefChequeno.Enabled = True
            txtChqBook.Text = "0"
            txtChqNo.Text = "0"
            hCheqBook.Value = "0"
        Else
            txtrefChequeno.Enabled = False
            txtrefChequeno.Text = "0"
        End If
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        FillACD()
        ClearStudentData()
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setStudntData()
        BindRefundData()
        Set_GridTotal()
        SetTCReference()
    End Sub
    Protected Sub imgStudentN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 'Handles imgStudentN.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub
    Protected Sub txtStudent_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStudent.TextChanged
        AddStudent()
    End Sub
    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        AddStudent()
    End Sub
    Private Sub AddStudent()
        If txtStudent.Text <> "" Then
            Dim str_stuid As String = FeeCommon.GetTransportStudentID(txtStudent.Text, ddlBusinessunit.SelectedValue, Me.rbEnquiry.Checked, True)
            If str_stuid <> "" Then
                If h_STUD_ID.Value.EndsWith("||") Then
                    h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                Else
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & str_stuid
                End If
                txtStudent.Text = ""
                FillSTUNames(h_STUD_ID.Value)
                h_STUD_ID.Value = ""
            End If
        End If
    End Sub
    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim DTSelectedStudents As DataTable
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        Dim TableName As String = ""
        TableName = IIf(rbEnquiry.Checked, "dbo.VW_OSO_STUDENT_ENQUIRY", "FEES.VW_OSO_STUDENT_DETAILS")
        str_Sql = " SELECT STU_NO , STU_NAME,STU_ID FROM " & TableName & " WHERE STU_ID IN (" + condition + ")"
        DTSelectedStudents = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql).Tables(0)
        If gvSelectedStudents.Rows.Count > 0 And DTSelectedStudents.Rows.Count > 0 Then
            Dim dtExisting As DataTable = ViewState("gvSelectedStudents")
            dtExisting.Merge(RemoveDuplicateRows(DTSelectedStudents))
            ViewState("gvSelectedStudents") = dtExisting
        Else
            ViewState("gvSelectedStudents") = DTSelectedStudents
        End If

        BindSelectedStudents(ViewState("gvSelectedStudents"))
        If DTSelectedStudents Is Nothing Or DTSelectedStudents.Rows.Count <= 0 Then
            Return False
        Else
            h_STUD_ID.Value = ""
        End If
        Return True
    End Function
    Private Sub BindSelectedStudents(ByVal DTT As DataTable)
        gvSelectedStudents.DataSource = DTT
        gvSelectedStudents.DataBind()
    End Sub
    Private Function RemoveDuplicateRows(ByVal DT2 As DataTable) As DataTable
        For Each gvr As GridViewRow In Me.gvSelectedStudents.Rows
            Dim StudentID As String = Me.gvSelectedStudents.DataKeys(gvr.RowIndex)("STU_ID")
            For j As Int16 = DT2.Rows.Count - 1 To 0 Step -1
                Dim dr As DataRow = DT2.Rows(j)
                If dr("STU_ID").ToString = StudentID Then
                    DT2.Rows.Remove(dr)
                    j -= 1
                End If
            Next
        Next

        Return DT2
    End Function

    Protected Sub gvSelectedStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSelectedStudents.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim GvStudDetail As GridView = DirectCast(e.Row.FindControl("gvStdDetails"), GridView)
            Dim LnkPayHistory As LinkButton = DirectCast(e.Row.FindControl("lnkPayHistory"), LinkButton)
            Dim lnkstuLedger As LinkButton = DirectCast(e.Row.FindControl("lnkstuLedger"), LinkButton)
            Dim StudentID As String = Me.gvSelectedStudents.DataKeys(e.Row.RowIndex)("STU_ID")
            LnkPayHistory.Attributes.Add("OnClick", "return OpenHistoryScreen(" + StudentID + ");")
            StudentDetails(GvStudDetail, StudentID)
        End If
    End Sub
    Protected Sub lnkStuLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim LNK As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(LNK.NamingContainer, GridViewRow)
        Dim StudentID As String = Me.gvSelectedStudents.DataKeys(gvr.RowIndex)("STU_ID")
        F_rptStudentLedger_Transport(StudentID)
    End Sub
    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.lblStudent.Text = ""
        Dim LNK As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(LNK.NamingContainer, GridViewRow)
        Dim StudentID As String = Me.gvSelectedStudents.DataKeys(gvr.RowIndex)("STU_ID")
        Dim StudentNo As String = DirectCast(gvr.FindControl("lblStudID"), Label).Text
        Dim StudentName As String = DirectCast(gvr.FindControl("lblStudentName"), Label).Text
        Me.gvSelectedStudents.SelectedIndex = gvr.RowIndex
        h_Student_no.Value = StudentID
        imgStudent_Click(Nothing, Nothing)
        Me.lblStudent.Text = "(" + StudentNo + ") - " + StudentName
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim LNK As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(LNK.NamingContainer, GridViewRow)
        Dim STUID As Int32 = gvSelectedStudents.DataKeys(gvr.RowIndex)("STU_ID")
        If ValidateStudentDelete(STUID) = True Then
            DirectCast(ViewState("gvSelectedStudents"), DataTable).Rows.RemoveAt(gvr.RowIndex)
            DirectCast(ViewState("gvSelectedStudents"), DataTable).AcceptChanges()
            BindSelectedStudents(ViewState("gvSelectedStudents"))
            ClearRefundDetails()
        End If
    End Sub
    Function ValidateStudentDelete(ByVal stuID As Int32) As Boolean
        If gvRefundSummary.Rows.Count > 0 Then
            For Each gvr As GridViewRow In gvRefundSummary.Rows
                If gvRefundSummary.DataKeys(gvr.RowIndex)("STU_ID") = stuID Then
                    ValidateStudentDelete = False
                    Exit Function
                End If
            Next
        End If
        ValidateStudentDelete = True
    End Function
    Private Sub BindRefundData()
        ViewState("gvRefund") = Nothing
        ViewState("gvNetRefund") = Nothing

        Dim dtHData As DataTable = TransportRefund.GetRefundData(ddlBusinessunit.SelectedValue, H_STU_TYPE.Value, h_Student_no.Value, "HEADER")
        If Not dtHData Is Nothing Then
            ViewState("gvRefund") = dtHData
            gvRefund.DataSource = dtHData
            gvRefund.DataBind()
        End If

        Dim dtDetail As DataTable = TransportRefund.GetRefundData(ddlBusinessunit.SelectedValue, H_STU_TYPE.Value, h_Student_no.Value, "DETAIL")
        If Not dtDetail Is Nothing Then
            ViewState("gvNetRefund") = dtDetail
            gvNetRefund.DataSource = dtDetail
            gvNetRefund.DataBind()
        End If

        If ViewState("gvRefund") Is Nothing AndAlso dtDetail.Rows.Count <= 0 Then
            tbl_Allocation.Visible = False
        Else
            tbl_Allocation.Visible = True
        End If
    End Sub
    Private Sub ClearRefundDetails()
        h_Student_no.Value = "0"
        imgStudent_Click(Nothing, Nothing)
        Me.lblStudent.Text = ""
        Me.gvSelectedStudents.SelectedIndex = -1
    End Sub

    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click
        If Me.gvRefund.Rows.Count > 0 Then
            Dim RefundAmount As Double = 0
            Dim FeeIDs As String = ""
            For Each gvr As GridViewRow In gvRefund.Rows
                If gvr.RowType = DataControlRowType.DataRow Then

                    If gvr.Cells(0).Text.ToUpper = "TOTAL" Or Me.gvRefund.DataKeys(gvr.RowIndex)("FEE_ID") = "999999" Then
                        RefundAmount = DirectCast(gvr.FindControl("txtApprAmt"), TextBox).Text
                    ElseIf Convert.ToDouble(DirectCast(gvr.FindControl("txtApprAmt"), TextBox).Text) > 0 Then
                        FeeIDs = FeeIDs & "|" & Me.gvRefund.DataKeys(gvr.RowIndex)("FEE_ID") & "#" & DirectCast(gvr.FindControl("txtApprAmt"), TextBox).Text
                    End If

                End If
            Next
            If RefundAmount > 0 Then
                If ViewState("gvRefundSummary") Is Nothing Then
                    Dim dtTemp As DataTable = CreategvRefundSummarySchema()
                    Dim drTemp As DataRow = dtTemp.NewRow
                    drTemp("STU_ID") = h_Student_no.Value
                    drTemp("STU_NO") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NO FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("STU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NAME FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("AMOUNT") = RefundAmount
                    drTemp("FEE_IDs") = FeeIDs

                    dtTemp.Rows.Add(drTemp)
                    ViewState("gvRefundSummary") = dtTemp
                    BindRefundSummary(dtTemp)
                Else
                    For Each gvrow As GridViewRow In Me.gvRefundSummary.Rows
                        If h_Student_no.Value = Me.gvRefundSummary.DataKeys(gvrow.RowIndex)("STU_ID") Then 'Duplicate row
                            '    Me.lblError2.Text = "Refund detail for student already added in current transaction, Please remove it from below and try again."
                            usrMessageBar.ShowNotification("Refund detail for student already added in current transaction, Please remove it from below and try again.", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    Next
                    Dim dtTemp As DataTable = DirectCast(ViewState("gvRefundSummary"), DataTable)
                    Dim drTemp As DataRow = dtTemp.NewRow
                    drTemp("STU_ID") = h_Student_no.Value
                    drTemp("STU_NO") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NO FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("STU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NAME FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("AMOUNT") = RefundAmount
                    drTemp("FEE_IDs") = FeeIDs

                    dtTemp.Rows.Add(drTemp)
                    ViewState("gvRefundSummary") = dtTemp
                    BindRefundSummary(dtTemp)
                End If
                ClearRefundDetails()
            End If

        End If
    End Sub
    Private Function CreategvRefundSummarySchema() As DataTable
        Dim dtRefundSummary As New DataTable
        dtRefundSummary.Columns.Add("STU_ID", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("STU_NO", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("STU_NAME", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("FEE_IDs", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("AMOUNT", Type.GetType("System.Double"))

        Return dtRefundSummary
    End Function
    Protected Sub btnCancelUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelUpdate.Click
        ClearRefundDetails()
    End Sub
    Private Sub BindRefundSummary(ByVal DT As DataTable)
        Me.gvRefundSummary.DataSource = DT
        Me.gvRefundSummary.DataBind()
    End Sub
    Protected Sub gvRefundSummary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRefundSummary.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gvFeeDetails As GridView = DirectCast(e.Row.FindControl("gvFeeDetails"), GridView)
            Dim LnkPayHistory As LinkButton = DirectCast(e.Row.FindControl("lnkBtnFeeDetails"), LinkButton)
            Dim LblFEEIDs As Label = DirectCast(e.Row.FindControl("lblFeeIDs"), Label)
            Dim FEEIDs As String() = LblFEEIDs.Text.Split("|")
            Dim QRY As String = ""
            For i As Int16 = 0 To FEEIDs.Length - 1
                If FEEIDs(i) <> "" Then
                    Dim FEEID As String = FEEIDs(i).Split("#")(0)
                    Dim AMT As Double = FEEIDs(i).Split("#")(1)
                    QRY = QRY & IIf(QRY = "", "", " UNION ALL ") & " SELECT FEE_DESCR AS FEE," & AMT & " AS AMOUNT FROM OASIS_FEES.FEES.FEES_M WHERE FEE_ID='" & FEEID & "' "
                End If
            Next
            gvFeeDetails.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QRY)
            gvFeeDetails.DataBind()

        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            DirectCast(e.Row.FindControl("lblAmtTotal"), Label).Text = Format(DirectCast(ViewState("gvRefundSummary"), DataTable).Compute("SUM(AMOUNT)", ""), Session("BSU_DataFormatString"))
        End If
    End Sub
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim LNK As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(LNK.NamingContainer, GridViewRow)
        DirectCast(ViewState("gvRefundSummary"), DataTable).Rows.RemoveAt(gvr.RowIndex)
        DirectCast(ViewState("gvRefundSummary"), DataTable).AcceptChanges()
        BindRefundSummary(ViewState("gvRefundSummary"))
    End Sub

    Protected Sub gvRefund_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRefund.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(0).Text.ToUpper = "TOTAL" Then
                DirectCast(e.Row.FindControl("txtApprAmt"), TextBox).Enabled = False
            Else
                DirectCast(e.Row.FindControl("txtApprAmt"), TextBox).Enabled = True
            End If
        End If
    End Sub

    Private Sub F_rptStudentLedger_Transport(ByVal STU_ID As Long)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim FromDate, ToDate As String
        ToDate = Format(Now.Date, OASISConstants.DateFormat)
        FromDate = UtilityObj.GetDataFromSQL("SELECT BSU_TRANSPORT_STARTDT FROM BUSINESSUNIT_M WHERE BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'", str_conn)
        If Not IsDate(FromDate) Then
            FromDate = "1/Sep/2007"
        Else
            Format(CDate(FromDate), OASISConstants.DateFormat)
        End If

        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub1 As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub1.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(STU_ID, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub1.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub1.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
        cmdSub2.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)
        cmdSub1.Parameters.AddWithValue("@STU_BSU_ID", ddlBusinessunit.SelectedItem.Value)
        cmdSub2.Parameters.AddWithValue("@STU_BSU_ID", ddlBusinessunit.SelectedItem.Value)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = IIf(rbEnrollment.Checked, "S", "E")
        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub1.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(FromDate)
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub1.Parameters.AddWithValue("@FromDT", CDate(FromDate))
        cmdSub2.Parameters.AddWithValue("@FromDT", CDate(FromDate))

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(ToDate)
        cmd.Parameters.Add(sqlpTODT)
        cmdSub1.Parameters.AddWithValue("@ToDT", CDate(ToDate))
        cmdSub2.Parameters.AddWithValue("@ToDT", CDate(ToDate))

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub1.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = FromDate
        params("ToDate") = ToDate
        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFee_Transport_StudentLedgerNext.rpt"
        Session("ReportSource") = repSource
        h_print.Value = "ledger"
    End Sub
End Class

