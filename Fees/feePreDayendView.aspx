<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feePreDayendView.aspx.vb" Inherits="Fees_feePreDayendView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function printTable() {
            var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            disp_setting += "scrollbars=yes,width=800, height=600, left=100, top=25";
            var content_vlue = document.getElementById('tblKpiTable').innerHTML;
            var docprint = window.open("", "", disp_setting);
            docprint.document.open();
            var HeaderRow = '<table width="100%" cellpadding="0" cellspacing="0"><tr><td class="title" align="center" >';
            HeaderRow = HeaderRow + 'Day end status </td></tr></table>';
            docprint.document.write('<html><head>');
            docprint.document.write('<link href="../cssfiles/sb-admin.css" rel="stylesheet" type="text/css" />');
            docprint.document.write('<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />');
            docprint.document.write('</head><body onLoad="self.print();self.close();" style="margin-top:0px;margin-left:0px;margin-right:0px;" ><center>');
            docprint.document.write(HeaderRow);
            docprint.document.write(content_vlue);
            docprint.document.write('</center></body></html>');
            docprint.document.close();
            docprint.focus();
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="View day end status"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="40%">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server"></uc1:usrTopFilter>
                        </td>
                        <td align="left" width="60%">
                            <a onclick='printTable()' id="a_print" style="vertical-align: middle;  cursor: pointer">Print </a></td>
                    </tr>
                    <tr>
                        <td valign="top" id="tblKpiTable" colspan="2">
                            <asp:GridView ID="gvFEEAdjustments" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="30" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField HeaderText="DOCNO">
                                        <HeaderTemplate>
                                            Transaction<br />
                                                                        <asp:TextBox ID="txtTRANTYPE" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnDocNoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("FPD_TRANTYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("FPD_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Log Date">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("FPD_LOGDATE", "{0:dd/MMM/yyyy hh:mm tt}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="user">
                                        <HeaderTemplate>
                                            User<br />
                                                                        <asp:TextBox ID="txtUSER" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FPD_USER") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Processed Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMOUNT" runat="server" Text='<%# Bind("FPD_PROCESSTIME") %>'></asp:Label>
                                            <itemstyle horizontalalign="Center"></itemstyle>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks<br />
                                                                            <asp:TextBox ID="txtRemarks" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                                                            <asp:ImageButton ID="btnRemarksSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                                        
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FPD_RETMESSAGE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>

</asp:Content>

