Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEEAdjustmentView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                gvFEECHQBOUNCE.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_BOUNCE) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                hlAddNew.NavigateUrl = "FEECHEQUEBOUNCE.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEECHQBOUNCE.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvFEECHQBOUNCE.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            Dim ds As New DataSet
            Dim str_Sql As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrOpr As String = String.Empty
            If gvFEECHQBOUNCE.Rows.Count > 0 Then
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)
                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtDate")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCR_DATE", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtRecNo")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCQ_CHQNO", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtBank")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BNK_DESCRIPTION", lstrCondn5)
            End If
            Dim str_cond As String = String.Empty
            If radOpen.Checked Then
                str_cond += " AND isnull( FCR_Bposted, 0) = 0"
            ElseIf radPosted.Checked Then
                str_cond += " AND isnull( FCR_Bposted, 0) = 1"
            End If

            Dim pParms(6) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuId")
            pParms(1) = New SqlClient.SqlParameter("@bPOSTED", SqlDbType.Bit)
            pParms(1).Value = radPosted.Checked
            pParms(2) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(2).Value = lstrCondn1
            pParms(3) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar, 100)
            pParms(3).Value = lstrCondn2
            pParms(4) = New SqlClient.SqlParameter("@FCR_DATE", SqlDbType.VarChar, 20)
            pParms(4).Value = lstrCondn3
            pParms(5) = New SqlClient.SqlParameter("@FCQ_CHQNO", SqlDbType.VarChar, 20)
            pParms(5).Value = lstrCondn4
            pParms(6) = New SqlClient.SqlParameter("@BNK_DESCRIPTION", SqlDbType.VarChar, 100)
            pParms(6).Value = lstrCondn5

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, "FEES.GET_CHEQUE_LIST_BOUNCED", pParms)

            'str_Sql = "select * from FEES.FeeChequeListBounced " & _
            '   " WHERE     (FCR_BSU_ID = '" & Session("sBSUID") & "') " & str_cond & str_Filter
            'Dim str_orderby As String = " ORDER BY FCR_DATE DESC"
            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql & str_orderby)
            gvFEECHQBOUNCE.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEECHQBOUNCE.DataBind()
                Dim columnCount As Integer = gvFEECHQBOUNCE.Rows(0).Cells.Count
                gvFEECHQBOUNCE.Rows(0).Cells.Clear()
                gvFEECHQBOUNCE.Rows(0).Cells.Add(New TableCell)
                gvFEECHQBOUNCE.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEECHQBOUNCE.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEECHQBOUNCE.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEECHQBOUNCE.DataBind()
            End If
            txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtRecNo")
            txtSearch.Text = lstrCondn4

            txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtBank")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEECHQBOUNCE.PageIndexChanging
        gvFEECHQBOUNCE.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEECHQBOUNCE.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim datamode As String = Encr_decrData.Encrypt("view")
                Dim lblFCR_ID As New Label
                lblFCR_ID = TryCast(e.Row.FindControl("lblFCR_ID"), Label)
                Dim hlview As New HyperLink 
                Dim MainMnu_code As String
                hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
                If (lblFCR_ID IsNot Nothing) Then
                    MainMnu_code = Encr_decrData.Encrypt(OASISConstants.MNU_FEE_CHEQUE_BOUNCE)
                    hlview.NavigateUrl = "FEECHEQUEBOUNCEDetailedView.aspx?FCR_ID=" & Encr_decrData.Encrypt(lblFCR_ID.Text) & _
                   "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode
                End If
                Dim docNoArr As String()
                Dim docNoStr As String = ""
                docNoArr = e.Row.Cells(9).Text.Replace("&nbsp;", "").Split(",")
                e.Row.Cells(9).Text = ""
                For x As Integer = 0 To docNoArr.Length - 1
                    Dim docNo As String
                    docNo = "<a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & _
                "javascript:getPrint( '" + docNoArr(x).Trim() + "', '" + e.Row.Cells(0).Text + "' ); " & Chr(34) & _
                " title = " & Chr(34) & " Click for print.. " & docNoArr(x) & Chr(34) & " style=" & Chr(34) & "cursor:hand;" & _
                " text-decoration: underline;" & Chr(34) & " class=" & Chr(34) & "gridheader_Link" & Chr(34) & " >" & docNoArr(x) & "</a>"
                    e.Row.Cells(9).Text += docNo & " "
                    e.Row.Cells(9).Style("cursor") = "hand"
                Next
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub radOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOpen.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPosted.CheckedChanged
        GridBind()
    End Sub

End Class
