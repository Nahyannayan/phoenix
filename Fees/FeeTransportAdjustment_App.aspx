<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeTransportAdjustment_App.aspx.vb" Inherits="FeeTransportAdjustment_App" Theme="General" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                if (document.getElementById('<%= h_print.ClientID %>').value == 'voucher') {
                    //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                    return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
                    return false;
                }
                if (document.getElementById('<%= h_print.ClientID %>').value == 'ledger') {
                    //showModelessDialog('../Reports/ASPX Report/RptViewerModalview.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                    return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModalview.aspx', 'search', '55%', '85%')
                    return false;
                }
                if (document.getElementById('<%= h_print.ClientID %>').value == 'audit') {
                    //showModelessDialog('../Reports/ASPX Report/rptReportViewerModelView.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                    return ShowWindowWithClose('../Reports/ASPX Report/rptReportViewerModelView.aspx', 'search', '55%', '85%')
                    return false;
                }
                document.getElementById('<%= h_print.ClientID %>').value = '';
            }
        }
    );
        function TransportAttendanceView() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 800px; dialogHeight: 675px; help: no; ";
            sFeatures += "resizable: no; scroll: yes; status: no; unadorned: no; ";
            var NameandCode;
            var result;
            var url = "feeTransportAttendanceView.aspx?stu=" + document.getElementById('<%=h_STUD_ID.ClientID %>').value + "&bsu=" + document.getElementById('<%= h_BSU_ID.ClientID %>').value;
            //result = window.showModalDialog(url, "", sFeatures)
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments Approval"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="Error" />--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="REJECTERROR" CssClass="error" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR" CssClass="error" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student Business Unit</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblStudBSUName" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Approval Date </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAppDate" runat="server"></asp:TextBox>
                            <asp:Image ID="ImgDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAppDate"
                                ErrorMessage="Approval Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reason</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblReason" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" width="20%"><span class="field-label">Requested Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtReqDate" runat="server" AutoPostBack="True"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg-lite">Student Details 
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student ID</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblStudentNO" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" width="20%"><span class="field-label">Name</span></td>
                        <td align="left" width="30%">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblStudName" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Grade</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr runat="server" id="trJoinDet">
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblAcademicYear" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" width="20%"><span class="field-label">Joined Date</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblJDate" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr runat="server" id="TrserviceDet">
                        <td align="left" width="20%"><span class="field-label">Service taken Date</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblSerTakenDate" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" width="20%"><span class="field-label">Discontinue Date</span></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblDiscontdate" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr runat="server">
                        <td align="left" width="20%"><span class="field-label">Area</span></td>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblArea" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="70%">
                                        <asp:LinkButton ID="lbLedger" runat="server">Student Ledger</asp:LinkButton>&nbsp;&nbsp;
               <asp:LinkButton ID="lbTransport" runat="server">Transport Audit</asp:LinkButton>&nbsp;&nbsp;
               <%--<asp:LinkButton ID="lblAttendance" runat="server" OnClientClick="TransportAttendanceView();return false;">Attendance</asp:LinkButton>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Request Remarks</span></td>
                        <td align="left" ccolspan="3">
                            <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg-lite" colspan="2">Service Usage History</td>
                        <td align="left" class="title-bg-lite" colspan="2">Fee Adjustments Details For Approval...</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:GridView ID="gvSerUsageHistory" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="SBL_DESCRIPTION" HeaderText="Area"></asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="SSV_FROMDATE" HeaderText="FROM DATE"></asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="SSV_TODATE" HeaderText="TO DATE"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td align="left" colspan="2">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FeeId">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemStyle Width="135px" HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                            <asp:LinkButton ID="lnkFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>' OnClientClick="return false;" Visible="False"></asp:LinkButton>
                                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="lnkFeeType" Position="Left" PopupControlID="Panel1" DynamicServiceMethod="GetDynamicContent" DynamicControlID="Panel1" DynamicContextKey='<%# Eval("FEE_ID") %>'>
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Charged Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# bind("CHARGEDAMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Paid Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("PAIDAMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="Conc. Duration">
                                        <ItemStyle Width="40px" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%# bind("DURATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reqd. Amount">
                                        <ItemStyle Width="75px" HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# bind("FEE_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Approved Amount">
                                        <ItemStyle Width="80px" HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtApprAmt" Style="text-align: right" OnTextChanged="txtApprAmt_TextChanged" runat="server" Width="80px" ValidationGroup="MAINERROR" AutoPostBack="True"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtApprAmt"
                                                ErrorMessage="Approved Amount Required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Tax Code">
                                        <ItemStyle Width="75px" HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxCode" runat="server" Text='<%# bind("FEE_TAX_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Tax Amount">
                                        <ItemStyle Width="75px" HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxAmount" runat="server" Text='<%# bind("FEE_TAX_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemStyle Width="75px" HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetAmount" runat="server" Text='<%# bind("FEE_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>









                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Approval Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtApprRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Approval Remarks required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtApprRemarks">*</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtApprRemarks"
                                ErrorMessage="Rejection Remarks required" ValidationGroup="REJECTERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" ValidationGroup="REJECTERROR" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server">
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server">
                </asp:Panel>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtAppDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtAppDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_BSU_ID" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_STU_SCT_DESCR" runat="server" />
                <asp:HiddenField ID="h_STU_STATUS" runat="server" />
                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });

                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                </script>


            </div>
        </div>
    </div>
</asp:Content>
