Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEETransportAdjustmentREQ_APPView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_TRANSPORT AndAlso _
                ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_APP_TRANSPORT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                ddBusinessunit.DataBind()
                If Not ddBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")) Is Nothing Then
                    ddBusinessunit.ClearSelection()
                    ddBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")).Selected = True
                End If
                FillACD()
                gvFEEAdjustments.Attributes.Add("bordercolor", "#1b80b6")
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_TRANSPORT
                        hlAddNew.NavigateUrl = "FeeTransportAdjustment_REQ.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Transport Fee Adjustment Request"
                        hlAddNew.Visible = True
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_APP_TRANSPORT
                        hlAddNew.Visible = False
                        lblHeader.Text = "Transport Fee Adjustment Approval"
                End Select
                ViewState("BSU_IDs") = ""
                LoadBusinessUnit()
                GridBind(ViewState("BSU_IDs"))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEAdjustments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvFEEAdjustments.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind(Optional ByVal BSU_IDs As String = "")
        If BSU_IDs = "" Then
            BSU_IDs = "'" & ddBusinessunit.SelectedItem.Value & "'"
        End If
        For iColumnIndex As Integer = 6 To gvFEEAdjustments.Columns.Count - 1
            gvFEEAdjustments.Columns(iColumnIndex).Visible = True
        Next
        If radApproved.Checked Then
            gvFEEAdjustments.Columns(9).Visible = True
        Else
            gvFEEAdjustments.Columns(9).Visible = False
        End If
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim larrSearchOpr() As String
            Dim ds As New DataSet
            Dim txtSearch As New TextBox
            Dim str_Sql As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim lstrOpr As String = String.Empty
            If gvFEEAdjustments.Rows.Count > 0 Then
                ' --- Initialize The Variables
                '   --- FILTER CONDITIONS ---
                '   -- 1   STU_NAME
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn1)
                '   -- 1  FAR_DATE
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAR_DATE", lstrCondn2)
                '   -- 2  ACY_DESCR
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtStudentNo")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAR_REMARKS", lstrCondn4)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtSlno")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAR_BSU_SLNO", lstrCondn5)
                '   -- 6   txtTDate
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDocno")
                lstrCondn6 = txtSearch.Text.Trim
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAH_DOCNO", lstrCondn6)

            End If
            Dim str_cond As String = String.Empty
            Dim vStatus As String = String.Empty
            If radApproved.Checked Then
                vStatus = "A"
            ElseIf radReject.Checked Then
                vStatus = "R"
            ElseIf radRequested.Checked Then
                vStatus = "N"
            End If
            Dim selFilter As String = " FAR_APPRSTATUS = '" & vStatus & "' AND FAR_BSU_ID ='" & Session("sBSUID") & "' AND FAR_STU_BSU_ID IN (" & BSU_IDs & ")"

            If ddBusinessunit.SelectedValue = "0" Then
                selFilter &= " AND B.ACD_ACY_ID = " & ddlAcademicYear.SelectedValue
            Else
                selFilter &= " AND FAR_ACD_ID = " & ddlAcademicYear.SelectedValue
            End If
            If radStud.Checked Then
                str_Sql = "SELECT *,C.BSU_SHORTNAME FROM FEES.VW_FEEADJREQUEST_Transport A INNER JOIN dbo.BUSINESSUNIT_M C WITH(NOLOCK) ON A.FAR_STU_BSU_ID = C.BSU_ID " & _
                " INNER JOIN OASIS.dbo.ACADEMICYEAR_D B WITH(NOLOCK) ON FAR_ACD_ID = B.ACD_ID " & _
                " WHERE isnull(FAR_STU_TYP,'S') = 'S' AND " & selFilter & str_Filter
            ElseIf radEnq.Checked Then
                str_Sql = "SELECT *,C.BSU_SHORTNAME FROM FEES.VW_FEEADJREQUEST_Transport A INNER JOIN dbo.BUSINESSUNIT_M C WITH(NOLOCK) ON A.FAR_STU_BSU_ID = C.BSU_ID " & _
                " INNER JOIN OASIS.dbo.ACADEMICYEAR_D B WITH(NOLOCK) ON FAR_ACD_ID = B.ACD_ID " & _
                " WHERE FAR_STU_TYP = 'E' AND " & selFilter & str_Filter
            End If
            str_Sql += " ORDER BY FAR_DATE DESC,C.BSU_SHORTNAME"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvFEEAdjustments.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEAdjustments.DataBind()
                Dim columnCount As Integer = gvFEEAdjustments.Rows(0).Cells.Count

                gvFEEAdjustments.Rows(0).Cells.Clear()
                gvFEEAdjustments.Rows(0).Cells.Add(New TableCell)
                gvFEEAdjustments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEAdjustments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEAdjustments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEAdjustments.DataBind()
            End If
            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtStudentNo")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn4

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtSlno")
            txtSearch.Text = lstrCondn5

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDocno")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEAdjustments.PageIndexChanging
        gvFEEAdjustments.PageIndex = e.NewPageIndex
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEAdjustments.RowDataBound
        Try
            Dim lblFAR_ID As New Label
            lblFAR_ID = TryCast(e.Row.FindControl("lblFAR_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)

            If hlEdit IsNot Nothing And lblFAR_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_TRANSPORT
                        hlEdit.NavigateUrl = "FeeTransportAdjustment_REQ.aspx?FAR_ID=" & Encr_decrData.Encrypt(lblFAR_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_APP_TRANSPORT
                        hlEdit.NavigateUrl = "FeeTransportAdjustment_App.aspx?FAR_ID=" & Encr_decrData.Encrypt(lblFAR_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub radRequested_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radRequested.CheckedChanged
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub radApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radApproved.CheckedChanged
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub radReject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radReject.CheckedChanged
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        If ddBusinessunit.SelectedValue = "0" Or ddBusinessunit.SelectedItem.Text = "ALL" Then
            Dim BsuIds As New StringBuilder
            For i As Integer = 0 To ddBusinessunit.Items.Count - 1 Step 1
                If ddBusinessunit.Items(i).Value <> "0" Then
                    BsuIds.Append(IIf(BsuIds.ToString = "", "", ",") & "'" & ddBusinessunit.Items(i).Value & "'")
                End If
            Next
            FillACY()
            ViewState("BSU_IDs") = BsuIds.ToString
            GridBind(BsuIds.ToString)
        Else
            Session("PROVIDER_BSU_ID") = ddBusinessunit.SelectedItem.Value
            FillACD()
            GridBind()
        End If
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind(ViewState("BSU_IDs"))
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFAH_ID As Label = sender.Parent.parent.findcontrol("lblFAH_ID")
            Dim hf_STU_BSU_ID As HiddenField = DirectCast(sender.Parent.parent.FindControl("hf_STU_BSU_ID"), HiddenField)
            If lblFAH_ID.Text <> "0" Then
                Session("ReportSource") = FEEADJUSTMENTTransport.PrintAdjustmentVoucher(lblFAH_ID.Text, hf_STU_BSU_ID.Value, _
                           Session("sBsuid"), Session("sUsr_name"))
                h_print.Value = "voucher"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub LoadBusinessUnit()
        Try
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_ADJUSTMENTS_APP_TRANSPORT
                    If ddBusinessunit.Items.Count > 0 Then
                        ddBusinessunit.Items.Insert(0, New ListItem("ALL", "0"))
                        ddBusinessunit.SelectedIndex = 0
                        ddBusinessunit_SelectedIndexChanged(Nothing, Nothing)
                    End If
            End Select

        Catch ex As Exception

        End Try
    End Sub

    Sub FillACY()
        Dim qry = "SELECT DISTINCT TOP 6  ACY_ID,ACY_DESCR,ISNULL(ACD_CURRENT,0)ACD_CURRENT FROM dbo.ACADEMICYEAR_M A " & _
        "LEFT JOIN dbo.ACADEMICYEAR_D B ON A.ACY_ID=B.ACD_ACY_ID " & _
        "AND B.ACD_BSU_ID='131001' AND ACD_CURRENT=1 WHERE ISNULL(ACY_bSHOW,0)=1 ORDER BY ACY_ID DESC"
        Dim dtACD As DataTable = Mainclass.getDataTable(qry, ConnectionManger.GetOASISConnectionString)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACY_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
End Class
