﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tax_Code_Configuration_View.aspx.vb" Inherits="Accounts_Tax_Code_Configuration_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Tax Code Configuration"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="text-align: center">
                            <table id="tbl_test" runat="server" width="100%">
                                <tr>
                                    <td align="left" class="matters" colspan="9" valign="top">
                                        <asp:GridView ID="gv_Data" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Country">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCTY_DESCR" runat="server" Text='<%# Bind("CTY_DESCR") %>'
                                                            __designer:wfdid="w4"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fee Type">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTXM_REF_TYPE" runat="server" Text='<%# Bind("TXM_REF_TYPE") %>'
                                                            __designer:wfdid="w7"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Ref. Type">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTXM_DESCR" runat="server" Text='<%# Bind("TXM_DESCR") %>'
                                                            __designer:wfdid="w11"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Tax Desc.">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTAX_DESCR" runat="server" Text='<%# Bind("TAX_DESCR") %>'
                                                            __designer:wfdid="w15"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                                        <asp:Label ID="lblTXM_FROMDT" runat="server" Text='<%# Convert.ToDateTime(Eval("TXM_FROMDT")).ToString("dd-MMM-yyyy")%>'
                                                            __designer:wfdid="w17"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" Width="20%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TXM_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTXM_ID" runat="server" Text='<%# Bind("TXM_ID") %>'
                                                            __designer:wfdid="w54"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

