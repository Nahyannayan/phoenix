Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj

Partial Class FEEAdjustmentPosting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If Page.IsPostBack = False Then
            btnPost.Attributes.Add("onclick", ("javascript:" & btnPost.ClientID & ".disabled=true;") + ClientScript.GetPostBackEventReference(btnPost, "").ToString())
            Dim MainMnu_code As String
            Dim menu_rights As Integer
            Dim datamode As String = "none"
            If Request.QueryString("editerror") <> "" Then
                'lblError.Text = "Record already posted/Locked"
                usrMessageBar2.ShowNotification("Record already posted/Locked", UserControls_usrMessageBar.WarningType.Danger)
            Else
                'lblError.Text = ""
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                datamode = ""
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or MainMnu_code <> "F301005" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Response.CacheControl = "no-cache"
                'calling pageright class to get the access rights
                menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, datamode)

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                FillACD()
                gvFeeAdjustmentDet.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
            End If
        End If
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()
        Dim vFEE_ADJ As FEEADJUSTMENT = FEEADJUSTMENT.GetFeeAdjustmentsInternalTransfer(contextKey)
        If vFEE_ADJ Is Nothing Then
            Return String.Empty
        End If
        sTemp.Append("<table >") '  bordercolor=#1b80b6 bgcolor=#ffcccc cellpadding=1 cellspacing=0>")
        sTemp.Append("<tr>")
        sTemp.Append("<td colspan=4><b>FEE Adjustment Details </b></td>")
        sTemp.Append("</tr>")
        sTemp.Append("<tr>")
        sTemp.Append("<td><b>FEE Type</b></td>")
        sTemp.Append("<td><b>CR. Student</b></td>")
        sTemp.Append("<td><b>Remarks</b></td>")
        sTemp.Append("<td><b>Amount</b></td>")
        sTemp.Append("</tr>")

        Dim STUD_ID As Integer = contextKey
        If vFEE_ADJ.FEE_ADJ_DET Is Nothing OrElse vFEE_ADJ.FEE_ADJ_DET.Count <= 0 Then
            'sTemp.Append("</table>")
            Return sTemp.ToString()
        End If
        For Each vFEE_ADJ_SUB As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
            sTemp.Append("<tr>")
            'sTemp.Append("<td>" & "1" & "</td>")
            sTemp.Append("<td>" & vFEE_ADJ_SUB.FEE_TYPE & "</td>")
            sTemp.Append("<td>" & vFEE_ADJ.FAH_STU_NAME_CR & "</td>")
            sTemp.Append("<td>" & vFEE_ADJ_SUB.FAD_REMARKS & "</td>")
            sTemp.Append("<td>" & vFEE_ADJ_SUB.FAD_AMOUNT & "</td>")
            'sTemp.Append("<td>" & FEE_SUB_DET.FPD_AMOUNT.ToString & "</td>")
            sTemp.Append("</tr>")
        Next
        sTemp.Append("</table>")

        Return sTemp.ToString()
    End Function

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFeeAdjustmentDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFeeAdjustmentDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeAdjustmentDet.RowDataBound
        Try
            Dim lblFAH_ID As New Label
            Dim lblbInter As New Label
            lblFAH_ID = TryCast(e.Row.FindControl("lblFAH_ID"), Label)
            lblbInter = TryCast(e.Row.FindControl("lblbInter"), Label)
            Dim hlview As New HyperLink
            Dim datamode As String = Encr_decrData.Encrypt("view")
            Dim MainMnu_code As String
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If (lblFAH_ID IsNot Nothing) AndAlso (lblbInter IsNot Nothing) Then
                If lblbInter.Text = "True" Then
                    MainMnu_code = Encr_decrData.Encrypt(OASISConstants.MNU_FEE_ADJUSTMENTS_HEADTOHEAD)
                    hlview.NavigateUrl = "FeeAdjustment_HeadtoHead.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                   "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode
                Else
                    MainMnu_code = Encr_decrData.Encrypt(OASISConstants.MNU_FEE_ADJUSTMENTS)
                    hlview.NavigateUrl = "FeeAdjustment.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                   "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function ValidateSelection(ByVal Page As Control) As Integer
        Dim retval As Integer = 0
        Dim lblFAH_DATE As Label
        Dim chkPost As CheckBox
        Dim vPostDate As Date = Date.MinValue
        Dim vHTSTU_DETAILS As New Hashtable
        For Each gvr As GridViewRow In gvFeeAdjustmentDet.Rows
            lblFAH_DATE = CType(gvr.FindControl("lblFAH_DATE"), Label)
            chkPost = CType(gvr.FindControl("chkPost"), CheckBox)
            If chkPost.Checked Then
                If vPostDate = Date.MinValue Then
                    vPostDate = lblFAH_DATE.Text
                ElseIf vPostDate <> CDate(lblFAH_DATE.Text) Then
                    Return -200
                End If
            End If
        Next
        Return retval
    End Function

    Private Function PostChecked(ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim retval As Integer
        Dim nochk As Boolean = False
        Dim FAH_IDs As String = String.Empty
        Session("arrPostedFEEAdj") = New ArrayList
        For Each gvr As GridViewRow In gvFeeAdjustmentDet.Rows
            Dim lblFAH_ID As Label = CType(gvr.FindControl("lblFAH_ID"), Label)
            Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
            If Not lblFAH_ID Is Nothing Then
                If IsNumeric(lblFAH_ID.Text) And chkPost.Checked Then
                    nochk = True
                    If FAH_IDs = String.Empty Then
                        FAH_IDs = lblFAH_ID.Text
                    Else
                        FAH_IDs = FAH_IDs & "|" & lblFAH_ID.Text
                    End If
                End If
            End If
        Next
        retval = F_POSTFEESADJUSTMENT(FAH_IDs, conn, trans)
        If Not nochk Then
            Return -100
        End If
        Return retval
    End Function

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtNarration.Text.Length > 300 Then
            txtNarration.Text = txtNarration.Text.Substring(0, 300)
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim stTrans As SqlTransaction
        Try
            objConn.Open()
            stTrans = objConn.BeginTransaction
            Try
                Dim iReturnvalue As Integer = ValidateSelection(Me.Page)
                If iReturnvalue = 0 Then iReturnvalue = PostChecked(objConn, stTrans)
                If (iReturnvalue = 0) Then
                    stTrans.Commit()
                    'lblError.Text = "Fee Adjustments Successfully Posted..."
                    usrMessageBar2.ShowNotification("Fee Adjustments Successfully Posted...", UserControls_usrMessageBar.WarningType.Success)
                    If chkPrint.Checked Then
                        PrintPostedVouchers()
                    End If
                Else
                    stTrans.Rollback()
                    If iReturnvalue = -100 Then
                        'lblError.Text = "Please select atleast 1 for Posting..."
                        usrMessageBar2.ShowNotification("Please select atleast 1 for Posting...", UserControls_usrMessageBar.WarningType.Danger)
                    ElseIf iReturnvalue = -200 Then
                        'lblError.Text = "Selected documents should belong to same date..."
                        usrMessageBar2.ShowNotification("Selected documents should belong to same date...", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    Else
                        'lblError.Text = getErrorMessage(iReturnvalue)
                        usrMessageBar2.ShowNotification(getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
                    End If
                End If
            Catch ex As Exception
                stTrans.Rollback()
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar2.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
                Errorlog(ex.Message)
            End Try
        Catch ex As Exception
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar2.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try
        gridbind()
    End Sub

    Private Sub PrintPostedVouchers()
        Dim arrList As ArrayList = Session("arrPostedFEEAdj")
        Dim ienum As IEnumerator = arrList.GetEnumerator
        Dim vDocNo As String = String.Empty
        Dim comma As String = String.Empty
        While ienum.MoveNext
            vDocNo = vDocNo & comma & "'" & ienum.Current & "'"
            comma = ","
        End While
        'Session("ReportSource") = AccountsReports.JournalVouchers(vDocNo, "", Session("sBSUID"), Session("F_YEAR"), vDocNo, "JV")
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
    End Sub

    Private Function F_POSTFEESADJUSTMENT(ByVal vFAH_ID As String, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim cmd As New SqlCommand("[FEES].[F_POSTFEESADJUSTMENT]", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUId")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFAH_ID As New SqlParameter("@FAH_ID", SqlDbType.VarChar)
        sqlpFAH_ID.Value = vFAH_ID
        cmd.Parameters.Add(sqlpFAH_ID)

        Dim sqlpNewDOCNo As New SqlParameter("@NewDOCNo", SqlDbType.VarChar, 20)
        sqlpNewDOCNo.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNewDOCNo)

        Dim sqlpNARRATION As New SqlParameter("@NARRATION", SqlDbType.VarChar)
        sqlpNARRATION.Value = txtNarration.Text
        cmd.Parameters.Add(sqlpNARRATION)

        Dim iReturnvalue As Integer
        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()

        iReturnvalue = retValParam.Value
        If iReturnvalue = 0 Then
            Session("arrPostedFEEAdj").add(sqlpNewDOCNo.Value)
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, sqlpNewDOCNo.Value, "FEE Adjustment Posting", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
        End If
        Return iReturnvalue
    End Function

    Private Sub gridbind()
        Dim larrSearchOpr() As String
        Dim txtSearch As New TextBox
        Dim lstrOpr As String = String.Empty
        Dim lstrCondn1 As String = String.Empty
        Dim lstrCondn2 As String = String.Empty
        Dim lstrCondn3 As String = String.Empty
        Dim lstrCondn4 As String = String.Empty
        Dim lstrCondn5 As String = String.Empty
        Dim str_Filter As String = String.Empty

        If gvFeeAdjustmentDet.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTU_NO")
            lstrCondn1 = Trim(txtSearch.Text)
            If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)
            '   -- 1  FAR_DATE
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTU_NAME")
            lstrCondn2 = Trim(txtSearch.Text)
            If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
            '   -- 1  FAR_DATE
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtFAH_DATE")
            lstrCondn3 = Trim(txtSearch.Text)
            If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAH_DATE", lstrCondn3)
            '   -- 2  ACY_DESCR
            larrSearchOpr = h_Selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtACY_DESCR")
            lstrCondn4 = txtSearch.Text
            If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "AMOUNT", lstrCondn4)
            '   -- 3   txtTDate
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtFAH_REMARKS")
            lstrCondn5 = txtSearch.Text
            If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAH_REMARKS", lstrCondn5)
        End If
        Dim str_Sql As String = String.Empty
        Dim str_orderBy As String = " ORDER BY FAH_DATE DESC"
        Dim str_groupby As String = " GROUP BY FAH_ID, FAH_ARM_ID,FAH_REMARKS, FAH_DATE, STU_NAME, FAH_ACD_ID, " & _
        " STU_NO, FAH_bInter, ACY_DESCR, FAH_bPosted, FAH_BSU_ID "
        Dim str_cond As String = " AND ISNULL(FAH_ARM_ID,0) NOT IN (11,13) AND isnull(FAH_bPosted,0) = 0 AND FAH_BSU_ID ='" & Session("sBSUID") & "' AND FAH_ACD_ID = " & ddlAcademicYear.SelectedValue
        If radStud.Checked Then
            str_Sql = "SELECT * FROM (SELECT FEES.FEEADJUSTMENT_H.FAH_ID,FAH_ARM_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
            " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NAME, VW_OSO_STUDENT_M.STU_NO," & _
            " FAH_bPosted, FAH_ACD_ID, " & _
            " FAH_bInter, VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID, " & _
            " sum(FEES.FEEADJUSTMENT_S.FAD_AMOUNT) AMOUNT " & _
            " FROM FEES.FEEADJUSTMENT_H INNER JOIN VW_OSO_STUDENT_M " & _
            " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = VW_OSO_STUDENT_M.STU_ID" & _
            " INNER JOIN FEES.FEEADJUSTMENT_S ON FEES.FEEADJUSTMENT_H.FAH_ID = FEES.FEEADJUSTMENT_S.FAD_FAH_ID " & _
            " WHERE FEES.FEEADJUSTMENT_H.FAH_bDeleted =0 AND FEES.FEEADJUSTMENT_H.FAH_STU_TYPE = 'S' " & str_groupby & ") a WHERE 1=1 "
        ElseIf radEnq.Checked Then
            str_Sql = "SELECT * FROM (SELECT FEES.FEEADJUSTMENT_H.FAH_ID,FAH_ARM_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
            " FEES.FEEADJUSTMENT_H.FAH_DATE, FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, vw_OSO_ENQUIRY_COMP.STU_NO, " & _
            " FAH_bPosted, FAH_ACD_ID, " & _
            " FAH_bInter, FEES.vw_OSO_ENQUIRY_COMP.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID, " & _
            " sum(FEES.FEEADJUSTMENT_S.FAD_AMOUNT) AMOUNT " & _
            " FROM FEES.FEEADJUSTMENT_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP " & _
            " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID" & _
            " INNER JOIN FEES.FEEADJUSTMENT_S ON FEES.FEEADJUSTMENT_H.FAH_ID = FEES.FEEADJUSTMENT_S.FAD_FAH_ID " & _
            " WHERE FEES.FEEADJUSTMENT_H.FAH_bDeleted =0 AND FEES.FEEADJUSTMENT_H.FAH_STU_TYPE = 'E' " & str_groupby & ") a WHERE 1=1 "
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, str_Sql & str_cond & str_Filter & str_orderBy)
        gvFeeAdjustmentDet.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvFeeAdjustmentDet.DataBind()
            Dim columnCount As Integer = gvFeeAdjustmentDet.Rows(0).Cells.Count

            gvFeeAdjustmentDet.Rows(0).Cells.Clear()
            gvFeeAdjustmentDet.Rows(0).Cells.Add(New TableCell)
            gvFeeAdjustmentDet.Rows(0).Cells(0).ColumnSpan = columnCount
            gvFeeAdjustmentDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvFeeAdjustmentDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvFeeAdjustmentDet.DataBind()
        End If

        txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTU_NO")
        If txtSearch IsNot Nothing Then
            txtSearch.Text = lstrCondn1
        End If
        txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTU_NAME")
        If txtSearch IsNot Nothing Then txtSearch.Text = lstrCondn2

        txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtFAH_DATE")
        If txtSearch IsNot Nothing Then txtSearch.Text = lstrCondn3


        txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtACY_DESCR")
        If txtSearch IsNot Nothing Then txtSearch.Text = lstrCondn4

        txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtFAH_REMARKS")
        If txtSearch IsNot Nothing Then txtSearch.Text = lstrCondn5

        set_Menu_Img()
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFeeAdjustmentDet.PageIndexChanging
        gvFeeAdjustmentDet.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        gridbind()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        gridbind()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnNarrationInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNarrationInsert.Click
        Dim str_stunos As String = String.Empty
        Dim str_date As String = String.Empty
        For Each gvr As GridViewRow In gvFeeAdjustmentDet.Rows
            Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
            Dim lblSTU_NO As Label = CType(gvr.FindControl("lblSTU_NO"), Label)
            Dim lblFAH_DATE As Label = CType(gvr.FindControl("lblFAH_DATE"), Label)
            If Not lblSTU_NO Is Nothing Then
                lblSTU_NO.Text = lblSTU_NO.Text.Trim()
                If chkPost.Checked Then
                    If str_stunos = "" Then
                        str_stunos = Convert.ToInt64(lblSTU_NO.Text.Substring(7, lblSTU_NO.Text.Length - 7))
                        str_date = lblFAH_DATE.Text.Trim
                    Else
                        str_stunos = str_stunos & ", " & Convert.ToInt64(lblSTU_NO.Text.Substring(7, lblSTU_NO.Text.Length - 7))
                    End If
                End If
            End If
        Next
        txtNarration.Text = "Fee adjustment for " & str_stunos & " As on " & str_date
    End Sub
End Class
