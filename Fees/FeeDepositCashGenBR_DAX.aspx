<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeDepositCashGenBR_DAX.aspx.vb" Inherits="Fees_FeeDepositCashGenBR_DAX" title="Untitled Page" %>
<%@ MasterType  virtualPath="~/mainMasterPage.master"%> 
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript">
    function ChangeCheckBoxState(id, checkState) {
        var cb = document.getElementById(id);
        if (cb != null)
            cb.checked = checkState;
    }
    function ChangeAllCheckBoxStates(checkState) {
        // Toggles through all of the checkboxes defined in the CheckBoxIDs array
        // and updates their value to the checkState input parameter
        var lstrChk = document.getElementById("chkAL").checked;
        if (CheckBoxIDs != null) {
            for (var i = 0; i < CheckBoxIDs.length; i++)
                ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
        }
    }
    function getBank() {
        var sFeatures;
        sFeatures = "dialogWidth: 820px; ";
        sFeatures += "dialogHeight: 450px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        <%--result = window.showModalDialog("../Accounts/accjvshowaccount.aspx?bankcash=b","", sFeatures)
        if (result=='' || result==undefined)
            {    return false;      } 
        lstrVal=result.split('___');     
        document.getElementById('<%=txtBankCode.ClientId %>').value=lstrVal[0];
        document.getElementById('<%=txtBankDescr.ClientId %>').value=lstrVal[1];
 }--%>

     var url = "../Accounts/accjvshowaccount.aspx?bankcash=b";
     var oWnd = radopen(url, "pop_getbank");

 }

 function OnClientClose1(oWnd, args) {
     //get the transferred arguments

     var arg = args.get_argument();

     if (arg) {

         NameandCode = arg.NameandCode.split('||');
         document.getElementById('<%=txtBankCode.ClientId %>').value = NameandCode[0];
         document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1];
         __doPostBack('<%=txtBankDescr.ClientID%>', 'TextChanged');
     }
 }


 function switchViews(obj, row) {
     var div = document.getElementById(obj);
     var img = document.getElementById('img' + obj);

     if (div.style.display == "none") {
         div.style.display = "inline";
         if (row == 'alt') {
             img.src = "../Images/expand_button_white_alt_down.jpg";
         }
         else {
             img.src = "../Images/Expand_Button_white_Down.jpg";
         }
         img.alt = "Click to close";
     }
     else {
         div.style.display = "none";
         if (row == 'alt') {
             img.src = "../Images/Expand_button_white_alt.jpg";
         }
         else {
             img.src = "../Images/Expand_button_white.jpg";
         }
         img.alt = "Click to expand";
     }
 }





 function autoSizeWithCalendar(oWindow) {
     var iframe = oWindow.get_contentFrame();
     var body = iframe.contentWindow.document.body;

     var height = body.scrollHeight;
     var width = body.scrollWidth;

     var iframeBounds = $telerik.getBounds(iframe);
     var heightDelta = height - iframeBounds.height;
     var widthDelta = width - iframeBounds.width;

     if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
     if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
     oWindow.center();
 }

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       
        <Windows>
            <telerik:RadWindow ID="pop_getbank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Generate Collection Receipt
        </div>
        <div class="card-body">
            <div class="table-responsive">

        <table width="100%" align="center">           
           <tr valign="top">
           <td align="left" valign="top"> 
           <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="error"></asp:Label>
           <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                </td></tr>
        </table>       
        <table align="center" width="100%">
           
            <tr>
                <TD align="left" width="20%">
                    <span class="field-label">Business Unit</span>
                </td>
                
                <td  align="left" width="30%">
                    <asp:DropDownList id="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                        DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal"
                        tabIndex="5" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                    </asp:DropDownList></td>
           
                <td align="left" width="20%">
                  <span class="field-label"> Doc Date</span></td>
                
                <td  align="left" width="30%">
                    <asp:TextBox ID="txtDocDate" runat="server" AutoPostBack="True" ></asp:TextBox>
                    &nbsp;
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
          <tr>
                
               
                <td align="left" colspan="4">
                    <table width="100%">
                        <tr>
                            <td align="left" width="20%" class="p-0">
                   <span class="field-label">Bank Account</span></td>
                            <td align="left" width="30%" class="p-0">
                                <asp:TextBox ID="txtBankCode" runat="server" ></asp:TextBox>
                    <a href="#" onclick="getBank();return false;">
                        <img border="0" src="../Images/cal.gif" id="IMG3" language="javascript"  /></a>
                            </td>
                            <td align="left" width="20%" class="p-0">
                   <span class="field-label">Bank Name</span></td>
                            <td align="left" width="30%" class="p-0">
                                <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>  
                            </td>
                        </tr>
                    </table>
                        
                                     
                </td>               
            </tr>
            <tr>
                <td align="left">
                    <span class="field-label">Narration</span></td>
                
                <td align="left">
                    <asp:TextBox ID="txtNarration" runat="server" SkinID="MultiText"
                        TabIndex="16" TextMode="MultiLine">CASH COLLECTION DEPOSIT</asp:TextBox></td>
                <td></td>
                <td></td>
            </tr>
            </table> 
             <table align="center" width="100%">
            <tr id="tr_grid" valign="top" runat="server">
                <td align="center"  colspan="4"  width="100%"> 
                   <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row" SkinID="GridViewNormal" 
                    AutoGenerateColumns="False" DataKeyNames="VHH_DOCNO"
                     OnRowDataBound="gvJournal_RowDataBound" Width="100%" AllowPaging="True" OnPageIndexChanging="gvJournal_PageIndexChanging"> 
                    <Columns>
                        <asp:TemplateField>
                           <ItemTemplate>
                               <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'one');">
                                   <img id="imgdiv<%# Eval("GUID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                               </a>
                           </ItemTemplate>
                           <AlternatingItemTemplate>
                              <a href="javascript:switchViews('div<%# Eval("GUID") %>', 'alt');">
                                   <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                               </a>
                           </AlternatingItemTemplate>
                       </asp:TemplateField>
                        <asp:BoundField DataField="VHH_DOCNO" HeaderText="Doc No" ReadOnly="True"
                                SortExpression="VHH_DOCNO" />
                            <asp:BoundField DataField="VHH_DOCDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc Date"
                                HtmlEncode="False" SortExpression="VHH_DOCDT" >
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                         
                            <asp:BoundField DataField="VHH_NARRATION" HeaderText="Narration" SortExpression="VHH_NARRATION" />
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("VHH_AMOUNT")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                             <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                   <asp:CheckBox ID="chkControl" runat="server" />
                                </ItemTemplate>
                                <HeaderTemplate>
        <input type="checkbox" name="chkAL" value="Check All" onclick="change_chk_state(this);" />Select 
                                </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="GUID" SortExpression="VHH_DOCNO" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="DOCNO" SortExpression="DOCNO" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("VHH_DOCNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                       <asp:TemplateField >
                           <ItemTemplate>
                              </td></tr>
                               <tr>
                                   <td colspan="100%">
                                       <div id="div<%# Eval("GUID") %>" style="display:none;position:relative;left:20px;" >
                                           <asp:GridView ID="GridView2" runat="server" Width="100%" CssClass="table table-bordered table-row"
                                              AutoGenerateColumns="false" DataKeyNames="GUID"
                                               EmptyDataText="No orders for this customer." >
                                       
                                               <Columns>
                                                   <asp:BoundField DataField="VHD_ACT_ID" HeaderText="Account Id"  HtmlEncode="False" >
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                                                   <asp:BoundField DataField="ACT_NAME" HeaderText="Account Name"  HtmlEncode="False" >
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                                                   <asp:BoundField DataField="VHD_AMOUNT" HeaderText="Amount" >
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                                               </Columns>
                                               <RowStyle CssClass="griditem" /> 
                                               <HeaderStyle CssClass="gridheader_pop" /> 
                                           </asp:GridView>
                                       </div>
                                   </td>
                               </tr>
                           </ItemTemplate>
                       </asp:TemplateField>
                   </Columns>                        
               </asp:GridView>
                    <br />
                    <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" /></td>
            </tr>
        </table> 
    <asp:ObjectDataSource id="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <selectparameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </selectparameters>
    </asp:ObjectDataSource>
    <input id="h_SelectedId" runat="server" type="hidden" />
     <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="ImageButton1" TargetControlID="txtDocDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtDocDate" TargetControlID="txtDocDate">
    </ajaxToolkit:CalendarExtender>

                </div>
            </div>
        </div>

</asp:Content>


