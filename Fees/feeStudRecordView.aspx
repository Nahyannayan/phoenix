<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeStudRecordView.aspx.vb" Inherits="StudRecordView" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>View Student/Edit Fee Sponsor
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" runat="server" align="center">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academic year</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" valign="top">
                                        <asp:GridView ID="gvStudentRecord" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("eqs_id") %>' __designer:wfdid="w7"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Join Date">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblJOINDTH" runat="server" Text="Join Date" __designer:wfdid="w10"></asp:Label><br />
                                                        <asp:TextBox ID="txtJOINDT" runat="server" Width="75%" __designer:wfdid="w11"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchJOINDT" OnClick="btnSearchJOINDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w12"></asp:ImageButton>
                                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblJOINTDT" runat="server" Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w8"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <HeaderTemplate>
                                                            <asp:Label ID="lblSTUD_IDH" runat="server" Text="Student ID" __designer:wfdid="w15"></asp:Label><br />
                                                            <asp:TextBox ID="txtSTUD_ID" runat="server" Width="75%" __designer:wfdid="w16"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchStud_ID" OnClick="btnSearchStud_ID_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w17"></asp:ImageButton>
                                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTUD_ID" runat="server" Text='<%# Bind("STU_No") %>' __designer:wfdid="w13"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStud_NameH" runat="server" Text="Student Name" __designer:wfdid="w3"></asp:Label><br />
                                                        <asp:TextBox ID="txtStud_Name" runat="server" Width="75%" __designer:wfdid="w4"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStud_Name" OnClick="btnSearchStud_Name_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w5"></asp:ImageButton>
                                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_PASPRTNAME") %>' __designer:wfdid="w6"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                       <asp:Label ID="lblGradeH" runat="server" Text="Grade Section" __designer:wfdid="w20"></asp:Label><br />
                                                        <asp:TextBox ID="txtStud_Grade" runat="server" Width="75%" __designer:wfdid="w21"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchGrade" OnClick="btnSearchGrade_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w22"></asp:ImageButton>
                                                        
                                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                                        <asp:Label ID="lblGradeH" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' __designer:wfdid="w18"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Edit" __designer:wfdid="w25"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblEdit" OnClick="lblEdit_Click" runat="server" __designer:wfdid="w23">Edit</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudentID" runat="server" Text='<%# Bind("stu_id") %>' __designer:wfdid="w26"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>

            </div>

        </div>

    </div>

</asp:Content>

