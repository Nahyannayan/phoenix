<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeDayEndProcessTransportAll.aspx.vb" Inherits="Fees_feeDayEndProcessTransportAll" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script Language="javascript" type="text/javascript">
 
 function getPrint(docNo,docDate)
   {     
        var sFeatures;
        sFeatures="dialogWidth: 800px; ";
        sFeatures+="dialogHeight: 700px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        if(docNo.substring(2,4) == "QC" )
        {
        alert('No voucher to print..');
        return ;
        }
        result = window.showModalDialog("rptDayenddocuments.aspx?DOCNO="+docNo+"&DOCDATE=" + docDate,"", sFeatures)
       return false;
    }      
  </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table width="100%">
        <tr valign="bottom">
            <td align="left">
                <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
         
    </table>
    <table width="100%" >
        
        <tr>
            <td align="left" width="20%" >
                <span class="field-label">Date</span></td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtDate" runat="server" Width="121px" AutoPostBack="True"></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return getDate(2);"
                    TabIndex="4" />
                <asp:Panel id="pnlLink" runat="server">
                    <asp:LinkButton id="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                        tabIndex="15">Dayend Details</asp:LinkButton>
                </asp:Panel>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                    PopDelay="25" PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="pnlLink">
                </ajaxToolkit:HoverMenuExtender>
            </td>
        
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                    DataTextField="BSU_NAME" DataValueField="SVB_PROVIDER_BSU_ID" SkinID="DropDownListNormal"
                    TabIndex="5" Enabled="False">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <asp:CheckBox id="chkHardClose" runat="server" Text="Hard Close" Checked="True">
                </asp:CheckBox></td>
        </tr> 
    <tr>
        <td  class="title-bg" align="left" colspan="4">
            Consolidated Dayend Status Details</td>                
    </tr> 
          <tr> 
            <td align="center" colspan="4"  >
                <asp:GridView ID="gvDayendDetails" runat="server" CssClass="table table-bordered table-row" Width="100%" EmptyDataText="No Details Added"  AutoGenerateColumns="False" >
                    <Columns>
                    <asp:BoundField DataField="SLNO" HeaderText="S.No">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit"></asp:BoundField>
                    <asp:BoundField DataField="FPD_DATE" HeaderText="Date"></asp:BoundField>
                    <asp:BoundField DataField="FPD_LOGDATE" HeaderText="Log Date"></asp:BoundField>
                    <asp:BoundField DataField="FPD_USER" HeaderText="User"></asp:BoundField>
                    <asp:BoundField DataField="FPD_RETMESSAGE" HeaderText="Status"></asp:BoundField>
                    <asp:BoundField DataField="FPD_PROCESSTIME" HeaderText="Process Time"></asp:BoundField> 
                    </Columns>
            </asp:GridView>
        </td >
        </tr>   
        <tr><td height ="10px" colspan="4" ></td></tr>
        <tr>
        <td  class="title-bg" align="left" colspan="4">
            Not Done Details</td>                
    </tr>
     <tr> 
            <td align="center" colspan="4" >
                <asp:GridView ID="gvNextstatus" runat="server" CssClass="table table-bordered table-row" Width="100%" EmptyDataText="No Details Added"  AutoGenerateColumns="False" >
                    <Columns>
                        <asp:BoundField DataField="SLNO" HeaderText="S.No">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit"></asp:BoundField>
                    </Columns>
            </asp:GridView>
        </td >
        </tr>  
        <tr>
        <td colspan="4" align="center">
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td>
        </tr>
        <tr>
        <td colspan="4" align="left" class="title-bg">
                Day End History</td>
        </tr>
        <tr>
        <td colspan="4">
    <asp:GridView ID="gvHistory" runat="server"  Width="100%">
    </asp:GridView>
              </td>
        </tr>
    </table>
      
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetTransportProviders" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" /> 
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Panel id="PanelTree" runat="server" BackColor="LightYellow" style="display: block">
        <table width="100%">
            <tr>
                <td align="left">
                    <span class="error">Dayend Status</span><asp:GridView id="gvCloseStatus" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="True" SkinID="GridViewRed"></asp:GridView>
                    <span class="error">Collection Details</span>&nbsp;<br />
                    <asp:GridView id="gvDayend" runat="server" AutoGenerateColumns="True" CssClass="table table-bordered table-row" >
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFrom" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
            
  
                </div>
            </div>
        </div>

</asp:Content>
