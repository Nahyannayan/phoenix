<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTranspotReminderAddDetail.aspx.vb" Inherits="Fees_feeTranspotReminderAddDetail" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 <script language="javascript" type="text/javascript">


function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);

__doPostBack("","");


}
}


function OnTreeClick1(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
if(isChkBoxClick)
{
var parentTable = GetParentByTagName("table", src);
var nxtSibling = parentTable.nextSibling;
//check if nxt sibling is not null & is an element node
if(nxtSibling && nxtSibling.nodeType == 1)
{
if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
{
//check or uncheck children at all levels
CheckUncheckChildren(parentTable.nextSibling, src.checked);
}
}
//check or uncheck parents at all levels
CheckUncheckParents(src, src.checked);
}
}


function CheckUncheckChildren(childContainer, check)
{
var childChkBoxes = childContainer.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = check;
}
}


function CheckUncheckParents(srcChild, check)
{
var parentDiv = GetParentByTagName("div", srcChild);
var parentNodeTable = parentDiv.previousSibling;
if(parentNodeTable)
{
var checkUncheckSwitch;
if(check) //checkbox checked
{
var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
if(isAllSiblingsChecked)
checkUncheckSwitch = true;
else
return; //do not need to check parent if any(one or more) child not checked
}
else //checkbox unchecked
{
checkUncheckSwitch = false;
}
var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
if(inpElemsInParentTable.length > 0)
{
var parentNodeChkBox = inpElemsInParentTable[0];
parentNodeChkBox.checked = checkUncheckSwitch;
//do the same recursively
CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
}
}
}

function AreAllSiblingsChecked(chkBox)
{
var parentDiv = GetParentByTagName("div", chkBox);
var childCount = parentDiv.childNodes.length;
for(var i=0;i<childCount;i++)
{
if(parentDiv.childNodes[i].nodeType == 1)
{
//check if the child node is an element node
if(parentDiv.childNodes[i].tagName.toLowerCase() == "table")
{
var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
//if any of sibling nodes are not checked, return false
if(!prevChkBox.checked)
{
return false;
}
}
}
}
return true;
}

//utility function to get the container of an element by tagname
function GetParentByTagName(parentTagName, childElementObj)
{
var parent = childElementObj.parentNode;
while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
{
parent = parent.parentNode;
}
return parent;
}

function UnCheckAll() 
{ 
var oTree=document.getElementById("<%=tvGrade.ClientId %>");
childChkBoxes = oTree.getElementsByTagName("input");
var childChkBoxCount = childChkBoxes.length;
for(var i=0;i<childChkBoxCount;i++)
{
childChkBoxes[i].checked = false;
}

return true;
}  
</script> 
 <script language="javascript" type="text/javascript">
 


//      function GetStudent()
//      {     
//        var sFeatures;
//        var sFeatures;
//        sFeatures="dialogWidth: 875px; ";
//        sFeatures+="dialogHeight: 600px; ";
//        sFeatures+="help: no; ";
//        sFeatures+="resizable: no; ";
//        sFeatures+="scroll: yes; ";
//        sFeatures+="status: no; ";
//        sFeatures+="unadorned: no; ";
//        var NameandCode;
//        var result; 
//        var url; 
//        url = "ShowStudentMultiTransport.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu="+document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
//        result = window.showModalDialog(url,"", sFeatures);
//        if(result != '' && result != undefined)
//         {        
//            
//         }
//        return true; 
//      }  
    </script>                


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Transport Fee Reminder Detail...
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
 <table align="center" width="100%" >
            
     <tr valign="top">
         <td align="left" width="20%">
             <span class="field-label">Select Business Unit</span></td>
         <td align="left" width="30%">
             <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                 DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal"
                 TabIndex="5" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
             </asp:DropDownList>
             </td>
             <td align="left" width="20%">
                <span class="field-label">Academic Year</td>
            <td align="left" width="30%">
            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
             </asp:DropDownList></td>
         </tr>
         <tr>
         <td align="left" valign="middle">
             <span class="field-label">Date</span>
         </td>
        <td align="left">
             <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
             <asp:ImageButton ID="ImgDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                        OnClientClick="return false;" />
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                 ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
         <td align="left" valign="middle" >
                    <span class="field-label">As On</span>
         </td>
        <td align="left">
            <asp:TextBox ID="txtAsOnDate" runat="server"  AutoPostBack="True"></asp:TextBox>
                    <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                        OnClientClick="return false;" />
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAsOnDate"
                         ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
     </tr>
            
            <tr valign="top">
            <td align="left" valign="middle" colspan="3" >
                    <asp:RadioButton ID="radThirdRM" runat="server" CssClass="field-label" Checked="True" GroupName="REMINDER" Text="Third Reminder" AutoPostBack="True" />
                    <asp:RadioButton ID="radSecRM" runat="server" CssClass="field-label" GroupName="REMINDER" Text="Second Reminder" AutoPostBack="True" />
                    <asp:RadioButton ID="radFirstRM" runat="server" CssClass="field-label" GroupName="REMINDER" Text="First Reminder" AutoPostBack="True" /></td>
           <td></td>     
        </tr>
     <tr valign="top">
         <td align="left" colspan="3" valign="middle">
             <asp:RadioButton id="radGrade" runat="server" CssClass="field-label" Checked="True" GroupName="Category"
                 Text="Grade wise" AutoPostBack="True" OnCheckedChanged="radGrade_CheckedChanged">
             </asp:RadioButton>
             &nbsp;
             <asp:RadioButton id="radBus" runat="server" CssClass="field-label" GroupName="Category" Text="Bus wise" AutoPostBack="True" OnCheckedChanged="radBus_CheckedChanged">
             </asp:RadioButton></td>
     </tr>
     <tr valign="top">
         <td align="left" colspan="4" width="100%">
             <table id="Table5" runat="server" width="100%">
                 <tr>
                     <td align="left" class="title-bg" colspan="4">                         
                             <asp:Label id="lblTitle" runat="server" Text="Label"></asp:Label></td>
                 </tr>
                 <tr>
                     <td align="left">
                         <div class="checkbox-list">
                         <asp:TreeView id="tvGrade" runat="server"  ExpandDepth="1"
                             MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick1(event);"
                             PopulateNodesFromClient="False" ShowCheckBoxes="All">
                             <parentnodestyle />
                             <hovernodestyle/>
                             <selectednodestyle />
                             <nodestyle nodespacing="1px" />
                         </asp:TreeView>
                         </div>
                     </td>
                     <td></td>
                     <td></td>
                     <td></td>
                 </tr>
             </table>
         </td>
     </tr>
    
           
     
     <tr>
         <td align="left" colspan="4">
             <asp:CheckBox id="ChkPrint" runat="server" OnCheckedChanged="ChkPrint_CheckedChanged"
                 Text="Print after Save" CssClass="field-label"  Checked="True">
             </asp:CheckBox></td>
     </tr>
     <tr>
         <td align="center" colspan="4">
             <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
             <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
             <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
     </tr>
        </table>

                </div>
            </div>
         </div>


<input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /> <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
    <input id="h_Selected_Value" runat="server" type="hidden" value="=" />
    <ajaxToolkit:CalendarExtender
    ID="calFromDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
    TargetControlID="txtAsOnDate">
</ajaxToolkit:CalendarExtender>
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:CalendarExtender
    ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgDate"
    TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    &nbsp;&nbsp;
    <asp:HiddenField ID="h_print" runat="server" />
    <asp:HiddenField ID="h_Grade_ID" runat="server" /><asp:HiddenField ID="h_Bus_ID" runat="server" />
</asp:Content>
