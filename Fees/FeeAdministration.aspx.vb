﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Data.OleDb
Imports Telerik.Web
Imports Telerik.Web.UI

Partial Class Fees_FeeAdministration
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property VSgvStLedger() As DataTable
        Get
            Return ViewState("gvStLedger")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvStLedger") = value
        End Set
    End Property
    Private Property VSGridRowCount() As Integer
        Get
            Return ViewState("GridRowCount")
        End Get
        Set(ByVal value As Integer)
            ViewState("GridRowCount") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> "F100175" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            FillACD()
            SetAcademicYearDate()
        End If
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
        SetAcademicYearDate()
    End Sub
    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, FeeCommon.GetCurrentAcademicYear(Session("sBSUID")), Session("sBSUID"))
        txtFrom.Text = DTFROM
        'txtToDate.Text = DTTO
        If txtFrom.Text = "" Then
            txtFrom.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtTo.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub
    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
        StudentDetail()
    End Sub
    Sub StudentDetail()
        lblError.Text = ""
        Me.lblName.Text = UsrSelStudent1.STUDENT_NAME
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "select STU_DOJ,STU_CURRSTATUS,STU_LASTATTDATE from OASIS..STUDENT_M where STU_ID='" & UsrSelStudent1.STUDENT_ID & "'")
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Me.lblDOJ.Text = Format(ds.Tables(0).Rows(0)("STU_DOJ"), OASISConstants.DateFormat)
                If IsDate(ds.Tables(0).Rows(0)("STU_LASTATTDATE").ToString) Then
                    Me.lblLastATTDate.Text = Format(ds.Tables(0).Rows(0)("STU_LASTATTDATE"), OASISConstants.DateFormat)
                End If
                Dim STU_CURRSTATUS As String
                STU_CURRSTATUS = ds.Tables(0).Rows(0)("STU_CURRSTATUS")
                btnRgnTC.Visible = IIf(STU_CURRSTATUS = "TC" Or STU_CURRSTATUS = "SO", True, False)
                btnRegCNReversal.Visible = IIf(STU_CURRSTATUS = "CN", True, False)
            End If
        End If
        'Me.lblDOJ.Text = Format(SqlHelper.ExescuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "select STU_DOJ from OASIS..STUDENT_M where STU_ID='" & UsrSelStudent1.STUDENT_ID & "'"), OASISConstants.DateFormat)
        Me.lblGrade.Text = UsrSelStudent1.STUDENT_GRADE_ID
    End Sub
    Private Sub StudentLedger(ByVal STUID As String, ByVal BSUID As String, ByVal FromDT As String, ByVal ToDT As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(STUID, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSUID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = FromDT 'DateTime.Now.AddYears(-2).ToShortDateString
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = ToDT 'DateTime.Now.ToShortDateString
        cmd.Parameters.Add(sqlpTODT)
        cmd.Connection = New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        adpt.SelectCommand = cmd
        'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adpt.SelectCommand.CommandTimeout = 0
        ds.Clear()
        adpt.Fill(ds)
        Dim bal As Decimal = 0.0
        ds.Tables(0).Columns.Add("Balance")
        ds.Tables(0).AcceptChanges()
        For Each Dr As DataRow In ds.Tables(0).Rows
            bal += Convert.ToDecimal(Dr("DEBIT")) - Convert.ToDecimal(Dr("CREDIT"))
            Dr("Balance") = String.Format("{0:n2}", bal)
        Next
        ViewState("gvStLedger") = ds.Tables(0)
        bindLedger()
    End Sub
   
    Sub bindLedger()
        If Convert.ToString(VSgvStLedger) <> "" AndAlso VSgvStLedger.Rows.Count > 0 Then
            Me.trLedger.Visible = True
            gvStLedger.DataSource = VSgvStLedger
            gvStLedger.DataBind()
        Else
            Me.trLedger.Visible = False
        End If

    End Sub
    Private Sub ClearAllFields()
        Me.lblError.Text = ""
        UsrSelStudent1.ClearDetails()
        StudentDetail()
        SetAcademicYearDate()
        bindLedger()
        TabContainer1.ActiveTabIndex = 0
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearAllFields()
    End Sub
    Protected Sub btnLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLedger.Click
        StudentLedger(UsrSelStudent1.STUDENT_ID, Session("sBsuid"), Me.txtFrom.Text, Me.txtTo.Text)
    End Sub
    Private Dr As Decimal = 0
    Private i As Int32 = 0
    'Private Bal As Decimal = 0

    Protected Sub gvStLedger_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStLedger.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(4).Text = "TOTAL"
            e.Row.Cells(5).Text = String.Format("{0:n2}", VSgvStLedger.Compute("Sum(DEBIT)", ""))
            e.Row.Cells(6).Text = String.Format("{0:n2}", VSgvStLedger.Compute("Sum(CREDIT)", ""))
            e.Row.Cells(7).Text = String.Format("{0:n2}", Convert.ToDecimal(e.Row.Cells(5).Text) - Convert.ToDecimal(e.Row.Cells(6).Text))
        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            i += 1
            If VSGridRowCount <> 0 And i > VSGridRowCount Then ' code to change the color of newly added rows in ledger
                e.Row.BackColor = Drawing.Color.BlanchedAlmond
            End If
        End If
    End Sub
    Protected Sub gvStLedger_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStLedger.PageIndexChanging
        gvStLedger.PageIndex = e.NewPageIndex
        bindLedger()
    End Sub

    

    Protected Sub btnRgnTC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRgnTC.Click
        RegenerateTCReverseRequest()
    End Sub
    Private Sub RegenerateTCReverseRequest()
        lblError.Text = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = ""
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.VarChar, 3000)
            pParms(1).Direction = ParameterDirection.Output
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid")
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(2).Value = UsrSelStudent1.STUDENT_ID
            pParms(3) = New SqlClient.SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            pParms(4) = New SqlClient.SqlParameter("@USER_ID", SqlDbType.VarChar, 200)
            pParms(4).Value = Session("sUserid")
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.REGENERATE_TC_REVERSAL", pParms)

            If pParms(3).Value = "0" Then
                stTrans.Commit()
                'Me.lblError.Text = pParms(1).Value
                usrMessageBar.ShowNotification(pParms(1).Value, UserControls_usrMessageBar.WarningType.Danger)
            Else
                stTrans.Rollback()
                'Me.lblError.Text = "Unable to complete the request"
                usrMessageBar.ShowNotification("Unable to complete the request", UserControls_usrMessageBar.WarningType.Danger)
            End If

        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnRegCNReversal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = ""
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.VarChar, 3000)
            pParms(1).Direction = ParameterDirection.Output
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid")
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(2).Value = UsrSelStudent1.STUDENT_ID
            pParms(3) = New SqlClient.SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            pParms(4) = New SqlClient.SqlParameter("@USER_ID", SqlDbType.VarChar, 200)
            pParms(4).Value = Session("sUserid")
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.REGENERATE_CN_REVERSAL", pParms)

            If pParms(3).Value = "0" Then
                stTrans.Commit()
                'Me.lblError.Text = pParms(1).Value
                usrMessageBar.ShowNotification(pParms(1).Value, UserControls_usrMessageBar.WarningType.Danger)
            Else
                stTrans.Rollback()
                'Me.lblError.Text = "Unable to complete the request"
                usrMessageBar.ShowNotification("Unable to complete the request", UserControls_usrMessageBar.WarningType.Danger)
            End If

        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
