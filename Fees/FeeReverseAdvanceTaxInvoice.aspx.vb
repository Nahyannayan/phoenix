﻿Imports System.Data
Imports System.Web.Services
Imports Telerik.Web.UI

Partial Class Fees_FeeReverseAdvanceTaxInvoice
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Property TOTAL_DR() As Double
        Get
            Return ViewState("TOTAL_DR")
        End Get
        Set(ByVal value As Double)
            ViewState("TOTAL_DR") = value
        End Set
    End Property
    Private Property TOTAL_CR() As Double
        Get
            Return ViewState("TOTAL_CR")
        End Get
        Set(ByVal value As Double)
            ViewState("TOTAL_CR") = value
        End Set
    End Property
    Private Property BALANCE() As Double
        Get
            Return ViewState("BALANCE")
        End Get
        Set(ByVal value As Double)
            ViewState("BALANCE") = value
        End Set
    End Property
    Private Property INV_TYPE() As String
        Get
            Return ViewState("INV_TYPE")
        End Get
        Set(ByVal value As String)
            ViewState("INV_TYPE") = value
        End Set
    End Property
    Private Property AIR_FROM() As String
        Get
            Return ViewState("AIR_FROM")
        End Get
        Set(ByVal value As String)
            ViewState("AIR_FROM") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = "add"
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300157" And ViewState("MainMnu_code") <> "F300158") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            BindBusinessUnits()
            LOAD_FEE_TYPE()
        End If

    End Sub
    Private Sub BindBusinessUnits()
        Try
            Dim dsBsu As New DataSet
            Dim SelectedValue As String = "0"
            Select Case ViewState("MainMnu_code")
                Case "F300157" 'Fees
                    dsBsu = clsTaxFunctions.GetBusinessUnits(Session("sUsr_name"), True)
                    SelectedValue = Session("sBsuid")
                    INV_TYPE = "TAXINV"
                    AIR_FROM = "FEES"
                Case "F300158" 'Transport
                    dsBsu = clsTaxFunctions.GetServiceBusinessUnits(Session("sUsr_name"), Session("sBsuid"), True)
                    INV_TYPE = "TRTAXINV"
                    AIR_FROM = "TRANSPORT"
            End Select
            ' clsAddVendor.GetBusinessUnits(Session("sUsr_name"), True)
            If Not dsBsu Is Nothing AndAlso dsBsu.Tables(0).Rows.Count > 0 Then
                ddlBusinessUnit.DataSource = dsBsu
                ddlBusinessUnit.DataTextField = "BSU_NAMEwithShort"
                ddlBusinessUnit.DataValueField = "BSU_ID"
                ddlBusinessUnit.DataBind()
                If ViewState("MainMnu_code") = "F300158" Then 'Transport
                    Dim AllItem As New Telerik.Web.UI.RadComboBoxItem
                    AllItem.Text = "--ALL UNITS--"
                    AllItem.Value = "0"
                    ddlBusinessUnit.Items.Add(AllItem)
                End If
                ddlBusinessUnit.SelectedIndex = -1
                ddlBusinessUnit.SelectedValue = SelectedValue
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("BindBusinessUnits()-" & ex.Message, ViewState("MainMnu_code"))
        End Try
    End Sub
    Private Sub LOAD_FEE_TYPE()
        ddlFeeType.DataSource = clsTaxFunctions.GetFeeType(Session("sBsuid"))
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
        Dim AllItem As New Telerik.Web.UI.RadComboBoxItem
        AllItem.Text = "--ALL FEE TYPES--"
        AllItem.Value = "0"
        ddlFeeType.Items.Add(AllItem)
        ddlFeeType.SelectedValue = 0
    End Sub

    <WebMethod()> _
    Public Shared Function GetStudent(ByVal BSUID As String, ByVal STUTYPE As String, ByVal prefix As String) As String()

        Return clsTaxFunctions.GetStudent(BSUID, STUTYPE, prefix)

    End Function
    Private Sub GridBind()
        gvAdvances.DataSource = ViewState("gvAdvances")
        gvAdvances.DataBind()
        gvSummary.DataSource = ViewState("gvSummary")
        gvSummary.DataBind()
        If gvSummary.Rows.Count > 0 Then
            Dim gvc As GridViewRowCollection = gvSummary.Rows
            If Not gvc Is Nothing Then
                gvc(0).Cells(1).Focus()
            End If
        End If
    End Sub
    Private Sub ClearAll()
        ViewState("gvAdvances") = Nothing
        ViewState("gvSummary") = Nothing
        GridBind()
        h_STU_ID.Value = 0
        txtStudent.Text = ""
        '  lblStudentId.Text = ""
        ' lblStuName.Text = ""
        lblStuGrade.Text = ""
        lblStuCurrstatus.Text = ""
        txtNarration.Text = ""
        trStuDetail.Visible = False
        '   trTCSO.Visible = False
        TOTAL_CR = 0
        TOTAL_DR = 0
        BALANCE = 0
        lblNetAdvBalance.Text = "0.00"
    End Sub
    Protected Sub lbtnLoadStuDetail_Click(sender As Object, e As EventArgs) Handles lbtnLoadStuDetail.Click
        Dim objclsTaxFunctions As New clsTaxFunctions
        objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_BSU_ID = ddlBusinessUnit.SelectedValue
        objclsTaxFunctions.STU_ID = h_STU_ID.Value
        Dim STU_LASTATTDATE As String = ""
        Dim STU_LEAVEDATE As String = ""
        objclsTaxFunctions.FEE_ID = ddlFeeType.SelectedValue
        If objclsTaxFunctions.GET_STUDENT_DETAILS(rblStuType.SelectedValue) Then
            trStuDetail.Visible = True
            txtStudent.Text = objclsTaxFunctions.STU_NO & " - " & objclsTaxFunctions.STU_NAME
            '   lblStudentId.Text = objclsTaxFunctions.STU_NO
            '   lblStuName.Text = objclsTaxFunctions.STU_NAME
            lblStuGrade.Text = objclsTaxFunctions.GRD_DISPLAY
            lblStuCurrstatus.Text = objclsTaxFunctions.STU_CURRSTATUS
            If objclsTaxFunctions.STU_CURRSTATUS <> "EN" And rblStuType.SelectedValue = "S" Then
                If IsDate(objclsTaxFunctions.STU_LEAVEDATE) And IsDate(objclsTaxFunctions.STU_LASTATTDATE) Then
                    'trTCSO.Visible = True
                    'lblStuLastAttdate.Text =
                    'lblStuLeaveDate.Text =
                    If (Format(objclsTaxFunctions.STU_LASTATTDATE, "dd/MMM/yyyy").ToString() <> "01/Jan/0001") Then
                        STU_LASTATTDATE = ", " & Format(objclsTaxFunctions.STU_LASTATTDATE, "dd/MMM/yyyy")
                    End If
                    If (Format(objclsTaxFunctions.STU_LEAVEDATE, "dd/MMM/yyyy").ToString() <> "01/Jan/0001") Then
                        STU_LEAVEDATE = ", " & Format(objclsTaxFunctions.STU_LEAVEDATE, "dd/MMM/yyyy")
                    End If
                    lblStuCurrstatus.Text = lblStuCurrstatus.Text & STU_LASTATTDATE & STU_LEAVEDATE
                End If
            End If
            If AIR_FROM = "FEES" Then
                ViewState("gvAdvances") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
                TOTAL_CR = objclsTaxFunctions.TOTAL_CR
                TOTAL_DR = objclsTaxFunctions.TOTAL_DR
                BALANCE = objclsTaxFunctions.BALANCE
                objclsTaxFunctions.bSUMMARY = True
                ViewState("gvSummary") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
            ElseIf AIR_FROM = "TRANSPORT" Then
                ViewState("gvAdvances") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT_TRANSPORT(rblStuType.SelectedValue)
                TOTAL_CR = objclsTaxFunctions.TOTAL_CR
                TOTAL_DR = objclsTaxFunctions.TOTAL_DR
                BALANCE = objclsTaxFunctions.BALANCE
                objclsTaxFunctions.bSUMMARY = True
                ViewState("gvSummary") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT_TRANSPORT(rblStuType.SelectedValue)
            End If
            Dim QueryString = "&STUID=" & Encr_decrData.Encrypt(h_STU_ID.Value) & "&SBSU=" & Encr_decrData.Encrypt(ddlBusinessUnit.SelectedValue) & "&STYPE=" & Encr_decrData.Encrypt(rblStuType.SelectedValue) & ""
            'lBtnLedger.Attributes.Add("onClick", "return ShowWindowWithClose('#','STUDENT LEDGER','60%','85%');")
            lBtnLedger.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("STULEDGER") & QueryString & "', 'STUDENT LEDGER', '60%', '75%');")
            GridBind()
        Else
            trStuDetail.Visible = False
            txtStudent.Text = ""
            ' lblStudentId.Text = ""
            ' lblStuName.Text = ""
            lblStuGrade.Text = ""
            lblStuCurrstatus.Text = ""
            TOTAL_CR = 0
            TOTAL_DR = 0
            BALANCE = 0
            lblNetAdvBalance.Text = Format(BALANCE, "#,##0.00")
            '  trTCSO.Visible = False
            ViewState("gvAdvances") = Nothing
            ViewState("gvSummary") = Nothing
            GridBind()
        End If
        txtNarration.Focus()
    End Sub

    Protected Sub gvAdvances_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvAdvances.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbtnInvNo As LinkButton = e.Row.FindControl("lbtnInvNo")
            Dim hf_FSH_ID As HiddenField = e.Row.FindControl("hf_FSH_ID")
            If Not lbtnInvNo Is Nothing AndAlso Not hf_FSH_ID Is Nothing Then
                Dim QueryString = "&ID=" & Encr_decrData.Encrypt(hf_FSH_ID.Value) & "&STUID=" & Encr_decrData.Encrypt(h_STU_ID.Value) & "&INVNO=" & Encr_decrData.Encrypt(lbtnInvNo.Text) & "&SBSU=" & Encr_decrData.Encrypt(ddlBusinessUnit.SelectedValue) & ""
                lbtnInvNo.Attributes.Add("onClick", "return ShowWindowWithClose('FeeChooseTaxInvoicee.aspx?TYPE=" & Encr_decrData.Encrypt(INV_TYPE) & QueryString & "','INVOICE','55%','85%');")
            End If
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTotalDebit As Label = e.Row.FindControl("lblTotalDebit")
            Dim lblTotalCredit As Label = e.Row.FindControl("lblTotalCredit")
            If Not lblTotalCredit Is Nothing AndAlso Not lblTotalDebit Is Nothing Then
                lblTotalCredit.Text = Format(TOTAL_CR, "#,##0.00")
                lblTotalDebit.Text = Format(TOTAL_DR, "#,##0.00")
                lblNetAdvBalance.Text = Format(BALANCE, "#,##0.00")
            End If
        End If
    End Sub

    Protected Sub lbtnClearStudent_Click(sender As Object, e As EventArgs) Handles lbtnClearStudent.Click
        ClearAll()
    End Sub

    Protected Sub imgStudent_Click(sender As Object, e As ImageClickEventArgs) Handles imgStudent.Click
        lbtnLoadStuDetail_Click(Nothing, Nothing)
    End Sub

    Protected Sub rblStuType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblStuType.SelectedIndexChanged
        ClearAll()
    End Sub

    Protected Sub ddlBusinessUnit_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBusinessUnit.SelectedIndexChanged
        ClearAll()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ClearAll()
    End Sub

    Protected Sub ddlFeeType_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlFeeType.SelectedIndexChanged
        Dim objclsTaxFunctions As New clsTaxFunctions
        objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_BSU_ID = ddlBusinessUnit.SelectedValue
        objclsTaxFunctions.STU_ID = h_STU_ID.Value
        objclsTaxFunctions.FEE_ID = ddlFeeType.SelectedValue
        objclsTaxFunctions.bSUMMARY = False
        ViewState("gvAdvances") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
        TOTAL_CR = objclsTaxFunctions.TOTAL_CR
        TOTAL_DR = objclsTaxFunctions.TOTAL_DR
        BALANCE = objclsTaxFunctions.BALANCE
        objclsTaxFunctions.bSUMMARY = True
        ViewState("gvSummary") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
        GridBind()
    End Sub

    Protected Sub rBtnSelectFee_CheckedChanged(sender As Object, e As EventArgs)

    End Sub

    Protected Sub gvSummary_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSummary.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblBalance As Label = e.Row.FindControl("lblBalance")
            Dim txtReverseAmt As TextBox = e.Row.FindControl("txtReverseAmt")
            Dim chkReverse As CheckBox = e.Row.FindControl("chkReverse")
            If Not lblBalance Is Nothing AndAlso IsNumeric(lblBalance.Text) AndAlso CDbl(lblBalance.Text) > 0 Then
                If Not txtReverseAmt Is Nothing Then
                    txtReverseAmt.Text = "0.00"
                    txtReverseAmt.Enabled = True
                End If
                If Not chkReverse Is Nothing Then
                    chkReverse.Enabled = True
                End If
            Else
                If Not txtReverseAmt Is Nothing Then
                    txtReverseAmt.Text = "0.00"
                    txtReverseAmt.Enabled = False
                End If
                If Not chkReverse Is Nothing Then
                    chkReverse.Checked = False
                    chkReverse.Enabled = False
                End If
            End If
            chkReverse.Focus()
            txtReverseAmt.Attributes.Add("disabled", "disabled")
            chkReverse.Attributes.Add("onChange", "return EnableReversal('" & chkReverse.ClientID & "','" & txtReverseAmt.ClientID & "');")
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        ' lblError.Text = ""
        If gvSummary.Rows.Count > 0 Then
            Dim TOTAL_REVERSAL As Double = 0, Message As String = "", RetVal As String = "0"
            Dim objclsTaxFunctions As New clsTaxFunctions
            objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
            objclsTaxFunctions.STU_BSU_ID = ddlBusinessUnit.SelectedValue
            objclsTaxFunctions.STU_ID = h_STU_ID.Value
            objclsTaxFunctions.STU_TYPE = rblStuType.SelectedValue
            objclsTaxFunctions.AIR_FROM = AIR_FROM
            objclsTaxFunctions.AIR_NARRATION = txtNarration.Text.Trim
            objclsTaxFunctions.AIR_DATE = Format(DateTime.Now, OASISConstants.DataBaseDateFormat)
            objclsTaxFunctions.User = Session("sUsr_name")
            For Each gvr As GridViewRow In gvSummary.Rows
                Dim lblBalance As Label = gvr.FindControl("lblBalance")
                Dim txtReverseAmt As TextBox = gvr.FindControl("txtReverseAmt")
                Dim chkReverse As CheckBox = gvr.FindControl("chkReverse")
                Dim FEE_ID As Integer = gvSummary.DataKeys(gvr.RowIndex).Value
                Dim FEE_DESCR As String = gvr.Cells(1).Text.Trim
                Message = ""
                If chkReverse.Checked Then
                    If RetVal = 0 Then
                        If IsNumeric(txtReverseAmt.Text) AndAlso CDbl(txtReverseAmt.Text) > 0 AndAlso CDbl(lblBalance.Text) > 0 Then
                            If CDbl(txtReverseAmt.Text) > CDbl(lblBalance.Text) Then
                                '     lblError.Text = "The maximum reversible amount for " & FEE_DESCR & " is " & lblBalance.Text
                                usrMessageBar.ShowNotification("The maximum reversible amount for " & FEE_DESCR & " is " & lblBalance.Text, UserControls_usrMessageBar.WarningType.Danger)
                                txtReverseAmt.Focus()
                                Exit Sub
                            Else
                                objclsTaxFunctions.FEE_ID = FEE_ID
                                objclsTaxFunctions.AIR_AMOUNT = CDbl(txtReverseAmt.Text)
                                RetVal = objclsTaxFunctions.SAVE_ADV_TAX_REVERSAL(Message)
                                If RetVal = 0 Then
                                    'Me.lblError.Text = "Fee Advance tax invoice has been reversed successfully."
                                    usrMessageBar.ShowNotification("Fee Advance tax invoice has been reversed successfully.", UserControls_usrMessageBar.WarningType.Success)
                                    txtNarration.Text = ""
                                    TOTAL_CR = 0
                                    TOTAL_DR = 0
                                    BALANCE = 0
                                    lblNetAdvBalance.Text = Format(BALANCE, "#,##0.00")
                                    objclsTaxFunctions.FEE_ID = 0
                                    objclsTaxFunctions.bSUMMARY = False
                                    If AIR_FROM = "FEES" Then
                                        ViewState("gvAdvances") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
                                        TOTAL_CR = objclsTaxFunctions.TOTAL_CR
                                        TOTAL_DR = objclsTaxFunctions.TOTAL_DR
                                        BALANCE = objclsTaxFunctions.BALANCE
                                        objclsTaxFunctions.bSUMMARY = True
                                        ViewState("gvSummary") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
                                    ElseIf AIR_FROM = "TRANSPORT" Then
                                        ViewState("gvAdvances") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT_TRANSPORT(rblStuType.SelectedValue)
                                        TOTAL_CR = objclsTaxFunctions.TOTAL_CR
                                        TOTAL_DR = objclsTaxFunctions.TOTAL_DR
                                        BALANCE = objclsTaxFunctions.BALANCE
                                        objclsTaxFunctions.bSUMMARY = True
                                        ViewState("gvSummary") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT_TRANSPORT(rblStuType.SelectedValue)
                                    End If
                                    GridBind()
                                Else
                                    If Message <> "" Then
                                        '  lblError.Text = Message
                                        usrMessageBar.ShowNotification(Message, UserControls_usrMessageBar.WarningType.Danger)
                                    Else
                                        '  lblError.Text = UtilityObj.getErrorMessage(RetVal)
                                        usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(RetVal), UserControls_usrMessageBar.WarningType.Danger)
                                    End If
                                    Exit Sub
                            End If
                            TOTAL_REVERSAL = TOTAL_REVERSAL + CDbl(txtReverseAmt.Text)
                                End If
                        Else
                            ' lblError.Text = "The reversal amount should be greater than zero for " & FEE_DESCR
                            usrMessageBar.ShowNotification("The reversal amount should be greater than zero for " & FEE_DESCR, UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    End If
                End If
            Next
            If TOTAL_REVERSAL = 0 Then
                '   lblError.Text = "There is no eligible Invoice(s) to do a reversal."
                usrMessageBar.ShowNotification("There is no eligible Invoice(s) to do a reversal.", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Else
            '    Me.lblError.Text = "There is no invoices to reverse."
            usrMessageBar.ShowNotification("There is no invoices to reverse.", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    'Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
    '    Dim objclsTaxFunctions As New clsTaxFunctions
    '    objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
    '    objclsTaxFunctions.STU_BSU_ID = ddlBusinessUnit.SelectedValue
    '    objclsTaxFunctions.STU_ID = h_STU_ID.Value
    '    objclsTaxFunctions.FEE_ID = ddlFeeType.SelectedValue
    '    objclsTaxFunctions.bSUMMARY = False
    '    If AIR_FROM = "FEES" Then
    '        ViewState("gvAdvances") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
    '        TOTAL_CR = objclsTaxFunctions.TOTAL_CR
    '        TOTAL_DR = objclsTaxFunctions.TOTAL_DR
    '        BALANCE = objclsTaxFunctions.BALANCE
    '        objclsTaxFunctions.bSUMMARY = True
    '        ViewState("gvSummary") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
    '    ElseIf AIR_FROM = "TRANSPORT" Then
    '        ViewState("gvAdvances") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT_TRANSPORT(rblStuType.SelectedValue)
    '        TOTAL_CR = objclsTaxFunctions.TOTAL_CR
    '        TOTAL_DR = objclsTaxFunctions.TOTAL_DR
    '        BALANCE = objclsTaxFunctions.BALANCE
    '        objclsTaxFunctions.bSUMMARY = True
    '        ViewState("gvSummary") = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT_TRANSPORT(rblStuType.SelectedValue)
    '    End If

    '    GridBind()
    'End Sub

    Protected Sub txtStudent_TextChanged(sender As Object, e As EventArgs)
        lbtnLoadStuDetail_Click(Nothing, Nothing)
    End Sub
End Class
