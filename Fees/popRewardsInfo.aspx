﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="popRewardsInfo.aspx.vb" Inherits="Fees_popRewardsInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
    <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet">
    <script type="text/javascript">
        function CheckValues() {
            var hf_Success = document.getElementById('<%= h_Success.ClientID%>');
            var hf_Module = document.getElementById('<%= h_Module.ClientID%>');
            if (hf_Success.value == '1') {
                hf_Success.value = '';
                if (hf_Module.value == "SCHOOL")
                    parent.setRewardsValue($("#<%=txtAmounttoRedeem.ClientID%>").val());
                else
                    parent.setTptRewardsValue($("#<%=txtAmounttoRedeem.ClientID%>").val());
                return false;
            }
        }
    </script>
</head>
<body onload="CheckValues();">
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="2">
                        <div>
                            <pre id="preError" runat="server" class="error"></pre>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="matters"><span class="field-label">Registered Info</span>
                    </td>
                    <td class="matters">
                        <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="matters"><span class="field-label">Points Summary</span></td>
                    <td class="matters">
                        <table style="width: 100%; padding: 5px;">
                            <tr class="title-bg">
                                <td style="padding: 5px; margin: 5px; text-align: center;">Available Points</td>
                                <td style="padding: 5px; margin: 5px; text-align: center;">Redeemable Points</td>
                                <td style="padding: 5px; margin: 5px; text-align: center;">Redeemable Amount <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(" & Session("BSU_CURRENCY") & ")")%></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; margin: 5px; text-align: center;">
                                    <asp:Label ID="lblAvailablePoints" runat="server" Text="0"></asp:Label></td>
                                <td style="padding: 5px; margin: 5px; text-align: center;">
                                    <asp:Label ID="lblRedeemablePoints" runat="server" Text="0"></asp:Label></td>
                                <td style="padding: 5px; margin: 5px; text-align: center;">
                                    <asp:Label ID="lblRedeemableAmt" runat="server" Text="0.00"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="matters"><span class="field-label">Amount</span></td>
                    <td>
                        <asp:TextBox ID="txtAmounttoRedeem" runat="server" Style="text-align: right;" Text="0.00"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">

                        <ul class="alert alert-info">
                            <%--<li>Minimum of AED 200 worth points required to redeem. </li>--%>
                            <li>Receipt cannot be cancelled once the points are redeemed.</li>
                            <%--<li>Amount redeemed should be in multiples of AED 10.</li>--%>
                        </ul>

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="chkAgree" runat="server" Text="I agree to the above terms and condition" CssClass="field-label" OnCheckedChanged="chkAgree_CheckedChanged" AutoPostBack="true" />
                    </td>
                </tr>
                <tr id="trapply" runat="server" visible="false">
                    <td colspan="2" align="center">
                        <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="button" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="h_Success" runat="server" />
        <asp:HiddenField ID="h_Module" runat="server" />
    </form>
</body>
</html>
