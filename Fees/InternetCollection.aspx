<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="InternetCollection.aspx.vb" Inherits="Fees_InternetCollection" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                document.getElementById('<%= h_print.ClientID %>').value = '';
                Popup('../Reports/ASPX Report/RptViewerModal.aspx');
            }
        }
    );
        function getFile() {
            var filepath = document.getElementById('<%=uploadFile.ClientID %>').value;
            document.getElementById('<%=HidUpload.ClientID %>').value = filepath;
        }
       <%-- function getBank() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "", sFeatures);
       if (result == '' || result == undefined)
       { return false; }
       lstrVal = result.split('||');
       document.getElementById('<%=txtBankCode.ClientID %>').value = lstrVal[0];
        document.getElementById('<%=txtBankDescr.ClientID %>').value = lstrVal[1];
   }--%>

    </script>
    <script>
                function getBank() {
       var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "pop_getBank");
       
                }
         function OnClientClose1(oWnd, args) {

            
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
            }
         }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getBank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Internet Fee Collection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_NextLine" runat="server" type="hidden" /></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">File Name</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:FileUpload ID="uploadFile" runat="server" EnableTheming="True"></asp:FileUpload></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Bank Account Code</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getBank();return false;" /></td>
                                    <td align="left" class="matters"><span class="field-label">Bank Account Name</span></td>
                                    <td>
                                        <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="False" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle" />&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtHDocdate"
                                            ErrorMessage="Invalid Date" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td align="left" class="matters" style="cursor: hand">
                                        <asp:RadioButton ID="RdDbf" runat="server" Checked="True" GroupName="Rd1" Text="From DBF" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="RdExcel" runat="server" GroupName="Rd1" Text="From Excel" CssClass="field-label"></asp:RadioButton></td>
                                    <td align="left" class="matters" style="cursor: hand">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload File" CausesValidation="False"
                                            TabIndex="30" OnClick="btnFind_Click" OnClientClick="getFile()" />&nbsp;<asp:RadioButton ID="RdNotprocessing" runat="server" OnCheckedChanged="RdNotprocessing_CheckedChanged"
                                                Text="View Unprocessed Records" CssClass="field-label" AutoPostBack="True"></asp:RadioButton></td>
                                </tr>
                            </table>
                            <br />
                            <table align="center" width="100%">
                                <tr class="title-bg">
                                    <td valign="middle" align="left">
                                        <asp:Label ID="labdetailHead" runat="server" EnableViewState="False">Details</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="matters">
                                        <asp:GridView ID="gvExcel" runat="server" Width="100%" EmptyDataText="No Accounts are Listing"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-row table-bordered" OnRowDataBound="gvExcel_RowDataBound" ShowFooter="True">
                                            <FooterStyle />
                                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True"></EmptyDataRowStyle>
                                            <Columns>
                                                <asp:BoundField ReadOnly="True" DataField="STU_NAME" SortExpression="STU_NAME" HeaderText="Student Name">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STU_NO" HeaderText="Student No"></asp:BoundField>
                                                <asp:BoundField HtmlEncode="False" DataFormatString="{0:dd/MMM/yyyy}" DataField="FCT_DATE" SortExpression="FCT_DATE" HeaderText="Date">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description"></asp:BoundField>
                                                <asp:BoundField DataField="FCT_PAID" HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FCT_ID" HeaderText="FctId"></asp:BoundField>
                                            </Columns>
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <SelectedRowStyle BackColor="Aqua"></SelectedRowStyle>
                                            <HeaderStyle CssClass="gridheader_new"></HeaderStyle>
                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                            <br />

                            <table id="Table1" width="100%" class="table table-bordered">
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center" class="matters" colspan="9">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="Details" CausesValidation="False" TabIndex="24" OnClick="btnAdd_Click" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="26" />
                                        <asp:CheckBox ID="ChkPrint" runat="server" Text="Print Voucher"></asp:CheckBox><br />
                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalendar" TargetControlID="txtHDocdate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Editid" runat="server" Value="0" />
                <asp:HiddenField ID="HidUpload" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

