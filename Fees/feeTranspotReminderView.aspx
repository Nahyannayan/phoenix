<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeTranspotReminderView.aspx.vb" Inherits="Fees_feeTranspotReminder"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        Sys.Application.add_load(
       function CheckForPrint() {
           if (document.getElementById('<%= h_print.ClientID %>').value != '') {

               document.getElementById('<%= h_print.ClientID %>').value = '';
               //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
               return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
               return false;
               
           }
       }
    );


    </script>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Transport Fee Reminder View..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%-- <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                    DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" OnSelectedIndexChanged="ddlBSUnit_SelectedIndexChanged">
                </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" valign="top">
                            <asp:GridView ID="gvFEEConcessionDet" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFRH_ID" runat="server" Text='<%# Bind("FRH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACAD. YEAR">
                                        <HeaderTemplate>
                                            Acad. Year #
                                                    <br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACY_DESCR" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AS ON DATE">
                                        <HeaderTemplate>
                                            As On Date
                                                   <br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAsOnDate" runat="server" Text='<%# Bind("FRH_ASONDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LEVEL">
                                        <HeaderTemplate>
                                            Level
                                                    <br />
                                            <asp:TextBox ID="txtConcession" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnConcession" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("FRH_Level") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date
                                                    <br />
                                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FRH_DT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks
                                                   <br />
                                            <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FRH_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkView" runat="server">View </asp:HyperLink>|&nbsp;<asp:LinkButton
                                                ID="lnkPrint" OnClick="lnkPrint_Click" runat="server">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>



                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server"
                        type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                            value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sUsr_name" Type="String" DefaultValue="" Name="USR_ID"></asp:SessionParameter>
                        <asp:SessionParameter SessionField="sBsuid" Type="String" DefaultValue="" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>
