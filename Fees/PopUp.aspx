<%@ Page Language="VB" AutoEventWireup="true" CodeFile="PopUp.aspx.vb" Inherits="PopUp" Theme="General" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server"> 
<base target="_self" />

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>

</head>


<body onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
    

 <table width="100%" align="center">
                <tr>
                    <td>                     
                        <input id="h_SelectedId" runat="server" type="hidden" value="-1" /> 
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvGroup" PageSize="15" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"  Width="100%" EmptyDataText="No Data" AllowPaging="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Account Code" SortExpression="Code">
                                    <HeaderTemplate>
                                        Account Code<br />
                                        <asp:TextBox ID="txtCode" runat="server" Width="75%"></asp:TextBox>
                                        <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Account Name" SortExpression="Description">
                                    <HeaderTemplate>
                                        Account Name<br />
                                        <asp:TextBox ID="txtName" runat="server" Width="75%"></asp:TextBox>
                                        <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("Description") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView> 
                    </td> 
                </tr>
            </table>
 
</form>
</body>
</html>
