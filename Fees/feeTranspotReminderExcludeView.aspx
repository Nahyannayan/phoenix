<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTranspotReminderExcludeView.aspx.vb" Inherits="Fees_feeTranspotReminderExcludeView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Transport Fee Reminder Exclude View...
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Business Unit</span>
                        </td>
                        <td align="center" width="30%">
                            <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" OnSelectedIndexChanged="ddlBSUnit_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="center" width="20%"></td>
                        <td align="center" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" valign="top">
                            <asp:GridView ID="gvFEEConcessionDet" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student No.">
                                        <HeaderTemplate>
                                                 STUDENT NO.&nbsp;#<br />
                                                            <asp:TextBox ID="txtStuno" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w39"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearchno" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w40"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>' __designer:wfdid="w57"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            STUDENT NAME<br />
                                            <asp:TextBox ID="txtStuname" runat="server" SkinID="Gridtxt" Width="100px" __designer:wfdid="w42"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w43"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTUDENTNAME" runat="server" Text='<%# Bind("STU_NAME") %>' __designer:wfdid="w41"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <HeaderTemplate>
                                            FROM DATE<br />
                                                                        <asp:TextBox ID="txtFromDate" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w45"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnConcession" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w46"></asp:ImageButton>
                                                             
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFROMDATE" runat="server" Text='<%# Bind("FRE_FROMDT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w44"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <HeaderTemplate>
                                            TO DATE<br />
                                            <asp:TextBox ID="txtToDate" runat="server" __designer:wfdid="w48"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w49"></asp:ImageButton>
                                                                
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTODATE" runat="server" Text='<%# Bind("FRE_TODT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w47"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sUsr_name" Type="String" DefaultValue="" Name="USR_ID"></asp:SessionParameter>
                        <asp:SessionParameter SessionField="sBsuid" Type="String" DefaultValue="" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>

</asp:Content>

