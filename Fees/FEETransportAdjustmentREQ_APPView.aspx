<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FEETransportAdjustmentREQ_APPView.aspx.vb" Inherits="FEETransportAdjustmentREQ_APPView"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
       function CheckForPrint() {
           if (document.getElementById('<%= h_print.ClientID %>').value != '') {
               document.getElementById('<%= h_print.ClientID %>').value = '';
               showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
           }
       }
    );
       function divIMG(pId, val, ctrl1, pImg) {
           var path;

           if (val == 'LI') {
               path = '../Images/operations/like.gif';
           } else if (val == 'NLI') {
               path = '../Images/operations/notlike.gif';
           } else if (val == 'SW') {
               path = '../Images/operations/startswith.gif';
           } else if (val == 'NSW') {
               path = '../Images/operations/notstartwith.gif';
           } else if (val == 'EW') {
               path = '../Images/operations/endswith.gif';
           } else if (val == 'NEW') {
               path = '../Images/operations/notendswith.gif';
           }

           if (pId == 1) {
               document.getElementById("<%=getid("mnu_1_img") %>").src = path;
           }
           else if (pId == 2) {
               document.getElementById("<%=getid("mnu_2_img") %>").src = path;
           }
           else if (pId == 3) {
               document.getElementById("<%=getid("mnu_3_img") %>").src = path;
           }
           else if (pId == 4) {
               document.getElementById("<%=getid("mnu_4_img") %>").src = path;
           }
           else if (pId == 5) {
               document.getElementById("<%=getid("mnu_5_img") %>").src = path;
           }
           else if (pId == 6) {
               document.getElementById("<%=getid("mnu_6_img") %>").src = path;
           }
           else if (pId == 7) {
               document.getElementById("<%=getid("mnu_7_img") %>").src = path;
           }

    document.getElementById(ctrl1).value = val + '__' + path;
}
    </script>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_print" runat="server" type="hidden" />
                        </td>
                    </tr>
                </table>
                <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" TabIndex="5" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                        <td align="left" colspan="2">
                            <asp:RadioButton ID="radRequested" runat="server" AutoPostBack="True" Checked="True" CssClass="field-label"
                                Text="Requested" GroupName="STU_APROVE" CausesValidation="True" />
                            <asp:RadioButton ID="radApproved" runat="server" AutoPostBack="True" Text="Approved" CssClass="field-label"
                                GroupName="STU_APROVE" />
                            <asp:RadioButton ID="radReject" runat="server" Text="Reject" AutoPostBack="True" CssClass="field-label"
                                GroupName="STU_APROVE" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" colspan="2">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="True" Checked="True" Text="Student" CssClass="field-label"
                                GroupName="STU_TYPE" />
                            <asp:RadioButton ID="radEnq" runat="server" Text="Enquiry" AutoPostBack="True" GroupName="STU_TYPE" CssClass="field-label"
                                Enabled="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFEEAdjustments" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText="FCH_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAR_ID" runat="server" Text='<%# Bind("FAR_ID") %>'></asp:Label>
                                            <asp:Label ID="lblFAH_ID" runat="server" Text='<%# Bind("FAH_ID") %>'></asp:Label>
                                            <asp:HiddenField ID="hf_STU_BSU_ID" Value='<%# Bind("FAR_STU_BSU_ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSU" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_NO">
                                        <HeaderTemplate>
                                            Student No
                                            <br />
                                            <asp:TextBox ID="txtStudentNo" runat="server" Width="80%"></asp:TextBox>

                                            <asp:ImageButton ID="btnAcademicYearSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtstudname" runat="server" Width="80%"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudnameSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date                                                       
                                            <br />
                                            <asp:TextBox ID="txtDate" runat="server" Width="80%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FAR_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks
                                                        <br />
                                            <asp:TextBox ID="txtRemarks" runat="server" Width="80%"></asp:TextBox>
                                            <asp:ImageButton ID="btnRemarksSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FAR_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doc No" SortExpression="FAH_DOCNO">
                                        <HeaderTemplate>
                                            Doc No
                                                        <br />
                                            <asp:TextBox ID="txtDocno" runat="server" Width="80%"></asp:TextBox>

                                            <asp:ImageButton ID="btndocnoSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FAH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <HeaderTemplate>
                                            Sl.No.
                                                       <br />
                                            <asp:TextBox ID="txtSlno" runat="server" Width="80%"></asp:TextBox>

                                            <asp:ImageButton ID="btnSlnoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FAR_BSU_SLNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print" Visible="False">
                                        <FooterTemplate>
                                            Print
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnPrint" OnClick="btnPrint_Click" runat="server">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>


                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>



                <p></p>
            </div>
        </div>
    </div>

</asp:Content>
