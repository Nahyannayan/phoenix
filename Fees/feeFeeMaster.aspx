<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeFeeMaster.aspx.vb" Inherits="Fees_feeFeeMaster" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">



        function popUpAccount(pWidth, pHeight, pMode, ctrl) {

            var sFeatures;
            var lstrVal;
            var lintScrVal;

            sFeatures = "dialogWidth: " + pWidth + "px; ";
            sFeatures += "dialogHeight: " + pHeight + "px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            if (pMode == 'ALLACCOUNTS' || pMode == 'INCOME' || pMode == 'CHARGE' || pMode == 'AXINCOME' || pMode == 'AXCHARGE' || pMode == 'CONCESSION' || pMode == 'FEEDISC') {

                //result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=", "", sFeatures);

                //if (result == '' || result == undefined)
                //{ return false; }
                //lstrVal = result.split('||');
                //document.getElementById(ctrl).value = lstrVal[0];

                document.getElementById('<%=hf_CTRL_VAL.ClientID%>').value = ctrl;
                var url = "PopUp.aspx?ShowType=" + pMode + "&codeorname=";
                var oWnd = radopen(url, "pop_upAccount");

            }
        }

        function gettxtFEE_INC_ACT_ID() {
            
            popUpAccount('460', '400', 'ALLACCOUNTS', '<%=txtFEE_INC_ACT_ID.ClientId %>')
        }
        function gettxtFEE_CHRG_INADV() {
            popUpAccount('460', '400', 'ALLACCOUNTS', '<%=txtFEE_CHRG_INADV.ClientId %>')//CHARGE
        }

        function gettxtFEE_AX_INC_ACT_ID() {
            popUpAccount('460', '400', 'ALLACCOUNTS', '<%=txtFEE_AX_INC_ACT_ID.ClientID%>')//INCOME
        }
        function gettxtFEE_AX_CHRG_INADV() {
            popUpAccount('460', '400', 'ALLACCOUNTS', '<%=txtFEE_AX_CHRG_INADV.ClientID%>')//CHARGE
        }
        function gettxtFEE_CONCESSION_ACT_ID() {
            popUpAccount('460', '400', 'CONCESSION', '<%=txtFEE_CONCESSION_ACT_ID.ClientId %>')
        }
        function gettxtFEE_DISC_ACT_ID() {
            popUpAccount('460', '400', 'FEEDISC', '<%=txtFEE_DISC_ACT_ID.ClientId %>')
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                var ctrl_id = document.getElementById('<%=hf_CTRL_VAL.ClientID%>').value;
                NameandCode = arg.NameandCode.split('||');
                document.getElementById(ctrl_id).value = NameandCode[0];

            }
        }



        function Showdata(mode) {
            var sFeatures, url;
            sFeatures = "dialogWidth:350px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                url = "../common/PopupShowData.aspx?id=FEEPRINT";
            else if (mode == 2)
                url = "../common/PopupShowData.aspx?id=FEESETTLE";

            //result = window.showModalDialog(url,"", sFeatures); 
            //return false;

            var oWnd = radopen(url, "pop_showdata");
        }


        function GetLinkToStage() {
            var sFeatures, url;
            sFeatures = "dialogWidth:350px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
        <%-- var result;
        result = window.showModalDialog("../common/PopupForm.aspx?id=LINKTOSTAGE&multiSelect=false","", sFeatures); 
        if (result=='' || result==undefined)
        {    return false;      } 
         NameandCode=result.split('___');     
         document.getElementById('<%=txtLinkToStage.ClientID %>').value=NameandCode[1];
         document.getElementById('<%=hf_PRO_ID.ClientID %>').value=NameandCode[0];
        return false;
    }      --%>
            var url = "../common/PopupForm.aspx?id=LINKTOSTAGE&multiSelect=false";
            var oWnd = radopen(url, "pop_linktostage");

        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtLinkToStage.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=hf_PRO_ID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%=txtLinkToStage.ClientID%>', 'TextChanged');
            }
        }



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_showdata" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_linktostage" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_upAccount" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Fee Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left" width="100%">
                            <%--<asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="main" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Fee Group<span class="text-danger">*</span></span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddFeeGroup" runat="server" DataSourceID="SqlFeeGroup" DataTextField="FGP_DESCR" DataValueField="FGP_ID">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">Event Type</span></td>
                        <td>
                            <asp:DropDownList ID="ddEventType" runat="server" DataSourceID="SqlEventType" DataTextField="DIM_DESC" DataValueField="DIM_CODE">
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Fee Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddFeeType" runat="server" DataSourceID="odsGetFEETYPE_M" DataTextField="FTP_DESCR" DataValueField="FTP_ID">
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkFixed" runat="server" Text="Fixed Fee" Checked="True" Visible="False" /></td>
                        <td align="left" width="20%">
                            <span class="field-label">Fee Setup</span></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkSetupbygrade" runat="server" Text="By Grade" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Description<span class="text-danger">*</span></span></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtRemarks"
                                CssClass="error" ErrorMessage="Please Enter Description" ValidationGroup="main">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" valign="top">
                            <asp:CheckBox ID="chkRevenueMonthly" runat="server" Text="Recognize revenue in the same month" CssClass="field-label" Visible="False" /><br />
                            <asp:CheckBox ID="chkConcession" runat="server" CssClass="field-label" Text="Allow Concession"></asp:CheckBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left"><span class="field-label">Charge Schedule</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddChargeschedule" runat="server" DataSourceID="odsGetSCHEDULE_M_CHARGE" DataTextField="SCH_DESCR" DataValueField="SCH_ID">
                            </asp:DropDownList>
                        </td>
                        <td align="left"><span class="field-label">Collection Schedule</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddCollectionSchedule" runat="server" DataSourceID="odsGetSCHEDULE_M_COLLN" DataTextField="SCH_DESCR" DataValueField="SCH_ID">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left">
                            <span class="field-label">Link to Service </span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddServicelink" runat="server" DataSourceID="odsGetSERVICES_SYS_M" DataTextField="SVC_DESCRIPTION" DataValueField="SVC_ID">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Collection Type</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddCollectiontype" runat="server" DataSourceID="SqlCollection" DataTextField="COL_DESCR" DataValueField="COL_ID">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Link To Stage</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLinkToStage" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetLinkToStage();return false;" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Late Fee Charge Type</span></td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="rbPercentage" runat="server" GroupName="type" CssClass="field-label" Text="Percentage" />
                            <asp:RadioButton ID="rbAmount" runat="server" GroupName="type" Text="Amount" CssClass="field-label" Checked="true" /></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Late Fee Amount/Percentage</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLateAmount" runat="server" AutoCompleteType="Disabled"></asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Fee Late Days</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLatefeedays" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Show Fee in Other Collection</span></td>
                        <td align="left">
                            <asp:CheckBox ID="chk_ShowFeeHead" runat="server" Text="" CssClass="field-label" /></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" class="title-bg">Others</td>
                    </tr>
                    <tr style="display: none">
                        <td align="left">
                            <span class="field-label">Pro rata Charging</span></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkJoin" runat="server" CssClass="field-label" Text="Join" />
                            <asp:CheckBox ID="chkDiscontinue" runat="server" CssClass="field-label" Text="Discontinue" /></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Print Order<span class="text-danger">*</span></span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFeeorder" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <asp:LinkButton ID="lbViewPrintorder" runat="server" OnClientClick="Showdata(1); return false;">View</asp:LinkButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFeeorder"
                                CssClass="error" ErrorMessage="Please Enter Fee Order" ValidationGroup="main">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left">
                            <span class="field-label">Settlement Order<span class="text-danger">*</span></span></td>
                        <td align="left">
                            <asp:TextBox ID="txtSettlementorder" runat="server" Width="50px" AutoCompleteType="Disabled"></asp:TextBox>
                            <asp:LinkButton ID="lbViewSettleorder" runat="server" OnClientClick="Showdata(2); return false;">View</asp:LinkButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSettlementorder"
                                CssClass="error" ErrorMessage="Please Enter Settlement order" ValidationGroup="main">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" class="title-bg">Link to Accounts</td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Income Account<span class="text-danger">*</span></span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFEE_INC_ACT_ID" runat="server" autocomplete="off" AutoPostBack="true"></asp:TextBox>
                            <asp:ImageButton ID="img1" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="gettxtFEE_INC_ACT_ID();return false;" />
                            &nbsp;
             <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtFEE_INC_ACT_ID"
                 CssClass="error" ErrorMessage="Please Enter income account" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Advance Account<span class="text-danger">*</span></span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFEE_CHRG_INADV" runat="server" autocomplete="off" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="img2" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="gettxtFEE_CHRG_INADV();return false;" />&nbsp;
             <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtFEE_CHRG_INADV"
                 CssClass="error" ErrorMessage="Please Enter Advance account" ValidationGroup="main">*</asp:RequiredFieldValidator>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td align="left">
                            <span class="field-label">Income Account (AX)<span class="text-danger">*</span></span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFEE_AX_INC_ACT_ID" runat="server" autocomplete="off" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="gettxtFEE_AX_INC_ACT_ID();return false;" />
                            &nbsp;
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFEE_AX_INC_ACT_ID"
                 CssClass="error" ErrorMessage="Please Enter AX income account" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Advance Account (AX)<span class="text-danger">*</span></span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFEE_AX_CHRG_INADV" runat="server" autocomplete="off" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="gettxtFEE_AX_CHRG_INADV();return false;" />&nbsp;
             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFEE_AX_CHRG_INADV"
                 CssClass="error" ErrorMessage="Please Enter AX Advance account" ValidationGroup="main">*</asp:RequiredFieldValidator>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr style="display: none">
                        <td align="left">
                            <span class="field-label">Concession</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFEE_CONCESSION_ACT_ID" runat="server" autocomplete="off" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="img3" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="gettxtFEE_CONCESSION_ACT_ID();return false;" />&nbsp;</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr style="display: none">
                        <td align="left">
                            <span class="field-label">Discount</span> </td>
                        <td align="left">
                            <asp:TextBox ID="txtFEE_DISC_ACT_ID" runat="server" autocomplete="off" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="img4" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="gettxtFEE_DISC_ACT_ID();return false;" />&nbsp;</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" UseSubmitBehavior="False" ValidationGroup="main" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="javascript:alert('You do not have privilege for this.Please contact Administrator');return false;" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>
                <ajaxToolkit:AutoCompleteExtender ID="acFEE_CONCESSION_ACT_ID" runat="server" BehaviorID="AutoCompleteEx2"
                    CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement"
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"
                    CompletionSetCount="19" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                    ServiceMethod="GetCompletionList" ServicePath="~/WebServices/AutoComplete.asmx"
                    TargetControlID="txtFEE_CONCESSION_ACT_ID">
                    <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx2');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx2')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx2')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                </ajaxToolkit:AutoCompleteExtender>
                <asp:ObjectDataSource ID="odsGetSCHEDULE_M_CHARGE" runat="server" SelectMethod="GetSCHEDULE_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="TRUE" Name="bisCollection" Type="Boolean"></asp:Parameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetSCHEDULE_M_COLLN" runat="server" SelectMethod="GetSCHEDULE_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="FALSE" Name="bisCollection" Type="Boolean"></asp:Parameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetSERVICES_SYS_M" runat="server"
                    SelectMethod="GetSERVICES_SYS_M" TypeName="FeeCommon"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" SelectMethod="GetFEETYPE_M"
                    TypeName="FeeCommon"></asp:ObjectDataSource>
                <asp:SqlDataSource ID="SqlCollection" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="SELECT [COL_ID], [COL_DESCR] FROM [COLLECTION_M]"></asp:SqlDataSource>

                <asp:SqlDataSource ID="SqlFeeGroup" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
                    SelectCommand="SELECT -1 As [FGP_ID], 'Select Group' As [FGP_DESCR] UNION ALL SELECT [FGP_ID], [FGP_DESCR] FROM FEES.FEE_GROUP_M"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlEventType" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_DAXConnectionString %>"
                    SelectCommand="SELECT '-1' As [DIM_CODE], 'Select Event Type' As [DIM_DESC] UNION ALL SELECT [DIM_CODE], [DIM_DESC] FROM DAX.DIMENSIONS_M  WHERE DIM_DTM_ID =4"></asp:SqlDataSource>

                <ajaxToolkit:AutoCompleteExtender ID="acFEE_DISC_ACT_ID" runat="server" BehaviorID="AutoCompleteEx3"
                    CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement"
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"
                    CompletionSetCount="20" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                    ServiceMethod="GetCompletionList" ServicePath="~/WebServices/AutoComplete.asmx"
                    TargetControlID="txtFEE_DISC_ACT_ID">
                    <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx3');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx3')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx3')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                </ajaxToolkit:AutoCompleteExtender>
                <ajaxToolkit:AutoCompleteExtender ID="acFEE_CHRG_INADV" runat="server" BehaviorID="AutoCompleteEx1"
                    CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement"
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"
                    CompletionSetCount="18" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                    ServiceMethod="GetCompletionList" ServicePath="~/WebServices/AutoComplete.asmx"
                    TargetControlID="txtFEE_CHRG_INADV">
                    <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                </ajaxToolkit:AutoCompleteExtender>
                <ajaxToolkit:AutoCompleteExtender ID="acFEE_INC_ACT_ID" runat="server" BehaviorID="AutoCompleteEx"
                    CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement"
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"
                    CompletionSetCount="17" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                    ServiceMethod="GetCompletionList" ServicePath="~/WebServices/AutoComplete.asmx"
                    TargetControlID="txtFEE_INC_ACT_ID">
                    <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                </ajaxToolkit:AutoCompleteExtender>
                <asp:HiddenField ID="hf_PRO_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hf_CTRL_VAL" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

