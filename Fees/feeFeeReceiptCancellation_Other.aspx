﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeFeeReceiptCancellation_Other.aspx.vb" Inherits="Fees_feeFeeReceiptCancellation_Other" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="true">
    <%--   <script language="javascript" type="text/javascript">
        function GetDocno() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 645px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var docBSU = document.getElementById("<%=ddBusinessunit.ClientID %>").value;

            result = window.showModalDialog("../Common/PopupFormThree.aspx?multiSelect=false&ID=RECNO_OTH&BSU=" + docBSU + "&dt=" + document.getElementById('<%=txtDate.ClientID %>').value, "", sFeatures)
            if (result != "" && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=txtRecieptNo.ClientID %>').value = NameandCode[1];
            }
            return true;
        }
    </script>--%>
    <script>
        function GetDocno() {
            var docBSU = document.getElementById("<%=ddBusinessunit.ClientID %>").value;
            var docType;
             var oWnd = radopen("../Common/PopupFormThree.aspx?multiSelect=false&ID=RECNO_OTH&BSU=" + docBSU + "&dt=" + document.getElementById('<%=txtDate.ClientID %>').value, "pop_GetDocno");
        }


        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=txtRecieptNo.ClientID %>').value = NameandCode[1];
             }
         }

         function autoSizeWithCalendar(oWindow) {
             var iframe = oWindow.get_contentFrame();
             var body = iframe.contentWindow.document.body;

             var height = body.scrollHeight;
             var width = body.scrollWidth;

             var iframeBounds = $telerik.getBounds(iframe);
             var heightDelta = height - iframeBounds.height;
             var widthDelta = width - iframeBounds.width;

             if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
             if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
             oWindow.center();
         }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_GetDocno" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Other Receipt Cancellation Request
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table   width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table class="BlueTable" align="center" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox><asp:ImageButton ID="imgFromDate"
                                runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtDate" Display="Dynamic"
                                    ErrorMessage="Enter the Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span>
                            <td align="left" width="30%">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="BSU_ID"   AutoPostBack="True" TabIndex="5">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Rec. No.</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRecieptNo" runat="server"  AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgDocno" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetDocno();return false;"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRecieptNo"
                                ErrorMessage="Rec. No Required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr class="matters" runat="server" id="trstudDet">
                        <td align="left"><span class="field-label">Amount</span></td>
                        <td align="left">
                            <asp:Label ID="lblAmount" runat="server"></asp:Label></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr class="matters" runat="server" id="trAlert" visible="false">
                        <td align="left" colspan="4">
                            <asp:Label ID="lblAlertMessage" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Remarks</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server"  TextMode="MultiLine" SkinID="MultiText"   TabIndex="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRemarks"
                                ErrorMessage="Remarks Required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center"><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Delete Receipt" TabIndex="105" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:Button ID="btnLoad" runat="server" Text="" Style="color: white !important; border: none; background: none;" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GETBSUFORUSER" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sUsr_name" DefaultValue="" Name="USR_ID" Type="String"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

