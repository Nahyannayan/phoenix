
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeFeeSettlement
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                'gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                If Session("sUsr_name") = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_FEE_SETTLEMENT Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_FEE_SETTLEMENT
                            lblHead.Text = "Fee Settlement"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    getDate()
                    ddlBUnit.DataBind()

                    ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
                    'BindHistory()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub

    Sub getDate()
        Dim strDate As String = UtilityObj.GetDataFromSQL("select REPLACE( CONVERT( CHAR,isnull(max(fta_trandt) + 1,getdate()),106),' ','/') from fees.FEE_TRNSALL  " _
        & " WHERE fta_trantype='DAYEND' AND fta_bhardclose=1 AND fta_bsu_id =  '" & Session("sBsuid") & "' ", _
       ConnectionManger.GetOASIS_FEESConnectionString)
        txtDate.Text = strDate
        If txtDate.Text = "" Then
            txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If 
        Dim str_err As String = ""
        If IsDate(txtDate.Text) = False Then
            str_err = str_err & "<br />" & "Please Enter Valid Date"
        End If
      
        Dim strfDate As String = txtDate.Text.Trim
        str_err = str_err & DateFunctions.checkdate(strfDate)
        Dim str_FYear As String = UtilityObj.GetDataFromSQL("SELECT FYR_ID FROM FINANCIALYEAR_S " _
        & " WHERE '" & strfDate & "' BETWEEN FYR_FROMDT AND FYR_TODT AND ISNULL(FYR_bCLOSE,0)=0", _
        ConnectionManger.GetOASISConnectionString)
        If str_FYear = "" Or str_FYear = "--" Then
            str_err = str_err & "<br />" & "Check Financial Year"
        End If
        If str_err <> "" Then
            'lblError.Text = str_err
            usrMessageBar.ShowNotification(str_err, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtDate.Text = strfDate
        End If
        Try
            Dim conn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            conn.Open()
            Try
                Dim cmd As New SqlCommand("FEES.F_FEESETTLEMENT", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 0
                Dim sqlpBSU_ID As New SqlParameter("@vBSU_ID", SqlDbType.VarChar, 20)
                sqlpBSU_ID.Value = ddlBUnit.SelectedValue
                cmd.Parameters.Add(sqlpBSU_ID)

                Dim sqlpDate As New SqlParameter("@vDATE", SqlDbType.DateTime, 20)
                sqlpDate.Value = txtDate.Text
                cmd.Parameters.Add(sqlpDate)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue = 0 Then
                    'lblError.Text = "Successfully Saved"
                    usrMessageBar.ShowNotification("Successfully Saved", UserControls_usrMessageBar.WarningType.Success)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

End Class

