<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeChequeBouncePosting.aspx.vb" Inherits="FEEAdjustmentPosting" Title="Untitled Page" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        
function ChangeAllCheckBoxStates(checkedBox) {
    var chk_state = checkedBox.checked;
    for (i = 0; i < document.forms[0].elements.length; i++)
        if (document.forms[0].elements[i].type == 'checkbox')
            document.forms[0].elements[i].checked = '';
    checkedBox.checked = chk_state;
}
    </script>
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Post Cheque Return
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                <input id="h_SelectedId" runat="server" type="hidden" value="-1" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
        </tr>
        
        <tr>
            <!--  <iput id="chkSelectall" type="checkbox" onclick="change_chk_state('0')" /><a href="javascript:change_chk_state('1')">Select All</a>-->
            <td align="left" width="15%"><span class="field-label">Date</span></td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtDate" runat="server" Width="96px"></asp:TextBox>&nbsp;<asp:ImageButton
                    ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><asp:RequiredFieldValidator
                        ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDate" ErrorMessage="Date  required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
            </td>
            <td align="left" width="25%"></td>
            <td align="left" width="30%"></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center">
                <asp:GridView ID="gvFeeAdjustmentDet" SkinID="GridViewView" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False"
                    Width="100%" AllowPaging="True" PageSize="25">
                    <Columns>
                        <asp:TemplateField HeaderText=" ">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPost" runat="server" onclick="ChangeAllCheckBoxStates(this)" />
                            </ItemTemplate>
                            <HeaderStyle Width="1%" Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField Visible="False" HeaderText="FCR_ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFCR_ID" runat="server" Text='<%# Bind("FCR_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="STUNO">
                            <HeaderTemplate>
                                Student ID<br />
                                                            <asp:TextBox ID="txtSTU_NO" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                                            <asp:ImageButton ID="btnDocNoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <HeaderTemplate>
                                Student Name<br />
                                <asp:TextBox ID="txtSTUNAME" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student/ Enquiry">
                            <ItemTemplate>
                                <asp:Label ID="lblStudtype" runat="server" Text='<%# bind("FCR_STU_TYPE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date<br />
                                <asp:TextBox ID="txtDate" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FCR_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RECNO">
                            <HeaderTemplate>
                                Chq. No<br />
                                <asp:TextBox ID="txtCHQNO" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                                                <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>&nbsp;</td>
                                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblACD_YEAR" runat="server" Text='<%# Bind("FCQ_CHQNO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BANK">
                            <HeaderTemplate>
                                Bank<br />
                                <asp:TextBox ID="txtBank" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                            
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("BNK_DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmt" runat="server" Text='<%# bind("TotalAmount") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="View">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left"></td>
        </tr>
        <tr>
            <td align="center" colspan="4">

                <asp:CheckBox ID="chkEmail" runat="server" CssClass="field-label" Text="EMail"></asp:CheckBox>
                <asp:CheckBox ID="ChkSMS" runat="server" CssClass="field-label" Text="SMS"></asp:CheckBox>
                <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" />
                <asp:Button ID="btnPrintReciept" runat="server" CssClass="button" Text="Print Receipt"
                    Visible="False" /></td>
        </tr>
    </table>

    
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtDate" PopupPosition="BottomLeft">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgDate" PopupPosition="BottomLeft" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>

                </div>
            </div>
        </div>
</asp:Content>

