Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeSetupforAcademic
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F300125" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            InitialiseComponents()
            txtLinkToStage.Attributes.Add("ReadOnly", "ReadOnly")
            Session("FeeSetup") = FeeMaster.CreateDataTableFeeSetup()
            If Request.QueryString("viewid") <> "" Then
                ddlRevReco.DataSource = GetSCHEDULE_M_NEW(False)
                ddlRevReco.DataTextField = "SCH_DESCR"
                ddlRevReco.DataValueField = "SCH_ID"
                ddlRevReco.DataBind()

                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")), _
                Encr_decrData.Decrypt(Request.QueryString("StrId").Replace(" ", "+")), _
                Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+")))
                tr_DetailViewDetailHeader.Visible = True
                tr_DetailViewDetails.Visible = True
                Dim dt As DataTable
                dt = FeeMaster.CreateDataTableFeeSetupMonthly
                gvMonthly.DataSource = dt
                gvMonthly.DataBind()
                gvFeeSetup.Columns(8).Visible = False
            Else
                ViewState("datamode") = "add"
                tr_DetailViewDetailHeader.Visible = False
                tr_DetailViewDetails.Visible = False
                ResetViewData()

                ddlRevReco.DataSource = GetSCHEDULE_M_NEW(False)
                ddlRevReco.DataTextField = "SCH_DESCR"
                ddlRevReco.DataValueField = "SCH_ID"
                ddlRevReco.DataBind()
                'ddlRevReco.Items.Add(New ListItem("--", -1))
                'ddlRevReco.Items.Insert(0, New ListItem("--", "-1"))
                'ddlRevReco.SelectedIndex = -1
                'ddlRevReco.Items.FindByText("--").Selected = True
            End If
            chkDefaultPlan.Checked = True
            Plan_changed()
            gvFeeSetup.DataBind()

        End If

    End Sub
    Public Shared Function GetSCHEDULE_M_NEW(ByVal bisCollection As Boolean) As DataSet
        Dim sql_query As String
        If bisCollection Then
            sql_query = "SELECT SCH_ID, SCH_DESCR FROM FEES.SCHEDULE_M where SCH_DESCR not like '%Quarterly%'"
        Else
            sql_query = "SELECT '-2' AS SCH_ID, '--' AS SCH_DESCR FROM FEES.SCHEDULE_M UNION SELECT '-1' AS SCH_ID, 'Not Applicable' AS SCH_DESCR FROM FEES.SCHEDULE_M UNION SELECT SCH_ID, SCH_DESCR FROM FEES.SCHEDULE_M WHERE SCH_ID IN (0,5)"
        End If
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData '.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Sub InitialiseComponents()
        txtLocation.Attributes.Add("readonly", "readonly")
        gvMonthly.Attributes.Add("bordercolor", "#1b80b6")
        gvFeeSetup.Attributes.Add("bordercolor", "#1b80b6")
        Dim isFlexiblePlan As Boolean = UtilityObj.GetDataFromSQL("SELECT ISNULL(BSU_BFlexiblePlan, 0) FROM BUSINESSUNIT_M WHERE (BSU_ID = '" & Session("sBsuid") & "')", ConnectionManger.GetOASISConnectionString)
        'If isFlexiblePlan Then
        '    gvMonthly.Columns(4).Visible = True
        '    gvMonthly.Columns(3).Visible = True
        'Else 
        '    gvMonthly.Columns(4).Visible = False
        '    gvMonthly.Columns(3).Visible = False
        'End If
        Me.chkInvDueDt.Checked = False
        chkInvDueDt_CheckedChanged(Nothing, Nothing)
        ddlCollectionSchedule.DataBind()
        ddlAcademic.DataBind()
        ddlAcademic.SelectedIndex = -1
        If Request.QueryString("acdid") = "" Then
            ddlAcademic.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        Else
            ddlAcademic.Items.FindByValue(Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))).Selected = True
        End If
        ddlFeetype.DataBind()
        setProRata()
        txtFrom.Text = FeeCommon.GetACDStartDate(Session("Current_ACD_ID"))
        Set_Collection()
    End Sub

    Private Sub setModifyHeader(ByVal p_GRD_ID As String, ByVal p_STM_ID As String, ByVal p_ACD_ID As String) 'setting header data on view/edit
        Try
            Dim str_Sql As String
            str_Sql = "SELECT * FROM FEES.VIEWFEESETUP " _
                & " WHERE ( ACD_ID = '" & p_ACD_ID & "') " _
                & " AND ( GRD_ID = '" & p_GRD_ID & "') AND ( FSP_BSU_ID = '" & Session("sBSUID") & "')" _
                & " and  STREAM='" & p_STM_ID & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            Session("FeeSetup") = ds.Tables(0)
            ddlAcademic.SelectedIndex = -1
            txtFrom.Text = (CDate(ds.Tables(0).Rows(0)("FDate")).ToString("dd/MMM/yyyy"))
            ddlAcademic.Items.FindByValue(ds.Tables(0).Rows(0)("Acd_id")).Selected = True
            If ds.Tables(0).Rows(0)("FSP_BRefundable") = True Then
                chkRefundable.Checked = True
            End If
            Me.chkActive.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("Active"))
            Me.txtDiscRemarks.Text = ds.Tables(0).Rows(0)("FSP_bDeactive_Remarks").ToString.Trim
            If Me.txtDiscRemarks.Text <> "" Then
                trDiscRemarks.Visible = True
            Else
                trDiscRemarks.Visible = False
            End If
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub setViewData()
        gvFeeSetup.Columns(8).Visible = False
        txtFrom.Attributes.Add("readonly", "readonly")
        ddlCollectionSchedule.Enabled = False
        ddlAcademic.Enabled = False
        ddlFeetype.Enabled = False
        ddlGrade.Enabled = False
        ddlStream.Enabled = False
        imgFrom.Enabled = False
        imgLinkToStage.Enabled = False
        lnkClear.Visible = False
        chkAllowFlexible.Enabled = False
        chkDefaultPlan.Enabled = False
    End Sub

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        gvFeeSetup.Columns(8).Visible = False
        ddlCollectionSchedule.Enabled = True
        ddlAcademic.Enabled = True
        ddlFeetype.Enabled = True
        ddlGrade.Enabled = True
        ddlStream.Enabled = True
        imgFrom.Enabled = True
        imgLinkToStage.Enabled = True
        lnkClear.Visible = True
        chkAllowFlexible.Enabled = True
        chkDefaultPlan.Enabled = True
        If ViewState("datamode") = "add" Then
            FeeMaster.SetDefaultLinkToState(txtLinkToStage, hf_PRO_ID)
        End If
        If ViewState("datamode").ToString <> "edit" Then
            setProRata()
            Set_Collection()
        End If
    End Sub

    Sub Clear_All()
        hf_PRO_ID.Value = ""
        txtLinkToStage.Text = ""
        txtLocation.Text = ""
        H_Location.Value = ""
        txtHAmount.Text = ""
        txtLateAmount.Text = "0"
        txtLatefeedays.Text = "0"
        gvFeeSetup.DataBind()
        Session("FeeSetup").Rows.Clear()
        gvMonthly.DataBind()
        chkAllowFlexible.Checked = False
        chkDefaultPlan.Checked = True
        chkInvDueDt.Checked = False
        txtInvDueDt.Text = ""
        Plan_changed()
    End Sub

    Sub gridbind()
        gvFeeSetup.DataSource = Session("FeeSetup")
        gvFeeSetup.DataBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        'If ViewState("datamode").ToString = "edit" Then
        '    lblError.Text = "Editing currently disabled...."
        '    Exit Sub
        'End If

        If ddlRevReco.SelectedValue = "-2" Then
            usrMessageBar.ShowNotification("Please Select Revenue Recognition", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        If gvMonthly.Rows.Count = 0 Then
            'lblError.Text = "Please add fee details"
            usrMessageBar.ShowNotification("Please add fee details", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        If hf_PRO_ID.Value = "" Then
            'lblError.Text = "Please Link To Stage..."
            usrMessageBar.ShowNotification("Please Link To Stage...", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        If tr_Transport.Visible And H_Location.Value = "" Then
            'lblError.Text = "Please Select location..."
            usrMessageBar.ShowNotification("Please Select location...", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim str_update_id, NEW_FSP_ID, NEW_FSM_ID As String
        str_update_id = ""
        NEW_FSP_ID = ""
        NEW_FSM_ID = ""
        Dim dblTotal As Decimal = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            If ViewState("datamode") = "edit" Then
                If Session("FeeSetup").Rows.Count = 0 And ViewState("datamode") <> "edit" Then
                    'lblError.Text = "There is no data to save"
                    usrMessageBar.ShowNotification("There is no data to save", UserControls_usrMessageBar.WarningType.Warning)
                    Exit Sub
                End If
                For Each gvr As GridViewRow In gvMonthly.Rows
                    'Get a programmatic reference to the CheckBox control
                    Dim lblREFId As Label = TryCast(gvFeeSetup.Rows(gvFeeSetup.EditIndex).FindControl("lblId"), Label)
                    Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
                    Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
                    Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
                    Dim txtOtherAmount As TextBox = CType(gvr.FindControl("txtOtherAmount"), TextBox)
                    Dim txtDueDate As TextBox = CType(gvr.FindControl("txtDueDate"), TextBox)
                    If Not txtAmount Is Nothing Then
                        If Not IsNumeric(txtAmount.Text) Or Not IsNumeric(txtOtherAmount.Text) Then
                            retval = "510" 'invalid amount
                            Exit For
                        End If
                        retval = FeeMaster.F_SaveFEESETUPMONTHLY_D(lblId.Text, ViewState("FDD_FSP_ID"), lblREFId.Text, txtChargeDate.Text, _
                        txtAmount.Text, 0, txtChargeDate.Text, txtChargeDate.Text, txtChargeDate.Text, _
                        Session("sbsuid"), ddlAcademic.SelectedItem.Value, "", Session("sUsr_name"), _
                        txtDueDate.Text, txtOtherAmount.Text, stTrans)
                        dblTotal = dblTotal + CDbl(txtAmount.Text)
                    End If
                    If retval <> "0" Then
                        Exit For
                    End If
                Next
                If retval = 0 Then
                    retval = FeeMaster.UpdateChangedFeesInConcession_OASIS(ddlAcademic.SelectedItem.Value, Session("sbsuid"), _
                    ddlGrade.SelectedItem.Value, ddlFeetype.SelectedItem.Value, ddlStream.SelectedItem.Value, _
                    Session("sUsr_name"), stTrans)
                End If
            Else
                'Id ,Acd_year, Acd_id,  Fee_id,  Fee_descr,  Grd_id, FDate,  TDate, Amount ,Status
                '  , Join, Discontinue, Active , Percentage,LateAmount,LateDays 
                Dim iLateAmttype As Integer = 1
                If rbPercentage.Checked Then
                    iLateAmttype = 2
                End If
                If Not IsDate(txtFrom.Text) Then
                    'lblError.Text = "Invalid date"
                    usrMessageBar.ShowNotification("Invalid date", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
                If chkInvDueDt.Checked AndAlso Not IsDate(txtInvDueDt.Text) Then
                    'lblError.Text = "Invalid invoice due date"
                    usrMessageBar.ShowNotification("Invalid invoice due date", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
                If Not IsNumeric(txtHAmount.Text) Or Not IsNumeric(txtLateAmount.Text) Or Not IsNumeric(txtLatefeedays.Text) Then
                    stTrans.Rollback()
                    'lblError.Text = "Invalid Amount/No. of Days"
                    usrMessageBar.ShowNotification("Invalid Amount/No. of Days", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                Dim boolJoin As Boolean = True
                Dim boolDiscontinue As Boolean = True

                If ddlJoinprorata.SelectedIndex = 0 Then
                    boolJoin = False
                End If
                If ddlDiscontinueprorata.SelectedIndex = 0 Then
                    boolDiscontinue = False
                End If
                Dim defaultplan As String = "-1"
                If chkDefaultPlan.Checked Then
                    defaultplan = ddlCollectionSchedule.SelectedItem.Value
                End If
                retval = FeeMaster.F_SaveFEESETUP_M(0, Session("sbsuid"), ddlAcademic.SelectedItem.Value, _
                   ddlFeetype.SelectedItem.Value, ViewState("FEE_ChargedFor_SCH_ID"), _
                   ddlCollectionSchedule.SelectedItem.Value, NEW_FSM_ID, stTrans, _
                   defaultplan, chkAllowFlexible.Checked)
                If retval = "0" Then
                    retval = FeeMaster.F_SaveFEESETUP_S(0, Session("sbsuid"), ddlAcademic.SelectedItem.Value, _
                       ddlFeetype.SelectedItem.Value, ddlGrade.SelectedItem.Value, txtHAmount.Text, 0, chkActive.Checked, _
                       txtFrom.Text, "", boolJoin, boolDiscontinue, iLateAmttype, _
                       txtLateAmount.Text, txtLatefeedays.Text, ddlCollectionSchedule.SelectedItem.Value, _
                       ddlStream.SelectedItem.Value, chkWeekly.Checked, ddlJoinprorata.SelectedItem.Value, _
                       ddlDiscontinueprorata.SelectedItem.Value, H_Location.Value, NEW_FSM_ID, NEW_FSP_ID, _
                       "", hf_PRO_ID.Value, chkRefundable.Checked, NEW_FSM_ID, _
                       ddlChargeScheduleFail.SelectedItem.Value, stTrans, chkInvDueDt.Checked, Me.txtInvDueDt.Text, ddlRevReco.SelectedItem.Value)

                    If retval = "0" Then
                        For Each gvr As GridViewRow In gvMonthly.Rows
                            'Get a programmatic reference to the CheckBox control
                            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
                            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
                            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
                            Dim txtOtherAmount As TextBox = CType(gvr.FindControl("txtOtherAmount"), TextBox)
                            Dim txtDueDate As TextBox = CType(gvr.FindControl("txtDueDate"), TextBox)
                            If gvMonthly.Columns(5).Visible = False Then
                                txtOtherAmount.Text = "0"
                                txtDueDate.Text = txtChargeDate.Text
                            End If
                            If CDate(txtDueDate.Text).Month <> CDate(txtDueDate.Text).Month Or _
                             CDate(txtDueDate.Text).Year <> CDate(txtDueDate.Text).Year Then
                                retval = "606" 'invalid date
                            End If
                            If Not txtAmount Is Nothing Then
                                If Not IsNumeric(txtAmount.Text) Then
                                    retval = "510" 'invalid amount
                                    Exit For
                                End If
                                retval = FeeMaster.F_SaveFEESETUPMONTHLY_D(0, NEW_FSP_ID, lblId.Text, txtChargeDate.Text, _
                                txtAmount.Text, 0, txtChargeDate.Text, txtChargeDate.Text, txtChargeDate.Text, _
                                Session("sbsuid"), ddlAcademic.SelectedItem.Value, "", Session("sUsr_name"), _
                                txtDueDate.Text, txtOtherAmount.Text, stTrans)
                                dblTotal = dblTotal + CDbl(txtAmount.Text)
                            End If
                            If retval <> "0" Then
                                Exit For
                            End If
                        Next
                        If retval = 0 Then
                            retval = VerifyFEESETUP_S(NEW_FSP_ID, stTrans)
                        End If
                    End If
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                    NEW_FSP_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If

            If retval = "0" And dblTotal <> CDbl(txtHAmount.Text) And ViewState("datamode") <> "edit" Then
                retval = "413" 'totals not tallying
            End If
            If retval = "0" Then
                stTrans.Commit()
                'lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                Clear_All()
                ViewState("datamode") = "add"
                gridbind()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Public Shared Function VerifyFEESETUP_S(ByVal FSP_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FSP_ID", SqlDbType.BigInt)
        pParms(0).Value = FSP_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.VerifyFEESETUP_S", pParms)
        Return pParms(1).Value
    End Function

    Protected Sub gvFeeSetup_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFeeSetup.RowDeleting
        Try
            Dim row As GridViewRow = gvFeeSetup.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As String = "1000"
                retval = FeeMaster.F_DeleteFeeSETUP_OASIS(iIndex, ddlAcademic.SelectedItem.Value, Session("sUsr_name"), stTrans)
                If retval = "0" Then
                    stTrans.Commit()
                    'loop through the data table row  for the selected rowindex item in the grid view
                    For iRemove = 0 To Session("FeeSetup").Rows.Count - 1
                        If iIndex = Session("FeeSetup").Rows(iRemove)(0) Then
                            Session("FeeSetup").Rows(iRemove).Delete()
                            Exit For
                        End If
                    Next
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, iIndex, _
                    "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    'lblError.Text = getErrorMessage("0")
                    usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                    gridbind()
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Record cannot be Deleted"
            usrMessageBar.ShowNotification("Record cannot be Deleted", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
        gvFeeSetup.Columns(8).Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            Clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        Clear_All()
        gridbind()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

    Protected Sub ddFeetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeetype.SelectedIndexChanged
        If ViewState("datamode").ToString <> "edit" Then
            setProRata()
            Set_Collection()
        End If
    End Sub

    Sub setProRata(Optional ByVal p_Collection As String = "")
        Dim str_Sql As String = "SELECT FEE.FEE_bJOINPRORATA, FEE.FEE_bDiscontinuePRORATA, " _
        & " CHG.SCH_DESCR AS CHG_DESCR, FEE.FEE_Collection_SCH_ID,FEE.FEE_ChargedFor_SCH_ID, " _
        & " FEE.FEE_bSETUPBYGRADE FROM FEES.FEES_M AS FEE INNER JOIN FEES.SCHEDULE_M AS CHG  " _
        & " ON FEE.FEE_ChargedFor_SCH_ID = CHG.SCH_ID" _
        & " where FEE_ID='" & ddlFeetype.SelectedItem.Value & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lblChargeSchedule.Text = "(" & ds.Tables(0).Rows(0)("CHG_DESCR") & ")"
            ViewState("FEE_ChargedFor_SCH_ID") = ds.Tables(0).Rows(0)("FEE_ChargedFor_SCH_ID")

            ddlCollectionSchedule.SelectedIndex = -1
            ddlCollectionSchedule.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_Collection_SCH_ID")).Selected = True
            If ds.Tables(0).Rows(0)("FEE_bSETUPBYGRADE") Then
                ddlGrade.Enabled = True
                ddlStream.Enabled = True
            Else
                ddlGrade.SelectedIndex = 0
                ddlStream.SelectedIndex = 0
                ddlGrade.Enabled = False
                ddlStream.Enabled = False
            End If
        End If
        SetTransport()
    End Sub

    Sub SetTransport()
        Dim sb_sql As New StringBuilder()
        sb_sql.Append("SELECT FEE_SVC_ID FROM FEES.FEES_M" _
        & "  where (FEE_ID= '" & ddlFeetype.SelectedItem.Value & "') ")
        Dim str_svc_id As String = UtilityObj.GetDataFromSQL(sb_sql.ToString, ConnectionManger.GetOASIS_FEESConnectionString)
        If str_svc_id <> "--" Then
            If str_svc_id = "1" Then
                tr_Transport.Visible = True
            Else
                tr_Transport.Visible = False
                txtLocation.Text = ""
                H_Location.Value = ""
            End If
        End If
    End Sub

    Protected Sub gvFeeSetup_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvFeeSetup.RowEditing
        gvFeeSetup.EditIndex = e.NewEditIndex
        Dim row As GridViewRow = gvFeeSetup.Rows(e.NewEditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("FeeSetup").Rows.Count - 1
            If iIndex = Session("FeeSetup").Rows(iEdit)(0) Then
                'txtAmount.Text = Session("FeeSetup").Rows(iEdit)("Amount")
                'ID, Acd_year, Acd_id, Fee_id, Fee_descr, Grd_id, FDate, TDate, Amount, Status
                ', Join, Discontinue, Active ,Percentage,LateAmount,LateDays ,Collection
                ddlAcademic.DataBind()
                ddlCollectionSchedule.SelectedIndex = -1
                ddlAcademic.SelectedIndex = -1
                ddlAcademic.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Acd_id").ToString).Selected = True
                ddlStream.DataBind()
                ddlGrade.DataBind()

                ddlStream.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Stream").ToString).Selected = True
                If Session("FeeSetup").Rows(iEdit)("Grd_id").ToString <> "NONE" Then
                    ddlGrade.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Grd_id").ToString).Selected = True
                End If
                ddlCollectionSchedule.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Collection").ToString).Selected = True
                txtFrom.Text = Format(CDate(Session("FeeSetup").Rows(iEdit)("FDate")), "dd/MMM/yyyy")

                ddlFeetype.SelectedIndex = -1
                ddlFeetype.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Fee_id")).Selected = True
                hf_PRO_ID.Value = Session("FeeSetup").Rows(iEdit)("PRO_ID").ToString
                txtLinkToStage.Text = Session("FeeSetup").Rows(iEdit)("PRO_DESCRIPTION").ToString
                chkRefundable.Checked = Session("FeeSetup").Rows(iEdit)("FSP_BRefundable")

                ddlJoinprorata.SelectedIndex = -1
                ddlJoinprorata.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Join")).Selected = True
                ddlDiscontinueprorata.SelectedIndex = -1
                ddlDiscontinueprorata.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Discontinue")).Selected = True

                chkActive.Checked = Session("FeeSetup").Rows(iEdit)("Active")
                txtHAmount.Text = Format(Session("FeeSetup").Rows(iEdit)("Amount"), "0.00")
                chkWeekly.Checked = Session("FeeSetup").Rows(iEdit)("Weekly")
                txtLateAmount.Text = Session("FeeSetup").Rows(iEdit)("LateAmount")
                txtLatefeedays.Text = Session("FeeSetup").Rows(iEdit)("LateDays")
                txtLocation.Text = Session("FeeSetup").Rows(iEdit)("Location").ToString
                H_Location.Value = Session("FeeSetup").Rows(iEdit)("Locationid").ToString
                rbAmount.Checked = False
                rbPercentage.Checked = False
                If Session("FeeSetup").Rows(iEdit)("Percentage") = 1 Then
                    rbAmount.Checked = True
                ElseIf Session("FeeSetup").Rows(iEdit)("Percentage") = 2 Then
                    rbPercentage.Checked = True
                End If

                If Not ddlChargeScheduleFail.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("FSP_NEXT_SCH_ID")) Is Nothing Then
                    ddlChargeScheduleFail.SelectedIndex = -1
                    ddlChargeScheduleFail.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("FSP_NEXT_SCH_ID")).Selected = True
                End If
                chkAllowFlexible.Checked = Session("FeeSetup").Rows(iEdit)("FSM_BFlexiblePlan")
                If Session("FeeSetup").Rows(iEdit)("Collection") = Session("FeeSetup").Rows(iEdit)("FSM_DEF_SCH_ID") Then
                    chkDefaultPlan.Checked = True
                Else
                    chkDefaultPlan.Checked = False
                End If
                setMonthlyDetails(iIndex)
                If Session("FeeSetup").Rows(iEdit)("FSP_INV_DUEDATE").ToString <> "" Then
                    Me.chkInvDueDt.Checked = True
                    Me.txtInvDueDt.Text = Format(Session("FeeSetup").Rows(iEdit)("FSP_INV_DUEDATE"), OASISConstants.DateFormat)
                End If
                chkInvDueDt_CheckedChanged(Nothing, Nothing)
                ddlRevReco.SelectedValue = Session("FeeSetup").Rows(iEdit)("FSP_REVENUE_RECO_ID")
                Exit For
            End If
        Next
        gvFeeSetup.Columns(7).Visible = False
        gridbind()
    End Sub

    Private Sub setMonthlyDetails(ByVal p_Monthlyid As String) 'setting header data on view/edit
        Try
            ViewState("FDD_FSP_ID") = p_Monthlyid
            Dim str_Sql As String
            str_Sql = "SELECT FDD_ID, FDD_FSP_ID, FDD_REF_ID, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_DATE, 113), ' ', '/') AS FDD_DATE, FDD_AMOUNT, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_FIRSTMEMODT, 113), ' ', '/') AS FDD_FIRSTMEMODT, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_SECONDMEMODT, 113), ' ', '/') AS FDD_SECONDMEMODT, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_THIRDMEMODT, 113), ' ', '/') AS FDD_THIRDMEMODT, " _
            & " DESCR, REPLACE(CONVERT(VARCHAR(11), FDD_DUEDT, 113), ' ', '/') AS FDD_DUEDT,FDD_OTHCHARGE FROM FEES.VW_OSO_FEEMONTHLY " _
            & " WHERE FDD_FSP_ID = '" & p_Monthlyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            Dim dt As DataTable
            dt = FeeMaster.CreateDataTableFeeSetupMonthly
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim dr As DataRow
                dr = dt.NewRow
                dr("id") = ds.Tables(0).Rows(i)("FDD_ID")
                dr("Descr") = ds.Tables(0).Rows(i)("descr")
                dr("FDD_DATE") = ds.Tables(0).Rows(i)("FDD_DATE")
                dr("FDD_FIRSTMEMODT") = ds.Tables(0).Rows(i)("FDD_FIRSTMEMODT")
                dr("FDD_SECONDMEMODT") = ds.Tables(0).Rows(i)("FDD_SECONDMEMODT")
                dr("FDD_THIRDMEMODT") = ds.Tables(0).Rows(i)("FDD_THIRDMEMODT")
                dr("FDD_REF_ID") = ds.Tables(0).Rows(i)("FDD_REF_ID")
                dr("FDD_AMOUNT") = ds.Tables(0).Rows(i)("FDD_AMOUNT")
                dr("FDD_DUEDT") = ds.Tables(0).Rows(i)("FDD_DUEDT")
                dr("FDD_OTHCHARGE") = ds.Tables(0).Rows(i)("FDD_OTHCHARGE")
                'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT, FDD_REF_ID
                dt.Rows.Add(dr)
            Next
            gvMonthly.DataSource = dt
            gvMonthly.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddAcademic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademic.SelectedIndexChanged
        ddlGrade.DataBind()
    End Sub

    Protected Sub ddCollectionSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCollectionSchedule.SelectedIndexChanged
        If ViewState("datamode").ToString <> "edit" Then
            Set_Collection()
        End If
    End Sub

    Sub Set_Collection()
        Dim iMonths As Integer
        Dim dTotal As Decimal = 0
        If IsNumeric(txtHAmount.Text) Then
            dTotal = txtHAmount.Text
        End If
        Dim dtTermorMonths As New DataTable
        Select Case ddlCollectionSchedule.SelectedItem.Value
            Case 0 '0 Monthly 
                iMonths = 999
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademic.SelectedItem.Value, False)
            Case 1 '1 Bi-Monthly
                iMonths = 5
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademic.SelectedItem.Value, False)
            Case 2 '2 Quarterly 
                iMonths = 999
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademic.SelectedItem.Value, True)
            Case 3 '3 Half Year
                iMonths = 2
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademic.SelectedItem.Value, False)
            Case 4, 5 '4 Annual 
                iMonths = 1
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademic.SelectedItem.Value, False)
            Case Else '5 Once Only 
                iMonths = 0
        End Select
        Dim dt As New DataTable
        dt = FeeMaster.CreateDataTableFeeSetupMonthly
        Select Case iMonths
            Case 999
                For i As Integer = 0 To (dtTermorMonths.Rows.Count - 1)
                    If dtTermorMonths.Rows.Count > i Then
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i)("descr")
                        dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
                        dr("FDD_FIRSTMEMODT") = dtTermorMonths.Rows(i)("FDD_FIRSTMEMODT")
                        dr("FDD_SECONDMEMODT") = dtTermorMonths.Rows(i)("FDD_SECONDMEMODT")
                        dr("FDD_THIRDMEMODT") = dtTermorMonths.Rows(i)("FDD_THIRDMEMODT")
                        dr("FDD_AMOUNT") = dTotal / dtTermorMonths.Rows.Count
                        dr("FDD_DUEDT") = dtTermorMonths.Rows(i)("FDD_DUEDT")
                        dr("FDD_OTHCHARGE") = 0
                        dt.Rows.Add(dr)
                    End If
                Next
            Case 1
                For i As Integer = 0 To iMonths - 1
                    If dtTermorMonths.Rows.Count > i Then
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i)("descr")
                        dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
                        dr("FDD_FIRSTMEMODT") = dtTermorMonths.Rows(i)("FDD_FIRSTMEMODT")
                        dr("FDD_SECONDMEMODT") = dtTermorMonths.Rows(i)("FDD_SECONDMEMODT")
                        dr("FDD_THIRDMEMODT") = dtTermorMonths.Rows(i)("FDD_THIRDMEMODT")
                        dr("FDD_AMOUNT") = dTotal / iMonths
                        dr("FDD_DUEDT") = dtTermorMonths.Rows(i)("FDD_DUEDT")
                        dr("FDD_OTHCHARGE") = 0
                        dt.Rows.Add(dr)
                    End If
                Next
            Case 5, 2
                For i As Integer = 0 To iMonths - 1
                    If dtTermorMonths.Rows.Count > i Then
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i * 10 / iMonths)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i * 10 / iMonths)("descr")
                        dr("FDD_DATE") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_DATE")
                        dr("FDD_FIRSTMEMODT") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_FIRSTMEMODT")
                        dr("FDD_SECONDMEMODT") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_SECONDMEMODT")
                        dr("FDD_THIRDMEMODT") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_THIRDMEMODT")
                        dr("FDD_AMOUNT") = dTotal / iMonths
                        dr("FDD_DUEDT") = dtTermorMonths.Rows(i)("FDD_DUEDT")
                        dr("FDD_OTHCHARGE") = 0
                        dt.Rows.Add(dr)
                    End If
                Next
        End Select
        gvMonthly.DataSource = dt
        gvMonthly.DataBind()
    End Sub

    Protected Sub gvFeeSetup_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvFeeSetup.RowUpdating
        gvFeeSetup.EditIndex = -1
        gridbind()
    End Sub

    Protected Sub gvFeeSetup_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvFeeSetup.RowCancelingEdit
        gvFeeSetup.EditIndex = -1
        gridbind()
    End Sub

    Protected Sub txtHAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHAmount.TextChanged
        If ViewState("datamode") = "edit" Then
            Set_Amount()
        Else
            Set_Collection()
        End If
    End Sub

    Sub Set_Amount()
        Dim iCount As Integer = gvMonthly.Rows.Count
        Dim dMonthly As Decimal = 0
        If IsNumeric(txtHAmount.Text) And iCount > 0 Then
            dMonthly = CDec(txtHAmount.Text) / iCount
        End If
        For Each gvr As GridViewRow In gvMonthly.Rows
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            If txtAmount IsNot Nothing And dMonthly > 0 Then
                txtAmount.Text = dMonthly
            End If
        Next
    End Sub

    Protected Sub chkDefaultPlan_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDefaultPlan.CheckedChanged
        Plan_changed()
    End Sub

    Sub Plan_changed()
        If chkDefaultPlan.Checked And ViewState("datamode").ToString <> "edit" And ViewState("datamode").ToString <> "view" Then
            gvMonthly.Columns(4).Visible = False
            gvMonthly.Columns(5).Visible = False
            'chkAllowFlexible.Checked = True
        Else
            gvMonthly.Columns(4).Visible = True
            gvMonthly.Columns(5).Visible = True
            'chkAllowFlexible.Checked = False
        End If
        If ViewState("datamode").ToString <> "edit" Then
            Set_Collection()
        End If
    End Sub

    Protected Sub chkInvDueDt_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkInvDueDt.Checked Then
            Me.txtInvDueDt.Enabled = True
            Me.imgInvDueDt.Enabled = True
        Else
            Me.txtInvDueDt.Enabled = False
            Me.imgInvDueDt.Enabled = False
        End If
    End Sub
End Class


