<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeFeeSettlement.aspx.vb" Inherits="Fees_FeeFeeSettlement" title="Untitled Page" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


 <table  width="100%">
        <tr valign="bottom">
            <td align="left" valign="bottom">
                <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
         
    </table>
    <table Width="100%">
        
        <tr >
            <td align="left" width="20%" >
                <span class="field-label">Date</span></td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtDate" runat="server" Width="121px"></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                    TabIndex="4" /></td>
        
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlBUnit" runat="server" DataSourceID="ODSGETBSUFORUSER" DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr>
        <td colspan="4" align="center">
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td>
        </tr>
    </table>
    <asp:ObjectDataSource ID="ODSGETBSUFORUSER" runat="server" SelectMethod="GETBSUFORUSER"
        TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter Name="USR_ID" SessionField="sUsr_name" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
     
    <ajaxToolkit:CalendarExtender ID="calDate" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFrom" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>

                </div>
            </div>
         </div>

         
</asp:Content>

