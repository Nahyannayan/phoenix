Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeChequeBounceViewTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                gvFEECHQBOUNCE.Attributes.Add("bordercolor", "#1b80b6")
                ddlBusinessunit.DataBind()
                If Not ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")) Is Nothing Then
                    ddlBusinessunit.ClearSelection()
                    ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")).Selected = True
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT And _
                ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT_POST) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT
                        hlAddNew.NavigateUrl = "FeeChequeBounceTransport.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Transport Cheque Return"
                        gvFEECHQBOUNCE.Columns(0).Visible = False
                    Case OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT_POST
                        hlAddNew.Visible = False
                        hlAddNew.NavigateUrl = "FeeChequeBounceTransport.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Transport Cheque Return Posting"
                        btnPost.Visible = True
                        ChkSMS.Visible = True
                        ChkEmail.Visible = True
                        lblDate.Visible = True
                        txtFrom.Visible = True
                        radOpen.Visible = False
                        radPosted.Visible = False
                        imgFrom.Visible = True
                        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
                End Select
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEECHQBOUNCE.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEECHQBOUNCE.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TransportConnectionString").ConnectionString
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""

            str_Filter = ""
            If gvFEECHQBOUNCE.Rows.Count > 0 Then

                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)

                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtDate")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCR_DATE", lstrCondn3)

                '   -- 3   txtTDate

                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtRecNo")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCR_RECNO", lstrCondn4)

                '   -- 5  city

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtBank")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BNK_DESCRIPTION", lstrCondn5)

            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim str_cond As String = String.Empty
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT
                    If radOpen.Checked Then
                        str_cond += " AND isnull( FCR_Bposted, 0) = 0"
                    ElseIf radPosted.Checked Then
                        str_cond += " AND isnull( FCR_Bposted, 0) = 1"
                    End If
                    str_Sql = "SELECT A.*,(SELECT COUNT(EML_ID) FROM OASIS.dbo.COM_EMAIL_SCHEDULE WITH(NOLOCK) WHERE EML_BSU_ID=A.FCR_BSU_ID  AND EML_TYPE='FCR' " _
                            & "AND ISNULL(EML_STATUS,'')='COMPLETED' AND ISNULL(EML_PROFILE_ID,'')=A.STU_ID)AS Emailed," _
                            & "(SELECT COUNT(LOG_ID) FROM OASIS_FEES.FEES.FEE_REMINDER_SMS WITH(NOLOCK) WHERE LOG_BSU_ID=A.FCR_BSU_ID AND " _
                            & "ISNULL(LOG_TYPE,'')='FCR' AND LOG_FRH_ID=A.FCR_ID AND LOG_STU_ID=A.STU_ID AND ISNULL(LOG_STATUS,'')='COMPLETED')AS SMS " _
                            & "FROM FEES.FeeChequeListBounced AS A " _
                         & " WHERE ( FCR_BSU_ID = '" & Session("sBSUID") & "') " _
                         & " AND( FCR_STU_BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "') " & str_cond & str_Filter
                Case OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT_POST
                    str_cond += " AND isnull( FCR_Bposted, 0) = 0"
                    str_Sql = "select *,0 as Emailed,0 as SMS from FEES.FeeChequeListBounced  " _
                        & " WHERE     (FCR_BSU_ID = '" & Session("sBSUID") & "') " _
                        & " AND(FCR_STU_BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "') " & str_cond & str_Filter
            End Select
            Dim str_orderby As String = " ORDER BY FCR_DATE "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_orderby)
            gvFEECHQBOUNCE.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEECHQBOUNCE.DataBind()
                Dim columnCount As Integer = gvFEECHQBOUNCE.Rows(0).Cells.Count

                gvFEECHQBOUNCE.Rows(0).Cells.Clear()
                gvFEECHQBOUNCE.Rows(0).Cells.Add(New TableCell)
                gvFEECHQBOUNCE.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEECHQBOUNCE.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEECHQBOUNCE.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEECHQBOUNCE.DataBind()
            End If
            'gvJournal.DataBind()
            'txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudNo")
            'txtSearch.Text = lstrCondn1

            'txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtStudname")
            'txtSearch.Text = lstrCondn2

            'txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtDate")
            'txtSearch.Text = lstrCondn3

            'txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtRecNo")
            'txtSearch.Text = lstrCondn4

            'txtSearch = gvFEECHQBOUNCE.HeaderRow.FindControl("txtBank")
            'txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEECHQBOUNCE.PageIndexChanging
        gvFEECHQBOUNCE.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEECHQBOUNCE.RowDataBound
        Try
            Dim datamode As String
            Dim lblFCR_ID As New Label, hfSTU_ID As New Label, lblEmail As New Label, lblSMS As New Label
            lblFCR_ID = TryCast(e.Row.FindControl("lblFCR_ID"), Label)
            lblEmail = TryCast(e.Row.FindControl("lblEmail"), Label)
            lblSMS = TryCast(e.Row.FindControl("lblSMS"), Label)
            hfSTU_ID = TryCast(e.Row.FindControl("hfSTU_ID"), Label)
            Dim hlview As New HyperLink
            datamode = Encr_decrData.Encrypt("view")
            Dim MainMnu_code As String
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If (lblFCR_ID IsNot Nothing) Then
                MainMnu_code = Encr_decrData.Encrypt(OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT)
                hlview.NavigateUrl = "FeeChequeBounceDetailedViewTransport.aspx?FCR_ID=" & Encr_decrData.Encrypt(lblFCR_ID.Text) & _
               "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode

                'If Not hfSTU_ID Is Nothing Then
                '    Dim objclsTCR As New TransportChequeReturn
                '    objclsTCR.FCR_STU_ID = hfSTU_ID.Text
                '    objclsTCR.FCR_ID = lblFCR_ID.Text
                '    objclsTCR.FCR_BSU_ID = Session("sBsuId").ToString
                '    objclsTCR.FCR_STU_BSU_ID = ddlBusinessunit.SelectedItem.Value

                '    If Not lblEmail Is Nothing Then
                '        lblEmail.Text = objclsTCR.GetEmailedCount
                '    End If
                '    If Not lblSMS Is Nothing Then
                '        lblSMS.Text = objclsTCR.GetSMSCount
                '    End If
                'End If
            End If
            
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub radOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOpen.CheckedChanged
        GridBind()
        ChkEmail.Visible = False
        ChkSMS.Visible = False
        btnSend.Visible = False
        gvFEECHQBOUNCE.Columns(0).Visible = False
    End Sub

    Protected Sub radPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPosted.CheckedChanged
        GridBind()
        ChkEmail.Visible = True
        ChkSMS.Visible = True
        btnSend.Visible = True
        gvFEECHQBOUNCE.Columns(0).Visible = True
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim stTrans As SqlTransaction
        Try
            objConn.Open()
            stTrans = objConn.BeginTransaction
            Try
                Dim iReturnvalue As Integer = PostCheckedCheques(Me.Page, objConn, stTrans)
                If (iReturnvalue = 0) Then
                    stTrans.Commit()
                    'lblError.Text = "Cheque Return Successfully Posted"
                    usrMessageBar2.ShowNotification("Cheque Return Successfully Posted", UserControls_usrMessageBar.WarningType.Success)
                    GridBind()
                Else
                    stTrans.Rollback()
                    If iReturnvalue = -100 Then
                        'lblError.Text = "Please select atleast 1 for Posting..."
                        usrMessageBar2.ShowNotification("Please select atleast 1 for Posting...", UserControls_usrMessageBar.WarningType.Danger)
                    Else
                        'lblError.Text = getErrorMessage(iReturnvalue)
                        usrMessageBar2.ShowNotification(getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
                    End If
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
            End Try
        Catch ex As Exception
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar2.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try
        GridBind()
    End Sub

    Private Function PostChequeBounce(ByVal vFAH_ID As Integer, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As Integer
        Dim cmd As New SqlCommand("[FEES].[F_POSTFEECHEQUERETURN]", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        'Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        'sqlpBSU_ID.Value = Session("sBSUId")
        'cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFAH_ID As New SqlParameter("@FCR_ID", SqlDbType.Int)
        sqlpFAH_ID.Value = vFAH_ID
        cmd.Parameters.Add(sqlpFAH_ID)

        Dim sqlpDate As New SqlParameter("@Date", SqlDbType.DateTime)
        sqlpDate.Value = CDate(txtFrom.Text)
        cmd.Parameters.Add(sqlpDate)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim iReturnvalue As Integer
        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        Dim sqlpSMSYN As New SqlParameter("@bSendSMSYN", SqlDbType.Bit)
        sqlpSMSYN.Value = ChkSMS.Checked
        cmd.Parameters.Add(sqlpSMSYN)

        Dim sqlpEmail As New SqlParameter("@bSendEmail", SqlDbType.Bit)
        sqlpEmail.Value = ChkEmail.Checked
        cmd.Parameters.Add(sqlpEmail)

        Dim sqlpNewDOCNo As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
        sqlpNewDOCNo.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNewDOCNo)

        Dim sqlpUserName As New SqlParameter("@UserName", SqlDbType.VarChar, 50)
        sqlpUserName.Value = Session("sUsr_name").ToString
        cmd.Parameters.Add(sqlpUserName)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value
        If iReturnvalue = 0 Then
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, vFAH_ID, "FEE Adjustment Posting", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
        End If
        Return iReturnvalue
    End Function

    Private Function PostCheckedCheques(ByVal Page As Control, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim retval As Integer
        Dim nochk As Boolean = False 
        For Each gvr As GridViewRow In gvFEECHQBOUNCE.Rows
            Dim lblFCR_ID As Label = CType(gvr.FindControl("lblFCR_ID"), Label)
            Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
            If Not lblFCR_ID Is Nothing Then
                If IsNumeric(lblFCR_ID.Text) And chkPost.Checked Then
                    nochk = True
                    retval = PostChequeBounce(lblFCR_ID.Text, conn, trans)
                    If retval <> 0 Then
                        Return retval
                    End If
                End If
            End If
        Next
        If Not nochk Then
            Return -100
        End If
        Return retval
    End Function

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        Session("PROVIDER_BSU_ID") = ddlBusinessunit.SelectedItem.Value
        GridBind()
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        For Each gvr As GridViewRow In gvFEECHQBOUNCE.Rows
            Dim lblFCR_ID As Label = CType(gvr.FindControl("lblFCR_ID"), Label)
            Dim hfSTU_ID As Label = CType(gvr.FindControl("hfSTU_ID"), Label)
            Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
            If Not lblFCR_ID Is Nothing AndAlso Not hfSTU_ID Is Nothing Then
                If IsNumeric(lblFCR_ID.Text) And chkPost.Checked Then
                    Dim objclsTCR As New TransportChequeReturn
                    objclsTCR.FCR_ID = lblFCR_ID.Text
                    objclsTCR.FCR_STU_ID = hfSTU_ID.Text
                    objclsTCR.FCR_bSendSMS = ChkSMS.Checked
                    objclsTCR.FCR_bSendEmail = ChkEmail.Checked
                    objclsTCR.UserName = Session("sUsr_name").ToString
                    If objclsTCR.SendAlert() Then
                        chkPost.Checked = False
                        gvr.BackColor = Drawing.Color.LawnGreen
                    End If
                End If
            End If
        Next
    End Sub

End Class
