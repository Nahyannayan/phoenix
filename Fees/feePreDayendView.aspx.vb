Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feePreDayendView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            gvFEEAdjustments.Attributes.Add("bordercolor", "#1b80b6")
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> "F300305") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS
                        lblHeader.Text = "Fee Adjustment"
                        'hlAddNew.NavigateUrl = "FeeAdjustment.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_HEADTOHEAD
                        'hlAddNew.NavigateUrl = "FeeAdjustment_HeadtoHead.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Fee Adjustment(Between Heads)"
                End Select
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEAdjustments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEAdjustments.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_Sql As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty

            Dim ds As New DataSet
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvFEEAdjustments.Rows.Count > 0 Then
                ' --- Initialize The Variables
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtTRANTYPE")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPD_TRANTYPE", lstrCondn1)
                '  -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtUSER")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPD_DATE", lstrCondn2)
                ''   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPD_RETMESSAGE", lstrCondn3)

            End If
            Dim str_cond As String = " AND FPD_BSU_ID ='" & Session("sBSUID") & "'"

            Dim str_Topfilter As String = ""
            If UsrTopFilter1.FilterCondition <> "All" Then
                str_Topfilter = " top " & UsrTopFilter1.FilterCondition
            End If
            str_Sql = "SELECT  " & str_Topfilter & "  FPD_BSU_ID, FPD_TRANTYPE, FPD_DATE, FPD_LOGDATE, " & _
            " FPD_USER,FPD_bHARDCLOSE, FPD_PROCESSTIME, FPD_RETMESSAGE " & _
            " FROM  FEES.FEE_PRE_DAYEND WHERE 1=1 " & str_cond & str_Filter

            Dim str_orderby As String = " ORDER BY  FPD_DATE DESC  "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
            CommandType.Text, str_Sql & str_cond & str_orderby)
            gvFEEAdjustments.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvFEEAdjustments.DataBind()
                Dim columnCount As Integer = gvFEEAdjustments.Rows(0).Cells.Count

                gvFEEAdjustments.Rows(0).Cells.Clear()
                gvFEEAdjustments.Rows(0).Cells.Add(New TableCell)
                gvFEEAdjustments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEAdjustments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEAdjustments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEAdjustments.DataBind()
            End If
            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtTRANTYPE")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtUSER")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn3

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEAdjustments.PageIndexChanging
        gvFEEAdjustments.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

End Class
