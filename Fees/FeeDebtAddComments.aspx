﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeeDebtAddComments.aspx.vb" Inherits="Fees_FeeDebtAddComments" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <style type="text/css">
        input.button.btn-yes {
            padding: 8px 20px !important;
        }
        span#lblEmail_preview table tr td {
            vertical-align: top !important;
        }

        .stud-profile {
            width: 80px;
            height: 80px;
            border-width: 0px;
            border-radius: 6px;
            vertical-align: top;
            border: 1px solid rgba(0,0,0,0.3) !important;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px !important;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: #fff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


        .RadComboBox .rcbInputCell {
            width: 100%;
            height: 31px !important;
            background-color: transparent !important;
            border-radius: 6px !important;
            background-image: none !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
            padding: 0px !important;
        }

        .RadComboBox_Default {
            /* color: #333; */
            font: inherit;
            width: 80% !important;
            font-style: normal !important;
        }

            .RadComboBox_Default .rcbEmptyMessage {
                font-style: normal !important;
            }

            .RadComboBox_Default .rcbFocused .rcbInput {
                color: black;
                /*padding: 10px;*/
            }

        .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image: none !important;
        }

        .RadComboBox table td.rcbInputCell {
            padding: 0px !important;
            border-width: 0;
        }

        .RadComboBox .rcbReadOnly .rcbInput {
            cursor: default;
            /*border: 1px solid rgba(0,0,0,0.12) !important;
    padding: 20px 10px !important;*/
        }

        /*Rad calender style overwride starts here*/
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image: url(../Images/calendar.gif) !important;
            width: 30px;
            height: 30px;
        }

        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
            background-image: url(../Images/calendar.gif) !important;
            width: 30px;
            height: 30px;
            background-position: 0 0 !important;
        }

        .RadPicker {
            width: 80% !important;
        }

            .RadPicker .rcCalPopup, .RadPicker .rcTimePopup {
                width: 30px !important;
                height: 30px !important;
            }

        table.RadCalendar_Default, .RadCalendar .rcMainTable {
            background: #ffffff !important;
        }

        .RadComboBox_Default .rcbActionButton {
            border-color: transparent !important;
        }

        .Mygrid {
            width: auto !important;
        }

        .invisible {
            display: none;
        }

        .inactive {
            color: orangered;
        }


        .RadEditor {
            width: 580px !important;
            min-width: 580px !important;
        }

        table td span.field-value {
            width: 100% !important;
        }

        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            z-index: 1000;
            overflow: auto;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            /*position: fixed;*/
            width: 100%;
        }
    </style>


</head>
<body>
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" lang="javascript">
        function HideAll() {

            document.getElementById('<%= pnlPayment.ClientID %>').style.display = 'none';

            return false;
        }
        function HideCard(id, index) {

            document.getElementById(id).style.display = 'table-row';
            document.getElementById('<%= h_CrCard.ClientID%>').value = index;
            return false;



        }
        function HideCardRow(id, index) {
            $("#" + id).hide(250);
            $('#<%=h_CrCard.ClientID%>').val(index);
            return false;
        }
    </script>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="sman" runat="server"></ajaxToolkit:ToolkitScriptManager>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td class="title-bg" colspan="3">Save Comments
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <pre id="preError" class="invisible" runat="server"></pre>
                        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table style="width: 100%;" class="table table-bordered table-row">
                            <tr>
                                <td rowspan="2">
                                    <asp:Image ID="imgStuImage" runat="server" class="stud-profile" ImageUrl="~/Images/stuimage.gif" ToolTip='' /></td>
                                <th class="title-bg"><span class="field-label">Student Id</span></th>
                                <th class="title-bg"><span class="field-label">Grade</span></th>
                                <th class="title-bg"><span class="field-label">Name</span></th>
                                <th class="title-bg"><span class="field-label">Status</span></th>
                                <th class="title-bg"><span class="field-label">COVID-19 Relief</span></th>
                                <th class="title-bg"><span class="field-label">Quick Links</span></th>
                            </tr>
                            <tr id="trStuDetail" runat="server">
                                <td id="tdStuNo" runat="server"></td>
                                <td id="tdGrade" runat="server"></td>
                                <td id="tdStuName" runat="server"></td>
                                <td id="tdStatus" runat="server"></td>
                                <td id="tdbCOVID" runat="server"></td>
                                <td>
                                    <asp:LinkButton ID="lbtnLedger" runat="server" Visible="false">Ledger AsOn</asp:LinkButton>&nbsp;|&nbsp;<asp:LinkButton ID="lbtnReceipts" runat="server">Receipts</asp:LinkButton>&nbsp;|&nbsp;<asp:LinkButton ID="lbtnChargedetails" runat="server" Visible="false">Charge Details</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 80%;" colspan="3">
                        <table style="width: 100%;">
                            <tr>
                                <td colspan="3">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20%"><span class="field-label">Current Outstanding</span></td>
                                            <td id="tdOS" runat="server" width="40%">
                                                <asp:GridView ID="gvOS" CssClass="table table-bordered table-row Mygrid" runat="server" AutoGenerateColumns="False" EmptyDataText="Outstanding details not available">
                                                    <Columns>
                                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Type" />
                                                        <asp:BoundField DataField="AMOUNT" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:#,##0.00}" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                            <td width="40%">
                                                <asp:Label ID="lblPP" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Sibling(s) </span></td>
                                <td colspan="2">
                                    <telerik:RadComboBox ID="ddlSiblings" runat="server" Filter="Contains" Width="100%" RenderMode="Lightweight" ZIndex="2000" AutoPostBack="true" ToolTip="Type in student id or name"></telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Reason <span class="text-danger">*</span></span></td>
                                <td colspan="2">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <telerik:RadComboBox ID="ddlfollowupReasons" runat="server" Filter="Contains" Width="100%" RenderMode="Lightweight" ZIndex="2000" AutoPostBack="true"></telerik:RadComboBox>
                                            <br />
                                            <span id="reasontext" runat="server" style="font-style: italic; font-size: smaller;">(Last reason selected, to change please click on the reason)</span>
                                            <table>
                                                <tr id="rbPayTyp" runat="server" visible="false">
                                                    <td></td>
                                                    <td colspan="2">

                                                        <asp:RadioButton ID="rbPP" runat="server" GroupName="SHOW4" Text="Payment Plan"></asp:RadioButton>
                                                        <asp:RadioButton ID="rbPDC" runat="server" GroupName="SHOW4" Text="PDC"></asp:RadioButton>

                                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="pnlPayment"
                                                            Position="Bottom" TargetControlID="rbPP">
                                                        </ajaxToolkit:PopupControlExtender>

                                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" PopupControlID="pnlPayment"
                                                            Position="Bottom" TargetControlID="rbPDC">
                                                        </ajaxToolkit:PopupControlExtender>

                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>



                            <tr>
                                <td align="left" style="width: 20%;"><span class="field-label">Comments <span class="text-danger">*</span></span>
                                </td>

                                <td align="left" width="50%">
                                    <asp:TextBox ID="txtComments" runat="server" MaxLength="750" TextMode="MultiLine"></asp:TextBox>
                                </td>
                                <td width="30%">
                                    <asp:CheckBox ID="chkCopyComments" Text="Apply reason & comments to sibling(s)" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 20%;"><span class="field-label">Next Follow Up Date</span></td>

                                <td align="left" width="50%">
                                    <asp:UpdatePanel ID="UP1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox ID="chkFollowup" runat="server" AutoPostBack="true" />
                                            <telerik:RadDatePicker ID="txtFollowupDate" runat="server" Width="50%" DateInput-EmptyMessage="Followup.Date" Calendar-EnableWeekends="true">
                                                <DateInput ID="DateInput2" EmptyMessage="Followup.Date" DateFormat="dd/MMM/yyyy" runat="server">
                                                </DateInput>
                                                <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                                    ViewSelectorText="x" runat="server">
                                                </Calendar>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td width="30%">
                                    <asp:CheckBox ID="chkCopyFollowupDate" Text="Apply this follow up date to sibling(s)" runat="server" />
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="3" align="center">

                        <asp:CheckBox ID="chkSendEmail" runat="server" Text="Send Email" AutoPostBack="true" CssClass="field-label"></asp:CheckBox>

                        <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Save" />
                        <asp:Button ID="btnEmail" CssClass="button" runat="server" Text="Email Preview" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="gvComments" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" EmptyDataText="No Comments added to date for the student" AllowPaging="True" PageSize="10">
                            <Columns>

                                <asp:BoundField DataField="Date" HeaderText="Date" />
                                <asp:BoundField DataField="Reason" HeaderText="Reason" />
                                <asp:BoundField DataField="Comments" HeaderText="Comments" />
                                <asp:BoundField DataField="Followup Date" HeaderText="Followup Date" />
                                <asp:BoundField DataField="Followed Up On" HeaderText="Followed Up On" />
                                <asp:BoundField DataField="Followed Up By" HeaderText="Followed Up By" />
                                <asp:BoundField DataField="Added On" HeaderText="Added On" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss}" />
                                <asp:BoundField DataField="Added By" HeaderText="Added By" />

                                <asp:TemplateField HeaderText="Communication">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnEmail" runat="server" CssClass="font-weight-bold" OnClick="lbtnEmail_Click" Visible='<%# Bind("DFC_bSEND_EMAIL")%>'>Email</asp:LinkButton>
                                        <asp:Label ID="lblDFC_ID" runat="server" Text='<%# Bind("DFC_ID")%>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>




        </div>

        <asp:Panel ID="pnlPayment" Style="display: none" runat="server" CssClass="panel-cover" Width="50%">
            <table border="1" align="center"
                bgcolor="#ffffff">
                <tr class="title-bg">
                    <th align="left" colspan="1" class="matters_Colln">Amount
                    </th>
                    <th align="left" colspan="1" class="matters_Colln">Date
                    </th>
                    <th align="center" colspan="1">
                        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif"
                            OnClientClick="return HideAll();" TabIndex="54" Style="text-align: right" />
                    </th>
                </tr>
                <tr>

                    <td align="left" style="font-size: 12pt">
                        <asp:TextBox ID="txtAmount1" runat="server" Style="width: 80% !important"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount1" runat="server" FilterType="Numbers, Custom"
                            ValidChars="." TargetControlID="txtAmount1" />
                    </td>
                    <td align="left" style="font-size: 12pt">

                        <telerik:RadDatePicker ID="RadDatePicker1" runat="server" Width="50%" DateInput-EmptyMessage="Date" Calendar-EnableWeekends="true">
                            <DateInput ID="DateInput1" EmptyMessage="Date" DateFormat="dd/MMM/yyyy" runat="server">
                            </DateInput>
                            <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" runat="server">
                            </Calendar>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>

                    <td align="left">
                        <asp:LinkButton ID="LinkButton11" runat="server" TabIndex="60" OnClientClick="HideCard('trCrCard2',2);return false;">Add</asp:LinkButton>
                        <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/close_red.gif"
                            OnClientClick="return HideAll();" TabIndex="61" />
                        <asp:HiddenField ID="hfCard1" runat="server" />
                    </td>
                </tr>
                <tr id="trCrCard2" style="display: none">
                    <td align="left" style="font-size: 12pt">
                        <asp:TextBox ID="txtAmount2" runat="server" Style="width: 80% !important"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                            ValidChars="." TargetControlID="txtAmount2" />
                    </td>
                    <td align="left" style="font-size: 12pt">
                        <telerik:RadDatePicker ID="RadDatePicker2" runat="server" Width="50%" DateInput-EmptyMessage="Date" Calendar-EnableWeekends="true">
                            <DateInput ID="DateInput3" EmptyMessage="Date" DateFormat="dd/MMM/yyyy" runat="server">
                            </DateInput>
                            <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" runat="server">
                            </Calendar>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>
                    <td align="left" style="font-size: 12pt; width: auto">
                        <asp:LinkButton ID="LinkButton12" runat="server" TabIndex="67" OnClientClick="HideCard('trCrCard3',3);return false;">Add</asp:LinkButton>
                        <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/close_red.gif"
                            OnClientClick="HideCardRow('trCrCard2',1);return false;" TabIndex="68" />
                        <asp:HiddenField ID="hfCard2" runat="server" />
                    </td>
                </tr>
                <tr id="trCrCard3" style="display: none">
                    <td align="left" style="font-size: 12pt">
                        <asp:TextBox ID="txtAmount3" runat="server" Style="width: 80% !important"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, Custom"
                            ValidChars="." TargetControlID="txtAmount3" />
                    </td>
                    <td align="left" style="font-size: 12pt">
                        <telerik:RadDatePicker ID="RadDatePicker3" runat="server" Width="50%" DateInput-EmptyMessage="Date" Calendar-EnableWeekends="true">
                            <DateInput ID="DateInput4" EmptyMessage="Date" DateFormat="dd/MMM/yyyy" runat="server">
                            </DateInput>
                            <Calendar ID="Calendar4" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" runat="server">
                            </Calendar>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>
                    <td align="left" style="font-size: 12pt">
                        <asp:LinkButton ID="LinkButton1" runat="server" TabIndex="67" OnClientClick="HideCard('trCrCard4',3);return false;">Add</asp:LinkButton>
                        <asp:ImageButton ID="ImageButton6" runat="server" TabIndex="74" ImageUrl="~/Images/close_red.gif"
                            OnClientClick="HideCardRow('trCrCard3',1);return false;" />
                        <asp:HiddenField ID="hfCard3" runat="server" />
                    </td>
                </tr>
                <tr id="trCrCard4" style="display: none">
                    <td align="left" style="font-size: 12pt">
                        <asp:TextBox ID="txtAmount4" runat="server" Style="width: 80% !important"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                            ValidChars="." TargetControlID="txtAmount4" />
                    </td>
                    <td align="left" style="font-size: 12pt">
                        <telerik:RadDatePicker ID="RadDatePicker4" runat="server" Width="50%" DateInput-EmptyMessage="Date" Calendar-EnableWeekends="true">
                            <DateInput ID="DateInput5" EmptyMessage="Date" DateFormat="dd/MMM/yyyy" runat="server">
                            </DateInput>
                            <Calendar ID="Calendar5" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" runat="server">
                            </Calendar>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>
                    <td align="left" style="font-size: 12pt">
                        <asp:ImageButton ID="ImageButton1" runat="server" TabIndex="74" ImageUrl="~/Images/close_red.gif"
                            OnClientClick="HideCardRow('trCrCard4',1);return false;" />
                        <asp:HiddenField ID="hfCard4" runat="server" />
                    </td>
                </tr>
            </table>

        </asp:Panel>
        <asp:HiddenField ID="h_CrCard" runat="server" />


        <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
            <div class="panel-cover inner_darkPanlAlumini">
                <div>
                    <asp:Button ID="btClose" type="button" runat="server" Visible="false"
                        Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                        ForeColor="White" Text="X"></asp:Button>
                    <div>
                        <div align="CENTER">
                            <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </div>

                        <table align="center" width="100%" cellpadding="2" cellspacing="0" width="100%">
                            <tr>

                                <td align="left" width="50%">
                                    <telerik:RadEditor ID="txtEmailText" runat="server" EditModes="All"
                                        Height="600px" ToolsFile="/PHOENIXBETA/masscom/xml/FullSetOfTools.xml">
                                    </telerik:RadEditor>
                                </td>

                                <td align="left" style="vertical-align: top;">
                                    <asp:Label ID="lblEmail_preview" class="field-value" runat="server" Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btnPreview" Text="Email Preview" CssClass="button" runat="server" />
                                    <asp:CheckBox ID="chk_prm_contact" Checked="true" Enabled="false" runat="server" GroupName="SHOW7" Text="Primary Contact"></asp:CheckBox>
                                    <asp:CheckBox ID="chk_sec_contact" runat="server" GroupName="SHOW7" Text="Secondary Contact"></asp:CheckBox>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btnSendEmail" Text="Save & Close" CssClass="button" runat="server" />
                                    <asp:Button ID="btnUClose" Text="Cancel" CssClass="button" runat="server" />
                                    <asp:LinkButton ID="btnHidden" runat="server"></asp:LinkButton>
                                    <ajaxToolkit:ModalPopupExtender ID="MPE1" BackgroundCssClass="ModalPopupBG" TargetControlID="btnHidden"
                                        PopupControlID="pnlEmail" OkControlID="btnClose" CancelControlID="btnCancel"
                                        DropShadow="true" PopupDragHandleControlID="gridheader" Drag="true" runat="server">
                                    </ajaxToolkit:ModalPopupExtender>
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlEmail" Style="display: none;" runat="server">
            <div class="panel-cover" style="padding: 0 15px 15px 15px">
               
                <div class="row">
                    <div class=" col-12 title-bg">
                        Confirmation
                        <asp:Button ID="btnClose" CssClass="button_small float-right"  runat="server" Text="X"  />
                    </div>
                </div>
                <div class="row">
                    <div class=" col-12 mt-4 mb-4" >
                        <asp:Label ID="lblMessage" runat="server" Text="" ></asp:Label>
                    </div>
                </div>
                <div style="text-align: center">
                    <asp:Button ID="btnOkay" CssClass="button btn-yes"  runat="server" Text="Yes" />
                    <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" />
                </div>
            </div>
        </asp:Panel>


        <asp:Panel ID="pnlemailhistory" runat="server" CssClass="darkPanlAlumini" Visible="false">
            <div class="panel-cover inner_darkPanlAlumini">
                <div>

                    <div>
                        <div align="CENTER">
                            <asp:Label ID="Label1" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </div>

                        <table align="center" cellpadding="2" cellspacing="0" width="60%">
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btn_EHclose" CssClass="button" runat="server" Text="Close" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_emailHistory" class="field-value" runat="server" Width="650px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btn_EHclose2" CssClass="button" runat="server" Text="Close" />
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>
        </asp:Panel>


    </form>
    <script type="text/javascript">
        function DisableDate() {
            var bFollowup = document.getElementById('<%=chkFollowup.ClientID%>').checked;
            if (bFollowup == true)
                document.getElementById('<%= txtFollowupDate.ClientID%>' + '$' + 'DateInput').attributes = true;
            else
                document.getElementById('<%= txtFollowupDate.ClientID%>' + '$' + 'DateInput').readOnly = false;
        }
    </script>
</body>
</html>
