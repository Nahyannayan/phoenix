Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports Microsoft.ApplicationBlocks.Data


Partial Class Fees_FeeTransportAdjustment_REQ
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim bShow As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        lblalert.CssClass = "divinfo"

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_TRANSPORT Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Panel1.Visible = False
            ddBusinessunit.DataBind()
            If Not ddBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")) Is Nothing Then
                ddBusinessunit.ClearSelection()
                ddBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")).Selected = True
            End If

            InitialiseCompnents()

            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            txtStudName.Attributes.Add("ReadOnly", "ReadOnly")
            txtHeaderRemarks.Attributes.Add("OnBlur", "FillDetailRemarks()")
            Session("sFEE_ADJUSTMENT") = Nothing
            txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)

            If (ddlAdjReason.SelectedItem.Text = "Non-Revenue Transfer Between Heads" Or ddlAdjReason.SelectedItem.Text = "Non-Revenue Adjustment") Then
                ddlTAX.Enabled = False
            Else
                ShowTAXControl()
            End If


            If ViewState("datamode") = "view" Then
                Dim FAR_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FAR_ID").Replace(" ", "+"))
                Dim vFEE_ADJ As FEEADJUSTMENTREQTransport = FEEADJUSTMENTREQTransport.GetFeeAdjustments(FAR_ID)
                txtDate.Text = Format(vFEE_ADJ.FAH_DATE, OASISConstants.DateFormat)
                txtHeaderRemarks.Text = vFEE_ADJ.FAH_REMARKS
                radEnq.Checked = vFEE_ADJ.IsEnquiry
                txtStudName.Text = vFEE_ADJ.FAR_STU_NAME
                h_STUD_ID.Value = vFEE_ADJ.FAH_STU_ID
                If Not ddBusinessunit.Items.FindByValue(vFEE_ADJ.FAR_STU_BSU_ID) Is Nothing Then
                    ddBusinessunit.SelectedIndex = -1
                    ddBusinessunit.Items.FindByValue(vFEE_ADJ.FAR_STU_BSU_ID).Selected = True
                End If

                FillGradeDetails(vFEE_ADJ.FAH_STU_ID)
                If Not ddlGrade.Items.FindByValue(vFEE_ADJ.FAH_GRD_ID) Is Nothing Then
                    ddlGrade.SelectedIndex = -1
                    ddlGrade.Items.FindByValue(vFEE_ADJ.FAH_GRD_ID).Selected = True
                End If
                If Not ddlAcademicYear.Items.FindByValue(vFEE_ADJ.FAH_ACD_ID) Is Nothing Then
                    ddlAcademicYear.SelectedIndex = -1
                    ddlAcademicYear.Items.FindByValue(vFEE_ADJ.FAH_ACD_ID).Selected = True
                End If

                lblArea.Text = vFEE_ADJ.SUB_LOCATION
                ViewState("viewid") = vFEE_ADJ.FAH_ID
                If Not ddlAdjReason.Items.FindByValue(vFEE_ADJ.FAR_EVENT) Is Nothing Then
                    ddlAdjReason.SelectedIndex = -1
                    ddlAdjReason.Items.FindByValue(vFEE_ADJ.FAR_EVENT).Selected = True
                End If
                If vFEE_ADJ.APPR_STATUS <> "N" Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                End If
                Session("sFEE_ADJUSTMENT") = vFEE_ADJ
                GridBindAdjustments()
                DissableAllControls(True)

            End If
        End If
    End Sub
    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Return String.Empty
    End Function
    Private Sub getAdjReason()
        ddlAdjReason.Items.Clear()
        ddlAdjReason.DataSource = FeeCommon.getADJREASON(False)
        ddlAdjReason.DataTextField = "ARM_DESCR"
        ddlAdjReason.DataValueField = "ARM_ID"
        ddlAdjReason.DataBind()
    End Sub
    Private Sub ShowTAXControl()
        Dim dtVATUserAccess As New DataTable
        Dim MainObj As Mainclass = New Mainclass()
        dtVATUserAccess = MainObj.getRecords("select count(*) ALLOW from OASIS.TAX.VW_VAT_ENABLE_USERS where USER_NAME='" & Session("sUsr_name") & "' and USER_SOURCE='FEES'", "OASIS_TRANSPORTConnectionString")
        If dtVATUserAccess.Rows.Count > 0 Then
            If dtVATUserAccess.Rows(0)("ALLOW") >= 1 Then
                ddlTAX.Enabled = True
            Else
                ddlTAX.Enabled = False
            End If
        Else
            ddlTAX.Enabled = False
        End If
    End Sub
    Private Sub DissableAllControls(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtHeaderRemarks.ReadOnly = dissble
        txtDetRemarks.ReadOnly = dissble
        txtDetAmount.ReadOnly = dissble
        ddlFeeType.Enabled = Not dissble
        ddlTAX.Enabled = Not dissble
        ddBusinessunit.Enabled = Not dissble
        ddlAcademicYear.Enabled = Not dissble
        ddlGrade.Enabled = Not dissble
        ddlAdjReason.Enabled = Not dissble
        btnDetAdd.Enabled = Not dissble
        imgDate.Enabled = Not dissble
        radEnq.Enabled = Not dissble
        radStud.Enabled = Not dissble
        imgProcess.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        'gvFeeDetails.Columns(5).Visible = Not dissble
    End Sub

    Private Sub GridBindAdjustments()
        Dim vFEE_ADJ As FEEADJUSTMENTREQTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            gvFeeDetails.DataSource = FEEADJUSTMENTREQTransport.GetSubDetailsAsDataTable(vFEE_ADJ)
            gvFeeDetails.DataBind()
        Else
            gvFeeDetails.DataSource = Nothing
            gvFeeDetails.DataBind()
        End If
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New FEEADJUSTMENTREQTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            vFEE_ADJ.FEE_ADJ_DET.Remove(CInt(lblFEE_ID.Text))
        End If
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        GridBindAdjustments()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim vFEE_ADJ_REQ As FEEADJUSTMENTREQTransport
        Dim conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim trans As SqlTransaction
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ_REQ = Session("sFEE_ADJUSTMENT")
            vFEE_ADJ_REQ.FAH_ACD_ID = ddlAcademicYear.SelectedValue
            vFEE_ADJ_REQ.IsEnquiry = radEnq.Checked
            vFEE_ADJ_REQ.FAR_BSU_ID = Session("sBSUID")
            vFEE_ADJ_REQ.FAR_STU_BSU_ID = ddBusinessunit.SelectedValue
            vFEE_ADJ_REQ.FAH_STU_ID = h_STUD_ID.Value
            vFEE_ADJ_REQ.FAH_GRD_ID = ddlGrade.SelectedValue
            vFEE_ADJ_REQ.FAH_DATE = CDate(txtDate.Text)
            vFEE_ADJ_REQ.FAH_REMARKS = txtHeaderRemarks.Text

            vFEE_ADJ_REQ.FAR_EVENT = ddlAdjReason.SelectedValue.ToString()

            trans = conn.BeginTransaction("sFEE_ADJUSTMENT")
            Dim vNewFAR_ID As Int64
            Dim retVal As Integer = FEEADJUSTMENTREQTransport.F_SAVEFEEADJREQUEST_H(vNewFAR_ID, vFEE_ADJ_REQ, Session("sUsr_name"), conn, trans)
            If retVal > 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                '-------Audit trial
                Dim str_KEY As String = "INSERT"
                Dim strRemark As String = GetTotalAmountRemarks(vFEE_ADJ_REQ, False)
                If ViewState("datamode") = "edit" Then
                    str_KEY = "EDIT"
                    strRemark = GetTotalAmountRemarks(vFEE_ADJ_REQ, True)
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, vNewFAR_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page, strRemark)
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ClearDetails()
                ViewState("datamode") = "add"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If chkPrintAfterSave.Checked Then
                    h_print.Value = "print"
                    PrintReceipt(vNewFAR_ID)
                End If
            End If
        End If
    End Sub

    Private Function GetTotalAmountRemarks(ByVal vFEE_ADJ As FEEADJUSTMENTREQTransport, ByVal bEditMode As Boolean) As String
        Dim totAmount As Double
        Dim vEnum As IDictionaryEnumerator = vFEE_ADJ.FEE_ADJ_DET.GetEnumerator()
        While vEnum.MoveNext()
            Dim vFEE_ADJ_DET As FeeAdjustmentRequestTransport_S = vEnum.Value
            If Not vFEE_ADJ_DET.bDelete Then
                totAmount += vFEE_ADJ_DET.FAD_AMOUNT
            End If
        End While
        If bEditMode Then
            Return "Total Amount :- " & vFEE_ADJ.TotalAdjustmentAmount & "changed to :- " & totAmount
        Else
            Return "New Transport Adjustment inserted with Total Amount :- " & totAmount
        End If
    End Function

    Protected Sub PrintReceipt(ByVal vFAR_ID As Integer)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETTRANSPORTFEEADJUSTMENTREQUESTFORVOUCHER]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAR_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
        sqlpFAR_ID.Value = vFAR_ID
        cmd.Parameters.Add(sqlpFAR_ID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable

        params("RPT_CAPTION") = "FEE ADJUSTMENT REQUEST"

        params("UserName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../fees/Reports/RPT/rptTransportFEEAdjustmentRequestVoucher_Request.rpt"
        Session("ReportSource") = repSource
    End Sub

    Private Sub ClearDetails()
        h_STUD_ID.Value = ""
        txtStudName.Text = ""
        txtStdNo.Text = ""
        lblArea.Text = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtHeaderRemarks.Text = ""
        ddlFeeType.ClearSelection()
        Session("sFEE_ADJUSTMENT") = Nothing
        ClearSubDetails()
        GridBindAdjustments()
        ddlGrade.DataSource = Nothing
        ddlGrade.DataBind()
        ddlFeeType.DataSource = Nothing
        ddlFeeType.DataBind()
    End Sub

    Private Sub ClearSubDetails()
        txtDetAmount.Text = ""
        txtDetRemarks.Text = ""
        ddlFeeType.SelectedIndex = -1
        btnDetAdd.Text = "Add"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        DissableAllControls(False)
        'gvFeeDetails.Columns(3).Visible = True
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        imgProcess.Enabled = True
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        Dim vFEE_ADJ As New FEEADJUSTMENTREQTransport
        If Not IsNumeric(txtDetAmount.Text) Then
            'lblError.Text = "Invalid Amount!!!"
            usrMessageBar2.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If CDec(txtDetAmount.Text) < 0 And ddlAdjReason.SelectedItem.Value <> 6 And ddlAdjReason.SelectedItem.Value <> 11 And ddlAdjReason.SelectedItem.Value <> 13 Then
            'lblError.Text = "Amount must be positive"
            usrMessageBar2.ShowNotification("Amount must be positive", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        vFEE_ADJ.FEE_ADJ_DET = AddDetails(vFEE_ADJ.FEE_ADJ_DET)
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        GridBindAdjustments()
        CalculateVAT(ddlFeeType.SelectedValue, 0, Now.Date)
    End Sub

    Private Function GetGrade(ByVal STUD_ID As String) As String
        Dim sql_query As String = "select STU_GRD_ID FROM STUDENT_M WHERE STU_ID =" & STUD_ID
        Dim connStr As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Return SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql_query)
    End Function

    Private Function AddDetails(ByVal htFEE_DET As Hashtable) As Hashtable
        Dim vFEE_DET As New FeeAdjustmentRequestTransport_S
        Dim eId As Integer = -1
        If btnDetAdd.Text = "Save" Then
            vFEE_DET = htFEE_DET(ViewState("Eid"))
            htFEE_DET.Remove(ViewState("Eid"))
            eId = ViewState("Eid")
        End If
        If ddlFeeType.SelectedValue = "" Then Return Nothing
        Dim selFeeTyp As Integer = ddlFeeType.SelectedValue
        If Not htFEE_DET(CInt(selFeeTyp)) Is Nothing Then
            'lblError.Text = "The Fee type is repeating..."
            usrMessageBar2.ShowNotification("The Fee type is repeating...", UserControls_usrMessageBar.WarningType.Danger)
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
        ElseIf FEEADJUSTMENTREQTransport.IsRepeated(vFEE_DET.FAD_ID, h_STUD_ID.Value, ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue, Session("sBSUID"), ddlFeeType.SelectedValue) Then
            'lblError.Text = "There is already approval pending for " & ddlFeeType.SelectedItem.Text
            usrMessageBar2.ShowNotification("There is already approval pending for " & ddlFeeType.SelectedItem.Text, UserControls_usrMessageBar.WarningType.Danger)
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
        Else
            vFEE_DET.FAD_AMOUNT = CDbl(txtDetAmount.Text)
            If vFEE_DET.FAD_AMOUNT = 0 Then
                'lblError.Text = "Amount is not valid..."
                usrMessageBar2.ShowNotification("Amount is not valid...", UserControls_usrMessageBar.WarningType.Danger)
                If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
                Return htFEE_DET
            End If
            vFEE_DET.FEE_TYPE = ddlFeeType.SelectedItem.Text
            vFEE_DET.FAD_FEE_ID = selFeeTyp
            vFEE_DET.FAD_TAX_CODE = ddlTAX.SelectedValue
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT tax_code,tax_perc_value from OASIS.TAX.TAX_CODES_M  where TAX_CODE='" & ddlTAX.SelectedValue & "'")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                vFEE_DET.FAD_TAX_CODE = ddlTAX.SelectedValue
                vFEE_DET.FAD_TAX_AMOUNT = ((CDbl(txtDetAmount.Text) * ds.Tables(0).Rows(0)("tax_perc_value")) / 100.0)
                vFEE_DET.FAD_NET_AMOUNT = CDbl(txtDetAmount.Text) + ((CDbl(txtDetAmount.Text) * ds.Tables(0).Rows(0)("tax_perc_value")) / 100.0)
            Else
                vFEE_DET.FAD_TAX_AMOUNT = 0.0
                vFEE_DET.FAD_NET_AMOUNT = CDbl(txtDetAmount.Text)
            End If



            'Dim ds1 As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",'')")
            'If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            '    ddlTAX.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE").ToString
            '    'lblTAX_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
            '    'lblNET_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            'End If


            'Dim selVal As String = GetFPMID(ddlFeeType.SelectedValue)
            vFEE_DET.FAD_REMARKS = txtDetRemarks.Text
            htFEE_DET(vFEE_DET.FAD_FEE_ID) = vFEE_DET
            ClearSubDetails()
        End If
        Return htFEE_DET
    End Function
    'Private Sub CalculateVAT(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",'')")
    '    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
    '        ddlTAX.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE").ToString
    '        'lblTAX_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
    '        'lblNET_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
    '    End If
    'End Sub

    Private Function GetAmountANDCounter(ByVal bcounter As Int16, ByRef week_MonthCount As Integer) As Double
        Dim conn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Try
            Dim cmd As New SqlCommand

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = Session("sbsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
            sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
            cmd.Parameters.Add(sqlpACD_ID)

            Dim sqlpDate As New SqlParameter("@Date", SqlDbType.DateTime)
            sqlpDate.Value = txtDate.Text
            cmd.Parameters.Add(sqlpDate)

            Dim sqlpReason As New SqlParameter("@REASON", SqlDbType.TinyInt)
            sqlpReason.Value = ddlAdjReason.SelectedValue
            cmd.Parameters.Add(sqlpReason)

            Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 10)
            sqlpGRD_ID.Value = ddlGrade.SelectedValue
            cmd.Parameters.Add(sqlpGRD_ID)

            Dim sqlCounter As New SqlParameter("@Counter", SqlDbType.Int)
            If bcounter = 1 Then
                sqlCounter.Value = Convert.ToInt16(txtDetAmount.Text)
            End If
            sqlCounter.Direction = ParameterDirection.InputOutput
            cmd.Parameters.Add(sqlCounter)

            Dim sqlpFEE_ID As New SqlParameter("@FEE_ID", SqlDbType.Int)
            sqlpFEE_ID.Value = ddlFeeType.SelectedValue
            cmd.Parameters.Add(sqlpFEE_ID)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            Dim sqlAmount As New SqlParameter("@Amount", SqlDbType.Decimal)
            sqlAmount.Direction = ParameterDirection.InputOutput
            If bcounter = 0 Then
                sqlAmount.Value = CDbl(txtDetAmount.Text)
            End If
            cmd.Parameters.Add(sqlAmount)

            cmd.CommandText = "FEES.FeeAdjustmentAmountFromCounter"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            conn.Open()
            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then
                Return -1
            Else
                week_MonthCount = sqlCounter.Value
                Return sqlAmount.Value
            End If
        Catch
            Return CDbl(IIf(txtDetAmount.Text = "", 0, txtDetAmount.Text))
        Finally
            conn.Close()
        End Try
    End Function

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubCancel.Click
        ClearSubDetails()
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New FEEADJUSTMENTREQTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            Dim vFEE_DET As FeeAdjustmentRequestTransport_S = vFEE_ADJ.FEE_ADJ_DET(CInt(lblFEE_ID.Text))
            If Not vFEE_DET Is Nothing Then
                ddlFeeType.SelectedIndex = -1
                If Not ddlFeeType.Items.FindByValue(vFEE_DET.FAD_FEE_ID) Is Nothing Then
                    ddlFeeType.Items.FindByValue(vFEE_DET.FAD_FEE_ID).Selected = True
                End If

                txtDetRemarks.Text = vFEE_DET.FAD_REMARKS
                If vFEE_DET.AdjustmentType = 0 Then
                    txtDetAmount.Text = vFEE_DET.FAD_AMOUNT
                Else
                    txtDetAmount.Text = vFEE_DET.DurationCount
                End If

                If Not ddlTAX.Items.FindByValue(vFEE_DET.FAD_TAX_CODE) Is Nothing Then
                    'ddlTAX.Items.FindByValue(vFEE_DET.FAD_TAX_CODE).Selected = True
                    ddlTAX.SelectedValue = vFEE_DET.FAD_TAX_CODE
                End If

                ViewState("Eid") = CInt(lblFEE_ID.Text)
                btnDetAdd.Text = "Save"
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged, ddlAdjReason.SelectedIndexChanged
        Try
            ClearDetails()
            Panel1.Visible = False
            Select Case ddlAdjReason.SelectedValue
                Case "2"
                    Panel1.Visible = True
                    radStud.Checked = True
                    radEnq.Enabled = False
                Case "11", "13"
                    ddlTAX.Enabled = False
                Case Else
                    radEnq.Enabled = True
                    ShowTAXControl()
            End Select
        Catch
        End Try
    End Sub



    Private Sub GetFeeReferenceTable(ByVal vBSU_ID As String, ByVal vACD_ID As Integer, ByVal vDate As DateTime)
        Dim cmd As New SqlCommand
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = vBSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = vACD_ID
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpDate As New SqlParameter("@Date", SqlDbType.DateTime)
        sqlpDate.Value = vDate
        cmd.Parameters.Add(sqlpDate)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FEES.GetFeeDetailsForAdjustment"

        Dim conn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim dsData As New DataSet
        Try
            cmd.Connection = conn
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(dsData)
        Finally
            conn.Close()
        End Try
        If Not dsData Is Nothing Then
            Session("sFeeRefTable") = dsData.Tables(0)
        End If
    End Sub

    Private Sub PopulateFeeType()
        If Not ddlGrade.Items Is Nothing Then
            ddlFeeType.DataSource = FEEADJUSTMENTREQTransport.PopulateFeeMaster(Session("sBSUID"))
            ddlFeeType.DataTextField = "FEE_DESCR"
            ddlFeeType.DataValueField = "FEE_ID"
            ddlFeeType.DataBind()
        End If
    End Sub

    Protected Sub imgProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgProcess.Click
        If h_STUD_ID.Value <> "" Then
            ddlGrade.Enabled = True
            Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
            FillGradeDetails(STUD_ID)
            ClearEnteredDatas()
            PopulateFeeType()
            BindArea()
        End If
    End Sub

    Private Sub BindArea()
        If txtDate.Text = "" Then Return
        GetAreaForStudent(h_STUD_ID.Value, ddlAcademicYear.SelectedValue, ddBusinessunit.SelectedValue, CDate(txtDate.Text))
    End Sub

    Private Sub GetAreaForStudent(ByVal vSTU_ID As String, ByVal vACD_ID As Integer, ByVal vBSU_ID As String, ByVal vDate As Date)
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Dim sqlStr As String = " SELECT VW_OSO_STUDENT_SERVICES_D.SSV_ID, VW_OSO_STUDENT_SERVICES_D.SSV_SBL_ID, " & _
            " TRANSPORT.SUBLOCATION_M.SBL_DESCRIPTION FROM VW_OSO_STUDENT_SERVICES_D INNER JOIN " & _
            " TRANSPORT.SUBLOCATION_M ON VW_OSO_STUDENT_SERVICES_D.SSV_SBL_ID = TRANSPORT.SUBLOCATION_M.SBL_ID " & _
            "WHERE VW_OSO_STUDENT_SERVICES_D.SSV_STU_ID = '" & vSTU_ID & "' AND " & _
            " VW_OSO_STUDENT_SERVICES_D.SSV_ACD_ID = " & vACD_ID & " AND VW_OSO_STUDENT_SERVICES_D.SSV_BSU_ID = '" & vBSU_ID & _
            "' AND '" & txtDate.Text & "' BETWEEN SSV_FROMDATE AND isnull(SSV_TODATE,'1/1/9999') "
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sqlStr)
            lblArea.Text = ""
            While (drReader.Read())
                lblArea.Text = drReader("SBL_DESCRIPTION")
                'ViewState("SSV_ID") = drReader("SSV_ID")
                'ViewState("SSV_SBL_ID") = drReader("SSV_SBL_ID")
            End While
        End Using
    End Sub

    Private Sub ClearEnteredDatas()
        ClearSubDetails()
        Session("sFEE_ADJUSTMENT") = Nothing
        GridBindAdjustments()
    End Sub

    Private Sub FillGradeDetails(ByVal STU_ID As Integer)
        Dim STU_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT
        If radEnq.Checked Then
            STU_TYP = STUDENTTYPE.ENQUIRY
        ElseIf radStud.Checked Then
            STU_TYP = STUDENTTYPE.STUDENT
        End If
        ddlGrade.DataSource = GetGradeDetails(STU_ID, STU_TYP)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Public Shared Function GetGradeDetails(ByVal STU_ID As Integer, ByVal STUD_TYP As STUDENTTYPE) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Dim sql_query As String = String.Empty
            Select Case STUD_TYP
                Case STUDENTTYPE.STUDENT
                    sql_query = " select STU_GRD_ID GRD_ID ,GRM_DISPLAY from STUDENT_M " & _
                    " INNER JOIN  dbo.VW_OSO_GRADE_M  ON GRD_ID = STUDENT_M.STU_GRD_ID " & _
                    " INNER JOIN dbo.VW_OSO_GRADE_BSU_M ON " & _
                    " VW_OSO_GRADE_BSU_M.GRM_GRD_ID = STUDENT_M.STU_GRD_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID AND " & _
                    " STU_ID = " & STU_ID & " union select " & _
                    " (select  GRD_ID from   dbo.VW_OSO_GRADE_M GRADE_M_1  " & _
                    " where  GRADE_M_1.GRD_DISPLAYORDER=VW_OSO_GRADE_M.GRD_DISPLAYORDER+1), " & _
                    " (select max(GRM_DISPLAY) from VW_OSO_GRADE_BSU_M GRADE_BSU_M,VW_OSO_GRADE_M GRADE_M_1 " & _
                    " where  GRADE_M_1.GRD_DISPLAYORDER=VW_OSO_GRADE_M.GRD_DISPLAYORDER+1 and " & _
                    " VW_OSO_GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    " GRM_GRD_ID =GRADE_M_1.GRD_ID and VW_OSO_GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID ) " & _
                    " from STUDENT_M INNER JOIN  dbo.VW_OSO_GRADE_M ON GRD_ID = STUDENT_M.STU_GRD_ID " & _
                    " INNER JOIN dbo.VW_OSO_GRADE_BSU_M ON " & _
                    " VW_OSO_GRADE_BSU_M.GRM_GRD_ID = STUDENT_M.STU_GRD_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_BSU_ID = STUDENT_M.STU_BSU_ID AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_ACD_ID = STUDENT_M.STU_ACD_ID  AND " & _
                    " VW_OSO_GRADE_BSU_M.GRM_SHF_ID = STUDENT_M.STU_SHF_ID AND " & _
                    " STU_ID = " & STU_ID
                Case STUDENTTYPE.ENQUIRY
                    sql_query = "SELECT GRD_ID, GRD_DISPLAY GRM_DISPLAY " & _
                    " FROM dbo.VW_OSO_GRADE_M WHERE GRD_DISPLAYORDER = " & _
                    " (SELECT GRADE_M.GRD_DISPLAYORDER  FROM dbo.VW_OSO_GRADE_M INNER JOIN" & _
                    " ENQUIRY_SCHOOLPRIO_S ON GRADE_M.GRD_ID = ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID " & _
                    " WHERE EQS_ID = " & STU_ID & ") "
            End Select
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            Dim dttable As New DataTable
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ClearEnteredDatas()
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        ClearEnteredDatas()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        DissableAllControls(False)
        DissableHeaderPart(True)
        Dim vFEE_ADJ As New FEEADJUSTMENTREQTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            vFEE_ADJ.bEdit = True
        End If
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub DissableHeaderPart(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtHeaderRemarks.ReadOnly = dissble
        imgDate.Enabled = Not dissble
        imgProcess.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        ddlAcademicYear.Enabled = Not dissble
        ddlGrade.Enabled = Not dissble
        ddlAdjReason.Enabled = Not dissble
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        ClearDetails()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        ClearDetails()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If ViewState("viewid") IsNot Nothing Then
            h_print.Value = "print"
            PrintReceipt(ViewState("viewid"))
        End If
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        Session("PROVIDER_BSU_ID") = ddBusinessunit.SelectedItem.Value
        FillACD()
        ClearDetails()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next

        Dim dtTAXCodes As DataTable = GetTAXCode()
        ddlTAX.DataSource = dtTAXCodes
        ddlTAX.DataTextField = "DESCR"
        ddlTAX.DataValueField = "ID"
        ddlTAX.DataBind()
    End Sub
    Function GetTAXCode() As DataTable
        Dim sql_query As String = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        If txtStdNo.Text.Trim <> "" Then
            Try
                txtStdNo.Text = txtStdNo.Text.Trim
                Dim iStdnolength As Integer = txtStdNo.Text.Length
                If iStdnolength < 9 Then
                    If txtStdNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStdNo.Text = "0" & txtStdNo.Text
                        Next
                    End If
                    txtStdNo.Text = ddBusinessunit.SelectedItem.Value & txtStdNo.Text
                End If

                If Not FeeCommon.GetStudentWithLocation_Transport(h_STUD_ID.Value, txtStdNo.Text, txtStudName.Text, _
               ddBusinessunit.SelectedItem.Value, True, "", lblArea.Text, txtDate.Text, ddlAcademicYear.SelectedValue) Then
                    'lblError.Text = "Invalid Student #/Area Not Found!!!"
                    usrMessageBar2.ShowNotification("Invalid Student #/Area Not Found!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                ddlGrade.Enabled = True
                Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
                FillGradeDetails(STUD_ID)
                ClearEnteredDatas()
                PopulateFeeType()
                BindArea()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub lbLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLedger.Click
        If h_STUD_ID.Value <> "" Then
            F_rptStudentLedger_Transport()
        End If
    End Sub

    Private Sub F_rptStudentLedger_Transport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim FromDate, ToDate As String
        ToDate = Format(Now.Date, OASISConstants.DateFormat)
        FromDate = UtilityObj.GetDataFromSQL("SELECT BSU_TRANSPORT_STARTDT FROM BUSINESSUNIT_M WHERE BSU_ID='" & ddBusinessunit.SelectedItem.Value & "'", str_conn)
        If Not IsDate(FromDate) Then
            FromDate = "1/Sep/2007"
        Else
            Format(CDate(FromDate), OASISConstants.DateFormat)
        End If

        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub1 As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub1.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure
        'ALTER procedure rptStudentLedger
        ' @BSU_ID VARCHAR(20)='125016',
        '@STU_IDS VARCHAR(2000)='',
        '@STU_TYPE VARCHAR(5)='S',
        '@FromDT DATETIME ='1-JUN-2008',
        '@ToDT DATETIME='1-JUN-2008'

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(h_STUD_ID.Value, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub1.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub1.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
        cmdSub2.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)
        cmdSub1.Parameters.AddWithValue("@STU_BSU_ID", ddBusinessunit.SelectedItem.Value)
        cmdSub2.Parameters.AddWithValue("@STU_BSU_ID", ddBusinessunit.SelectedItem.Value)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub1.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(FromDate)
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub1.Parameters.AddWithValue("@FromDT", CDate(FromDate))
        cmdSub2.Parameters.AddWithValue("@FromDT", CDate(FromDate))

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(ToDate)
        cmd.Parameters.Add(sqlpTODT)
        cmdSub1.Parameters.AddWithValue("@ToDT", CDate(ToDate))
        cmdSub2.Parameters.AddWithValue("@ToDT", CDate(ToDate))

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub1.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = FromDate
        params("ToDate") = ToDate
        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFee_Transport_StudentLedgerNext.rpt"
        Session("ReportSource") = repSource
        h_print.Value = "ledger"
        'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
    End Sub
    Private Sub CalculateVAT(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",'')")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlTAX.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE").ToString
        End If
    End Sub
    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
        CalculateVAT(ddlFeeType.SelectedValue, 0, Now.Date)
    End Sub
    Sub InitialiseCompnents()
        FillACD()
        getAdjReason()
        PopulateFeeType()
        If Not Session("BSU_bVATEnabled") Is Nothing AndAlso IsNumeric(Session("BSU_bVATEnabled")) Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        DISABLE_TAX_FIELDS()
        CalculateVAT(ddlFeeType.SelectedValue, 0, Now.Date)
        SET_VAT_DROPDOWN_RIGHT()
        ShowTAXControl()

    End Sub
    Private Sub SET_VAT_DROPDOWN_RIGHT()
        Dim bHasRight As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CASE WHEN COUNT(USER_NAME)>0 THEN 1 ELSE 0 END AS bEXIST FROM OASIS.TAX.VW_VAT_ENABLE_USERS WHERE USER_NAME='" & Session("sUsr_name").ToString & "' AND USER_SOURCE='FEES'"))
        If Not bHasRight Then
            ddlTAX.Enabled = False
            trVATInfo.VAlign = bShow
        End If
    End Sub

    Private Sub DISABLE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        ddlTAX.Enabled = bShow
        lblalert.Text = UtilityObj.getErrorMessage("642")
    End Sub

    Protected Sub txtStudentname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If h_STUD_ID.Value <> "" Then
            ddlGrade.Enabled = True
            Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
            FillGradeDetails(STUD_ID)
            ClearEnteredDatas()
            PopulateFeeType()
            BindArea()
        End If
    End Sub
End Class

