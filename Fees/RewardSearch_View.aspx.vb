﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_RewardsSearch_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studid As Integer
    Dim Studnumber As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ViewState("ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                studid = Convert.ToInt64(ViewState("ID"))


                ' Integer.TryParse(Request.QueryString("ID"), Studnumber)
                Call BindGrid()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub

    Protected Sub BindGrid()
        Try

            Dim ds As New DataSet()

            'Dim cmd As New SqlCommand
            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_LOYALITYConnectionString").ConnectionString)

            Try

                Dim cmd As New SqlCommand("[dbo].GET_REWARDS_ACCOUNT_GENERAL_DETAIL", sqlCon)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@STU_ID", SqlDbType.BigInt).Value = studid

                Dim Adapter As New SqlDataAdapter(cmd)

                Adapter.Fill(ds)
                gvStudDetailview_1.DataSource = ds.Tables(0)   '#Table 1
                gvStudDetailview_1.DataBind()

                gvStudDetailview_2.DataSource = ds.Tables(1)    '#Table 2
                gvStudDetailview_2.DataBind()

                gvStudDetailview_3.DataSource = ds.Tables(2)    '#Table 3
                gvStudDetailview_3.DataBind()

                gvStudDetailview_4.DataSource = ds.Tables(3)    '#Table 4
                gvStudDetailview_4.DataBind()

                gvStudDetailview_5.DataSource = ds.Tables(4)    '#Table 5
                gvStudDetailview_5.DataBind()

                gvStudDetailview_6.DataSource = ds.Tables(5)    '#Table 6
                gvStudDetailview_6.DataBind()



            Catch ex As Exception

                Throw ex

            Finally

                'sqlCon.Close()

                'cmd.Dispose()

            End Try

        Catch ex As Exception

        End Try

    End Sub
End Class
