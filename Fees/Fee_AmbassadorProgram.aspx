﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Fee_AmbassadorProgram.aspx.vb" Inherits="Fees_Fee_AmbassadorProgram" Title="OASIS:: GEMS AMBASSADOR PROGRAM" %>
<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OASIS:: GEMS AMBASSADOR PROGRAM</title>
    <link rel="stylesheet" href="../cssfiles/bootstrapAmbassador.css" type="text/css" media="screen" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <link href="../cssfiles/Ambassador.css" rel="stylesheet" />
    <style>
        .RadGrid_Default .rgRow td, .RadGrid_Default .rgAltRow td {
            border-color: #d0d7e5 !important;
        }
        .RadGrid_Default .rgRow td, .RadGrid_Default .rgAltRow td, .RadGrid_Default .rgEditRow td, .RadGrid_Default .rgFooter td {
            border-style: solid;
            border-width: 0 0 1px 1px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager runat="server" ID="ScriptManager1" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="width-100">
            <ContentTemplate>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="../Images/ambassdor-H.png" alt="logo" class="img-fluid" /></div>
                        <div class="col-lg-12 text-center">
                            <asp:Label ID="lbl_SchoolName" CssClass="ambassador-mainheader" runat="server" EnableViewState="True"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 dropdown-divider">&nbsp; </div>

                    </div>

                    <div class="row">
                        <%--<div  class="col-lg-12 text-right">    
                <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label></div>--%>
                        <div class="col-lg-12 text-right">
                            <asp:Label ID="lblMessage" runat="server" CssClass="ambassador-message" EnableViewState="True"></asp:Label>
                        </div>
                        <asp:HiddenField ID="Label1" runat="server" EnableViewState="False"></asp:HiddenField>
                    </div>


                    <div class="row text-left">
                        <asp:Label ID="lbl_PageTitle" CssClass="ambassador-header1" runat="server" EnableViewState="True"></asp:Label>
                    </div>
                    <%--<div  class="row text-left border"> Referral Student Details: 
             </div>
                    --%>

                    <div class="row">
                        <div class="col-8 col-lg-10 ambassador-tablefont">
                            <%-- <h1 class="">Referral Student Details: </h1>--%>
                            <div class="row border ambassador-header  ambassador-rowheader">
                                <div class="col-12 border">Referred Student Details: </div>

                            </div>
                            <div class="row border">
                                <div class="col-4 border">Student No:</div>
                                <div class="col-8">
                                    <asp:Label ID="StudentNo" runat="server" /></div>
                            </div>
                            <div class="row border">
                                <div class="col-4 border">Name:</div>
                                <div class="col-8">
                                    <asp:Label ID="Name" runat="server" /></div>
                            </div>
                            <div class="row table-bordered">
                                <div class="col-4 border">Grade:</div>
                                <div class="col-8">
                                    <asp:Label ID="Grade" runat="server" /></div>
                            </div>
                            <div class="row border">
                                <div class="col-4 border">DOJ:</div>
                                <div class="col-8">
                                    <asp:Label ID="DOJ" runat="server" /></div>
                            </div>
                            <div class="row border">
                                <div class="col-4 border">Annual Tuition Fee Amount (AED):</div>
                                <div class="col-8">
                                    <asp:Label ID="TUITION_AMT" runat="server" /></div>
                            </div>
                        </div>
                        <div class="col-4 col-lg-2 text-left">
                            <img id="cphParent_urcPInfo_repInfo_imgEmpImage_0" title='<%= Name.Text%>' src='<%= Label1.Value%>' style="height: 142px; width: 130px;" />

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 ">&nbsp; </div>

                    </div>

                    <div id="ref_parent"  runat="server" >
                    <div class="row border ambassador-header  ambassador-rowheader">
                        <div class="col-12 border">Referring Family Details: </div>

                    </div>
                    <div class="row border">

                        <div class="col-4 col-lg-2 border">Email: </div>
                        <div class="col-8 col-lg-4 border">
                            <asp:Label ID="lbl_Email" runat="server" />
                        </div>
                        <div class="col-4 col-lg-2  border">User Name: </div>
                        <div class="col-8 col-lg-4  border">
                            <asp:Label ID="lbl_UserName" runat="server" />
                        </div>
                    </div>
                    <div class="row border ">


                        <telerik:RadGrid ID="gv_stu_details" runat="server" AutoGenerateColumns="False" CellSpacing="0" GridLines="Both" 
                             AllowPaging="True" AllowSorting="True" AllowFilteringByColumn="False" Width="100%" pagesize="10">
                            <GroupingSettings CaseSensitive="false" />

                            <ClientSettings AllowKeyboardNavigation="true">
                                <KeyboardNavigationSettings AllowSubmitOnEnter="true" EnableKeyboardShortcuts="true" />
                            </ClientSettings>
                            <%--<SelectedItemStyle CssClass="rgSelectedRow" />--%>
                            <MasterTableView AllowFilteringByColumn="False">
                                <Columns>

                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column" HeaderText="Select" UniqueName="TemplateColumn1" AllowFiltering="false" Visible="false">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="rdSelect" runat="server" OnCheckedChanged="rdSelect_CheckedChanged" AutoPostBack="true" Visible="false" />

                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter BSU_SHORTNAME column" HeaderText="School" UniqueName="BSU_SHORTNAME" DataField="BSU_SHORTNAME" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter STUDENT_ID column" HeaderText="Student No" UniqueName="STU_NO" DataField="STU_NO" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter STU_NAME column" HeaderText="Student Name" UniqueName="STU_NAME" DataField="STU_NAME" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter STU_DOJ column" DataFormatString="{0:dd/MM/yyyy}" HeaderText="DOJ" UniqueName="STU_DOJ" DataField="STU_DOJ" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter GRD_DISPLAY column" HeaderText="Grade" UniqueName="GRD_DISPLAY" DataField="GRD_DISPLAY" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter FEE_AMOUNT column" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Fee Amount (AED)" UniqueName="FEE_AMOUNT" DataField="FEE_AMOUNT" ItemStyle-Width="100px" AllowFiltering="false">
                                    </telerik:GridBoundColumn>


                                </Columns>
                            </MasterTableView>
                            <HeaderStyle Font-Bold="true" />
                        </telerik:RadGrid>


                    </div>

                    <div class="row border ambassador-header  ambassador-rowheader">
                        <div class="col-4 border"></div>
                        <div class="col-4 border">Father</div>
                        <div class="col-4 border">Mother</div>
                    </div>
                    <div class="row border">
                        <div class="col-4 border">Name:</div>
                        <div class="col-4 border">
                            <asp:Label ID="P_FName" runat="server" /></div>
                        <div class="col-4 border">
                            <asp:Label ID="P_MName" runat="server" /></div>
                    </div>
                    <div class="row border">
                        <div class="col-4 border">Contact:</div>
                        <div class="col-4 border">
                            <asp:Label ID="P_FContact" runat="server" /></div>
                        <div class="col-4 border">
                            <asp:Label ID="P_MContact" runat="server" /></div>
                    </div>
                    <div class="row border">
                        <div class="col-4 border">Organization:</div>
                        <div class="col-4 border">
                            <asp:Label ID="P_FCompany" runat="server" /></div>
                        <div class="col-4 border">
                            <asp:Label ID="P_MCompany" runat="server" /></div>
                    </div>
                    <div class="row border">
                        <div class="col-4 border">Primary Contact:</div>
                        <div class="col-4 border">
                            <asp:Label ID="P_PRI_Contact_F" runat="server" /></div>
                        <div class="col-4 border">
                            <asp:Label ID="P_PRI_Contact_M" runat="server" /></div>
                    </div>
                        </div>

                    <div id="ref_employee"  runat="server" >
                    <div class="row border ambassador-header  ambassador-rowheader">
                        <div class="col-12 border">Referring Employee Details: </div>

                    </div>

                    <div class="row border">
                        <div class="col-4 border">ID:</div>
                        <div class="col-8 border">
                            <asp:Label ID="LBL_EMP_ID" runat="server" /></div>                       
                    </div>  

                    <div class="row border">
                        <div class="col-4 border">Name:</div>
                        <div class="col-8 border">
                            <asp:Label ID="LBL_EMP_NAME" runat="server" /></div>                       
                    </div>

                    <div class="row border">
                        <div class="col-4 border">Designation:</div>
                        <div class="col-8 border">
                            <asp:Label ID="LBL_EMP_DES" runat="server" /></div>                       
                    </div>

                    <div class="row border">
                        <div class="col-4 border">Email:</div>
                        <div class="col-8 border">
                            <asp:Label ID="LBL_EMP_EMAIL" runat="server" /></div>                       
                    </div>

                    <div class="row border">
                        <div class="col-4 border">DOJ:</div>
                        <div class="col-8 border">
                            <asp:Label ID="LBL_EMP_DOJ" runat="server" /></div>                       
                    </div>
                      </div>

                    <div class="row border">

                        <div class="col-12 border">
                            <asp:Label ID="lbl_Note" CssClass="ambassador-header" runat="server" EnableViewState="True"></asp:Label></div>

                    </div>
                    <div class="row">
                        <div class="col-12 ">&nbsp; </div>

                    </div>

                     <div class="row border ambassador-header  ambassador-rowheader">
                        <div class="col-12 border">Please select the below checklists: </div>

                    </div>
                    <div class="row border">
                        <telerik:RadGrid ID="gv_chklistdetails" runat="server" AutoGenerateColumns="False" CellSpacing="0" GridLines="Both"  AllowPaging="True" AllowSorting="True" AllowFilteringByColumn="False" Width="100%"  pagesize="25"> <%--Skin="Office2007"--%>
                            <GroupingSettings CaseSensitive="false" />

                            <ClientSettings AllowKeyboardNavigation="true">
                                <KeyboardNavigationSettings AllowSubmitOnEnter="true" EnableKeyboardShortcuts="true" />
                            </ClientSettings>

                            <MasterTableView AllowFilteringByColumn="False">
                                <Columns>


                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column" HeaderText="Select" UniqueName="TemplateColumn1" AllowFiltering="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_select" runat="server" AutoPostBack="false" />

                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter ACM_DESCR column" Visible="true" HeaderText="Check List" UniqueName="ACM_DESCR" DataField="ACM_DESCR" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter ACM_ID column" Display="false" HeaderText="ACM_ID" UniqueName="ACM_ID" DataField="ACM_ID" AllowFiltering="false">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <HeaderStyle Font-Bold="true" />
                        </telerik:RadGrid>
                    </div>

                    <div id="level1_cmnt" class="row border">
                        <div class="col-sm-2 col-md-2 col-lg-2 border">
                            <asp:Label ID="hdr_Remarks1" runat="server" Text="" EnableViewState="True"></asp:Label></div>
                        <div class="col-sm-10 col-md-10 col-lg-10 border">
                            <asp:Label ID="lbl_Remarks1" runat="server" EnableViewState="True"></asp:Label></div>
                    </div>

                    <div id="level2_cmnt" class="row border">
                        <div class="col-sm-2 col-md-2 col-lg-2 border">
                            <asp:Label ID="hdr_Remarks2" runat="server" Text="" EnableViewState="True"></asp:Label></div>
                        <div class="col-sm-10 col-md-10 col-lg-10 border">
                            <asp:Label ID="lbl_Remarks2" runat="server" EnableViewState="True"></asp:Label></div>
                    </div>

                    <div class="row">
                        <div class="col-12">Comments: </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <%--     <telerik:RadTextBox ID="txt_Remarks2" Runat="server" EmptyMessage="Enter Comments" Resize="None" Width="300px" TextMode="MultiLine">
                                    </telerik:RadTextBox>--%>
                            <textarea id="txt_Remarks2" rows="3" style="width: 100%;" runat="server" />

                        </div>


                    </div>
                    <div class="spacer5"></div>
                    <div class="row mt-3">
                        <div class="col-lg-12 text-center">
                            <asp:Button ID="btn_Process" CssClass="ambassador-button" runat="server" Text="Confirm" />
                            <asp:Button ID="btn_Reject" CssClass="ambassador-button" runat="server" Text="Reject" />
                        </div>
                    </div>
                    <div class="spacer5"></div>
                </div>

                <div id="divNote" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static">
                    <span class="msgInfoclose">
                        <asp:Button ID="btn" type="button" ValidationGroup="none" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X"
                            CausesValidation="false"></asp:Button>
                    </span>
                    <asp:Label ID="lblError" runat="server" EnableViewState="false"></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>



        <asp:UpdateProgress ID="upProgGv" runat="server">
            <ProgressTemplate>
                <asp:Panel ID="pnlProgress" runat="server">
                    <br />
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /><br />
                    Please Wait....
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" HorizontalOffset="10"
                    HorizontalSide="Center" ScrollEffectDuration=".1" TargetControlID="pnlProgress"
                    VerticalOffset="10" VerticalSide="Middle">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </form>
</body>
</html>
