﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Partial Class Fees_ShowStudentInfo

    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                If Not Session("sUsr_name") Is Nothing Then
                    GridBind()
                    ''added by nahyan to hide parent mobile while accessing from dependant add ESS 04feb2016
                    Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
                    If type = "DEP" Then
                        gvGroup.Columns(5).Visible = False
                    End If

                Else
                    gvGroup.DataBind()
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');") 
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub

    Private Sub GridBind()
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID)
            Case "ENQ_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindEnquiryCompany(COMP_ID)
            Case "REFER"
                Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
                GridbindStudents(STU_ID)
            Case "REFERBB"
                Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
                GridbindStudentsBBRefer(STU_ID)
            Case "TC"
                Dim TYPE_VAL As Integer = IIf(Request.QueryString("VAL") = "", -1, Request.QueryString("VAL"))
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                If TYPE_VAL = 1 Then
                    GridbindTC(False, vACD_ID)
                ElseIf TYPE_VAL = 2 Then
                    GridbindTC(True)
                ElseIf TYPE_VAL = 3 Then
                    GridbindTC(False)
                End If
            Case "TCENQ"
                Dim TYPE_VAL As Integer = IIf(Request.QueryString("VAL") = "", -1, Request.QueryString("VAL"))
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                If TYPE_VAL = 1 Then
                    GridbindTCEnquiry(False, vACD_ID)
                ElseIf TYPE_VAL = 2 Then
                    GridbindTCEnquiry(True)
                ElseIf TYPE_VAL = 3 Then
                    GridbindTCEnquiry(False)
                End If
            Case "STUD_DATE"
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                Dim vVALDATE As String = IIf(Request.QueryString("VALDATE") = "", -1, Request.QueryString("VALDATE"))
                If Not IsDate(vVALDATE) Then
                    vVALDATE = Now.Date
                End If
                GridbindStudentWithDate(vACD_ID, vVALDATE)
            Case "ENQ_DATE"
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                GridbindStudentsENQ_DATE(vACD_ID)
            Case "CONCESSION"
                ConcessionStudents()
            Case "EOS"
                Dim EMP_ID As String
                EMP_ID = Request.QueryString("EMPID")
                getStudentOfEmployee(EMP_ID)
            Case "SERVICE_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GetServiceStudents(COMP_ID)
            Case "ALL_SOURCE_STUDENTS"
                GridBindAllSourceStudents()
            Case Else
                GridbindStudents()

        End Select
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvGroup.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub GridbindTCEnquiry(ByVal bTC As Boolean, Optional ByVal vACD_ID As Integer = 0)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            If bTC Then
                ' str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                '& " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
                '& " FROM VW_OSO_STUDENT_M INNER JOIN TCM_M ON " _
                '& " TCM_BSU_ID =STU_BSU_ID AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID " _
                '& " WHERE STU_bActive=1  AND STU_BSU_ID='" & Session("sBsuid") & "' " _
                '& " AND isnull(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' AND TCM_bCANCELLED = 0 AND " & _
                '" TCM_bRegAppr = 1 And TCM_bLabAppr = 1 And TCM_bLibAppr = 1 "
            Else
                'Select * from FEES.vw_OSO_ENQUIRY_COMP 
                str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                & " ISNULL(GRM_DISPLAY,GRD_DISPLAY)GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID " _
                & " FROM FEES.vw_OSO_ENQUIRY_COMP " _
                & " WHERE STU_BSU_ID='" & Session("sBsuid") & "' "
                If vACD_ID <> 0 Then
                    str_Sql += " AND STU_ACD_ID = " & vACD_ID
                End If
                str_Sql &= " AND STU_STATUS<>'DEL' "
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & _
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentWithDate(ByVal vACD_ID As Integer, ByVal vDate As Date)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
                & " FROM OASIS_FEES.FEES.VW_OSO_STUDENT_ADJ_VIEW   " _
                & " WHERE STU_bActive=1  AND STU_BSU_ID='" & Session("sBsuid") & "' " & _
                " AND isnull( CASE WHEN isnull(STU_CURRSTATUS, '') = 'C' " & _
                " THEN STU_CANCELDATE ELSE STU_LEAVEDATE END,getdate()+1)>= '" & vDate & "'"
            If vACD_ID <> 0 Then
                str_Sql += " AND STU_ACD_ID = " & vACD_ID
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & _
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindTC(ByVal bTC As Boolean, Optional ByVal vACD_ID As Integer = 0)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            If bTC Then
                str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
               & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
               & " FROM VW_OSO_STUDENT_M INNER JOIN TCM_M ON " _
               & " TCM_BSU_ID =STU_BSU_ID AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID " _
               & " WHERE STU_bActive=1  AND STU_BSU_ID='" & Session("sBsuid") & "' " _
               & " AND isnull(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' AND TCM_bCANCELLED = 0 AND " & _
               " TCM_bRegAppr = 1 AND ISNULL(TCM_bCANCELLED,0) = 0 " '"And TCM_bLabAppr = 1 And TCM_bLibAppr = 1 "
            Else
                str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
                & " FROM OASIS_FEES.FEES.VW_OSO_STUDENT_ADJ_VIEW   " _
                & " WHERE  STU_BSU_ID='" & Session("sBsuid") & "' "
                If vACD_ID <> 0 Then
                    str_Sql += " AND STU_ACD_ID = " & vACD_ID
                End If
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & _
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudents(Optional ByVal STUD_ID As String = "")
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If STUD_ID = "-1" Then
            STUD_ID = ""
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            & " FROM VW_OSO_STUDENT_M " _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' AND STU_ID <> '" & STUD_ID & "'" _
            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN'"
            'If STUD_ID <> "" Then
            '    str_Sql += " AND STU_SIBLING_ID <> STU_ID  AND STU_ID <> '" & STUD_ID & "'"
            '    ' & " AND STU_SIBLING_ID = ( select STU_SIBLING_ID from  STUDENT_M where STU_ID='" & STUD_ID & "')"
            'End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname & _
            str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentsBBRefer(Optional ByVal STUD_ID As String = "")
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If STUD_ID = "-1" Then
            STUD_ID = ""
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            & " FROM VW_OSO_STUDENT_M " _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' AND STU_ID <> '" & STUD_ID & "'" _
            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN'"
            'If STUD_ID <> "" Then
            '    str_Sql += " AND STU_SIBLING_ID <> STU_ID  AND STU_ID <> '" & STUD_ID & "'"
            '    ' & " AND STU_SIBLING_ID = ( select STU_SIBLING_ID from  STUDENT_M where STU_ID='" & STUD_ID & "')"
            'End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname & _
            str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentsENQ_DATE(ByVal vACD_ID As Integer)
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If vACD_ID = -1 Then
            vACD_ID = 0
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, 1 STU_bActive" _
            & " FROM FEES.vw_OSO_ENQUIRY_COMP " _
            & " WHERE  STU_BSU_ID='" & BSU_ID & "' "

            If vACD_ID <> 0 Then
                str_Sql += " AND  STU_ACD_ID= " & vACD_ID & " "
            End If
            str_Sql &= " AND STU_STATUS<>'DEL' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname & _
            str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub getStudentOfEmployee(ByVal EMP_Id As String)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_Sql, EMP_MOB_NO As String
            EMP_MOB_NO = ""
            If EMP_Id <> "" Then
                str_Sql = "select right(EMD_CUR_MOBILE,6) from EMPLOYEE_D WHERE EMD_EMP_ID='" & EMP_Id & "'"
                EMP_MOB_NO = Mainclass.getDataValue(str_Sql, "OASISConnectionString")
            End If

            str_Sql = " SELECT      STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive FROM   FEES.FEE_CONCESSION_H INNER JOIN " _
            & " VW_OSO_STUDENT_M ON FEES.FEE_CONCESSION_H.FCH_BSU_ID = VW_OSO_STUDENT_M.STU_BSU_ID AND " _
            & " FEES.FEE_CONCESSION_H.FCH_STU_ID = VW_OSO_STUDENT_M.STU_ID " _
            & " WHERE STU_bActive=1  AND STU_CURRSTATUS='EN' AND ISNULL(FCH_bDeleted,0) = 0  "
            If EMP_MOB_NO <> "" And EMP_Id <> "" Then
                str_Sql += " AND right(PARENT_MOBILE,6) = '" & EMP_MOB_NO & "'"
            Else
                str_Sql += " AND 1=2 "
            End If
            Dim str_filter_refund_acd As String = ""
            Dim SqlGroupby As String = ""
            SqlGroupby = " GROUP BY STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname & _
            str_filter_parmobile & str_filter_refund_acd & SqlGroupby & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub ConcessionStudents(Optional ByVal STUD_ID As String = "")
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If STUD_ID = "-1" Then
            STUD_ID = ""
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT      STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive FROM   FEES.FEE_CONCESSION_H INNER JOIN " _
            & " VW_OSO_STUDENT_M ON FEES.FEE_CONCESSION_H.FCH_BSU_ID = VW_OSO_STUDENT_M.STU_BSU_ID AND " _
            & " FEES.FEE_CONCESSION_H.FCH_STU_ID = VW_OSO_STUDENT_M.STU_ID " _
            & " WHERE STU_bActive=1  AND ISNULL(FCH_bDeleted,0) = 0 AND STU_BSU_ID='" & BSU_ID & "' "
            If STUD_ID <> "" Then
                str_Sql += " AND STU_SIBLING_ID <> STU_ID  AND STU_ID <> '" & STUD_ID & "'" _
                & " AND STU_SIBLING_ID = ( select STU_SIBLING_ID from  STUDENT_M where STU_ID='" & STUD_ID & "')"
            End If
            Dim str_filter_refund_acd As String = ""
            'If Request.QueryString("refund") <> "" Then
            '    str_filter_refund_acd = " AND STU_ID IN ( SELECT FAR_STU_ID FROM FEES.FEEADJREQUEST_H " _
            '    & " WHERE (FAR_BSU_ID = '" & BSU_ID & "') AND (FAR_STU_TYP = 's') AND (FAR_ApprStatus = 'A')  " _
            '    & " AND (FAR_EVENT IN ('2' )) AND (FAR_ID NOT IN (SELECT FRH_FAR_ID FROM FEES.FEE_REFUND_H))) "
            'End If
            'If Request.QueryString("acd_id") <> "" Then
            '    str_filter_refund_acd = " AND STU_ACD_ID = '" & Request.QueryString("acd_id") & "'"
            'End If  
            Dim SqlGroupby As String = ""
            SqlGroupby = " GROUP BY STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname & _
            str_filter_parmobile & str_filter_refund_acd & SqlGroupby & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindEnquiryCompany(ByVal COMP_ID As Integer)
        Try
            Dim str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
            str_Sql += " WHERE  STU_BSU_ID='" & Session("sBSUID") & "'"
            If COMP_ID <> "-1" Then
                str_Sql += " AND  COMP_ID = " & COMP_ID
            End If
            str_Sql &= " AND STU_STATUS<>'DEL' "

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
                str_Sql &= " AND STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
                If EXL_TYPE = "PI" Then
                    str_Sql &= " AND STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0  and TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                End If
            End If

            Dim str_filter_refund As String = ""
            If Request.QueryString("refund") <> "" Then
                str_filter_refund = " AND STU_ID IN ( SELECT FAR_STU_ID FROM FEES.FEEADJREQUEST_H " _
                & " WHERE (FAR_BSU_ID = '" & Session("sBSUID") & "') AND (FAR_STU_TYP = 'E') AND (FAR_ApprStatus = 'A')  " _
                & " AND (FAR_EVENT IN ('R', 'W')) AND (FAR_ID NOT IN (SELECT FRH_FAR_ID FROM FEES.FEE_REFUND_H))) "
            End If







            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_refund & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GetServiceStudents(ByVal COMP_ID As Integer)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim BSU As String = ""
            If Request.QueryString("BSU") IsNot Nothing Then
                BSU = Request.QueryString("BSU")
            End If

            Dim str_Sql As String = " SELECT STU_ID,STU_NO,STU_BSU_ID,STU_NAME,STU_GENDER, " & _
                                    "PARENT_MOBILE,PARENT_NAME,GRD_DISPLAY,STU_ACD_ID , " & _
                                    "ACY_DESCR, STU_GRD_ID, STU_bActive " & _
                                    "FROM OASIS_SERVICES.dbo.VW_OSO_STUDENT_M " & _
                                    "WHERE STU_BSU_ID = '" & BSU & "' AND " & _
                                    "STU_ID IN ( SELECT DISTINCT SSR_STU_ID FROM STUDENT_SERVICES_REQUEST WITH(NOLOCK) WHERE SSR_BSU_ID = '500610' ) "

            If Request.QueryString("STUID") IsNot Nothing Then
                Dim STUID As String = Request.QueryString("STUID")
                str_Sql = str_Sql & " AND STU_SIBLING_ID <> STU_ID AND STU_SIBLING_ID=" & STUID & ""
            End If
            If COMP_ID <> "-1" Then
                str_Sql = str_Sql & " AND ISNULL(COMP_ID,0) = " & COMP_ID
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & " ORDER BY LTRIM(RTRIM(STU_NAME))")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GridbindStudentCompany(ByVal COMP_ID As Integer)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If

            Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, GRM_DESCR,SCT_DESCR,STU_GENDER,PARENT_NAME,PARENT_MOBILE, " & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " CASE isnull(oasis.dbo.STUDENT_D.STS_FEESPONSOR,0) WHEN 4 " & _
            " THEN IsNULL(oasis.dbo.STUDENT_D.STS_F_COMP_ID, '')   " & _
            " WHEN 5 THEN IsNULL(oasis.dbo.STUDENT_D.STS_M_COMP_ID, '')   " & _
            " WHEN 6 THEN IsNULL(oasis.dbo.STUDENT_D.STS_G_COMP_ID, '')  END AS COMP_ID " & _
            " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            " dbo.STUDENT_D ON ISNULL(dbo.VW_OSO_STUDENT_M.STU_SIBLING_ID, STU_ID) = STS_STU_ID" & _
            " AND  STU_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "' )  " & _
            " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF') "

            If Request.QueryString("NewStudent") IsNot Nothing And Request.QueryString("ACD_ID") IsNot Nothing Then
                Dim NewStudent As String = Request.QueryString("NewStudent")
                Dim ACD_ID As String = Request.QueryString("ACD_ID")
                If NewStudent Then
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) >= (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE ACD_ID= " & Val(ACD_ID) & ")"
                    str_Sql &= " AND STU_ACD_ID=" & ACD_ID.ToString
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) > dateadd(month,-2,getdate()) "
                Else
                    'str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01'))  < (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE ACD_ID= " & Val(ACD_ID) & ")"
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) < dateadd(month,-2,getdate()) "
                End If
            End If

            str_Sql &= " ) a "

            If Request.QueryString("IsService") IsNot Nothing Then
                Dim IsService As String = Request.QueryString("IsService")
                If IsService Then
                    str_Sql &= " INNER JOIN (SELECT DISTINCT SSV_STU_ID FROM OASIS_FEES.FEES.VW_PROFORMA_STUD_SERVICES WHERE isnull(SSV_TODATE,getdate())>=getdate() and  SSV_BSU_ID='" & Session("sBSUID") & "' "
                    str_Sql &= " AND  SSV_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "')"
                    If Request.QueryString("FeeTypes") IsNot Nothing Then
                        Dim FeeTypes As String = Request.QueryString("FeeTypes")
                        str_Sql &= " AND  SSV_FEE_ID IN (select * from dbo.fnSplitMe('" & FeeTypes & "','|'))"
                    End If
                    str_Sql &= ")AS B ON a.STU_ID=B.SSV_STU_ID "
                End If
            End If

            str_Sql &= "WHERE a.STU_BSU_ID='" & Session("sBSUID") & "'"

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
                str_Sql &= " AND a.STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
                If EXL_TYPE = "PI" Then
                    str_Sql &= " AND a.STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0 AND TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                End If
            End If

            If COMP_ID <> "-1" Then
                str_Sql += " AND a.COMP_ID = " & COMP_ID
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & " ORDER BY " & IIf(str_filter_grade <> "", "LTRIM(RTRIM(GRM_DESCR)),LTRIM(RTRIM(SCT_DESCR))", "LTRIM(RTRIM(STU_NAME))") & " ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        Gridbind()
    End Sub

    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind()
    End Sub

    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSTU_ID As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        lblSTU_ID = sender.Parent.FindControl("lblSTU_ID")
        Dim lblCode As New Label
        lblCode = sender.Parent.FindControl("lblCode")
        If (Not lblSTU_ID Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"

            h_SelectedId.Value = lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.Students = document.getElementById('h_SelectedId').value;")
            Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value);")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        GridBind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        GridBind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        GridBind()
    End Sub


    Private Sub GridBindAllSourceStudents()
        Dim ProviderBSUID, STU_TYPE, Stu_Bsu_Id As String
        If Request.QueryString("ProviderBSUID").ToString <> "" Then
            ProviderBSUID = Request.QueryString("ProviderBSUID").ToString
        End If
        If Request.QueryString("STU_TYPE").ToString <> "" Then
            STU_TYPE = Request.QueryString("STU_TYPE").ToString
        End If
        If Request.QueryString("Stu_Bsu_Id").ToString <> "" Then
            Stu_Bsu_Id = Request.QueryString("Stu_Bsu_Id").ToString
        End If

        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String
            Dim ds As New DataSet
            Select Case ProviderBSUID
                Case "900501", "900500"
                    str_Sql = " SELECT DISTINCT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                               & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
                               & " FROM VW_OSO_STUDENT_M INNER JOIN VW_STUDENT_SERVICES_D_MAX ON STU_ID=SSV_STU_ID" _
                               & " WHERE STU_bActive=1  AND STU_BSU_ID='" & Stu_Bsu_Id & "' "
                    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                    str_Sql & str_filter_grade & str_filter_code & _
                    str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME")
                Case Else
                    If STU_TYPE = "E" Then
                        str_Sql = " SELECT DISTINCT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                                & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, 1 STU_bActive" _
                                & " FROM FEES.vw_OSO_ENQUIRY_COMP " _
                                & " WHERE  STU_BSU_ID='" & ProviderBSUID & "' "
                        str_Sql &= " AND STU_STATUS<>'DEL' "
                        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
                        str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname & _
                        str_filter_parmobile & " ORDER BY STU_NAME ")
                    Else
                        str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
                            & " FROM VW_OSO_STUDENT_M " _
                            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & ProviderBSUID & "'" _
                            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN'"
                        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
                            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname & _
                            str_filter_parmobile & " ORDER BY STU_NAME ")
                    End If

            End Select

            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
