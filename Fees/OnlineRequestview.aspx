<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="OnlineRequestview.aspx.vb" Inherits="Fees_OnlineRequestview" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="../UserControls/usrTopFilter.ascx" TagName="usrTopFilter" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Online User Request
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td class="matters" valign="top" width="100%" align="left">

                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <a id='top'></a></td>
                        <td align="right"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <th align="left" valign="middle">
                            <uc1:usrTopFilter ID="UsrTopFilter1" runat="server" />
                        </th>
                    </tr>
                    <tr valign="top">
                        <td align="center" class="matters" width="100%">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Cash Payment Vouchers found" Width="100%" AllowPaging="True" CssClass="table table-row table-bordered"
                                OnSelectedIndexChanged="gvJournal_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField HeaderText="School" SortExpression="BSU_NAME">
                                        <HeaderTemplate>
                                            School<br />
                                            <asp:TextBox ID="txtSchool" runat="server" SkinID="Gridtxt">
                                            </asp:TextBox>
                                            <asp:ImageButton ID="btnDocno" OnClick="ImageButton1_Click"
                                                runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label22" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="OUR_DT" HeaderText="Date"></asp:BoundField>
                                    <asp:TemplateField HeaderText="StudentId" SortExpression="OUR_STU_NO">
                                        <HeaderTemplate>
                                            Student Id<br />
                                            <asp:TextBox ID="txtStudentId" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click"
                                                runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("OUR_STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name" SortExpression="OUR_STU_NAME">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("OUR_STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="OUR_PHONE" HeaderText="Contact No"></asp:BoundField>
                                    <asp:BoundField DataField="OUR_EMAIL" HeaderText="Email Id"></asp:BoundField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="OUR_ID" HeaderText="OUR_ID"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />


                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />


            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>


</asp:Content>

