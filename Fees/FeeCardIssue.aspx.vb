﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
'ts
Partial Class Fees_FeeCardIssue
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Private Property STUDENTCARDISSUE() As DataTable
        Get
            Return ViewState("STUDENTCARDISSUE")
        End Get
        Set(ByVal value As DataTable)
            ViewState("STUDENTCARDISSUE") = value
        End Set
    End Property
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "F733040" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") <> "" Then
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                SetDataMode("view")
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                setModifyHeader(0)
                txtBSUId.Text = Session("sBsuid")
                SetDataMode("add")
                Dim lstDrp As New ListItem
                lstDrp = ddlSchool.Items.FindByValue(Session("sBsuid").ToString)
                If Not lstDrp Is Nothing Then
                    ddlSchool.SelectedValue = lstDrp.Value
                End If
                txtIssuedDate.Text = Format(CDate(Now.ToString), OASISConstants.DateFormat)
            End If
            setModifyvalues(0)
        End If
    End Sub
    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            STUDENTCARDISSUE = GetNBADCardIssuedDetail(p_Modifyid)
            If STUDENTCARDISSUE.Rows.Count > 0 Then
                h_SCI_ID.Value = STUDENTCARDISSUE.Rows(0)("SCI_ID").ToString
                txtStudentId.Text = STUDENTCARDISSUE.Rows(0)("STU_NO").ToString
                h_STU_ID.Value = STUDENTCARDISSUE.Rows(0)("SCI_STU_ID").ToString
                txtParentName.Text = STUDENTCARDISSUE.Rows(0)("PARENTNAME").ToString
                txtStudentName.Text = STUDENTCARDISSUE.Rows(0)("STU_NAME").ToString
                If IsDate(STUDENTCARDISSUE.Rows(0)("SCI_ISSUEDT")) Then
                    txtIssuedDate.Text = Format(STUDENTCARDISSUE.Rows(0)("SCI_ISSUEDT"), "dd/MMM/yyyy")
                Else
                    txtIssuedDate.Text = ""
                End If
                txtStatus.Text = STUDENTCARDISSUE.Rows(0).Item("STU_CURRSTATUS").ToString
                txtRemarks.Text = STUDENTCARDISSUE.Rows(0)("SCI_REMARKS").ToString
            End If
            gvStudent.DataSource = STUDENTCARDISSUE
            gvStudent.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            mDisable = False
            ViewState("datamode") = "add"
            clear_All()
            STUDENTCARDISSUE.Rows.Clear()
            gvStudent.DataSource = STUDENTCARDISSUE
            gvStudent.DataBind()
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        trGridStudent.Visible = (ViewState("datamode") = "add")
        btnItemAdd.Visible = (ViewState("datamode") = "add")
        btnItemCancel.Visible = (ViewState("datamode") = "add")
        ddlSchool.Enabled = False ' (ViewState("datamode") = "add")
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        txtIssuedDate.Enabled = Not mDisable
        txtStudentId.Enabled = Not mDisable
        imgIssuedDate.Enabled = Not mDisable
        txtRemarks.Enabled = Not mDisable
    End Sub
    Sub clear_All()
        h_SCI_ID.Value = ""
        h_STU_ID.Value = ""
        txtStudentId.Text = ""
        'txtIssuedDate.Text = Format(CDate(Now.ToString), OASISConstants.DateFormat)
        'txtRemarks.Text = ""
        txtParentName.Text = ""
        txtStudentName.Text = ""
        txtStatus.Text = ""
    End Sub
    Private Sub setModifyvalues(ByVal flag As Integer)
        If ddlSchool.SelectedValue.Length = 0 Then
            bindDropDown(ddlSchool, "Select bsu_id,bsu_name from BUSINESSUNIT_M where  BSU_ENQ_GROUP=1  and  BSU_COUNTRY_ID IN (172) AND BSU_ID<>'300001' ORDER BY bsu_name")
            ddlSchool.SelectedValue = Session("sBsuid")
            txtBSUId.Text = ddlSchool.SelectedValue
        End If
    End Sub

    Private Sub bindDropDown(ByRef ddlToBind As DropDownList, ByVal strSQL As String)
        Dim dt As DataTable = MainObj.getRecords(strSQL, "OASISConnectionString")

        ddlToBind.DataSource = dt
        ddlToBind.DataValueField = dt.Columns(0).ColumnName
        ddlToBind.DataTextField = dt.Columns(1).ColumnName
        ddlToBind.DataBind()
    End Sub
    Protected Sub ddlSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSchool.SelectedIndexChanged
        setModifyvalues(2)
    End Sub
    Private Function SaveItem() As Boolean
        Try
            If h_STU_ID.Value = "" Or txtStudentId.Text = "" Then
                '  lblError.Text = "Please enter the Student ID!!!!"
                usrMessageBar.ShowNotification("Please enter the Student ID!!!!", UserControls_usrMessageBar.WarningType.Danger)
                SaveItem = False
                Exit Function
            End If
            If Not IsDate(txtIssuedDate.Text) Then
                'lblError.Text = "Invalid Issue Date"
                usrMessageBar.ShowNotification("Invalid Issue Date", UserControls_usrMessageBar.WarningType.Danger)
                SaveItem = False
                Exit Function
            End If

            Dim mrow As DataRow
            If STUDENTCARDISSUE.Select("SCI_STU_ID=" & h_STU_ID.Value).Length > 0 And ViewState("datamode") <> "edit" Then
                ' lblError.Text = STUDENTCARDISSUE.Select("SCI_STU_ID=" & h_STU_ID.Value)(0)("STU_NAME") & " already selected in the list"
                usrMessageBar.ShowNotification(STUDENTCARDISSUE.Select("SCI_STU_ID=" & h_STU_ID.Value)(0)("STU_NAME") & " already selected in the list", UserControls_usrMessageBar.WarningType.Danger)
                Exit Function
            End If
            mrow = STUDENTCARDISSUE.NewRow
            mrow("SCI_ID") = Val(h_SCI_ID.Value)
            mrow("STU_NO") = txtStudentId.Text
            mrow("PARENTNAME") = txtParentName.Text
            mrow("SCI_BSU_ID") = txtBSUId.Text
            mrow("SCI_STU_ID") = h_STU_ID.Value
            mrow("STU_NAME") = txtStudentName.Text
            mrow("SCI_ISSUEDT") = txtIssuedDate.Text
            mrow("SCI_REMARKS") = txtRemarks.Text
            mrow("SCI_ENTEREDBY") = Session("sUsr_name")

            STUDENTCARDISSUE.Rows.Add(mrow)
            SaveItem = True
        Catch ex As Exception
            SaveItem = False
            '  lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Function
    Protected Sub btnItemAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnItemAdd.Click
        Try
            If SaveItem() Then
                clear_All()
                gvStudent.DataSource = STUDENTCARDISSUE
                gvStudent.DataBind()
            End If
        Catch ex As Exception
            '  lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub gvStudent_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStudent.RowDeleting
        Try
            Dim row As GridViewRow = gvStudent.Rows(e.RowIndex)
            Dim lblSTU_ID As New Label
            lblSTU_ID = TryCast(row.FindControl("lblSTU_ID"), Label)
            If STUDENTCARDISSUE.Select("SCI_STU_ID=" & lblSTU_ID.Text, "").Length > 0 Then
                STUDENTCARDISSUE.Select("SCI_STU_ID=" & lblSTU_ID.Text, "")(0).Delete()
            End If
            gvStudent.DataSource = STUDENTCARDISSUE
            gvStudent.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
            '  lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ViewState("datamode") = "edit" Then
                If Not SaveItem() Then Exit Sub
            End If
            Dim retval As String
            retval = SaveNBADCardIssuedDetail(STUDENTCARDISSUE)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, h_SCI_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
                '  lblError.Text = "Data Saved Successfully !!!"
                usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
                SetDataMode("view")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                '   lblError.Text = IIf(IsNumeric(retval), getErrorMessage(retval), retval)
                usrMessageBar.ShowNotification(IIf(IsNumeric(retval), getErrorMessage(retval), retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            '  lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim retval As String
            retval = DeleteNBADCardIssuedDetail(h_SCI_ID.Value)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                ' lblError.Text = "Data Deleted Successfully !!!!"
                usrMessageBar.ShowNotification("Data Deleted Successfully !!!!", UserControls_usrMessageBar.WarningType.Success)
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                '  lblError.Text = retval
                usrMessageBar.ShowNotification(retval, UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub txtStudentId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStudentId.TextChanged
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@STU_NO", txtStudentId.Text, SqlDbType.VarChar)
            sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", txtBSUId.Text, SqlDbType.VarChar)
            mTABLE = Mainclass.getDataTable("[dbo].[GetStudentDataForNBAD]", sqlParam, str_conn)
            If mTable.Rows.Count > 0 Then
                txtStudentName.Text = mTable.Rows(0).Item("STU_NAME").ToString
                txtStudentId.Text = mTable.Rows(0).Item("STU_NO").ToString
                txtParentName.Text = mTable.Rows(0).Item("PARENTNAME").ToString
                h_STU_ID.Value = mTable.Rows(0).Item("STU_ID").ToString
                txtStatus.Text = mTable.Rows(0).Item("STU_CURRSTATUS").ToString
                txtRemarks.Focus()
            Else
                If txtStudentId.Text <> "" Then usrMessageBar.ShowNotification("Invalid Student ID", UserControls_usrMessageBar.WarningType.Danger) 'lblError.Text = "Invalid Student ID"

                txtStudentName.Text = ""
                txtParentName.Text = ""
                h_STU_ID.Value = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function GetNBADCardIssuedDetail(ByVal SCI_ID As Integer) As DataTable
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SCI_ID", SCI_ID, SqlDbType.Int)
            mSet = Mainclass.getDataSet("GetNBADCardIssuedDetail", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                mTable = mSet.Tables(0)
                Return mTable
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function SaveNBADCardIssuedDetail(ByVal STUDETAIL As DataTable) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Dim sqlParam(8) As SqlParameter
            Dim isRowDeleted As Boolean = False
            Dim mRow As DataRow
            Dim Retval As String
            Dim Success As Boolean = True
            Dim ErrorMsg As String = ""
            For Each mRow In STUDETAIL.Rows
                isRowDeleted = False
                If mRow.RowState = DataRowState.Deleted Then
                    isRowDeleted = True
                    mRow.RejectChanges()
                End If
                ReDim sqlParam(8)
                sqlParam(0) = Mainclass.CreateSqlParameter("@SCI_ID", mRow("SCI_ID"), SqlDbType.Int, True)
                sqlParam(1) = Mainclass.CreateSqlParameter("@SCI_BSU_ID", mRow("SCI_BSU_ID"), SqlDbType.VarChar)
                sqlParam(2) = Mainclass.CreateSqlParameter("@SCI_STU_ID", mRow("SCI_STU_ID"), SqlDbType.VarChar)
                sqlParam(3) = Mainclass.CreateSqlParameter("@SCI_ISSUEDT", mRow("SCI_ISSUEDT"), SqlDbType.DateTime)
                sqlParam(4) = Mainclass.CreateSqlParameter("@SCI_ENTEREDBY", mRow("SCI_ENTEREDBY"), SqlDbType.VarChar)
                sqlParam(5) = Mainclass.CreateSqlParameter("@SCI_REMARKS", mRow("SCI_REMARKS"), SqlDbType.VarChar)
                sqlParam(6) = Mainclass.CreateSqlParameter("@IsDeleted", isRowDeleted, SqlDbType.Bit)
                sqlParam(7) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
                Retval = Mainclass.ExecuteParamQRY(objConn, stTrans, "[SaveNBADCardIssuedDetail]", sqlParam)
                If (Retval = "0" Or Retval = "") And sqlParam(7).Value = "" Then
                Else
                    Success = False
                    ErrorMsg = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(7).Value)
                    Exit For
                End If
            Next
            If (Success) Then
                SaveNBADCardIssuedDetail = ""
                stTrans.Commit()
            Else
                stTrans.Rollback()
                SaveNBADCardIssuedDetail = ErrorMsg
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Public Shared Function DeleteNBADCardIssuedDetail(ByVal SCI_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SCI_ID", SCI_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "DeleteNBADCardIssuedDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(1).Value = "" Then
                DeleteNBADCardIssuedDetail = ""
            Else
                DeleteNBADCardIssuedDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
End Class
