<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeeCalculateFeeTransport.aspx.vb" Inherits="Fees_FeeCalculateFeeTransport" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
 <base target="_self" />
   <script language="javascript" type="text/javascript">  
    function ReturnTotal()
        { 
            window.returnValue = document.getElementById('<%=txtTotal.ClientID %>').value+"|"+document.getElementById('<%=h_Narration.ClientID %>').value; 
            window.close();    
        }   
    </script>  
    
        <script>
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
    </script>
    
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
</head>
<body onload="listen_window();">
    <form id="form1" runat="server"> 
    <table >
        <tr >
            <td align="left" class="title-bg" colspan="2" >
                Calculate Fee
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label></td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="2" valign="top" >
                <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added" Width="100%" CssClass="table table-row table-bordered" > 
                    <RowStyle CssClass="griditem" />
                    <HeaderStyle CssClass="gridheader_new"   />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelected" runat="server" Checked="True" OnCheckedChanged="chkSelected_CheckedChanged" AutoPostBack="True" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Checked="True"
                                    OnCheckedChanged="chkSelectAll_CheckedChanged" />
                            </HeaderTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Month" HeaderText="Month" />
                        <asp:TemplateField HeaderText="Amount Paying">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmountToPay" runat="server" AutoPostBack="True" style="text-align:right" 
                                    onFocus="this.select();" TabIndex="20" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                    Width="89px" OnTextChanged="txtAmountToPay_TextChanged"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters"  >
                Total Fee
            </td>
            <td class="matters"  >
                <asp:TextBox ID="txtTotal" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="2">
    <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish" OnClick="btnFinish_Click"  />
                <asp:Button id="btnPrint" runat="server" CssClass="button" OnClientClick="javascript:window.print();" Text="Print"></asp:Button></td>
        </tr>
        </table>
        <asp:HiddenField ID="h_Narration" runat="server" />
    </form>
</body>
</html>

