<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PerformaInvoiceView5K.aspx.vb" Inherits="Fees_PerformaInvoiceView5K"  %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 
 <script language="javascript" type="text/javascript">
      Sys.Application.add_load(  
function CheckForPrint()  
     {
       if (document.getElementById('<%= h_print.ClientID %>' ).value!='')
       { 
       document.getElementById('<%= h_print.ClientID %>' ).value=''; 
      showModelessDialog ('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
       }
     } 
    );    
    function ChangeAllCheckBoxStates(checkState)
    {
        var chk_state=document.getElementById("chkAL").checked; 
       for(i=0; i<document.forms[0].elements.length; i++)
        {
           if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
            if (document.forms[0].elements[i].type=='checkbox')
            {
                document.forms[0].elements[i].checked=chk_state;
            }
        }
    }     

     function divIMG(pId,val,ctrl1,pImg)
                {
                var path;             
              
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
              
                if (pId==1)
               { 
                    document.getElementById("<%=getid("mnu_1_img") %>").src = path;
               }
               else if (pId==2)
               { 
                    document.getElementById("<%=getid("mnu_2_img") %>").src = path;
               } 
               else if (pId==3)
               { 
                    document.getElementById("<%=getid("mnu_3_img") %>").src = path;
               } 
             else if (pId==4)
               { 
                    document.getElementById("<%=getid("mnu_4_img") %>").src = path;
               }  
               else if (pId==5)
               { 
                    document.getElementById("<%=getid("mnu_5_img") %>").src = path;
               }  
               else if (pId==6)
               { 
                    document.getElementById("<%=getid("mnu_6_img") %>").src = path;                  
               }   
               else if (pId==7)
               { 
                    document.getElementById("<%=getid("mnu_7_img") %>").src = path;                    
               }   
           
              document.getElementById(ctrl1).value=val+'__'+path; 
          } 
    </script>                
  
 <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="5 K Invoice..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblError" runat="server" EnableViewState="false" SkinID="Error"></asp:Label>

    <table align="center" border="0" cellpadding="5" cellspacing="0" width="98%">
           <%-- <tr>
                <td align="right" style="text-align: left">
                    <asp:Label ID="lblError" runat="server" EnableViewState="false" SkinID="Error"></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:HyperLink id="hlAddNew" runat="server">Add New</asp:HyperLink>
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                </td>
            </tr>
        </table> 
    <table align="center" class="BlueTableView" cellpadding="5" cellspacing="0" width="98%">
            <%--<tr class="subheader_BlueTableView">
                <th align="left" colspan="4" style="height: 19px"> 5 K Invoice...</th>
            </tr>--%>
        <tr>
            <td align="left" class="matters" colspan="1" >
                &nbsp;<asp:RadioButton ID="radStudent" runat="server" GroupName="StudType" Text="Student" AutoPostBack="True" Checked="True" />
                <asp:RadioButton ID="radEnquiry" runat="server" GroupName="StudType" Text="Enquiry" AutoPostBack="True" /></td>
            <td align="left" class="matters" >
                Academic Year :
                <asp:DropDownList ID="ddAcademic" runat="server" AutoPostBack="True"
                    DataSourceID="odsGetBSUAcademicYear" DataTextField="ACY_DESCR" DataValueField="ACD_ID" SkinID="DropDownListNormal">
                </asp:DropDownList> 
            </td>
            <td align="center" class="matters" >
                <asp:LinkButton ID="linkgrpInvoice" runat="server" Visible="false" >Print Group Invoice</asp:LinkButton> 
            </td>
            <td align="right" class="matters" colspan="1" >
                <asp:LinkButton ID="LinkButton1" runat="server" Visible="false" >Print Selected</asp:LinkButton></td>
        </tr>
            <tr>
                <td align="center" class="matters" colspan="4">
                    <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" SkinID="GridViewView"
                        Width="100%" AllowPaging="True" PageSize="30"> 
                            <Columns>
                            <asp:TemplateField HeaderText="" Visible="false" ><HeaderTemplate>
                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("FPH_INOICENO") %>' />                            
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inv. No"><HeaderTemplate>
                            <TABLE style="WIDTH: 100%; HEIGHT: 100%">
                                <TBODY>
                                    <TR>
                                        <TD align=center>
                                            Inv. No</td>
                                    </tr>
                                    <TR>
                                        <TD>
                                            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                                                <TBODY>
                                                    <TR>
                                                        <TD>
                                                            <DIV id="chromemenu1" class="chromestyle">
                                                                <UL>
                                                                    <LI><A href="#" rel="dropmenu1">
                                                                        <IMG id="mnu_1_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                        <TD>
                                                            <asp:TextBox ID="txtInvNo" runat="server" SkinID="Gridtxt" Width="62px"></asp:TextBox></td>
                                                        <TD vAlign=middle>
                                                            <asp:ImageButton ID="btnInvNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblInvNo" runat="server" Text='<%# bind("FPH_INOICENO") %>'></asp:Label>                                
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date"><HeaderTemplate>
                            <TABLE style="WIDTH: 100%; HEIGHT: 100%"><TBODY><TR><TD align=center>
                                Date</TD></TR><TR><TD><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD><DIV id="chromemenu2" class="chromestyle"><UL><LI><A href="#" rel="dropmenu2">
                                <IMG id="mnu_2_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></A> </LI></UL></DIV></TD><TD><asp:TextBox id="txtDate" runat="server" Width="56px" SkinID="Gridtxt"></asp:TextBox></TD><TD vAlign=middle><asp:ImageButton id="btnNarration" onclick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label id="lblDate" runat="server" Text='<%# Bind("DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label> 
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="False" HeaderText="COMP_ID"><EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                            <asp:Label id="lblCOMP_ID" runat="server" Text='<%# bind("FPH_COMP_ID") %>'></asp:Label> 
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Company Name"><HeaderTemplate>
                            <TABLE style="WIDTH: 100%; HEIGHT: 100%"><TBODY><TR><TD align=center>
                                Company</td>
                            </tr>
                                <TR>
                                    <TD>
                                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                                            <TBODY>
                                                <TR>
                                                    <TD>
                                                        <DIV id="chromemenu3" class="chromestyle">
                                                            <UL>
                                                                <LI><A href="#" rel="dropmenu3">
                                                                    <IMG id="mnu_3_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <TD>
                                                        <asp:TextBox ID="txtCompany" runat="server" SkinID="Gridtxt" Width="98px"></asp:TextBox></td>
                                                    <TD vAlign=middle>
                                                        <asp:ImageButton ID="btnCompanySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="ImageButton1_Click" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                            </HeaderTemplate>

                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                            <asp:Label id="lblCompanyName" runat="server" Text='<%# Bind("COMP_NAME") %>'></asp:Label> 
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Academic Year"><HeaderTemplate>
                            <TABLE style="WIDTH: 1%; HEIGHT: 100%"><TBODY><TR><TD align=center> Grade</TD></TR><TR><TD><%--Change To be made here--%><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD style="HEIGHT: 12px"><DIV id="chromemenu4" class="chromestyle"><UL><LI><A href="#" rel="dropmenu4"><IMG id="mnu_4_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></A> </LI></UL></DIV></TD><TD style="HEIGHT: 12px"><asp:TextBox id="txtGrade" runat="server" Width="70px" SkinID="Gridtxt"></asp:TextBox></TD><TD style="HEIGHT: 12px" vAlign=middle><asp:ImageButton id="btnBankACSearch" onclick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE><%---Till Here--%></TD></TR></TBODY></TABLE>
                            </HeaderTemplate>

                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                            <asp:Label id="Label3" runat="server" Text='<%# Bind("GRD_SEC") %>'></asp:Label> 
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student #"><HeaderTemplate>
                            <TABLE style="WIDTH: 1%; HEIGHT: 100%">
                                <TBODY>
                                    <TR>
                                        <TD align=center>
                                            Student#</td>
                                    </tr>
                                    <TR>
                                        <TD>
                                            <%--Change To be made here--%>
                                            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                                                <TBODY>
                                                    <TR>
                                                        <TD style="HEIGHT: 12px">
                                                            <DIV id="chromemenu5" class="chromestyle">
                                                                <UL>
                                                                    <LI><A href="#" rel="dropmenu5">
                                                                        <IMG id="mnu_5_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></a> </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                        <TD style="HEIGHT: 12px">
                                                            <asp:TextBox ID="txtStudNo" runat="server" SkinID="Gridtxt" Width="60px"></asp:TextBox></td>
                                                        <TD style="HEIGHT: 12px" vAlign=middle>
                                                            <asp:ImageButton ID="btnStudNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <%---Till Here--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                                                <asp:Label ID="lblStudNo" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                                                            
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Name"><HeaderTemplate>
                            <TABLE style="WIDTH: 100%; HEIGHT: 100%">
                                <TBODY>
                                    <TR>
                                        <TD align=center>
                                            Student Name</TD></TR><TR><TD><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD><DIV id="chromemenu6" class="chromestyle"><UL><LI><A href="#" rel="dropmenu6">
                                            <IMG id="mnu_6_img" alt="Menu" src="../Images/operations/like.gif" align=middle border=0 runat="server" /></A></LI></UL></DIV></TD><TD><asp:TextBox id="txtStudName" runat="server" Width="88px" SkinID="Gridtxt"></asp:TextBox></TD><TD vAlign=middle><asp:ImageButton id="btnStudnameSearch" onclick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton> </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>                                
                            </HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="lblStudName" runat="server" Text='<%# bind("STUD_NAME") %>'></asp:Label>                                
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="View" Visible="False" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>                                
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Print Receipt"><ItemTemplate>
                            <asp:LinkButton id="lblPrintReciept" onclick="lblPrintReciept_Click" runat="server" >Print Invoice</asp:LinkButton> 
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns> 
                    </asp:GridView> 
                </td>
            </tr>
        </table>

    
                <p></p>
            </div>
        </div>
    </div>

            <asp:HiddenField ID="h_print" runat="server" />
<script type="text/javascript">
cssdropdown.startchrome("chromemenu1");
cssdropdown.startchrome("chromemenu2");
cssdropdown.startchrome("chromemenu3");
cssdropdown.startchrome("chromemenu4");
cssdropdown.startchrome("chromemenu5");
cssdropdown.startchrome("chromemenu6");
</script>
    <asp:ObjectDataSource ID="odsGetBSUAcademicYear" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetBSUAcademicYear" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

