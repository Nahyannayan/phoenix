<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEConcessionTrans.aspx.vb" Inherits="Fees_FEEConcessionTrans" Title="Untitled Page" Theme="General" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<%@ Register Src="../UserControls/uscStudentPicker.ascx" TagName="uscStudentPicker" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script lang="javascript" type="text/javascript">
        function GETReference(HEAD_SUB) {
            var sFeatures;
            var sFeatures;
            var NameandCode;
            var result;
            var REF_TYPE;
            var STUD_ID;
            var url;
            var h_FCT_ID_HEAD = '<%=h_FCT_ID_HEAD.ClientID %>';
            var H_REFID_HEAD = '<%=H_REFID_HEAD.ClientID %>';
            var txtRefHEAD = '<%=txtRefHEAD.ClientID %>';
            if (HEAD_SUB == 0) {
                REF_TYPE = document.getElementById(h_FCT_ID_HEAD).value;
            }


            if (REF_TYPE == "") {
                alert('Please Select Concession Type')
                return false;
            }
            STUD_ID = document.getElementById('<%=h_STUD_ID.ClientID %>').value;
            if (STUD_ID == "") {
                alert('Please Select Student')
                return false;
            }
            document.getElementById('<%= hfRefType.ClientID%>').value = HEAD_SUB
            document.getElementById('<%= hfHeadType.ClientID%>').value = REF_TYPE

            if (REF_TYPE == 1) {
                sFeatures = "dialogWidth: 750px; ";
                sFeatures += "dialogHeight: 475px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: yes; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                if ($('#<%= chkIsDifferentSchool.ClientID %>').is(':checked')) {
                    url = "ShowStudentSchoolWise.aspx?type=REFER&STU_ID=" + STUD_ID;
                }
                else {
                    url = "ShowStudent.aspx?type=REFER&STU_ID=" + STUD_ID;
                }
                var oWnd = radopen(url, "pop_getref");
            }
            else if (REF_TYPE == 2) {
                sFeatures = "dialogWidth: 560px; ";
                sFeatures += "dialogHeight: 470px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: yes; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                url = '../common/PopupFormIDhidden.aspx?iD=REFEP_CONC&MULTISELECT=FALSE&CONCTYP=' + REF_TYPE + '&CONC_FCM_ID=' + document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value;
                var oWnd = radopen(url, "pop_getref");
            }
            else {
                return false;
            }
            //result = window.showModalDialog(url, "", sFeatures)
            //if (result != '' && result != undefined) {
            //    if (HEAD_SUB == 0) {
            //        if (REF_TYPE == 1) {
            //            NameandCode = result.split('||');
            //            document.getElementById(H_REFID_HEAD).value = NameandCode[0];
            //            document.getElementById(txtRefHEAD).value = NameandCode[1];
            //        }
            //        else if (REF_TYPE == 2) {
            //            NameandCode = result.split('___');
            //            document.getElementById(H_REFID_HEAD).value = NameandCode[0];
            //            document.getElementById(txtRefHEAD).value = NameandCode[1];
            //        }
            //    }

            //    return false;
            //}
            //else {
            //    return false;
            //}
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var H_REFID_HEAD = '<%=H_REFID_HEAD.ClientID %>';
            var txtRefHEAD = '<%=txtRefHEAD.ClientID %>';



            var HEAD_SUB, REF_TYPE;
            HEAD_SUB = document.getElementById('<%= hfRefType.ClientID%>').value
            REF_TYPE = document.getElementById('<%= hfHeadType.ClientID%>').value
            var arg = args.get_argument();

            if (arg) {

                if (HEAD_SUB == 0) {
                    if (REF_TYPE == 1) {

                        NameandCode = arg.NameandCode.split('||');
                        document.getElementById(H_REFID_HEAD).value = NameandCode[0];
                        document.getElementById(txtRefHEAD).value = NameandCode[1];
                        __doPostBack(txtRefHEAD, 'TextChanged');

                        document.getElementById('<%= btn_GridLoad.ClientID %>').click();
                    }
                    else if (REF_TYPE == 2) {
                        NameCode = arg.NameCode.split('||');
                        document.getElementById(H_REFID_HEAD).value = NameCode[0];
                        document.getElementById(txtRefHEAD).value = NameCode[1];
                        __doPostBack(txtRefHEAD, 'TextChanged');

                         document.getElementById('<%= btn_GridLoad.ClientID %>').click();
                    }
                }
            }
        }
        function GetConcession(HEAD) {
            //var sFeatures, url;
            //sFeatures = "dialogWidth: 600px; ";
            //sFeatures += "dialogHeight: 500px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            var NameandCode;
     <%--var result;
     url = "../common/PopupFormIDhidden.aspx?iD=CONCESSION&MULTISELECT=FALSE";
     result = window.showModalDialog(url, "", sFeatures);
     if (result == '' || result == undefined) {
         return false;
     }
     NameandCode = result.split('___');
     if (HEAD == 1) {
         document.getElementById('<%= h_FCT_ID_HEAD.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value = NameandCode[1];
        document.getElementById('<%= txtConcession_Head.ClientID %>').value = NameandCode[2];
        document.getElementById('<%= H_REFID_HEAD.ClientID %>').value = '';
        document.getElementById('<%= txtRefHEAD.ClientID %>').value = '';
    }
    return false;
 }--%>

            var url = "../common/PopupFormIDhidden.aspx?iD=CONCESSION&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_getconcession");
            document.getElementById('<%= hfHEAD.ClientID%>').value = HEAD
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            var HEAD;
            HEAD = document.getElementById('<%= hfHEAD.ClientID%>').value;

            if (arg) {
                NameCode = arg.NameCode.split('||');
                if (HEAD == 1) {
                    document.getElementById('<%= h_FCT_ID_HEAD.ClientID %>').value = NameCode[0];
                document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value = NameCode[1];
                document.getElementById('<%= txtConcession_Head.ClientID %>').value = NameCode[2];
                document.getElementById('<%= H_REFID_HEAD.ClientID %>').value = '';
                document.getElementById('<%= txtRefHEAD.ClientID %>').value = '';

                __doPostBack('<%=txtConcession_Head.ClientID%>', 'TextChanged');
                }

            }
        }

        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
            var amt;
            amt = parseFloat(roundOff)
            if (isNaN(amt))
                amt = 2;
            return amt;
        }

        function CheckAmount(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(getRoundOff());
        }

        Sys.Application.add_load(
            function CheckForPrint() {
                var url = '../Reports/ASPX Report/RptViewerModal.aspx';
                if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                    document.getElementById('<%= h_print.ClientID %>').value = '';
                    $.fancybox({
                        width: '850px',
                        height: '700px',
                        dialogWidth: '850px',
                        dialogHeight: '700px',
                        transitionIn: 'fade',
                        transitionOut: 'fade',
                        type: 'iframe',
                        autoSize: false,
                        fitToView: true,
                        href: url
                    });
                    return false;
                }
            }
        );


        //function Popup_Show(url) {           
        //    $.fancybox({
        //        'width': '60%',
        //        'height': '650px',
        //        'autoScale': true,
        //        'transitionIn': 'fade',
        //        'transitionOut': 'fade',
        //        'type': 'iframe',
        //        'href': url
        //    });
        //};

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>



    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getconcession" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getref" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="900px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="800px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <%-- <style type="text/css">
        .divinfo {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 13px;
            font-weight: normal;
            color: #E9330C;
            letter-spacing: 1px;
            font-weight: 600;
            width: 60%;
            background-position: 5px center;
            background: #feffb3 url(../images/Common/PageBody/info_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
        }
    </style>--%>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Fee Concession For Students
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table style="width: 100%;" align="center">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALSUB" CssClass="error" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ValidationGroup="VALMAIN" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left"><span class="field-label">Student</span></td>
                        <td align="left" colspan="3">
                            <uc3:uscStudentPicker ID="uscStudentPicker1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1"><span class="field-label">Academic Year</span></td>
                        <td>
                            <asp:Label ID="lblAcdYear" runat="server"></asp:Label></td>
                        <td></td>
                        <td align="right">
                            <span class="field-label">Date</span>&nbsp
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="Image1" runat="Server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator><br />
                            <asp:CheckBox ID="chkNextAcdConc" runat="server" AutoPostBack="True" Text="Next Academic Year Concession" /><br />
                            <asp:CheckBox ID="chkCopyPrev" runat="server" Text="Copy From Previous Academic Year" AutoPostBack="True" Enabled="False" Visible="False" />
                            <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" Visible="false" OnClientClick="return false;"
                                TabIndex="15">Student Details</asp:LinkButton>
                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                PopDelay="25" PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="lnkBtnSelectmonth">
                            </ajaxToolkit:HoverMenuExtender>
                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel1"
                                Position="Left" TargetControlID="lbHistory">
                            </ajaxToolkit:PopupControlExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%;"><span class="field-label">From Date</span></td>

                        <td align="left" style="width: 30%;">
                            <asp:TextBox ID="txtFromDT" runat="server" AutoPostBack="True" OnTextChanged="txtFromDT_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="ImagefromDate" runat="server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" OnClick="ImagefromDate_Click" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFromDT"
                                ErrorMessage="From Date Reqired" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtFromDT"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator></td>
                        <td align="left" style="width: 20%;"><span class="field-label">To Date</span></td>

                        <td align="left" style="width: 30%;">
                            <asp:TextBox ID="txtToDT" runat="server" AutoPostBack="True" OnTextChanged="txtToDT_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgToDT" runat="server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" OnClick="imgToDT_Click" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtToDT"
                                ErrorMessage="To Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDT"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtToDT"
                                ControlToValidate="txtFromDT" ErrorMessage="From Date should be less than To Date"
                                Operator="LessThan" Type="Date" ValidationGroup="VALMAIN" Enabled="False">*</asp:CompareValidator></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkIsDifferentSchool" runat="server" class="field-label" Text="Is different School / Business Unit ?" />
                        </td>
                    </tr>


                    <tr>
                        <td align="left"><span class="field-label">Concession Type</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtConcession_Head" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgConcession_Head" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetConcession(1); return false;" /></td>


                        <td align="left"><span class="field-label">Ref. Details</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtRefHEAD" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgRefHead" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GETReference(0); return false;" />
                            <asp:HyperLink ID="hylReviseNo" runat="server">[hylReviseNo]</asp:HyperLink>
                            <asp:Button ID="btn_GridLoad" runat="server" CausesValidation="False" CssClass="button" Text="Add" Style="display: none" />
                            <asp:GridView ID="Grid_Sibling" runat="server" AutoGenerateColumns="true" Width="100%" EmptyDataText="No Data"
                                CssClass="table table-bordered table-row" Visible="false">
                                <RowStyle CssClass="griditem" Wrap="False"  />
                                <HeaderStyle  CssClass="field-label"/>
                                <EmptyDataRowStyle Wrap="False" />                               
                            </asp:GridView>
                        </td>

                    </tr>
                    <tr id="trTptArea" runat="server" visible="false">
                        <td align="left"><span class="field-label">Area</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtLocation" runat="server"></asp:TextBox>
                            <asp:HiddenField ID="H_Location" runat="server" />
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Narration</span></td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRemarks"
                                ErrorMessage="Please specify remarks" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator></td>

                        <td>
                            <asp:CheckBox ID="chk_rndoff" runat="server" class="field-label" Text="Auto Amount Round Off" Checked="true" /></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4" valign="middle">Concession Details 
                <asp:LinkButton ID="lbHistory" runat="server" OnClientClick="return false;">(View History)</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Fee Type</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                        <td align="left"><span class="field-label">Total Fee</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtTotalFee" runat="server" AutoCompleteType="disabled"></asp:TextBox></td>
                    </tr>
                    <tr id="trVAT" runat="server">
                        <td align="left"><span class="field-label">Tax Code</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTAX" runat="server" SkinID="DropDownListNormal" TabIndex="9" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>
                        <td colspan="2">
                            <asp:RadioButtonList ID="rblTaxCalculation" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="E"><span class="field-label">Excl.Tax</span></asp:ListItem>
                                <asp:ListItem Value="I"><span class="field-label">Incl.Tax</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="left"><span class="field-label">Charge Type</span></td>

                        <td align="left">
                            <asp:RadioButton ID="radAmount" runat="server" Text="Amount" CssClass="field-label" ValidationGroup="AMTTYPE" GroupName="ChargeType" />
                            <asp:RadioButton ID="radPercentage" runat="server" Text="Percentage" CssClass="field-label" ValidationGroup="AMTTYPE" GroupName="ChargeType" /></td>
                        <td align="left"><span class="field-label">Amount/Percentage</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="disabled"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                                ValidChars="." TargetControlID="txtAmount" />
                            <asp:LinkButton ID="lnkFill" runat="server">Auto Fill</asp:LinkButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Amount required" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Amount should be valid"
                    Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:CompareValidator></td>
                    </tr>
                     <tr>
                        <td align="left"><span class="field-label">Total Tax Amount</span></td>

                        <td align="left">
                            <asp:Label ID="lbl_tot_taxamount" runat="server" Text="0" CssClass="field-value"></asp:Label></td>
                        <td align="left"><span class="field-label">Total Net Amount</span></td>

                        <td align="left">
                            <asp:Label ID="lbl_tot_netamount" runat="server" Text="0" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr style="display: none">
                        <td align="center" colspan="4" runat="server" id="tr_Message">
                            <asp:Label ID="lblAlert" runat="server" CssClass="alert alert-info"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlView" runat="server" Enabled="false">
                    <table align="center" width="100%">
                        <tr>
                            <td align="left" class="title-bg" colspan="2">Monthly/Termly Split up</td>
                        </tr>
                        <tr>
                            <td align="center" width="100%">
                                <asp:GridView ID="gvMonthly" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="DESCR" HeaderText="Term/Month" ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAmount" runat="server" AutoPostBack="True" AutoCompleteType="Disabled" Text='<%# Bind("CUR_AMOUNT") %>'
                                                    onblur="return CheckAmount(this);" onFocus="this.select();" Style="text-align: right" OnTextChanged="txtDAmount_TextChanged"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                                                    ValidChars="." TargetControlID="txtAmount" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tax Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTaxAmount" runat="server" Text='<%# Bind("TAX_AMOUNT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Net Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetAmount" runat="server" Text='<%# Bind("NET_AMOUNT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Charge Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtChargeDate" runat="server" Text='<%# Bind("FDD_DATE", "{0:dd/MMM/yyyy}") %>' Width="75%"></asp:TextBox>
                                                <asp:ImageButton ID="imgChargeDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                    TabIndex="4" />
                                                <ajaxToolkit:CalendarExtender ID="CalChargeDate" runat="server" CssClass="MyCalendar"
                                                    Format="dd/MMM/yyyy" PopupButtonID="imgChargeDate" PopupPosition="TopLeft" TargetControlID="txtChargeDate">
                                                </ajaxToolkit:CalendarExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pro Rate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProRateAmount" runat="server" Text='<%# Bind("PRO_AMOUNT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Actual Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblActualAmount" runat="server" Text='<%# Bind("FDD_AMOUNT") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center">
                                <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALSUB" />
                                <asp:Button ID="btnDetCancel" runat="server" CssClass="button" Text="Cancel" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="title-bg">Fee Concession Details
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gvFeeDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data Added Yet...">
                                    <Columns>
                                        <asp:TemplateField Visible="False" HeaderText="FEE_ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFCD_ID" runat="server" Text='<%# bind("FCD_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Concession Type">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# bind("FCM_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fee Type">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# bind("FEE_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount Type">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# bind("AMT_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Percentage">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# bind("PERCENTAGE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# bind("AMOUNT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tax Amount">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("TAXAMOUNT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NET Amount">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("NETAMOUNT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBtnDelete" runat="server" OnClick="lnkBtnDelete_Click">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>

                    </table>

                </asp:Panel>

                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALMAIN" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" OnClick="btnPrint_Click" Text="Print" />
                        </td>
                    </tr>
                </table>

                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image1" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="Calendarextender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDT" TargetControlID="txtToDT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="Calendarextender2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImagefromDate" TargetControlID="txtFromDT">
                </ajaxToolkit:CalendarExtender>
                <asp:Button ID="hiddenTargetControlForModalPopup" runat="server" Style="display: none" /><ajaxToolkit:ModalPopupExtender
                    ID="programmaticModalPopup" runat="server" BackgroundCssClass="modalBackground"
                    BehaviorID="programmaticModalPopupBehavior" DropShadow="True" PopupControlID="programmaticPopup"
                    PopupDragHandleControlID="programmaticPopupDragHandle" RepositionMode="RepositionOnWindowScroll"
                    TargetControlID="hiddenTargetControlForModalPopup">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="programmaticPopup" runat="server" CssClass="panel-cover" Width="300px" Style="display: none">

                    <br />
                    There is already one or more concession availed for the Reference Selected.Please
        check the history befrore saving?<br />
                    <div align="center">
                        <asp:Button ID="ButtonOk" runat="server" CssClass="button" Text="OK" Width="29px" />&nbsp;
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="h_FCT_ID_HEAD" runat="server" />
                <asp:HiddenField ID="h_FCM_ID_HEAD" runat="server" />
                <asp:HiddenField ID="hfHEAD" runat="server" />
                <asp:HiddenField ID="hfHeadType" runat="server" />
                <asp:HiddenField ID="hfRefType" runat="server" />
                <asp:HiddenField ID="H_REFID_HEAD" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
                <asp:Panel ID="Panel1" runat="server" Style="display: block" class="panel-cover">
                    <table align="center" width="100%">
                        <tr>
                            <td align="left">
                                <asp:GridView ID="gvHistory" runat="server" Width="100%" AutoGenerateColumns="False" SkinID="GridViewView" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:BoundField DataField="FCH_DT" HeaderText="Date" SortExpression="FCH_DT" />
                                        <asp:BoundField DataField="FCD_AMOUNT" HeaderText="Amount" SortExpression="FCD_AMOUNT" />
                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee" SortExpression="FEE_DESCR" />
                                        <asp:BoundField DataField="FCM_DESCR" HeaderText="Concession" SortExpression="FCM_DESCR" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelTree" runat="server" CssClass="Visibility_none" Style="display: none">
                    <div class="panel-cover" style="text-align: left">
                        <div style="text-align: center">
                            <asp:GridView ID="gvStudentDetails" runat="server" RowStyle-HorizontalAlign="Left" CssClass="table table-bordered table-row" Width="100%">
                            </asp:GridView>
                            &nbsp;
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>

