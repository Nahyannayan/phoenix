﻿Imports System.Data
Imports System.Data.SqlClient
Imports UtilityObj
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_Fee_EnableRefund
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Property VSgvStLedger() As DataTable
        Get
            Return ViewState("gvStLedger")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvStLedger") = value
        End Set
    End Property
    Private Property ACDID() As Integer
        Get
            Return ViewState("ACDID")
        End Get
        Set(ByVal value As Integer)
            ViewState("ACDID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> "F733032" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            UsrSelStudent1.IsStudent = True
            ClearAll()
        End If
    End Sub

    Protected Sub rbLEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbLEnrollment.CheckedChanged, rbLEnquiry.CheckedChanged
        UsrSelStudent1.IsStudent = rbLEnrollment.Checked
        ClearAll()
    End Sub

    Protected Sub btnEFR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEFR.Click
        EnableFeeRefund()
    End Sub

    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
        SetStudentDetail()
    End Sub

    Protected Sub btnLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLedger.Click
        StudentLedger(UsrSelStudent1.STUDENT_ID, Session("sBsuId"), txtFrom.Text, txtTo.Text)
    End Sub

    Protected Sub gvStLedger_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStLedger.PageIndexChanging
        gvStLedger.PageIndex = e.NewPageIndex
        BindLedger()
    End Sub

    Sub ClearAll()
        GetCurrentACD()
        lblError.Text = ""
        UsrSelStudent1.ACD_ID = ACDID
        UsrSelStudent1.ClearDetails()
        UsrSelStudent1_StudentNoChanged(Nothing, Nothing)
        Me.txtFrom.Text = Format(Now.Date.AddDays(-1).AddMonths(-1), OASISConstants.DateFormat)
        Me.txtTo.Text = Format(Now.Date.AddDays(-1), OASISConstants.DateFormat)
        VSgvStLedger = Nothing
        BindLedger()
    End Sub

    Sub SetStudentDetail()
        Me.lblName.Text = UsrSelStudent1.STUDENT_NAME
        Me.lblDOJ.Text = Format(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "select STU_DOJ from OASIS..STUDENT_M where STU_ID='" & UsrSelStudent1.STUDENT_ID.ToString & "'"), OASISConstants.DateFormat)
        Me.lblGrade.Text = UsrSelStudent1.STUDENT_GRADE_ID
    End Sub

    Private Sub StudentLedger(ByVal STUID As String, ByVal BSUID As String, ByVal FromDT As String, ByVal ToDT As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(STUID, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSUID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = FromDT 'DateTime.Now.AddYears(-2).ToShortDateString
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = ToDT 'DateTime.Now.ToShortDateString
        cmd.Parameters.Add(sqlpTODT)
        cmd.Connection = New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        adpt.SelectCommand = cmd
        'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adpt.SelectCommand.CommandTimeout = 0
        ds.Clear()
        adpt.Fill(ds)
        Dim bal As Decimal = 0.0
        ds.Tables(0).Columns.Add("Balance")
        ds.Tables(0).AcceptChanges()
        For Each Dr As DataRow In ds.Tables(0).Rows
            bal += Convert.ToDecimal(Dr("DEBIT")) - Convert.ToDecimal(Dr("CREDIT"))
            Dr("Balance") = String.Format("{0:n2}", bal)
        Next
        VSgvStLedger = ds.Tables(0)
        If Convert.ToString(VSgvStLedger) <> "" AndAlso VSgvStLedger.Rows.Count > 0 Then
            Me.trLedger.Visible = True
        Else
            Me.trLedger.Visible = False
        End If

        BindLedger()
    End Sub

    Sub BindLedger()
        gvStLedger.DataSource = VSgvStLedger
        gvStLedger.DataBind()
    End Sub

    Private Sub EnableFeeRefund()
        Dim pParms(4) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@EFR_BSU_ID", Session("sBsuid"), SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@EFR_STU_ID", UsrSelStudent1.STUDENT_ID, SqlDbType.VarChar)
        If rbLEnquiry.Checked = True Then
            pParms(3) = Mainclass.CreateSqlParameter("@EFR_STU_TYPE", "E", SqlDbType.VarChar)
        Else
            pParms(3) = Mainclass.CreateSqlParameter("@EFR_STU_TYPE", "S", SqlDbType.VarChar)
        End If
        pParms(4) = Mainclass.CreateSqlParameter("@EFR_LOGGEDUSER", Session("sUsr_name"), SqlDbType.VarChar)


        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "dbo.saveENABLEFEEREFUND", pParms)
            If RetVal <> "0" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                stTrans.Commit()
                Dim url = "feeRefundFee.aspx" & "?MainMnu_code=" & Encr_decrData.Encrypt("F300210") & "&datamode=" & Encr_decrData.Encrypt("add")
                lblError.Text = "Fee Refund option has been enabled for the student now. Click <a href='" & url & "'>here</a> to go Refund request screen."
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try



    End Sub

    Private Sub GetCurrentACD()
        Try
            ACDID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(ACD_ID,0)ACD_ID FROM dbo.ACADEMICYEAR_D WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuId").ToString & "'")
        Catch ex As Exception
            ACDID = 0
        End Try
    End Sub
End Class
