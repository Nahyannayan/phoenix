Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeSetupforAcademicViewBB
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F100040" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ddBusinessunit.DataBind() 
                    FillACD()
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If

    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim str_Sql, lstrOpr, str_Filter, lstrCondn1, lstrCondn2, lstrCondn4 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn4 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   from dt 
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFDate")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FSP_FROMDT", lstrCondn1)
                '   -- 1  to dt
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FEE_DESCR", lstrCondn2)
                '   -- 3   age
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAgegroup")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SBL_DESCRIPTION", lstrCondn4)
            End If
            str_Sql = "SELECT FSP.FSP_ID, FSP.FSP_ACD_ID, FEE.FEE_DESCR, FSP.FSP_AMOUNT, " _
                & " FSP.FSP_FROMDT, FSP.FSP_SBL_ID, SBL.SBL_DESCRIPTION" _
                & " FROM FEES.FEESETUP_S AS FSP INNER JOIN FEES.FEES_M AS FEE " _
                & " ON FSP.FSP_FEE_ID = FEE.FEE_ID INNER JOIN" _
                & " VV_TRANSPORT_SUBLOCATION AS SBL ON FSP.FSP_SBL_ID = SBL.SBL_ID AND SBL.BSU_ID = '" & ddBusinessunit.SelectedItem.Value & "'" _
                & " WHERE (FSP.FSP_ACD_ID = '" & ddlAcademicYear.SelectedItem.Value & "') AND (FSP.FSP_BSU_ID = '" & ddBusinessunit.SelectedItem.Value & "')" _
                & str_Filter
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtFDate")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtAgegroup")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
            txtSearch.Text = lstrCondn2
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "feeSetupforAcademicBB.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") _
                 & "&acdid=" & Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Value) & "&BSU=" & ddBusinessunit.SelectedItem.Value
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
 

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        FillACD()
    End Sub


    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        ddlAcademicYear.SelectedIndex = -1
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID").ToString).Selected = True
                Exit For
            End If
        Next
        hlAddnew.NavigateUrl = "feeSetupforAcademicBB.aspx" & "?MainMnu_code=" _
               & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add") & "&BSU=" & ddBusinessunit.SelectedItem.Value

        gridbind()
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        gridbind()
    End Sub


End Class
