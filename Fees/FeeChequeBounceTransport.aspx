<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeChequeBounceTransport.aspx.vb" Inherits="Fees_FeeChequeBounceTransport" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script lang="javascript" type="text/javascript">

        function GETCHEQUEBANK() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("PopUp.aspx?ShowType=BANK_LIST&codeorname=" + document.getElementById('<%=txtCHQBankCode.ClientID %>').value, "", sFeatures);
            url = "PopUp.aspx?ShowType=BANK_LIST&codeorname=" + document.getElementById('<%=txtCHQBankCode.ClientID %>').value;
            var oWnd = radopen(url, "pop_fee");
            <%-- if (result == '' || result == undefined)
            { return false; }
            lstrVal = result.split('-');
            document.getElementById('<%=txtCHQBankCode.ClientID %>').value = lstrVal[0];
            document.getElementById('<%=txtCHQBankDescr.ClientID %>').value = lstrVal[1];--%>
        }

        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            var chkReturn = ('<%=chkChqReturn.ClientID%>');

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].type == 'checkbox' && document.forms[0].elements[i].id != chkReturn) {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtCHQBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCHQBankDescr.ClientID %>').value = NameandCode[1];
                <%--__doPostBack('<%=txtStudName.ClientID%>', 'TextChanged');--%>
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Cheque Return"> </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span> </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                TabIndex="5">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Cheque Date From</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"><span class="field-label">To</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Issued Bank</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCHQBankCode" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GETCHEQUEBANK();return false;" />
                        </td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtCHQBankDescr" runat="server"></asp:TextBox></td>
                        <td align="left" width="30%">
                            <asp:Button ID="btnShow" runat="server" CssClass="button" Text="Search..." /></td>
                    </tr>
                </table>
                <br />

                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" style="width: 20%;"><span class="field-label">Return Date</span> </td>
                        <td align="left" style="width: 30%;">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                        <td align="left" style="width: 20%;"><span class="field-label">Bank Charge(Applies to all selected cheques)</span></td>
                        <td align="left" style="width: 30%;">
                            <asp:TextBox ID="txtBankcharge" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtBankcharge"
                                ErrorMessage="Please enter bank charge" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%;"><span class="field-label">Cheque Return</span> </td>
                        <td align="left" style="width: 80%;" colspan="3">
                            <asp:CheckBox ID="chkChqReturn" runat="server" />
                            <asp:Label ID="lblChequeReturnInfo" runat="server" Text="Matured cheque return cases(Not bounced)"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvFEEReminder" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" PageSize="15" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("FCQ_ID") %>' />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="FCQ_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCL_STU_ID" runat="server" Text='<%# Bind("FCL_STU_ID") %>'></asp:Label>
                                            <asp:Label ID="lblFCQ_ID" runat="server" Text='<%# Bind("FCQ_ID") %>'></asp:Label>
                                            <asp:Label ID="lblFCL_STU_TYPE" runat="server" Text='<%# Bind("FCL_STU_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label><br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Bank<br />
                                            <asp:TextBox ID="txtBank" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBank" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBNK_DESCRIPTION" runat="server" Text='<%# Bind("BNK_DESCRIPTION") %>'></asp:Label><br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cheque No">
                                        <HeaderTemplate>
                                            Cheque No<br />
                                            <asp:TextBox ID="txtChqno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("FCQ_CHQNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rec No">
                                        <HeaderTemplate>
                                            Rec. No.<br />
                                            <asp:TextBox ID="txtRecno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSectionSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# bind("FCL_RECNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# bind("StudentAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtNarration" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender
                    ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgDate"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender
                    ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender
                    ID="calFromDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_Selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_Selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_Selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_Selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_Selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_Selected_Value" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

