﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ResetTaxInvoice.aspx.vb" Inherits="Fees_ResetTaxInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        function LoopGrid() {
            var FSHID = '';
            var checked = false;
            var TYPE = $('#<%=hf_INVTYPE.ClientID%>').val();
            $('#<%=gvInvoices.ClientID%>').find('td')
          .each(function (row) {
              $(this).find('input')
                    .each(function (col) {
                        $ctl = $(this);
                        if ($ctl.is('input:checkbox') == true || $ctl.is('input:hidden') == true) {
                            if ($ctl.is('input:checkbox')) {
                                var isChecked = $ctl.is(":checked");
                                checked = isChecked;
                                //alert(isChecked);
                            } else if ($ctl.is('input:hidden')) {
                                if (checked == true)
                                    FSHID = FSHID + '|' + $ctl.val();
                            }
                        }
                    });
          });
            return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=' + TYPE + '&FSHID=' + FSHID + '', 'INVOICE', '55%', '85%');
            return false;
        }
    </script>

    <script language="javascript" type="text/javascript">

        function fnParent() {
            jQuery.fancybox.close();
            //ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=', 'INVOICE', '45%', '85%');
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Reset Tax Invoice
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hf_INVTYPE" Value='' runat="server" />
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <%--  <tr class="subheader_BlueTableView">
            <th align="left" style="height: 19px" valign="middle" colspan="2">Tax Invoice</th>
        </tr>--%>
                    <tr id="trBsu" runat="server">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%"> 
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                      </tr>                                              
                      <tr>
                        <td align="left"><span class="field-label">View </span></td>
                        <td>
                <asp:DropDownList ID="ddlTopFilter" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTopFilter_SelectedIndexChanged" SkinID="DropDownListNormal">
                    <asp:ListItem Selected="True" Value="10">Last 10</asp:ListItem>
                    <asp:ListItem Value="50">Last 50</asp:ListItem>
                    <asp:ListItem Value="100">Last 100</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList> </td>
                        <td align="right" colspan="2" >
                            <asp:RadioButtonList ID="rblInvType" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">All</asp:ListItem>
                                <asp:ListItem Value="INV">Invoice</asp:ListItem>
                                <asp:ListItem Value="DN">Debit Note</asp:ListItem>
                                <asp:ListItem Value="CN">Credit Note</asp:ListItem>
                                <asp:ListItem Value="ADV">Advance Invoice</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvInvoices" runat="server" AutoGenerateColumns="False" DataKeyNames="FSH_ID,FSH_STU_ID,STU_BSU_ID,bIsINVOICEE_SET" CssClass="table table-bordered table-row"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkFSH_ID" runat="server" />
                                            <asp:HiddenField ID="hf_FSH_ID" Value='<%# Bind("FSH_ID")%>' runat="server" />
                                            <asp:HiddenField ID="hf_FSH_STU_ID" Value='<%# Bind("FSH_STU_ID")%>' runat="server" />
                                            <asp:HiddenField ID="hf_bInvoiceeSet" Value='<%# Bind("bIsINVOICEE_SET")%>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FSH_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFSH_ID" runat="server" Text='<%# Bind("FSH_ID")%>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="InvoiceNo">
                                        <HeaderTemplate>
                                            Invoice No
                                            <br />
                                            <asp:TextBox ID="txtInvoiceNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblInvNo" runat="server" Text='<%# Bind("FSH_INVOICE_NO")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="InvoiceDate">
                                        <HeaderTemplate>
                                            Date
                                            <br />
                                            <asp:TextBox ID="txtInvDate" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblInvDate" runat="server" Text='<%# Bind("FSH_DATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StudentId">
                                        <HeaderTemplate>
                                            StudentId
                                            <br />
                                            <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StudentName">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnConcession" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("FSH_GRD_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Unit Short-Name">
                                        <HeaderTemplate>
                                            Unit Short-Name
                                            <br />
                                            <asp:TextBox ID="txtStuBsuShort" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStuBsuShort" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuBsuShort" runat="server" Text='<%# Bind("STU_BSU_SHORT")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FSH_AMOUNT" HeaderText="Amount" DataFormatString="{0:n}" HtmlEncode="false" ItemStyle-HorizontalAlign="Right">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FSH_NARRATION" HeaderText="Narration" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnView" runat="server">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Download">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnDownload" runat="server" Enabled="false" OnClick="lbtnDownload_Click">Download</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnPrintt" runat="server" CssClass="button" Text="PRINT" Visible="false" />
                            <asp:Button ID="btnClear" runat="server" CssClass="button" Text="CLEAR" Visible="false" />
                        </td>
                    </tr>
                </table>
                <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
                </script>
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>
</asp:Content>

