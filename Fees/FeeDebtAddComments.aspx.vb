﻿Imports System.Data
Imports Telerik.Web.UI
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Partial Class Fees_FeeDebtAddComments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("sUsr_name") Is Nothing Then
                ShowMessage("Your login session got expired, please login again.")
                Exit Sub
            End If

            ViewState("STU_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            ViewState("STU_BSU_ID") = Encr_decrData.Decrypt(Request.QueryString("BU").Replace(" ", "+"))
            ViewState("DFR_ID") = 0
            reasontext.Visible = False
            If Not ViewState("STU_ID") Is Nothing AndAlso ViewState("STU_ID") > 0 Then
                BindComments()
                GetStudentInfo()
                BindPaymentPlan()
            End If
            BindDebtFollowupReasons()
            'chkFollowup.Attributes.Add("onChange", "DisableDate();")
            chkFollowup_CheckedChanged(Nothing, Nothing)
        End If
    End Sub

    Sub BindComments()
        Dim dt As New DataTable
        dt = clsDebtFollowup.GET_SAVED_DEBT_FOLLOW_UP_COMMENTS(ViewState("STU_ID"))
        gvComments.DataSource = dt
        gvComments.DataBind()
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            ViewState("DFR_ID") = Convert.ToInt32(dt.Rows(0)("DFR_ID"))
        End If
    End Sub
    Sub BindDebtFollowupReasons()
        Dim dt As New DataTable
        dt = clsDebtFollowup.GET_DEBT_FOLLOW_UP_REASONS()
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            ddlfollowupReasons.DataSource = dt
            ddlfollowupReasons.DataTextField = "DFR_REASON"
            ddlfollowupReasons.DataValueField = "DFR_ID"
            ddlfollowupReasons.DataBind()
            If ViewState("DFR_ID") <> 0 Then
                ddlfollowupReasons.SelectedValue = ViewState("DFR_ID")
                reasontext.Visible = True
                ddlfollowupReasons_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
    End Sub


    Sub BindSiblings(ByVal BSU_ID As String, ByVal SIBLING_ID As Long)
        Dim dt As New DataTable
        dt = clsDebtFollowup.GET_SIBLINGS(BSU_ID, SIBLING_ID, ViewState("STU_ID"))
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            ddlSiblings.DataSource = dt
            ddlSiblings.DataTextField = "STU_NAME"
            ddlSiblings.DataValueField = "STU_ID"
            ddlSiblings.DataBind()
            Dim AllItem As New Telerik.Web.UI.RadComboBoxItem
            AllItem.Text = "<--Select sibling-->"
            AllItem.Value = "0"
            ddlSiblings.Items.Insert(0, AllItem)
            ddlSiblings.SelectedIndex = 0
            chkCopyComments.Visible = True
            chkCopyFollowupDate.Visible = True
        Else
            chkCopyComments.Visible = False
            chkCopyFollowupDate.Visible = False
        End If

    End Sub

    Sub GetStudentInfo()
        Dim dt As New DataTable
        dt = clsDebtFollowup.GET_STUDENT_DETAILS(ViewState("STU_BSU_ID"), ViewState("STU_ID"))
        Dim STU_SIBLING_ID As Long = 0
        Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            tdStuNo.InnerText = dt.Rows(0)("STU_NO").ToString
            tdStuName.InnerText = dt.Rows(0)("STU_NAME").ToString
            tdGrade.InnerText = dt.Rows(0)("GRD_DISPLAY").ToString
            tdbCOVID.InnerText = dt.Rows(0)("bCOVID_CONCESSION").ToString
            STU_SIBLING_ID = dt.Rows(0)("STU_SIBLING_ID").ToString
            imgStuImage.ImageUrl = connPath & dt.Rows(0)("STU_PHOTO_PATH").ToString
            tdStatus.InnerText = dt.Rows(0)("STU_CURRSTATUS").ToString
            trStuDetail.Attributes.Remove("class")
            If tdStatus.InnerText.Trim <> "EN" Then '----adding style for inactive students
                trStuDetail.Attributes.Add("class", "inactive")
            End If
            BindOutStanding()
            BindSiblings(ViewState("STU_BSU_ID"), STU_SIBLING_ID)
        End If
    End Sub
    Sub CallStudentAuditReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim STU_ID As Integer = Mainclass.cleanString(ViewState("STU_ID"))
        Dim str_query As String = "SELECT STU_NAME,GRM_DISPLAY,SCT_DESCR,STU_CURRSTATUS,STU_NO FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID=" & STU_ID
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        With ds.Tables(0).Rows(0)
            param.Add("studName", .Item(0))
            param.Add("Grade", .Item(1))
            param.Add("Section", .Item(2))
            param.Add("StuNo", .Item(4))
            param.Add("status", .Item(3))
        End With
        param.Add("stu_id", ViewState("STU_ID"))
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis"
            .reportPath = Server.MapPath("../../../Students/Reports/RPT/rptstudAudit.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        'If Session("ReportSel") = "POP" Then
        '    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        'Else
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        'End If
    End Sub
    Private Sub CallrptStudentLedger()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(ViewState("STU_ID"), XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmd.Parameters.AddWithValue("@bSuppressAdvInv", True)
        cmdSub.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub.Parameters.AddWithValue("@bSuppressAdvInv", True)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ViewState("STU_BSU_ID")
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub.Parameters.AddWithValue("@BSU_ID", sqlpBSU_ID.Value)
        cmdSub2.Parameters.AddWithValue("@BSU_ID", sqlpBSU_ID.Value)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(DateTime.Now.AddMonths(-12))
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub.Parameters.AddWithValue("@FromDT", sqlpFromDT.Value)
        cmdSub2.Parameters.AddWithValue("@FromDT", sqlpFromDT.Value)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(DateTime.Now.ToShortDateString)
        cmd.Parameters.Add(sqlpTODT)
        cmdSub.Parameters.AddWithValue("@ToDT", sqlpTODT.Value)
        cmdSub2.Parameters.AddWithValue("@ToDT", sqlpTODT.Value)

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = Format(sqlpFromDT.Value, OASISConstants.DataBaseDateFormat)
        params("ToDate") = Format(sqlpTODT.Value, OASISConstants.DataBaseDateFormat)
        params("RoundOffVal") = Session("BSU_ROUNDOFF")
        params("STU_TYPE") = "S"

        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = sqlpBSU_ID.Value
        repSource.ResourceName = "../FEES/REPORTS/RPT/rptFeeStudentLedger.rpt"
        Session("ReportSource") = repSource
        Response.Write("<script>window.open('../Reports/ASPX Report/Rptviewer.aspx');</script>")
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtComments.Text.Trim = "" Then
            ShowMessage("Please add comments and continue to save")
            Exit Sub
        End If
        If chkFollowup.Checked And txtFollowupDate.SelectedDate Is Nothing Then
            ShowMessage("Please select the follow up date and continue to save")
            Exit Sub
        End If
        Dim bSaveSuccess As Boolean = True, SuccessMessage As String = String.Empty
        Dim objcls As New clsDebtFollowup
        objcls.DFC_ID = 0
        objcls.DFC_BSU_ID = ViewState("STU_BSU_ID")
        objcls.DFC_COMMENTS = Left(txtComments.Text.Trim.Replace("'", "`").Replace(";", "").Replace("%", ""), 750)
        objcls.DFC_FOLLOWUP_DATE = IIf(chkFollowup.Checked, txtFollowupDate.SelectedDate, "01/Jan/1900")
        objcls.DFC_STU_ID = ViewState("STU_ID")
        objcls.DFC_LOG_USER = Session("sUsr_name")
        objcls.DFF_REASON = ddlfollowupReasons.SelectedValue

        If ddlfollowupReasons.SelectedValue = "12" Then
            If rbPP.Checked = False And rbPDC.Checked = False Then
                ShowMessage("Please select PDC or Payment Plan and continue to save")
                Exit Sub
            End If

        End If

        If ddlfollowupReasons.SelectedValue = "-1" Then
            ShowMessage("Please select Reason and continue to save")
            Exit Sub
        End If


        If rbPayTyp.Visible = True Then
            objcls.DFF_PAY_TYPE = IIf(rbPP.Checked = True, "PP", "PDC")
        Else
            objcls.DFF_PAY_TYPE = ""
        End If
        objcls.DFF_bPRIMARY_CONTACT = chk_prm_contact.Checked
        objcls.DFF_bSECONDARY_CONTACT = chk_sec_contact.Checked
        objcls.DFF_bSEND_EMAIL = chkSendEmail.Checked
        objcls.DFC_EMAIL_TEXT = txtEmailText.Content

        Dim dt As DataTable = GetTable()
        Dim dr As DataRow

        If rbPayTyp.Visible = True Then
            If (FeeCollection.GetDoubleVal(txtAmount1.Text) > 0) Then
                '-----validate date selection
                If Not IsValidDate(RadDatePicker1.SelectedDate) Then
                    ShowMessage("Please select a valid date for first installment")
                    Exit Sub
                End If
                If RadDatePicker1.SelectedDate < Date.Now.ToShortDateString Then
                    ShowMessage("Past date is not allowed, select a valid date for first installment")
                    Exit Sub
                End If
                dr = dt.NewRow()
                dr("AMOUNT") = FeeCollection.GetDoubleVal(txtAmount1.Text)
                dr("DATE") = RadDatePicker1.SelectedDate
                dt.Rows.Add(dr)
            End If

            If (FeeCollection.GetDoubleVal(txtAmount2.Text) > 0) Then
                '-----validate date selection
                If Not IsValidDate(RadDatePicker2.SelectedDate) Then
                    ShowMessage("Please select a valid date for second installment")
                    Exit Sub
                End If
                If RadDatePicker2.SelectedDate < Date.Now.ToShortDateString Then
                    ShowMessage("Past date is not allowed, select a valid date for second installment")
                    Exit Sub
                End If
                dr = dt.NewRow()
                dr("AMOUNT") = FeeCollection.GetDoubleVal(txtAmount2.Text)
                dr("DATE") = RadDatePicker2.SelectedDate
                dt.Rows.Add(dr)
            End If

            If (FeeCollection.GetDoubleVal(txtAmount3.Text) > 0) Then
                '-----validate date selection
                If Not IsValidDate(RadDatePicker3.SelectedDate) Then
                    ShowMessage("Please select a valid date for third installment")
                    Exit Sub
                End If
                If RadDatePicker3.SelectedDate < Date.Now.ToShortDateString Then
                    ShowMessage("Past date is not allowed, select a valid date for third installment")
                    Exit Sub
                End If
                dr = dt.NewRow()
                dr("AMOUNT") = FeeCollection.GetDoubleVal(txtAmount3.Text)
                dr("DATE") = RadDatePicker3.SelectedDate
                dt.Rows.Add(dr)
            End If

            If (FeeCollection.GetDoubleVal(txtAmount4.Text) > 0) Then
                '-----validate date selection
                If Not IsValidDate(RadDatePicker4.SelectedDate) Then
                    ShowMessage("Please select a valid date for fourth installment")
                    Exit Sub
                End If
                If RadDatePicker4.SelectedDate < Date.Now.ToShortDateString Then
                    ShowMessage("Past date is not allowed, select a valid date for fourth installment")
                    Exit Sub
                End If
                dr = dt.NewRow()
                dr("AMOUNT") = FeeCollection.GetDoubleVal(txtAmount4.Text)
                dr("DATE") = RadDatePicker4.SelectedDate
                dt.Rows.Add(dr)
            End If
            If dt Is Nothing OrElse dt.Rows.Count <= 0 Then
                ShowMessage("Please enter the installment information")
                Exit Sub
            End If
        End If
        Dim retMessage As String = ""
        Dim RetVal As Integer = objcls.SAVE_DEBT_FOLLOW_UP_COMMENTS(retMessage, dt)
        If RetVal = 0 And retMessage = "" Then
            SuccessMessage = "Comments saved successfully"
            'ShowMessage("Comments saved successfully", False)
            'txtComments.Text = ""
            'BindComments()
            'btnSave.Visible = False
        Else
            bSaveSuccess = False
            If retMessage <> "" Then
                ShowMessage(retMessage, True)
            Else
                ShowMessage(UtilityObj.getErrorMessage(RetVal), True)
            End If
        End If

        If (chkCopyComments.Checked = True Or chkCopyFollowupDate.Checked = True) And ddlSiblings.Items.Count > 0 And bSaveSuccess Then
            For Each item As RadComboBoxItem In ddlSiblings.Items
                If item.Value > 0 Then
                    objcls.DFC_ID = 0
                    objcls.DFC_BSU_ID = ViewState("STU_BSU_ID")
                    objcls.DFC_COMMENTS = Left(txtComments.Text.Trim.Replace("'", "`").Replace(";", "").Replace("%", ""), 750)
                    objcls.DFC_FOLLOWUP_DATE = IIf(chkFollowup.Checked, txtFollowupDate.SelectedDate, "01/Jan/1900")
                    objcls.DFC_STU_ID = item.Value
                    objcls.DFC_LOG_USER = Session("sUsr_name")
                    objcls.DFF_REASON = ddlfollowupReasons.SelectedValue
                    If rbPP.Visible = True Then
                        objcls.DFF_PAY_TYPE = IIf(rbPP.Checked = True, "PP", "PDC")
                    Else
                        objcls.DFF_PAY_TYPE = ""
                    End If

                    retMessage = ""
                    RetVal = objcls.SAVE_DEBT_FOLLOW_UP_COMMENTS(retMessage, dt)
                    If RetVal = 0 And retMessage = "" Then
                        'ShowMessage("Comments saved successfully", False)
                        'txtComments.Text = ""
                        'BindComments()
                        'btnSave.Visible = False
                    Else
                        bSaveSuccess = False
                        If retMessage <> "" Then
                            ShowMessage(retMessage & "[" & item.Text & "]", True)
                        Else
                            ShowMessage(UtilityObj.getErrorMessage(RetVal) & "[" & item.Text & "]", True)
                        End If
                        Exit For
                    End If
                End If

            Next
            If bSaveSuccess Then
                SuccessMessage = "Comments saved successfully for the selected student and sibling(s)."
            End If
        End If
        If bSaveSuccess Then
            ShowMessage(SuccessMessage, False)
            txtComments.Text = ""
            BindComments()
            btnSave.Visible = False
            chkSendEmail.Visible = False
        End If
    End Sub
    Public Function GetTable() As DataTable
        Dim dt As DataTable = New DataTable()
        dt.Columns.Add("AMOUNT")
        dt.Columns.Add("DATE")
        Return dt
    End Function
    Protected Sub chkFollowup_CheckedChanged(sender As Object, e As EventArgs) Handles chkFollowup.CheckedChanged
        txtFollowupDate.Enabled = chkFollowup.Checked
        If chkFollowup.Checked Then
            txtFollowupDate.SelectedDate = Date.Now
        Else
            txtFollowupDate.Clear()
        End If
    End Sub
    Protected Sub ddlfollowupReasons_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlfollowupReasons.SelectedIndexChanged
        If ddlfollowupReasons.SelectedValue > 0 Then
            If ddlfollowupReasons.SelectedValue = "12" Then
                rbPayTyp.Visible = True
            Else
                rbPayTyp.Visible = False
            End If
        End If
    End Sub
    Protected Sub ddlSiblings_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlSiblings.SelectedIndexChanged
        If ddlSiblings.SelectedValue > 0 Then
            ViewState("STU_ID") = ddlSiblings.SelectedValue
            BindComments()
            GetStudentInfo()
            btnSave.Visible = True
            If ViewState("DFR_ID") <> 0 AndAlso ddlfollowupReasons.Items.Count > 0 Then
                ddlfollowupReasons.SelectedValue = ViewState("DFR_ID")
                reasontext.Visible = True
                ddlfollowupReasons_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub lbtnAudit_Click(sender As Object, e As EventArgs) 'Handles lbtnAudit.Click
        CallStudentAuditReport()
    End Sub

    Protected Sub lbtnLedger_Click(sender As Object, e As EventArgs) Handles lbtnLedger.Click
        'CallrptStudentLedger()
        '../Students/StudentProfileView.aspx?mode=STUDENTFEES&Id=' + CAST(@STU_ID AS VARCHAR) +'&stutype=' + @STU_TYPE + '
        Response.Write("<script>window.open('../Students/StudentProfileView.aspx?mode=STUDENTFEES&Id=" & ViewState("STU_ID") & "&stutype=S');</script>")
    End Sub

    Protected Sub lbtnReceipts_Click(sender As Object, e As EventArgs) Handles lbtnReceipts.Click
        Response.Write("<script>window.open('../common/PaymentHistory.aspx?id=RECEIPTHISTORY&stuid=" & ViewState("STU_ID") & "');</script>")
    End Sub

    Protected Sub lbtnChargedetails_Click(sender As Object, e As EventArgs) Handles lbtnChargedetails.Click
        Response.Write("<script>window.open('../common/PopupShowData.aspx?id=CHARGEHISTORY&stuid=" & ViewState("STU_ID") & "');</script>")
    End Sub

    Sub BindOutStanding()
        gvOS.DataSource = clsDebtFollowup.GET_FEE_HEADWISE_OUTSTANDING(ViewState("STU_ID"), Session("sBsuId"), 0, False)
        gvOS.DataBind()
    End Sub

    Sub BindPaymentPlan()
        Dim PaymentPlan As String = ""
        Dim dt As DataTable = clsDebtFollowup.GET_PAYMENT_PLAN(ViewState("STU_ID"))
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            PaymentPlan = dt.Rows(0)("PAY_PLAN").ToString
            'Dim i As Integer = 1
            'For Each drow As DataRow In dt.Rows
            '    If PaymentPlan = "" Then
            '        PaymentPlan = "<b>" & drow.Item("PAY_TYPE") & ":</b> "
            '        PaymentPlan = PaymentPlan & i & "(" & drow.Item("PP") & ")"
            '    Else
            '        PaymentPlan = PaymentPlan & ", " & i & "(" & drow.Item("PP") & ")"
            '    End If

            '    i = i + 1
            'Next
        End If

        'lblPP.Text = PaymentPlan

        lblPP.Text = Server.HtmlDecode(PaymentPlan)

    End Sub

    Protected Sub gvOS_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvOS.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(0).Text.ToUpper().Contains("TOTAL") Then
                e.Row.Font.Bold = True
            End If
        End If
    End Sub

    Public Function IsValidDate(ByVal pdate As Object) As Boolean
        IsValidDate = False
        If pdate Is Nothing Then
            Exit Function
        End If
        Try
            Dim varDate As Date = CDate(pdate)
            Dim Month As Int16 = varDate.Month
            Dim Day As Int16 = varDate.Day
            Dim Year As Integer = varDate.Year
            If (Month >= 1 And Month <= 12) And (Day > 0 And Day <= 31) And Year > 2019 Then
                IsValidDate = True
            End If
        Catch ex As Exception

        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
        If Message <> "" Then
            preError.Attributes.Remove("class")
            If bError Then
                preError.Attributes.Add("class", "alert alert-danger")
            Else
                preError.Attributes.Add("class", "alert alert-success")
            End If
            preError.Focus()
        Else
            preError.Attributes.Remove("class")
            preError.Attributes.Add("class", "invisible")
        End If
        preError.InnerHtml = Message
    End Sub
    Protected Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click
        divAge.Visible = True
        Get_Email_Preview(ViewState("STU_ID"), ViewState("STU_BSU_ID"))
    End Sub
    Protected Sub btnSendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click
        Try
            'InsertIntoSingleEmailSchedule(1, ViewState("STU_ID"),
            '                                      ViewState("STU_BSU_ID"), "DEBTFLWUP", "GEMS", txtEmailText.Content, Session("sUsr_name"))
            'lblUerror.Text = "Email sent successfully"
            If txtEmailText.Content = "" Then
                divAge.Visible = True
                lblUerror.Text = "Email content is empty"
            Else
                divAge.Visible = False
            End If

        Catch ex As Exception
            lblUerror.Text = "Email failed"
        End Try


    End Sub
    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Get_Email_Preview(ViewState("STU_ID"), ViewState("STU_BSU_ID"))
    End Sub
    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False
        txtEmailText.Content = ""
        chkSendEmail.Checked = False
        'Me.MPE1.Show()
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        'divAge.Visible = False

        lblMessage.Text = "Are you sure, you don't need to send email to parent?"
        Me.MPE1.Show()
    End Sub

    Protected Sub btnOkay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOkay.Click
        divAge.Visible = False
        txtEmailText.Content = ""
        chkSendEmail.Checked = False
    End Sub
    Sub Get_Email_Preview(ByVal STU_ID As Long, ByVal STU_BSU_ID As String)
        Try
            Dim emailtext As String = clsDebtFollowup.GET_EMAIL_PREVIEW(STU_ID, STU_BSU_ID, "DEBTFLWUP", "GEMS", txtEmailText.Content, "")
            lblEmail_preview.Text = emailtext
            Dim Email_Content As String = Convert.ToString(SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(DFR_REASON_EMAIL_TEXT,'') FROM [FEES].[DEBT_FOLLOW_UP_REASONS] WHERE DFR_ID ='" & ddlfollowupReasons.SelectedValue & "'"))
            If Email_Content <> "" Then
                txtEmailText.Content = Email_Content
            End If
        Catch ex As Exception
            lblUerror.Text = "Error in fetching email preview"
        End Try

    End Sub

    Public Shared Function InsertIntoSingleEmailSchedule(ByVal OPTIONS As Integer, ByVal STU_ID As String,
                                                   ByVal BSU_ID As String, ByVal EML_TYPE As String, ByVal COMPANY As String, ByVal REFERENCE1 As String, ByVal REFERENCE2 As String) As Integer


        Dim pParms(7) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = STU_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 500)
        pParms(2).Value = BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 500)
        pParms(3).Value = EML_TYPE
        pParms(4) = New SqlClient.SqlParameter("@COMPANY", SqlDbType.VarChar, 500)
        pParms(4).Value = COMPANY
        pParms(5) = New SqlClient.SqlParameter("@REFERENCE1", SqlDbType.VarChar)
        pParms(5).Value = REFERENCE1
        pParms(6) = New SqlClient.SqlParameter("@REFERENCE2", SqlDbType.VarChar)
        pParms(6).Value = REFERENCE2

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "OASIS.dbo.SINGLE_EMAIL_INSERT_SCHEDULE_JOB", pParms)
        'Dim ReturnValue As Integer = pParms(6).Value
        Return ReturnFlag

    End Function

    Protected Sub chkSendEmail_CheckedChanged(sender As Object, e As EventArgs) Handles chkSendEmail.CheckedChanged
        If chkSendEmail.Checked Then
            divAge.Visible = True
            Get_Email_Preview(ViewState("STU_ID"), ViewState("STU_BSU_ID"))
        Else
            divAge.Visible = False
        End If

    End Sub
    Protected Sub lbtnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDFC_ID As Label = sender.Parent.parent.findcontrol("lblDFC_ID")
        Get_Email_Preview2(ViewState("STU_ID"), ViewState("STU_BSU_ID"), lblDFC_ID.Text)
        pnlemailhistory.Visible = True
    End Sub
    Sub Get_Email_Preview2(ByVal STU_ID As Long, ByVal STU_BSU_ID As String, ByVal DFC_ID As String)
        Try
            'Dim Email_Content As String = Convert.ToString(SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(DFC_EMAIL_TEXT,''),'''&amp;quot''','&amp;quot'),'''&quot''','quot'),'''Nunito''','Nunito'),'''&amp;amp;quot''','&amp;amp;quot')  DFC_EMAIL_TEXT FROM OASIS_FEES.FEES.DEBT_FOLLOW_UP_COMMENTS WHERE DFC_ID ='" & DFC_ID & "'"))
            Dim Email_Content As String = Convert.ToString(SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT OASIS_FEES.FEES.GET_DEBTFOLLOWUP_EMAIL_TEXT( '" & DFC_ID & "')"))
            If Email_Content <> "" Then
                Dim emailtext As String = clsDebtFollowup.GET_EMAIL_PREVIEW(STU_ID, STU_BSU_ID, "DEBTFLWUP", "GEMS", Email_Content, "")
                lbl_emailHistory.Text = emailtext
                pnlemailhistory.Visible = True
            End If


        Catch ex As Exception
            lblUerror.Text = "Error in fetching email preview"
        End Try

    End Sub
    Protected Sub btn_EHclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_EHclose.Click
        pnlemailhistory.Visible = False
    End Sub
    Protected Sub btn_EHclose2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_EHclose2.Click
        pnlemailhistory.Visible = False
    End Sub

    Protected Sub gvComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvComments.PageIndexChanging
        gvComments.PageIndex = e.NewPageIndex
        BindComments()

    End Sub
End Class
