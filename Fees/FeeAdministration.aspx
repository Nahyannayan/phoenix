﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeAdministration.aspx.vb" Inherits="Fees_FeeAdministration" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Fee Administration
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="Table1" width="100%">

                    <tr>
                        <td>
                            <table align="left" width="100%">
                                <tr>
                                    <td align="left" class="matters">
                                        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                            <asp:TabPanel ID="TabPanel2" HeaderText="Student Ledger" runat="server">
                                                <HeaderTemplate>
                                                    Student Ledger
                                                </HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                                            </td>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td width="20%"></td>
                                                            <td width="30%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="matters"><span class="field-label">Student</span>
                                                            </td>
                                                            <td align="left" class="matters" colspan="3">
                                                                <uc1:usrSelStudent ID="UsrSelStudent1" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="matters"><span class="field-label">From Date</span>
                                                            </td>
                                                            <td align="left" class="matters">
                                                                <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                                                    ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><asp:CalendarExtender
                                                                        ID="calendarButtonExtender" runat="server" CssClass="MyCalendar" Enabled="True"
                                                                        Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                                                    </asp:CalendarExtender>
                                                            </td>
                                                            <td align="left" class="matters"><span class="field-label">To Date</span>
                                                            </td>
                                                            <td align="left" class="matters">
                                                                <asp:TextBox ID="txtTo" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                                                    ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><asp:CalendarExtender
                                                                        ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                                                        PopupButtonID="imgTo" TargetControlID="txtTo">
                                                                    </asp:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="matters"><span class="field-label">Student</span>
                                                            </td>
                                                            <td align="left" class="matters">
                                                                <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label>
                                                            </td>
                                                            <td align="left" class="matters"><span class="field-label">DOJ</span>
                                                            </td>
                                                            <td align="left" class="matters">
                                                                <asp:Label ID="lblDOJ" runat="server" CssClass="field-value"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="matters"><span class="field-label">Last ATT Date</span></td>
                                                            <td align="left" class="matters">
                                                                <asp:Label ID="lblLastATTDate" runat="server" CssClass="field-value"></asp:Label></td>
                                                            <td align="left" class="matters"></td>
                                                            <td align="left" class="matters"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="matters"><span class="field-label">Grade</span>
                                                            </td>
                                                            <td align="left" class="matters">
                                                                <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label>
                                                            </td>
                                                            <td align="center" colspan="2">
                                                                <asp:Button ID="btnLedger" runat="server" CssClass="button" Text="Show Ledger" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="matters" colspan="4">
                                                                <asp:Panel ID="pHeader" runat="server" CssClass="panel-cover">
                                                                    <asp:Image ID="Image1" runat="server" /><asp:Label ID="lblText" runat="server"  />
                                                                </asp:Panel>
                                                                <asp:Panel ID="pBody" runat="server" class="matters">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center" class="matters">
                                                                                <asp:Button ID="btnRgnTC" runat="server" Text="Re-Generate TC reversal request"
                                                                                    CssClass="button" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                  &nbsp;  <asp:Button ID="btnRegCNReversal" runat="server" Text="Re-Generate CN reversal request"
                                                                        CssClass="button" OnClick="btnRegCNReversal_Click" />
                                                                </asp:Panel>
                                                                <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pBody"
                                                                    CollapseControlID="pHeader" ExpandControlID="pHeader" Collapsed="True" TextLabelID="lblText"
                                                                    CollapsedText="Other Options" ExpandedText="Hide Options" ImageControlID="Image1"
                                                                    ExpandedImage="~/Images/collapse.jpg" CollapsedImage="~/Images/expand.jpg" CollapsedSize="0"
                                                                    Enabled="True">
                                                                </asp:CollapsiblePanelExtender>
                                                            </td>
                                                        </tr>
                                                        <tr id="trLedger" runat="server">
                                                            <td id="Td1" runat="server" align="center" class="matters" colspan="4">
                                                                <asp:GridView ID="gvStLedger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                    EmptyDataText="No Data" ShowFooter="True" CssClass="table table-row table-bordered" Width="100%">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc.Date">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="Grade">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblGrade" runat="server" Text='<%#Bind("GRD_DISPLAY") %>'>&#39;&gt;</asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="RECNO" HeaderText="RefNo">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="COLLECTIONTYPE" HeaderText="Collection Type">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DEBIT" DataFormatString="{0:n2}" HeaderText="Debit">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CREDIT" DataFormatString="{0:n2}" HeaderText="Credit">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Balance" DataFormatString="{0:n2}" HeaderText="Balance">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <FooterStyle HorizontalAlign="Right" VerticalAlign="Bottom" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="4">
                                                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                                                    CssClass="button" OnClick="btnCancel_Click" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                        </asp:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
