<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTranspotRecCancellation.aspx.vb" Inherits="Fees_feeTranspotCollection" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="true">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Transport Receipt Cancellation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 100%">
                    <tr>
                        <td align="center">
                            <asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr>
                        <td align="left" class="matters" style="width: 20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" style="width: 30%">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" CssClass="listbox" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" AutoPostBack="True" TabIndex="5">
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="width: 20%"><span class="field-label"></span></td>
                        <td align="left" style="width: 30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="imgFromDate"
                                runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtDate" Display="Dynamic"
                                    ErrorMessage="Enter the Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Receipt No.</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRecieptNo" runat="server" AutoPostBack="True"></asp:TextBox><asp:ImageButton ID="imgReceipts" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="return GetReceipts();" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRecieptNo"
                                ErrorMessage="Rec. No Required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr runat="server" id="trReceiptDetails">
                        <td align="left"><span class="field-label">Receipt Details</span></td>
                        <td colspan="3">
                            <table style="border: solid 1px;">
                                <tr class="title-bg-lite">
                                    <td><span class="field-label">Student Id</span></td>
                                    <td><span class="field-label">Student Name</span></td>
                                    <td><span class="field-label">Receipt No.</span></td>
                                    <td><span class="field-label">Receipt Date</span></td>
                                    <td><span class="field-label">Amount</span></td>
                                    <td><span class="field-label">Created By</span></td>
                                    <td><span class="field-label">Created On</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblStuNo" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblReceiptNo" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblReceiptDate" runat="server"></asp:Label></td>
                                    <td align="right">
                                        <asp:Label ID="lblAmount" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblReceiptCreatedBy" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblReceiptCreatedOn" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Remarks </span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText" TabIndex="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRemarks"
                                ErrorMessage="Remarks Required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Delete Receipt" TabIndex="105" ValidationGroup="MAINERROR" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                TargetControlID="btnSave"
                                ConfirmText="Are you sure you want to delete the receipt?" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
        <table width="100" id="tbliframe" runat="server">
            <tr>
                <td style="width: 100%;" align="center">
                    <asp:Panel ID="pnlViewPopup" runat="server" CssClass="RadDarkPanlvisible">
                        <div class="RadPanelQual" runat="server" id="divViewPopup">
                            <iframe id="ifrmViewPopup" runat="server" width="0" height="0"></iframe>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
        <asp:HiddenField ID="hfFCLID" runat="server" />
    </div>

    <script type="text/javascript">
        function CloseFrame() {
            var myFrm = $("#<%=ifrmViewPopup.ClientID %>");
            myFrm.attr("src", "about:blank");
            $("#<%=pnlViewPopup.ClientID %>").css("display", "none");

        }
        function PlaceControlAtCentre(mWidth, mHeight) {
            var myDiv = $("#<%=divViewPopup.ClientID %>");
            var leftMargin, TopMargin, mPaperSizeX, mPaperSizeY, CalcWidth, CalcHeight

            CalcWidth = (90 * screen.width / 100);
            if (mWidth >= CalcWidth)
                mPaperSizeX = CalcWidth;
            else
                mPaperSizeX = mWidth;

            CalcHeight = (90 * screen.height / 100);

            if (mHeight >= CalcHeight)
                mPaperSizeY = CalcHeight;
            else
                mPaperSizeY = mHeight;

            var scrOfY = 0;
            var scrOfX = 0;
            if (typeof (window.pageYOffset) == 'number') {
                //Netscape compliant
                scrOfY = window.pageYOffset;
                scrOfX = window.pageXOffset;
            } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                //DOM compliant
                scrOfY = document.body.scrollTop;
                scrOfX = document.body.scrollLeft;
            } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                //IE6 standards compliant mode
                scrOfY = document.documentElement.scrollTop;
                scrOfX = document.documentElement.scrollLeft;
            }

            leftMargin = (screen.width - mPaperSizeX) / 2;
            TopMargin = (screen.height - mPaperSizeY) / 2;
            myDiv.css("position", "absolute");
            myDiv.css("top", TopMargin + scrOfY - 50);
            myDiv.css("left", leftMargin + scrOfX);
            myDiv.css("width", mWidth);
            myDiv.css("height", mHeight);
            myDiv.css("z-index", "1000");
        }

        function GetReceipts() {
            var BSU_ID = document.getElementById('<%=ddBusinessunit.ClientID%>').value;
            var url;
            var which = $("#<%=pnlViewPopup.ClientID %>");
            var myIFrame = $("#<%=ifrmViewPopup.ClientID %>");
            myIFrame.css("width", "100%");
            myIFrame.css("height", "500px");
            PlaceControlAtCentre(890, 510);
            which.css("display", "block");
            url = "../Common/SelectReceipts.aspx?stubsu=" + BSU_ID;
            myIFrame.attr("src", url);
            return false;
        }

        function SetFCLIDs(fclids) {
            $("#<%=hfFCLID.ClientID%>").val(fclids);
            CloseFrame();
            __doPostBack('<%=hfFCLID.ClientID%>', "");
        }
    </script>
</asp:Content>


