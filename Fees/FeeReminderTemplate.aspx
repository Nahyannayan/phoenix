<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeReminderTemplate.aspx.vb" Inherits="fees_FeeReminderTemplate" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>


<%@ MasterType VirtualPath="~/mainMasterPage.master" %>

<asp:Content ID="cphMasterpage" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
function CheckForPrint() {
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        //showModelessDialog('../Reports/ASPX Report/RptViewerModalView.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        var url = "../Reports/ASPX Report/RptViewerModalView.aspx";
        var oWnd = radopen(url, "pop_clickforprint");
    }
}
    );


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       
        <Windows>
            <telerik:RadWindow ID="pop_clickforprint" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="750px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label ID="lblHeader" runat="server" Text="Reminder Template"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
        SkinID="Error"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_CONC" />

    <table align="center" width="98%">
        
        <tr>
            <td align="left" width="20%"><span class="field-label">Business Unit</span>
            </td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlBusinessUnit" runat="server">
                </asp:DropDownList>
            </td>
        
            <td align="left" width="20%">
                <span class="field-label">Level</span>
            </td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlLevel" runat="server" DataSourceID="SqlDataSource1" DataTextField="RMD_DESCR"
                    DataValueField="RMD_ID">
                    <asp:ListItem Value="1">First Reminder</asp:ListItem>
                    <asp:ListItem Value="2">Second Reminder</asp:ListItem>
                    <asp:ListItem Value="3">Third Reminder</asp:ListItem>
                    <asp:ListItem Value="4">Transport Statement Letter</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Short Description</span>
            </td>
            <td align="left" class="border" colspan="3">
                <%--<asp:TextBox ID="txtShortDescr" runat="server" Height="66px" SkinID="MultiText_Large"
                    TextMode="MultiLine" Width="313px"></asp:TextBox>--%>
                <telerik:RadEditor ID="txtShortDescr" runat="server" Style="overflow:hidden;overflow-y:scroll;" Height="150px" Width="800px"
                    ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                     StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span" >
                    
                </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Remarks</span>
            </td>
            <td align="left" class="border" colspan="3">
                <%--<asp:TextBox ID="txtRemarks" runat="server" Height="81px" SkinID="MultiText_Large"
                    TextMode="MultiLine" Width="311px"></asp:TextBox>--%>
                <telerik:RadEditor ID="txtRemarks" runat="server" Style="overflow:hidden;overflow-y:scroll;" Height="250px" Width="800px" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                    StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                    ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Acknowledgement</span>
            </td>
            <td align="left" class="border" colspan="3">
                <%--<asp:TextBox ID="txtAcknowlg" runat="server" Height="85px" SkinID="MultiText_Large"
                    TextMode="MultiLine" Width="312px"></asp:TextBox>--%>
                <telerik:RadEditor ID="txtAcknowlg" runat="server" Style="overflow:hidden;overflow-y:scroll;" Height="250px" Width="800px" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                     StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                    ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                </telerik:RadEditor>
            </td>
       </tr>
        <tr>
            <td align="left"><span class="field-label">Signature</span>
            </td>
            <td align="left" class="border" colspan="3">
                <%--<asp:TextBox ID="txtSignature" runat="server" Height="85px" SkinID="MultiText_Large"
                    TextMode="MultiLine" Width="312px"></asp:TextBox>--%>
                <telerik:RadEditor ID="txtSignature" runat="server" Style="overflow:hidden;overflow-y:scroll;" Height="250px" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                     StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                    Width="800px" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">To Address</span>
            </td>
            <td align="left" class="border" colspan="3">
                <telerik:RadEditor ID="txtToAddress" runat="server" Style="overflow:hidden;overflow-y:scroll;" Height="250px" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                     StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                    Width="800px" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">SMS</span>
            </td>
            <td align="left" class="border" colspan="3">
                <asp:TextBox ID="txtSMS" runat="server" Style="overflow:hidden;overflow-y:scroll;" Height="85px" SkinID="MultiText_Large" TextMode="MultiLine"
                    Width="800px"></asp:TextBox>
            </td>
        </tr>
        <tr align="left">
            <td></td>
            <td colspan="3">
                Enter <b class="text-danger">####</b> For Student No<br />
                Enter <b class="text-danger">$$$$</b> For Student Name<br />
                Enter <b class="text-danger">@@@@</b> For Amount<br />
                Enter <b class="text-danger">!!!!</b> For Amount in words<br />
                Enter <b class="text-danger">%%%%</b> For Grade<br />
                Enter <b class="text-danger">&&&&</b> For Section<br />
                Enter <b class="text-danger">****</b> For Date<br />
                Enter <b class="text-danger">~~~~</b> For Parent First Name<br />
                Enter <b class="text-danger">^^^^</b> For Parent Middle Name<br />
                Enter <b class="text-danger">----</b> For Parent Last Name<br />
                Replace the HTML tag <b>&lt;</b> with <b class="text-danger">[</b> and<b> &gt;</b>
                with <b class="text-danger">]</b><br />
                <b class="text-danger">Note : This is applicable only in the case of "Remarks" and
                    "Acknowledgement"'></b>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                &nbsp;<asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" OnClick="btnAdd_Click" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" OnClick="btnEdit_Click" />
                <asp:Button ID="btnSave" runat="server" UseSubmitBehavior="false" CssClass="button" Text="Save" ValidationGroup="FEE_CONC" />
                <asp:Button ID="btnDelete" Visible="false" runat="server" CausesValidation="False"
                    CssClass="button" Text="Delete" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>
                </div>
            </div>
        </div>

    <asp:HiddenField ID="h_print" runat="server" />
        <asp:HiddenField ID="h_IsArabic" runat="server" />
    <asp:HiddenField ID="hf_FRM_ID" runat="server" />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
        SelectCommand="SELECT [RMD_ID], [RMD_DESCR] FROM [REMINDERTYPE_M]"></asp:SqlDataSource>
</asp:Content>
