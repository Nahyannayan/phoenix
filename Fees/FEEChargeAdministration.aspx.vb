Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Data.OleDb
Imports Telerik.Web
Imports Telerik.Web.UI
Imports GemBox.Spreadsheet

Partial Class Fees_FEEChargeAdministration
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property SPName() As String
        Get
            Return ViewState("SPName")
        End Get
        Set(ByVal value As String)
            ViewState("SPName") = value
        End Set
    End Property
    Private Property GroupBy() As String
        Get
            Return ViewState("GroupBy")
        End Get
        Set(ByVal value As String)
            ViewState("GroupBy") = value
        End Set
    End Property
    Private Property RESULTTYPE_ITEMS() As String
        Get
            Return ViewState("RESULTTYPE_ITEMS")
        End Get
        Set(ByVal value As String)
            ViewState("RESULTTYPE_ITEMS") = value
        End Set
    End Property

    Private Property VSgvStLedger() As DataTable
        Get
            Return ViewState("gvStLedger")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvStLedger") = value
        End Set
    End Property
    Private Property VSgvStudentCharges() As DataTable
        Get
            Return ViewState("gvStudentCharges")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvStudentCharges") = value
        End Set
    End Property
    Private Property VSgvExcelImport() As DataTable
        Get
            Return ViewState("gvExcelImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvExcelImport") = value
        End Set
    End Property
    Private Property VSgvTrRateImport() As DataTable
        Get
            Return ViewState("gvTrRateImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvTrRateImport") = value
        End Set
    End Property
    Private Property VSGridRowCount() As Integer
        Get
            Return ViewState("GridRowCount")
        End Get
        Set(ByVal value As Integer)
            ViewState("GridRowCount") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImport)
        smScriptManager.RegisterPostBackControl(btnImpTrRate)
        smScriptManager.RegisterPostBackControl(lnkExportToExcel)
        smScriptManager.RegisterPostBackControl(lnkExportToPDF)
        smScriptManager.RegisterPostBackControl(lnkExportToPDF)

        smScriptManager.RegisterPostBackControl(ddlSource_)
        smScriptManager.RegisterPostBackControl(ddlFeeType_)
        smScriptManager.RegisterPostBackControl(btnSaveAccessSettings)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnLedger)
        'smScriptManager.RegisterPostBackControl(chkSelectAllBsu)

        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> "F100171" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            UsrBSUnits1.MenuCode = MainMnu_code
            tvBsu.MenuCode = MainMnu_code
            ClearAllFields()
            FillACD()
            FillBSU()
            Fill_RR_BSU()
            DDL_RR_BSU_SelectedIndexChanged(sender, e)
            FillAdjReason()
            FillReportType()
            ddlBSU_SelectedIndexChanged(sender, e)
            ddlTBSU_SelectedIndexChanged(sender, e)
            Me.TRbsu.Visible = False
            SetAcademicYearDate()
            Me.rblFilter.SelectedValue = 0
            Me.btnCharge.Visible = False
            Me.txtAdjDT.Text = Format(Now.Date, OASISConstants.DateFormat)
            Me.txtDate.Text = Format(Now.Date.AddDays(-1), OASISConstants.DateFormat)
            Me.txtFromDate.Text = Format(Now.Date.AddDays(-1).AddMonths(-1), OASISConstants.DateFormat)
            Me.txtToDate.Text = Format(Now.Date.AddDays(-1), OASISConstants.DateFormat)
            lnkTrRateFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("TRANSPORT RATE") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/TransportRateFormat.xls")
            lnkXcelFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("ADJUSTMENT") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/StudentAdjustmentsFormatFile.xls")
            LoadFeeGrid()
            LoadTransportFeeGrid()
            HideFilterRows()
            UsrSelStudent1.IsStudent = True
            'UsrStudentForReport.IsStudent = True
            UsrSelStudent1.ACD_ID = 0 'ddlAcademicYear.SelectedValue
            'UsrStudentForReport.ACD_ID = 0

            Me.btnFCharges.Attributes.Add("onClick", "return confirm('Continue to delete charges?');")
            Me.chkNewJV.Attributes.Add("onclick", "javascript:DisableJV();")
            BindBusinessUnitForAdjustment()
            BindSourceScreen()
            BindFeeTypes()
            'BindFeeTypeAccessBsu()

        End If
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Sub FillAdjReason()
        Dim dtReason As DataTable = FEEADJUSTMENTREQ.GET_ADJ_REASON(Session("sBsuId"))
        If Not dtReason Is Nothing Then
            ddlAdjType.DataSource = dtReason
            ddlAdjType.DataTextField = "ARM_DESCR"
            ddlAdjType.DataValueField = "ARM_ID"
            ddlAdjType.DataBind()
        End If
    End Sub
    Sub FillBSU()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID as SVB_BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') order by BSU_NAME")
        ddlBSU.DataSource = ds.Tables(0)
        ddlBSU.DataBind()
        ddlTBSU.DataSource = ds.Tables(0)
        ddlTBSU.DataBind()
        If ddlBSU.Items.Count > 0 Then
            ddlBSU.SelectedValue = Session("sBsuid")
            ddlTBSU.SelectedValue = Session("sBsuid")
        End If
    End Sub
    Sub BindBusinessUnitForAdjustment()
        Dim ds As New DataSet
        Dim str_sql As String = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW, 0) = 1 ORDER BY BSU_NAME"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataTextField = "BSU_NAMEwithShort"
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataBind()
        ddlBUnit.SelectedIndex = -1
        If Not ddlBUnit.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        UsrSelStudent1.ACD_ID = 976 'ddlAcademicYear.SelectedValue
        ddlFeeType_.DataBind()
        SetAcademicYearDate()
    End Sub
    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, FeeCommon.GetCurrentAcademicYear(Session("sBSUID")), Session("sBSUID"))
        txtFrom.Text = DTFROM
        txtFromDT.Text = DTFROM
        'txtToDate.Text = DTTO
        If txtFrom.Text = "" Then
            txtFrom.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtFromDT.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtTo.Text = Format(Now.Date, OASISConstants.DateFormat)
        txtToDT.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub
    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
        StudentDetail()
    End Sub
    Sub StudentDetail()
        Me.lblName.Text = UsrSelStudent1.STUDENT_NAME
        Me.lblDOJ.Text = Format(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "select STU_DOJ from OASIS..STUDENT_M where STU_ID=" & UsrSelStudent1.STUDENT_ID & ""), OASISConstants.DateFormat)
        Me.lblGrade.Text = UsrSelStudent1.STUDENT_GRADE_ID
    End Sub
    Private Sub StudentLedger(ByVal STUID As String, ByVal BSUID As String, ByVal FromDT As String, ByVal ToDT As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(STUID, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSUID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = FromDT 'DateTime.Now.AddYears(-2).ToShortDateString
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = ToDT 'DateTime.Now.ToShortDateString
        cmd.Parameters.Add(sqlpTODT)
        cmd.Connection = New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        adpt.SelectCommand = cmd
        'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adpt.SelectCommand.CommandTimeout = 0
        ds.Clear()
        adpt.Fill(ds)
        Dim bal As Decimal = 0.0
        ds.Tables(0).Columns.Add("Balance")
        ds.Tables(0).AcceptChanges()
        For Each Dr As DataRow In ds.Tables(0).Rows
            bal += Convert.ToDecimal(Dr("DEBIT")) - Convert.ToDecimal(Dr("CREDIT"))
            Dr("Balance") = String.Format("{0:n2}", bal)
        Next
        ViewState("gvStLedger") = ds.Tables(0)
        bindLedger()
    End Sub
    Private Sub StudentLedgerWithTrans(ByVal STUID As String, ByVal BSUID As String, ByVal FromDT As String, ByVal ToDT As String, ByRef Trans As SqlTransaction)
        'Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(STUID, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSUID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = FromDT 'DateTime.Now.AddYears(-2).ToShortDateString
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = ToDT 'DateTime.Now.ToShortDateString
        cmd.Parameters.Add(sqlpTODT)
        cmd.Transaction = Trans
        cmd.Connection = Trans.Connection
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        adpt.SelectCommand = cmd
        'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adpt.SelectCommand.CommandTimeout = 0
        ds.Clear()
        adpt.Fill(ds)
        Dim bal As Decimal = 0.0
        ds.Tables(0).Columns.Add("Balance")
        ds.Tables(0).AcceptChanges()
        For Each Dr As DataRow In ds.Tables(0).Rows
            bal += Convert.ToDecimal(Dr("DEBIT")) - Convert.ToDecimal(Dr("CREDIT"))
            Dr("Balance") = String.Format("{0:n2}", bal)
        Next
        If DirectCast(ViewState("gvStLedger"), DataTable).Rows.Count < ds.Tables(0).Rows.Count Then
            ViewState("GridRowCount") = DirectCast(ViewState("gvStLedger"), DataTable).Rows.Count
        Else
            ViewState("GridRowCount") = 0
        End If
        ViewState("gvStLedger") = ds.Tables(0)
        bindLedger()
    End Sub
    Sub bindLedger()
        If Convert.ToString(VSgvStLedger) <> "" AndAlso VSgvStLedger.Rows.Count > 0 Then
            Me.trLedger.Visible = True
            gvStLedger.DataSource = VSgvStLedger
            gvStLedger.DataBind()
        Else
            Me.trLedger.Visible = False
        End If

    End Sub

    Private Sub ClearAllFields()
        Me.btnProceedImpt.Visible = False
        Me.lblError2.Text = ""
        lblError.Text = ""
        Me.lblMessage.Text = ""
        Me.lblError3.Text = ""
        Me.lblError.Text = ""
        Me.lblXLOneway.Text = ""
        Me.lblXLNormal.Text = ""
        Me.btnImport.Visible = True
        Me.btnCommitImprt.Visible = False
        Me.gvExcelImport.Visible = True
        UsrSelStudent1.ClearDetails()
        Me.TRbsu.Visible = False
        BindExcel()
        SetAcademicYearDate()
        bindLedger()
        bindStudentCharges()
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call ClearAllFields()
    End Sub

    Private Dr As Decimal = 0
    Private i As Int32 = 0
    'Private Bal As Decimal = 0

    Protected Sub gvStLedger_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStLedger.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(4).Text = "TOTAL"
            e.Row.Cells(5).Text = String.Format("{0:n2}", VSgvStLedger.Compute("Sum(DEBIT)", ""))
            e.Row.Cells(6).Text = String.Format("{0:n2}", VSgvStLedger.Compute("Sum(CREDIT)", ""))
            e.Row.Cells(7).Text = String.Format("{0:n2}", Convert.ToDecimal(e.Row.Cells(5).Text) - Convert.ToDecimal(e.Row.Cells(6).Text))
        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            i += 1
            If VSGridRowCount <> 0 And i > VSGridRowCount Then ' code to change the color of newly added rows in ledger
                e.Row.BackColor = Drawing.Color.BlanchedAlmond
            End If
        End If
    End Sub

    Protected Sub btnLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLedger.Click

        StudentLedger(UsrSelStudent1.STUDENT_ID, Session("sBsuid"), Me.txtFrom.Text, Me.txtTo.Text)
    End Sub
    Protected Sub btnFCharges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFCharges.Click
        lblError.Text = ""
        If UsrSelStudent1.STUDENT_ID <> "" And Not Session("sBsuid") Is Nothing Then
            DeleteFutureCharges()
        Else
            'lblError.Text = "Please select the student or session is expired"
            usrMessageBar.ShowNotification("Please select the student or session is expired", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Sub DeleteFutureCharges()
        lblError.Text = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = ""
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.Output
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            pParms(0).Value = Session("sBsuid")
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar)
            pParms(2).Value = UsrSelStudent1.STUDENT_ID
            pParms(3) = New SqlClient.SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.DELETE_STU_FUTUREDATEDCHARGES", pParms)

            If pParms(3).Value = "0" Then
                stTrans.Commit()
                'Me.lblError.Text = IIf(pParms(1).Value > 0, "Charges has been removed successfully", "No future charges found")
                usrMessageBar.ShowNotification(IIf(pParms(1).Value > 0, "Charges has been removed successfully", "No future charges found"), UserControls_usrMessageBar.WarningType.Danger)
                StudentLedger(UsrSelStudent1.STUDENT_ID, Session("sBsuid"), Me.txtFrom.Text, Me.txtTo.Text)
            Else
                stTrans.Rollback()
                'Me.lblError.Text = "Unable to complete the request"
                usrMessageBar.ShowNotification("Unable to complete the request", UserControls_usrMessageBar.WarningType.Danger)
            End If

        Catch
            stTrans.Rollback()
            'Me.lblError.Text = "Unable to complete the request"
            usrMessageBar.ShowNotification("Unable to complete the request", UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub btnLoadChgs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadChgs.Click
        StudentCharges()
    End Sub
    Private Sub StudentCharges()
        Dim dt As New DataTable
        Dim ds As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim sqlParam(4) As SqlParameter

        Dim cmd As New SqlCommand("FEES.FeeChargeSkipped")
        cmd.CommandType = CommandType.StoredProcedure
        'sqlParam(0) = Mainclass.CreateSqlParameter("@FeeID", Me.ddlFeeType.SelectedValue, SqlDbType.Int)
        Dim sqlpFeeID As New SqlParameter("@FeeID", SqlDbType.Int)
        sqlpFeeID.Value = Me.ddlFeeType_.SelectedValue
        cmd.Parameters.Add(sqlpFeeID)
        'sqlParam(1) = Mainclass.CreateSqlParameter("@BsuID", Me.ddlBSU.SelectedValue, SqlDbType.VarChar)
        Dim sqlpBsuID As New SqlParameter("@BsuID", SqlDbType.VarChar, 10)
        sqlpBsuID.Value = Me.ddlBSU.SelectedValue
        cmd.Parameters.Add(sqlpBsuID)
        'sqlParam(2) = Mainclass.CreateSqlParameter("@DateFrom", Me.txtFromDT.Text, SqlDbType.DateTime)
        Dim sqlpFromDT As New SqlParameter("@DateFrom", SqlDbType.DateTime)
        sqlpFromDT.Value = Me.txtFromDT.Text
        cmd.Parameters.Add(sqlpFromDT)
        'sqlParam(3) = Mainclass.CreateSqlParameter("@DateTo", Me.txtToDT.Text, SqlDbType.DateTime)
        Dim sqlpTODT As New SqlParameter("@DateTo", SqlDbType.DateTime)
        sqlpTODT.Value = Me.txtToDT.Text
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        'dt = Mainclass.getDataTable("FEES.FeeChargeSkipped", sqlParam, str_conn)
        Dim adpt As New SqlDataAdapter
        adpt.SelectCommand = cmd
        adpt.SelectCommand.CommandTimeout = 0
        ds.Clear()
        adpt.Fill(ds)
        VSgvStudentCharges = ds.Tables(0)

        bindStudentCharges()
    End Sub
    Sub bindStudentCharges()
        If Convert.ToString(VSgvStudentCharges) <> "" AndAlso VSgvStudentCharges.Rows.Count > 0 Then
            Me.trgvStudentCharges.Visible = True
            Me.gvStudentCharges.DataSource = VSgvStudentCharges
            Me.gvStudentCharges.DataBind()
        Else
            Me.trgvStudentCharges.Visible = False
        End If

    End Sub

    Protected Sub gvStLedger_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStLedger.PageIndexChanging
        gvStLedger.PageIndex = e.NewPageIndex
        bindLedger()
    End Sub

    Protected Sub gvStudentCharges_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentCharges.PageIndexChanging
        gvStudentCharges.PageIndex = e.NewPageIndex
        bindStudentCharges()
    End Sub

    Protected Sub ddlBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSU.SelectedIndexChanged
        fillAcdY()
    End Sub

    Sub fillAcdY()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Me.ddlBSU.SelectedValue)
        ddlAcdY.DataSource = dtACD
        ddlAcdY.DataTextField = "ACY_DESCR"
        ddlAcdY.DataValueField = "ACD_ID"
        ddlAcdY.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcdY.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        FillFeeType()
    End Sub
    Sub FillFeeType()
        Dim sql As String = "select distinct FEE_ID,FEE_DESCR from fees.FEESETUP_S A INNER JOIN FEES.FEES_M B ON A.FSP_FEE_ID=B.FEE_ID where FSP_BSU_ID='" & Me.ddlBSU.SelectedValue & "' and FSP_ACD_ID='" & Me.ddlAcdY.SelectedValue & "'"
        Dim dtFeeType As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, sql)
        ddlFeeType_.DataSource = dtFeeType
        ddlFeeType_.DataTextField = "FEE_DESCR"
        ddlFeeType_.DataValueField = "FEE_ID"
        ddlFeeType_.DataBind()
    End Sub

    Protected Sub ddlAcdY_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdY.SelectedIndexChanged
        FillFeeType()
    End Sub
    Protected Sub btnCharge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCharge.Click
        Charging(False)
    End Sub
    Protected Sub btnCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommit.Click
        Charging(True)
    End Sub
    Sub Charging(ByVal Commit As Boolean)
        lblError.Text = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Me.btnCommit.Visible = False
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = ""
            retval = F_GenerateFeeCharge(Me.ddlBSU.SelectedValue, Me.ddlAcdY.SelectedValue, Me.lblGrade.Text.Trim, UsrSelStudent1.STUDENT_ID, Me.txtTo.Text, stTrans)
            If retval = "0" Then
                StudentLedgerWithTrans(UsrSelStudent1.STUDENT_ID, Me.ddlBSU.SelectedValue, Me.txtFromDT.Text, Me.txtToDT.Text, stTrans)
                If Commit = True Then
                    ChargeAuditLog(stTrans)
                    stTrans.Commit()
                Else
                    Me.btnCommit.Visible = True
                    stTrans.Rollback()
                End If
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Sub ChargeAuditLog(ByRef trans As SqlTransaction)
        Dim Qry As String
        Qry = "insert into FEES.FeeChargeAuditLog(FCA_STU_ID,FCA_BSU_ID,FCA_Date,FCA_User)Values('" & UsrSelStudent1.STUDENT_ID & "','" & Me.ddlBSU.SelectedValue & "','" & DateTime.Now & "','" & Session("sUsr_name") & "')"
        Dim retval As Integer = SqlHelper.ExecuteNonQuery(trans, CommandType.Text, Qry)
    End Sub
    Private Shared Function F_GenerateFeeCharge(ByVal BSU_ID As String, ByVal ACD_ID As Integer, ByVal GRD_ID As String, ByVal STU_ID As Integer, ByVal Dt As String, ByRef p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(0).Value = BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(2).Value = ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@GRD_ID", SqlDbType.VarChar)
        pParms(3).Value = GRD_ID
        pParms(4) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(4).Value = STU_ID
        pParms(5) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(5).Value = Dt
        pParms(6) = New SqlClient.SqlParameter("@BForceCharge", SqlDbType.TinyInt)
        pParms(6).Value = 1

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_GenerateFeeCharge", pParms)
        F_GenerateFeeCharge = pParms(1).Value
    End Function
    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Me.gvImportSmry.DataSource = Nothing
        Me.gvImportSmry.DataBind()
        VSgvExcelImport = Nothing
        BindExcel()
        ImportAdjustmentSheet()
        'UpLoadDBF()
        'Dim conn As New OleDbConnection
        'Me.lblError.Text = ""
        'Try
        '    Dim str_conn As String = ""
        '    Dim cmd As New OleDbCommand
        '    Dim da As New OleDbDataAdapter
        '    'Dim ds As DataSet
        '    Dim dtXcel As DataTable
        '    Dim FileName As String = ""
        '    If (FileUpload1.HasFile) Then
        '        Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
        '        FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
        '        If strFileType = ".xls" Or strFileType = ".xlsx" Then
        '            If File.Exists(FileName) Then
        '                File.Delete(FileName)
        '            End If
        '            Me.FileUpload1.SaveAs(FileName)
        '            str_conn = IIf(strFileType = ".xls", "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties=""Excel 8.0;HDR=YES;""" _
        '                            , "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2""")
        '        Else
        '            Me.lblError.Text = "Only Excel files are allowed"
        '            Exit Try
        '        End If

        '        Dim Query = "SELECT * FROM [Sheet1$]"
        '        conn = New OleDbConnection(str_conn)
        '        'If conn.State = ConnectionState.Closed Then conn.Open()
        '        'cmd = New OleDbCommand(Query, conn)
        '        'da = New OleDbDataAdapter(cmd)
        '        'ds = New DataSet()
        '        'da.Fill(ds)
        '        'If conn.State = ConnectionState.Open Then
        '        '    conn.Close()
        '        'End If
        '        'dtXcel = ds.Tables(0)
        '        dtXcel = Mainclass.FetchFromExcelIntoDataTable(FileName, 1, 1, 9)
        '        If dtXcel.Rows.Count > 0 Then
        '            VSgvExcelImport = dtXcel
        '            BindExcel()
        '            ValidateAndSave(dtXcel)
        '        Else
        '            Me.lblError.Text += "Empty Document!!"
        '        End If

        '    Else
        '        Me.lblError.Text += "File not Found!!"
        '        Me.FileUpload1.Focus()
        '    End If

        '    File.Delete(FileName) 'delete the file after copying the contents
        'Catch
        '    If conn.State = ConnectionState.Open Then
        '        conn.Close()
        '    End If
        '    Me.lblError.Text += Err.Description
        'End Try
    End Sub

    Private Sub ImportAdjustmentSheet()
        Try
            lblError.Text = ""
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If
            End If
            If File.Exists(FileName) Then
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                'Dim ef As New GemBox.Spreadsheet.ExcelFile
                Dim workbook = ExcelFile.Load(FileName)
                ' Select active worksheet.
                Dim worksheet = workbook.Worksheets.ActiveWorksheet
                Dim dtXL As New DataTable
                If worksheet.Rows.Count > 1 Then
                    dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With _
                            {
                             .ColumnHeaders = True, _
                             .StartRow = 0, _
                             .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count, _
                             .NumberOfRows = worksheet.Rows.Count,
                             .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                            })
                End If
                If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then
                   Dim dvxl As DataView = New DataView(dtXL)
                    dvxl.RowFilter = "ISNULL(STU_NO,'') <> ''"
                    VSgvExcelImport = dvxl.ToTable()
                    Dim Columns As DataColumnCollection = VSgvExcelImport.Columns
                    If Not Columns.Contains("TAX_CODE") Then
                        VSgvExcelImport.Columns.Add("TAX_CODE", GetType(String), "NA")
                    End If
                    If Not Columns.Contains("bVALID") Then
                        VSgvExcelImport.Columns.Add("bVALID", GetType(Boolean), True)
                    End If
                    If Not Columns.Contains("ERROR_MSG") Then
                        VSgvExcelImport.Columns.Add("ERROR_MSG", GetType(String), "")
                    End If
                    BindExcel()
                    Me.trGvImport.Visible = True
                    Me.lblMessage.Text = "Click Proceed to Import the data"
                    gvExcelImport.Columns(7).Visible = False 'hides the error message column initially
                    Me.btnProceedImpt.Visible = True
                    Me.btnImport.Visible = False
                End If
                'SAVE_SFTP_LOG("IMPORTDATA", LocalFilePath, "SUCCESS")
            End If
        Catch ex As Exception
            'SAVE_SFTP_LOG("IMPORTDATA", LocalFilePath, "FAILED", ex.InnerException.Message)
            Errorlog("Unable to load datatable from file - ImportAdjustmentSheet()")
        End Try
    End Sub
    Sub BindExcel()
        Me.gvExcelImport.DataSource = VSgvExcelImport
        Me.gvExcelImport.DataBind()
    End Sub

    Private Sub ValidateAndSave(ByRef DT As DataTable)
        Dim pParms(11) As SqlClient.SqlParameter
        Dim ErrorLog As String = ""
        Me.lblError2.Text = ""
        Dim str_conn As String = IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString)
        Dim qry As String = "select ISNULL(MAX(FAI_BATCHNO),0)+1 from [DataImport].[FEE_ADJUSTMENT_IMPORT]"
        Dim FAI_BATCHNO As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, qry)
        ViewState("FAI_BATCHNO") = FAI_BATCHNO
        Dim i = 1
        Dim StuType As String = "S"
        If rbEnquiry.Checked Then
            StuType = "E"
        Else
            StuType = "S"
        End If


        Dim cmd As New SqlCommand
        Dim strconnn As String = IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString)
        Dim objConn As New SqlConnection(strconnn)
        objConn.Open()
        Dim Success As Boolean = True
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim Narration As String, NarrationColumnExists As Boolean
        If DT.Columns.Contains("NARRATION") Then
            NarrationColumnExists = True
        Else
            NarrationColumnExists = False
        End If
        For Each Dr As DataRow In DT.Rows
            Narration = ""
            If NarrationColumnExists Then
                Narration = Dr("NARRATION").ToString.Replace("'", "")
            End If
            If Narration = "" Then Narration = txtNarration.Text
            cmd.Dispose()
            cmd = New SqlCommand("FEES.ValidateAndInsertExceldata", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(pParms(1))
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            pParms(0).Value = Session("sBsuid")
            cmd.Parameters.Add(pParms(0))
            pParms(2) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar)
            pParms(2).Value = Dr("STU_NO").ToString
            cmd.Parameters.Add(pParms(2))
            pParms(3) = New SqlClient.SqlParameter("@FEE_DESC", SqlDbType.VarChar)
            pParms(3).Value = Dr("FEE_DESCR").ToString
            cmd.Parameters.Add(pParms(3))
            pParms(4) = New SqlClient.SqlParameter("@DRCR", SqlDbType.VarChar, 2)
            pParms(4).Value = Dr("DRCR").ToString
            cmd.Parameters.Add(pParms(4))
            pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal)
            pParms(5).Value = Dr("AMOUNT").ToString
            cmd.Parameters.Add(pParms(5))
            pParms(6) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
            pParms(6).Value = Me.txtAdjDT.Text
            cmd.Parameters.Add(pParms(6))
            pParms(7) = New SqlClient.SqlParameter("@ADJ_TYPE_ID", SqlDbType.Int)
            pParms(7).Value = Me.ddlAdjType.SelectedValue
            cmd.Parameters.Add(pParms(7))
            pParms(8) = New SqlClient.SqlParameter("@NARRATION", SqlDbType.VarChar)
            pParms(8).Value = Narration
            cmd.Parameters.Add(pParms(8))
            pParms(9) = New SqlClient.SqlParameter("@FAI_BATCHNO", SqlDbType.Int)
            pParms(9).Value = FAI_BATCHNO
            cmd.Parameters.Add(pParms(9))
            pParms(11) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar)
            pParms(11).Value = StuType
            cmd.Parameters.Add(pParms(11))
            pParms(10) = New SqlClient.SqlParameter("@FAI_USER", SqlDbType.VarChar)
            pParms(10).Value = Session("sUsr_name")
            cmd.Parameters.Add(pParms(10))
            Dim retval As String
            cmd.ExecuteNonQuery()
            '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "FEES.ValidateAndInsertExceldata", pParms)
            retval = pParms(1).Value
            If retval <> 0 Then
                Success = False
                If Me.gvExcelImport.Rows.Count > i Then
                    Me.gvExcelImport.Rows(i - 1).BackColor = Drawing.Color.Red
                End If
                ErrorLog = ErrorLog & "@" & retval & "#" & Convert.ToString(Dr("STU_NO")) ' keeping errono and line in a string variable for further use
            End If
            i += 1
        Next
        If Success = True Then
            stTrans.Commit()
        Else
            stTrans.Rollback()
        End If
        If ErrorLog <> "" Then
            Me.btnProceedImpt.Visible = False
            Me.btnImport.Visible = True
            Me.lblError2.Text = ""
            ShowErrorLog(ErrorLog)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "delete from [DataImport].[FEE_ADJUSTMENT_IMPORT] where FAI_BATCHNO='" & FAI_BATCHNO & "'")
        Else
            Me.btnProceedImpt.Visible = True
            Me.btnImport.Visible = False
        End If
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
    End Sub
    Private Sub ShowErrorLog(ByVal Err As String)
        Dim msg As String = ""
        Dim ReturnVal() As String = Err.Split("@")
        For i As Integer = 0 To ReturnVal.Length - 1
            If ReturnVal(i).Trim <> "" Then
                Dim aa() As String = ReturnVal(i).Split("#")
                'Me.lblError2.Text = Me.lblError2.Text & "Student No(" & aa(1) & ") : "
                'Me.lblError2.Text = Me.lblError2.Text & IIf(aa(0) = "100", "Not found,", IIf(aa(0) = "101", "No Fee Description,", IIf(aa(0) = "102", "Amount not found,", IIf(aa(0) = "103", "Select Fee Adjustment type,", ""))))
                msg += "Student No(" & aa(1) & ") : "
                msg += IIf(aa(0) = "100", "Not found,", IIf(aa(0) = "101", "No Fee Description,", IIf(aa(0) = "102", "Amount not found,", IIf(aa(0) = "103", "Select Fee Adjustment type,", ""))))
                usrMessageBar.ShowNotification(msg, UserControls_usrMessageBar.WarningType.Danger)
            End If
        Next
    End Sub

    Protected Sub gvExcelImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExcelImport.PageIndexChanging
        Me.gvExcelImport.PageIndex = e.NewPageIndex
        BindExcel()
    End Sub

    Protected Sub btnProceedImpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceedImpt.Click
        Try
            Dim AdjDt As Date = CDate(txtAdjDT.Text)
        Catch ex As Exception
            'lblError.Text = "Invalid adjustment date"
            usrMessageBar.ShowNotification("Invalid adjustment date", UserControls_usrMessageBar.WarningType.Danger)
            txtAdjDT.Focus()
            Exit Sub
        End Try
        Dim str_conn As String = IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString)
        Using con As New SqlConnection(str_conn)
            con.Open()
            Dim stTrans As SqlTransaction = con.BeginTransaction
            Try
                Using cmd As New SqlCommand("DataImport.IMPORT_BULK_ADJUSTMENT", con, stTrans)
                    cmd.CommandTimeout = 0
                    cmd.CommandType = CommandType.StoredProcedure
                    Dim sqlParam(9) As SqlParameter
                    Dim dtParam As DataTable = VSgvExcelImport
                    If dtParam.Columns.Contains("bVALID") Then
                        dtParam.Columns.Remove("bVALID")
                    End If
                    If dtParam.Columns.Contains("ERROR_MSG") Then
                        dtParam.Columns.Remove("ERROR_MSG")
                    End If
                    dtParam.AcceptChanges()
                    sqlParam(0) = New SqlParameter("@DT_XL", dtParam)
                    cmd.Parameters.Add(sqlParam(0))
                    sqlParam(1) = New SqlParameter("@BSU_ID", ddlBUnit.SelectedValue)
                    cmd.Parameters.Add(sqlParam(1))
                    sqlParam(2) = New SqlParameter("@ARM_ID", ddlAdjType.SelectedValue)
                    cmd.Parameters.Add(sqlParam(2))
                    sqlParam(3) = New SqlParameter("@ADJ_DATE", txtAdjDT.Text)
                    cmd.Parameters.Add(sqlParam(3))
                    sqlParam(4) = New SqlParameter("@FAI_USER", Session("sUsr_name"))
                    cmd.Parameters.Add(sqlParam(4))
                    sqlParam(5) = New SqlParameter("@ADJ_REASON_ID", 0)
                    cmd.Parameters.Add(sqlParam(5))
                    sqlParam(6) = New SqlParameter("@RET_VAL", 0)
                    sqlParam(6).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(6))
                    sqlParam(7) = New SqlParameter("@BATCH_NO", 0)
                    sqlParam(7).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(7))
                    sqlParam(8) = New SqlParameter("@bSET_DEFAULT_TAX_CODE", chkDefaultTaxcode.Checked)
                    cmd.Parameters.Add(sqlParam(8))

                    Using dr As SqlDataReader = cmd.ExecuteReader()
                        Dim dttb As New DataTable
                        dttb.Load(dr)
                        If sqlParam(6).Value <> 0 Then
                            stTrans.Rollback()
                            'stTrans.Commit()
                            Me.btnImport.Visible = True
                            Me.btnCommitImprt.Visible = False
                            btnProceedImpt.Visible = False
                        Else
                            stTrans.Commit()
                            btnProceedImpt.Visible = False
                            Me.btnCommitImprt.Visible = True
                            ViewState("FAI_BATCHNO") = sqlParam(7).Value
                            ShowAdjustmentSummary()
                            DisableControls(False)
                        End If
                        If Not dttb Is Nothing AndAlso dttb.Rows.Count > 0 Then
                            gvExcelImport.Columns(7).Visible = True 'shoes the error message column
                            VSgvExcelImport = dttb
                            BindExcel()
                        End If
                    End Using
                    con.Close()
                End Using
            Catch ex As Exception
                'Me.lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
            End Try
        End Using
    End Sub

    Private Sub ShowAdjustmentSummary()
        Dim str_conn As String = IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString)
        Dim dsSummary = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "dbo.GET_FEEADJUSTMENT_BULK_IMPORT_SUMMARY '" & ViewState("FAI_BATCHNO") & "'")
        If dsSummary.Tables(0).Rows.Count > 0 Then
            Me.gvImportSmry.DataSource = dsSummary.Tables(0)
            Me.gvImportSmry.DataBind()
        End If
    End Sub
    Private Sub ImportCharges(ByVal Commit As Boolean)
        'Dim str_conn As String = IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString)
        Dim objConn As New SqlConnection(IIf(rblAdjFor.SelectedValue = "F", ConnectionManger.GetOASIS_FEESConnectionString, ConnectionManger.GetOASIS_SERVICESConnectionString)) '
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(2) As SqlParameter
        'Dim sqlParam2(1) As SqlParameter
        Dim msg As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand
            cmd.Dispose()
            cmd = New SqlCommand("IMPORT_FEE_ADJUSTMENTS", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            Dim retval As String = ""
            sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retval, SqlDbType.VarChar, True, 200)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@FAI_BATCHNO", ViewState("FAI_BATCHNO"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            'Mainclass.ExecuteParamQRY(objConn, stTrans, "IMPORT_FEE_ADJUSTMENTS", sqlParam)
            cmd.ExecuteNonQuery()
            retval = sqlParam(0).Value
            ' Me.lblError.Text += retval
            msg += retval
            usrMessageBar.ShowNotification(msg, UserControls_usrMessageBar.WarningType.Danger)

            If retval Is Nothing Or retval = "" Then
                dsSummary = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, "GET_FEEADJUSTMENTS_IMPORT_SUMMARY '" & ViewState("FAI_BATCHNO") & "'")
                If dsSummary.Tables(0).Rows.Count > 0 Then
                    Me.btnCommitImprt.Visible = True
                    Me.trGvImport.Visible = False
                    Me.gvImportSmry.DataSource = dsSummary.Tables(0)
                    Me.gvImportSmry.DataBind()
                Else
                    Me.btnCommitImprt.Visible = False
                    Me.trGvImport.Visible = True
                End If
            End If

            If Commit = True Then
                'Me.lblError.Text = "Data Saved successfully"
                usrMessageBar.ShowNotification("Data Saved successfully", UserControls_usrMessageBar.WarningType.Success)
                Me.lblMessage.Text = ""
                stTrans.Commit()
                Me.btnCommitImprt.Visible = False
                Me.btnProceedImpt.Visible = False
                Me.btnImport.Visible = True
                Me.btnAddAdj.Visible = True
                DisableControls(True)
            Else
                Me.lblMessage.Text = "Click the Commit button to commit the transaction"
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub btnCommitImprt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitImprt.Click
        'ImportCharges(True)
        h_Processeing.Value = "1"
        Dim objfba As New FeeBulkAdjustment
        If objfba.IMPORT_ADJUSTMENT(ViewState("FAI_BATCHNO"), rblAdjFor.SelectedValue) Then
            'Me.lblError.Text = "Data Saved successfully"
            'Me.lblMessage.Text = ""
            'Me.btnCommitImprt.Visible = False
            'Me.btnProceedImpt.Visible = False
            'Me.btnImport.Visible = True
            'Me.btnAddAdj.Visible = True
            'DisableControls(True)
            'Else
            '    Me.lblError.Text = "Unable to Save adjustments"
        End If
    End Sub
    Protected Sub btnCancelAdj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAdj.Click
        txtNarration.Text = ""
        lblError.Text = ""
        DisableControls(True)
        Me.gvImportSmry.DataSource = Nothing
        Me.gvImportSmry.DataBind()
        VSgvExcelImport = Nothing
        BindExcel()
        Me.trGvImport.Visible = False
        Me.lblMessage.Text = ""
        gvExcelImport.Columns(7).Visible = False 'hides the error message column initially
        Me.btnProceedImpt.Visible = False
        Me.btnCommitImprt.Visible = False
        Me.btnImport.Visible = True
        ViewState("FAI_BATCHNO") = 0
        If Not ddlBUnit.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub btnAddAdj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAdj.Click
        btnCancelAdj_Click(sender, e)
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBUnit.SelectedIndexChanged
        btnCancelAdj_Click(Nothing, Nothing)
    End Sub
    Protected Sub btnPostback_Click(sender As Object, e As EventArgs)
        If Me.h_STATUS.Value = "0" Then
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            '    ddlAcademicYear.SelectedItem.Value & " - " & txtFrom.Text & " - " & ddBusinessunit.SelectedItem.Value, _
            '    "Insert", Page.User.Identity.Name.ToString, Me.Page)
            Dim errormsg As String = h_errormessage.Value
            'Clear_All()
            Me.lblMessage.Text = ""
            Me.btnCommitImprt.Visible = False
            Me.btnProceedImpt.Visible = False
            Me.btnImport.Visible = True
            Me.btnAddAdj.Visible = True
            DisableControls(True)
            'Me.lblError.Text = errormsg
            usrMessageBar.ShowNotification(errormsg, UserControls_usrMessageBar.WarningType.Danger)
            h_Processeing.Value = ""
        End If
    End Sub
    Protected Sub hfPostback_ValueChanged(sender As Object, e As EventArgs) Handles hfPostback.ValueChanged
        If Me.h_STATUS.Value = "0" Then
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            '    ddlAcademicYear.SelectedItem.Value & " - " & txtFrom.Text & " - " & ddBusinessunit.SelectedItem.Value, _
            '    "Insert", Page.User.Identity.Name.ToString, Me.Page)
            Dim errormsg As String = h_errormessage.Value
            'Clear_All()
            Me.lblMessage.Text = ""
            Me.btnCommitImprt.Visible = False
            Me.btnProceedImpt.Visible = False
            Me.btnImport.Visible = True
            Me.btnAddAdj.Visible = True
            DisableControls(True)
            'Me.lblError.Text = errormsg
            usrMessageBar.ShowNotification(errormsg, UserControls_usrMessageBar.WarningType.Danger)
            h_Processeing.Value = ""
        End If
    End Sub
    Sub DisableControls(ByVal bEnabled As Boolean)
        ddlBUnit.Enabled = bEnabled
        txtAdjDT.Enabled = bEnabled
        rblAdjFor.Enabled = bEnabled
        ddlAdjType.Enabled = bEnabled
        rbEnquiry.Enabled = bEnabled
        rbEnrollment.Enabled = bEnabled
        FileUpload1.Enabled = bEnabled
        txtNarration.Enabled = bEnabled
    End Sub
    Protected Sub gvExcelImport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExcelImport.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim DR, CR As New Double
            'VSgvExcelImport.Columns(3).DataType = GetType(String)
            e.Row.Cells(2).Text = "TOTAL (DR-CR)"
            DR = 0 : CR = 0
            For Each dtr As DataRow In VSgvExcelImport.Rows
                If dtr("DRCR").ToString.ToUpper = "DR" Then
                    DR = DR + FeeCollection.GetDoubleVal(dtr("AMOUNT"))
                ElseIf dtr("DRCR").ToString.ToUpper = "CR" Then
                    CR = CR + FeeCollection.GetDoubleVal(dtr("AMOUNT"))
                End If
            Next
            'DR = Convert.ToDouble(IIf(VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'DR'").Equals(DBNull.Value), 0, VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'DR'")))
            'CR = Convert.ToDouble(IIf(VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'CR'").Equals(DBNull.Value), 0, VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'CR'")))
            e.Row.Cells(4).Text = String.Format("{0:n2}", DR - CR)
            e.Row.Cells(0).Text = "Records(" & VSgvExcelImport.Rows.Count & ")"
            'ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            '    Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)
            '    ' Retrieve the key value for the current row. Here it is an int.
            '    Dim bVALID As Boolean = CBool(rowView("bVALID"))
            '    If gvExcelImport.Columns(7).Visible Then
            '        If bVALID = False Then
            '            e.Row.ForeColor = Drawing.Color.Red
            '        Else
            '            e.Row.Cells(7).ForeColor = Drawing.Color.Green
            '        End If
            '    End If
            '    i += 1
            '    If Val(ViewState("GridRowCount")) <> 0 And i > Val(ViewState("GridRowCount")) Then ' code to change the color of newly added rows in ledger
            '        e.Row.BackColor = Drawing.Color.BlanchedAlmond
            '    End If
        End If
    End Sub

    'Protected Sub lnkXcelFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkXcelFormat.Click
    '    Try
    '        Dim filePath As String = "~\Template\StudentAdjustmentsFormatFile.xls"lnkXcelFormat_Click
    '        Response.Redirect(filePath)
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.Message
    '    End Try

    'End Sub

    Protected Sub lnkBLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkButton As LinkButton = TryCast(sender, LinkButton)
        If lnkButton.CommandName = "LEDGER" Then
            Dim row As GridViewRow = DirectCast(lnkButton.NamingContainer, GridViewRow)
            Dim stuid As Integer = Me.gvStudentCharges.DataKeys(row.RowIndex)("STU_ID")
            UsrSelStudent1.SetStudentDetails(stuid)
            'UsrSelStudent1.DataBind()
            UsrSelStudent1_StudentNoChanged(sender, e)
            StudentLedger(stuid, Me.ddlBSU.SelectedValue, Me.txtFromDT.Text, Me.txtToDT.Text)
            Me.txtFrom.Text = Me.txtFromDT.Text
            Me.txtTo.Text = Me.txtToDT.Text
            TabContainer1.ActiveTabIndex = 1
        End If
    End Sub

    '--------------Transport Rate Import-----------------------

    Protected Sub ddlTBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fillTranAcdY()
    End Sub
    Sub fillTranAcdY()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Me.ddlTBSU.SelectedValue)
        ddlACDYear.DataSource = dtACD
        ddlACDYear.DataTextField = "ACY_DESCR"
        ddlACDYear.DataValueField = "ACD_ID"
        ddlACDYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlACDYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnImpTrRate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New OleDbConnection
        Me.lblError.Text = ""
        Dim msg As String = ""
        Try
            Dim str_conn As String = ""
            Dim cmd As New OleDbCommand
            Dim da As New OleDbDataAdapter
            Dim dtXcel As DataTable
            Dim FileName As String = ""
            If (FileUpload2.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload2.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload2.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload2.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If

                dtXcel = Mainclass.FetchFromExcelIntoDataTable(FileName, 1, 1, 9)
                If Not dtXcel Is Nothing AndAlso dtXcel.Rows.Count > 0 Then
                    VSgvTrRateImport = dtXcel.Copy
                    BindTrRateExcel()
                    ValidateAndSaveTrRates(dtXcel)
                Else
                    ' Me.lblError.Text += "Empty Document!!"
                    msg += "Empty Document!!"
                    usrMessageBar.ShowNotification(msg, UserControls_usrMessageBar.WarningType.Danger)
                End If

            Else
                'Me.lblError.Text += "File not Found!!"
                msg += "File not Found!!"
                usrMessageBar.ShowNotification(msg, UserControls_usrMessageBar.WarningType.Danger)
                Me.FileUpload1.Focus()
            End If

            File.Delete(FileName) 'delete the file after copying the contents
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            'Me.lblError.Text += Err.Description
            msg += Err.Description
            usrMessageBar.ShowNotification(msg, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Sub BindTrRateExcel()
        If Not VSgvTrRateImport Is Nothing Then
            Me.gvTrRateImport.DataSource = VSgvTrRateImport
            Me.gvTrRateImport.DataBind()
        Else
            'Me.trGvImport.Visible = False
        End If

    End Sub

    Protected Sub gvTrRateImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Me.gvTrRateImport.PageIndex = e.NewPageIndex
        BindTrRateExcel()
    End Sub

    Protected Sub gvTrRateImport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'VSgvTrRateImport.Columns(3).DataType = GetType(Double)
            'VSgvTrRateImport.AcceptChanges()
            e.Row.Cells(3).Text = IIf(IsNumeric(e.Row.Cells(3).Text), String.Format("{0:n2}", Val(e.Row.Cells(3).Text)), e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = IIf(IsNumeric(e.Row.Cells(4).Text), String.Format("{0:n2}", Val(e.Row.Cells(4).Text)), e.Row.Cells(4).Text)
            e.Row.Cells(5).Text = IIf(IsNumeric(e.Row.Cells(5).Text), String.Format("{0:n2}", Val(e.Row.Cells(5).Text)), e.Row.Cells(5).Text)
            e.Row.Cells(6).Text = IIf(IsNumeric(e.Row.Cells(6).Text), String.Format("{0:n2}", Val(e.Row.Cells(6).Text)), e.Row.Cells(6).Text)
            e.Row.Cells(7).Text = IIf(IsNumeric(e.Row.Cells(7).Text), String.Format("{0:n2}", Val(e.Row.Cells(7).Text)), e.Row.Cells(7).Text)
            e.Row.Cells(8).Text = IIf(IsNumeric(e.Row.Cells(8).Text), String.Format("{0:n2}", Val(e.Row.Cells(8).Text)), e.Row.Cells(8).Text)
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            Dim N1, N2, N3, Way1, Way2, Way3 As New Double
            'VSgvExcelImport.Columns(3).DataType = GetType(String)
            e.Row.Cells(2).Text = "TOTAL :"

            For Each dtr As DataRow In VSgvTrRateImport.Rows
                N1 = N1 + Val(dtr(3).ToString)
                N2 = N2 + Val(dtr(5).ToString)
                N3 = N3 + Val(dtr(7).ToString)
                Way1 = Way1 + Val(dtr(4).ToString)
                Way2 = Way2 + Val(dtr(6).ToString)
                Way3 = Way3 + Val(dtr(8).ToString)
            Next
            e.Row.Cells(3).Text = String.Format("{0:n2}", N1)
            e.Row.Cells(4).Text = String.Format("{0:n2}", Way1)
            e.Row.Cells(5).Text = String.Format("{0:n2}", N2)
            e.Row.Cells(6).Text = String.Format("{0:n2}", Way2)
            e.Row.Cells(7).Text = String.Format("{0:n2}", N3)
            e.Row.Cells(8).Text = String.Format("{0:n2}", Way3)

            lblXLNormal.Text = "(EXCEL TOTAL)   Normal : " & String.Format("{0:n2}", N1 + N2 + N3)
            lblXLOneway.Text = "OneWay : " & String.Format("{0:n2}", Way1 + Way2 + Way3)
        End If
    End Sub

    Private Sub ValidateAndSaveTrRates(ByRef DT As DataTable)
        Dim pParms(12) As SqlClient.SqlParameter
        Dim ErrorLog As String = ""
        Me.lblError3.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim qry As String = "select ISNULL(MAX(FTI_BATCHNO),0)+1 from [DataImport].[FEE_TRANSPORTRATE_IMPORT]"
        Dim FTI_BATCHNO As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, qry)
        ViewState("FTI_BATCHNO") = FTI_BATCHNO
        Dim i = 1
        Dim cmd As New SqlCommand

        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim Success As Boolean = True

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        For Each Dr As DataRow In DT.Rows
            cmd.Dispose()
            cmd = New SqlCommand("FIX.ValidateAndImportTrRateExceldata", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            pParms(0).Value = Me.ddlTBSU.SelectedValue
            cmd.Parameters.Add(pParms(0))
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@FLOC", SqlDbType.VarChar)
            pParms(2).Value = Dr("Loc").ToString
            cmd.Parameters.Add(pParms(2))
            pParms(3) = New SqlClient.SqlParameter("@TLOC", SqlDbType.VarChar)
            pParms(3).Value = Dr("Area").ToString
            cmd.Parameters.Add(pParms(3))
            pParms(4) = New SqlClient.SqlParameter("@FTI_1NORMAL", SqlDbType.Decimal)
            pParms(4).Value = Val(Dr("1Normal").ToString)
            cmd.Parameters.Add(pParms(4))
            pParms(5) = New SqlClient.SqlParameter("@FTI_1ONEWAY", SqlDbType.Decimal)
            pParms(5).Value = Val(Dr("1OneWay").ToString)
            cmd.Parameters.Add(pParms(5))
            pParms(6) = New SqlClient.SqlParameter("@FTI_2NORMAL", SqlDbType.Decimal)
            pParms(6).Value = Val(Dr("2Normal").ToString)
            cmd.Parameters.Add(pParms(6))
            pParms(7) = New SqlClient.SqlParameter("@FTI_2ONEWAY", SqlDbType.Decimal)
            pParms(7).Value = Val(Dr("2OneWay").ToString)
            cmd.Parameters.Add(pParms(7))
            pParms(8) = New SqlClient.SqlParameter("@FTI_3NORMAL", SqlDbType.Decimal)
            pParms(8).Value = Val(Dr("3Normal").ToString)
            cmd.Parameters.Add(pParms(8))
            pParms(9) = New SqlClient.SqlParameter("@FTI_3ONEWAY", SqlDbType.Decimal)
            pParms(9).Value = Val(Dr("3OneWay").ToString)
            cmd.Parameters.Add(pParms(9))
            pParms(10) = New SqlClient.SqlParameter("@FTI_USER", SqlDbType.VarChar)
            pParms(10).Value = Session("sUsr_name")
            cmd.Parameters.Add(pParms(10))
            pParms(11) = New SqlClient.SqlParameter("@FTI_BATCHNO", SqlDbType.BigInt)
            pParms(11).Value = FTI_BATCHNO
            cmd.Parameters.Add(pParms(11))
            pParms(12) = New SqlClient.SqlParameter("@FTI_ACD_ID", SqlDbType.Int)
            pParms(12).Value = Me.ddlACDYear.SelectedValue
            cmd.Parameters.Add(pParms(12))

            Dim retval As String
            cmd.ExecuteNonQuery()
            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "FIX.ValidateAndImportTrRateExceldata", pParms)
            retval = pParms(1).Value
            If retval <> 0 Then
                If Me.gvTrRateImport.Rows.Count > i Then
                    Me.gvTrRateImport.Rows(i - 1).BackColor = Drawing.Color.Red
                End If
                ErrorLog = ErrorLog & "@" & retval & "#" & Convert.ToString(Dr("SlNo")) ' keeping errono and line in a string variable for further use
            End If
            i += 1
        Next

        If ErrorLog <> "" Then
            Me.btntrProceed.Visible = False
            Me.btnTrCommit.Visible = False
            Me.btnImpTrRate.Visible = True
            Me.lblError3.Text = ""
            ShowErrorLogTr(ErrorLog)
            stTrans.Rollback()
            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "delete from [DataImport].[FEE_TRANSPORTRATE_IMPORT] where FTI_BATCHNO='" & FTI_BATCHNO & "'")
        Else
            stTrans.Commit()
            Me.btntrProceed.Visible = True
            Me.btnImpTrRate.Visible = False
        End If
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
    End Sub
    Private Sub ShowErrorLogTr(ByVal Err As String)
        Dim msg As String = ""
        Dim ReturnVal() As String = Err.Split("@")
        For i As Integer = 0 To ReturnVal.Length - 1
            If ReturnVal(i).Trim <> "" Then
                Dim aa() As String = ReturnVal(i).Split("#")
                'Me.lblError3.Text = Me.lblError3.Text & "Serial No(" & aa(1) & ") : "
                'Me.lblError3.Text = Me.lblError3.Text & IIf(aa(0) = "100", "Business Unit not found,", IIf(aa(0) = "101", "From Location not found,", IIf(aa(0) = "102", "To Location not found,", IIf(aa(0) = "103", "incorrect 1Normal amount,", ""))))
                'Me.lblError3.Text = Me.lblError3.Text & IIf(aa(0) = "104", "incorrect 1ONEWAY amount,", IIf(aa(0) = "105", "incorrect 2Normal amount,", IIf(aa(0) = "106", "incorrect 2ONEWAY amount,", IIf(aa(0) = "107", "incorrect 3Normal amount,", ""))))
                'Me.lblError3.Text = Me.lblError3.Text & IIf(aa(0) = "108", "incorrect 3ONEWAY amount,", IIf(aa(0) = "109", "Unable to Save,", IIf(aa(0) = "110", "Sublocation not found,", "")))
                msg += "Serial No(" & aa(1) & ") : "
                msg += IIf(aa(0) = "100", "Business Unit not found,", IIf(aa(0) = "101", "From Location not found,", IIf(aa(0) = "102", "To Location not found,", IIf(aa(0) = "103", "incorrect 1Normal amount,", ""))))
                msg += IIf(aa(0) = "104", "incorrect 1ONEWAY amount,", IIf(aa(0) = "105", "incorrect 2Normal amount,", IIf(aa(0) = "106", "incorrect 2ONEWAY amount,", IIf(aa(0) = "107", "incorrect 3Normal amount,", ""))))
                msg += IIf(aa(0) = "108", "incorrect 3ONEWAY amount,", IIf(aa(0) = "109", "Unable to Save,", IIf(aa(0) = "110", "Sublocation not found,", "")))
                usrMessageBar.ShowNotification(msg, UserControls_usrMessageBar.WarningType.Danger)
            End If
        Next
    End Sub

    Protected Sub btntrProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ImportTrRates(False)
    End Sub
    Private Sub ImportTrRates(ByVal Commit As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString) '
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(3) As SqlParameter
        Dim sqlParam2(1) As SqlParameter

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = ""
            sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retval, SqlDbType.VarChar, True, 100)
            sqlParam(1) = Mainclass.CreateSqlParameter("@FTI_BATCHNO", ViewState("FTI_BATCHNO"), SqlDbType.BigInt)
            sqlParam(2) = Mainclass.CreateSqlParameter("@FTI_ACD_ID", Me.ddlACDYear.SelectedValue, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@OutputXML", "", SqlDbType.Xml, True)

            Mainclass.ExecuteParamQRY(objConn, stTrans, "FIX.IMPORT_TRANSPORT_RATES", sqlParam)
            retval = sqlParam(0).Value
            'Me.lblError.Text += IIf(retval <> "0", retval, "")
            usrMessageBar.ShowNotification(IIf(retval <> "0", retval, ""), UserControls_usrMessageBar.WarningType.Danger)

            If Not retval Is Nothing And retval = "0" Then
                If Not sqlParam(3).Value Is DBNull.Value Then
                    Dim readXML As StringReader = New StringReader(HttpUtility.HtmlDecode(sqlParam(3).Value))
                    dsSummary.ReadXml(readXML)
                End If
                If dsSummary.Tables(0).Rows.Count > 0 Then ' AndAlso (Val(dsSummary.Tables(0).Rows(0)(0)) = Val(dsSummary.Tables(0).Rows(0)(2)) And Val(dsSummary.Tables(0).Rows(0)(1)) = Val(dsSummary.Tables(0).Rows(0)(3))) Then
                    Me.btnTrCommit.Visible = True
                    Me.btntrProceed.Visible = False
                    Me.btnImpTrRate.Visible = False
                    Me.gvTrRateImportSummary.DataSource = dsSummary.Tables(0)
                    Me.gvTrRateImportSummary.DataBind()
                Else
                    Me.btntrProceed.Visible = True
                    Me.btnImpTrRate.Visible = True
                    Me.btnTrCommit.Visible = False
                End If
            ElseIf retval = "222" Then
                Commit = False
                'Me.lblError3.Text = "Totals not tallying, unable to save"
                usrMessageBar.ShowNotification("Totals not tallying, unable to save", UserControls_usrMessageBar.WarningType.Danger)
            End If

            If Commit = True Then
                'Me.lblError.Text = "Data Saved successfully"
                usrMessageBar.ShowNotification("Data Saved successfully", UserControls_usrMessageBar.WarningType.Success)
                stTrans.Commit()
                Me.btnTrCommit.Visible = False
            Else
                'Me.lblError3.Text = IIf(Me.lblError3.Text = "", "Click the Commit button to commit the transaction", Me.lblError3.Text)
                usrMessageBar.ShowNotification(IIf(Me.lblError3.Text = "", "Click the Commit button to commit the transaction", Me.lblError3.Text), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "delete from [DataImport].[FEE_TRANSPORTRATE_IMPORT] where FTI_BATCHNO='" & ViewState("FTI_BATCHNO") & "'")
            ViewState("FTI_BATCHNO") = ""
            VSgvTrRateImport.Clear()
            BindTrRateExcel()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub btnTrCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ImportTrRates(True)
    End Sub

    Protected Sub btnCancelTrImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTrImport.Click
        Me.lblError3.Text = ""
        Me.lblError.Text = ""
        Me.lblXLOneway.Text = ""
        Me.lblXLNormal.Text = ""
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "delete from [DataImport].[FEE_TRANSPORTRATE_IMPORT] where FTI_BATCHNO='" & ViewState("FTI_BATCHNO") & "'")
        ViewState("FTI_BATCHNO") = ""
        VSgvTrRateImport.Clear()
        BindTrRateExcel()
        Me.btntrProceed.Visible = False
        Me.btnTrCommit.Visible = False
        Me.btnImpTrRate.Visible = True
    End Sub
    'Protected Sub lnkTrRateFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTrRateFormat.Click
    '    Try
    '        Dim filePath As String = "~\Template\TransportRateFormat.xls"lnkTrRateFormat
    '        Response.Redirect(filePath)
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.Message
    '    End Try

    'End Sub

    '-------------------------------Fee DayEnd---------------------------
#Region "FeeDayend"
    Sub LoadFeeGrid()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim qry As String = "FEES.GetFeePreDayend"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, qry)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("gvFeeDE") = ds
            FeeGridBind()
            Me.btnDayend.Enabled = True
        Else
            ViewState("gvFeeDE") = Nothing
            FeeGridBind()
            Me.btnDayend.Enabled = False
        End If

    End Sub
    Sub FeeGridBind()
        gvFeeDE.DataSource = ViewState("gvFeeDE")
        gvFeeDE.DataBind()
    End Sub
    Protected Sub btnDayend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDayend.Click
        Dim values As String = String.Empty
        Me.lblError.Text = ""
        For Each gvr As GridViewRow In gvFeeDE.Rows
            Dim chk As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            Me.gvFeeDE.SelectedIndex = gvr.RowIndex
            If chk.Checked Then
                If values = String.Empty Then
                    values = Me.gvFeeDE.SelectedDataKey.Item(0).ToString '+ "_" + gvr.Cells(2).Text
                Else
                    values = values + "|" + Me.gvFeeDE.SelectedDataKey.Item(0).ToString '+ "_" + gvr.Cells(2).Text
                End If
            End If
        Next
        Dim iReturnvalue As Integer
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand("FEES.DoManualFeeDayEnd", objConn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.VarChar)
        sqlpBSU_ID.Value = values
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUSR_ID As New SqlParameter("@MODI_USER", SqlDbType.VarChar)
        sqlpUSR_ID.Value = Session("sUsr_name")
        cmd.Parameters.Add(sqlpUSR_ID)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value

        If iReturnvalue = 0 Then
            'stTrans.Commit()
            'lblError.Text = "Day end executed successfully for the selected business units"
            usrMessageBar.ShowNotification("Day end executed successfully for the selected business units", UserControls_usrMessageBar.WarningType.Success)
            LoadFeeGrid()
            clearcheckboxes()
        Else
            'stTrans.Rollback()
            'lblError.Text = getErrorMessage(iReturnvalue)
            usrMessageBar.ShowNotification(getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Sub clearcheckboxes()
        For Each gvr As GridViewRow In gvFeeDE.Rows
            Dim chk As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            chk.Checked = False
        Next
    End Sub
#End Region
    '------------------------------Transport Fee Dayend------------------
#Region "Transport Fee Dayend"
    Sub LoadTransportFeeGrid()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Me.lblErrorTr.Text = ""
        Dim qry As String = "EXEC FEES.TransportFeePreDayend"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, qry)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("gvTrFeeDE") = ds
            TrFeeGridBind()
            Me.btnTptDayEnd.Enabled = True
        Else
            ViewState("gvTrFeeDE") = Nothing
            TrFeeGridBind()
            'Me.lblErrorTr.Text = "No Pending Dayends"
            Me.btnTptDayEnd.Enabled = False
        End If

    End Sub
    Sub TrFeeGridBind()
        gvTrFeeDE.DataSource = ViewState("gvTrFeeDE")
        gvTrFeeDE.DataBind()
    End Sub
    Protected Sub btnTptDayEnd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTptDayEnd.Click
        Dim values As String = String.Empty
        Me.lblError.Text = ""
        For Each gvr As GridViewRow In gvTrFeeDE.Rows
            Dim chk As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            Me.gvTrFeeDE.SelectedIndex = gvr.RowIndex
            If chk.Checked Then
                If values = String.Empty Then
                    values = Me.gvTrFeeDE.SelectedDataKey.Item(0).ToString '+ "_" + gvr.Cells(2).Text
                Else
                    values = values + "|" + Me.gvTrFeeDE.SelectedDataKey.Item(0).ToString '+ "_" + gvr.Cells(2).Text
                End If
            End If
        Next
        Dim iReturnvalue As Integer
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        'Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand("FEES.DoManualTransportFeeDayEnd", objConn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        Dim sqlpPRO_BSU_ID As New SqlParameter("@PRO_BSU_ID", SqlDbType.VarChar)
        sqlpPRO_BSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpPRO_BSU_ID)

        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.VarChar)
        sqlpBSU_ID.Value = values
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUSR_ID As New SqlParameter("@MODI_USER", SqlDbType.VarChar)
        sqlpUSR_ID.Value = Session("sUsr_name")
        cmd.Parameters.Add(sqlpUSR_ID)

        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retValParam.Value

        If iReturnvalue = 0 Then
            'stTrans.Commit()
            'lblError.Text = "Day end executed successfully for the selected business units"
            usrMessageBar.ShowNotification("Day end executed successfully for the selected business units", UserControls_usrMessageBar.WarningType.Success)
            cleargvTrFeeDEcheckboxes()
            LoadTransportFeeGrid()
        Else
            'stTrans.Rollback()
            'lblError.Text = getErrorMessage(iReturnvalue)
            usrMessageBar.ShowNotification(getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Sub cleargvTrFeeDEcheckboxes()
        For Each gvr As GridViewRow In gvTrFeeDE.Rows
            Dim chk As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            chk.Checked = False
        Next
    End Sub

#End Region

    '------------------Reports-------------------------
#Region "Reports"
    Sub FillReportType()
        Dim sql As String
        Dim ds As New DataSet
        sql = "Select * from FEES.GetDynamicReportLists"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, sql)
        radReportType.DataSource = ds.Tables(0)
        radReportType.DataTextField = "Title"
        radReportType.DataValueField = "ID"
        radReportType.DataBind()

    End Sub
    Private Sub FillReportData(ByVal bind As Boolean)
        Try
            Dim ConnectionString As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim strSQL As String
            Dim ParamList As DataTable, ParamRow As DataRow
            If SPName = "" Then Exit Sub
            strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType from information_schema.parameters where PARAMETER_MODE='IN' AND specific_schema + '.' + specific_name = '" & SPName & "'"
            ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
            Dim cmd As New SqlCommand
            cmd.Dispose()
            cmd = New SqlCommand(SPName, ConnectionManger.GetOASIS_FEESConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim ParamName As String
            If ParamList IsNot Nothing Then
                For Each ParamRow In ParamList.Rows
                    ParamName = UCase(ParamRow("parameter_name"))
                    Select Case ParamName
                        Case "@BSU_ID", "@BSU_IDS"
                            If trBSUIDs.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, Me.UsrBSUnits1.GetSelectedNode("|"), SqlDbType.VarChar)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@STU_ID"
                            If trStudents.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                'param0 = Mainclass.CreateSqlParameter(ParamName, UsrStudentForReport.STUDENT_ID, SqlDbType.BigInt)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@USR_ID"
                            If trUserID.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, txtUserID.Text, SqlDbType.VarChar)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@ASONDT"
                            If trAsOnDate.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, Me.txtDate.Text, SqlDbType.DateTime)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@FROMDT"
                            If trDateRange.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, Me.txtFromDate.Text, SqlDbType.DateTime)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@TODT"
                            If trDateRange.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, Me.txtToDate.Text, SqlDbType.DateTime)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@RESULTTYPE"
                            If trRESULTTYPE.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, Me.rblFilter.SelectedValue, SqlDbType.TinyInt)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@SEARCHSTR"
                            If trSearchStr.Style("display") = "block" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, txtSearchStr.Text, SqlDbType.VarChar)
                                cmd.Parameters.Add(param0)
                            End If
                    End Select
                Next
            End If
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            Dim i As Int16
            If ds.Tables.Count > 0 Then
                RadGridReport.DataSource = ds.Tables(0)
                If bind Then RadGridReport.DataBind()
                If GroupBy <> "" Then
                    Dim ge As New GridGroupByExpression
                    Dim gf As New GridGroupByField
                    gf.FieldName = GroupBy
                    ge.GroupByFields.Add(gf)
                    ge.SelectFields.Add(gf)
                    RadGridReport.MasterTableView.GroupByExpressions.Add(ge)
                    If bind Then RadGridReport.DataBind()
                    For i = 0 To RadGridReport.MasterTableView.AutoGeneratedColumns.Length - 1
                        If RadGridReport.MasterTableView.AutoGeneratedColumns(i).UniqueName = GroupBy Then
                            RadGridReport.MasterTableView.AutoGeneratedColumns(i).Visible = False
                        End If
                    Next
                Else
                    For i = 0 To RadGridReport.MasterTableView.GroupByExpressions.Count - 1
                        RadGridReport.MasterTableView.GroupByExpressions.RemoveAt(i)
                    Next
                End If
                '    
            End If
            Exit Sub
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        FillReportData(True)
    End Sub
    Private Sub HideFilterRows()
        trBSUIDs.Style.Add("display", "none")
        trAsOnDate.Style.Add("display", "none")
        trRESULTTYPE.Style.Add("display", "none")
        trDateRange.Style.Add("display", "none")
        trUserID.Style.Add("display", "none")
        trStudents.Style.Add("display", "none")
        trSearchStr.Style.Add("display", "none")
    End Sub

    Private Sub SetFilters()
        Try
            Dim ConnectionString As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim strSQL As String
            Dim ParamList As DataTable, ParamRow As DataRow
            HideFilterRows()
            If SPName = "" Then Exit Sub
            strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType from information_schema.parameters where PARAMETER_MODE='IN' AND specific_schema + '.' + specific_name = '" & SPName & "'"
            ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
            If ParamList IsNot Nothing Then
                For Each ParamRow In ParamList.Rows
                    Select Case UCase(ParamRow("parameter_name"))
                        Case "@BSU_ID", "@BSU_IDS"
                            trBSUIDs.Style.Add("display", "block")
                        Case "@USR_ID"
                            trUserID.Style.Add("display", "block")
                        Case "@STU_ID"
                            trStudents.Style.Add("display", "block")
                        Case "@ASONDT"
                            trAsOnDate.Style.Add("display", "block")
                        Case "@FROMDT", "@TODT"
                            trDateRange.Style.Add("display", "block")
                        Case "@RESULTTYPE"
                            trRESULTTYPE.Style.Add("display", "block")
                            rblFilter.Items.Clear()
                            If RESULTTYPE_ITEMS <> "" Then
                                Dim rItems As String()
                                Dim itm As String()
                                Dim str As String
                                rItems = RESULTTYPE_ITEMS.Split(",")
                                For Each str In RESULTTYPE_ITEMS.Split(",")
                                    itm = str.Split("|")
                                    If itm.Length > 1 Then
                                        Dim lstItem As New ListItem
                                        lstItem.Value = itm(0)
                                        lstItem.Text = itm(1)
                                        rblFilter.Items.Add(lstItem)
                                    End If
                                Next
                                If rblFilter.Items.Count > 0 Then rblFilter.Items(0).Selected = True
                            End If
                        Case "@SEARCHSTR"
                            trSearchStr.Style.Add("display", "block")
                    End Select
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub radReportType_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radReportType.SelectedIndexChanged
        Try
            Dim ReportType As String
            If radReportType.SelectedItem IsNot Nothing Then
                ReportType = radReportType.SelectedItem.Value
                If ReportType = "0" Then
                    SPName = ""
                    GroupBy = ""
                Else
                    Dim sql As String
                    Dim ds As New DataTable
                    sql = "Select * from FEES.GetDynamicReportLists where ID='" & ReportType & "'"
                    ds = Mainclass.getDataTable(sql, ConnectionManger.GetOASIS_FEESConnectionString)
                    If ds.Rows.Count > 0 Then
                        SPName = ds.Rows(0).Item("SPNAME").ToString
                        GroupBy = ds.Rows(0).Item("GroupBy").ToString
                        RESULTTYPE_ITEMS = ds.Rows(0).Item("ResultType").ToString
                    End If
                End If
                Dim i As Int16
                For i = 0 To RadGridReport.MasterTableView.GroupByExpressions.Count - 1
                    RadGridReport.MasterTableView.GroupByExpressions.RemoveAt(i)
                Next
                Me.RadGridReport.AllowPaging = False
                Me.RadGridReport.DataSource = ""
                Me.RadGridReport.DataBind()
                Me.RadGridReport.AllowPaging = True
                SetFilters()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub RadGridReport_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGridReport.NeedDataSource
        FillReportData(False)
    End Sub
    Protected Sub RadGridReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGridReport.ItemDataBound
        If TypeOf e.Item Is GridGroupHeaderItem Then
            Dim groupHeader As GridGroupHeaderItem = TryCast(e.Item, GridGroupHeaderItem)
            groupHeader.DataCell.Text = groupHeader.DataCell.Text.Split(":")(1).ToString()
        End If

    End Sub

    Protected Sub lnkExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportToExcel.Click
        ' FillReportData(False)
        RadGridReport.ExportSettings.ExportOnlyData = True
        RadGridReport.ExportSettings.IgnorePaging = True
        RadGridReport.ExportSettings.OpenInNewWindow = True
        RadGridReport.MasterTableView.ExportToExcel()
    End Sub

    Protected Sub lnkExportToPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportToPDF.Click
        'FillReportData(False)
        RadGridReport.ExportSettings.ExportOnlyData = True
        RadGridReport.ExportSettings.IgnorePaging = True
        RadGridReport.ExportSettings.OpenInNewWindow = True
        RadGridReport.MasterTableView.ExportToPdf()
    End Sub

#End Region

#Region "RevenueRecognition"
    Sub Fill_RR_BSU()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') order by BSU_NAME")
        DDL_RR_BSU.DataSource = ds.Tables(0)
        DDL_RR_BSU.DataBind()
        If DDL_RR_BSU.Items.Count > 0 Then
            DDL_RR_BSU.SelectedValue = Session("sBsuid")
        End If
    End Sub
    Protected Sub btnRRProceed_Click(sender As Object, e As EventArgs) Handles btnRRProceed.Click
        If ValidateRR() Then
            RegenerateRevenue(False)
        End If
    End Sub

    Protected Sub btnRRCancel_Click(sender As Object, e As EventArgs) Handles btnRRCancel.Click
        Me.btnRRCommit.Visible = False
        Me.btnRRProceed.Visible = True
        Me.monthpicker.Value = ""
        Me.txtBankCode.Text = ""
        Me.txtBankDescr.Text = ""
        Me.chkNewJV.Checked = False
        Me.txtJVNO.Text = ""
        Me.hfACT_CODE.Value = ""
        Me.hfJVNO.Value = ""
    End Sub
    Protected Sub btnRRCommit_Click(sender As Object, e As EventArgs) Handles btnRRCommit.Click
        If ValidateRR() Then
            RegenerateRevenue(True)
        End If
    End Sub

    Sub RegenerateRevenue(ByVal Commit As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(9) As SqlParameter

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim MonYear As String = Me.monthpicker.Value
            Dim Docdate As String = "01/" & Left(MonthName(MonYear.Split("/")(0)), 3) & "/" & MonYear.Split("/")(1)
            Dim retval As String
            Dim PROCESSSTATUS As String
            sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retval, SqlDbType.VarChar, True, 2000)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ACT_ID", Me.hfACT_CODE.Value, SqlDbType.VarChar, False, 20)
            sqlParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", Me.DDL_RR_BSU.SelectedValue, SqlDbType.VarChar, False, 20)
            sqlParam(3) = Mainclass.CreateSqlParameter("@ACD_ID", Me.DDL_RR_ACD.SelectedValue, SqlDbType.Int)
            sqlParam(4) = Mainclass.CreateSqlParameter("@DOCNO", Me.hfJVNO.Value, SqlDbType.VarChar, False, 20)
            sqlParam(5) = Mainclass.CreateSqlParameter("@FROMDT", Docdate, SqlDbType.DateTime)
            sqlParam(6) = Mainclass.CreateSqlParameter("@bCreateNewJV", Me.chkNewJV.Checked, SqlDbType.Bit)
            sqlParam(7) = Mainclass.CreateSqlParameter("@PROCESSSTATUS", PROCESSSTATUS, SqlDbType.VarChar, True, 2000)

            Mainclass.ExecuteParamQRY(objConn, stTrans, "FEES.REGENERATE_FEE_REVENUE", sqlParam)
            retval = sqlParam(0).Value
            PROCESSSTATUS = sqlParam(7).Value

            If PROCESSSTATUS = "SUCCESS" Then
                ShowSummary(stTrans, objConn, Docdate)
                'Me.lblErrorRR.Text = retval 'getErrorMessage(retval)
                usrMessageBar.ShowNotification(retval, UserControls_usrMessageBar.WarningType.Danger)
            Else
                Commit = False
                'Me.lblErrorRR.Text = retval 'getErrorMessage(retval)
                usrMessageBar.ShowNotification(retval, UserControls_usrMessageBar.WarningType.Danger)
            End If

            If Commit = True Then
                stTrans.Commit()
                'Me.lblErrorRR.Text = "Data Saved successfully"
                usrMessageBar.ShowNotification("Data Saved successfully", UserControls_usrMessageBar.WarningType.Success)
            Else
                stTrans.Rollback()
            End If
            If Commit = False And PROCESSSTATUS = "SUCCESS" Then
                Me.btnRRCommit.Visible = True
                Me.btnRRProceed.Visible = False
            Else
                Me.btnRRCommit.Visible = False
                Me.btnRRProceed.Visible = True
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblErrorRR.Text = getErrorMessage(ex.Message)
            usrMessageBar.ShowNotification(getErrorMessage(ex.Message), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Function ShowSummary(ByRef stTrans As SqlTransaction, ByRef objConn As SqlConnection, ByVal Docdate As String) As Boolean
        ShowSummary = True
        Dim sqlParam(3) As SqlParameter
        Dim dsSummary As New DataSet
        Dim Dad As New SqlDataAdapter
        Try
            Dim cmd As New SqlCommand
            cmd.Dispose()
            cmd = New SqlCommand("FEES.REGENERATE_REVENUE_SUMMARY", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            Dim retval As String = ""
            sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Me.DDL_RR_BSU.SelectedValue, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@ACT_ID", Me.hfACT_CODE.Value, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@FROMDT", Docdate, SqlDbType.DateTime)
            cmd.Parameters.Add(sqlParam(2))


            Dad.SelectCommand = cmd
            Dad.Fill(dsSummary)

            Me.RadGV.DataSource = dsSummary
            Me.RadGV.DataBind()
            'Mainclass.ExecuteParamQRY(objConn, stTrans, "IMPORT_FEE_ADJUSTMENTS", sqlParam)
            'dsSummary = SqlHelper.ExecuteDataset(stTrans, "", sqlParam)
        Catch
            ShowSummary = False
        End Try

    End Function
    Function ValidateRR() As Boolean
        Me.lblErrorRR.Text = ""
        ValidateRR = True
        If Me.txtBankCode.Text.Trim = "" Then
            'Me.lblErrorRR.Text = "Please select the Account"
            usrMessageBar.ShowNotification("Please select the Account", UserControls_usrMessageBar.WarningType.Danger)
            ValidateRR = False
        ElseIf Me.txtJVNO.Text.Trim = "" And Me.chkNewJV.Checked = False Then
            'Me.lblErrorRR.Text = "Please select an existing JV or Select New JV"
            usrMessageBar.ShowNotification("Please select an existing JV or Select New JV", UserControls_usrMessageBar.WarningType.Danger)
            ValidateRR = False
        ElseIf Me.monthpicker.Value.Trim = "" Then
            'Me.lblErrorRR.Text = "Please select the month"
            usrMessageBar.ShowNotification("Please select the month", UserControls_usrMessageBar.WarningType.Danger)
            ValidateRR = False
        End If
    End Function
    Protected Sub DDL_RR_BSU_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDL_RR_BSU.SelectedIndexChanged
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(DDL_RR_BSU.SelectedValue)
        DDL_RR_ACD.DataSource = dtACD
        DDL_RR_ACD.DataTextField = "ACY_DESCR"
        DDL_RR_ACD.DataValueField = "ACD_ID"
        DDL_RR_ACD.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                DDL_RR_ACD.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Protected Sub imgBank_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgBank.Click
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim MonYear As String = Me.monthpicker.Value
        Dim Docdate As String = "01/" & Left(MonthName(MonYear.Split("/")(0)), 3) & "/" & MonYear.Split("/")(1)
        Try
            ShowSummary(stTrans, objConn, Docdate)
        Catch ex As Exception
            stTrans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

#End Region

    Protected Sub btnEFR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEFR.Click
        EnableFeeRefund()
    End Sub

    Private Sub EnableFeeRefund()
        Dim pParms(4) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@EFR_BSU_ID", Session("sBsuid"), SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@EFR_STU_ID", UsrSelStudent1.STUDENT_ID, SqlDbType.VarChar)
        If rbLEnquiry.Checked = True Then
            pParms(3) = Mainclass.CreateSqlParameter("@EFR_STU_TYPE", "E", SqlDbType.VarChar)
        Else
            pParms(3) = Mainclass.CreateSqlParameter("@EFR_STU_TYPE", "S", SqlDbType.VarChar)
        End If
        pParms(4) = Mainclass.CreateSqlParameter("@EFR_LOGGEDUSER", Session("sUsr_name"), SqlDbType.VarChar)


        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "dbo.saveENABLEFEEREFUND", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                stTrans.Commit()
                'lblError.Text = "Data Saved Successfully !!!"
                usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try



    End Sub
    Protected Sub btnResettlement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResettlement.Click
        Resettlement()
    End Sub
    Private Sub Resettlement()
        Dim pParms(4) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@STU_ID", UsrSelStudent1.STUDENT_ID, SqlDbType.VarChar)
        If rbLEnquiry.Checked = True Then
            pParms(3) = Mainclass.CreateSqlParameter("@STU_TYPE", "E", SqlDbType.VarChar)
        Else
            pParms(3) = Mainclass.CreateSqlParameter("@STU_TYPE", "S", SqlDbType.VarChar)
        End If
        pParms(4) = Mainclass.CreateSqlParameter("@ASONDT", Now(), SqlDbType.DateTime)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "FEES.STUDENT_RESETTLE_FEE", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            Else
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                stTrans.Commit()
                'lblError.Text = "Data Saved Successfully !!!"
                usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try



    End Sub
    Private Function Reenrol_process(ByVal Type As String) As String
        Dim pParms(5) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID ", Session("sBsuid"), SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@STU_ID", UsrSelStudent1.STUDENT_ID, SqlDbType.VarChar)
        If rbLEnquiry.Checked = True Then
            pParms(3) = Mainclass.CreateSqlParameter("@STU_TYPE", "E", SqlDbType.VarChar)
        Else
            pParms(3) = Mainclass.CreateSqlParameter("@STU_TYPE", "S", SqlDbType.VarChar)
        End If
        pParms(4) = Mainclass.CreateSqlParameter("@TYPE", Type, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@RETURN_VAL", 0, SqlDbType.Int, True)

        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "STUDENT_REENROLLMENT_PROCESSES", pParms)
            If RetVal = "-1" Then
                'lblError.Text = "Unexpected Error !!!"
                usrMessageBar.ShowNotification("Unexpected Error !!!", UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Return ""
            Else
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ViewState("EntryId"), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                stTrans.Commit()
                'lblError.Text = "Data Saved Successfully !!!"
                usrMessageBar.ShowNotification("Data Saved Successfully !!!", UserControls_usrMessageBar.WarningType.Success)
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function
    Protected Sub btnEnrollStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnrollStudent.Click
        Reenrol_process("E")
    End Sub

    Protected Sub btnResetEnrollment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetEnrollment.Click
        Reenrol_process("R")
    End Sub

    Protected Sub btnNotReenrolling_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNotReenrolling.Click
        Reenrol_process("N")
    End Sub

    Protected Sub rbLEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbLEnrollment.CheckedChanged, rbLEnquiry.CheckedChanged
        UsrSelStudent1.IsStudent = rbLEnrollment.Checked
        'UsrStudentForReport.IsStudent = rbLEnrollment.Checked
    End Sub

    Public Sub BindSourceScreen()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "SELECT DISTINCT A.BFS_SOURCE FROM OASIS_FEES.FEES.BSU_FEETYPES_SELECTION_ACCESS A")
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSource_.DataValueField = "BFS_SOURCE"
                ddlSource_.DataTextField = "BFS_SOURCE"
                ddlSource_.DataSource = ds.Tables(0)
                ddlSource_.DataBind()
            End If
        Catch ex As Exception
            'lblError.Text = "Error occured getting source screen(s)"
            usrMessageBar.ShowNotification("Error occured getting source screen(s)", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub BindFeeTypes()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "SELECT F.FEE_ID, F.FEE_DESCR FROM FEES.FEES_M F ORDER BY F.FEE_DESCR")
            If ds.Tables(0).Rows.Count > 0 Then
                ddlFeeType_.DataValueField = "Fee_id"
                ddlFeeType_.DataTextField = "Fee_Descr"
                ddlFeeType_.DataSource = ds.Tables(0)
                ddlFeeType_.DataBind()
                If ddlFeeType_.Items.Count > 0 Then
                    ddlFeeType_.SelectedIndex = ddlFeeType_.Items.Count - 1
                    ddlFeeType_.SelectedIndex = 0
                End If

            End If
        Catch ex As Exception
            'lblError.Text = "Error occured getting fee type(s)"
            usrMessageBar.ShowNotification("Error occured getting fee type(s)", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Public Sub BindFeeTypeAccessBsu()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
    '        Dim ds As DataSet
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "SELECT BSU_ID, BSU_NAME FROM dbo.BUSINESSUNIT_M ORDER BY BSU_NAME")
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            chkBusUnit.DataValueField = "bsu_id"
    '            chkBusUnit.DataTextField = "bsu_name"
    '            chkBusUnit.DataSource = ds.Tables(0)
    '            chkBusUnit.DataBind()
    '        End If
    '        If ddlSource_.Items.Count > 0 And ddlFeeType_.Items.Count > 0 Then GetAccessSettings_Tree()
    '    Catch ex As Exception
    '        lblError.Text = "Error occured getting business unit(s) list"
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub

    'Protected Sub chkSelectAllBsu_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAllBsu.CheckedChanged
    '    For i = 0 To Me.chkBusUnit.Items.Count - 1
    '        Me.chkBusUnit.Items(i).Selected = Me.chkSelectAllBsu.Checked
    '    Next
    'End Sub

    Protected Sub btnSaveAccessSettings_Click(sender As Object, e As EventArgs) Handles btnSaveAccessSettings.Click
        Me.SaveAccessSettings_Tree()
    End Sub

    'Public Sub SaveAccessSettings()
    '    Dim tr As SqlTransaction
    '    Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnection.ConnectionString
    '    Dim con As New SqlConnection(str_conn)
    '    Try
    '        Dim params(6) As SqlParameter
    '        con.Open()
    '        tr = con.BeginTransaction
    '        For i As Integer = 0 To chkBusUnit.Items.Count - 1
    '            If chkBusUnit.Items(i).Selected = True Then
    '                params(0) = New SqlParameter("@BSU_ID", chkBusUnit.Items(i).Value)
    '                params(1) = New SqlParameter("@SOURCE", ddlSource_.Text)
    '                params(2) = New SqlParameter("@FEE_ID", ddlFeeType_.SelectedItem.Value)
    '                params(3) = New SqlParameter("@FROMDATE", Format(Date.Now, "yyyy-MM-dd"))
    '                params(4) = New SqlParameter("@TODATE", DBNull.Value)
    '                params(5) = New SqlParameter("@LOGDATE", Format(Date.Now, "yyyy-MM-dd"))
    '                SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "[FEES].[ADD_UPDATE_FEETYPES_SELECTION_ACCESS]", params)
    '            End If
    '        Next
    '        tr.Commit()
    '    Catch ex As Exception
    '        tr.Rollback()
    '        lblError.Text = "Error occured saving access settings"
    '        UtilityObj.Errorlog(ex.Message)
    '    Finally
    '        If Not con.State = ConnectionState.Closed Then con.Close()
    '    End Try
    'End Sub

    Public Sub SaveAccessSettings_Tree()
        Dim tr As SqlTransaction = Nothing
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnection.ConnectionString
        Dim con As New SqlConnection(str_conn)
        Try
            Dim tmpBsu() As String = tvBsu.GetSelectedNode.Split("||")
            Dim Bsus As String = Replace(tvBsu.GetSelectedNode, "||", "|")
            If Bsus = Nothing Then Bsus = ""
            If Bsus.EndsWith("|") Then Bsus = Bsus.TrimEnd("|")
            Dim params(7) As SqlParameter
            con.Open()
            tr = con.BeginTransaction
            'For i As Integer = 0 To tmpBsu.Length - 1
            '    If tmpBsu(i) <> "XXXXXX" And tmpBsu(i) <> Nothing Then
            '        params(0) = New SqlParameter("@BSU_ID", tmpBsu(i).ToString)
            '        params(1) = New SqlParameter("@SOURCE", ddlSource_.Text)
            '        params(2) = New SqlParameter("@FEE_ID", ddlFeeType_.SelectedItem.Value)
            '        params(3) = New SqlParameter("@FROMDATE", Format(Date.Now, "yyyy-MM-dd"))
            '        params(4) = New SqlParameter("@TODATE", DBNull.Value)
            '        params(5) = New SqlParameter("@LOGDATE", Format(Date.Now, "yyyy-MM-dd"))
            '        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "[FEES].[ADD_UPDATE_FEETYPES_SELECTION_ACCESS]", params)
            '    End If
            'Next
            params(0) = New SqlParameter("@BSU_IDs", Bsus)
            params(1) = New SqlParameter("@SOURCE", ddlSource_.Text)
            params(2) = New SqlParameter("@FEE_ID", ddlFeeType_.SelectedItem.Value)
            params(3) = New SqlParameter("@FROMDATE", Format(Date.Now, "yyyy-MM-dd"))
            params(4) = New SqlParameter("@TODATE", DBNull.Value)
            params(5) = New SqlParameter("@LOGDATE", Format(Date.Now, "yyyy-MM-dd"))
            params(6) = New SqlParameter("@LOG_USER_NAME", Session("sUsr_name"))
            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "[FEES].[ADD_UPDATE_FEETYPES_SELECTION_ACCESS]", params)
            tr.Commit()
            'lblError.Text = "Access settings saved successfully"
            usrMessageBar.ShowNotification("Access settings saved successfully", UserControls_usrMessageBar.WarningType.Success)
        Catch ex As Exception
            If Not tr Is Nothing Then tr.Rollback()
            'lblError.Text = "Error occured saving access settings"
            usrMessageBar.ShowNotification("Error occured saving access settings", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message)
        Finally
            If Not con.State = ConnectionState.Closed Then con.Close()
        End Try
    End Sub

    'Public Sub GetAccessSettings()

    '    Dim tr As SqlTransaction
    '    Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnection.ConnectionString
    '    Dim con As New SqlConnection(str_conn)
    '    Dim ds As DataSet
    '    Try
    '        Dim params(1) As SqlParameter
    '        params(0) = New SqlParameter("@SOURCE", ddlSource_.Text)
    '        params(1) = New SqlParameter("@FEE_ID", ddlFeeType_.SelectedItem.Value)
    '        con.Open()
    '        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[FEES].[GET_FEETYPES_SELECTION_ACCESS]", params)
    '        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
    '            Dim tmpRows() As DataRow = Nothing
    '            For i As Integer = 0 To Me.chkBusUnit.Items.Count - 1
    '                tmpRows = ds.Tables(0).Select("Bsu_id='" & chkBusUnit.Items(i).Value & "'")
    '                If tmpRows.Length > 0 Then
    '                    chkBusUnit.Items(i).Selected = True
    '                Else
    '                    chkBusUnit.Items(i).Selected = False
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = "Error occured getting allowed business unit(s) for the selected source and fee type"
    '        UtilityObj.Errorlog(ex.Message)
    '    Finally
    '        If Not con.State = ConnectionState.Closed Then con.Close()
    '    End Try
    'End Sub

    Public Sub GetAccessSettings_Tree()

        'Dim tr As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnection.ConnectionString
        Dim con As New SqlConnection(str_conn)
        Dim ds As DataSet
        Try
            Dim tmpBsu As String = ""
            Dim params(1) As SqlParameter
            params(0) = New SqlParameter("@SOURCE", ddlSource_.Text)
            params(1) = New SqlParameter("@FEE_ID", ddlFeeType_.SelectedItem.Value)
            con.Open()
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[FEES].[GET_FEETYPES_SELECTION_ACCESS]", params)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    tmpBsu &= row.Item("bsu_id") & "||"
                Next
                If tmpBsu.EndsWith("||") Then
                    tmpBsu = tmpBsu.TrimEnd("||")
                End If
                Dim tv As TreeView = tvBsu.FindControl("tvBusinessunit")
                If Not tv Is Nothing Then
                    For i As Integer = tv.CheckedNodes.Count - 1 To 0 Step -1
                        tv.CheckedNodes(i).Checked = False
                    Next
                End If
                tvBsu.SetSelectedNodes(tmpBsu)
            Else
                Dim tv As TreeView = tvBsu.FindControl("tvBusinessunit")
                If Not tv Is Nothing Then
                    For i As Integer = tv.CheckedNodes.Count - 1 To 0 Step -1
                        tv.CheckedNodes(i).Checked = False
                    Next
                End If
            End If
        Catch ex As Exception
            'lblError.Text = "Error occured getting allowed business unit(s) for the selected source and fee type"
            usrMessageBar.ShowNotification("Error occured getting allowed business unit(s) for the selected source and fee type", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message)
        Finally
            If Not con.State = ConnectionState.Closed Then con.Close()
        End Try
    End Sub

    Protected Sub ddlSource__SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSource_.SelectedIndexChanged
        GetAccessSettings_Tree()
        Me.TabContainer1.ActiveTabIndex = 8
    End Sub

    Protected Sub ddlFeeType__SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFeeType_.SelectedIndexChanged
        GetAccessSettings_Tree()
        Me.TabContainer1.ActiveTabIndex = 8
    End Sub

    
End Class
