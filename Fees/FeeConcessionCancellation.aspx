﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeConcessionCancellation.aspx.vb" Inherits="Fees_FeeConcessionCancellation" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link href="../Scripts/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />--%>
    <style type="text/css">
        table.c7 {
            border: solid 2px !important;
            padding: 4px;
        }

            table.c7 td {
                border: 1px solid !important;
                text-align: center;
            }

            table.c7 th {
                border: 1px solid !important;
                text-align: center;
            }
    </style>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Cancel Fee Concession
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <div style="width: 100%;">
                    <pre id="preError" class="invisible" runat="server"></pre>
                </div>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                        <td align="left" wdith="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Student</span></td>
                        <td align="left" width="30%">
                            <asp:Panel ID="pnlStudents" runat="server">
                                <asp:TextBox ID="txtStdNo" runat="server"></asp:TextBox>
                                <asp:LinkButton ID="lbtnClearStudent" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                    TargetControlID="txtStdNo"
                                    WatermarkText="Type in Student Id or Name"
                                    WatermarkCssClass="watermarked" />
                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server"
                                    TargetControlID="lbtnClearStudent"
                                    ConfirmText="Are you sure you want to remove the Student selection?" />
                                <asp:ImageButton
                                    ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetStudent(); return false;" />
                                <asp:TextBox ID="txtStud_Name" runat="server" Visible="false"></asp:TextBox>
                                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                            </asp:Panel>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table width="100%" class="table table-bordered table-row">
                                <tr align="center">
                                    <th>Concession No</th>
                                    <th>Period</th>
                                    <th>Type</th>
                                    <th>Ref. Details</th>
                                    <th>Narration</th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlConcession" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlConcession_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td>
                                        <div style="width=100%">
                                            <div style="width: 40%; float: left;">
                                                <asp:TextBox ID="txtFromDT" runat="server" AutoPostBack="True"></asp:TextBox>
                                                <asp:ImageButton ID="ImagefromDate" runat="server" AlternateText="Click to show calendar"
                                                    ImageUrl="~/Images/calendar.gif" />

                                                <ajaxToolkit:CalendarExtender ID="Calendarextender1" runat="server" Format="dd/MMM/yyyy"
                                                    PopupButtonID="imgToDT" TargetControlID="txtToDT">
                                                </ajaxToolkit:CalendarExtender>
                                                <ajaxToolkit:CalendarExtender ID="Calendarextender3" runat="server" Format="dd/MMM/yyyy"
                                                    PopupButtonID="txtToDT" TargetControlID="txtToDT">
                                                </ajaxToolkit:CalendarExtender>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFromDT"
                                                    ErrorMessage="From Date Reqired" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtFromDT"
                                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                    ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator>
                                            </div>
                                            <div style="width: 10%; float: left; line-height: 45px;"><span class="field-label">To</span></div>
                                            <div style="width: 40%; float: left;">
                                                <asp:TextBox ID="txtToDT" runat="server" AutoPostBack="True"></asp:TextBox>
                                                <asp:ImageButton ID="imgToDT" runat="server" AlternateText="Click to show calendar"
                                                    ImageUrl="~/Images/calendar.gif" />

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtToDT"
                                                    ErrorMessage="To Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDT"
                                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                    ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator>
                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtToDT"
                                                    ControlToValidate="txtFromDT" ErrorMessage="From Date should be less than To Date"
                                                    Operator="LessThan" Type="Date" ValidationGroup="VALMAIN" Enabled="False">*</asp:CompareValidator>
                                                <ajaxToolkit:CalendarExtender ID="Calendarextender4" runat="server" Format="dd/MMM/yyyy"
                                                    PopupButtonID="txtFromDT" TargetControlID="txtFromDT">
                                                </ajaxToolkit:CalendarExtender>
                                                <ajaxToolkit:CalendarExtender ID="Calendarextender2" runat="server" Format="dd/MMM/yyyy"
                                                    PopupButtonID="ImagefromDate" TargetControlID="txtFromDT">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:HiddenField ID="h_Tdate" runat="server" />
                                                <asp:HiddenField ID="h_Fdate" runat="server" />
                                            </div>
                                        </div>


                                    </td>
                                    <td>
                                        <asp:Label ID="txtConcession_Head" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="txtRefHEAD" runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="txtRemarks" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4" valign="middle">Concession Details</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table width="100%" class="table table-bordered table-row">
                                <tr class="subHeader_big" style="height: 30px;">
                                    <th>Fee Type</th>
                                    <th>Charge Type</th>
                                    <th>Tax Code</th>
                                    <th>Tax Type</th>
                                    <th style="display: none;">Total Fee</th>
                                    <th>Actual Amount/Percentage</th>
                                    <th>Cancel Amount/Percentage</th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlFeeType" runat="server" SkinID="DropDownListNormal" Enabled="False">
                                        </asp:DropDownList></td>
                                    <td>
                                        <asp:RadioButton ID="radAmount" runat="server" Text="Amount" ValidationGroup="AMTTYPE" GroupName="ChargeType" Checked="true" Enabled="False" />
                                        <asp:RadioButton ID="radPercentage" runat="server" Text="Percentage" ValidationGroup="AMTTYPE" GroupName="ChargeType" Enabled="False" /></td>
                                    <td>
                                        <asp:DropDownList ID="ddlTAX" runat="server" SkinID="DropDownListNormal" TabIndex="9" AutoPostBack="True" Enabled="False">
                                        </asp:DropDownList></td>
                                    <td>
                                        <asp:RadioButtonList ID="rblTaxCalculation" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                            <asp:ListItem Selected="True" Value="E"><span class="field-label">Excl.Tax</span></asp:ListItem>
                                            <asp:ListItem Value="I"><span class="field-label">Incl.Tax</span></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="center" style="display: none;">
                                        <asp:Label ID="txtTotalFee" runat="server" Text="0"></asp:Label></td>
                                    <td align="center">
                                        <asp:Label ID="txtAmount" runat="server" Text="0"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtPercAmountCancel" runat="server" Width="122px" AutoCompleteType="disabled" Text="0" SkinID="TextBoxNormal" Style="text-align: right;">
                                        </asp:TextBox>&nbsp;<asp:LinkButton ID="lnkFill_Cancel" runat="server" OnClick="lnkFill_Cancel_Click">(Update)</asp:LinkButton>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmountToPay" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtPercAmountCancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <pre id="lblAlert" class="alert alert-info" runat="server" style="width: 50% !important;"></pre>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4" valign="middle">Monthly/Termly Split up(Cancellation Details)</td>
                    </tr>
                    <tr>
                        <td align="center" width="100%" colspan="4">

                            <asp:GridView ID="gvMonthly" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" DataKeyNames="FMD_ID,FMD_ORG_AMOUNT,FMD_REF_ID">
                                <Columns>
                                    <asp:BoundField DataField="DESCR" HeaderText="Term/Month" ReadOnly="True"></asp:BoundField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="tbtCancelAmountHdr" runat="server" Text="Amount To Cancel </ br> [Set 0.00 against Month/Term to skip concession cancellation]"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="Disabled" Text='<%# Bind("FMD_AMOUNT", "{0:0.00}")%>' Width="102px" onfocus="this.select();" onblur="return CheckAmount(this);"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtAmount" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tax Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxAmount" runat="server" Text='<%# Bind("FMD_TAX_AMOUNT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetAmount" runat="server" Text='<%# Bind("FMD_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Charge Dt.">
                                        <ItemTemplate>
                                            <asp:Label ID="txtChargeDate" runat="server" Text='<%# Bind("FMD_DATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FMD_ORG_AMOUNT" HeaderText="Actual Amount" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("FMD_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" width="100%" align="center">
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALSUB" />&nbsp;<asp:Button ID="btnDetCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4">Fee Concession Details(Actual)</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvFeeDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data Added Yet..." SkinID="GridViewNormal" DataKeyNames="FCD_ID,FCD_FEE_ID,FCD_ACTUALAMT,FCD_AMTTYPE,FCD_SCH_ID,FCD_REF_BSU_ID,FCD_TAX_AMOUNT,FCD_NET_AMOUNT,FCD_ISINCLUSIVE_TAX,FCD_TAX_CODE">
                                <Columns>
                                    <asp:TemplateField HeaderText="FEE_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCD_ID" runat="server" Text='<%# bind("FCD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Concession Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# bind("FCM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("FEE_DESCR") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# bind("AMT_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount/%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgvFeeDetailsAMT" runat="server" Text='<%# bind("AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" OnClick="lnkBtnDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALMAIN" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                OnClientClick="return confirm('Are you sure you want to Delete This Record ?');"
                                Text="Delete" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                        </td>
                    </tr>
                </table>
                <%--<asp:HiddenField ID="h_GRD_ID" runat="server" />--%>
                <asp:HiddenField ID="h_FCT_ID_HEAD" runat="server" />
                <asp:HiddenField ID="H_REFID_HEAD" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <script type="text/javascript" language="javascript">
                    $(document).ready(function () {
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        prm.add_initializeRequest(InitializeRequest);
                        prm.add_endRequest(EndRequest);
                        InitStudentAutoComplete();
                    });
                    function InitializeRequest(sender, args) {
                    }
                    function EndRequest(sender, args) {
                        // after update occur on UpdatePanel re-init the Autocomplete
                        InitStudentAutoComplete();
                    }
                    function InitStudentAutoComplete() {
                        $("#<%=txtStdNo.ClientID%>").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: "../Fees/FeeConcessionCancellation.aspx/GetStudent",
                                    data: "{ 'BSUID': '" + GetBsuId() + "','prefix': '" + request.term + "'}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('$$')[1] + ' - ' + item.split('$$')[0],
                                                val: item.split('$$')[2]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        alert(response.responseText);
                                    },
                                    failure: function (response) {
                                        alert(response.responseText);
                                    }
                                });
                            },
                            select: function (e, i) {
                                $("#<%=h_STUD_ID.ClientID%>").val(i.item.val);
                                $("#<%=txtStdNo.ClientID%>").val(i.item.label);
                                __doPostBack('<%=h_STUD_ID.ClientID%>', "");
                            },
                            minLength: 1
                        });
                    }
                    function GetBsuId() {
                        return '<%= Session("sBsuId")%>';
                    }
                    function GetStudent() {

                        var NameandCode;
                        var result;
                        var url = "ShowStudent.aspx?type=CONCESSION";
    <%--result = window.showModalDialog(url, "", sFeatures)
    if (result != '' && result != undefined) {
        NameandCode = result.split('||');
        document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                //document.getElementById('<%=txtStud_Name.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2] + ' - ' + NameandCode[1];
                return true;
            }
            else {
                return false;
            }
}--%>

            var oWnd = radopen(url, "pop_getstud");

        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStdNo.ClientID%>').value = NameandCode[2] + ' - ' + NameandCode[1];
                __doPostBack('<%=txtStdNo.ClientID%>', 'TextChanged');
            }
        }


        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(getRoundOff());
            return true;
        }
        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
    var amt;
    amt = parseFloat(roundOff)
    if (isNaN(amt))
        amt = 2;
    return amt;
}

Sys.Application.add_load(
    function CheckForPrint() {
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            document.getElementById('<%= h_print.ClientID %>').value = '';
            //showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
            Popup('../Reports/ASPX Report/RptViewerModal.aspx');
        }
    }
            );


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

                </script>

                <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
                    ReloadOnShow="true" runat="server" EnableShadow="true">
                    <Windows>
                        <telerik:RadWindow ID="pop_getstud" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>
                </telerik:RadWindowManager>

            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

