<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeFeeMasterView.aspx.vb" Inherits="Fees_feeFeeMasterView" Theme="General" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 
 <script language="javascript" type="text/javascript">
 function ShowLeaveDetail(id)
       {    
            var sFeatures;
            sFeatures="dialogWidth: 559px; ";
            sFeatures+="dialogHeight: 405px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowEmpLeave.aspx?ela_id="+id,"", sFeatures)
         
            return false;
        }
        
    </script>                
     
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Fee Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
            <tr valign="top" >          
                <td valign="top" align="left">
                    <asp:HyperLink ID="hlAddnew" runat="server">Add New</asp:HyperLink>
                    <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                    </td>
            </tr>
        </table> 
    <table align="center" width="100%">
           
            <tr>
                <td align="center" valign="top" >
                    <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                        EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" > 
                        <Columns>  
                            <asp:TemplateField HeaderText="From Date">
                                <HeaderTemplate>
                                    Description<br />
                                    <asp:TextBox ID="txtFDate" runat="server"  Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Settlement Schedule"> 
                                <HeaderTemplate>
                                    Charge Schedule<br />
                                    <asp:TextBox ID="txtFClass" runat="server"  Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("SET_SCHEDULE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField >
                            <asp:TemplateField HeaderText="Age Group">
                                <HeaderTemplate>
                                   Collection Schedule<br />
                                   <asp:TextBox ID="txtAgegroup" runat="server"  Width="46px"></asp:TextBox>
                                   <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>        
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("COL_SCHEDULE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City">
                                <HeaderTemplate>
                                    Collection<br />
                                    <asp:TextBox ID="txtCity" runat="server"  Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("COL_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Date">
                                <HeaderTemplate>
                                    Service<br />
                                    <asp:TextBox ID="txtTDate" runat="server"  Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:CheckBoxField DataField="FEE_bSETUPBYGRADE" HeaderText="By Grade">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:CheckBoxField>
                             
                            <asp:TemplateField HeaderText="View">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FEE_ID" Visible="False">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView> 
                </td>
            </tr>
        </table>

                </div>
            </div>
         </div>
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
</asp:Content>

