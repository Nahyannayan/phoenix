﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Fee_Rec_Adj_IU.aspx.vb" Inherits="Fees_Fee_Rec_Adj_IU" Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <%--    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>--%>
    <script src="../Scripts/PopupJQuery.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
            function print() {
                if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                    document.getElementById('<%= h_print.ClientID %>').value = '';
                    //showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                    return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
                    return false;
                }
            }
        );

            function getStudent() {

                var sFeatures;
                var lstrVal;
                var lintScrVal;
                var pMode;
                var sFeatures;
                sFeatures = "dialogWidth: 875px; ";
                sFeatures += "dialogHeight: 600px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: yes; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";

                var ProviderBSU = document.getElementById('<%= h_BSUID.ClientID %>').value;
                var StuBsuId = document.getElementById('<%= ddlStudentBSU.ClientID %>').value;

                if (document.getElementById('<%= radStud.ClientID %>').checked == true)
                    Stu_type = "S";
                else
                    Stu_type = "E";

                pMode = "ALL_SOURCE_STUDENTS"
                //url = "../Fees/ShowStudent.aspx?type=" + pMode + "&ProviderBSUID=" + ProviderBSU + "&Stu_Bsu_Id=" + StuBsuId + "&STU_TYPE=" + Stu_type;
                url = "ShowStudent.aspx?type=" + pMode + "&ProviderBSUID=" + ProviderBSU + "&Stu_Bsu_Id=" + StuBsuId + "&STU_TYPE=" + Stu_type;
                //return ShowWindowWithClose(url, 'search', '55%', '85%')
                //return false;
                var oWnd = radopen(url, "pop_fee");
            <%--result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }

            NameandCode = result.split('||');
            document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];


            return true;--%>
            }
        function SetAllStudentValue(result) {
            var NameandCode = result.split('||');
            document.getElementById('<%=txtStdNo.ClientID%>').value = NameandCode[2];
            document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
            CloseFrame();
            return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }

        function showDocument(contenttype, filename) {
            var sFeatures;

            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";
            //result = window.showModalDialog("../Common/MultiDocViewer.aspx?id=0&path=Fees&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            //return false;
            return ShowWindowWithClose("../Common/MultiDocViewer.aspx?id=0&path=Fees&filename=" + filename + "&contenttype=" + contenttype, 'search', '55%', '85%')
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=txtStdNo.ClientID%>').value = NameandCode[2];
                document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtStdNo.ClientID%>', 'TextChanged');
            }
        }
    </script>
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Receive Fee Adjustment Transfer"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"
                                CssClass="error" />
                            <asp:HiddenField ID="hf_FAI_ID" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR"
                                CssClass="error" />
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="Error" />--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%" border="0">
                    <%--  <tr class="subheader_img">
            <td align="left" colspan="6" style="height: 19px">
                <asp:Label ID="lblHeader" runat="server" Text="Receive Fee Adjustment Transfer"></asp:Label>
            </td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From BusinessUnit</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblFrmBSU" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Created On</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblAdjCreatedDate" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">StudentNo</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblStuNo" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">StudentName</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblStuName" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblGrade" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Parent</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblParent" runat="server" Text="" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Comments</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtHeaderRemarks">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Supporting Docs</span>
                        </td>

                        <td align="left" colspan="3">
                            <asp:GridView ID="gvSuppDocsDR" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Documents Attached/Shared">
                                <Columns>
                                    <asp:TemplateField HeaderText="FileName">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkFileName" Text='<%# Bind("FAS_ORG_FILENAME") %>' runat="server"></asp:LinkButton>
                                            <asp:HiddenField ID="hf_ContentType" Value='<%# Bind("FAS_CONTENT_TYPE") %>' runat="server" />
                                            <asp:HiddenField ID="hf_filename" Value='<%# Bind("FAS_FILENAME") %>' runat="server" />
                                            <asp:HiddenField ID="hf_FASID" Value='<%# Bind("FAS_ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FAS_CONTENT_TYPE" HeaderText="FileType" />
                                    <asp:BoundField DataField="FAS_USER" HeaderText="Uploaded By" />
                                    <asp:TemplateField HeaderText="Shared" Visible="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSharedOB" Checked='<%# Bind("FAS_bShared") %>' AutoPostBack="true"
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDeleteSD" Text='Remove' runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:Label ID="lblRemarks" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Total Amount</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:Label ID="lblDRAmount" runat="server" Text="0.00" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg-lite" colspan="4">Fee Adjustments Details...
                        </td>
                    </tr>
                    <tr id="trStudentBSU" runat="server">
                        <td align="left" width="20%"><span class="field-label">Student Business Unit</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlStudentBSU" runat="server" AutoPostBack="True"
                                TabIndex="5">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"></td>

                        <td align="left" width="30%">
                            <asp:RadioButton ID="radStud" runat="server" CssClass="field-label" AutoPostBack="true" Checked="True" GroupName="STUD_TYPE"
                                Text="Student"></asp:RadioButton>
                            <asp:RadioButton ID="radEnq" runat="server" CssClass="field-label" AutoPostBack="true" GroupName="STUD_TYPE"
                                Text="Enquiry"></asp:RadioButton>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date  required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtDate">*</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date Required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" TargetControlID="txtDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True"></asp:TextBox>
                            &nbsp;<asp:ImageButton ID="imgStudent" runat="server" OnClientClick="getStudent();return false;"
                                ImageUrl="~/Images/cal.gif" />
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFeeType" runat="server">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblBalance" runat="server" Text="Balance :"></asp:Label><asp:Label
                                ID="lblBalanceAmt" runat="server">0.00</asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Amount</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAdjAmount" runat="server" Style="text-align: right;"
                                AutoCompleteType="Disabled">0.00</asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtAdjAmount">*</asp:RequiredFieldValidator>&nbsp;<asp:Label
                                    ID="lbDescription" runat="server"></asp:Label>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtAdjAmount"
                                FilterType="Custom,Numbers" ValidChars="." />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trSuppDocs" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Supporting Docs</span>
                        </td>

                        <td align="left" colspan="3">
                            <asp:FileUpload ID="UploadDocPhoto" runat="server" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />
                            <asp:RegularExpressionValidator ID="uplValidator" runat="server" ControlToValidate="UploadDocPhoto"
                                ErrorMessage="Only Excel, Word, Pdf and Image formats are allowed" ValidationExpression="(.+\.([Xx][Ll][Ss])|.+\.([Xx][Ll][Ss][Xx])|
                    .+\.([Dd][Oo][Cc])|.+\.([Dd][Oo][Cc][Xx])|.+\.([Pp][Dd][Ff])|.+\.([Pp][Nn][Gg])|.+\.([Jj][Pp][Gg])|.+\.([Jj][Pp][Ee][Gg])|.+\.([Gg][Ii][Ff]))"></asp:RegularExpressionValidator>
                            <asp:GridView ID="gvSuppDocsCR" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Documents Attached/Shared">
                                <Columns>
                                    <asp:TemplateField HeaderText="FileName">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkFileNameCR" Text='<%# Bind("FAS_ORG_FILENAME") %>' runat="server"></asp:LinkButton>
                                            <asp:HiddenField ID="hf_ContentTypeCR" Value='<%# Bind("FAS_CONTENT_TYPE") %>' runat="server" />
                                            <asp:HiddenField ID="hf_filenameCR" Value='<%# Bind("FAS_FILENAME") %>' runat="server" />
                                            <asp:HiddenField ID="hf_FASIDCR" Value='<%# Bind("FAS_ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FAS_CONTENT_TYPE" HeaderText="FileType" />
                                    <asp:BoundField DataField="FAS_USER" HeaderText="Uploaded By" />
                                    <asp:TemplateField HeaderText="Shared" Visible="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSharedOBCR" Checked='<%# Bind("FAS_bShared") %>' AutoPostBack="true"
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDeleteCR" OnClick="lnkDeleteCR_Click" Text='Remove' runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="tr_Addbutton" runat="server">
                        <td align="left" colspan="4" style="text-align: right">
                            <asp:Label ID="lblErrorFooter" runat="server" CssClass="error" EnableViewState="False" />&nbsp;
                <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                CssClass="table table-bordered table-row" CellPadding="4" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="FeeId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%# bind("DURATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("FEE_AMOUNT", "{0:###,###.00}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ApprovedAmount" Visible="false">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAppAmt" runat="server" Text='<%# Bind("FEE_AMOUNT", "{0:###,###.00}")%>'
                                                Style="text-align: right;" CssClass="textboxmedium" Width="110px" SkinID="TextBoxNormal"
                                                AutoCompleteType="Disabled"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbeApp" runat="server" TargetControlID="txtAppAmt"
                                                FilterType="Custom,Numbers" ValidChars="." />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" runat="server">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="4">
                            <asp:CheckBox ID="chkPrint" runat="server" Text="Print After Save" Checked="True"></asp:CheckBox>
                            <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="button"
                                Text="Approve" Visible="False" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" Visible="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
                <p></p>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="h_print" runat="server" />
    <asp:HiddenField ID="h_STUD_ID" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />


</asp:Content>
