﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeDebtFollowup.aspx.vb" Inherits="Fees_FeeDebtFollowup" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link href="../Scripts/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />--%>
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <link href="../cssfiles/FeePopUp.css" rel="stylesheet" />

    <style type="text/css">
        .RadComboBox_Default .rcbReadOnly {
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbDisabled {
            background-color: rgba(0,0,0,0.01) !important;
        }

            .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                background-color: transparent !important;
                border-radius: 0px !important;
                border: 0px !important;
                padding: initial !important;
                box-shadow: none !important;
            }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBoxDropDown .rcbList {
            line-height: 30px !important;
            font-size: 14px;
        }
    </style>


    <script language="javascript" type="text/javascript">
        function GetStudent() {

            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var sFeatures;
            sFeatures = "dialogWidth: 875px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var StuBsuId = document.getElementById('<%= h_Sel_Bsu_Id.ClientID %>').value;

            Stu_type = "S";


            pMode = "ALL_SOURCE_STUDENTS"
            url = "../Fees/ShowStudent.aspx?type=" + pMode + "&ProviderBSUID=" + StuBsuId + "&Stu_Bsu_Id=" + StuBsuId + "&STU_TYPE=" + Stu_type;
            result = radopen(url, "pop_student");
            <%--if (result == '' || result == undefined) {
                return false;
            }

            NameandCode = result.split('||');
            document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];


            return true;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Studentname.ClientID%>').value = NameandCode[1];
                document.getElementById('<%=h_STU_ID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtStudent.ClientID%>').value = NameandCode[2];

            }
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Fees Debt Follow Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar1" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">School</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="ddlBUnit" runat="server" Filter="Contains" ZIndex="2000" AutoPostBack="true" Width="100%" RenderMode="Lightweight" ToolTip="Type in unit name or short code"></telerik:RadComboBox>

                        </td>
                        <td align="left" width="20%"><span class="field-label">Follow Up List</span></td>

                        <td align="left" width="30%">
                            <table width="100%">
                                <tr>
                                    <td width="80%" align="left">
                                        <telerik:RadComboBox ID="ddlFollowups" runat="server" AutoPostBack="True" Filter="Contains" Width="100%" RenderMode="Lightweight" ZIndex="2000"></telerik:RadComboBox>
                                    </td>
                                    <td width="20%" align="left">
                                        <asp:ImageButton ID="btnReloadFollowup" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/refresh-icon2.gif" ToolTip="Click to reload the Students for the selected follow up list" Style="width: 20pt; height: 20pt" />
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Saved Filters</span></td>

                        <td align="left">
                            <telerik:RadComboBox ID="ddlFilters" runat="server" AutoPostBack="True" Filter="Contains" Width="100%" RenderMode="Lightweight" ZIndex="2000"></telerik:RadComboBox>

                        </td>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnSaveFilter" runat="server" CssClass="button" Text="Save Filter" />
                            <asp:Button ID="btnhidden" runat="server" Style="display: none;" />
                            <ajaxToolkit:ModalPopupExtender ID="MPE1" BackgroundCssClass="ModalPopupBG" TargetControlID="btnhidden"
                                PopupControlID="pnlSaveFilter" OkControlID="btnClose" CancelControlID="btnCancel"
                                DropShadow="true" PopupDragHandleControlID="gridheader" Drag="true" runat="server">
                            </ajaxToolkit:ModalPopupExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Academic Year</span></td>

                        <td align="left">
                            <telerik:RadComboBox ID="ddlAcdYear" runat="server" Filter="Contains" Width="100%" RenderMode="Lightweight" ZIndex="2000" AutoPostBack="true" ToolTip="Type in academic year"></telerik:RadComboBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" rowspan="3"><span class="field-label">Section</span></td>

                        <td align="left" rowspan="3">
                            <div class="checkbox-list" style="width: 80% !important;" runat="server" id="pnlGrades2">
                                <asp:TreeView ID="trGrades" ShowCheckBoxes="All" ExpandDepth="1" onclick="client_OnTreeNodeChecked()"
                                    runat="server">
                                </asp:TreeView>
                            </div>
                        </td>
                        <td align="left"><span class="field-label">Mobile No.</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>

                        <td align="left"><span class="field-label">Email Id</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtEmailId" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Outstanding</span></td>

                        <td align="left" class="pl-0">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:DropDownList ID="ddlfunction" runat="server">
                                            <asp:ListItem Selected="True" Value="&gt;">Greater Than</asp:ListItem>
                                            <asp:ListItem Value="&lt;">Less Than</asp:ListItem>
                                            <asp:ListItem Value="=">Equals</asp:ListItem>
                                            <asp:ListItem Value="!=">Not Equal</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtOutstanding" runat="server" Style="text-align: right;">0.00</asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtOutstanding" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="display: none;"><span class="field-label">Fees due since</span></td>

                        <td align="left" style="display: none;">
                            <asp:TextBox ID="txtDuedays" runat="server">30</asp:TextBox>
                            (days)
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                    TargetControlID="txtDuedays" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Parent name starting with</span></td>

                        <td align="left">
                            <telerik:RadComboBox CloseDropDownOnBlur="true" RenderMode="Lightweight" ID="ddlAlphabets" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true"
                                Width="100%" CheckedItemsTexts="DisplayAllInInput">
                            </telerik:RadComboBox>
                        </td>
                        <td align="left"><span class="field-label">Include COVID-19 Relief</span></td>
                        <td align="left">
                            <asp:RadioButton ID="rbALL" runat="server" CssClass="field-label" GroupName="SHOW3" Text="All" Checked="true"></asp:RadioButton>
                            <asp:RadioButton ID="rbYes" runat="server" CssClass="field-label" GroupName="SHOW3" Text="Yes"></asp:RadioButton>
                            <asp:RadioButton ID="rbNo" runat="server" CssClass="field-label" GroupName="SHOW3" Text="No"></asp:RadioButton>
                        </td>
                    </tr>

                    <tr>
                        <td align="left"><span class="field-label">Reason</span></td>

                        <td align="left" colspan="2">
                            <telerik:RadComboBox ID="ddlfollowupReasons" runat="server" Filter="Contains" Width="100%" RenderMode="Lightweight" ZIndex="2000"></telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Student</span></td>

                        <td align="left" colspan="2">
                            <br />
                            <asp:TextBox ID="txtStudent" runat="server" ToolTip="Type in if Student No is known or click search image" Width="90%" placeholder="Type in or Search Student No"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent();return false;" />
                            <asp:LinkButton ID="lbtnClearStudent" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                            <asp:LinkButton ID="lbtnLoadStuDetail" runat="server" CausesValidation="False"></asp:LinkButton>
                            <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TBWE3" runat="server"
                                TargetControlID="txtStudent"
                                WatermarkText="Type in or Search Student Id or Name"
                                WatermarkCssClass="watermarked" />--%>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                TargetControlID="lbtnClearStudent"
                                ConfirmText="Are you sure you want to remove the Student selection?" />
                            <asp:HiddenField ID="h_STU_ID" runat="server" />
                            <asp:HiddenField ID="h_Studentname" runat="server" />
                            <asp:HiddenField ID="h_Sel_Bsu_Id" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClientClick="return ConfirmFollowupClear();" CausesValidation="false" />
                        </td>
                    </tr>
                    <tr class="subHeader">
                        <td align="left" colspan="4"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvStudents" CssClass="table table-bordered table-row" runat="server" Width="100%" AutoGenerateColumns="False" SkinID="GridViewNormal" EmptyDataText="No students to show" DataKeyNames="ID,STU_ID" AllowPaging="True" PageSize="40">
                                <Columns>
                                    <%--<asp:BoundField DataField="REASON" HeaderText="Reason" />--%>
                                    <asp:BoundField DataField="SLNO" HeaderText="#" />
                                    <asp:TemplateField HeaderText="StudentID">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GRD_DISPLAY" HeaderText="Grade" />
                                    <asp:BoundField DataField="STU_DUE_AMOUNT" HeaderText="Due" DataFormatString="{0:#,##0.00}" />
                                    <asp:BoundField DataField="REASON" HeaderText="Reason" />
                                    <asp:TemplateField HeaderText="PrimayContact">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("STU_PRI_CONTACT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("STU_MOBILE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("STU_PRI_EMAIL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Res.Phone">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("STU_PRI_RESPHONE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FOLLOWUP_DATE" HeaderText="Follow Up On" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnAddComment" runat="server">Add Comments</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>

                    </tr>
                </table>
                <asp:Panel ID="pnlSaveFilter" Style="display: none;" runat="server">
                    <div style="min-width: 500px; min-height: 200px; background: white; border: 2px solid rgba(0,0,0,0.3); border-radius: 8px; padding: 0;">
                        <table width="100%">
                            <tr>
                                <td align="right" colspan="3">
                                    <asp:Button ID="btnClose" CssClass="button_small" runat="server" Text="X" />
                                </td>
                            </tr>
                            <tr>
                                <td class="title-bg" colspan="3">Save search filter
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <uc1:usrMessageBar runat="server" ID="usrMessageBar2" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Filter Name</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtFilterName" runat="server" Style="min-width: 400px;"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btnOkay" CssClass="button" runat="server" Text="Save" />
                                    <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" />
                                    <%--<input id="btnCancel" type="button" value="Cancel" />--%>
                                </td>
                            </tr>
                        </table>
                        <div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitStudentAutoComplete();
        });
        function InitializeRequest(sender, args) {
        }
        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitStudentAutoComplete();
        }
        function InitStudentAutoComplete() {

            $("#<%=txtStudent.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "FeeDebtFollowup.aspx/GetStudent",
                        data: "{ 'BSUID': '" + GetBsuId() + "','STUTYPE':'" + GetStuType() + "','prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1] + ' - ' + item.split('-')[0],
                                    val: item.split('-')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=h_STU_ID.ClientID%>").val(i.item.val);
                    document.getElementById('<%=lbtnLoadStuDetail.ClientID%>').click();
                },
                minLength: 1
            });
        }
        function GetBsuId() {
            var dropFind = $find("<%= ddlBUnit.ClientID%>");
            var valueFind = dropFind.get_value();
            return valueFind;
        }
        function GetStuType() {
            var STU_TYPE = "S";
            return STU_TYPE;
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        function ConfirmFollowupClear() {

            var dropFind = $find("<%= ddlFollowups.ClientID%>");
            var valueFind = dropFind.get_value();
            if (valueFind > 0) {
                return confirm("Selected follow up list will be cleared(if exists) and student list will be populated as per the search criteria. Are you sure you want to continue?")
            }
            else
                return true;
        }
    </script>
</asp:Content>

