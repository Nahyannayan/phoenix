<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeProcessFlowView.aspx.vb" Inherits="FeeProcessFlowView" Theme="General" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Process Flow"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">

                    <tr>
                        <td align="center" valign="top">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="PROC_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPROC_ID" runat="server" Text='<%# Bind("PRO_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>
                                            Business Unit<br />
                                            <asp:TextBox ID="txtTDate" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                                       
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Academic Year">
                                        <HeaderTemplate>
                                            Academic Year<br />
                                            <asp:TextBox ID="txtFrom" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>&nbsp;</td>
                                                               
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process">
                                        <HeaderTemplate>
                                            Process<br />
                                            <asp:TextBox ID="txtRemarks" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("PRO_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="ACD_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACD_ID" runat="server" Text='<%# bind("ACD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
