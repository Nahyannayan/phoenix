Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeDayEndProcess
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load 
        If Page.IsPostBack = False Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            btnSave.Attributes.Add("onclick", "return UnpostOthFeecharge(" & h_UnpostFChg.ClientID & ") ")


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                gvHistory.Attributes.Add("bordercolor", "#1b80b6")
               
                If Session("sUsr_name") = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_DAYEND_PROCESS Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_DAYEND_PROCESS
                            lblHead.Text = "Day End Process"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ' txtDate.Text = Format(Now.Date.AddDays(1), OASISConstants.DateFormat)
                    getDate()
                    ddlBUnit.DataBind()

                    ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
                    BindHistory()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
            btnprint.Visible = False
        End If
    End Sub

    Sub getDate()
        
        Dim strDate As String = UtilityObj.GetDataFromSQL("select REPLACE( CONVERT( VARCHAR(11),isnull(max(fta_trandt) + 1,getdate()),106),' ','/') from fees.FEE_TRNSALL  " _
        & " WHERE fta_trantype='DAYEND' AND fta_bhardclose=1 AND fta_bsu_id =  '" & Session("sBsuid") & "' ", _
       ConnectionManger.GetOASIS_FEESConnectionString)
        txtDate.Text = strDate
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        lblError.Text = ""
        Dim STATUS As Integer
        Dim str_err As String = ""
        If IsDate(txtDate.Text) = False Then
            str_err = str_err & "<br />" & "Please Enter Valid Date"  
        End If
        If Not IsNumeric(txtDaysToInclude.Text) Then
            str_err = str_err & "<br />" & "Please Enter Numebr of Days" 
        End If
        Dim strfDate As String = txtDate.Text.Trim 
        str_err = str_err & DateFunctions.checkdate(strfDate)
        Dim str_FYear As String = UtilityObj.GetDataFromSQL("SELECT FYR_ID FROM FINANCIALYEAR_S " _
        & " WHERE '" & strfDate & "' BETWEEN FYR_FROMDT AND FYR_TODT AND ISNULL(FYR_bCLOSE,0)=0", _
       ConnectionManger.GetOASISConnectionString)
        If str_FYear = "" Or str_FYear = "--" Then
            str_err = str_err & "<br />" & "Check Financial Year"
        End If
        If str_err <> "" Then
            'lblError.Text = str_err
            usrMessageBar.ShowNotification(str_err, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtDate.Text = strfDate
        End If
        Try 
            Dim JHD_NEWDOCNO As String = ""
            Dim trans As SqlTransaction

            Dim conn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            conn.Open()
            trans = conn.BeginTransaction("SampleTransaction")
            Try 
                STATUS = FeeDayendProcess.F_GenerateCollectionVoucher(ddlBUnit.SelectedItem.Value, _
                txtDate.Text, Session("sUsr_name"), chkHardClose.Checked, txtDaysToInclude.Text, conn, trans)
                Dim ShowWarning As Boolean = False
                If STATUS = 2250 Then
                    ShowWarning = True
                    STATUS = 0
                End If
                If STATUS = 0 Then
                    Session("sQCdocnos") = New ArrayList
                    Dim dsQCVoucherNos As New DataSet
                    Dim str_sql_vouchernos As String = "SELECT VCH_DOCNO FROM " & OASISConstants.dbFinance & ".dbo.VOUCHER_CHQ_H" _
                    & " WHERE (VCH_DOCTYPE = 'QC') AND (VCH_SUB_ID = '" & Session("SUB_ID") & "') " _
                    & " AND (VCH_BSU_ID = '" & ddlBUnit.SelectedItem.Value & "')" _
                    & " AND VCH_DOCDT='" & strfDate & "' AND ISNULL(VCH_bPOSTED,0)=0 "
                    dsQCVoucherNos = SqlHelper.ExecuteDataset(trans, CommandType.Text, str_sql_vouchernos)
                    For I As Integer = 0 To dsQCVoucherNos.Tables(0).Rows.Count - 1
                        STATUS = FeeDayendProcess.POSTVOUCHER_CHQ(Session("SUB_ID"), _
                        ddlBUnit.SelectedItem.Value, str_FYear, "QC", dsQCVoucherNos.Tables(0).Rows(I)(0), trans)
                        Session("sQCdocnos").add(dsQCVoucherNos.Tables(0).Rows(I)(0))
                        If STATUS <> 0 Then
                            Exit For
                        End If
                    Next
                End If
                If STATUS = 0 Then
                    trans.Commit()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlBUnit.SelectedItem.Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                    btnprint.Visible = True
                    BindHistory()
                Else
                    trans.Rollback()
                End If
                If STATUS = 0 And ShowWarning = True Then
                    'lblError.Text = getErrorMessage(2250)
                    usrMessageBar.ShowNotification(getErrorMessage(2250), UserControls_usrMessageBar.WarningType.Danger)
                Else
                    'lblError.Text = getErrorMessage(STATUS)
                    usrMessageBar.ShowNotification(getErrorMessage(STATUS), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                trans.Rollback()
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub


    Protected Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        lblError.Text = ""
        PrintPostedVouchers()
    End Sub


    Private Sub PrintPostedVouchers()
       
        Dim arrList As ArrayList = Session("sQCdocnos")
        Dim ienum As IEnumerator = arrList.GetEnumerator
        Dim vDocNo As String = String.Empty
        Dim comma As String = String.Empty
        While ienum.MoveNext
            vDocNo = vDocNo & comma & "'" & ienum.Current & "'"
            comma = ","
        End While

        'Session("ReportSource") = AccountsReports.JournalVouchers(vDocNo, "", Session("sBSUID"), "", vDocNo, "QC")
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)

        Session("ReportSource") = AccountsReports.JournalVouchers(vDocNo, "", Session("sBSUID"), Session("F_YEAR"))
        ' Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub


    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        BindHistory()
    End Sub


    Sub BindHistory()
        lblError.Text = ""
        If IsDate(txtDate.Text) Then
            Dim dsDayendDetails As DataSet = FeeDayendProcess.GetDayEndHistory(False, _
            ddlBUnit.SelectedItem.Value, txtDate.Text, False)
            If dsDayendDetails.Tables.Count > 0 Then
                gvHistory.DataSource = dsDayendDetails.Tables(0)
                gvHistory.DataBind()
            End If
            If dsDayendDetails.Tables.Count > 1 Then
                gvCloseStatus.DataSource = dsDayendDetails.Tables(1)
                gvCloseStatus.DataBind()
            End If

            gvDayend.DataSource = FeeDayendProcess.GETDAYENDSTATUS(ddlBUnit.SelectedItem.Value, txtDate.Text, False)
            gvDayend.DataBind()
            Dim _daSet As DataSet = FeeDayendProcess.UnpostedOthFeecharge(ddlBUnit.SelectedItem.Value, txtDate.Text, False)
            h_UnpostFChg.Value = _daSet.Tables(0).Rows(0)("TOTCOUNT").ToString()

        End If
    End Sub


    Protected Sub imgFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrom.Click
        BindHistory()
    End Sub


    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        BindHistory()
    End Sub

   
    Protected Sub gvHistory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim LnkBtn As LinkButton = New LinkButton()
            If e.Row.RowIndex < 3 Then
                Dim docNoArr As String()
                Dim docNoStr As String = ""
                docNoArr = e.Row.Cells(1).Text.ToString().Split(",")
                e.Row.Cells(1).Text = ""
                For x As Integer = 0 To docNoArr.Length - 1
                    Dim docNo As String
                    'docNo = "<a onclick = 'getPrint(" & "'" & docNoArr(x) & "'" & " )' style = 'cursor:hand'>" & docNoArr(x) & "</a>"

                    docNo = "<a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & _
                "javascript:getPrint( '" + docNoArr(x).Trim() + "', '" + e.Row.Cells(0).Text + "' ); " & Chr(34) & _
                " title = " & Chr(34) & " Click for print.. " & docNoArr(x) & Chr(34) & " style=" & Chr(34) & "cursor:hand;" & _
                " text-decoration: underline;" & Chr(34) & " class=" & Chr(34) & "gridheader_Link" & Chr(34) & " >" & docNoArr(x) & "</a>"

                    e.Row.Cells(1).Text += docNo & " "
                Next
                e.Row.Cells(1).Style("cursor") = "hand"
            Else
                e.Row.Cells(1).Text = e.Row.Cells(1).Text.Replace(",", "  : ")
            End If



        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

