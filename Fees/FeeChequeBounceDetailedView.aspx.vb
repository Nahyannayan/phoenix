Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Web.UI
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Reflection

Partial Class FEES_FEECHEQUEBOUNCEDETAILEDVIEW
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_BOUNCE Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("datamode") = "view" Then
                    Dim FCR_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FCR_ID").Replace(" ", "+"))
                    Dim drreader As SqlDataReader = FEECHEQUEBOUNCE.GetBouncedChequeDetails(FCR_ID, ConnectionManger.GetOASIS_FEESConnectionString)
                    If drreader.Read() Then
                        txtBDate.Attributes.Add("ReadOnly", "ReadOnly")
                        txtCHQDate.Attributes.Add("ReadOnly", "ReadOnly")
                        txtBNKID.Attributes.Add("ReadOnly", "ReadOnly")
                        txtBNKName.Attributes.Add("ReadOnly", "ReadOnly")
                        txtCHQNO.Attributes.Add("ReadOnly", "ReadOnly")
                        txtEMR.Attributes.Add("ReadOnly", "ReadOnly")
                        'txtNarration.Attributes.Add("ReadOnly", "ReadOnly")
                        txtStudName.Attributes.Add("ReadOnly", "ReadOnly")
                        txtStudNo.Attributes.Add("ReadOnly", "ReadOnly")

                        txtBDate.Text = Format(drreader("FCR_DATE"), OASISConstants.DateFormat)
                        txtCHQDate.Text = Format(drreader("CHQ_DATE"), OASISConstants.DateFormat)
                        txtBNKID.Text = drreader("BNK_ID")
                        txtBNKName.Text = drreader("BNK_DESCRIPTION")
                        txtCHQNO.Text = drreader("CHQ_NO")
                        txtEMR.Text = drreader("EMR_DESCR")
                        txtNarration.Text = drreader("FCR_NARRATION")
                        ' txtRecNo.Text = drreader("REC_NO")
                        txtStudName.Text = drreader("STU_NAME")
                        txtStudNo.Text = drreader("STU_NO")
                        lblACY_DESCR.Text = drreader("ACY_DESCR")
                        lblAmount.Text = drreader("CHQ_AMOUNT")
                        lblGRM_ID.Text = drreader("GRM_DESCR")
                        lblSTUType.Text = drreader("STU_TYPE")
                        lblSTU_BSU.Text = drreader("STU_BSU_NAME")
                        hfSTUID.Value = drreader("FCR_STU_ID")
                        GridBindFeeDetails(FCR_ID)
                        DisableControls(MainTable)
                        If drreader("FCR_Bposted") = True Then
                            'Me.lblError.Text = "Entry posted, no modifications are allowed."
                            usrMessageBar.ShowNotification("Entry posted, no modifications are allowed.", UserControls_usrMessageBar.WarningType.Danger)
                            Me.btnEdit.Enabled = False
                        End If
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Private Sub GridBindFeeDetails(ByVal vFCR_ID As Integer)
        gvFeeDetails.DataSource = GetBouncedChequesFeeDetails(vFCR_ID, ConnectionManger.GetOASIS_FEESConnectionString)
        gvFeeDetails.DataBind()
    End Sub

    Public Shared Function GetBouncedChequesFeeDetails(ByVal vFCR_ID As Integer, ByVal str_conn As String) As SqlDataReader
        Dim str_sql As String = "exec FEES.GetChequeBounceDetails '" & vFCR_ID & "'"
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Private Sub DisableControls(control As System.Web.UI.Control)

        For Each c As System.Web.UI.Control In control.Controls

            ' Get the Enabled property by reflection.
            Dim type As Type = c.GetType
            If TypeOf c Is TextBox Then
                Dim prop As PropertyInfo = type.GetProperty("Enabled")

                ' Set it to False to disable the control.
                If Not prop Is Nothing Then
                    prop.SetValue(c, False, Nothing)
                End If
            End If

            ' Recurse into child controls.
            If c.Controls.Count > 0 Then
                Me.DisableControls(c)
            End If

        Next

    End Sub
    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Me.txtNarration.Enabled = True
        Me.txtNarration.ReadOnly = False
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim trans As SqlTransaction = objConn.BeginTransaction("FEE_CHQ_CANCEL")
        Try
            Dim vNEW_FCR_ID As String = String.Empty
            Dim retVal As Integer
            Dim FCR_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FCR_ID").Replace(" ", "+"))
            retVal = FEECHEQUEBOUNCE.F_SAVEFEECHEQUERETURN_H(FCR_ID, txtBDate.Text, Session("sBsuid"), "S", hfSTUID.Value, _
                                          0, txtNarration.Text, False, Session("sBsuid"), "0", vNEW_FCR_ID, trans)
            If retVal <> 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                DisableControls(MainTable)
            End If
        Catch

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub
End Class
