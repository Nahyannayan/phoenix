﻿Imports Telerik.Web.UI
Imports System
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports UtilityObj


Partial Class Fees_PerformaInvoiceNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property IsTuitionINV() As Boolean
        Get
            Return ViewState("IsTuitionINV")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsTuitionINV") = value
        End Set
    End Property
    Private Property bMultiCurrency() As Boolean
        Get
            Return ViewState("bMultiCurrency")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bMultiCurrency") = value
        End Set
    End Property
    Private Property ExgRate() As Double
        Get
            Return ViewState("ExgRate")
        End Get
        Set(ByVal value As Double)
            ViewState("ExgRate") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("SID") = (IIf(Request.QueryString("SID") Is Nothing, "0", Request.QueryString("SID")).Replace(" ", "+"))
            ViewState("SID") = IIf(ViewState("SID") = "0", "0", Encr_decrData.Decrypt(ViewState("SID")))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If ViewState("MainMnu_code") = OASISConstants.MNU_FEE_PERFORMAINVOICE Or ViewState("MainMnu_code") = "F300402" Then
                IsTuitionINV = True
            Else
                IsTuitionINV = False
            End If
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_PERFORMAINVOICE And ViewState("MainMnu_code") <> "F100136" And ViewState("MainMnu_code") <> "F300402") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Dim sqlStr As String
            sqlStr = "SELECT ISNULL(BSU_bFeesMulticurrency,0) FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE bsu_id='" & Session("sbsuid") & "'"
            If Mainclass.getDataValue(sqlStr, "OASISConnectionString") Then
                bMultiCurrency = True
                pnlCurrency.Visible = True
                gvStudentDetails.Columns(6).Visible = True
            Else
                pnlCurrency.Visible = False
                bMultiCurrency = False
                gvStudentDetails.Columns(6).Visible = False
            End If

            ClearForm()
            trACD.Attributes.Add("onClick", "AlertMe();")
            trTerm.Attributes.Add("onClick", "AlertMe();")
            trCompany.Attributes.Add("onClick", "AlertMe();")
            trFeeType.Attributes.Add("onClick", "AlertMe();")
            trFilters.Attributes.Add("onClick", "AlertMe();")
            trStudentFilters.Attributes.Add("onClick", "AlertMe();")

            FillACD()
            FillFeeTypeDLL()
            PopulateTerms_Months()
            chkGroupInvoice.Attributes.Add("onclick", "GroupInvoice();")
            Session("FEE_PERF") = Nothing
            'If bMultiCurrency Then
            '    bind_Currency()
            'End If
            If ViewState("datamode") = "view" Then
                Dim vINV_NO As String = Encr_decrData.Decrypt(Request.QueryString("inv_no").Replace(" ", "+"))
                Dim vCOMP_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("COMP_ID").Replace(" ", "+"))
                Dim ACD_ID As Integer = 0
                If Not Request.QueryString("ACDID") Is Nothing AndAlso Encr_decrData.Decrypt(Request.QueryString("ACDID").Replace(" ", "+")) <> "" Then
                    ACD_ID = Encr_decrData.Decrypt(Request.QueryString("ACDID").Replace(" ", "+"))
                    ddlAcademicYear.SelectedValue = ACD_ID
                End If
                Dim vType As String = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                Dim vENQ As Boolean = False
                If vType = "ENQ" Then
                    chkEnquiry.Checked = True
                End If
                ViewState("vInvNo") = vINV_NO
                FillDetails(vINV_NO, chkEnquiry.Checked)
            ElseIf ViewState("datamode") = "add" Then
                If Not Request.QueryString("SID") Is Nothing Then
                    h_Students.Value = Encr_decrData.Decrypt(Request.QueryString("SID").Replace(" ", "+"))
                    txtStuNo.Text = FeeCommon.GetStudentNo(h_Students.Value, False)
                    txtStuName.Text = FeeCommon.GetStudentName(h_Students.Value, False)
                End If
            End If

        End If
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Private Sub FillFeeTypeDLL()
        ddlFeeType.Items.Clear()
        ddlAddFeeType.Items.Clear()
        h_FeeTypes.Value = ""
        Dim dtFeetype As DataTable = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), Convert.ToInt16(h_Company_ID.Value), chkService.Checked, False, IsTuitionINV)
        If Not dtFeetype Is Nothing AndAlso dtFeetype.Rows.Count > 0 Then
            For Each dr As DataRow In dtFeetype.Rows
                Dim rad As New RadComboBoxItem
                rad.Text = dr(2)
                rad.Value = dr(3)
                If ViewState("MainMnu_code") = OASISConstants.MNU_FEE_PERFORMAINVOICE And rad.Value = 5 Then
                    rad.Checked = True
                End If
                'rad.Checked = True
                ddlFeeType.Items.Add(rad)
                h_FeeTypes.Value = h_FeeTypes.Value & "|" & dr(3)
            Next
            ddlAddFeeType.DataSource = dtFeetype
            ddlAddFeeType.DataValueField = "FEEID"
            ddlAddFeeType.DataTextField = "FEE_DESCR"
            ddlAddFeeType.DataBind()
        End If
        CreateAddFeeTypeSchema()
        GridBindgvAddFeeDet()
    End Sub

    Private Sub PopulateTerms_Months()
        If rblInvPeriod.SelectedValue = "M" Then
            PopulateMonths()
            Me.lnkBtnSelectmonth.Text = "Select Month(s)"
        Else
            PopulateTerms()
            Me.lnkBtnSelectmonth.Text = "Select Term(s)"
        End If
        SelectAllTermsorMonths(trMonths.Nodes, True)
    End Sub

    Private Sub PopulateMonths()
        Dim dtTable As DataTable = FEEPERFORMAINVOICE.PopulateMonthsInAcademicYear(ddlAcademicYear.SelectedValue, Session("sbsuid"))
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("AMS_MONTH"))
            If contains Then
                Continue While
            End If
            'If trSelectAll.ChildNodes.Contains(trNodeTRM_DESCRIPTION) Then
            '    Continue While
            'End If
            Dim strAMS_MONTH As String = "TRM_DESCRIPTION = '" & _
            drTRM_DESCRIPTION("TRM_DESCRIPTION") & "'"
            Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "AMS_MONTH", DataViewRowState.OriginalRows)
            Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
            While (ienumAMS_MONTH.MoveNext())
                Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("MONTH_DESCR"), drMONTH_DESCR("AMS_ID")) 'drMONTH_DESCR("AMS_MONTH"))
                trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub
    Private Sub PopulateTerms()
        Dim dtTable As DataTable = FEEPERFORMAINVOICE.PopulateTermsInAcademicYear(ddlAcademicYear.SelectedValue, Session("sbsuid"))
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("TRM_ID"))
            If contains Then
                Continue While
            End If
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub

    Private Sub SelectAllTermsorMonths(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTermsorMonths(node.ChildNodes, selAll)
            End If
        Next
    End Sub

    Private Sub ClearForm()
        Session("FEE_PERF") = Nothing
        ViewState("Months") = Nothing
        ViewState("dtMonths") = Nothing
        ViewState("gvAddFeeDet") = Nothing
        ViewState("vInvNo") = Nothing
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        Me.h_Company_ID.Value = "-1"
        Me.h_duplicate.Value = ""
        Me.h_STU_GRD_ID.Value = ""
        Me.h_stu_selected4edit.Value = ""
        Me.h_Students.Value = ""
        Me.h_print.Value = ""
        Me.txtCompanyDescr.Text = ""
        Me.chkCompanyFilter.Checked = False
        Me.txtRemarks.Text = ""
        ExgRate = 1
        lblExgRate.Text = 1
        If bMultiCurrency Then
            bind_Currency()
        End If
    End Sub
    Private Function CreateFeeDetailSchema() As DataTable
        Dim dtFee As New DataTable
        Try
            Dim dcSTU_ID As New DataColumn("STU_ID", System.Type.GetType("System.Int32"))
            Dim dcSTU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim dcSTU_NAME As New DataColumn("STU_NAME", System.Type.GetType("System.String"))
            Dim dcSTU_GRD_ID As New DataColumn("STU_GRD_ID", System.Type.GetType("System.String"))
            Dim dcSTU_AMOUNT As New DataColumn("STU_AMOUNT", System.Type.GetType("System.Double"))

            dtFee.Columns.Add(dcSTU_ID)
            dtFee.Columns.Add(dcSTU_NO)
            dtFee.Columns.Add(dcSTU_NAME)
            dtFee.Columns.Add(dcSTU_GRD_ID)
            dtFee.Columns.Add(dcSTU_AMOUNT)
        Catch ex As Exception
            dtFee = Nothing
        End Try
        Return dtFee
    End Function
    Private Sub UpdateMonthsSelected()
        Try
            Dim dtDt As New DataTable
            Dim cMonthID As New DataColumn("MonthID", System.Type.GetType("System.Int32"))
            Dim cMonthDescr As New DataColumn("MonthDescr", System.Type.GetType("System.String"))
            dtDt.Columns.Add(cMonthID)
            dtDt.Columns.Add(cMonthDescr)
            ViewState("Months") = ""
            Dim pipe = ""
            Dim dr As DataRow
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                dr = dtDt.NewRow()
                dr("MonthID") = node.Value
                dr("MonthDescr") = node.Text
                dtDt.Rows.Add(dr)
                ViewState("Months") &= pipe & node.Value
                pipe = "|"
            Next
            ViewState("dtMonths") = dtDt
        Catch
            ViewState("dtMonths") = Nothing
        End Try
    End Sub
    Private Function GetSelectedFeeTypes() As String
        Dim collection As IList(Of RadComboBoxItem) = ddlFeeType.CheckedItems
        Dim FeeTypes As String = "", pipe As String = ""
        For Each item As RadComboBoxItem In collection
            FeeTypes &= pipe & item.Value
            pipe = "|"
        Next
        Return FeeTypes
    End Function
    Private Function UPDATEFEESPLIUPDETAILS(ByVal STUD_ID As Integer, ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal STU_TYPE As STUDENTTYPE, ByVal selPD As SELECTEDPROFORMADURATION, ByRef TOTAL As Double) As ArrayList
        'FEEPERFORMAINVOICE.GetFeeDetails(STUD_ID, STU_TYPE, ACD_ID, GRD_ID, Session("sBsuId"), ViewState("Months"), GetSelectedFeeTypes(), selPD, TOTAL, chkNextAcademicYear.Checked)
        Dim STUDENT_SUBDETAILS As ArrayList = FEEPERFORMAINVOICE.GetTotalFees(STUD_ID, STU_TYPE, ACD_ID, GRD_ID, Session("sBSUID"), ViewState("Months"), GetSelectedFeeTypes(), selPD, TOTAL, chkNextAcademicYear.Checked, ExgRate)
        'Dim objFPI As New FEEPERFORMAINVOICE
        'objFPI.STU_ID = STUD_ID
        'objFPI.STU_TYPE = IIf(chkEnquiry.Checked, "E", "S")
        'objFPI.GetStudentDetails(chkNextAcademicYear.Checked)
        'Dim dr As DataRow = DirectCast(ViewState("gvStudentDetails"), DataTable).NewRow
        'dr("STU_ID") = STUD_ID
        'dr("STU_NO") = objFPI.STU_NO
        'dr("STU_NAME") = objFPI.STU_NAME
        'dr("STU_GRD_ID") = GRD_ID
        'dr("STU_AMOUNT") = TOTAL
        'DirectCast(ViewState("gvStudentDetails"), DataTable).Rows.Add(dr)
        'DirectCast(ViewState("gvStudentDetails"), DataTable).AcceptChanges()
        Return STUDENT_SUBDETAILS
    End Function
    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        sTemp.Append("<table class='popdetails'>") ' border=1 bordercolor=#1b80b6 bgcolor=#ffcccc cellpadding=1 cellspacing=0>")
        sTemp.Append("<tr>")
        sTemp.Append("<td colspan='2' align='center'><b>FEE SPLITUP</b></td>")
        sTemp.Append("</tr>")

        Dim STUD_ID As Integer = contextKey
        If HttpContext.Current.Session("FEE_PERF") Is Nothing Then
            sTemp.Append("</table>")
            Return sTemp.ToString()
        End If
        Dim vFEE_PERF As New FEEPERFORMAINVOICE
        Dim httab As Hashtable = HttpContext.Current.Session("FEE_PERF")
        If httab.ContainsKey(STUD_ID.ToString) Then
            vFEE_PERF = httab.Item(STUD_ID.ToString)
        End If

        If Not vFEE_PERF Is Nothing Then
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan='2'><b>Student : " & vFEE_PERF.FPH_STU_NAME & "</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            'sTemp.Append("<td><b>FEE ID</b></td>")
            sTemp.Append("<td><b>FEE DESCR</b></td>")
            sTemp.Append("<td align='right'><b>AMOUNT</b></td>")
            sTemp.Append("</tr>")

            Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
            Dim ienum As IEnumerator = arrList.GetEnumerator()
            Dim htFeeTypes As New Hashtable
            While (ienum.MoveNext())
                Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
                htFeeTypes(FEE_SUB_DET.FPD_FEE_DESCR) = htFeeTypes(FEE_SUB_DET.FPD_FEE_DESCR) + FEE_SUB_DET.FPD_AMOUNT + FEE_SUB_DET.FPD_ADJ_AMOUNT
            End While
            Dim iDictEnum As IDictionaryEnumerator = htFeeTypes.GetEnumerator()
            While (iDictEnum.MoveNext())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & iDictEnum.Key & "</td>")
                sTemp.Append("<td align='right'>" & iDictEnum.Value & "</td>")
                sTemp.Append("</tr>")
            End While
        End If
        sTemp.Append("</table>")

        Return sTemp.ToString()
    End Function
    Private Shared Sub ShowCheckedItems(ByVal comboBox As RadComboBox, ByVal literal As Literal)
        Dim sb As New StringBuilder()
        Dim collection As IList(Of RadComboBoxItem) = comboBox.CheckedItems

        If (collection.Count <> 0) Then

            sb.Append("<h3>Checked Items:</h3><ul class=""results"">")

            For Each item As RadComboBoxItem In collection
                sb.Append("<li>" + item.Text + "</li>")
            Next

            sb.Append("</ul>")

            literal.Text = sb.ToString()
        Else
            literal.Text = "<p>No items selected</p>"
        End If


    End Sub
    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
        h_FeeTypes.Value = GetSelectedGrades()
    End Sub

    Function GetSelectedGrades() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlFeeType.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                If item.Text = "ALL" Then
                    str += item.Value
                Else
                    str += item.Value
                End If

                If str <> "" Then
                    str += "|"
                End If
            Next
        End If
        Return str

    End Function
    <WebMethod()> _
    Public Shared Function GetCompany(ByVal prefix As String) As String()
        Dim company As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT COMP_ID,COMP_NAME FROM dbo.COMP_LISTED_M WITH(NOLOCK) WHERE ISNULL(COMP_bACTIVE,0)=1 AND ISNULL(COMP_COUNTRY,'')='" & HttpContext.Current.Session("BSU_COUNTRY_ID") & "' AND " & "COMP_NAME like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        company.Add(String.Format("{0}-{1}", sdr("COMP_NAME"), sdr("COMP_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return company.ToArray()
        End Using
    End Function

    Private Sub FillDetails(ByVal vINV_NO As String, ByVal bENQ As Boolean)
        Dim FPH_ID As Integer
        Dim httab As Hashtable = FEEPERFORMAINVOICE.GetDetails(vINV_NO, bENQ, FPH_ID, Session("sBsuId"), ddlAcademicYear.SelectedValue)
        'Dim httab As Hashtable = FEEPERFORMAINVOICE.GetDetails(Session("sBSUID"), ACD_ID, GRD_ID, COMP_ID, vDATE, FPH_ID)
        If Not httab Is Nothing Then
            Session("FEE_PERF") = httab

            Dim ienum As IDictionaryEnumerator = httab.GetEnumerator
            While (ienum.MoveNext())
                Dim FEE_DET As FEEPERFORMAINVOICE = ienum.Value
                txtCompanyDescr.Text = FEE_DET.FPH_COMP_NAME
                h_Company_ID.Value = FEE_DET.FPH_COMP_ID
                txtDate.Text = Format(FEE_DET.FPH_DT, OASISConstants.DateFormat)
                txtInvoiceCaption.Text = FEE_DET.FPH_INVOICE_CAPTION
                txtRemarks.Text = FEE_DET.FPH_REMARKS
                chkService.Checked = FEE_DET.FPH_IsServiceINV
                chkNewStudent.Checked = FEE_DET.FPH_IsNewStudent   'swapna added
                chkAccStatus.Checked = FEE_DET.FPH_bAccountStatus   'swapna added

                If ddCurrency.Items.Count > 0 And bMultiCurrency And FEE_DET.FPH_CURRENCY <> "" Then
                    Dim lstDrp As New ListItem
                    lstDrp = ddCurrency.Items.FindByText(FEE_DET.FPH_CURRENCY)
                    If Not lstDrp Is Nothing Then
                        ddCurrency.SelectedValue = lstDrp.Value
                    End If
                    DDCurrency_SelectedIndexChanged(Nothing, Nothing)
                End If
                DisableControls(True)
                Exit While
            End While
            GridBindStudDetails()
            Session("FEE_PERF") = httab
            lnkBtnSelectmonth.Text = "Selected Months"
            Dim arrListSelectedMonth As ArrayList = FEEPERFORMAINVOICE.GetSelectedMonthsDetails(FPH_ID)
            FillTreeViewDetails(arrListSelectedMonth)
        End If
    End Sub
    Private Sub FillTreeViewDetails(ByVal arrListSelectedMonth As ArrayList)
        If arrListSelectedMonth Is Nothing OrElse arrListSelectedMonth.Count <= 0 Then Return
        Dim i As Integer = 0
        While (i < arrListSelectedMonth.Count)
            FillTreeWithValue(arrListSelectedMonth(i), trMonths.Nodes)
            i += 1
        End While
    End Sub
    Private Sub FillTreeWithValue(ByVal MonthID As Integer, ByRef Nodes As TreeNodeCollection)
        For Each Node As TreeNode In Nodes
            If (Node.ChildNodes Is Nothing OrElse Node.ChildNodes.Count <= 0) Then
                If Node.Value = MonthID Then Node.Checked = True
            Else
                FillTreeWithValue(MonthID, Node.ChildNodes)
            End If
        Next
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        FillFeeTypeDLL()
        rblInvPeriod_SelectedIndexChanged(sender, e)
    End Sub
    Protected Sub lbtnClearCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnClearCompany.Click
        ClearStudents()
    End Sub
    Private Sub ClearStudents()
        Me.chkCompanyFilter.Checked = False
        Me.chkEnquiry.Checked = False
        Me.chkExcludeTC.Checked = False
        Me.chkGroupInvoice.Checked = False
        Me.chkNewStudent.Checked = False
        Me.h_Company_ID.Value = "-1"
        Me.txtCompanyDescr.Text = ""
        txtRemarks.Text = ""
        lbtnRemoveAll_Click(Nothing, Nothing)
    End Sub
    Protected Sub chkService_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkService.CheckedChanged
        FillFeeTypeDLL()
        lbtnClearCompany_Click(sender, e)
    End Sub
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim drRow As GridViewRow = sender.parent.parent()
        'gvStudentDetails.Columns(4).Visible = False
        'gvStudentDetails.SelectedIndex = drRow.RowIndex
        'Dim lblStudID As Label = drRow.FindControl("lblStudID")
        'If Not lblStudID Is Nothing Then
        '    Dim httab As Hashtable = Session("STUD_DET")
        '    Dim vFEE_PERF As FEEPERFORMAINVOICE = httab(CInt(lblStudID.Text))
        '    If Not vFEE_PERF Is Nothing Then

        '        'ddlAddFeeType.DataSource = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), Val(h_Company_ID.Value), chkService.Checked, False, IsTuitionINV)
        '        'ddlAddFeeType.DataTextField = "FEE_DESCR"
        '        'ddlAddFeeType.DataValueField = "FEEID"
        '        'ddlAddFeeType.DataBind()

        '        'txtAddFeeStudDet.Text = vFEE_PERF.FPH_STU_NO & " - " & vFEE_PERF.FPH_STU_NAME
        '        Session("vEditedStudInfo") = vFEE_PERF
        '    End If
        'End If
        'GridBindAdjustment()
        'trEditFeeDetails.Visible = True
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim htFEE_PERF As Hashtable = Nothing
        Try
            Dim lblStudID As New Label
            lblStudID = TryCast(sender.parent.FindControl("lblStudID"), Label)
            Dim lblStud_no As New Label
            lblStud_no = TryCast(sender.parent.FindControl("lblStud_no"), Label)
            htFEE_PERF = DirectCast(Session("FEE_PERF"), Hashtable)
            If Not htFEE_PERF Is Nothing And Not lblStudID Is Nothing Then
                If htFEE_PERF.ContainsKey((lblStudID.Text.Trim)) Then
                    htFEE_PERF.Remove((lblStudID.Text.Trim))
                    h_duplicate.Value = h_duplicate.Value.Replace(lblStud_no.Text & " , ", "")
                End If
            End If
            If htFEE_PERF.Keys.Count <= 1 And chkGroupInvoice.Checked Then
                chkGroupInvoice.Checked = False
                chkGroupInvoice.Enabled = True
            End If
        Catch ex As Exception

        End Try
        Session("FEE_PERF") = htFEE_PERF
        GridBindStudDetails()
        If htFEE_PERF.Keys.Count < 1 Then
            DisableControls(False)
        End If
    End Sub
    Protected Sub rblInvPeriod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblInvPeriod.SelectedIndexChanged
        PopulateTerms_Months()
        lbtnClearCompany_Click(sender, e)
    End Sub

    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        UpdateMonthsSelected()
        'CreateFeeDetailSchema()
        If ViewState("dtMonths") Is Nothing OrElse ViewState("dtMonths").Rows.count <= 0 Then
            If rblInvPeriod.SelectedValue = "T" Then
                '  lblError.Text = "Please select Terms"
                usrMessageBar.ShowNotification("Please select Terms", UserControls_usrMessageBar.WarningType.Danger)
            Else
                ' lblError.Text = "Please select Months(Period)"
                usrMessageBar.ShowNotification("Please select Months(Period)", UserControls_usrMessageBar.WarningType.Danger)
            End If
            Return
        End If
        Dim TOTAL As Double = 0
        Dim STU_TYPE As STUDENTTYPE = IIf(chkEnquiry.Checked, STUDENTTYPE.ENQUIRY, STUDENTTYPE.STUDENT)
        Dim selPD As SELECTEDPROFORMADURATION
        If rblInvPeriod.SelectedValue = "T" Then
            selPD = SELECTEDPROFORMADURATION.TERMS
        Else
            selPD = SELECTEDPROFORMADURATION.MONTHS
        End If
        Dim vFEE_PERF As FEEPERFORMAINVOICE = Nothing
        If Session("FEE_PERF") Is Nothing Then
            Session("FEE_PERF") = New Hashtable
        End If
        Dim htFEE_PERF As Hashtable = Session("FEE_PERF")
        If h_Students.Value <> "" Then
            Dim STU_ID() As String
            h_Students.Value = h_Students.Value.ToString.Replace("||", "@")
            Dim charSeparators As Char() = New Char() {"@"c}
            STU_ID = h_Students.Value.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries)
            If chkGroupInvoice.Checked Then
                For i As Int16 = 0 To STU_ID.Length - 1 Step 1
                    vFEE_PERF = New FEEPERFORMAINVOICE
                    vFEE_PERF.FPH_ACD_ID = ddlAcademicYear.SelectedValue
                    vFEE_PERF.FPH_BSU_ID = Session("sBsuId")
                    vFEE_PERF.FPH_COMP_ID = IIf(h_Company_ID.Value = "", -1, h_Company_ID.Value)
                    vFEE_PERF.FPH_bPrintYearly = False
                    vFEE_PERF.FPH_GRD_ID = h_STU_GRD_ID.Value
                    'vFEE_PERF.FEE_TYPES = arrList
                    vFEE_PERF.FPH_INVOICENO = "1"
                    vFEE_PERF.FPH_STUD_TYPE = STU_TYPE
                    vFEE_PERF.ProformaDuration = selPD
                    Dim GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STU_ID(i), chkNextAcademicYear.Checked, STU_TYPE)
                    Dim objFPI As New FEEPERFORMAINVOICE
                    objFPI.STU_ID = STU_ID(i)
                    objFPI.STU_TYPE = IIf(chkEnquiry.Checked, "E", "S")
                    objFPI.FPH_ACD_ID = ddlAcademicYear.SelectedValue
                    objFPI.FPH_IsNewStudent = chkNewStudent.Checked
                    objFPI.FPH_BSU_ID = Session("sBsuId")
                    objFPI.FPH_COMP_ID = h_Company_ID.Value
                    objFPI.FPH_IsServiceINV = chkService.Checked
                    objFPI.FPH_FEE_TYPES = h_FeeTypes.Value
                    objFPI.FPH_EXL_TYPE = "PI"
                    objFPI.FPH_IsEXCL_TC = IIf(chkExcludeTC.Checked, 1, 0)
                    objFPI.FPH_IsMULTI_SEL = IIf(chkGroupInvoice.Checked, 1, 0)
                    objFPI.GetStudentDetails(chkNextAcademicYear.Checked, IIf(ViewState("SID") = "0", False, True))
                    vFEE_PERF.FPH_STU_ID = objFPI.STU_ID
                    vFEE_PERF.FPH_STU_NAME = objFPI.STU_NAME
                    vFEE_PERF.FPH_STU_NO = objFPI.STU_NO
                    vFEE_PERF.FPH_GRD_ID = GRD_ID
                    If GRD_ID <> "" Then
                        vFEE_PERF.STUDENT_SUBDETAILS = UPDATEFEESPLIUPDETAILS(vFEE_PERF.FPH_STU_ID, vFEE_PERF.FPH_ACD_ID, vFEE_PERF.FPH_GRD_ID, vFEE_PERF.FPH_STUD_TYPE, vFEE_PERF.ProformaDuration, vFEE_PERF.TOTAL_AMOUNT)
                        htFEE_PERF(STU_ID(i)) = vFEE_PERF
                        CheckDuplicate(STU_ID(i))
                    Else
                        '   Me.lblError.Text &= IIf(Me.lblError.Text = "", "", "<br />") & "Grade details not found for student " & vFEE_PERF.FPH_STU_NO & " and excluded from list."
                        usrMessageBar.ShowNotification("Grade details not found for student " & vFEE_PERF.FPH_STU_NO & " and excluded from list.", UserControls_usrMessageBar.WarningType.Danger)
                    End If
                Next
            Else 'for single student
                vFEE_PERF = New FEEPERFORMAINVOICE
                vFEE_PERF.FPH_ACD_ID = ddlAcademicYear.SelectedValue
                vFEE_PERF.FPH_BSU_ID = Session("sBsuId")
                vFEE_PERF.FPH_COMP_ID = IIf(h_Company_ID.Value = "", -1, h_Company_ID.Value)
                vFEE_PERF.FPH_bPrintYearly = False
                If h_STU_GRD_ID.Value = "" Then
                    Dim GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STU_ID(0), chkNextAcademicYear.Checked, STU_TYPE)
                    h_STU_GRD_ID.Value = GRD_ID
                End If
                vFEE_PERF.FPH_GRD_ID = h_STU_GRD_ID.Value
                'vFEE_PERF.FEE_TYPES = arrList
                vFEE_PERF.FPH_INVOICENO = "1"
                vFEE_PERF.FPH_STUD_TYPE = STU_TYPE
                vFEE_PERF.ProformaDuration = selPD
                Dim objFPI As New FEEPERFORMAINVOICE
                objFPI.STU_ID = STU_ID(0)
                objFPI.STU_TYPE = IIf(chkEnquiry.Checked, "E", "S")
                objFPI.FPH_ACD_ID = ddlAcademicYear.SelectedValue
                objFPI.FPH_IsNewStudent = chkNewStudent.Checked
                objFPI.FPH_BSU_ID = Session("sBsuId")
                objFPI.FPH_COMP_ID = h_Company_ID.Value
                objFPI.FPH_IsServiceINV = chkService.Checked
                objFPI.FPH_FEE_TYPES = h_FeeTypes.Value
                objFPI.FPH_EXL_TYPE = "PI"
                objFPI.FPH_IsEXCL_TC = IIf(chkExcludeTC.Checked, 1, 0)
                objFPI.FPH_IsMULTI_SEL = IIf(chkGroupInvoice.Checked, 1, 0)
                objFPI.GetStudentDetails(chkNextAcademicYear.Checked, IIf(ViewState("SID") = "0", False, True))
                vFEE_PERF.FPH_STU_ID = objFPI.STU_ID
                vFEE_PERF.FPH_STU_NAME = objFPI.STU_NAME
                vFEE_PERF.FPH_STU_NO = objFPI.STU_NO
                vFEE_PERF.STUDENT_SUBDETAILS = UPDATEFEESPLIUPDETAILS(vFEE_PERF.FPH_STU_ID, vFEE_PERF.FPH_ACD_ID, vFEE_PERF.FPH_GRD_ID, vFEE_PERF.FPH_STUD_TYPE, vFEE_PERF.ProformaDuration, vFEE_PERF.TOTAL_AMOUNT)
                htFEE_PERF(STU_ID(0)) = vFEE_PERF
                CheckDuplicate(STU_ID(0))
            End If
            If htFEE_PERF.Keys.Count > 0 Then ''disable or enable the controls once student is added to grid, which will block further modifications
                DisableControls(True)
                txtStuNo.Text = ""
                txtStuName.Text = ""
                h_Students.Value = ""
            Else
                DisableControls(False)
            End If
            If htFEE_PERF.Keys.Count > 1 Then
                chkGroupInvoice.Checked = True
                chkGroupInvoice.Enabled = False
            End If
            Session("FEE_PERF") = htFEE_PERF
            GridBindStudDetails()
            h_STU_GRD_ID.Value = ""
        End If
    End Sub
    Protected Sub lbtnRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnRemoveAll.Click
        h_Students.Value = ""
        txtStuNo.Text = ""
        txtStuName.Text = ""
        h_STU_GRD_ID.Value = ""
        h_duplicate.Value = ""
        If chkGroupInvoice.Checked Then
            chkGroupInvoice.Checked = False
        End If
        If Session("FEE_PERF") Is Nothing Then
            Session("FEE_PERF") = New Hashtable
        End If
        DirectCast(Session("FEE_PERF"), Hashtable).Clear()
        GridBindStudDetails()
        Session("FEE_PERF") = Nothing
        DisableControls(False)
    End Sub

    Private Sub DisableControls(ByVal bDisable As Boolean)
        Me.trACD.Disabled = bDisable
        Me.trTerm.Disabled = bDisable
        Me.trCompany.Disabled = bDisable
        imgCompany.Enabled = Not bDisable
        Me.lbtnClearCompany.Enabled = Not bDisable
        Me.trFeeType.Disabled = bDisable
        Me.ddlFeeType.Enabled = Not bDisable
        Me.trFilters.Disabled = bDisable
        'Me.trStudentFilters.Disabled = bDisable
    End Sub

    Public Function CHECK_DUPLICATE_INVOICE(ByVal BSU_ID As String, ByVal ACD_ID As Integer, ByVal STU_ID As Integer, ByVal IS_NEXT_ACY As Boolean) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.Int)
        pParms(2).Value = STU_ID
        pParms(3) = New SqlClient.SqlParameter("@IS_NEXT_ACY", SqlDbType.Bit)
        pParms(3).Value = IS_NEXT_ACY
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[CHECK_DUPLICATE_INVOICE]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Sub CheckDuplicate(ByVal Stud_Id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        'str_Sql = " SELECT STU_NO FROM FEES.FEE_PERFORMAINVOICE_H WITH(NOLOCK) INNER JOIN " _
        '    & " OASIS.dbo.STUDENT_M WITH(NOLOCK) ON FPD_STU_ID = STU_ID " _
        '    & " WHERE FPH_BSU_ID ='" & Session("sBsuid") & "' " _
        '    & " AND FPH_ACD_ID =" & ddlAcademicYear.SelectedValue _
        '    & " AND ISNULL(FPH_bDeleted, 0) <> 1 AND FPD_STU_ID = " & Stud_Id

        'Dim ds As New DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim dt As New DataTable
        dt = CHECK_DUPLICATE_INVOICE(Session("sBsuid"), ddlAcademicYear.SelectedValue, Stud_Id, chkNextAcademicYear.Checked)

        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            h_duplicate.Value += dt.Rows(0)(0).ToString() & " , "
        End If
    End Sub
    Private Sub GridBindStudDetails()
        Dim htFEE_PERF As Hashtable = Nothing
        Dim dtDt As DataTable = CreateFeeDetailSchema()
        Try
            htFEE_PERF = DirectCast(Session("FEE_PERF"), Hashtable)
            For Each vSTD_DET As FEEPERFORMAINVOICE In htFEE_PERF.Values
                If Not vSTD_DET.bDelete Then
                    If vSTD_DET.FPH_GRD_ID = "" Then 'Or vSTD_DET.TOTAL_AMOUNT = 0 Then 'Amount checking commented by Jacob on 21/AUG/2016
                        '  Me.lblError.Text &= IIf(Me.lblError.Text = "", "", "<br />") & "Grade details not found for student " & vSTD_DET.FPH_STU_NO & " and excluded from list."
                        usrMessageBar.ShowNotification("Grade details not found for student " & vSTD_DET.FPH_STU_NO & " and excluded from list.", UserControls_usrMessageBar.WarningType.Danger)
                    Else
                        Dim dr As DataRow = dtDt.NewRow()
                        dr("STU_ID") = vSTD_DET.FPH_STU_ID
                        dr("STU_NO") = vSTD_DET.FPH_STU_NO
                        dr("STU_NAME") = vSTD_DET.FPH_STU_NAME
                        dr("STU_AMOUNT") = Format(vSTD_DET.TOTAL_AMOUNT, "#,##0.00")
                        dr("STU_GRD_ID") = vSTD_DET.FPH_GRD_ID
                        dtDt.Rows.Add(dr)
                    End If
                End If
            Next
        Catch ex As Exception
        Finally
            gvStudentDetails.DataSource = dtDt
            gvStudentDetails.DataBind()
        End Try
    End Sub

    Protected Sub gvStudentDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvStudentDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblStud_no As Label = DirectCast(e.Row.FindControl("lblStud_no"), Label)
                Dim lblStudentName As Label = DirectCast(e.Row.FindControl("lblStudentName"), Label)
                Dim lblFCAmount As Label = DirectCast(e.Row.FindControl("lblFCAmount"), Label)
                Dim lnkEdit As LinkButton = DirectCast(e.Row.FindControl("lnkEdit"), LinkButton)
                Dim lblAmount As LinkButton = DirectCast(e.Row.FindControl("lblAmount"), LinkButton)
                Dim STU_ID As Int64 = gvStudentDetails.DataKeys(e.Row.RowIndex).Values("STU_ID")
                lnkEdit.Attributes.Add("onclick", "return ShowEditFee('" & lblStud_no.Text & " - " & lblStudentName.Text & "','" & STU_ID & "');")
                If bMultiCurrency AndAlso Not lblFCAmount Is Nothing AndAlso Not lblAmount Is Nothing Then
                    lblFCAmount.Text = Format(FeeCollection.GetDoubleVal(lblAmount.Text) / ExgRate, "#,##0.00")

                End If
                'lblStud_no.Attributes.Add("onClick", "return ShowStudentData('" & STU_ID & "','" & Session("sBsuId") & "','" & lblStud_no.Text & " - " & lblStudentName.Text & "');")
                '../StudentServices/StudPro_detailsNew.aspx?id=' + id + '&bsuID=' + bsuid + ''
            End If
            'If e.Row.RowType = DataControlRowType.Header Then
            '    If bMultiCurrency Then

            '    End If
            'End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub btnFeeAmtCancel_Click(sender As Object, e As EventArgs)
        divStuEdit.Style("display") = "none"
        h_stu_selected4edit.Value = ""
    End Sub
    Protected Sub btnFeeAddAmt_Click(sender As Object, e As EventArgs)
        If gvAddFeeDet.Rows.Count <= 0 Then
            CreateAddFeeTypeSchema()
        End If
        Dim dtaddfee As DataTable = DirectCast(ViewState("gvAddFeeDet"), DataTable)
        Dim drRows() As DataRow
        drRows = dtaddfee.Select("FEE_ID = " & ddlAddFeeType.SelectedValue & "")
        If Not drRows Is Nothing AndAlso drRows.Length > 0 Then
            lblAddFeeError.Text = "Fee type already exists, remove it from grid and try again"
            Return
        End If
        Dim dr As DataRow = dtaddfee.NewRow
        dr("FEE_ID") = ddlAddFeeType.SelectedValue
        dr("FEE_TYPE") = ddlAddFeeType.SelectedItem.Text
        dr("FEE_ADJ_REMARKS") = txtAddFeeRemarks.Text.Trim
        dr("FEE_ADJ_AMT") = Convert.ToDouble(txtAddFeeAmount.Text)
        dtaddfee.Rows.Add(dr)
        dtaddfee.AcceptChanges()
        ViewState("gvAddFeeDet") = dtaddfee
        divStuEdit.Style("display") = "inline"
        GridBindgvAddFeeDet()
        ddlAddFeeType.SelectedIndex = 0
        txtAddFeeAmount.Text = "0.00"
        txtAddFeeRemarks.Text = ""
        lblStudent.Text = h_stu_selected4edit.Value.Split("|")(0)
    End Sub
    Private Sub ClearAddFeeGrid()
        ddlAddFeeType.SelectedIndex = 0
        txtAddFeeAmount.Text = "0.00"
        txtAddFeeRemarks.Text = ""
        lblStudent.Text = ""
        lblAddFeeError.Text = ""
        h_stu_selected4edit.Value = ""
        chkApplyToAll.Checked = False
        divStuEdit.Style("display") = "none"
        ViewState("gvAddFeeDet") = Nothing
        GridBindgvAddFeeDet()
    End Sub
    Private Sub GridBindgvAddFeeDet()
        gvAddFeeDet.DataSource = ViewState("gvAddFeeDet")
        gvAddFeeDet.DataBind()
    End Sub
    Private Sub CreateAddFeeTypeSchema()
        Dim dtAddFee As New DataTable
        Try
            Dim dcFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.Int32"))
            Dim dcFEE_TYPE As New DataColumn("FEE_TYPE", System.Type.GetType("System.String"))
            Dim dcFEE_ADJ_REMARKS As New DataColumn("FEE_ADJ_REMARKS", System.Type.GetType("System.String"))
            Dim dcFEE_ADJ_AMT As New DataColumn("FEE_ADJ_AMT", System.Type.GetType("System.Double"))

            dtAddFee.Columns.Add(dcFEE_ID)
            dtAddFee.Columns.Add(dcFEE_TYPE)
            dtAddFee.Columns.Add(dcFEE_ADJ_REMARKS)
            dtAddFee.Columns.Add(dcFEE_ADJ_AMT)
        Catch ex As Exception
            dtAddFee = Nothing
        Finally
            ViewState("gvAddFeeDet") = dtAddFee
        End Try
    End Sub
    Protected Sub lnkDeleteFEEADDADJ_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFEE_ID As Label = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            Dim dt As DataTable = DirectCast(ViewState("gvAddFeeDet"), DataTable)
            For Each dr As DataRow In dt.Rows
                If dr("FEE_ID").ToString = lblFEE_ID.Text Then
                    dt.Rows.Remove(dr)
                    dt.AcceptChanges()
                    Exit For
                End If
            Next
            ViewState("gvAddFeeDet") = dt
            GridBindgvAddFeeDet()
        End If
    End Sub

    Protected Sub btnSaveAddFee_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim htFEE_PERF As Hashtable = Nothing
        lblAddFeeError.Text = ""
        Try
            If ViewState("gvAddFeeDet") Is Nothing OrElse DirectCast(ViewState("gvAddFeeDet"), DataTable).Rows.Count <= 0 Then
                lblAddFeeError.Text = "Please add atleast one fee type and amount to be updated"
                Exit Sub
            End If
            If Not chkApplyToAll.Checked Then
                Dim STU_ID As String = h_stu_selected4edit.Value.Split("|")(1)
                htFEE_PERF = DirectCast(Session("FEE_PERF"), Hashtable)
                If Not htFEE_PERF Is Nothing And STU_ID <> "" Then
                    If htFEE_PERF.ContainsKey(STU_ID) Then
                        Dim vFEE_PERF As FEEPERFORMAINVOICE = htFEE_PERF(STU_ID)
                        If Not vFEE_PERF Is Nothing Then
                            For Each dr As DataRow In DirectCast(ViewState("gvAddFeeDet"), DataTable).Rows
                                Dim FeeID As Integer = dr("FEE_ID").ToString
                                Dim FeeType As String = dr("FEE_TYPE").ToString
                                Dim Amount As Double = Convert.ToDouble(dr("FEE_ADJ_AMT"))
                                Dim Comments As String = dr("FEE_ADJ_REMARKS").ToString
                                Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = vFEE_PERF.GetSubDetails(FeeID)
                                If FEE_SUB_DET IsNot Nothing Then
                                    Dim oldAmt As Double
                                    oldAmt = FEE_SUB_DET.FPD_ADJ_AMOUNT
                                    FEE_SUB_DET.FPD_ADJ_AMOUNT = Amount
                                    FEE_SUB_DET.FPD_ADJ_REMARKS = Comments
                                    vFEE_PERF.TOTAL_AMOUNT += Amount - oldAmt
                                    FEE_SUB_DET.FEE_ApplyToAll = chkApplyToAll.Checked
                                Else
                                    FEE_SUB_DET = New FEEPERFORMANCEREVIEW_SUB
                                    FEE_SUB_DET.SORT_DATE = Now.Date
                                    FEE_SUB_DET.FPD_FEE_ID = FeeID
                                    FEE_SUB_DET.FPD_FEE_DESCR = FeeType
                                    FEE_SUB_DET.FPD_ADJ_AMOUNT = Amount
                                    FEE_SUB_DET.FPD_ADJ_REMARKS = Comments
                                    FEE_SUB_DET.FEE_ApplyToAll = chkApplyToAll.Checked
                                    FEE_SUB_DET.FPD_EXG_RATE = ExgRate 'Added by Jacob on 24/Jul/2019 for multicurrency
                                    vFEE_PERF.TOTAL_AMOUNT += Amount

                                    vFEE_PERF.STUDENT_SUBDETAILS.Add(FEE_SUB_DET)
                                End If
                            Next
                            htFEE_PERF(STU_ID) = vFEE_PERF
                            Session("FEE_PERF") = htFEE_PERF
                            GridBindStudDetails()
                            ClearAddFeeGrid()
                        End If
                    End If
                End If
            Else 'Since Apply to all students is checked, the amount update will apply to all students.
                htFEE_PERF = DirectCast(Session("FEE_PERF"), Hashtable)
                If Not htFEE_PERF Is Nothing Then
                    For Each vFee_PERF As FEEPERFORMAINVOICE In htFEE_PERF.Values
                        For Each dr As DataRow In DirectCast(ViewState("gvAddFeeDet"), DataTable).Rows
                            Dim FeeID As Integer = dr("FEE_ID").ToString
                            Dim FeeType As String = dr("FEE_TYPE").ToString
                            Dim Amount As Double = Convert.ToDouble(dr("FEE_ADJ_AMT"))
                            Dim Comments As String = dr("FEE_ADJ_REMARKS").ToString
                            Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = vFee_PERF.GetSubDetails(FeeID)
                            If FEE_SUB_DET IsNot Nothing Then
                                Dim oldAmt As Double
                                oldAmt = FEE_SUB_DET.FPD_ADJ_AMOUNT
                                FEE_SUB_DET.FPD_ADJ_AMOUNT = Amount
                                FEE_SUB_DET.FPD_ADJ_REMARKS = Comments
                                vFee_PERF.TOTAL_AMOUNT += Amount - oldAmt
                                FEE_SUB_DET.FEE_ApplyToAll = chkApplyToAll.Checked
                            Else
                                FEE_SUB_DET = New FEEPERFORMANCEREVIEW_SUB
                                FEE_SUB_DET.SORT_DATE = Now.Date
                                FEE_SUB_DET.FPD_FEE_ID = FeeID
                                FEE_SUB_DET.FPD_FEE_DESCR = FeeType
                                FEE_SUB_DET.FPD_ADJ_AMOUNT = Amount
                                FEE_SUB_DET.FPD_ADJ_REMARKS = Comments
                                FEE_SUB_DET.FEE_ApplyToAll = chkApplyToAll.Checked
                                FEE_SUB_DET.FPD_EXG_RATE = ExgRate 'Added by Jacob on 24/Jul/2019 for multicurrency
                                vFee_PERF.TOTAL_AMOUNT += Amount
                                vFee_PERF.STUDENT_SUBDETAILS.Add(FEE_SUB_DET)
                            End If
                        Next
                    Next
                    Session("FEE_PERF") = htFEE_PERF
                    GridBindStudDetails()
                    ClearAddFeeGrid()
                End If
            End If
        Catch ex As Exception

        End Try


    End Sub
    Protected Sub btnSaveAddFeeCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearAddFeeGrid()
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If ViewState("vInvNo").ToString <> "" Then
            If h_Company_ID.Value <> "-1" Then
                Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & ViewState("vInvNo").ToString & "'", True, Session("sBsuid"), chkEnquiry.Checked, Session("sUsr_name"))
            Else
                Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & ViewState("vInvNo").ToString & "'", False, Session("sBsuid"), chkEnquiry.Checked, Session("sUsr_name"))
            End If
            FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), ViewState("vInvNo").ToString, Session("sUsr_name"))

            h_print.Value = "print"
        Else
            ' lblError.Text = "Please Select the Invoice!!!!"
            usrMessageBar.ShowNotification("Please Select the Invoice!!!!", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Protected Function PrintReceiptX(ByVal vINV_NOs As ArrayList, ByVal bCompany As Boolean) As MyReportClass
        Dim ienum As IEnumerator = vINV_NOs.GetEnumerator
        Dim str_invnos As String = String.Empty
        Dim str_invnos_audit As String = String.Empty
        Dim comma As String = String.Empty
        Dim pipe As String = String.Empty
        Dim repSource As New MyReportClass
        While (ienum.MoveNext())
            str_invnos += comma & " '" & ienum.Current & "'"
            comma = ", "
            str_invnos_audit += pipe & ienum.Current
            pipe = "|"
        End While

        FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), str_invnos_audit, Session("sUsr_name"))

        repSource = FEEPERFORMAINVOICE.PrintReceipt(str_invnos, bCompany, Session("sBsuid"), chkEnquiry.Checked, Session("sUsr_name"))
        h_print.Value = "print"
        Return repSource
    End Function
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If ValidateSave() Then
            Dim htStudDetails As Hashtable = Session("FEE_PERF")
            Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction("FEE_PERFORMA")
            Dim FPH_DT As DateTime = CDate(txtDate.Text)
            Dim DTSelectedDates As DataTable = ViewState("dtMonths")
            Dim arrInvNos As New ArrayList
            Dim ReGenerate As Boolean = False
            Dim FeeLists As String = GetSelectedFeeTypes()
            Dim Hprint As Boolean = False
            Dim Currency As String = Session("BSU_CURRENCY")
            If bMultiCurrency Then
                Currency = ddCurrency.SelectedItem.Text
            End If
            Dim retVal As Integer = FEEPERFORMAINVOICE.SaveDetails(ViewState("vInvNo"), chkAnually.Checked, txtInvoiceCaption.Text, htStudDetails, DTSelectedDates, FPH_DT, Session("sBSUID"), ddlAcademicYear.SelectedValue, Session("sUsr_name"), conn, trans, arrInvNos, ReGenerate, txtRemarks.Text, chkService.Checked, chkNewStudent.Checked, chkAccStatus.Checked, Currency, ExgRate, chkExcludePaidAdvance.Checked, "", FeeLists, chkNextAcademicYear.Checked, IsTuitionINV, chkXcludeDue.Checked)

            If retVal <> 0 Then
                trans.Rollback()
                '   lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                If ViewState("datamode") = "view" Then
                    ' lblError.Text = "Invoice updated Successfully"
                    usrMessageBar.ShowNotification("Invoice updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                Else
                    'lblError.Text = "Invoice created Successfully"
                    usrMessageBar.ShowNotification("Invoice created Successfully", UserControls_usrMessageBar.WarningType.Success)
                End If
                ViewState("datamode") = "none"
                If arrInvNos.Count > 0 Then
                    If ViewState("vInvNo") = "" Then
                        ViewState("vInvNo") = arrInvNos(0)
                    End If
                End If
                If chkPrintReciept.Checked Then
                    Dim bComp As Boolean = False
                    If h_Company_ID.Value <> "" And h_Company_ID.Value <> "-1" Then
                        bComp = True
                    End If
                    If arrInvNos.Count > 0 Then
                        'ViewState("vInvNo") = arrInvNos(0)
                        Session("ReportSource") = PrintReceiptX(arrInvNos, bComp)
                        If h_print.Value = "print" Then
                            Hprint = True
                        End If
                    End If
                End If
                '  ClearForm()
                If Hprint = True Then
                    h_print.Value = "print"
                End If
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("PERFORMA", ViewState("vInvNo"), str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If
        End If
    End Sub
    Private Function ValidateSave() As Boolean
        ValidateSave = False
        If Not Master.IsSessionMatchesForSave() Then
            ' lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        If Not IsDate(txtDate.Text) Then
            '  lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        If Session("FEE_PERF") Is Nothing OrElse DirectCast(Session("FEE_PERF"), Hashtable).Keys.Count <= 0 Then
            ' lblError.Text = "Student details not found!"
            usrMessageBar.ShowNotification("Student details not found!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        ValidateSave = True
    End Function
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Then
            Call ClearForm()
            ViewState("vInvNo") = ""
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ElseIf ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Not Session("FEE_PERF") Is Nothing Then
            Dim httab As Hashtable = Session("FEE_PERF")
            Dim ienum As IDictionaryEnumerator = httab.GetEnumerator
            While (ienum.MoveNext())
                Dim FEE_DET As FEEPERFORMAINVOICE = ienum.Value
                FEE_DET.bEdit = True
                FEE_DET.bDelete = True
                Dim arrList As ArrayList = FEE_DET.STUDENT_SUBDETAILS
                For Each vFEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB In FEE_DET.STUDENT_SUBDETAILS
                    vFEE_SUB_DET.bEdit = True
                    vFEE_SUB_DET.bDelete = True
                Next
            End While
            Session("FEE_PERF") = httab
        End If
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        ClearForm()
        gvStudentDetails.DataSource = ""
        gvStudentDetails.DataBind()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            ddCurrency.Items.Clear()
            ddCurrency.DataSource = MasterFunctions.GetExchangeRates(txtDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            ddCurrency.DataTextField = "EXG_CUR_ID"

            ddCurrency.DataValueField = "RATES"
            ddCurrency.DataBind()
            If ddCurrency.Items.Count > 0 Then
                Dim lstDrp As New ListItem
                lstDrp = ddCurrency.Items.FindByText(Session("BSU_CURRENCY"))
                If Not lstDrp Is Nothing Then
                    ddCurrency.SelectedValue = lstDrp.Value
                End If
                lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCurrency.SelectedIndexChanged
        lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
        ExgRate = lblExgRate.Text
        gvStudentDetails.Columns(6).HeaderText = "Amount(" & ddCurrency.SelectedItem.Text & ")"
        'gvFeeCollection.Columns(13).HeaderText = "Paying Now (" & ddCurrency.SelectedItem.Text & ")"
        'Gridbind_Feedetails()
        'If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") Then
        '    gvFeeCollection.Columns(13).Visible = True
        '    lblOutstandingFC.Visible = True
        '    lblDueFC.Visible = True
        'Else
        '    gvFeeCollection.Columns(13).Visible = False
        '    lblOutstandingFC.Visible = False
        '    lblDueFC.Visible = False
        'End If
    End Sub

    Protected Sub txtDate_TextChanged(sender As Object, e As EventArgs) Handles txtDate.TextChanged
        If bMultiCurrency Then
            bind_Currency()
        End If
    End Sub
End Class
