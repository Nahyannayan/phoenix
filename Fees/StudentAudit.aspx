﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentAudit.aspx.vb" Inherits="Fees_StudentAudit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" />
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script lang="javascript" type="text/javascript">
        function PrintReceipt() {
            document.getElementById('tr_Print').style.display = 'none';
            window.print();
            document.getElementById('tr_Print').style.display = 'inline';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%">
            <tr valign="top" id="tr_Print">
                <td align="right" colspan="2">
                    <img src="../Images/print.gif" onclick="PrintReceipt();" style="cursor: pointer" /></td>
            </tr>
            <tr valign="top">
                <td colspan="2" align="center" style="border-bottom: #6a923a 2pt solid">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" rowspan="4" width="10%">
                                <asp:Image ID="imgLogo" runat="server" Height="70px" />
                            </td>
                            <td align="right" width="60%"></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblHeader" runat="server" Style="font-weight: bold; text-align: center;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="60%"></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lbldate" runat="server" Style="font-weight: bold;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="60%"></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblusername" runat="server" Style="font-weight: bold;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" width="60%">
                                <asp:Label ID="lblSchool" runat="server" Style="font-weight: bold;"></asp:Label>
                            </td>
                            <td align="left" width="30%"></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td height="20px" colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td style="vertical-align: top; width: 50%">
                    <asp:DataList ID="dtlStudentInfo" runat="server" RepeatDirection="Vertical" ShowHeader="true" Width="100%">
                        <ItemTemplate>
                            <asp:Label ID="lblLHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LHS")%>'></asp:Label>&nbsp;
                            <asp:Label ID="lblRHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RHS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
                <td style="vertical-align: top; width: 50%">
                    <asp:DataList ID="dtlStudentServiceInfo" runat="server" RepeatDirection="Vertical" ShowHeader="true" Width="100%">
                        <ItemTemplate>
                            <asp:Label ID="lblLHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LHS")%>'></asp:Label>&nbsp;
                            <asp:Label ID="lblRHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RHS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:DataList></td>
            </tr>
            <tr>
                <td colspan="2" class="title-bg">TC Records</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvTCRecords" runat="server" Width="100%" EmptyDataText="No records to show" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="TCM_TCSO" HeaderText="Status" />
                            <asp:BoundField DataField="RefNo" HeaderText="TC RefNo" />
                            <asp:BoundField DataField="TCM_LEAVEDATE" HeaderText="Leave Date" />
                            <asp:BoundField DataField="TCM_LASTATTDATE" HeaderText="Last Date of Attendance" />
                            <asp:BoundField DataField="TCT_DESCR" HeaderText="TC Type" />
                            <asp:BoundField DataField="Cleared" HeaderText="Cleared" />
                            <asp:BoundField DataField="Approved" HeaderText="Approved" />
                            <asp:BoundField DataField="Cancelled" HeaderText="Cancelled" />
                            <asp:BoundField DataField="ApprovalDate" HeaderText="Approved On" />
                            <asp:BoundField DataField="CancelDate" HeaderText="Cancelled On" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="title-bg">TC Transactions Audit</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvTCTransAudit" runat="server" Width="100%" EmptyDataText="No audit data to show" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="RefNo" HeaderText="Ref.No" />
                            <asp:BoundField DataField="TCM_ADD_DATE" HeaderText="Transaction Date" />
                            <asp:BoundField DataField="TCM_ADD_USER" HeaderText="User" />
                            <asp:BoundField DataField="TCM_LEAVEDATE" HeaderText="Leave Date" />
                            <asp:BoundField DataField="TCM_LASTATTDATE" HeaderText="LDA" />
                            <asp:BoundField DataField="TCT_DESCR" HeaderText="TC Type" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="title-bg">Critical Data Changes</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvCriticalChanges" runat="server" Width="100%" EmptyDataText="No audit data to show" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="STU_TRANSACTIONDATE" HeaderText="Transaction Date" DataFormatString="{0: dd/MMM/yyyy hh:mm:ss }" />
                            <asp:BoundField DataField="STU_USER" HeaderText="User" />
                            <asp:BoundField DataField="CLM_DESCR" HeaderText="Curriculum" />
                            <asp:BoundField DataField="ACY_DESCR" HeaderText="Academic Year" />
                            <asp:BoundField DataField="GRM_DISPLAY" HeaderText="Grade" />
                            <asp:BoundField DataField="SCT_DESCR" HeaderText="Section" />
                            <asp:BoundField DataField="STM_DESCR" HeaderText="Stream" />
                            <asp:BoundField DataField="STU_DOJ" HeaderText="DOJ" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="title-bg">Service Transactions Audit</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvServiceAudit" runat="server" Width="100%" EmptyDataText="No audit data to show" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ACY_DESCR" HeaderText="Academic Year" />
                            <asp:BoundField DataField="SVC_DESCRIPTION" HeaderText="Service" />
                            <asp:BoundField DataField="SSV_FROMDATE" HeaderText="From" />
                            <asp:BoundField DataField="SSV_TODATE" HeaderText="To" />
                            <asp:BoundField DataField="SSR_REQUESTED_TRANDT" HeaderText="Requested On" DataFormatString="{0: dd/MMM/yyyy hh:mm:ss }" />
                            <asp:BoundField DataField="SSR_APPROV_TRANDT" HeaderText="Req.Approved On" DataFormatString="{0: dd/MMM/yyyy hh:mm:ss }" />
                            <asp:BoundField DataField="SSR_APPR_USR" HeaderText="Req.Approved by" />
                            <asp:BoundField DataField="SSD_REQUESTED_TRANDT" HeaderText="Discontinue Req.On" DataFormatString="{0: dd/MMM/yyyy }" />
                            <asp:BoundField DataField="SSD_APPROV_TRANDT" HeaderText="Discontinue Req.Apprd On" DataFormatString="{0: dd/MMM/yyyy hh:mm:ss }" />
                            <asp:BoundField DataField="SSD_APPR_USR" HeaderText="Discontinue Apprd by" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="table-row">End Of Report</td>
            </tr>
        </table>
    </form>
</body>
</html>
