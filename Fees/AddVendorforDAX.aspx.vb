﻿Imports Telerik.Web.UI
Imports System
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports Microsoft.ApplicationBlocks.Data
Partial Class Fees_AddVendorforDAX
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F733043") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Clear()
            BindBusinessUnits()
            If Not Request.QueryString("ID") Is Nothing Then
                ViewState("VDR_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                Dim objvendor As New clsAddVendor
                objvendor.VDR_ID = ViewState("VDR_ID")
                If objvendor.GET_VENDOR_DETAILSBYID() Then
                    ddlBSU.SelectedValue = objvendor.VDB_BSU_ID
                    ddlWorkUnit.SelectedValue = objvendor.EMP_BSU_ID
                    txtEmployee.Text = objvendor.VDR_REF_NO + " - " + objvendor.EMP_NAME
                    h_EMP_ID.Value = objvendor.EMP_ID
                    txtVendorCode.Text = objvendor.VDR_CODE
                    lbtnLoadEmpDetail_Click(Nothing, Nothing)
                    If lblSubHeading.Text = "" Then
                        trheading.Visible = False
                    Else
                        trheading.Visible = True
                    End If
                    'lblSubHeading.Text = "Vendor Code(s) for " & objvendor.VDR_REF_NO

                    'Dim dt As DataTable = objvendor.GET_VENDOR_CODES_FOR_EMPLOYEE()
                    'If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    '    gvVendor.DataSource = dt
                    '    gvVendor.DataBind()
                    'End If
                End If
            End If
        End If
    End Sub

    Private Sub Clear()
        ViewState("VDR_ID") = 0
        ddlBSU.SelectedIndex = -1
        ddlBSU.SelectedValue = Session("sBsuid")
        ddlWorkUnit.SelectedIndex = -1
        ddlWorkUnit.SelectedValue = Session("sBsuid")

        txtEmployee.Text = ""
        h_EMP_ID.Value = "0"
        txtVendorCode.Text = ""
        lblSubHeading.Text = ""
        trheading.Visible = False
        lblEmpDesig.Text = ""
        lblEmpNo.Text = ""
        lblEmpName.Text = ""
        lblDept.Text = ""
        lblWorkingUnit.Text = ""
        gvVendor.DataSource = Nothing
        gvVendor.DataBind()
    End Sub
    Private Sub BindBusinessUnits()
        Try
            Dim dsBsu As DataSet = clsAddVendor.GetBusinessUnits(Session("sUsr_name"), True)
            If Not dsBsu Is Nothing AndAlso dsBsu.Tables(0).Rows.Count > 0 Then
                ddlBSU.DataSource = dsBsu
                ddlBSU.DataTextField = "BSU_NAMEwithShort"
                ddlBSU.DataValueField = "BSU_ID"
                ddlBSU.DataBind()
                ddlBSU.SelectedIndex = -1
                ddlBSU.SelectedValue = Session("sBsuid")
                ''----Working Unit
                ddlWorkUnit.DataSource = dsBsu
                ddlWorkUnit.DataTextField = "BSU_NAMEwithShort"
                ddlWorkUnit.DataValueField = "BSU_ID"
                ddlWorkUnit.DataBind()
                ddlWorkUnit.SelectedIndex = -1
                ddlWorkUnit.SelectedValue = Session("sBsuid")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("BindBusinessUnits()-" & ex.Message, ViewState("MainMnu_code"))
        End Try
    End Sub


    <WebMethod()> _
    Public Shared Function GetEmployee(ByVal BSUID As String, ByVal prefix As String) As String()

        Return clsAddVendor.GetEmployee(BSUID, prefix)

    End Function

    Protected Sub ddlWorkUnit_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlWorkUnit.SelectedIndexChanged
        txtEmployee.Text = ""
        h_EMP_ID.Value = "0"
        txtVendorCode.Text = ""
        lblSubHeading.Text = ""
        lblEmpDesig.Text = ""
        lblEmpNo.Text = ""
        lblEmpName.Text = ""
        lblDept.Text = ""
        lblWorkingUnit.Text = ""
        imgEmpImage.ImageUrl = "#"
        gvVendor.DataSource = Nothing
        gvVendor.DataBind()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        lblError.Text = ""
        If h_EMP_ID.Value <> "" AndAlso CInt(h_EMP_ID.Value) > 0 Then
            If txtVendorCode.Text.Trim <> "" Then
                Dim objcls As New clsAddVendor
                objcls.VDB_BSU_ID = ddlBSU.SelectedValue
                objcls.VDR_REF_NO = lblEmpNo.Text
                objcls.EMP_NAME = lblEmpName.Text
                objcls.VDR_CODE = txtVendorCode.Text.Trim
                objcls.BSU_SHORTNAME = clsAddVendor.GET_BSU_SHORT_NAME(ddlWorkUnit.SelectedValue)
                objcls.EMP_ID = h_EMP_ID.Value
                objcls.VDR_ID = ViewState("VDR_ID")
                If objcls.SAVE_VENDOR(lblError.Text) Then
                    'lblError.Text = "Vendor Code added successfully."
                    usrMessageBar.ShowNotification("Vendor Code added successfully.", UserControls_usrMessageBar.WarningType.Success)
                    txtVendorCode.Text = ""
                    ViewState("VDR_ID") = objcls.VDR_ID 'new VDR_ID
                    Dim dt As DataTable = objcls.GET_VENDOR_CODES_FOR_EMPLOYEE()
                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        gvVendor.DataSource = dt
                        gvVendor.DataBind()
                    End If
                End If
            Else
                'lblError.Text = "Please enter the Vendor Code"
                usrMessageBar.ShowNotification("Please enter the Vendor Code", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        Else
            'lblError.Text = "Please select the Employee"
            usrMessageBar.ShowNotification("Please select the Employee", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
    End Sub

    Protected Sub lbtnLoadEmpDetail_Click(sender As Object, e As EventArgs)
        Dim objcls As New clsAddVendor
        objcls.EMP_BSU_ID = ddlWorkUnit.SelectedValue
        objcls.EMP_ID = h_EMP_ID.Value
        If objcls.GET_EMPLOYEE_DETAILS() Then
            lblEmpDesig.Text = objcls.DESIGNATION
            lblEmpNo.Text = objcls.EMP_NO
            lblEmpName.Text = objcls.EMP_NAME
            lblDept.Text = objcls.DEPARTMENT
            lblWorkingUnit.Text = objcls.WORKING_UNIT
            txtEmployee.Text = objcls.EMP_NO & " - " & objcls.EMP_NAME
            imgEmpImage.ImageUrl = objcls.EMP_IMAGE_URL
            lblSubHeading.Text = "Vendor code(s) for Employee Id " & lblEmpNo.Text
            Dim dt As DataTable = objcls.GET_VENDOR_CODES_FOR_EMPLOYEE()
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                gvVendor.DataSource = dt
                gvVendor.DataBind()
            End If
        End If

    End Sub

    Protected Sub lbtnClearEmployee_Click(sender As Object, e As EventArgs) Handles lbtnClearEmployee.Click
        ddlWorkUnit_SelectedIndexChanged(sender, Nothing)
    End Sub

    Protected Sub imgEmployee_Click(sender As Object, e As ImageClickEventArgs) Handles imgEmployee.Click
        lbtnLoadEmpDetail_Click(sender, Nothing)
    End Sub

    Protected Sub gvVendor_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvVendor.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbtnDelete As LinkButton = DirectCast(e.Row.FindControl("lbtnDelete"), LinkButton)
            Dim rowVDR_ID As Integer = DirectCast(gvVendor.DataKeys(e.Row.RowIndex).Values("VDR_ID"), Integer)
            If Not lbtnDelete Is Nothing Then
                If rowVDR_ID = ViewState("VDR_ID") Then
                    lbtnDelete.Visible = True
                Else
                    lbtnDelete.Visible = True
                End If
            End If
        End If

    End Sub

    Protected Sub lbtnDelete_Click(sender As Object, e As EventArgs)
        Dim VDR_ID As Integer = DirectCast(sender, LinkButton).CommandArgument
        Dim gvRow As GridViewRow = DirectCast(DirectCast(sender, LinkButton).NamingContainer, GridViewRow)
        Dim lblgvBSU_SHORT As Label = DirectCast(gvRow.FindControl("lblgvBSU_SHORT"), Label)
        Dim lblgvVDR_CODE As Label = DirectCast(gvRow.FindControl("lblgvVDR_CODE"), Label)
        Dim objcls As New clsAddVendor
        objcls.VDR_REF_NO = lblEmpNo.Text
        objcls.EMP_NAME = lblEmpName.Text
        objcls.VDR_CODE = lblgvVDR_CODE.Text
        objcls.BSU_SHORTNAME = lblgvBSU_SHORT.Text
        objcls.User = Session("sUsr_name")
        objcls.VDR_ID = ViewState("VDR_ID")
        objcls.bDELETE = True
        ''Delete the vendor code just created
        If objcls.SAVE_VENDOR(lblError.Text) Then
            'lblError.Text = "Vendor Code deleted successfully."
            usrMessageBar.ShowNotification("Vendor Code deleted successfully.", UserControls_usrMessageBar.WarningType.Danger)
            txtVendorCode.Text = ""
            ViewState("VDR_ID") = objcls.VDR_ID 'new VDR_ID
            Dim dt As DataTable = objcls.GET_VENDOR_CODES_FOR_EMPLOYEE()
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                gvVendor.DataSource = dt
                gvVendor.DataBind()
            End If
        End If
    End Sub

    Protected Sub txtEmployee_TextChanged(sender As Object, e As EventArgs)
        Dim objcls As New clsAddVendor
        objcls.EMP_BSU_ID = ddlWorkUnit.SelectedValue
        objcls.EMP_ID = h_EMP_ID.Value
        If objcls.GET_EMPLOYEE_DETAILS() Then
            lblEmpDesig.Text = objcls.DESIGNATION
            lblEmpNo.Text = objcls.EMP_NO
            lblEmpName.Text = objcls.EMP_NAME
            lblDept.Text = objcls.DEPARTMENT
            lblWorkingUnit.Text = objcls.WORKING_UNIT
            txtEmployee.Text = objcls.EMP_NO & " - " & objcls.EMP_NAME
            imgEmpImage.ImageUrl = objcls.EMP_IMAGE_URL
            lblSubHeading.Text = "Vendor code(s) for Employee Id " & lblEmpNo.Text
            Dim dt As DataTable = objcls.GET_VENDOR_CODES_FOR_EMPLOYEE()
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                gvVendor.DataSource = dt
                gvVendor.DataBind()
            End If
        End If
    End Sub
End Class
