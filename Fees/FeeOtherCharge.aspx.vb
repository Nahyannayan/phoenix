Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb

Partial Class Fees_FeeOtherCharge
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass
    Dim bShow As Boolean = False
    Private Property ACD_ID() As Integer
        Get
            Return ViewState("ACD_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("ACD_ID") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager

        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            'ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBSuid")
            Dim USR_NAME As String = Session("sUsr_name")
            Dim _dt As New DataTable
            Session("ChkFees") = _dt
            InitialiseCompnents()

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300136" And ViewState("MainMnu_code") <> "F351080") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("datamode") = "view" Then
                    Dim viewid As String = ""
                    viewid = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    bindData(viewid)
                End If

                If ViewState("MainMnu_code") = "F351080" Then
                    'btnEdit.Visible = False
                End If
            End If
            'gvStudentDetails.DataBind()
        End If
    End Sub

    Sub bindData(ByVal viewid As String)
        Dim SqlQuery As String = ""
        Dim SqlQuery1 As String = ""
        Dim FOH_SDL_ID As String = ""
        SqlQuery = "SELECT ISNULL(FOH_FEE_ID,0)FOH_FEE_ID,FOH_ACD_ID,ISNULL(FOH_TAX_CODE,'NA')FOH_TAX_CODE," & _
            "FOH_DATE,ISNULL(FOH_AMOUNT,0)FOH_AMOUNT,ISNULL(FOH_NARRATION,'')FOH_NARRATION,ISNULL(FOH_Bposted,0)FOH_Bposted, " & _
            "ISNULL(FOH_bNextAcademicYear,0)FOH_bNextAcademicYear, ISNULL(FOH_SDL_ID, '') FOH_SDL_ID, ISNULL(FOH_ISINCLUSIVE_TAX, 0) FOH_ISINCLUSIVE_TAX FROM FEES.FEEOTHCHARGE_H WITH(NOLOCK) WHERE FOH_ID = " & viewid

        Dim _table As DataTable = MainObj.getRecords(SqlQuery, "OASIS_FEESConnectionString")

        If _table.Rows.Count > 0 Then
            h_SelectId.Value = viewid
            ddFeetype.SelectedValue = _table.Rows(0)("FOH_FEE_ID")
            ddlAcademicYear.SelectedValue = _table.Rows(0)("FOH_ACD_ID")
            ddlTAX.SelectedValue = _table.Rows(0)("FOH_TAX_CODE")
            ACD_ID = ddlAcademicYear.SelectedValue
            txtFrom.Text = Convert.ToDateTime(_table.Rows(0).Item("FOH_DATE")).ToString("dd/MMM/yyyy")
            txtTotalAmount.Text = _table.Rows(0).Item("FOH_AMOUNT")
            txtRemarks.Text = _table.Rows(0).Item("FOH_NARRATION")
            h_Posted.Value = _table.Rows(0).Item("FOH_Bposted")

            If _table.Rows(0)("FOH_ISINCLUSIVE_TAX") = True Then
                rblTaxCalculation.SelectedValue = "I"
            Else
                rblTaxCalculation.SelectedValue = "E"
            End If


            If CBool(_table.Rows(0).Item("FOH_Bposted")) Then
                pnlEditable.Enabled = False
                usrMessageBar.ShowNotification("Data has been Posted, No more modifications are allowed.", UserControls_usrMessageBar.WarningType.Information)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
            Else
                pnlEditable.Enabled = True
                btnSave.Visible = True
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            'chkNextAcd.Checked = Convert.ToBoolean(_table.Rows(0).Item("FOH_bNextAcademicYear"))
            FOH_SDL_ID = _table.Rows(0).Item("FOH_SDL_ID").ToString
            SqlQuery = " SELECT ROW_NUMBER() OVER (ORDER BY STU_NO) AS SLNO, STU_ID, STU_NO, STU_NAME,FOD_AMOUNT, ISNULL(FOD_TAX_AMOUNT,0) FOD_TAX_AMOUNT, ISNULL(FOD_NET_AMOUNT ,0) FOD_NET_AMOUNT " & _
            " FROM VW_OSO_STUDENT_M INNER JOIN FEES.FEEOTHCHARGE_D WITH(NOLOCK) ON VW_OSO_STUDENT_M.STU_ID = FEES.FEEOTHCHARGE_D.FOD_STU_ID " & _
            " WHERE FOD_FOH_ID = " & viewid & ""

            _table = MainObj.getRecords(SqlQuery, "OASIS_FEESConnectionString")
            gvStudentDetails.DataSource = _table
            gvStudentDetails.DataBind()
            txtStuCount.Text = _table.Rows.Count.ToString()
            For Each grow As GridViewRow In gvStudentDetails.Rows
                If h_StuidAll.Value = "" Then
                    h_StuidAll.Value = grow.Cells(0).Text.ToString()
                Else
                    h_StuidAll.Value &= "@" & grow.Cells(0).Text.ToString()
                End If
            Next
            h_StuidAll.Value &= "@"

            LockYN(False)
            SqlQuery = "SELECT SDL_SCH_ID FROM [FEES].[VW_ALL_SCHEDULES] WHERE SDL_ID = '" & FOH_SDL_ID & "'"
            Dim SchID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, SqlQuery)
            If Not SchID Is Nothing AndAlso FeeCollection.GetDoubleVal(SchID) >= 0 Then
                ddlSchedule.SelectedValue = SchID
                ddlSchedule_SelectedIndexChanged(Nothing, Nothing)
                If ddlScheduleItem.Items.Count > 0 Then
                    ddlScheduleItem.SelectedValue = FOH_SDL_ID
                End If
            End If
        End If
    End Sub

    Sub InitialiseCompnents()
        gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        ACD_ID = 0
        FillACD()
        fillFees()
        If Not Session("BSU_bVATEnabled") Is Nothing AndAlso IsNumeric(Session("BSU_bVATEnabled")) Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        DISABLE_TAX_FIELDS()

        CalculateVAT(ddFeetype.SelectedValue, 0, Now.Date)
        SET_VAT_DROPDOWN_RIGHT()
        chkNextAcd.Checked = False
        ACD_ID = ddlAcademicYear.SelectedValue
        ddlSchedule_SelectedIndexChanged(Nothing, Nothing)
    End Sub
    Private Sub SET_VAT_DROPDOWN_RIGHT()
        Dim bHasRight As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CASE WHEN COUNT(USER_NAME)>0 THEN 1 ELSE 0 END AS bEXIST FROM OASIS.TAX.VW_VAT_ENABLE_USERS WHERE USER_NAME='" & Session("sUsr_name").ToString & "' AND USER_SOURCE='FEES'"))
        If Not bHasRight Then
            ddlTAX.Enabled = False
        End If
    End Sub

    Private Sub DISABLE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        trVAT.Visible = bShow
        trVATInfo.Visible = bShow
        lblalert.Text = UtilityObj.getErrorMessage("642")
        gvStudentDetails.Columns(4).Visible = bShow
        gvStudentDetails.Columns(5).Visible = bShow
    End Sub
    Private Sub CalculateVAT(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",'')")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlTAX.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE").ToString
            'lblTAX_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
            'lblNET_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
        End If

    End Sub
    Protected Sub rblTaxCalculation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblTaxCalculation.SelectedIndexChanged
        AUTO_FILL_AMOUNT()
    End Sub
    Private Sub AUTO_FILL_AMOUNT()
        If IsNumeric(txtAmountAdd.Text) AndAlso CDbl(txtAmountAdd.Text) > 0 Then
            If gvStudentDetails.Rows.Count > 0 Then
                Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label
                For Each gvr As GridViewRow In gvStudentDetails.Rows
                    txtDAmount = gvr.FindControl("txtDAmount")
                    lblTAXAmount = gvr.FindControl("lblTAXAmount")
                    lblNETTotal = gvr.FindControl("lblNETTotal")
                    txtDAmount.Text = Convert.ToDouble(txtAmountAdd.Text).ToString(Session("BSU_DataFormatString"))
                    CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
                Next
                'txtTotalAmount.Text = Format(CDbl(txtAmountAdd.Text) * (gvStudentDetails.Rows.Count), "#,##0.00")
                txtTotalAmount.Text = Format(CDbl(lblNETTotal.Text) * (gvStudentDetails.Rows.Count), "#,##0.00") ' commented above line on 20FEB2020
                txtStuCount.Text = gvStudentDetails.Rows.Count
            End If
        End If
    End Sub


    Private Sub AddStudent(Optional ByVal StuId As String = "")

        Dim _table As DataTable
        Dim _parameter As String(,) = New String(3, 1) {}

        If ChkBulk.Checked Then
            h_STUD_ID.Value = h_STUD_ID.Value & "||"

            _parameter(0, 0) = "@STUIDs"
            _parameter(0, 1) = h_STUD_ID.Value.Replace("||", "@")
            _parameter(1, 0) = "@StuBsuID"
            _parameter(1, 1) = Session("sBSuid")
            _parameter(2, 0) = "@AcdID"
            _parameter(2, 1) = ddlAcademicYear.SelectedValue.ToString()
            _parameter(3, 0) = "@Mode"
            _parameter(3, 1) = "BULK"

            _table = MainObj.getRecords("Students_List", _parameter, "OASIS_FEESConnectionString")

            For x As Integer = 0 To _table.Rows.Count - 1
                If h_StuidAll.Value = "" Then
                    h_StuidAll.Value = _table.Rows(x)("STU_ID").ToString()
                Else
                    h_StuidAll.Value += "||" & _table.Rows(x)("STU_ID").ToString()
                End If
            Next
        Else
            h_StuidAll.Value += h_STUD_ID.Value & "||"
        End If

        _parameter(0, 0) = "@STUIDs"
        _parameter(0, 1) = h_StuidAll.Value.ToString().Replace("||", "@")
        _parameter(1, 0) = "@StuBsuID"
        _parameter(1, 1) = Session("sBSuid")
        _parameter(2, 0) = "@AcdID"
        _parameter(2, 1) = ddlAcademicYear.SelectedValue.ToString()
        _parameter(3, 0) = "@Mode"
        _parameter(3, 1) = ""

        _table = MainObj.getRecords("Students_List", _parameter, "OASIS_FEESConnectionString")
        If _table.Rows.Count = 0 Then
            h_StuidAll.Value = ""
        End If
        gvStudentDetails.DataSource = _table
        gvStudentDetails.DataBind()

        txtStuCount.Text = _table.Rows.Count.ToString()
        h_STUD_ID.Value = ""
        If txtAmountAdd.Text = "" Then
            txtAmountAdd.Text = "0"
        End If


        Dim lblTAXAmount7 As New Label
        Dim lblNETTotal7 As New Label
        lblTAXAmount7.Text = "0"
        lblNETTotal7.Text = "0"
        CALCULATE_TAX(txtAmountAdd, lblTAXAmount7, lblNETTotal7, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
        'txtTotalAmount.Text = Convert.ToDouble(Convert.ToDouble(txtAmountAdd.Text) * _table.Rows.Count).ToString(Session("BSU_DataFormatString"))
        txtTotalAmount.Text = Convert.ToDouble(Convert.ToDouble(lblNETTotal7.Text) * _table.Rows.Count).ToString(Session("BSU_DataFormatString")) ' commented above line on 20FEB2020
    End Sub

    Private Sub doInsert(ByVal Mode As String)
        Dim StudentId As String = ""
        Dim AmountS As String = ""
        Dim TaxAmountS As String = ""
        Dim NETAmountS As String = ""
        Dim Slno As String = ""
        Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label
        Dim x As Integer = 1

        For Each grow As GridViewRow In gvStudentDetails.Rows
            txtDAmount = grow.FindControl("txtDAmount")
            lblTAXAmount = grow.FindControl("lblTAXAmount")
            lblNETTotal = grow.FindControl("lblNETTotal")
            If txtDAmount.Text = "" Then
                txtDAmount.Text = "0"
            End If
            If lblTAXAmount.Text = "" Then
                lblTAXAmount.Text = "0"
            End If
            If lblNETTotal.Text = "" Then
                lblNETTotal.Text = "0"
            End If
            If StudentId = "" Then
                StudentId = grow.Cells(0).Text.ToString()
                AmountS = txtDAmount.Text.ToString()
                TaxAmountS = lblTAXAmount.Text.ToString()
                NETAmountS = lblNETTotal.Text.ToString()
                Slno = x.ToString()
            Else
                StudentId += "@" & grow.Cells(0).Text.ToString()
                AmountS += "@" & txtDAmount.Text.ToString()
                TaxAmountS += "@" & lblTAXAmount.Text.ToString()
                NETAmountS += "@" & lblNETTotal.Text.ToString()
                Slno += "@" & x.ToString()
            End If

            x = x + 1
        Next
        Dim _parameter As String(,) = New String(21, 1) {}
        _parameter(0, 0) = "@FOH_BSU_ID"
        _parameter(0, 1) = Session("sBsuid")
        _parameter(1, 0) = "@FOH_DATE"
        _parameter(1, 1) = Convert.ToDateTime(txtFrom.Text.ToString()).ToString("dd-MMM-yyyy")
        _parameter(2, 0) = "@FOH_ACD_ID"
        _parameter(2, 1) = ddlAcademicYear.SelectedValue.ToString()
        _parameter(3, 0) = "@FOH_FEE_ID"
        _parameter(3, 1) = ddFeetype.SelectedValue.ToString()
        _parameter(4, 0) = "@FOH_AMOUNT"
        _parameter(4, 1) = FeeCollection.GetDoubleVal(txtTotalAmount.Text)
        _parameter(5, 0) = "@FOH_NARRATION"
        _parameter(5, 1) = txtRemarks.Text
        _parameter(6, 0) = "@STUIDs"
        _parameter(6, 1) = StudentId
        _parameter(7, 0) = "@AMOUNTs"
        _parameter(7, 1) = AmountS
        _parameter(8, 0) = "@SLNOs"
        _parameter(8, 1) = Slno
        _parameter(9, 0) = "@Edit"
        _parameter(9, 1) = Mode
        _parameter(10, 0) = "@AUD_WINUSER"
        _parameter(10, 1) = Page.User.Identity.Name.ToString()
        _parameter(11, 0) = "@Aud_form"
        _parameter(11, 1) = Master.MenuName.ToString()
        _parameter(12, 0) = "@Aud_user"
        _parameter(12, 1) = Session("sUsr_name")
        _parameter(13, 0) = "@Aud_module"
        _parameter(13, 1) = Session("sModule")
        _parameter(14, 0) = "@FOH_ID"
        _parameter(14, 1) = h_SelectId.Value.ToString()
        _parameter(15, 0) = "@FOH_TAX_CODE"
        _parameter(15, 1) = ddlTAX.SelectedValue
        _parameter(16, 0) = "@FOH_bNextAcademicYear"
        _parameter(16, 1) = chkNextAcd.Checked
        _parameter(17, 0) = "@FOH_SDL_ID"
        _parameter(17, 1) = ddlScheduleItem.SelectedValue
        _parameter(18, 0) = "@FOH_ISINCLUSIVE_TAX"
        _parameter(18, 1) = IIf(rblTaxCalculation.SelectedValue = "I", 1, 0)
        _parameter(19, 0) = "@TAXAMOUNTs"
        _parameter(19, 1) = TaxAmountS
        _parameter(20, 0) = "@NETAMOUNTs"
        _parameter(20, 1) = NETAmountS


        MainObj.doExcutiveRetvalue("[FEES].[Save_FEEOTHCHARGE]", _parameter, "OASIS_FEESConnectionString", "")
        'lblError.Text = MainObj.MESSAGE
        'lblError2.Text = lblError.Text
        usrMessageBar.ShowNotification(MainObj.MESSAGE, IIf(MainObj.MESSAGE.ToLower() = "Data Successfully updated".ToLower(), UserControls_usrMessageBar.WarningType.Success, UserControls_usrMessageBar.WarningType.Danger))
        If MainObj.SPRETVALUE.Equals(0) Then
            btnSave.Visible = False
            clear_All()
        End If
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Sub fillFees()

        ddFeetype.DataSource = FEE_GETFEESFORCOLLECTION_Other(Session("sBsuid"), ddlAcademicYear.SelectedValue)
        ddFeetype.DataTextField = "FEE_DESCR"
        ddFeetype.DataValueField = "FEE_ID"
        ddFeetype.DataBind()
        Dim dtACD As DataTable = GetTAXCode()
        ddlTAX.DataSource = dtACD
        ddlTAX.DataTextField = "TAX_DESCR"
        ddlTAX.DataValueField = "TAX_CODE"
        ddlTAX.DataBind()


    End Sub

    Function GetTAXCode() As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim dsData As New DataSet
        dsData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.[GET_TAX_CODES]", pParms)


        'Dim sql_query As String = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
        'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        'CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function

    Function FEE_GETFEESFORCOLLECTION_Other(ByVal BSU_ID As String, Optional ByVal ACD_ID As Integer = 0) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[FEE_GETFEESFORCOLLECTION_Other]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function




    Sub clear_All()
        lblError2.Text = ""
        txtRemarks.Text = ""
        txtAmountAdd.Text = ""
        txtTotalAmount.Text = "0"
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        gvStudentDetails.DataSource = Nothing
        gvStudentDetails.DataBind()
        h_SelectId.Value = "0"
        h_Posted.Value = "0"
        LinkContinue.Visible = False
        Session("ChkFees") = Nothing
        ChkBulk.Checked = False
        ' UsrSelStudent1.ClearDetails()
        chkNextAcd.Checked = False
        ACD_ID = 0
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ddlAcademicYear.SelectedValue = "" Then
                'lblError.Text = "Invalid..Academic Year..!"
                'lblError2.Text = lblError.Text
                usrMessageBar.ShowNotification("Invalid..Academic Year..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If txtTotalAmount.Text = "" Or Convert.ToDouble(txtTotalAmount.Text) = 0 Then
                'lblError.Text = "Invalid..Amount..!"
                'lblError2.Text = lblError.Text
                usrMessageBar.ShowNotification("Invalid..Amount..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If txtRemarks.Text = "" Then
                'lblError.Text = "Narration Can not be Empty..!"
                'lblError2.Text = lblError.Text
                usrMessageBar.ShowNotification("Narration Can not be Empty..!", UserControls_usrMessageBar.WarningType.Danger)
                txtRemarks.Focus()
                Exit Sub
            End If
            If h_Posted.Value = "True" Then
                'lblError.Text = "Particular Document Already Posted..!"
                'lblError2.Text = lblError.Text
                usrMessageBar.ShowNotification("Particular Document Already Posted..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If gvStudentDetails.Rows.Count = 0 Then
                'lblError.Text = "Please Select Students..!"
                'lblError2.Text = lblError.Text
                usrMessageBar.ShowNotification("Please Select Students..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            For Each grow As GridViewRow In gvStudentDetails.Rows
                If grow.Cells(0).Text = "" Or Not IsNumeric(grow.Cells(0).Text) Then
                    'lblError.Text = "Invalid Student Details..!"

                    'lblError2.Text = lblError.Text
                    usrMessageBar.ShowNotification("Invalid Student Details..!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If

            Next

            If bShow = True And ddlTAX.SelectedValue = "NA" Then
                'lblError.Text = "Selected VAT Code is not allowed for the Unit..!"
                usrMessageBar.ShowNotification("Selected VAT Code is not allowed for the Unit..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If ddlScheduleItem.Items.Count <= 0 OrElse ddlScheduleItem.SelectedValue = "" Then
                'lblError.Text = "Selected VAT Code is not allowed for the Unit..!"
                usrMessageBar.ShowNotification("Please select a Charge Schedule", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If


            If h_SelectId.Value = "0" Or h_SelectId.Value = "" Then
                If CheckFees() <> 0 Then
                    'lblError.Text = "Selected Student's Fess are Already Charged ..!"
                    'lblError2.Text = lblError.Text
                    usrMessageBar.ShowNotification("Selected Student's Fees are Already Charged ..!", UserControls_usrMessageBar.WarningType.Danger)
                    LinkContinue.Visible = True
                    Exit Sub
                End If
                doInsert("0")
            Else
                doInsert("1")
            End If
        Catch ex As Exception
            'lblError.Text = getErrorMessage(1000)
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
        End Try
    End Sub

    Function CheckFees() As Integer
        Dim retValue As Integer = 0
        Dim StudentIdS As String = ""
        For Each grow As GridViewRow In gvStudentDetails.Rows
            If StudentIdS = "" Then
                StudentIdS = grow.Cells(0).Text.ToString()
            Else
                StudentIdS += "@" & grow.Cells(0).Text.ToString()
            End If
        Next
        Dim _parameter As String(,) = New String(4, 1) {}
        _parameter(0, 0) = "@FOH_BSU_ID"
        _parameter(0, 1) = Session("sBsuid")
        _parameter(1, 0) = "@FOH_DATE"
        _parameter(1, 1) = Convert.ToDateTime(txtFrom.Text.ToString()).ToString("dd-MMM-yyyy")
        _parameter(2, 0) = "@FOH_ACD_ID"
        _parameter(2, 1) = ddlAcademicYear.SelectedValue.ToString()
        _parameter(3, 0) = "@FOH_FEE_ID"
        _parameter(3, 1) = ddFeetype.SelectedValue.ToString()
        _parameter(4, 0) = "@STUIDs"
        _parameter(4, 1) = StudentIdS

        Dim _table As DataTable = MainObj.getRecords("[FEES].[Check_FEEOTHCHARGE]", _parameter, "OASIS_FEESConnectionString")
        Session("ChkFees") = _table
        SelectDuplicate(_table)
        retValue = _table.Rows.Count
        Return retValue
    End Function

    Sub SelectDuplicate(ByVal _table As DataTable)
        If _table.Rows.Count > 0 Then
            Dim txtDAmount As New TextBox
            For Each grow As GridViewRow In gvStudentDetails.Rows
                txtDAmount = grow.FindControl("txtDAmount")
                If txtDAmount.Text = "" Then
                    txtDAmount.Text = "0"
                End If
                For x As Integer = 0 To _table.Rows.Count - 1
                    If grow.Cells(0).Text.ToString() = _table.Rows(x)(0).ToString() And txtDAmount.Text <> "0" Then
                        grow.CssClass = "griditem_hilight"
                        Exit For
                    End If
                Next
            Next
        End If
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        LockYN(True)
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub txtStudName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If h_STUD_ID.Value <> "" Then
            AddStudent("")
        End If
        txtStudName.Text = ""



    End Sub

    Protected Sub gvStudentDetails_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs)
        Dim StuId As String = ""
        StuId = gvStudentDetails.Rows(e.NewSelectedIndex).Cells(0).Text.ToString()
        h_StuidAll.Value = h_StuidAll.Value.ToString().Replace(StuId, "0")
        AddStudent()
        If Session("ChkFees").rows.count > 0 Then
            SelectDuplicate(Session("ChkFees"))
            LinkContinue.Visible = False
        End If
    End Sub

    Protected Sub gvStudentDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudentDetails.RowDataBound
        'If gvStudentDetails.Rows.Count > 1 Then
        Dim tAmount As Double = 0
        Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label

        If e.Row.RowType = DataControlRowType.DataRow Then
            If txtAmountAdd.Text <> "0" And txtAmountAdd.Text <> "" Then
                txtDAmount = e.Row.FindControl("txtDAmount")
                lblTAXAmount = e.Row.FindControl("lblTAXAmount")
                lblNETTotal = e.Row.FindControl("lblNETTotal")
                txtDAmount.Text = Convert.ToDouble(txtAmountAdd.Text).ToString(Session("BSU_DataFormatString"))
                CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
            End If

        End If
        If e.Row.Cells.Count > 1 Then
            e.Row.Cells(0).Style("display") = "none"
        End If
    End Sub

    Sub LockYN(ByVal lockYN As Boolean)
        ddlAcademicYear.Enabled = lockYN
        ddFeetype.Enabled = lockYN
        txtRemarks.Enabled = lockYN
        txtAmountAdd.Enabled = lockYN
        gvStudentDetails.Enabled = lockYN
        imgFrom.Enabled = lockYN
        imgStudent.Enabled = lockYN
        btnSave.Enabled = lockYN
        ddlTAX.Enabled = lockYN
        uploadFile.Enabled = lockYN
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If h_Posted.Value = "True" Then
            'lblError.Text = "Particular Document Already Posted..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Particular Document Already Posted..!", UserControls_usrMessageBar.WarningType.Danger)

            Exit Sub
        End If
        LockYN(True)
        ddlTAX.Enabled = False
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        txtStuCount.Text = gvStudentDetails.Rows.Count.ToString()
    End Sub

    Protected Sub LinkContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlAcademicYear.SelectedValue = "" Then
            'lblError.Text = "Invalid..Academic Year..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Invalid..Academic Year..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtTotalAmount.Text = "" Or Convert.ToDouble(txtTotalAmount.Text) = 0 Then
            'lblError.Text = "Invalid..Amount..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Invalid..Amount..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtRemarks.Text = "" Then
            'lblError.Text = "Can not be null..Narration..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Can not be null..Narration..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If h_Posted.Value = "1" Then
            'lblError.Text = "Particular Document Already Posted..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Particular Document Already Posted..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If gvStudentDetails.Rows.Count = 0 Then
            'lblError.Text = "Please Select Students..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Please Select Students..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        doInsert("0")
    End Sub

    Sub doDelete(ByVal PostId As String)
        Dim _parameter As String(,) = New String(6, 1) {}

        _parameter(0, 0) = "@FOH_ID"
        _parameter(0, 1) = PostId
        _parameter(1, 0) = "@FOH_BSU_ID"
        _parameter(1, 1) = Session("sBsuid")
        _parameter(2, 0) = "@AUD_WINUSER"
        _parameter(2, 1) = Page.User.Identity.Name.ToString()
        _parameter(3, 0) = "@Aud_form"
        _parameter(3, 1) = Master.MenuName.ToString()
        _parameter(4, 0) = "@Aud_user"
        _parameter(4, 1) = Session("sUsr_name")
        _parameter(5, 0) = "@Aud_module"
        _parameter(5, 1) = Session("sModule")


        MainObj.doExcutiveRetvalue("[FEES].[Delete_FEEOTHCHARGE]", _parameter, "OASIS_FEESConnectionString", "")
        'lblError.Text = MainObj.MESSAGE
        'lblError2.Text = lblError.Text
        usrMessageBar.ShowNotification(MainObj.MESSAGE, UserControls_usrMessageBar.WarningType.Danger)
        If MainObj.SPRETVALUE.Equals(0) Then
            clear_All()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            btnDelete.Visible = False
            btnEdit.Visible = False
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If h_Posted.Value = "True" Then
            'lblError.Text = "Particular Document Already Posted..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Particular Document Already Posted..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If h_SelectId.Value = "0" Or h_SelectId.Value = " " Then
            'lblError.Text = "Invalid Document..!"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification("Invalid Document..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        doDelete(h_SelectId.Value)

    End Sub
    Private Sub UpLoadDBF()
        Try
            If uploadFile.HasFile Then
                Dim FName As String = "" '"Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString()
                'FName += uploadFile.FileName.ToString().Substring(uploadFile.FileName.Length - 4)
                'Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                FName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & uploadFile.FileName.ToString()
                'If Not Directory.Exists(filePath & "\OnlineExcel") Then
                '    Directory.CreateDirectory(filePath & "\OnlineExcel")
                'End If
                'Dim FolderPath As String = filePath & "\OnlineExcel\"
                'filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

                If uploadFile.HasFile Then
                    If File.Exists(FName) Then
                        File.Delete(FName)
                    End If
                    uploadFile.SaveAs(FName)
                    Try
                        getdataExcel(FName)
                        File.Delete(FName)
                    Catch ex As Exception
                        Errorlog(ex.Message)
                        'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
                        'lblError2.Text = lblError.Text
                        usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
                    End Try
                End If
            End If
        Catch ex As Exception
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        'Dim conn As New OleDbConnection
        Try
            'Dim cmd As New OleDbCommand
            'Dim da As New OleDbDataAdapter
            'Dim ds As New DataSet
            'Dim dtXcel As New DataTable

            'Dim str_conn As String = ""
            'Dim strFileType As String = System.IO.Path.GetExtension(uploadFile.FileName).ToString().ToLower()
            'str_conn = IIf(strFileType = ".xls", "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=""Excel 8.0;HDR=YES;""" _
            '    , "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filePath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2""")
            'Dim excelQuery As String = ""
            'excelQuery = " SELECT * FROM [Sheet1$]"
            'conn = New OleDbConnection(str_conn)
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel(excelQuery, filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 4)
            If xltable.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.Invalid data in excel..!")
            End If

            Dim StuNos As String = ""
            Dim mRow As DataRow
            For Each mRow In xltable.Rows
                If mRow(0).ToString <> "" Or mRow(1).ToString <> "" Then
                    StuNos &= IIf(StuNos <> "", "@", "") & mRow(1).ToString
                End If
            Next

            Dim table As DataTable
            Dim parameter As String(,) = New String(2, 1) {}
            parameter(0, 0) = "@STUNos"
            parameter(0, 1) = StuNos
            parameter(1, 0) = "@StuBsuID"
            parameter(1, 1) = Session("sBSuid")
            parameter(2, 0) = "@AcdID"
            parameter(2, 1) = ddlAcademicYear.SelectedValue.ToString()

            table = MainObj.getRecords("VerifyStudents_List", parameter, "OASIS_FEESConnectionString")
            Dim iRow As DataRow
            Dim j As Integer = 0
            For Each mRow In xltable.Rows
                If table.Select("BSUNo='" & mRow(1) & "'").Length > 0 Then
                    iRow = table.Select("BSUNo='" & mRow(1) & "'")(0)
                    If iRow("NotValid") = 1 Then
                        iRow("STU_NAME") = "Not Valid  - Sl No:" & mRow(0) & " , ADMN No:" & mRow(1) & " , Student Name:" & mRow(2)
                    Else
                        iRow("FOD_AMOUNT") = CDbl(mRow(3))
                    End If
                End If
                j += 1
            Next

            gvStudentDetails.DataSource = table
            gvStudentDetails.DataBind()

            If table.Select("NotValid =1").Length > 0 Then
                For i As Integer = 0 To table.Rows.Count - 1
                    If (table).Rows(i)("NotValid") = 1 Then
                        gvStudentDetails.Rows(i).BackColor = Drawing.Color.Red
                    End If
                Next
            End If

            'txtStuCount.Text = table.Rows.Count.ToString()
            h_STUD_ID.Value = ""
            'If txtAmountAdd.Text = "" Then
            '    txtAmountAdd.Text = "0"
            'End If
            'txtTotalAmount.Text = Convert.ToDouble(Convert.ToDouble(txtAmountAdd.Text) * table.Rows.Count).ToString(Session("BSU_DataFormatString"))
            ddlTAX_SelectedIndexChanged(Nothing, Nothing)

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            'lblError2.Text = lblError.Text
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub doClear()
        'gvExcel.DataSource = Nothing
        'gvExcel.DataBind()
        Session("ExcelTable") = Nothing
        HidUpload.Value = ""
    End Sub
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click

        If Not uploadFile.HasFile Then
            'lblError.Text = "Select Particular File...!"
            usrMessageBar.ShowNotification("Select Particular File...!", UserControls_usrMessageBar.WarningType.Danger)

            Exit Sub
        End If
        doClear()
        Dim strFileType As String = System.IO.Path.GetExtension(uploadFile.FileName).ToString().ToLower()
        If Not strFileType = ".xls" Or strFileType = ".xlsx" Or strFileType = ".csv" Then
            'lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
            usrMessageBar.ShowNotification("Invalid file type.. Only Excel files are alowed.!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        UpLoadDBF()
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STUD_ID.Value <> "" Then
            AddStudent("")
        End If
    End Sub
    Sub bindStudentId()
        Try

            Dim str_data As String = String.Empty
            Dim str_sql As String = String.Empty
            txtStudName.Text = txtStudName.Text.Trim
            Dim iStdnolength As Integer = txtStudName.Text.Length
            'If IsStudent Then
            If iStdnolength < 9 Then
                If txtStudName.Text.Trim.Length < 8 Then
                    For i As Integer = iStdnolength + 1 To 8
                        txtStudName.Text = "0" & txtStudName.Text
                    Next
                End If
                txtStudName.Text = Session("sBsuid") & txtStudName.Text
            End If
            str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME + '|' + GRD_DISPLAY + '|' + STU_GRD_ID + '|' + isnull(STU_CURRSTATUS,'EN') FROM VW_OSO_STUDENT_M" _
             & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_NO='" & txtStudName.Text & "'"
            str_data = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, Data.CommandType.Text, str_sql)
            'Else
            '    str_data = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, Data.CommandType.Text, _
            '    "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME + '|' + GRD_DISPLAY + '|' + STU_GRD_ID FROM FEES.vw_OSO_ENQUIRY_COMP" _
            '    & " WHERE     (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_NO='" & txtStdNo.Text & "'")
            'End If
            If str_data <> "--" AndAlso str_data <> String.Empty AndAlso str_data <> "" Then
                h_StuidAll.Value += str_data.Split("|")(0) & "||"
                txtStudName.Text = str_data.Split("|")(2)
            Else
                'lblError.Text = "Student ID Entered is not valid  !!!"
                usrMessageBar.ShowNotification("Student ID Entered is not valid  !!!", UserControls_usrMessageBar.WarningType.Danger)
                txtStudName.Text = ""
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub ddFeetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddFeetype.SelectedIndexChanged
        CalculateVAT(ddFeetype.SelectedValue, 0, Now.Date)
        ddlTAX_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub CALCULATE_TAX(ByRef Amount As TextBox, ByRef lblTAX As Label, ByRef lblNet As Label, ByVal IsInclusiveTax As Integer)
        If IsNumeric(Amount.Text) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & Amount.Text & ",'" & ddlTAX.SelectedValue & "'," & IsInclusiveTax & ")")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTAX.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                If IsInclusiveTax Then
                    Amount.Text = (Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT")) - Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT"))).ToString

                End If
                lblNet.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub

    Protected Sub linkAutofill_Click(sender As Object, e As EventArgs) Handles linkAutofill.Click
        AUTO_FILL_AMOUNT()
    End Sub

    Protected Sub txtDAmount_TextChanged(sender As Object, e As EventArgs)
        Dim gvr As GridViewRow = sender.parent.parent
        Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblTotal As Double = 0
        txtDAmount = gvr.FindControl("txtDAmount")
        lblTAXAmount = gvr.FindControl("lblTAXAmount")
        lblNETTotal = gvr.FindControl("lblNETTotal")
        CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
        For Each gvro As GridViewRow In gvStudentDetails.Rows
            Dim txtDAmount2 As TextBox = gvro.FindControl("txtDAmount")
            Dim lblNETTotal2 As Label = gvro.FindControl("lblNETTotal")
            'dblTotal += CDbl(txtDAmount2.Text)
            dblTotal += CDbl(lblNETTotal2.Text) ' commented above line on 20FEB2020
        Next
        txtTotalAmount.Text = Format(dblTotal, "#,##0.00")
        txtStuCount.Text = gvStudentDetails.Rows.Count
    End Sub

    Protected Sub ddlTAX_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTAX.SelectedIndexChanged
        If gvStudentDetails.Rows.Count > 0 Then
            Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblGridTotal As Double = 0
            For Each gvr As GridViewRow In gvStudentDetails.Rows
                txtDAmount = gvr.FindControl("txtDAmount")
                lblTAXAmount = gvr.FindControl("lblTAXAmount")
                lblNETTotal = gvr.FindControl("lblNETTotal")
                dblGridTotal += CDbl(txtDAmount.Text)
                CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
            Next
            txtTotalAmount.Text = Format(dblGridTotal, "#,##0.00")
            txtStuCount.Text = gvStudentDetails.Rows.Count
        End If
    End Sub

    'Protected Sub rblSchedule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSchedule.SelectedIndexChanged
    '    Dim Qry As String = "SELECT SDL_ID,CASE WHEN SDL_SCH_ID = 2 THEN SDL_DESCR + ' (' + ISNULL(SDL_ACY_DESCR,'') + ')' ELSE SDL_DESCR END SDL_DESCR FROM [FEES].[VW_ALL_SCHEDULES] WHERE SDL_ACD_ID = " & ACD_ID & " AND SDL_SCH_ID = " & rblSchedule.SelectedValue & ""
    '    ddlScheduleItem.DataValueField = "SDL_ID"
    '    ddlScheduleItem.DataTextField = "SDL_DESCR"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry)
    '    ddlScheduleItem.DataSource = ds
    '    ddlScheduleItem.DataBind()
    'End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ACD_ID = ddlAcademicYear.SelectedValue
        If chkNextAcd.Checked Then
            ACD_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(NEXT_ACD_ID ,0) NEXT_ACD_ID FROM dbo.[fN_GetNextAcademicYearAndGrade]('" & Session("sBsuId") & "'," & ddlAcademicYear.SelectedValue & ",'',0,0)")
        End If
        ddlSchedule_SelectedIndexChanged(sender, Nothing)
    End Sub

    Protected Sub chkNextAcd_CheckedChanged(sender As Object, e As EventArgs) Handles chkNextAcd.CheckedChanged
        If chkNextAcd.Checked Then
            ACD_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(NEXT_ACD_ID ,0) NEXT_ACD_ID FROM dbo.[fN_GetNextAcademicYearAndGrade]('" & Session("sBsuId") & "'," & ddlAcademicYear.SelectedValue & ",'',0,0)")
        Else
            ACD_ID = ddlAcademicYear.SelectedValue
        End If
        ddlSchedule_SelectedIndexChanged(sender, Nothing)
    End Sub

    Protected Sub ddlSchedule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSchedule.SelectedIndexChanged
        Dim Qry As String = "SELECT SDL_ID,CASE WHEN SDL_SCH_ID = 2 THEN SDL_DESCR + ' (' + ISNULL(SDL_ACY_DESCR,'') + ')' ELSE SDL_DESCR END SDL_DESCR FROM [FEES].[VW_ALL_SCHEDULES] WHERE SDL_ACD_ID = " & ACD_ID & " AND SDL_SCH_ID = " & ddlSchedule.SelectedValue & ""
        ddlScheduleItem.DataValueField = "SDL_ID"
        ddlScheduleItem.DataTextField = "SDL_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry)
        ddlScheduleItem.DataSource = ds
        ddlScheduleItem.DataBind()
        ddlScheduleItem_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub ddlScheduleItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlScheduleItem.SelectedIndexChanged
        Dim Qry As String = "SELECT OASIS.dbo.fnFormatDateToDisplay(SDL_DTFROM) SDL_DTFROM FROM [FEES].[VW_ALL_SCHEDULES] WHERE SDL_ID = '" & ddlScheduleItem.SelectedValue & "'"
        Dim SDL_DTFROM As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry)
        If Not SDL_DTFROM Is Nothing Then
            txtFrom.Text = SDL_DTFROM
        End If
    End Sub
End Class

