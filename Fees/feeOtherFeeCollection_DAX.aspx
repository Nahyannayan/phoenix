<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeOtherFeeCollection_DAX.aspx.vb" Inherits="Fees_feeOtherFeeCollection_DAX"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">

    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(

function CheckForPrint() {
    ShowHideBankAccount();
    //alert($('#<%= hf_BNK_TYPE.ClientID%>').val());
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        Popup('../Reports/ASPX Report/RptViewerModal.aspx')
    }
}
    );

        <%--function getAccount() {
    var sFeatures;
    sFeatures = "dialogWidth: 709px; ";
    sFeatures += "dialogHeight: 434px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var bsu = document.getElementById('<%= h_BSU_ID.ClientID %>').value;
    var COLLID = document.getElementById('<%= ddCollection.ClientID%>').value;
    var docdt = document.getElementById('<%= txtFrom.ClientID%>').value;
    var NameandCode;
    var result;
    pMode = "ACCOTHFCNEW"
    url = "../Common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu + "&collid=" + COLLID + "&docdt=" + docdt;
    result = window.showModalDialog(url, "", sFeatures);
    //result = window.showModalDialog("..\/accounts\/ShowAccount.aspx?ShowType=NOTCC&codeorname=" + document.getElementById('<%=txtDAccountCode.ClientID %>').value, "", sFeatures);

    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('___');
    document.getElementById('<%=hf_OFM_ID.ClientID%>').value = NameandCode[0];
    document.getElementById('<%=txtDAccountCode.ClientID %>').value = NameandCode[1];
    document.getElementById('<%=txtDAccountName.ClientID %>').value = NameandCode[2];

    return false;
}--%>
        <%--function getEmployee() {
    var sFeatures;
    sFeatures = "dialogWidth: 709px; ";
    sFeatures += "dialogHeight: 434px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    pMode = "VENDOR"
    url = "../Common/PopupSelect.aspx?id=" + pMode;
    result = window.showModalDialog(url, "", sFeatures);

    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('___');
    document.getElementById('<%=hf_VDR_CODE.ClientID%>').value = NameandCode[0];
    document.getElementById('<%=txtVendorCode.ClientID%>').value = NameandCode[1];
    document.getElementById('<%=txtVendorDescr.ClientID%>').value = NameandCode[2];

    return false;
}--%>

        // --------------------------------------------------------------NOT USING ------------------------------------------------------------------------------------------------
        function PayTransportFee(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 755px; ";
            sFeatures += "dialogHeight: 670px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            return false;
        }
<%--function getBankOrEmirate(mode, ctrl) {
    var sFeatures, url;
    sFeatures = "dialogWidth: 600px; ";
    sFeatures += "dialogHeight: 500px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    if (mode == 1)
        url = "../common/PopupBanks.aspx?iD=BANK&MULTISELECT=FALSE";
    else
        url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('___');
    if (mode == 1) {
        if (ctrl == 1) {
            document.getElementById('<%= h_Bank.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= hf_BNK_TYPE.ClientID%>').value = NameandCode[2];
            if (NameandCode[2] == "BANKTRF") {
                $("#tdhBA").show(250);
                $("#tdBA1").show(250);
            }
            else {
                $("#tdhBA").hide(250);
                $("#tdBA1").hide(250);
            }
        }
    }
    else {
    }
    return false;
}--%>
        function ShowHideBankAccount() {
            if ($('#<%= hf_BNK_TYPE.ClientID%>').val() != 'BANKTRF') {
                $("#tdhBA").hide();
                $("#tdBA1").hide();
            }
            else {
                $("#tdhBA").show();
                $("#tdBA1").show();
            }
        }
<%--function getBankAct(mode) {
    var sFeatures;
    var sFeatures;
    sFeatures = "dialogWidth: 560px; ";
    sFeatures += "dialogHeight: 475px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var txtBankCode;
    if (mode == 1) txtBankCode = document.getElementById('<%=txtBankAct1.ClientID%>').value;
    result = window.showModalDialog("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + txtBankCode, "", sFeatures);
    if (result == '' || result == undefined)
    { return false; }
    lstrVal = result.split('||');
    if (mode == 1) {
        document.getElementById('<%=hfBankAct1.ClientID%>').value = lstrVal[0];
        document.getElementById('<%=txtBankAct1.ClientID%>').value = lstrVal[0];
    }

}--%>
        function Showdata(mode) {
            var url;
            var NameandCode;
            var STU_ID = document.getElementById('').value;
            var result;
            if (mode == 1)
                url = "../common/PopupShowData.aspx?id=RECEIPTHISTORY&stuid=" + STU_ID;
            else if (mode == 2)
                url = "../common/PopupShowData.aspx?id=CHARGEHISTORY&stuid=" + STU_ID;
            else if (mode == 3)
                url = "../common/PopupShowData.aspx?id=SIBBLINGS&stuid=" + STU_ID;
            else if (mode == 4)
                url = "../common/PopupShowData.aspx?id=CONCESSION&stuid=" + STU_ID;
            else if (mode == 5)
                url = "../common/PopupShowData.aspx?id=ADJUSTMENT&stuid=" + STU_ID;
            else if (mode == 6)
                url = "../common/PopupShowData.aspx?id=CHQBOUNCE&stuid=" + STU_ID;
            else if (mode == 7)
                url = "../common/PopupShowData.aspx?id=ADVPAY&stuid=" + STU_ID;
            else if (mode == 8)
                url = "../common/PopupShowData.aspx?id=AGING&stuid=" + STU_ID;
            else if (mode == 9)
                url = "FeeCalculateFee.aspx";
            Popup(url);
        }

        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
            var amt;
            amt = parseFloat(roundOff)
            if (isNaN(amt))
                amt = 2;
            return amt;
        }
        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(getRoundOff());
            return true;
        }
        function UpdateSum() {

            var txtCCTotal, txtChequeTotal, txtCashTotal,
                    txtTotal, txtReturn, lblTotalNETAmount, txtBalance, txtBankTotal;
            try {
                txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
            }
            catch (e) { }
            if (isNaN(txtCCTotal))
                txtCCTotal = 0;
            try {
                txtChequeTotal = parseFloat(document.getElementById('<%=txtChequeTotal.ClientID %>').value);
            }
            catch (e) { }

            if (isNaN(txtChequeTotal))
                txtChequeTotal = 0;
            try {
                txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
            }
            catch (e) { }

            if (isNaN(txtCashTotal))
                txtCashTotal = 0;

            if (isNaN(txtTotal))
                txtTotal = 0;
            lblTotalNETAmount = txtCCTotal + txtChequeTotal + txtCashTotal;
            txtBankTotal = txtChequeTotal;
            txtBalance = 0;
            if (txtCashTotal > 0)
                if (lblTotalNETAmount > txtTotal) {
                    txtBalance = lblTotalNETAmount - txtTotal;
                    if (txtBalance > txtCashTotal)
                        txtBalance = 0;
                }
            //document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtReceivedTotal.toFixed(getRoundOff());
            try {
                document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());
            } catch (e) { }
            try {
                document.getElementById('<%=txtChequeTotal.ClientID %>').value = txtBankTotal.toFixed(getRoundOff());
            } catch (e) { }
            try {
                document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
            } catch (e) { }


            if (txtCCTotal >= 0) {
                CalculateRate(txtCCTotal);
            }
        }
        function CalculateRate(CardAmount) {
            var temp = new Array();
            temp = document.getElementById('<%=hfCobrand.ClientID %>').value.split("|");
            var ddlist = document.getElementById('<%=ddCreditcard.ClientID %>');

            if (temp.length > 0 && ddlist != null) {
                for (var i = 0; i < temp.length; i++) {
                    if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                        document.getElementById('<%=txtCrCharge.ClientID %>').value = Math.ceil(CardAmount * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff());
                        //document.getElementById('<%=txtReceivedTotal.ClientID %>').value = CardAmount + Math.ceil(CardAmount * (temp[i].split("=")[1] / 100.00));
                        document.getElementById('<%=txtChargeTotal.ClientID %>').value = CardAmount + Math.ceil(CardAmount * (temp[i].split("=")[1] / 100.00));
                    }
                }
            }
        }
        function allownumber() {
            var e = window.event;
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);

            }
            if (key == 13) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }
<%--function GetEMPName() {
    var sFeatures;
    sFeatures = "dialogWidth: 729px; ";
    sFeatures += "dialogHeight: 445px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=ENR", "", sFeatures)
    if (result != '' && result != undefined) {
        NameandCode = result.split('___');
        document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
        document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
        //
        return true;
    }
    return false;
}--%>
        function uploadComplete(sender) {
            $get("<%=btnUpload.ClientID%>").click();
        }

        function uploadError(sender) {
            $get("<%=lblMesg.ClientID%>").innerHTML = "File upload failed.";
        }

        function GetSTUDENTS() {
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }

            var oWnd = radopen("../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=TextBox1.ClientID%>").value = NameandCode[0];
                __doPostBack("<%=TextBox1.ClientID%>", 'TextChanged');
                //document.getElementById("<%=btnCheck.ClientID %>").click();
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

    <script>
        function getAccount() {
            var bsu = document.getElementById('<%= h_BSU_ID.ClientID %>').value;
            var COLLID = document.getElementById('<%= ddCollection.ClientID%>').value;
            var docdt = document.getElementById('<%= txtFrom.ClientID%>').value;
            var pMode = "ACCOTHFCNEW"
            url = "../Common/PopupSelect.aspx?id=" + pMode + "&bsu=" + bsu + "&collid=" + COLLID + "&docdt=" + docdt;

            var oWnd = radopen(url, "pop_getAccount");
        }
        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=hf_OFM_ID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtDAccountCode.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtDAccountName.ClientID %>').value = NameandCode[2];
                __doPostBack("<%=txtDAccountCode.ClientID%>", 'TextChanged');
            }
        }
        function getEmployee() {

            var pMode = "VENDOR"
            url = "../Common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_getEmployee");

        }
        function OnClientClose2(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=hf_VDR_CODE.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtVendorCode.ClientID%>').value = NameandCode[1];
                document.getElementById('<%=txtVendorDescr.ClientID%>').value = NameandCode[2];
            }
        }
        function getBankOrEmirate(mode, ctrl) {

            var url;
            document.getElementById('<%= hf_mode.ClientID%>').value = mode
            document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
            if (mode == 1)
                url = "../common/PopupBanks.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_getBankOrEmirate");

        }
        function OnClientClose3(oWnd, args) {

            var mode = document.getElementById('<%= hf_mode.ClientID%>').value
            var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                if (mode == 1) {
                    if (ctrl == 1) {
                        document.getElementById('<%= h_Bank.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
                        document.getElementById('<%= hf_BNK_TYPE.ClientID%>').value = NameandCode[2];
                        if (NameandCode[2] == "BANKTRF") {
                            $("#tdhBA").show(250);
                            $("#tdBA1").show(250);
                        }
                        else {
                            $("#tdhBA").hide(250);
                            $("#tdBA1").hide(250);
                        }
                    }
                }
            }
        }
        function getBankAct(mode) {
            document.getElementById('<%= hf_mode.ClientID%>').value = mode
            var txtBankCode;
            if (mode == 1) txtBankCode = document.getElementById('<%=txtBankAct1.ClientID%>').value;
            var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + txtBankCode, "pop_getBankAct");

        }
        function OnClientClose4(oWnd, args) {
            var mode = document.getElementById('<%= hf_mode.ClientID%>').value
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                if (mode == 1) {
                    document.getElementById('<%=hfBankAct1.ClientID%>').value = NameandCode[0];
                    document.getElementById('<%=txtBankAct1.ClientID%>').value = NameandCode[0];
                }
            }
        }


        function GetEMPName() {

            url = "../Accounts/accShowEmpDetail.aspx?id=ENR";
            var oWnd = radopen(url, "pop_GetEMPName");

        }
        function OnClientClose5(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
            }
        }




    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getAccount" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getEmployee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankAct" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_GetEMPName" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Other Fee Collection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
                <asp:HiddenField ID="HidUpload" runat="server" />
                <asp:HiddenField ID="h_Emp_No" runat="server" Value="0" />
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr id="tr1_header" runat="server">
                        <td align="left" class="matters" width="20%"></td>
                        <td align="left" colspan="1" width="30%"></td>
                        <td align="left" class="matters" width="20%"></td>
                        <td align="left" class="matters" width="30%">Enter DAX Invoice No
                        </td>
                    </tr>

                    <tr id="tr_taxInv" runat="server">
                        <td align="left" class="matters" width="20%"><span class="field-label">Generate Auto Tax Invoice<span class="text-danger">*</span> </span></td>
                        <td align="left" colspan="1" width="30%">
                            <asp:CheckBox Text="Yes" CssClass="field-label" runat="server" ID="Chk_InvYes" AutoPostBack="true" OnCheckedChanged="CheckedChangedYes" />
                            <asp:CheckBox Text="No" CssClass="field-label" runat="server" ID="Chk_InvNo" AutoPostBack="true" OnCheckedChanged="CheckedChangedNo" />

                        </td>
                        <td align="left" class="matters" width="20%">
                            <asp:Label ID="lblInvno" runat="server" class="field-label" Text="Invoice No"></asp:Label>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtInvno" runat="server" AutoPostBack="true" OnTextChanged="txtInvno_TextChanged">
                            </asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Receipt No  </span></td>
                        <td align="left" colspan="1" width="30%">
                            <asp:TextBox ID="txtRefno" runat="server" TabIndex="80" AutoCompleteType="Disabled" Enabled="false">
                            </asp:TextBox>
                        </td>
                        <td align="right" colspan="1" width="20%" style="text-align: left"><span class="field-label">Date</span>
                        </td>
                        <td align="left" colspan="1" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Collection <span class="text-danger">*</span></span>
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddCollection" runat="server" DataSourceID="sdsCollection" DataTextField="COL_DESCR"
                                DataValueField="COL_ID" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                        <%--<asp:TemplateField HeaderText="Paying Now(FC)" Visible="False">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmountToPayFC" runat="server" AutoPostBack="True" onblur="return CheckAmount(this);"
                                    onfocus="this.select();" OnTextChanged="txtAmountToPayFC_TextChanged" Style="text-align: right"
                                    TabIndex="25" Text='<%# AccountFunctions.Round(Container.DataItem("FCAmount")) %>'
                                    Width="89px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </tr>

                    <tr id="trCurrency" runat="server">
                        <td align="left" class="matters"></td>
                        <td align="right" class="matters" colspan="3">
                            <asp:Panel ID="pnlCurrency" runat="server">
                                <table style="width: 60% !important;" class="table table-bordered">
                                    <tr>
                                        <td class="matters" width="30%"><span class="field-label">Currency</span>
                                        </td>
                                        <td class="matters" width="70%">
                                            <asp:DropDownList ID="ddCurrency" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="matters"><span class="field-label">Exchange Rate</span>
                                        </td>
                                        <td class="matters">
                                            <asp:Label ID="lblExgRate" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>

                    <tr id="trCustomer" runat="server">
                        <td align="left" class="matters" colspan="4">
                            <table style="width: 100% !important;">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Customer<span class="text-danger">*</span></span></td>
                                    <td align="left" class="matters" width="30%">
                                        <%-- <asp:DropDownList ID="ddlCustomers" runat="server">
                                        </asp:DropDownList>--%>
                                        <telerik:RadComboBox RenderMode="Lightweight" ID="ddlCustomers" Filter="Contains" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged" Width="100%">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Customer Tax Registration No</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtCustTAXRegNo" runat="server" MaxLength="20"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Address</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtCustAddress" TextMode="MultiLine" runat="server" MaxLength="1000" SkinID="MultiText"></asp:TextBox></td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type<span class="text-danger">*</span></span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddFeetype" runat="server" OnSelectedIndexChanged="ddFeetype_SelectedIndexChanged" TabIndex="9" AutoPostBack="true">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"> <asp:Label ID="lbleventtype" runat="server" CssClass="field-label" Text=""></asp:Label><span id="lbleventid" runat="server" class="text-danger">*</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddEventtype" runat="server" TabIndex="9" AutoPostBack="true">
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr id="trAccount" runat="server">
                        <td align="left" class="matters"><span class="field-label">Account Code</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtDAccountCode" runat="server" AutoPostBack="True" CssClass="inputbox" Enabled="false"
                                Width="20%" OnTextChanged="txtDAccountCode_TextChanged">
                            </asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txtDAccountCode" ErrorMessage="Account Code Cannot Be Empty"
                                ValidationGroup="Details">*</asp:RequiredFieldValidator><asp:ImageButton ID="btnAccount" Enabled="false"
                                    runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAccount();return false;"
                                    TabIndex="14"></asp:ImageButton></td>
                        <td align="left" class="matters"><span class="field-label">Account Name</span></td>
                        <td>
                            <asp:TextBox ID="txtDAccountName" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trVendor" runat="server">
                        <td align="left" class="matters"><span class="field-label">Vendor Code</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtVendorCode" runat="server" AutoPostBack="True" CssClass="inputbox"
                                Width="20%" OnTextChanged="txtEmpNo_TextChanged"></asp:TextBox><asp:ImageButton ID="imgBtnEmp"
                                    runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getEmployee();return false;"
                                    TabIndex="15"></asp:ImageButton></td>
                        <td align="left" class="matters"><span class="field-label">Vendor Name</span></td>
                        <td>
                            <asp:TextBox ID="txtVendorDescr" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr id="trEvents" runat="server" visible="false">
                        <td align="left" class="matters"><span class="field-label">Events &amp; Activities</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlEvents" runat="server" AutoPostBack="True" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trCashFlow" runat="server" visible="false">
                        <td align="left" class="matters"><span class="field-label">CashFlow</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlCashFlow" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Narration</span>
                        </td>
                        <td align="left" class="matters" colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText"
                                TabIndex="160" Style="height: 100px;"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr class="gridheader_pop">
                        <td align="left" colspan="6"><span class="field-label">Bulk Upload</span>
                            <asp:CheckBox ID="chkBulkUpload" runat="server" AutoPostBack="True" />
                            <div>
                                <asp:RadioButtonList ID="rbUpload" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" Visible="false">
                                    <asp:ListItem Selected="True" Value="1"><span class="field-label">Excel Upload</span></asp:ListItem>
                                    <asp:ListItem Value="2"><span class="field-label">Student Selection</span></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                        </td>
                    </tr>

                    <tr id="trBulkUpload" runat="server" style="display: none;">
                        <td colspan="4" align="center">

                            <table style="width: 100%">
                                <tr>
                                    <td align="left" class="matters" valign="middle" width="20%"><span class="field-label">Choose File</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <span style="font-weight: bolder;" class="text-danger">Note: The amount against each student in excel sheet should be including tax if applicable.</span>
                                        <ajaxToolkit:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete"
                                            runat="server" ID="AsyncFileUpload1" UploaderStyle="Traditional"
                                            CompleteBackColor="White" UploadingBackColor="#CCFFFF" ThrobberID="imgLoader"
                                            OnUploadedComplete="FileUploadComplete" />
                                        <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" />
                                        <br />
                                        <asp:Label ID="lblMesg" runat="server" CssClass="text-info" Text="Only excel file with (.xls) extension is allowed, Maximum 200 records allowed in a single transaction."></asp:Label>
                                    </td>
                                    <td align="left" class="matters" width="20%">
                                        <asp:Button ID="btnUpload" runat="server" Style="background-color: transparent !important; border: none;" Text="" CausesValidation="False" />
                                        <asp:HiddenField ID="hfFilePath" runat="server" />
                                    </td>
                                    <td width="30%">
                                        <asp:HyperLink
                                            ID="lnkXcelFormat" runat="server" Visible="false">Click here to get the formatted Excel file</asp:HyperLink></td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr id="trStudentSelection" runat="server" style="display: none;">
                        <td colspan="4" align="center">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" valign="middle">
                                        <asp:Label ID="Label4" runat="server" Text="Academic Year" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left" class="matters" valign="middle">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Grade" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Section" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Amount" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDefaultAmount" runat="server" Text="0.00" Style="text-align: right;"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtDefaultAmount" />

                                    </td>



                                </tr>



                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Student (S)</span></td>
                                    <td align="left" class="matters" colspan="4">
                                        <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS();return false;" OnClick="imgStudent_Click"></asp:ImageButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>





                    <tr id="trGridandStaff" runat="server" style="display: none;">

                        <td colspan="4" align="center">
                            <table width="100%">
                                <tr>
                                    <td align="center" class="matters" valign="middle" colspan="4" width="100%">
                                        <asp:GridView ID="gvError" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered" EmptyDataText="" PageSize="5">
                                            <Columns>
                                                <asp:BoundField DataField="STU_NO" HeaderText="Student Id" />
                                                <asp:BoundField DataField="STU_NAME" HeaderText="Name" />
                                                <asp:BoundField DataField="GRADE" HeaderText="Grade" />
                                                <asp:BoundField DataField="AMOUNT" HeaderText="Amount" DataFormatString="{0:0.00}" HtmlEncode="false" />
                                                <asp:BoundField DataField="ERR_MSG" HeaderText="Error Message" ItemStyle-ForeColor="Red" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" class="matters" valign="middle" colspan="4" width="100%">
                                        <asp:GridView ID="gvStudSelection" runat="server" AutoGenerateColumns="False" DataKeyNames="STU_NO" CssClass="table table-row table-bordered" AllowPaging="True">
                                            <Columns>
                                                <asp:BoundField DataField="STU_NO" HeaderText="Student Id" />
                                                <asp:BoundField DataField="STU_NAME" HeaderText="Name" />
                                                <asp:BoundField DataField="GRADE" HeaderText="Grade" />
                                                <asp:BoundField DataField="AMOUNT" HeaderText="Amount" DataFormatString="{0:0.00}" HtmlEncode="false" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnDelete" runat="server" OnClick="lbtnDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">No Of Students</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblNoofStudents" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Total Amount</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblTotalBulkAmount" runat="server" CssClass="field-value"></asp:Label>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Employee</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" CausesValidation="False" /></td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>


                    </tr>





                    <tr class="title-bg">
                        <td colspan="6" align="left">Invoice Details
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <table style="width: 60% !important;">
                                <tr class="title-bg-light">
                                    <td align="center">Amount is</td>
                                    <td align="center">Amount</td>
                                    <td align="center" runat="server" id="trTAXcode1">Tax Code</td>
                                    <td align="right" runat="server" id="trTAXAmt1">Tax Amount</td>
                                    <td align="right" runat="server" id="trNetAmt1">Net Amount</td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters">
                                        <asp:RadioButtonList ID="rblTaxCalculation" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="E">Excl.Tax</asp:ListItem>
                                            <asp:ListItem Value="I">Incl.Tax</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="center" class="matters">
                                        <asp:TextBox ID="txtAmount" runat="server" Text="0.00" Style="text-align: right; !important;" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtAmount" />
                                    </td>
                                    <td align="center" class="matters" runat="server" id="trTAXcode2">
                                        <asp:DropDownList ID="ddlTAXCode" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                                    <td align="right" class="matters" runat="server" id="trTAXAmt2">
                                        <asp:Label ID="lblTaxAmount" runat="server" Text="0.00"></asp:Label></td>
                                    <td align="right" class="matters" runat="server" id="trNetAmt2">
                                        <asp:Label ID="lblNetAmount" runat="server" Text="0.00"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trAlertmessage">
                                    <td align="center"></td>
                                    <td colspan="4" align="center">
                                        <asp:Label ID="lblAlert" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td align="left" colspan="6">Payment Details
                        </td>
                    </tr>

                    <tr runat="server">
                        <td align="center" class="matters" colspan="6">

                            <asp:RadioButtonList ID="rblPaymentModes" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            </asp:RadioButtonList>

                        </td>
                    </tr>

                    <tr id="tr_Cash" runat="server">
                        <td align="left" class="matters" width="20%"><span class="field-label">Cash</span>
                        </td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtCashTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.blur();"
                                onBlur="UpdateSum();" Style="text-align: right" TabIndex="45"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtCashTotal"
                                FilterType="Numbers,Custom" ValidChars="." />
                        </td>
                        <td align="left" rowspan="1" class="matters" colspan="4" width="60%">
                            <asp:Button ID="btnAddCash" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="46" />
                        </td>
                    </tr>
                    <tr id="tr_CreditCard" runat="server">
                        <td colspan="6">
                            <center>
                    <table  >
                        <tr class="title-bg">
                            <td align="left" class="matters_Colln">Card Type
                            </td>
                            <td align="left" class="matters_Colln">Auth Code
                            </td>
                            <td align="left" class="matters_Colln">Amount
                            </td>
                            <td align="left" class="matters_Colln">Charge
                            </td>
                            <td align="left" class="matters_Colln">Total
                            </td>
                            <td align="left" class="matters_Colln"></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard" DataTextField="CRI_DESCR"
                                    DataValueField="CRR_ID" SkinID="DropDownListNormal" TabIndex="55" onChange="UpdateSum();">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="56"
                                    ></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCCTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.blur();"
                                    onBlur="UpdateSum();" Style="text-align: right" TabIndex="57"  ></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                    TargetControlID="txtCCTotal" FilterType="Numbers,Custom" ValidChars="." />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCrCharge" runat="server" AutoCompleteType="Disabled" Text="0.00"
                                    Style="text-align: right" TabIndex="58"  Enabled="false"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChargeTotal" runat="server" AutoCompleteType="Disabled" Text="0.00"
                                    Style="text-align: right" TabIndex="59"  Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnAddCreditcard" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                    TabIndex="60" />
                            </td>
                        </tr>
                    </table>
                </center>
                        </td>
                    </tr>
                    <tr id="tr_Cheque1" runat="server">
                        <td align="left" class="matters" width="15%"><span class="field-label">Cheque Amount</span>
                        </td>
                        <td align="left" width="15%">
                            <asp:TextBox ID="txtChequeTotal" onBlur="UpdateSum();" onFocus="this.blur();" runat="server"
                                Style="text-align: right" TabIndex="85" AutoCompleteType="Disabled"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                TargetControlID="txtChequeTotal" FilterType="Numbers,Custom" ValidChars="." />
                        </td>
                        <td align="left" class="matters" width="30%" colspan="2">
                            <table style="border: none 0;" width="100%">
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Bank</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBank" runat="server" AutoPostBack="True" TabIndex="86"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton5" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getBankOrEmirate(1,1); return false;" TabIndex="87" />
                                        <asp:HiddenField ID="hf_BNK_TYPE" runat="server" />
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" class="matters" id="tdhBA"><span class="field-label">Account</span>
                                    </td>
                                    <td id="tdBA1">
                                        <asp:TextBox ID="txtBankAct1" runat="server" onfocus="this.blur();" TabIndex="88"
                                            SkinID="TextBoxCollection"></asp:TextBox>
                                        <asp:ImageButton ID="imgBankAct1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getBankAct(1);return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td align="left" class="matters" width="15%"><span class="field-label">Emirate</span>
                        </td>
                        <td align="left" width="15%">
                            <asp:DropDownList ID="ddlEmirate" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="89">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr_Cheque2" runat="server">
                        <td align="left" class="matters"><span class="field-label">Chq. No.</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqno" runat="server" TabIndex="90" AutoCompleteType="Disabled">
                            </asp:TextBox>
                        </td>
                        <td align="left" class="matters" colspan="2"><span class="field-label">Date</span> &nbsp;&nbsp;
                            <asp:TextBox ID="txtChqDate" runat="server" TabIndex="91"></asp:TextBox>
                            <asp:ImageButton ID="imgChequedate" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="92" />
                        </td>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnAddCheque" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="93" OnClick="btnAddCheque_Click" />
                        </td>
                    </tr>
                </table>
                <table class="BlueTable_simple" align="center" width="100%">

                    <tr>
                        <td align="center" colspan="6">
                            <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                CssClass="table table-row table-bordered" DataKeyNames="REF_ID,PayMode">
                                <Columns>
                                    <asp:BoundField DataField="REF_DESCR" HeaderText="Descr" ReadOnly="True"></asp:BoundField>
                                    <asp:BoundField DataField="REF_NO" HeaderText="Ref No." ReadOnly="True"></asp:BoundField>
                                    <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CrAmount" DataFormatString="{0:0.00}" HeaderText="Amount">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CrCharge" DataFormatString="{0:0.00}" HeaderText="Charge">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CrTotal" DataFormatString="{0:0.00}" HeaderText="Total">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FCAmount" DataFormatString="{0:0.00}" HeaderText="Paid(FC)"
                                        ReadOnly="True" Visible="false">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AMOUNT" DataFormatString="{0:0.00}" HeaderText="Paid"
                                        ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="TAX Code" DataField="TAX_CODE" />
                                    <asp:BoundField HeaderText="TAX Amount" DataField="TAX_AMOUNT" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField HeaderText="Net Amount" DataField="TAX_NET_AMOUNT" DataFormatString="{0:0.00}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>

                        <td align="right" colspan="6">
                            <table style="width: 25% !important;">
                                <tr>
                                    <td rowspan="4">
                                        <span class="field-label">Total</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="50%"><span class="field-label">Sub Total</span></td>
                                    <td align="left" class="matters">
                                        <asp:Label ID="lblSubTotal" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trVATTotal">
                                    <td align="left" class="matters"><span class="field-label">Tax Amount</span></td>
                                    <td align="left" class="matters">
                                        <asp:Label ID="lblTotalVATAmount" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Net Total</span></td>
                                    <td align="left" class="matters">
                                        <asp:Label ID="lblTotalNETAmount" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </table>
                            <asp:TextBox ID="txtReceivedTotal" Style="text-align: right" runat="server" Visible="false"></asp:TextBox>
                            <asp:HiddenField ID="hfCobrand" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button
                                ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" /><asp:Button
                                    ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                    SelectCommand="SELECT [EMR_CODE], [EMR_DESCR] FROM [EMIRATE_M]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="GetCreditCardListForCollection" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="CollectFrom" DefaultValue="FEES" Type="String" />
                        <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sdsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="SELECT DISTINCT [COL_ID], [COL_DESCR], [COL_ACT_ID] FROM [COLLECTION_M] WITH(NOLOCK) WHERE ([COL_ID] IN (SELECT DISTINCT OFM_COL_ID FROM OASIS_FEES.DAX.OTH_FEECOLLECTION_MAPPING WITH(NOLOCK))) AND ([COL_ID] <> 1)"></asp:SqlDataSource>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_Bank" runat="server" />
                <asp:HiddenField ID="h_BSU_ID" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="h_OLD_STU_IDs" runat="server" />
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="hf_OFM_ID" runat="server" />
                <asp:HiddenField ID="hf_VDR_CODE" runat="server" />
                <asp:HiddenField ID="hf_bCashFlowRequired" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
                <asp:HiddenField ID="hf_ctrl" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calCheque" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgChequedate" TargetControlID="txtChqDate" Format="dd/MMM/yyyy"
                    Enabled="True" PopupPosition="TopLeft">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>


        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>
