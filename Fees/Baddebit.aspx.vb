Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GemBox.Spreadsheet

Partial Class Fees_Baddebit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass
    Dim AegAmount As Double
    Dim SetAmount As Double
    Dim SerialNo As Int32

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            'ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBSuid")
            Dim USR_NAME As String = Session("sUsr_name")
            InitialiseCompnents()
            lnkXcelFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("SAMPLEFILE") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("BadDebtsExcelFilePath").ToString() & "/Template/SetBadDebtsProvision.xls")
        End If
    End Sub

    Sub bindData()
        If txtAigingDays.Text = "" Then
            'lblError.Text = "Please enter Ageing days..!"
            usrMessageBar.ShowNotification("Please enter Ageing days..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ASONDT", SqlDbType.VarChar)
        pParms(0).Value = txtFrom.Text
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(1).Value = ddlBSUnit.SelectedItem.Value
        pParms(2) = New SqlClient.SqlParameter("@Bkt1", SqlDbType.Int)
        pParms(2).Value = txtAigingDays.Text
        pParms(3) = New SqlClient.SqlParameter("@InActive", SqlDbType.Bit)
        pParms(3).Value = chkInActive.Checked

        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "[FEES].[BadDebit_Provision]", pParms)
        BindDataGird(_table.Tables(0))


    End Sub

    Sub BindDataGird(ByVal _table As DataTable)

        gvStudentDetails.DataSource = _table
        gvStudentDetails.DataBind()
        If _table.Rows.Count > 0 Then

            'txtDAmount = gvStudentDetails.Row.FindControl("txtDAmount")
            '' txtDAmount.Attributes.Add("OnKeyup", "return   Numeric_Only()")
            lbltotaldue.Text = Convert.ToDouble(_table.Compute("Sum(BDP_AMOUNT)", "")).ToString("##,#.00")

            divDetails.Visible = True
            lbltotalstudents.Text = _table.Rows.Count
            gvStudentDetails.FooterRow.Visible = False
            Dim txtPrvAmount As New TextBox
            txtPrvAmount = gvStudentDetails.FooterRow.FindControl("txtPrvAmount")
            txtPrvAmount.Text = If(SetAmount = 0 = True, "0.00", SetAmount.ToString("##,#.00"))
            txtPrvAmount.Style("text-align") = "right"
            gvStudentDetails.FooterRow.Cells(6).Text = AegAmount.ToString("##,#.00")
            gvStudentDetails.FooterRow.Cells(6).HorizontalAlign = HorizontalAlign.Right
            'gvStudentDetails.FooterRow.Cells(6).Text = SetAmount.ToString("##,#.000")
            txtPrvAmount.Attributes.Add("Readonly", "Readonly")
            lbltotaldue.Text = txtPrvAmount.Text
            'gvStudentDetails.Columns(0).Visible = False
            ViewState("gvStudentDetails") = _table
        End If

    End Sub

    Sub InitialiseCompnents()
        gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        'txtAigingDays.Attributes.Add("onkeypress", " return Numeric_Only() ")
        BindBusinessUnit()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If gvStudentDetails.Rows.Count = 0 Then
                'lblError.Text = "Please Select Students..!"
                usrMessageBar.ShowNotification("Please Select Students..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            doInsert()

        Catch ex As Exception
            'lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
        End Try
    End Sub
    Private Sub doInsert()
        Dim StudentId As String = ""
        Dim AmountS As String = ""
        Dim Slno As String = ""
        Dim txtDAmount As New TextBox


        Dim dt As New DataTable()

        'For Each cell As TableCell In gvStudentDetails.HeaderRow.Cells
        '    dt.Columns.Add(cell.Text)
        'Next
        dt = ViewState("gvStudentDetails")
        'ViewState("gvStudentDetails") = Nothing
        For Each row As GridViewRow In gvStudentDetails.Rows
            txtDAmount = row.FindControl("txtDAmount")
            If Not txtDAmount Is Nothing Then
                dt.Rows(row.RowIndex)("BDP_AMOUNT") = FeeCollection.GetDoubleVal(txtDAmount.Text)
            End If
            'dt.Rows.Add()
            'For i As Integer = 0 To row.Cells.Count - 1
            '    dt.Rows(row.RowIndex)(i) = If(row.Cells(i).Text = "&nbsp;", "", If(i = 7, txtDAmount.Text, row.Cells(i).Text))
            'Next
        Next


        'dt.Columns(0).ColumnName = "STU_NO"
        'dt.Columns(1).ColumnName = "SNO"
        'dt.Columns(2).ColumnName = "STU_ID"
        'dt.Columns(3).ColumnName = "STU_NAME"
        'dt.Columns(4).ColumnName = "GRADE"
        'dt.Columns(5).ColumnName = "LEAVEDATE"
        'dt.Columns(6).ColumnName = "DUE"
        'dt.Columns(7).ColumnName = "PROVISION"
        If (dt.Columns.Contains("SLNO")) Then
            dt.Columns.Remove("SLNO")
        End If
        If (dt.Columns.Contains("STU_NAME")) Then
            dt.Columns.Remove("STU_NAME")
        End If
        If (dt.Columns.Contains("GRM_DISPLAY")) Then
            dt.Columns.Remove("GRM_DISPLAY")
        End If
        If (dt.Columns.Contains("STU_ID")) Then
            dt.Columns.Remove("STU_ID")
        End If
        If (dt.Columns.Contains("STU_LEAVEDATE")) Then
            dt.Columns.Remove("STU_LEAVEDATE")
        End If


        Dim TempTD As DataTable = dt.Select("BDP_AMOUNT > 0").CopyToDataTable()
        dt = TempTD

        dt.Columns("STU_NO").SetOrdinal(0)
        dt.Columns("AGEDUE").SetOrdinal(1)
        dt.Columns("BDP_AMOUNT").SetOrdinal(2)

        Dim iParms(4) As SqlParameter

        iParms(0) = Mainclass.CreateSqlParameter("@BDP_DATE", Convert.ToDateTime(txtFrom.Text.ToString()).ToString("dd-MMM-yyyy"), SqlDbType.DateTime)
        iParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSUnit.SelectedItem.Value, SqlDbType.VarChar)
        iParms(2) = New SqlClient.SqlParameter("@DT_XL", dt)
        iParms(2).Value = dt
        iParms(3) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        iParms(4).Direction = ParameterDirection.ReturnValue

        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim p_stTrans As SqlTransaction
        conn.Open()
        p_stTrans = conn.BeginTransaction()
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.SAVE_BADDEBT_PROV_BULK", iParms)
        Dim ReturnFlag As Integer = iParms(4).Value

        If (ReturnFlag = 0) Then
            dt.Dispose()
            ViewState("gvStudentDetails") = Nothing
            p_stTrans.Commit()
            gvStudentDetails.DataSource = Nothing
            gvStudentDetails.DataBind()
            usrMessageBar.ShowNotification("Data Successfully updated", UserControls_usrMessageBar.WarningType.Success)
            lbltotalstudents.Text = ""
            lbltotaldue.Text = ""
            divDetails.Visible = False
        Else
            p_stTrans.Rollback()
            usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
        End If

    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    'Protected Sub  (ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    '    If (e.Row.Cells.Count = 1) Then
    '        Return
    '    End If
    '    Dim txtDAmount As New TextBox

    '    If e.Row.RowType = DataControlRowType.DataRow And Not String.IsNullOrEmpty(e.Row.Cells(6).Text) And e.Row.Cells(6).Text <> "&nbsp;" Then
    '        txtDAmount = e.Row.FindControl("txtDAmount")
    '        ' txtDAmount.Attributes.Add("OnKeyup", "return   Numeric_Only()")
    '        ' txtDAmount.Attributes.Add("ondblclick", "return   valueCopy('" & e.Row.Cells(6).Text & "','" & txtDAmount.ClientID & "')")
    '        'If Convert.ToDouble(txtDAmount.Text) = 0 Then
    '        '    txtDAmount.Text = e.Row.Cells(6).Text
    '        '    txtDAmount.Text = e.Row.Cells(6).Text
    '        'Else
    '        '    e.Row.BackColor = Drawing.Color.BurlyWood
    '        'End If

    '        'txtDAmount.Attributes.Add("onfocusout", "return UpdateSum()")
    '        AegAmount += Convert.ToDouble(e.Row.Cells(6).Text)

    '        SetAmount += Convert.ToDouble(txtDAmount.Text)

    '        If (hdnfield.Value = True) Then
    '            txtDAmount.Enabled = False
    '            SerialNo = SerialNo + 1
    '            e.Row.Cells(1).Text = SerialNo
    '        End If
    '    End If
    '    If e.Row.Cells.Count > 1 Then
    '        e.Row.Cells(0).Style("display") = "none"
    '    End If
    'End Sub

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "BSU_NAME"
        ddlBSUnit.DataValueField = "BSU_ID"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindData()
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        bindData()
    End Sub

    Protected Sub btnuploadfile_Click(sender As Object, e As EventArgs)

        hdnfield.Value = True
        ImportAdjustmentSheet()
        hdnfield.Value = False
        ClearControls(False)

    End Sub
    Private Sub ImportAdjustmentSheet()
        Try
            lblError.Text = ""
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = Web.Configuration.WebConfigurationManager.AppSettings("BadDebtsExcelFilePath") & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If
            Else
                usrMessageBar.ShowNotification("Upload the file", UserControls_usrMessageBar.WarningType.Danger)
            End If
            If File.Exists(FileName) Then
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                'Dim ef As New GemBox.Spreadsheet.ExcelFile
                Dim workbook = ExcelFile.Load(FileName)
                ' Select active worksheet.
                Dim worksheet = workbook.Worksheets.ActiveWorksheet
                Dim dtXL As New DataTable
                If worksheet.Rows.Count > 1 Then

                    dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With
                            {
                             .ColumnHeaders = True,
                             .StartRow = 0,
                             .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count,
                             .NumberOfRows = worksheet.Rows.Count,
                             .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                            })
                End If
                ' File delete first 
                File.Delete(FileName)

                If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then

                    ' eliminate data which has stu_no empty   
                    Dim DtExcel As DataTable = dtXL.Select("STU_NO <> ''").CopyToDataTable()
                    dtXL = DtExcel
                    divDetails.Visible = True
                    Dim DT As New DataTable

                    If (dtXL.Columns.Contains("SLNO") = False) Then
                        Dim SLNO_COLUMN As New Data.DataColumn("SLNO", GetType(Int64))
                        dtXL.Columns.Add(SLNO_COLUMN)

                    End If
                    Dim STU_ID_COLUMN As New Data.DataColumn("STU_ID", GetType(Int64))
                    dtXL.Columns.Add(STU_ID_COLUMN)

                    'Dim STU_NO_COLUMN As New Data.DataColumn("STU_NO", GetType(System.String))
                    'DT.Columns.Add(STU_NO_COLUMN)

                    'Dim STU_NAME_COLUMN As New Data.DataColumn("STU_NAME", GetType(System.String))
                    'DT.Columns.Add(STU_NAME_COLUMN)


                    Dim GRM_DISPLAY_COLUMN As New Data.DataColumn("GRM_DISPLAY", GetType(System.String))
                    dtXL.Columns.Add(GRM_DISPLAY_COLUMN)

                    Dim AGEDUE_COLUMN As New Data.DataColumn("AGEDUE", GetType(System.Double))
                    AGEDUE_COLUMN.DefaultValue = "0.00"
                    dtXL.Columns.Add(AGEDUE_COLUMN)

                    'Dim BDP_AMOUNT_COLUMN As New Data.DataColumn("BDP_AMOUNT", GetType(System.Double))

                    'DT.Columns.Add(BDP_AMOUNT_COLUMN)

                    Dim STU_LEAVEDATE_COLUMN As New Data.DataColumn("STU_LEAVEDATE", GetType(System.String))
                    dtXL.Columns.Add(STU_LEAVEDATE_COLUMN)

                    dtXL.Columns("BAD_DEBT").ColumnName = "BDP_AMOUNT"
                    'Dim MergeDT As DataTable = DT.Copy()
                    'MergeDT.Merge(dtXL)
                    dtXL.AcceptChanges()
                    BindDataGird(dtXL)
                    lbltotalstudents.Text = dtXL.Rows.Count
                    lbltotaldue.Text = Convert.ToDouble(dtXL.Compute("Sum(BDP_AMOUNT)", "")).ToString("##,#.00")
                Else
                    gvStudentDetails.DataSource = Nothing
                    gvStudentDetails.DataBind()
                End If
                'SAVE_SFTP_LOG("IMPORTDATA", LocalFilePath, "SUCCESS")
            End If
        Catch ex As Exception
            'SAVE_SFTP_LOG("IMPORTDATA", LocalFilePath, "FAILED", ex.InnerException.Message)
            Errorlog("Unable to load datatable from file - ImportAdjustmentSheet()")

        End Try
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs)
        ClearControls(True)
        gvStudentDetails.DataSource = Nothing
        gvStudentDetails.DataBind()
        lbltotalstudents.Text = ""
        lbltotaldue.Text = ""
        divDetails.Visible = False
        txtAigingDays.Text = ""


    End Sub

    Private Sub ClearControls(ByVal Enabled As Boolean)
        txtAigingDays.Enabled = Enabled
        txtFrom.Enabled = Enabled
        ddlBSUnit.Enabled = Enabled
        chkInActive.Enabled = Enabled
        chkInActive.Enabled = Enabled
        btnFind.Enabled = Enabled
        imgFrom.Enabled = Enabled
    End Sub
End Class


