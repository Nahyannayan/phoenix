Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeOtherFeeCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Session("BSU_IsOnDAX") = 1 Then
                Response.Redirect("feeOtherFeeCollection_DAX.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add"))
            End If
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBSuid")
            Dim USR_NAME As String = Session("sUsr_name")
            bind_Currency()
            InitialiseCompnents()
            BindEmirate()

            '------------------------
            Dim sqlStr As String
            sqlStr = "select isnull(BSU_bFeesMulticurrency,0) from BUSINESSUNIT_M where bsu_id='" & Session("sbsuid") & "'"
            If Mainclass.getDataValue(sqlStr, "OASISConnectionString") Then
                pnlCurrency.Visible = True
                trCurrency.Visible = True
            Else
                pnlCurrency.Visible = False
                trCurrency.Visible = False
            End If
            '------------------------
            Dim formatstring As String = Session("BSU_DataFormatString")
            formatstring = formatstring.Replace("0.", "###,###,###,##0.")
            DirectCast(gvFeeCollection.Columns(3), BoundField).DataFormatString = "{0:" & formatstring & "}"
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code").ToString <> OASISConstants.MNU_FEE_COLLECTION_OTHER _
                            And ViewState("MainMnu_code").ToString <> "F300255") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            Dim qry As String = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM CREDITCARD_S INNER JOIN " & _
                                " CREDITCARD_M ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
                                " CREDITCARD_PROVD_M ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
                                " WHERE     (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
            Me.hfCobrand.Value = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)
            'FillEvents()
        End If
    End Sub

    Sub BindEmirate()
        Try
            Dim str_default_emirate As String = GetDataFromSQL("SELECT BSU_CITY FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)
            If str_default_emirate <> "--" Then
                ddlEmirate.SelectedIndex = -1 
                ddlEmirate.Items.FindByValue(str_default_emirate).Selected = True 
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "emirate")
        End Try
    End Sub
    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            ddCurrency.Items.Clear()
            ddCurrency.DataSource = MasterFunctions.GetExchangeRates(Now.Date.ToString("dd/MMM/yyyy"), Session("sBsuid"), Session("BSU_CURRENCY"))
            ddCurrency.DataTextField = "EXG_CUR_ID"

            ddCurrency.DataValueField = "RATES"
            ddCurrency.DataBind()
            If ddCurrency.Items.Count > 0 Then
                Dim lstDrp As New ListItem
                lstDrp = ddCurrency.Items.FindByText(Session("BSU_CURRENCY"))
                If Not lstDrp Is Nothing Then
                    ddCurrency.SelectedValue = lstDrp.Value
                End If
                lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCurrency.SelectedIndexChanged
        lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
        'Gridbind_Feedetails()
        Session("FeeCollection") = FeeCollectionOther.CreateFeeCollectionOther()
        Me.gvFeeCollection.DataSource = ""
        Me.gvFeeCollection.DataBind()
        Me.txtReceivedTotal.Text = "0"
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") Then
            gvFeeCollection.Columns(5).Visible = True

            gvFeeCollection.Columns(5).HeaderText = "Paying Now (" & ddCurrency.SelectedItem.Text & ")"
            'lblOutstandingFC.Visible = True
            'lblDueFC.Visible = True
        Else
            gvFeeCollection.Columns(5).Visible = False
            'lblOutstandingFC.Visible = False
            'lblDueFC.Visible = False
        End If

    End Sub
    Sub InitialiseCompnents() 
        txtReceivedTotal.Attributes.Add("readonly", "readonly")
        txtCrCharge.Attributes.Add("readonly", "readonly")
        'txtCashTotal.Attributes.Add("onKeyPress", "allownumber();")
        ViewState("BSU_IsOnDAX") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BSU_IsOnDAX, 0) AS BSU_IsOnDAX FROM [dbo].[BUSINESSUNIT_M] WITH(NOLOCK) WHERE BSU_ID='" & Session("sBsuId") & "'")
        ddCollection.DataBind()
        If Not ddCollection.Items.FindByValue("5") Is Nothing Then
            ddCollection.SelectedIndex = -1
            ddCollection.Items.FindByValue("5").Selected = True
        End If
        ddCollection_SelectedIndexChanged(Nothing, Nothing)
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqDate.Text = txtFrom.Text  
        ddlEmirate.DataBind() 
        Session("FeeCollection") = FeeCollectionOther.CreateFeeCollectionOther()
        ddlPaymentMode.Items.Add(New ListItem("Cash", COLLECTIONTYPE.CASH))
        ddlPaymentMode.Items.Add(New ListItem("Cheque", COLLECTIONTYPE.CHEQUES))
        ddlPaymentMode.Items.Add(New ListItem("Credit Card", COLLECTIONTYPE.CREDIT_CARD))
        Set_CollectionControls()
        gvFeeCollection.DataBind()
        txtRemarks.Text = ddCollection.SelectedItem.Text.ToString() & "  For : " & txtFrom.Text.ToString()
    End Sub

    'Private Sub FillEvents()
    '    If CBool(ViewState("BSU_IsOnDAX")) Then
    '        Me.trEvents.Visible = True
    '        Dim dtEvents As DataTable = FeeCommon.Get_Events_and_Activities(Session("sBSUID"), ddCollection.SelectedValue, txtDAccountCode.Text, Me.txtFrom.Text)
    '        ddlEvents.DataSource = dtEvents
    '        ddlEvents.DataTextField = "EVT_DESCR"
    '        ddlEvents.DataValueField = "EVT_ID"
    '        ddlEvents.DataBind()
    '        'For Each rowACD As DataRow In dtEvents.Rows
    '        '    If rowACD("ACD_CURRENT") Then
    '        '        ddlEvents.Items.FindByValue(rowACD("ACD_ID")).Selected = True
    '        '        Exit For
    '        '    End If
    '        'Next
    '    End If
    'End Sub
    Sub Clear_All()
        txtCCTotal.Text = "0.00"
        txtCrCharge.Text = "0.00"
        Me.hfCobrand.Value = ""
        txtChequeTotal.Text = ""
        txtCashTotal.Text = ""
        txtReceivedTotal.Text = ""
        txtRefno.Text = ""
        txtCreditno.Text = ""
        txtBank.Text = ""
        h_Bank.Value = ""
        txtChqno.Text = ""
        txtChqDate.Text = txtFrom.Text
        txtDAccountCode.Text = ""
        txtDAccountName.Text = ""
        txtChequeTotal.Text = ""
        txtRemarks.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        If Not ddCollection.Items.FindByValue("5") Is Nothing Then
            ddCollection.SelectedIndex = -1
            ddCollection.Items.FindByValue("5").Selected = True
        End If
    End Sub

    Sub Clear_Details()
        txtCCTotal.Text = "0.00"
        txtCrCharge.Text = "0.00"
        txtChargeTotal.Text = "0.00"
        txtChequeTotal.Text = "0.00"
        txtCashTotal.Text = "0.00"
        txtCreditno.Text = ""
        txtBank.Text = ""
        h_Bank.Value = ""
        txtChqno.Text = ""
        txtChqDate.Text = txtFrom.Text
        txtChequeTotal.Text = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Set_GridTotal()
        Dim str_error As String = check_errors_details()
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
       
        'If dtFrom > Now.Date Then
        '    str_error = str_error &"Invalid date(Future Date)!!!" 
        'End If
        'If gvFeeCollection.Rows.Count = 0 Then
        '    str_error = str_error & "Please add fee details<br />"
        'End If
        Dim dblReceivedAmount As Decimal
        dblReceivedAmount = CDbl(txtReceivedTotal.Text)
        'dblTotal = CDbl(txtTotal.Text)

         
        If Session("FeeCollection").Rows.Count = 0 Then
            str_error = str_error & "Please enter details<br />"
        End If
        If str_error <> "" Then
            'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim str_new_FOC_ID As Integer
        Dim str_NEW_FOC_RECNO As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim STR_TYPE As Char = "S"
            retval = FeeCommon.CheckFeeclose(Session("sBsuid"), txtFrom.Text, stTrans)
            Dim XRate As Decimal
            Dim Currency As String, FOC_EVT_ID As String = ""
            'If ddlEvents.Items.Count > 0 Then
            '    FOC_EVT_ID = ddlEvents.SelectedValue
            'Else
            '    FOC_EVT_ID = ""
            'End If
            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                XRate = Me.lblExgRate.Text
                Currency = ddCurrency.SelectedItem.Text
            Else
                XRate = 1
                Currency = Session("BSU_CURRENCY")
            End If
            If retval = "0" Then
                retval = FeeCollectionOther.F_SaveFEEOTHCOLLECTION_H(0, txtRefno.Text, txtFrom.Text, _
                   Session("sBsuid"), txtReceivedTotal.Text, Session("sUsr_name"), ddlPaymentMode.SelectedItem.Value, _
                 False, txtRemarks.Text, "CR", False, ddCollection.SelectedItem.Value, txtDAccountCode.Text, _
                 str_NEW_FOC_RECNO, str_new_FOC_ID, stTrans, objConn, Currency, XRate, FOC_EVT_ID)
            End If

            If retval = "0" Then
                For I As Integer = 0 To Session("FeeCollection").Rows.Count - 1
                    Dim FCD_AMount As Double
                    If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                        FCD_AMount = Session("FeeCollection").Rows(I)("FCAmount")
                    Else
                        FCD_AMount = Session("FeeCollection").Rows(I)("Amount")
                    End If
                    retval = FeeCollectionOther.F_SaveFEEOTHCOLLSUB_D(0, str_new_FOC_ID, Session("FeeCollection").Rows(I)("REF_ID"), _
                     Session("FeeCollection").Rows(I)("EMR_ID"), Session("FeeCollection").Rows(I)("Amount"), _
                     Session("FeeCollection").Rows(I)("REF_NO"), Session("FeeCollection").Rows(I)("Date"), _
                      0, "", False, stTrans, objConn, FCD_AMount, Session("FeeCollection").Rows(I)("CrCharge"))
                    If retval <> "0" Then
                        Exit For
                    End If
                Next
            End If

            If retval = "0" Then
                stTrans.Commit()
                h_print.Value = "print"
                ViewState("recno") = str_NEW_FOC_RECNO
                Session("FeeCollection").rows.clear()

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FOC_RECNO, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                PrintReceipt(str_NEW_FOC_RECNO)
                'lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                Clear_All()
                gvFeeCollection.DataBind()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub Set_GridTotal()
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim txtAmountToPayFC As TextBox = CType(gvr.FindControl("txtAmountToPayFC"), TextBox)
            If Not txtAmountToPay Is Nothing Then
                If Not IsNumeric(txtAmountToPay.Text) Then
                    txtAmountToPay.Text = "0"
                End If
                If gvr.RowIndex < Session("FeeCollection").rows.count Then
                    Session("FeeCollection").rows(gvr.RowIndex)(("Amount")) = txtAmountToPay.Text
                End If
            End If
            If Not txtAmountToPayFC Is Nothing Then
                If Not IsNumeric(txtAmountToPayFC.Text) Then
                    txtAmountToPayFC.Text = "0"
                End If
                If gvr.RowIndex < Session("FeeCollection").rows.count Then
                    Session("FeeCollection").rows(gvr.RowIndex)(("FCAmount")) = txtAmountToPayFC.Text
                End If
            End If
        Next

        Dim dAmount As Decimal = 0
        Dim dFCAmount As Decimal = 0
        For i As Integer = 0 To Session("FeeCollection").Rows.Count - 1
            dAmount = dAmount + Session("FeeCollection").Rows(i)("Amount")
            dFCAmount = dFCAmount + Session("FeeCollection").Rows(i)("FCAmount")
        Next
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
            txtReceivedTotal.Text = Format(dFCAmount, Session("BSU_DataFormatString"))
        Else
            txtReceivedTotal.Text = Format(dAmount, Session("BSU_DataFormatString"))
        End If

        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
    End Sub

    Function check_errors_details() As String
        Dim str_error As String = ""

        If txtRemarks.Text.Trim = "" Then
            str_error = str_error & "Enter Remarks <br />"
        End If
       
        txtDAccountName.Text = AccountFunctions.Validate_Account(txtDAccountCode.Text, Session("sbsuid"), "NOTCC")
        If txtDAccountName.Text = "" Then
            str_error = str_error & "Invalid Account Selected in Details<br />"
        End If
        Return str_error
    End Function

    Protected Sub PrintReceipt(ByVal p_Receiptno As String)
        Dim FC As Boolean = False
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
            FC = True
        End If
        Session("ReportSource") = FeeCollectionOther.PrintReceipt(p_Receiptno, Session("sUsr_name"), Session("sBsuid"), _
        ConnectionManger.GetOASIS_FEESConnectionString)
    End Sub

    Protected Sub txtBank_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank.Text = FeeCommon.GetBankName(txtBank.Text, str_bankid, str_bankho)
        h_Bank.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate)
            If str_bankho <> "" Then
                ddlEmirate.SelectedIndex = -1
                ddlEmirate.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank)
            'lblError.Text = "Invalid Bank Selected"
            usrMessageBar.ShowNotification("Invalid Bank Selected", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Protected Sub ddlPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_CollectionControls()
    End Sub

    Sub Set_CollectionControls()
        Session("FeeCollection").rows.clear()
        Set_GridTotal()
        'gvFeeCollection.DataBind()
        Select Case ddlPaymentMode.SelectedItem.Value
            Case COLLECTIONTYPE.CASH
                tr_Cash.Visible = True
                tr_Cheque1.Visible = False
                tr_Cheque2.Visible = False
                tr_CreditCard.Visible = False
                gvFeeCollection.Columns(0).Visible = False
                gvFeeCollection.Columns(1).Visible = False
                gvFeeCollection.Columns(3).Visible = False
                gvFeeCollection.Columns(4).Visible = False
                gvFeeCollection.Columns(5).Visible = False
            Case COLLECTIONTYPE.CHEQUES
                tr_Cash.Visible = False
                tr_Cheque1.Visible = True
                tr_Cheque2.Visible = True
                tr_CreditCard.Visible = False
                gvFeeCollection.Columns(0).Visible = True
                gvFeeCollection.Columns(1).Visible = True
                gvFeeCollection.Columns(3).Visible = False
                gvFeeCollection.Columns(4).Visible = False
                gvFeeCollection.Columns(5).Visible = False
            Case COLLECTIONTYPE.CREDIT_CARD
                tr_Cash.Visible = False
                tr_Cheque1.Visible = False
                tr_Cheque2.Visible = False
                tr_CreditCard.Visible = True
                gvFeeCollection.Columns(0).Visible = True
                gvFeeCollection.Columns(1).Visible = True
                gvFeeCollection.Columns(3).Visible = True
                gvFeeCollection.Columns(4).Visible = True
                gvFeeCollection.Columns(5).Visible = True
        End Select
    End Sub

    Protected Sub BtnCashAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCash.Click
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtCashTotal.Text) Then
            If CDbl(txtCashTotal.Text) > 0 Then
                If ViewState("ID") Is Nothing Then
                    ViewState("ID") = 1
                Else
                    ViewState("ID") = ViewState("ID") + 1
                End If
                dr = Session("FeeCollection").NewRow
                dr("ID") = ViewState("ID")
                dr("PayMode") = ddlPaymentMode.SelectedValue
                dr("REF_ID") = ""
                dr("REF_NO") = ""
                dr("REF_DESCR") = ""
                dr("EMR_ID") = ""
                dr("Date") = ""
                dr("Amount") = txtCashTotal.Text
                dr("FCAmount") = 0
                dr("CrCharge") = 0
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    dr("FCAmount") = txtCashTotal.Text
                    dr("Amount") = Format(txtCashTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                End If
                Session("FeeCollection").rows.add(dr)
                Clear_Details()
                Set_GridTotal()
                smScriptManager.SetFocus(txtCashTotal)
            Else
                'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtCashTotal)
            End If
        Else
            'lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtCashTotal)
        End If
    End Sub

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub
    Protected Sub txtAmountToPayFC_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub

    Protected Sub btnAddCreditcard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCreditcard.Click
        lblError.Text = ""
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtCreditno.Text.Trim = "" Then
            'lblError.Text = "Invalid Auth. Code!!!"
            usrMessageBar.ShowNotification("Invalid Auth. Code!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtCCTotal.Text) Then
            If CDbl(txtCCTotal.Text) > 0 Then
                If ViewState("ID") Is Nothing Then
                    ViewState("ID") = 1
                Else
                    ViewState("ID") = ViewState("ID") + 1
                End If
                dr = Session("FeeCollection").NewRow
                dr("ID") = ViewState("ID")
                dr("PayMode") = ddlPaymentMode.SelectedValue
                dr("REF_ID") = ddCreditcard.SelectedItem.Value
                dr("REF_NO") = txtCreditno.Text
                dr("REF_DESCR") = ddCreditcard.SelectedItem.Text
                dr("EMR_ID") = ""
                dr("Date") = ""
                dr("CrAmount") = Val(Me.txtCCTotal.Text)
                dr("CrCharge") = Val(Me.txtCrCharge.Text)
                dr("CrTotal") = Val(Me.txtChargeTotal.Text)
                dr("Amount") = Val(Me.txtChargeTotal.Text)
                dr("FCAmount") = 0
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    dr("FCAmount") = Val(Me.txtChargeTotal.Text)
                    dr("Amount") = Format(Val(Me.txtChargeTotal.Text) * Val(lblExgRate.Text), Session("BSU_DataFormatString"))
                End If
                Session("FeeCollection").rows.add(dr)
                Clear_Details()
                Set_GridTotal()
                smScriptManager.SetFocus(txtCCTotal)
            Else
                'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtCCTotal)
            End If
        Else
            'lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtCashTotal)
        End If
    End Sub

    Protected Sub btnAddCheque_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtChqno.Text.Trim = "" Then
            'lblError.Text = "Invalid Cheque No!!!"
            usrMessageBar.ShowNotification("Invalid Cheque No!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtBank.Text.Trim = "" Or h_Bank.Value.Trim = "" Then
            'lblError.Text = "Invalid Bank!!!"
            usrMessageBar.ShowNotification("Invalid Bank!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not IsDate(txtChqDate.Text) Then
            'lblError.Text = "Invalid Cheque Date!!!"
            usrMessageBar.ShowNotification("Invalid Cheque Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtChequeTotal.Text) Then
            If CDbl(txtChequeTotal.Text) > 0 Then
                If ViewState("ID") Is Nothing Then
                    ViewState("ID") = 1
                Else
                    ViewState("ID") = ViewState("ID") + 1
                End If
                dr = Session("FeeCollection").NewRow
                dr("ID") = ViewState("ID")
                dr("PayMode") = ddlPaymentMode.SelectedValue
                dr("REF_ID") = h_Bank.Value
                dr("REF_NO") = txtChqno.Text
                dr("REF_DESCR") = txtBank.Text
                dr("EMR_ID") = ddlEmirate.SelectedItem.Value
                dr("Date") = txtChqDate.Text
                dr("Amount") = txtChequeTotal.Text
                dr("FCAmount") = 0
                dr("CrCharge") = 0
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    dr("FCAmount") = txtChequeTotal.Text
                    dr("Amount") = Format(txtChequeTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                End If
                Session("FeeCollection").rows.add(dr)
                Clear_Details()
                Set_GridTotal()
                smScriptManager.SetFocus(txtChequeTotal)
            Else
                'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtChequeTotal)
            End If
        Else
            'lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtChequeTotal)
        End If
    End Sub

    Protected Sub gvFeeCollection_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvFeeCollection.RowCancelingEdit
        gvFeeCollection.EditIndex = -1
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
        Select Case ddlPaymentMode.SelectedItem.Value
            Case COLLECTIONTYPE.CASH
                btnAddCash.Visible = True
            Case COLLECTIONTYPE.CHEQUES
                btnAddCheque.Visible = True
            Case COLLECTIONTYPE.CREDIT_CARD
                btnAddCreditcard.Visible = True
        End Select
    End Sub

    Protected Sub gvFeeCollection_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvFeeCollection.RowEditing
        gvFeeCollection.EditIndex = e.NewEditIndex
        Dim row As GridViewRow = gvFeeCollection.Rows(e.NewEditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("FeeCollection").Rows.Count - 1
            If iIndex = Session("FeeCollection").Rows(iEdit)(0) Then
                '"  "REF_ID" "REF_NO", "EMR_ID" "Date", "Amount",
                Select Case ddlPaymentMode.SelectedItem.Value
                    Case COLLECTIONTYPE.CASH
                        btnAddCash.Visible = False
                        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                            txtCashTotal.Text = Session("FeeCollection").Rows(iEdit)("FCAmount")
                        Else
                            txtCashTotal.Text = Session("FeeCollection").Rows(iEdit)("Amount")
                        End If

                    Case COLLECTIONTYPE.CHEQUES
                        btnAddCheque.Visible = False
                        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                            txtChequeTotal.Text = Session("FeeCollection").Rows(iEdit)("FCAmount")
                        Else
                            txtChequeTotal.Text = Session("FeeCollection").Rows(iEdit)("Amount")
                        End If

                        h_Bank.Value = Session("FeeCollection").Rows(iEdit)("REF_ID")
                        txtBank.Text = Session("FeeCollection").Rows(iEdit)("REF_DESCR")
                        txtChqDate.Text = Session("FeeCollection").Rows(iEdit)("Date")
                        txtChqno.Text = Session("FeeCollection").Rows(iEdit)("REF_NO")
                        ddlEmirate.SelectedIndex = -1
                        ddlEmirate.Items.FindByValue(Session("FeeCollection").Rows(iEdit)("EMR_ID")).Selected = True
                    Case COLLECTIONTYPE.CREDIT_CARD
                        btnAddCreditcard.Visible = False
                        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                            txtCCTotal.Text = Session("FeeCollection").Rows(iEdit)("FCAmount")

                        Else
                            txtChargeTotal.Text = Session("FeeCollection").Rows(iEdit)("Amount")
                            txtCCTotal.Text = Session("FeeCollection").Rows(iEdit)("CrAmount")
                            txtCrCharge.Text = Session("FeeCollection").Rows(iEdit)("CrCharge")

                        End If
                        ddCreditcard.SelectedIndex = -1
                        ddCreditcard.Items.FindByValue(Session("FeeCollection").Rows(iEdit)("REF_ID")).Selected = True
                        txtCreditno.Text = Session("FeeCollection").Rows(iEdit)("REF_NO")
                End Select
                Exit For
            End If
        Next
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
    End Sub

    Protected Sub gvFeeCollection_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvFeeCollection.RowUpdating
        Dim row As GridViewRow = gvFeeCollection.Rows(gvFeeCollection.EditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("FeeCollection").Rows.Count - 1
            If iIndex = Session("FeeCollection").Rows(iEdit)(0) Then
                '"  "REF_ID" "REF_NO", "EMR_ID" "Date", "Amount",
                Select Case ddlPaymentMode.SelectedItem.Value
                    Case COLLECTIONTYPE.CASH
                        If IsNumeric(txtCashTotal.Text) AndAlso CDbl(txtCashTotal.Text) > 0 Then
                            btnAddCash.Visible = True
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = txtCashTotal.Text
                                Session("FeeCollection").Rows(iEdit)("Amount") = Format(txtCashTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                            Else
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = 0
                                Session("FeeCollection").Rows(iEdit)("Amount") = txtCashTotal.Text
                            End If
                        Else
                            'lblError.Text = "Invalid Amount!!!"
                            usrMessageBar.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    Case COLLECTIONTYPE.CHEQUES
                        If txtChqno.Text.Trim = "" Then
                            'lblError.Text = "Invalid Cheque No!!!"
                            usrMessageBar.ShowNotification("Invalid Cheque No!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If txtBank.Text.Trim = "" Or h_Bank.Value.Trim = "" Then
                            'lblError.Text = "Invalid Bank!!!"
                            usrMessageBar.ShowNotification("Invalid Bank!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If Not IsDate(txtChqDate.Text) Then
                            'lblError.Text = "Invalid Cheque Date!!!"
                            usrMessageBar.ShowNotification("Invalid Cheque Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If IsNumeric(txtChequeTotal.Text) AndAlso CDbl(txtChequeTotal.Text) > 0 Then
                            btnAddCheque.Visible = True
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = txtChequeTotal.Text
                                Session("FeeCollection").Rows(iEdit)("Amount") = Format(txtChequeTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                            Else
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = 0
                                Session("FeeCollection").Rows(iEdit)("Amount") = txtChequeTotal.Text
                            End If
                            'Session("FeeCollection").Rows(iEdit)("Amount") = txtChequeTotal.Text
                            Session("FeeCollection").Rows(iEdit)("REF_ID") = h_Bank.Value
                            Session("FeeCollection").Rows(iEdit)("REF_DESCR") = txtBank.Text
                            Session("FeeCollection").Rows(iEdit)("Date") = txtChqDate.Text
                            Session("FeeCollection").Rows(iEdit)("REF_NO") = txtChqno.Text
                            Session("FeeCollection").Rows(iEdit)("EMR_ID") = ddlEmirate.SelectedItem.Value
                        Else
                            'lblError.Text = "Invalid Amount!!!"
                            usrMessageBar.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    Case COLLECTIONTYPE.CREDIT_CARD
                        If txtCreditno.Text.Trim = "" Then
                            'lblError.Text = "Invalid Auth. Code!!!"
                            usrMessageBar.ShowNotification("Invalid Auth. Code!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If IsNumeric(txtCCTotal.Text) AndAlso CDbl(txtCCTotal.Text) > 0 Then
                            btnAddCreditcard.Visible = True
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = txtCCTotal.Text
                                Session("FeeCollection").Rows(iEdit)("Amount") = Format(txtCCTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                            Else
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = 0
                                Session("FeeCollection").Rows(iEdit)("Amount") = txtCCTotal.Text
                            End If
                            'Session("FeeCollection").Rows(iEdit)("Amount") = txtCCTotal.Text
                            Session("FeeCollection").Rows(iEdit)("REF_ID") = ddCreditcard.SelectedItem.Value
                            Session("FeeCollection").Rows(iEdit)("REF_NO") = txtCreditno.Text
                        Else
                            'lblError.Text = "Invalid Amount!!!"
                            usrMessageBar.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                End Select
                Exit For
            End If
        Next
        Clear_Details()
        gvFeeCollection.EditIndex = -1
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
    End Sub

    Protected Sub txtDAccountCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        chk_DetailsAccount()
        'FillEvents()
    End Sub

    Sub chk_DetailsAccount()
        txtDAccountName.Text = AccountFunctions.Validate_Account(txtDAccountCode.Text, Session("sbsuid"), "NOTCC")
        If txtDAccountName.Text = "" Then
            'lblError.Text = "Invalid Account Selected in Details"
            usrMessageBar.ShowNotification("Invalid Account Selected in Details", UserControls_usrMessageBar.WarningType.Danger)
            txtDAccountCode.Focus()
        End If
    End Sub

    Protected Sub ddCollection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCollection.SelectedIndexChanged
        Dim str_collection As String = AccountFunctions.get_CollectionAccount(ddCollection.SelectedItem.Value, Session("sBsuid"))
        Try
            txtDAccountCode.Text = str_collection.Split("|")(0)
            txtDAccountName.Text = str_collection.Split("|")(1)
            txtRemarks.Text = ddCollection.SelectedItem.Text.ToString() & "  For : " & txtFrom.Text.ToString()
            'FillEvents()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub gvFeeCollection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeCollection.RowDataBound
        Try
            Dim txtAmountToPay As TextBox = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
            Dim txtAmountToPayFC As TextBox = CType(e.Row.FindControl("txtAmountToPayFC"), TextBox)
            If txtAmountToPay IsNot Nothing And txtAmountToPayFC IsNot Nothing Then

                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtAmountToPay.Text = Format(txtAmountToPayFC.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                    txtAmountToPay.Enabled = False
                Else
                    txtAmountToPay.Enabled = True
                    'txtAmountToPayFC.Text = Format(txtAmountToPay.Text, Session("BSU_DataFormatString"))
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim gvr As GridViewRow = DirectCast(DirectCast(sender, LinkButton).Parent.Parent, GridViewRow)
        Dim RowIndex As Integer = gvr.RowIndex
        Dim PayMode As Int16 = gvFeeCollection.DataKeys(RowIndex).Item("PayMode")
        Select Case PayMode
            Case COLLECTIONTYPE.CASH
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtCashTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("FCAmount"), Session("BSU_DataFormatString"))
                Else
                    txtCashTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("Amount"), Session("BSU_DataFormatString"))
                End If

            Case COLLECTIONTYPE.CHEQUES
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtChequeTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("FCAmount"), Session("BSU_DataFormatString"))
                Else
                    txtChequeTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("Amount"), Session("BSU_DataFormatString"))
                End If

                h_Bank.Value = Session("FeeCollection").Rows(RowIndex)("REF_ID")
                txtBank.Text = Session("FeeCollection").Rows(RowIndex)("REF_DESCR")
                txtChqDate.Text = Session("FeeCollection").Rows(RowIndex)("Date")
                txtChqno.Text = Session("FeeCollection").Rows(RowIndex)("REF_NO")
                ddlEmirate.SelectedIndex = -1
                ddlEmirate.Items.FindByValue(Session("FeeCollection").Rows(RowIndex)("EMR_ID")).Selected = True
            Case COLLECTIONTYPE.CREDIT_CARD
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtChargeTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("FCAmount"), Session("BSU_DataFormatString"))
                    txtCCTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("CrAmount"), Session("BSU_DataFormatString"))
                    txtCrCharge.Text = Format(Session("FeeCollection").Rows(RowIndex)("CrCharge"), Session("BSU_DataFormatString"))
                    txtReceivedTotal.Text = txtChargeTotal.Text
                Else
                    txtChargeTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("Amount"), Session("BSU_DataFormatString"))
                    txtCCTotal.Text = Format(Session("FeeCollection").Rows(RowIndex)("CrAmount"), Session("BSU_DataFormatString"))
                    txtCrCharge.Text = Format(Session("FeeCollection").Rows(RowIndex)("CrCharge"), Session("BSU_DataFormatString"))
                    txtReceivedTotal.Text = txtChargeTotal.Text
                End If
                ddCreditcard.SelectedIndex = -1
                ddCreditcard.Items.FindByValue(Session("FeeCollection").Rows(RowIndex)("REF_ID")).Selected = True
                txtCreditno.Text = Session("FeeCollection").Rows(RowIndex)("REF_NO")
        End Select
        DirectCast(Session("FeeCollection"), DataTable).Rows(RowIndex).Delete()
        DirectCast(Session("FeeCollection"), DataTable).AcceptChanges()
        Set_GridTotal()

    End Sub
End Class