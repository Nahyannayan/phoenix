<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeAdjustment_REQNew.aspx.vb" Inherits="FeeAdjustment_REQNew" Theme="General" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/uscStudentPicker.ascx" TagPrefix="uc1" TagName="uscStudentPicker" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />


    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" />
    <script type="text/javascript" lang="javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                document.getElementById('<%= h_print.ClientID %>').value = '';
                //showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
                return false;
            }
        }
    );

        function FillDetailRemarks() {
            var HeaderRemarks;
            HeaderRemarks = document.getElementById('<%=txtHeaderRemarks.ClientID %>').value;
            if (HeaderRemarks != '')
                document.getElementById('<%=txtDetRemarks.ClientID %>').value = HeaderRemarks;
            return false;
        }
        function ChangeTextEnteringCaption(checkedItem) {
            var lblRadText;
            var lbl = checkedItem.parentElement.getElementsByTagName('label');
            lblRadText = lbl[0].innerHTML;
            if (lblRadText == 'Amount')
                document.getElementById('<%=txtDetAmount.ClientID %>').value = document.getElementById('<%=h_Amount.ClientID %>').value;
            else
                document.getElementById('<%=txtDetAmount.ClientID %>').value = document.getElementById('<%=h_WEEKS_MONTHS_COUNT.ClientID %>').value;
            return false;
        }
        function GetStudent(type) {

            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var STUD_TYP;
            STUD_TYP = document.getElementById('<%=chkEnquiry.ClientID%>').checked;
            var url;
            if (STUD_TYP == true)
                url = "ShowStudent.aspx?TYPE=TCENQ&VAL=1&ACD_ID=0";
            else
                url = "ShowStudent.aspx?TYPE=TC&VAL=1&ACD_ID=0";
            //result = window.showModalDialog(url, "", sFeatures)
            var oWnd = radopen(url, "pop_fee");
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                document.getElementById('<%=h_STUD_ID_CR.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudNameCR.ClientID %>').value = NameandCode[1];
                return false;
            }
            else {
                return false;
            }--%>
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=h_STUD_ID_CR.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudNameCR.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= h_STUD_ID_CR.ClientID%>', 'ValueChanged');
            }
        }
    </script>
    <style type="text/css">
        .divinfo {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            color: #2e2e2e;
            letter-spacing: 1px;
            font-weight: 600;
            width: 966px;
            background-position: 5px center;
            background: #feffb3 url(../images/Common/PageBody/info_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
        }
    </style>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments Request..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%; border: 0px;">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR" CssClass="alert alert-danger" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR" CssClass="alert alert-danger" />
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="Error" />--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%; border: 0px;">
                    <tr>
                        <td align="left"><span class="field-label">Adjustment Type</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlAdjReason" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <%--<asp:RadioButton ID="radStud" AutoPostBack="true" runat="server" GroupName="ENQ_STUD" Text="Student" Checked="True" />
                            <asp:RadioButton ID="radEnq" AutoPostBack="true" runat="server"
                                GroupName="ENQ_STUD" Text="Enquiry" />--%>
                            <asp:LinkButton ID="lnkRefundable" OnClientClick="return false;"
                                runat="server">View Refundable Fees</asp:LinkButton></td>
                        <td><span class="field-label">Date</span></td>
                        <td>
                            <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;
                            <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date  required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtDate">*</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date Required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Adjustment Reason</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlReasons" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left" width="15%"><span class="field-label">Select Student</span></td>
                        <td align="left" width="85%" colspan="3">
                            <%--<asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>--%>
                            <uc1:uscStudentPicker runat="server" ID="uscStudentPicker" />
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left" width="20%"><span class="field-label">Student</span></td>
                        <td align="left" colspan="3">
                            <%--<uc1:usrSelStudent ID="UsrSelStudent1" runat="server" EnableViewState="true" />--%>

                            <%--<asp:Label ID="labGrade" runat="server"></asp:Label>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Header remarks required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtHeaderRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr class="title-bg-lite">
                        <td align="left" colspan="2">Fee Adjustments Details...</td>
                        <td colspan="2" align="right">
                            <asp:RadioButtonList ID="rblTaxCalculation" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="E"><span class="field-label">Excl.Tax</span></asp:ListItem>
                                <asp:ListItem Value="I"><span class="field-label">Incl.Tax</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <center>
                    <asp:Table runat="server" width="100%">
                        <asp:TableHeaderRow>
                            <asp:TableCell HorizontalAlign="center" ><span class="field-label">Fee Type</span></asp:TableCell>
                            <asp:TableCell HorizontalAlign="center" ID="tcDRAMOUNT" runat="server"><span class="field-label">Amount</span></asp:TableCell>
                            <asp:TableCell HorizontalAlign="center" ID="tcTAXCODE" runat="server"><span class="field-label">Tax Code</span></asp:TableCell>
                            <asp:TableCell HorizontalAlign="center" ID="tcTAXAMOUNT" runat="server"><span class="field-label">Tax Amount</span></asp:TableCell>
                            <asp:TableCell HorizontalAlign="center" ID="tcNETAMOUNT" runat="server"><span class="field-label">Net Amount</span></asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="center" >
                                <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                                <br />
                                <asp:Label ID="lblFeeType" runat="server" ForeColor="OrangeRed" Text=""></asp:Label>
                                <br />
                                <asp:Label ID="lblBalance" runat="server" class="field-label" Text="Balance Amount :"></asp:Label>&nbsp;
                                <asp:Label
                                    ID="lblBalanceAmt" class="field-label" runat="server"></asp:Label>

                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="center" ID="tcDRAMOUNT2" runat="server">
                                <asp:TextBox ID="txtDetAmount" runat="server" Text="0.00" Style="text-align: right;" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount required"
                                    ValidationGroup="SUBERROR" ControlToValidate="txtDetAmount">*</asp:RequiredFieldValidator>
                               <asp:Label ID="lbDescription" runat="server"></asp:Label>
                                <br />
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                                    ValidChars=".-" TargetControlID="txtDetAmount" />

                                <asp:Label ID="lblAdjTypeRemarks" runat="server" CssClass="error" />

                            </asp:TableCell><asp:TableCell ID="tcTAXCODE2" runat="server" style="vertical-align:top;">
                                <asp:DropDownList ID="ddlVAT_CODE" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                                 <br />
                            </asp:TableCell><asp:TableCell HorizontalAlign="center" ID="tcTAXAMOUNT2" runat="server" style="vertical-align:top;">
                                <asp:Label ID="lblTAX_AMOUNT" runat="server" Text="0.00" CssClass="field-value" style="text-align: right;"></asp:Label>

                            </asp:TableCell><asp:TableCell HorizontalAlign="center" ID="tcNETAMOUNT2" runat="server" style="vertical-align:top;">
                                <asp:Label ID="lblNET_AMOUNT" runat="server" Text="0.00" CssClass="field-value" style="text-align: right;"></asp:Label>

                            </asp:TableCell></asp:TableRow><asp:TableRow runat="server" ID="trTAXalert">
                            <asp:TableCell HorizontalAlign="center"  ColumnSpan="5">
                                <div id="divMessage" runat="server" class="divinfo">
                                    <asp:Label ID="lblalert" runat="server"></asp:Label>
                                </div>
                            </asp:TableCell></asp:TableRow></asp:Table></center>
                        </td>
                    </tr>
                    <%--<tr>
            <td align="left" class="matters">Fee Type</td><td align="left" class="matters" colspan="3"></td>
        </tr>
        <tr id="tr_ToStudent0" runat="server">
            <td align="left" class="matters" style="height: 51px">Amount</td><td align="left" class="matters" colspan="3" style="height: 51px"></td>
        </tr>--%><tr id="tr_CRStudent" runat="server" visible="false">
            <td align="center" colspan="4">
                <table width="100%">
                    <tr>
                        <td align="left" class="title-bg-lite">Credit Student </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="10%">
                                        <span class="field-label">Student (CR)</span> </td>
                                    <td align="left" width="35%">
                                        <asp:CheckBox ID="chkEnquiry" runat="server" Text="Enquiry" AutoPostBack="true" CssClass="align-items-lg-start" />
                                        <span style="display: inline-block !important; width: 81%;">
                                            <asp:TextBox ID="txtStudNameCR" runat="server" Style="width: 80% !important;"></asp:TextBox><asp:ImageButton ID="ImgCRStud" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                OnClientClick="GetStudent(0); return false;" OnClick="ImgCRStud_Click"></asp:ImageButton><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                                    ControlToValidate="txtStudNameCR" ErrorMessage="Student Name required"
                                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></span></td>
                                    <td align="left" width="10%">
                                        <span class="field-label">Fee Type </span></td>
                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlFEETYPECR" runat="server">
                                        </asp:DropDownList><br />
                                        <asp:Label ID="lblFeeTypeCR" ForeColor="OrangeRed" runat="server" Text=""></asp:Label></td>
                                    <td align="left" width="15%">
                                        <span class="field-label">Amount</span>  </td>
                                    <td align="left" width="15%">
                                        <asp:TextBox ID="txtDetAmount_HH" runat="server" AutoCompleteType="Disabled"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDetAmount"
                                            ErrorMessage="Amount required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator><ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtDetAmount_HH" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr id="tr_Addbutton" runat="server">
                        <td align="center" colspan="4">
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row" CellPadding="4">
                                <Columns>
                                    <asp:TemplateField HeaderText="FadId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAD_ID" runat="server" Text='<%# Bind("FAD_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FeeId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%# bind("DURATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FAR_TAX_CODE" HeaderText="Tax Code" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# String.Format("{0:F}", Eval("FEE_AMOUNT"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FAR_TAX_AMOUNT" HeaderText="Tax Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:F}" HtmlEncode="false" />
                                    <asp:BoundField DataField="FAR_NET_AMOUNT" HeaderText="NET Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:F}" HtmlEncode="false" />
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" runat="server">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkPrint" runat="server" Text="Print After Save" Checked="True" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server"
                    PopupControlID="Panel1"
                    Position="left" TargetControlID="lnkRefundable">
                </ajaxToolkit:PopupControlExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                    Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_WEEKS_MONTHS_COUNT" runat="server" />
                <asp:SqlDataSource ID="sqlADJREASON" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
                    SelectCommand="SELECT ARM_ID, ARM_DESCR&#13;&#10;FROM FEES.ADJ_REASON_M where ARM_bSHOW=1 order by ARM_DESCR  "></asp:SqlDataSource>
                <asp:HiddenField ID="h_STUD_ID_CR" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_GRD_ID_CR" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="hf_GRD_ID" runat="server" />
                <asp:HiddenField ID="h_Amount" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_CanEdit" runat="server" />
                <asp:Panel ID="Panel1" runat="server">
                    <asp:GridView ID="gvFeePaidHistory" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE Type" />
                            <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False" HeaderText="Date" />
                            <asp:BoundField DataField="PAID_AMT" HeaderText="Paid Amount" />
                            <asp:BoundField DataField="CHARGED_AMT" HeaderText="Charged Amt" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>


                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });

                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                </script>

            </div>
        </div>
    </div>
</asp:Content>
