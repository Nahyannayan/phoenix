Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class StudRecordEdit
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_STUDENT_MASTER) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("datamode") = "edit"
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call control_Disable()
                    txtFComp_Name.Attributes.Add("readonly", "readonly")
                    txtMComp_Name.Attributes.Add("readonly", "readonly")
                    txtGComp_Name.Attributes.Add("readonly", "readonly")
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Call Student_M_Details(ViewState("viewid"))
                    Session("Fee_SLIB_ID") = h_SliblingID.Value
                    Session("TYPE_FEE_SPON") = "S"
                    Call Student_D_Details(h_SliblingID.Value)
                    Call call_Function()
                    btnCancel.Text = "Back"
                    ddlFCompany_Name.Attributes.Add("onchange", "Set_Company(this,'" & txtFComp_Name.ClientID & "')")
                    ddlMCompany_Name.Attributes.Add("onchange", "Set_Company(this,'" & txtMComp_Name.ClientID & "')")
                    ddlGCompany_Name.Attributes.Add("onchange", "Set_Company(this,'" & txtGComp_Name.ClientID & "')")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    
    Sub call_Function()
        Call DropDownList_RecordSelect()
        Call GetCompany_Name()
    End Sub

    Private Sub BindEmirate_info(ByVal ddlEmirate As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
        ddlEmirate.DataBind()
    End Sub

    Sub Student_D_Details(ByVal Slib_ID As String)
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "-"
        Using readerStudent_D_Detail As SqlDataReader = AccessStudentClass.GetStudent_D(Slib_ID)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read
                    'handle the null value returned from the reader incase  convert.tostring
                    txtFFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME"))
                    txtFMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME"))
                    txtFLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))
                    If Not ddFeeSponsor.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEESPONSOR"))) Is Nothing Then
                        ddFeeSponsor.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEESPONSOR"))).Selected = True
                    End If
                    txtMFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME"))
                    txtMMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME"))
                    txtMLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))

                    txtGFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_GFIRSTNAME"))
                    txtGMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_GMIDNAME"))
                    txtGLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_GLASTNAME"))

                    ViewState("temp_FNation1") = Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))
                    ViewState("temp_FNation2") = Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))
                    ViewState("temp_FComCountry") = Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))
                    ViewState("temp_FPRM_Country") = Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))
                    ViewState("temp_F_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))
                    ViewState("temp_MNationality1") = Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))
                    ViewState("temp_MNationality2") = Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))
                    ViewState("temp_MCOMCountry") = Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))
                    ViewState("temp_MPRMCOUNTRY") = Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))
                    ViewState("temp_M_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))
                    ViewState("temp_GNationality1") = Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))
                    ViewState("temp_GNationality2") = Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))
                    ViewState("temp_GCOMCountry") = Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))
                    ViewState("temp_GPRMCOUNTRY") = Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))

                    ViewState("temp_STS_F_COMP_ID") = Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))
                    ViewState("temp_STS_M_COMP_ID") = Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))
                    ViewState("temp_STS_G_COMP_ID") = Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))

                    txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                    txtMComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                    txtGComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY"))

                End While
            End If
        End Using
    End Sub

    Sub Student_M_Details(ByVal Stud_No As String)
        Dim temp_PrimContact As String
        Dim arInfo As String() = New String(2) {}
        Dim Temp_Phone_Split As String = String.Empty
        Dim splitter As Char = "-"
        Try
            Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        txtShift_ID.Text = Convert.ToString(readerStudent_Detail("SHF_DESCR"))
                        txtStream.Text = Convert.ToString(readerStudent_Detail("STM_DESCR"))

                        txtStud_No.Text = Convert.ToString(readerStudent_Detail("STU_NO"))

                        txtACD_ID.Text = Convert.ToString(readerStudent_Detail("ACD_ID_Y"))
                        txtGRD_ID.Text = Convert.ToString(readerStudent_Detail("GRD_ID"))

                        txtSCT_ID.Text = Convert.ToString(readerStudent_Detail("SCT_ID"))

                        txtFname_E.Text = Convert.ToString(readerStudent_Detail("SFIRSTNAME_E"))
                        txtMname_E.Text = Convert.ToString(readerStudent_Detail("SMIDNAME_E"))
                        txtLname_E.Text = Convert.ToString(readerStudent_Detail("SLASTNAME_E"))

                        h_SliblingID.Value = Convert.ToString(readerStudent_Detail("SSIBLING_ID"))
                        'viewState setting
                        ViewState("temp_Type") = Convert.ToString(readerStudent_Detail("STFRTYPE"))
                        ViewState("temp_Rlg_ID") = Convert.ToString(readerStudent_Detail("RLG_ID"))
                        ViewState("temp_Nationality") = Convert.ToString(readerStudent_Detail("SNATIONALITY"))
                        ViewState("temp_COB") = Convert.ToString(readerStudent_Detail("COB"))
                        ViewState("temp_HOUSE") = Convert.ToString(readerStudent_Detail("HOUSE"))
                        ViewState("temp_MINLIST") = Convert.ToString(readerStudent_Detail("MINLIST"))
                        ViewState("temp_MINLISTTYPE") = Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))
                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))
                        ViewState("temp_Blood") = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        'code added by dhanya
                        ViewState("ACD_ID") = Convert.ToString(readerStudent_Detail("ACD_ID"))
                        'Setting date
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If
                        'setting radio buttons
                        temp_PrimContact = Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT"))
                        If UCase(temp_PrimContact) = "F" Then
                            rdFather.Checked = True
                        ElseIf UCase(temp_PrimContact) = "M" Then
                            rdMother.Checked = True
                        Else
                            rdGuardian.Checked = True
                        End If
                    End While
                Else
                End If
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Sub DropDownList_RecordSelect()
        For ItemTypeCounter As Integer = 0 To ddlPrefContact.Items.Count - 1
            'keep loop until you get the School to Not Available into  the SelectedIndex
            If UCase(ddlPrefContact.Items(ItemTypeCounter).Value) = UCase(ViewState("temp_PrefContact")) Then
                ddlPrefContact.SelectedIndex = ItemTypeCounter
            End If
        Next
    End Sub

    Sub GetCompany_Name()
        Try
            Using GetCompany_Name_reader As SqlDataReader = AccessStudentClass.GetCompany_Name()
                ddlFCompany_Name.Items.Clear()
                ddlMCompany_Name.Items.Clear()
                ddlGCompany_Name.Items.Clear()

                ddlFCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlMCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlGCompany_Name.Items.Add(New ListItem("Other", "0"))

                If GetCompany_Name_reader.HasRows = True Then
                    While GetCompany_Name_reader.Read
                        ddlFCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlMCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlGCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlGCompany_Name.Items.Count - 1
                'keep loop until you get the Country to Not Available into  the SelectedIndex
                If ddlFCompany_Name.Items(ItemTypeCounter).Value = ViewState("temp_STS_F_COMP_ID") Then
                    ddlFCompany_Name.SelectedIndex = ItemTypeCounter
                    If ItemTypeCounter = 0 Then
                        txtFComp_Name.Attributes.Remove("readonly")
                    End If
                End If

                If ddlMCompany_Name.Items(ItemTypeCounter).Value = ViewState("temp_STS_M_COMP_ID") Then
                    ddlMCompany_Name.SelectedIndex = ItemTypeCounter
                    If ItemTypeCounter = 0 Then
                        txtMComp_Name.Attributes.Remove("readonly")
                    End If
                End If

                If ddlGCompany_Name.Items(ItemTypeCounter).Value = ViewState("temp_STS_G_COMP_ID") Then
                    ddlGCompany_Name.SelectedIndex = ItemTypeCounter
                    If ItemTypeCounter = 0 Then
                        txtGComp_Name.Attributes.Remove("readonly")
                    End If
                End If
            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, " GetCompany_Name()")
        End Try
    End Sub 

    Private Sub control_Disable()
        txtStud_No.Attributes.Add("Readonly", "Readonly")

        txtDOJ.Attributes.Add("Readonly", "Readonly")
        txtACD_ID.Attributes.Add("Readonly", "Readonly")
        txtGRD_ID.Attributes.Add("Readonly", "Readonly")
        txtSCT_ID.Attributes.Add("Readonly", "Readonly")

        txtShift_ID.Attributes.Add("Readonly", "Readonly")
        txtStream.Attributes.Add("Readonly", "Readonly")
        txtFname_E.Attributes.Add("Readonly", "Readonly")
        txtMname_E.Attributes.Add("Readonly", "Readonly")
        txtLname_E.Attributes.Add("Readonly", "Readonly")

        txtShift_ID.Attributes.Add("Readonly", "Readonly")
        txtStream.Attributes.Add("Readonly", "Readonly")
       
        txtStud_No.Attributes.Add("Readonly", "Readonly")

        txtDOJ.Attributes.Add("Readonly", "Readonly")
        txtACD_ID.Attributes.Add("Readonly", "Readonly")
        txtGRD_ID.Attributes.Add("Readonly", "Readonly")
        txtSCT_ID.Attributes.Add("Readonly", "Readonly")

        txtFFirstName.Attributes.Add("Readonly", "Readonly")
        txtFMidName.Attributes.Add("Readonly", "Readonly")
        txtFLastName.Attributes.Add("Readonly", "Readonly")
        txtMFirstName.Attributes.Add("Readonly", "Readonly")
        txtMMidName.Attributes.Add("Readonly", "Readonly")
        txtMLastName.Attributes.Add("Readonly", "Readonly")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim status As Integer
        Dim transaction As SqlTransaction
        If Page.IsValid = True Then
            If ViewState("datamode") = "edit" Then
                Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        Dim STU_ID As String = ViewState("viewid")
                        Dim STS_FCOMPANY As String
                        Dim STS_MCOMPANY As String
                        Dim STS_GCOMPANY As String
                        Dim STS_F_COMP_ID As String
                        Dim STS_M_COMP_ID As String
                        Dim STS_G_COMP_ID As String
                        If ddlFCompany_Name.SelectedItem.Text = "Other" Then
                            STS_FCOMPANY = txtFComp_Name.Text
                            STS_F_COMP_ID = "0"
                        Else
                            STS_FCOMPANY = ""
                            STS_F_COMP_ID = ddlFCompany_Name.SelectedItem.Value
                        End If

                        If ddlMCompany_Name.SelectedItem.Text = "Other" Then
                            STS_MCOMPANY = txtMComp_Name.Text
                            STS_M_COMP_ID = "0"
                        Else
                            STS_MCOMPANY = ""
                            STS_M_COMP_ID = ddlMCompany_Name.SelectedItem.Value
                        End If

                        If ddlGCompany_Name.SelectedItem.Text = "Other" Then
                            STS_GCOMPANY = txtGComp_Name.Text
                            STS_G_COMP_ID = "0"
                        Else
                            STS_GCOMPANY = ""
                            STS_G_COMP_ID = ddlGCompany_Name.SelectedItem.Value
                        End If

                        status = AccessStudentClass.F_UPDATEFEESPONSOR(STU_ID, ddFeeSponsor.SelectedItem.Value, _
                          STS_F_COMP_ID, STS_M_COMP_ID, STS_G_COMP_ID, _
                            STS_FCOMPANY, STS_MCOMPANY, STS_GCOMPANY, transaction)
                        If status <> 0 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "Edit", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        transaction.Commit()
                        ' populateGrade_Edit()
                        Dim msg As String = String.Empty
                        'lblError.Text = "Record Updated Successfully"
                        usrMessageBar.ShowNotification("Record Updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                        ViewState("datamode") = "none"
                        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        'lblError.Text = myex.Message
                        usrMessageBar.ShowNotification(myex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    Catch ex As Exception
                        transaction.Rollback()
                        'lblError.Text = "Record could not be Updated"
                        usrMessageBar.ShowNotification("Record could not be Updated", UserControls_usrMessageBar.WarningType.Danger)
                    End Try
                End Using
            End If
        End If
    End Sub

End Class
