<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FEEPerformaInvoice.aspx.vb" Inherits="Fees_FEEPerformaInvoice" Theme="General" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
function CheckForPrint() {

    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';

        var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up3');
        return false;
    }
}
    );
function CheckControls() {
    var GroupSel = document.getElementById('<%=chkGroupInvoice.ClientID %>').checked;
    var radEnq = document.getElementById('<%=radEnquiry.ClientID %>').checked;
    var enabled;
    if (GroupSel == 0 && radEnq == 0) {
        enabled = false;
    }
    else {
        enabled = true;
    }
    var CURR_ACD_ID = document.getElementById('<%=h_CURR_ACD_ID.ClientID %>').value;
    document.getElementById('<%=ddlAcademicYear.ClientID %>').value = CURR_ACD_ID;
    document.getElementById('<%=txtStudName.ClientID %>').value = '';
    document.getElementById('<%=h_STUD_ID.ClientID %>').value = '';
    document.getElementById('<%=ddlGrade.ClientID %>').clearValue();
}

function GetStudent() {
   
    var NameandCode;
    var result;
    var GroupSel = document.getElementById('<%=chkGroupInvoice.ClientID %>').checked;
    var COMP_ID = document.getElementById('<%=h_Company_ID.ClientID %>').value;
    var STUD_TYP = document.getElementById('<%=radEnquiry.ClientID %>').checked;
    var CompFilter = document.getElementById('<%=chkFilter.ClientID %>').checked;
    var chkService = document.getElementById('<%=chkService.ClientID %>').checked;
    var hFeeTypes = document.getElementById('<%=h_FeeTypes.ClientID %>').value;
    var NewStud = document.getElementById('<%=chkNewStudent.ClientID %>').checked;
    var ExcludeTC = document.getElementById('<%=chkExcludeTC.ClientID%>').checked;
    var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
    if (CompFilter != true) {
        COMP_ID = "-1";
    }
    var TC = "0";
    if (ExcludeTC == true)
        TC = "1";
    var url;
    if (STUD_TYP == true) {//Enquiry
        if (GroupSel == true) {
            var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            url = "ShowStudentMulti.aspx?TYPE=ENQ_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID + "&IsService=" + chkService + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC;
        }
        else {
            url = "ShowStudent.aspx?TYPE=ENQ_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&IsService=" + chkService + "&ACD_ID=" + ACD_ID + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC;
        }
    }
    else {//Enrolled
        if (GroupSel == true) {
            url = "ShowStudentMulti.aspx?TYPE=STUD_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID + "&IsService=" + chkService + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC;
        }
        else {
            url = "ShowStudent.aspx?TYPE=STUD_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&IsService=" + chkService + "&ACD_ID=" + ACD_ID + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC;
        }
    }
    result = radopen(url, "pop_up");
   <%-- if (GroupSel == false) {
        if (result != '' && result != undefined) {
            NameandCode = result.split('||');
            document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
        }
        return false;
    }
    else {
        if (result != '' && result != undefined) {
            document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
        }
        return false;
    }--%>
    }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            var GroupSel = document.getElementById('<%=chkGroupInvoice.ClientID %>').checked;
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (GroupSel == false) {                                   
                    document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;     
                    __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');
                }
                else {                   
                    document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                    document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode; 
                    __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');
                }
            }
        }


function GetCompany() {
   
    var NameandCode;
    var result;
    var url = "../Common/popupForm.aspx?ID=COMPANY&multiSelect=false";
    result = radopen(url, "pop_up2");
   <%-- if (result != '' && result != undefined) {
        NameandCode = result.split('___');
        document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
           document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
           return true;
       }
       else {
           return false;
       }--%>
   }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtCompanyDescr.ClientID%>', 'TextChanged');
            }
        }

   function CheckDuplicat() {
       var StuNo = document.getElementById('<%=h_duplicate.ClientID %>').value;
         if (StuNo != "") {
             var Ret = confirm('Duplicate Invoice..  \n ' + String(StuNo).substring(0, String(StuNo).length - 3) + '.\n Do you want to Continue..? ');
             return Ret;
         }
         else
             return true;
     }
     function change_chk_state(src) {
         var chk_state = (src.checked);
         for (i = 0; i < document.forms[0].elements.length; i++) {
             if (document.forms[0].elements[i].type == 'checkbox') {
                 document.forms[0].elements[i].checked = chk_state;
             }
         }
     }
     function client_OnTreeNodeChecked() {
         var obj = window.event.srcElement;
         var treeNodeFound = false;
         var checkedState;
         if (obj.tagName == "INPUT" && obj.type == "checkbox") {
             var treeNode = obj;
             checkedState = treeNode.checked;
             do {
                 obj = obj.parentElement;
             } while (obj.tagName != "TABLE")
             var parentTreeLevel = obj.rows[0].cells.length;
             var parentTreeNode = obj.rows[0].cells[0];
             var tables = obj.parentElement.getElementsByTagName("TABLE");
             var numTables = tables.length
             if (numTables >= 1) {
                 for (i = 0; i < numTables; i++) {
                     if (tables[i] == obj) {
                         treeNodeFound = true;
                         i++;
                         if (i == numTables) {
                             return;
                         }
                     }
                     if (treeNodeFound == true) {
                         var childTreeLevel = tables[i].rows[0].cells.length;
                         if (childTreeLevel > parentTreeLevel) {
                             var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                             var inputs = cell.getElementsByTagName("INPUT");
                             inputs[0].checked = checkedState;
                         }
                         else {
                             return;
                         }
                     }
                 }
             }
         }
     }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
            <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Pro forma invoice"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <%--<tr class="">
                        <td align="left" colspan="4" style="height: 19px">
                            <asp:Label ID="lblHeader" runat="server" Text="Fee Pro forma invoice"></asp:Label></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Invoice Caption</span></td>
                        <td align="left" colspan="2"  >
                            <asp:TextBox ID="txtInvoiceCaption" runat="server"></asp:TextBox></td>
                         <td align="left" colspan="2" rowspan="2">
                            <asp:CheckBox ID="chkFilter" runat="server" Text="Company Filter" CssClass="field-label" />
                            <br />
                            <asp:CheckBox ID="chkService" runat="server" Text="Service Invoice" AutoPostBack="True"  CssClass="field-label"/>
                            <br />
                            <asp:CheckBox ID="chkXcludeAdv" runat="server" Text="Exclude Advance Fees" CssClass="field-label" />
                            <br /> 
                            <asp:CheckBox ID="chkXcludeDue" runat="server" Text="Exclude Previous Outstanding" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Company</span></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtCompanyDescr" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetCompany(); return false;" />
                        </td>
                       
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="center"><span class="field-label">Date </span>
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="Image1" runat="Server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator
                                ID="revFromdate" runat="server" ControlToValidate="txtDate" Display="Dynamic"
                                ErrorMessage="Enter the Date in given format dd/mmm/yyyy e.g.  21/Sep/2007" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left">
                            <asp:RadioButton ID="rbTermly" runat="server" AutoPostBack="True" Checked="True" CssClass="field-label"
                                GroupName="trm" Text="Termly" />
                            <asp:RadioButton ID="rbMonthly" runat="server" CssClass="field-label"
                                AutoPostBack="True" GroupName="trm" Text="Monthly" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Fee Type</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlFeeType" runat="server">
                            </asp:DropDownList>
                            <asp:LinkButton ID="lnkAddFEETYPE" runat="server">Add</asp:LinkButton></td>
                        <td></td>
                        <td align="left" >
                            <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;">Select Month</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Narration</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TabIndex="160"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkNewStudent" runat="server" Text="New Student" CssClass="field-label" />
                            <asp:CheckBox ID="chkAccStatus" runat="server" Text="Account Status" CssClass="field-label" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg"  colspan="2" width="20%">Selected Fee Types
                        </td>
                        <td align="left" class="title-bg"  colspan="4" width="80%"> Select Students...</td>
                    </tr>
                    <tr>
                        <td rowspan="3" valign="top" colspan="2">
                            <asp:GridView ID="gvFEETYPEDET" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="FEE ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FEE TYPE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("FEE_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDeleteFEETYPE" runat="server" OnClick="lnkDeleteFEETYPE_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td  align="left" colspan="4">
                            <asp:CheckBox ID="chkGroupInvoice" runat="server" Text="Group Invoice" CssClass="field-label" />
                            <asp:RadioButton ID="radStudent" runat="server" Checked="True" GroupName="STUD_ENQ" CssClass="field-label"
                                Text="Student" />
                            <asp:RadioButton ID="radEnquiry" runat="server" GroupName="STUD_ENQ" Text="Enquiry" CssClass="field-label" />
                            <asp:CheckBox ID="chkNextAcademicYear" runat="server" Text="Next Academic Year Invoice" CssClass="field-label" />
                            <asp:CheckBox ID="chkExcludeTC" runat="server" Text="Exclude Future TC Student(s)" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td  align="left" >
                            <asp:Label ID="lblStud_Enq" runat="server" Text="Student" CssClass="field-label"></asp:Label></td>
                        <td  align="left"  width="30%">
                            <asp:TextBox ID="txtStudName" runat="server" AutoPostBack="true" OnTextChanged="txtStudName_TextChanged" ></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(); return false;" /></td>
                        <td align="left"  ><span class="field-label">Grade</span></td>
                        <td align="left"  width="30%"> 
                            <asp:DropDownList ID="ddlGrade" runat="server">
                            </asp:DropDownList>
                            <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False">Add</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td colspan="4" valign="top" align="right">
                            <asp:GridView ID="gvStudentDetails" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                SkinID="GridViewNormal">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STUD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStud_no" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STUD_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblAmount" runat="server" Text='<%# bind("STUD_AMOUNT") %>' OnClientClick="return false;"
                                                CausesValidation="False" ToolTip="Click here for the Split up details"></asp:LinkButton>
                                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" DynamicServiceMethod="GetDynamicContent"
                                                DynamicContextKey='<%# Eval("STUD_ID") %>' DynamicControlID="Panel1" TargetControlID="lblAmount"
                                                PopupControlID="Panel1" Position="Right">
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:CheckBox ID="ChkRegenerate" runat="server" Text="Regenerate" Visible="False"></asp:CheckBox>
                        </td>
                    </tr>
                </table>
                <table align="center" runat="server" id="trEditFeeDetails" cellpadding="5" cellspacing="0"
                    width="100%">
                    <tr>
                        <td align="left" class="title-bg" colspan="4">Edit Fee payment Details...</td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Student Details #</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAddFeeStudDet" runat="server"></asp:TextBox></td>
                        <td rowspan="5" valign="top">
                            <asp:GridView ID="gvAddFeeDet" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="FEE ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_ADJ_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# bind("FEE_ADJ_AMT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDeleteFEEADDADJ" runat="server" OnClick="lnkDeleteFEEADDADJ_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Fee Type</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlAddFeeType" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Amount</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAddFeeAmount" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="cmpAmtVer" runat="server" ControlToValidate="txtAddFeeAmount"
                                ErrorMessage="Please enter only numbers" Operator="DataTypeCheck" Type="Double"
                                ValidationGroup="EditFEETYPE">*</asp:CompareValidator></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left"><span class="field-label">Remarks</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAddFeeRemarks" runat="server"  ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <asp:CheckBox ID="chkApplyToAll" runat="server" Text="Apply to all" AutoPostBack="True" />
                            <asp:Button ID="btnFeeAddAmt" runat="server" CssClass="button" Text="Add" ValidationGroup="EditFEETYPE" />
                            <asp:Button ID="btnFeeAmtCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="EditFEETYPE" />
                            <asp:Button ID="btnSaveAddFee" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnSaveAddFeeCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" >
                            <asp:CheckBox ID="chkPrintReciept" runat="server" Text="Print Receipt After Save" CssClass="field-label"
                                Checked="True" />
                            <asp:CheckBox ID="chkAnually" runat="server" Text="Generate Annual Receipt" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR"
                                OnClientClick="return CheckDuplicat()" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
                    <tr>
                        <td align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                                CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" TargetControlID="lnkBtnSelectmonth"
                    PopupControlID="Panel2" CommitProperty="value" Position="Bottom" CommitScript="" />
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" PopupButtonID="Image1"
                    TargetControlID="txtDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:Panel ID="Panel2" runat="server" CssClass="Visibility_none" >
                    <div >
                        <asp:UpdatePanel runat="server" ID="up2">
                            <ContentTemplate>
                                <asp:TreeView ID="trMonths" ShowCheckBoxes="all" onclick="client_OnTreeNodeChecked();" BackColor="#8dc24c"
                                    runat="server">
                                </asp:TreeView>
                                <div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server">
                </asp:Panel>
                <asp:HiddenField ID="h_Company_ID" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_CURR_ACD_ID" runat="server" />
                <asp:HiddenField ID="hfFeeType" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_FeeTypes" runat="server" EnableViewState="False" />
                <asp:HiddenField ID="h_duplicate" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
