<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeServiceFeeChargeView.aspx.vb" Inherits="Fees_feeServiceFeeChargeView" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 
 <script language="javascript" type="text/javascript">
  
 
 function ShowLeaveDetail(id)
       {    
            var sFeatures;
            
            var NameandCode;
            var result;
            //result = window.showModalDialog("ShowEmpLeave.aspx?ela_id="+id,"", sFeatures)
         
            //return false;
            var url = "ShowEmpLeave.aspx?ela_id="+id;
            var oWnd = radopen(url, "pop_showleavedetail");
        }
        
    


function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_showleavedetail" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
       
    </telerik:RadWindowManager>              
     
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Fee Setup
        </div>
        <div class="card-body">
            <div class="table-responsive">

 
    <table align="center" width="100%">
            <tr valign="top" >
          
                <td valign="top" width="100%" align="left">
                    <asp:HyperLink ID="hlAddnew" runat="server">Add New</asp:HyperLink>
                    <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                </td>
                    <td align="right">
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    </td>               
            </tr>
        </table>  <a id='top'></a>
    <table align="center" width="100%">
            
            <tr>
                <td align="center" valign="top">
                    <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                        EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                        
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_pop" />
                        
                        <Columns>  
                            <asp:TemplateField HeaderText="Settlement Schedule"> 
                                <HeaderTemplate>
                                    Service<br />
                                    <asp:TextBox ID="txtFClass" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField >
                            <asp:TemplateField HeaderText="Age Group">
                                <HeaderTemplate>
                                    Pick Up Point<br />
                                    <asp:TextBox ID="txtAgegroup" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate> 
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("PNT_DESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:BoundField DataField="SBR_RATE" HeaderText="Amount" DataFormatString="{0:0.00}" />
                            <asp:TemplateField HeaderText="From Date">
                                <HeaderTemplate>
                                    From Dt<br />
                                    <asp:TextBox ID="txtFDate" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("SBR_DTFROM","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Date">
                                <HeaderTemplate>
                                   To Dt<br />
                                    <asp:TextBox ID="txtTDate" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("SBR_DTTO","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="View">
                                <ItemTemplate>
                                    &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SBR_ID" Visible="False">
                                <EditItemTemplate>    
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("SBR_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView> 
                </td>
            </tr>
        </table>

                </div>
            </div>
        </div>
</asp:Content>


