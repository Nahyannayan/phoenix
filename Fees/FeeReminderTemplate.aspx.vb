Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.Shared
Imports Telerik.Web.UI

Partial Class fees_FeeReminderTemplate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Shared reportHeader As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REMINDER_TEMPLATE Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ClearDetails()
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim tblbUSuper As Boolean = Session("sBusper")
                If tblbUSuper = True Then
                    ddlBusinessUnit.DataSource = AccessRoleUser.GetBusinessUnits()
                    ddlBusinessUnit.DataTextField = "bsu_name"
                    ddlBusinessUnit.DataValueField = "bsu_id"
                Else
                    ddlBusinessUnit.DataSource = AccessRoleUser.GetTotalBUnit(Session("sUsr_id"))
                    ddlBusinessUnit.DataTextField = "bname"
                    ddlBusinessUnit.DataValueField = "B_unit"
                End If
                ddlBusinessUnit.DataBind()
                ddlBusinessUnit.SelectedIndex = -1
                ddlBusinessUnit.SelectedValue = Session("sBSUID")
                btnDelete.Visible = False
            End If
            If ViewState("datamode") = "view" Then
                Me.hf_FRM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("FRM_ID").Replace(" ", "+"))
                Dim vViewFEERem As FeeReminderTemplate = FeeReminderTemplate.GetFeeReminderTemplate(Me.hf_FRM_ID.Value, Session("sBsuId"), ConnectionManger.GetOASIS_FEESConnection)
                ddlBusinessUnit.ClearSelection()
                ddlBusinessUnit.SelectedValue = vViewFEERem.FRM_BSU_ID
                ddlLevel.SelectedValue = vViewFEERem.FRM_Level
                txtRemarks.Content = vViewFEERem.FRM_REMARKS
                txtShortDescr.Content = vViewFEERem.FRM_SHORT_DESCR
                txtSignature.Content = vViewFEERem.FRM_SIGNATORY
                txtToAddress.Content = vViewFEERem.FRM_TO_ADDRESS
                txtAcknowlg.Content = vViewFEERem.FRM_Acknowledgement
                txtSMS.Text = vViewFEERem.FRM_SMS
                Session("sFEE_REMINDER_TEMPLATE") = vViewFEERem
                DissableControls(True)

                Dim bArabic As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(RMD_bARABIC, 0) FROM dbo.REMINDERTYPE_M WITH ( NOLOCK ) WHERE RMD_ID = " & vViewFEERem.FRM_Level.ToString & "")
                If IsNothing(bArabic) Then
                    bArabic = False
                End If
                If bArabic Then
                    Dim smScriptManager As New ScriptManager
                    smScriptManager = Master.FindControl("ScriptManager1")
                    smScriptManager.RegisterPostBackControl(btnPrint)
                End If
                h_IsArabic.Value = bArabic

                

            End If
        End If
    End Sub
    
    Private Sub DissableControls(ByVal bDissable As Boolean)
        ddlBusinessUnit.Enabled = Not bDissable
        ddlLevel.Enabled = Not bDissable
        txtRemarks.Enabled = Not bDissable
        txtShortDescr.Enabled = Not bDissable
        txtSignature.Enabled = Not bDissable
        txtToAddress.Enabled = Not bDissable
        txtAcknowlg.Enabled = Not bDissable
        txtSMS.Enabled = Not bDissable
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If Not Master.IsSessionMatchesForSave() Then
        '    'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
        '    Exit Sub
        'End If
        Dim vFeeRem As New FeeReminderTemplate
        If Not Session("sFEE_REMINDER_TEMPLATE") Is Nothing Then
            vFeeRem = Session("sFEE_REMINDER_TEMPLATE")
        End If

        vFeeRem.FRM_ID = vFeeRem.FRM_ID
        vFeeRem.FRM_BSU_ID = ddlBusinessUnit.SelectedValue
        vFeeRem.FRM_Level = ddlLevel.SelectedValue
        vFeeRem.FRM_REMARKS = txtRemarks.Content
        vFeeRem.FRM_SHORT_DESCR = txtShortDescr.Content
        vFeeRem.FRM_SIGNATORY = txtSignature.Content
        vFeeRem.FRM_TO_ADDRESS = txtToAddress.Content
        vFeeRem.FRM_Acknowledgement = txtAcknowlg.Content
        vFeeRem.FRM_SMS = txtSMS.Text
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction("FEE_CONCESSION")
        Dim retVal As Integer = FeeReminderTemplate.SaveFeeReminderTemplate(vFeeRem, trans, conn)
        If retVal <> 0 Then
            trans.Rollback()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            trans.Commit()
            'lblError.Text = "Data saved Successfully"
            usrMessageBar.ShowNotification("Data saved Successfully", UserControls_usrMessageBar.WarningType.Success)
            'ClearDetails()
            DissableControls(True)
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
        End If
    End Sub

    Private Sub ClearDetails()
        ddlBusinessUnit.ClearSelection()
        ddlLevel.ClearSelection()
        txtRemarks.Content = ""
        txtShortDescr.Content = ""
        txtSignature.Content = ""
        txtToAddress.Content = ""
        txtAcknowlg.Content = ""
        txtSMS.Text = ""
        Me.hf_FRM_ID.Value = ""
        Session("sFEE_REMINDER_TEMPLATE") = Nothing
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim tblbUSuper As Boolean = Session("sBusper")
        If h_IsArabic.Value.ToLower = "true" And ((Not Session("sBITSupport") Is Nothing AndAlso Convert.ToBoolean(Session("sBITSupport")) <> True)) Then
            'lblError.Text = "Edit not possible for Arabic reminder template, Please contact IT to update the template."
            usrMessageBar.ShowNotification("Edit not possible for Arabic reminder template, Please contact IT to update the template.", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        ViewState("datamode") = "edit"
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        DissableControls(False)
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ddlLevel.Enabled = True
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Session("ReportSource") = PrintReminder(Session("sUsr_name"), True)

    End Sub
    Public Function PrintReminder(ByVal Usr_name As String, Optional ByVal Fees As Boolean = True) As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        If Fees = False Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        End If

        Dim cmd As New SqlCommand("[FEES].[F_GETReminderLetter_Preview]", New SqlConnection(str_conn))
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRH_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpFRH_ID.Value = Me.ddlBusinessUnit.SelectedValue
        cmd.Parameters.Add(sqlpFRH_ID)

        Dim sqlpSTU_ID As New SqlParameter("@Level", SqlDbType.Int)
        sqlpSTU_ID.Value = Me.ddlLevel.SelectedValue
        cmd.Parameters.Add(sqlpSTU_ID)


        Dim ds As New DataSet
        Dim _adapter As New SqlDataAdapter(cmd)
        _adapter.Fill(ds)

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass

        Dim cmdSubColln As New SqlCommand

        ''SUBREPORT1
        cmdSubColln.CommandText = "EXEC [FEES].[F_GETReminderLetterDetail_Preview] '" & Me.ddlBusinessUnit.SelectedValue & "'"
        cmdSubColln.Connection = New SqlConnection(str_conn)
        cmdSubColln.CommandType = CommandType.Text
        repSourceSubRep(0).Command = cmdSubColln


        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("UserName") = Usr_name
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Dim bArabic As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(RMD_bARABIC, 0) FROM dbo.REMINDERTYPE_M WITH ( NOLOCK ) WHERE RMD_ID = " & Me.ddlLevel.SelectedValue & "")
        If IsNothing(bArabic) Then
            bArabic = False
        End If

        repSource.ReportUniqueName = "ArabicReminder"
        Dim BSUID As String
        BSUID = Session("sBsuid")
        repSource.ResourceName = IIf(bArabic, "../../fees/Reports/RPT/rptFeeReminder_Arabic.rpt", "../../fees/Reports/RPT/rptFeeReminder.rpt")

        If h_IsArabic.Value.ToLower = "true" Then
            repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT/rptFeeReminder_Arabic.rpt")
            If Not repSource Is Nothing Then
                Dim repClassVal As New RepClass
                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName
                repClassVal.SetDataSource(ds.Tables(0))
                If repSource.IncludeBSUImage Then
                    If repSource.HeaderBSUID Is Nothing Then
                        IncludeBSUmage(repClassVal, BSUID)
                    Else
                        IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                    End If

                End If
                SubReport(repSource, repSource, repClassVal)
                Dim pdfFilePath As String
                Dim pdfFileName As String
                pdfFileName = "ArabicReminder.pdf"

                pdfFilePath = WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
                ' pdfFilePath = "C:\0Junk\"
                pdfFilePath += pdfFileName

                repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                End While
                Try
                    If System.IO.File.Exists(pdfFilePath) Then
                        System.IO.File.Delete(pdfFilePath)
                    End If
                Catch ex As Exception

                End Try

                repClassVal.ExportToDisk(ExportFormatType.PortableDocFormat, pdfFilePath)

                Dim BytesData As Byte()
                BytesData = ConvertFiletoBytes(pdfFilePath)
                Dim ContentType, Extension As String
                ContentType = "application/pdf"
                Extension = GetFileExtension(ContentType)
                Dim Title As String = "Receipt"
                If repSource.ReportUniqueName.ToString.Split("_").Length > 1 Then
                    Title = repSource.ReportUniqueName.ToString.Split("_")(1)
                End If
                DownloadFile(System.Web.HttpContext.Current, BytesData, ContentType, Extension, Title, pdfFileName)
                repClassVal.Close()
                repClassVal.Dispose()
                repSource = Nothing
                repClassVal = Nothing
            End If

        Else
            h_print.Value = "print"
            Return repSource
        End If
    End Function

    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String, ByVal FileName As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.End()
    End Sub
    Private Shared Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Shared Sub SubReport(ByVal repSource As MyReportClass, ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(repSource, subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            reportHeader = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Shared Sub IncludeBSUmage(ByRef repClassVal As RepClass, ByVal BSUID As String)
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = BSUID
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub
End Class
