<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeStudRecordEdit.aspx.vb" Inherits="StudRecordEdit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript">
   function Set_Company(obj,compid)
   {
         if (obj.selectedIndex==0)
          {document.getElementById(compid).readOnly=false}
          else
           {document.getElementById(compid).readOnly='readonly';
           document.getElementById(compid).value='';}
        }
   
   </script> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>  Student Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
        <tr>
            <td align="left"> 
                <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>       
        <tr>
            <td>
             <table align="left" width="100%">                    
                    <tr>
                        <td align="center" width="100%" colspan="5">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%">
                            <span class="field-label">Student ID &amp; Name</span></td>                        
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtStud_No" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtFname_E" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%">
                        <asp:TextBox ID="txtMname_E" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%">
                        <asp:TextBox ID="txtLname_E" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label"> Grade</span></td>
                        
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtGRD_ID" runat="server" Width="112px" ></asp:TextBox>
                            </td>
                        <td align="left" width="20%">
                            <span class="field-label">Section</span></td>
                        
                        <td align=""left" width="30%">
                            <asp:TextBox ID="txtSCT_ID" runat="server" Width="85px" ></asp:TextBox>
                            </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Shift</span></td>
                       
                        <td text-align: left">
                            <asp:TextBox ID="txtShift_ID" runat="server"></asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Stream</span></td>
                       
                        <td style="text-align: left">
                            <asp:TextBox ID="txtStream" runat="server" Width="125px"></asp:TextBox></td>
                    </tr>
                     <tr>
                    <td align="left">
                        <span class="field-label">Date of Join</span></td>
                    
                    <td align="left">
                        <asp:TextBox ID="txtDOJ" runat="server" ></asp:TextBox></td>
                         <td align="left">
                            <span class="field-label">Academic Year</span></td>
                         
                         <td align="left">
                            <asp:TextBox ID="txtACD_ID" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        <span class="field-label">Father</span></td>
                    
                    <td align="left" colspan="3">
                        <table align="center" width="100%">
                            <tr>
                                <td align="left" width="33%"><asp:TextBox ID="txtFFirstName" runat="server"></asp:TextBox></td>
                                <td align="left" width="33%"><asp:TextBox ID="txtFMidName" runat="server"></asp:TextBox></td>
                                <td align="left" width="33%"><asp:TextBox ID="txtFLastName" runat="server"></asp:TextBox></td>
                            </tr>
                        </table>
                       </td>
                </tr>
                  <tr>
                    <td align="left">
                        <span class="field-label">Mother</span></td>
                    
                    <td align="left" colspan="3">
                        <table align="center" width="100%">
                            <tr>
                                <td align="left" width="33%"><asp:TextBox ID="txtMFirstName" runat="server"></asp:TextBox></td>
                                <td align="left" width="33%"><asp:TextBox ID="txtMMidName" runat="server" Width="175px"></asp:TextBox></td>
                                <td align="left" width="33%"><asp:TextBox ID="txtMLastName" runat="server" Width="175px"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>

                </tr>
                 <tr>
                    <td align="left">
                        <span class="field-label">Guardian</span></td>
                    
                    <td align="left" colspan="3">
                        <table align="center" width="100%">
                            <tr>
                                <td align="left" width="33%"><asp:TextBox ID="txtGFirstName" runat="server"></asp:TextBox></td>
                                <td align="left" width="33%"><asp:TextBox ID="txtGMidName" runat="server"></asp:TextBox></td>
                                <td align="left" width="33%"><asp:TextBox ID="txtGLastName" runat="server" Width="175px"></asp:TextBox></td>
                                </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="4" class="title-bg">
                        Edit Fee Sponsor</td>
                </tr>
                <tr>
                    <td align="left" class="text-dark">
                        <span class="field-label">Primary Contact</span></td>
                    
                    <td align="left" colspan="3">
                        <asp:RadioButton ID="rdFather" runat="server" GroupName="ContactPrimary"
                            Text="Father" CssClass="text-dark" Enabled="False" />
                        <asp:RadioButton ID="rdMother" runat="server" GroupName="ContactPrimary" Text="Mother" CssClass="text-dark" Enabled="False" />
                        <asp:RadioButton ID="rdGuardian" runat="server" GroupName="ContactPrimary" Text="Guardian" CssClass="text-dark" Enabled="False" /></td>
                </tr>
                <tr>
                    <td align="left" class="text-dark">
                        <span class="field-label">Preferred Contact</span></td>
                    
                    <td align="left">
                        <asp:DropDownList ID="ddlPrefContact" runat="server" CssClass="text-dark" Enabled="False">
                            <asp:ListItem>Home Phone</asp:ListItem>
                            <asp:ListItem>Office Phone</asp:ListItem>
                            <asp:ListItem>Mobile</asp:ListItem>
                            <asp:ListItem>Email</asp:ListItem>
                        </asp:DropDownList></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" class="text-dark" >
                        <span class="field-label">Father</span></td>
                    
                    <td align="left">
                        <asp:DropDownList id="ddlFCompany_Name" runat="server">
                        </asp:DropDownList>
                        <br />
                        (If you choose other,please specify the Company Name)</td>
                    <td align="left">
                        <asp:TextBox ID="txtFComp_Name" runat="server"></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" class="text-dark">
                        <span class="field-label">Mother</span></td>
                    
                    <td align="left">
                        <asp:DropDownList id="ddlMCompany_Name" runat="server">
                        </asp:DropDownList>
                        <br />
                        (If you choose other,please specify the Company Name)</td>
                    <td align="left">
                        <asp:TextBox ID="txtMComp_Name" runat="server"></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" class="text-dark">
                        <span class="field-label">Guardian</span></td>
                   
                    <td align="left">
                        <asp:DropDownList id="ddlGCompany_Name" runat="server">
                        </asp:DropDownList>
                        <br />
                        (If you choose other,please specify the Company Name)</td>
                    <td align="left">
                        <asp:TextBox ID="txtGComp_Name" runat="server"></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" class="text-dark" >
                        <span class="field-label">Fee Sponsor</span></td>
                    
                    <td align="left">
                        <asp:DropDownList id="ddFeeSponsor" runat="server" CssClass="text-dark">
                            <asp:ListItem Value="1">Father</asp:ListItem>
                            <asp:ListItem Value="2">Mother</asp:ListItem>
                            <asp:ListItem Value="3">Guardian</asp:ListItem>
                            <asp:ListItem Value="4">Fathers Company</asp:ListItem>
                            <asp:ListItem Value="5">Mothers Company</asp:ListItem>
                            <asp:ListItem Value="6">Guardians Company</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                        <td align="left">
                        <asp:Button id="btnSave" runat="server" OnClick="BtnSave_Click" Text="Save Fee Sponsor" CssClass="button" /></td>
                    <td></td>
                </tr>                             
            </table>                        
          </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
                <asp:HiddenField ID="h_EmpImagePath" runat="server" />
                <asp:HiddenField ID="h_SliblingID" runat="server" /> 
                
                </div>
            </div>
        </div>
                   
</asp:Content>

