<%@ Page Language="VB" AutoEventWireup="false" CodeFile="feeSetUpServicesAndCharge.aspx.vb" Inherits="Transport_feeSetUpServicesAndCharge" Theme="General" %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
 <base target="_self" />
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    </head>
   <script language="javascript" type="text/javascript">  
    function ReturnTotal()
        { 
          //  window.returnValue = document.getElementById(' ').value+"|"+document.getElementById(' ').value; 
            window.close();    
        }   
<%--function getLOCATION() 
   {  
        var sFeatures,url;
        sFeatures="dialogWidth: 600px; ";
        sFeatures+="dialogHeight: 500px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result; 
          url= "../common/PopupFormIDhidden.aspx?iD=LOCATION&bsu=<%= Request.QueryString("bsu") %>&MULTISELECT=FALSE";
          result = window.showModalDialog(url,"", sFeatures);
           if (result=='' || result==undefined)
            {
            return false;
            }             
            NameandCode = result.split('___');
           document.getElementById('<%= H_Location.ClientID %>').value=NameandCode[0]; 
           document.getElementById('<%= txtLocation.ClientID %>' ).value=NameandCode[1] ; 
           return true;
    }  --%>
    
<%--    function getStudent() 
   {     
        var sFeatures,url,stuName;
        sFeatures="dialogWidth: 700px; ";
        sFeatures+="dialogHeight: 600px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        stuName='<%= txtStudentname.ClientID %>';
        var NameandCode; 
        var result;
            url= "ShowStudentTransport.aspx?type=STUD&bsu=<%= Request.QueryString("bsu") %>";
        result = window.showModalDialog(url,"", sFeatures);
        if (result=='' || result==undefined)
            {
            return false;
            } 
        NameandCode = result.split('||');
        document.getElementById('<%= h_Student_no.ClientID %>').value=NameandCode[0]; 
        document.getElementById(stuName).value=NameandCode[1] ; 
        document.getElementById('<%= txtStdNo.ClientID %>' ).value=NameandCode[2] ; 
        return true;
    } --%>      
    </script>  
    <script>    
        function getStudent() 
        {
         var stuName='<%= txtStudentname.ClientID %>';
      
            url= "ShowStudentTransport.aspx?type=STUD&bsu=<%= Request.QueryString("bsu") %>";
            var oWnd = radopen(url, "pop_student");
        }


        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%= h_Student_no.ClientID %>').value=NameandCode[0]; 
                document.getElementById('<%= txtStudentname.ClientID %>').value=NameandCode[1] ; 
                document.getElementById('<%= txtStdNo.ClientID %>' ).value=NameandCode[2] ; 
		// __doPostBack('<%= txtStdNo.ClientID%>', 'TextChanged');
            }
        }
        function getLOCATION() 
   {  
          var url= "../common/PopupFormIDhidden.aspx?iD=LOCATION&bsu=<%= Request.QueryString("bsu") %>&MULTISELECT=FALSE";
        
            var oWnd = radopen(url, "pop_getLOCATION");        
        }
                function OnClientClose2(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameCode.split('||');

              document.getElementById('<%= H_Location.ClientID %>').value=NameandCode[0]; 
           document.getElementById('<%= txtLocation.ClientID %>' ).value=NameandCode[1] ; 
            }
        }
  
function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_getLOCATION" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

<body>
    <form id="form1" runat="server">
   <table width="100%">
        <tr >
            <td align="left" class="title-bg" colspan="2"  >
                Set Area<ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
                </ajaxToolkit:ToolkitScriptManager>
                </td>
        </tr>
       <tr> 
           <td align="left" class="matters" >
            <span class="field-label">   Service Start
               Date</span></td>
           <td align="left" class="matters" >
               <asp:TextBox ID="txtFrom" runat="server" TabIndex="15"
                  ></asp:TextBox>
               <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20"   />
               
           </td>
       </tr>
       <tr>
           <td align="left" class="matters" >
              <span class="field-label"> Select Student</span></td>
           <td align="left" class="matters" >
               <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True" ></asp:TextBox>&nbsp;
               <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getStudent();return false;" />
               <asp:TextBox ID="txtStudentname" runat="server"  ></asp:TextBox>
               <br />
               <asp:Label ID="lblNoStudent" runat="server" CssClass="error" EnableViewState="False"
                   ForeColor="Red"></asp:Label></td>
       </tr>
       <tr>
           <td align="left" class="matters" >
               <span class="field-label">Area</span></td>
           <td align="left" class="matters" valign="middle" >
               <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="True"  
                    ></asp:TextBox>&nbsp;
               <asp:ImageButton ID="imgLocation" runat="server"   ImageAlign="AbsMiddle"
                   ImageUrl="~/Images/Misc/Route.png" OnClientClick="getLOCATION(); return false;" TabIndex="30"   /></td>
       </tr>
        <tr>
            <td align="center" class="matters" colspan="2">
    <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish" />
                <asp:Button ID="Button1" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
                    Text="Close" />
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label></td>
        </tr>
        </table>
        <asp:HiddenField ID="H_Location" runat="server" />
       
        <asp:HiddenField ID="h_Student_no" runat="server" />
         <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFrom"
                    TargetControlID="txtFrom">
                   </ajaxToolkit:CalendarExtender>
    </form>
   
</body>
</html>
