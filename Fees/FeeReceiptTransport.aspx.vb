﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Fees_FeeReceiptTransport
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Private Property BALANCE() As Double
        Get
            Return ViewState("BALANCE")
        End Get
        Set(ByVal value As Double)
            ViewState("BALANCE") = value
        End Set
    End Property
    Private Property BSU_CURRENCY() As String
        Get
            Return ViewState("BSU_CURRENCY")
        End Get
        Set(ByVal value As String)
            ViewState("BSU_CURRENCY") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'Page.Title = FeeCollectionOnlineBB.GetTransportTitle()7bad10
                gvPayments.Attributes.Add("bordercolor", "#000000")
                gvFeeDetails.Attributes.Add("bordercolor", "#000000")
                Select Case Request.QueryString("type")
                    Case "REC"
                        If Request.QueryString("id") <> "" Then
                            PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("user").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("stu_bsu_id").Replace(" ", "+")))
                        End If
                End Select
                If Session("sbsuid") = "900520" Then
                    Me.trAddress.Visible = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub PrintReceipt(ByVal p_BSU_ID As String, ByVal p_Receiptno As String, _
        ByVal USER_NAME As String, ByVal p_Stu_Bsu_id As String)

        Dim str_Sql, strFilter As String
        strFilter = "  FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & p_BSU_ID & "' "
        str_Sql = "select * FROM FEES.VW_OSO_FEES_RECEIPT WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text

        Dim dsReceipt As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        dsReceipt = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If Not dsReceipt Is Nothing Or dsReceipt.Tables(0).Rows.Count > 0 Then
            imgLogo.ImageUrl = "~/Common/GetLogo.aspx?BSU_ID=" & p_BSU_ID
            lblAddress.Text = dsReceipt.Tables(0).Rows(0)("BSU_ADDRESS")
            lblHeader1.Text = dsReceipt.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = dsReceipt.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = dsReceipt.Tables(0).Rows(0)("BSU_HEADER3")
            lblProvider.Text = dsReceipt.Tables(0).Rows(0)("PROVIDER")
            lblProvidername.Text = lblProvider.Text
            lblSchoolNameCheque.Text = lblProvidername.Text

            lblProvidernameFavour.Text = "Thank you for choosing " & lblProvider.Text & "."
            lblStuShoolName.Text = dsReceipt.Tables(0).Rows(0)("BSU_NAME")
            lblArea.Text = dsReceipt.Tables(0).Rows(0)("SBL_DESCRIPTION")

            'lblBusno.Text = IIf(dsReceipt.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", dsReceipt.Tables(0).Rows(0)("BNO_DESCR"))
            lblBusno.Text = IIf(dsReceipt.Tables(0).Rows(0)("ONWARDBUS") Is System.DBNull.Value, "", dsReceipt.Tables(0).Rows(0)("ONWARDBUS"))
            lblBusno.Text = lblBusno.Text + IIf(dsReceipt.Tables(0).Rows(0)("RETURNBUS") Is System.DBNull.Value, "", "/ " + dsReceipt.Tables(0).Rows(0)("RETURNBUS"))
            lblDate.Text = Format(dsReceipt.Tables(0).Rows(0)("FCL_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(dsReceipt.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", dsReceipt.Tables(0).Rows(0)("GRD_DISPLAY"))
            lblRecno.Text = dsReceipt.Tables(0).Rows(0)("FCL_RECNO")


            lblStudentNo.Text = dsReceipt.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = dsReceipt.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblNarration.Text = dsReceipt.Tables(0).Rows(0)("FCL_NARRATION")
            Dim formatstring As String = Session("BSU_DataFormatString")
            formatstring = formatstring.Replace("0.", "###,###,###,##0.")
            If FeeCollection.GetDoubleVal(dsReceipt.Tables(0).Rows(0)("FCL_BALANCE")) <> 0 Then
                'If dsReceipt.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
                '    lblBalance.Text = "Due : " & dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(dsReceipt.Tables(0).Rows(0)("FCL_BALANCE"), formatstring)
                'Else
                '    lblBalance.Text = "Advance : " & dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(dsReceipt.Tables(0).Rows(0)("FCL_BALANCE") * -1, formatstring)
                'End If
                BALANCE = FeeCollection.GetDoubleVal(dsReceipt.Tables(0).Rows(0)("FCL_BALANCE"))
            End If
            Dim mcSpell As New Mainclass
            lbluserloggedin.Text = "Printed by " & USER_NAME & " (Time: " + Now.ToString("dd/MMM/yyyy hh:mm:ss tt") & ")"
            Dim LogDatetime As String = Format(dsReceipt.Tables(0).Rows(0)("FCL_LOGDATE"), "dd/MMM/yyyy hh:mm:ss tt")
            lblPrintTime.Text = "Ref. " & dsReceipt.Tables(0).Rows(0)("FCL_EMP_ID") & " - " & LogDatetime & " - " & dsReceipt.Tables(0).Rows(0)("FCL_COUNTERSERIES").ToString

            'lblAmount.Text = "Amount in words " + mcSpell.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            Dim str_paymnts As String = " exec fees.GetReceiptPrint_Transport @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & p_BSU_ID & "'"

            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()
            gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True

            gvPayments.DataSource = ds1.Tables(1)
            gvPayments.DataBind()
            Dim objTotal As Object = gvPayments.DataKeys(gvPayments.Rows.Count - 1).Values(2)
            Dim str_Total As String = "0.00"
            If IsNumeric(objTotal) Then
                str_Total = Format(objTotal, "#,##0.00")
            End If

            Dim columnCount As Integer = gvPayments.FooterRow.Cells.Count
            gvPayments.FooterRow.Cells.Clear()
            gvPayments.FooterRow.Cells.Add(New TableCell)
            gvPayments.FooterRow.Cells(0).ColumnSpan = columnCount
            gvPayments.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Left
            gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(objTotal, BSU_CURRENCY, Session("BSU_ROUNDOFF")) 'mcSpell.SpellNumberWithDenomination(dsReceipt.Tables(0).Rows(0)("FCL_AMOUNT"), dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY"), 0)
            gvPayments.FooterRow.Cells(0).Font.Bold = True
            'Dim str_Total As String = gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(4).Text
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Clear()
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).ColumnSpan = columnCount - 1
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).Text = "Total"
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).HorizontalAlign = HorizontalAlign.Right

            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).Text = str_Total
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).HorizontalAlign = HorizontalAlign.Right
            lblMessageGMS.Text = F_GetTransportFeeReceiptNarration(dsReceipt.Tables(0).Rows(0)("FCL_ACD_ID").ToString)
            If lblMessageGMS.Text = "" Then
                lblMessageGMS.Visible = False
            Else
                lblMessageGMS.Visible = True
            End If

            If Request.QueryString("isDuplicate") = 1 Then
                lblCopy.Visible = True
            Else
                lblCopy.Visible = False
            End If
        End If
    End Sub
    Public Shared Function F_GetTransportFeeReceiptNarration(ByVal p_ACD_ID As String) As String
        Dim pParms(0) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(0).Value = p_ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetTransportFeeReceiptNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function
    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #000000 0pt solid; border-right: #000000 1pt dotted; border-top: #000000 1pt dotted; border-bottom: #000000 1pt dotted;"
        Next
        If e.Row.Cells(0).Text = "Total" Then
            e.Row.Font.Bold = True
        End If
        If e.Row.Cells(0).Text.ToUpper.Trim.StartsWith("TRANSPORT") Then
            If lblNarration.Text.Replace("&nbsp;", "").Trim <> "" Then
                e.Row.Cells(0).Text = lblNarration.Text
            End If
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblFCSAmt As Label = e.Row.FindControl("lblFCSAmt")
            Dim lblFEEDESCR As Label = e.Row.FindControl("lblFEEDESCR")
            Dim AMOUNT_DIFFERENCE As Double = 0, FCA_bPDC_CANCELLED As Boolean = False, FCA_PDC_CANCEL_AMOUNT As Double = 0
            Dim FCS_FEE_ID As Int16 = CInt(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCS_FEE_ID"))
            FCA_bPDC_CANCELLED = CBool(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCA_bPDC_CANCELLED"))
            FCA_PDC_CANCEL_AMOUNT = CDbl(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCA_PDC_CANCEL_AMOUNT"))
            AMOUNT_DIFFERENCE = CDbl(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("AMOUNT_DIFFERENCE"))

            Dim diff As Double = 0
            If FCA_bPDC_CANCELLED And FCA_PDC_CANCEL_AMOUNT > 0 Then
                diff = AMOUNT_DIFFERENCE
                Dim tc As TableCell = e.Row.Cells(1)
                tc.Text = "<span>" & Format(diff, "#,##0.00") & "&nbsp;&nbsp;<span style='text-decoration: line-through; color:red;'>" & lblFCSAmt.Text & "</span></span>"
            End If

            If Not lblFEEDESCR Is Nothing Then
                If lblFEEDESCR.Text.ToUpper.Trim.StartsWith("TRANSPORT") Then
                    If lblNarration.Text.Replace("&nbsp;", "").Trim <> "" Then
                        e.Row.Cells(0).Text = lblNarration.Text
                    End If
                End If
            End If

            If lblFEEDESCR.Text = "Total Paid" Then
                Dim tc As TableCell = e.Row.Cells(1)
                If AMOUNT_DIFFERENCE <> 0 Then
                    tc.Text = "<span>" & Format(AMOUNT_DIFFERENCE, "#,##0.00") & "&nbsp;&nbsp;<span style='text-decoration: line-through; color:red;'>" & lblFCSAmt.Text & "</span></span>"
                Else
                    tc.Text = "<span>" & Format(CDbl(lblFCSAmt.Text), "#,##0.00") & "</span>"
                End If

                If (BALANCE) > 0 Then
                    Dim NewBal = (BALANCE - FCA_PDC_CANCEL_AMOUNT) 'BALANCE
                    lblBalance.Text = IIf(NewBal > 0, "Due", "Advance") & " : " & BSU_CURRENCY & " " & Format(Math.Abs(NewBal), "#,##0.00")
                Else
                    Dim NewBal = (BALANCE + FCA_PDC_CANCEL_AMOUNT) 'BALANCE '
                    lblBalance.Text = IIf(NewBal > 0, "Due", "Advance") & " : " & BSU_CURRENCY & " " & Format(Math.Abs(NewBal), "#,##0.00")
                End If
            End If
        End If
    End Sub

    Protected Sub gvPayments_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvPayments.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim FCD_bPDC_CANCELLED As New Boolean
            Dim FCD_PDC_CANCELLED_DATE As New Date
            If Not gvPayments.DataKeys(e.Row.RowIndex).Values(0) Is Nothing AndAlso Not gvPayments.DataKeys(e.Row.RowIndex).Values(0) Is DBNull.Value Then
                FCD_bPDC_CANCELLED = gvPayments.DataKeys(e.Row.RowIndex).Values(0)
            Else
                FCD_bPDC_CANCELLED = False
            End If
            If Not gvPayments.DataKeys(e.Row.RowIndex).Values(1) Is Nothing AndAlso Not gvPayments.DataKeys(e.Row.RowIndex).Values(1) Is DBNull.Value Then
                FCD_PDC_CANCELLED_DATE = gvPayments.DataKeys(e.Row.RowIndex).Values(1)
            Else
                FCD_PDC_CANCELLED_DATE = Nothing
            End If

            If FCD_bPDC_CANCELLED AndAlso IsDate(FCD_PDC_CANCELLED_DATE) Then
                Dim i As Int16 = 0
                For Each tr As TableCell In e.Row.Cells
                    'tr.Attributes("style") = "background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAA8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD5rooor8DP9oD/2Q=='); background-repeat: repeat-x; background-position: 50% 50%; width:100%;"
                    If (i = 0) Then
                        tr.Text = "<span>" & tr.Text & "<span style='color:red;'>(Cancelled on " & Format(FCD_PDC_CANCELLED_DATE, OASISConstants.DataBaseDateFormat) & ")</span></span>"
                    Else
                        tr.Text = "<span style='text-decoration: line-through;width:100%;color:red;'>" & tr.Text & "</span>"
                        'tr.Attributes("style") = "color:red;"
                    End If
                    i += 1
                Next

            End If
        End If
    End Sub
End Class

