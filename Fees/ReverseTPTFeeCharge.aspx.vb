Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GemBox.Spreadsheet
Imports Telerik.Web
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Threading.Tasks

Partial Class ReverseTPTFeeCharge
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass
    Dim AegAmount As Double
    Dim SetAmount As Double
    Dim SerialNo As Int32
    Public Shared Has_Error As Boolean

    Private Property VSgvExcelImport() As DataTable
        Get
            Return ViewState("gvExcelImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvExcelImport") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImport)

        lnkXcelFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("REVERSETRANSPORT") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/TransportChargeReverseFormatFile.xls")
        'smScriptManager.RegisterPostBackControl(chkSelectAllBsu)

        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> "F100177" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If Session("sroleid") = "204" Then
                rblChargeType.Visible = True
            Else
                rblChargeType.Visible = False
            End If

        End If
        txtAdjDT.Text = Date.Now.ToString("dd/MMM/yyyy")
        'BindFeeTypeAccessBsu()
    End Sub

    Protected Sub btnPostback_Click(sender As Object, e As EventArgs)
        If Me.h_STATUS.Value = "0" Then
            Dim errormsg As String = h_errormessage.Value
            Dim IsSuccess As String = h_IsSuccess.Value
            Me.lblMessage.Text = ""
            Me.btnCommitImprt.Visible = False
            Me.btnProceedImpt.Visible = False
            Me.btnImport.Visible = True
            Me.btnAddAdj.Visible = True
            usrMessageBar.ShowNotification(errormsg, If(IsSuccess = "1", UserControls_usrMessageBar.WarningType.Success, UserControls_usrMessageBar.WarningType.Danger))
            h_Processeing.Value = ""
            h_IsSuccess.Value = 0
        End If
    End Sub

    Protected Sub gvExcelImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExcelImport.PageIndexChanging
        Me.gvExcelImport.PageIndex = e.NewPageIndex
        BindExcel()
    End Sub

    Sub BindExcel()
        Me.gvExcelImport.DataSource = VSgvExcelImport
        Me.gvExcelImport.DataBind()
    End Sub

    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Me.gvImportSmry.DataSource = Nothing
        Me.gvImportSmry.DataBind()
        VSgvExcelImport = Nothing
        BindExcel()
        ImportAdjustmentSheet()

    End Sub

    Protected Sub btnProceedImpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceedImpt.Click
        Try
            Dim AdjDt As Date = CDate(txtAdjDT.Text)
        Catch ex As Exception
            usrMessageBar.ShowNotification("Invalid adjustment date", UserControls_usrMessageBar.WarningType.Danger)
            txtAdjDT.Focus()
            Exit Sub
        End Try
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Using con As New SqlConnection(str_conn)
            con.Open()
            Dim stTrans As SqlTransaction = con.BeginTransaction
            Try
                Using cmd As New SqlCommand("DataImport.IMPORT_BULK_REVERSE_TRANSPORT", con, stTrans)
                    cmd.CommandTimeout = 0
                    cmd.CommandType = CommandType.StoredProcedure
                    Dim sqlParam(9) As SqlParameter
                    Dim dtParam As DataTable = VSgvExcelImport
                    If dtParam.Columns.Contains("bVALID") Then
                        dtParam.Columns.Remove("bVALID")
                    End If
                    If dtParam.Columns.Contains("ERROR_MSG") Then
                        dtParam.Columns.Remove("ERROR_MSG")
                    End If
                    Dim drlist As New List(Of DataRow)()
                    If (dtParam.Columns.Contains("SLNO")) Then
                        dtParam.Columns.Remove("SLNO")
                    End If
                    dtParam.AcceptChanges()
                    sqlParam(0) = New SqlParameter("@DT_XL", dtParam)
                    cmd.Parameters.Add(sqlParam(0))
                    sqlParam(1) = New SqlParameter("@BSU_ID", Session("sBsuId"))
                    cmd.Parameters.Add(sqlParam(1))

                    sqlParam(2) = New SqlParameter("@ADJ_DATE", txtAdjDT.Text)
                    cmd.Parameters.Add(sqlParam(2))
                    sqlParam(3) = New SqlParameter("@FAI_USER", Session("sUsr_name"))
                    cmd.Parameters.Add(sqlParam(3))

                    sqlParam(4) = New SqlParameter("@RET_VAL", 0)
                    sqlParam(4).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(4))
                    sqlParam(5) = New SqlParameter("@BATCH_NO", 0)
                    sqlParam(5).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(5))
                    sqlParam(6) = New SqlParameter("@ADJ_TYPE", rblChargeType.SelectedValue)
                    cmd.Parameters.Add(sqlParam(6))

                    Using dr As SqlDataReader = cmd.ExecuteReader()
                        Dim dttb As New DataTable
                        dttb.Load(dr)
                        If sqlParam(4).Value <> 0 Then
                            stTrans.Rollback()
                            'stTrans.Commit()
                            Me.btnImport.Visible = True
                            Me.btnCommitImprt.Visible = False
                            btnProceedImpt.Visible = False
                        Else
                            stTrans.Commit()
                            'stTrans.Rollback()
                            btnProceedImpt.Visible = False
                            Me.btnCommitImprt.Visible = True
                            ViewState("FAI_BATCHNO") = sqlParam(5).Value
                            hdBATCHNO.Value = sqlParam(5).Value
                            ShowAdjustmentSummary()
                            DisableControls(False)
                        End If
                        If Not dttb Is Nothing AndAlso dttb.Rows.Count > 0 Then
                            gvExcelImport.Columns(10).Visible = True 'shoes the error message column

                            If (Not dttb.Columns.Contains("SLNO")) Then
                                dttb.Columns.Add("SLNO")
                                Dim dtDataTable As DataTable = ViewState("ExcelData")
                                For i As Integer = 0 To dttb.Rows.Count - 1
                                    dttb.Rows(i)(dttb.Columns.Count - 1) = DirectCast(ViewState("ExcelData"), DataTable).Rows(i)("SLNO")
                                Next
                            End If
                            VSgvExcelImport = dttb
                            BindExcel()
                        End If
                    End Using
                    con.Close()
                End Using
            Catch ex As Exception
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
            End Try
        End Using
    End Sub

    Sub DisableControls(ByVal bEnabled As Boolean)
        txtAdjDT.Enabled = bEnabled
        FileUpload1.Enabled = bEnabled
    End Sub

    Private Sub ShowAdjustmentSummary()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim dsSummary = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "dbo.GET_FEEREVERSE_TRANSPORT_SUMMARY '" & ViewState("FAI_BATCHNO") & "'")
        If dsSummary.Tables(0).Rows.Count > 0 Then
            Me.gvImportSmry.DataSource = dsSummary.Tables(0)
            Me.gvImportSmry.DataBind()
        End If
    End Sub

    Private Sub ImportAdjustmentSheet()
        Try
            lblError.Text = ""
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If
            End If
            If File.Exists(FileName) Then
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                'Dim ef As New GemBox.Spreadsheet.ExcelFile
                Dim workbook = ExcelFile.Load(FileName)
                ' Select active worksheet.
                Dim worksheet = workbook.Worksheets.ActiveWorksheet
                Dim dtXL As New DataTable
                If worksheet.Rows.Count > 1 Then
                    dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With
                            {
                             .ColumnHeaders = True,
                             .StartRow = 0,
                             .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count,
                             .NumberOfRows = worksheet.Rows.Count,
                             .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                            })
                End If
                ' File delete first 
                File.Delete(FileName)
                If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then
                    ViewState("ExcelData") = dtXL

                    Dim dvxl As DataView = New DataView(dtXL)
                    VSgvExcelImport = dvxl.ToTable()

                    Dim Columns As DataColumnCollection = VSgvExcelImport.Columns

                    If Not Columns.Contains("STU_NAME") Then
                        VSgvExcelImport.Columns.Add("STU_NAME", GetType(String), "")
                    End If

                    If Not Columns.Contains("STU_BSU_ID") Then
                        VSgvExcelImport.Columns.Add("STU_BSU_ID", GetType(String), "")
                    End If

                    If Not Columns.Contains("STU_ID") Then
                        VSgvExcelImport.Columns.Add("STU_ID", GetType(String), "")
                    End If
                    If Not Columns.Contains("FEE_ID") Then
                        VSgvExcelImport.Columns.Add("FEE_ID", GetType(String), "")
                    End If

                    VSgvExcelImport.AcceptChanges()

                    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
                    Using con As New SqlConnection(str_conn)
                        con.Open()
                        Dim stTrans As SqlTransaction = con.BeginTransaction
                        Try
                            Using cmd As New SqlCommand("[DBO].[RESERVE_TRANSPORT_EXCEL_DATA]", con, stTrans)
                                cmd.CommandTimeout = 0
                                cmd.CommandType = CommandType.StoredProcedure
                                Dim sqlParam(2) As SqlParameter
                                Dim dtParam As DataTable = VSgvExcelImport

                                If (dtParam.Columns.Contains("SLNO")) Then
                                    dtParam.Columns.Remove("SLNO")
                                End If

                                dtParam.AcceptChanges()
                                sqlParam(0) = New SqlParameter("@DT_XL", dtParam)
                                cmd.Parameters.Add(sqlParam(0))

                                Using dr As SqlDataReader = cmd.ExecuteReader()
                                    Dim dttb As New DataTable
                                    dttb.Load(dr)
                                    If Not dttb Is Nothing AndAlso dttb.Rows.Count > 0 Then
                                        gvExcelImport.Columns(10).Visible = True 'shoes the error message column
                                        If (Not dttb.Columns.Contains("SLNO")) Then
                                            dttb.Columns.Add("SLNO")
                                            Dim dtDataTable As DataTable = ViewState("ExcelData")
                                            For i As Integer = 0 To dttb.Rows.Count - 1
                                                dttb.Rows(i)(dttb.Columns.Count - 1) = DirectCast(ViewState("ExcelData"), DataTable).Rows(i)("SLNO")
                                            Next
                                        End If

                                        If Not dttb.Columns.Contains("bVALID") Then
                                            dttb.Columns.Add("bVALID", GetType(Boolean), True)
                                        End If
                                        If Not dttb.Columns.Contains("ERROR_MSG") Then
                                            dttb.Columns.Add("ERROR_MSG", GetType(String), "")
                                        End If
                                        ViewState("ExcelData") = dttb
                                        VSgvExcelImport = dttb
                                        BindExcel()
                                    End If
                                End Using
                                con.Close()
                            End Using
                        Catch ex As Exception
                            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                        Finally
                            If con.State = ConnectionState.Open Then
                                con.Close()
                            End If
                        End Try
                    End Using

                    Me.trGvImport.Visible = True
                    Me.lblMessage.Text = "Click Proceed to Import the data"
                    gvExcelImport.Columns(10).Visible = False 'hides the error message column initially
                    Me.btnProceedImpt.Visible = True
                    Me.btnImport.Visible = False
                End If
            End If
        Catch ex As Exception
            Errorlog("Unable to load datatable from file - ImportAdjustmentSheet()")
        End Try
    End Sub

    Protected Sub btnCancelAdj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAdj.Click

        lblError.Text = ""
        DisableControls(True)
        Me.gvImportSmry.DataSource = Nothing
        Me.gvImportSmry.DataBind()
        VSgvExcelImport = Nothing
        BindExcel()
        Me.trGvImport.Visible = False
        Me.lblMessage.Text = ""
        gvExcelImport.Columns(10).Visible = False 'hides the error message column initially
        Me.btnProceedImpt.Visible = False
        Me.btnCommitImprt.Visible = False
        Me.btnImport.Visible = True
        ViewState("FAI_BATCHNO") = 0
        hdBATCHNO.Value = 0
    End Sub

    Protected Sub btnCommitImprt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitImprt.Click
        Session("API_ERROR-FEE_REV") = False
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)

        Dim retmsg As String = ""
        If VSgvExcelImport.Rows.Count < 15 Or 1 = 1 Then
            objConn.Open()
            transaction = objConn.BeginTransaction("SampleTransaction")
            Try
                Dim sqlParam(2) As SqlParameter
                Dim cmd As New SqlCommand
                cmd.Dispose()
                cmd = New SqlCommand("[DBO].[IMPORT_FEE_CHARGE]", objConn, transaction)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 0

                sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retmsg, SqlDbType.VarChar, True, 200)
                cmd.Parameters.Add(sqlParam(0))
                sqlParam(1) = Mainclass.CreateSqlParameter("@FAI_BATCHNO", ViewState("FAI_BATCHNO"), SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(1))
                cmd.ExecuteNonQuery()
                retmsg = sqlParam(0).Value
                If retmsg = "0" Then
                    transaction.Commit()
                    ' transaction.Rollback()
                    usrMessageBar.ShowNotification("Charge reversal processed successfully", UserControls_usrMessageBar.WarningType.Success)
                    Me.btnImport.Visible = True
                    Me.btnCommitImprt.Visible = False
                    btnProceedImpt.Visible = False
                Else
                    transaction.Rollback()
                    usrMessageBar.ShowNotification("Unable to process charge reversal, " & getErrorMessage(retmsg), UserControls_usrMessageBar.WarningType.Danger)
                    Errorlog("Rollback - " & retmsg, "PHOENIX")
                    Exit Sub
                End If
            Catch ex As Exception
                transaction.Rollback()
                usrMessageBar.ShowNotification("Unable to process charge reversal, Error:" & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                Errorlog("Transport bulk charge reversal - Exception: " & ex.Message, "PHOENIX")
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        Else
            h_Processeing.Value = "1"
            Dim task As Task
            task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                    objConn.Open()
                                                                    transaction = objConn.BeginTransaction("SampleTransaction")
                                                                    Try
                                                                        Dim sqlParam(2) As SqlParameter

                                                                        Dim cmd As New SqlCommand
                                                                        cmd.Dispose()
                                                                        cmd = New SqlCommand("[DBO].[IMPORT_FEE_CHARGE]", objConn, transaction)
                                                                        cmd.CommandType = CommandType.StoredProcedure
                                                                        cmd.CommandTimeout = 0
                                                                        sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retmsg, SqlDbType.VarChar, True, 200)
                                                                        cmd.Parameters.Add(sqlParam(0))
                                                                        sqlParam(1) = Mainclass.CreateSqlParameter("@FAI_BATCHNO", ViewState("FAI_BATCHNO"), SqlDbType.VarChar)
                                                                        cmd.Parameters.Add(sqlParam(1))
                                                                        cmd.ExecuteNonQuery()
                                                                        retmsg = sqlParam(0).Value
                                                                        If retmsg = "0" Then
                                                                            transaction.Commit()
                                                                            Me.btnImport.Visible = True
                                                                            Me.btnCommitImprt.Visible = False
                                                                            btnProceedImpt.Visible = False
                                                                            System.Threading.Thread.Sleep(1000)
                                                                            h_Processeing.Value = ""
                                                                        Else
                                                                            transaction.Rollback()
                                                                            System.Threading.Thread.Sleep(1000)
                                                                            h_Processeing.Value = ""
                                                                            Session("API_ERROR-FEE_REV") = True
                                                                            Exit Sub
                                                                        End If
                                                                    Catch ex As Exception
                                                                        transaction.Rollback()
                                                                        System.Threading.Thread.Sleep(1000)
                                                                        h_Processeing.Value = ""
                                                                        Session("API_ERROR-FEE_REV") = True
                                                                        Errorlog("Transport bulk charge reversal - Exception: " & ex.Message, "PHOENIX")
                                                                    Finally
                                                                        If objConn.State = ConnectionState.Open Then
                                                                            objConn.Close()
                                                                        End If
                                                                    End Try
                                                                End Sub)
        End If
    End Sub

End Class


