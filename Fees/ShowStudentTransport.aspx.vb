Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Partial Class Fees_ShowStudentTransport
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        If Page.IsPostBack = False Then
            Try

                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"

                Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
                Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
                Select Case type
                    Case "STUD_COMP"
                        Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                        GridbindStudentCompany(COMP_ID, BSU)
                    Case "ENQ_COMP"
                        Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                        GridbindEnquiryCompany(COMP_ID, BSU)
                    Case "TC"
                        Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
                        Dim TYPE_VAL As Integer = IIf(Request.QueryString("VAL") = "", -1, Request.QueryString("VAL"))
                        Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))

                        'If TYPE_VAL = 1 Then
                        '    GridbindTC(BSU, False, STU_ID, vACD_ID)
                        'ElseIf TYPE_VAL = 2 Then
                        '    GridbindTC(BSU, True, STU_ID)
                        'ElseIf TYPE_VAL = 3 Then
                        GridbindTC(BSU, False, STU_ID)
                        'End If

                    Case "REFER"
                        Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
                        Gridbind(BSU, STU_ID)
                    Case "STU_TRAN"
                        Dim str_ACD As String = Request.QueryString("acd") & ""
                        Dim str_Prevacd As String = Request.QueryString("prevacd") & ""
                        GridbindTransport(BSU, str_ACD, str_Prevacd)
                    Case Else
                        Gridbind(BSU)
                End Select
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        Dim txtSearch As TextBox = gvGroup.HeaderRow.FindControl("txtCode")
        If Not txtSearch Is Nothing Then
            txtSearch.Focus()
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))

    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvGroup.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub GridbindTC(ByVal BSU_ID As String, ByVal bTC As Boolean, _
    Optional ByVal STUD_ID As String = "", Optional ByVal vACD_ID As Integer = 0)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_Sql As String = String.Empty
            'If bTC Then
            '    ' str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            '    '& " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            '    '& " FROM VW_OSO_STUDENT_M INNER JOIN VW_OSO_TCM_M ON " _
            '    '& " TCM_BSU_ID =STU_BSU_ID AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID " _
            '    '& " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' " _
            '    '& " AND isnull(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' AND TCM_bCANCELLED = 0 AND " & _
            '    '" TCM_bRegAppr = 1 AND ISNULL(TCM_bCANCELLED,0) = 0 "

            '    str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            '    & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            '    & " FROM VW_OSO_STUDENT_M INNER JOIN STUDENT_DISCONTINUE_S ON " & _
            '    " VW_OSO_STUDENT_M.STU_ID = STUDENT_DISCONTINUE_S.STD_STU_ID " & _
            '    " WHERE ISNULL(STUDENT_DISCONTINUE_S.STD_bAPPROVE, 0) = 1 AND " & _
            '    " STUDENT_DISCONTINUE_S.STD_ID not in( select FAR_EVENT_REF_ID from [OASIS_TRANSPORT].[FEES].[FEEADJREQUEST_H] ) AND " & _
            '    " STUDENT_DISCONTINUE_S.STD_TYPE = 'P' AND STU_BSU_ID='" & BSU_ID & "' "
            'Else
            '    str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            '    & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            '    & " FROM VW_OSO_STUDENT_M " _
            '    & " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' "
            'End If


            str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
                & " FROM VW_OSO_STUDENT_M " _
                & " WHERE  STU_BSU_ID='" & BSU_ID & "' "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & _
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub Gridbind(ByVal BSU_ID As String, Optional ByVal STUD_ID As String = "")
        Try
            Dim str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""

            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""

            ''''''''''
            Dim str_CodeorName As String = Request.QueryString("codeorname") & ""
            str_CodeorName = str_CodeorName.Replace("'", "")
            If IsNumeric(str_CodeorName) Then
                str_txtCode = str_CodeorName
                str_filter_code = " AND STU_NO LIKE '%" & str_CodeorName & "%'"
            Else
                str_txtName = str_CodeorName
                str_filter_name = " AND STU_NAME LIKE '%" & str_CodeorName & "%'"
            End If
            Dim str_ACD As String = Request.QueryString("acd") & ""

            If IsNumeric(str_ACD) Then
                str_filter_name = " AND STU_ACD_ID = '" & str_ACD & "'"
            End If
            '''''''''''''''''''''''

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            & " FROM VW_OSO_STUDENT_M " _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' "
            If STUD_ID <> "" Then
                str_Sql += " AND STU_SIBLING_ID <> STU_ID AND STU_SIBLING_ID=" & STUD_ID
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & _
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindTransport(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal PREV_ACD_ID As String)
        Try
            Dim str_search As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter_grade As String = String.Empty
            Dim str_filter_parname As String = String.Empty
            Dim str_filter_parmobile As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_txtGrade As String = String.Empty
            Dim str_txtPName As String = String.Empty
            Dim str_txtMobile As String = String.Empty 
            ''''''''''
            Dim str_CodeorName As String = Request.QueryString("codeorname") & ""
            str_CodeorName = str_CodeorName.Replace("'", "")
            If IsNumeric(str_CodeorName) Then
                str_txtCode = str_CodeorName
                str_filter_code = " AND STU_NO LIKE '%" & str_CodeorName & "%'"
            Else
                str_txtName = str_CodeorName
                str_filter_name = " AND STU_NAME LIKE '%" & str_CodeorName & "%'"
            End If
            If IsNumeric(ACD_ID) And IsNumeric(PREV_ACD_ID) Then
                str_filter_name = str_filter_name & " AND SSV_ACD_ID in ( " & ACD_ID & "," & PREV_ACD_ID & ")"
            ElseIf IsNumeric(ACD_ID) Then
                str_filter_name = str_filter_name & " AND SSV_ACD_ID = '" & ACD_ID & "'"
            End If

            '''''''''''''''''''''''

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            & " FROM VW_OSO_STUDENT_M INNER JOIN VW_STUDENT_SERVICES_D_MAX ON STU_ID=SSV_STU_ID" _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & _
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindEnquiryCompany(ByVal COMP_ID As Integer, ByVal BSU_ID As String)
        Try
            Dim str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
            str_Sql += " WHERE  STU_BSU_ID='" & BSU_ID & "'"
            If COMP_ID <> "-1" Then
                str_Sql += " AND  COMP_ID = " & COMP_ID
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentCompany(ByVal COMP_ID As Integer, ByVal BSU_ID As String)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String

            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            '--------Commented by Jacob on 18/12/2013-------------
            'Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " & _
            '" STU_BSU_ID, STU_NAME, STU_GENDER," & _
            '" GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            '" CASE dbo.VW_OSO_STUDENT_M .STU_PRIMARYCONTACT WHEN 'F' " & _
            '" THEN IsNULL(dbo.STUDENT_D.STS_F_COMP_ID, '') " & _
            '" WHEN 'M' THEN IsNULL(dbo.STUDENT_D.STS_M_COMP_ID, '') " & _
            '" WHEN 'G' THEN IsNULL(dbo.STUDENT_D.STS_G_COMP_ID, '') END AS COMP_ID, PARENT_MOBILE, PARENT_NAME " & _
            '" FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            '" dbo.STUDENT_D ON dbo.VW_OSO_STUDENT_M.STU_SIBLING_ID = dbo.STUDENT_D.STS_STU_ID) a" & _
            '" WHERE a.STU_BSU_ID='" & BSU_ID & "'"
            Dim str_Sql As String = " SELECT * FROM ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, STU_GENDER," & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " COMP_ID, PARENT_MOBILE, PARENT_NAME " & _
            " FROM TRANSPORT.[VW_STUDENT_DETAIL] AS STU )AS A" & _
            " WHERE A.STU_BSU_ID='" & BSU_ID & "'"
            If COMP_ID <> "-1" Then
                str_Sql += " AND A.COMP_ID = " & COMP_ID
            End If
            str_Sql += " AND A.STU_ID NOT IN (SELECT TCM_STU_ID FROM OASIS.dbo.TCM_M WITH(NOLOCK) WHERE ISNULL(TCM_Bcancelled, 0) = 0 AND TCM_BSU_ID = '" & BSU_ID & "' AND ISNULL(TCM_LASTATTDATE,GETDATE())<GETDATE())"
            
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID, BSU)
            Case Else
                Gridbind(BSU)
        End Select
    End Sub


    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID, BSU)
            Case Else
                Gridbind(BSU)
        End Select
        ' set_Menu_Img()
    End Sub


    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID, BSU)
            Case Else
                Gridbind(BSU)
        End Select
        ' set_Menu_Img()
    End Sub


    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID, BSU)
            Case Else
                Gridbind(BSU)
        End Select
        'set_Menu_Img()
    End Sub


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSTU_ID As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        lblSTU_ID = sender.Parent.FindControl("lblSTU_ID")
        Dim lblCode As New Label
        lblCode = sender.Parent.FindControl("lblCode")
        If (Not lblSTU_ID Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID, BSU)
            Case Else
                Gridbind(BSU)
        End Select
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID, BSU)
            Case Else
                Gridbind(BSU)
        End Select
    End Sub


    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID, BSU)
            Case Else
                Gridbind(BSU)
        End Select
    End Sub


End Class
