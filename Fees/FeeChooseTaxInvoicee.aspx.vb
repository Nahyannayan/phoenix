﻿Imports System.Data
Imports UtilityObj
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading.Tasks
Imports System.Threading

Partial Class Fees_FeeChooseTaxInvoicee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property sqlConnectionString() As String
        Get
            Return ViewState("sqlconn")
        End Get
        Set(ByVal value As String)
            ViewState("sqlconn") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Session("sUsr_name") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If


            CLEAR_ALL()
            If Not Request.QueryString("ID") Is Nothing Then
                ViewState("FSH_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                hfFSHID.Value = Request.QueryString("ID").Replace(" ", "+")
            End If
            If Not Request.QueryString("SBSU") Is Nothing Then
                ViewState("BSU_ID") = Encr_decrData.Decrypt(Request.QueryString("SBSU").Replace(" ", "+"))
            Else
                ViewState("BSU_ID") = Session("sBsuId")
            End If
            If Not Request.QueryString("TYPE") Is Nothing Then
                ViewState("INV_TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
            Else
                ViewState("INV_TYPE") = "TAXINV"
            End If
            If Not Request.QueryString("STUID") Is Nothing Then
                ViewState("STU_ID") = Encr_decrData.Decrypt(Request.QueryString("STUID").Replace(" ", "+"))
            End If
            If Not Request.QueryString("INVNO") Is Nothing Then
                ViewState("INVNO") = Encr_decrData.Decrypt(Request.QueryString("INVNO").Replace(" ", "+"))
            End If
            hfINVTYPE.Value = Encr_decrData.Encrypt(ViewState("INV_TYPE"))
            If ViewState("INV_TYPE") = "TAXINV" Then
                sqlConnectionString = ConnectionManger.GetOASIS_FEESConnectionString
            ElseIf ViewState("INV_TYPE") = "TRTAXINV" Then
                sqlConnectionString = ConnectionManger.GetOASISTRANSPORTConnectionString
            End If
            VALIDATE_INVOICE()
            LOAD_STUDENT_INFO()
            lblMessage1.Text = UtilityObj.getErrorMessage("644")
            lblMessage2.Text = UtilityObj.getErrorMessage("645")


            If Not Request.QueryString("RESET") Is Nothing AndAlso Request.QueryString("RESET").ToString = "TRUE" Then
                btnSave.Visible = False
                btnClear.Visible = True
            End If
        End If
    End Sub
    Private Sub CLEAR_ALL()
        h_print.Value = 0
        hfFSHID.Value = ""
        hfINVTYPE.Value = ""
        F_COMP_TAX_REGISTRATION_NO.Value = ""
        M_COMP_TAX_REGISTRATION_NO.Value = ""
        COMP_TAX_REGISTRATION_NO.Value = ""
        ViewState("FSH_ID") = "0"
        ViewState("STU_ID") = "0"
        ViewState("INVNO") = ""
        lblMessage1.Text = ""
        lblMessage2.Text = ""
        lblStuNo.Text = ""
        lblStuName.Text = ""
        lblInvNo.Text = ""
        ViewState("F_ADDRESS") = ""
        ViewState("M_ADDRESS") = ""
        ViewState("F_COMP_ADDRESS") = ""
        ViewState("M_COMP_ADDRESS") = ""
        ViewState("BSU_ID") = ""
        ViewState("INV_TYPE") = ""
        btnSave.Visible = True
        btnCancel.Text = "CANCEL"
    End Sub
    Private Sub VALIDATE_INVOICE()
        Dim Qry As New StringBuilder
        Qry.Append("SELECT CASE WHEN ISNULL(B.FSD_INVOICED_FOR,'')='' AND ISNULL(B.FSD_ADDRESS,'')='' THEN 0 ELSE 1 END AS bIsINVOICEE_SET ")
        Qry.Append("FROM FEES.FEESCHEDULE AS A WITH(NOLOCK) LEFT JOIN FEES.FEESCHEDULE_D AS B WITH(NOLOCK) ON A.FSH_ID=B.FSD_FSH_ID WHERE A.FSH_ID=" & ViewState("FSH_ID"))
        Dim IsInvoiceeSet As Boolean = SqlHelper.ExecuteScalar(sqlConnectionString, CommandType.Text, Qry.ToString)
        If IsInvoiceeSet = True AndAlso Request.QueryString("RESET") Is Nothing Then 'QS for reset the value -> negative it from ResetTaxInvoice.aspx 
            Response.Redirect("FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt(ViewState("INV_TYPE")) & "&ID=" & Request.QueryString("ID").Replace(" ", "+") & "&SBSU=" & Encr_decrData.Encrypt(ViewState("BSU_ID")) & "")
        End If
    End Sub
    Private Sub LOAD_STUDENT_INFO()
        Dim str_conn As String = sqlConnectionString
        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = ViewState("BSU_ID")
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = ViewState("STU_ID")
        pParms(2) = New SqlClient.SqlParameter("@INVOICE_FOR", SqlDbType.VarChar, 2)
        pParms(2).Value = rblInvoicee.SelectedValue

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GET_STUDENT_COMPANY_DETAILS", pParms)

        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            lblStuNo.Text = ds.Tables(0).Rows(0)("STU_NO").ToString
            lblStuName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString
            lblInvNo.Text = ViewState("INVNO").ToString

            ViewState("F_ADDRESS") = ds.Tables(0).Rows(0)("F_ADDRESS").ToString
            ViewState("M_ADDRESS") = ds.Tables(0).Rows(0)("M_ADDRESS").ToString
            ViewState("F_COMP_ADDRESS") = ds.Tables(0).Rows(0)("F_COMP_ADDRESS").ToString
            ViewState("M_COMP_ADDRESS") = ds.Tables(0).Rows(0)("M_COMP_ADDRESS").ToString
            F_COMP_TAX_REGISTRATION_NO.Value = ds.Tables(0).Rows(0)("F_COMP_TAX_REGISTRATION_NO").ToString
            M_COMP_TAX_REGISTRATION_NO.Value = ds.Tables(0).Rows(0)("M_COMP_TAX_REGISTRATION_NO").ToString
            rblInvoicee_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub rblInvoicee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblInvoicee.SelectedIndexChanged
        lblAddress.Text = ""
        lblTRNo.Text = ""
        Select Case rblInvoicee.SelectedValue
            Case "F"
                lblAddress.Text = ViewState("F_ADDRESS")
            Case "M"
                lblAddress.Text = ViewState("M_ADDRESS")
            Case "FC"
                lblAddress.Text = ViewState("F_COMP_ADDRESS")
                lblTRNo.Text = F_COMP_TAX_REGISTRATION_NO.Value
            Case "MC"
                lblAddress.Text = ViewState("M_COMP_ADDRESS")
                lblTRNo.Text = M_COMP_TAX_REGISTRATION_NO.Value
            Case Else
                lblAddress.Text = ""
                lblTRNo.Text = ""
        End Select

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim objConn As New SqlConnection(sqlConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As Integer = 1000
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FSH_ID", SqlDbType.BigInt)
            pParms(0).Value = ViewState("FSH_ID")
            pParms(1) = New SqlClient.SqlParameter("@FSD_INVOICED_FOR", SqlDbType.VarChar, 2)
            pParms(1).Value = rblInvoicee.SelectedValue
            pParms(2) = New SqlClient.SqlParameter("@FSD_ADDRESS", SqlDbType.VarChar, 1000)
            pParms(2).Value = lblAddress.Text.Trim
            pParms(3) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
            pParms(3).Value = Session("sUsr_name").ToString
            pParms(4) = New SqlClient.SqlParameter("@FSD_COMP_TAX_REGISTRATION_NO", SqlDbType.VarChar, 20)
            pParms(4).Value = lblTRNo.Text.Trim
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.UPDATE_TAX_INVOICEE_DETAILS", pParms)
            retval = pParms(5).Value
            If retval = 0 Then
                stTrans.Commit()
                SHOW_MESSAGE(True, getErrorMessage("646"))
                btnSave.Visible = False
                btnCancel.Text = "CLOSE"
                h_print.Value = 1
                trAnimation1.Visible = True
                Response.AddHeader("REFRESH", "2;URL=FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt(ViewState("INV_TYPE")) & "&ID=" & Request.QueryString("ID").Replace(" ", "+") & "&SBSU=" & Encr_decrData.Encrypt(ViewState("BSU_ID")) & "")
            Else
                stTrans.Rollback()
                SHOW_MESSAGE(False, getErrorMessage("1"))
            End If
        Catch
            stTrans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub SHOW_MESSAGE(ByVal bSuccess As Boolean, ByVal Message As String)
        If bSuccess Then
            lblError.CssClass = "validLabel"
        Else
            lblError.CssClass = "errLabel"
        End If
        lblError.Text = Message
    End Sub
    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click

        lblAddress.Text = ""
        lblTRNo.Text = ""
        Dim objConn As New SqlConnection(sqlConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As Integer = 1000
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FSH_ID", SqlDbType.BigInt)
            pParms(0).Value = ViewState("FSH_ID")

            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = ViewState("BSU_ID")

            pParms(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 50)
            pParms(2).Value = Session("sUsr_name").ToString

            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.RESET_TAX_INVOICEE_DETAILS", pParms)
            retval = pParms(3).Value
            If retval = 0 Then
                stTrans.Commit()
                SHOW_MESSAGE(True, getErrorMessage("646"))
                btnClear.Visible = False

            Else
                stTrans.Rollback()
                SHOW_MESSAGE(False, getErrorMessage("1"))
            End If
        Catch ex As Exception
            Dim ERROR2 As String = ex.Message
            stTrans.Rollback()
        End Try
    End Sub
End Class
