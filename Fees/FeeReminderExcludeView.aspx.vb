

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeReminderExcludeView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEConcessionDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEConcessionDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEConcessionDet.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REMINDEREXCLUDE Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                hlAddNew.NavigateUrl = "FeeReminderExclude.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                'ddlBSUnit.DataBind()
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim ds As New DataSet
            Dim str_Filter, lstrOpr, str_Sql, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            str_Filter = ""
            If gvFEEConcessionDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)

                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_FIRSTNAME", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFromDate")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FROM DATE", lstrCondn3)

                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtToDate")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "TO DATE", lstrCondn4)
            End If

            str_Sql = "SELECT  FRE.FRE_ID,   FRE.FRE_STU_ID, FRE.FRE_BSU_ID, FRE.FRE_STU_ACD_ID, " _
                      & " FRE.FRE_FROMDT, FRE.FRE_TODT, FRE.FRE_bDELETE,FRE.FRE_LOGDATE, " _
                      & " STU.STU_NO, ISNULL(STU.STU_FIRSTNAME,'') + ' ' + ISNULL(STU.STU_MIDNAME,'') + ' ' + ISNULL(STU.STU_LASTNAME,'') STU_NAME " _
                      & " FROM FEES.FEE_REMINDER_EXCLUDE AS FRE INNER JOIN " _
                      & " OASIS..STUDENT_M AS STU WITH(NOLOCK) ON FRE.FRE_STU_ID = STU.STU_ID AND " _
                      & " FRE.FRE_BSU_ID = STU.STU_BSU_ID " _
                      & " WHERE     FRE.FRE_BSU_ID = '" & Session("sBsuid") & "' " & str_Filter

            Dim str_orderby As String = " ORDER BY ISNULL(FRE.FRE_LOGDATE,FRE.FRE_FROMDT) desc"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_orderby)
            gvFEEConcessionDet.DataSource = ds.Tables(0)
            'gvFEEConcessionDet.DataBind()
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEConcessionDet.DataBind()
                Dim columnCount As Integer = gvFEEConcessionDet.Rows(0).Cells.Count
                gvFEEConcessionDet.Rows(0).Cells.Clear()
                gvFEEConcessionDet.Rows(0).Cells.Add(New TableCell)
                gvFEEConcessionDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEConcessionDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEConcessionDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEConcessionDet.DataBind()
            End If

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFromDate")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtToDate")
            txtSearch.Text = lstrCondn4

            'txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtDeleted")
            'txtSearch.Text = lstrCondn5

            'txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
            'txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvFEEConcessionDet_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvFEEConcessionDet.RowCommand
        Try
            If e.CommandName = "edit" Then
                Dim lblFREID As New Label
                Dim lblStudID As New Label
                Dim lblSTU_NO As New Label


                Dim lblFROMDATE As Label
                Dim lblTODATE As Label


                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvFEEConcessionDet.Rows(index), GridViewRow)
                lblFREID = selectedRow.FindControl("lblFREID")
                lblStudID = selectedRow.FindControl("lblStudID")
                lblSTU_NO = selectedRow.FindControl("lblSTU_NO")
                lblFROMDATE = selectedRow.FindControl("lblFROMDATE")
                lblTODATE = selectedRow.FindControl("lblTODATE")

                divAge.Visible = True
                pnl_txtFromDate.Text = lblFROMDATE.Text
                pnl_txtToDate.Text = lblTODATE.Text
                ViewState("FRE_ID") = lblFREID.Text
                ViewState("STU_ID") = lblStudID.Text
                ViewState("FROMDATE") = lblFROMDATE.Text
                ViewState("TODATE") = lblTODATE.Text

            End If

            'If e.CommandName = "delete" Then

            'End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Public Function checkdatevalues(ByRef f_date As String, ByRef t_date As String) As Boolean
        Dim c_date As String
        Dim ps_date As String

        c_date = f_date + " 00:00" 'DateTime.Now.ToString("dd/MM/yyyy HH:mm")
        ps_date = t_date + " 00:00"

        Dim pass_date As DateTime = DateTime.ParseExact(ps_date, "dd/MMM/yyyy HH:mm", Nothing)
        Dim cur_date As DateTime = DateTime.ParseExact(c_date, "dd/MMM/yyyy HH:mm", Nothing)

        If pass_date > cur_date Then
            Return True

        Else
            Return False
        End If

    End Function
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEConcessionDet.PageIndexChanging
        gvFEEConcessionDet.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub lnkDelete(ByVal sender As Object, ByVal e As CommandEventArgs)


        'int iStID=int32.Parse(e.CommandArgument.ToString());
        Dim index As Integer = Convert.ToInt32(e.CommandArgument.ToString())
        Dim selectedRow As GridViewRow = DirectCast(gvFEEConcessionDet.Rows(index), GridViewRow)


        Dim btn As LinkButton = selectedRow.FindControl("deletebtn")
        btn.Attributes.Add("onclick", "return confirm('delete this record?');")
    End Sub
    Protected Sub gvFEEConcessionDet_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFEEConcessionDet.RowDeleting
        Try
            Dim selectedRow As GridViewRow = DirectCast(gvFEEConcessionDet.Rows(e.RowIndex), GridViewRow)
            Dim lblStudID As New Label
            Dim lblSTU_NO As New Label


            Dim lblFROMDATE As Label
            Dim lblTODATE As Label



            lblStudID = selectedRow.FindControl("lblStudID")
            lblSTU_NO = selectedRow.FindControl("lblSTU_NO")
            lblFROMDATE = selectedRow.FindControl("lblFROMDATE")
            lblTODATE = selectedRow.FindControl("lblTODATE")

            ViewState("STU_ID") = lblStudID.Text
            ViewState("FROMDATE") = lblFROMDATE.Text
            ViewState("TODATE") = lblTODATE.Text


            Dim STU_ID As String = String.Empty
            Dim Status As Integer
            Dim param(14) As SqlClient.SqlParameter
            Dim transaction As SqlTransaction


            Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim dtFromdate As DateTime = CDate(pnl_txtFromDate.Text)
                    Dim dtToDate As DateTime = CDate(pnl_txtToDate.Text)
                    param(0) = New SqlParameter("@OPTION", 3)
                    param(1) = New SqlParameter("@FRE_ID", ViewState("FRE_ID"))
                    param(2) = New SqlParameter("@STU_ID", ViewState("STU_ID"))
                    param(3) = New SqlParameter("@FROM_DATE", ViewState("FROMDATE"))
                    param(4) = New SqlClient.SqlParameter("@TO_DATE", ViewState("TODATE"))
                    param(5) = New SqlClient.SqlParameter("@NEW_FROM_DATE", ViewState("FROMDATE"))
                    param(6) = New SqlClient.SqlParameter("@NEW_TO_DATE", ViewState("TODATE"))
                    param(7) = New SqlClient.SqlParameter("@APPL_USER", Session("sUsr_name"))
                    param(8) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                    param(8).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[FEES].[RUD_FEE_REMINDER_EXCLUDE]", param)
                    Status = param(8).Value
                Catch ex As Exception
                    'lblError.Text = "Record could not be updated"
                    usrMessageBar2.ShowNotification("Record could not be updated ", UserControls_usrMessageBar.WarningType.Danger)
                Finally
                    If Status <> 0 Then
                        UtilityObj.Errorlog("Record could not be updated ")
                        transaction.Rollback()
                    Else
                        'lblError.Text = "Record Updated Successfully"
                        usrMessageBar2.ShowNotification("Record Deleted Successfully ", UserControls_usrMessageBar.WarningType.Success)
                        transaction.Commit()
                    End If
                End Try

            End Using
            GridBind()
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvFEEConcessionDet_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvFEEConcessionDet.RowEditing
        'Try

        '    divAge.Visible = True


        'Catch ex As Exception
        '    'lblError.Text = "Request could not be processed "
        '    usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try

    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False

    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divAge.Visible = False

    End Sub

    Sub update_reminder_date()


        Dim STU_ID As String = String.Empty
        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction


        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim dtFromdate As DateTime = CDate(pnl_txtFromDate.Text)
                Dim dtToDate As DateTime = CDate(pnl_txtToDate.Text)

                param(0) = New SqlParameter("@OPTION", 2)
                param(1) = New SqlParameter("@FRE_ID", ViewState("FRE_ID"))
                param(2) = New SqlParameter("@STU_ID", ViewState("STU_ID"))
                param(3) = New SqlParameter("@FROM_DATE", ViewState("FROMDATE"))
                param(4) = New SqlClient.SqlParameter("@TO_DATE", ViewState("TODATE"))
                param(5) = New SqlClient.SqlParameter("@NEW_FROM_DATE", dtFromdate)
                param(6) = New SqlClient.SqlParameter("@NEW_TO_DATE", dtToDate)
                param(7) = New SqlClient.SqlParameter("@APPL_USER", Session("sUsr_name"))
                param(8) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(8).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[FEES].[RUD_FEE_REMINDER_EXCLUDE]", param)
                Status = param(8).Value
            Catch ex As Exception
                'lblError.Text = "Record could not be updated"
                usrMessageBar2.ShowNotification("Record could not be updated ", UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If Status <> 0 Then
                    UtilityObj.Errorlog("Record could not be updated ")
                    transaction.Rollback()
                Else
                    'lblError.Text = "Record Updated Successfully"
                    usrMessageBar2.ShowNotification("Record Updated Successfully ", UserControls_usrMessageBar.WarningType.Success)
                    transaction.Commit()
                End If
            End Try

        End Using

    End Sub





    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim k As Boolean = checkdatevalues(pnl_txtFromDate.Text, pnl_txtToDate.Text)
        If k = True Then
            update_reminder_date()
            divAge.Visible = False
            GridBind()
        Else
            lblUerror.Text = "Selected To Date should be greater than From Date"
            'popuperrmsg.Text = "Selected Date and Time should be greater than Current Date and Time"
            usrMessageBar2.ShowNotification("Selected To Date should be greater than From Date", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
End Class
