﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AmbassadorProgramEntryList.aspx.vb" Inherits="Fees_AmbassadorProgramEntryList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  <asp:Literal ID="ltHeader" runat="server" Text="Ambassador Program Entry List (Registrar, Finance, Principal)"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" colspan="4" >



                            <table id="Table1" border="0" width="100%">
                                <tr >
                                    <td align="left" width="50%">
                                       </td>

                                    <td align="right" width="50%" >
                                        <asp:RadioButton ID="rbPending" runat="server" Checked="True" GroupName="type" Text="Pending" AutoPostBack="True" />&nbsp; &nbsp;
                            <asp:RadioButton ID="rbApproved" runat="server" GroupName="type" Text="Approved" AutoPostBack="True" />&nbsp; &nbsp;
                                <asp:RadioButton ID="rbRejected" runat="server" GroupName="type" Text="Rejected" AutoPostBack="True" />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;                            
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4" >
                            <table id="tbl_test" runat="server" align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                <tr >
                                    <td align="left" >
                                        <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr >
                                    <td align="left" >
                                        <asp:GridView ID="gvAmbassadorList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"  />
                                            <Columns>
                                                <%--<asp:BoundField DataField="APH_ID" HeaderText="APH_ID"></asp:BoundField>--%>

                                                <asp:TemplateField HeaderText="ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPH_ID" runat="server" Text='<%# Bind("APEH_ID")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Referal Student ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("REFERAL_ID")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Referal No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("REFERAL_NO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Referal Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("REFERAL_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Referal Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRD_DISPLAY" runat="server" Text='<%# Bind("REFERAL_GRADE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Referal DOJ">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_DOJ" runat="server" Text='<%# Bind("REFERAL_DOJ", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Referer No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblREFERER_NO" runat="server" Text='<%# Bind("REFERER_NO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Referer Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblREFERER_NAME" runat="server" Text='<%# Bind("REFERER_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Source">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPH_SOURCE" runat="server" Text='<%# Bind("APEH_SOURCE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>





                                                <asp:TemplateField HeaderText="USR_EMP_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUSR_EMP_ID" runat="server" Text='<%# Bind("USR_EMP_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="LEVEL" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLEVEL" runat="server" Text='<%# Bind("LEVEL")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

</asp:Content>
