<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeOtherCharge.aspx.vb" Inherits="Fees_FeeOtherCharge" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">

    <script type="text/javascript" language="javascript">

        Sys.Application.add_load(
function CheckForPrint() {

    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        //showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        Popup('../Reports/ASPX Report/RptViewerModal.aspx')
    }
}
    );

function getFile() {

    var filepath = document.getElementById('<%=uploadFile.ClientID %>').value;
    document.getElementById('<%=HidUpload.ClientID %>').value = filepath;
}

<%--function getStudent() {

    //        var Amt = document.getElementById('<%=txtAmountAdd.ClientID %>' ).value;
    //        if (Amt == "" || Amt == "0")
    //        {
    //        alert('Invalid Amount..!');
    //        return;
    //        }

    var sFeatures, url;
    sFeatures = "dialogWidth: 800px; ";
    sFeatures += "dialogHeight: 600px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var BsuId = '<%= Session("sBSuid") %>'
    var TYPE = 'S';
    var selType = 's';
    var selACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
    var bulkCheck = document.getElementById('<%=ChkBulk.ClientID %>');

    if (bulkCheck.checked == true)
        url = "BulkOtherfeeCharge.aspx?TYPE=bsu=" + BsuId;
    else
        //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
        url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + BsuId + '&ACD_ID=' + selACD_ID;

    result = window.showModalDialog(url, "", sFeatures);

    if (result == '' || result == undefined) {

        return false;
    }
    if (result != '' && result != undefined) {
        document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students Selected';
        document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
        return true;
    }

}--%>

        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
            var amt;
            amt = parseFloat(roundOff)
            if (isNaN(amt))
                amt = 2;
            return amt;
        }

        function UpdateSum(FRM) {

            var sum = 0.0;
            var dsum = 0.0;
            var NumStu = 0
            //for (i = 0; i < document.forms[0].elements.length; i++) {
            //    if (document.forms[0].elements[i].name.search(/txtDAmount/) > 0) {

            //        CheckAmount(document.forms[0].elements[i])
            //        dsum = parseFloat(document.forms[0].elements[i].value);
            //        if (isNaN(dsum))
            //            document.forms[0].elements[i].value = 0;
            //        if (document.forms[0].elements[i].value != 0)
            //            NumStu = Number(NumStu) + 1;

            //        sum += parseFloat(document.forms[0].elements[i].value);
            //    }
            //}

            //document.getElementById('<%=txtTotalAmount.ClientID %>').value = sum.toFixed(getRoundOff());
            NumStu = parseFloat(document.getElementById('<%=txtStuCount.ClientID %>').value);
            sum = parseFloat(document.getElementById('<%=txtTotalAmount.ClientID %>').value);
            //document.getElementById('<%=txtStuCount.ClientID %>').innerText = NumStu;
            if (FRM == 'S')
                return confirm('Total Student : ' + NumStu + '.... Total Amount : ' + sum.toFixed(getRoundOff()) + '.. You want to Continue..?');


        }


        function autoFill() {
            var rowLength = 0

            var Amount = Number(document.getElementById('<%=txtAmountAdd.ClientID %>').value);
            if (isNaN(Amount))
                Amount = 0;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/txtDAmount/) > 0)
                    rowLength++;
            }

            if (rowLength == 0)
                return;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/txtDAmount/) > 0)
                    document.forms[0].elements[i].value = Amount.toFixed(getRoundOff());
            }
            UpdateSum('C')
        }

        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(getRoundOff());
            return true;
        }





    </script>

    <script>
        function getStudent() {
            var url;

            var BsuId = '<%= Session("sBSuid") %>'
            var TYPE = 'S';
            var selType = 's';
            var selACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var bulkCheck = document.getElementById('<%=ChkBulk.ClientID %>');

            if (bulkCheck.checked == true)
                url = "BulkOtherfeeCharge.aspx?TYPE=bsu=" + BsuId;
            else
                //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
                url = "ShowStudentMulti.aspx?MainMnu_code=y7oTXCwUH+0=&TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + BsuId + '&ACD_ID=' + selACD_ID;

            var oWnd = radopen(url, "pop_student");



        }



        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=txtStudName.ClientID %>').value = arg.NameandCode;
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;
                __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Other Fee Charge
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Panel runat="server" ID="pnlEditable">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4" align="left">
                                <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"
                                    CssClass="error"></asp:Label>
                                <asp:LinkButton ID="LinkContinue" runat="server" CssClass="error" OnClick="LinkContinue_Click"
                                    Visible="False">Continue..?</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                    <table class="BlueTable" align="center" width="100%">
                        <tr>
                            <td align="left" width="10%"><span class="field-label">Academic year</span>
                            </td>
                            <td align="left" colspan="1" width="40%">
                                <table style="width: 100%; padding: 0px; border-spacing: 0px;">
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:CheckBox ID="chkNextAcd" runat="server" AutoPostBack="true" Text="Next Academic Year" /></td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right" colspan="1" width="20%"><span class="field-label">Date</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtFrom" runat="server" TabIndex="2"></asp:TextBox>
                                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Charge Schedule</span></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlSchedule" runat="server" AutoPostBack="True">
                                    <asp:ListItem Selected="True" Value="1000"><--Select--></asp:ListItem>
                                    <asp:ListItem Value="4">Annual</asp:ListItem>
                                    <asp:ListItem Value="2">Termly</asp:ListItem>
                                    <asp:ListItem Value="0">Monthly</asp:ListItem>
                                </asp:DropDownList>
                                <br>
                                <span style="font-style: italic;">(Only for reporting purpose)</span></td>

                            <td align="left"><span class="field-label">Charge Period</span></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlScheduleItem" runat="server" AutoPostBack="True"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trVATInfo" runat="server">

                            <td align="center" colspan="3"></td>
                            <td align="left">
                                <asp:Label ID="lblalert" runat="server"
                                    CssClass="alert alert-info"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Fee Type</span>
                            </td>
                            <td align="left" colspan="1">
                                <asp:DropDownList ID="ddFeetype" runat="server" SkinID="DropDownListNormal" TabIndex="9" AutoPostBack="true">
                                </asp:DropDownList><br />
                                <asp:CheckBox ID="ChkBulk" runat="server" Text="Bulk Selection" CssClass="field-label" />
                            </td>
                            <td align="right" colspan="1">
                                <span class="field-label">Amount</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAmountAdd" runat="server" Style="text-align: right" AutoCompleteType="Disabled"
                                    TabIndex="10"></asp:TextBox><asp:LinkButton ID="linkAutofill"
                                        runat="server">Auto Fill</asp:LinkButton>
                                <asp:LinkButton ID="linkCalculate" runat="server" Visible="false">Calculate</asp:LinkButton>

                            </td>
                            <%--OnClientClick="autoFill();return false;"OnClientClick="UpdateSum('C');return false;"--%>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Student</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtStudName" runat="server" AutoPostBack="True" OnTextChanged="txtStudName_TextChanged"></asp:TextBox><asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif"
                                    OnClientClick="getStudent();return false;" TabIndex="7" OnClick="imgStudent_Click"></asp:ImageButton></td>
                            <td><span class="field-label">Total Students </span>
                                <asp:TextBox ID="txtStuCount" runat="server" AutoCompleteType="Disabled" Enabled="false"
                                    Font-Bold="True" Style="text-align: center; width: 100px; min-width: 10%!important;" TabIndex="10"></asp:TextBox></td>
                            <td><span class="field-label">Total Amount </span>
                                <asp:TextBox ID="txtTotalAmount" runat="server" Style="text-align: right; width: 200px; min-width: 10%!important;" AutoCompleteType="Disabled"
                                    TabIndex="10" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>


                        <tr id="trVAT" runat="server">
                            <td align="left"><span class="field-label">Tax Code</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlTAX" runat="server" SkinID="DropDownListNormal" TabIndex="9" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:RadioButtonList ID="rblTaxCalculation" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="E"><span class="field-label">Excl.Tax</span></asp:ListItem>
                                    <asp:ListItem Value="I"><span class="field-label">Incl.Tax</span></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">Narration</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText"
                                    TabIndex="160"></asp:TextBox>

                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td align="left"><span class="field-label">File Name</span>
                            </td>
                            <td align="left" colspan="3">
                                <asp:FileUpload ID="uploadFile" runat="server" Width="532px"></asp:FileUpload>
                                <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload File" CausesValidation="False"
                                    TabIndex="30" OnClientClick="getFile()" />
                            </td>
                        </tr>
                    </table>
                    <table class="BlueTable_simple" align="center" width="100%">
                        <tr>
                            <td align="left" class="title-bg">Student Details
                            </td>
                        </tr>
                        <tr>
                            <td align="center">

                                <asp:GridView ID="gvStudentDetails" runat="server" Width="100%" EmptyDataText="No Details Added"
                                    CssClass="table table-row table-bordered" OnSelectedIndexChanging="gvStudentDetails_SelectedIndexChanging"
                                    AutoGenerateColumns="False" OnRowDataBound="gvStudentDetails_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="STU_ID" HeaderText="StuID"></asp:BoundField>
                                        <asp:BoundField DataField="STU_NO" HeaderText="Student ID">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="STU_NAME" HeaderText="Student Name">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDAmount" onFocus="this.select();" onblur="return CheckAmount(this);"
                                                    runat="server" Style="direction: rtl" Text='<%# Bind("FOD_AMOUNT") %>' AutoPostBack="True" OnTextChanged="txtDAmount_TextChanged"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                                                    ValidChars="." TargetControlID="txtDAmount" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tax Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTAXAmount" runat="server" Text='<%# Bind("FOD_TAX_AMOUNT", "{0:F}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Net Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNETTotal" runat="server" Text='<%# Bind("FOD_NET_AMOUNT", "{0:F}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:CommandField SelectText="Delete" ShowSelectButton="True" HeaderText="Exclude">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:CommandField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblError2" runat="server" SkinID="LabelError" EnableViewState="False"
                                    CssClass="error"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table style="width: 100%;">
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False"
                                OnClick="btnEdit_Click" />
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165"
                                OnClientClick="return UpdateSum('S');" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False"
                                OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_SelectId" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_StuidAll" runat="server" />
                <asp:HiddenField ID="h_Posted" runat="server" Value="0" />
                <asp:HiddenField ID="HidUpload" runat="server" />

            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
