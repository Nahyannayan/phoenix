Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeRemainderSMSsend
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEConcessionDet.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300400" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEConcessionDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEConcessionDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter, lstrOpr, str_Sql, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            str_Filter = ""
            If gvFEEConcessionDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)

                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCM_DESCR", lstrCondn3)

                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_DT", lstrCondn4)

                '   -- 5  city
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
                lstrCondn6 = txtSearch.Text.Trim
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_REMARKS", lstrCondn6)
            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            Dim str_cond As String = String.Empty
            

            str_Sql = "SELECT     ACM.ACY_DESCR, FEH.FRH_DT, FEH.FRH_ASONDATE, FEH.FRH_ID, REM.RMD_DESCR AS FRH_Level, " & _
            " FEH.FRH_REMARKS, GRD.GRD_DISPLAY " & _
            " FROM  SECTION_M AS SEC INNER JOIN " & _
            " FEES.FEE_REMINDER_D AS FED ON SEC.SCT_ID = FED.FRD_SCT_ID INNER JOIN " & _
            " GRADE_M AS GRD ON SEC.SCT_GRD_ID = GRD.GRD_ID INNER JOIN " & _
            " OASIS.dbo.ACADEMICYEAR_D AS ADC INNER JOIN " & _
            " OASIS.dbo.ACADEMICYEAR_M AS ACM ON ADC.ACD_ACY_ID = ACM.ACY_ID INNER JOIN  " & _
            " FEES.FEE_REMINDER_H AS FEH ON ADC.ACD_ID = FEH.FRH_ACD_ID INNER JOIN  " & _
            " REMINDERTYPE_M AS REM ON REM.RMD_ID = FEH.FRH_Level ON FED.FRD_FRH_ID = FEH.FRH_ID  " & _
            " AND FED.FRD_ACD_ID = FEH.FRH_ACD_ID  " & _
            " WHERE FRH_BSU_ID='" & Session("sBsuid") & "' AND isnull(FEH.FRH_bSENDSMS,0) = 0 AND ISNULL(FEH.FRH_bDeleted,0) = 0 " & _
            " GROUP BY ACM.ACY_DESCR, FEH.FRH_DT, FEH.FRH_ASONDATE, FEH.FRH_ID, REM.RMD_DESCR, FEH.FRH_REMARKS, GRD.GRD_DISPLAY " & str_Filter
            Dim str_orderby As String = " ORDER BY FRH_ID DESC,FRH_ASONDATE DESC"

         



            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql & str_orderby)
            gvFEEConcessionDet.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEConcessionDet.DataBind()
                Dim columnCount As Integer = gvFEEConcessionDet.Rows(0).Cells.Count
                gvFEEConcessionDet.Rows(0).Cells.Clear()
                gvFEEConcessionDet.Rows(0).Cells.Add(New TableCell)
                gvFEEConcessionDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEConcessionDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEConcessionDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEConcessionDet.DataBind()
            End If

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn4

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEConcessionDet.PageIndexChanging
        gvFEEConcessionDet.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEConcessionDet.RowDataBound
        Try
            Dim ChkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("ChkSelect"), HtmlInputCheckBox)
            

            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And ChkSelect IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "FEEReminderViewDetails.aspx?FRH_ID=" & Encr_decrData.Encrypt(ChkSelect.Value) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            doInsert()
            GridBind()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub doInsert()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim FRH_ID As Integer = 0
            For Each grow As GridViewRow In gvFEEConcessionDet.Rows
                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                If ChkSelect IsNot Nothing And ChkSelect.Checked Then
                    FRH_ID = ChkSelect.Value
                    Dim pParms(4) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.Int)
                    pParms(0).Value = ChkSelect.Value
                    pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
                    pParms(1).Value = Session("sBsuid")
                    pParms(2) = New SqlClient.SqlParameter("@USER_NAME", SqlDbType.VarChar)
                    pParms(2).Value = Session("sUsr_name")
                    pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParms(3).Direction = ParameterDirection.ReturnValue
                    Dim retval As Integer
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[SendFeeRemainder_SMS]", pParms)
                    If pParms(3).Value <> "0" Then
                        'lblError.Text = getErrorMessage(pParms(3).Value)
                        usrMessageBar.ShowNotification(getErrorMessage(pParms(3).Value), UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        objConn.Close()
                        Exit Sub
                    End If
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", FRH_ID, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            objConn.Close()
            'Exit Sub
        End Try
        stTrans.Commit()
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
        'lblError.Text = getErrorMessage("0")
        usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
    End Sub
End Class