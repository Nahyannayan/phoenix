﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class Fees_feeTransportSetupPaymentMandateView
    Inherits System.Web.UI.Page


    Protected Sub lnkadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkadd.Click
        Response.Redirect("feeTransportSetupPaymentMandate.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Request.QueryString("datamode"))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBank()
            BindBranch()
            BindGrid()
        End If
    End Sub


    Public Sub BindBank()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString()
        Dim query = " select BNK_ID,BNK_DESCRIPTION from oasis..bank_M where BANK_ONLINE is not null "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        dropbank.DataSource = ds
        dropbank.DataTextField = "BNK_DESCRIPTION"
        dropbank.DataValueField = "BNK_ID"
        dropbank.DataBind()
        Dim item As New ListItem
        item.Text = "Select a Bank"
        item.Value = "-1"
        dropbank.Items.Insert(0, item)


    End Sub

    Public Sub BindBranch()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString()
        Dim query = " select EMR_CODE,EMR_DESCR from OASIS.dbo.EMIRATE_M ORDER BY EMR_DESCR  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        dropbranch.DataSource = ds
        dropbranch.DataTextField = "EMR_DESCR"
        dropbranch.DataValueField = "EMR_CODE"
        dropbranch.DataBind()
        Dim item As New ListItem
        item.Text = "Select a Branch"
        item.Value = "-1"
        dropbranch.Items.Insert(0, item)

    End Sub

    Public Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString()
            Dim pParms(13) As SqlClient.SqlParameter

            If txtaccount.Text <> "" Then
                pParms(0) = New SqlClient.SqlParameter("@SPM_ACCOUNT", txtaccount.Text.Trim())
            End If

            If txtbac.Text <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@SPM_BANK_ACT_ID", txtbac.Text.Trim())
            End If

            If txtpayeename.Text <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@SPM_PAYEE_NAME", txtpayeename.Text.Trim())
            End If

            If txtstartdate.Text <> "" Then
                pParms(3) = New SqlClient.SqlParameter("@SPM_STARTDT", txtstartdate.Text.Trim())
            End If
            If txtstuname.Text <> "" Then
                pParms(4) = New SqlClient.SqlParameter("@STU_FIRSTNAME", txtstuname.Text.Trim())
            End If
            If dropbank.SelectedIndex > 0 Then
                pParms(5) = New SqlClient.SqlParameter("@SPM_BANK", dropbank.SelectedValue) 'h_Bank2.Value()
            End If
            If dropbranch.SelectedIndex > 0 Then
                pParms(6) = New SqlClient.SqlParameter("@SPM_BRANCH", dropbranch.SelectedValue)
            End If

            pParms(7) = New SqlClient.SqlParameter("@SPM_BSU_ID", Session("sbsuid"))

            pParms(8) = New SqlClient.SqlParameter("@SPM_STU_BSU_ID", ddlBusinessunit0.SelectedValue)

            If txtstuno.Text <> "" Then
                pParms(9) = New SqlClient.SqlParameter("@STU_NO", txtstuno.Text.Trim())
            End If



            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRAN_STU_PAYMENT_MANDATE", pParms)

            Griddata.DataSource = ds
            Griddata.DataBind()

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        BindGrid()
    End Sub

    Protected Sub Griddata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Griddata.PageIndexChanging
        Griddata.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


    Protected Sub Griddata_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Griddata.RowCommand

        If e.CommandName = "view" Then

            Response.Redirect("feeTransportSetupPaymentMandate.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Request.QueryString("datamode") & "&Rid=" & e.CommandArgument)

        End If



    End Sub
End Class
