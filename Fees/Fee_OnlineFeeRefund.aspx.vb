﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic

Partial Class Fees_Fee_OnlineFeeRefund
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("sBsuid")
        If Session("sUsr_name") Is Nothing Then
            Response.Redirect("~\Login.aspx")
        End If
        If Page.IsPostBack = False Then

            Page.Title = "::GEMS Education | Online Fee Refund::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
            'set_bankaccount()
            txtRemarks.Text = "ONLINE - "
            ViewState("datamode") = "add"
            Dim USR_NAME As String = Session("sUsr_name")
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")


            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                gridbind_fee_details()
            End If

        End If
    End Sub
    Protected Sub uscStudentPicker_StudentCleared(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentCleared
        ClearDetails()
    End Sub
    Sub ClearDetails()
        grdFee.DataSource = Nothing
        grdFee.DataBind()
        tr_list.Visible = False

        grdOnline.DataSource = Nothing
        grdOnline.DataBind()
        tbl_online.Visible = False

        grdRefund.DataSource = Nothing
        grdRefund.DataBind()
        tbl_refund.Visible = False
        txtRemarks.Text = ""
        txt_Total_Refund_Amt.Text = "0.00"

    End Sub
    Sub gridbind_fee_details()
        Try
            Dim str_Filter As String = ""
            Dim lstrCondn1 As String = ""

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            If grdFee.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)

                txtSearch = grdFee.HeaderRow.FindControl("txtReceiptNo")
                lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
                'If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "[Receipt No]", lstrCondn1)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & "%" & lstrCondn1 & "%"
            Else

                str_Filter = "%%"
            End If

            Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OPTION", 1)
            param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            param(2) = New SqlClient.SqlParameter("@STU_ID", uscStudentPicker.STU_ID.ToString)
            param(3) = New SqlClient.SqlParameter("@RECEIPT_FILTER", str_Filter)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "FEES.FEE_COLLECTION_REPORT", param)


            grdFee.DataSource = ds.Tables(0)
            grdFee.DataBind()

            If grdFee.Rows.Count > 0 Then

                txtSearch = grdFee.HeaderRow.FindControl("txtReceiptNo")
                txtSearch.Text = lstrCondn1
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                tr_list.Visible = True
            Else
                tr_list.Visible = False
            End If

        Catch ex As Exception


        End Try
    End Sub
    Protected Sub uscStudentPicker_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentNoChanged
        Try
            ClearDetails()
            gridbind_fee_details()
            uscStudentPicker.STU_MODULE = "FEE"

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub grdRefund_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRefund.RowDataBound
        Try
            Dim lb1 As Label
            Dim lb2 As TextBox
            lb1 = TryCast(e.Row.FindControl("lbl_RefundableAmount"), Label)
            lb2 = TryCast(e.Row.FindControl("lbl_RefundAmount"), TextBox)
            Dim amt1 As Decimal = CDbl(IIf(lb1.Text = "", 0, lb1.Text))
            If amt1 <= 0 Then
                lb2.Enabled = False

            End If
            'Dim cmdCol As Integer = grdFee.Columns.Count - 1
            'Dim hlview As New LinkButton
            'hlview = TryCast(e.Row.FindControl("hlView"), LinkButton)
            'If hlview IsNot Nothing And lbl_ReceiptNo IsNot Nothing Then
            '    hlview.Attributes.Add("onclick", "GetOnlineGrid(" & lbl_ReceiptNo.Text & ",'F730369')")
            '    hlview.Text = "Select"
            'End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub grdFee_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdFee.RowCommand
        Try

            Dim lbl_ReceiptNo As Label
            Dim lbl_StudentType As Label
            Dim lbl_FCL_FCL_ID As Label
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(grdFee.Rows(index), GridViewRow)
            lbl_ReceiptNo = selectedRow.FindControl("lbl_ReceiptNo")
            lbl_StudentType = selectedRow.FindControl("lbl_StudentType")
            lbl_FCL_FCL_ID = selectedRow.FindControl("lbl_FCL_FCL_ID")
            ViewState("StudentType") = lbl_StudentType.Text
            ViewState("lbl_FCL_FCL_ID") = lbl_FCL_FCL_ID.Text

            If e.CommandName = "Select" Then
                'divAge.Visible = True
                OnlineGridbind(lbl_ReceiptNo.Text)

                Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
                Dim param(3) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@OPTION", 1)
                param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                param(2) = New SqlClient.SqlParameter("@RECEIPT_NO", lbl_ReceiptNo.Text)

                Dim dso As New DataSet
                dso = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[FEES].[GET_FEE_REFUND_BANK_DETAILS]", param)
                If dso.Tables.Count > 0 Then
                    If dso.Tables(0).Rows.Count > 0 Then
                        txtBankCode.Text = dso.Tables(0).Rows(0)("ACCOUNT_NO")
                        txtBankDescr.Text = dso.Tables(0).Rows(0)("ACCOUNT_NAME")
                        'txtBankCharge.Text = dso.Tables(0).Rows(0)("ACCOUNT_CHARGE")
                    End If
                End If

            End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar2.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub grdFee_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdFee.PageIndexChanging
        grdFee.PageIndex = e.NewPageIndex
        gridbind_fee_details()
    End Sub
    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divAge.Visible = False
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divAge.Visible = False
    End Sub

    Protected Sub grdOnline_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdOnline.PageIndexChanging
        grdOnline.PageIndex = e.NewPageIndex
        If Not ViewState("REC_NO").ToString Is Nothing And ViewState("REC_NO").ToString <> "" Then
            OnlineGridbind(ViewState("REC_NO").ToString)
        End If
    End Sub

    Sub OnlineGridbind(ByVal RECEIPT_NO As String)
        Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OPTION", 1)
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlClient.SqlParameter("@STU_ID", uscStudentPicker.STU_ID.ToString)
        param(3) = New SqlClient.SqlParameter("@REC_NO", RECEIPT_NO)
        ViewState("REC_NO") = RECEIPT_NO


        Dim dso As New DataSet
        dso = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "FEES.FEE_ONLINE_COLLECTION_REPORT", param)


        grdOnline.DataSource = dso.Tables(0)
        grdOnline.DataBind()
        If dso.Tables(0).Rows.Count > 0 Then
            tbl_online.Visible = True
        Else
            tbl_online.Visible = False
        End If

        RefundGridbind(RECEIPT_NO)
        txtRemarks.Text = "ONLINE - "
    End Sub

    Sub RefundGridbind(ByVal RECEIPT_NO As String)
        Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OPTION", 1)
        param(1) = New SqlClient.SqlParameter("@RECEIPT_NO", RECEIPT_NO)
        param(2) = New SqlClient.SqlParameter("@FEE_BSU_ID", Session("sBsuid"))
        ViewState("REC_NO") = RECEIPT_NO


        Dim dsr As New DataSet
        dsr = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "FEES.FEE_REFUND_DETAILS_REPORT", param)


        grdRefund.DataSource = dsr.Tables(0)
        grdRefund.DataBind()
        If dsr.Tables(0).Rows.Count > 0 Then
            tbl_refund.Visible = True
        Else
            tbl_refund.Visible = False
        End If
    End Sub
    Protected Sub grdRefund_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRefund.PageIndexChanging
        grdRefund.PageIndex = e.NewPageIndex
        If Not ViewState("REC_NO").ToString Is Nothing And ViewState("REC_NO").ToString <> "" Then
            RefundGridbind(ViewState("REC_NO").ToString)
        End If
    End Sub
    Protected Sub lbl_RefundAmount_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim dcl_amount As TextBox = CType(sender, TextBox)
        Dim str_amount As String = dcl_amount.Text
        Dim Cur_Amount As Decimal = 0
        Dim Tot_Amount As Decimal = 0
        If str_amount <> "" Then
            Cur_Amount = CInt(str_amount)
        Else
            dcl_amount.Text = "0.00"
        End If

        For Each gvrow2 As GridViewRow In grdRefund.Rows
            Dim lb As TextBox = gvrow2.FindControl("lbl_RefundAmount")
            Dim lb1 As Label = gvrow2.FindControl("lbl_CollectionAmount")
            Dim lb2 As Label = gvrow2.FindControl("lbl_RefundedAmount")
            Dim lb3 As Label = gvrow2.FindControl("lbl_RefundableAmount")
            Dim amt1 As Decimal = CDbl(IIf(lb1.Text = "", 0, lb1.Text))
            Dim amt2 As Decimal = CDbl(IIf(lb2.Text = "", 0, lb2.Text))
            Dim amt3 As Decimal = CDbl(IIf(lb3.Text = "", 0, lb3.Text))

            'If CDbl(IIf(lb.Text = "", 0, lb.Text)) > (amt1 - amt2) Then
            If CDbl(IIf(lb.Text = "", 0, lb.Text)) > (amt3) Then
                usrMessageBar2.ShowNotification("Entered Amount is greater than Refundable Amount", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            Else
                Tot_Amount = Tot_Amount + CDbl(IIf(lb.Text = "", 0, lb.Text))
            End If

        Next
        txt_Total_Refund_Amt.Text = Tot_Amount

    End Sub

    Protected Sub txtBankCharge_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        'Dim dcl_amount As TextBox = CType(sender, TextBox)
        Dim str_amount As String = txtBankCharge.Text
        If str_amount = "" Then
            txtBankCharge.Text = "0.00"
        End If
    End Sub

    Sub set_bankaccount()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_PAYMENTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_PAYMENTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", ConnectionManger.GetOASISFINConnectionString)
        If str_bankact_name <> "--" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            'txtBankDescr.Text = str_bankact_name.Split("|")(1)
        Else
            txtBankCode.Text = ""
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_error As String = ""
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If

        If txtBankCode.Text = "" Then
            str_error = str_error & "Please select bank <br />"
        End If

        Dim lblAmt As New Label
        Dim dRefundAmount As Decimal = CDbl(txt_Total_Refund_Amt.Text)

        If dRefundAmount = 0 Then
            str_error = str_error & "Please enter Refund Amount <br />"
        End If

        'If Not IsNumeric(txtBankCharge.Text) Then
        '    txtBankCharge.Text = "0"
        'End If

        If txtRemarks.Text = "" Then
            str_error = str_error & "Please enter remarks<br />"
        End If
        Dim STR_BankOrCash As String = "O"
        Dim STR_ACCOUNT As String = String.Empty

        If str_error <> "" Then
            '  lblError.Text = str_error
            usrMessageBar2.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()

        Dim str_NEW_FRH_ID As String = ""
        Dim dDiffAmount As Decimal = 0
        Dim FRNNo As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000" 'current acd of student selected in sp so here scd is pasased as 0
            'Dim FRH_STU_ID As String = Me.gvRefundSummary.DataKeys(gvRefundSummary.Rows.Count - 1)("STU_ID")
            retval = FeeRefund.F_SaveFEE_REFUND_H(0, Session("sBsuid"), 0, ViewState("StudentType"), _
                uscStudentPicker.STU_ID.ToString, 0, dtFrom.ToString, dtFrom.ToString, False, str_NEW_FRH_ID, False, _
             txtRemarks.Text, "", STR_BankOrCash, txtBankCode.Text, "", dRefundAmount, txtBankCharge.Text, stTrans, 0, "ONLINE_REFUND", ViewState("lbl_FCL_FCL_ID").ToString, Session("sUsr_id"), "PENDING")
            If retval = "0" Then

                For Each gvr As GridViewRow In grdRefund.Rows
                    'Dim STU_ID As String = Me.gvRefundSummary.DataKeys(gvr.RowIndex)("STU_ID")
                    Dim STUID As String = DirectCast(gvr.FindControl("lbl_STU_ID"), Label).Text
                    Dim FEEID As String = DirectCast(gvr.FindControl("lbl_FEE_ID"), Label).Text
                    Dim dAmount As Double = Convert.ToDouble(DirectCast(gvr.FindControl("lbl_RefundAmount"), TextBox).Text)
                    FRNNo = DirectCast(gvr.FindControl("lbl_ReceiptNo"), Label).Text
                    'Dim FEE_ID_AMOUNT As String()
                    'If FEEIDs.Trim <> "" Then
                    '    FEE_ID_AMOUNT = FEEIDs.Split("|")
                    'Else
                    '    FEE_ID_AMOUNT = FEEIDs.Split("")
                    'End If
                    If dAmount > 0 Then

                        ' Dim FRD_FEE_ID As String = FEE_ID_AMOUNT(i).Split("#")(0)
                        Dim FRD_NARRATION As String = SqlHelper.ExecuteScalar(objConn.ConnectionString, CommandType.Text, "SELECT MAX(FEE_DESCR) FROM FEES.FEES_M WHERE FEE_ID=" & IIf(FEEID.Trim <> "", FEEID, 0) & "")
                        Dim FRD_AMOUNT As Double = Convert.ToDouble(dAmount)
                        retval = FeeRefund.F_SaveFEE_REFUND_D(0, str_NEW_FRH_ID, FRD_AMOUNT, _
                            FEEID, FRD_NARRATION, STUID, stTrans) 'uscStudentPicker.STU_ID.ToString

                        If retval <> 0 Then
                            Exit For
                        End If


                        'Else
                        '    retval = -1
                        '    Exit For
                    End If

                Next

            End If

            If retval = "0" Then
                retval = F_SaveFEE_ONLINE_REFUND_H(0, Session("sBsuid"), str_NEW_FRH_ID, dtFrom, 0, txtRemarks.Text, txtBankCode.Text, dRefundAmount, stTrans)
                'If retval = "0" Then
                '    retval = F_UpdateFEE_ONLINE_REFUND_H(str_NEW_FRH_ID, Session("sBsuid"), FRNNo, stTrans)
                'End If
            End If
            'If dDiffAmount < CDec(txtBankCharge.Text) Or CDec(txtBankCharge.Text) < 0 Then
            '    retval = 941 'Please check bank charge
            'End If
            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FRH_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                ' lblError.Text = getErrorMessage("0")
                usrMessageBar2.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                ClearDetails()
                uscStudentPicker.STU_ID = 0
                uscStudentPicker.ClearDetails()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar2.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            ' lblError.Text = getErrorMessage(1000)
            usrMessageBar2.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind_fee_details()
    End Sub


    Public Shared Function F_SaveFEE_ONLINE_REFUND_H(ByVal p_FRO_ID As Integer, ByVal p_FRO_BSU_ID As String, _
  ByVal p_FRO_FRH_ID As String, ByVal p_FRO_DATE As String, _
   ByRef NEW_FRO_ID As String, _
  ByVal p_FRO_REMARKS As String, _
  ByVal p_FRO_ACT_ID As String, ByVal p_FRO_AMOUNT As String, _
   ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FRO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FRO_ID
        pParms(1) = New SqlClient.SqlParameter("@FRO_FRH_ID", SqlDbType.Int)
        pParms(1).Value = p_FRO_FRH_ID
        pParms(2) = New SqlClient.SqlParameter("@FRO_AMOUNT", SqlDbType.Decimal)
        pParms(2).Value = p_FRO_AMOUNT
        pParms(3) = New SqlClient.SqlParameter("@FRO_ACT_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_FRO_ACT_ID
        pParms(4) = New SqlClient.SqlParameter("@FRO_BSU_ID", SqlDbType.VarChar, 25)
        pParms(4).Value = p_FRO_BSU_ID
        pParms(5) = New SqlClient.SqlParameter("@FRO_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FRO_DATE
        pParms(6) = New SqlClient.SqlParameter("@FRO_REMARKS", SqlDbType.VarChar, 300)
        pParms(6).Value = p_FRO_REMARKS
        pParms(7) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.ReturnValue
        pParms(8) = New SqlClient.SqlParameter("@NEW_FRO_ID", SqlDbType.BigInt)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@FRO_USER", SqlDbType.VarChar, 50)
        pParms(9).Value = Convert.ToString(HttpContext.Current.Session("sUsr_name"))
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFees & ".FEES.F_SaveFEE_ONLINE_REFUND_H", pParms)
        If pParms(7).Value = 0 Then
            NEW_FRO_ID = pParms(8).Value
        End If
        F_SaveFEE_ONLINE_REFUND_H = pParms(7).Value
    End Function

    Public Shared Function F_UpdateFEE_ONLINE_REFUND_H(ByVal p_FRH_ID As Integer, ByVal p_FRH_BSU_ID As String, _
ByVal p_FCL_RECEIPT_NO As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FRH_ID
        pParms(1) = New SqlClient.SqlParameter("@FRH_BSU_ID", SqlDbType.VarChar, 25)
        pParms(1).Value = p_FRH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@FCL_RECEIPT_NO", SqlDbType.VarChar, 25)
        pParms(2).Value = p_FCL_RECEIPT_NO

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFees & ".FEES.F_UpdateFEE_ONLINE_REFUND_H", pParms)

        Return pParms(3).Value
    End Function
End Class
