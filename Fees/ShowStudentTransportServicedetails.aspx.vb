Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Partial Class ShowStudentTransportServicedetails
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsPostBack = False Then
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")

                Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
                Dim BSU As String = IIf(Request.QueryString("BSU") Is Nothing, "", Request.QueryString("BSU"))
                Dim ACDID As String = IIf(Request.QueryString("ACDID") Is Nothing, "", Request.QueryString("ACDID"))
                Dim STUID As String = IIf(Request.QueryString("STUID") Is Nothing, "", Request.QueryString("STUID"))
                bindservicedetails(BSU, ACDID, STUID)
            End If

            If h_SelectedId.Value <> "Close" Then
                Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
                'Response.Write(" alert('uuu');")
                Response.Write("} </script>" & vbCrLf)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub bindservicedetails(ByVal bsu As String, ByVal acdid As String, ByVal STUID As String)

        Dim constr As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sqlstr As String = "SELECT SSV_ID,CONVERT(VARCHAR(100),SSV_FROMDATE,106) AS SSV_FROMDATE,CONVERT(VARCHAR(100),SSV_TODATE,106) AS SSV_TODATE,A.SBL_DESCRIPTION PICKUPAREA,B.SBL_DESCRIPTION DROPOFFAREA,SSV_ROUNDTRIP FROM VW_OSO_STUDENT_SERVICES_D INNER JOIN TRANSPORT.SUBLOCATION_M  A ON A.SBL_ID=SSV_SBL_ID INNER JOIN TRANSPORT.SUBLOCATION_M B ON B.SBL_ID=SSV_SBL_ID_DROPOFF WHERE SSV_STU_ID=" & STUID & " and SSV_BSU_ID='" & bsu & "'  AND SSV_ACD_ID='" & acdid & "' AND SSV_SVC_ID=1 AND SSV_PROVIDER_BSU_ID='" & Session("sBsuid") & "'"

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(constr, CommandType.Text, sqlstr)
        If ds.Tables(0).Rows.Count > 0 Then

            gvGroup.DataSource = ds
            gvGroup.DataBind()
        Else
            gvGroup.DataSource = ds
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGroup.DataBind()
            Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
            gvGroup.Rows(0).Cells.Clear()
            gvGroup.Rows(0).Cells.Add(New TableCell)
            gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
            'gvGroup.HeaderRow.Visible = True

        End If


    End Sub
   

  
    Protected Sub lbtnselect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HfSSV_ID As New HiddenField
        Dim HF_roundtrip As New HiddenField
        Dim lbClose As New LinkButton
        lbClose = sender
        HfSSV_ID = sender.Parent.FindControl("HfSSV_ID")
        HF_roundtrip = sender.Parent.FindControl("HF_roundtrip")
        Dim lbllocfrom As New Label
        lbllocfrom = sender.Parent.FindControl("lbllocfrom")
        If (Not HfSSV_ID Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & HfSSV_ID.Value & "||" & lbllocfrom.Text & "||" & HF_roundtrip.Value & "';")

            'Response.Write("window.close();")
            'Response.Write("} </script>")
            'h_SelectedId.Value = "Close"

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & HfSSV_ID.Value & "||" & lbllocfrom.Text.Replace("'", "\'") & "||" & HF_roundtrip.Value & "';")
            Response.Write("var oWnd = GetRadWindow('" & HfSSV_ID.Value & "||" & lbllocfrom.Text.Replace("'", "\'") & "||" & HF_roundtrip.Value & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub
End Class
