﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_feeTransportAttendanceView
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtToDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            txtFromDate.Text = "01/" & Format(Date.Now, "MMM/yyyy")
            gvFeeCollection.Attributes.Add("bordercolor", "#1b80b6")
            GridBind()
            Page.Title = "Transport Attendance Log"
        End If
    End Sub

    Sub GridBind()
        Dim BSU_ID As String = IIf(Request.QueryString("bsu") Is Nothing, String.Empty, Request.QueryString("bsu"))
        Dim STU_ID As String = IIf(Request.QueryString("stu") Is Nothing, String.Empty, Request.QueryString("stu"))
        Gridbind_Feedetails(BSU_ID, STU_ID)
    End Sub 

    Sub Gridbind_Feedetails(ByVal BSU_ID As String, ByVal STU_ID As String)
        Dim dtOnDate As DateTime = CDate(txtFromDate.Text)
        Dim str_Sql As String = " exec GetAttendanceDataForStudent @FromDate ='" & txtFromDate.Text & "'," & _
        " @ToDate='" & txtToDate.Text & "', @BSU_ID ='" & BSU_ID & "' , @STU_ID='" & STU_ID & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_TRANSPORT_ATTENDANCEConnection, _
        CommandType.Text, str_Sql)
        gvFeeCollection.DataSource = ds.Tables(0)
        gvFeeCollection.DataBind() 
    End Sub
 
    Protected Sub txtFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromDate.TextChanged
        GridBind()
    End Sub

    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToDate.TextChanged
        GridBind()
    End Sub

End Class
