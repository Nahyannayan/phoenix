Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_InternetCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)
        If Page.IsPostBack = False Then
            doClear()
            ChkPrint.Checked = True
            btnAdd.Visible = False

            initialize_components()

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A200354"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300300") Then
                'And ViewState("MainMnu_code") <> "A200310" And ViewState("MainMnu_code") <> "A200351"
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("MainMnu_code").Equals("F300300") Then
                    btnAdd.Visible = False
                    'btnSave.Visible = True

                End If
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        gvExcel.Attributes.Add("bordercolor", "#1b80b6")
    End Sub

    Public Sub getdata(ByVal filePath As String, ByVal Fname As String)
        Try
            Dim Query As String = ""
            'Query = " SELECT NO,DATE,STUDENTID,CASH,FEECODE,PAID,CHEQUE,CHKNO,CHKDATE,BANK  FROM  " & filePath & Fname
            Query = " SELECT * FROM  " & filePath & Fname
            Dim _table As DataTable
            _table = Mainclass.FecthFromDBFodbc(Query, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
            If h_Editid.Value.Equals("0") Then
                InsertTemtable(_table)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            ' lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim excelQuery As String = ""
            'and year(DT) = " & Session("F_YEAR") & "   
            excelQuery = " SELECT * FROM  [TableName] "

            'excelQuery = "SELECT * from [Sheet1$]"
            Dim _table As DataTable
            _table = Mainclass.FetchFromExcel(excelQuery, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
            If h_Editid.Value.Equals("0") Then
                InsertTemtable(_table)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            '  lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)

        End Try
    End Sub
   
    Sub initialize_components()
        txtHDocdate.Text = DateTime.Now.ToString("dd/MMM/yyyy")
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")
    End Sub

    Private Function CheckForErrors() As Boolean
        Dim retValue As Boolean = True
        If txtHDocdate.Text.Equals("") Then
            '  lblError.Text = "Invalid Date...!"
            usrMessageBar.ShowNotification("Invalid Date...!", UserControls_usrMessageBar.WarningType.Danger)

        ElseIf h_Editid.Equals("0") Then
            '  lblError.Text = "Could not prosess you are request Please contact Admin...!"
            usrMessageBar.ShowNotification("Could not prosess you are request Please contact Admin...!", UserControls_usrMessageBar.WarningType.Danger)
        Else
            retValue = False

        End If
        Return retValue
    End Function

    Private Sub doClear()
        gvExcel.DataSource = Nothing
        gvExcel.DataBind()
        h_Editid.Value = "0"
        btnSave.Visible = False
        ' RdExcel.Visible = False
        btnFind.Enabled = True
        ' btnAdd.Visible = True
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            ' lblError.Text = "Invalid Bank Selected"
            usrMessageBar.ShowNotification("Invalid Bank Selected", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Try
            If CheckForErrors.Equals(True) Then
                Exit Sub
            End If
            doInsert()
            doClear()
        Catch ex As Exception
            Errorlog(ex.Message)
            ' lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub printVoucher(ByVal ReceiptNo As String)
        Try
            Dim str_Sql, strFilter As String
            strFilter = " SELECT FCT_RECNO FROM FEES.FEECOLLECTION_Temp WHERE FCT_ID IN (" & ReceiptNo & ") "

            str_Sql = " SELECT * FROM FEES.VW_OSO_FEES_RECEIPT WHERE FCL_RECNO IN ( " & strFilter & ") AND FCL_BSU_ID = '" & Session("sBsuid") & "' "
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim repSource As New MyReportClass
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim ds As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)
                Dim params As New Hashtable

                params("STU_TYPE") = "Student ID"
                params("UserName") = Session("sUsr_name")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = True

                Dim repSourceSubRep(1) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                repSourceSubRep(1) = New MyReportClass
                'repSourceSubRep(2) = New MyReportClass
                Dim cmdSubColln As New SqlCommand

                ''SUBREPORT1
                cmdSubColln.CommandText = "SELECT * FROM FEES.VW_OSO_FEES_FEECOLLECTED WHERE FCL_RECNO IN ( " & strFilter & ") AND FCL_BSU_ID = '" & Session("sBsuid") & "' "
                cmdSubColln.Connection = New SqlConnection(str_conn)
                cmdSubColln.CommandType = CommandType.Text
                repSourceSubRep(0).Command = cmdSubColln

                ''''SUBREPORT2
                str_Sql = " SELECT FCL_ID,FCL_STU_ID,SUM(FCD_AMOUNT)FCD_AMOUNT, FCD_REFNO,FCD_DATE, " & _
                " FCD_CLT_ID,CLT_DESCR,EMR_DESCR,CUR_DESCR,CUR_DENOMINATION,BSU_CURRENCY, CRI_DESCR,FCL_RECNO " & _
                " FROM   FEES.VW_OSO_FEES_PAYMENTS WHERE FCL_RECNO IN ( " & strFilter & ") AND FCL_BSU_ID = '" & Session("sBsuid") & "' " & _
                " GROUP BY FCL_ID,FCL_STU_ID, FCD_REFNO,FCD_DATE, FCD_CLT_ID,CLT_DESCR,EMR_DESCR, " & _
                " CUR_DESCR,CUR_DENOMINATION,BSU_CURRENCY, CRI_DESCR,FCL_RECNO "

                Dim cmdSubPayments As New SqlCommand
                cmdSubPayments.CommandText = str_Sql
                cmdSubPayments.Connection = New SqlConnection(str_conn)
                cmdSubPayments.CommandType = CommandType.Text
                repSourceSubRep(1).Command = cmdSubPayments

                repSource.SubReport = repSourceSubRep
                repSource.ResourceName = "../../fees/Reports/RPT/rptInternetFeeCollectionReceipt.rpt"
                'repSource.ResourceName = "../../fees/Reports/RPT/rptFeeCollectionReceipt.rpt"
                Session("ReportSource") = repSource
                'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                h_print.Value = "print"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        doClear()
        btnSave.Visible = False
    End Sub

    Private Sub UpLoadDBF(ByVal flieType As String)
        Try
            If uploadFile.HasFile Then
                'Dim filePath As String = Server.MapPath("")
                Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                If Not Directory.Exists(filePath & "\temp") Then
                    Directory.CreateDirectory(filePath & "\temp")
                End If
                Dim FolderPath As String = filePath & "\temp\"
                filePath = filePath & "\temp\" & uploadFile.FileName 'FileName
                If uploadFile.HasFile Then
                    uploadFile.SaveAs(filePath)
                    Try
                        If flieType = "DBF" Then
                            getdata(FolderPath, uploadFile.FileName)
                        ElseIf flieType = "EXCEL" Then
                            getdataExcel(filePath)
                        End If
                        If File.Exists(filePath) Then
                            File.Delete(filePath)
                        End If
                    Catch ex As Exception
                        Errorlog(ex.Message)
                        'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
                        usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
                    End Try
                End If
            End If
        Catch ex As Exception
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Dim fileType As String = "DBF"
        If RdExcel.Checked Then
            fileType = "EXCEL"
        End If
        If (h_Editid.Value = "0") Then
            If Not uploadFile.HasFile Then
                '  lblError.Text = "Select Particular File...!"
                usrMessageBar.ShowNotification("Select Particular File...!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If RdDbf.Checked Then
                If Not (uploadFile.FileName.EndsWith("dbf", StringComparison.OrdinalIgnoreCase)) Then
                    '  lblError.Text = "Invalid file type.. Only DBF files are alowed.!"
                    usrMessageBar.ShowNotification("Invalid file type.. Only DBF files are alowed.!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            Else
                If Not (uploadFile.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or uploadFile.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase)) Then
                    ' lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
                    usrMessageBar.ShowNotification("Invalid file type.. Only Excel files are alowed.!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If
            UpLoadDBF(fileType)
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
           
        End If
    End Sub

    Private Sub InsertTemtable(ByVal _table As DataTable)
        If _table.Rows.Count > 0 Then
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("FEES.Insert_FEECOLLECTION_Temp", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim iReturnvalue As Integer = 0

            Dim x As Int32
            Try
                For x = 0 To _table.Rows.Count - 1
                    SqlCmd.Parameters.AddWithValue("@FCT_NO", _table.Rows(x)("NO"))
                    SqlCmd.Parameters.AddWithValue("@FCT_DATE", _table.Rows(x)("DATE"))
                    SqlCmd.Parameters.AddWithValue("@FCT_STUDENTID", _table.Rows(x)("STUDENTID"))
                    SqlCmd.Parameters.AddWithValue("@FCT_FEECODE", _table.Rows(x)("FEECODE"))
                    SqlCmd.Parameters.AddWithValue("@FCT_PAID", _table.Rows(x)("PAID"))
                    SqlCmd.Parameters.AddWithValue("@FCT_BANK", _table.Rows(x)("BANK"))
                    SqlCmd.Parameters.AddWithValue("@FCT_SOURCE", "MSQONLINE")
                    SqlCmd.Parameters.AddWithValue("@FCT_BSU_ID", Session("sBsuid"))
                    SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                    SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    SqlCmd.ExecuteNonQuery()
                    iReturnvalue = CInt(SqlCmd.Parameters("@ReturnValue").Value)

                    If iReturnvalue <> 0 Then
                        stTrans.Rollback()
                        '  lblError.Text = getErrorMessage(iReturnvalue.ToString())
                        usrMessageBar.ShowNotification(getErrorMessage(iReturnvalue.ToString()), UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    SqlCmd.Parameters.Clear()
                Next
                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                    '  lblError.Text = getErrorMessage(iReturnvalue.ToString())
                    usrMessageBar.ShowNotification(getErrorMessage(iReturnvalue.ToString()), UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                stTrans.Commit()

            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                '   lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
                usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try

            h_Editid.Value = "1"
            btnSave.Visible = True
            btnFind.Enabled = False
            bindData()

        End If
    End Sub

    Private Sub bindData()
        Dim Query As String = ""
        Query = " SELECT FEES.FEECOLLECTION_Temp.FCT_ID, " & _
                " VW_OSO_STUDENT_M.STU_NAME,  " & _
                " VW_OSO_STUDENT_M.STU_NO,  " & _
                " FEES.FEECOLLECTION_Temp.FCT_DATE,  " & _
                " FEES.FEECOLLECTION_Temp.FCT_PAID,  " & _
                " FEES.FEES_M.FEE_DESCR  " & _
                " FROM  " & _
                " FEES.FEECOLLECTION_Temp INNER JOIN " & _
                " FEES.FEES_M ON FEES.FEECOLLECTION_Temp.FCT_FEE_ID = FEES.FEES_M.FEE_ID INNER JOIN " & _
                " VW_OSO_STUDENT_M ON FEES.FEECOLLECTION_Temp.FCT_STU_ID = VW_OSO_STUDENT_M.STU_ID " & _
                " WHERE FCT_bProcessed = 0 AND FCT_BSU_ID = '" & Session("sBsuid") & "' ORDER BY VW_OSO_STUDENT_M.STU_NAME "
        Dim _table As DataTable = MainObj.getRecords(Query, "OASIS_FEESConnectionString")
        gvExcel.DataSource = _table
        gvExcel.DataBind()
        If _table.Rows.Count.Equals(0) Then
            '  lblError.Text = "No data found..."
            usrMessageBar.ShowNotification("No data found...", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If gvExcel.Rows.Count > 0 Then
            gvExcel.FooterRow.Cells(3).Text = "Total "
            gvExcel.FooterRow.Cells(4).Text = Amount.ToString("#,###0.00")
            gvExcel.FooterRow.Cells(4).HorizontalAlign = HorizontalAlign.Right
            btnSave.Visible = True
        End If
    End Sub

    Public Amount As Double = 0
    
    Protected Sub gvExcel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If Not e.Row.RowIndex.Equals(-1) Then
            Amount += Convert.ToDouble(e.Row.Cells(4).Text.ToString())
        End If
        If e.Row.Cells.Count > 5 Then
            e.Row.Cells(5).Style("display") = "none"
        End If
    End Sub

    Private Sub doInsert()
        Dim FctId As String = ""
        For Each grow As GridViewRow In gvExcel.Rows
            FctId += grow.Cells(5).Text.ToString() & "@"
        Next
        Dim _parameter As String(,) = New String(6, 1) {}
        _parameter(0, 0) = "@FCT_BSUID"
        _parameter(0, 1) = Session("sBsuid")
        _parameter(1, 0) = "@FCT_DATE"
        _parameter(1, 1) = Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("dd-MMM-yyyy")
        _parameter(2, 0) = "@FCTID"
        _parameter(2, 1) = FctId
        _parameter(3, 0) = "@FCT_UserName"
        _parameter(3, 1) = Session("sUsr_name")
        _parameter(4, 0) = "@FCL_BANK_ACT_ID"
        _parameter(4, 1) = txtBankCode.Text

        MainObj.doExcutiveRetvalue("FEES.Save_FEECOLLECTION_Temp", _parameter, "OASIS_FEESConnectionString", "")
        ' lblError.Text = MainObj.MESSAGE
        usrMessageBar.ShowNotification(MainObj.MESSAGE, UserControls_usrMessageBar.WarningType.Danger)

        If MainObj.SPRETVALUE.Equals(0) Then
            If (ChkPrint.Checked) Then
                printVoucher(FctId.Replace("@", ",") & "0")
            End If
            'btnAdd.Visible = True
            btnSave.Visible = False
            btnFind.Enabled = True
        End If
    End Sub

    Protected Sub RdNotprocessing_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindData()
        RdNotprocessing.Checked = False
    End Sub

    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
    End Sub

    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            ' lblError.Text = "Invalid bank selected"
            usrMessageBar.ShowNotification("Invalid bank selected", UserControls_usrMessageBar.WarningType.Danger)
            txtBankCode.Focus()
        Else
            lblError.Text = ""
            txtBankCode.Focus()
        End If
    End Sub

End Class

