Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeOtherChargePost
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F351080" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""

                    lblHead.Text = "Other Fee Charge Posting"

                    ddlAcademicYear.Items.Clear()
                    FillACD()
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next

    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""
            Dim lstrCondn2, lstrCondn5, lstrCondn6, lstrCondn7 As String
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            lstrCondn2 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOH_DATE", lstrCondn2)
                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FEE_DESCR", lstrCondn5)
                '   -- 6  txtAmount
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOH_AMOUNT", lstrCondn6)
                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOH_NARRATION", lstrCondn7)
            End If
            str_Sql = " SELECT FEES.FEEOTHCHARGE_H.FOH_ID, FEES.FEEOTHCHARGE_H.FOH_DATE,  FEES.FEES_M.FEE_DESCR, " & _
                        " FEES.FEEOTHCHARGE_H.FOH_AMOUNT, FEES.FEEOTHCHARGE_H.FOH_NARRATION, FEES.FEEOTHCHARGE_H.FOH_Bposted  " & _
                        " FROM  FEES.FEEOTHCHARGE_H INNER JOIN " & _
                        " FEES.FEES_M ON FEES.FEEOTHCHARGE_H.FOH_FEE_ID = FEES.FEES_M.FEE_ID  " & _
                        " WHERE FOH_BSU_ID = '" & Session("sBSUID") & "' AND  isnull(FOH_bDELETED,0)=0  AND FOH_Bposted = 0 " & _
                        " AND FOH_ACD_ID = '" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter & _
                        " ORDER BY FOH_DATE DESC , FOH_ID DESC "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrCondn6

            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim ChkPost As New CheckBox
            Dim linBtn As New LinkButton
            Dim lbVoucher As New HyperLink
            lbVoucher = TryCast(e.Row.FindControl("lbVoucher"), HyperLink)
            If lbVoucher IsNot Nothing Then

                ChkPost = e.Row.FindControl("ChkPost")
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", ChkPost.ClientID, "'"))
                ChkPost.Attributes.Add("onclick", " return getChkBoxid('" & ChkPost.ClientID & "')")
                ChkPost.Style("cursor") = "hand"

                ViewState("datamode") = Encr_decrData.Encrypt("view")
                lbVoucher.NavigateUrl = "FeeOtherCharge.aspx?viewid=" & Encr_decrData.Encrypt(e.Row.Cells(1).Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            e.Row.Cells(1).Style("display") = "none"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ChkPost As New CheckBox
        For Each grow As GridViewRow In gvJournal.Rows
            ChkPost = grow.FindControl("ChkPost")
            If ChkPost.Checked Then
                doPost(grow.Cells(1).Text.ToString())
                Exit Sub
            End If
        Next
    End Sub

    Sub doPost(ByVal PostId As String)
        Dim _parameter As String(,) = New String(6, 1) {}
        _parameter(0, 0) = "@FOH_ID"
        _parameter(0, 1) = PostId
        _parameter(1, 0) = "@FOH_BSU_ID"
        _parameter(1, 1) = Session("sBsuid")
        _parameter(2, 0) = "@AUD_WINUSER"
        _parameter(2, 1) = Page.User.Identity.Name.ToString()
        _parameter(3, 0) = "@Aud_form"
        _parameter(3, 1) = Master.MenuName.ToString()
        _parameter(4, 0) = "@Aud_user"
        _parameter(4, 1) = Session("sUsr_name")
        _parameter(5, 0) = "@Aud_module"
        _parameter(5, 1) = Session("sModule")
        MainObj.doExcutiveRetvalue("[FEES].[Post_FEEOTHCHARGE]", _parameter, "OASIS_FEESConnectionString", "")
        'lblError.Text = MainObj.MESSAGE
        If MainObj.MESSAGE = "Data Successfully updated" Then
            usrMessageBar.ShowNotification(MainObj.MESSAGE, UserControls_usrMessageBar.WarningType.Success)
        Else
            usrMessageBar.ShowNotification(MainObj.MESSAGE, UserControls_usrMessageBar.WarningType.Danger)
        End If

        If MainObj.SPRETVALUE.Equals(0) Then
            gridbind()
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        gridbind()
    End Sub
End Class
