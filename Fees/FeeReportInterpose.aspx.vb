﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
'ts
Partial Class Fees_FeeReportInterpose
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Session("sUsr_name") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If
            If Not Request.QueryString Is Nothing Then
                If Not Request.QueryString("STUID") Is Nothing Then
                    ViewState("STU_ID") = Encr_decrData.Decrypt(Request.QueryString("STUID").Replace(" ", "+"))
                End If
                If Not Request.QueryString("STYPE") Is Nothing Then
                    ViewState("STU_TYPE") = Encr_decrData.Decrypt(Request.QueryString("STYPE").Replace(" ", "+"))
                End If
                If Not Request.QueryString("SBSU") Is Nothing Then
                    ViewState("STU_BSU_ID") = Encr_decrData.Decrypt(Request.QueryString("SBSU").Replace(" ", "+"))
                End If
                If Not Request.QueryString("FDT") Is Nothing Then
                    ViewState("TO_DT") = Encr_decrData.Decrypt(Request.QueryString("FDT").Replace(" ", "+"))
                End If
                If Not Request.QueryString("FPHID") Is Nothing Then
                    ViewState("FPH_ID") = Encr_decrData.Decrypt(Request.QueryString("FPHID").Replace(" ", "+"))
                End If
                If Not Request.QueryString("INVNO") Is Nothing Then
                    ViewState("INV_NO") = Encr_decrData.Decrypt(Request.QueryString("INVNO").Replace(" ", "+"))
                End If
                If Not Request.QueryString("COMPID") Is Nothing Then
                    ViewState("COMP_ID") = Encr_decrData.Decrypt(Request.QueryString("COMPID").Replace(" ", "+"))
                End If
                If Not Request.QueryString("TYPE") Is Nothing Then
                    Dim ReportType = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                    Select Case ReportType
                        Case "LEDGER"
                            F_rptStudentLedger_Transport()
                        Case "AUDIT"
                            AuditReport()
                        Case "TPINV" 'Transport performa Invoice
                            PrintTransportPerformaInvoice()
                        Case "COURSEM"
                            PRINT_COURSE_MASTER()
                        Case "RECIEPT"
                            PRINT_RECEIPT()
                        Case "TAXINV"
                            PRINT_TAX_INVOICE()
                        Case "OTHCOLINV"
                            PRINT_OTHER_FEE_COLLECTION_INVOICE()
                        Case "BSTAXINV"
                            PRINT_BOOK_SALES_TAX_INVOICE()
                        Case "SRVTAXINV"
                            PRINT_SERVICE_FEE_TAX_INVOICE()
                        Case "TRTAXINV"
                            PRINT_TRANSPORT_TAX_INVOICE()
                        Case "STULEDGER"
                            F_rptStudentLedger()
                    End Select
                End If
            End If
        End If
    End Sub

    Private Sub F_rptStudentLedger_Transport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim FromDate, ToDate As String
        ToDate = ViewState("TO_DT") 'Format(Now.Date, OASISConstants.DateFormat)
        FromDate = UtilityObj.GetDataFromSQL("SELECT BSU_TRANSPORT_STARTDT FROM BUSINESSUNIT_M WHERE BSU_ID='" & ViewState("STU_BSU_ID") & "'", str_conn)
        If Not IsDate(FromDate) Then
            FromDate = "1/Sep/2007"
        Else
            Format(CDate(FromDate), OASISConstants.DateFormat)
        End If

        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub1 As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub1.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(ViewState("STU_ID"), XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub1.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub1.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
        cmdSub2.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ViewState("STU_BSU_ID")
        cmd.Parameters.Add(sqlpSTU_BSU_ID)
        cmdSub1.Parameters.AddWithValue("@STU_BSU_ID", ViewState("STU_BSU_ID"))
        cmdSub2.Parameters.AddWithValue("@STU_BSU_ID", ViewState("STU_BSU_ID"))

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = ViewState("STU_TYPE")
        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub1.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(FromDate)
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub1.Parameters.AddWithValue("@FromDT", CDate(FromDate))
        cmdSub2.Parameters.AddWithValue("@FromDT", CDate(FromDate))

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(ToDate)
        cmd.Parameters.Add(sqlpTODT)
        cmdSub1.Parameters.AddWithValue("@ToDT", CDate(ToDate))
        cmdSub2.Parameters.AddWithValue("@ToDT", CDate(ToDate))

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub1.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = FromDate
        params("ToDate") = ToDate
        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFee_Transport_StudentLedgerNext.rpt"
        Session("ReportSource") = repSource
        'h_print.Value = "ledger"
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub

    Sub AuditReport()
        Dim objTC As New FeeCollectionTransport
        objTC.BSU_ID = Session("sBsuId")
        objTC.USER_NAME = Session("sUsr_name")
        objTC.STU_ID = ViewState("STU_ID")

        objTC.GET_STUDENT_AUDIT_DETAIL()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", objTC.BSU_ID)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", objTC.USER_NAME)
        param.Add("CurrentDate", ViewState("TO_DT"))
        param.Add("studName", objTC.STU_NAME)
        param.Add("Grade", objTC.GRADE)
        param.Add("Section", objTC.SECTION)
        param.Add("StuNo", objTC.STU_NO)
        param.Add("status", objTC.STATUS)
        param.Add("stu_id", objTC.STU_ID)
        'param.Add("@STU_ID", objTC.STU_ID)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = ConnectionManger.GetOASISTransportConnection.Database
            .reportPath = HttpContext.Current.Server.MapPath("~/Transport/Reports/RPT/rptstudTransportAudit.rpt")
            .reportParameters = param
        End With
        Session("rptClassPopup") = rptClass

        Response.Redirect("../Reports/ASPX Report/rptReportViewer_Popup.aspx")
    End Sub

    Sub PrintTransportPerformaInvoice()
        Try
            If ViewState("COMP_ID") > 0 Then
                Session("ReportSource") = FeeTranspotInvoice.PrintReceiptComp(ViewState("INV_NO"), Session("sBsuid"), ViewState("STU_BSU_ID"), _
                                            Session("sUsr_name"))
            Else
                Session("ReportSource") = FeeTranspotInvoice.PrintReceipt(ViewState("FPH_ID"), Session("sBsuid"), _
                            Session("sUsr_name"), ViewState("STU_BSU_ID"))
            End If
            Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "TPINV_PRINT")
        End Try
    End Sub

    Sub PRINT_COURSE_MASTER()
        If Not Request.QueryString("ID") Is Nothing Then
            ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString

        Dim cmd As New SqlCommand("ENR.GET_RPT_COURSE_MASTER")
        Dim cmdSub1 As New SqlCommand("[ENR].[GET_FEE_TERMS]")
        Dim cmdSub2 As New SqlCommand("[ENR].[GET_FEESETUP]")
        Dim cmdSub3 As New SqlCommand("[ENR].[GET_FEESETUP]")
        Dim cmdSub4 As New SqlCommand("[ENR].[GET_SAVED_SCHOOLS]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub1.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure
        cmdSub3.CommandType = CommandType.StoredProcedure
        cmdSub4.CommandType = CommandType.StoredProcedure

        Dim sqlpCM_ID As New SqlParameter("@CM_ID", SqlDbType.Int)
        sqlpCM_ID.Value = ViewState("CM_ID")
        cmd.Parameters.Add(sqlpCM_ID)
        cmdSub1.Parameters.AddWithValue("@CM_ID", ViewState("CM_ID"))
        cmdSub2.Parameters.AddWithValue("@CM_ID", ViewState("CM_ID"))
        cmdSub2.Parameters.AddWithValue("@CM_bSESSION", False)
        cmdSub3.Parameters.AddWithValue("@CM_ID", ViewState("CM_ID"))
        cmdSub3.Parameters.AddWithValue("@CM_bSESSION", True)
        cmdSub4.Parameters.AddWithValue("@CM_ID", ViewState("CM_ID"))

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub1.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        cmdSub3.Connection = New SqlConnection(str_conn)
        cmdSub4.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(4) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        repSourceSubRep(2) = New MyReportClass
        repSourceSubRep(2).Command = cmdSub3
        repSourceSubRep(3) = New MyReportClass
        repSourceSubRep(3).Command = cmdSub4
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        repSource.SubReport = repSourceSubRep
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = "../../Enrichment365/Reports/RPT/rptCourseMaster.rpt"
        Session("ReportSource") = repSource
        'h_print.Value = "ledger"
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")

    End Sub
    Sub PRINT_RECEIPT()
        Dim FCL_ID As Int64 = 0, FCL_RECNO As String = "", BSU_ID As String = Session("sBsuId")
        If Not Request.QueryString("ID") Is Nothing Then
            FCL_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        End If
        If Not Request.QueryString("RECNO") Is Nothing Then
            FCL_RECNO = Encr_decrData.Decrypt(Request.QueryString("RECNO").Replace(" ", "+"))
        End If
        Dim str_Sql, strFilter As String
        strFilter = " FCL_RECNO = '" & FCL_RECNO & "' AND FCL_BSU_ID = '" & BSU_ID & "' "
        str_Sql = " SELECT * FROM [FEES].[VW_OSO_FEES_EMAIL_FEERECEIPT] WHERE " & strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        Dim repSource As New MyReportClass
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        repSource.ReportUniqueName = BSU_ID & "_" & FCL_RECNO
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            Dim params As New Hashtable
            params("UserName") = "SYSTEM"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = False
            '''''''''''''''
            Dim repSourceSubRep(2) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            repSourceSubRep(1) = New MyReportClass
            repSourceSubRep(2) = New MyReportClass

            Dim cmdHeader As New SqlCommand("getBsuInFoWithImage", New SqlConnection(ConnectionManger.GetOASISConnectionString))
            cmdHeader.CommandType = CommandType.StoredProcedure
            cmdHeader.Parameters.AddWithValue("@IMG_BSU_ID", BSU_ID)
            cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
            repSourceSubRep(1).Command = cmdHeader

            Dim cmdSubEarn As New SqlCommand
            cmdSubEarn.CommandText = "EXEC FEES.GetReceiptPrint_Online @FCL_RECNO ='" & FCL_RECNO & "', @FCL_BSU_ID  = '" & BSU_ID & "'"
            cmdSubEarn.Connection = New SqlConnection(str_conn)
            cmdSubEarn.CommandType = CommandType.Text
            repSourceSubRep(0).Command = cmdSubEarn

            Dim cmdSubPayment As New SqlCommand
            cmdSubPayment.CommandText = "FEES.GetReceiptPrint_Online"
            cmdSubPayment.CommandType = CommandType.StoredProcedure
            cmdSubPayment.Parameters.AddWithValue("@FCL_BSU_ID", BSU_ID)
            cmdSubPayment.Parameters.AddWithValue("@FCL_RECNO", FCL_RECNO)
            cmdSubPayment.Parameters.AddWithValue("@bPATMENT_DETAIL", True)
            cmdSubPayment.Connection = New SqlConnection(str_conn)
            repSourceSubRep(2).Command = cmdSubPayment
            repSource.SubReport = repSourceSubRep

            'repSource.ResourceName = "~/Reports/RPT/rptFeeReceipt.rpt"
            Dim bBSUTaxable As Boolean

            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & BSU_ID & "'"), Boolean)
            If bBSUTaxable Then
                repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT_EMAIL/rptFeeReceipt_Tax.rpt")
            Else
                repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT_EMAIL/rptFeeReceipt.rpt")
            End If
        End If
        Session("ReportSource") = repSource
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub
    Sub PRINT_TAX_INVOICE()
        Dim FSH_ID As String = "", BSU_ID As String = Session("sBsuId")
        If Not Request.QueryString("ID") Is Nothing Then
            FSH_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        End If
        If Not Request.QueryString("FSHID") Is Nothing Then
            FSH_ID = (Request.QueryString("FSHID").Replace(" ", "+"))
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.PRINT_FEE_TAX_INVOICE")
        cmd.CommandType = CommandType.StoredProcedure
        Dim cmdSub1 As New SqlCommand("FEES.PRINT_FEE_TAX_INVOICE_SPLITUP")
        cmdSub1.CommandType = CommandType.StoredProcedure
        Dim cmdSub2 As New SqlCommand("dbo.getBsuInFoWithImage")
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpFSH_ID As New SqlParameter("@FSH_ID", SqlDbType.BigInt)
        sqlpFSH_ID.Value = FSH_ID
        cmd.Parameters.Add(sqlpFSH_ID)
        cmd.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub.Value = BSU_ID
        cmdSub1.Parameters.Add(sqlpBSU_IDsub)
        Dim sqlpFSH_IDsub As New SqlParameter("@FSH_ID", SqlDbType.BigInt)
        sqlpFSH_IDsub.Value = FSH_ID
        cmdSub1.Parameters.Add(sqlpFSH_IDsub)
        cmdSub1.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub2 As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub2.Value = BSU_ID
        cmdSub2.Parameters.Add(sqlpBSU_IDsub2)
        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmdSub2.Parameters.Add(sqlpIMG_TYPE)
        cmdSub2.Connection = New SqlConnection(str_conn)

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub1
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub2


        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("userName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.HeaderBSUID = BSU_ID
        repSource.ResourceName = "../../Fees/Reports/RPT/rptFeeTAXInvoicePrint.rpt"

        Session("ReportSource") = repSource
        Dim retval = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "UPDATE FEES.FEESCHEDULE SET FSH_INVOICE_PRINTED_COUNT=ISNULL(FSH_INVOICE_PRINTED_COUNT,0) + 1 WHERE FSH_BSU_ID='" & BSU_ID & "' AND FSH_ID=" & FSH_ID & "")
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub

    Sub PRINT_OTHER_FEE_COLLECTION_INVOICE()
        Dim FOC_ID As String = "", BSU_ID As String = Session("sBsuId")
        If Not Request.QueryString("ID") Is Nothing Then
            FOC_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        End If
        If Not Request.QueryString("FOCID") Is Nothing Then
            FOC_ID = (Request.QueryString("FOCID").Replace(" ", "+"))
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.PRINT_OTHER_FEE_COLLECTION")
        cmd.CommandType = CommandType.StoredProcedure


        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpFOC_ID As New SqlParameter("@FOC_ID", SqlDbType.BigInt)
        sqlpFOC_ID.Value = FOC_ID
        cmd.Parameters.Add(sqlpFOC_ID)
        cmd.Connection = New SqlConnection(str_conn)


        Dim repSource As New MyReportClass
        Dim params As New Hashtable

        params("UserName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.HeaderBSUID = BSU_ID
        repSource.ResourceName = "../../Fees/Reports/RPT/rptOtherFeeCollectionInvoice.rpt"

        Session("ReportSource") = repSource
        Dim retval = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "UPDATE FEES.FEEOTHCOLLECTION_H SET FOC_bINVOICE_PRINTED=1 WHERE FOC_BSU_ID='" & BSU_ID & "' AND FOC_ID=" & FOC_ID & "")
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub

    Sub PRINT_BOOK_SALES_TAX_INVOICE()
        Dim SAL_ID As String = "", BSU_ID As String = Session("sBsuId")
        If Not Request.QueryString("ID") Is Nothing Then
            SAL_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim cmd As New SqlCommand("dbo.PRINT_BOOK_SALES_TAX_INVOICE")
        cmd.CommandType = CommandType.StoredProcedure
        Dim cmdSub1 As New SqlCommand("dbo.PRINT_BOOK_SALES_TAX_INVOICE_SPLITUP")
        cmdSub1.CommandType = CommandType.StoredProcedure
        Dim cmdSub2 As New SqlCommand("dbo.getBsuInFoWithImage")
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSAL_ID As New SqlParameter("@SAL_ID", SqlDbType.BigInt)
        sqlpSAL_ID.Value = SAL_ID
        cmd.Parameters.Add(sqlpSAL_ID)
        cmd.Connection = New SqlConnection(str_conn)

        Dim sqlpSAL_IDsub As New SqlParameter("@SAL_ID", SqlDbType.BigInt)
        sqlpSAL_IDsub.Value = SAL_ID
        cmdSub1.Parameters.Add(sqlpSAL_IDsub)
        cmdSub1.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub2 As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub2.Value = BSU_ID
        cmdSub2.Parameters.Add(sqlpBSU_IDsub2)
        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmdSub2.Parameters.Add(sqlpIMG_TYPE)
        cmdSub2.Connection = New SqlConnection(ConnectionManger.GetOASISConnectionString)

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2


        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("UserName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.HeaderBSUID = BSU_ID
        repSource.ResourceName = "../../Fees/Reports/RPT/rptBookSalesTAXInvoice.rpt"

        Session("ReportSource") = repSource
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub

    Sub PRINT_SERVICE_FEE_TAX_INVOICE()
        Dim FSH_ID As String = "", BSU_ID As String = Session("sBsuId")
        If Not Request.QueryString("ID") Is Nothing Then
            FSH_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        End If
        If Not Request.QueryString("FSHID") Is Nothing Then
            FSH_ID = (Request.QueryString("FSHID").Replace(" ", "+"))
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("FEES.PRINT_FEE_TAX_INVOICE")
        cmd.CommandType = CommandType.StoredProcedure
        Dim cmdSub1 As New SqlCommand("FEES.PRINT_FEE_TAX_INVOICE_SPLITUP")
        cmdSub1.CommandType = CommandType.StoredProcedure
        Dim cmdSub2 As New SqlCommand("dbo.getBsuInFoWithImage")
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpFSH_ID As New SqlParameter("@FSH_ID", SqlDbType.BigInt)
        sqlpFSH_ID.Value = FSH_ID
        cmd.Parameters.Add(sqlpFSH_ID)
        cmd.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub.Value = BSU_ID
        cmdSub1.Parameters.Add(sqlpBSU_IDsub)
        Dim sqlpFSH_IDsub As New SqlParameter("@FSH_ID", SqlDbType.BigInt)
        sqlpFSH_IDsub.Value = FSH_ID
        cmdSub1.Parameters.Add(sqlpFSH_IDsub)
        cmdSub1.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub2 As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub2.Value = BSU_ID
        cmdSub2.Parameters.Add(sqlpBSU_IDsub2)
        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmdSub2.Parameters.Add(sqlpIMG_TYPE)
        cmdSub2.Connection = New SqlConnection(str_conn)

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub1
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub2


        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("userName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.HeaderBSUID = BSU_ID
        repSource.ResourceName = "../../StudentServices/Reports/RPT/rptServiceFeeTAXInvoicePrint.rpt"

        Session("ReportSource") = repSource
        Dim retval = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "UPDATE FEES.FEESCHEDULE SET FSH_INVOICE_PRINTED_COUNT=ISNULL(FSH_INVOICE_PRINTED_COUNT,0) + 1 WHERE FSH_BSU_ID='" & BSU_ID & "' AND FSH_ID=" & FSH_ID & "")
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub

    Sub PRINT_TRANSPORT_TAX_INVOICE()
        Dim FSH_ID As String = "", BSU_ID As String = Session("sBsuId")
        If Not Request.QueryString("ID") Is Nothing Then
            FSH_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
        End If
        If Not Request.QueryString("FSHID") Is Nothing Then
            FSH_ID = (Request.QueryString("FSHID").Replace(" ", "+"))
        End If

        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("FEES.PRINT_FEE_TAX_INVOICE")
        cmd.CommandType = CommandType.StoredProcedure
        Dim cmdSub1 As New SqlCommand("FEES.PRINT_FEE_TAX_INVOICE_SPLITUP")
        cmdSub1.CommandType = CommandType.StoredProcedure
        Dim cmdSub2 As New SqlCommand("dbo.getBsuInFoWithImage")
        cmdSub2.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSU_ID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpFSH_ID As New SqlParameter("@FSH_ID", SqlDbType.BigInt)
        sqlpFSH_ID.Value = FSH_ID
        cmd.Parameters.Add(sqlpFSH_ID)
        cmd.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub.Value = BSU_ID
        cmdSub1.Parameters.Add(sqlpBSU_IDsub)
        Dim sqlpFSH_IDsub As New SqlParameter("@FSH_ID", SqlDbType.BigInt)
        sqlpFSH_IDsub.Value = FSH_ID
        cmdSub1.Parameters.Add(sqlpFSH_IDsub)
        cmdSub1.Connection = New SqlConnection(str_conn)

        Dim sqlpBSU_IDsub2 As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_IDsub2.Value = BSU_ID
        cmdSub2.Parameters.Add(sqlpBSU_IDsub2)
        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmdSub2.Parameters.Add(sqlpIMG_TYPE)
        cmdSub2.Connection = New SqlConnection(str_conn)

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub1
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub2


        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("userName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.HeaderBSUID = BSU_ID
        repSource.ResourceName = "../../Fees/Reports/RPT/rptFeeTransportTAXInvoicePrint.rpt"

        Session("ReportSource") = repSource
        Dim retval = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "UPDATE FEES.FEESCHEDULE SET FSH_INVOICE_PRINTED_COUNT=ISNULL(FSH_INVOICE_PRINTED_COUNT,0) + 1 WHERE FSH_BSU_ID='" & BSU_ID & "' AND FSH_ID=" & FSH_ID & "")
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub

    Private Sub F_rptStudentLedger()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure

        'ALTER procedure rptStudentLedger
        ' @BSU_ID VARCHAR(20)='125016',
        '@STU_IDS VARCHAR(2000)='',
        '@STU_TYPE VARCHAR(5)='S',
        '@FromDT DATETIME ='1-JUN-2008',
        '@ToDT DATETIME='1-JUN-2008'

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(ViewState("STU_ID"), XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ViewState("STU_BSU_ID")
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub.Parameters.AddWithValue("@BSU_ID", ViewState("STU_BSU_ID"))
        cmdSub2.Parameters.AddWithValue("@BSU_ID", ViewState("STU_BSU_ID"))

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = ViewState("STU_TYPE")

        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = Format(Date.Today.AddMonths(-12), OASISConstants.DataBaseDateFormat)
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub.Parameters.AddWithValue("@FromDT", Format(Date.Today.AddMonths(-12), OASISConstants.DataBaseDateFormat))
        cmdSub2.Parameters.AddWithValue("@FromDT", Format(Date.Today.AddMonths(-12), OASISConstants.DataBaseDateFormat))

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = Date.Today
        cmd.Parameters.Add(sqlpTODT)
        cmdSub.Parameters.AddWithValue("@ToDT", Format(Date.Today, OASISConstants.DataBaseDateFormat))
        cmdSub2.Parameters.AddWithValue("@ToDT", Format(Date.Today, OASISConstants.DataBaseDateFormat))

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ViewState("STU_BSU_ID"))
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = sqlpFromDT.Value
        params("ToDate") = sqlpTODT.Value
        params("RoundOffVal") = ViewState("RoundOffVal")
        params("STU_TYPE") = sqlpSTU_TYPE.Value

        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ViewState("STU_BSU_ID")
        'repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeStudentLedger.rpt"
        Session("ReportSource") = repSource
        Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
    End Sub
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub
End Class
