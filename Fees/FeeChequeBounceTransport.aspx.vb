Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeChequeBounceTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEReminder.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                ddlBusinessunit.DataBind()
                If Not ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")) Is Nothing Then
                    ddlBusinessunit.ClearSelection()
                    ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")).Selected = True
                End If

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ClientScript.RegisterStartupScript(Me.GetType(), _
                "script", "<script language='javascript'>  CheckOnPostback(); </script>")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_BOUNCE_TRANSPORT Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtFromDate.Text = Format(DateTime.Now.AddDays(-1 * DateTime.Now.Day + 1), OASISConstants.DateFormat)
                txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtCHQBankDescr.Attributes.Add("ReadOnly", "ReadOnly")
                txtCHQBankCode.Attributes.Add("ReadOnly", "ReadOnly") 
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "view" Then
                    Exit Sub
                End If
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEReminder.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEReminder.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Sub GridBind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TransportConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim str_Filter, lstrOpr, str_Sql, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            str_Filter = ""
            If gvFEEReminder.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)

                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)

                '   -- 2  txtFrom
                'larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtConcession")
                'lstrCondn3 = txtSearch.Text.Trim
                'If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCM_DESCR", lstrCondn3)

                ''   -- 5  city
                'larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtPeriod")
                'lstrCondn5 = txtSearch.Text.Trim
                'If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "PERIOD", lstrCondn5)

            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            Dim str_cond As String = String.Empty
            Dim asOnDate As Date = CDate(txtDate.Text)
            Dim vLevel As Int16
            'str_cond += " AND FEES.FEESCHEDULE.FSH_BSU_ID='" & Session("sBsuid") & "' " & str_Filter
            str_Sql = "SELECT * FROM FEES.vw_OSA_CHEQUEBOUNCELIST " & _
            " WHERE FCL_BSU_ID = '" & Session("sBSUID") & "' and FCL_STU_BSU_ID= '" & ddlBusinessunit.SelectedItem.Value & "' " & _
            "AND (FCQ_ID NOT IN " & _
            " (SELECT FCR_FCQ_ID FROM OASIS.FEES.FEECHEQUERETURN_H))"
            str_Sql += GetFilter()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvFEEReminder.DataSource = ds.Tables(0)
            Session("sAsOnDate") = txtDate.Text
            Session("sLevel") = vLevel
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEReminder.DataBind()
                Dim columnCount As Integer = gvFEEReminder.Rows(0).Cells.Count
                gvFEEReminder.Rows(0).Cells.Clear()
                gvFEEReminder.Rows(0).Cells.Add(New TableCell)
                gvFEEReminder.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEReminder.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEReminder.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEReminder.DataBind()
            End If

            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtConcession")
            'txtSearch.Text = lstrCondn3

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtFrom")
            'txtSearch.Text = lstrCondn4

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtPeriod")
            'txtSearch.Text = lstrCondn5

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtRemarks")
            'txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Public Function GetFilter() As String
        Dim str_filter As String = String.Empty
        If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
            str_filter = " AND FCQ_DATE BETWEEN '" & txtFromDate.Text & "' AND '" & txtToDate.Text & "' "
        End If
        'If txtBankCode.Text <> "" Then
        '    str_filter += "AND VHH_ACT_ID = '" & txtCHQBankCode.Text & "'"
        'End If
        If txtCHQBankCode.Text <> "" Then
            str_filter += " AND FCQ_BANK = '" & txtCHQBankCode.Text & "'"
        End If
        Return str_filter
    End Function


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEReminder.PageIndexChanging
        gvFEEReminder.PageIndex = e.NewPageIndex 
        GridBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub 

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim trans As SqlTransaction = objConn.BeginTransaction("FEE_CHQ_CANCEL")

        Dim dtDate As DateTime = CDate(txtDate.Text)
        Try
            Dim NEW_FCR_ID As String = "0"
            Dim retVal As Integer
            For Each gvr As GridViewRow In gvFEEReminder.Rows
                Dim chkControl As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
                Dim lblFCL_STU_ID As Label = CType(gvr.FindControl("lblFCL_STU_ID"), Label)
                Dim lblFCQ_ID As Label = CType(gvr.FindControl("lblFCQ_ID"), Label)
                Dim lblFCL_STU_TYPE As Label = CType(gvr.FindControl("lblFCL_STU_TYPE"), Label)
                If Not chkControl Is Nothing And Not lblFCL_STU_ID Is Nothing And Not lblFCQ_ID Is Nothing Then
                    If chkControl.Checked Then
                        retVal = F_SAVEFEECHEQUERETURN_H(0, txtDate.Text, Session("sBsuid"), lblFCL_STU_TYPE.Text, lblFCL_STU_ID.Text, _
                                           CDec(txtBankcharge.Text), txtNarration.Text, False, ddlBusinessunit.SelectedItem.Value, lblFCQ_ID.Text, NEW_FCR_ID, chkChqReturn.Checked, trans)
                    End If
                    If retVal <> 0 Then
                        Exit For
                    End If
                End If
            Next
            If retVal <> 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                txtNarration.Text = ""
                txtBankcharge.Text = ""
                GridBind()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", NEW_FCR_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Function F_SAVEFEECHEQUERETURN_H(ByVal p_FCR_ID As Long, ByVal p_FCR_DATE As String, _
        ByVal p_FCR_BSU_ID As String, ByVal p_FCR_STU_TYPE As String, ByVal p_FCR_STU_ID As String, _
        ByVal p_FCR_BNK_CHARG As Decimal, ByVal p_FCR_NARRATION As String, ByVal p_FCR_Bposted As Boolean, _
        ByVal p_FCR_STU_BSU_ID As String, ByVal p_FCR_FCQ_ID As String, ByRef p_NEW_FCR_ID As String, _
       ByVal p_FCR_CHQ_RETURN As Boolean, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(13) As SqlClient.SqlParameter
       
        pParms(0) = New SqlClient.SqlParameter("@FCR_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCR_ID
        pParms(1) = New SqlClient.SqlParameter("@FCR_DATE", SqlDbType.DateTime)
        pParms(1).Value = p_FCR_DATE
        pParms(2) = New SqlClient.SqlParameter("@FCR_BSU_ID", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCR_BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@FCR_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(3).Value = p_FCR_STU_TYPE
        pParms(4) = New SqlClient.SqlParameter("@FCR_STU_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCR_STU_ID
        pParms(5) = New SqlClient.SqlParameter("@FCR_BNK_CHARG", SqlDbType.Decimal, 21)
        pParms(5).Value = p_FCR_BNK_CHARG
        pParms(6) = New SqlClient.SqlParameter("@FCR_Bposted", SqlDbType.Bit)
        pParms(6).Value = p_FCR_Bposted
        pParms(7) = New SqlClient.SqlParameter("@FCR_NARRATION", SqlDbType.VarChar)
        pParms(7).Value = p_FCR_NARRATION
        pParms(8) = New SqlClient.SqlParameter("@FCR_STU_BSU_ID", SqlDbType.VarChar)
        pParms(8).Value = p_FCR_STU_BSU_ID
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCR_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCR_FCQ_ID", SqlDbType.BigInt)
        pParms(11).Value = p_FCR_FCQ_ID
        pParms(12) = New SqlClient.SqlParameter("@FCR_bCHQ_RETURN", SqlDbType.Bit)
        pParms(12).Value = p_FCR_CHQ_RETURN

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SAVEFEECHEQUERETURN_H", pParms)
        If pParms(9).Value = 0 Then
            p_NEW_FCR_ID = pParms(10).Value
        End If
        F_SAVEFEECHEQUERETURN_H = pParms(9).Value
    End Function


    Private Sub ClearDetails()
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        'lblError.Text = ""
        txtBankcharge.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        GridBind()
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        Session("PROVIDER_BSU_ID") = ddlBusinessunit.SelectedItem.Value
        GridBind()
    End Sub

End Class
