<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEReminderWithAging.aspx.vb" Inherits="Fees_FEEReminder" Title="Untitled Page" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

   <%-- <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />--%>


    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
function CheckForPrint() {
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        //showModelessDialog('../Reports/ASPX Report/RptViewerModalView.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModalView.aspx', 'search', '55%', '85%')
        return false;
    }
}
    );
function CheckBoxListSelect(cbControl, state) {
    var chkBoxList = document.getElementById(cbControl);
    var chkBoxCount = chkBoxList.getElementsByTagName("input");
    for (var i = 0; i < chkBoxCount.length; i++) {
        chkBoxCount[i].checked = state;
    }

    return false;
}

function ChangeCheckBoxState(id, checkState) {
    var cb = document.getElementById(id);
    if (cb != null)
        cb.checked = checkState;
}

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Fee Reminder..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_Value" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%" border="0">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reminder Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="RMD_DESCR" DataValueField="RMD_ID">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Reminder letter Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reminder Title</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtReminderHeading" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4">Filtering Criteria</td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Reminder As On</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAsOnDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAsOnDate"
                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <asp:Button ID="btnResettle" runat="server" CssClass="button" Text="Re-Settle" OnClick="btnResettle_Click" ToolTip="Re settle students fees, be patient and it may take a while to complete" />
                        </td>
                        <td align="left" colspan="3">
                            <asp:LinkButton ID="lnkBtnHelp" runat="server" OnClientClick="return false;">?</asp:LinkButton>
                            <ajaxToolkit:HoverMenuExtender ID="hmeG" runat="Server" HoverCssClass="popupHover"
                                PopDelay="25" PopupControlID="PanelG" PopupPosition="Left" TargetControlID="lnkBtnHelp">
                            </ajaxToolkit:HoverMenuExtender>
                            <asp:Panel ID="PanelG" runat="server" CssClass="panel-cover" Style="display: none">
                                <div style="border: rgba(0,0,0,0.2) 1px solid; padding: 5px; margin: 1px; left:100px;">
                                    <asp:Label runat="server" ID="lblSettle" Text="" CssClass="field-label"></asp:Label>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Section</span></td>
                        <td align="left" width="30%">
                            <asp:LinkButton ID="lnkSection" runat="server" OnClientClick="return false;">Select Section</asp:LinkButton>
                            <asp:Panel ID="Panel2" runat="server" CssClass="panel-cover" Width="250px">
                                <div class="checkbox-list-full">

                                    <uc1:usrTreeView ID="UsrTreeView1" runat="server" />
                                    <div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Ageing Details</span></td>
                        <td align="left" width="30%">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="40%">
                                        <asp:Label ID="lblAgefrom" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="20%">To
                                    </td>
                                    <td align="left" width="40%">
                                        <asp:Label ID="lblAgeTo" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Amount Limit</span></td>
                        <td align="left" width="30%">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="70%">
                                        <asp:TextBox ID="txtAmountLimit" runat="server">0</asp:TextBox>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkIncludePDC" runat="server" Text="Include PDC" CssClass="field-label" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reminder in Arabic</span></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkArabic" runat="server" CssClass="field-label" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">Send Reminder to</span></td>
                        <td align="left" width="30%">
                            <asp:RadioButtonList ID="rblReminderFor" runat="server">
                                <asp:ListItem Selected="True" class="field-label" Value="PC">Primary Contact</asp:ListItem>
                                <asp:ListItem Value="SC" class="field-label">Secondary Contact</asp:ListItem>
                                <asp:ListItem Value="B" class="field-label">Both the above</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFeeType" runat="server">
                            </asp:DropDownList>

                        </td>
                        <td align="left" width="20%">
                            <asp:LinkButton ID="lnkAddFEETYPE" runat="server">Add</asp:LinkButton></td>
                        <td align="left" width="30%">
                            <asp:Button ID="btnListSudents" runat="server" CssClass="button" Text="List Students" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <asp:GridView ID="gvFEETYPEDET" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="FEE ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FEE TYPE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("FEE_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDeleteFEETYPE" runat="server" OnClick="lnkDeleteFEETYPE_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </td>
                        <td align="center" colspan="3">
                            <asp:GridView ID="gvFEEReminder" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>

                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFSH_STU_ID" runat="server" Text='<%# Bind("FSH_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label><br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("GRM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>
                                            Section<br />
                                            <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSectionSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount Due">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAmtDue" runat="server" OnClientClick="return false;" Text='<%# Bind("Due_AMOUNT") %>'></asp:LinkButton>
                                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" DynamicContextKey='<%# Eval("FSH_STU_ID") %>'
                                                DynamicControlID="Panel1" DynamicServiceMethod="GetAmountDue" PopupControlID="Panel1"
                                                Position="left" TargetControlID="lnkAmtDue">
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
                                SelectCommand="SELECT cast(RMD_ID as varchar) + '_' + CAST(ISNULL(RMD_AgeFrom, '') as varchar) + '_' + cast(ISNULL(RMD_AgeTo,'') as varchar) RMD_ID, RMD_DESCR FROM REMINDERTYPE_M"></asp:SqlDataSource>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print after Save" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnSaveNExport" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>


                <ajaxToolkit:CalendarExtender
                    ID="calFromDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender
                    ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgDate"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_print" runat="server" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server"
                    TargetControlID="lnkSection"
                    PopupControlID="Panel2"
                    CommitProperty="value"
                    Position="Bottom"
                    CommitScript="" />

            </div>
        </div>
    </div>

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>
