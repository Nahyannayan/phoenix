Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class fees_FeeProformaInvPrintTemplate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AppendHeader("Pragma", "no-cache")
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300231" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ClearDetails()
                GetTemplateDetails(0)
            End If
            If ViewState("datamode") = "view" Then
                Dim PVM_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("PVM_ID") = PVM_ID
                GetTemplateDetails(PVM_ID)
                DissableControls(True)
                SetEditingRight(PVM_ID)
                'Me.btnPreview.Visible = True
            End If
        End If
    End Sub
    Sub SetEditingRight(ByVal PVMID As Integer)
        Dim qry = "SELECT count(PVM_ID) FROM FEES.PERFORMA_VERSION_M WHERE PVM_BSU_ID='" & Session("sBsuid") & "' AND PVM_ID>" & PVMID & " "
        If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry) > 0 Then
            Me.btnEdit.Enabled = False
            Me.btnSave.Visible = False
            '  Me.lblError.Text = "Editing disabled, because a latest version exists."
            usrMessageBar.ShowNotification("Editing disabled, because a latest version exists.", UserControls_usrMessageBar.WarningType.Danger)

        End If
    End Sub

    Private Sub GetTemplateDetails(ByVal PVM_ID As Integer)
        Dim qry As String = String.Empty
        If PVM_ID = 0 Then
            qry = "SELECT TOP 1 PVM_ID ,(PVM_VERSION_NO) PVM_VERSION_NO ,PVM_PAYMENTMODE ,PVM_BANKTRANSFERDETAIL ,PVM_PROMOTIONDETAIL , " & _
                    "PVM_FEE_AMEND_DESCR ,PVM_THANKYOU_MSG ,PVM_FOOTER, PVM_bSHOW_REFER_A_FRIEND ,PVM_CREATEDDATE ,PVM_LASTMODIFIEDDATE , " & _
                    "PVM_CREATEDUSER ,PVM_LOGUSER ,PVM_BSU_ID FROM FEES.[vw_PERFORMA_VERSION_M] " & _
                    "WHERE PVM_BSU_ID='" & Session("sBsuid") & "' ORDER BY PVM_VERSION_NO DESC "
        Else
            qry = "SELECT PVM_ID ,(PVM_VERSION_NO) PVM_VERSION_NO,PVM_PAYMENTMODE ,PVM_BANKTRANSFERDETAIL ,PVM_PROMOTIONDETAIL , " & _
                    "PVM_FEE_AMEND_DESCR ,PVM_THANKYOU_MSG,PVM_FOOTER ,PVM_bSHOW_REFER_A_FRIEND ,PVM_CREATEDDATE ,PVM_LASTMODIFIEDDATE , " & _
                    "PVM_CREATEDUSER ,PVM_LOGUSER ,PVM_BSU_ID FROM FEES.[vw_PERFORMA_VERSION_M] " & _
                    "WHERE PVM_ID=" & PVM_ID & ""
        End If

        Dim dreader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_FEESConnection, CommandType.Text, qry)
        While (dreader.Read())
            Me.lblVsnNo.Text = CDec(dreader("PVM_VERSION_NO")) + 0.1
            ViewState("VERSION_NO") = dreader("PVM_VERSION_NO").ToString
            Me.txtPayMode.Content = dreader("PVM_PAYMENTMODE").ToString
            Me.txtBankTfrDetail.Content = dreader("PVM_BANKTRANSFERDETAIL").ToString
            Me.txtPromotion.Content = dreader("PVM_PROMOTIONDETAIL").ToString
            Me.txtFeeDescr.Content = dreader("PVM_FEE_AMEND_DESCR").ToString
            Me.txtThankU.Content = dreader("PVM_THANKYOU_MSG").ToString
            Me.txtFooter.Content = dreader("PVM_FOOTER").ToString
            Me.chkRAF.Checked = CBool(dreader("PVM_bSHOW_REFER_A_FRIEND"))
            If Me.chkRAF.Checked Then
                Me.imgRAF.Visible = True
                Me.imgRAF.ImageUrl = "../Images/ReferAFriend.png"
            Else
                Me.imgRAF.ImageUrl = ""
                Me.imgRAF.Visible = False
            End If
        End While


    End Sub

    Private Sub DissableControls(ByVal bDissable As Boolean)
        txtPromotion.Enabled = Not bDissable
        txtPayMode.Enabled = Not bDissable
        txtFeeDescr.Enabled = Not bDissable
        txtThankU.Enabled = Not bDissable
        txtFooter.Enabled = Not bDissable
        txtBankTfrDetail.Enabled = Not bDissable
        Me.btnSave.Visible = Not bDissable
        Me.chkRAF.Enabled = Not bDissable
        Me.btnPreview.Visible = bDissable
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim iReturnvalue As Integer
        Dim trans As SqlTransaction
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        trans = conn.BeginTransaction("FEEProformaTemplate")
        Dim cmd As SqlCommand
        cmd = New SqlCommand("FEES.SAVE_FEEProformaTemplate", conn, trans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpPVM_PAYMENTMODE As New SqlParameter("@PVM_PAYMENTMODE", SqlDbType.VarChar)
        sqlpPVM_PAYMENTMODE.Value = Me.txtPayMode.Content
        cmd.Parameters.Add(sqlpPVM_PAYMENTMODE)

        Dim sqlpPVM_BANKTRANSFERDETAIL As New SqlParameter("@PVM_BANKTRANSFERDETAIL", SqlDbType.VarChar)
        sqlpPVM_BANKTRANSFERDETAIL.Value = Me.txtBankTfrDetail.Content
        cmd.Parameters.Add(sqlpPVM_BANKTRANSFERDETAIL)

        Dim sqlpPVM_PROMOTIONDETAIL As New SqlParameter("@PVM_PROMOTIONDETAIL", SqlDbType.VarChar)
        sqlpPVM_PROMOTIONDETAIL.Value = Me.txtPromotion.Content
        cmd.Parameters.Add(sqlpPVM_PROMOTIONDETAIL)

        Dim sqlpPVM_FEE_AMEND_DESCR As New SqlParameter("@PVM_FEE_AMEND_DESCR", SqlDbType.VarChar)
        sqlpPVM_FEE_AMEND_DESCR.Value = Me.txtFeeDescr.Content
        cmd.Parameters.Add(sqlpPVM_FEE_AMEND_DESCR)

        Dim sqlpPVM_THANKYOU_MSG As New SqlParameter("@PVM_THANKYOU_MSG", SqlDbType.VarChar)
        sqlpPVM_THANKYOU_MSG.Value = Me.txtThankU.Content
        cmd.Parameters.Add(sqlpPVM_THANKYOU_MSG)

        Dim sqlpPVM_FOOTER As New SqlParameter("@PVM_FOOTER", SqlDbType.VarChar)
        sqlpPVM_FOOTER.Value = Me.txtFooter.Content
        cmd.Parameters.Add(sqlpPVM_FOOTER)

        Dim sqlpPVM_bSHOW_REFER_A_FRIEND As New SqlParameter("@PVM_bSHOW_REFER_A_FRIEND", SqlDbType.Bit)
        sqlpPVM_bSHOW_REFER_A_FRIEND.Value = Me.chkRAF.Checked
        cmd.Parameters.Add(sqlpPVM_bSHOW_REFER_A_FRIEND)

        Dim sqlpPVM_LOGUSER As New SqlParameter("@PVM_LOGUSER", SqlDbType.VarChar)
        sqlpPVM_LOGUSER.Value = Session("sUsr_name")
        cmd.Parameters.Add(sqlpPVM_LOGUSER)

        Dim sqlpPVM_BSU_ID As New SqlParameter("@PVM_BSU_ID", SqlDbType.VarChar)
        sqlpPVM_BSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpPVM_BSU_ID)

        Dim sqlpPVM_ID As New SqlParameter("@PVM_ID", SqlDbType.Int)
        sqlpPVM_ID.Value = ViewState("PVM_ID")
        sqlpPVM_ID.Direction = ParameterDirection.InputOutput
        cmd.Parameters.Add(sqlpPVM_ID)

        Dim sqlpPVM_VERSION_NO As New SqlParameter("@PVM_VERSION_NO", SqlDbType.Int)
        sqlpPVM_VERSION_NO.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpPVM_VERSION_NO)

        Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retSValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retSValParam)

        cmd.ExecuteNonQuery()
        iReturnvalue = retSValParam.Value
        ViewState("PVM_ID") = sqlpPVM_ID.Value
        If iReturnvalue <> 0 Then
            trans.Rollback()
            '   lblError.Text = UtilityObj.getErrorMessage(iReturnvalue)
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)

        Else
            trans.Commit()
            '  lblError.Text = "Version No " & sqlpPVM_VERSION_NO.Value & " saved Successfully"
            usrMessageBar.ShowNotification("Version No " & sqlpPVM_VERSION_NO.Value & " saved Successfully", UserControls_usrMessageBar.WarningType.Success)

            GetTemplateDetails(ViewState("PVM_ID"))
            DissableControls(True)
            SetEditingRight(ViewState("PVM_ID"))

            'Me.btnPreview.Visible = True
            ViewState("VERSION_NO") = sqlpPVM_VERSION_NO.Value
            ViewState("datamode") = "view"
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
        End If
    End Sub

    Private Sub ClearDetails()
        Me.lblVsnNo.Text = ""
        txtPromotion.Content = ""
        txtPayMode.Content = ""
        txtFeeDescr.Content = ""
        txtThankU.Content = ""
        txtFooter.Content = ""
        txtBankTfrDetail.Content = ""
        Me.chkRAF.Checked = False
        Me.btnEdit.Enabled = True
        Me.btnPreview.Visible = False
        ViewState("PVM_ID") = 0
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim DummyInvNo As String, DummyBSUID As String
        DummyInvNo = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "select INVOICENO from FEES.VW_PROFORMA_PREVIEW_DUMMY")
        DummyBSUID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "select BSU_ID from FEES.VW_PROFORMA_PREVIEW_DUMMY")
        Session("ReportSource") = PrintReceipt(DummyInvNo, ViewState("VERSION_NO"), False, DummyBSUID, Session("sBsuid"), False, Session("sUsr_name"), False)
        h_print.Value = "print"
    End Sub
    Public Function PrintReceipt(ByVal vINV_NO As String, ByVal vVersionNo As Integer, ByVal bCompanyProforma As Boolean, _
         ByVal BSU_ID As String, ByVal ActualBSU_ID As String, ByVal bEnquiry As Boolean, ByVal USR_NAME As String, Optional ByVal AdvBooking As Boolean = False) As MyReportClass

        Dim repSource As New MyReportClass
        Dim PVM_VER_VER_NO As Int16 = 0
        PVM_VER_VER_NO = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(PVM_VER_VER_NO,1) FROM FEES.PERFORMA_VERSION_M WHERE PVM_ID=" & ViewState("PVM_ID") & "")

        Dim ShowNewPI As Boolean
        Dim sqlStr As String
        sqlStr = "select isnull(BSU_bNewProformaInv,0) from BUSINESSUNIT_SUB where BUS_BSU_ID='" & BSU_ID & "'"
        ShowNewPI = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
        Dim RptNameComp As String = "../../fees/Reports/RPT/rptFeeProformaInvoiceCompany.rpt"

        Dim RptName As String = IIf(PVM_VER_VER_NO = 2, "../../fees/Reports/RPT/rptFeeProformaInvoiceVer_2.rpt", "../../fees/Reports/RPT/rptFeeProformaInvoiceVer_1.rpt")

        Dim str_Sql, strFilter, str_Sql_net As String
        strFilter = "  FPH_INOICENO in ('" & vINV_NO & "') AND  BSU_ID='" & BSU_ID & "' "
        str_Sql_net = "select * from fees.VW_FEE_PERFORMAACCBALANACE  WHERE " & strFilter
        If bEnquiry Then
            str_Sql = "select * from FEES.VW_OSO_FEES_PROFORMA_REPORT_ENQ_VER_1 WHERE " & strFilter
        Else
            str_Sql = "select * from FEES.VW_OSO_FEES_PROFORMA_REPORT_NEW WHERE " & strFilter
        End If

        'Dim cmd As New SqlCommand
        'cmd.CommandText = str_Sql
        'cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet

        Dim cmd As New SqlCommand
        cmd.CommandText = "[FEES].[SP_OSO_FEES_PROFORMA_REPORT_PREVIEW]"
        cmd.CommandType = Data.CommandType.StoredProcedure
        Dim param(3), cmdParam(2) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_ID", ActualBSU_ID, SqlDbType.VarChar)
        param(1) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
        param(2) = Mainclass.CreateSqlParameter("@bEnquiry", bEnquiry, SqlDbType.Bit)
        param(3) = Mainclass.CreateSqlParameter("@PVM_VERSION_NO", vVersionNo, SqlDbType.Int)

        cmd.Connection = New SqlConnection(str_conn)
        cmd.Parameters.Add(param(0))
        cmd.Parameters.Add(param(1))
        cmd.Parameters.Add(param(2))
        cmd.Parameters.Add(param(3))
        ' SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
        cmd.Connection = New SqlConnection(str_conn)

        Dim params As New Hashtable

        params("RPT_CAPTION") = "Proforma Invoice"
        params("UserName") = USR_NAME
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If bCompanyProforma Then
            If AdvBooking = False Then
                Dim repSourceSubRep(2) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                Dim cmdSubEarn As New SqlCommand

                cmdSubEarn.CommandText = str_Sql
                cmdSubEarn.Connection = New SqlConnection(str_conn)
                cmdSubEarn.CommandType = CommandType.Text
                repSourceSubRep(0).Command = cmdSubEarn
                repSource.SubReport = repSourceSubRep

                repSourceSubRep(1) = New MyReportClass
                Dim cmdNetPayable As New SqlCommand
                cmdNetPayable.CommandText = str_Sql_net
                cmdNetPayable.Connection = New SqlConnection(str_conn)
                cmdNetPayable.CommandType = CommandType.Text
                repSourceSubRep(1).Command = cmdNetPayable
                repSource.SubReport = repSourceSubRep

            End If
            repSource.ResourceName = RptNameComp
        Else

            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            Dim cmdNetPayable As New SqlCommand
            cmdNetPayable.CommandText = str_Sql_net
            cmdNetPayable.Connection = New SqlConnection(str_conn)
            cmdNetPayable.CommandType = CommandType.Text
            If AdvBooking = False Then
                repSourceSubRep(0).Command = cmdNetPayable
                repSource.SubReport = repSourceSubRep
            End If
            repSource.ResourceName = RptName
        End If
        '   End If
        'And bEnquiry  = False 
        If (bCompanyProforma = False And (RptName.EndsWith("rptFeeProformaInvoiceVer_1.rpt") Or RptName.EndsWith("rptFeeProformaInvoiceVer_2.rpt"))) Then

            Dim repSourceSubRep(2) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            repSourceSubRep(1) = New MyReportClass
            repSourceSubRep(2) = New MyReportClass
            Dim cmdNetPayable As New SqlCommand
            Dim cmdNetPayable1 As New SqlCommand
            Dim cmdNetPayableSub As New SqlCommand

            cmdNetPayable.CommandText = str_Sql_net
            cmdNetPayable.Connection = New SqlConnection(str_conn)
            cmdNetPayable.CommandType = CommandType.Text


            cmdNetPayable1.CommandText = str_Sql_net
            cmdNetPayable1.Connection = New SqlConnection(str_conn)
            cmdNetPayable1.CommandType = CommandType.Text

            ' cmdNetPayableSub.CommandText = "exec FEES.GEtPerformaDetails 0,'" & BSU_ID & "'," & vINV_NO

            cmdNetPayableSub.CommandText = "[FEES].[GEtPerformaDetails]"
            cmdNetPayableSub.CommandType = Data.CommandType.StoredProcedure
            Dim netPparam(3) As SqlParameter
            netPparam(0) = Mainclass.CreateSqlParameter("@FPH_ID", 0, SqlDbType.Int)
            netPparam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            netPparam(2) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", vINV_NO.Replace("'", ""), SqlDbType.VarChar)
            netPparam(3) = Mainclass.CreateSqlParameter("@ReturnTotal", False, SqlDbType.Bit)


            cmdNetPayableSub.Parameters.Add(netPparam(0))
            cmdNetPayableSub.Parameters.Add(netPparam(1))
            cmdNetPayableSub.Parameters.Add(netPparam(2))
            cmdNetPayableSub.Parameters.Add(netPparam(3))

            cmdNetPayableSub.Connection = New SqlConnection(str_conn)
            If AdvBooking = False Then
                repSourceSubRep(0).Command = cmdNetPayable
                repSource.SubReport = repSourceSubRep

                repSourceSubRep(1).Command = cmdNetPayable1
                repSource.SubReport = repSourceSubRep

                repSourceSubRep(2).Command = cmdNetPayableSub
                repSource.SubReport = repSourceSubRep
            End If
            repSource.ResourceName = RptName
        End If
        Return repSource
    End Function
End Class
