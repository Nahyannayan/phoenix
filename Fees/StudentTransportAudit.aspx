﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentTransportAudit.aspx.vb" Inherits="Fees_StudentTransportAudit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function PrintReceipt() {
            document.getElementById('tr_Print').style.display = 'none';
            window.print();
            document.getElementById('tr_Print').style.display = 'inline';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%">
            <tr valign="top" id="tr_Print">
                <td align="right" colspan="2">
                    <img src="../Images/print.gif" onclick="PrintReceipt();" style="cursor: hand" /></td>
            </tr>
            <tr valign="top">
                <td colspan="2" align="center" style="border-bottom: #6a923a 2pt solid">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" rowspan="4" width="10%">
                                <asp:Image ID="imgLogo" runat="server" Height="70px" />
                            </td>
                            <td align="right" width="60%"></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblHeader" runat="server" Style="font-weight: bold; text-align: center;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="60%"></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lbldate" runat="server" Style="font-weight: bold;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="60%"></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblusername" runat="server" Style="font-weight: bold;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" width="60%">
                                <asp:Label ID="lblSchool" runat="server" Style="font-weight: bold;"></asp:Label>
                            </td>
                             <td align="left" width="30%"></td>
                        </tr>

                    </table>
                </td>

            </tr>
            <tr>
                <td height="20px" colspan="2">&nbsp;</td>
            </tr>

            <tr>
                <td style="vertical-align: top; width: 50%">
                    <asp:DataList ID="dtlStudentInfo" runat="server" RepeatDirection="Vertical" ShowHeader="true" Width="100%">
                        <ItemTemplate>
                            <asp:Label ID="lblLHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LHS")%>'></asp:Label>&nbsp;
                            <asp:Label ID="lblRHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RHS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
                <td style="vertical-align: top; width: 50%">
                    <asp:DataList ID="dtlStudentServiceInfo" runat="server" RepeatDirection="Vertical" ShowHeader="true" Width="100%">
                        <ItemTemplate>
                            <asp:Label ID="lblLHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LHS")%>'></asp:Label>&nbsp;
                            <asp:Label ID="lblRHS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RHS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:DataList></td>
            </tr>
            <tr>
                <td colspan="2" class="title-bg">Transport Service History</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvServiceHistory" runat="server" Width="100%" EmptyDataText="No Services to show" CssClass="table table-bordered table-row"></asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="title-bg">Transport Service Audit</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvTransportAudit" runat="server" Width="100%" EmptyDataText="No audit data to show" CssClass="table table-bordered table-row"></asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="title-bg">Transport Pickup Dropoff Area Audit</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvPickDropAudit" runat="server" Width="100%" EmptyDataText="No audit data to show" CssClass="table table-bordered table-row"></asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
