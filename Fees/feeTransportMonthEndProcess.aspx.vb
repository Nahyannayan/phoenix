Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeRevenueRecognition
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
 

    <System.Web.Services.WebMethod()> _
    Public Shared Sub HitPage()
        Dim client As Net.WebClient = New Net.WebClient()
        client.DownloadData(HttpContext.Current.Request.Url.ToString.Replace("feeRevenueRecognition.aspx", "popup.aspx"))
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                'collect the url of the file to be redirected in view state
                imgStudent.Enabled = False
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvRevReco.Attributes.Add("bordercolor", "#1b80b6")
                gvCharging.Attributes.Add("bordercolor", "#1b80b6")
                gvFOMonthend.Attributes.Add("bordercolor", "#1b80b6")
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ddlBUnit.DataBind()
                ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
                ' FillACD()
                trACD.Visible = False
                ddlBUnit.Enabled = False
                If Session("sUsr_name") = "" _
                Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_REVENUE_RECOGNITION _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_ADJTO_FINANCE _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_DO_MONTHEND _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_POST_CHARGING) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                    trHardClose.Visible = False
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_TRANSPORT_REVENUE_RECOGNITION
                            lblHead.Text = "Revenue Recognition"
                        Case OASISConstants.MNU_FEE_TRANSPORT_POST_CHARGING
                            lblHead.Text = "Post Transport Fee Charging"
                        Case OASISConstants.MNU_FEE_TRANSPORT_ADJTO_FINANCE
                            lblHead.Text = "Post Transport Adjustment To Finance"
                        Case OASISConstants.MNU_FEE_TRANSPORT_DO_MONTHEND
                            lblHead.Text = "Do MonthEnd..."
                            trHardClose.Visible = True
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    setDate()
                    GridbindAll()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'lblError.Text = ""
        H_DOCNO.Value = ""
        lblViewVoucher.Visible = False
        btnPrint.Visible = False
        Dim STATUS As Integer
        Dim STR_NEW_DOCNO As String = ""
        If IsDate(txtDate.Text) = False Then
            'lblError.Text = "Please Enter Valid Date"
            usrMessageBar.ShowNotification("Please Enter Valid Date", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        h_Save.Value = "true"
        Dim transaction As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        transaction = objConn.BeginTransaction("SampleTransaction")
        Try
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_TRANSPORT_REVENUE_RECOGNITION
                    STATUS = FeeTransportDayendProcess.F_POSTFEE_REVENUE(ddlBUnit.SelectedItem.Value, _
                   txtDate.Text, "", STR_NEW_DOCNO, transaction)
                    'txtDate.Text, ddlAcademic.SelectedItem.Value, STR_NEW_DOCNO, transaction)
                    H_DOCNO.Value = STR_NEW_DOCNO
                    If STR_NEW_DOCNO <> "" Then
                        lblViewVoucher.Visible = True
                        btnPrint.Visible = True
                    End If
                Case OASISConstants.MNU_FEE_TRANSPORT_DO_MONTHEND
                    STATUS = FeeTransportDayendProcess.F_DOMONTHEND(ddlBUnit.SelectedItem.Value, _
                    txtDate.Text, "", 1, Session("sUsr_name"), chkHardClose.Checked, objConn, transaction)
                Case OASISConstants.MNU_FEE_TRANSPORT_POST_CHARGING
                    STATUS = FeeTransportDayendProcess.F_POSTFEECHARGE(ddlBUnit.SelectedItem.Value, _
                    txtDate.Text, Session("sUsr_name"), "", STR_NEW_DOCNO, transaction)
                    'txtDate.Text, Session("sUsr_name"), ddlAcademic.SelectedItem.Value, STR_NEW_DOCNO, transaction)
                    H_DOCNO.Value = STR_NEW_DOCNO
                    lblViewVoucher.Visible = True
                    btnPrint.Visible = True
                Case OASISConstants.MNU_FEE_TRANSPORT_ADJTO_FINANCE
                    STATUS = FeeTransportDayendProcess.F_POSTFEESADJUSTMENT_MONTHLY(ddlBUnit.SelectedItem.Value, _
                    txtDate.Text, STR_NEW_DOCNO, transaction)
                    H_DOCNO.Value = STR_NEW_DOCNO
                    lblViewVoucher.Visible = True
                    btnPrint.Visible = True
            End Select
            If STATUS = 0 Then
                transaction.Commit()
                h_STUD_ID.Value = ""
                FillSTUNames(h_STUD_ID.Value)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlBUnit.SelectedItem.Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                If ViewState("MainMnu_code") = OASISConstants.MNU_FEE_ADJTO_FINANCE Then
                    'lblError.Text = "New Document Generated : " & STR_NEW_DOCNO & "<br />Data Successfully Updated"
                    usrMessageBar.ShowNotification("New Document Generated : " & STR_NEW_DOCNO & "<br />Data Successfully Updated", UserControls_usrMessageBar.WarningType.Danger)
                Else
                    'lblError.Text = getErrorMessage(STATUS)
                    usrMessageBar.ShowNotification(getErrorMessage(STATUS), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Else
                transaction.Rollback()
                'lblError.Text = getErrorMessage(STATUS)
                usrMessageBar.ShowNotification(getErrorMessage(STATUS), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            transaction.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message)
        Finally
            h_Save.Value = ""
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        'lblError.Text = ""
        FillACD()
        setDate()
        GridbindAll()
    End Sub

    Protected Sub ddlAcademic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademic.SelectedIndexChanged
        setAcademicyearDate()
    End Sub

    Sub setDate()
        Dim sql_query As String = "SELECT MAX(FTA_TRANDT) FROM FEES.FEE_TRNSALL WHERE FTA_TRANTYPE='MONTHEND' " _
        & "AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "'" ' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        Dim DTFROM As Object = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sql_query)
        If IsDate(DTFROM) Then
            txtDate.Text = Format(DTFROM, OASISConstants.DateFormat)
        ElseIf Not ddlAcademic.DataSource Is Nothing Then
            setAcademicyearDate()
        End If
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBUnit.SelectedItem.Value)
        ddlAcademic.DataSource = dtACD
        ddlAcademic.DataTextField = "ACY_DESCR"
        ddlAcademic.DataValueField = "ACD_ID"
        ddlAcademic.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademic.Items.FindByValue(rowACD("ACD_ID")).Selected = True
            End If
        Next
        setDate()
    End Sub

    Sub GridbindAll()
        Dim sql_query As String = "SELECT MAX(FTA_TRANDT) FROM FEES.FEE_TRNSALL WHERE FTA_TRANTYPE='MONTHEND' " _
        & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "'" ' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        Dim sql_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        sql_query = "SELECT   top(6)   REPLACE(CONVERT(VARCHAR(11), FTA_TRANDT, 113), ' ', '/') as Date  ," _
        & " FTA_DESCRIPTION as 'Desc', OASIS.dbo.fn_GetMonth ( FTA_MONTH) Month,  " _
        & " FTA_REMARKS as Remarks FROM FEES.FEE_TRNSALL" _
        & " WHERE FTA_TRANTYPE='MONTHEND' " _
        & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "'" ' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        gvRevReco.DataSource = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        gvRevReco.DataBind()
        sql_query = "SELECT  top(6)    REPLACE(CONVERT(VARCHAR(11), FTA_TRANDT, 113), ' ', '/') as Date  ," _
              & " FTA_DESCRIPTION as 'Desc', OASIS.dbo.fn_GetMonth ( FTA_MONTH) Month,  " _
              & " FTA_REMARKS as Remarks FROM FEES.FEE_TRNSALL" _
              & " WHERE FTA_TRANTYPE='MONTHLY FEE CHARGE' " _
              & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "'" ' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        gvCharging.DataSource = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        gvCharging.DataBind()
        gvFOMonthend.DataBind()
        sql_query = "SELECT  top(6)  REPLACE(CONVERT(VARCHAR(11), FTA_TRANDT, 113), ' ', '/') as Date  ," _
                     & " FTA_DESCRIPTION as 'Desc', OASIS.dbo.fn_GetMonth ( FTA_MONTH) Month,  " _
                     & " FTA_REMARKS as Remarks FROM FEES.FEE_TRNSALL" _
                     & " WHERE FTA_TRANTYPE='MONTHLY CHARGE POSTING' " _
                     & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "'" ' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        gvPosting.DataSource = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        gvPosting.DataBind()
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim lblBSUID As New Label
        'lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        'If Not lblBSUID Is Nothing Then
        '    h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
        '    If Not FillBSUNames(h_BSUID.Value) Then
        '        h_BSUID.Value = lblBSUID.Text
        '    End If
        '    grdBSU.PageIndex = grdBSU.PageIndex
        '    FillBSUNames(h_BSUID.Value)
        'End If
    End Sub

    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = " SELECT STU_ID , STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvStudentDetails.DataSource = ds
        gvStudentDetails.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub gvStudentDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentDetails.PageIndexChanging
        gvStudentDetails.PageIndex = e.NewPageIndex
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Sub setAcademicyearDate()
        Dim DTFROM As String = ""
        Dim DTTO As String = ""
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademic.SelectedItem.Value, Session("sBsuid"))
        txtDate.Text = DTFROM
    End Sub


    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        'h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        'FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        'Session("ReportSource") = AccountsReports.JournalVouchers(H_DOCNO.Value, String.Format("{0:dd/MMM/yyyy}", CDate(txtDate.Text)), ddlBUnit.SelectedItem.Value, "", H_DOCNO.Value, "JV")
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)

    End Sub

    Protected Sub chkStudentby_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkStudentby.CheckedChanged
        If chkStudentby.Checked Then
            imgStudent.Enabled = True
        Else
            imgStudent.Enabled = False
            h_STUD_ID.Value = ""
            FillSTUNames(h_STUD_ID.Value)
        End If

    End Sub



    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePageMethods = True
    End Sub
End Class
