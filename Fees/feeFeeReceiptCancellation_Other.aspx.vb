﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeFeeReceiptCancellation_Other
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            trstudDet.Visible = False
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            ViewState("ID") = 1
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F300223" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub

    Function ValidateRecieptNo(ByVal recNo As String, ByVal vBSUID As String) As Boolean
        Dim CommandText As String = "SELECT COUNT(FOC_ID) FROM FEES.FEEOTHCOLLECTION_H WITH(NOLOCK) WHERE FOC_BSU_ID = '" & vBSUID & "' AND ISNULL(FOC_bDELETED,0)=0 AND FOC_RECNO='" & recNo & "'"
        Dim vCount As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText)
        If vCount > 0 Then
            Return True
        Else
            Return False
        End If
        Return False
    End Function 

    Private Function ChechErrors() As Boolean
        Try
            If CDate(txtDate.Text) > Date.Now Then
                ' lblError.Text = "Future Date is not allowed"
                usrMessageBar.ShowNotification("Future Date is not allowed", UserControls_usrMessageBar.WarningType.Danger)
                Return False
            End If
        Catch
            ' lblError.Text = "Date is not valid"
            usrMessageBar.ShowNotification("Date is not valid", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End Try
        Return True
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            ' lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not ChechErrors() Then Return
        If Not ValidateRecieptNo(txtRecieptNo.Text, ddBusinessunit.SelectedValue) Then
            ' lblError.Text = "Receipt No is not Valid..."
            usrMessageBar.ShowNotification("Receipt No is not Valid...", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)

        'Dim trans As SqlTransaction
        'conn.Open()
        'trans = conn.BeginTransaction("DeleteReceipt_TRANS")
        'Try
        '    Dim cmd As New SqlCommand("FEES.DeleteReceipt_Other", conn, trans)
        '    cmd.CommandType = CommandType.StoredProcedure

        '    Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
        '    sqlpAUD_BSU_ID.Value = ddBusinessunit.SelectedValue
        '    cmd.Parameters.Add(sqlpAUD_BSU_ID)

        '    Dim sqlpFCL_RECNO As New SqlParameter("@FOC_RECNO", SqlDbType.VarChar, 20)
        '    sqlpFCL_RECNO.Value = txtRecieptNo.Text
        '    cmd.Parameters.Add(sqlpFCL_RECNO)

        '    Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
        '    sqlpAUD_USER.Value = Session("sUsr_name")
        '    cmd.Parameters.Add(sqlpAUD_USER)

        '    Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
        '    sqlpAUD_REMARKS.Value = txtRemarks.Text
        '    cmd.Parameters.Add(sqlpAUD_REMARKS)

        '    Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
        '    sqlpFromDT.Value = CDate(txtDate.Text)
        '    cmd.Parameters.Add(sqlpFromDT)

        '    Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        '    retSValParam.Direction = ParameterDirection.ReturnValue
        '    cmd.Parameters.Add(retSValParam)

        '    cmd.ExecuteNonQuery()
        '    Dim iReturnvalue As Integer = retSValParam.Value
        '    If iReturnvalue <> 0 Then
        '        trans.Rollback()
        '        lblError.Text = "Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue)
        '    Else
        '        trans.Commit()
        '        lblError.Text = "Receipt deleted Successfully.."
        '        ClearAll()
        '    End If
        'Catch ex As Exception
        '    lblError.Text = "Error occured while deleting  " & ex.Message
        '    trans.Rollback()
        'Finally
        '    If conn.State = ConnectionState.Open Then
        '        conn.Close()
        '    End If
        'End Try
        Try
            SentForApproval()

        Catch ex As Exception
            '  lblError.Text = "Error while sending for Approval.. " & ex.Message
            usrMessageBar.ShowNotification("Error while sending for Approval.. " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub SentForApproval()
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim trans As SqlTransaction
        Try
            conn.Open()
            'trans = conn.BeginTransaction("DeleteReceipt_TRANS")

            Dim cmd As New SqlCommand("FEES.DeleteReceipt_OASIS_Approval", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
            sqlpAUD_BSU_ID.Value = ddBusinessunit.SelectedValue
            cmd.Parameters.Add(sqlpAUD_BSU_ID)

            Dim sqlpFCL_RECNO As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            sqlpFCL_RECNO.Value = txtRecieptNo.Text
            cmd.Parameters.Add(sqlpFCL_RECNO)


            Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
            sqlpAUD_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpAUD_USER)

            Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
            sqlpAUD_REMARKS.Value = txtRemarks.Text
            cmd.Parameters.Add(sqlpAUD_REMARKS)

            Dim sqlpAUD_TYPE As New SqlParameter("@FRCA_TYPE", SqlDbType.VarChar, 10)
            sqlpAUD_TYPE.Value = "OTH"
            cmd.Parameters.Add(sqlpAUD_TYPE)

            Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
            sqlpFromDT.Value = CDate(txtDate.Text)
            cmd.Parameters.Add(sqlpFromDT)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then
                ' lblError.Text = "Error occured while sending for Approval : " & UtilityObj.getErrorMessage(iReturnvalue)
                usrMessageBar.ShowNotification("Error occured while sending for Approval : " & UtilityObj.getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
            Else
                '  lblError.Text = "Request sent for approval and is valid only for today ."
                usrMessageBar.ShowNotification("Request sent for approval and is valid only for today .", UserControls_usrMessageBar.WarningType.Success)
                ClearAll()
            End If
        Catch ex As Exception
            ' lblError.Text = "Error occured while sending for Approval  " & ex.Message
            usrMessageBar.ShowNotification("Error occured while sending for Approval  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            'trans.Rollback()
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

    End Sub

    Private Sub ClearAll()
        lblAmount.Text = "" 
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtRecieptNo.Text = ""
        txtRemarks.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        If txtRecieptNo.Text <> "" Then
            Dim CommandText As String = "SELECT ISNULL(FOC_AMOUNT,0)FOC_AMOUNT,ISNULL(FOC_bINVOICE_PRINTED,0)FOC_bINVOICE_PRINTED,ISNULL(FOC_INVOICE_NO,'')FOC_INVOICE_NO " & _
                                        "FROM FEES.FEEOTHCOLLECTION_H WITH(NOLOCK) WHERE FOC_BSU_ID = '" & ddBusinessunit.SelectedValue & "' AND ISNULL(FOC_bDELETED,0)=0 AND FOC_RECNO='" & txtRecieptNo.Text.Trim & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                trstudDet.Visible = True
                lblAmount.Text = ds.Tables(0).Rows(0)(0)
                If Convert.ToBoolean(ds.Tables(0).Rows(0)("FOC_bINVOICE_PRINTED")) = True Then
                    trAlert.Visible = True
                    lblAlertMessage.Text = getErrorMessage("643").Replace("$$$", ds.Tables(0).Rows(0)("FOC_INVOICE_NO"))
                End If
            End If
        End If
    End Sub

    Protected Sub imgDocno_Click(sender As Object, e As ImageClickEventArgs) Handles imgDocno.Click
        If txtRecieptNo.Text <> "" Then
            Dim CommandText As String = "SELECT ISNULL(FOC_AMOUNT,0)FOC_AMOUNT,ISNULL(FOC_bINVOICE_PRINTED,0)FOC_bINVOICE_PRINTED,ISNULL(FOC_INVOICE_NO,'')FOC_INVOICE_NO " & _
                                        "FROM FEES.FEEOTHCOLLECTION_H WITH(NOLOCK) WHERE FOC_BSU_ID = '" & ddBusinessunit.SelectedValue & "' AND ISNULL(FOC_bDELETED,0)=0 AND FOC_RECNO='" & txtRecieptNo.Text.Trim & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblAmount.Text = ds.Tables(0).Rows(0)(0)
                trstudDet.Visible = True
                If Convert.ToBoolean(ds.Tables(0).Rows(0)("FOC_bINVOICE_PRINTED")) = True Then
                    trAlert.Visible = True
                    lblAlertMessage.Text = getErrorMessage("643").Replace("$$$", ds.Tables(0).Rows(0)("FOC_INVOICE_NO"))
                End If
            End If
        End If
    End Sub
End Class
