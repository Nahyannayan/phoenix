Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeCollectionProFormaInvoice
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim dsStudent, dsFee, dsStudentFee, dtStudent As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_Chequeid.Value <> "" Then
            SetChequeDetails(h_Chequeid.Value)
        End If

        'Hide Enrollment html row
        Me.EnrollmentRow.Visible = False
        'Hide Student html row
        Me.StudentRow.Visible = False
        'Hide Academic html row
        Me.AcademicYearRow.Visible = False

        If Page.IsPostBack = False Then
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            'some.Visible = False
            Dim str_disableSavebutton As String = ("javascript:" & btnSave.ClientID & ".disabled=true;") + ClientScript.GetPostBackEventReference(btnSave, "").ToString() & ";"
            'Dim str_scriptWhethercobrand As String = "if ( (parseFloat(document.getElementById('" & txtCCTotal.ClientID & "').value)>0 ) && document.getElementById('" & ddCreditcard.ClientID & "').options[document.getElementById('" & ddCreditcard.ClientID & "').selectedIndex].text.search(/Co-Brand/)>0)"

            'Dim str_savescript As String = "javascript:" & str_scriptWhethercobrand & "{ if (confirm('NBAD Co-brand card Selected. Do you want to continue?')==1) { " & str_disableSavebutton & " } else return false; } else  { " & str_disableSavebutton & " }"
            'btnSave.Attributes.Add("onclick", str_savescript)

            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            InitialiseCompnents()
            BindEmirate()
            If Session("sUsr_name") = "" Or Session("sBSuid") = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_COLLECTION Then
                If Not Request.UrlReferrer Is Nothing Then
                    'Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBSuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            gvFeeCollection.DataBind()
            ' SetDiscountData_BSU()
            Set_COBrand_Payment_controls()
        End If
    End Sub

    Sub BindEmirate()
        Try
            Dim str_default_emirate As String = GetDataFromSQL("SELECT BSU_CITY FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "'", _
            ConnectionManger.GetOASISConnectionString)
            If str_default_emirate <> "--" Then
                ddlEmirate1.SelectedIndex = -1
                ddlEmirate2.SelectedIndex = -1
                ddlEmirate3.SelectedIndex = -1
                ddlEmirate1.Items.FindByValue(str_default_emirate).Selected = True
                ddlEmirate2.Items.FindByValue(str_default_emirate).Selected = True
                ddlEmirate3.Items.FindByValue(str_default_emirate).Selected = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "emirate")
        End Try
    End Sub

    Sub InitialiseCompnents()
        txtTotal.Attributes.Add("readonly", "readonly")
        'txtTotalDiscount.Attributes.Add("readonly", "readonly")
        txtReceivedTotal.Attributes.Add("readonly", "readonly")
        txtBalance.Attributes.Add("readonly", "readonly")
        txtDue.Attributes.Add("readonly", "readonly")
        txtOutstanding.Attributes.Add("readonly", "readonly")

        gvFeeCollection.Attributes.Add("bordercolor", "#1b80b6")
        gvDiscount.Attributes.Add("bordercolor", "#fc7f03")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqDate1.Text = txtFrom.Text
        txtChqDate2.Text = txtFrom.Text
        txtChqDate3.Text = txtFrom.Text
        Dim formatstring As String = Session("BSU_DataFormatString")
        formatstring = formatstring.Replace("0.", "###,###,###,##0.")
        For columnIndex As Integer = 3 To 8
            DirectCast(gvFeeCollection.Columns(columnIndex), BoundField).DataFormatString = "{0:" & formatstring & "}"
        Next

        FillACD()
        FillFeeType()
        SetNarration()

        ddlEmirate1.DataBind()
        ddlEmirate2.DataBind()
        ddlEmirate3.DataBind()
        UsrSelStudent1.IsStudent = True
        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
        lnkTransportfee.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "feeTranspotCollectionfromFee.aspx") & "&stuno=" & UsrSelStudent1.STUDENT_NO & "&acdid=" & ddlAcademicYear.SelectedItem.Value & "');return false;"
        Set_Provider()
    End Sub

    Sub Set_Provider()
        Dim str_PROVIDER_BSU_ID As String = UtilityObj.GetDataFromSQL("SELECT SVB_PROVIDER_BSU_ID FROM OASIS.dbo.SERVICES_BSU_M WITH(NOLOCK) " & _
        " WHERE SVB_SVC_ID = 1 AND SVB_BSU_ID = '" & Session("sBsuid") & "' AND SVB_ACD_ID = '" & ddlAcademicYear.SelectedItem.Value & "'", _
        ConnectionManger.GetOASISConnectionString)
        If str_PROVIDER_BSU_ID <> "--" Then
            Session("PROVIDER_BSU_ID") = str_PROVIDER_BSU_ID
        Else
            Session("PROVIDER_BSU_ID") = ""
        End If
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBsuId"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Sub FillFeeType()
        Dim dtFeeType As DataTable = FeeCommon.getFeeTypes(Session("sBsuId"), Int32.Parse(ddlAcademicYear.SelectedValue.ToString()))
        ddFeetype.DataSource = dtFeeType
        ddFeetype.DataTextField = "FEE_DESCR"
        ddFeetype.DataValueField = "FEE_ID"
        ddFeetype.DataBind()
    End Sub

    Sub clear_All()
        'txtCCTotal.Text = ""
        txtChequeTotal1.Text = ""
        'txtCashTotal.Text = ""
        txtTotal.Text = ""
        txtReceivedTotal.Text = ""
        h_hideBank.Value = ""
        txtBankTotal.Text = ""
        labGrade.Text = ""
        If Not chkRemember.Checked Then
            txtCreditno.Text = ""
            txtBank1.Text = ""
            txtBank2.Text = ""
            txtBank3.Text = ""
            h_Bank1.Value = ""
            h_Bank2.Value = ""
            h_Bank3.Value = ""
            txtChqno1.Text = ""
            txtChqno2.Text = ""
            txtChqno3.Text = ""
            txtChqDate1.Text = txtFrom.Text
            txtChqDate2.Text = txtFrom.Text
            txtChqDate3.Text = txtFrom.Text
        End If
        txtChqno1.Attributes.Remove("readonly")
        h_Chequeid.Value = ""
        txtBalance.Text = ""
        txtChequeTotal1.Text = ""
        txtChequeTotal2.Text = ""
        txtChequeTotal3.Text = ""
        txtRemarks.Text = ""
        txtAmountAdd.Text = ""
        txtDue.Text = ""
        txtOutstanding.Text = ""
        ' txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        SetNarration()
        chkRemember.Checked = False
        ClearStudentData()
    End Sub

    Private Sub Save()
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        'Set_GridTotal(True)
        'Set_Total()
        Dim str_error As String = check_errors_details()
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        'If UsrSelStudent1.STUDENT_NO = "" Then
        '    str_error = str_error & "Please select student <br />"
        'End If

        If Not IsNumeric(txtBalance.Text) Then
            txtBalance.Text = "0"
        End If
        If grvStudent.Rows.Count = 0 Then
            str_error = str_error & "Please add student details<br />"
        End If

        'If IsNumeric(txtCCTotal.Text) AndAlso CDbl(txtCCTotal.Text > 0) Then
        '    Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S", ConnectionManger.GetOASISFINConnectionString)
        '    If ddCreditcard.SelectedItem.Value = str_CRR_ID And Not chkCoBrand.Checked Then
        '        str_error = str_error & "Please select co-brand check box...<br />"
        '    End If
        'End If

        Dim dblReceivedAmount, dblTotal, dblCash, dblBalance As Decimal
        dblReceivedAmount = CDbl(txtReceivedTotal.Text)
        dblTotal = CDbl(txtTotal.Text)
        'dblCash = CDbl(txtCashTotal.Text)
        If dblReceivedAmount <> dblTotal Then
            If dblCash > 0 Then
                If dblTotal < dblReceivedAmount Then
                    dblBalance = dblReceivedAmount - dblTotal
                    If dblBalance > dblCash Then
                        str_error = str_error & "Cannot Refund!!!<br />"
                        txtReceivedTotal.Text = 0
                    Else
                        txtBalance.Text = Format(dblBalance, Session("BSU_DataFormatString"))
                    End If
                Else
                    str_error = str_error & "Totals not tallying<br />"
                End If
            Else
                txtBalance.Text = Format(CDbl(txtBalance.Text), Session("BSU_DataFormatString"))
                str_error = str_error & "Totals not tallying<br />"
            End If
        End If
        If dblTotal <= 0 Then
            str_error = str_error & "Invalid Amount!!!<br />"
        End If
        dblBalance = CDbl(txtBalance.Text)
        'If txtRemarks.Text = "" Then
        '    str_error = str_error & "Please enter remarks<br />"
        'End If
        If str_error <> "" Then
            'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim str_new_FCL_ID As Long
        Dim str_NEW_FCL_RECNO As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim STR_TYPE As Char = "S"

            If rbEnquiry.Checked Then
                STR_TYPE = "E"
            End If

            Dim InvoiceIds, StudentIds As String
            'For i As Integer = 0 To Me.grvStudent.Rows.Count - 1
            '    InvoiceIds &= CType(Me.grvStudent.Rows(i).FindControl("lblInvoiceId"), Label).Text & "|"
            '    StudentIds &= CType(Me.grvStudent.Rows(i).FindControl("lblStudentId"), Label).Text & "|"
            'Next

            For Each row As DataRow In CType(Session("dsStudent"), DataSet).Tables(0).Rows
                InvoiceIds &= row.Item("InvoiceId") & "|"
                StudentIds &= row.Item("StudentId") & "|"
            Next

            InvoiceIds = InvoiceIds.Remove(InvoiceIds.Length - 1)
            StudentIds = StudentIds.Remove(StudentIds.Length - 1)

            'First insert the data into FEES.GroupProformaPayment table
            Dim param(9) As SqlParameter
            param(0) = New SqlParameter("@InvoiceIds", SqlDbType.VarChar)
            param(0).Value = InvoiceIds
            param(1) = New SqlParameter("@StudentIds", SqlDbType.VarChar)
            param(1).Value = StudentIds
            param(2) = New SqlParameter("@FPP_Bank_ID", SqlDbType.Int)
            param(2).Value = Me.h_Bank1.Value
            param(3) = New SqlParameter("@FPP_Emirate_ID", SqlDbType.VarChar)
            param(3).Value = Me.ddlEmirate1.SelectedItem.Value
            param(4) = New SqlParameter("@FPP_CHQ_No", SqlDbType.VarChar)
            param(4).Value = Me.txtChqno1.Text
            param(5) = New SqlParameter("@FPP_CHQ_Amount", SqlDbType.Decimal)
            param(5).Value = CDbl(Me.txtChequeTotal1.Text)
            param(6) = New SqlParameter("@FPP_CHQ_Date", SqlDbType.VarChar)
            param(6).Value = Me.txtChqDate1.Text
            param(7) = New SqlParameter("@FPP_LOG_User", SqlDbType.VarChar)
            param(7).Value = Session("sUsr_name")
            param(8) = New SqlParameter("@FPP_Narration", SqlDbType.VarChar)
            param(8).Value = Me.txtRemarks.Text
            param(9) = New SqlParameter("@FPP_ACT_ID", SqlDbType.VarChar)
            param(9).Value = hfBankAct1.Value
            
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.SaveProformaPayment", param)

            If retval = "0" Then
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            stTrans.Commit()
            ViewState("recno") = Nothing
           
            grvStudent.DataSource = Nothing
            grvStudent.DataBind()
            grvFee.DataSource = Nothing
            grvFee.DataBind()
            Session("SkippedStudents") = Nothing

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FCL_RECNO, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
            '   Session("ReportSource") = FeeCollection.PrintReceipt(str_NEW_FCL_RECNO, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), True)
            'lblError.Text = getErrorMessage("0")
            usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
            clear_All()
            'Gridbind_Feedetails()
        Catch ex As Exception

            stTrans.Rollback()
            'lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Private Function GetNarration(ByVal BsuId As String, ByVal InvoiceNo As String) As String
        GetNarration = ""
        If InvoiceNo = Nothing Then Return Nothing
        Try
            Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, "[FeeCollection_GetProFormaInvoiceNarration]", Mainclass.CreateSqlParameter("@BSU_ID", BsuId, SqlDbType.VarChar), Mainclass.CreateSqlParameter("@InvoiceNo", Me.txtInvoice.Text, SqlDbType.VarChar))
        Catch ex As Exception
            'Me.lblError.Text = "Unable to get the narration"
            usrMessageBar.ShowNotification("Unable to get the narration", UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.Save()
        Exit Sub
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
        '    chkCoBrand.Checked = False
        '    Set_COBrand_Payment_controls()
        '    clear_All()
        '    ViewState("datamode") = "view"
        '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'Else
        '    Response.Redirect(ViewState("ReferrerUrl"))
        'End If
        Session("SkippedStudents") = Nothing
        Me.clear_All()
        Me.grvStudent.DataSource = Nothing
        Me.grvFee.DataSource = Nothing
        Me.dsStudent.Clear()
        Me.dsFee.Clear()
        Me.dsStudentFee.Clear()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub setStudntData()
        Gridbind_Feedetails()
    End Sub

    Sub Gridbind_Feedetails()
        gvFeeCollection.DataSource = Nothing
        gvFeeCollection.DataBind()
        If IsDate(txtFrom.Text) And UsrSelStudent1.STUDENT_ID <> "" Then
            Dim dt As New DataTable
            If rbEnquiry.Checked Then
                dt = FeeCollection.F_GetFeeDetailsFOrCollection(txtFrom.Text, _
                UsrSelStudent1.STUDENT_ID, "E", Session("sBsuid"), ddlAcademicYear.SelectedItem.Value, False)
            Else
                dt = FeeCollection.F_GetFeeDetailsFOrCollection(txtFrom.Text, _
                UsrSelStudent1.STUDENT_ID, "S", Session("sBsuid"), ddlAcademicYear.SelectedItem.Value, False)
            End If
            gvFeeCollection.DataSource = dt
            Session("FeeCollection") = dt
            gvFeeCollection.DataBind()
        End If
        Set_GridTotal(False)
    End Sub

    Sub Set_GridTotal(ByVal DonotCalulateDisc As Boolean)
        Dim dAmount As Decimal = 0
        Dim dDueAmount As Decimal = 0
        Dim dDiscountAmount As Decimal = 0
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
            Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
            Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
            Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
            Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
            Dim txtDiscount As TextBox = CType(gvr.FindControl("txtDiscount"), TextBox)
            If Not txtAmountToPay Is Nothing Then
                If IsNumeric(lblAmount.Text) Then
                    dDueAmount = dDueAmount + CDbl(lblAmount.Text)
                End If
                If IsNumeric(txtAmountToPay.Text) Then
                    dAmount = dAmount + CDbl(txtAmountToPay.Text)
                Else
                    txtAmountToPay.Text = Session("BSU_DataFormatString")
                End If

                If gvr.RowIndex < Session("FeeCollection").rows.count Then
                    Session("FeeCollection").rows(gvr.RowIndex)(("Amount")) = txtAmountToPay.Text
                    If gvFeeCollection.Columns(10).Visible Then 'Manual discount
                        If IsNumeric(txtDiscount.Text) Then
                            Session("FeeCollection").rows(gvr.RowIndex)(("Discount")) = txtDiscount.Text
                        Else
                            txtDiscount.Text = "0"
                            Session("FeeCollection").rows(gvr.RowIndex)(("Discount")) = 0
                        End If
                    Else
                        Session("FeeCollection").rows(gvr.RowIndex)(("Discount")) = lblDiscount.Text
                    End If
                End If
                If Session("FeeCollection").rows(gvr.RowIndex)(("AUTOYN")) = 1 Then
                    ChkPost.Checked = True
                End If
                If gvFeeCollection.Columns(9).Visible And lblFSR_FEE_ID.Text.Trim = "5" And lnkDiscount.Visible And Not lbCancel.Visible Then
                    If DonotCalulateDisc = False Then
                        Dim decDiscountamt As Decimal = 0
                        If rbEnrollment.Checked Then
                            FeeCollection.DIS_FindDiscount(UsrSelStudent1.STUDENT_ID, "S", txtFrom.Text, Session("sBsuid"), "", txtAmountToPay.Text, decDiscountamt)
                        Else
                            FeeCollection.DIS_FindDiscount(UsrSelStudent1.STUDENT_ID, "E", txtFrom.Text, Session("sBsuid"), "", txtAmountToPay.Text, decDiscountamt)
                        End If
                        lblDiscount.Text = Format(decDiscountamt, Session("BSU_DataFormatString"))
                        'txtAmountToPay.Text = Convert.ToDouble(txtAmountToPay.Text) - Convert.ToDouble(lblDiscount.Text)
                    End If
                End If
                If gvFeeCollection.Columns(9).Visible And lblFSR_FEE_ID.Text.Trim = "5" And Not lbCancel.Visible Then
                    'chkCoBrand.Checked And
                    If DonotCalulateDisc = False Then

                        Dim decDiscountamt As Decimal = 0
                        If rbEnrollment.Checked Then
                            If chkCoBrand.Checked Then
                                GetDiscountedFeesPayable_COBRAND(UsrSelStudent1.STUDENT_ID, "S", ddCreditcard.SelectedItem.Value, _
                                                           COLLECTIONTYPE.CREDIT_CARD, lblFSR_FEE_ID.Text, txtAmountToPay.Text, decDiscountamt)
                            Else
                                GetDiscountedFeesPayable_COBRAND(UsrSelStudent1.STUDENT_ID, "S", 0, _
                                                           0, lblFSR_FEE_ID.Text, txtAmountToPay.Text, decDiscountamt)
                            End If

                            'Else
                            '    FeeCollection.DIS_FindDiscount(UsrSelStudent1.STUDENT_ID, "E", txtFrom.Text, Session("sBsuid"), "", txtAmountToPay.Text, decDiscountamt)
                        End If
                        If decDiscountamt <> 0 Then
                            txtAmountToPay.Attributes.Add("readonly", "readonly")
                            lbCancel.Visible = True
                        End If
                        lblDiscount.Text = Format(decDiscountamt, Session("BSU_DataFormatString"))
                        txtAmountToPay.Text = Convert.ToDouble(txtAmountToPay.Text) - Convert.ToDouble(lblDiscount.Text)
                        dAmount = dAmount - Convert.ToDouble(lblDiscount.Text)
                    End If
                End If
            End If
            If gvFeeCollection.Columns(9).Visible And IsNumeric(lblDiscount.Text) Then
                dDiscountAmount = dDiscountAmount + CDbl(lblDiscount.Text)
            Else
                lblDiscount.Text = Session("BSU_DataFormatString")
            End If
            If gvFeeCollection.Columns(10).Visible And IsNumeric(txtDiscount.Text) Then
                dDiscountAmount = dDiscountAmount + CDbl(txtDiscount.Text)
            Else
                txtDiscount.Text = Session("BSU_DataFormatString")
            End If
        Next
        txtDue.Text = Format(dDueAmount - dAmount, Session("BSU_DataFormatString"))
        txtOutstanding.Text = Format(dDueAmount, Session("BSU_DataFormatString"))
        txtTotal.Text = Format(dAmount, Session("BSU_DataFormatString"))
        'If chkCoBrand.Checked Then
        '    txtCCTotal.Text = txtTotal.Text
        'End If
        lblTotalDiscount.Text = "Discount : " & Format(dDiscountAmount, Session("BSU_DataFormatString"))
    End Sub

    Function check_errors_details() As String
        Dim str_error As String = ""
        
        If txtChequeTotal1.Text = "" Then
            txtChequeTotal1.Text = 0
        End If
        'If txtChequeTotal2.Text = "" Then
        '    txtChequeTotal2.Text = 0
        'End If
        'If txtChequeTotal3.Text = "" Then
        '    txtChequeTotal3.Text = 0
        'End If
        
        If Not IsNumeric(txtChequeTotal1.Text) Then
            str_error = str_error & "Invalid Cheque amount 1  <br />"
        Else
            If CDbl(txtChequeTotal1.Text) > 0 Then
                If Not IsDate(txtChqDate1.Text) Then
                    str_error = str_error & "Invalid Cheque date 1  <br />"
                End If
                If txtChqno1.Text = "" Then
                    str_error = str_error & "Invalid Cheque no # 1  <br />"
                End If
                If h_Bank1.Value = "" Or txtBank1.Text = "" Then
                    str_error = str_error & "Invalid Bank for Cheque # 1  <br />"
                End If
            End If
        End If

        If IsNumeric(txtChequeTotal2.Text) And Val(txtChequeTotal2.Text) > 0 Then
            str_error = str_error & "Only 1 cheque is allowed. Kindly remove the other cheques.  <br />"
            Return str_error
        Else
            'If CDbl(txtChequeTotal2.Text) > 0 Then
            '    If Not IsDate(txtChqDate2.Text) Then
            '        str_error = str_error & "Invalid Cheque date 2  <br />"
            '    End If
            '    If txtChqno2.Text = "" Then
            '        str_error = str_error & "Invalid Cheque no # 2  <br />"
            '    End If
            '    If h_Bank2.Value = "" Or txtBank2.Text = "" Then
            '        str_error = str_error & "Invalid Bank for Cheque # 2  <br />"
            '    End If
            '    'If h_Emirate2.Value = "" Or txtEmirate2.Text = "" Then
            '    '    str_error = str_error & "Invalid emirate for Cheque # 2  <br />"
            '    'End If
            'End If
        End If

        If IsNumeric(txtChequeTotal3.Text) And Val(txtChequeTotal3.Text) > 0 Then
            str_error = str_error & "Only 1 cheque is allowed. Kindly remove the other cheques.  <br />"
            Return str_error
        Else
            'If CDbl(txtChequeTotal3.Text) > 0 Then
            '    If Not IsDate(txtChqDate3.Text) Then
            '        str_error = str_error & "Invalid Cheque date 3  <br />"
            '    End If
            '    If txtChqno3.Text = "" Then
            '        str_error = str_error & "Invalid Cheque no # 3  <br />"
            '    End If
            '    If h_Bank3.Value = "" Or txtBank3.Text = "" Then
            '        str_error = str_error & "Invalid Bank for Cheque # 3  <br />"
            '    End If
            '    'If h_Emirate3.Value = "" Or txtEmirate3.Text = "" Then
            '    '    str_error = str_error & "Invalid emirate for Cheque # 3  <br />"
            '    'End If
            'End If
        End If
        Return str_error
    End Function

    Sub Set_Total()
        Dim str_error As String = check_errors_details()
        If str_error = "" Then
            Dim dblReceivedAmount, dblTotal, dblCash As Decimal
            'dblReceivedAmount = CDbl(txtCCTotal.Text) + CDbl(txtCashTotal.Text) + CDbl(txtChequeTotal1.Text) + CDbl(txtChequeTotal2.Text) + CDbl(txtChequeTotal3.Text)
            dblReceivedAmount = CDbl(txtChequeTotal1.Text) + CDbl(txtChequeTotal2.Text) + CDbl(txtChequeTotal3.Text)
            dblTotal = CDbl(txtTotal.Text)
            txtReceivedTotal.Text = dblReceivedAmount
            txtReceivedTotal.Text = Format(CDbl(txtReceivedTotal.Text), Session("BSU_DataFormatString"))
            'txtCCTotal.Text = Format(CDbl(txtCCTotal.Text), Session("BSU_DataFormatString"))
            'dblCash = CDbl(txtCashTotal.Text)
            'txtCashTotal.Text = Format(dblCash, Session("BSU_DataFormatString"))
            txtChequeTotal1.Text = Format(CDbl(txtChequeTotal1.Text), Session("BSU_DataFormatString"))
            txtChequeTotal2.Text = Format(CDbl(txtChequeTotal2.Text), Session("BSU_DataFormatString"))
            txtChequeTotal3.Text = Format(CDbl(txtChequeTotal3.Text), Session("BSU_DataFormatString"))
            txtBalance.Text = 0
            If dblCash > 0 Then
                If dblTotal < dblReceivedAmount Then
                    Dim dblBalance As Decimal = dblReceivedAmount - dblTotal
                    If dblBalance > dblCash Then
                        'lblError.Text = "Cannot Refund!!!(Excess amount)"
                        usrMessageBar.ShowNotification("Cannot Refund!!!(Excess amount)", UserControls_usrMessageBar.WarningType.Danger)
                        txtReceivedTotal.Text = 0
                    Else
                        txtBalance.Text = Format(dblBalance, Session("BSU_DataFormatString"))
                    End If
                End If
            Else
                txtBalance.Text = Format(CDbl(txtBalance.Text), Session("BSU_DataFormatString"))
            End If
        Else
            'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            txtReceivedTotal.Text = 0
        End If
    End Sub

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal(False)
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.SetFocus(sender)
        If sender.Parent.parent.rowindex() = gvFeeCollection.Rows.Count - 1 Then
            smScriptManager.SetFocus(txtTotal)
        Else
            smScriptManager.SetFocus(gvFeeCollection.Rows(sender.Parent.parent.rowindex + 1).FindControl("txtAmountToPay"))
        End If
    End Sub

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
        ClearStudentData()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
        ClearStudentData()
    End Sub

    Sub ClearStudentData()
        UsrSelStudent1.IsStudent = rbEnrollment.Checked
        h_Student_no.Value = ""
        UsrSelStudent1.ClearDetails()
        Gridbind_Feedetails()
        SetDiscountData_Student()
        lbLDAdate.Text = ""
        lblStuStatus.Text = ""
        lbStuDOJ.Text = ""
        lbTCdate.Text = ""
        labGrade.Text = ""
    End Sub

    Protected Sub btnAddDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetails.Click
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtAmountAdd.Text) Then
            If CDbl(txtAmountAdd.Text) > 0 Then
                'FEE_ID, FEE_DESCR, Opening, CurrentCharge, Net,Amount
                'FSH_ID, FSH_DATE, FEE_ID, FEE_DESCR, AMOUNT, FSH_naRRATION
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)

                    If Not lblFSR_FEE_ID Is Nothing Then
                        If lblFSR_FEE_ID.Text = ddFeetype.SelectedItem.Value Then
                            'lblError.Text = "Fee repeating!!!"
                            usrMessageBar.ShowNotification("Fee repeating!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    End If
                Next
                dr = Session("FeeCollection").NewRow
                dr("FEE_ID") = ddFeetype.SelectedItem.Value
                dr("FEE_DESCR") = ddFeetype.SelectedItem.Text
                dr("CLOSING") = 0
                dr("Amount") = txtAmountAdd.Text
                dr("AUTOYN") = "0"
                dr("Discount") = "0"
                If ChkChgPost.Checked Then
                    dr("AUTOYN") = "1"
                    dr("CLOSING") = txtAmountAdd.Text
                End If
                Session("FeeCollection").rows.add(dr)
                gvFeeCollection.DataSource = Session("FeeCollection")
                txtAmountAdd.Text = ""
                gvFeeCollection.DataBind()
                Set_GridTotal(False)
                'smScriptManager.SetFocus(txtCashTotal)
            Else
                'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtAmountAdd)
            End If
        Else
            'lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtAmountAdd)
        End If
    End Sub

    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
        Try
            chkCoBrand.Checked = False
            Set_COBrand_Payment_controls()

            h_Student_no.Value = UsrSelStudent1.STUDENT_ID
            labGrade.Text = "Grade : " & UsrSelStudent1.GRM_DISPLAY
            lblStuStatus.Visible = False
            lbLDAdate.Text = ""
            lbTCdate.Text = ""
            lbStuDOJ.Text = ""
            If UsrSelStudent1.STUDOJ <> "" Then
                lbStuDOJ.Text = "Join Date : " & UsrSelStudent1.STUDOJ
            End If
            If UsrSelStudent1.LDADATE <> "" Then
                lbLDAdate.Text = "LDA Date : " & UsrSelStudent1.LDADATE
            End If
            If UsrSelStudent1.TCDATE <> "" Then
                lbTCdate.Text = "Cancel Date : " & UsrSelStudent1.TCDATE
            End If

            If UsrSelStudent1.STU_STATUS <> "EN" Then
                lblStuStatus.Visible = True
                lblStuStatus.Text = "Status : " & UsrSelStudent1.STU_STATUS
                UsrSelStudent1.ChangeColor(True)
            Else
                UsrSelStudent1.ChangeColor(False)
            End If
            Gridbind_Feedetails()
            lnkTransportfee.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "feeTranspotCollectionfromFee.aspx") & "&stuno=" & UsrSelStudent1.STUDENT_NO & "&acdid=" & ddlAcademicYear.SelectedItem.Value & "');return false;"
            If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "select count(*) from PosData.dbo.CUSTOMER  where CUS_NO='" & UsrSelStudent1.STUDENT_NO & "'") > 0 Then
                lnkCatering.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "feeCatering.aspx") & "&stuno=" & UsrSelStudent1.STUDENT_NO & "&acdid=" & ddlAcademicYear.SelectedItem.Value & "');return false;"
                lnkCatering.Visible = True
            Else
                lnkCatering.Visible = False
            End If
            If Not ddlAcademicYear.Items.FindByValue(UsrSelStudent1.ACD_ID) Is Nothing Then
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(UsrSelStudent1.ACD_ID).Selected = True
            End If
            SetDiscountData_Student()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub imgFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrom.Click
        Gridbind_Feedetails()
        SetNarration()
    End Sub

    Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom.TextChanged
        Gridbind_Feedetails()
        SetNarration()
    End Sub

    Protected Sub lbViewTotal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbViewTotal.Click
        Set_Total()
    End Sub

    Protected Sub txtBank1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank1.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank1.Text = FeeCommon.GetBankName(txtBank1.Text, str_bankid, str_bankho)
        h_Bank1.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank1.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate1)
            If str_bankho <> "" Then
                ddlEmirate1.SelectedIndex = -1
                ddlEmirate1.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank1)
            lblBankInvalid.Text = "Invalid Bank Selected in Cheque # 1"
        End If
    End Sub

    Protected Sub txtBank2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank2.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank2.Text = FeeCommon.GetBankName(txtBank2.Text, str_bankid, str_bankho)
        h_Bank2.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank2.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate2)
            If str_bankho <> "" Then
                ddlEmirate2.SelectedIndex = -1
                ddlEmirate2.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank2)
            lblBankInvalid.Text = "Invalid Bank Selected in Cheque # 2"
        End If
    End Sub

    Protected Sub txtBank3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank3.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank3.Text = FeeCommon.GetBankName(txtBank3.Text, str_bankid, str_bankho)
        h_Bank3.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank3.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate3)
            If str_bankho <> "" Then
                ddlEmirate3.SelectedIndex = -1
                ddlEmirate3.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank3)
            lblBankInvalid.Text = "Invalid Bank Selected in Cheque # 3"
        End If
    End Sub

    Sub SetNarration()
        If IsDate(txtFrom.Text) Then
            txtRemarks.Text = FeeCollection.F_GetFeeNarration(txtFrom.Text, ddlAcademicYear.SelectedItem.Value, Session("sBsuid"))
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        SetNarration()
        'UsrSelStudent1.ClearDetails()
        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
        ddFeetype.DataBind()
        Set_Provider()
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Dim stu_no As String = Oasis_Counter.Counter.GetNextStudentInQueue(Session("counterId"), Session("sBsuid"), Session("sUsr_name"))
        UsrSelStudent1.SetStudentDetailsByNO(stu_no)
        labGrade.Text = "Grade : " & UsrSelStudent1.GRM_DISPLAY
        Gridbind_Feedetails()
    End Sub

    Protected Sub imgCheque_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCheque.Click
        SetChequeDetails(h_Chequeid.Value)
    End Sub

    Sub SetChequeDetails(ByVal CHQ_ID As String)
        If IsNumeric(CHQ_ID) Then
            Dim ds As New DataSet
            Dim sql_query As String = " SELECT     BNK.BNK_DESCRIPTION,   FCD.FCD_REFNO, FCD.FCD_EMR_ID, FCD.FCD_REF_ID, " & _
            " REPLACE(CONVERT(VARCHAR(11), FCD.FCD_DATE, 113), ' ', '/') FCD_DATE" & _
            " FROM FEES.FEECOLLSUB_D AS FCD INNER JOIN" & _
            " FEES.FEECOLLECTION_H AS FCL ON FCD.FCD_FCL_ID = FCL.FCL_ID INNER JOIN" & _
            " VW_OSO_STUDENT_M AS STU ON FCL.FCL_STU_ID = STU.STU_ID INNER JOIN" & _
            " BANK_M AS BNK ON FCD.FCD_REF_ID = BNK.BNK_ID" & _
            " WHERE     (FCD.FCD_CLT_ID = 2) AND (FCL.FCL_BSU_ID = '" & Session("sBsuid") & "') " & _
            " AND (FCD.FCD_UNIQCHQ_ID = " & CHQ_ID & " )"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, sql_query)
            If ds.Tables.Count > 0 Then
                txtBank1.Text = ds.Tables(0).Rows(0)("BNK_DESCRIPTION")
                h_Bank1.Value = ds.Tables(0).Rows(0)("FCD_REF_ID")
                txtChqno1.Text = ds.Tables(0).Rows(0)("FCD_REFNO")
                txtChqDate1.Text = ds.Tables(0).Rows(0)("FCD_DATE")
                If Not ddlEmirate1.Items.FindByValue(ds.Tables(0).Rows(0)("FCD_EMR_ID")) Is Nothing Then
                    ddlEmirate1.SelectedIndex = -1
                    ddlEmirate1.Items.FindByValue(ds.Tables(0).Rows(0)("FCD_EMR_ID")).Selected = True
                End If
            End If
        End If
    End Sub

    Protected Sub lbApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDiscount.SelectedIndex = sender.Parent.Parent.RowIndex
        Dim dtDiscountGrid As DataTable
        If rbEnrollment.Checked Then
            dtDiscountGrid = FeeCommon.DIS_SelectDiscount(txtFrom.Text, UsrSelStudent1.STUDENT_ID, "S", Session("sBsuid"), "", False)
        Else
            dtDiscountGrid = FeeCommon.DIS_SelectDiscount(txtFrom.Text, UsrSelStudent1.STUDENT_ID, "E", Session("sBsuid"), "", False)
        End If
        gvDiscount.DataSource = dtDiscountGrid
        gvDiscount.DataBind()
        Dim lblFee_ID As Label = sender.Parent.Parent.Findcontrol("lblFee_ID")
        Dim lblFds_ID As Label = sender.Parent.Parent.Findcontrol("lblFds_ID")

        Dim dtDiscountData As DataTable
        If rbEnrollment.Checked Then
            dtDiscountData = FeeCollection.DIS_GetDiscount(lblFds_ID.Text, UsrSelStudent1.STUDENT_ID, "S", txtFrom.Text)
        Else
            dtDiscountData = FeeCollection.DIS_GetDiscount(lblFds_ID.Text, UsrSelStudent1.STUDENT_ID, "E", txtFrom.Text)
        End If
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
        txtRemarks.Text = dtDiscountData.Rows(0)("Narration")
        If Not dtDiscountData Is Nothing AndAlso dtDiscountData.Rows.Count > 0 Then
            For Each gvr As GridViewRow In gvFeeCollection.Rows
                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
                If Not txtAmountToPay Is Nothing AndAlso lblFSR_FEE_ID.Text.Trim = lblFee_ID.Text.Trim Then
                    'txtAmountToPay.Text = Format(Math.Round(dtDiscountData.Rows(0)("Net"), 0) + Convert.ToDecimal(lblAmount.Text), Session("BSU_DataFormatString"))
                    txtAmountToPay.Text = Format(Math.Round(dtDiscountData.Rows(0)("Net"), 0), Session("BSU_DataFormatString"))
                    If txtAmountToPay.Text < 0 Then
                        txtAmountToPay.Text = Session("BSU_DataFormatString")
                    End If
                    txtAmountToPay.Attributes.Add("readonly", "readonly")
                    lblDiscount.Text = Format(Math.Round(dtDiscountData.Rows(0)("FDS_DISCAMOUNT"), 0), Session("BSU_DataFormatString"))
                    lblAmount.Text = Format(Math.Round(dtDiscountData.Rows(0)("Total"), 0), Session("BSU_DataFormatString"))
                    lbCancel.Visible = True
                End If
            Next
        End If
        Set_GridTotal(True)
    End Sub

    Sub SetDiscountData_BSU()
        Dim str_discount_sql As String = "select isnull(BSU_bAPPLYDISCOUNTSCHEME,0) from businessunit_m where bsu_id='" & Session("sBsuid") & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_discount_sql)
        lnkDiscount.Visible = False
        gvFeeCollection.Columns(9).Visible = False
        gvFeeCollection.Columns(10).Visible = True
        pnlDiscount.Visible = False
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If Convert.ToBoolean(ds.Tables(0).Rows(0)(0)) Then
                lnkDiscount.Visible = True
                gvFeeCollection.Columns(9).Visible = True
                gvFeeCollection.Columns(10).Visible = False
                pnlDiscount.Visible = True
            End If
        End If
    End Sub

    Sub SetDiscountData_Student()
        If lnkDiscount.Visible And h_Student_no.Value <> "" Then
            lnkDiscount.Visible = True
            Dim dtDiscount As DataTable
            If rbEnrollment.Checked Then
                dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, UsrSelStudent1.STUDENT_ID, "S", Session("sBsuid"), "", False)
            Else
                dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, UsrSelStudent1.STUDENT_ID, "E", Session("sBsuid"), "", False)
            End If
            gvDiscount.DataSource = dtDiscount
            gvDiscount.DataBind()
        Else
            gvDiscount.SelectedIndex = -1
            gvDiscount.DataSource = Nothing
            gvDiscount.DataBind()
        End If
    End Sub

    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtDiscount As DataTable
        gvDiscount.SelectedIndex = -1
        If rbEnrollment.Checked Then
            dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, UsrSelStudent1.STUDENT_ID, "S", Session("sBsuid"), "", False)
        Else
            dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, UsrSelStudent1.STUDENT_ID, "E", Session("sBsuid"), "", False)
        End If
        gvDiscount.DataSource = dtDiscount
        gvDiscount.DataBind()
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
            If Not txtAmountToPay Is Nothing Then
                txtAmountToPay.Attributes.Remove("readonly")
                lbCancel.Visible = False
            End If
        Next
        For iIndex As Integer = 0 To Session("FeeCollection").rows.count - 1
            Session("FeeCollection").rows(iIndex)(("Amount")) = IIf(Session("FeeCollection").rows(iIndex)("Closing") > 0, Session("FeeCollection").rows(iIndex)("Closing"), 0)
            Session("FeeCollection").rows(iIndex)(("Discount")) = 0
        Next
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
        SetNarration()
        Set_GridTotal(True)
    End Sub

    Protected Sub lbStudentAudit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbStudentAudit.Click

        Dim param As New Hashtable

        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("studName", UsrSelStudent1.STUDENT_NAME)
        param.Add("Grade", UsrSelStudent1.STUDENT_GRADE_ID)
        param.Add("Section", UsrSelStudent1.STUDENT_GRADE_ID)
        param.Add("StuNo", UsrSelStudent1.STUDENT_NO)
        param.Add("status", UsrSelStudent1.STU_STATUS)
        param.Add("stu_id", UsrSelStudent1.STUDENT_ID)
        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "Oasis"
            .reportPath = Server.MapPath("../Students/Reports/RPT/rptstudAudit.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        h_print.Value = "audit"
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    Protected Sub chkCoBrand_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_COBrand_Payment_controls()
    End Sub

    Sub Set_COBrand_Payment_controls(Optional ByVal bClearSelection As Boolean = True)
        If chkCoBrand.Checked Then
            'txtCashTotal.Attributes.Add("readonly", "readonly")
            txtChequeTotal1.Attributes.Add("readonly", "readonly")
            txtChequeTotal2.Attributes.Add("readonly", "readonly")
            txtChequeTotal3.Attributes.Add("readonly", "readonly")
            'txtCCTotal.Attributes.Add("readonly", "readonly")

            'txtCashTotal.Text = "0"
            txtChequeTotal1.Text = "0"
            txtChequeTotal2.Text = "0"
            txtChequeTotal3.Text = "0"
            txtBankTotal.Text = "0"

            ddCreditcard.Enabled = False
            gvFeeCollection.Columns(9).Visible = True
            gvFeeCollection.Columns(10).Visible = False
            'txtRemarks.Text = "NBAD-GEMS co-brand discount on advance Tuition fees. " & txtRemarks.Text
            Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S", ConnectionManger.GetOASISFINConnectionString)
            If Not ddCreditcard.Items.FindByValue(str_CRR_ID) Is Nothing Then
                ddCreditcard.ClearSelection()
                ddCreditcard.Items.FindByValue(str_CRR_ID).Selected = True
            End If
        Else
            ddCreditcard.Enabled = True
            'txtCashTotal.Attributes.Remove("readonly")
            txtChequeTotal1.Attributes.Remove("readonly")
            txtChequeTotal2.Attributes.Remove("readonly")
            txtChequeTotal3.Attributes.Remove("readonly")
            'txtCCTotal.Attributes.Remove("readonly")

            gvFeeCollection.Columns(9).Visible = True 'changed to show discount for ever
            gvFeeCollection.Columns(10).Visible = False
            If bClearSelection Then
                ddCreditcard.SelectedIndex = 0
            End If
        End If
        If chkCoBrand.Checked Then
            Set_GridTotal(False)
        Else
            Gridbind_Feedetails()
            'txtCCTotal.Text = "0"
        End If

    End Sub

    Public Shared Function GetDiscountedFeesPayable_COBRAND(ByVal p_STU_ID As String, ByVal p_STU_TYPE As String, _
            ByVal p_CRR_ID As String, ByVal p_CLT_ID As String, ByVal p_FEE_ID As String, _
            ByVal p_AMOUNT As Decimal, ByRef p_Discount As Decimal) As String
        Dim pParms(14) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = p_STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(1).Value = p_STU_TYPE
        pParms(2) = New SqlClient.SqlParameter("@CRR_ID", SqlDbType.Int)
        pParms(2).Value = p_CRR_ID
        pParms(3) = New SqlClient.SqlParameter("@CLT_ID", SqlDbType.Int)
        pParms(3).Value = p_CLT_ID
        pParms(4) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FEE_ID
        pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal, 21)
        pParms(5).Value = p_AMOUNT
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@Discount", SqlDbType.Decimal, 21)
        pParms(7).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "GetDiscountedFeesPayable_COBRAND", pParms)
        If pParms(6).Value = 0 Then
            If Not pParms(7).Value Is Nothing And pParms(7).Value Is System.DBNull.Value Then
                p_Discount = 0
            Else
                p_Discount = pParms(7).Value
            End If
        End If
        GetDiscountedFeesPayable_COBRAND = pParms(6).Value
    End Function

    Protected Sub txtInvoice_TextChanged(sender As Object, e As System.EventArgs) Handles txtInvoice.TextChanged
        If Not Session("SkippedStudents") Is Nothing Then Session.Remove("SkippedStudents")
        Me.LoadStudentAndFeeDetails("")
        Me.txtRemarks.Text = GetNarration(Session("sBsuid"), Me.txtInvoice.Text)
    End Sub

    Public Sub LoadStudentAndFeeDetails(ByVal SkipStudentIds As String)
        If txtInvoice.Text = Nothing Then Exit Sub
        Dim stuParms(3) As SqlClient.SqlParameter
        Dim SkippedStudents As String = ""
        Try

            If SkipStudentIds = Nothing Then
                SkippedStudents = ""
            Else
                If SkipStudentIds.Contains(",") Then SkippedStudents = SkipStudentIds.Remove(SkipStudentIds.Length - 1, 1)
            End If


            'First clear the datasets from the session if they are existing
            If Not Session("dsStudent") Is Nothing Then Session("dsStudent") = Nothing
            If Not Session("dsFee") Is Nothing Then Session("dsFee") = Nothing
            If Not Session("dsStudentFee") Is Nothing Then Session("dsStudentFee") = Nothing
            If Not Session("dtStudent") Is Nothing Then Session("dtStudent") = Nothing

            'Load student grid with total fee being paid per student
            stuParms(0) = New SqlClient.SqlParameter("@Type", SqlDbType.VarChar)
            stuParms(0).Value = "SG"
            stuParms(1) = New SqlClient.SqlParameter("@InvoiceNo", SqlDbType.VarChar)
            stuParms(1).Value = Me.txtInvoice.Text
            stuParms(2) = New SqlClient.SqlParameter("@SkipStudentId", SqlDbType.VarChar)
            stuParms(2).Value = SkippedStudents
            stuParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            stuParms(3).Value = Session("sBsuid")
            Me.dsStudent = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FeeCollection_GetProFormaInvoiceDetails", stuParms)
            If Not Me.dsStudent Is Nothing Then
                Me.grvStudent.DataSource = Me.dsStudent.Tables(0)
                Me.grvStudent.DataBind()
                If Me.dsStudent.Tables(0).Rows.Count > 0 Then
                    Me.txtOutstanding.Text = Val(CDbl(Me.dsStudent.Tables(0).Compute("sum(PayingNow)", Nothing)))
                    Me.txtTotal.Text = CDbl(Me.txtOutstanding.Text)
                Else
                    Me.txtOutstanding.Text = CDbl(0)
                    Me.txtTotal.Text = CDbl(0)
                End If
            End If
            'Load FeeType grid
            stuParms(0) = New SqlClient.SqlParameter("@Type", SqlDbType.VarChar)
            stuParms(0).Value = "FG"
            Me.dsFee = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FeeCollection_GetProFormaInvoiceDetails", stuParms)
            If Not Me.dsFee Is Nothing Then
                Me.grvFee.DataSource = Me.dsFee.Tables(0)
                Me.grvFee.DataBind()
            End If
            'Load dsStudentFee
            stuParms(0) = New SqlClient.SqlParameter("@Type", SqlDbType.VarChar)
            stuParms(0).Value = ""
            Me.dsStudentFee = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FeeCollection_GetProFormaInvoiceDetails", stuParms)

            Session.Add("dsStudent", dsStudent)
            Session.Add("dsFee", dsFee)
            Session.Add("dsStudentFee", dsStudentFee)

        Catch ex As Exception
            'Me.lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try


    End Sub

    'Protected Sub btnGetInvoiceDetails_Click(sender As Object, e As System.EventArgs) Handles btnGetInvoiceDetails.Click
    '    If Not Session("SkippedStudents") Is Nothing Then Session.Remove("SkippedStudents")
    '    Me.LoadStudentAndFeeDetails("")
    'End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'gridbind()
    End Sub



    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If grvStudent.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = grvStudent.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub grvStudent_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grvStudent.PageIndexChanging
        'gvResult.DataSource = dt;
        '    gvResult.PageIndex = e.NewPageIndex;
        '    gvResult.DataBind();
        '    Session["datatablePaging"] = dt
        If Not Session("dtStudent") Is Nothing Then
            grvStudent.DataSource = CType(Session("dtStudent"), DataTable)
        Else
            grvStudent.DataSource = CType(Session("dsStudent"), DataSet).Tables(0)
        End If
        Me.grvStudent.PageIndex = e.NewPageIndex
        grvStudent.DataBind()
    End Sub

    Public Sub GridSearchHandler(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim dt, tmpdt As DataTable
        Dim txtBox As TextBox = Nothing
        Dim cond As String = ""

        If Not Session("dtStudent") Is Nothing Then
            Session("dtStudent") = Nothing
        End If

        dt = New DataTable

        If Not Session("dsStudent") Is Nothing Then
            tmpdt = CType(Session("dsStudent"), DataSet).Tables(0)
            'ElseIf Not Session("dtStudent") Is Nothing Then
            '    tmpdt = CType(Session("dtStudent"), DataTable)
        End If

        For Each col As DataColumn In tmpdt.Columns
            dt.Columns.Add(col.ColumnName, col.DataType)
        Next


        If Not grvStudent.HeaderRow.Cells(0).FindControl("txtStudentNo") Is Nothing Then 'StudentNo
            txtBox = CType(Me.grvStudent.HeaderRow.Cells(0).FindControl("txtStudentNo"), TextBox)
            If Not txtBox.Text = Nothing Then
                cond = "StudentNo Like '%" & txtBox.Text & "%'"
            End If
        End If
        If Not grvStudent.HeaderRow.Cells(0).FindControl("txtStudentName") Is Nothing Then 'StudentName
            txtBox = CType(Me.grvStudent.HeaderRow.Cells(0).FindControl("txtStudentName"), TextBox)
            If Not txtBox.Text = Nothing Then
                cond &= IIf(cond = Nothing, Nothing, " And ") & " StudentName Like '%" & txtBox.Text & "%'"
            End If
        End If
        If Not grvStudent.HeaderRow.Cells(0).FindControl("txtGrade") Is Nothing Then 'Grade
            txtBox = CType(Me.grvStudent.HeaderRow.Cells(0).FindControl("txtGrade"), TextBox)
            If Not txtBox.Text = Nothing Then
                cond &= IIf(cond = Nothing, Nothing, " And ") & " Grade Like '%" & txtBox.Text & "%'"
            End If
        End If
        If Not grvStudent.HeaderRow.Cells(0).FindControl("txtGrade") Is Nothing Then 'PayingNow
            txtBox = CType(Me.grvStudent.HeaderRow.Cells(0).FindControl("txtPayingNow"), TextBox)
            If Not txtBox.Text = Nothing Then
                cond &= IIf(cond = Nothing, Nothing, " And ") & " PayingNow = " & txtBox.Text
            End If
        End If
        If cond = Nothing Then
            Me.grvStudent.DataSource = CType(Session("dsStudent"), DataSet).Tables(0)
            Me.grvStudent.DataBind()
            Exit Sub
        End If

        Dim row, rows() As DataRow
        rows = tmpdt.Select(cond)
        If rows.Length > 0 Then
            'dt = rows.CopyToDataTable
            For Each tmpRow As DataRow In rows
                'dt.Rows.Add(tmpRow.ItemArray)
                row = dt.NewRow
                row.Item("InvoiceId") = tmpRow.Item("InvoiceId")
                row.Item("StudentId") = tmpRow.Item("StudentId")
                row.Item("StudentNo") = tmpRow.Item("StudentNo")
                row.Item("StudentName") = tmpRow.Item("StudentName")
                row.Item("JoiningDate") = tmpRow.Item("JoiningDate")
                row.Item("Grade") = tmpRow.Item("Grade")
                row.Item("AcademicId") = tmpRow.Item("AcademicId")
                row.Item("PayingNow") = tmpRow.Item("PayingNow")
                dt.Rows.Add(row)
            Next
            dt.AcceptChanges()
        End If

        If Not Session("dtStudent") Is Nothing Then
            Session("dtStudent") = Nothing
        End If
        Session("dtStudent") = dt
        Me.grvStudent.DataSource = dt
        Me.grvStudent.DataBind()

    End Sub

    Protected Sub grvStudent_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grvStudent.RowCommand
        'Dim Str As String = e.CommandName

        'Str = grvStudent.SelectedRow.RowIndex

    End Sub

    Protected Sub grvStudent_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvStudent.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim strDate As String = CType(e.Row.Cells(2).Controls(0), Label).Text

        '    Dim DT As DateTime = Convert.ToDateTime(strDate)

        '    strDate = DT.ToString().Format(Session("BSU_DataFormatString"))
        '    'strDate = DT.ToString


        '    '// or DT.ToString(Session["Locale"])

        '    CType(e.Row.Cells(0).Controls(0), Label).Text = strDate

        'End If
    End Sub

    Protected Sub grvStudent_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grvStudent.RowDeleting
        Dim Id As Integer = 0
        Dim lblStu As Label
        lblStu = CType(grvStudent.Rows(e.RowIndex).FindControl("lblStudentId"), Label)

        If Not lblStu Is Nothing Then
            If Session("SkippedStudents") Is Nothing Then
                'Dim stu As New ArrayList
                'stu.Add(lblStu.Text)
                Session("SkippedStudents") &= lblStu.Text & ","
                Me.LoadStudentAndFeeDetails(Session("SkippedStudents"))
            Else
                Session("SkippedStudents") &= lblStu.Text & ","
                Me.LoadStudentAndFeeDetails(Session("SkippedStudents"))
            End If

        End If

    End Sub

    Protected Sub grvStudent_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grvStudent.SelectedIndexChanged

    End Sub
End Class
