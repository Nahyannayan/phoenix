<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeChequeClearance.aspx.vb" Inherits="Fees_feeChequeClearance" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getBank() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 460px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            <%--var result;
            result = window.showModalDialog("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "", sFeatures);
            if (result == '' || result == undefined)
            { return false; }
            lstrVal = result.split('||');
            document.getElementById('<%=txtBankCode.ClientID %>').value = lstrVal[0];
        document.getElementById('<%=txtBankDescr.ClientID %>').value = lstrVal[1];
        }--%>
            var url = "../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value;
            var oWnd = radopen(url, "pop_getbank");
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID%>').value = NameandCode[1];
                __doPostBack('<%=txtBankDescr.ClientID%>', 'TextChanged');
            }
        }



        function getCheckboxstatus() {
            var chk;
            chk = 'notchecked';
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].checked == true)
                        chk = 'checked';
                }
            }
            if (chk == 'notchecked') {
                alert('Please select atleast one detail!!!')

            }
            else
                return true;
        }
        function ShowCostcenterDetail(id) {
            var sFeatures;
            sFeatures = "dialogWidth: 559px; ";
            sFeatures += "dialogHeight: 405px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../common/PopupShowData.aspx?id=COSTCENTER&vcdid=" + id, "", sFeatures)

            return false;
        }

        function ChangeAllCheckBoxStates(checkState) {
            document.getElementById('<%=txtAmount.ClientID %>').value = 0
       var chk_state = document.getElementById("chkAL").checked;
       for (i = 0; i < document.forms[0].elements.length; i++) {
           if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
               if (document.forms[0].elements[i].type == 'checkbox') {
                   document.forms[0].elements[i].checked = chk_state;
                   getAmount(document.forms[0].elements[i]);
               }
       }
   }

   function getAmount(Obj) {
       var Amnt = document.getElementById('<%=txtAmount.ClientID %>').value;
        var SelAmnt = Obj.value

        if (isNaN(SelAmnt))
            SelAmnt = 0;

        var TotalAmnt = Number(Amnt) + Number(SelAmnt)
        if (Obj.checked == false)
            TotalAmnt = Number(Amnt) - Number(SelAmnt)
        if (Number(TotalAmnt) < 0)
            TotalAmnt = 0;
        document.getElementById('<%=txtAmount.ClientID %>').value = TotalAmnt.toFixed(2);
    }
    
    

function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       
        <Windows>
            <telerik:RadWindow ID="pop_getbank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Cheque Deposit
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <table cellspacing="0" width="100%" align="center" border="0">
        <tr>
            <td align="left">
                <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                </td>
        </tr>
    </table>

    <table width="100%" align="center">
        
        <tr id="tr_Businessunit" runat="server">
            <td align="left" width="15%"> 
                <span class="field-label">Business Unit</span>
            </td>
            <td align="left" colspan="2">
                <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged"
                TabIndex="5">
            </asp:DropDownList></td>
            <td align="left" width="25%"></td>
            <td align="left" width="10%"></td>
            <td align="left" width="10%"></td>
        </tr>
        <tr>
            <td width="15%" align="left">
                 <span class="field-label">Doc No</span> </td>
            <td width="25%" align="left" colspan="2">
                <asp:TextBox ID="txtdocNo" runat="server"></asp:TextBox>
            </td>
            <td width="15%" align="left"> 
                <span class="field-label">Doc Date</span>
                </td>
            <td align="left" width="25%" colspan="2">
                    <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="true"></asp:TextBox>
                <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
           
        </tr>
        <tr>
            <td align="left"> 
                <span class="field-label">Bank Account</span></td>
            <td colspan="3" align="left">
                <table width="100%" align="center">
                    <tr>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="true"></asp:TextBox>
                <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getBank();return false;" />
                        </td>
                        <td align="left" width="60%">
                            
                <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                
            </td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td align="left">
                <span class="field-label">Remarks</span>
            </td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtNarrn" runat="server" TextMode="MultiLine" CssClass="inputbox" SkinID="MultiText"></asp:TextBox></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left" width="15%">
                <span class="field-label">Currency</span>
            </td>
            <td align="left" width="25%">
                <table width="100%">
                    <tr>
                        <td align="left" width="45%" class="p-0" >
                            <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="true">
                </asp:DropDownList>
                        </td>
                        <td align="left" width="45%" class="p-0">
                            <asp:TextBox ID="txtExchRate" runat="server" style="text-align:right"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                
                </td>
            <td width="15%" align="left">
                <span class="field-label">Group Rate</span>
            </td>
            <td width="25%" align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="45%" class="p-0" >
                    <asp:TextBox ID="txtLocalRate" runat="server" style="text-align:right"></asp:TextBox>
                        </td>
                        <td align="left" width="45%" class="p-0">
                <asp:DropDownList ID="ddCollection" runat="server" TabIndex="10" SkinID="DropDownListNormal" DataSourceID="SqlCollection" DataTextField="COL_DESCR" DataValueField="COL_ID" Visible="False">
                </asp:DropDownList>
                             </td>
                    </tr>
                </table>
            </td>
            <td align="left" width="10%">
                <span class="field-label">More Days to Include</span>
            </td>
            <td width="10%" align="left">
            <asp:TextBox ID="txtDaysInclude" runat="server"  AutoPostBack="True">0</asp:TextBox></td>
        </tr>

        <tr>
            <td colspan="6" align="left" class="title-bg" >Credit Details </td>
        </tr>
    </table>



    <table align="center" width="100%">


        <tr>
            <td align="center" colspan="6">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" cssclass="table table-bordered table-row" ShowFooter="True" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Doc No.">
                            <HeaderTemplate>
                               Doc No<br />
                               <asp:TextBox ID="txtDocNo" runat="server"  Width="75%"></asp:TextBox>
                               <asp:ImageButton ID="btnDocNoSearch6" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchControl_Click" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDocno" runat="server" Text='<%# Bind("VCD_DOCNO") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="VCD_AMOUNT" HeaderText="Amount"></asp:BoundField>
                        <asp:TemplateField HeaderText="Desc.">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("VCD_NARRATION") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Chq. No.">
                            <HeaderTemplate>
                                Chq No<br />

                                                        <asp:TextBox ID="txtChqNo" runat="server"  Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDateSearch1" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchControl_Click" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("VCD_CHQNO") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="VCD_CHQDT" HeaderText="Chq. Dt."></asp:BoundField>
                        <asp:TemplateField HeaderText="Rec. No.">
                            <HeaderTemplate>
                                Rec No<br />
                                <asp:TextBox ID="txtRecNo" runat="server"  Width="75%"></asp:TextBox>
                                <asp:ImageButton ID="btnBankACSearch2" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchControl_Click" />
                                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("VCD_FCL_RECNO") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bank">
                            <HeaderTemplate>
                                Bank<br />
                                <asp:TextBox ID="txtBank" runat="server"  Width="75%"></asp:TextBox>
                                <asp:ImageButton ID="btnNarration4" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchControl_Click" />&nbsp;</td>
                                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("BNK_SHORT") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DEP_BANK" HeaderText="Deposit Bank"></asp:BoundField>
                        <asp:TemplateField HeaderText="Emirate">
                            <HeaderTemplate>
                                Emirate<br />
                                <asp:TextBox ID="txtEmirate" runat="server" Width="75%"></asp:TextBox>
                                 <asp:ImageButton ID="btnCurrencySearch5" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                            OnClick="btnSearchControl_Click" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("EMR_DESCR") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IDS" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblVCD_EMR_ID" runat="server" Text='<%# Bind("VCD_EMR_ID") %>'></asp:Label>
                                <asp:Label ID="lblVCD_BNK_ID" runat="server" Text='<%# Bind("VCD_BNK_ID") %>'></asp:Label>
                                <asp:Label ID="lblVCD_CHQID" runat="server" Text='<%# Bind("VCD_CHQID") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Select">
                            <HeaderTemplate>
                                &nbsp;<input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                    value="Check All" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <input id="chkVCD_ID" runat="server" style="cursor: hand" type="checkbox" value='<%# Bind("VCD_AMOUNT") %>' onclick="getAmount(this)" />

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>


        <tr>
            <td align="center" colspan="6">
                <table width="100%">
                    <tr>
                        <td align="right" width="50%" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" /><asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Text="Print" />
                        </td>
                        <td align="right" width="20%">
                            <span class="field-label">Selected Amount</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server"  Text="0" ReadOnly="true" style="text-align:right"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                
                 
                 </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlCollection" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
        SelectCommand="SELECT COL_ID, COL_DESCR FROM COLLECTION_M"></asp:SqlDataSource>
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtdocDate">
    </ajaxToolkit:CalendarExtender>
    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />

                </div>
            </div>
        </div>

</asp:Content>

