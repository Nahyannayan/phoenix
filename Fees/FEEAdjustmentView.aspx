<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEAdjustmentView.aspx.vb" Inherits="FEEAdjustmentView" Theme="General" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
   <link href="../cssfiles/Popup.css" rel="stylesheet" />


    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
       function CheckForPrint() {
           if (document.getElementById('<%= h_print.ClientID %>').value != '') {
               document.getElementById('<%= h_print.ClientID %>').value = '';
               //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
               return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
               return false;
           }
       }
    );
      
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew1" runat="server" Visible="False">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year </span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="True" Checked="True" Text="Student" GroupName="STUD_TYPE" />
                            <asp:RadioButton ID="radEnq" runat="server" Text="Enquiry" AutoPostBack="True" GroupName="STUD_TYPE" />
                        </td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="radOpen" runat="server" AutoPostBack="True" Checked="True" Text="Open" GroupName="OPEN_POSTED" />
                            <asp:RadioButton ID="radPosted" runat="server" AutoPostBack="True" Text="Posted" GroupName="OPEN_POSTED" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvFEEAdjustments" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="FCH_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_ID" runat="server" Text='<%# Bind("FAH_ID") %>'></asp:Label>
                                            <asp:Label ID="lblFAH_bInter" runat="server" Text='<%# Bind("FAH_bInter") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DOCNO">
                                        <HeaderTemplate>
                                            Doc. No<br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("FAH_DOCNO") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStudNo" runat="server"></asp:TextBox><br />
                                            <asp:ImageButton ID="btnStuNoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtstudname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStuNameSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FAH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <HeaderTemplate>
                                            Amount<br />
                                            <asp:TextBox ID="txtAcademicYear" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAcademicYearSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMOUNT" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks<br />
                                            <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnRemarksSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FAH_REMARKS") %>'></asp:Label>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FAR_DOCNO" HeaderText="Req.Doc No"></asp:BoundField>
                                    <asp:TemplateField HeaderText="View" Visible="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnPrint" runat="server" OnClick="btnPrint_Click">Print</asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <script type="text/javascript">
                    cssdropdown.startchrome("chromemenu1");
                    cssdropdown.startchrome("chromemenu2");
                    cssdropdown.startchrome("chromemenu3");
                    cssdropdown.startchrome("chromemenu4");
                    cssdropdown.startchrome("chromemenu5");
                    cssdropdown.startchrome("chromemenu6");
                </script>
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>


    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        </script>
</asp:Content>
