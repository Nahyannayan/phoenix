Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CrystalDecisions.Shared
Partial Class Fees_FEEReminder
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Shared reportHeader As String
    Private Property strFEEIds() As String
        Get
            Return ViewState("strFEEIds")
        End Get
        Set(ByVal value As String)
            ViewState("strFEEIds") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager, smScriptManage1 As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSaveNExport)
        If Page.IsPostBack = False Then
            btnSaveNExport.Visible = False
            gvFEEReminder.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REMINDER Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                ddlAcademicYear.Items.Clear()
                ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                ddlAcademicYear.DataTextField = "ACY_DESCR"
                ddlAcademicYear.DataValueField = "ACD_ID"
                ddlAcademicYear.DataBind()
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                ddlLevel.DataBind()
                FillAgeDetails(ddlLevel.SelectedValue)
                BindGrade()
                BindSection()
                txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                Session("liUserList") = New ArrayList
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("gvFEETYPEDET") = GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"))
                'BindFeeType()
                BindFeeTypeOne(ddlAcademicYear.SelectedValue, Session("sbsuid"))
                BindDDLFeeType()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Me.lblSettle.Text = "This will process the pending settlement of students. " & _
                "Please be patient, it will take " & _
                "some time to complete the process."
                If ViewState("datamode") = "view" Then
                    GridBind()
                    Exit Sub
                End If
                ' GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Public Shared Function GetAllFEEType(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataTable
        Dim dsData As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID

        dsData = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GET_FEE_TYPES_FOR_REMINDER", pParms)

        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Private Sub BindDDLFeeType()
        ddlFeeType.Items.Clear()
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataSource = DirectCast(ViewState("gvFEETYPEDET"), DataTable)
        ddlFeeType.DataBind()
    End Sub
    Private Sub BindFeeType()
        gvFEETYPEDET.DataSource = ViewState("gvFEETYPEDET")
        gvFEETYPEDET.DataBind()
    End Sub
    Private Sub BindFeeTypeOne(ByVal ACD_ID As String, ByVal BSU_ID As String)
        Dim str_Sql As String = ""
        Dim dsData As DataSet


        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim sql_query As String = "SELECT DISTINCT FEE_ORDER ,CAST(FEES.FEES_M.FEE_ID AS VARCHAR(5)) + '___' + CAST(FEE_ORDER AS VARCHAR(5))  FEE_ID, FEES.FEES_M.FEE_DESCR FEE_DESCR " & _
            " FROM FEES.FEES_M "
            sql_query &= " INNER JOIN FEES.FEESETUP_S ON "
            sql_query &= " FEES.FEES_M.FEE_ID = FEES.FEESETUP_S.FSP_FEE_ID "
            sql_query &= " WHERE FEES.FEESETUP_S.FSP_ACD_ID = '" & ACD_ID & "'"
            sql_query &= " AND isnull(FEE_SVC_ID,0)<>1 "
            sql_query &= " AND FEES.FEESETUP_S.FSP_BSU_ID = '" & BSU_ID & "'"
            sql_query &= " AND ISNULL(FSP_PRO_ID,0) NOT IN (11,12) and FEE_ID in(5) "

            sql_query &= " UNION "
            sql_query &= " SELECT DISTINCT FEE_ORDER,CAST(FOH_FEE_ID AS VARCHAR(5)) + '___' + CAST(FEE_ORDER AS VARCHAR(5)),B.FEE_DESCR FROM FEES.FEEOTHCHARGE_H A INNER JOIN FEES.FEES_M B WITH(NOLOCK) "
            sql_query &= " ON A.FOH_FEE_ID=B.FEE_ID where FOH_BSU_ID='" & BSU_ID & "'"
            sql_query &= " AND FOH_ACD_ID='" & ACD_ID & "' AND isnull(FEE_SVC_ID,0)<>1 and FEE_ID in(5)  "
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using


        'str_Sql = "select FEE_ID,FEE_DESCR  from FEES.FEES_M where FEE_ID in(5) "
        'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        gvFEETYPEDET.DataSource = dsData.Tables(0)
        gvFEETYPEDET.DataBind()
        ViewState("gvFEETYPEDETOne") = dsData.Tables(0)
    End Sub


    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEReminder.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEReminder.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim Fee_FilterID As String = String.Empty
            Dim Section_FilterID As String = String.Empty
            Dim str_Filter, lstrOpr, str_Sql, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            str_Filter = ""
            If gvFEEReminder.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)

                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)

                '   -- 2  section
                'larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtSection")
                'lstrCondn4 = txtSearch.Text.Trim
                'If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SCT_DESCR", lstrCondn4)

                ''   -- 5  city
                'larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtPeriod")
                'lstrCondn5 = txtSearch.Text.Trim
                'If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "PERIOD", lstrCondn5)

            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            Dim str_cond As String = String.Empty
            Dim asOnDate As Date = CDate(txtAsOnDate.Text)
            Dim vLevel As Int16
            'If radFirstRM.Checked Then
            '    str_cond = " AND FSH_FDUEDATE <= '" & asOnDate & "'"
            'ElseIf radSecRM.Checked Then
            '    str_cond = " AND FSH_SDUEDATE <= '" & asOnDate & "'"
            'ElseIf radThirdRM.Checked Then
            '    str_cond = " AND FSH_TDUEDATE <= '" & asOnDate & "'"
            'End If
            If ddlGrade.SelectedValue <> "ALL" Then
                str_cond = "AND FSH_GRD_ID ='" & ddlGrade.SelectedValue & "'"
            End If
            Dim strSelectedNodes As String = UsrTreeView1.GetSelectedNode(",")
            Section_FilterID = UsrTreeView1.GetSelectedNode("|")
            If strSelectedNodes <> "" Then 'Then strSelectedNodes = "0"
                str_cond += "AND FSH_SCT_ID in (" & strSelectedNodes & ")"


                'If ddlSection.SelectedValue <> "ALL" Then
                '    str_cond += "AND FSH_SCT_ID ='" & ddlSection.SelectedValue & "'"
            End If

            'getting selected fee types added by jacob on 7/mar/2013
            Dim feeid() As String
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            For Each gvr As GridViewRow In gvFEETYPEDET.Rows
                Dim lblFeeID As Label = DirectCast(gvr.FindControl("lblFEE_ID"), Label)
                feeid = lblFeeID.Text.Split("___")
                Fee_FilterID = IIf(Fee_FilterID = String.Empty, feeid(0), Fee_FilterID + "|" + feeid(0))
            Next
            strFEEIds = Fee_FilterID
            Dim reminderTypeID As String = ddlLevel.SelectedValue
            reminderTypeID = reminderTypeID.Split("_")(0)

            If Fee_FilterID <> String.Empty Then
                Dim cmd As New SqlCommand
                cmd.CommandText = "[FEES].[Get_FEEReminderDetails]"
                cmd.CommandType = Data.CommandType.StoredProcedure
                Dim param(6) As SqlParameter
                param(0) = Mainclass.CreateSqlParameter("@BSU_IDs", Session("sBSUID"), SqlDbType.VarChar)
                param(1) = Mainclass.CreateSqlParameter("@FEE_IDs", Fee_FilterID, SqlDbType.VarChar)
                param(2) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue, SqlDbType.VarChar)
                param(3) = Mainclass.CreateSqlParameter("@SCT_ID", Section_FilterID, SqlDbType.VarChar)
                param(4) = Mainclass.CreateSqlParameter("@AsOnDt", asOnDate, SqlDbType.DateTime)
                param(5) = Mainclass.CreateSqlParameter("@reminderType", reminderTypeID, SqlDbType.VarChar)
                param(6) = Mainclass.CreateSqlParameter("@amount_limit", txtAmountLimit.Text, SqlDbType.Decimal)

                cmd.Connection = New SqlConnection(str_conn)
                ds = SqlHelper.ExecuteDataset(cmd.Connection.ConnectionString, CommandType.StoredProcedure, cmd.CommandText, param)
            Else
                str_Sql = "SELECT * FROM [FEES].[fn_Get_FEEReminderDetails] " & _
                            "( " & Session("sBSUID") & ", '" & asOnDate & "', " & reminderTypeID & _
                            ", " & txtAmountLimit.Text & ")" & _
                            " WHERE 1=1 " & str_cond & str_Filter & "order by SCT_DESCR,STU_NAME "
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            End If

            ViewState("gvFEEReminder") = ds.Tables(0)
            Session("sAsOnDate") = txtAsOnDate.Text
            Session("sLevel") = vLevel
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEReminder.DataBind()
                Dim columnCount As Integer = gvFEEReminder.Rows(0).Cells.Count
                gvFEEReminder.Rows(0).Cells.Clear()
                gvFEEReminder.Rows(0).Cells.Add(New TableCell)
                gvFEEReminder.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEReminder.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEReminder.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                BindGridSource()
            End If

            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtConcession")
            'txtSearch.Text = lstrCondn3

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtFrom")
            'txtSearch.Text = lstrCondn4

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtPeriod")
            'txtSearch.Text = lstrCondn5

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtRemarks")
            'txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindGridSource()
        gvFEEReminder.DataSource = ViewState("gvFEEReminder")
        gvFEEReminder.DataBind()
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetAmountDue(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        sTemp.Append("<table >") ' border=1 bordercolor=#1b80b6 bgcolor=#ffcccc cellpadding=1 cellspacing=0>")
        sTemp.Append("<tr>")
        sTemp.Append("<td colspan='6'><b>The FEE Due Details</b></td>")
        sTemp.Append("</tr>")
        sTemp.Append("<tr>")
        sTemp.Append("<td><b>ACAD.Yr</b></td>")
        sTemp.Append("<td><b>GRADE</b></td>")
        sTemp.Append("<td><b>FEE DESCR</b></td>")
        sTemp.Append("<td><b>CHG.AMT</b></td>")
        sTemp.Append("<td><b>PAID AMT</b></td>")
        sTemp.Append("<td><b>DUE. AMT</b></td>")
        sTemp.Append("</tr>")

        Dim STUD_ID As Integer = contextKey
        Dim AsOnDate As Date = HttpContext.Current.Session("sAsOnDate")
        Dim BSU_ID As String = HttpContext.Current.Session("sBSUID")
        Dim Level As String = HttpContext.Current.Session("sLevel")

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim conn As New SqlConnection(str_conn)

        Try
            Dim drread As SqlDataReader = FEEReminder.GetDueAmount(STUD_ID, AsOnDate, BSU_ID, 30)
            While (drread.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drread("ACY_DESCR").ToString() & "</td>")
                sTemp.Append("<td>" & drread("GRM_DISPLAY").ToString() & "</td>")
                sTemp.Append("<td>" & drread("FEE_DESCR").ToString() & "</td>")
                sTemp.Append("<td>" & Format(drread("CHARGE_AMT"), "0.00") & "</td>")
                sTemp.Append("<td>" & Format(drread("PAID_AMT"), "0.00") & "</td>")
                sTemp.Append("<td>" & Format(drread("DUE_AMT"), "0.00") & "</td>")
                sTemp.Append("</tr>")
            End While
            drread.Close()
        Catch ex As Exception
        Finally
            sTemp.Append("</table>")
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return sTemp.ToString()
    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEReminder.PageIndexChanging
        gvFEEReminder.PageIndex = e.NewPageIndex
        SetChk(Page)
        GridBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not ViewState("gvFEEReminder") Is Nothing AndAlso gvFEEReminder.Rows.Count > 0 Then
            Dim txtStuNo As TextBox = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
            Dim txtStuname As TextBox = gvFEEReminder.HeaderRow.FindControl("txtStuname")
            If (Not txtStuNo Is Nothing AndAlso Not txtStuname Is Nothing) AndAlso (txtStuNo.Text <> "" Or txtStuname.Text <> "") Then
                Dim dt As DataTable = DirectCast(ViewState("gvFEEReminder"), DataTable)
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = " STU_NO LIKE '%" & txtStuNo.Text.Trim & "%'"
                ViewState("gvFEEReminder") = dv.ToTable()
                BindGridSource()
            Else
                GridBind()
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        ViewState("gvFEETYPEDET") = GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"))
        BindFeeType()
    End Sub
    Private Sub BindGrade()
        ddlGrade.DataSource = FEEReminder.GetGradeForBSU(Session("sBSUID"), ddlAcademicYear.SelectedValue)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count > 0 Then
            ddlGrade.SelectedIndex = 0
        End If
        ddlGrade_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveReminder()
    End Sub
    Protected Sub btnSaveNExport_Click(sender As Object, e As EventArgs) Handles btnSaveNExport.Click
        SaveReminder()
    End Sub
    Private Function SaveReminder() As Boolean
        SaveReminder = False
        'If Not Master.IsSessionMatchesForSave() Then
        '    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    Exit Sub
        'End If
        'SetChk(Page)
        'If (Session("liUserList") Is Nothing) OrElse (Session("liUserList").Count <= 0) Then
        'lblError.Text = "Please Select atleast 1 Student..."
        'Exit Sub
        'End If
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction

        If UsrTreeView1.GetSelectedNode(",") = "" Then
            'lblError.Text = "Select atleast one section."
            usrMessageBar2.ShowNotification("Select atleast one section.", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If

        Dim stud_ids As Hashtable = GetStudentList()
        If stud_ids Is Nothing OrElse stud_ids.Count <= 0 Then
            'lblError.Text = "There should be atleast 1 student to save this entry"
            usrMessageBar2.ShowNotification("There should be atleast 1 student to save this entry", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        trans = conn.BeginTransaction("FEE_REMINDER")
        Dim dtAsOndate As DateTime = CDate(txtAsOnDate.Text)
        Dim dtDate As DateTime = CDate(txtDate.Text)
        Dim vFEE_REM As New FEEReminder
        vFEE_REM.FRH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_REM.FRH_ASONDATE = dtAsOndate
        vFEE_REM.FRH_DT = dtDate
        vFEE_REM.FRH_Level = ddlLevel.SelectedValue.Split("_")(0)
        vFEE_REM.FRH_REMARKS = txtReminderHeading.Text
        vFEE_REM.FRH_bARABIC = chkArabic.Checked
        'If radFirstRM.Checked Then
        '    vFEE_REM.FRH_Level = ReminderType.FIRST
        'ElseIf radSecRM.Checked Then
        '    vFEE_REM.FRH_Level = ReminderType.SECOND
        'ElseIf radThirdRM.Checked Then
        '    vFEE_REM.FRH_Level = ReminderType.THIRD
        'End If
        vFEE_REM.FRH_bIncludePDC = Me.chkIncludePDC.Checked
        vFEE_REM.FRH_BSU_ID = Session("sBSUID")
        vFEE_REM.SEND_REMINDER_TO = rblReminderFor.SelectedValue
        Try
            Dim vFRH_ID As Integer
            Dim vLevel As Integer = ddlLevel.SelectedValue.Split("_")(0)
            Dim retVal As Integer = FEEReminder.SaveFEEReminderDetails(stud_ids, CDbl(txtAmountLimit.Text), vFEE_REM, vFRH_ID, False, conn, trans, strFEEIds)
            If retVal > 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                'trans.Rollback()
                trans.Commit()
                SaveReminder = True
                'lblError.Text = "Data updated Successfully"
                usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", vFRH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                Dim bArabic As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(RMD_bARABIC, 0) FROM dbo.REMINDERTYPE_M WITH ( NOLOCK ) WHERE RMD_ID = " & vFEE_REM.FRH_Level.ToString & "")
                If IsNothing(bArabic) Then
                    bArabic = False
                End If
                Try
                    If chkPrint.Checked Then
                        If Not bArabic Then
                            Session("ReportSource") = FEEReminder.PrintReminder(vFRH_ID, Session("sUsr_name"))
                            h_print.Value = "print"
                        Else
                            'lblError.Text = "Data updated Successfully"
                            usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                            ExportArabicReminder(vFRH_ID, Session("sUsr_name"))
                        End If

                    End If
                Catch ex As Exception

                End Try

            End If
        Catch ex As Exception
        Finally
            conn.Close()
        End Try

    End Function
    Public Function ExportArabicReminder(ByVal vFRH_ID As Integer, ByVal Usr_name As String, Optional ByVal Fees As Boolean = True, Optional ByVal vSTU_IDs As String = "") As MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        If Fees = False Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        End If

        Dim cmd As New SqlCommand("[FEES].[F_GETReminderLetter]", New SqlConnection(str_conn))
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRH_ID As New SqlParameter("@FRH_ID", SqlDbType.Int)
        sqlpFRH_ID.Value = vFRH_ID
        cmd.Parameters.Add(sqlpFRH_ID)

        Dim sqlpSTU_ID As New SqlParameter("@FRD_STU_ID", SqlDbType.VarChar)
        sqlpSTU_ID.Value = vSTU_IDs
        cmd.Parameters.Add(sqlpSTU_ID)


        Dim ds As New DataSet
        Dim _adapter As New SqlDataAdapter(cmd)
        _adapter.Fill(ds)

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass

        Dim cmdSubColln As New SqlCommand

        ''SUBREPORT1
        cmdSubColln.CommandText = "EXEC [FEES].F_GETReminderLetterDetail " & vFRH_ID & ",'" & vSTU_IDs & "'"
        cmdSubColln.Connection = New SqlConnection(str_conn)
        cmdSubColln.CommandType = CommandType.Text
        repSourceSubRep(0).Command = cmdSubColln

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.SubReport = repSourceSubRep
        params("UserName") = Usr_name
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If Fees = True Then
            repSource.ReportUniqueName = "ArabicReminder"
            Dim BSUID As String
            BSUID = Session("sBsuid")
            repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT/rptFeeReminder_Arabic.rpt")
            If Not repSource Is Nothing Then
                Dim repClassVal As New RepClass
                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName
                repClassVal.SetDataSource(ds.Tables(0))
                If repSource.IncludeBSUImage Then
                    If repSource.HeaderBSUID Is Nothing Then
                        IncludeBSUmage(repClassVal, BSUID)
                    Else
                        IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                    End If

                End If
                SubReport(repSource, repSource, repClassVal)
                Dim pdfFilePath As String
                Dim pdfFileName As String
                pdfFileName = "ArabicReminder.pdf"

                pdfFilePath = WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
                '  pdfFilePath = "C:\0Junk\"
                pdfFilePath += pdfFileName

                repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                End While
                Try
                    If System.IO.File.Exists(pdfFilePath) Then
                        System.IO.File.Delete(pdfFilePath)
                    End If
                Catch ex As Exception

                End Try

                repClassVal.ExportToDisk(ExportFormatType.PortableDocFormat, pdfFilePath)

                Dim BytesData As Byte()
                BytesData = ConvertFiletoBytes(pdfFilePath)
                Dim ContentType, Extension As String
                ContentType = "application/pdf"
                Extension = GetFileExtension(ContentType)
                Dim Title As String = "Receipt"
                If repSource.ReportUniqueName.ToString.Split("_").Length > 1 Then
                    Title = repSource.ReportUniqueName.ToString.Split("_")(1)
                End If
                DownloadFile(System.Web.HttpContext.Current, BytesData, ContentType, Extension, Title, pdfFileName)
                repClassVal.Close()
                repClassVal.Dispose()
                repSource = Nothing
                repClassVal = Nothing
            End If
        End If
        Return repSource
    End Function
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String, ByVal FileName As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.End()
    End Sub
    Private Shared Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Shared Sub SubReport(ByVal repSource As MyReportClass, ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(repSource, subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            reportHeader = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Shared Sub IncludeBSUmage(ByRef repClassVal As RepClass, ByVal BSUID As String)
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = BSUID
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub
    Private Function GetStudentList() As Hashtable
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_cond As String = String.Empty
        Dim asOnDate As Date = CDate(txtAsOnDate.Text)

        Dim vLevel As Int16
        'If radFirstRM.Checked Then
        '    vLevel = 1
        'ElseIf radSecRM.Checked Then
        '    vLevel = 2
        'ElseIf radThirdRM.Checked Then
        '    vLevel = 3
        'End If
        Dim str_Sql As String
        'str_Sql = "SELECT * FROM [FEES].[fn_Get_FEEReminderDetails] " & _
        '"( " & Session("sBSUID") & ", '" & asOnDate & "', " & ddlLevel.SelectedValue & ", " & ddlLevel.SelectedValue & ", " & txtAmountLimit.Text & ")"
        If ddlGrade.SelectedValue <> "ALL" Then
            str_cond = "AND FSH_GRD_ID ='" & ddlGrade.SelectedValue & "'"
        End If
        str_cond += "AND FSH_SCT_ID in (" & UsrTreeView1.GetSelectedNode(",") & ")"
        'If ddlSection.SelectedValue <> "ALL" Then
        '    str_cond += "AND FSH_SCT_ID ='" & ddlSection.SelectedValue & "'"
        'End If

        Dim reminderTypeID As String = ddlLevel.SelectedValue
        reminderTypeID = reminderTypeID.Split("_")(0)
        Dim feeid(), Fee_FilterID As String
        Fee_FilterID = ""
        For Each gvr As GridViewRow In gvFEETYPEDET.Rows
            Dim lblFeeID As Label = DirectCast(gvr.FindControl("lblFEE_ID"), Label)
            feeid = lblFeeID.Text.Split("___")
            Fee_FilterID = IIf(Fee_FilterID = String.Empty, feeid(0), Fee_FilterID + "|" + feeid(0))
        Next

        Dim cmd As New SqlCommand
        Dim ds As New DataSet
        cmd.CommandText = "[FEES].[Get_FEEReminderDetails]"
        cmd.CommandType = Data.CommandType.StoredProcedure
        Dim param(6) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@BSU_IDs", Session("sBSUID"), SqlDbType.VarChar)
        param(1) = Mainclass.CreateSqlParameter("@FEE_IDs", Fee_FilterID.ToString, SqlDbType.VarChar)
        param(2) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue, SqlDbType.VarChar)
        param(3) = Mainclass.CreateSqlParameter("@SCT_ID", UsrTreeView1.GetSelectedNode(","), SqlDbType.VarChar)
        param(4) = Mainclass.CreateSqlParameter("@AsOnDt", asOnDate, SqlDbType.DateTime)
        param(5) = Mainclass.CreateSqlParameter("@reminderType", reminderTypeID, SqlDbType.VarChar)
        param(6) = Mainclass.CreateSqlParameter("@amount_limit", txtAmountLimit.Text, SqlDbType.Decimal)

        cmd.Connection = New SqlConnection(str_conn)
        ds = SqlHelper.ExecuteDataset(cmd.Connection.ConnectionString, CommandType.StoredProcedure, cmd.CommandText, param)

        Dim httab As New Hashtable
        Dim dt As New DataTable, dRow As DataRow

        'str_Sql = "SELECT * FROM [FEES].[fn_Get_FEEReminderDetails] " & _
        '"( '" & Session("sBSUID") & "', '" & asOnDate & "', " & reminderTypeID & _
        '", " & txtAmountLimit.Text & ")" & _
        '" WHERE 1=1 " & str_cond
        'dt = Mainclass.getDataTable(str_Sql, str_conn)

        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If

        For Each dRow In dt.Rows
            httab(dRow("FSH_STU_ID")) = False
        Next
        If (Session("liUserList") IsNot Nothing) AndAlso (Session("liUserList").Count > 0) Then
            Dim bSelExclude As Boolean '= RadIncludeSelected.Checked
            For Each STU_ID As Integer In Session("liUserList")
                httab(STU_ID) = bSelExclude
            Next
        End If

        'Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        '    While (dr.Read())
        '        httab(dr("FSH_STU_ID")) = False
        '    End While
        '    If (Session("liUserList") IsNot Nothing) AndAlso (Session("liUserList").Count > 0) Then
        '        Dim bSelExclude As Boolean '= RadIncludeSelected.Checked
        '        For Each STU_ID As Integer In Session("liUserList")
        '            httab(STU_ID) = bSelExclude
        '        Next
        '    End If
        'End Using
        Return httab
    End Function

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAsOnDate.TextChanged
        GridBind()
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Private Sub ClearDetails()
        txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        'lblError.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnListSudents_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListSudents.Click
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Private Sub BindSection()
        'ddlSection.DataSource = FEEReminder.GetSectionForGrade(Session("sBSUID"), ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue)
        'ddlSection.DataTextField = "SCT_DESCR"
        'ddlSection.DataValueField = "SCT_ID"
        'ddlSection.DataBind()
        'ddlSection.Items.Add(New ListItem("ALL", "ALL"))
        'ddlSection.Items.FindByText("ALL").Selected = True
        UsrTreeView1.DataSource = FEEReminder.GetSectionForGrade(Session("sBSUID"), ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue)
        UsrTreeView1.DataTextField = "SCT_DESCR"
        UsrTreeView1.DataValueField = "SCT_ID"
        UsrTreeView1.ExpandOnLoad = True
        UsrTreeView1.Bind()

    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        FillAgeDetails(ddlLevel.SelectedValue)
        GridBind()
        Dim bArabic As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(RMD_bARABIC, 0) FROM dbo.REMINDERTYPE_M WHERE RMD_ID=" & ddlLevel.SelectedValue.Split("_")(0).ToString & "")
        If IsNothing(bArabic) Then
            bArabic = False
        End If
        If bArabic Then
            btnSave.Visible = False
            btnSaveNExport.Visible = True
        Else
            btnSave.Visible = True
            btnSaveNExport.Visible = False
        End If
    End Sub

    Private Sub FillAgeDetails(ByVal val As String)
        Dim str()
        str = val.Split("_")
        lblAgefrom.Text = str(1)
        lblAgeTo.Text = str(2)
        txtReminderHeading.Text = GetReminderHeading(str(0))
    End Sub

    Private Function GetReminderHeading(ByVal RMD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim strsql As String = " SELECT ISNULL(RMD_REMARKS, '') FROM REMINDERTYPE_M WITH(NOLOCK) WHERE RMD_ID = " & RMD_ID
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strsql)
    End Function

    Protected Sub lnkDeleteFEETYPE_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim link As LinkButton = DirectCast(sender, LinkButton)
        Dim gv As GridViewRow = DirectCast(link.Parent.Parent, GridViewRow)
        DirectCast(ViewState("gvFEETYPEDET"), DataTable).Rows(gv.RowIndex).Delete()
        DirectCast(ViewState("gvFEETYPEDET"), DataTable).AcceptChanges()
        BindFeeType()
        ''gvFEETYPEDET.Rows(gv.RowIndex)
        'Dim CustomerID As LinkButton = DirectCast(gv.FindControl("lnk_CustomerID"), LinkButton)
    End Sub

    Protected Sub lnkAddFEETYPE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddFEETYPE.Click
        If ddlFeeType.Items.Count <= 0 Then Return
        'lblError.Text = ""
        'Dim dt As DataTable = DirectCast(ViewState("gvFEETYPEDET"), DataTable)

        Dim dt As DataTable = DirectCast(ViewState("gvFEETYPEDETOne"), DataTable)

        For Each dtr As DataRow In dt.Rows
            If dtr("FEE_ID") = ddlFeeType.SelectedValue Then
                'lblError.Text = "This Fee Type is Duplicated..."
                usrMessageBar2.ShowNotification("This Fee Type is Duplicated...", UserControls_usrMessageBar.WarningType.Danger)
                Return
            End If
        Next
        Dim dr As DataRow = dt.NewRow
        dr("FEE_ORDER") = 0
        dr("FEE_ID") = ddlFeeType.SelectedValue
        dr("FEE_DESCR") = ddlFeeType.SelectedItem.Text
        dt.Rows.Add(dr)
        dt.AcceptChanges()
        ViewState("gvFEETYPEDET") = dt
        BindFeeType()
    End Sub

    Protected Sub btnResettle_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ReSettlement()
    End Sub
    Private Sub ReSettlement()
        Dim objconn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Try
            Dim cmd As SqlCommand

            'objconn.Open()
            cmd = New SqlCommand("FEES.SP_RESETTLE_STU_FEE_AGING_WITH_DIFF", objconn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_ID.Value = Session("sBsuId")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpASON_DT As New SqlParameter("@ASONDT", SqlDbType.VarChar)
            sqlpASON_DT.Value = txtAsOnDate.Text
            cmd.Parameters.Add(sqlpASON_DT)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
        Catch
        Finally
            If objconn.State = ConnectionState.Open Then
                objconn.Close()
            End If
        End Try
    End Sub


End Class
