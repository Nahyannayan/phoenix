﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeReverseAdvanceTaxInvoice.aspx.vb" Inherits="Fees_FeeReverseAdvanceTaxInvoice" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<%--    <script type="text/javascript" src="~/Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="~/Scripts/jquery-ui-1.10.2.min.js"></script>
    <script  type="text/javascript" src="../Scripts/jquery-ui.js" ></script>
   
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />--%>
    <style>
        .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        
        
    </style>
    <script language="javascript" type="text/javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Reverse Advance Tax Invoice
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                  <%--  <tr class="subheader_img">
                        <td colspan="3" style="height: 19px" align="left">Reverse Advance Tax Invoice
                        </td>
                    </tr>--%>
                    <tr>
                        <td  align="left" width="15%"><span class="field-label">Business Unit</span>
                        </td>
                       
                        <td  align="left" width="40%">
                            <telerik:RadComboBox ID="ddlBusinessUnit" RenderMode="Lightweight"  runat="server" AutoPostBack="True" Filter="Contains"  ZIndex="2000"  width="100%" ToolTip="Type in unit name or short code">
                            </telerik:RadComboBox>
                        </td>
                         <td  align="left" width="15%"><span class="field-label">Fee Type</span></td>
                        
                        <td  align="left" width="30%">
                            <telerik:RadComboBox ID="ddlFeeType" RenderMode="Lightweight" width="100%" runat="server" AutoPostBack="True" Filter="Contains" ZIndex="2000" ToolTip="Type in fee type"></telerik:RadComboBox>
                        </td>                        
                    </tr>
                    <tr>
                       <td  align="left" width="20%"><span class="field-label">Select Student</span></td>
                        
                        <td  align="left" width="30%">
                            <asp:RadioButtonList ID="rblStuType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                <asp:ListItem Selected="True" Value="S"><span class="field-label">Enrolled</span></asp:ListItem>
                                <asp:ListItem Value="E"><span class="field-label">Enquiry</span></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:TextBox ID="txtStudent" runat="server" AutoPostBack="true" OnTextChanged="txtStudent_TextChanged" placeholder="Type in or Search Student Id or Name" ></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(); return false;" />
                            <asp:LinkButton ID="lbtnClearStudent" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                            <asp:LinkButton ID="lbtnLoadStuDetail" runat="server" CausesValidation="False"></asp:LinkButton>
                            <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TBWE3" runat="server"
                                TargetControlID="txtStudent"
                                WatermarkText="Type in or Search Student Id or Name"
                                WatermarkCssClass="watermarked" />--%>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                TargetControlID="lbtnClearStudent"
                                ConfirmText="Are you sure you want to remove the Student selection?" />
                            <asp:HiddenField ID="h_STU_ID" runat="server" />                            
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trStuDetail" runat="server" visible="false">
                      <%--  <td  align="left"></td>
                        <td  align="center" ></td>--%>
                        <td  align="left" colspan="4">
                            <table  cellpadding="3" cellspacing="3" style="width: 100% !important;">
                                <tr>
                                    <%--<td  align="left" style="width: 20%;"><span class="field-label">Student Id</span></td>
                                   
                                    <td  align="left" width="30%">
                                        <asp:Label ID="lblStudentId" runat="server" ></asp:Label></td>--%>
                                    <td  align="left" width="20%" ><span class="field-label"> Status :</span>
                                        <asp:Label ID="lblStuCurrstatus" runat="server" class="field-label" ></asp:Label>
                                    </td>
                                    
                                   
                                    <td  align="left" width="20%"><span class="field-label">Grade :</span>
                                        <asp:Label ID="lblStuGrade" runat="server" class="field-label" ></asp:Label>
                                    </td>
                                    
                                   
                                    <td  align="left" width="20%"><span class="field-label">Ledger :</span>
                                        <asp:LinkButton ID="lBtnLedger" runat="server">View</asp:LinkButton>
                                    </td>                                    
                                    <td width="70%"></td>
                                </tr>
                               <%-- <tr>
                                    <td  align="left" width="20%"><span class="field-label">Student Name</span></td>
                                  
                                    <td width="30%">
                                        <asp:Label ID="lblStuName" runat="server" ></asp:Label></td>
                                    
                                </tr>--%>
                               <%-- <tr runat="server" id="trTCSO" visible="false">
                                    <td  align="left" width="20%"><span class="field-label">Leave Date</span></td>
                                    
                                    <td width="30%">
                                        <asp:Label ID="lblStuLeaveDate" runat="server"></asp:Label>
                                    </td>
                                    <td  align="left" width="20%"><span class="field-label">Last Attendance</span></td>
                                    
                                    <td  align="left" width="30%">
                                        <asp:Label ID="lblStuLastAttdate" runat="server" ></asp:Label></td>
                                </tr>--%>
                                <%--<tr runat="server">
                                    
                                    <td  align="left"></td>
                                    <td  align="center"></td>
                                   <%-- <td  align="left"></td>
                                </tr>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-bg" align="left" colspan="4">Fee Advance History</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvAdvances" runat="server" AutoGenerateColumns="False" Width="100%"  EmptyDataText="No Advance invoices to show" ShowFooter="True" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Invoice No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnInvNo" runat="server" Text='<%# Bind("FSH_INVOICE_NO") %>'></asp:LinkButton>
                                            <asp:HiddenField ID="hf_FSH_ID" runat="server" Value='<%# Bind("FSH_ID")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterText" runat="server" Text="TOTAL"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FSH_INVOICE_DATE" HeaderText="Invoice Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="false" />
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description" />
                                    <asp:BoundField DataField="FSH_NARRATION" HeaderText="Narration" />
                                    <asp:TemplateField HeaderText="Debit">
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalDebit" runat="server">0.00</asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("DEBIT", "{0:0.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Credit">
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCredit" runat="server" Text="0.00"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("CREDIT", "{0:0.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Label ID="Label3" runat="server" Text="Net Advance Balance : " ></asp:Label>
                            <asp:Label ID="lblNetAdvBalance" runat="server" Text="0.00" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="title-bg">Reversal Entry
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvSummary" CssClass="table table-bordered table-row" runat="server" Width="100%" AutoGenerateColumns="False" SkinID="GridViewNormal" EmptyDataText="No Advance invoices to show" DataKeyNames="FEE_ID">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkReverse" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description" />
                                    <asp:BoundField DataField="DEBIT" HeaderText="Debit" DataFormatString="{0:0.00}" HtmlEncode="false" />
                                    <asp:BoundField DataField="CREDIT" HeaderText="Credit" DataFormatString="{0:0.00}" HtmlEncode="false" />
                                    <asp:TemplateField HeaderText="Balance">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBalance" runat="server" Text='<%# Bind("BALANCE", "{0:0.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reverse Amount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtReverseAmt" runat="server" Style="text-align: right;" onfocus="this.select();" SkinID="Gridtxt" Width="80px" Text="0.00"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtReverseAmt" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:TextBox ID="txtNarration" runat="server" Style="height: 80px;" MaxLength="500" SkinID="MultiText" TextMode="MultiLine" Width="30%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" SkinID="ButtonNormal" Text="Save" />
                            <asp:Button ID="btnCancel" runat="server" SkinID="ButtonNormal" Text="Cancel" />
                        </td>
                    </tr>
                </table>


                <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
                    ReloadOnShow="true" runat="server" EnableShadow="true">
                    <Windows>
                        <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>
                </telerik:RadWindowManager>
                <script type="text/javascript" language="javascript">
                    $(document).ready(function () {
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        prm.add_initializeRequest(InitializeRequest);
                        prm.add_endRequest(EndRequest);
                        InitStudentAutoComplete();
                    });
                    function InitializeRequest(sender, args) {
                    }
                    function EndRequest(sender, args) {
                        // after update occur on UpdatePanel re-init the Autocomplete
                        InitStudentAutoComplete();
                    }
                    function InitStudentAutoComplete() {

                        $("#<%=txtStudent.ClientID%>").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: "/Fees/FeeReverseAdvanceTaxInvoice.aspx/GetStudent",
                                    data: "{ 'BSUID': '" + GetBsuId() + "','STUTYPE':'" + GetStuType() + "','prefix': '" + request.term + "'}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('-')[1] + ' - ' + item.split('-')[0],
                                                val: item.split('-')[2]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        alert(response.responseText);
                                    },
                                    failure: function (response) {
                                        alert(response.responseText);
                                    }
                                });
                            },
                            select: function (e, i) {
                                $("#<%=h_STU_ID.ClientID%>").val(i.item.val);
                                document.getElementById('<%=lbtnLoadStuDetail.ClientID%>').click();
                            },
                            minLength: 1
                        });
                    }
                    function GetBsuId() {
                        var dropFind = $find("<%= ddlBusinessUnit.ClientID%>");
                        var valueFind = dropFind.get_value();
                        return valueFind;
                    }
                    function GetStuId() {
                        alert($("#<%=h_STU_ID.ClientID%>").val());
                        return false;
                    }
                    function GetStuType() {
                        var STU_TYPE = $('#<%= rblStuType.ClientID%>').find(":checked").val();
                        //alert(STU_TYPE);
                        return STU_TYPE;
                    }
                    function GetStudent() {

                        var NameandCode;
                        var result;
                        var url = "ShowStudent.aspx?type=NO&bsu=" + GetBsuId();
                        result = radopen(url, "pop_up");
                        <%--if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                document.getElementById('<%=h_STU_ID.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtStudent.ClientID%>').value = NameandCode[1];
                return true;
            }
            else {
                return false;
            }--%>
                    }

                    function autoSizeWithCalendar(oWindow) {
                        var iframe = oWindow.get_contentFrame();
                        var body = iframe.contentWindow.document.body;
                        var height = body.scrollHeight;
                        var width = body.scrollWidth;
                        var iframeBounds = $telerik.getBounds(iframe);
                        var heightDelta = height - iframeBounds.height;
                        var widthDelta = width - iframeBounds.width;
                        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                        oWindow.center();
                    }

                    function OnClientClose(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {                                                       
                            NameandCode = arg.NameandCode.split('||');                           
                            document.getElementById('<%=h_STU_ID.ClientID%>').value = NameandCode[0];                             
                            document.getElementById('<%=txtStudent.ClientID%>').value = NameandCode[1];                           
                            __doPostBack('<%=txtStudent.ClientID%>', 'TextChanged');
                        }
                    }

                    function EnableReversal(chkRev, txtRevAmt) {
                        if ($('#' + chkRev).is(':checked') == true) {
                            $('#' + txtRevAmt).removeAttr("disabled");
                        }
                        else {
                            $('#' + txtRevAmt).attr("disabled", "disabled");
                        }
                        //alert($('#' + chkRev).is(':checked'));
                    }
                </script>

            </div>
        </div>
    </div>
</asp:Content>

