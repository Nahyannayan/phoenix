<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeRefundFee.aspx.vb" Inherits="Fees_feeRefundFee" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <style>
        table td {
            vertical-align: top !important;
        }
    </style>
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
function CheckForPrint() {
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up9');
        return false;
    }
}
    );


function GetADJUSTMENT() {

    var NameandCode;
    var result;
    var url = "../Common/PopupAdjustment.aspx?id=ADJUSTMENT&multiSelect=false&stu=" + document.getElementById('<%=h_Student_no.ClientID %>').value + "&stype=" + document.getElementById('<%=H_STU_TYPE.ClientID %>').value;
            result = radopen(url, "pop_up4");

         <%--   if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                document.getElementById('<%=h_Adjustment.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtAdjustment.ClientID %>').value = NameandCode[1];
                return true;
            }
            else {
                return false;
            }--%>
        }


        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Adjustment.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtAdjustment.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtAdjustment.ClientID%>', 'TextChanged');
            }
        }

        function get_Bank() {

            var NameandCode;
            var result;

            result = radopen("..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "pop_up5");

            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= txtChqBook.ClientId %>').value = '';
            document.getElementById('<%= hCheqBook.ClientId %>').value = '';
            return false;--%>
        }

        function OnClientClose5(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtChqBook.ClientId %>').value = '';
                document.getElementById('<%= hCheqBook.ClientId %>').value = '';
                __doPostBack('<%= txtAdjustment.ClientID%>', 'TextChanged');
            }
        }

        function get_Cheque() {

            var NameandCode;
            var result;
            var bisondax = document.getElementById('<%= h_bIsonDAX.ClientID%>').value;
            if (bisondax == 1) {
                return false;
            }
            if (document.getElementById('<%= txtBankCode.ClientId %>').value == "") {
                alert("Please Select The Bank");
                return false;
            }
            result = radopen("..\/accounts\/ShowChqs.aspx?ShowType=CHQBOOK_PDC&BankCode=" + document.getElementById('<%= txtBankCode.ClientId %>').value + "&docno=0", "pop_up6");
            <%--if (result == '' || result == undefined)
            { return false; }
            lstrVal = result.split('||');
            document.getElementById('<%= txtChqBook.ClientId %>').value = lstrVal[1];
            document.getElementById('<%= hCheqBook.ClientId %>').value = lstrVal[0];
            document.getElementById('<%= txtChqNo.ClientId %>').value = lstrVal[2];
            document.getElementById(ctrl).focus();--%>
        }

        function OnClientClose6(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%= txtChqBook.ClientId %>').value = NameandCode[1];
                document.getElementById('<%= hCheqBook.ClientId %>').value = NameandCode[0];
                document.getElementById('<%= txtChqNo.ClientId %>').value = NameandCode[2];
                document.getElementById(ctrl).focus();
                __doPostBack('<%= txtChqBook.ClientID%>', 'TextChanged');
            }
        }

        function get_Cash() {

            var NameandCode;
            var result;

            result = radopen("..\/accounts\/ShowAccount.aspx?ShowType=CASHONLY&codeorname=" + document.getElementById('<%=txtCashAcc.ClientID %>').value, "pop_up7");
<%--            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtCashAcc.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtCashDescr.ClientID %>').value = NameandCode[1];
            return false;--%>
        }

        function OnClientClose7(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtCashAcc.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCashDescr.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtCashAcc.ClientID%>', 'TextChanged');
            }
        }

        function getProvision() {

            var NameandCode;
            var result;
            result = radopen("..\/accounts\/ShowPrepaid.aspx?ShowType=PREPDACcodeorname=" + document.getElementById('<%=txtProvCode.ClientId %>').value, "pop_up8");
           <%-- if (result == '' || result == undefined)
            { return false; }
            lstrVal = result.split('||');
            document.getElementById('<%=txtProvCode.ClientId %>').value = lstrVal[0];
            document.getElementById('<%=txtProvDescr.ClientId %>').value = lstrVal[1];--%>

        }


        function OnClientClose8(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtProvCode.ClientId %>').value = NameandCode[0];
                document.getElementById('<%=txtProvDescr.ClientId %>').value = NameandCode[1];
                __doPostBack('<%= txtProvCode.ClientID%>', 'TextChanged');
            }
        }

        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(2);
            return true;
        }

        function Showdata(STU_ID, mode) {
            var url;
            var NameandCode;
            //var STU_ID = document.getElementById('<%= h_Student_no.ClientID %>').value;
            var result;
            if (mode == 1) {
                url = "../common/PopupShowData.aspx?id=PAYMENTHISTORYREFUND&stuid=" + STU_ID;
                result = radopen(url, "pop_up3");
                return false;
            }
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up6" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose6"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up7" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose7"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up8" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose8"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up9" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <%--<tr class="subheader_img">
            <td colspan="4" style="height: 19px" align="left">
                <asp:Label ID="lblHead" runat="server"></asp:Label>
            </td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student /Enquiry</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbEnrollment" runat="server" Checked="True" GroupName="mode" CssClass="field-label"
                                Text="Student" AutoPostBack="True" />
                            <asp:RadioButton ID="rbEnquiry" runat="server" GroupName="mode" Text="Enquiry" AutoPostBack="True" CssClass="field-label" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Payment mode </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbCash" runat="server" AutoPostBack="True" Checked="True" GroupName="pay" CssClass="field-label"
                                Text="Cash" />
                            <asp:RadioButton ID="rbBank" runat="server" AutoPostBack="True" GroupName="pay" CssClass="field-label"
                                Text="Bank" />
                            <asp:RadioButton ID="rbCard" runat="server" AutoPostBack="True" GroupName="pay" CssClass="field-label"
                                Text="Online (Credit Card)" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">Bank charge </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankCharge" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="tr_Cash" runat="server">
                        <td align="left" width="20%"><span class="field-label">Cash A/C </span>
                        </td>
                        <td align="left" colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCashAcc" runat="server" AutoPostBack="True" Width="90%"></asp:TextBox>
                                        <asp:ImageButton ID="imgCash" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Cash(); return false;" />

                                    </td>
                                    <td align="left" width="70%">
                                        <asp:TextBox ID="txtCashDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                    </tr>
                    <tr id="tr_Bank" runat="server">
                        <td align="left" width="20%"><span class="field-label">Bank A/C </span>
                        </td>
                        <td align="left" colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True" Width="90%"></asp:TextBox>
                                        <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank(); return false;" />
                                    </td>
                                    <td align="left" width="70%">
                                        <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                    </tr>
                    <tr id="tr_Provision" runat="server">
                        <td align="left" width="20%"><span class="field-label">Provision A/C(PDC) </span>
                        </td>
                        <td align="left" colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="40%">
                                        <asp:TextBox ID="txtProvCode" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgProv" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getProvision();return false;" />
                                    </td>
                                    <td align="left" width="60%">
                                        <asp:TextBox ID="txtProvDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                    </tr>
                    <tr id="tr_ChqType" runat="server">
                        <td width="20%" align="left"><span class="field-label">Cheque/Refrence </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:RadioButton ID="rbCheque" runat="server" Checked="True" GroupName="chq" Text="Cheque"
                                CssClass="field-label" AutoPostBack="True" />

                            <asp:RadioButton ID="rbOthers" runat="server" GroupName="chq" Text="Other Instruments"
                                CssClass="field-label" AutoPostBack="True" />
                        </td>
                        <td width="20%" align="left"><span class="field-label">Ref. No. </span>
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtrefChequeno" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="tr_Cheque" runat="server">
                        <td align="left" width="20%"><span class="field-label">Cheque Lot </span>
                            <asp:CheckBox ID="ChkBearer" runat="server" Text="Bearer Cheque" />
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtChqBook" runat="server" Width="53px"></asp:TextBox>
                            <a href="#" onclick="get_Cheque(); return false;">
                                <img id="IMG1" border="0" alt="" src="../Images/cal.gif" /></a>
                            <asp:TextBox ID="txtChqNo" runat="server" Width="84px"></asp:TextBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtChqdt" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgChq" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Student(s) </span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtStudName" runat="server" AutoPostBack="True" Width="50%" OnTextChanged="txtStudName_TextChanged">
                            </asp:TextBox>
                            <asp:LinkButton
                                ID="lblAddNewStudent" runat="server" CausesValidation="False">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgStudentN" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudentMulti(); return false;" />
                            <asp:GridView ID="gvSelectedStudents" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%" DataKeyNames="STU_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnStdDetails" runat="server" OnClientClick="return false;">Student Details</asp:LinkButton>
                                            <ajaxToolkit:HoverMenuExtender ID="hmeG" runat="Server" HoverCssClass="popupHover"
                                                PopDelay="25" PopupControlID="PanelG" PopupPosition="Center" TargetControlID="lnkBtnStdDetails">
                                            </ajaxToolkit:HoverMenuExtender>
                                            <asp:Panel ID="PanelG" runat="server" CssClass="Visibility_none" Style="display: none"
                                                BackColor="LightYellow">
                                                <div style="border-right: maroon 1px solid; border-top: maroon 1px solid; border-left: maroon 1px solid; border-bottom: maroon 1px solid">
                                                    <asp:GridView ID="gvStdDetails" runat="server" SkinID="GridViewRed">
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPayHistory" runat="server">Payment History</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click">View</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <%----%>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">T C Reference</span>
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtAdjustment" runat="server"></asp:TextBox>
                            <br />
                            <asp:CheckBox ID="chkRegistrar" runat="server" Enabled="False" Text="Registrar" CssClass="field-label" />
                            <asp:CheckBox ID="chkLab" runat="server" Enabled="False" Text="Lab" CssClass="field-label" />
                            <asp:CheckBox ID="chkFee" runat="server" Enabled="False" Text="Fee" CssClass="field-label" />
                            <asp:CheckBox ID="chkLibrary" runat="server" Enabled="False" Text="Library" CssClass="field-label" />
                            <asp:Label ID="lblAdjType" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tbl_Allocation" runat="server" align="center"
                    width="100%">
                    <tr class="title-bg">
                        <td colspan="4" align="left">Refund Details
                <asp:Label ID="lblStudent" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" class="table table-bordered table-row">
                                <tr>
                                    <th align="center">Refundable Balance
                                    </th>
                                    <th align="center">Net Balance
                                    </th>
                                </tr>
                                <tr valign="top">
                                    <td align="center" valign="top">
                                        <asp:GridView ID="gvRefund" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            Width="100%" DataKeyNames="FEE_ID">
                                            <Columns>
                                                <asp:BoundField DataField="Fee" HeaderText="Fee"></asp:BoundField>
                                                <asp:BoundField HtmlEncode="False" DataFormatString="{0:0.00}" DataField="Amount"
                                                    HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Appr. Amount">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtApprAmt" runat="server" Text='<%# Bind("Amount") %>' AutoPostBack="True"
                                                            Width="111px" onblur="return CheckAmount(this)" OnTextChanged="txtApprAmt_TextChanged"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False" HeaderText="ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvNetRefund" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                            Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="Fee" HeaderText="Fee" />
                                                <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Amount"
                                                    HtmlEncode="False">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAddUpdate" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                                        <asp:Button ID="btnCancelUpdate" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table id="tbl_Approval" runat="server" align="center" width="100%"
                    visible="false">
                    <tr class="title-bg">
                        <td colspan="4" align="left">Refund Details
                        </td>
                    </tr>
                    <tr>
                        <td align="center" rowspan="4">
                            <asp:GridView ID="gvApproval" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="false"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="Fee" HeaderText="Fee"></asp:BoundField>
                                    <asp:BoundField HtmlEncode="False" DataFormatString="{0:0.00}" DataField="Amount"
                                        HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Appr. Amount">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtApprAmt" runat="server" Text='<%# Bind("Amount") %>' AutoPostBack="True"
                                                Width="111px" onblur="return CheckAmount(this)" OnTextChanged="txtApprAmt_Approve_TextChanged"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("FRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td align="left"><span class="field-label">Total</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Paid To</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txPaidto" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%"
                    cellpadding="3" cellspacing="0">
                    <tr>
                        <td colspan="4">
                            <%--<asp:Label ID="lblError2" runat="server" SkinID="LabelError" EnableViewState="False"--%>
                            <%--Font-Size="X-Small"></asp:Label>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle"
                            colspan="4">

                            <asp:GridView ID="gvRefundSummary" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                Width="100%" ShowFooter="True" DataKeyNames="STU_ID">
                                <Columns>
                                    <asp:BoundField DataField="STU_NO" HeaderText="StudentNo" />
                                    <asp:BoundField DataField="STU_NAME" FooterText="Total :" HeaderText="Name">
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmt" runat="server" Text='<%# Bind("AMOUNT", "{0:#,0.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblAmtTotal" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnFeeDetails" runat="server" OnClientClick="return false;">Refund Details</asp:LinkButton>
                                            <ajaxToolkit:HoverMenuExtender ID="hmeFee" runat="Server" HoverCssClass="popupHover"
                                                PopDelay="25" PopupControlID="PanelFee" PopupPosition="Center" TargetControlID="lnkBtnFeeDetails">
                                            </ajaxToolkit:HoverMenuExtender>
                                            <asp:Panel ID="PanelFee" runat="server" CssClass="Visibility_none" Style="display: none"
                                                BackColor="LightYellow">
                                                <div>
                                                    <asp:GridView ID="gvFeeDetails" runat="server" CssClass="table table-bordered table-row">
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="BtnDelete" runat="server" OnClick="BtnDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeIDs" runat="server" Text='<%# Bind("FEE_IDs") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" width="20%"><span class="field-label">Narration <span class="text-danger">* </span> </span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                MaxLength="300"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /><asp:Button
                                ID="btnApprove" runat="server" CssClass="button" Text="Approve" Visible="False" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" Visible="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:Button ID="btnPrint1" runat="server" CssClass="button" Text="Print" Visible="false"
                                CausesValidation="False" />
                            <asp:CheckBox ID="chkPrintChq" runat="server" Text="Print Cheque" CssClass="field-label" Visible="false" />
                            <asp:Button ID="btnSettlement" runat="server" CssClass="button" Text="Settlement" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgChq" TargetControlID="txtChqdt">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsGetheader" runat="server" SelectMethod="GetRefundData"
                    TypeName="FeeRefund">
                    <SelectParameters>
                        <asp:SessionParameter Name="p_BSU_ID" SessionField="sBsuid" Type="String" />
                        <asp:ControlParameter ControlID="H_STU_TYPE" DefaultValue="" Name="p_STU_TYPE" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="h_Student_no" DefaultValue="" Name="p_STU_ID" PropertyName="Value"
                            Type="String" />
                        <asp:Parameter DefaultValue="HEADER" Name="p_MODE" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetDetail" runat="server" SelectMethod="GetRefundData"
                    TypeName="FeeRefund">
                    <SelectParameters>
                        <asp:SessionParameter Name="p_BSU_ID" SessionField="sBsuid" Type="String" />
                        <asp:ControlParameter ControlID="H_STU_TYPE" DefaultValue="" Name="p_STU_TYPE" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="h_Student_no" DefaultValue="" Name="p_STU_ID" PropertyName="Value"
                            Type="String" />
                        <asp:Parameter DefaultValue="DETAIL" Name="p_MODE" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Panel ID="PanelTree" runat="server" CssClass="Visibility_none" Style="display: none"
                    BackColor="LightYellow">
                    <div>
                        <asp:GridView ID="gvStudentDetails" runat="server" CssClass="table table-bordered table-row">
                        </asp:GridView>
                    </div>
                </asp:Panel>

                <script runat="server">
                    Protected Function TrueorFalse(ByVal Fee As String) As Boolean
                        If Fee.ToUpper = "TOTAL" Then
                            Return False
                        Else
                            Return True
                        End If
                    End Function
                </script>

                <script type="text/javascript" language="javascript">
                    function GetStudentSingle() {

                        var NameandCode;
                        var result;
                        var url = "../../ShowStudent.aspx?type=NO";
                        result = radopen(url, "pop_up");
                       <%-- if (result != '' && result != undefined) {
                            NameandCode = result.split('||');
                            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                            return true;
                        }
                        else {
                            return false;
                        }--%>
                    }

                    function OnClientClose(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {
                            NameandCode = arg.NameandCode.split('||');
                            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                            __doPostBack('<%= h_STUD_ID.ClientID%>', 'ValueChanged');
                        }
                    }

                    function GetStudentMulti() {
                        var SingleYN = document.getElementById('<%=h_Single.ClientID %>').value
                        if (SingleYN == "1") {
                            GetStudentSingle()
                        }

                        else {
                            var NameandCode;
                            var result;

                            var STUD_TYP = document.getElementById('<%=rbEnquiry.ClientID %>').checked;
                            var url;
                            if (STUD_TYP == true) {
                                url = "ShowStudentMulti.aspx?TYPE=ENQUIRY&MULTI_SEL=true&bsu=";
                                result = radopen(url, "pop_up2");
                            }
                            else {
                                url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=";
                                result = radopen(url, "pop_up2");
                            }
                           <%-- if (result != '' && result != undefined) {
                                document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                                document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
                            }--%>
                        }
                        //return true;
                    }

                    function OnClientClose2(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {
                            // NameandCode = arg.NameandCode.split('||');
                            document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                            document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;
                            __doPostBack('<%= h_STUD_ID.ClientID%>', 'ValueChanged');
                        }
                    }

                    function autoSizeWithCalendar(oWindow) {
                        var iframe = oWindow.get_contentFrame();
                        var body = iframe.contentWindow.document.body;
                        var height = body.scrollHeight;
                        var width = body.scrollWidth;
                        var iframeBounds = $telerik.getBounds(iframe);
                        var heightDelta = height - iframeBounds.height;
                        var widthDelta = width - iframeBounds.width;
                        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                        oWindow.center();
                    }


                </script>
                <asp:HiddenField ID="h_bIsonDAX" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" OnValueChanged="h_STUD_ID_ValueChanged" />
                <asp:HiddenField ID="h_Single" runat="server" Value="0" />
                <asp:HiddenField ID="h_Student_no" runat="server" />
                <asp:HiddenField ID="h_Adjustment" runat="server" />
                <asp:HiddenField ID="H_STU_TYPE" runat="server" />
                <asp:HiddenField ID="hCheqBook" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
