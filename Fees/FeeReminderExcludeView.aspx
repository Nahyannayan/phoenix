<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeReminderExcludeView.aspx.vb" Inherits="Fees_FeeReminderExcludeView" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function ConfirmUpdate() {
            return confirm('Are you sure you want update reminder date..?');
        }

        function ConfirmDelete() {
            return confirm('Are you sure you want delete fee reminder exclude..?');
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Fee Reminder Exclude View..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFEEConcessionDet" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="98%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">

                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FRE ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFREID" runat="server" Text='<%# Bind("FRE_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudID" runat="server" Text='<%# BIND("FRE_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LogDate">
                                        <%--<EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FRE_LOGDATE", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                        </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FRE_LOGDATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No.">
                                        <HeaderTemplate>
                                            Student No. #<br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTUDENTNAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <HeaderTemplate>
                                            From Date<br />
                                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnConcession" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFROMDATE" runat="server" Text='<%# Bind("FRE_FROMDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <HeaderTemplate>
                                            To Date<br />
                                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTODATE" runat="server" Text='<%# Bind("FRE_TODT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>

                                    <%-- <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="Delete" ItemStyle-CssClass="del" >
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>--%>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="deletebtn" runat="server" CommandName="delete" HeaderText="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want delete fee reminder exclude..?');">                                       
                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle Wrap="False" />
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <SelectedRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                            <%-- <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="lnkDelete"
                                ConfirmText="Are you sure you want to send reminders to these employees?">
                            </ajaxToolkit:ConfirmButtonExtender>--%>
                        </td>
                    </tr>
                </table>

                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sUsr_name" Type="String" DefaultValue="" Name="USR_ID"></asp:SessionParameter>
                        <asp:SessionParameter SessionField="sBsuid" Type="String" DefaultValue="" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>


                <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="left">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>

                                <table align="center" width="100%">
                                    <tr>

                                        <td align="left" width="20%"><span class="field-label">From Date</span>
                                        </td>

                                        <td align="left">
                                            <asp:TextBox ID="pnl_txtFromDate" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="return false;" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="pnl_txtFromDate"
                                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="pnl_txtFromDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>


                                        <td align="left" width="20%"><span class="field-label">To Date</span>
                                        </td>

                                        <td align="left">
                                            <asp:TextBox ID="pnl_txtToDate" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="return false;" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="pnl_txtToDate"
                                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="pnl_txtToDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" OnClientClick="return ConfirmUpdate('C');" />
                                            <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                        </td>
                                    </tr>

                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script>
        $(".del").click(function () {
            alert("test");
        });
    </script>
</asp:Content>

