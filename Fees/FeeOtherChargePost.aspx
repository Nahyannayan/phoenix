<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeOtherChargePost.aspx.vb" Inherits="Fees_FeeOtherChargePost" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
   function CheckForPrint() {
       if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
           //showModelessDialog ('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
           var url = "../Reports/ASPX Report/RptViewerModalView.aspx";
           var oWnd = radopen(url, "pop_clickforprint");
       }
}
    );

   function getChkBoxid(ChkId) {

       if (CheckBoxIDs != null) {
           for (var i = 0; i < CheckBoxIDs.length; i++) {
               var ChId = CheckBoxIDs[i];
               var cb = document.getElementById(ChId);
               if (cb != null)
                   cb.checked = '';
           }
           cb = document.getElementById(ChkId);
           cb.checked = 'checked';

       }
   }




   function autoSizeWithCalendar(oWindow) {
       var iframe = oWindow.get_contentFrame();
       var body = iframe.contentWindow.document.body;

       var height = body.scrollHeight;
       var width = body.scrollWidth;

       var iframeBounds = $telerik.getBounds(iframe);
       var heightDelta = height - iframeBounds.height;
       var widthDelta = width - iframeBounds.width;

       if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
       if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
       oWindow.center();
   }

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_clickforprint" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr valign="top">
                        <td align="left" width="20%">
                            <span class="field-label">Select Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="25" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkPost" runat="server" __designer:wfdid="w28"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FOH_ID" HeaderText="FOH_ID"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                                                        <asp:TextBox ID="txtDate" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w33"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w34"></asp:ImageButton>
                                                                
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FOH_DATE", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w32"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Fees<br />
                                                                        <asp:TextBox ID="txtStuname" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w36"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnstunameSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w37"></asp:ImageButton>
                                                               
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("FEE_DESCR") %>' __designer:wfdid="w35"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <HeaderTemplate>
                                            Amount<br />
                                                                        <asp:TextBox ID="txtAmount" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w39"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnAmountSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w40"></asp:ImageButton>
                                                                
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("FOH_AMOUNT") %>' __designer:wfdid="w38"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                            Description<br />
                                                                        <asp:TextBox ID="txtDesc" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w42"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnDescSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w43"></asp:ImageButton>
                                                               
                                        </HeaderTemplate>

                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("FOH_NARRATION") %>' __designer:wfdid="w41"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lbVoucher" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnPost" runat="server" Text="Post" CssClass="button" CausesValidation="False" Width="65px" OnClick="btnPost_Click"></asp:Button>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="h_selected_menu_2" runat="server" />
    <asp:HiddenField ID="h_selected_menu_5" runat="server" />
    <asp:HiddenField ID="h_selected_menu_6" runat="server" />
    <asp:HiddenField ID="h_selected_menu_7" runat="server" />
    <asp:HiddenField ID="h_print" runat="server" />
</asp:Content>


