<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeChequeBounceDetailedView.aspx.vb"  Inherits="FEES_FEECHEQUEBOUNCEDETAILEDVIEW" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Cheque Return Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
      <table align="center" width="100%" id="MainTable" runat="server">
        
          <tr>
              <td align="left" width="20%">
                <span class="field-label">Bounced Date</span></td>
              
              <td align="left" width="30%">
             <asp:TextBox ID="txtBDate" runat="server"></asp:TextBox></td>
              <td align="left" width="20%">
                <span class="field-label">Student/Enquiry</span></td>
              
              <td align="left" width="30%">
                <asp:Label ID="lblSTUType" runat="server"></asp:Label></td>
          </tr>
        <tr>
            <td align="left">
                                           <span class="field-label"> Student</span>
                                           </td>
            
            <td align="left" colspan="3">
                <table width="100%">
                    <tr>
                        <td width="50%" align="left"><asp:TextBox ID="txtStudNo" runat="server"></asp:TextBox></td>
                        <td width="50%" align="left"><asp:TextBox ID="txtStudName" runat="server"></asp:TextBox></td>
                        <asp:HiddenField ID="hfSTUID" runat="server" />
                    </tr>
                </table>
                
                
                
            </td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Student Business Unit</span></td>
            
            <td align="left" colspan="3">
                <asp:Label ID="lblSTU_BSU" runat="server"></asp:Label></td>
        </tr>
                <tr>
                <td align="left"><span class="field-label">Academic Year</span></td>
               
                <td align="left"><asp:Label ID="lblACY_DESCR" runat="server" CssClass="field-value"></asp:Label></td>
                <td align="left">
                    <span class="field-label">GRADE</span></td>
                
                <td align="left">
                    <asp:Label ID="lblGRM_ID" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Cheque No</span></td>
            
            <td align="left" >
                <asp:TextBox ID="txtCHQNO" runat="server"></asp:TextBox></td>
                <td align="left">
                    <span class="field-label">Cheque Date</span></td>
                
                <td align="left">
                    <asp:TextBox ID="txtCHQDate" runat="server"></asp:TextBox></td>
       </tr>
        <tr>
            <td align="left">
               <span class="field-label"> Bank Details</span></td>
            
            <td align="left" colspan="6">
                <asp:TextBox ID="txtBNKID" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtBNKName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Emirate</span></td>
            
            <td align="left" colspan="6">
                <asp:TextBox ID="txtEMR" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Remarks</span></td>
            
            <td align="left" colspan="6">
                <asp:TextBox ID="txtNarration" runat="server" Width="496px" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Amount</span></td>
           
            <td align="left" colspan="6">
                <asp:Label ID="lblAmount" runat="server"></asp:Label></td>
        </tr>
        <tr id="tr_Deatails" runat="server">
            <td align="center" colspan="4" valign="top">
                <asp:GridView ID="gvFeeDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="true" SkinID="GridViewNormal" Width="100%">
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Update" />&nbsp;<asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />&nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>

                </div>
            </div>
        </div>
</asp:Content>
