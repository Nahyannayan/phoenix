﻿Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports GemBox.Spreadsheet
Imports System.Data.SqlClient
Imports System.Threading.Tasks
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_DebtfollowupPayplanUpload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property VSgvExcelImport() As DataTable
        Get
            Return ViewState("gvExcelImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvExcelImport") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImport)
        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or (MainMnu_code <> "F733049") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            lnkXcelFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("DEBTFOLLOWUP") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/" & "DebtFollowupPayplanFormatFile" & ".xls")
            lnkXcelFormat.Attributes.Add("onClick", "return ConfirmFormatFileDownload();")
        End If
    End Sub

    Sub BindExcel()
        Me.gvExcelImport.DataSource = VSgvExcelImport
        Me.gvExcelImport.DataBind()
    End Sub
    Private Sub ImportExcelSheet()
        Try
            lblError.Text = ""
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If
            End If
            If File.Exists(FileName) Then
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                'Dim ef As New GemBox.Spreadsheet.ExcelFile
                Dim workbook = ExcelFile.Load(FileName)
                ' Select active worksheet.
                Dim worksheet = workbook.Worksheets.ActiveWorksheet
                Dim dtXL As New DataTable
                If worksheet.Rows.Count > 1 Then
                    dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With
                            {
                             .ColumnHeaders = True,
                             .StartRow = 0,
                             .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count,
                             .NumberOfRows = worksheet.Rows.Count,
                             .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                            })
                End If
                ' File delete first 
                File.Delete(FileName)
                If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then
                    ViewState("ExcelData") = dtXL

                    Dim dvxl As DataView = New DataView(dtXL)
                    VSgvExcelImport = dvxl.ToTable()
                    Dim Columns As DataColumnCollection = VSgvExcelImport.Columns
                    If Not Columns.Contains("bVALID") Then
                        VSgvExcelImport.Columns.Add("bVALID", GetType(Boolean), True)
                    End If
                    If Not Columns.Contains("ERROR_MSG") Then
                        VSgvExcelImport.Columns.Add("ERROR_MSG", GetType(String), "")
                    End If
                    BindExcel()
                    Me.trGvImport.Visible = True
                    lblMessage.CssClass = "alert alert-info"
                    Me.lblMessage.Text = "Click 'Proceed' button to validate the data"
                    'gvExcelImport.Columns(9).Visible = False 'hides the error message column initially
                    Me.btnProceedImpt.Visible = True
                    Me.btnImport.Visible = False
                End If
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog("Unable to load datatable from file - import debt follow up payplan()")
        End Try
    End Sub
    Sub DisableControls(ByVal bEnabled As Boolean)
        FileUpload1.Enabled = bEnabled
    End Sub
    Public Function GetUploadStatus() As DataTable
        GetUploadStatus = Nothing
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sBsuId")
        pParms(1) = New SqlClient.SqlParameter("@BATCH_NO", SqlDbType.Int)
        pParms(1).Value = ViewState("FAI_BATCHNO")

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_DEBT_FOLLOWUP_PAYPLAN_UPLOAD_STATUS", pParms)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            GetUploadStatus = ds.Tables(0)
        End If
    End Function

    Protected Sub gvExcelImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExcelImport.PageIndexChanging
        Me.gvExcelImport.PageIndex = e.NewPageIndex
        BindExcel()
    End Sub
    Protected Sub btnPostback_Click(sender As Object, e As EventArgs)
        If Me.h_STATUS.Value = "0" Then
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            '    ddlAcademicYear.SelectedItem.Value & " - " & txtFrom.Text & " - " & ddBusinessunit.SelectedItem.Value, _
            '    "Insert", Page.User.Identity.Name.ToString, Me.Page)
            Dim errormsg As String = h_errormessage.Value
            Dim IsSuccess As String = h_IsSuccess.Value

            'Clear_All()
            'Me.lblMessage.Text = ""
            Me.btnCommitImprt.Visible = False
            Me.btnProceedImpt.Visible = False
            Me.btnImport.Visible = True
            Me.btnClear.Visible = True

            'Me.lblError.Text = errormsg
            usrMessageBar.ShowNotification(errormsg, If(IsSuccess = "1", UserControls_usrMessageBar.WarningType.Success, UserControls_usrMessageBar.WarningType.Danger))
            h_Processeing.Value = ""
            h_IsSuccess.Value = 0
            VSgvExcelImport = GetUploadStatus()
            BindExcel()
        End If
    End Sub

    Protected Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        VSgvExcelImport = Nothing
        BindExcel()
        ImportExcelSheet()
    End Sub

    Protected Sub btnProceedImpt_Click(sender As Object, e As EventArgs) Handles btnProceedImpt.Click
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Using con As New SqlConnection(str_conn)
            con.Open()
            Dim stTrans As SqlTransaction = con.BeginTransaction
            Try
                Using cmd As New SqlCommand("[DataImport].IMPORT_DEBT_FOLLOWUP_PAYPLAN", con, stTrans)
                    cmd.CommandTimeout = 0
                    cmd.CommandType = CommandType.StoredProcedure
                    Dim sqlParam(9) As SqlParameter
                    Dim dtParam As DataTable = VSgvExcelImport
                    If dtParam.Columns.Contains("bVALID") Then
                        dtParam.Columns.Remove("bVALID")
                    End If
                    If dtParam.Columns.Contains("ERROR_MSG") Then
                        dtParam.Columns.Remove("ERROR_MSG")
                    End If
                    'If (dtParam.Columns.Contains("SLNO")) Then
                    '    dtParam.Columns.Remove("SLNO")
                    'End If
                    dtParam.AcceptChanges()
                    sqlParam(0) = New SqlParameter("@DT_XL", dtParam)
                    cmd.Parameters.Add(sqlParam(0))
                    sqlParam(1) = New SqlParameter("@LOG_BSU_ID", Session("sBsuId"))
                    cmd.Parameters.Add(sqlParam(1))
                    sqlParam(2) = New SqlParameter("@LOG_USER", Session("sUsr_name"))
                    cmd.Parameters.Add(sqlParam(2))
                    sqlParam(3) = New SqlParameter("@RET_VAL", 0)
                    sqlParam(3).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(3))
                    sqlParam(4) = New SqlParameter("@BATCH_NO", 0)
                    sqlParam(4).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(4))

                    Using dr As SqlDataReader = cmd.ExecuteReader()
                        Dim dttb As New DataTable
                        dttb.Load(dr)
                        If sqlParam(3).Value <> 0 Then
                            stTrans.Rollback()
                            'stTrans.Commit()
                            Me.btnImport.Visible = True
                            Me.btnCommitImprt.Visible = False
                            btnProceedImpt.Visible = False
                            lblMessage.CssClass = "alert alert-danger"
                            lblMessage.Text = "Please correct the excel row(s) data for the below highlighted and try to upload again."
                        Else
                            stTrans.Commit()
                            btnProceedImpt.Visible = False
                            Me.btnCommitImprt.Visible = True
                            ViewState("FAI_BATCHNO") = sqlParam(4).Value
                            hdBATCHNO.Value = sqlParam(4).Value
                            DisableControls(False)
                            lblMessage.CssClass = "alert alert-info"
                            lblMessage.Text = "Click on 'Commit' button to complete the upload process"
                        End If
                        If Not dttb Is Nothing AndAlso dttb.Rows.Count > 0 Then
                            'gvExcelImport.Columns(9).Visible = True 'shoes the error message column
                            VSgvExcelImport = dttb
                            'Dim Counter As Int32 = 0
                            If (Not dttb.Columns.Contains("SLNO")) Then
                                dttb.Columns.Add("SLNO")
                                'Dim dtDataTable As DataTable = ViewState("ExcelData")
                                For i As Integer = 0 To dttb.Rows.Count - 1
                                    dttb.Rows(i)("SLNO") = i + 1 ' DirectCast(ViewState("ExcelData"), DataTable).Rows(i)("SLNO")
                                Next
                            End If
                            BindExcel()
                        End If
                    End Using
                    If con.State = ConnectionState.Open Then
                        con.Close()
                    End If

                End Using
            Catch ex As Exception
                UtilityObj.Errorlog("DebtfollowupPayplanUpload - BtnProceedImport(), Error:" & ex.Message, "PHOENIX")
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
            End Try
        End Using
    End Sub

    Protected Sub btnCommitImprt_Click(sender As Object, e As EventArgs) Handles btnCommitImprt.Click
        h_Processeing.Value = "1"
        Session("API_ERROR-DFUP") = False
        ' Kick off the actual, private long-running process in a new Task
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim task As Task
        task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                Dim flag As Boolean = True
                                                                Try
                                                                    Dim sqlParam(2) As SqlParameter
                                                                    objConn.Open()
                                                                    transaction = objConn.BeginTransaction("SampleTransaction")
                                                                    Dim cmd As New SqlCommand
                                                                    cmd.Dispose()
                                                                    cmd = New SqlCommand("FEES.COMMIT_DEBT_FOLLOWUP_PAYPLAN", objConn, transaction)
                                                                    cmd.CommandType = CommandType.StoredProcedure
                                                                    cmd.CommandTimeout = 0
                                                                    Dim retmsg As String = ""
                                                                    sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retmsg, SqlDbType.VarChar, True, 200)
                                                                    cmd.Parameters.Add(sqlParam(0))
                                                                    sqlParam(1) = Mainclass.CreateSqlParameter("@BATCH_NO", ViewState("FAI_BATCHNO"), SqlDbType.Int)
                                                                    cmd.Parameters.Add(sqlParam(1))
                                                                    cmd.ExecuteNonQuery()
                                                                    retmsg = sqlParam(0).Value
                                                                    If retmsg = "" Then
                                                                        transaction.Commit()
                                                                        System.Threading.Thread.Sleep(1000)
                                                                        h_Processeing.Value = ""
                                                                    Else
                                                                        transaction.Rollback()
                                                                        UtilityObj.Errorlog("IMPORT_DEBTFOLLOWUP - Rollback - " & retmsg, "PHOENIX")
                                                                        h_Processeing.Value = ""
                                                                        Session("API_ERROR-DFUP") = True
                                                                        Exit Sub
                                                                    End If
                                                                Catch ex As Exception
                                                                    System.Threading.Thread.Sleep(1500)
                                                                    h_Processeing.Value = ""
                                                                    Session("API_ERROR-DFUP") = True
                                                                    UtilityObj.Errorlog("IMPORT_DEBTFOLLOWUP - Exception: " & ex.Message, "PHOENIX")
                                                                Finally
                                                                    If objConn.State = ConnectionState.Open Then
                                                                        objConn.Close()
                                                                    End If
                                                                End Try
                                                            End Sub)
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        lblError.Text = ""
        DisableControls(True)
        VSgvExcelImport = Nothing
        BindExcel()
        Me.lblMessage.Text = ""
        lblMessage.CssClass = ""
        Me.btnProceedImpt.Visible = False
        Me.btnCommitImprt.Visible = False
        Me.btnImport.Visible = True
        ViewState("FAI_BATCHNO") = 0
        hdBATCHNO.Value = 0
    End Sub
End Class
