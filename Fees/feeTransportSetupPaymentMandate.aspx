﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTransportSetupPaymentMandate.aspx.vb" Inherits="Fees_feeTransportSetupPaymentMandate" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="true">

    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
    function CheckForPrint() {
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            showModelessDialog('FeeReceiptTransport.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
            document.getElementById('<%= h_print.ClientID %>').value = '';
        }
    }
    );
        function CheckNumber(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(2);
        }

        function getStudent() {
            var sFeatures, url, stuName;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            stuName = '<%= txtStudentname.ClientID %>';
            var NameandCode;
            var result;
            var prevAcd = document.getElementById('<%= h_PREV_ACD.ClientID %>').value;
            var BSU_ID = document.getElementById('<%= ddlBusinessunit.ClientID %>').value;

            <%-- url = "ShowStudentTransport.aspx?type=STU_TRAN&COMP_ID=-1&bsu=" + BSU_ID;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%= h_Student_no.ClientID %>').value = NameandCode[0];
            document.getElementById(stuName).value = NameandCode[1];
            document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
            return true;
        }--%>

            var url = "ShowStudentTransport.aspx?type=STU_TRAN&COMP_ID=-1&bsu=" + BSU_ID;
            var oWnd = radopen(url, "pop_getstud");
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%= h_Student_no.ClientID %>').value = NameandCode[0];
                document.getElementById(stuName).value = NameandCode[1];
                document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                __doPostBack('<%=txtStdNo.ClientID%>', 'TextChanged');
            }
        }


        

        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(2);
            return true;
        }

        function get_Bank() {
            var sFeatures;
            sFeatures = "dialogWidth: 509px; ";
            sFeatures += "dialogHeight: 434px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

         
           <%-- result = window.showModalDialog("..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
           

            return false;
        }--%>
            var url = "..\/accounts\/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value;
            var oWnd = radopen(url, "pop_getbank");
                        
        } 

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtBankDescr.ClientID%>', 'TextChanged');
            }
        }


function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getbank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getstud" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>  Payment Mandate Setup-Direct Debit
        </div>
        <div class="card-body">
            <div class="table-responsive"> 

    <asp:HiddenField ID="h_Bank2" runat="server" />

    <table align="center" width="100%">
        <tr>
            <td colspan="4" align="left">
                <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
    </table>
    <table align="center" width="100%">
        
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span></td>
                
                <td align="left" width="30%">
                    <asp:DropDownList ID="ddlBusinessunit" runat="server" DataSourceID="odsSERVICES_BSU_M"
                                    DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"   AutoPostBack="True" TabIndex="5"
                        SkinID="DropDownListNormal">
                    </asp:DropDownList>
                </td>
            <td width="20%"></td>
            <td width="30%"></td>
        </tr>
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Bank A/C</span>
            </td>
            
            <td align="left" width="30%">
                <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True"></asp:TextBox>
                <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank(); return false;" />
                </td>
            <td align="left" width="20%">
                <asp:TextBox ID="txtBankDescr" runat="server" ></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Select Student</span>
            </td>
            
            <td align="left">
                <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True"></asp:TextBox>
                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getStudent();return false;" />
                </td>
            <td align="left">
                <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Payee Name</span>
            </td>
            
            <td align="left">
                <asp:TextBox ID="txtpayeename" runat="server" ></asp:TextBox>
            </td>
            
            <td align="left">
               <span class="field-label"> Payee Bank</span>
            </td>
            
            <td align="left">
                <asp:DropDownList ID="ddbank" runat="server">
                </asp:DropDownList>
                <%# Eval("SPM_BANK_ACT_ID")%>
            </td>            
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Branch</span>
            </td>
           
            <td align="left">
                <asp:DropDownList ID="DropDownList1" runat="server" 
                    DataSourceID="SqlDataSourceBranch" DataTextField="EMR_DESCR" 
                    DataValueField="EMR_CODE">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSourceBranch" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>" 
                    SelectCommand="SELECT [EMR_CODE], [EMR_DESCR] FROM [EMIRATE_M]">
                </asp:SqlDataSource>
            </td>
        
            <td align="left">
                <span class="field-label">Payee Account</span>
            </td>
            
            <td align="left">
                <asp:TextBox ID="txtAccount" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Amount</span>
            </td>
           
            <td align="left">
                <asp:TextBox ID="txtAmmount" runat="server"></asp:TextBox>
            </td>
        
            <td align="left">
                <span class="field-label">Start Date</span>
            </td>
            
            <td align="left">
                <asp:TextBox ID="txtFrom" runat="server" TabIndex="15" AutoPostBack="True"></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Notes</span></td>
            
            <td align="left">
                <asp:TextBox ID="txtremarks" runat="server"></asp:TextBox>
            </td>
       
            <td align="left">
                <span class="field-label">Email Id</span></td>
            
            <td align="left">
                <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    
    <table align="center" width="100%">
        <tr>
            <td align="center" valign="bottom">
            <div id ="Editdiv" runat="server">
                <asp:Button ID="btndisc" runat="server" CssClass="button" TabIndex="155" 
                    Text="Discontinue"/>
                <asp:Button ID="btnDelete" runat="server" CssClass="button" TabIndex="155" 
                    Text="Delete" />
                <asp:Button ID="btnupdate" runat="server" CssClass="button" TabIndex="155" 
                    Text="Update"/>
            </div>
               
                <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="155" 
                    Text="Save"/>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" TabIndex="158" />&nbsp;
            </td>
        </tr>
    </table>

    

    <table width="100%">
        <tr>
            <td class="title-bg">
            Payment Mandate</td>
        </tr>
        <tr>
            <td align="left">
    <asp:GridView ID="Griddata" runat="server" CssClass="table table-bordered table-row" 
        AutoGenerateColumns="false" 
        EmptyDataText="No Records Found." 
        PageSize="15" ShowFooter="true" Width="100%">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    Bank A/C
                </HeaderTemplate>
                <ItemTemplate>
                    <center>
                         <%# Eval("SPM_BANK_ACT_ID")%>
                    </center>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                   Student Name
                            
                </HeaderTemplate>
                <ItemTemplate>
                   
                                                <%# Eval("STU_NAME")%>
                 
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Payee Name
                           
                </HeaderTemplate>
                <ItemTemplate>
                     
                        <%#Eval("SPM_PAYEE_NAME")%>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Payee Bank
                </HeaderTemplate>
                <ItemTemplate>
                                            <%# Eval("BNK_DESCRIPTION")%>
                                        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Branch
                </HeaderTemplate>
                <ItemTemplate>
                                            <%# Eval("EMR_DESCR")%>
                                        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Account
                </HeaderTemplate>
                <ItemTemplate>
                    <center>
                                                <%# Eval("SPM_ACCOUNT")%>
                                            </center>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Amount
                </HeaderTemplate>
                <ItemTemplate>
                    <center>
                                                <%# Eval("SPM_AMOUNT")%> 
                                            </center>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                   Start Date
                </HeaderTemplate>
                <ItemTemplate>
                                            <%# Eval("SPM_STARTDT", "{0:dd/MMM/yyy}")%>
                                        </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Entry Date
                </HeaderTemplate>
                <ItemTemplate>
                    <center>
                                                <%# Eval("SPM_LOGDT", "{0:dd/MMM/yyy}")%>
                                            </center>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <RowStyle CssClass="griditem" Wrap="False" />
        <EmptyDataRowStyle Wrap="False" />
        <%--<SelectedRowStyle CssClass="Green" Wrap="False" />--%>
        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
        <EditRowStyle Wrap="False" />
        <%--<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
    </asp:GridView>
    

            </td>
        </tr>
    </table>
    <br />
    <ajaxToolkit:CalendarExtender ID="C2" TargetControlID="txtFrom" Format="dd/MMM/yyyy" PopupButtonID="imgfrom" runat="server"></ajaxToolkit:CalendarExtender>
    
        <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
            TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetFEES_M_TRANSPORT" TypeName="FeeCommon"></asp:ObjectDataSource>
    <asp:HiddenField ID="h_Student_no" runat="server" />
    <asp:HiddenField ID="H_Location" runat="server" />
    <asp:HiddenField ID="h_du" runat="server" />
    <asp:HiddenField ID="h_PREV_ACD" runat="server" />
    <asp:HiddenField ID="h_print" runat="server" />
    <asp:HiddenField ID="h_rid" runat="server" />

                </div>
            </div>
        </div>
</asp:Content>
