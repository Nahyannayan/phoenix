<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeTransportReminderSMS.aspx.vb" Inherits="Fees_FeeTransportReminderSMS" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">



        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("ChkSelAll").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Fee Reminder SMS"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit </span> </td>
                         <td align="left" width="80%">
                            <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:GridView ID="gvFEEConcessionDet" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="ChkSelect" type="checkbox" runat="server" value='<%# Bind("FRH_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACAD. YEAR">
                                        <HeaderTemplate>
                                            Academic Year<br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACY_DESCR" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AS ON DATE">
                                        <HeaderTemplate>
                                            As On Date<br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAsOnDate" runat="server" Text='<%# Bind("FRH_ASONDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REM. TITLE">
                                        <HeaderTemplate>
                                            Rem. Title<br />
                                            <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FRH_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LEVEL">
                                        <HeaderTemplate>
                                            Level<br />
                                            <asp:TextBox ID="txtConcession" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnConcession" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("FRH_Level") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FRH_DT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GRD_DISPLAY" HeaderText="Grade"></asp:BoundField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                                TabIndex="26" Text="Send SMS" /></td>
                    </tr>
                </table>

                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sUsr_name" Type="String" DefaultValue="" Name="USR_ID"></asp:SessionParameter>
                        <asp:SessionParameter SessionField="sBsuid" Type="String" DefaultValue="" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>



