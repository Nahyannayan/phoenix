Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeStudentNotes
    Inherits BasePage
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Me.lblError.Text = ""
                ViewState("datamode") = "add"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")


                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_COLLECTION Then
                    lblError.Text = "Access Denied"
                    Me.btnAddDetails.Enabled = False
                Else
                    Me.btnAddDetails.Enabled = True
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, OASISConstants.MNU_FEE_TRANSPORT_FEE_COLLECTION)
                    Call AccessRight.setpage(Page, ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("stuno") <> "" Then
                    lblStNo.Text = Request.QueryString("stuno")
                    LoadStudent(lblStNo.Text)
                    LoadGrid()
                Else
                    gvStNotes.DataBind()
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Sub LoadStudent(ByVal STU_NO As String)
        Dim dtSTU As DataTable
        Dim Qry As String
        Qry = "SELECT A.STU_ID,ISNULL(STU_FIRSTNAME,'')+ISNULL(STU_MIDNAME,'')+ISNULL(STU_LASTNAME,'')STU_NAME,GRD_DISPLAY from dbo.STUDENT_M A WITH(NOLOCK) LEFT JOIN " & _
        "dbo.GRADE_M B WITH(NOLOCK) ON A.STU_GRD_ID=B.GRD_ID where STU_NO='" & STU_NO & "'"
        dtSTU = Mainclass.getDataTable(Qry, ConnectionManger.GetOASISConnectionString)
        If dtSTU.Rows.Count > 0 Then
            h_Student_ID.Value = dtSTU.Rows(0)(0).ToString
            Me.lblGrade.Text = dtSTU.Rows(0)(2).ToString
            Me.lblStuName.Text = dtSTU.Rows(0)(1).ToString
        End If
    End Sub


    Protected Sub btnAddDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetails.Click

        If h_Student_ID.Value.ToString <> "" Then
            If Me.txtRemarks.Text.Trim <> "" Then
                SaveNotes()
            End If
        Else
            Me.lblError.Text = "Student Details not found!!"
        End If


    End Sub
    Sub LoadGrid()
        Dim dtNotes As DataTable
        dtNotes = Mainclass.getDataTable("SELECT STN_ID,STN_STU_ID,STN_DATE,STN_NOTES,STN_USER_NAME from FEES.STUFEECOLLNOTES WITH(NOLOCK) where STN_STU_ID='" & Me.h_Student_ID.Value & "' order by STN_DATE desc", ConnectionManger.GetOASIS_FEESConnectionString)
        If dtNotes.Rows.Count > 0 Then
            ViewState("gvStNotes") = dtNotes
            bindGrid()
        End If
    End Sub
    Sub bindGrid()
        Me.gvStNotes.DataSource = ViewState("gvStNotes")
        Me.gvStNotes.DataBind()
    End Sub
    Private Sub SaveNotes()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Me.lblError.Text = ""
        Try

            Dim qry As String = ""
            Dim cmdCollection As New SqlCommand
            cmdCollection.CommandType = CommandType.Text
            cmdCollection.CommandText = "INSERT INTO FEES.STUFEECOLLNOTES(STN_STU_ID,STN_DATE,STN_USER_NAME,STN_NOTES)" & _
            "VALUES('" & Me.h_Student_ID.Value & "',getdate(),'" & Session("sUsr_name") & "','" & Me.txtRemarks.Text.Trim & "')"
            cmdCollection.Transaction = stTrans
            cmdCollection.Connection = stTrans.Connection
            cmdCollection.CommandTimeout = 0
            cmdCollection.ExecuteNonQuery()

            stTrans.Commit()
            LoadGrid()
            Me.txtRemarks.Text = ""
        Catch ex As Exception
            stTrans.Rollback()
            Me.lblError.Text = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    'Protected Sub lbEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'Dim rowindex As Int16 = sender.Parent.Parent.RowIndex
    '    'gvStNotes.SelectedIndex = rowindex
    '    'Me.txtRemarks.Text = Me.gvStNotes.Rows(rowindex).Cells(1).Text
    '    'Me.h_STN_ID.Value = Me.gvStNotes.SelectedDataKey.Item(0)
    'End Sub

    'Protected Sub lbDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    'End Sub

    Protected Sub gvStNotes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStNotes.PageIndexChanging
        Me.gvStNotes.PageIndex = e.NewPageIndex
        bindGrid()
    End Sub
End Class
