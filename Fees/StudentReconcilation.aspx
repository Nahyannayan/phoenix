<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudentReconcilation.aspx.vb" Inherits="Fees_StudentReconcilation" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
<%--    <script type="text/javascript" language="javascript">
        function FindStudent() {
            var sFeatures, url;
            sFeatures = "dialogWidth: 800px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var BsuId = document.getElementById('<%=drpBusinessunit.ClientID %>').value
            var TYPE = 'S';
            var selType = 's';
            var selACD_ID = "N"
            url = "ShowStudent.aspx?TYPE=REFER&bsu=" + BsuId;
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
        document.getElementById('<%=txtStuNo.ClientID %>').value = NameandCode[2];
        document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
    }
    return true;
}
    </script>--%>
    <script>    
        function FindStudent() {
            var url;

            var NameandCode;
            var result;
            var BsuId = document.getElementById('<%=drpBusinessunit.ClientID %>').value
            var TYPE = 'S';
            var selType = 's';
            var selACD_ID = "N"
            url = "ShowStudent.aspx?TYPE=REFER&bsu=" + BsuId;
            var oWnd = radopen(url, "pop_student");

}

        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
          
        document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
        document.getElementById('<%=txtStuNo.ClientID %>').value = NameandCode[2];
		 __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');
            }
        }
  
function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Student Reconcilation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">

                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="drpBusinessunit" runat="server" AutoPostBack="True"
                                            TabIndex="2" SkinID="DropDownListNormal">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">From Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2"  OnTextChanged="txtFrom_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="MyCalendar" runat="server"
                                            PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td   class="matters"><span class="field-label">To Date</span> </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTo" runat="server"   AutoPostBack="True" TabIndex="2" OnTextChanged="txtTo_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
       <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
           PopupButtonID="imgTo" TargetControlID="txtTo" Format="dd/MMM/yyyy">
       </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Student</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtStuNo" runat="server" AutoPostBack="True" OnTextChanged="txtStudName_TextChanged"
                                             ></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif"
                                            OnClientClick="FindStudent();return false;" TabIndex="7"></asp:ImageButton></td>
                                    <td><span class="field-label">Student Name</span></td>
                                        
                                    <td><asp:TextBox ID="txtStudName" runat="server" AutoPostBack="True" OnTextChanged="txtStudName_TextChanged"
                                              ReadOnly="True"></asp:TextBox>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Grade</span></td>
                                    <td>
                                        <asp:TextBox ID="txtGrade" runat="server" AutoPostBack="True" OnTextChanged="txtStudName_TextChanged"
                                              ReadOnly="True"></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%">
                                <tr>
                                    <td width="30%">
                                        <asp:ImageButton ID="btnPrevious" runat="server" CausesValidation="False" ImageAlign="Top"
                                            ImageUrl="~/Images/Misc/LEFT1.gif" ToolTip="Previous Record" OnClick="btnPrevious_Click" /></td>
                                    <td width="40%" align="center">
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                                    </td>
                                    <td width="30%" align="right">
                                        <asp:ImageButton ID="btnNext" runat="server" CausesValidation="False" ImageAlign="Top" ImageUrl="~/Images/Misc/RIGHT1.gif"
                                            ToolTip="Next Record" OnClick="btnNext_Click" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%">
                                <tr>
                                    <td rowspan="5" valign="top" runat="server" id="TbSchedule" align="left" width="60%">
                                        <table width="100%" class="BlueTable_simple">
                                            <tr>
                                                <td class="title-bg" align="center">Fee Charge Information</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvSchedule" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-row table-bordered"
                                                        AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:BoundField DataField="ONE" HeaderText="Date"></asp:BoundField>
                                                            <asp:BoundField DataField="FIVE" HeaderText="Source" />
                                                            <asp:BoundField DataField="FOUR" HeaderText="Narration">
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="THREE" HeaderText="Dr/Cr">
                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SIX" HeaderText="Amount">
                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Area">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="linkArea" runat="server" Text='<%# Bind("SEVEN") %>' OnClientClick="return false;"> </asp:LinkButton>
                                                                    <asp:Label ID="labAcdId" runat="server" Text='<%# Bind("NINE") %>' Visible="False"></asp:Label>
                                                                    <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2"
                                                                        runat="server" TargetControlID="linkArea"
                                                                        Position="Bottom" PopupControlID="Panel1"
                                                                        DynamicServiceMethod="GetDynamicContent" DynamicContextKey='<%# Eval("EIGHT") %>' DynamicControlID="Panel1">
                                                                    </ajaxToolkit:PopupControlExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td runat="server" id="TbService" align="center" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td class="title-bg" align="center">Student Service Information</td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvService" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-row table-bordered"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="ONE" HeaderText="From Date"></asp:BoundField>
                                                <asp:BoundField DataField="TWO" HeaderText="To Date">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Area">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkArea" runat="server" Text='<%# Bind("THREE") %>' OnClientClick="return false;"> </asp:LinkButton>
                                                        <asp:Label ID="labAcdId" runat="server" Text='<%# Bind("NINE") %>' Visible="False"></asp:Label>
                                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1"
                                                            runat="server" TargetControlID="linkArea"
                                                            Position="Left" PopupControlID="Panel1"
                                                            DynamicServiceMethod="GetDynamicContent" DynamicContextKey='<%# Eval("FIVE") %>' DynamicControlID="Panel1">
                                                        </ajaxToolkit:PopupControlExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FOUR" HeaderText="Remarks" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td runat="server" id="TbRequest" align="center" valign="top">
                                        <table width="100%" class="BlueTable">
                                            <tr>
                                                <td class="title-bg" align="center">Change Area Request Details</td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvAreaChange" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-row table-bordered"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="ONE" HeaderText="Start Date"></asp:BoundField>
                                                <asp:BoundField DataField="FOUR" HeaderText="Old Area" />
                                                <asp:BoundField DataField="THREE" HeaderText="New Area">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TWO" HeaderText="Approve Date">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FIVE" HeaderText="Approved By" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td runat="server" id="TbDiscontinue" align="center" valign="top">
                                        <table width="100%" class="BlueTable">
                                            <tr>
                                                <td class="title-bg" align="center">Service Discontinuation</td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvDiscontinue" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-row table-bordered"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="ONE" HeaderText="From Date"></asp:BoundField>
                                                <asp:BoundField DataField="TWO" HeaderText="To Date">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="THREE" HeaderText="Area">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FOUR" HeaderText="Approve Date" />
                                                <asp:BoundField DataField="FIVE" HeaderText="Approved By" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td runat="server" id="TbChange" align="center" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td class="title-bg" align="center">New Transport Requests</td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvTrnRequest" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-row table-bordered"
                                            AutoGenerateColumns="False" CaptionAlign="Top">
                                            <Columns>
                                                <asp:BoundField DataField="ONE" HeaderText="From Date"></asp:BoundField>
                                                <asp:BoundField DataField="THREE" HeaderText="Pick Up">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FOUR" HeaderText="Drop Off" />
                                                <asp:BoundField DataField="TWO" HeaderText="Approve Date">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FIVE" HeaderText="Approved By" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:Panel ID="panel1" runat="server"></asp:Panel>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
