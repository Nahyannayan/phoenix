﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_popFeeReminder
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Public Property StudentIds() As String()
        Get
            Return ViewState("students")
        End Get
        Set(ByVal value As String())
            ViewState("students") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ViewState("sid") = Encr_decrData.Decrypt(Request.QueryString("sid").Replace(" ", "+"))
            ViewState("frhid") = Encr_decrData.Decrypt(Request.QueryString("frhid").Replace(" ", "+"))
            ViewState("arrayindex") = 0
            ClearAll()
            LoadReminderData()
        End If
    End Sub
    Private Sub LoadReminderData()
        If Not ViewState("sid") Is Nothing Then
            Dim StuIds() As String = ViewState("sid").ToString().Split("|")
            If StuIds.Length > 0 Then
                StudentIds = StuIds
                ViewState("arrayindex") = 0
                GetReminder(ViewState("arrayindex"))
            End If
        End If
    End Sub
    Private Sub ClearAll()
        lblToEmail.Text = ""
        ltrlHtml.Text = ""
        lblToMobile.Text = ""
        ltrlMobile.Text = ""
    End Sub
    Private Sub GetReminder(ByVal ArrayIndex As Integer)
        Try
            lblIndex.Text = (ArrayIndex + 1) & " out of " & StudentIds.Length
            If ArrayIndex >= 0 And ArrayIndex < StudentIds.Length Then
                Dim StuId As Long = StudentIds(ArrayIndex)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.BigInt)
                pParms(0).Value = Session("sBsuId")
                pParms(1) = New SqlClient.SqlParameter("@FRD_STU_ID", SqlDbType.BigInt)
                pParms(1).Value = StuId
                pParms(2) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.BigInt)
                pParms(2).Value = ViewState("frhid")

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.PREVIEW_AUTO_FEE_REMINDER", pParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    lblToEmail.Text = ds.Tables(0).Rows(0)("PRIMARY_EMAIL_ID").ToString
                    If ds.Tables(0).Rows(0)("SECONDARY_EMAIL_ID").ToString <> "" Then
                        lblToEmail.Text += "," + ds.Tables(0).Rows(0)("SECONDARY_EMAIL_ID").ToString
                    End If
                    ltrlHtml.Text = ds.Tables(0).Rows(0)("EMAIL_TEMPLATE").ToString

                    lblToMobile.Text = ds.Tables(0).Rows(0)("PRIMARY_MOBILE_NO").ToString
                    If ds.Tables(0).Rows(0)("SECONDARY_MOBILE_NO").ToString <> "" Then
                        lblToMobile.Text += "," + ds.Tables(0).Rows(0)("SECONDARY_MOBILE_NO").ToString
                    End If
                    ltrlMobile.Text = ds.Tables(0).Rows(0)("SMS_TEMPLATE").ToString
                Else
                    ClearAll()
                End If
            Else
                ClearAll()
            End If
        Catch ex As Exception
            ClearAll()
        End Try
    End Sub

    Protected Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        ViewState("arrayindex") = ViewState("arrayindex") + 1
        If ViewState("arrayindex") >= StudentIds.Length Then
            ViewState("arrayindex") = 0
        End If
        GetReminder(ViewState("arrayindex"))
    End Sub

    Protected Sub btnPrevious_Click(sender As Object, e As EventArgs) Handles btnPrevious.Click
        ViewState("arrayindex") = ViewState("arrayindex") - 1
        If ViewState("arrayindex") < 0 Then
            ViewState("arrayindex") = StudentIds.Length - 1
        End If
        GetReminder(ViewState("arrayindex"))
    End Sub
End Class
