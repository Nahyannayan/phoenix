Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeReceipt_TAX
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Property BALANCE() As Double
        Get
            Return ViewState("BALANCE")
        End Get
        Set(ByVal value As Double)
            ViewState("BALANCE") = value
        End Set
    End Property
    Private Property BSU_CURRENCY() As String
        Get
            Return ViewState("BSU_CURRENCY")
        End Get
        Set(ByVal value As String)
            ViewState("BSU_CURRENCY") = value
        End Set
    End Property
    Private Property BALANCE_TEXT() As String
        Get
            Return ViewState("BALANCE_TEXT")
        End Get
        Set(ByVal value As String)
            ViewState("BALANCE_TEXT") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'Page.Title = FeeCollectionOnlineBB.GetTransportTitle()7bad10

                If Session("sUsr_name") & "" = "" Then
                    Response.Redirect("~/login.aspx")
                End If
                gvPayments.Attributes.Add("bordercolor", "#000095")
                gvFeeDetails.Attributes.Add("bordercolor", "#000095")

                Dim formatstring As String = Session("BSU_DataFormatString")
                formatstring = formatstring.Replace("0.", "###,###,###,##0.")
                DirectCast(gvFeeDetails.Columns(1), BoundField).DataFormatString = "{0:" & formatstring & "}"
                DirectCast(gvPayments.Columns(4), BoundField).DataFormatString = "{0:" & formatstring & "}"

                BALANCE = 0
                Select Case Request.QueryString("type")
                    Case "REC"
                        If Request.QueryString("id") <> "" Then
                            Dim iscolln, isenq As Boolean
                            iscolln = Encr_decrData.Decrypt(Request.QueryString("iscolln").Replace(" ", "+"))
                            isenq = Encr_decrData.Decrypt(Request.QueryString("isenq").Replace(" ", "+"))
                            PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")), isenq, iscolln, _
                            Encr_decrData.Decrypt(Request.QueryString("user").Replace(" ", "+")), formatstring)
                        End If
                End Select
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub PrintReceipt(ByVal p_BSU_ID As String, ByVal p_Receiptno As String, _
        ByVal IsEnquiry As Boolean, ByVal IsCollection As Boolean, ByVal USER_NAME As String, _
        ByVal formatstring As String)
        If IsEnquiry Then
            lblStudentCaption.Text = "Enquiry ID"
        Else
            lblStudentCaption.Text = "Student ID"
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
        pParms(1).Value = p_Receiptno
        pParms(2) = New SqlClient.SqlParameter("@bENQUIRY", SqlDbType.Bit)
        pParms(2).Value = IsEnquiry 'Not used
        pParms(3) = New SqlClient.SqlParameter("@bCOLLECTION", SqlDbType.Bit)
        pParms(3).Value = IsCollection
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.FeeReceipt_TAX", pParms)
        Dim strFilter As String = "  FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & p_BSU_ID & "' "
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            imgLogo.ImageUrl = "~/Common/GetLogo.aspx?BSU_ID=" & p_BSU_ID
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_ADDRESS").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_CONTACT").ToString
            lblGSTRegistration.Text = ds.Tables(0).Rows(0)("BUS_TAX_REGN_DETAIL").ToString
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblDate.Text = Format(ds.Tables(0).Rows(0)("FCL_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY"))
            lblRecno.Text = ds.Tables(0).Rows(0)("FCL_RECNO").ToString
            lblSchoolName.Text = ds.Tables(0).Rows(0)("BSU_NAME").ToString
            lblSchoolNameCheque.Text = ds.Tables(0).Rows(0)("BSU_INVOICE_CHQ_FOOTER").ToString
            BSU_CURRENCY = ds.Tables(0).Rows(0)("BSU_CURRENCY")
            lblSchoolnameFavour.Text = lblSchoolName.Text
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO").ToString

            If ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString <> Session("BSU_CURRENCY") And ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString <> "" Then
                lblCurrency.Visible = True
                lblCurrencyLabel.Visible = True
                lblCurrColon.Visible = True
                gvFeeDetails.Columns(4).Visible = True
                gvFeeDetails.Columns(4).HeaderText = "Total Amount(" & ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString & ")"
                gvPayments.Columns(5).Visible = True
                gvPayments.Columns(5).HeaderText = "Total Amount(" & ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString & ")"
                lblCurrency.Text = ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString & "(" & ds.Tables(0).Rows(0)("FCL_EXCHANGE1").ToString & ")"
            End If

            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblNarration.text = "Narration : " & ds.Tables(0).Rows(0)("FCL_NARRATION")
            Dim mcSpell As New Mainclass
            lbluserloggedin.Text = "Printed by " & USER_NAME & " (Time: " + Now.ToString("dd/MMM/yyyy hh:mm:ss tt") & ")"

            If ds.Tables(0).Rows(0)("Duplicate") = "0" Then
                lblPrintTime.Text = "Ref. " + ds.Tables(0).Rows(0)("FCL_EMP_ID") + " - " + ds.Tables(0).Rows(0)("FCL_COUNTERSERIES").ToString
                lblCopy.Visible = False
            Else
                lblCopy.Visible = True
                lblPrintTime.Text = "Ref. " & ds.Tables(0).Rows(0)("FCL_EMP_ID") & " - " & ds.Tables(0).Rows(0)("FCL_COUNTERSERIES").ToString & " (" & CDate(ds.Tables(0).Rows(0)("FCL_LOGDATE")).ToString("hh:mm:ss tt") & ")"
            End If
            'lblAmount.Text = "Amount in words " + mcSpell.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            BALANCE_TEXT = ds.Tables(0).Rows(0)("FCL_BALANCE_TEXT").ToString
            Dim str_paymnts As String = " EXEC FEES.RECEIPT_FEEANDPAYMENT_DETAILS @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & p_BSU_ID & "'"
            If IsNumeric(ds.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
                BALANCE = CDbl(ds.Tables(0).Rows(0)("FCL_BALANCE"))
                'If ds.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
                '    lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), formatstring)
                'Else
                '    lblBalance.Text = "Advance : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE") * -1, formatstring)
                'End If
                'lblBalance.Text = ds.Tables(0).Rows(0)("FCL_BALANCE_TEXT").ToString
            End If

            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()
            gvFeeDetails.HeaderRow.Cells(3).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True

            gvPayments.DataSource = ds1.Tables(1)
            gvPayments.DataBind()
            Dim objTotal As Object = gvPayments.DataKeys(gvPayments.Rows.Count - 1).Values(2)
            Dim str_Total As String = "0.00"
            If IsNumeric(objTotal) Then
                str_Total = Format(objTotal, "#,##0.00")
            End If
            'Dim str_Total As String = gvPayments.DataKeys(gvPayments.Rows.Count - 1).Values(2) 'gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(4).Text
            Dim columnCount As Integer = gvPayments.FooterRow.Cells.Count

            gvPayments.FooterRow.Cells.Clear()
            gvPayments.FooterRow.Cells.Add(New TableCell)
            gvPayments.FooterRow.Cells(0).ColumnSpan = columnCount
            gvPayments.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Left
            If gvPayments.Columns(5).Visible = True Then
                gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(ds.Tables(0).Rows(0)("FCL_CURRENCY_AMOUNT"), ds.Tables(0).Rows(0)("FCL_CUR_ID"), Session("BSU_ROUNDOFF"))
            Else
                gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(objTotal, BSU_CURRENCY, Session("BSU_ROUNDOFF"))
                'gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(ds.Tables(0).Rows(0)("FCL_AMOUNT"), ds.Tables(0).Rows(0)("BSU_CURRENCY"), Session("BSU_ROUNDOFF"))
            End If
            gvPayments.FooterRow.Cells(0).Font.Bold = True
            columnCount = columnCount - 1
            'Dim str_Total As String = gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(4).Text
            Dim str_TotalFC As String = gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(5).Text
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Clear()
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).ColumnSpan = columnCount - 1
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).Text = "Total"
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).HorizontalAlign = HorizontalAlign.Right

            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).Text = str_Total
            If gvPayments.Columns(5).Visible = True Then
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(2).Text = str_TotalFC
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(2).HorizontalAlign = HorizontalAlign.Right
            End If
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).HorizontalAlign = HorizontalAlign.Right

            If ds.Tables(0).Rows(0)("FCL_STU_TYPE") = "E" Then
                lblStudentCaption.Text = "Enquiry ID"
            Else
                lblStudentCaption.Text = "Student ID"
            End If
        End If
    End Sub

    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #000095 0pt solid; border-right: #000095 1pt dotted; border-top: #000095 1pt dotted; border-bottom: #000095 1pt dotted;"
        Next
        If e.Row.Cells(0).Text = "Total" Then
            e.Row.Font.Bold = True
        End If
        If e.Row.Cells(0).Text.ToLower.Contains("discount") Then
            lblDiscount.Visible = True
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblFCSAmt As Label = e.Row.FindControl("lblFCSAmt")
            Dim lblFEEDESCR As Label = e.Row.FindControl("lblFEEDESCR")
            Dim AMOUNT_DIFFERENCE As Double = 0, FCA_bPDC_CANCELLED As Boolean = False, FCA_PDC_CANCEL_AMOUNT As Double = 0
            Dim FCS_FEE_ID As Int16 = CInt(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCS_FEE_ID"))
            FCA_bPDC_CANCELLED = CBool(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCA_bPDC_CANCELLED"))
            FCA_PDC_CANCEL_AMOUNT = CDbl(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCA_PDC_CANCEL_AMOUNT"))
            AMOUNT_DIFFERENCE = CDbl(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("AMOUNT_DIFFERENCE"))

            Dim diff As Double = 0
            If FCA_bPDC_CANCELLED And FCA_PDC_CANCEL_AMOUNT > 0 Then
                diff = AMOUNT_DIFFERENCE
                Dim tc As TableCell = e.Row.Cells(3)
                tc.Text = "<span>" & Format(diff, "#,##0.00") & "&nbsp;&nbsp;<span style='text-decoration: line-through; color:red;'>" & lblFCSAmt.Text & "</span></span>"
            End If

            If lblFEEDESCR.Text = "Total Paid" Then
                Dim tc As TableCell = e.Row.Cells(3)
                If AMOUNT_DIFFERENCE <> 0 Then
                    tc.Text = "<span>" & Format(AMOUNT_DIFFERENCE, "#,##0.00") & "&nbsp;&nbsp;<span style='text-decoration: line-through; color:red;'>" & lblFCSAmt.Text & "</span></span>"
                Else
                    tc.Text = "<span>" & Format(CDbl(lblFCSAmt.Text), "#,##0.00") & "</span>"
                End If
                lblBalance.Text = BALANCE_TEXT
                'If (BALANCE) > 0 Then
                '    Dim NewBal = BALANCE '(BALANCE - FCA_PDC_CANCEL_AMOUNT)
                '    lblBalance.Text = IIf(NewBal > 0, "Due", "Advance") & " : " & BSU_CURRENCY & " " & Format(Math.Abs(NewBal), "#,##0.00")
                'Else
                '    Dim NewBal = BALANCE '(BALANCE + FCA_PDC_CANCEL_AMOUNT)
                '    lblBalance.Text = IIf(NewBal > 0, "Due", "Advance") & " : " & BSU_CURRENCY & " " & Format(Math.Abs(NewBal), "#,##0.00")
                'End If
            End If
        End If
    End Sub

    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEmail.Click
        Dim RetMessage As String
        RetMessage = DownloadEmailReceipt.DownloadOrEMailReceipt("EMAIL", lblRecno.Text, Session("sBsuId"), True)
        If RetMessage.Contains("successfully emailed") Then
            lblMessage.Text = RetMessage
        End If
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & lblRecno.Text & "','" & hf_FCLID.Value & "','" & RetMessage & "' ")
    End Sub
    Protected Sub gvPayments_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvPayments.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim FCD_bPDC_CANCELLED As New Boolean
            Dim FCD_PDC_CANCELLED_DATE As New Date
            If Not gvPayments.DataKeys(e.Row.RowIndex).Values(0) Is Nothing AndAlso Not gvPayments.DataKeys(e.Row.RowIndex).Values(0) Is DBNull.Value Then
                FCD_bPDC_CANCELLED = gvPayments.DataKeys(e.Row.RowIndex).Values(0)
            Else
                FCD_bPDC_CANCELLED = False
            End If
            If Not gvPayments.DataKeys(e.Row.RowIndex).Values(1) Is Nothing AndAlso Not gvPayments.DataKeys(e.Row.RowIndex).Values(1) Is DBNull.Value Then
                FCD_PDC_CANCELLED_DATE = gvPayments.DataKeys(e.Row.RowIndex).Values(1)
            Else
                FCD_PDC_CANCELLED_DATE = Nothing
            End If

            If FCD_bPDC_CANCELLED AndAlso IsDate(FCD_PDC_CANCELLED_DATE) Then
                Dim i As Int16 = 0
                For Each tr As TableCell In e.Row.Cells
                    'tr.Attributes("style") = "background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAA8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD5rooor8DP9oD/2Q=='); background-repeat: repeat-x; background-position: 50% 50%; width:100%;"
                    If (i = 0) Then
                        tr.Text = "<span>" & tr.Text & "<span style='color:red;'>(Cancelled on " & Format(FCD_PDC_CANCELLED_DATE, OASISConstants.DataBaseDateFormat) & ")</span></span>"
                    Else
                        tr.Text = "<span style='text-decoration: line-through;width:100%;color:red;'>" & tr.Text & "</span>"
                        'tr.Attributes("style") = "color:red;"
                    End If
                    i += 1
                Next

            End If
        End If

    End Sub
End Class

