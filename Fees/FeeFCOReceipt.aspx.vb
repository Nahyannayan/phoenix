Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class ParentLogin_FeeFCOReceipt
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Property BALANCE() As Double
        Get
            Return ViewState("BALANCE")
        End Get
        Set(ByVal value As Double)
            ViewState("BALANCE") = value
        End Set
    End Property
    Private Property BSU_CURRENCY() As String
        Get
            Return ViewState("BSU_CURRENCY")
        End Get
        Set(ByVal value As String)
            ViewState("BSU_CURRENCY") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'Page.Title = FeeCollectionOnlineBB.GetTransportTitle()7bad10
                gvPayments.Attributes.Add("bordercolor", "#000095")
                gvFeeDetails.Attributes.Add("bordercolor", "#000095")

                Dim formatstring As String = Session("BSU_DataFormatString")
                formatstring = formatstring.Replace("0.", "###,###,###,##0.")
                'DirectCast(gvFeeDetails.Columns(1), BoundField).DataFormatString = "{0:" & formatstring & "}"
                'DirectCast(gvPayments.Columns(4), BoundField).DataFormatString = "{0:" & formatstring & "}"
                BALANCE = 0
                Select Case Request.QueryString("type")
                    Case "REC_FCO"
                        If Request.QueryString("id") <> "" Then
                            Dim iscolln, isenq As Boolean
                            iscolln = Encr_decrData.Decrypt(Request.QueryString("iscolln").Replace(" ", "+"))
                            isenq = Encr_decrData.Decrypt(Request.QueryString("isenq").Replace(" ", "+"))
                            PrintFCOReceipt(Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+")),
                            Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")), isenq, iscolln,
                            Encr_decrData.Decrypt(Request.QueryString("user").Replace(" ", "+")), formatstring)

                        End If
                End Select
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub PrintFCOReceipt(ByVal p_FCO_BSU_ID As String, ByVal p_FCO_ID As String,
        ByVal IsEnquiry As Boolean, ByVal IsCollection As Boolean, ByVal USER_NAME As String,
        ByVal formatstring As String)
        lblStudentCaptionFCO.Text = "Student ID"
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FCO_BSU_ID

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GET_FCO_FEERECEIPT", pParms)
        Dim strFilter As String = "  FCO_ID='" & p_FCO_ID & "' AND FCO_BSU_ID='" & p_FCO_BSU_ID & "' "


        Dim BSU_bBSU_Version_Enabled As Boolean
        BSU_bBSU_Version_Enabled = False
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Columns.Contains("BSU_bBSU_Version_Enabled") Then
                BSU_bBSU_Version_Enabled = ds.Tables(0).Rows(0)("BSU_bBSU_Version_Enabled")
                If BSU_bBSU_Version_Enabled Then
                    Dim BSU_LOGO_VER_NO As String, BSU_VER_NO As String
                    BSU_LOGO_VER_NO = "0"
                    BSU_VER_NO = "0"
                    BSU_LOGO_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_LOGO_VER_NO").ToString)
                    BSU_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_VER_NO").ToString)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", p_FCO_BSU_ID, SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_VER_NO", BSU_VER_NO, SqlDbType.Int)
                    Dim dtBSUDetail As New DataTable
                    dtBSUDetail = Mainclass.getDataTable("SP_GET_BUSINESSUNIT_DETAILS", sqlParam, ConnectionManger.GetOASISConnectionString)

                    lblHeader1.Text = dtBSUDetail.Rows(0)("BSU_HEADER1").ToString().ToLower()
                    lblHeader2.Text = dtBSUDetail.Rows(0)("BSU_HEADER2")
                    lblHeader3.Text = dtBSUDetail.Rows(0)("BSU_HEADER3")
                    lblSchool.Text = dtBSUDetail.Rows(0)("BSU_NAME")
                    lblSchoolName.Text = dtBSUDetail.Rows(0)("BSU_NAME")
                    lblSchoolNameCheque.Text = dtBSUDetail.Rows(0)("BSU_INVOICE_CHQ_FOOTER")
                    BSU_CURRENCY = dtBSUDetail.Rows(0)("BSU_CURRENCY")
                    imgLogo.ImageUrl = "~/Common/GetLogo.aspx?BSU_ID=" & p_FCO_BSU_ID & "&LOGO_VER_NO=" & BSU_LOGO_VER_NO
                    lblAddressMain.Text = dtBSUDetail.Rows(0)("BSU_ADDRESS_MAIN").ToString
                    If lblAddressMain.Text <> "" Then
                        trAddMain.Visible = True
                    Else
                        trAddMain.Visible = False
                    End If
                End If
            End If
            If Not BSU_bBSU_Version_Enabled Then
                lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
                lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2")
                lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3")
                lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
                lblSchoolName.Text = ds.Tables(0).Rows(0)("BSU_NAME")
                lblSchoolNameCheque.Text = ds.Tables(0).Rows(0)("BSU_INVOICE_CHQ_FOOTER")
                BSU_CURRENCY = ds.Tables(0).Rows(0)("BSU_CURRENCY")
                imgLogo.ImageUrl = "~/Common/GetLogo.aspx?BSU_ID=" & p_FCO_BSU_ID
                lblAddressMain.Text = ds.Tables(0).Rows(0)("BSU_ADDRESS_MAIN").ToString
                If lblAddressMain.Text <> "" Then
                    trAddMain.Visible = True
                Else
                    trAddMain.Visible = False
                End If
            End If

            hf_FCLID.Value = ds.Tables(0).Rows(0)("FCO_ID").ToString
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblDateFCO.Text = Format(ds.Tables(0).Rows(0)("FCO_DATE"), "dd/MMM/yyyy")
            lblGradeFCO.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY"))
            's lblRecno.Text = ds.Tables(0).Rows(0)("FCL_RECNO")
            lblSchoolnameFavour.Text = lblSchoolName.Text
            lblStudentNoFCO.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblGradeTitleFCO.Text = ds.Tables(0).Rows(0)("BUS_RECEIPT_GRADE_TITLE").ToString

            'If ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString <> Session("BSU_CURRENCY") And ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString <> "" Then
            '    lblCurrency.Visible = True
            '    lblCurrencyLabel.Visible = True
            '    lblCurrColon.Visible = True
            '    gvFeeDetails.Columns(2).Visible = True
            '    gvFeeDetails.Columns(2).HeaderText = "Amount(" & ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString & ")"
            '    gvPayments.Columns(5).Visible = True
            '    gvPayments.Columns(5).HeaderText = "Amount(" & ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString & ")"
            '    lblCurrency.Text = ds.Tables(0).Rows(0)("FCL_CUR_ID").ToString & "(" & ds.Tables(0).Rows(0)("FCL_EXCHANGE1").ToString & ")"
            'End If

            lblStudentNameFCO.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblNarration.Text = "Narration : " & ds.Tables(0).Rows(0)("FCO_NARRATION")
            Dim mcSpell As New Mainclass
            lbluserloggedin.Text = "Printed by " & USER_NAME & " (Time: " + Now.ToString("dd/MMM/yyyy hh:mm:ss tt") & ")"

            If ds.Tables(0).Rows(0)("Duplicate") = "0" Then
                lblPrintTime.Text = "Ref. " + ds.Tables(0).Rows(0)("OLU_NAME") '+ " - " + ds.Tables(0).Rows(0)("FCL_COUNTERSERIES").ToString
                'lblCopy.Visible = False
            Else
                ' lblCopy.Visible = True
                lblPrintTime.Text = "Ref. " & ds.Tables(0).Rows(0)("OLU_NAME") & " - " & " (" & CDate(ds.Tables(0).Rows(0)("FCL_LOGDATE")).ToString("hh:mm:ss tt") & ")"
            End If
            'lblAmount.Text = "Amount in words " + mcSpell.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            Dim str_paymnts As String = " exec fees.GetReceiptPrint_FOC_Online @FCO_ID ='" & p_FCO_ID & "', @FCO_BSU_ID  = '" & p_FCO_BSU_ID & "'"
            'If IsNumeric(ds.Tables(0).Rows(0)("FCO_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
            '    BALANCE = CDbl(ds.Tables(0).Rows(0)("FCO_BALANCE"))
            '    If BALANCE > 0 Then
            '        lblBalance.Text = "Due : " & BSU_CURRENCY & " " & Format(BALANCE, formatstring)
            '    Else
            '        lblBalance.Text = "Advance : " & BSU_CURRENCY & " " & Format(BALANCE * -1, formatstring)
            '    End If
            'End If
            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)

            ds1.Tables(0).Columns("FSO_AMOUNT").ColumnName = "FCS_AMOUNT"
            ds1.Tables(0).Columns("FSO_FEE_ID").ColumnName = "FCS_FEE_ID"

            gvFeeDetails.DataBind()
            gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & BSU_CURRENCY & ")"
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True

            gvPayments.DataSource = ds1.Tables(1)
            ds1.Tables(1).Columns("FDO_bPDC_CANCELLED").ColumnName = "FCD_bPDC_CANCELLED"
            ds1.Tables(1).Columns("FDO_PDC_CANCELLED_DATE").ColumnName = "FCD_PDC_CANCELLED_DATE"
            ds1.Tables(1).Columns("FDO_REFNO").ColumnName = "FCD_REFNO"
            ds1.Tables(1).Columns("FDO_DATE").ColumnName = "FCD_DATE"
            ds1.Tables(1).Columns("FDO_AMOUNT").ColumnName = "FCD_AMOUNT"
            ds1.Tables(1).Columns("FDO_CURRENCY_AMOUNT").ColumnName = "FCD_CURRENCY_AMOUNT"

            gvPayments.DataBind()
            Dim objTotal As Object = gvPayments.DataKeys(gvPayments.Rows.Count - 1).Values(2)
            Dim str_Total As String = "0.00"
            If IsNumeric(objTotal) Then
                str_Total = Format(objTotal, "#,##0.00")
            End If
            'Dim str_Total As String = gvPayments.DataKeys(gvPayments.Rows.Count - 1).Values(2) 'gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(4).Text
            Dim columnCount As Integer = gvPayments.FooterRow.Cells.Count

            gvPayments.FooterRow.Cells.Clear()
            gvPayments.FooterRow.Cells.Add(New TableCell)
            gvPayments.FooterRow.Cells(0).ColumnSpan = columnCount
            gvPayments.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Left
            If gvPayments.Columns(5).Visible = True Then
                gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(ds.Tables(0).Rows(0)("FCL_CURRENCY_AMOUNT"), ds.Tables(0).Rows(0)("FCL_CUR_ID"), Session("BSU_ROUNDOFF"))
            Else
                gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(objTotal, BSU_CURRENCY, Session("BSU_ROUNDOFF"))
            End If
            gvPayments.FooterRow.Cells(0).Font.Bold = True
            columnCount = columnCount - 1

            Dim str_TotalFC As String = gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(5).Text
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Clear()
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).ColumnSpan = columnCount - 1
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).Text = "Total"
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).HorizontalAlign = HorizontalAlign.Right

            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).Text = str_Total
            If gvPayments.Columns(5).Visible = True Then
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(2).Text = str_TotalFC
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(2).HorizontalAlign = HorizontalAlign.Right
            End If
            gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).HorizontalAlign = HorizontalAlign.Right

            If ds.Tables(0).Rows(0)("FCO_STU_TYPE") = "E" Then
                lblStudentCaptionFCO.Text = "Enquiry ID"
            Else
                lblStudentCaptionFCO.Text = "Student ID"
            End If
        End If
    End Sub


    Dim TotalDiffAmt As Double = 0, TotalCancelAmt As Double = 0
    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #000095 0pt solid; border-right: #000095 1pt dotted; border-top: #000095 1pt dotted; border-bottom: #000095 1pt dotted;"
        Next
        If e.Row.Cells(0).Text = "Total" Then
            e.Row.Font.Bold = True
        End If
        If e.Row.Cells(0).Text.ToLower.Contains("discount") Then
            lblDiscount.Visible = True
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblFCSAmt As Label = e.Row.FindControl("lblFCSAmt")
            Dim lblFEEDESCR As Label = e.Row.FindControl("lblFEEDESCR")
            Dim AMOUNT_DIFFERENCE As Double = 0, FCA_bPDC_CANCELLED As Boolean = False, FCA_PDC_CANCEL_AMOUNT As Double = 0
            Dim FCS_FEE_ID As Int16 = CInt(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCS_FEE_ID"))
            FCA_bPDC_CANCELLED = CBool(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCA_bPDC_CANCELLED"))
            FCA_PDC_CANCEL_AMOUNT = CDbl(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("FCA_PDC_CANCEL_AMOUNT"))
            AMOUNT_DIFFERENCE = CDbl(gvFeeDetails.DataKeys(e.Row.RowIndex).Values("AMOUNT_DIFFERENCE"))

            Dim diff As Double = 0
            If FCA_bPDC_CANCELLED And FCA_PDC_CANCEL_AMOUNT > 0 Then
                diff = AMOUNT_DIFFERENCE
                Dim tc As TableCell = e.Row.Cells(1)
                tc.Text = "<span>" & Format(diff, "#,##0.00") & "&nbsp;&nbsp;<span style='text-decoration: line-through; color:red;'>" & lblFCSAmt.Text & "</span></span>"
            End If

            If lblFEEDESCR.Text = "Total Paid" Then
                Dim tc As TableCell = e.Row.Cells(1)
                If AMOUNT_DIFFERENCE <> 0 Then
                    tc.Text = "<span>" & Format(AMOUNT_DIFFERENCE, "#,##0.00") & "&nbsp;&nbsp;<span style='text-decoration: line-through; color:red;'>" & lblFCSAmt.Text & "</span></span>"
                Else
                    tc.Text = "<span>" & Format(CDbl(lblFCSAmt.Text), "#,##0.00") & "</span>"
                End If

                If (BALANCE) > 0 Then
                    Dim NewBal = BALANCE '(BALANCE - FCA_PDC_CANCEL_AMOUNT)
                    lblBalance.Text = IIf(NewBal > 0, "Due", "Advance") & " : " & BSU_CURRENCY & " " & Format(Math.Abs(NewBal), "#,##0.00")
                Else
                    Dim NewBal = BALANCE '(BALANCE + FCA_PDC_CANCEL_AMOUNT)
                    lblBalance.Text = IIf(NewBal > 0, "Due", "Advance") & " : " & BSU_CURRENCY & " " & Format(Math.Abs(NewBal), "#,##0.00")
                End If
            End If
        End If

    End Sub

    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEmail.Click
        Dim RetMessage As String
        'RetMessage = DownloadEmailReceipt.DownloadOrEMailReceipt("EMAIL", lblRecno.Text, Session("sBsuId"), True)
        If RetMessage.Contains("successfully emailed") Then
            lblMessage.Text = RetMessage
        End If
        'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & lblRecno.Text & "','" & hf_FCLID.Value & "','" & RetMessage & "' ")
    End Sub

    Protected Sub gvPayments_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvPayments.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim FCD_bPDC_CANCELLED As New Boolean
            Dim FCD_PDC_CANCELLED_DATE As New Date
            If Not gvPayments.DataKeys(e.Row.RowIndex).Values(0) Is Nothing AndAlso Not gvPayments.DataKeys(e.Row.RowIndex).Values(0) Is DBNull.Value Then
                FCD_bPDC_CANCELLED = gvPayments.DataKeys(e.Row.RowIndex).Values(0)
            Else
                FCD_bPDC_CANCELLED = False
            End If
            If Not gvPayments.DataKeys(e.Row.RowIndex).Values(1) Is Nothing AndAlso Not gvPayments.DataKeys(e.Row.RowIndex).Values(1) Is DBNull.Value Then
                FCD_PDC_CANCELLED_DATE = gvPayments.DataKeys(e.Row.RowIndex).Values(1)
            Else
                FCD_PDC_CANCELLED_DATE = Nothing
            End If

            If FCD_bPDC_CANCELLED AndAlso IsDate(FCD_PDC_CANCELLED_DATE) Then
                Dim i As Int16 = 0
                For Each tr As TableCell In e.Row.Cells
                    'tr.Attributes("style") = "background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAA8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD5rooor8DP9oD/2Q=='); background-repeat: repeat-x; background-position: 50% 50%; width:100%;"
                    If (i = 0) Then
                        tr.Text = "<span>" & tr.Text & "<span style='color:red;'>(Cancelled on " & Format(FCD_PDC_CANCELLED_DATE, OASISConstants.DataBaseDateFormat) & ")</span></span>"
                    Else
                        tr.Text = "<span style='text-decoration: line-through;width:100%;color:red;'>" & tr.Text & "</span>"
                        'tr.Attributes("style") = "color:red;"
                    End If
                    i += 1
                Next

            End If
        End If

    End Sub
End Class

