Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_FEEPerformaInvoice
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property IsTuitionINV() As Boolean
        Get
            Return ViewState("IsTuitionINV")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsTuitionINV") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CheckForGroupInvoice()
        If Not IsPostBack Then
            trEditFeeDetails.Visible = False
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckForPrint(); </script>")
            hfFeeType.Value = "display"
            txtAddFeeStudDet.Attributes.Add("ReadOnly", "ReadOnly")

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If ViewState("MainMnu_code") = OASISConstants.MNU_FEE_PERFORMAINVOICE Then
                IsTuitionINV = True
            Else
                IsTuitionINV = False
            End If
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_PERFORMAINVOICE And ViewState("MainMnu_code") <> "F100136") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            h_print.Value = ""
            gvAddFeeDet.Attributes.Add("bordercolor", "#1b80b6")
            gvFEETYPEDET.Attributes.Add("bordercolor", "#1b80b6")
            gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvAddFeeDet.DataBind()
            gvFEETYPEDET.DataBind()
            gvStudentDetails.DataBind()

            txtStudName.Attributes.Add("ReadOnly", "ReadOnly")
            txtCompanyDescr.Attributes.Add("ReadOnly", "ReadOnly")
            'chkGroupInvoice.Attributes.Add("OnBlur", "CheckControls()")
            chkGroupInvoice.Attributes("onclick") = "CheckControls();"
            radEnquiry.Attributes("onclick") = "CheckControls();"
            radStudent.Attributes("onclick") = "CheckControls();"
            txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
            txtInvoiceCaption.Text = "Pro forma Invoice"

            FillACD()
            h_CURR_ACD_ID.Value = Session("Current_ACD_ID")

            Session("STUD_DET") = Nothing
            PopulateTerms_Months()
            SelectAllTreeView(trMonths.Nodes, True)
            SetDefaultInvoiceNarration()
            If ViewState("datamode") = "view" Then
                Dim vINV_NO As String = Encr_decrData.Decrypt(Request.QueryString("inv_no").Replace(" ", "+"))
                'Dim vACD_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("ACD_ID").Replace(" ", "+"))
                'Dim vGRD_ID As String = Encr_decrData.Decrypt(Request.QueryString("GRD_ID").Replace(" ", "+"))
                Dim vCOMP_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("COMP_ID").Replace(" ", "+"))
                'Dim vDATE As DateTime = Encr_decrData.Decrypt(Request.QueryString("Date").Replace(" ", "+"))
                Dim vType As String = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                Dim vENQ As Boolean = False
                If vType = "ENQ" Then
                    vENQ = True
                End If
                FillDetails(vINV_NO, vENQ)
                FillFeeTypeDLL()
                FillAllFeeType()
                ViewState("vInvNo") = vINV_NO
                MakeControlsDissabled(False)
                If txtCompanyDescr.Text = "" Then
                    'btnEdit.Visible = False
                Else
                    imgCompany_Click(sender, Nothing)
                    'btnEdit.Visible = True
                End If
            Else
                FillFeeTypeDLL()
                FillAllFeeType()
                ViewState("vInvNo") = ""
                lnkBtnSelectmonth.Text = "Select Terms"
                'dtnMonthSelDone.Visible = True
                trMonths.Enabled = True
                MakeControlsDissabled(True)
            End If
        End If
    End Sub
    Private Sub FillFeeTypeDLL()
        'ddlFeeType.DataSource = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), chkService.Checked)
        ddlFeeType.DataSource = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), Val(h_Company_ID.Value), chkService.Checked, False, IsTuitionINV)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub
    'Public Shared Function GetAllFEEType(ByVal ACD_ID As String, ByVal BSU_ID As String, ByVal COMP_ID As Int16, Optional ByVal IsServiceType As Boolean = False, Optional ByVal IsOnlyFeeSetup As Boolean = False, Optional ByVal IsOnlyTuitionFee As Boolean = False) As DataTable
    '    Dim dsData As DataSet
    '    Dim pParms(5) As SqlParameter
    '    pParms(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
    '    pParms(1) = Mainclass.CreateSqlParameter("@ACD_ID", ACD_ID, SqlDbType.Int)
    '    pParms(2) = Mainclass.CreateSqlParameter("@IsServiceType", IsServiceType, SqlDbType.Bit)
    '    pParms(3) = Mainclass.CreateSqlParameter("@IsOnlyFeeSetup", IsOnlyFeeSetup, SqlDbType.Bit)
    '    pParms(4) = Mainclass.CreateSqlParameter("@IsOnlyTuitionFee", IsOnlyTuitionFee, SqlDbType.Bit)
    '    pParms(5) = Mainclass.CreateSqlParameter("@COMP_ID", COMP_ID, SqlDbType.Int)

    '    dsData = Mainclass.getDataSet("FEES.SP_FEE_TYPES_FOR_PERFORMA", pParms, ConnectionManger.GetOASIS_FEESConnectionString)

    '    If Not dsData Is Nothing Then
    '        Return dsData.Tables(0)
    '    Else
    '        Return Nothing
    '    End If
    'End Function

    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub

    Private Sub CheckForGroupInvoice()
        Dim enable As Boolean = Not (chkGroupInvoice.Checked OrElse radEnquiry.Checked)
        'radEnquiry.Enabled = enable
        'radStudent.Enabled = enable
        'ddlAcademicYear.Enabled = enable
        'lblAddNewStudent.Enabled = enable
        'ddlGrade.Enabled = enable
    End Sub

    Private Sub MakeControlsDissabled(ByVal enable As Boolean)
        imgCompany.Enabled = enable
        imgStudent.Enabled = enable
        Image1.Enabled = enable
        ddlGrade.Enabled = enable
        txtDate.ReadOnly = Not enable
        lblAddNewStudent.Visible = enable
        txtRemarks.Enabled = enable
        gvStudentDetails.Columns(4).Visible = enable
    End Sub
    Sub SetDefaultInvoiceNarration()
        Try
            txtRemarks.Text = F_GetDefaultInvoiceNarration(ddlAcademicYear.SelectedItem.Value, Session("sBsuid"))
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function F_GetDefaultInvoiceNarration(ByVal p_ACD_ID As String, ByVal p_BSU_ID As String) As String
        '[FEES].[F_GetFeeNarration] 
        '@DOCDT varchar(12),
        '@BSU_ID varchar(20),
        '@ACD_ID bigint 
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(1).Value = p_ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetDefaultInvoiceNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function
    Private Sub FillAllFeeType()
        Dim httable As New Hashtable
        'If Not Session("SEL_FEE_TYPE") Is Nothing Then
        '    httable = Session("SEL_FEE_TYPE")
        'End If
        Dim FeeTypeDT As DataTable
        If h_Company_ID.Value <> "" AndAlso h_Company_ID.Value <> -1 Then
            If ViewState("datamode") <> "view" Then ClearDetails()
            ViewState("vInvNo") = ""
            'FeeTypeDT = FEEPERFORMAINVOICE.GetFEETypeForCompany(h_Company_ID.Value)
        End If
        FeeTypeDT = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), Val(h_Company_ID.Value), chkService.Checked, True, IsTuitionINV)
        h_FeeTypes.Value = ""
        For i As Integer = 0 To FeeTypeDT.Rows.Count - 1
            httable(FeeTypeDT.Rows(i).Item("FEE_ID")) = FeeTypeDT.Rows(i).Item("FEE_DESCR")
            h_FeeTypes.Value &= FeeTypeDT.Rows(i).Item("FEE_ID").ToString().Split("___")(0) & "|"
        Next
        Session("SEL_FEE_TYPE") = httable
        GridBindAddALLFEETYPE(FeeTypeDT)
    End Sub


    Protected Sub lnkAddFEETYPE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddFEETYPE.Click
        If ddlFeeType.Items.Count <= 0 Then Return
        Dim httable As New Hashtable
        If Not Session("SEL_FEE_TYPE") Is Nothing Then
            httable = Session("SEL_FEE_TYPE")
        End If
        If httable(ddlFeeType.SelectedValue) Is Nothing Then
            httable(ddlFeeType.SelectedValue) = ddlFeeType.SelectedItem.Text
            h_FeeTypes.Value &= ddlFeeType.SelectedValue.ToString().Split("___")(0) & "|"
        Else
            'lblError.Text = "This Fee Type is Duplicated..."
            usrMessageBar.ShowNotification("This Fee Type is Duplicated...", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Session("SEL_FEE_TYPE") = httable
        GridBindFeeAddFEETYPE(httable)
    End Sub

    Protected Sub lnkDeleteFEETYPE_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim httable As New Hashtable
        If Not Session("SEL_FEE_TYPE") Is Nothing Then
            httable = Session("SEL_FEE_TYPE")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            httable.Remove(lblFEE_ID.Text)
            h_FeeTypes.Value = Replace(h_FeeTypes.Value.ToString().Split("___")(0), lblFEE_ID.Text.Split("___")(0), "")
        End If
        Session("SEL_FEE_TYPE") = httable
        GridBindFeeAddFEETYPE(httable)
    End Sub

    Private Sub GridBindAddALLFEETYPE(ByVal ds As DataTable)
        gvFEETYPEDET.DataSource = ds
        gvFEETYPEDET.DataBind()
    End Sub


    Private Sub GridBindFeeAddFEETYPE(ByVal httable As Hashtable)
        If httable Is Nothing Then
            Return
        Else
            Dim dt As DataTable = CreateDataTable()
            Dim dr As DataRow
            Dim ienum As IDictionaryEnumerator = httable.GetEnumerator
            While (ienum.MoveNext)
                dr = dt.NewRow
                dr("FEE_ID") = ienum.Key.ToString() '.Split("___")(0)
                dr("FEE_ORDER") = ienum.Key.ToString().Split("___")(3)
                dr("FEE_DESCR") = ienum.Value
                dt.Rows.Add(dr)
            End While
            dt.DefaultView.Sort = "FEE_ORDER"
            gvFEETYPEDET.DataSource = dt
            gvFEETYPEDET.DataBind()
        End If
    End Sub

    Private Shared Function CreateDataTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.String"))
            Dim cFEE_ORDER As New DataColumn("FEE_ORDER", System.Type.GetType("System.Int32"))
            Dim cFEE_TYPE As New DataColumn("FEE_DESCR", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cFEE_ORDER)
            dtDt.Columns.Add(cFEE_TYPE)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Private Function CreateDataTableAddFeeType() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cFEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.Int32"))
            Dim cFEE_TYPE As New DataColumn("FEE_TYPE", System.Type.GetType("System.String"))
            Dim cFEE_AMT As New DataColumn("FEE_ADJ_AMT", System.Type.GetType("System.Double"))
            Dim cFEE_REMARKS As New DataColumn("FEE_ADJ_REMARKS", System.Type.GetType("System.String"))
            Dim cFEE_ApplyToAll As New DataColumn("FEE_ADJ_REMARKS", System.Type.GetType("System.Boolean"))

            dtDt.Columns.Add(cFEE_ID)
            dtDt.Columns.Add(cFEE_TYPE)
            dtDt.Columns.Add(cFEE_AMT)
            dtDt.Columns.Add(cFEE_REMARKS)
            dtDt.Columns.Add(cFEE_ApplyToAll)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function


    Private Sub FillGradeDetails(ByVal STU_ID As Integer)
        Dim STU_TYP As STUDENTTYPE
        If radEnquiry.Checked Then
            STU_TYP = STUDENTTYPE.ENQUIRY
        Else
            STU_TYP = STUDENTTYPE.STUDENT
        End If
        ddlGrade.DataSource = FEEPERFORMAINVOICE.GetGradeDetails(STU_ID, STU_TYP)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Private Sub FillDetails(ByVal vINV_NO As String, ByVal bENQ As Boolean)
        Dim FPH_ID As Integer
        Dim httab As Hashtable = FEEPERFORMAINVOICE.GetDetails(vINV_NO, bENQ, FPH_ID, Session("sBSUID"), ddlAcademicYear.SelectedValue)
        'Dim httab As Hashtable = FEEPERFORMAINVOICE.GetDetails(Session("sBSUID"), ACD_ID, GRD_ID, COMP_ID, vDATE, FPH_ID)
        If Not httab Is Nothing Then
            GridBindStudDetails(httab)
            Dim ienum As IDictionaryEnumerator = httab.GetEnumerator
            While (ienum.MoveNext())
                Dim FEE_DET As FEEPERFORMAINVOICE = ienum.Value
                txtCompanyDescr.Text = FEE_DET.FPH_COMP_NAME
                h_Company_ID.Value = FEE_DET.FPH_COMP_ID
                txtDate.Text = Format(FEE_DET.FPH_DT, OASISConstants.DateFormat)
                txtInvoiceCaption.Text = FEE_DET.FPH_INVOICE_CAPTION
                txtRemarks.Text = FEE_DET.FPH_REMARKS
                chkService.Checked = FEE_DET.FPH_IsServiceINV
                chkNewStudent.Checked = FEE_DET.FPH_IsNewStudent   'swapna added
                chkAccStatus.Checked = FEE_DET.FPH_bAccountStatus   'swapna added
                Exit While
            End While
            Session("STUD_DET") = httab
            lnkBtnSelectmonth.Text = "Selected Months"
            'dtnMonthSelDone.Visible = False
            'trMonths.Enabled = False
            Dim arrListSelectedMonth As ArrayList = FEEPERFORMAINVOICE.GetSelectedMonthsDetails(FPH_ID)
            FillTreeViewDetails(arrListSelectedMonth)
        End If
    End Sub

    Private Sub FillTreeViewDetails(ByVal arrListSelectedMonth As ArrayList)
        If arrListSelectedMonth Is Nothing OrElse arrListSelectedMonth.Count <= 0 Then Return
        Dim i As Integer = 0
        While (i < arrListSelectedMonth.Count)
            FillTreeWithValue(arrListSelectedMonth(i), trMonths.Nodes)
            i += 1
        End While
    End Sub

    Private Sub FillTreeWithValue(ByVal MonthID As Integer, ByRef Nodes As TreeNodeCollection)
        For Each Node As TreeNode In Nodes
            If (Node.ChildNodes Is Nothing OrElse Node.ChildNodes.Count <= 0) Then
                If Node.Value = MonthID Then Node.Checked = True
            Else
                FillTreeWithValue(MonthID, Node.ChildNodes)
            End If
        Next
    End Sub

    Private Sub UPDATESTUDENTDETAILS(ByVal STUD_DET As String)
        Dim stutype As STUDENTTYPE = IIf(Me.radEnquiry.Checked, STUDENTTYPE.ENQUIRY, STUDENTTYPE.STUDENT)
        Dim STUD_ID As Integer = STUD_DET.Split("||")(0)
        Dim STU_NO As String = String.Empty
        Dim STUD_NAME As String = FEEPERFORMAINVOICE.GetStudentName(STUD_ID, STU_NO, radEnquiry.Checked) 'STUD_DET.Split("||")(2)
        txtStudName.Text = STUD_NAME
        If Session("STUD_DET") Is Nothing Then
            Session("STUD_DET") = New Hashtable
        End If
        Dim htList As Hashtable = Session("STUD_DET")
        Dim total As Double = 0
        Dim vFEE_PERF As New FEEPERFORMAINVOICE
        vFEE_PERF.FPH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_PERF.FPH_BSU_ID = Session("sBSUID")
        vFEE_PERF.FPH_COMP_ID = IIf(h_Company_ID.Value = "", -1, h_Company_ID.Value)
        vFEE_PERF.FPH_bPrintYearly = chkAnually.Checked
        If chkGroupInvoice.Checked Then
            If Session("next_ACD_ID") = ddlAcademicYear.SelectedValue Then
                Dim sql As String, STU_ACD_ID As Int16
                sql = "SELECT STU_ACD_ID FROM dbo.VW_OSO_STUDENT_ENQUIRY_NEW WHERE STU_ID=" & STUD_ID
                STU_ACD_ID = Mainclass.getDataValue(sql, "OASIS_FEESConnectionString")
                If STU_ACD_ID = Session("next_ACD_ID") Then
                    vFEE_PERF.FPH_GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STUD_ID, False, stutype)
                Else
                    vFEE_PERF.FPH_GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STUD_ID, True, stutype)
                End If
            Else
                vFEE_PERF.FPH_GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STUD_ID, False, stutype)
            End If
        Else
            vFEE_PERF.FPH_GRD_ID = ddlGrade.SelectedValue
        End If
        If vFEE_PERF.FPH_GRD_ID = "" Then
            vFEE_PERF = Nothing
            Return
        End If
        Dim htTab As Hashtable = Session("SEL_FEE_TYPE")
        Dim ienum As IDictionaryEnumerator = htTab.GetEnumerator
        Dim arrList As New ArrayList
        While (ienum.MoveNext())
            arrList.Add(ienum.Key.ToString().Split("___")(0))
        End While
        vFEE_PERF.FEE_TYPES = arrList
        vFEE_PERF.FPH_INVOICENO = "1"
        vFEE_PERF.FPH_STU_ID = STUD_ID
        vFEE_PERF.FPH_STU_NAME = STUD_NAME
        vFEE_PERF.FPH_STU_NO = STU_NO
        If radStudent.Checked Then
            vFEE_PERF.FPH_STUD_TYPE = STUDENTTYPE.STUDENT
        ElseIf radEnquiry.Checked Then
            vFEE_PERF.FPH_STUD_TYPE = STUDENTTYPE.ENQUIRY
        End If
        Dim selPD As SELECTEDPROFORMADURATION
        If rbTermly.Checked Then
            selPD = SELECTEDPROFORMADURATION.TERMS
        Else
            selPD = SELECTEDPROFORMADURATION.MONTHS
        End If
        vFEE_PERF.ProformaDuration = selPD
        vFEE_PERF.STUDENT_SUBDETAILS = UPDATEFEESPLIUPDETAILS(STUD_ID, arrList, vFEE_PERF.FPH_STUD_TYPE, selPD, ddlAcademicYear.SelectedValue, vFEE_PERF.FPH_GRD_ID, total)
        vFEE_PERF.TOTAL_AMOUNT = total
        If Not vFEE_PERF.STUDENT_SUBDETAILS Is Nothing Then ''And vFEE_PERF.TOTAL_AMOUNT <> 0 Then 'Amount checking commented by Jacob on 21/AUG/2016
            htList(STUD_ID) = vFEE_PERF
        Else
            '  Me.lblError.Text &= IIf(Me.lblError.Text = "", "", "<br />") & "Fee details not found for student " & STU_NO & " and excluded from list."
            usrMessageBar.ShowNotification("Fee details not found for student " & STU_NO & " and excluded from list.", UserControls_usrMessageBar.WarningType.Danger)
        End If
        Session("STUD_DET") = htList
        h_STUD_ID.Value = ""
        txtStudName.Text = ""
        'gvStudentDetails.DataSource = dtDt
        'gvStudentDetails.DataBind()
    End Sub

    Private Sub GridBindStudDetails(ByVal httab As Hashtable)
        gvStudentDetails.DataSource = CreateDataTableFromHashTable(httab)
        gvStudentDetails.DataBind()
    End Sub

    Private Function CreateDataTableFromHashTable(ByVal htSTUD_DET As Hashtable) As DataTable
        Dim dtDt As DataTable = CreateSTUD_DETTable()
        For Each vSTD_DET As FEEPERFORMAINVOICE In htSTUD_DET.Values
            If Not vSTD_DET.bDelete Then
                Dim dr As DataRow = dtDt.NewRow()
                dr("STUD_ID") = vSTD_DET.FPH_STU_ID
                dr("STU_NO") = vSTD_DET.FPH_STU_NO
                dr("STUD_NAME") = vSTD_DET.FPH_STU_NAME
                dr("STUD_AMOUNT") = vSTD_DET.TOTAL_AMOUNT
                If vSTD_DET.FPH_GRD_ID = "" Then 'Or vSTD_DET.TOTAL_AMOUNT = 0 Then 'Amount checking commented by Jacob on 21/AUG/2016
                    Dim stuno = vSTD_DET.FPH_STU_NO
                    '   Me.lblError.Text &= IIf(Me.lblError.Text = "", "", "<br />") & "Grade details not found for student " & stuno & " and excluded from list."
                    usrMessageBar.ShowNotification("Grade details not found for student " & stuno & " and excluded from list.", UserControls_usrMessageBar.WarningType.Danger)
                Else
                    dtDt.Rows.Add(dr)
                End If
                'dtDt.Rows.Add(dr)
            End If
        Next
        Return dtDt
    End Function

    'Private Sub UPDATESTUDENTDETAILS(ByVal STUD_DET As String)

    '    Dim STUD_ID As Integer = STUD_DET.Split("||")(0)
    '    Dim STUD_NAME As String = STUD_DET.Split("||")(1)
    '    If Session("STUD_DET") Is Nothing Then
    '        Session("STUD_DET") = CreateSTUD_DETTable()
    '    End If
    '    Dim dtDt As DataTable = Session("STUD_DET")
    '    Dim dr As DataRow = dtDt.NewRow()
    '    dr("STUD_ID") = STUD_DET.Split("||")(0)
    '    dr("STUD_NAME") = STUD_DET.Split("||")(2)
    '    dr("STUD_AMOUNT") = 100
    '    dtDt.Rows.Add(dr)
    '    Session("STUD_DET") = dtDt
    '    UPDATEFEESPILUPDETAILS(h_STUD_ID.Value.Split("||")(0))

    '    gvStudentDetails.DataSource = dtDt
    '    gvStudentDetails.DataBind()
    'End Sub

    Private Function UPDATEFEESPLIUPDETAILS(ByVal STUD_ID As Integer, ByVal arrFEETYPE As ArrayList, ByVal STU_TYPE As STUDENTTYPE, ByVal selProDur As SELECTEDPROFORMADURATION, ByVal ACD_ID As String, ByVal GRD_ID As String, ByRef total As Double) As ArrayList

        Dim str_months As String = String.Empty
        Dim str_feeTypes As String = String.Empty
        Dim str_pipe As String = String.Empty
        Dim dtTab As DataTable = Session("dtMonths")
        If dtTab Is Nothing OrElse dtTab.Rows.Count <= 0 Then
            Return Nothing
        End If
        For Each drrow As DataRow In dtTab.Rows
            str_months += str_pipe & drrow("MonthID")
            str_pipe = "|"
        Next
        str_pipe = String.Empty
        For Each vFEE_ID As String In arrFEETYPE
            str_feeTypes += str_pipe & vFEE_ID
            str_pipe = "|"
        Next
        Return FEEPERFORMAINVOICE.GetTotalFees(STUD_ID, STU_TYPE, ACD_ID, GRD_ID, Session("sBSUID"), str_months, str_feeTypes, selProDur, total, chkNextAcademicYear.Checked)
        Return Nothing
    End Function

    Private Function CreateSTUD_DETTable() As DataTable
        Dim dtDt As New DataTable()
        Dim dcSTUD_ID As New DataColumn("STUD_ID", System.Type.[GetType]("System.Int32"))
        Dim dcSTU_NO As New DataColumn("STU_NO", System.Type.[GetType]("System.String"))
        Dim dcSTUD_NAME As New DataColumn("STUD_NAME", System.Type.[GetType]("System.String"))
        Dim dcSTUD_AMOUNT As New DataColumn("STUD_AMOUNT", System.Type.[GetType]("System.Decimal"))
        dtDt.Columns.Add(dcSTUD_ID)
        dtDt.Columns.Add(dcSTU_NO)
        dtDt.Columns.Add(dcSTUD_NAME)
        dtDt.Columns.Add(dcSTUD_AMOUNT)
        Return dtDt
    End Function

    Private Sub PopulateTerms_Months()
        If rbMonthly.Checked Then
            PopulateMonths()
        ElseIf rbTermly.Checked Then
            PopulateTerms()
        End If
    End Sub

    Private Sub PopulateMonths()
        Dim dtTable As DataTable = FEEPERFORMAINVOICE.PopulateMonthsInAcademicYear(ddlAcademicYear.SelectedValue, Session("sbsuid"))
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("AMS_MONTH"))
            If contains Then
                Continue While
            End If
            'If trSelectAll.ChildNodes.Contains(trNodeTRM_DESCRIPTION) Then
            '    Continue While
            'End If
            Dim strAMS_MONTH As String = "TRM_DESCRIPTION = '" & _
            drTRM_DESCRIPTION("TRM_DESCRIPTION") & "'"
            Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "AMS_MONTH", DataViewRowState.OriginalRows)
            Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
            While (ienumAMS_MONTH.MoveNext())
                Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("MONTH_DESCR"), drMONTH_DESCR("AMS_ID")) 'drMONTH_DESCR("AMS_MONTH"))
                trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub

    Private Sub PopulateTerms()
        Dim dtTable As DataTable = FEEPERFORMAINVOICE.PopulateTermsInAcademicYear(ddlAcademicYear.SelectedValue, Session("sbsuid"))
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("TRM_ID"))
            If contains Then
                Continue While
            End If
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        sTemp.Append("<table class='popdetails'>") ' border=1 bordercolor=#1b80b6 bgcolor=#ffcccc cellpadding=1 cellspacing=0>")
        sTemp.Append("<tr>")
        sTemp.Append("<td colspan=2><b>The FEE Setup details</b></td>")
        sTemp.Append("</tr>")

        Dim STUD_ID As Integer = contextKey
        If HttpContext.Current.Session("STUD_DET") Is Nothing Then
            sTemp.Append("</table>")
            Return sTemp.ToString()
        End If
        Dim httab As Hashtable = HttpContext.Current.Session("STUD_DET")
        Dim vFEE_PERF As FEEPERFORMAINVOICE = httab(STUD_ID)
        If Not vFEE_PERF Is Nothing Then
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=2><b>Student Name : " & vFEE_PERF.FPH_STU_NAME & "</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            'sTemp.Append("<td><b>FEE ID</b></td>")
            sTemp.Append("<td><b>FEE DESCR</b></td>")
            sTemp.Append("<td><b>AMOUNT</b></td>")
            sTemp.Append("</tr>")

            Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
            Dim ienum As IEnumerator = arrList.GetEnumerator()
            Dim htFeeTypes As New Hashtable
            While (ienum.MoveNext())
                Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
                htFeeTypes(FEE_SUB_DET.FPD_FEE_DESCR) = htFeeTypes(FEE_SUB_DET.FPD_FEE_DESCR) + FEE_SUB_DET.FPD_AMOUNT + FEE_SUB_DET.FPD_ADJ_AMOUNT
            End While
            Dim iDictEnum As IDictionaryEnumerator = htFeeTypes.GetEnumerator()
            While (iDictEnum.MoveNext())
                sTemp.Append("<tr>")
                'sTemp.Append("<td>" & "1" & "</td>")
                sTemp.Append("<td>" & iDictEnum.Key & "</td>")
                sTemp.Append("<td>" & iDictEnum.Value & "</td>")
                'sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_ID.ToString & "</td>")
                'sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_DESCR & "</td>")
                'sTemp.Append("<td>" & FEE_SUB_DET.FPD_AMOUNT.ToString & "</td>")
                sTemp.Append("</tr>")
            End While
        End If
        sTemp.Append("</table>")

        Return sTemp.ToString()
    End Function

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Programmatically reference the PopupControlExtender 
            Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("PopupControlExtender1"), AjaxControlToolkit.PopupControlExtender)

            ' Set the BehaviorID 
            Dim behaviorID As String = String.Concat("pce", e.Row.RowIndex)
            pce.BehaviorID = behaviorID

            ' Programmatically reference the Image control 
            Dim i As LinkButton = DirectCast(e.Row.Cells(1).FindControl("lblAmount"), LinkButton)

            ' Add the clie nt-side attributes (onmouseover & onmouseout) 
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)

            i.Attributes.Add("onmouseover", OnMouseOverScript)
            i.Attributes.Add("onmouseout", OnMouseOutScript)
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        ViewState("vInvNo") = ""
        DissableControls(False)
        MakeControlsDissabled(True)
        txtCompanyDescr.Text = ""
        h_Company_ID.Value = ""
        txtStudName.Text = ""
        h_STUD_ID.Value = ""
        txtDate.Text = ""
        lnkBtnSelectmonth.Text = "Select Months"
        'dtnMonthSelDone.Visible = True
        trMonths.Enabled = True
        ViewState("datamode") = "add"
        PopulateTerms_Months()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub DissableControls(ByVal bDissable As Boolean)
        'lblAddNewStudent.Enabled = bDissable
        'gvStudentDetails.Columns(3).Visible = bDissable
        trMonths.Enabled = bDissable
    End Sub

    Private Sub ClearDetails()
        Image1.Enabled = True
        txtDate.ReadOnly = False
        rbMonthly.Enabled = True
        rbTermly.Enabled = True
        ChkRegenerate.Visible = False
        chkNextAcademicYear.Checked = False
        chkXcludeDue.Checked = False
        txtStudName.Text = ""
        h_STUD_ID.Value = ""
        h_duplicate.Value = ""
        txtRemarks.Text = ""
        SetDefaultInvoiceNarration()
        txtInvoiceCaption.Text = "Proforma Invoice"
        'ViewState("vInvNo") = ""
        chkPrintReciept.Checked = True
        ddlGrade.DataSource = Nothing
        ddlGrade.DataBind()
        Session("STUD_DET") = Nothing
        gvStudentDetails.DataSource = Nothing
        gvStudentDetails.DataBind()


    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            ' lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not IsDate(txtDate.Text) Then
            '  lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim htStudDetails As Hashtable = Session("STUD_DET")
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction("FEE_PROCESS_FLOW")
        Dim dtdate As DateTime = CDate(txtDate.Text)
        Dim dtSelectedDates As DataTable = Session("dtMonths")
        Dim arrInvNos As New ArrayList
        Dim ReGenerate As Boolean = False
        If ChkRegenerate.Checked Then
            ReGenerate = True
        End If

        Dim htTab As Hashtable = Session("SEL_FEE_TYPE")
        Dim ienum As IDictionaryEnumerator = htTab.GetEnumerator
        Dim FeeLists As String = ""
        While (ienum.MoveNext())
            FeeLists &= ienum.Key.ToString().Split("___")(0).ToString + "|"
        End While


        Dim retVal As Integer = FEEPERFORMAINVOICE.SaveDetails(ViewState("vInvNo"), chkAnually.Checked, txtInvoiceCaption.Text, htStudDetails, dtSelectedDates, dtdate, Session("sBSUID"), ddlAcademicYear.SelectedValue, Session("sUsr_name"), conn, trans, arrInvNos, ReGenerate, txtRemarks.Text, chkService.Checked, chkNewStudent.Checked, chkAccStatus.Checked, chkXcludeAdv.Checked, "", FeeLists, chkNextAcademicYear.Checked, IsTuitionINV, chkXcludeDue.Checked)
        If retVal > 0 Then
            trans.Rollback()
            '  lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            trans.Commit()
            '   lblError.Text = "Data updated Successfully"
            usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Danger)
            ViewState("datamode") = "none"
            If arrInvNos.Count > 0 Then
                If ViewState("vInvNo") = "" Then
                    ViewState("vInvNo") = arrInvNos(0)
                End If
            End If
            If chkPrintReciept.Checked Then
                Dim bComp As Boolean = False
                If h_Company_ID.Value <> "" And h_Company_ID.Value <> "-1" Then
                    bComp = True
                End If
                If arrInvNos.Count > 0 Then
                    ViewState("vInvNo") = arrInvNos(0)
                    Session("ReportSource") = PrintReceiptX(arrInvNos, bComp)

                End If
            End If
            ClearDetails()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
        End If
    End Sub

    Private Function GetInvoiceNos(ByVal vINV_NOs As ArrayList) As String
        Dim str_Filt As String = String.Empty
        Dim comma As String = String.Empty
        For Each invNo As String In vINV_NOs
            str_Filt += comma & "'" & invNo & "'"
            comma = ", "
        Next
        Return str_Filt
    End Function

    Protected Function PrintReceiptX(ByVal vINV_NOs As ArrayList, ByVal bCompany As Boolean) As MyReportClass
        Dim ienum As IEnumerator = vINV_NOs.GetEnumerator
        Dim str_invnos As String = String.Empty
        Dim str_invnos_audit As String = String.Empty
        Dim comma As String = String.Empty
        Dim pipe As String = String.Empty
        Dim repSource As New MyReportClass
        While (ienum.MoveNext())
            str_invnos += comma & " '" & ienum.Current & "'"
            comma = ", "
            str_invnos_audit += pipe & ienum.Current
            pipe = "|"
        End While

        FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), str_invnos_audit, Session("sUsr_name"))

        repSource = FEEPERFORMAINVOICE.PrintReceipt(str_invnos, bCompany, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"))
        h_print.Value = "print"
        Return repSource
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("vInvNo") = ""
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Function UpdateMonthsSelected() As DataTable
        Try
            Dim dtDt As New DataTable
            Dim cMonthID As New DataColumn("MonthID", System.Type.GetType("System.Int32"))
            Dim cMonthDescr As New DataColumn("MonthDescr", System.Type.GetType("System.String"))
            dtDt.Columns.Add(cMonthID)
            dtDt.Columns.Add(cMonthDescr)
            Dim dr As DataRow
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                dr = dtDt.NewRow()
                dr("MonthID") = node.Value
                dr("MonthDescr") = node.Text
                dtDt.Rows.Add(dr)
            Next
            Session("dtMonths") = dtDt
            Return dtDt
        Catch
            Return Nothing
        End Try
    End Function

    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        UpdateMonthsSelected()
        If Session("dtMonths") Is Nothing OrElse Session("dtMonths").Rows.count <= 0 Then
            If rbTermly.Checked Then
                'lblError.Text = "Please select Terms"
                usrMessageBar.ShowNotification("Please select Terms", UserControls_usrMessageBar.WarningType.Danger)
            ElseIf rbMonthly.Checked Then
                '  lblError.Text = "Please select Months(Period)"
                usrMessageBar.ShowNotification("Please select Months(Period)", UserControls_usrMessageBar.WarningType.Danger)
            End If
            Return
        End If
        If h_STUD_ID.Value <> "" Then
            Dim STU_ID() As String
            If chkGroupInvoice.Checked Then
                STU_ID = h_STUD_ID.Value.Split("||")
                Dim GRD_ID As String = ""
                For i As Integer = 0 To STU_ID.Length Step 2
                    'Dim stutype As STUDENTTYPE = IIf(Me.radEnquiry.Checked, STUDENTTYPE.ENQUIRY, STUDENTTYPE.STUDENT)
                    'Dim STUD_ID As Integer = STU_ID(i).Split("||")(0)
                    'If Session("next_ACD_ID") = ddlAcademicYear.SelectedValue Then
                    '    Dim sql As String, STU_ACD_ID As Int16
                    '    sql = "SELECT STU_ACD_ID FROM dbo.VW_OSO_STUDENT_ENQUIRY_NEW WHERE STU_ID=" & STUD_ID
                    '    STU_ACD_ID = Mainclass.getDataValue(sql, "OASIS_FEESConnectionString")
                    '    If STU_ACD_ID = Session("next_ACD_ID") Then
                    '        GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STUD_ID, False, stutype)
                    '    Else
                    '        GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STUD_ID, True, stutype)
                    '    End If
                    'Else
                    '    GRD_ID = FEEPERFORMAINVOICE.GetGradeDetails(STUD_ID, False, stutype)
                    'End If
                    'If GRD_ID <> "" Then
                    UPDATESTUDENTDETAILS(STU_ID(i))
                    CheckDuplicate(STU_ID(i))
                    'End If

                Next
            Else
                STU_ID = h_STUD_ID.Value.Split("||")
                UPDATESTUDENTDETAILS(h_STUD_ID.Value)
                CheckDuplicate(STU_ID(0))
            End If
            GridBindStudDetails(Session("STUD_DET"))
            ddlGrade.DataSource = Nothing
            ddlGrade.DataBind()
        End If
    End Sub


    Sub CheckDuplicate(ByVal Stud_Id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = " SELECT VW_OSO_STUDENT_M.STU_NO FROM FEES.FEE_PERFORMAINVOICE_H INNER JOIN " _
            & " VW_OSO_STUDENT_M ON FEES.FEE_PERFORMAINVOICE_H.FPD_STU_ID = VW_OSO_STUDENT_M.STU_ID " _
            & " WHERE FPH_BSU_ID ='" & Session("sBsuid") & "' " _
            & " AND FPH_ACD_ID =" & ddlAcademicYear.SelectedValue _
            & " AND FPD_STU_ID = " & Stud_Id

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            h_duplicate.Value += ds.Tables(0).Rows(0)(0).ToString() & " , "
        End If

    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim htStudDetails As Hashtable = Session("STUD_DET")
        Dim vfeeP As FEEPERFORMAINVOICE
        If Not Session("STUD_DET") Is Nothing Then
            htStudDetails = Session("STUD_DET")
        End If
        Dim lblStudID As New Label
        lblStudID = TryCast(sender.parent.FindControl("lblStudID"), Label)
        Dim lblStud_no As New Label
        lblStud_no = TryCast(sender.parent.FindControl("lblStud_no"), Label)
        If Not lblStudID Is Nothing Then
            h_duplicate.Value = h_duplicate.Value.Replace(lblStud_no.Text & " , ", "")
            vfeeP = htStudDetails(CInt(lblStudID.Text))
            If Not vfeeP Is Nothing Then
                vfeeP.Delete()
                Session("sFEE_PROC_FLOW") = htStudDetails
                GridBindStudDetails(htStudDetails)
            End If
        End If
    End Sub

    Protected Sub imgCompany_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCompany.Click
        If h_Company_ID.Value <> "" AndAlso h_Company_ID.Value <> -1 Then
            If ViewState("datamode") <> "view" Then ClearDetails()
            ViewState("vInvNo") = ""
            ' ddlFeeType.DataSource = FEEPERFORMAINVOICE.GetFEETypeForCompany(h_Company_ID.Value)
        Else
        End If
        'ddlFeeType.DataSource = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), Val(h_Company_ID.Value), chkService.Checked, True, IsTuitionINV)
        'ddlFeeType.DataTextField = "FEE_DESCR"
        'ddlFeeType.DataValueField = "FEE_ID"
        'ddlFeeType.DataBind()
        FillFeeTypeDLL()
        FillAllFeeType()
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        If h_STUD_ID.Value <> "" Then
            If chkGroupInvoice.Checked Then
                'Dim STUD_ID As String = h_STUD_ID.Value.Split("||")(0)
                'Dim displayList As New ArrayList()
                'displayList.AddRange(h_STUD_ID.Value.Split("||"))
                'displayList.Sort()
                'For i As Integer = 1 To displayList.Count - 1
                '    If (displayList(i).ToString() = displayList(i - 1).ToString()) Then
                '        displayList.RemoveAt(i)
                '        i -= 1
                '    End If
                'Next
                'displayList.Remove("")
                'FillGroupStudents(displayList)
                ddlGrade.Enabled = False
            Else
                ddlGrade.Enabled = True
                Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
                FillGradeDetails(STUD_ID)
            End If
        End If
    End Sub

    Private Sub FillGroupStudents(ByVal STU_IDs As ArrayList)
        For i As Integer = 0 To STU_IDs.Count
            UPDATESTUDENTDETAILS(STU_IDs(i))
            GridBindStudDetails(Session("STUD_DET"))
        Next
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'ddlFeeType.DataSource = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), Val(h_Company_ID.Value), chkService.Checked, True, IsTuitionINV)
        'ddlFeeType.DataTextField = "FEE_DESCR"
        'ddlFeeType.DataValueField = "FEE_ID"
        'ddlFeeType.DataBind()
        FillFeeTypeDLL()
        PopulateTerms_Months()
        FillAllFeeType()
        PopulateTerms_Months()
        ClearDetails()
        ViewState("vInvNo") = ""
        Session("dtMonths") = Nothing
    End Sub

    Protected Sub rbTermly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTermly.CheckedChanged
        ChangeCaptionForMonth_Term()
        PopulateTerms_Months()
    End Sub

    Private Sub ChangeCaptionForMonth_Term()
        If rbTermly.Checked Then
            lnkBtnSelectmonth.Text = "Select Term"
        ElseIf rbMonthly.Checked Then
            lnkBtnSelectmonth.Text = "Select Months"
        End If
    End Sub

    Protected Sub rbMonthly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbMonthly.CheckedChanged
        ChangeCaptionForMonth_Term()
        PopulateTerms_Months()
    End Sub

    Protected Sub btnFeeAddAmt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFeeAddAmt.Click
        Try
            If Session("vEditedStudInfo") Is Nothing Then Return
            Dim vFEE_PERF As FEEPERFORMAINVOICE = Session("vEditedStudInfo")
            If Not vFEE_PERF Is Nothing Then
                Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = vFEE_PERF.GetSubDetails(ddlAddFeeType.SelectedValue)
                If FEE_SUB_DET IsNot Nothing Then
                    Dim oldAmt As Double
                    oldAmt = FEE_SUB_DET.FPD_ADJ_AMOUNT
                    FEE_SUB_DET.FPD_ADJ_AMOUNT = CDbl(txtAddFeeAmount.Text)
                    FEE_SUB_DET.FPD_ADJ_REMARKS = txtAddFeeRemarks.Text
                    vFEE_PERF.TOTAL_AMOUNT += CDbl(txtAddFeeAmount.Text) - oldAmt
                    FEE_SUB_DET.FEE_ApplyToAll = chkApplyToAll.Checked
                    ClearSubFeeType()
                Else
                    FEE_SUB_DET = New FEEPERFORMANCEREVIEW_SUB
                    FEE_SUB_DET.SORT_DATE = Now.Date
                    FEE_SUB_DET.FPD_FEE_ID = ddlAddFeeType.SelectedValue
                    FEE_SUB_DET.FPD_FEE_DESCR = ddlAddFeeType.SelectedItem.Text
                    FEE_SUB_DET.FPD_ADJ_AMOUNT = CDbl(txtAddFeeAmount.Text)
                    FEE_SUB_DET.FPD_ADJ_REMARKS = txtAddFeeRemarks.Text
                    FEE_SUB_DET.FEE_ApplyToAll = chkApplyToAll.Checked
                    vFEE_PERF.TOTAL_AMOUNT += CDbl(txtAddFeeAmount.Text)
                    vFEE_PERF.STUDENT_SUBDETAILS.Add(FEE_SUB_DET)
                    ClearSubFeeType()
                End If
                'Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
                'Dim ienum As IEnumerator = arrList.GetEnumerator()
                'Dim htFeeTypes As New Hashtable
                'While (ienum.MoveNext())
                '    Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
                '    If (FEE_SUB_DET.FPD_FEE_ID = ddlAddFeeType.SelectedValue) Then
                '        FEE_SUB_DET.FPD_ADJ_AMOUNT = CDbl(txtAddFeeAmount.Text)
                '        FEE_SUB_DET.FPD_ADJ_REMARKS = txtAddFeeRemarks.Text
                '        vFEE_PERF.TOTAL_AMOUNT += CDbl(txtAddFeeAmount.Text)
                '        ClearSubFeeType()
                '        Exit While
                '    End If
                'End While
                Session("vEditedStudInfo") = vFEE_PERF
            End If
        Catch ex As Exception

        End Try
        GridBindAdjustment()

    End Sub

    Private Sub GridBindAdjustment()
        If Session("vEditedStudInfo") Is Nothing Then
            gvAddFeeDet.DataSource = Session("vEditedStudInfo")
            gvAddFeeDet.DataBind()
            Return
        End If
        Dim dtTab As DataTable = CreateDataTableAddFeeType()
        Dim drrow As DataRow
        Dim vFEE_PERF As FEEPERFORMAINVOICE = Session("vEditedStudInfo")
        If vFEE_PERF.STUDENT_SUBDETAILS Is Nothing Then Exit Sub
        Dim ienum As IEnumerator = vFEE_PERF.STUDENT_SUBDETAILS.GetEnumerator()
        While (ienum.MoveNext())
            Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
            If FEE_SUB_DET.FPD_ADJ_AMOUNT <> 0 Then
                drrow = dtTab.NewRow()
                drrow("FEE_ID") = FEE_SUB_DET.FPD_FEE_ID
                drrow("FEE_TYPE") = FEE_SUB_DET.FPD_FEE_DESCR
                drrow("FEE_ADJ_AMT") = FEE_SUB_DET.FPD_ADJ_AMOUNT
                drrow("FEE_ADJ_REMARKS") = FEE_SUB_DET.FPD_ADJ_REMARKS
                dtTab.Rows.Add(drrow)
            End If
        End While
        gvAddFeeDet.DataSource = dtTab
        gvAddFeeDet.DataBind()
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim drRow As GridViewRow = sender.parent.parent()
        gvStudentDetails.Columns(4).Visible = False
        gvStudentDetails.SelectedIndex = drRow.RowIndex
        Dim lblStudID As Label = drRow.FindControl("lblStudID")
        If Not lblStudID Is Nothing Then
            Dim httab As Hashtable = Session("STUD_DET")
            Dim vFEE_PERF As FEEPERFORMAINVOICE = httab(CInt(lblStudID.Text))
            If Not vFEE_PERF Is Nothing Then
                'Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
                'Dim ienum As IEnumerator = arrList.GetEnumerator()
                'Dim htFeeTypes As New Hashtable
                'While (ienum.MoveNext())
                '    Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
                '    If FEE_SUB_DET.AllowConcession Then
                '        htFeeTypes(FEE_SUB_DET.FPD_FEE_ID) = FEE_SUB_DET.FPD_FEE_DESCR

                '    End If
                'End While
                ddlAddFeeType.DataSource = FEEPERFORMAINVOICE.GetAllFEEType(ddlAcademicYear.SelectedValue, Session("sbsuid"), Val(h_Company_ID.Value), chkService.Checked, False, IsTuitionINV)
                ddlAddFeeType.DataTextField = "FEE_DESCR"
                ddlAddFeeType.DataValueField = "FEEID"
                ddlAddFeeType.DataBind()

                txtAddFeeStudDet.Text = vFEE_PERF.FPH_STU_NO & " - " & vFEE_PERF.FPH_STU_NAME
                Session("vEditedStudInfo") = vFEE_PERF
            End If
        End If
        GridBindAdjustment()
        trEditFeeDetails.Visible = True
    End Sub

    Protected Sub lnkDeleteFEEADDADJ_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("vEditedStudInfo") Is Nothing Then Return
        Dim vFEE_PERF As FEEPERFORMAINVOICE = Session("vEditedStudInfo")
        If Not vFEE_PERF Is Nothing Then
            Dim drRow As GridViewRow = sender.parent.parent()
            Dim lblFEE_ID As Label = drRow.FindControl("lblFEE_ID")
            If lblFEE_ID Is Nothing Then Return
            Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
            Dim ienum As IEnumerator = arrList.GetEnumerator()
            Dim htFeeTypes As New Hashtable
            While (ienum.MoveNext())
                Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
                If (FEE_SUB_DET.FPD_FEE_ID = lblFEE_ID.Text) Then
                    vFEE_PERF.TOTAL_AMOUNT -= FEE_SUB_DET.FPD_ADJ_AMOUNT
                    FEE_SUB_DET.FPD_ADJ_AMOUNT = 0
                    FEE_SUB_DET.FPD_ADJ_REMARKS = ""
                    Exit While
                End If
            End While
            Session("vEditedStudInfo") = vFEE_PERF
        End If
        GridBindAdjustment()
    End Sub

    Protected Sub btnSaveAddFee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddFee.Click
        If Session("vEditedStudInfo") Is Nothing Then Return
        If Session("STUD_DET") Is Nothing Then Return
        Dim vFEE_EditPERF As FEEPERFORMAINVOICE = Session("vEditedStudInfo")
        Dim vFEE_PERF As FEEPERFORMAINVOICE
        Dim httab As Hashtable = Session("STUD_DET")
        Dim vSubDetArr As ArrayList
        vSubDetArr = vFEE_EditPERF.STUDENT_SUBDETAILS
        If Session("vEditedStudInfo") Is Nothing Then Return
        Dim mRow As DataRow
        For Each EDIT_STU_SUB_DET As FEEPERFORMANCEREVIEW_SUB In vSubDetArr
            If EDIT_STU_SUB_DET.FEE_ApplyToAll Then
                Dim mSTUDDT As DataTable
                mSTUDDT = CreateDataTableFromHashTable(Session("STUD_DET"))
                Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB
                For Each mRow In mSTUDDT.Rows
                    If mRow("STUD_ID") = vFEE_EditPERF.FPH_STU_ID Then Continue For
                    vFEE_PERF = httab(mRow("STUD_ID"))
                    If Not vFEE_PERF Is Nothing Then
                        FEE_SUB_DET = vFEE_PERF.GetSubDetails(EDIT_STU_SUB_DET.FPD_FEE_ID)
                        If FEE_SUB_DET IsNot Nothing Then
                            Dim oldAmt As Double
                            oldAmt = FEE_SUB_DET.FPD_ADJ_AMOUNT
                            FEE_SUB_DET.FPD_ADJ_AMOUNT = EDIT_STU_SUB_DET.FPD_ADJ_AMOUNT
                            FEE_SUB_DET.FPD_ADJ_REMARKS = EDIT_STU_SUB_DET.FPD_ADJ_REMARKS
                            vFEE_PERF.TOTAL_AMOUNT += EDIT_STU_SUB_DET.FPD_ADJ_AMOUNT - oldAmt
                            ClearSubFeeType()
                        Else
                            FEE_SUB_DET = New FEEPERFORMANCEREVIEW_SUB
                            FEE_SUB_DET.SORT_DATE = EDIT_STU_SUB_DET.SORT_DATE
                            FEE_SUB_DET.FPD_FEE_ID = EDIT_STU_SUB_DET.FPD_FEE_ID
                            FEE_SUB_DET.FPD_FEE_DESCR = EDIT_STU_SUB_DET.FPD_FEE_DESCR
                            FEE_SUB_DET.FPD_ADJ_AMOUNT = EDIT_STU_SUB_DET.FPD_ADJ_AMOUNT
                            FEE_SUB_DET.FPD_ADJ_REMARKS = EDIT_STU_SUB_DET.FPD_ADJ_REMARKS
                            vFEE_PERF.TOTAL_AMOUNT += EDIT_STU_SUB_DET.FPD_ADJ_AMOUNT
                            vFEE_PERF.STUDENT_SUBDETAILS.Add(FEE_SUB_DET)
                            ClearSubFeeType()
                        End If
                    End If
                    httab(mRow("STUD_ID")) = vFEE_PERF
                Next
            Else
                vFEE_PERF = Session("vEditedStudInfo")
                httab = Session("STUD_DET")
                httab(vFEE_PERF.FPH_STU_ID) = vFEE_PERF
            End If
        Next
        Session("STUD_DET") = httab
        gvStudentDetails.SelectedIndex = -1
        gvStudentDetails.Columns(4).Visible = True
        ClearAllFeeType()
    End Sub

    Private Sub ClearSubFeeType()
        txtAddFeeRemarks.Text = ""
        txtAddFeeAmount.Text = ""
        ddlAddFeeType.ClearSelection()
    End Sub

    Private Sub ClearAllFeeType()
        ClearSubFeeType()
        txtAddFeeStudDet.Text = ""
        ddlAddFeeType.Items.Clear()
        Session("vEditedStudInfo") = Nothing
        GridBindAdjustment()
        GridBindStudDetails(Session("STUD_DET"))
        gvStudentDetails.SelectedIndex = -1
        gvStudentDetails.Columns(4).Visible = True
        trEditFeeDetails.Visible = False
        txtInvoiceCaption.Text = "Pro forma Invoice"
    End Sub

    Protected Sub btnFeeAmtCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFeeAmtCancel.Click
        ClearSubFeeType()
    End Sub

    Protected Sub btnSaveAddFeeCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddFeeCancel.Click
        ClearAllFeeType()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        MakeControlsDissabled(True)
        MakeControlsForEditing()
        MakeDataAsEdited(Session("STUD_DET"))
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub MakeControlsForEditing()
        imgCompany.Enabled = False
        imgStudent.Enabled = True
        txtDate.ReadOnly = True
        Image1.Enabled = False
        ddlAcademicYear.Enabled = False
        gvStudentDetails.Columns(4).Visible = False
        txtRemarks.Enabled = False
        ChkRegenerate.Visible = True
        txtDate.ReadOnly = True
        Image1.Enabled = False
        rbTermly.Enabled = False
        rbMonthly.Enabled = False


    End Sub

    Private Sub MakeDataAsEdited(ByVal httab As Hashtable)
        If Not httab Is Nothing Then
            Dim ienum As IDictionaryEnumerator = httab.GetEnumerator
            While (ienum.MoveNext())
                Dim FEE_DET As FEEPERFORMAINVOICE = ienum.Value
                FEE_DET.bEdit = True
                Dim arrList As ArrayList = FEE_DET.STUDENT_SUBDETAILS
                For Each vFEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB In FEE_DET.STUDENT_SUBDETAILS
                    vFEE_SUB_DET.bEdit = True
                Next
            End While
        End If
        GridBindStudDetails(httab)
    End Sub

    Private Sub DeleteAll(ByVal httab As Hashtable)
        If Not httab Is Nothing Then
            Dim ienum As IDictionaryEnumerator = httab.GetEnumerator
            While (ienum.MoveNext())
                Dim FEE_DET As FEEPERFORMAINVOICE = ienum.Value
                FEE_DET.bEdit = True
                FEE_DET.bDelete = True
                Dim arrList As ArrayList = FEE_DET.STUDENT_SUBDETAILS
                For Each vFEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB In FEE_DET.STUDENT_SUBDETAILS
                    vFEE_SUB_DET.bEdit = True
                    vFEE_SUB_DET.bDelete = True
                Next
            End While
            GridBindStudDetails(httab)
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteAll(Session("STUD_DET"))
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If ViewState("vInvNo").ToString <> "" Then
            If h_Company_ID.Value <> "-1" Then
                Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & ViewState("vInvNo").ToString & "'", True, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"))
            Else
                Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & ViewState("vInvNo").ToString & "'", False, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"))
            End If
            FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), ViewState("vInvNo").ToString, Session("sUsr_name"))

            h_print.Value = "print"
        Else
            '  lblError.Text = "Please Select the Invoice!!!!"
            usrMessageBar.ShowNotification("Please Select the Invoice!!!!", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub


    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next

    End Sub

    Protected Sub chkService_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkService.CheckedChanged
        FillFeeTypeDLL()
        FillAllFeeType()
    End Sub

    Protected Sub txtStudName_TextChanged(sender As Object, e As EventArgs)
        If h_STUD_ID.Value <> "" Then
            If chkGroupInvoice.Checked Then
                'Dim STUD_ID As String = h_STUD_ID.Value.Split("||")(0)
                'Dim displayList As New ArrayList()
                'displayList.AddRange(h_STUD_ID.Value.Split("||"))
                'displayList.Sort()
                'For i As Integer = 1 To displayList.Count - 1
                '    If (displayList(i).ToString() = displayList(i - 1).ToString()) Then
                '        displayList.RemoveAt(i)
                '        i -= 1
                '    End If
                'Next
                'displayList.Remove("")
                'FillGroupStudents(displayList)
                ddlGrade.Enabled = False
            Else
                ddlGrade.Enabled = True
                Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
                FillGradeDetails(STUD_ID)
            End If
        End If
    End Sub
End Class
