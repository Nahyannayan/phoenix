﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="feeTransportSetupPaymentMandateView.aspx.vb"
    MasterPageFile="~/mainMasterPage.master" Inherits="Fees_feeTransportSetupPaymentMandateView" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Payment Mandate
        </div>
        <div class="card-body">
            <div class="table-responsive">
       
        <table align="center" width="100%">
            
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkadd" runat="server">Add New</asp:LinkButton>
                            </td>
                            
                        </tr>
                        <tr>
                            <td align="left" width="20%">
                               <span class="field-label"> Business Unit</span>
                            </td>
                            
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlBusinessunit0" runat="server" DataSourceID="odsSERVICES_BSU_M"
                                    DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal">
                                </asp:DropDownList>
                            </td>
                        
                            <td align="left" width="20%">
                                <span class="field-label">Bank A/C</span>
                            </td>
                            
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtbac" runat="server"></asp:TextBox>
                            </td>
                            </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">Student No</span>
                            </td>
                            
                            <td align="left">
                                <asp:TextBox ID="txtstuno" runat="server"></asp:TextBox>
                            </td>
                            <td align="left">
                                <span class="field-label">Student Name</span>
                            </td>
                            
                            <td align="left">
                                <asp:TextBox ID="txtstuname" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">Account</span>
                            </td>
                           
                            <td align="left">
                                <asp:TextBox ID="txtaccount" runat="server"></asp:TextBox>
                            </td>
                            <td align="left">
                                <span class="field-label">Start Date</span>
                            </td>
                            
                            <td align="left">
                                <asp:TextBox ID="txtstartdate" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">Payee Name</span>
                            </td>
                            
                            <td align="left">
                                <asp:TextBox ID="txtpayeename" runat="server"></asp:TextBox>
                            </td>
                       
                            <td align="left">
                               <span class="field-label"> Payee Bank</span>
                            </td>
                            
                            <td align="left">
                                <asp:DropDownList ID="dropbank" runat="server">
                                </asp:DropDownList>
                            </td>
                            </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">Branch</span>
                            </td>
                            
                            <td align="left">
                                <asp:DropDownList ID="dropbranch" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <ajaxToolkit:CalendarExtender ID="C2" TargetControlID="txtstartdate" Format="dd/MMM/yyyy"
            PopupButtonID="txtstartdate" runat="server" CssClass="table table-bordered table-row">
        </ajaxToolkit:CalendarExtender>
        <table width="100%">
            <tr>
                <td class="title-bg">
                    Payment Mandate List
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="Griddata" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" AutoGenerateColumns="false"
                        EmptyDataText="No Records Found." PageSize="15" ShowFooter="true" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Student ID
                                            
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("STU_NO")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Student Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("STU_NAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField>
                                <HeaderTemplate>
                                    Grade
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("GRADE")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField>
                                <HeaderTemplate>
                                    Section
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("SECTION")%>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Payee Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("SPM_PAYEE_NAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Payee Bank
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("BNK_DESCRIPTION")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                                Bank A/C
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("SPM_BANK_ACT_ID")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--  <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Branch
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <%# Eval("EMR_DESCR")%>
                                        </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Account
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("SPM_ACCOUNT")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Amount
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("SPM_AMOUNT")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                From&nbsp;Date
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("SPM_STARTDT", "{0:dd/MMM/yyy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>

                             <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                End&nbsp;Date
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("SPM_TODT", "{0:dd/MMM/yyy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Entry&nbsp;Date
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("SPM_LOGDT", "{0:dd/MMM/yyy}")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>

                             <asp:TemplateField>
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                               View
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="lnkview" CommandName="view" CommandArgument='<%# Eval("SPM_ID")%>' runat="server">View</asp:LinkButton>   
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
            TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
            </div>
        </div>

</asp:Content>
