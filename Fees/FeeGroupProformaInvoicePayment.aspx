<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeGroupProformaInvoicePayment.aspx.vb" Inherits="Fees_FeeGroupProformaInvoicePayment"
    Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
    function CheckForPrint() {
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            document.getElementById('<%= h_print.ClientID %>').value = '';
            showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        }
    }
    );
    function ChangeAllCheckBoxStates(checkState) {
        var chk_state = document.getElementById("chkAL").checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
                }
            }
        }
    }

    function GetCompany() {
      
        var NameandCode;
        var result;
        var url = "../Common/popupForm.aspx?ID=COMPANY&multiSelect=false";
        result = radopen(url, "pop_up")
        <%--if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
            return true;
        }
        else {
            return false;
        }--%>
    }

         function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtCompanyDescr.ClientID%>', 'TextChanged');
            }
        }

        function getBankOrEmirate(mode, ctrl) {
            var url;
            var NameandCode;
            var result;            
            document.getElementById('<%= h_mode.ClientID%>').value = mode;
            document.getElementById('<%= h_ctrl.ClientID%>').value = ctrl;
            if (mode == 1)
                url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            result = radopen(url, "pop_up2");
           <%-- if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            if (mode == 1) {
                if (ctrl == 1) {
                    document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= txtBank1.ClientID %>').value = NameandCode[1];
                }
            }
            return false;--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var mode, ctrl;
            mode = document.getElementById('<%= h_mode.ClientID%>').value;
            ctrl = document.getElementById('<%= h_ctrl.ClientID%>').value;
            var arg = args.get_argument();            
            if (arg) {
                if (mode == 1) {
                    if (ctrl == 1) {
                        NameandCode = arg.NameandCode.split('||');
                        document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank1.ClientID %>').value = NameandCode[1];
                        __doPostBack('<%= txtCompanyDescr.ClientID%>', 'TextChanged');
                    }
                }
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                   <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <table align="center" class="BlueTableView" cellpadding="5" cellspacing="0" width="98%">
                    <%--<tr class="subheader_BlueTableView" valign="top">
            <th align="left" valign="middle" colspan="4">
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </th>
        </tr>--%>
                    <tr>
                        <td align="left" width="10%"><span class="field-label">Company</span>
                        </td>

                        <td align="left" width="40%">
                            <asp:TextBox ID="txtCompanyDescr" runat="server" AutoPostBack="true" OnTextChanged="txtCompanyDescr_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetCompany(); return false;" />
                        </td>
                        <td align="right" colspan="2">
                            <asp:RadioButtonList ID="rblFilter" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Value="PD">Paid</asp:ListItem>
                                <asp:ListItem Value="P" Selected="True">Pending</asp:ListItem>
                                <asp:ListItem Value="G">All</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" width="100%" colspan="4">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                               AllowPaging="True" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol1" runat="server" EnableViewState="True"
                                                Text="Col1"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol1" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol2" runat="server" EnableViewState="True"
                                                Text="Col2"></asp:Label><br />
                                            <asp:TextBox ID="txtCol2" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol3" runat="server" EnableViewState="True"
                                                Text="Col3"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol3" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol4" runat="server" EnableViewState="True"
                                                Text="Col4"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol4" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol5" runat="server" EnableViewState="True"
                                                Text="Col5"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol5" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol6" runat="server" EnableViewState="True"
                                                Text="Col6"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol6" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblPrintReciept" runat="server" OnClick="lblPrintReciept_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Print Invoice</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table style="width: 100%;">
                                <tr>

                                    <td align="center">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="Select All"
                                            Visible="False" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Generate Invoice" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                        <asp:HiddenField ID="h_Company_ID" runat="server" />
                                        <asp:HiddenField ID="h_print" runat="server" />
                                    </td>

                                </tr>
                                <tr id="trChqDetail" runat="server">
                                    <td align="left" >

                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-row">
                                            <tr>
                                                <th align="left">Bank
                                                </th>
                                                <th align="left">Emirate
                                                </th>
                                                <th align="left">Cheque #<asp:LinkButton ID="lnkClear" OnClientClick="return ClearCheque();" runat="server">(Clear)</asp:LinkButton>
                                                </th>
                                                <th align="left">Amount
                                                </th>
                                                <th align="left">Cheque Date
                                                </th>
                                            </tr>
                                            <tr id="tr_chq1">
                                                <td align="left">
                                                    <asp:TextBox ID="txtBank1" runat="server" AutoPostBack="True" TabIndex="70"
                                                        SkinID="TextBoxCollection"></asp:TextBox>
                                                    <asp:ImageButton ID="imgBank1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getBankOrEmirate(1,1); return false;" TabIndex="75" />
                                                </td>
                                                <td align="center">
                                                    <asp:DropDownList ID="ddlEmirate1" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                                        DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="78">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtChqno1" runat="server" TabIndex="80" AutoCompleteType="Disabled"
                                                        SkinID="TextBoxCollection"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCheque" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getCheque(); return false;" TabIndex="75" />
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtChequeTotal1" onFocus="this.select();"
                                                        runat="server" Style="text-align: right" TabIndex="85" AutoCompleteType="Disabled"
                                                        SkinID="TextBoxCollection"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtChqDate1" runat="server" TabIndex="87" SkinID="TextBoxCollection"></asp:TextBox>
                                                    <asp:ImageButton ID="imgChequedate1" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        TabIndex="88" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="lblBankInvalid" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>
                                        <asp:HiddenField ID="h_Bank1" runat="server" />
                                        <asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                                            SelectCommand="SELECT     EMR_CODE, EMR_DESCR, EMR_ENQ_GROUP, EMR_CTY_ID&#13;&#10;FROM         EMIRATE_M&#13;&#10;WHERE     (EMR_CTY_ID IN&#13;&#10;                          (SELECT     BSU_COUNTRY_ID&#13;&#10;                            FROM          BUSINESSUNIT_M&#13;&#10;                            WHERE      (BSU_ID =@BSU_ID)))">
                                            <SelectParameters>
                                                <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                        <ajaxToolkit:CalendarExtender ID="calCheque1" CssClass="MyCalendar" runat="server"
                                            PopupButtonID="imgChequedate1" TargetControlID="txtChqDate1" Format="dd/MMM/yyyy"
                                            Enabled="True" PopupPosition="TopLeft">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                </tr>
                                <tr id="trChqSave" runat="server">
                                    <td align="center">
                                        <asp:Button ID="btnSavePay" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Save" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>

                <script type="text/javascript">
                    function Hidebank(id, index) {
                        document.getElementById(id).style.display = 'block';
                        document.getElementById('<%=h_hideBank.ClientID %>').value = index;
                        return false;
                    }

                    function getCheque() {
                        
                        var NameandCode;
                        var result;
                        url = "../common/PopupFormThreeIDHidden.aspx?iD=FEE_CHEQUE&MULTISELECT=FALSE";
                        result = radopen(url, "pop_up3");
                       <%-- if (result == '' || result == undefined)
                            return false;
                        NameandCode = result.split('||');
                        document.getElementById('<%= txtChqno1.ClientID %>').readOnly = true;
                        document.getElementById('<%= h_Chequeid.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtChqno1.ClientID %>').value = NameandCode[1];
                        return true;--%>
                    }

                    function OnClientClose3(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {
                              NameandCode = arg.NameandCode.split('||');
                              document.getElementById('<%= txtChqno1.ClientID %>').readOnly = true;
                              document.getElementById('<%= h_Chequeid.ClientID %>').value = NameandCode[0];
                              document.getElementById('<%= txtChqno1.ClientID %>').value = NameandCode[1];
                                 __doPostBack('<%= txtCompanyDescr.ClientID%>', 'TextChanged');
                        }
                    }

                    function ClearCheque() {
                        document.getElementById('<%= txtChqno1.ClientID %>').readOnly = '';
                        document.getElementById('<%= h_Chequeid.ClientID %>').value = '';
                        document.getElementById('<%= txtChqno1.ClientID %>').value = '';
                        return false;
                    }
                    if (document.getElementById("chromemenu1") != null) { cssdropdown.startchrome("chromemenu1") }
                    if (document.getElementById("chromemenu2") != null) { cssdropdown.startchrome("chromemenu2") }
                    if (document.getElementById("chromemenu3") != null) { cssdropdown.startchrome("chromemenu3") }
                    if (document.getElementById("chromemenu4") != null) { cssdropdown.startchrome("chromemenu4") }
                    if (document.getElementById("chromemenu5") != null) { cssdropdown.startchrome("chromemenu5") }
                    if (document.getElementById("chromemenu6") != null) { cssdropdown.startchrome("chromemenu6") }
                    //cssdropdown.startchrome("chromemenu8")
                </script>

                <asp:HiddenField ID="h_hideBank" runat="server" />
                <asp:HiddenField ID="h_mode" runat="server" />
                <asp:HiddenField ID="h_ctrl" runat="server" />                                
                <asp:HiddenField ID="h_Chequeid" runat="server" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />

                <input id="h_SelectedId" runat="server" type="hidden" value="0" />

            </div>
        </div>
    </div>
</asp:Content>
