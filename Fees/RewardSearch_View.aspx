﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RewardSearch_View.aspx.vb" Inherits="Fees_RewardsSearch_View" Title="" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<link href="../cssfiles/sb-admin.css" rel="stylesheet" />
<body>
    <form id="form1" runat="server">
       <style>
        table th, table td {
            vertical-align : top !important;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblError" runat="server"   EnableViewState="False"></asp:Label></td>
                    </tr>
                </table>
                <table width="100%">

                    <tr >
                      
            
                        <td style="vertical-align:top">
                            
                                <table width="100%" class="table table-bordered table-row m-0">
                                    <tr>
                                        <th>
                                            Basic Information
                                        </th>
                                    </tr>
                                </table>
                            
                            <asp:GridView ID="gvStudDetailview_1" runat="server" AllowPaging="True" AutoGenerateColumns="False" ShowHeader ="false"
                                            CssClass="table table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                              
                                            <RowStyle CssClass="griditem" Height="25px" />
                                             
                                            <Columns> 
                                                                                  
                                                <asp:TemplateField>
                                                    
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudname" runat="server" Text='<%# Bind("NAME")%>'  align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudvalue" runat="server" Text='<%# Bind("VALUE")%>'  align="left" ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                      

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                           </td>
                      
                        <td style="vertical-align:top">
                             <table width="100%" class="table table-bordered table-row m-0">
                                    <tr>
                                        <th>
                                            Sibling Information
                                        </th>
                                    </tr>
                                </table>
                             <asp:GridView ID="gvStudDetailview_2" runat="server" AllowPaging="True" AutoGenerateColumns="False" ShowHeader ="false"
                                            CssClass="table table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <RowStyle CssClass="griditem" Height="25px"  />
                                          
                                            <Columns>                                      
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudID" runat="server" Text='<%# Bind("ID")%>'  align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudname" runat="server" Text='<%# Bind("NAME")%>' align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudvalue" runat="server" Text='<%# Bind("VALUE")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                      

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView></td>
                          
                       

                        </tr>

                            <tr >
                      
                   
                        <td style="vertical-align:top">
                            <table width="100%" class="table table-bordered table-row m-0">
                                    <tr>
                                        <th>
                                            Referrel Information
                                        </th>
                                    </tr>
                                </table>
                            <asp:GridView ID="gvStudDetailview_3" runat="server" AllowPaging="True" AutoGenerateColumns="False" ShowHeader ="false"
                                            CssClass="table table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                
                                            <RowStyle CssClass="griditem" Height="25px"/>
                                          
                                            <Columns>                                      
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudID" runat="server" Text='<%# Bind("ID")%>'  align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudname" runat="server" Text='<%# Bind("NAME")%>' align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudvalue" runat="server" Text='<%# Bind("VALUE")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                      

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView></td>
                       
                        <td style="vertical-align:top">
                            <table width="100%" class="table table-bordered table-row m-0">
                                    <tr>
                                        <th>
                                          Reference  Information
                                        </th>
                                    </tr>
                                </table>
                             <asp:GridView ID="gvStudDetailview_4" runat="server" AllowPaging="True" AutoGenerateColumns="False" ShowHeader ="false"
                                            CssClass="table table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <RowStyle CssClass="griditem" Height="25px"  />
                                          
                                            <Columns>                                      
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudID" runat="server" Text='<%# Bind("ID")%>'  align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudname" runat="server" Text='<%# Bind("NAME")%>' align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudvalue" runat="server" Text='<%# Bind("VALUE")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                      

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView></td>
                          
                       

                        </tr>

                     <tr >
                      
                     
                        <td style="vertical-align:top">
                            <table width="100%" class="table table-bordered table-row m-0">
                                    <tr>
                                        <th>
                                           Referrel Information
                                        </th>
                                    </tr>
                                </table>
                            <asp:GridView ID="gvStudDetailview_5" runat="server" AllowPaging="True" AutoGenerateColumns="False" ShowHeader ="false"
                                            CssClass="table table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <RowStyle CssClass="griditem" Height="25px" />
                                          
                                            <Columns>                                      
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudID" runat="server" Text='<%# Bind("ID")%>'  align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudname" runat="server" Text='<%# Bind("NAME")%>' align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudvalue" runat="server" Text='<%# Bind("VALUE")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                      

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView></td>
                      
                        <td style="vertical-align:top">
                            <table width="100%" class="table table-bordered table-row m-0">
                                    <tr>
                                        <th>
                                           Referrence Information
                                        </th>
                                    </tr>
                                </table>
                             <asp:GridView ID="gvStudDetailview_6" runat="server" AllowPaging="True" AutoGenerateColumns="False" ShowHeader ="false"
                                            CssClass="table table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <RowStyle CssClass="griditem" Height="25px"/>
                                          
                                            <Columns>                                      
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudID" runat="server" Text='<%# Bind("ID")%>'  align="left" Font-Bold="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudname" runat="server" Text='<%# Bind("NAME")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudvalue" runat="server" Text='<%# Bind("VALUE")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                      

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView></td>
                          
                       

                        </tr>


                            </table>
                   </div>
        </div>
        
    </div>
    </form>
</body>
</html>
