Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml

Partial Class Fees_feeChqCollectionAll_DAX
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            btnSave.Attributes.Add("onclick", ("javascript:" & btnSave.ClientID & ".disabled=true;") + ClientScript.GetPostBackEventReference(btnSave, "").ToString())
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = "add"
            End If
            ViewState("datamode") = "add"
            If Request.QueryString("MainMnu_code") <> "" Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = ""
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300455") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case "F300455"
                    lblHeader.Text = "Cheque Collection - Transport"
            End Select
            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            ddlHeaderBank.DataBind()
            txtDocDate.Text = GetDiplayDate()
            gridbind()
            btnPrint.Visible = False
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub

    Private Sub gridbind()
        Try
            Dim lstrDocNo, lstrBank, lstrPartyAC, lstrCreditor, lstrChqNo, lstrCurrency, lstrAmount As String
            Dim lstrOpr, lstrFiltDocNo, lstrFiltBank, lstrFiltCreditor, lstrFiltChqNo, lstrFiltCurrency As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrDocNo = ""
            lstrBank = ""
            lstrPartyAC = ""
            lstrCreditor = ""
            lstrChqNo = ""
            lstrCurrency = ""
            lstrAmount = ""
            lstrFiltDocNo = ""
            lstrFiltBank = ""
            lstrFiltCreditor = ""
            lstrFiltChqNo = ""
            lstrFiltCurrency = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   DocNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                If txtSearch IsNot Nothing Then
                    lstrDocNo = Trim(txtSearch.Text)
                    If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "VHD_DOCNO", lstrDocNo)
                End If
                '   -- 2  DocDate
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtBank")
                If txtSearch IsNot Nothing Then
                    lstrBank = txtSearch.Text
                    If (lstrBank <> "") Then lstrFiltBank = SetCondn(lstrOpr, "Bank", lstrBank)
                End If
                '   -- 3  Bank AC
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtCreditor")
                If txtSearch IsNot Nothing Then
                    lstrCreditor = txtSearch.Text
                    If (lstrCreditor <> "") Then lstrFiltCreditor = SetCondn(lstrOpr, "ACCOUNT", lstrCreditor)
                End If
                '   -- 5  Narration
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
                If txtSearch IsNot Nothing Then
                    lstrChqNo = txtSearch.Text
                    If (lstrChqNo <> "") Then lstrFiltChqNo = SetCondn(lstrOpr, "VHD_CHQNO", lstrChqNo)
                End If
                '   -- 7  Currency
                larrSearchOpr = h_Selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
                If txtSearch IsNot Nothing Then
                    lstrCurrency = txtSearch.Text
                    If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "CUR_DESCR", lstrCurrency)
                End If
            End If
            Dim str_Filter As String = lstrFiltDocNo & lstrFiltBank & lstrFiltCreditor & lstrFiltChqNo & lstrFiltCurrency
            If ddlHeaderBank.Items.Count > 0 Then
                str_Filter = str_Filter & " AND BANK='" & ddlHeaderBank.SelectedItem.Value & "'"
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
            Dim str_Sql As String = ""
            str_Filter = str_Filter & " AND VHD_CHQDT <='" & txtDocDate.Text & "'"
            str_Filter = str_Filter & " AND VHH_DOCDT <='" & txtDocDate.Text & "'"
            Select Case ViewState("MainMnu_code").ToString
                Case "F300455"
                    'Dim strBsuId As String = " AND ISNULL(VHH_OWNER_BSU_ID,'')='" & ddlBusinessunit.SelectedItem.Value & "' "
                    str_Sql = "SELECT * FROM FIN.vw_OSA_CHEQCOLLECTION_FEE WHERE VHD_CHQDT <= Getdate() " & _
                    " AND VHH_BSU_ID ='" & Session("sBSUID") & "' " & _
                    str_Filter & _
                    " ORDER BY VHD_CHQDT DESC, ACCOUNT DESC"
            End Select
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                'Dim helper As GridViewHelper
                'helper = New GridViewHelper(gvJournal, True)
                'helper.RegisterSummary("AMOUNT", SummaryOperation.Sum)
                gvJournal.DataBind()
                gvJournal.FooterRow.Cells(1).Text = ds.Tables(0).Rows.Count.ToString()
            End If
            If gvJournal.Rows.Count > 0 Then
                txtSearch = gvJournal.HeaderRow.FindControl("txtDocNo")
                If txtSearch IsNot Nothing Then
                    txtSearch.Text = lstrDocNo
                End If
                txtSearch = gvJournal.HeaderRow.FindControl("txtBank")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrBank

                txtSearch = gvJournal.HeaderRow.FindControl("txtCreditor")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrCreditor

                txtSearch = gvJournal.HeaderRow.FindControl("txtChqNo")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrChqNo

                txtSearch = gvJournal.HeaderRow.FindControl("txtCurrency")
                If txtSearch IsNot Nothing Then txtSearch.Text = lstrCurrency
                set_Menu_Img()
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "Empty query")
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim lintRetVal As Integer
        Dim ldblAmount As Decimal = 0
        Dim lstrNewDocNo, str_refdocnos As String
        str_refdocnos = ""
        '--- GET THE DATES
        Dim str_FYear As String = UtilityObj.GetDataFromSQL("SELECT FYR_ID FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtDocDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT AND ISNULL(FYR_bCLOSE,0)=0", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        If str_FYear = "" Or str_FYear = "--" Then
            'lblError.Text = "Check Financial Year"
            usrMessageBar.ShowNotification("Check Financial Year", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim lblVHD_COL_ID As New Label
        For Each gvr As GridViewRow In gvJournal.Rows
            Dim chkPosted As HtmlInputCheckBox = CType(gvr.FindControl("chkPosted"), HtmlInputCheckBox)
            Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
            lblVHD_COL_ID = CType(gvr.FindControl("lblVHD_COL_ID"), Label)

            If Not chkPosted Is Nothing And Not lblAmount Is Nothing And Not lblVHD_COL_ID Is Nothing Then
                If chkPosted.Checked Then
                    ldblAmount = ldblAmount + CDbl(lblAmount.Text)
                End If
            End If
        Next
        If ldblAmount = 0 Then
            'lblError.Text = "Please Select Atleast one"
            usrMessageBar.ShowNotification("Please Select Atleast one", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        '   --- Save It ---
        Dim objConn As New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            'lstrDocNo = Master.GetNextDocNo("BR", Month(txtDocDate.Text), Year(txtDocDate.Text)).ToString
            Dim SqlCmd As New SqlCommand("FIN.SaveVOUCHER_H", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
            sqlpGUID.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpGUID)
            SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
            SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", str_FYear)
            SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", "BR")
            SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "R")
            SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtDocDate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtDocDate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", ddlHeaderBank.SelectedItem.Value.Split("-")(0).Trim)
            SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
            SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", "Cheque clearance as on" & txtDocDate.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", lblVHD_COL_ID.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", ldblAmount)
            SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
            SqlCmd.Parameters.AddWithValue("@vhh_bAuto", True)
            SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
            Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
            sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
            SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
            SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
            SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
            SqlCmd.Parameters.AddWithValue("@bEdit", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue

            Select Case ViewState("MainMnu_code").ToString
                Case "F300455"
                    SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "TPT CHEQUE COLN DEP")
            End Select


            SqlCmd.ExecuteNonQuery()
            lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
            If lintRetVal <> 0 Then
                'lblError.Text = getErrorMessage(lintRetVal)
                usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Exit Sub
            End If
            lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
            SqlCmd.Parameters.Clear()
            Dim decTotal As Decimal
            If (lintRetVal = 0) Then
                lintRetVal = DoTransactions(objConn, stTrans, lstrNewDocNo, str_FYear, str_refdocnos, decTotal)
                If lintRetVal = "0" Then
                    '-- Proceed to POST THE TRANSACTION
                    Dim cmd As New SqlCommand("FIN.POSTVOUCHER", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBSUID")
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = str_FYear
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = "BR"
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = lstrNewDocNo
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    lintRetVal = retValParam.Value
                    cmd.Parameters.Clear()
                    'If (lintRetVal = 0) And ViewState("MainMnu_code").ToString = OASISConstants.MNU_FEE_CHEQUE_CLEARANCE_TRANSPORT Then
                    '    lintRetVal = AccountFunctions.SaveDayendJournal(objConn, stTrans, "JV", Session("SUB_ID"), _
                    '  ddlBusinessunit.SelectedItem.Value, str_FYear, txtDocDate.Text, Session("BSU_CURRENCY"), 1, 1, _
                    '   "Cheque Collection ", Session("sUsr_name"), "", "", decTotal, True, Session("sBsuid"), True)
                    'End If
                    If (lintRetVal = 0) Then

                        'Calling Newely Added Procedure.....
                        Dim cmdReverse As New SqlCommand("[OASIS_TRANSPORT].[DAX].[ReversePDCAccounted]", objConn, stTrans)
                        cmdReverse.CommandType = CommandType.StoredProcedure

                        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                        sqlpBSU_ID.Value = Session("sBSUID")
                        cmdReverse.Parameters.Add(sqlpBSU_ID)

                        Dim sqlpDATE As New SqlParameter("@DT", SqlDbType.DateTime)
                        sqlpDATE.Value = Trim(txtDocDate.Text)
                        cmdReverse.Parameters.Add(sqlpDATE)

                        Dim sqlpHArdClose As New SqlParameter("@bHArdClose", SqlDbType.Bit)
                        sqlpHArdClose.Value = 0
                        cmdReverse.Parameters.Add(sqlpHArdClose)

                        Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 50)
                        sqlpUSER.Value = Session("sUsr_name")
                        cmdReverse.Parameters.Add(sqlpUSER)

                        cmdReverse.ExecuteNonQuery()
                        cmdReverse.Parameters.Clear()


                        Dim RETURN_STATUS, RETURN_MESSAGE As String
                        RETURN_STATUS = ""
                        RETURN_MESSAGE = ""
                        If lintRetVal = "0" Then
                            lintRetVal = FeeDayendProcess_DAX.CopyVouchersToDAXStaging(Session("sBsuid"), "TPT", "", "PENDING", "", lstrNewDocNo, stTrans, RETURN_STATUS, RETURN_MESSAGE)
                        End If

                        If RETURN_STATUS = "FAILED" Then
                            'lblError.Text = getErrorMessage(5005) + "-" & RETURN_MESSAGE
                            usrMessageBar.ShowNotification(getErrorMessage(5005) + "-" & RETURN_MESSAGE, UserControls_usrMessageBar.WarningType.Danger)
                            stTrans.Rollback()
                            objConn.Close()
                            Exit Sub
                        End If


                        stTrans.Commit() ''commit herestTrans.Rollback()
                        btnPrint.Visible = True
                        h_DOC_NO.Value = lstrNewDocNo
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "Cash Deposit", Page.User.Identity.Name.ToString, Me.Page, str_refdocnos)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        'lblError.Text = getErrorMessage(lintRetVal)
                        usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                        ddlHeaderBank.DataBind()
                        gridbind()
                    Else
                        stTrans.Rollback()
                        'lblError.Text = getErrorMessage(lintRetVal)
                        usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(lintRetVal)
                    usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(lintRetVal)
                usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message, "Gencc")
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub PrintPostedVouchers(ByVal vDOC_NO As String)
        'Session("ReportSource") = AccountsReports.BankPayment(vDOC_NO, String.Format("{0:dd/MMM/yyyy}", CDate(txtDocDate.Text)), Session("sBSUID"), Session("F_YEAR"), "BR", vDOC_NO)
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If TypeOf e.Row.FindControl("txtChqDate") Is TextBox Then
                Dim txtChqDate As TextBox = e.Row.FindControl("txtChqDate")
                txtChqDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        End If
    End Sub

    Private Function DoTransactions(ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, _
    ByVal p_docno As String, ByVal p_FYear As String, ByRef str_refdocnos As String, ByRef TotAmount As Decimal) As String
        Dim iReturnvalue As Integer
        Dim decAmount As Decimal = 0
        Dim xmlDoc As New XmlDocument
        Dim XMLVHD_DET As XmlElement
        Dim XMLVHD_ACT_ID As XmlElement
        Dim XMLVHD_AMOUNT As XmlElement
        Dim XMLVHD_COL_ID As XmlElement

        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        For Each gvr As GridViewRow In gvJournal.Rows
            Dim chkPosted As HtmlInputCheckBox = CType(gvr.FindControl("chkPosted"), HtmlInputCheckBox)
            Dim lblAccount As Label = CType(gvr.FindControl("lblAccount"), Label)
            Dim lblCheqNo As Label = CType(gvr.FindControl("lblCheqNo"), Label)
            Dim lblDocNo As Label = CType(gvr.FindControl("lblDocNo"), Label)
            Dim lblBank As Label = CType(gvr.FindControl("lblBank"), Label)
            Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
            Dim lblVHD_CHQID As Label = CType(gvr.FindControl("lblVHD_CHQID"), Label)
            Dim lblVHD_COL_ID As Label = CType(gvr.FindControl("lblVHD_COL_ID"), Label)
            Dim txtChqDate As TextBox = CType(gvr.FindControl("txtChqDate"), TextBox)
            If Not chkPosted Is Nothing And Not lblAccount Is Nothing And Not lblCheqNo Is Nothing Then
                If chkPosted.Checked Then
                    '''''xml
                    'VHD_ACT_ID, VHD_AMOUNT, VHD_COL_ID
                    XMLVHD_DET = xmlDoc.CreateElement("VHD_DET")
                    XMLVHD_ACT_ID = xmlDoc.CreateElement("VHD_ACT_ID")
                    XMLVHD_ACT_ID.InnerText = lblAccount.Text.Split("-")(0).Trim
                    XMLVHD_DET.AppendChild(XMLVHD_ACT_ID)

                    XMLVHD_AMOUNT = xmlDoc.CreateElement("VHD_AMOUNT")
                    XMLVHD_AMOUNT.InnerText = lblAmount.Text
                    XMLVHD_DET.AppendChild(XMLVHD_AMOUNT)

                    XMLVHD_COL_ID = xmlDoc.CreateElement("VHD_COL_ID")
                    XMLVHD_COL_ID.InnerText = lblVHD_COL_ID.Text
                    XMLVHD_DET.AppendChild(XMLVHD_COL_ID)
                    xmlDoc.DocumentElement.InsertBefore(XMLVHD_DET, xmlDoc.DocumentElement.LastChild)
                    '''''xml
                    decAmount = decAmount + Convert.ToDecimal(lblAmount.Text)
                    If iReturnvalue <> 0 Then
                        Return iReturnvalue
                    End If
                    '''''UPDATE VOUCHER HERE
                    '@VHD_DOCTYPE varchar(3),
                    '@VHD_DOCNO varchar(20),
                    '@VHD_BSU_ID varchar(20),
                    '@VHD_SUB_ID varchar(20),
                    '@VHD_bBANKRECNO varchar(20),
                    '@VHD_CHQID INT,
                    '@VHD_CHQNO  varchar(20) 
                    str_refdocnos = lblDocNo.Text & "," & str_refdocnos
                    Dim cmd2 As New SqlCommand("FIN.UPDATEVOUCHER_QR", objConn, stTrans)
                    cmd2.CommandType = CommandType.StoredProcedure
                    cmd2.Parameters.AddWithValue("@VHD_DOCTYPE", "QR")
                    cmd2.Parameters.AddWithValue("@VHD_DOCNO", lblDocNo.Text)
                    cmd2.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
                    cmd2.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
                    cmd2.Parameters.AddWithValue("@VHD_bBANKRECNO", p_docno)
                    cmd2.Parameters.AddWithValue("@VHD_CHQID", lblVHD_CHQID.Text)
                    cmd2.Parameters.AddWithValue("@VHD_CHQNO", lblCheqNo.Text)

                    cmd2.Parameters.Add("@ReturnValue", SqlDbType.Int)
                    cmd2.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                    cmd2.ExecuteNonQuery()
                    iReturnvalue = CInt(cmd2.Parameters("@ReturnValue").Value)
                    If iReturnvalue <> 0 Then
                        Return iReturnvalue
                    End If
                    cmd2.Parameters.Clear()
                End If
            End If
        Next
        If iReturnvalue = 0 Then
            iReturnvalue = FeeDayendProcess_DAX.GenerateBRFromQR(Session("sBsuid"), txtDocDate.Text, Session("SUB_ID"), _
                  "BR", xmlDoc.OuterXml, _
                   Trim(p_docno), Session("sUsr_name"), p_FYear, "FEE Cheque Collection " & txtDocDate.Text, stTrans)

        End If
        TotAmount = decAmount
        Return iReturnvalue
    End Function

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        PrintPostedVouchers(h_DOC_NO.Value)
    End Sub

    Protected Sub ddlHeaderBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlHeaderBank.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDocDate.TextChanged
        gridbind()
    End Sub

    Protected Sub imgDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDate.Click
        gridbind()
    End Sub
End Class
