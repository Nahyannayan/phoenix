<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeCollectionView.aspx.vb" Inherits="Fees_feeCollectionView" Theme="General" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
  <%--  <style type="text/css">
        /*table class overrides starts here*/
        table {
            width: 90%;
            margin-left: auto;
            margin-right: auto;
            /*border-right: #8dc24c 1pt solid;
	    border-bottom: #8dc24c 1pt solid;
	    border-top: #8dc24c 1pt solid;
	    border-left: #8dc24c 1pt solid;*/
        }

            table th input[type=text] {
                background-color: lightgoldenrodyellow;
                height: 20px;
            }

            table th {
                text-align: center !important;
            }

        .form-inline {
            display: inline !important;
        }

        .table {
            border: 0px solid #8dc24c !important;
        }

        body {
            line-height: 1.4 !important;
        }

        .button {
            width: auto !important;
            min-width: 10%;
            padding: 4px !important;
            background-color: #8dc24c !important;
            background-image: none !important;
            height: auto !important;
            margin: 4px;
        }



        .table tr {
            border: 1px !important;
        }

        .table th,
        .table td {
            padding: 0.0rem;
            padding-left: 0.2rem;
            padding-right: 0.2rem;
            vertical-align: middle;
            margin: .5px;
            border: 1px solid #dee2e6 !important;
            /*border-bottom: 0px solid #dee2e6 !important;*/
        }

        .table th {
            border: 1px solid #8dc24c !important;
            /*border-bottom: 0px solid #dee2e6 !important;*/
            background-color: #8dc24c !important;
        }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #e3f3e7;
        }

        input.form-control {
            width: auto;
        }
        /*table class overrides ends here*/
        .header {
            font-weight: bold;
            text-align: center;
            /*background-color:#8dc24c !important;*/
        }

        .link {
            color: darkblue;
            font-weight: bold;
            text-decoration: underline;
            font-family: 'Nunito', sans-serif !important;
        }

        .table-noborder {
            border: none !important;
            width: 90% !important;
        }

        .form-control {
            border: 0px solid #ced4da !important;
        }
    </style>--%>
    <%--    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>--%>
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <script type="text/javascript" lang="javascript">

        Sys.Application.add_load(
           function CheckForPrint() {
               var frmReceipt = "";
               if ($("#<%=hfTaxable.ClientID%>").val() == "1")
                   frmReceipt = "FeeReceipt_TAX.aspx";
               else
                   frmReceipt = "FeeReceipt.aspx";
               if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                   if (isIE()) {
                       window.showModalDialog(frmReceipt + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                   } else {
                      Popup(frmReceipt + document.getElementById('<%= h_print.ClientID %>').value);
                   }
                   

                   
                   //document.getElementById('<%= h_print.ClientID %>').value = '';
               }
           }
       );
        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        } 
        function popup(url1) {

            var url = url1;
            var dialog = $('<div style="display:none" class="loading"></div>').appendTo('body');
            dialog.dialog({
                maxWidth: 925,
                maxHeight: 670,
                width: 925,
                height: 670,
                title: "Fee Receipt",
                open: function () {
                    //var closeBtn = $('.ui-dialog-titlebar-close');
                    //closeBtn.append('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>');
                },
                buttons: {
                    Close: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
            dialog.load(
                   url,
                   function (responseText, textStatus, XMLHttpRequest) {
                       // remove the loading class
                       dialog.removeClass('loading');
                   }
               );
            return false;

        };

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Fee Collection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="12">
                            <asp:Label ID="lblHead" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12">
                        <asp:HyperLink ID="hlAddNew" runat="server"  >Add New</asp:HyperLink>
                        <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            </td>
                    </tr>
                    <tr>
                        <td  style="vertical-align:middle"> 
                           <span class="field-label"> View  </span>
                            </td>
                        <td>
                <asp:DropDownList ID="ddlTopFilter" runat="server" AutoPostBack="True"  
                    OnSelectedIndexChanged="ddlCount_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="10">Last 10</asp:ListItem>
                    <asp:ListItem Value="25">Last 50</asp:ListItem>
                    <asp:ListItem Value="50">Last 100</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td  style="vertical-align:middle">
                          <span class="field-label">  Academic Year    </span></td>
                        <td colspan="2">
                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"  >
                </asp:DropDownList>
                        </td>
                        <td colspan="2" style="vertical-align:middle">
                            
                               <asp:RadioButton ID="rbEnrollment" runat="server" AutoPostBack="True" Checked="True"  CssClass="field-label"
                            GroupName="mode" Text="Enrollment" />&nbsp;
                           <asp:RadioButton ID="rbEnquiry" runat="server"  CssClass="field-label"
                               AutoPostBack="True" GroupName="mode" Text="Enquiry" />
                        </td>
                        <td  style="vertical-align:middle">
                             <span class="field-label"> EmailStatus  </span>
                            </td>
                        <td colspan="2">
                <asp:DropDownList ID="ddlEmailStatus" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlEmailStatus_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="A">All</asp:ListItem>
                    <asp:ListItem Value="NE">Not Emailed</asp:ListItem>
                    <asp:ListItem Value="E">Emailed</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td colspan="2" style="vertical-align:middle" align="right">
                            <asp:RadioButton ID="rbAll" runat="server" Text="All" AutoPostBack="True" Checked="True" CssClass="field-label"
                            GroupName="deleted" />&nbsp;
                <asp:RadioButton ID="rdDeleted" runat="server" Text="Only Deleted" AutoPostBack="True"  CssClass="field-label"
                    GroupName="deleted" />
                        </td>
                    </tr>
                
              <tr>
                  <td colspan="12">
                       <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                            Width="100%" AllowPaging="True" PageSize="25" class="table table-row table-bordered" DataKeyNames="FCL_BSU_ID,FCL_PAYMENT_BSU_ID,BSU_VER_NO,BSU_bBSU_Version_Enabled">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectH" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectH_CheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="School shortcode">
                                    <HeaderTemplate>
                                         Short Code<br />
                                         <asp:TextBox ID="txtSchoolcode" runat="server"  style="width:50% !important;"></asp:TextBox>
                                            <asp:ImageButton ID="btnSchoolcodeSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSchoolcode" runat="server" Text='<%# Bind("BSU_SHORTNAME")%>' ></asp:Label>
                                    </ItemTemplate>
                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Docno">
                                    <HeaderTemplate>
                                         Receipt#<br />
                                         <asp:TextBox ID="txtReceiptno" runat="server"  Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnReceiptSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblReceipt" runat="server" Text='<%# Bind("FCL_RECNO") %>' ></asp:Label>
                                        <asp:HiddenField ID="hf_FCLID" Value='<%# Bind("FCL_ID") %>' runat="server" />
                                        <asp:HiddenField ID="hf_Emailed" Value='<%# Bind("FCL_bEMAILED") %>' runat="server" />
                                    </ItemTemplate>
                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                                    <HeaderTemplate>

                                            Date<br />
                                            <asp:TextBox ID="txtDate" runat="server"   Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("FCL_DATE", "{0:dd/MMM/yyyy}") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Grade">
                                    <HeaderTemplate>

                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"  Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRD_DISPLAY") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Student#">
                                    <HeaderTemplate>

                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server"   Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunoSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>


                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server"  Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />


                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>' ></asp:Label>
                                    </ItemTemplate>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount">
                                    <HeaderTemplate>
                                            Amount<br />
                                            <asp:TextBox ID="txtAmount" runat="server"   Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmountSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("FCL_AMOUNT") %>'  ></asp:Label>
                                    </ItemTemplate>
                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <HeaderTemplate>
                                            Description<br />
                                            <asp:TextBox ID="txtDesc" runat="server"   Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDescSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("FCL_NARRATION") %>' ></asp:Label>
                                    </ItemTemplate>
                                  <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Print Receipt">
                                    <HeaderTemplate>

                                            Print Receipt
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbVoucher" runat="server" >Print</asp:LinkButton>
                                        <br />
                                        <asp:LinkButton ID="lbExport" runat="server" >ViewReceipt</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="E-Mail" Visible="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnEmail" runat="server" OnClick="lbtnEmail_Click" >Send as EMail</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                  </td>
              </tr>
                <tr>
                    <td colspan="12" align="center">
                        <asp:Button ID="btnEmail" runat="server" CssClass="button" Text="Send EMail" />
                        <%--<asp:Button ID="btnHidden" runat="server" Text="Send EMail" CssClass="hdn_footerbutton"/>--%>
                        <asp:LinkButton ID="btnHidden" runat="server" ></asp:LinkButton>
                        <ajaxToolkit:ModalPopupExtender ID="MPE1" BackgroundCssClass="ModalPopupBG" TargetControlID="btnHidden"
                            PopupControlID="pnlEmail" OkControlID="btnClose" CancelControlID="btnCancel"
                            DropShadow="true" PopupDragHandleControlID="gridheader" Drag="true" runat="server">
                        </ajaxToolkit:ModalPopupExtender>
                    </td>
                </tr>
                </table>
                <asp:Panel ID="pnlEmail" Style="display: none;" runat="server">
                    <div  class="panel-cover" style="padding:15px">

                        <div class="row">
                            <div class=" col-12 " style="text-align:right">

                                <asp:Button ID="btnClose"  CssClass="button_small" runat="server" Text="X" />
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-12 title-bg">
                                Email Confirmation
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-12">
                                <asp:Label ID="lblMessage" CssClass="error_password" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-12">
                                <br />
                                <%-- <center>--%>
                                <asp:GridView ID="gvEmailStatus" AutoGenerateColumns="False" ShowHeader="true" runat="server"
                                    CssClass="table table-row table-bordered" AllowPaging="true" PageSize="8">
                                    <Columns>
                                        <asp:BoundField DataField="REC_NO" HeaderText="Receipt.No" />
                                        <asp:BoundField DataField="STU_NO" HeaderText="Student.No" />
                                        <asp:BoundField DataField="STU_NAME" HeaderText="Name" />
                                        <asp:BoundField DataField="STU_GRADE" HeaderText="Grade" />
                                        <asp:BoundField DataField="FCL_AMOUNT" HeaderText="Amount" ItemStyle-HorizontalAlign="Right"
                                            DataFormatString="{0:0.000}" />
                                    </Columns>
                                </asp:GridView>
                                <%-- </center>--%>
                            </div>
                        </div>
                        <div style="text-align:center">
                            <asp:Button ID="btnOkay" CssClass="button" runat="server" Text="Yes" />
                            <asp:Button ID="btnEmailUnSent" CssClass="button" runat="server" Text="No, Send to Remaining only" />
                            <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" />

                        </div>
                    </div>
                </asp:Panel>
               <%-- <link href="../cssfiles/FeePopUp.css" rel="stylesheet" type="text/css" />--%>



                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
    
    <script type="text/javascript" lang="javascript">
        function ShowSubWindowWithClose(idmId, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: idmId,
                maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = fancyTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                }
            });

            return false;
        }
        function fancyTitle(title) {
            if (title != '') {
                //var counterText = currentOpts.custom_counterText;
                var container = '<div id="fancybox-custom-title-container" style="text-align:left !important;" class="darkPanelFooter"><span id="fancybox-custom-title" CssClass="TitlePl">' + title + '</span></div>';
                //var $title = $('<span id="fancybox-custom-title" CssClass="TitlePl"></span>');
                //$title.text(title);
                //$container.append($title);
                return container;
            }
        }
    </script>
</asp:Content>
