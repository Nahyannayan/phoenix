﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="FeePaymentPlannerView.aspx.vb" Inherits="Fees_FeePaymentPlannerView" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />

    <style>
      .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 10%;
            z-index: 1000;
            overflow: auto;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            /*position: fixed;*/
            width: 100%;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function calldisable() {

            document.getElementById("<%=btnApprove.ClientID%>").disabled = true;
            document.getElementById("<%=btnReject.ClientID%>").disabled = true;
            document.getElementById("<%=btnRevert.ClientID%>").disabled = true;
        }
        function ApprovalForm(viewmode, printval, menucode) {
            //alert(printval);
            //alert(menucode);
            if (menucode == "F300271") {
                if (isIE()) {
                    window.showModalDialog('../Fees/FeePaymentPlannerPopup.aspx?viewmode=' + viewmode + '&datamode=4xCdg/cr4Xw=&MainMnu_code=3LgIRivRp4E=&viewid=' + printval, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                } else {

                    return ShowWindowWithClose('../Fees/FeePaymentPlannerPopup.aspx?viewmode=' + viewmode + '&datamode=4xCdg/cr4Xw=&MainMnu_code=3LgIRivRp4E=&viewid=' + printval, 'search', '85%', '85%');
                    return false;
                }
            }
        }

        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        }

        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }
    </script>
    <script language="javascript" type="text/javascript">
        function change_chk_state(chkThis) {
            var ids = chkThis.id

            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = state;

                }
            }
            //document.forms[0].submit()
            return false;
        }


        function uncheckall(chkThis) {
            var ids = chkThis.id
            alert(ids)
            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent

            var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child

            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }
            var uncheckflag = 0

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                    if (currentid.indexOf(value) == -1) {
                        if (document.forms[0].elements[i].checked == false) {
                            uncheckflag = 1
                        }
                    }
                }
            }

            if (uncheckflag == 1) {
                // uncheck parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = false;
                    }
                }
            }
            else {
                // Check parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = true;

                    }
                }
                // document.forms[0].submit();
            }


            return false;
        }


        function change_chk_stateg(chkThis) {
            //             var a=chkThis.id
            //             alert(a)

            var chk_state = !chkThis.checked;


            var a = document.getElementsByName('').length

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Fee Payment Planner
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>


                <table runat="server" width="100%">

                    <tr>
                        <td align="right" width="50%"></td>

                        <td align="right" width="50%">
                            <table style="margin-left: 0px; height: 1px;" width="100%">
                                <tr>
                                    <td width="0" height="0px">
                                        <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" AutoPostBack="True" Checked="true" Visible="True" Text="Pending" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" AutoPostBack="True" Visible="True" Text="On Hold" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" AutoPostBack="True" Visible="True" Text="Rejected" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad4" runat="server" GroupName="Rad" AutoPostBack="True" Visible="True" Text="Approved" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">

                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" AllowPaging="True" PageSize="25" class="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select All">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ckbheader" runat="server" Text="SelectAll" OnCheckedChanged="ckbheader_CheckedChanged" AutoPostBack="True"></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckbSelect" runat="server" OnCheckedChanged="ckbSelect_CheckedChanged" AutoPostBack="false"></asp:CheckBox>
                                        </ItemTemplate>

                                        <ItemStyle Width="50px" HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <HeaderTemplate>
                                            ID<br />
                                            <asp:TextBox ID="txtId" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnIdSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID")%>'></asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStudentID" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudentIDSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentID" runat="server" Text='<%# Bind("StudentID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStudentName" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudentNameSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# Bind("StudentName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtDate" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("DocumentDate", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("Grade")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Parent Name">
                                        <HeaderTemplate>
                                            Parent Name<br />
                                            <asp:TextBox ID="txtParentName" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnParentNameSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentName" runat="server" Text='<%# Bind("ParentName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Amount">
                                        <HeaderTemplate>
                                            Amount<br />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("Amount", "{0:0.00}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="right" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderTemplate>
                                            Status<br />
                                            <asp:TextBox ID="txtStatus" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnStatusSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <HeaderTemplate>
                                            View
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="hlview" runat="server">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email Text">
                                        <HeaderTemplate>
                                            Email Text
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="hlEmail" OnClick="lbEmail_Click" runat="server">Email</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>


                </table>
                <table runat="server" width="100%">
                    <tr id="id_princ_cmnts" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Comments</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txt_princ_comments" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" OnClientClick="calldisable();" UseSubmitBehavior="false" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" OnClientClick="calldisable();" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRevert" runat="server" CssClass="button" Text="Revert" OnClientClick="calldisable();" UseSubmitBehavior="false" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
            </div>
        </div>
    </div>


        <asp:Panel ID="pnlemailhistory" runat="server" CssClass="darkPanlAlumini" Visible="false">
            <div class="panel-cover inner_darkPanlAlumini">
                <div>

                    <div>
                        <div align="CENTER">
                            <asp:Label ID="Label1" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </div>

                        <table align="center" cellpadding="2" cellspacing="0" width="60%">
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btn_EHclose" CssClass="button" runat="server" Text="Close" Visible="true" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_emailHistory" class="field-value" Style="overflow:scroll;" runat="server" Width="700px" Height="500px" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btn_EHclose2" CssClass="button" runat="server" Text="Close" />
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>
        </asp:Panel>

</asp:Content>

