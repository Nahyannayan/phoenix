﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeRefundTransportFeeView.aspx.vb" Inherits="Transport_TransportRefundFeeView" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" ></asp:Label></td>--%>
                              <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <%--  <tr class="subheader_BlueTableView" valign="top">
            <th align="left" valign="middle" style="height: 19px" colspan="4">
                <asp:Label ID="lblHead" runat="server"></asp:Label></th>
        </tr>--%>


                    <tr>
                        <td align="left" valign="middle"  width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left"  valign="middle" width="30%">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" AutoPostBack="True" TabIndex="5" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                         <td align="left" valign="middle"  width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left"  valign="middle" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                            </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:RadioButton ID="rbOpen" runat="server" Checked="True" GroupName="post" Text="Open" AutoPostBack="True" />
                            <asp:RadioButton ID="rbPosted" runat="server" GroupName="post" Text="Posted" AutoPostBack="True" />
                            <asp:RadioButton ID="rbRejected" runat="server" GroupName="post" Text="Rejected" AutoPostBack="True" />
                        </td>
                    </tr>
                   
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvRefundFee" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row" >
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FRH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFRH_ID" runat="server" Text='<%# Bind("FRH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Student ID <br />
                                            <asp:TextBox ID="txtStuno" runat="server"  ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Student Name
                                            <br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date
                                            <br />
                                            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FRH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Account">
                                        <HeaderTemplate>
                                            Account Code
                                            <br />
                                            <asp:TextBox ID="txtPeriod" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriod" runat="server" Text='<%# Bind("FRH_ACT_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks
                                            <br />
                                            <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FRH_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FRH_TOTAL" HeaderText="Amount" />
                                    <asp:TemplateField HeaderText="Post" Visible="False">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EVENT" HeaderText="Event" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>

                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>

                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>


