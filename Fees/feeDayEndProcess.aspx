<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeDayEndProcess.aspx.vb" Inherits="Fees_feeDayEndProcess" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function UnpostOthFeecharge(ObjName) {
            var ChCnt = ObjName.value
            if (ChCnt != 0) {
                return confirm('Other FeeCharge Pending Posting..! \n \n  Number of Vouchers.:' + ChCnt + ' \n \n. Do you want to continue..?')
            }
            else {
                return true;
            }
        }

        function getPrint(docNo, docDate) {
            
            if (docNo.substring(2, 4) == "QC") {
                alert('No voucher to print..');
                return;
            }
            //result = window.showModalDialog("rptDayenddocuments.aspx?DOCNO=" + docNo + "&DOCDATE=" + docDate, "", sFeatures)
            Popup("rptDayenddocuments.aspx?DOCNO=" + docNo + "&DOCDATE=" + docDate);
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblError" runat="server"   EnableViewState="False"></asp:Label></td>
                    </tr>
                </table>
                <table width="100%">

                    <tr class="matters">
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"   AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4" />
                            <asp:Panel ID="pnlLink" runat="server">
                                <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                                    TabIndex="15">Dayend Details</asp:LinkButton>
                            </asp:Panel>
                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                PopDelay="25" PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="pnlLink">
                            </ajaxToolkit:HoverMenuExtender>
                        </td>
                        <td class="matters" align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBUnit" runat="server" DataSourceID="ODSGETBSUFORUSER" DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Days to Include (PDC)</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtDaysToInclude" runat="server" >0</asp:TextBox></td>
                        <td align="left" class="matters" colspan="2">
                            <asp:CheckBox ID="chkHardClose" runat="server" Checked="true" Text="Hard Close" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnprint" runat="server" CssClass="button" Text="Print" /></td>
                    </tr>
                </table>
                <table width="100%"  >
                    <tr>
                        <td class="title-bg" align="left"  >Day End History</td>
                    </tr>
                    <tr>
                        <td align="center"  >
                            <asp:GridView ID="gvHistory" runat="server"  CssClass="table table-row table-bordered" Width="100%" OnRowDataBound="gvHistory_RowDataBound">
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="ODSGETBSUFORUSER" runat="server" SelectMethod="GETBSUFORUSER"
                    TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter Name="USR_ID" SessionField="sUsr_name" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Panel ID="PanelTree" runat="server" Style="display: block" CssClass="panel-cover">
                    <table cellpadding="3" cellspacing="0">
                        <tr>
                            <td align="left">
                                <span class="field-label">Collection Details</span>
                                <asp:GridView ID="gvDayend" runat="server" AutoGenerateColumns="True" CssClass="table table-row table-bordered">
                                </asp:GridView>
                                <br />
                                <span class="field-label">Dayend Status</span>
                                <asp:GridView ID="gvCloseStatus" runat="server" AutoGenerateColumns="True" CssClass="table table-row table-bordered"></asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDate" PopupButtonID="imgFrom">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_UnpostFChg" runat="server" Value="0" />

            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>
