<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEConcessionTransCancelBB.aspx.vb" Inherits="Fees_FEEConcessionTransCancelBB" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
                  function CheckForPrint() {
                      if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                          document.getElementById('<%= h_print.ClientID %>').value = '';
                          radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up5');
                      }
                  }
                    );

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function GetStudent() {

            var NameandCode;
            var result;
            var url = "ShowStudent.aspx";
            result = radopen(url, "pop_up");
            <%--if (result != '' && result != undefined) {
                          NameandCode = result.split('||');
                          document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                          document.getElementById('<%=txtStud_Name.ClientID %>').value = NameandCode[1];
                          document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                          return true;
                      }
                      else {
                          return false;
                      }--%>
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStud_Name.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                __doPostBack('<%= txtStud_Name.ClientID%>', 'TextChanged');
            }
        }
        function GetCONCESSION() {

            var NameandCode;
            var result;
            var url = "../Common/PopupFormThreeIDHidden.aspx?id=CONCESSION_TRANSPORT&multiSelect=false" + "&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
            result = radopen(url, "pop_up2")
            <%--if (result != '' && result != undefined) {
                          NameandCode = result.split('||');
                          document.getElementById('<%=h_FCH_ID.ClientID %>').value = NameandCode[0];
                          document.getElementById('<%=txtConcession.ClientID %>').value = NameandCode[1];
                          return true;
                      }
                      else {
                          return false;
                      }--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_FCH_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtConcession.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtConcession.ClientID%>', 'TextChanged');
            }
        }
        function GETReference(HEAD_SUB) {

            var NameandCode;
            var result;
            var REF_TYPE;
            var STUD_ID;
            var url;
            
            document.getElementById('<%=hf_HEAD_SUB.ClientID%>').value = HEAD_SUB;
            var h_FCT_ID_HEAD = '<%=h_FCT_ID_HEAD.ClientID %>';
            var h_FCT_ID_DET = '<%=h_FCT_ID_DET.ClientID %>';
            var H_REFID_HEAD = '<%=H_REFID_HEAD.ClientID %>';
            var txtRefHEAD = '<%=txtRefHEAD.ClientID %>';
            var txtRefIDsub = '<%=txtRefIDsub.ClientID %>';
            var H_REFID_SUB = '<%=H_REFID_SUB.ClientID %>';
            if (HEAD_SUB == 0) {
                REF_TYPE = document.getElementById(h_FCT_ID_HEAD).value;
            }
            else {
                REF_TYPE = document.getElementById(h_FCT_ID_DET).value;
            }
            if (REF_TYPE == "") {
                alert('Please Select Concession Type')
                return false;
            }

            STUD_ID = document.getElementById('<%=h_STUD_ID.ClientID %>').value;
            if (STUD_ID == "") {
                alert('Please Select Student')
                return false;
            }
            //alert(REF_TYPE);
            if (REF_TYPE == 1) {

                url = "ShowStudent.aspx?type=REFERBB&STU_ID=" + STUD_ID + "&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
            }
            else if (REF_TYPE == 2) {

                url = "../Payroll/empShowMasterEmp.aspx?id=REFEP&STU_ID=" + STUD_ID;
            }
            else {
                return false;
            }
            result = radopen(url, "pop_up3")
            //if (result != '' && result != undefined) {
            //    if (HEAD_SUB == 0) {
            //        if (REF_TYPE == 1) {
            //            NameandCode = result.split('||');
            //            document.getElementById(H_REFID_HEAD).value = NameandCode[0];
            //            document.getElementById(txtRefHEAD).value = NameandCode[1];
            //            if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
            //                document.getElementById(H_REFID_SUB).value = NameandCode[0];
            //                document.getElementById(txtRefIDsub).value = NameandCode[1];
            //            }
            //        }
            //        else if (REF_TYPE == 2) {
            //            NameandCode = result.split('___');
            //            document.getElementById(H_REFID_HEAD).value = NameandCode[1];
            //            document.getElementById(txtRefHEAD).value = NameandCode[0];
            //            if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
            //                document.getElementById(H_REFID_SUB).value = NameandCode[1];
            //                document.getElementById(txtRefIDsub).value = NameandCode[0];
            //            }
            //        }
            //    }
            //    else {
            //        if (REF_TYPE == 1) {
            //            NameandCode = result.split('||');
            //            document.getElementById(H_REFID_SUB).value = NameandCode[0];
            //            document.getElementById(txtRefIDsub).value = NameandCode[1];
            //        }
            //        else if (REF_TYPE == 2) {
            //            NameandCode = result.split('___');
            //            document.getElementById(H_REFID_SUB).value = NameandCode[1];
            //            document.getElementById(txtRefIDsub).value = NameandCode[0];
            //        }
            //    }
            //    return false;
            //}
            //else {
            //    return false;
            //}
        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            var HEAD_SUB = document.getElementById('<%=hf_HEAD_SUB.ClientID%>').value;
            if (HEAD_SUB == 0) {
                REF_TYPE = document.getElementById(h_FCT_ID_HEAD).value;
            }
            else {
                REF_TYPE = document.getElementById(h_FCT_ID_DET).value;
            }
            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                if (HEAD_SUB == 0) {
                    if (REF_TYPE == 1) {
                        // NameandCode = result.split('||');
                        document.getElementById(H_REFID_HEAD).value = NameandCode[0];
                        document.getElementById(txtRefHEAD).value = NameandCode[1];
                        if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
                            document.getElementById(H_REFID_SUB).value = NameandCode[0];
                            document.getElementById(txtRefIDsub).value = NameandCode[1];
                            __doPostBack('<%= txtRefHEAD.ClientID%>', 'TextChanged');
                        }
                    }
                    else if (REF_TYPE == 2) {
                        // NameandCode = result.split('___');
                        document.getElementById(H_REFID_HEAD).value = NameandCode[1];
                        document.getElementById(txtRefHEAD).value = NameandCode[0];
                        if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
                            document.getElementById(H_REFID_SUB).value = NameandCode[1];
                            document.getElementById(txtRefIDsub).value = NameandCode[0];
                            __doPostBack('<%= txtRefHEAD.ClientID%>', 'TextChanged');
                        }
                    }
                }
                else {
                    if (REF_TYPE == 1) {
                        //  NameandCode = result.split('||');
                        document.getElementById(H_REFID_SUB).value = NameandCode[0];
                        document.getElementById(txtRefIDsub).value = NameandCode[1];
                        __doPostBack('<%= txtRefIDsub.ClientID%>', 'TextChanged');
                    }
                    else if (REF_TYPE == 2) {
                        //  NameandCode = result.split('___');
                        document.getElementById(H_REFID_SUB).value = NameandCode[1];
                        document.getElementById(txtRefIDsub).value = NameandCode[0];
                        __doPostBack('<%= txtRefIDsub.ClientID%>', 'TextChanged');
                    }
                }
            }
        }

        function GetConcession(HEAD) {
            var url;
            document.getElementById('<%=hf_HEAD_SUB2.ClientID%>').value = HEAD;
            var NameandCode;
            var result;
            url = "../common/PopupFormIDhidden.aspx?iD=CONCESSION&MULTISELECT=FALSE";
            result = radopen(url, "pop_up4");
            <%-- if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        if (HEAD == 1) {
            document.getElementById('<%= h_FCT_ID_HEAD.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= txtConcession_Head.ClientID %>').value = NameandCode[2];
            document.getElementById('<%= H_REFID_HEAD.ClientID %>').value = '';
            document.getElementById('<%= txtRefHEAD.ClientID %>').value = '';
            if (document.getElementById('<%= txtConcession_Det.ClientID %>').value == '') {
                document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
            }
        }
        else {
            document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
            document.getElementById('<%= H_REFID_SUB.ClientID %>').value = '';
            document.getElementById('<%= txtRefIDsub.ClientID %>').value = '';
        }
        return false;--%>
        }
        function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var HEAD = document.getElementById('<%=hf_HEAD_SUB2.ClientID%>').value;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');

                if (HEAD == 1) {
                    document.getElementById('<%= h_FCT_ID_HEAD.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%= txtConcession_Head.ClientID %>').value = NameandCode[2];
                    document.getElementById('<%= H_REFID_HEAD.ClientID %>').value = '';
                    document.getElementById('<%= txtRefHEAD.ClientID %>').value = '';
                    if (document.getElementById('<%= txtConcession_Det.ClientID %>').value == '') {
                        document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
                        document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];                        
                    }
                      __doPostBack('<%= txtRefHEAD.ClientID%>', 'TextChanged');
                }
                else {
                    document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
                    document.getElementById('<%= H_REFID_SUB.ClientID %>').value = '';
                    document.getElementById('<%= txtRefIDsub.ClientID %>').value = '';
                     __doPostBack('<%= txtRefIDsub.ClientID%>', 'TextChanged');
                }                             
            }
        }

    </script>
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />


     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
          <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>

</telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Cancel Fee Concession
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                    <tr>
                        <td align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALSUB" CssClass="error" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ValidationGroup="VALMAIN" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <%--  <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 19px">Cancel Fee Concession</td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Select Business Unit</span>
                        </td>
                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                TabIndex="5">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Concession</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtConcession" runat="server" AutoPostBack="true" OnTextChanged="txtConcession_TextChanged"> </asp:TextBox>
                            <asp:ImageButton
                                ID="imgSelConcession" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetCONCESSION(); return false;" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList
                                ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="Image1" runat="Server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDT" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImagefromDate" runat="server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFromDT"
                                ErrorMessage="From Date Reqired" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtFromDT"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDT" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDT" runat="server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtToDT"
                                ErrorMessage="To Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDT"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtToDT"
                                ControlToValidate="txtFromDT" ErrorMessage="From Date should be less than To Date"
                                Operator="LessThan" Type="Date" ValidationGroup="VALMAIN" Enabled="False">*</asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStdNo" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetStudent(); return false;" Enabled="False" Visible="False" />
                            <asp:TextBox ID="txtStud_Name" runat="server"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStud_Name"
                    ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Area</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgLocation" runat="server" ImageAlign="AbsMiddle"
                    ImageUrl="~/Images/Misc/Route.png" OnClientClick="getLOCATION(); return false;" TabIndex="30"
                    Visible="False" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Concession Type</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtConcession_Head" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgConcession_Head" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetConcession(1); return false;" Enabled="False" Visible="False" /></td>
                        <td align="left" width="20%"><span class="field-label">Ref. Details</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtRefHEAD" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgRefHead" runat="server" ImageUrl="~/Images/pickbutton.gif" OnClientClick="GETReference(0); return false;" Enabled="False" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRemarks"
                                ErrorMessage="Please specify remarks" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4" valign="middle">Concession Details</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Concession Type</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtConcession_Det" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgConcession_Det" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetConcession(0); return false;" Enabled="False" Visible="False" /></td>
                        <td align="left" width="20%"><span class="field-label">Ref. Details</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtRefIDsub" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgRefSub" runat="server" ImageUrl="~/Images/pickbutton.gif" OnClientClick="GETReference(1); return false;" Enabled="False" Visible="False" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True" Enabled="False" DataSourceID="odsGetFEETYPE_M" DataTextField="FEE_DESCR" DataValueField="FEE_ID">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Total Fee</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtTotalFee" runat="server" AutoCompleteType="disabled"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Charge Type</span></td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="radAmount" runat="server" Text="Amount" CssClass="field-label" ValidationGroup="AMTTYPE" GroupName="ChargeType" />
                            <asp:RadioButton ID="radPercentage" runat="server" Text="Percentage" CssClass="field-label" ValidationGroup="AMTTYPE" GroupName="ChargeType" /></td>
                        <td align="left" width="20%"><span class="field-label">Amount/Percentage </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="disabled"></asp:TextBox>
                            <asp:LinkButton ID="lnkFill" runat="server" Enabled="False" Visible="False">Auto Fill</asp:LinkButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Amount required" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Amount should be valid"
                    Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Label ID="lblAlert" CssClass="alert alert-info" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" class="title-bg">Monthly/Termly Split up(Cancellation Details)</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvMonthly" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="table table-bordered table-row" >
                                <Columns>
                                    <asp:BoundField DataField="DESCR" HeaderText="Term/Month" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Cancel Amount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="Disabled" Text='<%# Bind("CUR_AMOUNT") %>'
                                               ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Charge Dt.">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td >
                                                        <asp:TextBox ID="txtChargeDate" runat="server" Text='<%# Bind("FDD_DATE" ) %>' ></asp:TextBox>
                                                    </td>
                                                    <td >
                                            <asp:ImageButton ID="imgChargeDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                TabIndex="4" /></td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:CalendarExtender ID="CalChargeDate" runat="server" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy" PopupButtonID="imgChargeDate" PopupPosition="TopLeft" TargetControlID="txtChargeDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FDD_AMOUNT" HeaderText="Actual Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALSUB" Visible="False" />
                            <asp:GridView ID="gvFeeDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data Added Yet..." SkinID="GridViewNormal" Visible="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="FEE_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCD_ID" runat="server" Text='<%# bind("FCD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Concession Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# bind("FCM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("FEE_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount Type">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# bind("AMT_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# bind("AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" Visible="False">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" OnClick="lnkBtnDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <asp:Button ID="btnDetCancel" runat="server" CssClass="button" Text="Cancel" Visible="False" /></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                              <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALMAIN" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="defaultCalendarExtender" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image1" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="Calendarextender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDT" TargetControlID="txtToDT">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetFEES_M_TRANSPORT" TypeName="FeeCommon"></asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="Calendarextender2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImagefromDate" TargetControlID="txtFromDT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDT">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_FCT_ID_HEAD" runat="server" />
                <asp:HiddenField ID="h_FCM_ID_HEAD" runat="server" />
                <asp:HiddenField ID="h_FCM_ID_DET" runat="server" />
                <asp:HiddenField ID="H_REFID_HEAD" runat="server" />
                <asp:HiddenField ID="h_FCT_ID_DET" runat="server" />
                <asp:HiddenField ID="H_REFID_SUB" runat="server" />
                <asp:HiddenField ID="H_Location" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_FCH_ID" runat="server" />
                <asp:HiddenField ID="hf_HEAD_SUB" runat="SERVER" />
                <asp:hiddenfield ID="hf_HEAD_SUB2" runat="server" />
                 <asp:HiddenField ID="H_FCH_FCH_ID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
