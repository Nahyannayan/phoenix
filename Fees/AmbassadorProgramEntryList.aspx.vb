﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Fees_AmbassadorProgramEntryList
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S000222") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else



                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Dim STATUS As String = "P"
                    If rbPending.Checked = True Then
                        STATUS = "P"
                    End If
                    If rbApproved.Checked = True Then
                        STATUS = "A"
                    End If
                    If rbRejected.Checked = True Then
                        STATUS = "R"
                    End If

                    If ViewState("MainMnu_code") = "S000222" Then
                        ltHeader.Text = "Ambassador Program Entry List - Registrar"
                        Call gridbind(Session("sUsr_id"), STATUS, ViewState("MainMnu_code"))
                    Else
                        ltHeader.Text = "Ambassador Program Entry List"
                        Call gridbind(Session("sUsr_id"), STATUS, ViewState("MainMnu_code"))
                    End If



                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub
    Public Sub gridbind(ByVal USR_ID As String, ByVal STATUS As String, ByVal MNU_ID As String)
        Try


            Dim pParms(4) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@USR_ID", SqlDbType.VarChar, 50)
            pParms(1).Value = USR_ID
            pParms(2) = New SqlClient.SqlParameter("@STATUS", SqlDbType.VarChar, 50)
            pParms(2).Value = STATUS
            pParms(3) = New SqlClient.SqlParameter("@MNU_ID", SqlDbType.VarChar, 50)
            pParms(3).Value = MNU_ID


            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
              CommandType.StoredProcedure, "FEES.GET_AMBASSADOR_ENTRY_LIST", pParms)

            If dsData.Tables(0).Rows.Count > 0 Then
                gvAmbassadorList.DataSource = dsData.Tables(0)
                gvAmbassadorList.DataBind()
            Else
                dsData.Tables(0).Rows.Add(dsData.Tables(0).NewRow())
                'start the count from 1 no matter gridcolumn is visible or not
                'dsData.Tables(0).Rows(0)(6) = True
                gvAmbassadorList.DataSource = dsData.Tables(0)

                Try
                    gvAmbassadorList.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAmbassadorList.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAmbassadorList.Rows(0).Cells.Clear()
                gvAmbassadorList.Rows(0).Cells.Add(New TableCell)
                gvAmbassadorList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAmbassadorList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAmbassadorList.Rows(0).Cells(0).Text = "Record not available."
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub rbPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPending.CheckedChanged
        Dim STATUS As String = "P"
        If rbPending.Checked = True Then
            STATUS = "P"
        End If
        If rbApproved.Checked = True Then
            STATUS = "A"
        End If
        If rbRejected.Checked = True Then
            STATUS = "R"
        End If

        Call gridbind(Session("sUsr_id"), STATUS, ViewState("MainMnu_code"))
    End Sub

    Protected Sub rbApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbApproved.CheckedChanged
        Dim STATUS As String = "P"
        If rbPending.Checked = True Then
            STATUS = "P"
        End If
        If rbApproved.Checked = True Then
            STATUS = "A"
        End If
        If rbRejected.Checked = True Then
            STATUS = "R"
        End If

        Call gridbind(Session("sUsr_id"), STATUS, ViewState("MainMnu_code"))
    End Sub

    Protected Sub rbRejected_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRejected.CheckedChanged
        Dim STATUS As String = "P"
        If rbPending.Checked = True Then
            STATUS = "P"
        End If
        If rbApproved.Checked = True Then
            STATUS = "A"
        End If
        If rbRejected.Checked = True Then
            STATUS = "R"
        End If

        Call gridbind(Session("sUsr_id"), STATUS, ViewState("MainMnu_code"))
    End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblAPH_ID As New Label
            Dim lblEMP_ID As New Label
            Dim lblLEVEL As New Label
            Dim url As String
            Dim aphid As String
            Dim empid As String
            Dim level As String
            lblEMP_ID = TryCast(sender.FindControl("lblUSR_EMP_ID"), Label)
            lblAPH_ID = TryCast(sender.FindControl("lblAPH_ID"), Label)
            lblLEVEL = TryCast(sender.FindControl("lblLEVEL"), Label)
            aphid = lblAPH_ID.Text
            empid = lblEMP_ID.Text
            level = lblLEVEL.Text

            aphid = Encr_decrData.Encrypt(aphid)
            empid = Encr_decrData.Encrypt(empid)
            level = Encr_decrData.Encrypt(level)


            url = String.Format("Fee_AmbassadorProgram.aspx?UID={0}&ID={1}&LVL={2}", empid, aphid, level)
           

            Dim queryString As String = "test.aspx"
            Dim newWin As String = "window.open('" + url + "','_blank');"

            ClientScript.RegisterStartupScript(Me.GetType(), "pop", newWin, True)


        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Fees\AmbassadorProgramEntry.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
End Class
