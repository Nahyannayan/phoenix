<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeeStudentNotes.aspx.vb" Inherits="Fees_FeeStudentNotes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <base target="_self" />
    <title>Notes</title>

    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
</head>
<body>
    <script type="text/javascript" language="javascript">
        function CloseWindow() {
            if (confirm('Are you sure?'))
                window.close();
        }
    </script>
    <form id="form1" runat="server">
        <center>
            <table style="width: 100%">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                        </ajaxToolkit:ToolkitScriptManager>
                        <asp:HiddenField ID="h_Student_ID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <center>
                            <table width="100%">
                                <tr class="title-bg">
                                    <td colspan="4">Notes</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student No</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblStNo" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span> </td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Student Name</span></td>
                                    <td align="left" colspan="3">
                                        <asp:Label ID="lblStuName" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="tr_AddFee">
                                    <td align="left" ><span class="field-label">Notes</span></td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtRemarks" runat="server" SkinID="MultiText" TabIndex="1"
                                            TextMode="MultiLine" MaxLength="400"></asp:TextBox>
                                    </td>
                                    <td align="center" >
                                        <asp:Button ID="btnAddDetails" runat="server" CssClass="button" Text="Add"
                                            CausesValidation="False" TabIndex="2" /></td>
                                </tr>
                                <tr>
                                    <td class="style1" align="center" colspan="4">
                                        <asp:GridView ID="gvStNotes" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added" CssClass="table table-row table-bordered"
                                            Width="100%" AllowPaging="True"
                                            DataKeyNames="STN_ID,STN_STU_ID">
                                            <Columns>
                                                <asp:BoundField DataField="STN_DATE" HeaderText="Date" DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="false">
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemStyle Width="10%" Wrap="false" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STN_NOTES" HeaderText="Notes">
                                                    <ItemStyle HorizontalAlign="Left" Width="80%" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STN_USER_NAME" HeaderText="User">
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" Wrap="false" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" valign="bottom">
                                        <asp:HiddenField ID="h_STN_ID" runat="server" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Style=" visibility:hidden;"
                                            Text="Close" TabIndex="3" OnClientClick="CloseWindow();" />
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
