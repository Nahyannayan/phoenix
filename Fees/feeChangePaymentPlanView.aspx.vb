﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeChangePaymentPlanView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEConcessionDet.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300245" _
                And ViewState("MainMnu_code") <> "F351085") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case "F300245"
                        lblHead.Text = "Set Payment Plan"
                        hlAddNew.NavigateUrl = "feeChangePaymentPlan.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        rbAll.Visible = True
                        rbApproved.Visible = True
                        rbRejected.Visible = True
                        btnPost.Visible = False
                        btnReject.Visible = False
                    Case "F351085"
                        lblHead.Text = "Post Payment Plan"
                        gvFEEConcessionDet.Columns(8).Visible = True
                        hlAddNew.Visible = False
                        lblDate.Visible = True
                        txtFrom.Visible = True
                        imgFrom.Visible = True
                        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
                        rbAll.Visible = False
                        rbApproved.Visible = False
                        rbRejected.Visible = False
                        btnPost.Visible = True
                        btnReject.Visible = True
                End Select
                ddlAcademicYear.Items.Clear()
                ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                ddlAcademicYear.DataTextField = "ACY_DESCR"
                ddlAcademicYear.DataValueField = "ACD_ID"
                ddlAcademicYear.DataBind()
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEConcessionDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvFEEConcessionDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvFEEConcessionDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)
                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SCH_DESCR", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STP_FROMDT", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STP_TODT", lstrCondn5)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
                lstrCondn6 = txtSearch.Text.Trim
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STP_REMARKS", lstrCondn6)
            End If
            If rbApproved.Checked Then
                str_Filter = str_Filter & " AND STP_STATUS = 'A'"
            ElseIf rbRejected.Checked Then
                str_Filter = str_Filter & " AND STP_STATUS = 'R'"
            End If
            str_Filter = str_Filter & " ORDER BY STP_FROMDT DESC"
            Select Case ViewState("MainMnu_code").ToString
                Case "F300245"
                    str_Sql = " SELECT STP.STP_ID, ACY.ACY_DESCR, STP.STP_FROMDT, STP.STP_TODT, " _
                    & " STP.STP_REMARKS, STU.STU_NO, STU.STU_NAME, SCH.SCH_DESCR, " _
                    & " STP.STP_BSU_ID, STP.STP_ACD_ID,case STP.STP_STATUS when 'N' then 'Not Approved' when 'A' then 'Approved' else 'Rejected' end STP_STATUS" _
                    & " FROM FEES.STUDENT_PAYMENTPLAN AS STP INNER JOIN" _
                    & " ACADEMICYEAR_D AS ACD ON STP.STP_ACD_ID = ACD.ACD_ID INNER JOIN" _
                    & " ACADEMICYEAR_M AS ACY ON ACD.ACD_ACY_ID = ACY.ACY_ID INNER JOIN" _
                    & " VW_OSO_STUDENT_M AS STU ON STP.STP_STU_ID = STU.STU_ID INNER JOIN" _
                    & " FEES.SCHEDULE_M AS SCH ON STP.STP_PAY_SCH_ID = SCH.SCH_ID" _
                    & " WHERE STP_BSU_ID='" & Session("sBsuid") & "' AND STP_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
                Case "F351085"
                    str_Sql = " SELECT STP.STP_ID, ACY.ACY_DESCR, STP.STP_FROMDT, STP.STP_TODT, " _
                     & " STP.STP_REMARKS, STU.STU_NO, STU.STU_NAME, SCH.SCH_DESCR, " _
                     & " STP.STP_BSU_ID, STP.STP_ACD_ID,case STP.STP_STATUS when 'N' then 'Not Approved' when 'A' then 'Approved' else 'Rejected' end STP_STATUS" _
                     & " FROM FEES.STUDENT_PAYMENTPLAN AS STP INNER JOIN" _
                     & " ACADEMICYEAR_D AS ACD ON STP.STP_ACD_ID = ACD.ACD_ID INNER JOIN" _
                     & " ACADEMICYEAR_M AS ACY ON ACD.ACD_ACY_ID = ACY.ACY_ID INNER JOIN" _
                     & " VW_OSO_STUDENT_M AS STU ON STP.STP_STU_ID = STU.STU_ID INNER JOIN" _
                     & " FEES.SCHEDULE_M AS SCH ON STP.STP_PAY_SCH_ID = SCH.SCH_ID" _
                     & " WHERE STP_STATUS ='N' AND STP_BSU_ID='" & Session("sBsuid") & "' AND STP_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
            End Select
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvFEEConcessionDet.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEConcessionDet.DataBind()
                Dim columnCount As Integer = gvFEEConcessionDet.Rows(0).Cells.Count
                gvFEEConcessionDet.Rows(0).Cells.Clear()
                gvFEEConcessionDet.Rows(0).Cells.Add(New TableCell)
                gvFEEConcessionDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEConcessionDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEConcessionDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEConcessionDet.DataBind()
            End If
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
            txtSearch.Text = lstrCondn3
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn4
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
            txtSearch.Text = lstrCondn5
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEConcessionDet.PageIndexChanging
        gvFEEConcessionDet.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim str_ids As String = String.Empty
        Dim str_First_STP_ID As String = String.Empty
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Try
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As Integer
                For Each gvr As GridViewRow In gvFEEConcessionDet.Rows
                    Dim lblSTP_ID As Label = CType(gvr.FindControl("lblSTP_ID"), Label)
                    Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
                    If Not lblSTP_ID Is Nothing Then
                        If IsNumeric(lblSTP_ID.Text) And chkPost.Checked Then
                            str_ids = str_ids & "," & lblSTP_ID.Text
                            If str_First_STP_ID = "" Then
                                str_First_STP_ID = lblSTP_ID.Text
                            End If
                            retval = F_POSTPAYMENTPLAN(Session("sBsuid"), txtFrom.Text, lblSTP_ID.Text, "A", stTrans)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
                If (retval = 0) And str_ids = "" Then
                    stTrans.Rollback()
                    'lblError.Text = "Please Select Atleast One Plan!!!"
                    usrMessageBar.ShowNotification("Please Select Atleast One Plan!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                If (retval = 0) Then
                    stTrans.Commit()
                    'lblError.Text = getErrorMessage(0)
                    usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
                    GridBind()

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_ids, "Posting", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub 

    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbApproved.CheckedChanged, rbRejected.CheckedChanged
        GridBind()
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim str_ids As String = String.Empty
        Dim str_First_STP_ID As String = String.Empty
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Try
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As Integer
                For Each gvr As GridViewRow In gvFEEConcessionDet.Rows
                    Dim lblSTP_ID As Label = CType(gvr.FindControl("lblSTP_ID"), Label)
                    Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
                    If Not lblSTP_ID Is Nothing Then
                        If IsNumeric(lblSTP_ID.Text) And chkPost.Checked Then
                            str_ids = str_ids & "," & lblSTP_ID.Text
                            If str_First_STP_ID = "" Then
                                str_First_STP_ID = lblSTP_ID.Text
                            End If
                            retval = F_POSTPAYMENTPLAN(Session("sBsuid"), txtFrom.Text, lblSTP_ID.Text, "R", stTrans)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
                If (retval = 0) And str_ids = "" Then
                    stTrans.Rollback()
                    'lblError.Text = "Please Select Atleast One Plan!!!"
                    usrMessageBar.ShowNotification("Please Select Atleast One Plan!!!", UserControls_usrMessageBar.WarningType.Information)
                    Exit Sub
                End If
                If (retval = 0) Then
                    stTrans.Commit()
                    'lblError.Text = getErrorMessage(0)
                    usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
                    GridBind()

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_ids, "Posting", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Public Shared Function F_POSTPAYMENTPLAN(ByVal BSU_ID As String, ByVal Dt As String, _
    ByVal STP_ID As String, ByVal STATUS As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(1).Value = Dt
        pParms(2) = New SqlClient.SqlParameter("@STP_ID", SqlDbType.BigInt)
        pParms(2).Value = STP_ID
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        pParms(4) = New SqlClient.SqlParameter("@STATUS", SqlDbType.VarChar)
        pParms(4).Value = STATUS
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_POSTPAYMENTPLAN", pParms)
        Return pParms(3).Value
    End Function

End Class
