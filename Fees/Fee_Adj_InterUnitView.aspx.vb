Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_Fee_Adj_InterUnitView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                gvFEEAdjustments.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300149" And ViewState("MainMnu_code") <> "F300152" And ViewState("MainMnu_code") <> "F300184" And ViewState("MainMnu_code") <> "F300185") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If

                Select Case ViewState("MainMnu_code").ToString
                    Case "F300149", "F300185"
                        hlAddNew.NavigateUrl = "Fee_Adj_InterUnit.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Fee Adjustment Request- Inter Unit"
                        hlAddNew.Visible = True
                    Case "F300152", "F300184" 'Approval/Reject
                        lblHeader.Text = "Approve Fee Adjustment Request- Inter Unit"
                        hlAddNew.Visible = False
                End Select
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEAdjustments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEAdjustments.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            If gvFEEAdjustments.Rows.Count > 0 Then
                '   --- FILTER CONDITIONS ---
                '   -- 1   STU_NAME
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn1)

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudno")
                lstrCondn6 = Trim(txtSearch.Text)
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn6)


                '   -- 1  FAI_DATE
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAI_DATE", lstrCondn2)

                '   -- 2  GRD_DISPLAY
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)

                '   -- 3   remarks
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAI_REMARKS", lstrCondn4)

                ''   -- 3  reason
                'larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtReason")
                'lstrCondn5 = txtSearch.Text
                'If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ARM_DESCR", lstrCondn5)
            End If

            Dim str_cond As String = String.Empty
            Dim vStatus As String = String.Empty
            If radApproved.Checked Then
                vStatus = "A"
            ElseIf radReject.Checked Then
                vStatus = "R"
            ElseIf radRequested.Checked Then
                vStatus = "N"
            End If
            Dim STU_TYPE = IIf(radStud.Checked, "S", "E")

            'FEES.FEEADJREQUEST_H.FAR_REMARKS FAH_REMARKS,
            Dim selFilter As String = " FAI_BSU_ID ='" & Session("sBsuid") & "'" _
            & " AND ISNULL(FAI_bDELETED,0)=0 AND ISNULL(FAI_APPR_STATUS,'') = '" & vStatus & "' AND FAI_STU_TYPE='" & STU_TYPE & "' "

            If ViewState("MainMnu_code") = "F300149" Or ViewState("MainMnu_code") = "F300152" Then
                selFilter = selFilter & " AND ISNULL(FAI_DR_IsProvider,0) = 0 "
            Else
                selFilter = selFilter & " AND ISNULL(FAI_DR_IsProvider,0) = 1 "
            End If

            Dim strGroupBy As String = ""

            str_Sql = "SELECT * FROM (SELECT STU_NO,STU_NAME,GRD_DISPLAY,FAI_DATE,FAI_REMARKS,FAI_ID,OBU_STATUS AS [STATUS] FROM  FEES.vw_FEE_ADJ_INTERUNIT " & _
                "WHERE 1=1 AND " & selFilter & ")AS A WHERE 1=1 " & str_Filter

            Dim str_orderby As String = " ORDER BY FAI_DATE DESC,FAI_ID DESC "

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
            CommandType.Text, str_Sql & str_orderby)
            gvFEEAdjustments.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEAdjustments.DataBind()
                Dim columnCount As Integer = gvFEEAdjustments.Rows(0).Cells.Count

                gvFEEAdjustments.Rows(0).Cells.Clear()
                gvFEEAdjustments.Rows(0).Cells.Add(New TableCell)
                gvFEEAdjustments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEAdjustments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEAdjustments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEAdjustments.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn4

            'txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtReason")
            'txtSearch.Text = lstrCondn5

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEAdjustments.PageIndexChanging
        gvFEEAdjustments.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEAdjustments.RowDataBound
        Try
            Dim lblFAI_ID As New Label
            lblFAI_ID = TryCast(e.Row.FindControl("lblFAI_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            Dim vStatus As String = String.Empty
            If radApproved.Checked Then
                vStatus = "A"
            ElseIf radReject.Checked Then
                vStatus = "R"
            ElseIf radRequested.Checked Then
                vStatus = "N"
            End If
            If hlEdit IsNot Nothing And lblFAI_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("MainMnu_code").ToString
                    Case "F300149", "F300185"
                        hlEdit.NavigateUrl = "Fee_Adj_InterUnit.aspx?FAI_ID=" & Encr_decrData.Encrypt(lblFAI_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case "F300152", "F300184"
                        hlEdit.NavigateUrl = "Fee_Adj_InterUnit.aspx?FAI_ID=" & Encr_decrData.Encrypt(lblFAI_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("approve") & "&status=" & Encr_decrData.Encrypt(vStatus)
                End Select
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radRequested_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radRequested.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radApproved.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radReject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radReject.CheckedChanged
        GridBind()
    End Sub
End Class
