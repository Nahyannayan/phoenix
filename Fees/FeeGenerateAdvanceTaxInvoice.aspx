﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeGenerateAdvanceTaxInvoice.aspx.vb" Inherits="Fees_FeeGenerateAdvanceTaxInvoice" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link href="../Scripts/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />--%>
    <style>
        .RadComboBox_Default .rcbReadOnly {
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbDisabled {
            background-color: rgba(0,0,0,0.01) !important;
        }

            .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                background-color: transparent !important;
                border-radius: 0px !important;
                border: 0px !important;
                padding: initial !important;
                box-shadow: none !important;
            }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Generate Advance Tax Invoice
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="2" cellspacing="0"
                    style="border-collapse: collapse;" width="100%">
                    <%-- <tr class="subheader_img">
            <td colspan="3" style="height: 19px" align="left">Generate Advance Tax Invoice
            </td>
        </tr>--%>
                    <tr>
                        <td align="left" width="10%"><span class="field-label">Fee Type</span></td>
                        <td align="left" width="40%">
                            <telerik:RadComboBox ID="ddlFeeType" Width="100%" RenderMode="Lightweight" runat="server" Filter="Contains" AutoPostBack="true" ZIndex="2000" ToolTip="Type in fee type"></telerik:RadComboBox>
                        </td>

                        <td align="left" width="10%"><span class="field-label">Company</span></td>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtCompanyDescr" runat="server" ToolTip="Type in if company name is known or click search image" placeholder="Type in or Search Company Name"></asp:TextBox>
                            <asp:ImageButton ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetCompany(); return false;" />
                            <asp:LinkButton ID="lbtnClearCompany" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                            <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                TargetControlID="txtCompanyDescr"
                                WatermarkText="Type in or Search Company Name"
                                WatermarkCssClass="watermarked" />--%>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server"
                                TargetControlID="lbtnClearCompany"
                                ConfirmText="Are you sure you want to remove the company selection? It will also clear the selected students." />
                            <asp:HiddenField ID="h_Company_ID" runat="server" />                            
                            </td>
                    </tr>
                    <tr>
                        <td align="left" width="10%"><span class="field-label">Period</span></td>

                        <td align="left" width="40%">
                            <table class="table table-bordered table-row">
                                <tr>
                                    <th>Academic Year</th>
                                    <th align="center" runat="server" id="tdPeriod1">Schedule</th>
                                    <th runat="server" id="tdterms1">Select Month(s)/Term(s)</th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" TabIndex="10" Width="100%">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" runat="server" id="tdPeriod2">
                                        <asp:RadioButtonList ID="rblTermOrMonth" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="T">Termly</asp:ListItem>
                                            <asp:ListItem Value="M">Monthly</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="center" runat="server" id="tdterms2">
                                        <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;" Text="Select Term(s)"></asp:LinkButton>
                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" TargetControlID="lnkBtnSelectmonth"
                                            PopupControlID="Panel2" CommitProperty="value" Position="Bottom" CommitScript="" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trMessage">
                                    <td align="left" colspan="3">
                                        <asp:Label ID="Label1" runat="server" Text="*Only future Terms/Months will be shown" CssClass="highlight"></asp:Label>
                                    </td>

                                </tr>
                            </table>
                        </td>

                        <td align="left" width="10%"><span class="field-label">Select Student</span></td>

                        <td align="left" width="40%">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="40%" class="p-0">
                                        <asp:CheckBox ID="chkCompanyFilter" Text="Company Filter" runat="server" CssClass="field-label" />
                                    </td>
                                    <td align="left" width="60%" class="p-0">
                                        <asp:RadioButtonList ID="rblStuType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                <asp:ListItem Selected="True" Value="S"><span class="field-label">Enrolled</span></asp:ListItem>
                                <asp:ListItem Value="E"><span class="field-label">Enquiry</span></asp:ListItem>
                            </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                            
                            
                            
                            <asp:TextBox ID="txtStudent" runat="server" AutoPostBack="true" placeholder="Type in or Search Student Id or Name"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(); return false;" />
                            <asp:LinkButton ID="lbtnClearStudent" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                            <asp:LinkButton ID="lbtnLoadStuDetail" runat="server" CausesValidation="False"></asp:LinkButton>
                            <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TBWE3" runat="server"
                                TargetControlID="txtStudent"
                                WatermarkText="Type in or Search Student Id or Name"
                                WatermarkCssClass="watermarked" />--%>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                TargetControlID="lbtnClearStudent"
                                ConfirmText="Are you sure you want to remove the Student selection?" />
                            <asp:HiddenField ID="h_STU_ID" runat="server" OnValueChanged="h_STU_ID_ValueChanged" />
                        </td>
                    </tr>
                    <tr id="trBulkStudents" runat="server" visible="false">
                        <td align="left" width="10%"><span class="field-label">Student Bulk Import</span></td>

                        <td align="left" width="40%">
                            <asp:TextBox ID="txtStudentBulk" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                TargetControlID="txtStudentBulk"
                                WatermarkText="Input Student Id with comma (,) seperation. eg:- 12345,54321,56897"
                                WatermarkCssClass="watermarked" />
                            <asp:RequiredFieldValidator ID="rfvtxtStudentBulk" ControlToValidate="txtStudentBulk"
                                Display="Dynamic" Text="Enter Student Id with comma (,) seperation" runat="server" ValidationGroup="1" />

                        </td>
                        <td>
                            <asp:Button ID="btnImport" runat="server" CssClass="button" Text="Import" CausesValidation="true" ValidationGroup="1" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr class="mt-3">
                        <td align="left" class="title-bg" colspan="4">Student Details
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:UpdatePanel ID="up3" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvStudents" CssClass="table table-bordered table-row" runat="server" Width="100%" AutoGenerateColumns="False" EmptyDataText="No students to show" DataKeyNames="ID,STU_ID" AllowPaging="True">
                                        <Columns>
                                            <asp:BoundField DataField="ID" HeaderText="SlNo" />
                                            <asp:BoundField DataField="STU_NO" HeaderText="StudentId" />
                                            <asp:BoundField DataField="STU_NAME" HeaderText="Name" HtmlEncode="false" />
                                            <asp:BoundField DataField="GRD_DISPLAY" HeaderText="Grade" HtmlEncode="false" />
                                            <asp:TemplateField HeaderText="Adv.Inv.Balance">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAdvBalance" runat="server" Text='<%# Bind("ADV_BALANCE", "{0:0.00}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkReverseAll" Text="Reverse Balance" runat="server" AutoPostBack="True" OnCheckedChanged="chkReverseAll_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkReverseInv" runat="server" Checked='<%# Bind("bREVERSE")%>' AutoPostBack="True" OnCheckedChanged="chkReverseInv_CheckedChanged" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right; width: 80px;" Text='<%# Bind("AMOUNT", "{0:0.00}") %>'></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                                                        ValidChars="." TargetControlID="txtAmount" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnLedger" runat="server">View Ledger</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnVwInvoices" runat="server">View Invoices</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnDelete" runat="server" OnClick="lbtnDelete_Click">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings PageButtonCount="15" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="10%"><span class="field-label">Narration</span></td>

                        <td align="left" width="40%">
                            <asp:TextBox ID="txtNarration" runat="server" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtNarration"
                                Display="Dynamic" Text="Narration is required" runat="server" ValidationGroup="2" />

                        </td>
                        <td>
                            <asp:Button ID="btnFillNarration" runat="server" CssClass="button" Text="Fill Auto Narration" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" CausesValidation="true" ValidationGroup="2" /><asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button" /><asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel2" runat="server" CssClass="panel-cover">
                    <div style="text-align: left">
                        <asp:UpdatePanel runat="server" ID="up2">
                            <ContentTemplate>
                                <asp:TreeView ID="trMonths" ShowCheckBoxes="all" onclick="client_OnTreeNodeChecked();"
                                    runat="server">
                                </asp:TreeView>
                                <div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
                    ReloadOnShow="true" runat="server" EnableShadow="true">
                    <Windows>
                        <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>
                    <Windows>
                        <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>
                </telerik:RadWindowManager>

                <script type="text/javascript" lang="javascript">
                    $(document).ready(function () {
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        prm.add_initializeRequest(InitializeRequest);
                        prm.add_endRequest(EndRequest);
                        InitStudentAutoComplete();
                        InitCompanyAutoComplete();
                    });
                    function InitializeRequest(sender, args) {
                    }
                    function EndRequest(sender, args) {
                        // after update occur on UpdatePanel re-init the Autocomplete
                        InitStudentAutoComplete();
                        InitCompanyAutoComplete();
                    }

                    function InitCompanyAutoComplete() {
                       
                        $("#<%=txtCompanyDescr.ClientID%>").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: "../GlobalSearchService.asmx/GetCompany",
                                    data: "{ 'prefix': '" + request.term + "','COUNTRY_ID': '" + GetCountryId() + "'}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('-')[0],
                                                val: item.split('-')[1]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        alert(response.responseText);
                                    },
                                    failure: function (response) {
                                        alert(response.responseText);
                                    }
                                });
                            },
                            select: function (e, i) {
                                $("#<%=h_Company_ID.ClientID%>").val(i.item.val);
                            },
                            minLength: 1
                        });
                        }

                        function InitStudentAutoComplete() {
                        
                            $("#<%=txtStudent.ClientID%>").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: "../GlobalSearchService.asmx/GetStudent",
                                    data: "{ 'BSUID': '" + GetBsuId() + "','STU_TYPE':'" + GetStuType() + "','prefix': '" + request.term + "','COMPID':'" + GetCompanyId() + "','STUBSUID':'" + GetBsuId() + "'}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('$$')[1] + ' - ' + item.split('$$')[0],
                                                val: item.split('$$')[2]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        alert(response.responseText);
                                    },
                                    failure: function (response) {
                                        alert(response.responseText);
                                    }
                                });
                            },
                            select: function (e, i) {
                                $("#<%=h_STU_ID.ClientID%>").val(i.item.val);
                                $("#<%=txtStudent.ClientID%>").val('');
                                document.getElementById('<%=lbtnLoadStuDetail.ClientID%>').click();
                            },
                            minLength: 1
                        });
                    }

                    function GetCompanyId() {
                        var COMP_ID = $('#<%=h_Company_ID.ClientID %>').val();
                        var CompFilter = $('#<%=chkCompanyFilter.ClientID%>').is(':checked');
                        if (CompFilter != true) {
                            COMP_ID = "-1";
                        }
                        return COMP_ID;
                    }

                    function GetBsuId() {
                        //var dropFind = $find("<%= Session("sBsuId")%>");
                        //var valueFind = dropFind.get_value();
                        return '<%= Session("sBsuId")%>';
                    }
                    function GetCountryId() {
                       
                        return '<%= Session("BSU_COUNTRY_ID")%>';
                    }
                    function GetStuType() {
                        var STU_TYPE = $('#<%= rblStuType.ClientID%>').find(":checked").val();

                        return STU_TYPE;
                    }
                    function GetAcdId() {
                        //var dropFind = $find("<%= ddlAcademicYear.ClientID%>");
                        var valueFind = $('#<%= ddlAcademicYear.ClientID%>').val();
                        return valueFind;
                    }
                    function GetFeeId() {
                        var valueFind = $find("<%= ddlFeeType.ClientID%>").get_selectedItem().get_value();
                        //var dropFind = $('#<%= ddlFeeType.ClientID%>').val();
                        return valueFind;
                    }
                    function GetStudent() {

                        var NameandCode;
                        var result;
                        var COMP_ID = $('#<%=h_Company_ID.ClientID %>').val();
                        var CompFilter = $('#<%=chkCompanyFilter.ClientID%>').is(':checked');
                        if (CompFilter != true) {
                            COMP_ID = "-1";
                        }
                        var url = "";
                        if (GetStuType() == "E") {
                            url = "ShowStudentMulti.aspx?TYPE=ENQ_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&MULTI_SEL=1&ACD_ID=" + GetAcdId() + "&IsService=0";
                        }
                        else if (GetStuType() == "S") {
                            url = "ShowStudentMulti.aspx?TYPE=STUD_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&MULTI_SEL=1&ACD_ID=" + GetAcdId() + "&IsService=0&FeeTypes=" + GetFeeId() + "&NewStudent=0&EXCL_TC=";
                        }
                        result = radopen(url, "pop_up");
                        <%--if (result != '' && result != undefined) {
                                        NameandCode = result.split('||');
                                        if (NameandCode.length > 1) {
                                            document.getElementById('<%=h_STU_ID.ClientID%>').value = result;
                                            document.getElementById('<%=txtStudent.ClientID%>').value = "Multiple students selected";
                                        }
                                        else if (NameandCode.length = 1) {
                                            document.getElementById('<%=h_STU_ID.ClientID%>').value = NameandCode[0];
                                    }
                                return true;--%>
                   
                        //else {
                        //    return false;
                        //}
                    }

                    function autoSizeWithCalendar(oWindow) {
                        var iframe = oWindow.get_contentFrame();
                        var body = iframe.contentWindow.document.body;
                        var height = body.scrollHeight;
                        var width = body.scrollWidth;
                        var iframeBounds = $telerik.getBounds(iframe);
                        var heightDelta = height - iframeBounds.height;
                        var widthDelta = width - iframeBounds.width;
                        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                        oWindow.center();
                    }

                    function OnClientClose(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {
                            NameandCode = arg.NameandCode.split('||');
                            if (NameandCode.length > 1) {                            
                                document.getElementById('<%=h_STU_ID.ClientID%>').value = arg.NameandCode;
                                document.getElementById('<%=txtStudent.ClientID%>').value = "Multiple Students Selected";
                            }
                            else if (NameandCode.length = 1) {                               
                                document.getElementById('<%=h_STU_ID.ClientID%>').value = NameandCode[0];
                            }
                        __doPostBack('<%= h_STU_ID.ClientID%>', 'ValueChanged');
                        }
                    }

                    function GetCompany() {

                        var NameandCode;
                        var result;
                        var url = "../Common/popupForm.aspx?ID=COMPANY&multiSelect=false";
                        result = radopen(url, "pop_up2");
                                        <%--  if (result != '' && result != undefined) {
                            NameandCode = result.split('___');
                            document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                            return true;
                        }
                        else {
                            return false;
                        }--%>
                    }

                    function OnClientClose2(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {
                            NameandCode = arg.NameandCode.split('||');                           
                            document.getElementById('<%=h_Company_ID.ClientID%>').value = NameandCode[0];                           
                            document.getElementById('<%=txtCompanyDescr.ClientID%>').value = NameandCode[1];                           
                            __doPostBack('<%= txtCompanyDescr.ClientID%>', 'TextChanged');                           
                        }
                    }

                    function client_OnTreeNodeChecked() {
                        var obj = window.event.srcElement;
                        var treeNodeFound = false;
                        var checkedState;
                        if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                            var treeNode = obj;
                            checkedState = treeNode.checked;
                            do {
                                obj = obj.parentElement;
                            } while (obj.tagName != "TABLE")
                            var parentTreeLevel = obj.rows[0].cells.length;
                            var parentTreeNode = obj.rows[0].cells[0];
                            var tables = obj.parentElement.getElementsByTagName("TABLE");
                            var numTables = tables.length
                            if (numTables >= 1) {
                                for (i = 0; i < numTables; i++) {
                                    if (tables[i] == obj) {
                                        treeNodeFound = true;
                                        i++;
                                        if (i == numTables) {
                                            return;
                                        }
                                    }
                                    if (treeNodeFound == true) {
                                        var childTreeLevel = tables[i].rows[0].cells.length;
                                        if (childTreeLevel > parentTreeLevel) {
                                            var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                            var inputs = cell.getElementsByTagName("INPUT");
                                            inputs[0].checked = checkedState;
                                        }
                                        else {
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });
                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                    $(function () {

                        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                            item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong style='background:yellow;'>$1</strong>");
                            return $("<li></li>")
                                    .data("item.autocomplete", item)
                                    .append("<a>" + item.label + "</a>")
                                    .appendTo(ul);
                        };
                    });
                </script>

            </div>
        </div>
    </div>
</asp:Content>
