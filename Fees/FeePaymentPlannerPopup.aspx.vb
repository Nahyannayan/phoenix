﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic
Partial Class Fees_FeePaymentPlannerPopup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Property FPP_ID() As Integer
        Get
            Return ViewState("FPP_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("FPP_ID") = value
        End Set
    End Property
    Public Property TotalDue() As Double
        Get
            Return ViewState("dbl_TotalDue")
        End Get
        Set(ByVal value As Double)
            ViewState("dbl_TotalDue") = value
        End Set
    End Property
    Public Property TotalUpcomingDue() As Double
        Get
            Return ViewState("dbl_TotalUpcomingDue")
        End Get
        Set(ByVal value As Double)
            ViewState("dbl_TotalUpcomingDue") = value
        End Set
    End Property
    Public Property GrandTotalDue() As Double
        Get
            Return ViewState("dbl_GrandTotalDue")
        End Get
        Set(ByVal value As Double)
            ViewState("dbl_GrandTotalDue") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = "add"
            End If
            If Session("sUsr_name") = "" Or Session("sBSuid") = "" Or (ViewState("MainMnu_code") <> "F300162" And ViewState("MainMnu_code") <> "F300171" And ViewState("MainMnu_code") <> "F300271") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBSuid"), ViewState("MainMnu_code"))
                'Call AccessRight.setpage(Page.Master.FindControl("btn_set"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            InitialiseCompnents()
            Select Case ViewState("datamode")
                Case "view"
                    If ViewState("MainMnu_code") = "F300162" Then 'view
                        FPP_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        id_finalComments.Visible = False
                        LoadSavedData(FPP_ID)
                        DisableorEnableControls(False)

                        'below buttons not required in this stage these are approval level buttons - EDIT/VIEW/SUBMIT STAGES HERE
                        Me.btnApprove.Visible = False
                        Me.btnReject.Visible = False
                        Me.btnOnhold.Visible = False
                        Me.btnRevert.Visible = False

                        'Me.btnSubmit.Visible = False
                        gvPayPlanDetail.Enabled = True
                        id_fin_cmnts.Visible = False
                        id_princ_cmnts.Visible = False

                    ElseIf ViewState("MainMnu_code") = "F300171" Then 'approve/reject
                        FPP_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        id_finalComments.Visible = False
                        LoadSavedData(FPP_ID)
                        DisableorEnableControls(False)

                        'below buttons not required in this stage these are save/edit/delete/submit level buttons - APPROVAL STAGE
                        Me.btnDelete.Visible = False
                        Me.btnSave.Visible = False
                        Me.btnSubmit.Visible = False
                        Me.btnEdit.Visible = False

                        PnlOuter2.Visible = False
                        gvPayPlanDetail.Columns(7).Visible = False
                        gvPayPlanDetail.Columns(8).Visible = False
                        gvPayPlanDetail.Enabled = False

                        id_cashier.Visible = False
                        id_fin_cmnts.Visible = True
                        id_princ_cmnts.Visible = False
                    ElseIf ViewState("MainMnu_code") = "F300271" Then 'approve/reject
                        FPP_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        id_finalComments.Visible = False
                        LoadSavedData(FPP_ID)
                        DisableorEnableControls(False)

                        Dim ViewMode As String = (Encr_decrData.Decrypt(Request.QueryString("viewmode").Replace(" ", "+")))
                        If ViewMode = "view" Then
                            btnApprove.Visible = False
                            btnReject.Visible = False
                            btnRevert.Visible = False
                            btnOnhold.Visible = False
                        End If

                        'below buttons not required in this stage these are save/edit/delete/submit level buttons - APPROVAL STAGE
                        Me.btnDelete.Visible = False
                        Me.btnSave.Visible = False
                        Me.btnEdit.Visible = False
                        Me.btnSubmit.Visible = False

                        id_cashier.Visible = False
                        PnlOuter2.Visible = False
                        gvPayPlanDetail.Columns(7).Visible = False
                        gvPayPlanDetail.Columns(8).Visible = False
                        gvPayPlanDetail.Enabled = False

                    End If

                Case Else
                    Session("gintGridLine") = 1
                    id_finalComments.Visible = False
                    id_fin_cmnts.Visible = False
                    id_princ_cmnts.Visible = False

                    'below buttons not required in this stage these are approval/submit level buttons -NEW STAGE
                    Me.btnApprove.Visible = False
                    Me.btnReject.Visible = False
                    Me.btnOnhold.Visible = False
                    Me.btnRevert.Visible = False
                    Me.btnSubmit.Visible = False

                    gvPayPlanDetail.Columns(7).Visible = False
                    gvPayPlanDetail.Columns(8).Visible = False
                    PnlOuter2.Visible = True
                    FPP_ID = 0
            End Select
            'Dim dt As DataTable = stam()

        End If
    End Sub
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    'End Sub
    'Private Sub studentSelected(sender As Object, e As EventArgs) Handles UsrSelStudent.StudentNoChanged
    '    If ViewState("datamode") = "edit" Then
    '        clearAll(True)
    '    End If
    '    btnLoad_Click(sender, e)
    '    Me.lblGrade.Text = UsrSelStudent.GRM_DISPLAY
    'End Sub
    Private Sub InitialiseCompnents()
        Me.txtDT.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        Me.txtPayDT.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        Me.txtChqDT.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        TotalDue = 0
        GrandTotalDue = 0
        TotalUpcomingDue = 0
        FPP_ID = 0
        BindAcademicYear()
        uscStudentPicker.STU_ACD_ID = ddlAcademicYear.SelectedValue

        'UsrSelStudent.IsStudent = True
        'UsrSelStudent.ACD_ID = ddlAcademicYear.SelectedValue
        BindFeeType(True)
        BindcollectionType()
    End Sub
    Private Sub DisableorEnableControls(ByVal Flag As Boolean)
        Me.PnlOuter.Enabled = Flag
        'Me.gvPayPlanDetail.Enabled = Flag
        'Me.btnAddPP.Enabled = Flag
    End Sub
    Protected Sub uscStudentPicker_StudentCleared(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentCleared
        clearAll()
    End Sub
    Protected Sub uscStudentPicker_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentNoChanged
        Try
            If ViewState("datamode") = "edit" Then
                clearAll(True)
            End If
            Dim objFPP As New FeePaymentPlanner
            objFPP.GetContactDetails()
            'lblParentName.Text = "Parent:" + objFPP.ContactName
            'lblEmail.Text = "Email:" + objFPP.Email
            'lblMobileNo.Text = "Mobile:" + objFPP.MobileNo
            'lblResidenceNo.Text = "Pesidence:" + objFPP.ResidenceNo
            lblParentDetails.Text = "<b>Parent: </b>" + objFPP.ContactName + " , " + "<b>Email: </b>" + objFPP.Email + " , " + "<b>Mobile No: </b>" + objFPP.MobileNo + " , " + "<b>Residence No: </b>" + objFPP.ResidenceNo

            btnLoad_Click(sender, e)
            uscStudentPicker.STU_MODULE = "FEE"
            'Me.lblGrade.Text = UsrSelStudent.GRM_DISPLAY
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadSavedData(ByVal FPPID As String)
        Dim objFPP As New FeePaymentPlanner
        objFPP.FPP_ID = FPPID
        objFPP.GetSavedDetails()
        Me.btnSubmit.Visible = True
        If objFPP.IS_CASHIER_EDITABLE(FPPID) Then ' FB PB PA
            btnEdit.Visible = True
            btnDelete.Visible = True
            If objFPP.IsPrincipalApprove Then 'PA
                Me.btnSubmit.Visible = True
                gvPayPlanDetail.Columns(7).Visible = False
                gvPayPlanDetail.Columns(8).Visible = True
                'gvPayPlanDetail.Columns(9).Visible = False
            Else
                Me.btnSubmit.Visible = False
                gvPayPlanDetail.Columns(7).Visible = False
                gvPayPlanDetail.Columns(8).Visible = False
                'gvPayPlanDetail.Columns(9).Visible = True
            End If
        Else
            Me.btnEdit.Visible = False
            Me.btnDelete.Visible = False
            Me.btnSubmit.Visible = False
        End If

        If objFPP.FPP_PLAN_TYPE = "PARENT" Then
            id_PayPlanReq.Visible = True
            rblParentPaymodesReq.SelectedValue = objFPP.FPP_REQ_MODE_OF_PAY
        Else
            id_PayPlanReq.Visible = False
        End If

        If objFPP.IsAmountEditable = 0 Then 'STATUS 3
            PnlOuter2.Visible = False
            gvPayPlanDetail.Columns(7).Visible = False
            gvPayPlanDetail.Columns(8).Visible = True
            'gvPayPlanDetail.Columns(9).Visible = False
            Me.btnSubmit.Visible = True

        End If
        ddlAcademicYear.SelectedValue = objFPP.ACD_ID
        'UsrSelStudent.SetStudentDetails(objFPP.STU_ID)
        uscStudentPicker.STU_ID = objFPP.STU_ID
        uscStudentPicker.LoadStudentsByID(objFPP.STU_ID)
        objFPP.GetContactDetails()
        lblParentDetails.Text = "<b>Parent: </b>" + objFPP.ContactName + " , " + "<b>Email: </b>" + objFPP.Email + " , " + "<b>Mobile No: </b>" + objFPP.MobileNo + " , " + "<b>Residence No: </b>" + objFPP.ResidenceNo

        'uscStudentPicker_StudentNoChanged(Nothing, Nothing)
        'studentSelected("", Nothing)
        Me.txtDT.Text = Format(Convert.ToDateTime(objFPP.DOCDATE), OASISConstants.DataBaseDateFormat)
        ViewState("gvStudentDetail") = XMLtoDataTable(objFPP.Aging_XML)
        showSelectedFees(objFPP.FEE_IDs)
        'Fee for upcoming Terms/Months
        Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param(1) = New SqlClient.SqlParameter("@BSU_IDS", Session("sBSuid").ToString)
        param(2) = New SqlClient.SqlParameter("@GRD_IDS", uscStudentPicker.STU_GRD_ID.ToString)
        param(3) = New SqlClient.SqlParameter("@FEE_IDS", objFPP.FEE_IDs)
        param(4) = New SqlClient.SqlParameter("@STU_ID", objFPP.STU_ID.ToString)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[dbo].[FEE_FUTURE_PAYMENTS]", param)
        If Not ds.Tables(0) Is Nothing Then
            ViewState("gvFeeDetail") = ds.Tables(0)
        End If

        Bindgrid()
        ViewState("gvPayPlanDetail") = objFPP.gvPayPlanDetail
        BindGridPP()
        If (objFPP.bPLAN_APPROVED Or objFPP.bPLAN_REJECTED) And ViewState("MainMnu_code") = "F300271" Then
            'Me.lblError.Text = "The Payment plan has been approved"
            'usrMessageBar.ShowMessage("The Payment plan has been approved", True)
            Me.btnEdit.Enabled = False
            Me.btnDelete.Enabled = False
            btnApprove.Visible = False
            btnReject.Visible = False
            btnRevert.Visible = False
            btnOnhold.Visible = False

            id_princ_cmnts.Visible = False
            id_finalComments.Visible = True
            lblFinalComments.Text = "<b>Cashier Comments:</b> " & objFPP.Comments & "</br> <b>Finance Comments:</b> " & objFPP.FinanceComments & "</br> </br>  <b>Principal Comments:</b> " & objFPP.PrincipalComments & ""

            id_fin_cmnts.Visible = False
            id_princ_cmnts.Visible = False

        ElseIf objFPP.bPLAN_APPROVED = False And objFPP.bPLAN_REJECTED = False And ViewState("MainMnu_code") = "F300271" Then
            Me.btnEdit.Enabled = False
            Me.btnDelete.Enabled = False
            btnApprove.Visible = True
            btnReject.Visible = True
            btnRevert.Visible = True
            btnOnhold.Visible = True

            id_finalComments.Visible = True
            lblFinalComments.Text = "<b>Cashier Comments:</b> " & objFPP.Comments & "</br> <b>Finance Comments:</b> " & objFPP.FinanceComments & ""
            id_fin_cmnts.Visible = False
            id_princ_cmnts.Visible = True
        ElseIf ViewState("MainMnu_code") = "F300171" Then
            Me.btnEdit.Enabled = False
            Me.btnDelete.Enabled = False
            btnApprove.Visible = True
            btnReject.Visible = True
            btnRevert.Visible = True
            btnOnhold.Visible = True

            id_fin_cmnts.Visible = True
            id_princ_cmnts.Visible = False
        Else
            'Me.lblError.Text = ""
            'Me.btnEdit.Enabled = False
            'Me.btnDelete.Enabled = False
        End If
    End Sub

    Sub showSelectedFees(ByVal feeIDs As String)
        BindFeeType(False)
        Dim FEE_ID As String() = feeIDs.Split("|")
        For i As Int16 = 0 To FEE_ID.Length - 1 Step 1
            For Each li As ListItem In cblFeeType.Items
                If li.Value = FEE_ID(i) Then
                    li.Selected = True
                    'Else
                    '    li.Selected = False
                End If
            Next
        Next
    End Sub
    Public Function XMLtoDataTable(ByVal XMLData As String) As DataTable
        Dim theReader As New StringReader(XMLData)
        Dim theDataSet As New DataSet()
        theDataSet.ReadXml(theReader)
        Dim dtCloned = theDataSet.Tables(0).Clone
        dtCloned.Columns("TOTALDUE").DataType = Type.GetType("System.Double")
        For Each row As DataRow In theDataSet.Tables(0).Rows
            dtCloned.ImportRow(row)
        Next
        Return dtCloned
    End Function
    Private Sub clearAll(Optional ByVal KeepStudent As Boolean = False)
        If KeepStudent = False Then
            'Me.UsrSelStudent.ClearDetails()
            'Me.uscStudentPicker.ClearDetails()
        End If
        Try
            InitialiseCompnents()
        Catch Ex As Exception
        End Try
        ViewState("gvStudentDetail") = Nothing
        ViewState("gvFeeDetail") = Nothing
        Bindgrid()
        ViewState("gvPayPlanDetail") = Nothing
        BindGridPP()
        Me.lblGrade.Text = ""
        'Me.lblError.Text = ""
        hfBank.Value = ""
        hfBankCrd.Value = ""
        hfFPD_ID.Value = 0
        h_print.Value = ""
        lblGrandTotal.Text = ""
        txtPayAmt.Text = ""
        btnSubmit.Visible = False
        If ViewState("MainMnu_code") = "F300171" Or ViewState("MainMnu_code") = "F300271" Then
            btnApprove.Visible = False
            btnReject.Visible = False
            btnOnhold.Visible = False
            btnRevert.Visible = False
        End If
    End Sub
    Private Sub BindAcademicYear()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        ddlAcademicYear.Enabled = False
    End Sub
    Sub BindFeeType(ByVal flag As Boolean)
        Dim objFPP As New FeePaymentPlanner
        objFPP.BSU_ID = Session("sBSUID")
        objFPP.ACD_ID = Convert.ToInt32(ddlAcademicYear.SelectedValue)
        Dim dtFeeType As DataTable = objFPP.getFeeTypes()

        cblFeeType.DataSource = dtFeeType
        cblFeeType.DataTextField = "FEE_DESCR"
        cblFeeType.DataValueField = "FEE_ID"
        cblFeeType.DataBind()
        For Each li As ListItem In cblFeeType.Items
            li.Selected = flag
        Next
    End Sub
    Sub BindcollectionType()
        Dim objFPP As New FeePaymentPlanner
        Dim dtCollType As DataTable = getCollectionTypes() 'objFPP.getCollectionTypes

        rblPaymodes.DataSource = dtCollType
        rblPaymodes.DataTextField = "CLT_DESCR"
        rblPaymodes.DataValueField = "CLT_ID"
        rblPaymodes.DataBind()
        Me.rblPaymodes.SelectedIndex = 0
    End Sub
    Sub BindParentcollectionType()
        Dim objFPP As New FeePaymentPlanner
        Dim dtCollType As DataTable = getCollectionTypes() 'objFPP.getCollectionTypes

        rblParentPaymodesReq.DataSource = dtCollType
        rblParentPaymodesReq.DataTextField = "CLT_DESCR"
        rblParentPaymodesReq.DataValueField = "CLT_ID"
        rblParentPaymodesReq.DataBind()
        Me.rblParentPaymodesReq.SelectedIndex = 0
    End Sub

    Protected Sub OnrblPaymodes_Changed(sender As Object, e As EventArgs)
        If Me.rblPaymodes.SelectedValue = 2 Then
            trChq.Visible = True
        Else
            trChq.Visible = False
        End If

    End Sub
    Public Function getCollectionTypes() As DataTable

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.Text, "SELECT CLT_ID, CLT_DESCR, CLT_BREQBANKREC, CLT_ORDER FROM COLLECTIONTYP_M WHERE CLT_ID IN (2,9)")
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Protected Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        If bValidate() Then

            'Outstanding
            Dim objFPP As New FeePaymentPlanner
            objFPP.BSU_ID = Session("sBsuid")
            objFPP.ASONDATE = Me.txtDT.Text
            'objFPP.STU_ID = UsrSelStudent.STUDENT_ID
            objFPP.STU_ID = uscStudentPicker.STU_ID
            objFPP.Bkt1 = "30"
            objFPP.Bkt2 = "60"
            objFPP.Bkt3 = "90"
            objFPP.Bkt4 = "180"
            objFPP.FEE_IDs = GetSelectedFEE()
            Dim dt As New DataTable
            dt = objFPP.GetStudentFeeAging
            If Not dt Is Nothing Then
                ViewState("gvStudentDetail") = dt
            End If
            'Bindgrid()

            'Fee for upcoming Terms/Months
            Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            param(1) = New SqlClient.SqlParameter("@BSU_IDS", Session("sBSuid").ToString)
            param(2) = New SqlClient.SqlParameter("@GRD_IDS", uscStudentPicker.STU_GRD_ID.ToString)
            param(3) = New SqlClient.SqlParameter("@FEE_IDS", GetSelectedFEE())
            param(4) = New SqlClient.SqlParameter("@STU_ID", uscStudentPicker.STU_ID.ToString)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[dbo].[FEE_FUTURE_PAYMENTS]", param)
            If Not ds.Tables(0) Is Nothing Then
                ViewState("gvFeeDetail") = ds.Tables(0)
            End If
            Bindgrid()

        ElseIf Me.gvStudentDetail.Rows.Count <> 0 Then
            ViewState("gvStudentDetail") = Nothing
            ViewState("gvFeeDetail") = Nothing
            Bindgrid()
        End If
    End Sub
    Sub Bindgrid()
        Me.gvStudentDetail.DataSource = ViewState("gvStudentDetail")
        Me.gvStudentDetail.DataBind()

        Me.gvFeeDetail.DataSource = ViewState("gvFeeDetail")
        Me.gvFeeDetail.DataBind()

    End Sub
    Private Function bValidate() As Boolean
        bValidate = True
        'Me.lblError.Text = "" '
        'If UsrSelStudent.STUDENT_ID Is Nothing Or UsrSelStudent.STUDENT_ID.Trim = "" Or UsrSelStudent.STUDENT_NO = "" Then
        'If () > 0 Then
        If FeeCollection.GetDoubleVal(uscStudentPicker.STU_ID.ToString) <= 0 Or uscStudentPicker.STU_NO = "" Then
            bValidate = False
            'Me.lblError.Text = "Please select a student"
            usrMessageBar.ShowMessage("Please select a student", True)
            Exit Function
        ElseIf GetSelectedFEE() = "" Then
            bValidate = False
            'Me.lblError.Text = "Please select a FeeType"
            usrMessageBar.ShowMessage("Please select a FeeType", True)
            Exit Function
        End If
    End Function
    Private Function GetSelectedFEE() As String
        GetSelectedFEE = ""
        For Each li As ListItem In cblFeeType.Items
            If li.Selected Then
                GetSelectedFEE += "|" + li.Value
            End If
        Next

    End Function

    Protected Sub gvStudentDetail_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvStudentDetail.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblsum As Label = DirectCast(e.Row.FindControl("lblTotalDue"), Label)
            Dim due As String = Val(DirectCast(ViewState("gvStudentDetail"), DataTable).Compute("Sum(TOTALDUE)", "")).ToString("#,##0.00")
            TotalDue = due
            GrandTotalDue = TotalDue + TotalUpcomingDue
            lblsum.Text = "(TOTAL DUE)  :  " & due
            Me.txtPayAmt.Text = GrandTotalDue
            'lblGrandTotal.Text = "Grand Total :" + Convert.ToString(GrandTotalDue)
            'lblbalance.Text = "Balance : " + Convert.ToString(GrandTotalDue)
            lblGrandTotal.Text = "Grand Total :" + GrandTotalDue.ToString("#,##0.00")
            lblbalance.Text = "Balance : " + GrandTotalDue.ToString("#,##0.00")
        End If
    End Sub

    Protected Sub gvFeeDetail_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvFeeDetail.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(2).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(9).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(10).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(11).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(12).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(13).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(14).HorizontalAlign = HorizontalAlign.Right
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(1).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(2).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(9).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(10).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(11).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(12).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(13).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(14).HorizontalAlign = HorizontalAlign.Right
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblsum As Label = DirectCast(e.Row.FindControl("lblTotalUPCOMING"), Label)
            Dim due As String = Val(DirectCast(ViewState("gvFeeDetail"), DataTable).Compute("Sum(TOTAL)", "")).ToString("#,##0.00")
            TotalUpcomingDue = due
            GrandTotalDue = TotalDue + TotalUpcomingDue
            lblsum.Text = "(TOTAL UPCOMING)  :  " & due
            Me.txtPayAmt.Text = GrandTotalDue 'Convert.ToDouble(Val(txtPayAmt.Text)) + due
            'TotalDue = Convert.ToDouble(Val(txtPayAmt.Text)) + due
            'lblGrandTotal.Text = "Grand Total :" + Convert.ToString(GrandTotalDue)
            'lblbalance.Text = "Balance : " + Convert.ToString(GrandTotalDue)
            lblGrandTotal.Text = "Grand Total :" + GrandTotalDue.ToString("#,##0.00")
            lblbalance.Text = "Balance : " + Val(GrandTotalDue).ToString("#,##0.00")

            e.Row.Cells(14).HorizontalAlign = HorizontalAlign.Right
        End If
    End Sub

    Protected Sub btnAddPP_Click(sender As Object, e As EventArgs) Handles btnAddPP.Click
        Dim objpp As New FeePaymentPlanner
        Dim dtPayPlan As DataTable = objpp.CreateSchema
        If ValidateAdd() Then
            If Me.rblPaymodes.SelectedValue = "1" Then 'cash
                Dim dr As DataRow = dtPayPlan.NewRow
                dr("ID") = Session("gintGridLine") + 1
                dr("CLT_ID") = Me.rblPaymodes.SelectedValue
                dr("PAYMODE") = Me.rblPaymodes.SelectedItem.Text
                dr("PAY_DATE") = Me.txtPayDT.Text
                dr("AMOUNT") = Me.txtPayAmt.Text
                dr("CHQNO") = ""
                dr("CHQDT") = ""
                dr("BANK_ID") = "0"
                dr("CRR_ID") = "0"
                dr("BANK") = ""
                dr("COMMENTS") = Me.txtComments.Text.Trim
                dtPayPlan.Rows.Add(dr)
                dtPayPlan.AcceptChanges()
                LoadGridPayPlan(dtPayPlan)
                Me.txtComments.Text = ""
            ElseIf Me.rblPaymodes.SelectedValue = "2" Then 'cheque
                Dim dr As DataRow = dtPayPlan.NewRow
                dr("ID") = Session("gintGridLine") + 1
                dr("CLT_ID") = Me.rblPaymodes.SelectedValue
                dr("PAYMODE") = Me.rblPaymodes.SelectedItem.Text
                dr("PAY_DATE") = Me.txtPayDT.Text
                dr("AMOUNT") = Me.txtPayAmt.Text
                dr("CHQNO") = Me.txtChqNo.Text.Trim
                dr("CHQDT") = Me.txtChqDT.Text.Trim
                dr("BANK_ID") = hfBank.Value.Trim
                dr("CRR_ID") = "0"
                dr("BANK") = Me.txtBank.Text.Trim
                dr("COMMENTS") = Me.txtComments.Text.Trim
                dtPayPlan.Rows.Add(dr)
                dtPayPlan.AcceptChanges()
                LoadGridPayPlan(dtPayPlan)
                ClearCheque()
            ElseIf Me.rblPaymodes.SelectedValue = "3" Then 'card
                Dim dr As DataRow = dtPayPlan.NewRow
                dr("ID") = Session("gintGridLine") + 1
                dr("CLT_ID") = Me.rblPaymodes.SelectedValue
                dr("PAYMODE") = Me.rblPaymodes.SelectedItem.Text
                dr("PAY_DATE") = Me.txtPayDT.Text
                dr("AMOUNT") = Me.txtPayAmt.Text
                dr("CHQNO") = Me.txtCardNo.Text.Trim
                dr("CHQDT") = ""
                dr("BANK_ID") = "0"
                dr("CRR_ID") = ddCreditcard.SelectedValue
                dr("BANK") = Me.ddCreditcard.SelectedItem.Text
                dr("COMMENTS") = Me.txtComments.Text.Trim
                dtPayPlan.Rows.Add(dr)
                dtPayPlan.AcceptChanges()
                LoadGridPayPlan(dtPayPlan)
                ClearCard()
            ElseIf Me.rblPaymodes.SelectedValue = "4" Then 'Other Modes
                Dim dr As DataRow = dtPayPlan.NewRow
                dr("ID") = Session("gintGridLine") + 1
                dr("CLT_ID") = Me.rblPaymodes.SelectedValue
                dr("PAYMODE") = Me.rblPaymodes.SelectedItem.Text
                dr("PAY_DATE") = Me.txtPayDT.Text
                dr("AMOUNT") = Me.txtPayAmt.Text
                dr("CHQNO") = ""
                dr("CHQDT") = ""
                dr("BANK_ID") = "0"
                dr("CRR_ID") = "0"
                dr("BANK") = ""
                dr("COMMENTS") = Me.txtComments.Text.Trim
                dtPayPlan.Rows.Add(dr)
                dtPayPlan.AcceptChanges()
                LoadGridPayPlan(dtPayPlan)
                Me.txtComments.Text = ""
            ElseIf Me.rblPaymodes.SelectedValue = "9" Then 'DEBIT AUTHORITY
                Dim dr As DataRow = dtPayPlan.NewRow
                dr("ID") = Session("gintGridLine") + 1
                dr("CLT_ID") = Me.rblPaymodes.SelectedValue
                dr("PAYMODE") = Me.rblPaymodes.SelectedItem.Text
                dr("PAY_DATE") = Me.txtPayDT.Text
                dr("AMOUNT") = Me.txtPayAmt.Text
                dr("CHQNO") = ""
                dr("CHQDT") = ""
                dr("BANK_ID") = "0"
                dr("CRR_ID") = "0"
                dr("BANK") = ""
                dr("COMMENTS") = Me.txtComments.Text.Trim
                dtPayPlan.Rows.Add(dr)
                dtPayPlan.AcceptChanges()
                LoadGridPayPlan(dtPayPlan)
                Me.txtComments.Text = ""
            End If
        End If
        Session("gintGridLine") = Session("gintGridLine") + 1
    End Sub
    Private Sub ClearCheque()
        Me.txtChqNo.Text = ""
        Me.txtChqDT.Text = ""
        hfBank.Value = ""
        Me.txtBank.Text = ""
        Me.txtComments.Text = ""
    End Sub
    Private Sub ClearCard()
        Me.txtCardNo.Text = ""
        Me.txtChqDT.Text = ""
        hfBankCrd.Value = ""
        Me.txtComments.Text = ""
    End Sub
    Private Sub LoadGridPayPlan(ByVal dtPayPlan As DataTable)
        If Me.gvPayPlanDetail.Rows.Count = 0 Then
            ViewState("gvPayPlanDetail") = dtPayPlan
            BindGridPP()
        Else
            If Me.gvPayPlanDetail.Rows.Count > 0 Then
                'DirectCast(ViewState("gvPayPlanDetail"), DataTable).Merge(dtPayPlan)
                For Each row As DataRow In dtPayPlan.Rows
                    Dim dr As DataRow = DirectCast(ViewState("gvPayPlanDetail"), DataTable).NewRow
                    dr("ID") = row("ID")
                    dr("CLT_ID") = row("CLT_ID")
                    dr("PAYMODE") = row("PAYMODE")
                    dr("PAY_DATE") = row("PAY_DATE")
                    dr("AMOUNT") = row("AMOUNT")
                    dr("CHQNO") = row("CHQNO")
                    dr("CHQDT") = row("CHQDT")
                    dr("BANK_ID") = row("BANK_ID")
                    dr("CRR_ID") = row("CRR_ID")
                    dr("BANK") = row("BANK")
                    dr("COMMENTS") = row("COMMENTS")
                    DirectCast(ViewState("gvPayPlanDetail"), DataTable).Rows.Add(dr)
                    DirectCast(ViewState("gvPayPlanDetail"), DataTable).AcceptChanges()
                Next
                BindGridPP()
            End If
        End If
    End Sub
    Private Sub BindGridPP()
        Try
            Me.gvPayPlanDetail.DataSource = ViewState("gvPayPlanDetail")
            Me.gvPayPlanDetail.DataBind()
        Catch Ex As Exception
        End Try
    End Sub
    Private Function ValidateAdd() As Boolean
        ValidateAdd = True
        'Me.lblError.Text = ""
        If rblPaymodes.SelectedIndex <> -1 AndAlso Me.gvStudentDetail.Rows.Count > 0 Then
            If Convert.ToDouble(Val(Me.txtPayAmt.Text)) = 0 Then
                'Me.lblError.Text = "Enter the amount"
                usrMessageBar.ShowMessage("Enter the amount", True)
                ValidateAdd = False
                Exit Function
            End If
            If Me.txtPayDT.Text.Trim = "" Or Convert.ToDateTime(Me.txtPayDT.Text.Trim) < Convert.ToDateTime(Format(Date.Today, OASISConstants.DateFormat)) Then
                'Me.lblError.Text = "Empty date or Previous date is not allowed"
                usrMessageBar.ShowMessage("Empty date or Previous date is not allowed", True)
                ValidateAdd = False
                Exit Function
            End If
            If rblPaymodes.SelectedValue = "2" Then
                'If Me.txtChqNo.Text.Trim = "" Then
                '    'Me.lblError.Text = "Enter cheque no"
                '    usrMessageBar.ShowMessage("Enter cheque no", True)
                '    ValidateAdd = False
                'End If
                If Me.txtChqDT.Text.Trim = "" Then
                    'Me.lblError.Text = "Enter cheque date"
                    usrMessageBar.ShowMessage("Enter cheque date", True)
                    ValidateAdd = False
                End If
                'If Me.txtBank.Text.Trim = "" Or hfBank.Value = "" Then
                '    'Me.lblError.Text = "Select a bank for cheque"
                '    usrMessageBar.ShowMessage("Select a bank for cheque", True)
                '    ValidateAdd = False
                'End If
            End If
            If rblPaymodes.SelectedValue = "3" Then
                If Me.txtCardNo.Text.Trim = "" Then
                    'Me.lblError.Text = "Enter card no"
                    usrMessageBar.ShowMessage("Enter card no", True)
                    ValidateAdd = False
                End If

            End If
        Else
            ValidateAdd = False
        End If
    End Function

    Protected Sub gvPayPlanDetail_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvPayPlanDetail.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Try
                Dim gridtotal As String = Val(DirectCast(ViewState("gvPayPlanDetail"), DataTable).Compute("Sum(AMOUNT)", ""))
                Dim lblBalanceDue As Label = DirectCast(e.Row.FindControl("lblBalanceDue"), Label)
                lblBalanceDue.Text = (GrandTotalDue - Val(gridtotal)).ToString("#,##0.00")
                Me.txtPayAmt.Text = lblBalanceDue.Text
                lblbalance.Text = "Balance : " + lblBalanceDue.Text
            Catch ex As Exception
                'Me.lblError.Text = ex.Message
                usrMessageBar.ShowMessage(ex.Message, True)
            End Try

        End If
    End Sub

    Protected Sub gvPayPlanDetail_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvPayPlanDetail.RowDeleting
        DirectCast(ViewState("gvPayPlanDetail"), DataTable).Rows(e.RowIndex).Delete()
        DirectCast(ViewState("gvPayPlanDetail"), DataTable).AcceptChanges()
        BindGridPP()
        Try
            Dim gridtotal As String = Val(DirectCast(ViewState("gvPayPlanDetail"), DataTable).Compute("Sum(AMOUNT)", "")).ToString("#,##0.00")
            Me.txtPayAmt.Text = (TotalDue - Val(gridtotal)).ToString("#,##0.00")
            lblbalance.Text = "Balance : " + (TotalDue - Val(gridtotal)).ToString("#,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Function GEtFeeIDs() As String
        GEtFeeIDs = ""
        For Each dr As DataRow In DirectCast(ViewState("gvStudentDetail"), DataTable).Rows
            GEtFeeIDs &= "|" & dr("FEE_ID")
        Next
    End Function
    Private Function GEtAgingXMLdata() As String
        Dim view As New System.Data.DataView(ViewState("gvStudentDetail"))
        Dim selected As System.Data.DataTable = view.ToTable(False, "STU_ID", "FEE_ID", "FEE_DESCR", "BKT1", "BKT2", "BKT3", "BKT4", "BKT5", "TOTALDUE")
        Dim sw As New StringWriter
        selected.WriteXml(sw)
        GEtAgingXMLdata = sw.ToString
    End Function

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If Me.gvPayPlanDetail.Rows.Count > 0 Then
            Dim objPP As New FeePaymentPlanner

            If FPP_ID <= 0 Then
                If Not objPP.bNEW_APPROVAL_PLAN(uscStudentPicker.STU_ID) Then
                    usrMessageBar.ShowMessage("Pending request exists for this student", True)
                    Exit Sub
                End If
            End If
            objPP.BSU_ID = Session("sBsuid")
            objPP.ACD_ID = ddlAcademicYear.SelectedValue
            objPP.DOCDATE = Me.txtDT.Text
            'objPP.STU_ID = UsrSelStudent.STUDENT_ID
            objPP.STU_ID = uscStudentPicker.STU_ID
            objPP.STU_TYPE = "S"
            objPP.User = Session("sUsr_name")
            objPP.Aging_XML = GEtAgingXMLdata()
            objPP.FEE_IDs = GEtFeeIDs()
            objPP.FPP_ID = FPP_ID
            objPP.Comments = txt_cashier_cmnts.Text
            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try
                If objPP.SAVE_FEE_PAYMENT_PLAN_H(sqlConn, sqlTrans) = 0 Then
                    RetFlag = True
                    For Each gvr As GridViewRow In Me.gvPayPlanDetail.Rows
                        objPP.FPD_CLT_ID = Me.gvPayPlanDetail.DataKeys(gvr.RowIndex)("CLT_ID")
                        objPP.FPD_BANK_ID = Me.gvPayPlanDetail.DataKeys(gvr.RowIndex)("BANK_ID")
                        Dim lblPayAmt As Label = DirectCast(gvr.FindControl("lblPayAmt"), Label)
                        objPP.FPD_AMOUNT = IIf(lblPayAmt Is Nothing Or IsNumeric(lblPayAmt.Text.Trim) = False, "0", Val(lblPayAmt.Text))
                        objPP.FPD_PAYMENT_DATE = IIf(IsDate(gvr.Cells(3).Text) = False, "", gvr.Cells(3).Text.Trim) 'gvr.Cells(2)
                        objPP.FPD_CHQNO = Replace(gvr.Cells(4).Text.Trim, "&nbsp;", "") 'gvr.Cells(3)
                        objPP.FPD_CHQDATE = IIf(IsDate(gvr.Cells(5).Text) = False, "", gvr.Cells(5).Text.Trim) 'gvr.Cells(4)
                        objPP.FPD_COMMENTS = gvr.Cells(7).Text.Trim 'gvr.Cells(6)
                        If objPP.SAVE_FEE_PAYMENT_PLAN_D(sqlConn, sqlTrans) <> 0 Then
                            RetFlag = False
                            Exit For
                        End If
                    Next
                Else
                    RetFlag = False
                End If

                If RetFlag = True Then
                    sqlTrans.Commit()
                    clearAll()
                    ViewState("datamode") = "add"
                    'Me.lblError.Text = "Data Saved Successfully"
                    Me.btnSave.Visible = False

                    usrMessageBar.ShowMessage("Data Saved Successfully", False)
                Else
                    sqlTrans.Rollback()
                    'Me.lblError.Text = "Unable to save data"
                    usrMessageBar.ShowMessage("Unable to save data", True)
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "Unable to save data"
                usrMessageBar.ShowMessage("Unable to save data", True)
            Finally
                If sqlConn.State = ConnectionState.Open Then
                    sqlConn.Close()
                End If
            End Try


        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Then
            clearAll()
            ViewState("datamode") = "clear"
        ElseIf ViewState("datamode") = "edit" Then
            LoadSavedData(FPP_ID)
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        DisableorEnableControls(True)
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If FPP_ID <> 0 Then
            Dim objFPP As New FeePaymentPlanner
            objFPP.FPP_ID = FPP_ID
            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try
                If objFPP.DELETE_FEE_PAYMENT_PLAN(sqlConn, sqlTrans) Then
                    sqlTrans.Commit()
                    clearAll(False)
                    'Me.lblError.Text = "Payment plan has been deleted successfully"
                    usrMessageBar.ShowMessage("Payment plan has been deleted successfully", False)
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to delete the payment plan, " & ex.Message
                usrMessageBar.ShowMessage("unable to delete the payment plan, " & ex.Message, True)
            Finally
                If sqlConn.State = ConnectionState.Open Then
                    sqlConn.Close()
                End If
            End Try
        End If
    End Sub

    'Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
    '    Dim objFPP As New FeePaymentPlanner
    '    If FPP_ID <> 0 Then
    '        objFPP.FPP_ID = FPP_ID
    '        objFPP.User = Session("sUsr_name")
    '        Session("ReportSource") = objFPP.PrintPayPlan
    '        h_print.Value = "print"
    '        ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "myJS", "CheckForPrint();", True)
    '    End If
    'End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim objFPP As New FeePaymentPlanner
        If FPP_ID <> 0 Then

            objFPP.FPP_ID = FPP_ID
            objFPP.User = Session("sUsr_name")

            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                If ViewState("MainMnu_code") = "F300162" Then
                    objFPP.Comments = txt_cashier_cmnts.Text
                    RetFlag = objFPP.SUBMIT_FEE_PAYMENT_PLAN(sqlConn, sqlTrans)

                    RetFlag = True

                    For Each gvr As GridViewRow In Me.gvPayPlanDetail.Rows
                        Dim lblId As Label = DirectCast(gvr.FindControl("lblId"), Label)
                        objFPP.FPD_ID = IIf(lblId Is Nothing Or IsNumeric(lblId.Text.Trim) = False, "0", Val(lblId.Text))
                        objFPP.FPD_CLT_ID = Me.gvPayPlanDetail.DataKeys(gvr.RowIndex)("CLT_ID")
                        objFPP.FPD_BANK_ID = Me.gvPayPlanDetail.DataKeys(gvr.RowIndex)("BANK_ID")
                        Dim lblPayAmt As Label = DirectCast(gvr.FindControl("lblPayAmt"), Label)
                        objFPP.FPD_AMOUNT = IIf(lblPayAmt Is Nothing Or IsNumeric(lblPayAmt.Text.Trim) = False, "0", Val(lblPayAmt.Text))
                        objFPP.FPD_PAYMENT_DATE = IIf(IsDate(gvr.Cells(3).Text) = False, "", gvr.Cells(3).Text.Trim) 'gvr.Cells(2)
                        objFPP.FPD_CHQNO = Replace(gvr.Cells(4).Text.Trim, "&nbsp;", "") 'gvr.Cells(3)
                        objFPP.FPD_CHQDATE = IIf(IsDate(gvr.Cells(5).Text) = False, "", gvr.Cells(5).Text.Trim) 'gvr.Cells(4)
                        objFPP.FPD_COMMENTS = gvr.Cells(7).Text.Trim 'gvr.Cells(6)
                        If objFPP.UPDATE_FEE_PAYMENT_PLAN_D(sqlConn, sqlTrans) = False Then
                            RetFlag = False
                            Exit For
                        End If
                    Next

                End If

                If RetFlag Then
                    sqlTrans.Commit()
                    clearAll()
                    ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                    usrMessageBar.ShowMessage("Fee Payment plan has been submitted successfully", False)
                Else
                    sqlTrans.Rollback()
                    'Me.lblError.Text = "unable to approve fee payment plan"
                    usrMessageBar.ShowMessage("unable to submit fee payment plan", True)
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                usrMessageBar.ShowMessage("unable to submit fee payment plan", True)
            End Try
        End If

    End Sub
    Protected Sub btnOnhold_Click(sender As Object, e As EventArgs) Handles btnOnhold.Click
        Dim objFPP As New FeePaymentPlanner
        If FPP_ID <> 0 Then
            objFPP.FPP_ID = FPP_ID
            objFPP.User = Session("sUsr_name")

            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                If ViewState("MainMnu_code") = "F300171" Then
                    objFPP.Comments = txt_fin_comments.Text
                    RetFlag = objFPP.ONHOLD_FEE_PAYMENT_PLAN(sqlConn, sqlTrans)
                ElseIf ViewState("MainMnu_code") = "F300271" Then
                    objFPP.Comments = txt_princ_comments.Text
                    'RetFlag = objFPP.ONHOLD_FEE_PAYMENT_PLAN_LEVEL2(sqlConn, sqlTrans)
                    RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(4, sqlConn, sqlTrans)
                End If


                If RetFlag Then
                    sqlTrans.Commit()
                    clearAll()
                    ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                    usrMessageBar.ShowMessage("Fee Payment plan has been onhold successfully", False)
                Else
                    sqlTrans.Rollback()
                    'Me.lblError.Text = "unable to approve fee payment plan"
                    usrMessageBar.ShowMessage("unable to onhold fee payment plan", True)
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                usrMessageBar.ShowMessage("unable to onhold fee payment plan", True)
            End Try
        End If
    End Sub
    Protected Sub btnRevert_Click(sender As Object, e As EventArgs) Handles btnRevert.Click
        Dim objFPP As New FeePaymentPlanner
        If FPP_ID <> 0 Then
            objFPP.FPP_ID = FPP_ID
            objFPP.User = Session("sUsr_name")

            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                If ViewState("MainMnu_code") = "F300171" Then
                    objFPP.Comments = txt_fin_comments.Text
                    RetFlag = objFPP.REVERT_FEE_PAYMENT_PLAN(sqlConn, sqlTrans)
                ElseIf ViewState("MainMnu_code") = "F300271" Then
                    objFPP.Comments = txt_princ_comments.Text
                    'RetFlag = objFPP.REVERT_FEE_PAYMENT_PLAN_LEVEL2(sqlConn, sqlTrans)
                    RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(3, sqlConn, sqlTrans)
                End If


                If RetFlag Then
                    sqlTrans.Commit()
                    clearAll()
                    ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                    usrMessageBar.ShowMessage("Fee Payment plan has been revert successfully", False)
                Else
                    sqlTrans.Rollback()
                    'Me.lblError.Text = "unable to approve fee payment plan"
                    usrMessageBar.ShowMessage("unable to revert fee payment plan", True)
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                usrMessageBar.ShowMessage("unable to revert fee payment plan", True)
            End Try
        End If
    End Sub
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Dim objFPP As New FeePaymentPlanner
        If FPP_ID <> 0 Then
            objFPP.FPP_ID = FPP_ID
            objFPP.User = Session("sUsr_name")
            objFPP.BSU_ID = Session("sBsuid")

            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                If ViewState("MainMnu_code") = "F300171" Then
                    objFPP.Comments = txt_fin_comments.Text
                    RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN(sqlConn, sqlTrans)
                ElseIf ViewState("MainMnu_code") = "F300271" Then
                    objFPP.Comments = txt_princ_comments.Text
                    'RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_LEVEL2(sqlConn, sqlTrans)
                    RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(1, sqlConn, sqlTrans)
                End If


                If RetFlag Then
                    sqlTrans.Commit()
                    clearAll()
                    ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                    usrMessageBar.ShowMessage("Fee Payment plan has been approved successfully", False)
                Else
                    sqlTrans.Rollback()
                    'Me.lblError.Text = "unable to approve fee payment plan"
                    usrMessageBar.ShowMessage("unable to approve fee payment plan", True)
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                usrMessageBar.ShowMessage("unable to approve fee payment plan", True)
            End Try
        End If
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Dim objFPP As New FeePaymentPlanner
        If FPP_ID <> 0 Then
            objFPP.FPP_ID = FPP_ID
            objFPP.User = Session("sUsr_name")
            objFPP.BSU_ID = Session("sBsuid")

            Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            sqlConn.Open()
            Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction
            Dim RetFlag As Boolean = False
            Try

                If ViewState("MainMnu_code") = "F300171" Then
                    objFPP.Comments = txt_fin_comments.Text
                    RetFlag = objFPP.REJECT_FEE_PAYMENT_PLAN(sqlConn, sqlTrans)
                ElseIf ViewState("MainMnu_code") = "F300271" Then
                    objFPP.Comments = txt_princ_comments.Text
                    'RetFlag = objFPP.REJECT_FEE_PAYMENT_PLAN_LEVEL2(sqlConn, sqlTrans)
                    RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(2, sqlConn, sqlTrans)
                End If

                If RetFlag Then
                    sqlTrans.Commit()
                    clearAll()
                    'Me.lblError.Text = "Fee Payment plan has been rejected"
                    usrMessageBar.ShowMessage("Fee Payment plan has been rejected", False)
                Else
                    sqlTrans.Rollback()
                    ' Me.lblError.Text = "unable to reject fee payment plan"
                    usrMessageBar.ShowMessage("unable to reject fee payment plan", True)
                End If
            Catch ex As Exception
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to reject fee payment plan"
                usrMessageBar.ShowMessage("unable to reject fee payment plan", True)
            End Try
        End If
    End Sub



End Class
