﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeTransportSetupPaymentMandate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                ClientScript.RegisterStartupScript(Me.GetType(), _
                         "script", "<script language='javascript'>  CheckForPrint(); </script>")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                ViewState("datamode") = "add"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                ddlBusinessunit.DataBind()
                Session("sval") = ""
                BindBank()

                txtFrom.Text = Today.Date.ToString("dd/MMM/yyyy")

                If Request.QueryString("Rid") <> "" Then
                    Editcbind()
                End If

                'If USR_NAME = "" Or CurBsUnit = "" Or _
                '(ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_COLLECTION_NEW And _
                'ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_COLLECTION) Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else
                '        Response.Redirect("~\noAccess.aspx")
                '    End If
                'Else
                '    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                'End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try

        End If
        bindedit()
    End Sub

    Public Sub Editcbind()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim query As String = "select * from FEES.STU_PAYMENT_MANDATE WHERE SPM_ID='" & Request.QueryString("Rid") & "' "
        Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        If DS.Tables(0).Rows.Count > 0 Then
            ddlBusinessunit.SelectedValue = DS.Tables(0).Rows(0).Item("SPM_STU_BSU_ID").ToString()
            h_Student_no.Value = DS.Tables(0).Rows(0).Item("SPM_STU_ID").ToString()
            txtBankCode.Text = DS.Tables(0).Rows(0).Item("SPM_BANK_ACT_ID").ToString()
            txtpayeename.Text = DS.Tables(0).Rows(0).Item("SPM_PAYEE_NAME").ToString()
            ddbank.SelectedValue = DS.Tables(0).Rows(0).Item("SPM_BANK").ToString()
            DropDownList1.SelectedValue = DS.Tables(0).Rows(0).Item("SPM_BRANCH").ToString() '' branch
            txtAccount.Text = DS.Tables(0).Rows(0).Item("SPM_ACCOUNT").ToString()
            txtAmmount.Text = DS.Tables(0).Rows(0).Item("SPM_AMOUNT").ToString()
            txtFrom.Text = Convert.ToDateTime(DS.Tables(0).Rows(0).Item("SPM_STARTDT").ToString()).ToString("dd/MMM/yyyy")
            txtremarks.Text = DS.Tables(0).Rows(0).Item("SPM_REMARKS").ToString()
            txtemail.Text = DS.Tables(0).Rows(0).Item("SPM_EMAIL").ToString()

            query = "SELECT STU_NO ,UPPER( ISNULL(STU_FIRSTNAME,'') + ' ' +  ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') ) NAME  FROM dbo.STUDENT_M WHERE STU_ID='" & h_Student_no.Value & "'"
            DS.Tables.Clear()
            DS = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

            If DS.Tables(0).Rows.Count > 0 Then
                txtStdNo.Text = DS.Tables(0).Rows(0).Item("STU_NO").ToString()
                txtStudentname.Text = DS.Tables(0).Rows(0).Item("NAME").ToString()
            End If

            query = " SELECT  AM.ACT_NAME as Description,AM.ACT_ID as Code  FROM OASISFIN.dbo.ACCOUNTS_M AM WHERE AM.ACT_BSU_ID Like   '%" & Session("sBsuid") & "%'  AND ACT_Bctrlac='False' AND ACT_ID='" & txtBankCode.Text & "' "
            txtBankDescr.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)
        End If
    End Sub

    Public Sub bindedit()

        If Request.QueryString("Rid") <> "" Or h_rid.Value <> "" Then
            h_rid.Value = Request.QueryString("Rid")
            btnSave.Visible = False
            Editdiv.Visible = True
        Else
            Editdiv.Visible = False
            btnSave.Visible = True
        End If

    End Sub

    Public Sub BindBank()
        Dim query As String = " select BNK_ID,BNK_DESCRIPTION from oasis..bank_M where BANK_ONLINE is not null "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, query)
        ddbank.DataSource = ds
        ddbank.DataTextField = "BNK_DESCRIPTION"
        ddbank.DataValueField = "BNK_ID"
        ddbank.DataBind()
        Dim list As New ListItem
        list.Text = "Select Bank"
        list.Value = "-1"
        ddbank.Items.Insert(0, list)
    End Sub

    Public Sub BindGrid()
        Dim query As String = " select SPM_BANK_ACT_ID,ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') AS STU_NAME, " & _
                    " SPM_PAYEE_NAME,BNK_DESCRIPTION,EMR_DESCR,SPM_ACCOUNT,SPM_AMOUNT,SPM_STARTDT,SPM_LOGDT " & _
                    " from FEES.STU_PAYMENT_MANDATE A " & _
                    " INNER JOIN OASIS.dbo.BUSINESSUNIT_M B ON B.BSU_ID=A.SPM_STU_BSU_ID " & _
                    " INNER JOIN dbo.STUDENT_M C ON A.SPM_STU_ID=C.STU_ID " & _
                    " INNER JOIN OASIS.dbo.BANK_M D ON A.SPM_BANK=D.BNK_ID " & _
                    " INNER JOIN OASIS.dbo.EMIRATE_M  E ON A.SPM_BRANCH=E.EMR_CODE " & _
                    " WHERE SPM_ID in (" & Session("sval") & ")" & _
                    " ORDER BY SPM_ID DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, query)
        Griddata.DataSource = ds
        Griddata.DataBind()
    End Sub

    Sub clear_All()
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        ClearStudentData()
    End Sub

    Function check_errors_details() As String
        Dim str_error As String = ""

        If txtBankCode.Text = "" Then
            str_error = str_error & "Please select Bank A/C <br />"
        End If

        If h_Student_no.Value = "" Then
            str_error = str_error & "Please select student <br />"
        End If

        If txtpayeename.Text = "" Then
            str_error = str_error & "Please enter Payee Name <br />"
        End If

        'If h_Bank2.Value = "" Then
        '    str_error = str_error & "Please select Payee Bank <br />"
        'End If

        If ddbank.SelectedIndex = 0 Then
            str_error = str_error & "Please select Payee Bank <br />"
        End If

        If txtpayeename.Text = "" Then
            str_error = str_error & "Please enter Payee Name <br />"
        End If

        If txtAccount.Text = "" Then
            str_error = str_error & "Please enter Account <br />"
        End If

        If txtAmmount.Text = "" Then
            str_error = str_error & "Please enter Amount <br />"
        End If

        If Not IsNumeric(txtAmmount.Text) Then
            str_error = str_error & "Please enter correct Amount <br />"
        End If

        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If

        If txtemail.Text = "" Then
            str_error = str_error & "Please enter emailid  <br />"
        End If
        If isEmail(txtemail.Text) = False Then
            str_error = str_error & "Please enter valid emailid  <br />"
        End If

        Return str_error
    End Function

    Public Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            Dim pattern As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" '"(.+@.+\.[a-z]+)"
            Dim expression As Regex = New Regex(pattern)
            Return expression.IsMatch(inputEmail)
        End If
    End Function

    Public Function CheckDuplicates() As Boolean
        Dim rval As Boolean = False
        Dim query As String = " select count(*) from FEES.STU_PAYMENT_MANDATE " & _
                    " WHERE SPM_PAYEE_NAME='" & txtpayeename.Text.Trim() & "' AND SPM_AMOUNT='" & txtAmmount.Text.Trim() & "' AND SPM_EMAIL='" & txtemail.Text.Trim() & "' " & _
                    " AND SPM_STU_ID='" & h_Student_no.Value & "' AND REPLACE(CONVERT(VARCHAR(11), SPM_STARTDT , 106), ' ', '-') ='" & txtFrom.Text.Trim() & "' "

        Dim count = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, query)

        If count > 0 Then
            rval = True
            'lblError.Text = "The record has already been entered."
            usrMessageBar.ShowNotification("The record has already been entered.", UserControls_usrMessageBar.WarningType.Information)
        End If

        Return rval
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'lblError.Text = ""

        Dim rval = ""
        Dim val = check_errors_details()
        If val = "" Then
            If CheckDuplicates() = False Then
                Dim pParms(17) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@SPM_BSU_ID", Session("sbsuid"))
                pParms(1) = New SqlClient.SqlParameter("@SPM_STU_BSU_ID", ddlBusinessunit.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@SPM_BANK_ACT_ID", txtBankCode.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@SPM_STU_ID", h_Student_no.Value)
                pParms(4) = New SqlClient.SqlParameter("@SPM_PAYEE_NAME", txtpayeename.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@SPM_BANK", ddbank.SelectedValue) 'h_Bank2.Value()
                pParms(6) = New SqlClient.SqlParameter("@SPM_BRANCH", DropDownList1.SelectedValue)
                pParms(7) = New SqlClient.SqlParameter("@SPM_ACCOUNT", txtAccount.Text())
                pParms(8) = New SqlClient.SqlParameter("@SPM_AMOUNT", txtAmmount.Text())
                pParms(9) = New SqlClient.SqlParameter("@SPM_STARTDT", txtFrom.Text())
                pParms(10) = New SqlClient.SqlParameter("@SPM_ADDEDBY", Session("EmployeeId"))
                pParms(11) = New SqlClient.SqlParameter("@SPM_REMARKS", txtremarks.Text())
                pParms(12) = New SqlClient.SqlParameter("@SPM_EMAIL", txtemail.Text())
                pParms(13) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
                pParms(14) = New SqlClient.SqlParameter("@MENU_ID", ViewState("MainMnu_code"))
                pParms(15) = New SqlClient.SqlParameter("@OPTION", 1)
                pParms(16) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(16).Direction = ParameterDirection.ReturnValue

                rval = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "INSERT_UPDATE_STU_PAYMENT_MANDATE", pParms)

                If pParms(16).Value = 0 Then
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtStdNo.Text & "_" & txtAccount.Text, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If Session("sval") Is Nothing Then
                        Session("sval") = rval.Split("_")(0).ToString()
                    Else
                        If Session("sval").ToString() = "" Then
                            Session("sval") = rval.Split("_")(0).ToString()
                        Else
                            Session("sval") &= "," & rval.Split("_")(0).ToString()
                        End If
                    End If
                    txtStdNo.Text = ""
                    txtStudentname.Text = ""
                    txtpayeename.Text = ""
                    txtAccount.Text = ""
                    txtAmmount.Text = ""
                    txtFrom.Text = ""
                    txtremarks.Text = ""
                    txtemail.Text = ""
                    BindGrid()
                End If
                'lblError.Text = getErrorMessage(pParms(16).Value)
                usrMessageBar.ShowNotification(getErrorMessage(pParms(16).Value), UserControls_usrMessageBar.WarningType.Information)
            End If
        Else
            'lblError.Text = val
            usrMessageBar.ShowNotification(val, UserControls_usrMessageBar.WarningType.Information)
        End If
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
        '    clear_All()
        '    ViewState("datamode") = "view"

        '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'Else
        '    Response.Redirect(ViewState("ReferrerUrl"))
        'End If
        redirect()
    End Sub


    Public Sub redirect()
        Response.Redirect("feeTransportSetupPaymentMandateView.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Request.QueryString("datamode"))
    End Sub

    Sub ClearStudentData()
        H_Location.Value = ""
        h_Student_no.Value = ""
        txtStdNo.Text = ""
        txtStudentname.Text = ""
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        clear_All()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clear_All()
    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        'lblError.Text = ""

        Dim query As String = " SELECT STU_ID,STU_NAME,STU_NO FROM VW_OSO_STUDENT_M  " & _
                    " WHERE STU_bActive=1  AND STU_BSU_ID='" & ddlBusinessunit.SelectedValue & "' AND STU_NO ='" & txtStdNo.Text & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, query)
        If ds.Tables(0).Rows.Count > 0 Then
            txtStudentname.Text = ds.Tables(0).Rows(0).Item("STU_NAME").ToString()
            h_Student_no.Value = ds.Tables(0).Rows(0).Item("STU_ID").ToString()
            btnSave.Visible = True
        Else
            'Dim stuname = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)
            'lblError.Text = "Student not found for entered student number. please enter correct number"
            usrMessageBar.ShowNotification("Student not found for entered student number. please enter correct number", UserControls_usrMessageBar.WarningType.Information)
            btnSave.Visible = False
        End If
    End Sub


    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Dim pParms(17) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SPM_BSU_ID", Session("sbsuid"))
        pParms(1) = New SqlClient.SqlParameter("@SPM_STU_BSU_ID", ddlBusinessunit.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@SPM_BANK_ACT_ID", txtBankCode.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@SPM_STU_ID", h_Student_no.Value)
        pParms(4) = New SqlClient.SqlParameter("@SPM_PAYEE_NAME", txtpayeename.Text.Trim())
        pParms(5) = New SqlClient.SqlParameter("@SPM_BANK", ddbank.SelectedValue) 'h_Bank2.Value()
        pParms(6) = New SqlClient.SqlParameter("@SPM_BRANCH", DropDownList1.SelectedValue)
        pParms(7) = New SqlClient.SqlParameter("@SPM_ACCOUNT", txtAccount.Text())
        pParms(8) = New SqlClient.SqlParameter("@SPM_AMOUNT", txtAmmount.Text())
        pParms(9) = New SqlClient.SqlParameter("@SPM_STARTDT", txtFrom.Text())
        pParms(10) = New SqlClient.SqlParameter("@SPM_ADDEDBY", Session("EmployeeId"))
        pParms(11) = New SqlClient.SqlParameter("@SPM_REMARKS", txtremarks.Text())
        pParms(12) = New SqlClient.SqlParameter("@SPM_EMAIL", txtemail.Text())
        pParms(13) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
        pParms(14) = New SqlClient.SqlParameter("@MENU_ID", ViewState("MainMnu_code"))
        pParms(15) = New SqlClient.SqlParameter("@OPTION", 2)
        pParms(16) = New SqlClient.SqlParameter("@SPM_ID", h_rid.Value)
        'lblError.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "INSERT_UPDATE_STU_PAYMENT_MANDATE", pParms)
        usrMessageBar.ShowNotification(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "INSERT_UPDATE_STU_PAYMENT_MANDATE", pParms), UserControls_usrMessageBar.WarningType.Danger)
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_rid.Value, "Update", Page.User.Identity.Name.ToString, Me.Page)
    End Sub

    Protected Sub btndisc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndisc.Click
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SPM_ID", h_rid.Value)
        pParms(1) = New SqlClient.SqlParameter("@SPM_BSU_ID", Session("sbsuid"))
        pParms(2) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
        pParms(3) = New SqlClient.SqlParameter("@MENU_ID", h_Student_no.Value)
        pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
        'lblError.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRAN_STU_PAYMENT_MANDATE_EDIT", pParms)
        usrMessageBar.ShowNotification(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRAN_STU_PAYMENT_MANDATE_EDIT", pParms), UserControls_usrMessageBar.WarningType.Danger)
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_rid.Value, "Discontinue", Page.User.Identity.Name.ToString, Me.Page)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SPM_ID", h_rid.Value)
        pParms(1) = New SqlClient.SqlParameter("@SPM_BSU_ID", Session("sbsuid"))
        pParms(2) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
        pParms(3) = New SqlClient.SqlParameter("@MENU_ID", ViewState("MainMnu_code"))
        pParms(4) = New SqlClient.SqlParameter("@OPTION", 2)
        'lblError.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRAN_STU_PAYMENT_MANDATE_EDIT", pParms)
        usrMessageBar.ShowNotification(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRAN_STU_PAYMENT_MANDATE_EDIT", pParms), UserControls_usrMessageBar.WarningType.Information)
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, h_rid.Value, "Delete", Page.User.Identity.Name.ToString, Me.Page)
    End Sub

End Class
