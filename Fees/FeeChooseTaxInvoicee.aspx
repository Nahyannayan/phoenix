﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeeChooseTaxInvoicee.aspx.vb" Inherits="Fees_FeeChooseTaxInvoicee" %>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="../cssfiles/SiteStyle.css" rel="stylesheet" />
    <style type="text/css">
        .InfoLabel {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            
            font-size: 11px;
            font-weight: normal;
            color: #2e2e2e;
            letter-spacing: 1px;
            font-weight: 600;
            background-position: 5px center;
           
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
            text-align: justify;
        }

        .errLabel {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            color: #2e2e2e;
            letter-spacing: 1px;
            font-weight: 600;
            background-position: 5px center;
            background: #ffc7c7 url(../images/Common/PageBody/error_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
        }

        .validLabel {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            color: #2e2e2e;
            letter-spacing: 1px;
            font-weight: 600;
            background-position: 5px center;
            background: #d1ff83 url(../images/Common/PageBody/success_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
        }
    </style>--%>

</head>
<body>
    <form id="form1" runat="server">
        <table width="100%">
            <tr>
                <td class="title-bg">
                    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td colspan="4" style="text-align: center">
                    <asp:HiddenField ID="h_print" runat="server" />
                    <asp:HiddenField ID="hfINVTYPE" runat="server" />
                    <asp:HiddenField ID="hfFSHID" runat="server" />
                    <asp:HiddenField ID="F_COMP_TAX_REGISTRATION_NO" runat="server" />
                    <asp:HiddenField ID="M_COMP_TAX_REGISTRATION_NO" runat="server" />
                    <asp:HiddenField ID="COMP_TAX_REGISTRATION_NO" runat="server" />
                </td>
            </tr>
            <tr>
                <td width="30%"><span class="field-label">Student Id</span></td>

                <td width="70%">
                    <asp:Label ID="lblStuNo" runat="server" CssClass="field-value"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="30%"><span class="field-label">Student Name</span></td>

                <td width="70%">
                    <asp:Label ID="lblStuName" runat="server" CssClass="field-value"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="30%"><span class="field-label">Invoice No</span></td>

                <td width="70%">
                    <asp:Label ID="lblInvNo" runat="server" CssClass="field-value"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="30%"><span class="field-label">Invoice Generated For</span>
                </td>

                <td width="70%">
                    <asp:RadioButtonList ID="rblInvoicee" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="F"><span class="field-label">Father</span></asp:ListItem>
                        <asp:ListItem Value="M"><span class="field-label">Mother</span></asp:ListItem>
                        <asp:ListItem Value="FC"><span class="field-label">Father&#39;s Company</span></asp:ListItem>
                        <asp:ListItem Value="MC"><span class="field-label">Mother&#39;s Company</span></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td width="30%"><span class="field-label">Address</span></td>

                <td width="70%">
                    <%--<asp:Label ID="lbl" runat="server"></asp:Label>--%>
                    <asp:TextBox ID="lblAddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td width="30%"><span class="field-label">Tax Registration No</span>
                </td>
              
                <td width="70%">
                    <%--<asp:Label ID="lblTRNo" runat="server"></asp:Label>--%>
                    <asp:TextBox ID="lblTRNo" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Label ID="lblMessage2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Label ID="lblError" runat="server"></asp:Label></td>
            </tr>
            <tr id="trAnimation1" runat="server" visible="false">
                <td colspan="2" style="text-align: center">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /></td>
            </tr>
            <tr id="trAnimation2" runat="server" visible="false">
                <td colspan="2" style="text-align: center">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnClear"  runat="server" Text="CLEAR" Visible="false"  CssClass="button" />
                    <asp:Button ID="btnSave" runat="server" Text="SAVE" OnClientClick="return confirm('Are you sure you want to continue?');" CssClass="button" />
                    <asp:Button ID="btnCancel" runat="server" Text="CANCEL" OnClientClick="parent.fnParent();" CssClass="button" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
