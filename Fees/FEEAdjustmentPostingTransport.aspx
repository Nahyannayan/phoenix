<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEAdjustmentPostingTransport.aspx.vb" Inherits="FEEAdjustmentPostingTransport" Title="Untitled Page" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Post Fee Adjustments..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <input id="h_SelectedId" runat="server" type="hidden" value="-1" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
                    </tr>

                    <tr>
                        <td align="left" width="10%">
                            <input id="chkSelectall" type="checkbox" onclick="change_chk_state('0')" /><a href="javascript:change_chk_state('1')">Select All</a>
                        </td>
                        <td align="left" width="10%"><span class="field-label">Business Units</span>
                        </td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBSUnit_SelectedIndexChanged" DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="10%"><span class="field-label">Academic Year </span>
                        </td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="True" Checked="True" GroupName="STUD_TYPE"
                                Text="Student" />
                            <asp:RadioButton ID="radEnq" runat="server" AutoPostBack="True"
                                GroupName="STUD_TYPE" Text="Enquiry" /></td>
                    </tr>
                </table>

                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvFeeAdjustmentDet" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="FAH_ID" AllowPaging="True" EmptyDataText="No Data Found" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderStyle Width="1%" Wrap="False"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost" runat="server" __designer:wfdid="w20"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="FSH_ID" Visible="False" HeaderText="FSH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_ID" runat="server" Text='<%# Bind("FAH_ID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_NO">
                                        <HeaderTemplate>
                                            STUD# NO<br />
                                            <asp:TextBox ID="txtSTU_NO" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudNoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemStyle Width="1%" HorizontalAlign="Center"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>' __designer:wfdid="w43"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_NAME">
                                        <HeaderTemplate>
                                            STUD# NAME<br />
                                            <asp:TextBox ID="txtSTU_NAME" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSTU_NAMESearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemStyle Width="2%" HorizontalAlign="Left"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTUD_NAME" runat="server" Text='<%# Bind("STU_NAME") %>' __designer:wfdid="w42"></asp:Label>&nbsp; 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DATE">
                                        <HeaderTemplate>
                                            DATE<br />
                                            <asp:TextBox ID="txtFAH_DATE" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemStyle Width="2%" HorizontalAlign="Left"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_DATE" runat="server" Text='<%# Bind("FAH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACD_YEAR">
                                        <HeaderTemplate>
                                            ACAD. YEAR<br />
                                            <asp:TextBox ID="txtACY_DESCR" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnACYDESCRSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>

                                        <ItemStyle Width="10%" HorizontalAlign="Left"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACY_DESCR" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REMARKS">
                                        <EditItemTemplate>
                                            &nbsp;
                                    
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            REMARKS<br />
                                            <asp:TextBox ID="txtFAH_REMARKS" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>

                                        <ItemStyle Width="10%" HorizontalAlign="Left"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_REMARKS" runat="server" Text='<%# Bind("FAH_REMARKS") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="bInter">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FAH_bInter") %>'></asp:TextBox>

                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblbInter" runat="server" Text='<%# Bind("FAH_bInter") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print After posting" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                       <td align="center" >
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" />
                        </td>
                    </tr>
                </table>

              
                <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover">
                </asp:Panel>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sUsr_name" Type="String" DefaultValue="" Name="USR_ID"></asp:SessionParameter>
                        <asp:SessionParameter SessionField="sBsuid" Type="String" DefaultValue="" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>

            </div>
        </div>
    </div>
</asp:Content>

