<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEAdjustmentPosting.aspx.vb" Inherits="FEEAdjustmentPosting" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Post Fee Adjustments..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <input id="chkSelectall" type="checkbox" onclick="change_chk_state('0')" /><a href="javascript:change_chk_state('1')">Select All</a>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="True" Checked="True" GroupName="STUD_TYPE" Text="Student" CssClass="field-label" />
                            <asp:RadioButton ID="radEnq" runat="server" AutoPostBack="True" GroupName="STUD_TYPE" Text="Enquiry" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeAdjustmentDet" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="FAH_ID" AllowPaging="False" EmptyDataText="No Data Found"
                                PageSize="25" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderStyle Width="1%" Wrap="False"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="FSH_ID" Visible="False" HeaderText="FSH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_ID" runat="server" Text='<%# Bind("FAH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_NO">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtSTU_NO" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudNoSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle Width="1%" HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_NAME">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtSTU_NAME" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSTU_NAMESearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle Width="2%" HorizontalAlign="Left"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewDet" runat="server" Text='<%# Bind("STU_NAME") %>' OnClientClick="return false;"></asp:LinkButton>
                                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" DynamicContextKey='<%# Eval("FAH_ID") %>'
                                                DynamicControlID="Panel1" DynamicServiceMethod="GetDynamicContent" PopupControlID="Panel1"
                                                Position="center" TargetControlID="lnkViewDet">
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DATE">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtFAH_DATE" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle Width="2%" HorizontalAlign="Left"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_DATE" runat="server" Text='<%# Bind("FAH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT">
                                        <HeaderTemplate>
                                            Amount<br />
                                            <asp:TextBox ID="txtACY_DESCR" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnACYDESCRSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REMARKS">
                                        <HeaderTemplate>
                                            Narration<br />
                                            <asp:TextBox ID="txtFAH_REMARKS" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_REMARKS" runat="server" Text='<%# Bind("FAH_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                            <itemstyle horizontalalign="center" width="10%" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="bInter">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbInter" runat="server" Text='<%# Bind("FAH_bInter") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span></td>

                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtNarration" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                        <td align="left">
                            <asp:Button ID="btnNarrationInsert" runat="server" Text="Fill Narration" CssClass="button" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print After posting" CssClass="field-label" />
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" /></td>
                    </tr>
                </table>


                <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover">
                </asp:Panel>
                <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>

