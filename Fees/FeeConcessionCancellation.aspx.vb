﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Web.Services

Partial Class Fees_FeeConcessionCancellation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Public Property FCH_ID() As Integer
        Get
            Return ViewState("FCH_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("FCH_ID") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name").ToString, Session("sBsuid").ToString, ViewState("MainMnu_code"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
            If Session("sUsr_name").ToString = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_CANCEL And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_APPROVAL And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
            InitializeScripts()
            fillVATCodes()
            PopulateControls()
            FCH_ID = 0
            btnPrint.Visible = False
            If ViewState("datamode") = "view" Then
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                FCH_ID = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
                LoadConcessionData(FCH_ID, True)
                btnPrint.Visible = True
            ElseIf ViewState("datamode") = "add" Then
                If Not Request.QueryString("SID") Is Nothing Then
                    h_STUD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("SID").Replace(" ", "+"))
                    txtStdNo.Text = FeeCommon.GetStudentNo(h_STUD_ID.Value, False) & " - " & FeeCommon.GetStudentName(h_STUD_ID.Value, False)
                    h_STUD_ID_ValueChanged(Nothing, Nothing)
                End If
            End If
            lblAlert.InnerHtml = getErrorMessage("642")
        End If
    End Sub
    Sub InitializeScripts()
        txtPercAmountCancel.Attributes.Add("onblur", "return CheckAmount(this);")
        txtPercAmountCancel.Attributes.Add("onfocus", "this.select();")
    End Sub


    Private Sub SET_VAT_DROPDOWN_RIGHT()
        Dim bHasRight As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CASE WHEN COUNT(USER_NAME)>0 THEN 1 ELSE 0 END AS bEXIST FROM OASIS.TAX.VW_VAT_ENABLE_USERS WHERE USER_NAME='" & Session("sUsr_name").ToString & "' AND USER_SOURCE='FEES'"))
        If Not bHasRight Then
            ddlTAX.Enabled = False
        End If
    End Sub
    Private Sub CalculateVAT(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",'')")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlTAX.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE").ToString

        End If

    End Sub
    Private Sub CALCULATE_TAX(ByRef Amount As TextBox, ByRef lblTAX As Label, ByRef lblNet As Label, ByVal IsInclusiveTax As Integer)
        If IsNumeric(Amount.Text) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & Amount.Text & ",'" & ddlTAX.SelectedValue & "'," & IsInclusiveTax & ")")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTAX.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                If IsInclusiveTax Then
                    Amount.Text = (Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT")) - Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT"))).ToString

                End If
                lblNet.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub

    Private Sub CALCULATE_TAX_DBLVALUES(ByRef Amount As Double, ByRef lblTAX As Double, ByRef lblNet As Double, ByVal IsInclusiveTax As Integer)
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & Amount & ",'" & ddlTAX.SelectedValue & "'," & IsInclusiveTax & ")")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTAX = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                If IsInclusiveTax Then
                    Amount = (Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT")) - Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT"))).ToString

                End If
                lblNet = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub

    Protected Sub txtDAmount_TextChanged(sender As Object, e As EventArgs) 'Handles txtAmount.TextChanged
        Dim gvr As GridViewRow = sender.parent.parent
        Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblTotal As Double = 0
        txtDAmount = gvr.FindControl("txtAmount")
        lblTAXAmount = gvr.FindControl("lblTaxAmount")
        lblNETTotal = gvr.FindControl("lblNetAmount")
        CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
        For Each gvro As GridViewRow In gvMonthly.Rows
            Dim txtDAmount2 As TextBox = gvro.FindControl("txtAmount")
            Dim lblNETTotal2 As Label = gvro.FindControl("lblNetAmount")
            dblTotal += CDbl(lblNETTotal2.Text)
        Next

    End Sub

    Protected Sub ddlTAX_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTAX.SelectedIndexChanged
        If gvMonthly.Rows.Count > 0 Then
            Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblGridTotal As Double = 0
            For Each gvr As GridViewRow In gvMonthly.Rows
                txtDAmount = gvr.FindControl("txtAmount")
                lblTAXAmount = gvr.FindControl("lblTaxAmount")
                lblNETTotal = gvr.FindControl("lblNetAmount")
                dblGridTotal += CDbl(txtDAmount.Text)
                CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
            Next

        End If
    End Sub
    Sub fillVATCodes()
        Dim dtACD As DataTable = GetTAXCode()
        ddlTAX.DataSource = dtACD
        ddlTAX.DataTextField = "TAX_DESCR"
        ddlTAX.DataValueField = "TAX_CODE"
        ddlTAX.DataBind()


    End Sub
    Function GetTAXCode() As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim dsData As New DataSet
        dsData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.[GET_TAX_CODES]", pParms)


        'Dim sql_query As String = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
        'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        'CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Protected Sub rblTaxCalculation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblTaxCalculation.SelectedIndexChanged
        'AutoFill()
        If gvMonthly.Rows.Count > 0 Then
            Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblGridTotal As Double = 0
            For Each gvr As GridViewRow In gvMonthly.Rows
                txtDAmount = gvr.FindControl("txtAmount")
                lblTAXAmount = gvr.FindControl("lblTaxAmount")
                lblNETTotal = gvr.FindControl("lblNetAmount")
                dblGridTotal += CDbl(txtDAmount.Text)
                CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
            Next

        End If
    End Sub
    'Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged

    '    CalculateVAT(ddlFeeType.SelectedValue, 0, Now.Date)
    '    ddlTAX_SelectedIndexChanged(Nothing, Nothing)
    'End Sub



    Private Sub PopulateControls()
        BindAcademicYear()
        BindFee()
        SetAcademicyearDate()
    End Sub
    Sub BindAcademicYear()
        ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        ddlAcademicYear.SelectedIndex = -1
        ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
    End Sub
    Sub BindFee()
        ddlFeeType.DataSource = FEEConcessionTransaction.GetConcessionFEEType(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub
    Sub BindConcession(Optional ByVal FCH_ID As Integer = 0)
        ddlConcession.DataSource = clsConcessionCancellation.StudentConcessions(Session("sBsuid"), h_STUD_ID.Value, ddlAcademicYear.SelectedItem.Value, FCH_ID)
        ddlConcession.DataTextField = "FCH_RECNO"
        ddlConcession.DataValueField = "FCH_ID"
        ddlConcession.DataBind()
    End Sub
    Sub SetAcademicyearDate()
        Dim DTFROM As String = ""
        Dim DTTO As String = ""
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, Session("sBsuid"))
        txtFromDT.Text = DTFROM
        txtToDT.Text = DTTO
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
    End Sub
    Sub LoadStudentData()
        If IsNumeric(h_STUD_ID.Value) AndAlso h_STUD_ID.Value > 0 Then
            BindConcession()
            If ddlConcession.Items.Count > 0 AndAlso ddlConcession.SelectedValue > 0 Then
                LoadConcessionData(ddlConcession.SelectedValue)
                EnabelYN(False)
            End If
        End If
    End Sub
    Sub LoadConcessionData(ByVal FCH_ID As Long, Optional ByVal bView As Boolean = False)
        Dim objclsCC As New clsConcessionCancellation
        objclsCC.GetConcesionDetails(Session("sBsuId"), FCH_ID)
        If bView Then
            txtDate.Text = Format(objclsCC.FCH_DT, OASISConstants.DateFormat)
            pnlStudents.Enabled = False
            ddlConcession.Enabled = False
            ddlAcademicYear.Enabled = False
            BindConcession(objclsCC.FCH_FCH_ID)
        Else
            pnlStudents.Enabled = True
            ddlConcession.Enabled = True
            ddlAcademicYear.Enabled = True
            txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        End If

        txtFromDT.Text = Format(objclsCC.FCH_DTFROM, OASISConstants.DateFormat)
        txtToDT.Text = Format(objclsCC.FCH_DTTO, OASISConstants.DateFormat)
        h_Fdate.Value = txtFromDT.Text
        h_Tdate.Value = txtToDT.Text

        'h_FCM_ID_HEAD.Value = vFEE_CON.FCH_FCM_ID
        txtConcession_Head.Text = objclsCC.FCM_DESCR
        h_FCT_ID_HEAD.Value = objclsCC.FCM_FCT_ID
        'h_GRD_ID.Value = objclsCC.STU_GRD_ID
        txtRefHEAD.Text = objclsCC.FCH_REF_NAME
        H_REFID_HEAD.Value = objclsCC.FCH_REF_ID

        txtRefHEAD.Text = objclsCC.REF_NAME
        txtRemarks.Text = objclsCC.FCH_REMARKS
        h_STUD_ID.Value = objclsCC.FCH_STU_ID
        txtStud_Name.Text = objclsCC.STU_NAME
        txtStdNo.Text = objclsCC.STU_NO & " - " & objclsCC.STU_NAME
        ddlAcademicYear.SelectedIndex = -1
        ddlAcademicYear.Items.FindByValue(objclsCC.FCH_ACD_ID).Selected = True
        BindFee()
        Dim dttab As DataTable = clsConcessionCancellation.GET_FEE_CONCESION_D(FCH_ID)
        ViewState("gvFeeDetails") = dttab
        BindgvFeeDetails()
        If objclsCC.FCH_bPOSTED And bView Then
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), 3, "view")
            gvFeeDetails.Columns(5).Visible = False 'Disables editing if already posted
            gvFeeDetails.Columns(6).Visible = False 'Disables delete if already posted
        End If
    End Sub
    Private Sub BindgvFeeDetails()
        If Not ViewState("gvFeeDetails") Is Nothing Then
            Dim dtView As DataView = New DataView(ViewState("gvFeeDetails"))
            dtView.RowFilter = "FCD_bDELETE = False"
            gvFeeDetails.DataSource = dtView.ToTable
        End If
        gvFeeDetails.DataBind()
    End Sub
    Sub EnabelYN(ByVal EnYN As Boolean)
        txtFromDT.Enabled = EnYN
        Calendarextender1.Enabled = EnYN
        Calendarextender2.Enabled = EnYN
        txtToDT.Enabled = EnYN
    End Sub
    'Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
    '    If Message <> "" Then
    '        preError.Attributes.Remove("class")
    '        If bError Then
    '            preError.Attributes.Add("class", "alert alert-error")
    '        Else
    '            preError.Attributes.Add("class", "alert alert-success")
    '        End If
    '    Else
    '        preError.Attributes.Remove("class")
    '        preError.Attributes.Add("class", "invisible")
    '    End If
    '    preError.InnerHtml = Message
    'End Sub
    Private Sub BindgvMonthly()
        If Not ViewState("gvMonthly") Is Nothing Then
            Dim dtView As DataView = New DataView(ViewState("gvMonthly"))
            dtView.RowFilter = "FMD_bDelete = False"
            gvMonthly.DataSource = dtView.ToTable
        End If
        gvMonthly.DataBind()
    End Sub
    Private Function ValidateAndUpdate(Optional ByVal bDateChange As Boolean = False) As Boolean
        ValidateAndUpdate = False
        If ViewState("gvMonthly") Is Nothing Then
            'ShowMessage("Please select the concession for editing..!")
            usrMessageBar.ShowNotification("Please select the concession for editing..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        If DateValue(txtFromDT.Text) < DateValue(h_Fdate.Value) Then
            'ShowMessage("From date should be grater than Concession From Date..!")
            usrMessageBar.ShowNotification("From date should be grater than Concession From Date..!", UserControls_usrMessageBar.WarningType.Danger)
            txtFromDT.Text = h_Fdate.Value
            txtFromDT.Focus()
            Exit Function
        End If
        If DateValue(txtToDT.Text) > DateValue(h_Tdate.Value) Then
            'ShowMessage("To date should be less than Concession To Date..!")
            usrMessageBar.ShowNotification("To date should be less than Concession To Date..!", UserControls_usrMessageBar.WarningType.Danger)
            txtToDT.Text = h_Tdate.Value
            txtToDT.Focus()
            Exit Function
        End If

        txtAmount.Text = FeeCollection.GetDoubleVal(txtAmount.Text)
        Dim DisPrec As Double = Convert.ToDouble(txtAmount.Text)
        Dim CnDisPrec As Double = 0
        If FeeCollection.GetDoubleVal(txtPercAmountCancel.Text) = 0 Then
            txtPercAmountCancel.Text = txtAmount.Text
        End If
        CnDisPrec = FeeCollection.GetDoubleVal(txtPercAmountCancel.Text)
        Dim AMT_TYPE As String = "Percentage"
        If radAmount.Checked Then
            CnDisPrec = CnDisPrec / CDbl(txtTotalFee.Text) * 100
            DisPrec = DisPrec / CDbl(txtTotalFee.Text) * 100
            AMT_TYPE = "Amount"
        End If
        If CnDisPrec > DisPrec Then
            'ShowMessage("Concession Cancel " & AMT_TYPE & " Exceeds Actual " & AMT_TYPE & " ..!")
            usrMessageBar.ShowNotification("Concession Cancel " & AMT_TYPE & " Exceeds Actual " & AMT_TYPE & " ..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        Dim dt As New DataTable, dtTermorMonths As New DataTable
        dt = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
        Dim STR_SCH_ID As Integer
        Dim TOTALAMOUNT As Decimal = 0
        dtTermorMonths = FeeCommon.GetFee_MonthorTerm_OASIS(ddlFeeType.SelectedItem.Value, _
                        ddlAcademicYear.SelectedValue, Session("sBsuId"), h_STUD_ID.Value, _
                        txtFromDT.Text, txtToDT.Text, TOTALAMOUNT, STR_SCH_ID)
        Dim ConsAmount As Double = 0
        Dim CurAmount As Double = 0
        If dtTermorMonths Is Nothing OrElse dtTermorMonths.Rows.Count = 0 Then
            'ShowMessage("Term / Month is not set..!")
            usrMessageBar.ShowNotification("Term / Month is not set..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If

        Dim dtgvMonthly As DataTable = ViewState("gvMonthly")
        If Not dtgvMonthly Is Nothing AndAlso dtgvMonthly.Rows.Count > 0 Then
            If dtgvMonthly.Columns.Contains("FMD_bDelete") Then
                dtgvMonthly.Columns.Remove("FMD_bDelete")
                dtgvMonthly.AcceptChanges()
            End If
            Dim bUpdated As New DataColumn("FMD_bDelete", System.Type.GetType("System.Boolean"))
            bUpdated.DefaultValue = 1
            dtgvMonthly.Columns.Add(bUpdated)

        End If

        For i As Integer = 0 To dtTermorMonths.Rows.Count - 1
            CurAmount = (FeeCollection.GetDoubleVal(dtTermorMonths.Rows(i)("FDD_PROAMOUNT")) * CnDisPrec) / 100
            If dtgvMonthly.Rows.Count > 0 Then
                For Each drr As DataRow In dtgvMonthly.Rows
                    If Month(CDate(drr("FMD_DATE"))) = Month(CDate(dtTermorMonths.Rows(i)("FDD_DATE"))) And Year(CDate(drr("FMD_DATE"))) = Year(CDate(dtTermorMonths.Rows(i)("FDD_DATE"))) Then
                        drr("FMD_ORG_AMOUNT") = drr("FMD_AMOUNT")
                        If bDateChange = False Then
                            drr("FMD_AMOUNT") = Math.Floor(CurAmount)
                        End If
                        drr("FMD_DATE") = Format(CDate(dtTermorMonths.Rows(i)("FDD_DATE")), OASISConstants.DataBaseDateFormat)
                        drr("FMD_bDelete") = False
                    End If
                Next
            End If
        Next
        For Each drr As DataRow In dtgvMonthly.Rows
            If (CBool(drr("FMD_bDelete"))) = True Then
                drr("FMD_AMOUNT") = 0
            Else
                CALCULATE_TAX_DBLVALUES(drr("FMD_AMOUNT"), drr("FMD_TAX_AMOUNT"), drr("FMD_NET_AMOUNT"), IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
            End If
        Next
        ViewState("gvMonthly") = dtgvMonthly
        txtTotalFee.Text = TOTALAMOUNT
        BindgvMonthly()
    End Function
    Public Shared Function CreateDataTableFeeConcessionMonthly() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
        Dim cREF_Id As New DataColumn("REF_ID", System.Type.GetType("System.String"))
        Dim cFDD_DATE As New DataColumn("FDD_DATE", System.Type.GetType("System.DateTime"))
        Dim cdescr As New DataColumn("DESCR", System.Type.GetType("System.String"))
        Dim cFDD_AMOUNT As New DataColumn("FDD_AMOUNT", System.Type.GetType("System.String"))
        Dim cCUR_AMOUNT As New DataColumn("CUR_AMOUNT", System.Type.GetType("System.String"))
        Dim cPRO_AMOUNT As New DataColumn("PRO_AMOUNT", System.Type.GetType("System.String"))
        Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
        dtDt.Columns.Add(cId)
        dtDt.Columns.Add(cREF_Id)
        dtDt.Columns.Add(cFDD_DATE)
        dtDt.Columns.Add(cdescr)
        dtDt.Columns.Add(cFDD_AMOUNT)
        dtDt.Columns.Add(cCUR_AMOUNT)
        dtDt.Columns.Add(cPRO_AMOUNT)
        dtDt.Columns.Add(cStatus)
        Return dtDt
    End Function
    Private Sub ClearSubDetails()
        'txtRefIDsub.Text = ""
        txtAmount.Text = "0"
        txtPercAmountCancel.Text = "0"
        btnDetAdd.Text = "Add"
        gvMonthly.DataBind()
    End Sub
    Private Sub ClearAll(Optional ByVal bCLEARCONCESSION As Boolean = True)
        'txtStdNo.Text = ""
        'txtStud_Name.Text = ""
        'h_STUD_ID.Value = ""
        If bCLEARCONCESSION Then
            ddlConcession.Items.Clear()
        End If
        FCH_ID = 0
        txtFromDT.Text = ""
        txtToDT.Text = ""
        h_Fdate.Value = ""
        h_Tdate.Value = ""
        txtConcession_Head.Text = ""
        h_FCT_ID_HEAD.Value = ""
        txtRefHEAD.Text = ""
        H_REFID_HEAD.Value = ""
        txtRemarks.Text = ""
        txtTotalFee.Text = "0.00"
        txtAmount.Text = "0"
        txtPercAmountCancel.Text = "0"
        ViewState("gvFeeDetails") = Nothing
        BindgvFeeDetails()
        ViewState("gvMonthly") = Nothing
        BindgvMonthly()
        ViewState("FCD_ID") = Nothing
        btnDetAdd.Text = "Add"
        ViewState("MONTHLY_D") = Nothing
        ' ShowMessage("")
    End Sub
    Private Function ValidateSave() As Boolean
        ValidateSave = False
        If Not Master.IsSessionMatchesForSave() Then
            'ShowMessage(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH)
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If

        If ViewState("gvFeeDetails") Is Nothing OrElse DirectCast(ViewState("gvFeeDetails"), DataTable).Rows.Count <= 0 Then
            'ShowMessage("Concession cancellation details not found, Please add the cancellation details and try again.")
            usrMessageBar.ShowNotification("Concession cancellation details not found, Please add the cancellation details and try again.", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        If ViewState("MONTHLY_D") Is Nothing OrElse DirectCast(ViewState("MONTHLY_D"), DataTable).Rows.Count <= 0 Then
            'ShowMessage("Concession cancellation monthly split up details not found, Please edit and the details and try again.")
            usrMessageBar.ShowNotification("Concession cancellation monthly split up details not found, Please edit and the details and try again.", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        If Not FEEConcessionTransaction.PeriodBelongstoAcademicYear(CDate(txtFromDT.Text), CDate(txtToDT.Text), ddlAcademicYear.SelectedValue, Session("sBsuId")) Then
            'ShowMessage("The date period doesnot belongs to current Academic Year")
            usrMessageBar.ShowNotification("The date period doesnot belongs to current Academic Year", UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If

        Dim strErrorMsg As String = String.Empty
        Dim dtDocDate, dtFromDT, dtToDT As DateTime
        dtDocDate = CDate(txtDate.Text)
        dtFromDT = CDate(txtFromDT.Text)
        dtToDT = CDate(txtToDT.Text)



        If btnDetAdd.Text = "Update" Then
            strErrorMsg &= "Please Update Selected Details..!<br />"
        End If
        If DateValue(txtFromDT.Text) < DateValue(h_Fdate.Value) Then
            strErrorMsg &= "Invalid From date..!<br />"
        End If
        If DateValue(txtToDT.Text) > DateValue(h_Tdate.Value) Then
            strErrorMsg &= "Invalid To date..!<br />"
        End If
        If dtToDT < dtFromDT Then
            strErrorMsg &= "From date is greater than to date<br />"
        End If

        If strErrorMsg <> "" Then
            'ShowMessage("Please check the Following : <br>" & strErrorMsg)
            usrMessageBar.ShowNotification("Please check the Following : <br>" & strErrorMsg, UserControls_usrMessageBar.WarningType.Danger)
            Exit Function
        End If
        ValidateSave = True
    End Function
    <WebMethod()> _
    Public Shared Function GetStudent(ByVal BSUID As String, ByVal prefix As String) As String()
        Return clsConcessionCancellation.GetStudent(BSUID, prefix)
    End Function
    Protected Sub imgCompany_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCompany.Click
        LoadStudentData()
    End Sub
    Protected Sub ddlConcession_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlConcession.SelectedIndexChanged
        If ddlConcession.SelectedValue <> Nothing Then
            ClearAll(False)
            LoadConcessionData(ddlConcession.SelectedValue)
        End If
    End Sub
    Protected Sub txtFromDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromDT.TextChanged
        'Set_Concession()
        ValidateAndUpdate(True)
    End Sub

    Protected Sub txtToDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToDT.TextChanged
        'Set_Concession()
        ValidateAndUpdate(True)
    End Sub

    Protected Sub txtChargeDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Set_Concession()
    End Sub
    Protected Sub txtChargeDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtChargeDate As New TextBox
        txtChargeDate = sender
        txtChargeDate.Attributes.Add("ReadOnly", "ReadOnly")
    End Sub
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim gvr As GridViewRow = sender.parent.parent
        If Not gvr Is Nothing Then
            ' ShowMessage("")
            If ViewState("FCD_ID") Is Nothing Then
                ViewState("FCD_ID") = 0
            End If
            Dim FCD_ID As String = gvFeeDetails.DataKeys(gvr.RowIndex)("FCD_ID")

            If ViewState("MONTHLY_D") Is Nothing Then
                Dim dt As DataTable = clsConcessionCancellation.GET_FEE_CONCESSIONMONTHLY_D(FCD_ID) 'gets the data from concession
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    ViewState("gvMonthly") = dt
                End If
            Else
                If DirectCast(ViewState("MONTHLY_D"), DataTable).Select("FMD_FCD_ID = " & FCD_ID).Length > 0 Then 'if already selected and edited before, now selected again for re editing
                    Dim dv As DataView = New DataView(ViewState("MONTHLY_D"))
                    dv.RowFilter = "FMD_FCD_ID = " & FCD_ID
                    ViewState("gvMonthly") = dv.ToTable
                Else
                    Dim dt As DataTable = clsConcessionCancellation.GET_FEE_CONCESSIONMONTHLY_D(FCD_ID) 'the viewstate may contain details of another fee type only, so get from concession table data
                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        ViewState("gvMonthly") = dt
                    End If
                End If
            End If
            ViewState("FCD_ID") = FCD_ID
            BindgvMonthly()

            'Dim dtCONCESION_D As DataTable = clsConcessionCancellation.GET_FEE_CONCESION_D(0, lblFCD_ID.Text)
            Dim FCD_FEE_ID As String = gvFeeDetails.DataKeys(gvr.RowIndex)("FCD_FEE_ID")
            Dim FCD_ACTUALAMT As String = gvFeeDetails.DataKeys(gvr.RowIndex)("FCD_ACTUALAMT")
            Dim FCD_AMTTYPE As String = gvFeeDetails.DataKeys(gvr.RowIndex)("FCD_AMTTYPE")
            Dim lblgvFeeDetailsAMT As Label = gvr.FindControl("lblgvFeeDetailsAMT")
            ddlFeeType.SelectedIndex = -1
            ddlFeeType.Items.FindByValue(FCD_FEE_ID).Selected = True


            Dim FCD_ISINCLUSIVE_TAX As Boolean = gvFeeDetails.DataKeys(gvr.RowIndex)("FCD_ISINCLUSIVE_TAX")
            Dim FCD_TAX_CODE As String = gvFeeDetails.DataKeys(gvr.RowIndex)("FCD_TAX_CODE")
            If FCD_ISINCLUSIVE_TAX = True Then
                rblTaxCalculation.SelectedValue = "I"
            Else
                rblTaxCalculation.SelectedValue = "E"
            End If
            ddlTAX.SelectedValue = FCD_TAX_CODE


            txtAmount.Text = FeeCollection.GetDoubleVal(lblgvFeeDetailsAMT.Text)
            txtTotalFee.Text = FeeCollection.GetDoubleVal(FCD_ACTUALAMT)
            Select Case FCD_AMTTYPE
                Case 1
                    radAmount.Checked = True
                    radPercentage.Checked = False
                Case 2
                    radAmount.Checked = False
                    radPercentage.Checked = True
            End Select
            txtPercAmountCancel.Text = txtAmount.Text
            btnDetAdd.Text = "Update"
            EnabelYN(True)
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        End If
    End Sub
    Protected Sub lnkBtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim gvr As GridViewRow = sender.parent.parent
        If Not gvr Is Nothing Then
            Dim FCD_ID As String = gvFeeDetails.DataKeys(gvr.RowIndex)("FCD_ID")
            Dim dtgvFeeDetails As DataTable = DirectCast(ViewState("gvFeeDetails"), DataTable)
            If Not dtgvFeeDetails Is Nothing AndAlso dtgvFeeDetails.Rows.Count > 0 And dtgvFeeDetails.Columns.Contains("FCD_bDELETE") Then
                For Each DR In dtgvFeeDetails.Rows
                    If DR("FCD_ID") = FCD_ID Then
                        DR("FCD_bDELETE") = True
                    End If
                Next
            End If
            ViewState("gvFeeDetails") = dtgvFeeDetails
            BindgvFeeDetails()
        End If
    End Sub
    Protected Sub lnkFill_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' ShowMessage("")
        ValidateAndUpdate()
    End Sub

    Protected Sub h_STUD_ID_ValueChanged(sender As Object, e As EventArgs) Handles h_STUD_ID.ValueChanged
        ClearAll()
        LoadStudentData()
    End Sub

    Protected Sub btnDetAdd_Click(sender As Object, e As EventArgs) Handles btnDetAdd.Click
        If btnDetAdd.Text = "Add" Then
            Exit Sub
        End If
        For Each gvr As GridViewRow In gvMonthly.Rows
            Dim txtChargeDate As Label = CType(gvr.FindControl("txtChargeDate"), Label)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)

            Dim lblTaxAmount As Label = CType(gvr.FindControl("lblTaxAmount"), Label)
            Dim lblNetAmount As Label = CType(gvr.FindControl("lblNetAmount"), Label)

            Dim dblActualAmount As Double = FeeCollection.GetDoubleVal(gvMonthly.DataKeys(gvr.RowIndex)("FMD_ORG_AMOUNT")) ' gvr.Cells(3).Text
            Dim FMD_ID As Integer = gvMonthly.DataKeys(gvr.RowIndex)("FMD_ID")
            If Not txtAmount Is Nothing AndAlso Not txtChargeDate Is Nothing Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    'ShowMessage("Invalid Amount/Date!!!")
                    usrMessageBar.ShowNotification("Invalid Amount/Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                    txtAmount.Focus()
                    Exit Sub
                Else
                    If dblActualAmount < FeeCollection.GetDoubleVal(txtAmount.Text) Then
                        'ShowMessage("Concession amount exceeds actual Concession amount !!!")
                        usrMessageBar.ShowNotification("Concession amount exceeds actual Concession amount !!!", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                End If
                For Each dr As DataRow In DirectCast(ViewState("gvMonthly"), DataTable).Rows
                    If dr("FMD_ID") = FMD_ID Then
                        dr("FMD_AMOUNT") = FeeCollection.GetDoubleVal(txtAmount.Text)
                        dr("FMD_TAX_AMOUNT") = FeeCollection.GetDoubleVal(lblTaxAmount.Text)
                        dr("FMD_NET_AMOUNT") = FeeCollection.GetDoubleVal(lblNetAmount.Text)

                    End If
                Next
            End If
        Next
        If btnDetAdd.Text = "Update" Then
            Dim dtFeeDetails As DataTable = ViewState("gvFeeDetails")
            For Each drr As DataRow In dtFeeDetails.Rows
                If drr("FCD_ID") = ViewState("FCD_ID") Then
                    drr("AMOUNT") = FeeCollection.GetDoubleVal(txtPercAmountCancel.Text)
                End If
            Next
            If Not ViewState("MONTHLY_D") Is Nothing Then
                Dim dtMONTHLY_D As DataTable = ViewState("MONTHLY_D")

                If dtMONTHLY_D.Rows.Count <= 0 Then
                    dtMONTHLY_D = ViewState("gvMonthly")
                Else
                    Dim dvM As DataView = New DataView(dtMONTHLY_D)
                    dvM.RowFilter = "CONVERT(ISNULL(FMD_FCD_ID, ''), System.String) <> '" & ViewState("FCD_ID").ToString & "'"
                    dtMONTHLY_D = dvM.ToTable
                    dtMONTHLY_D.Merge(ViewState("gvMonthly"))
                End If
                ViewState("MONTHLY_D") = dtMONTHLY_D
            Else
                ViewState("MONTHLY_D") = ViewState("gvMonthly")
            End If

            ViewState("gvFeeDetails") = dtFeeDetails
            ClearSubDetails()
            EnabelYN(False)
            BindgvFeeDetails()
            ViewState("gvMonthly") = Nothing
            ViewState("FCD_ID") = Nothing
        End If
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "edit")
    End Sub

    Protected Sub btnDetCancel_Click(sender As Object, e As EventArgs) Handles btnDetCancel.Click
        ClearSubDetails()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If ValidateSave() Then
            Dim trans As SqlTransaction
            Try
                Dim objclsConcession As New clsConcessionCancellation
                Dim pFCH_ID As Integer = IIf(FCH_ID = 0, ddlConcession.SelectedValue, FCH_ID)
                objclsConcession.GetConcesionDetails(Session("sBsuId"), pFCH_ID)
                objclsConcession.FCH_ID = FCH_ID
                If FCH_ID = 0 Then
                    objclsConcession.FCH_FCH_ID = ddlConcession.SelectedValue
                End If
                objclsConcession.FCH_ACD_ID = ddlAcademicYear.SelectedValue
                objclsConcession.FCH_BSU_ID = Session("sbsuid")
                objclsConcession.FCH_STU_ID = h_STUD_ID.Value
                objclsConcession.FCH_DT = DateValue(txtDate.Text)
                objclsConcession.FCH_DTFROM = DateValue(txtFromDT.Text)
                objclsConcession.FCH_DTTO = DateValue(txtToDT.Text)

                objclsConcession.STU_NAME = txtStud_Name.Text
                objclsConcession.FCH_DRCR = "DR"
                objclsConcession.FCH_USER = Session("sUsr_name").ToString
                objclsConcession.CONCESSION_D = ViewState("gvFeeDetails")
                objclsConcession.CONCESSIONMONTHLY_D = ViewState("MONTHLY_D")
                Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
                    trans = conn.BeginTransaction("FEE_CONCESSION_TRANS")
                    Try
                        Dim NEW_FCH_ID As String = ""
                        Dim RetVal As Integer = objclsConcession.F_SaveFEE_CONCESSION_H(NEW_FCH_ID, conn, trans)
                        If RetVal <> 0 Then
                            trans.Rollback()
                            'ShowMessage(UtilityObj.getErrorMessage(RetVal))
                            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(RetVal), UserControls_usrMessageBar.WarningType.Danger)
                        Else
                            trans.Commit()
                            'ShowMessage("Concession cancellation has been " & IIf(FCH_ID = 0, "saved", "updated") & " successfully.", False)
                            usrMessageBar.ShowNotification("Concession cancellation has been " & IIf(FCH_ID = 0, "saved", "updated") & " successfully.", UserControls_usrMessageBar.WarningType.Success)
                            FCH_ID = NEW_FCH_ID
                            ViewState("datamode") = "view"
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            Dim str_KEY As String = "INSERT"
                            If ViewState("datamode") <> "edit" Then
                                str_KEY = "EDIT"
                            End If
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, NEW_FCH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page, txtRemarks.Text)
                        End If
                    Catch ex As Exception
                        trans.Rollback()
                        'ShowMessage(ex.Message)
                        usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                        UtilityObj.Errorlog("Concession cancellation" & ex.Message, "OASIS")
                    End Try
                End Using
            Catch ex As Exception
                'ShowMessage(ex.Message)
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                UtilityObj.Errorlog("Concession cancellation" & ex.Message, "OASIS")
            End Try
        End If
    End Sub

    Protected Sub lbtnClearStudent_Click(sender As Object, e As EventArgs) Handles lbtnClearStudent.Click
        txtStdNo.Text = ""
        txtStud_Name.Text = ""
        h_STUD_ID.Value = ""
        ClearAll()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            lbtnClearStudent_Click(Nothing, Nothing)
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        lbtnClearStudent_Click(Nothing, Nothing)
        ClearSubDetails()
        pnlStudents.Enabled = True
        ddlConcession.Enabled = True
        ddlAcademicYear.Enabled = True
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If FCH_ID <> 0 Then
            Try
                Dim trans As SqlTransaction
                Dim objclsConcession As New clsConcessionCancellation
                Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
                    trans = conn.BeginTransaction("FEE_CONCESSION_TRANS")
                    Try
                        Dim NEW_FCH_ID As String = ""
                        objclsConcession.FCH_ID = FCH_ID
                        objclsConcession.FCH_ACD_ID = ddlAcademicYear.SelectedValue
                        objclsConcession.FCH_BSU_ID = Session("sbsuid")
                        objclsConcession.FCH_STU_ID = h_STUD_ID.Value
                        objclsConcession.FCH_DT = DateValue(txtDate.Text)
                        objclsConcession.FCH_DTFROM = DateValue(txtFromDT.Text)
                        objclsConcession.FCH_DTTO = DateValue(txtToDT.Text)
                        objclsConcession.FCH_FCM_ID = 0
                        objclsConcession.FCH_REF_ID = 0
                        objclsConcession.STU_NAME = txtStud_Name.Text
                        objclsConcession.FCH_DRCR = ""
                        objclsConcession.FCH_USER = Session("sUsr_name").ToString
                        objclsConcession.FCH_REMARKS = txtRemarks.Text
                        Dim RetVal As Integer = objclsConcession.F_SaveFEE_CONCESSION_H(NEW_FCH_ID, conn, trans, True)
                        If RetVal <> 0 Then
                            trans.Rollback()
                            'ShowMessage(UtilityObj.getErrorMessage(RetVal))
                            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(RetVal), UserControls_usrMessageBar.WarningType.Danger)
                        Else
                            trans.Commit()
                            ViewState("datamode") = "view"
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            Dim str_KEY As String = "DELETE"
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, FCH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page, txtRemarks.Text)
                            FCH_ID = 0
                            lbtnClearStudent_Click(Nothing, Nothing)
                            ClearSubDetails()
                            'ShowMessage("Concession cancellation has been deleted successfully.", False)
                            usrMessageBar.ShowNotification("Concession cancellation has been deleted successfully.", UserControls_usrMessageBar.WarningType.Success)
                        End If
                    Catch ex As Exception
                        trans.Rollback()
                        'ShowMessage(ex.Message)
                        usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                        UtilityObj.Errorlog("Concession cancellation" & ex.Message, "OASIS")
                    End Try
                End Using
            Catch ex As Exception

            End Try
        Else
            'ShowMessage("Concession cancellation details not found to delete.")
            usrMessageBar.ShowNotification("Concession cancellation details not found to delete.", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If ViewState("datamode") = "view" Then
            PrintConcessionCancel(FCH_ID)
        End If
    End Sub
    Private Sub PrintConcessionCancel(ByVal IntFCH_ID As Integer)
        Session("ReportSource") = FEEConcessionTransaction.PrintConcessionCancel(IntFCH_ID, Session("sBsuid"), Session("sUsr_name"))
        h_print.Value = "print"
    End Sub
End Class
